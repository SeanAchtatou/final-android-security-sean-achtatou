package X;

import android.content.Context;
import android.content.IntentFilter;
import android.net.Uri;
import com.facebook.acra.ACRA;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.forker.Process;
import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.notify.DirectMessageStorySeenNotification;
import com.facebook.messaging.notify.EventReminderNotification;
import com.facebook.messaging.notify.FailedToSendMessageNotification;
import com.facebook.messaging.notify.FailedToSetProfilePictureNotification;
import com.facebook.messaging.notify.FriendInstallNotification;
import com.facebook.messaging.notify.JoinRequestNotification;
import com.facebook.messaging.notify.LoggedOutMessageNotification;
import com.facebook.messaging.notify.MessageReactionNotification;
import com.facebook.messaging.notify.MessageRequestNotification;
import com.facebook.messaging.notify.MessengerLivingRoomCreateNotification;
import com.facebook.messaging.notify.MessengerRoomInviteReminderNotification;
import com.facebook.messaging.notify.MissedCallNotification;
import com.facebook.messaging.notify.MontageMessageNotification;
import com.facebook.messaging.notify.MultipleAccountsNewMessagesNotification;
import com.facebook.messaging.notify.NewBuildNotification;
import com.facebook.messaging.notify.OmniMNotification;
import com.facebook.messaging.notify.PageMessageNotification;
import com.facebook.messaging.notify.PaymentNotification;
import com.facebook.messaging.notify.ReadThreadNotification;
import com.facebook.messaging.notify.SimpleMessageNotification;
import com.facebook.messaging.notify.StaleNotification;
import com.facebook.messaging.notify.TalkMessagingNotification;
import com.facebook.messaging.notify.UriNotification;
import com.facebook.messaging.notify.VideoChatLinkJoinAttemptNotification;
import com.facebook.messaging.notify.type.MessagingNotification;
import com.facebook.messaging.notify.type.NewMessageNotification;
import com.facebook.orca.notify.LoggedOutNotification;
import com.facebook.orca.notify.SwitchToFbAccountNotification;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.push.constants.PushProperty;
import java.util.Iterator;
import java.util.List;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.210  reason: invalid class name */
public final class AnonymousClass210 implements AnonymousClass1YQ {
    public static final AnonymousClass1Y7 A0B = ((AnonymousClass1Y7) C05690aA.A0m.A09("processed_logout_notification"));
    private static volatile AnonymousClass210 A0C;
    public AnonymousClass0UN A00;
    public final Context A01;
    public final C06880cE A02;
    public final C11330mk A03 = ((AnonymousClass1CU) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AuL, this.A00)).A01("notification_instance");
    public final Boolean A04;
    public final C04310Tq A05;
    public final C04310Tq A06;
    public final C04310Tq A07;
    private final AnonymousClass2NX A08 = new C404121m(this);
    private final AnonymousClass2NX A09 = new AnonymousClass2GD(this);
    public volatile FolderCounts A0A;

    public String getSimpleName() {
        return "MessagesNotificationManager";
    }

    public static final AnonymousClass210 A00(AnonymousClass1XY r4) {
        if (A0C == null) {
            synchronized (AnonymousClass210.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r4);
                if (A002 != null) {
                    try {
                        A0C = new AnonymousClass210(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    public static void A01(AnonymousClass210 r7, MessagingNotification messagingNotification) {
        Iterator it = ((C60052wU) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AMa, r7.A00)).iterator();
        while (it.hasNext()) {
            C60072wW r4 = (C60072wW) it.next();
            C005505z.A06("%s:%s", r4.A0I(), "beforeNotify", -1545743454);
            try {
                switch (messagingNotification.A01.ordinal()) {
                    case 0:
                    case 1:
                        r4.A0t((NewMessageNotification) messagingNotification);
                        break;
                    case 3:
                        r4.A0u((LoggedOutNotification) messagingNotification);
                        break;
                }
            } finally {
                C005505z.A00(1275664087);
            }
        }
        Iterator it2 = ((C60052wU) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AMa, r7.A00)).iterator();
        while (it2.hasNext()) {
            C60072wW r2 = (C60072wW) it2.next();
            if (!messagingNotification.A00) {
                C005505z.A06("%s:%s", r2.A0I(), "notify", 1225124623);
                try {
                    switch (messagingNotification.A01.ordinal()) {
                        case 0:
                        case 1:
                            r2.A11((NewMessageNotification) messagingNotification);
                            break;
                        case 2:
                            r2.A0U((LoggedOutMessageNotification) messagingNotification);
                            break;
                        case 3:
                            r2.A0u((LoggedOutNotification) messagingNotification);
                            break;
                        case 4:
                            r2.A0S((FriendInstallNotification) messagingNotification);
                            break;
                        case 5:
                            r2.A0Q((FailedToSendMessageNotification) messagingNotification);
                            break;
                        case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                            r2.A0k((ReadThreadNotification) messagingNotification);
                            break;
                        case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                            r2.A0g((NewBuildNotification) messagingNotification);
                            break;
                        case 8:
                            r2.A0j((PaymentNotification) messagingNotification);
                            break;
                        case Process.SIGKILL:
                            r2.A0q((UriNotification) messagingNotification);
                            break;
                        case AnonymousClass1Y3.A01:
                            r2.A0o((StaleNotification) messagingNotification);
                            break;
                        case AnonymousClass1Y3.A02:
                            r2.A0l((SimpleMessageNotification) messagingNotification);
                            break;
                        case AnonymousClass1Y3.A03:
                            r2.A10((MissedCallNotification) messagingNotification);
                            break;
                        case 13:
                            r2.A0W((MessageRequestNotification) messagingNotification);
                            break;
                        case 14:
                            r2.A0m((SimpleMessageNotification) messagingNotification);
                            break;
                        case 16:
                            r2.A0f((MultipleAccountsNewMessagesNotification) messagingNotification);
                            break;
                        case 17:
                            r2.A0V((MessageReactionNotification) messagingNotification);
                            break;
                        case Process.SIGCONT:
                            r2.A0T((JoinRequestNotification) messagingNotification);
                            break;
                        case Process.SIGSTOP:
                            r2.A0Y((MessengerRoomInviteReminderNotification) messagingNotification);
                            break;
                        case 20:
                            r2.A0v((SwitchToFbAccountNotification) messagingNotification);
                            break;
                        case AnonymousClass1Y3.A05:
                            r2.A0P((EventReminderNotification) messagingNotification);
                            break;
                        case AnonymousClass1Y3.A06:
                            r2.A0R((FailedToSetProfilePictureNotification) messagingNotification);
                            break;
                        case 23:
                            r2.A0X((MessengerLivingRoomCreateNotification) messagingNotification);
                            break;
                        case AnonymousClass1Y3.A07:
                            r2.A0a((MontageMessageNotification) messagingNotification);
                            break;
                        case 25:
                            if (!(messagingNotification instanceof MontageMessageNotification)) {
                                break;
                            } else {
                                r2.A0Z((MontageMessageNotification) messagingNotification);
                                break;
                            }
                        case AnonymousClass1Y3.A08:
                            if (!(messagingNotification instanceof MontageMessageNotification)) {
                                break;
                            } else {
                                r2.A0c((MontageMessageNotification) messagingNotification);
                                break;
                            }
                        case AnonymousClass1Y3.A09:
                            r2.A0e((MontageMessageNotification) messagingNotification);
                            break;
                        case 28:
                            r2.A0b((MontageMessageNotification) messagingNotification);
                            break;
                        case 29:
                            r2.A0d((MontageMessageNotification) messagingNotification);
                            break;
                        case AnonymousClass1Y3.A0A:
                            r2.A0h((OmniMNotification) messagingNotification);
                            break;
                        case AnonymousClass1Y3.A0B:
                            r2.A0n((SimpleMessageNotification) messagingNotification);
                            break;
                        case 32:
                            r2.A0i((PageMessageNotification) messagingNotification);
                            break;
                        case 33:
                            r2.A0p((TalkMessagingNotification) messagingNotification);
                            break;
                        case 35:
                            r2.A0O((DirectMessageStorySeenNotification) messagingNotification);
                            break;
                        case 36:
                            r2.A0r((VideoChatLinkJoinAttemptNotification) messagingNotification);
                            break;
                    }
                } finally {
                    C005505z.A00(-920317266);
                }
            } else {
                return;
            }
        }
    }

    public static void A02(AnonymousClass210 r9, MessagingNotification messagingNotification) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        C57112rR r92 = (C57112rR) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AZp, r9.A00);
        PushProperty pushProperty = messagingNotification.A02;
        String str6 = null;
        if (pushProperty != null) {
            str = pushProperty.A02.toString();
        } else {
            str = null;
        }
        if (pushProperty != null) {
            str2 = pushProperty.A06;
        } else {
            str2 = null;
        }
        if (pushProperty != null) {
            str3 = pushProperty.A05;
        } else {
            str3 = null;
        }
        if (pushProperty != null) {
            str4 = pushProperty.A08;
        } else {
            str4 = null;
        }
        if (pushProperty != null) {
            str5 = pushProperty.A04;
        } else {
            str5 = null;
        }
        if (pushProperty != null) {
            str6 = pushProperty.A03;
        }
        r92.A02.A02(StringFormatUtil.formatStrLocaleSafe("%s-%s-%s", "notif_received", str, str2));
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r92.A04.A01("notif_received"), AnonymousClass1Y3.A3t);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("client_notif_type", messagingNotification.A01.toString());
            uSLEBaseShape0S0000000.A0Q(str);
            uSLEBaseShape0S0000000.A0D("notif_type", str2);
            uSLEBaseShape0S0000000.A0D("push_id", str3);
            uSLEBaseShape0S0000000.A0D("sender_id", str4);
            uSLEBaseShape0S0000000.A0D("message_id", str5);
            uSLEBaseShape0S0000000.A0D("delivery_id", str6);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public static boolean A03(AnonymousClass210 r3) {
        return ((C14780u1) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BLe, r3.A00)).A02().A03();
    }

    public void A04() {
        Iterator it = ((C60052wU) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AMa, this.A00)).iterator();
        while (it.hasNext()) {
            ((C60072wW) it.next()).A0J();
        }
    }

    public void A05() {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, this.A00)).AOx();
        A01(this, new FailedToSetProfilePictureNotification(this.A01.getString(2131821487), this.A01.getString(2131827713), this.A01.getString(2131827712)));
    }

    public void A06(ThreadKey threadKey, String str) {
        Iterator it = ((C60052wU) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AMa, this.A00)).iterator();
        while (it.hasNext()) {
            ((C60072wW) it.next()).A0N(threadKey, str);
        }
        C69703Ym.A01(threadKey, this.A02);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0379, code lost:
        if (r11.A01 != false) goto L_0x037b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x03ba, code lost:
        if (r1 != false) goto L_0x03bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01fc, code lost:
        if (r12 != X.C10950l8.A06) goto L_0x01fe;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0C(com.facebook.messaging.notify.type.NewMessageNotification r22) {
        /*
            r21 = this;
            int r2 = X.AnonymousClass1Y3.APr
            r6 = r21
            X.0UN r1 = r6.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Y6 r0 = (X.AnonymousClass1Y6) r0
            r0.AOx()
            monitor-enter(r6)
            r4 = 406(0x196, float:5.69E-43)
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            r3 = 14
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r2 = 3473429(0x350015, float:4.867311E-39)
            r0.markerStart(r2)     // Catch:{ all -> 0x0485 }
            int r1 = X.AnonymousClass1Y3.AZp     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            r8 = 5
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2rR r7 = (X.C57112rR) r7     // Catch:{ all -> 0x0476 }
            r5 = r22
            boolean r0 = r5.A05()     // Catch:{ all -> 0x0476 }
            if (r0 == 0) goto L_0x015f
            com.facebook.push.constants.PushProperty r11 = r5.A02     // Catch:{ all -> 0x0476 }
            r9 = 0
            if (r11 == 0) goto L_0x0044
            X.31X r0 = r11.A02     // Catch:{ all -> 0x0476 }
            java.lang.String r1 = r0.toString()     // Catch:{ all -> 0x0476 }
            goto L_0x0045
        L_0x0044:
            r1 = r9
        L_0x0045:
            if (r11 == 0) goto L_0x004a
            java.lang.String r10 = r11.A06     // Catch:{ all -> 0x0476 }
            goto L_0x004b
        L_0x004a:
            r10 = r9
        L_0x004b:
            if (r11 == 0) goto L_0x0052
            java.lang.String r0 = r11.A05     // Catch:{ all -> 0x0476 }
            r20 = r0
            goto L_0x0054
        L_0x0052:
            r20 = r9
        L_0x0054:
            if (r11 == 0) goto L_0x005b
            java.lang.String r0 = r11.A08     // Catch:{ all -> 0x0476 }
            r19 = r0
            goto L_0x005d
        L_0x005b:
            r19 = r9
        L_0x005d:
            if (r11 == 0) goto L_0x0064
            java.lang.String r0 = r11.A04     // Catch:{ all -> 0x0476 }
            r18 = r0
            goto L_0x0066
        L_0x0064:
            r18 = r9
        L_0x0066:
            if (r11 == 0) goto L_0x006a
            java.lang.String r9 = r11.A03     // Catch:{ all -> 0x0476 }
        L_0x006a:
            X.0dK r12 = r7.A02     // Catch:{ all -> 0x0476 }
            java.lang.String r11 = "%s-%s-%s"
            java.lang.String r0 = "notif_received"
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r11, r0, r1, r10)     // Catch:{ all -> 0x0476 }
            r12.A02(r0)     // Catch:{ all -> 0x0476 }
            com.facebook.messaging.push.flags.ServerMessageAlertFlags r13 = r5.A03     // Catch:{ all -> 0x0476 }
            r12 = 0
            int r11 = X.AnonymousClass1Y3.AmL     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r12, r11, r0)     // Catch:{ all -> 0x0476 }
            X.0jJ r0 = (X.AnonymousClass0jJ) r0     // Catch:{ all -> 0x0476 }
            com.fasterxml.jackson.databind.node.ObjectNode r12 = r0.createObjectNode()     // Catch:{ all -> 0x0476 }
            X.3da r0 = r5.A02     // Catch:{ all -> 0x0476 }
            java.lang.String r11 = r0.name()     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "presence_level"
            r12.put(r0, r11)     // Catch:{ all -> 0x0476 }
            if (r13 == 0) goto L_0x00b1
            boolean r11 = r13.A01     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "disable_sound"
            r12.put(r0, r11)     // Catch:{ all -> 0x0476 }
            boolean r11 = r13.A02     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "disable_vibrate"
            r12.put(r0, r11)     // Catch:{ all -> 0x0476 }
            boolean r11 = r13.A00     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "disable_led"
            r12.put(r0, r11)     // Catch:{ all -> 0x0476 }
            boolean r11 = r13.A03     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "aggressive_notify"
            r12.put(r0, r11)     // Catch:{ all -> 0x0476 }
        L_0x00b1:
            java.lang.String r17 = r12.toString()     // Catch:{ all -> 0x0476 }
            com.facebook.messaging.model.messages.Message r0 = r5.A00     // Catch:{ all -> 0x0476 }
            java.lang.String r12 = r0.A10     // Catch:{ all -> 0x0476 }
            boolean r0 = X.C06850cB.A0B(r12)     // Catch:{ all -> 0x0476 }
            if (r0 == 0) goto L_0x00c1
            r12 = 0
            goto L_0x00f4
        L_0x00c1:
            int r14 = r12.length()     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = ": "
            int r15 = r12.indexOf(r0)     // Catch:{ all -> 0x0476 }
            r0 = -1
            r16 = 0
            r13 = 2
            int r11 = r15 + r13
            if (r15 != r0) goto L_0x00d4
            r11 = 0
        L_0x00d4:
            int r0 = r11 + 4
            if (r14 <= r0) goto L_0x00f4
            java.lang.StringBuffer r15 = new java.lang.StringBuffer     // Catch:{ all -> 0x0476 }
            r15.<init>()     // Catch:{ all -> 0x0476 }
            char[] r12 = r12.toCharArray()     // Catch:{ all -> 0x0476 }
            int r11 = r11 + r13
            r0 = r16
            r15.append(r12, r0, r11)     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "..."
            r15.append(r0)     // Catch:{ all -> 0x0476 }
            int r14 = r14 - r13
            r15.append(r12, r14, r13)     // Catch:{ all -> 0x0476 }
            java.lang.String r12 = r15.toString()     // Catch:{ all -> 0x0476 }
        L_0x00f4:
            X.1ZE r11 = r7.A04     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "notif_received"
            X.0bW r13 = r11.A01(r0)     // Catch:{ all -> 0x0476 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r11 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x0476 }
            r0 = 469(0x1d5, float:6.57E-43)
            r11.<init>(r13, r0)     // Catch:{ all -> 0x0476 }
            boolean r0 = r11.A0G()     // Catch:{ all -> 0x0476 }
            if (r0 == 0) goto L_0x0147
            if (r12 == 0) goto L_0x0110
            java.lang.String r0 = "message"
            r11.A0D(r0, r12)     // Catch:{ all -> 0x0476 }
        L_0x0110:
            java.lang.String r12 = "debug_info"
            r0 = r17
            r11.A0D(r12, r0)     // Catch:{ all -> 0x0476 }
            X.327 r0 = r5.A01     // Catch:{ all -> 0x0476 }
            java.lang.String r12 = r0.toString()     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "client_notif_type"
            r11.A0D(r0, r12)     // Catch:{ all -> 0x0476 }
            r11.A0Q(r1)     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "notif_type"
            r11.A0D(r0, r10)     // Catch:{ all -> 0x0476 }
            java.lang.String r10 = "push_id"
            r0 = r20
            r11.A0D(r10, r0)     // Catch:{ all -> 0x0476 }
            java.lang.String r10 = "sender_id"
            r0 = r19
            r11.A0D(r10, r0)     // Catch:{ all -> 0x0476 }
            java.lang.String r10 = "message_id"
            r0 = r18
            r11.A0D(r10, r0)     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "delivery_id"
            r11.A0D(r0, r9)     // Catch:{ all -> 0x0476 }
            r11.A06()     // Catch:{ all -> 0x0476 }
        L_0x0147:
            java.lang.String r9 = "[notif_received]"
            java.lang.String r10 = " source = "
            java.lang.String r12 = "; notifId = "
            java.lang.String r14 = "; msgId = "
            java.lang.String r16 = "; debugInfo = "
            r11 = r1
            r13 = r20
            r15 = r18
            java.lang.String r1 = X.AnonymousClass08S.A0W(r9, r10, r11, r12, r13, r14, r15, r16, r17)     // Catch:{ all -> 0x0476 }
            X.0mk r0 = r7.A01     // Catch:{ all -> 0x0476 }
            r0.BIo(r1)     // Catch:{ all -> 0x0476 }
        L_0x015f:
            com.facebook.messaging.model.messages.Message r9 = r5.A00     // Catch:{ all -> 0x0476 }
            com.facebook.push.constants.PushProperty r11 = r5.A02     // Catch:{ all -> 0x0476 }
            com.facebook.messaging.model.threadkey.ThreadKey r10 = r9.A0U     // Catch:{ all -> 0x0476 }
            r7 = 0
            if (r10 != 0) goto L_0x0186
            int r1 = X.AnonymousClass1Y3.AZp     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2rR r1 = (X.C57112rR) r1     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "d2_null_threadkey"
            r1.A0F(r0, r5, r7)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
            goto L_0x045d
        L_0x0186:
            r12 = 9
            int r1 = X.AnonymousClass1Y3.BMo     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r12, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2c5 r0 = (X.C49362c5) r0     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = r0.A01()     // Catch:{ all -> 0x0476 }
            boolean r0 = X.C06850cB.A0B(r0)     // Catch:{ all -> 0x0476 }
            if (r0 == 0) goto L_0x01ba
            int r1 = X.AnonymousClass1Y3.AZp     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2rR r1 = (X.C57112rR) r1     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "d2_no_user"
            r1.A0F(r0, r5, r7)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
            goto L_0x045d
        L_0x01ba:
            r12 = 4
            int r1 = X.AnonymousClass1Y3.ArW     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r12, r1, r0)     // Catch:{ all -> 0x0476 }
            X.0XN r0 = (X.AnonymousClass0XN) r0     // Catch:{ all -> 0x0476 }
            boolean r0 = r0.A0I()     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x01e9
            int r1 = X.AnonymousClass1Y3.AZp     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2rR r1 = (X.C57112rR) r1     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "d2_logged_out_user"
            r1.A0F(r0, r5, r7)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
            goto L_0x045d
        L_0x01e9:
            X.0Tq r0 = r6.A05     // Catch:{ all -> 0x0476 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0476 }
            X.1bq r0 = (X.C26681bq) r0     // Catch:{ all -> 0x0476 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A08(r10)     // Catch:{ all -> 0x0476 }
            if (r0 == 0) goto L_0x01fe
            X.0l8 r12 = r0.A0O     // Catch:{ all -> 0x0476 }
            X.0l8 r1 = X.C10950l8.A06     // Catch:{ all -> 0x0476 }
            r0 = 1
            if (r12 == r1) goto L_0x01ff
        L_0x01fe:
            r0 = 0
        L_0x01ff:
            if (r0 == 0) goto L_0x021f
            int r1 = X.AnonymousClass1Y3.AZp     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2rR r1 = (X.C57112rR) r1     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "d2_disabled_for_montage"
            r1.A0G(r0, r5, r7)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
            goto L_0x045d
        L_0x021f:
            boolean r0 = A03(r6)     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x0250
            int r1 = X.AnonymousClass1Y3.AZp     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2rR r1 = (X.C57112rR) r1     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "d2_global_disabled"
            r1.A0G(r0, r5, r7)     // Catch:{ all -> 0x0476 }
            X.0mk r5 = r6.A03     // Catch:{ all -> 0x0476 }
            java.lang.String r1 = "GlobalDisabled:"
            java.lang.String r0 = r9.A0q     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0476 }
            r5.BIo(r0)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
            goto L_0x045d
        L_0x0250:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0U     // Catch:{ all -> 0x0476 }
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r0)     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x02b4
            X.0Tq r0 = r6.A05     // Catch:{ all -> 0x0476 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0476 }
            X.1bq r0 = (X.C26681bq) r0     // Catch:{ all -> 0x0476 }
            boolean r0 = r0.A0G(r9)     // Catch:{ all -> 0x0476 }
            if (r0 == 0) goto L_0x02b4
            r9 = 6
            int r1 = X.AnonymousClass1Y3.BQ3     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x0476 }
            X.0dK r9 = (X.C07380dK) r9     // Catch:{ all -> 0x0476 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "message_read-"
            r1.<init>(r0)     // Catch:{ all -> 0x0476 }
            if (r11 != 0) goto L_0x027c
            r0 = r7
            goto L_0x027e
        L_0x027c:
            X.31X r0 = r11.A02     // Catch:{ all -> 0x0476 }
        L_0x027e:
            r1.append(r0)     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "-"
            r1.append(r0)     // Catch:{ all -> 0x0476 }
            if (r11 != 0) goto L_0x028a
            r0 = r7
            goto L_0x028c
        L_0x028a:
            java.lang.String r0 = r11.A06     // Catch:{ all -> 0x0476 }
        L_0x028c:
            r1.append(r0)     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0476 }
            r9.A02(r0)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.AZp     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2rR r1 = (X.C57112rR) r1     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "d2_read_locally"
            r1.A0G(r0, r5, r7)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
            goto L_0x045d
        L_0x02b4:
            r11 = 8
            int r1 = X.AnonymousClass1Y3.BLe     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r11, r1, r0)     // Catch:{ all -> 0x0476 }
            X.0u1 r0 = (X.C14780u1) r0     // Catch:{ all -> 0x0476 }
            com.facebook.messaging.model.threads.NotificationSetting r12 = r0.A04(r10)     // Catch:{ all -> 0x0476 }
            boolean r0 = r5.A05     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x02f9
            boolean r0 = r12.A03()     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x02f9
            int r1 = X.AnonymousClass1Y3.AZp     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            X.2rR r1 = (X.C57112rR) r1     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = "d2_thread_disabled"
            r1.A0G(r0, r5, r7)     // Catch:{ all -> 0x0476 }
            X.0mk r5 = r6.A03     // Catch:{ all -> 0x0476 }
            java.lang.String r1 = "ThreadDisabled:"
            java.lang.String r0 = r9.A0q     // Catch:{ all -> 0x0476 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0476 }
            r5.BIo(r0)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
            goto L_0x045d
        L_0x02f9:
            java.lang.String r1 = "NotifManager.EnsureDisp"
            r0 = 1223571010(0x48ee3642, float:487858.06)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BDQ     // Catch:{ all -> 0x046a }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x046a }
            r7 = 12
            java.lang.Object r14 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x046a }
            X.3dd r14 = (X.C72083dd) r14     // Catch:{ all -> 0x046a }
            X.C72083dd.A01(r14)     // Catch:{ all -> 0x046a }
            java.util.Map r8 = r14.A04     // Catch:{ all -> 0x046a }
            monitor-enter(r8)     // Catch:{ all -> 0x046a }
            boolean r0 = r9.A05()     // Catch:{ all -> 0x0467 }
            if (r0 == 0) goto L_0x0320
            X.32C r11 = new X.32C     // Catch:{ all -> 0x0467 }
            r11.<init>()     // Catch:{ all -> 0x0467 }
        L_0x031e:
            monitor-exit(r8)     // Catch:{ all -> 0x0467 }
            goto L_0x0360
        L_0x0320:
            java.lang.String r13 = r9.A0q     // Catch:{ all -> 0x0467 }
            java.util.Map r0 = r14.A04     // Catch:{ all -> 0x0467 }
            java.lang.Object r11 = r0.get(r13)     // Catch:{ all -> 0x0467 }
            X.32C r11 = (X.AnonymousClass32C) r11     // Catch:{ all -> 0x0467 }
            if (r11 != 0) goto L_0x0347
            if (r13 == 0) goto L_0x035a
            X.1Yd r11 = r14.A02     // Catch:{ all -> 0x0467 }
            r0 = 282943855724619(0x101560003084b, double:1.397928388153964E-309)
            boolean r0 = r11.Aem(r0)     // Catch:{ all -> 0x0467 }
            if (r0 == 0) goto L_0x035a
            X.7me r11 = new X.7me     // Catch:{ all -> 0x0467 }
            com.facebook.prefs.shared.FbSharedPreferences r0 = r14.A03     // Catch:{ all -> 0x0467 }
            r11.<init>(r13, r0)     // Catch:{ all -> 0x0467 }
        L_0x0342:
            java.util.Map r0 = r14.A04     // Catch:{ all -> 0x0467 }
            r0.put(r13, r11)     // Catch:{ all -> 0x0467 }
        L_0x0347:
            r13 = 0
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x0467 }
            X.0UN r0 = r14.A00     // Catch:{ all -> 0x0467 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r13, r1, r0)     // Catch:{ all -> 0x0467 }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x0467 }
            long r0 = r0.now()     // Catch:{ all -> 0x0467 }
            r11.A06(r0)     // Catch:{ all -> 0x0467 }
            goto L_0x031e
        L_0x035a:
            X.32C r11 = new X.32C     // Catch:{ all -> 0x0467 }
            r11.<init>()     // Catch:{ all -> 0x0467 }
            goto L_0x0342
        L_0x0360:
            r0 = 743722615(0x2c544e77, float:3.0170569E-12)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0476 }
            boolean r0 = r11.A02     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x037b
            boolean r0 = r11.A05     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x037b
            boolean r0 = r11.A04     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x037b
            boolean r0 = r11.A03     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x037b
            boolean r1 = r11.A01     // Catch:{ all -> 0x0476 }
            r0 = 0
            if (r1 == 0) goto L_0x037c
        L_0x037b:
            r0 = 1
        L_0x037c:
            if (r0 != 0) goto L_0x037f
            goto L_0x0382
        L_0x037f:
            r4 = 407(0x197, float:5.7E-43)
            goto L_0x0391
        L_0x0382:
            com.facebook.messaging.model.messages.Message r0 = r5.A00     // Catch:{ all -> 0x0476 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0U     // Catch:{ all -> 0x0476 }
            java.lang.String r8 = r9.A0q     // Catch:{ all -> 0x0476 }
            X.0cE r1 = r6.A02     // Catch:{ all -> 0x0476 }
            android.net.Uri r0 = X.C06980cP.A01(r0)     // Catch:{ all -> 0x0476 }
            r1.A06(r0, r8)     // Catch:{ all -> 0x0476 }
        L_0x0391:
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r10)     // Catch:{ all -> 0x0476 }
            r10 = 1
            if (r0 == 0) goto L_0x03b1
            boolean r0 = r12.A02     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x03af
            r8 = 3
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0476 }
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1     // Catch:{ all -> 0x0476 }
            X.1Y7 r0 = X.C10990lD.A04     // Catch:{ all -> 0x0476 }
            boolean r0 = r1.Aep(r0, r10)     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x03b1
        L_0x03af:
            r1 = 1
            goto L_0x03b2
        L_0x03b1:
            r1 = 0
        L_0x03b2:
            boolean r0 = X.C28841fS.A0Y(r9)     // Catch:{ all -> 0x0476 }
            if (r0 != 0) goto L_0x03bc
            r18 = 0
            if (r1 == 0) goto L_0x03be
        L_0x03bc:
            r18 = 1
        L_0x03be:
            com.facebook.messaging.model.messages.Message r10 = r5.A00     // Catch:{ all -> 0x0476 }
            X.3da r9 = r5.A02     // Catch:{ all -> 0x0476 }
            com.facebook.push.constants.PushProperty r8 = r5.A02     // Catch:{ all -> 0x0476 }
            com.facebook.messaging.push.flags.ServerMessageAlertFlags r1 = r5.A03     // Catch:{ all -> 0x0476 }
            boolean r0 = r5.A05     // Catch:{ all -> 0x0476 }
            r12 = 407(0x197, float:5.7E-43)
            com.facebook.messaging.notify.type.NewMessageNotification r5 = new com.facebook.messaging.notify.type.NewMessageNotification     // Catch:{ all -> 0x0476 }
            r13 = r5
            r14 = r10
            r15 = r9
            r16 = r8
            r17 = r11
            r19 = r1
            r20 = r0
            r13.<init>(r14, r15, r16, r17, r18, r19, r20)     // Catch:{ all -> 0x0476 }
            java.lang.String r1 = "NotifManager.dispatch"
            r0 = 943077118(0x383636fe, float:4.3443397E-5)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x0476 }
            A01(r6, r5)     // Catch:{ all -> 0x0462 }
            r0 = -546814013(0xffffffffdf6847c3, float:-1.6737561E19)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0476 }
            int r1 = X.AnonymousClass1Y3.BDQ     // Catch:{ all -> 0x0476 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0476 }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0476 }
            X.3dd r8 = (X.C72083dd) r8     // Catch:{ all -> 0x0476 }
            java.util.Map r5 = r8.A04     // Catch:{ all -> 0x0476 }
            monitor-enter(r5)     // Catch:{ all -> 0x0476 }
            java.util.Map r0 = r8.A04     // Catch:{ all -> 0x045f }
            int r1 = r0.size()     // Catch:{ all -> 0x045f }
            r0 = 100
            if (r1 < r0) goto L_0x043e
            r7 = 0
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x045f }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x045f }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x045f }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x045f }
            long r14 = r0.now()     // Catch:{ all -> 0x045f }
            java.util.Map r0 = r8.A04     // Catch:{ all -> 0x045f }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x045f }
            java.util.Iterator r13 = r0.iterator()     // Catch:{ all -> 0x045f }
        L_0x041b:
            boolean r0 = r13.hasNext()     // Catch:{ all -> 0x045f }
            if (r0 == 0) goto L_0x043e
            java.lang.Object r0 = r13.next()     // Catch:{ all -> 0x045f }
            X.32C r0 = (X.AnonymousClass32C) r0     // Catch:{ all -> 0x045f }
            long r0 = r0.A00     // Catch:{ all -> 0x045f }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x045f }
            long r0 = r0.longValue()     // Catch:{ all -> 0x045f }
            long r9 = r14 - r0
            r7 = 3600000(0x36ee80, double:1.7786363E-317)
            int r0 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r0 <= 0) goto L_0x041b
            r13.remove()     // Catch:{ all -> 0x045f }
            goto L_0x041b
        L_0x043e:
            monitor-exit(r5)     // Catch:{ all -> 0x045f }
            if (r12 == r4) goto L_0x0450
            boolean r0 = r11.A04     // Catch:{ all -> 0x0476 }
            if (r0 == 0) goto L_0x044e
            boolean r0 = r11.A02     // Catch:{ all -> 0x0476 }
            r4 = 408(0x198, float:5.72E-43)
            if (r0 == 0) goto L_0x0450
            r4 = 409(0x199, float:5.73E-43)
            goto L_0x0450
        L_0x044e:
            r4 = 406(0x196, float:5.69E-43)
        L_0x0450:
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
        L_0x045d:
            monitor-exit(r6)
            return
        L_0x045f:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x045f }
            throw r0     // Catch:{ all -> 0x0476 }
        L_0x0462:
            r1 = move-exception
            r0 = -1595549691(0xffffffffa0e5d805, float:-3.893706E-19)
            goto L_0x0472
        L_0x0467:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0467 }
            throw r0     // Catch:{ all -> 0x046a }
        L_0x046a:
            r1 = move-exception
            r0 = -123973415(0xfffffffff89c50d9, float:-2.536369E34)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0476 }
            goto L_0x0475
        L_0x0472:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0476 }
        L_0x0475:
            throw r1     // Catch:{ all -> 0x0476 }
        L_0x0476:
            r5 = move-exception
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0485 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0485 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0485 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0485 }
            r0.markerEnd(r2, r4)     // Catch:{ all -> 0x0485 }
            throw r5     // Catch:{ all -> 0x0485 }
        L_0x0485:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass210.A0C(com.facebook.messaging.notify.type.NewMessageNotification):void");
    }

    public void A0D(String str) {
        Iterator it = ((C60052wU) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AMa, this.A00)).iterator();
        while (it.hasNext()) {
            ((C60072wW) it.next()).A0x(str);
        }
    }

    public void A0E(String str) {
        Iterator it = ((C60052wU) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AMa, this.A00)).iterator();
        while (it.hasNext()) {
            ((C60072wW) it.next()).A0y(str);
        }
    }

    public void A0F(List list) {
        Iterator it = ((C60052wU) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AMa, this.A00)).iterator();
        while (it.hasNext()) {
            ((C60072wW) it.next()).A0z(list);
        }
    }

    private AnonymousClass210(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(19, r4);
        this.A01 = AnonymousClass1YA.A00(r4);
        this.A05 = C26681bq.A05(r4);
        this.A02 = C06880cE.A00(r4);
        this.A04 = AnonymousClass0UU.A08(r4);
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.AUM, r4);
        this.A07 = AnonymousClass0WY.A02(r4);
    }

    public void A07(FriendInstallNotification friendInstallNotification) {
        String str;
        A02(this, friendInstallNotification);
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, this.A00)).AOx();
        String r5 = friendInstallNotification.A02.A02.toString();
        String str2 = friendInstallNotification.A02.A05;
        if (!((AnonymousClass0XN) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ArW, this.A00)).A0I()) {
            ((C56722qj) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B26, this.A00)).A06(r5, str2, "10003", "logged_out_user");
            return;
        }
        ((C56662qd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BSC, this.A00)).A01(friendInstallNotification.A02, true);
        if (!((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A00)).Aep(C05690aA.A0L, true) || !A03(this)) {
            ((C56722qj) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B26, this.A00)).A06(r5, str2, "10003", "notifications_disabled");
            return;
        }
        A01(this, friendInstallNotification);
        if (friendInstallNotification.A00) {
            str = "user_alerted_";
        } else {
            str = "user_not_alerted_";
        }
        ((C56722qj) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B26, this.A00)).A06(r5, str2, "10003", str);
    }

    public void A08(LoggedOutMessageNotification loggedOutMessageNotification) {
        String str;
        A02(this, loggedOutMessageNotification);
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, this.A00)).AOx();
        String r6 = loggedOutMessageNotification.A02.A02.toString();
        String str2 = loggedOutMessageNotification.A02.A05;
        if (!A03(this)) {
            ((C56722qj) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B26, this.A00)).A06(r6, str2, "10004", "notifications_disabled");
            return;
        }
        A01(this, loggedOutMessageNotification);
        if (loggedOutMessageNotification.A00) {
            str = "user_alerted_";
        } else {
            str = "user_not_alerted_";
        }
        ((C56722qj) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B26, this.A00)).A06(r6, str2, "10004", str);
    }

    public void A09(MessageRequestNotification messageRequestNotification) {
        A02(this, messageRequestNotification);
        if (!A03(this)) {
            ThreadKey threadKey = messageRequestNotification.A01;
            PushProperty pushProperty = messageRequestNotification.A02;
            C57112rR r4 = (C57112rR) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AZp, this.A00);
            if (r4 != null) {
                r4.A0E(null, threadKey, pushProperty.A02.toString(), pushProperty.A05, "notifications_disabled");
                return;
            }
            return;
        }
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, this.A00)).AOx();
        A01(this, messageRequestNotification);
    }

    public void A0A(PaymentNotification paymentNotification) {
        String str;
        A02(this, paymentNotification);
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, this.A00)).AOx();
        String r5 = paymentNotification.A02.A02.toString();
        String str2 = paymentNotification.A02.A05;
        if (!((AnonymousClass0XN) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ArW, this.A00)).A0I()) {
            ((C56722qj) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B26, this.A00)).A06(r5, str2, "10014", "logged_out_user");
        } else if (!A03(this)) {
            ((C56722qj) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B26, this.A00)).A06(r5, str2, "10014", "notifications_disabled");
        } else {
            A01(this, paymentNotification);
            if (paymentNotification.A00) {
                str = "user_alerted_";
            } else {
                str = "user_not_alerted_";
            }
            ((C56722qj) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B26, this.A00)).A06(r5, str2, "10014", str);
        }
    }

    public void A0B(UriNotification uriNotification) {
        A02(this, uriNotification);
        if (A03(this) || !((AnonymousClass1YI) AnonymousClass1XX.A02(15, AnonymousClass1Y3.AcD, this.A00)).AbO(86, false)) {
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, this.A00)).AOx();
            A01(this, uriNotification);
        }
    }

    public void init() {
        int A032 = C000700l.A03(-2059099537);
        this.A02.A05(Uri.parse("peer://msg_notification_unread_count/clear_thread"), this.A09);
        this.A02.A05(C06980cP.A06, this.A08);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(C05360Yq.$const$string(3));
        new BST(this, this.A01, intentFilter).A00();
        C72083dd.A01((C72083dd) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BDQ, this.A00));
        C000700l.A09(-263949623, A032);
    }
}
