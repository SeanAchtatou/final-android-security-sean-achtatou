package X;

import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.1fd  reason: invalid class name and case insensitive filesystem */
public final class C28951fd extends C14680to {
    public final short _value;

    public boolean canConvertToInt() {
        return true;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            return obj != null && obj.getClass() == getClass() && ((C28951fd) obj)._value == this._value;
        }
        return true;
    }

    public boolean asBoolean(boolean z) {
        if (this._value != 0) {
            return true;
        }
        return false;
    }

    public String asText() {
        return C48502aX.toString(this._value);
    }

    public C182811d asToken() {
        return C182811d.VALUE_NUMBER_INT;
    }

    public BigInteger bigIntegerValue() {
        return BigInteger.valueOf((long) this._value);
    }

    public BigDecimal decimalValue() {
        return BigDecimal.valueOf((long) this._value);
    }

    public double doubleValue() {
        return (double) this._value;
    }

    public float floatValue() {
        return (float) this._value;
    }

    public int hashCode() {
        return this._value;
    }

    public int intValue() {
        return this._value;
    }

    public long longValue() {
        return (long) this._value;
    }

    public C29501gW numberType() {
        return C29501gW.INT;
    }

    public Number numberValue() {
        return Short.valueOf(this._value);
    }

    public final void serialize(C11710np r2, C11260mU r3) {
        r2.writeNumber(this._value);
    }

    public C28951fd(short s) {
        this._value = s;
    }
}
