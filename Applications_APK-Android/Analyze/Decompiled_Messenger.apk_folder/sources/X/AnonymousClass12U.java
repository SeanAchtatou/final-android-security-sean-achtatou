package X;

import android.net.Uri;
import com.facebook.graphql.enums.GraphQLMessengerThreadViewMode;
import com.facebook.messaging.customthreads.model.ThreadThemeInfo;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.12U  reason: invalid class name */
public final class AnonymousClass12U {
    public int A00;
    public long A01;
    public Uri A02;
    public Uri A03;
    public Uri A04;
    public GraphQLMessengerThreadViewMode A05;
    public ImmutableList A06;
    public ImmutableList A07;
    public String A08;
    public Set A09;
    public boolean A0A;

    public ThreadThemeInfo A00() {
        return new ThreadThemeInfo(this);
    }

    public void A01(String str) {
        this.A08 = str;
        C28931fb.A06(str, C22298Ase.$const$string(26));
    }

    public AnonymousClass12U() {
        this.A09 = new HashSet();
        this.A08 = BuildConfig.FLAVOR;
        ImmutableList immutableList = RegularImmutableList.A02;
        this.A06 = immutableList;
        this.A07 = immutableList;
        this.A01 = -1;
    }

    public AnonymousClass12U(C28921fa r3) {
        this.A09 = new HashSet();
        C28931fb.A05(r3);
        if (r3 instanceof ThreadThemeInfo) {
            ThreadThemeInfo threadThemeInfo = (ThreadThemeInfo) r3;
            this.A08 = threadThemeInfo.A08;
            this.A00 = threadThemeInfo.A00;
            this.A06 = threadThemeInfo.A06;
            this.A0A = threadThemeInfo.A0A;
            this.A02 = threadThemeInfo.A02;
            this.A03 = threadThemeInfo.A03;
            this.A07 = threadThemeInfo.A07;
            this.A04 = threadThemeInfo.A04;
            this.A01 = threadThemeInfo.A01;
            this.A05 = threadThemeInfo.A05;
            this.A09 = new HashSet(threadThemeInfo.A09);
            return;
        }
        A01(r3.AbY());
        this.A00 = r3.AmT();
        ImmutableList Ao6 = r3.Ao6();
        this.A06 = Ao6;
        C28931fb.A06(Ao6, "gradientColors");
        this.A0A = r3.BGP();
        this.A02 = r3.Art();
        this.A03 = r3.Aru();
        ImmutableList B01 = r3.B01();
        this.A07 = B01;
        C28931fb.A06(B01, "reactionAssets");
        this.A04 = r3.B3J();
        this.A01 = r3.B5o();
        GraphQLMessengerThreadViewMode B61 = r3.B61();
        this.A05 = B61;
        C28931fb.A06(B61, "threadViewMode");
        this.A09.add("threadViewMode");
    }
}
