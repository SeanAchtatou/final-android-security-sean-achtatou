package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.widget.recyclerview.BetterRecyclerView;

/* renamed from: X.0vo  reason: invalid class name and case insensitive filesystem */
public abstract class C15730vo {
    public void A01(int i, int i2) {
        if (this instanceof AnonymousClass1IN) {
            ((AnonymousClass1IN) this).A07();
        } else if (this instanceof C21691Ii) {
            C24151Sm r1 = ((C21691Ii) this).A00;
            r1.A01.A04(i + r1.A01.size(), i2, null);
        } else if (this instanceof C21711Ik) {
            ((C21711Ik) this).A04();
        }
    }

    public void A02(int i, int i2, int i3) {
        if (this instanceof AnonymousClass1IN) {
            ((AnonymousClass1IN) this).A07();
        } else if (this instanceof C21691Ii) {
            C24151Sm r1 = ((C21691Ii) this).A00;
            r1.A01.A01(i + r1.A01.size(), i2);
        } else if (this instanceof C21711Ik) {
            ((C21711Ik) this).A04();
        } else if (this instanceof C32911mX) {
            C32911mX r6 = (C32911mX) this;
            r6.A00.A17(null);
            C16150wZ r5 = r6.A00.A0G;
            boolean z = false;
            if (i != i2) {
                if (i3 == 1) {
                    r5.A05.add(r5.BMn(8, i, i2, null));
                    r5.A00 |= 8;
                    if (r5.A05.size() == 1) {
                        z = true;
                    }
                } else {
                    throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
                }
            }
            if (z) {
                C32911mX.A00(r6);
            }
        }
    }

    public void A04() {
        if (this instanceof AnonymousClass1IN) {
            ((AnonymousClass1IN) this).A07();
        } else if (this instanceof C21691Ii) {
            ((C21691Ii) this).A00.A04();
        } else if (this instanceof C21711Ik) {
            C21711Ik r2 = (C21711Ik) this;
            C23611Qe r1 = r2.A00;
            if (!r1.A03) {
                r1.A03 = true;
                r1.A06.notifyDataSetChanged();
                r2.A00.A03 = false;
            }
        } else if (this instanceof C21721Il) {
            BetterRecyclerView.A01(((C21721Il) this).A00);
        } else if (this instanceof C32911mX) {
            C32911mX r3 = (C32911mX) this;
            r3.A00.A17(null);
            RecyclerView recyclerView = r3.A00;
            recyclerView.A0y.A0C = true;
            recyclerView.A19(true);
            boolean z = false;
            if (r3.A00.A0G.A05.size() > 0) {
                z = true;
            }
            if (!z) {
                r3.A00.requestLayout();
            }
        }
    }

    public void A05(int i, int i2) {
        if (this instanceof AnonymousClass1IN) {
            ((AnonymousClass1IN) this).A07();
        } else if (this instanceof C21691Ii) {
            C24151Sm r1 = ((C21691Ii) this).A00;
            r1.A01.A02(i + r1.A01.size(), i2);
        } else if (this instanceof C21711Ik) {
            ((C21711Ik) this).A04();
        } else if (this instanceof C21721Il) {
            BetterRecyclerView.A01(((C21721Il) this).A00);
        } else if (this instanceof C32911mX) {
            C32911mX r5 = (C32911mX) this;
            r5.A00.A17(null);
            C16150wZ r4 = r5.A00.A0G;
            boolean z = false;
            if (i2 >= 1) {
                r4.A05.add(r4.BMn(1, i, i2, null));
                r4.A00 |= 1;
                if (r4.A05.size() == 1) {
                    z = true;
                }
            }
            if (z) {
                C32911mX.A00(r5);
            }
        }
    }

    public void A06(int i, int i2) {
        if (this instanceof AnonymousClass1IN) {
            ((AnonymousClass1IN) this).A07();
        } else if (this instanceof C21691Ii) {
            C24151Sm r1 = ((C21691Ii) this).A00;
            r1.A01.A03(i + r1.A01.size(), i2);
        } else if (this instanceof C21711Ik) {
            ((C21711Ik) this).A04();
        } else if (this instanceof C21721Il) {
            BetterRecyclerView.A01(((C21721Il) this).A00);
        } else if (this instanceof C32911mX) {
            C32911mX r6 = (C32911mX) this;
            r6.A00.A17(null);
            C16150wZ r5 = r6.A00.A0G;
            boolean z = false;
            if (i2 >= 1) {
                r5.A05.add(r5.BMn(2, i, i2, null));
                r5.A00 |= 2;
                if (r5.A05.size() == 1) {
                    z = true;
                }
            }
            if (z) {
                C32911mX.A00(r6);
            }
        }
    }

    public void A03(int i, int i2, Object obj) {
        if (!(this instanceof C32911mX)) {
            A01(i, i2);
            return;
        }
        C32911mX r6 = (C32911mX) this;
        r6.A00.A17(null);
        C16150wZ r5 = r6.A00.A0G;
        boolean z = false;
        if (i2 >= 1) {
            r5.A05.add(r5.BMn(4, i, i2, obj));
            r5.A00 |= 4;
            if (r5.A05.size() == 1) {
                z = true;
            }
        }
        if (z) {
            C32911mX.A00(r6);
        }
    }
}
