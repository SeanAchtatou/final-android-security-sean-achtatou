package X;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1iq  reason: invalid class name and case insensitive filesystem */
public final class C30911iq {
    public static final Class A07 = C30911iq.class;
    public final C23361Pf A00;
    public final C23011Nw A01;
    public final C22701Mm A02;
    public final C23381Ph A03 = new C23381Ph();
    public final C30661iR A04;
    public final Executor A05;
    private final Executor A06;

    public void A01() {
        this.A03.A01();
        try {
            C23901Rk.A00(new C30299Etg(this, C27491dH.A01("BufferedDiskCache_clearAll")), this.A06);
        } catch (Exception e) {
            AnonymousClass02w.A09(A07, e, "Failed to schedule disk-cache clear", new Object[0]);
            new C23951Rp().A01(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0056, code lost:
        if (r7.A00.BBV(r8) != false) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(X.C23601Qd r8) {
        /*
            r7 = this;
            X.1Ph r6 = r7.A03
            monitor-enter(r6)
            X.C05520Zg.A02(r8)     // Catch:{ all -> 0x0083 }
            java.util.Map r0 = r6.A00     // Catch:{ all -> 0x0083 }
            boolean r0 = r0.containsKey(r8)     // Catch:{ all -> 0x0083 }
            if (r0 == 0) goto L_0x0048
            java.util.Map r0 = r6.A00     // Catch:{ all -> 0x0083 }
            java.lang.Object r5 = r0.get(r8)     // Catch:{ all -> 0x0083 }
            X.1NY r5 = (X.AnonymousClass1NY) r5     // Catch:{ all -> 0x0083 }
            monitor-enter(r5)     // Catch:{ all -> 0x0083 }
            boolean r0 = X.AnonymousClass1NY.A07(r5)     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0043
            java.util.Map r0 = r6.A00     // Catch:{ all -> 0x0045 }
            r0.remove(r8)     // Catch:{ all -> 0x0045 }
            java.lang.Class r4 = X.C23381Ph.A01     // Catch:{ all -> 0x0045 }
            java.lang.String r3 = "Found closed reference %d for key %s (%d)"
            int r0 = java.lang.System.identityHashCode(r5)     // Catch:{ all -> 0x0045 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0045 }
            java.lang.String r1 = r8.B7w()     // Catch:{ all -> 0x0045 }
            int r0 = java.lang.System.identityHashCode(r8)     // Catch:{ all -> 0x0045 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0045 }
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r1, r0}     // Catch:{ all -> 0x0045 }
            X.AnonymousClass02w.A07(r4, r3, r0)     // Catch:{ all -> 0x0045 }
            monitor-exit(r5)     // Catch:{ all -> 0x0045 }
            goto L_0x0048
        L_0x0043:
            monitor-exit(r5)     // Catch:{ all -> 0x0045 }
            goto L_0x004b
        L_0x0045:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0045 }
            throw r0     // Catch:{ all -> 0x0083 }
        L_0x0048:
            monitor-exit(r6)
            r0 = 0
            goto L_0x004d
        L_0x004b:
            monitor-exit(r6)
            r0 = 1
        L_0x004d:
            if (r0 != 0) goto L_0x0058
            X.1Pf r0 = r7.A00
            boolean r1 = r0.BBV(r8)
            r0 = 0
            if (r1 == 0) goto L_0x0059
        L_0x0058:
            r0 = 1
        L_0x0059:
            if (r0 == 0) goto L_0x005d
            r0 = 1
            return r0
        L_0x005d:
            X.1Ph r0 = r7.A03
            X.1NY r0 = r0.A00(r8)
            if (r0 == 0) goto L_0x0072
            r0.close()
            r8.B7w()
            X.1Mm r0 = r7.A02
            r0.BpN(r8)
            r0 = 1
            return r0
        L_0x0072:
            r8.B7w()
            X.1Mm r0 = r7.A02
            r0.BpO(r8)
            X.1Pf r0 = r7.A00     // Catch:{ Exception -> 0x0081 }
            boolean r0 = r0.BBU(r8)     // Catch:{ Exception -> 0x0081 }
            return r0
        L_0x0081:
            r0 = 0
            return r0
        L_0x0083:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30911iq.A04(X.1Qd):boolean");
    }

    public C30911iq(C23361Pf r2, C30661iR r3, C23011Nw r4, Executor executor, Executor executor2, C22701Mm r7) {
        this.A00 = r2;
        this.A04 = r3;
        this.A01 = r4;
        this.A05 = executor;
        this.A06 = executor2;
        this.A02 = r7;
    }

    public C23901Rk A00(C23601Qd r5, AtomicBoolean atomicBoolean) {
        C23901Rk r1;
        try {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("BufferedDiskCache#get");
            }
            AnonymousClass1NY A002 = this.A03.A00(r5);
            if (A002 != null) {
                r5.B7w();
                this.A02.BpN(r5);
                if (A002 == null) {
                    r1 = C23901Rk.A09;
                } else if (!(A002 instanceof Boolean)) {
                    C23951Rp r0 = new C23951Rp();
                    r0.A02(A002);
                    r1 = r0.A00;
                } else if (((Boolean) A002).booleanValue()) {
                    r1 = C23901Rk.A0A;
                } else {
                    r1 = C23901Rk.A08;
                }
            } else {
                r1 = C23901Rk.A00(new C23861Rf(this, C27491dH.A01("BufferedDiskCache_getAsync"), atomicBoolean, r5), this.A05);
            }
        } catch (Exception e) {
            AnonymousClass02w.A09(A07, e, "Failed to schedule disk-cache read for %s", r5.B7w());
            C23951Rp r02 = new C23951Rp();
            r02.A01(e);
            r1 = r02.A00;
        } catch (Throwable th) {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
            throw th;
        }
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
        return r1;
    }

    public void A02(C23601Qd r5) {
        C05520Zg.A02(r5);
        this.A03.A02(r5);
        try {
            C23901Rk.A00(new C30298Etf(this, C27491dH.A01("BufferedDiskCache_remove"), r5), this.A06);
        } catch (Exception e) {
            AnonymousClass02w.A09(A07, e, "Failed to schedule disk-cache remove for %s", r5.B7w());
            new C23951Rp().A01(e);
        }
    }

    public void A03(C23601Qd r6, AnonymousClass1NY r7) {
        AnonymousClass1NY A032;
        try {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("BufferedDiskCache#put");
            }
            C05520Zg.A02(r6);
            C05520Zg.A04(AnonymousClass1NY.A07(r7));
            C23381Ph r2 = this.A03;
            synchronized (r2) {
                C05520Zg.A02(r6);
                C05520Zg.A04(AnonymousClass1NY.A07(r7));
                AnonymousClass1NY.A04((AnonymousClass1NY) r2.A00.put(r6, AnonymousClass1NY.A03(r7)));
                synchronized (r2) {
                    r2.A00.size();
                }
            }
            A032 = AnonymousClass1NY.A03(r7);
            AnonymousClass07A.A04(this.A06, new C56092pF(this, C27491dH.A01("BufferedDiskCache_putAsync"), r6, A032), -1105191477);
        } catch (Exception e) {
            AnonymousClass02w.A09(A07, e, "Failed to schedule disk-cache write for %s", r6.B7w());
            this.A03.A03(r6, r7);
            AnonymousClass1NY.A04(A032);
        } catch (Throwable th) {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
            throw th;
        }
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
    }
}
