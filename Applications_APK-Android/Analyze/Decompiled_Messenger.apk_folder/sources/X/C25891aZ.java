package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1aZ  reason: invalid class name and case insensitive filesystem */
public final class C25891aZ extends AnonymousClass0Wl {
    public static final String A01 = "LOCATION_MANAGER_DETOUR".concat("_getLastKnownLocation()");
    public static final String A02 = "LOCATION_MANAGER_DETOUR".concat("_removeUpdates()");
    public static final String A03 = "LOCATION_MANAGER_DETOUR".concat("_requestLocationUpdates()");
    public static final String A04 = "LOCATION_MANAGER_DETOUR".concat("_requestSingleUpdate()");
    public static final String A05 = "TELEPHONY_MANAGER_DETOUR".concat("_getAllCellInfo()");
    public static final String A06 = "TELEPHONY_MANAGER_DETOUR".concat("_getCellLocation()");
    public static final String A07 = "WIFI_MANAGER_DETOUR".concat("_getConfiguredNetworks()");
    public static final String A08 = "WIFI_MANAGER_DETOUR".concat("_getConnectionInfo()");
    public static final String A09 = "WIFI_MANAGER_DETOUR".concat("_getScanResults()");
    public static final String A0A = "WIFI_MANAGER_DETOUR".concat("_startScan()");
    private static volatile C25891aZ A0B;
    public AnonymousClass0UN A00;

    public String getSimpleName() {
        return "LocationRequestDetector";
    }

    public static final C25891aZ A00(AnonymousClass1XY r4) {
        if (A0B == null) {
            synchronized (C25891aZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0B, r4);
                if (A002 != null) {
                    try {
                        A0B = new C25891aZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0B;
    }

    public static void A01(C25891aZ r3, String str) {
        ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r3.A00)).CGU("LocationRequestDetector", str, new C187898o0(), 1);
    }

    public static boolean A02(C25891aZ r3) {
        return ((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, r3.A00)).A0G();
    }

    private C25891aZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    /* JADX INFO: finally extract failed */
    public void init() {
        int A032 = C000700l.A03(1152061153);
        int i = AnonymousClass1Y3.AcD;
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(2, i, this.A00)).AbO(AnonymousClass1Y3.A0y, false)) {
            C000700l.A09(-1886082684, A032);
            return;
        }
        C29991hJ r1 = new C29991hJ(this, ((AnonymousClass1YI) AnonymousClass1XX.A02(2, i, this.A00)).AbO(132, false), ((AnonymousClass1YI) AnonymousClass1XX.A02(2, i, this.A00)).AbO(AnonymousClass1Y3.A0z, false));
        try {
            C29981hI.A01.writeLock().lock();
            C29981hI.A00 = r1;
            C29981hI.A01.writeLock().unlock();
            C000700l.A09(387580205, A032);
        } catch (Throwable th) {
            C29981hI.A01.writeLock().unlock();
            throw th;
        }
    }
}
