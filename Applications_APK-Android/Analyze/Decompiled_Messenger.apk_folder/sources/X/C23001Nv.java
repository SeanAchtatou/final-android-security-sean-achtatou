package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Nv  reason: invalid class name and case insensitive filesystem */
public final class C23001Nv extends C22731Mp {
    private static volatile C23001Nv A00;

    public static final C23001Nv A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (C23001Nv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new C23001Nv(C22741Mq.A01(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private C23001Nv(C22751Mr r1) {
        super(r1);
    }
}
