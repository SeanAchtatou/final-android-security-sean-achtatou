package X;

import com.facebook.user.model.UserKey;

/* renamed from: X.1KQ  reason: invalid class name */
public final class AnonymousClass1KQ {
    public static AnonymousClass1JY A00(UserKey userKey, String str, int i) {
        boolean A08 = userKey.A08();
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.USER_KEY;
        r1.A03 = userKey;
        r1.A06 = C21381Gs.A0L;
        r1.A07 = userKey.A04();
        if (!A08) {
            str = null;
        }
        r1.A08 = str;
        if (!A08) {
            i = 0;
        }
        r1.A00 = i;
        return new AnonymousClass1JY(r1);
    }
}
