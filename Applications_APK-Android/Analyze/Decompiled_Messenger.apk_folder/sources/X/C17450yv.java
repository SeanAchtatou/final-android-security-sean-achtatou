package X;

import android.os.Handler;
import android.os.Looper;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0yv  reason: invalid class name and case insensitive filesystem */
public final class C17450yv {
    private static volatile C17450yv A05;
    public long A00 = Long.MAX_VALUE;
    public AnonymousClass0UN A01;
    public String A02;
    public boolean A03 = false;
    public final Object A04 = new Object();

    public static final C17450yv A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C17450yv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C17450yv(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static void A01(C17450yv r5, String str) {
        synchronized (r5.A04) {
            if (r5.A03) {
                r5.A00 = Long.MAX_VALUE;
                r5.A03 = false;
                String str2 = r5.A02;
                r5.A02 = BuildConfig.FLAVOR;
                AnonymousClass00S.A04(new Handler(Looper.getMainLooper()), new AnonymousClass48s(r5, str, str2), -1673833355);
            }
        }
    }

    private C17450yv(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(3, r3);
    }
}
