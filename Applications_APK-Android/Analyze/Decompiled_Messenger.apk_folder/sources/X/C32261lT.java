package X;

/* renamed from: X.1lT  reason: invalid class name and case insensitive filesystem */
public final class C32261lT implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.push.mqtt.service.MqttPushServiceClientImpl$1";
    public final /* synthetic */ C36991uR A00;

    public C32261lT(C36991uR r1) {
        this.A00 = r1;
    }

    public void run() {
        C36991uR r7 = this.A00;
        C32811mN r6 = r7.A07;
        r7.A06.A01(new C34431pZ(r7.A05.now(), "ServiceUnbound (MqttPushServiceClientManager)", new Object[0]));
        try {
            r7.A08.A04(r6);
        } catch (IllegalArgumentException e) {
            C010708t.A08(C36991uR.A0C, "Exception unbinding", e);
        }
    }
}
