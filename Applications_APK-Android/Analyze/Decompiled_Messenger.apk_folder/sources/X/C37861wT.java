package X;

/* renamed from: X.1wT  reason: invalid class name and case insensitive filesystem */
public final class C37861wT extends Exception {
    public C37861wT(String str) {
        super(str);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C37861wT(String str, String str2) {
        super(AnonymousClass08S.A0P(str, ": ", str2 == null ? "null" : str2));
    }
}
