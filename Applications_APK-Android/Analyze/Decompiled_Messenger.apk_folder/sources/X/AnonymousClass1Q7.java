package X;

import java.util.Map;

/* renamed from: X.1Q7  reason: invalid class name */
public final class AnonymousClass1Q7 implements AnonymousClass1Q3 {
    private final C22601Mc A00;
    private final C23311Pa A01;
    private final AnonymousClass1Q3 A02;

    public void ByS(C23581Qb r18, AnonymousClass1QK r19) {
        Map map;
        AnonymousClass1NY r8;
        String $const$string = C99084oO.$const$string(AnonymousClass1Y3.A0p);
        try {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("EncodedMemoryCacheProducer#produceResults");
            }
            AnonymousClass1QK r7 = r19;
            AnonymousClass1MN r5 = r7.A07;
            r5.Bk8(r7, $const$string);
            C23601Qd A05 = this.A00.A05(r7.A09, r7.A0A);
            AnonymousClass1PS Ab8 = this.A01.Ab8(A05);
            Map map2 = null;
            C23581Qb r12 = r18;
            if (Ab8 != null) {
                try {
                    r8 = new AnonymousClass1NY(Ab8);
                    if (r5.C3Q(r7, $const$string)) {
                        map2 = C22665B6t.A01("cached_value_found", "true");
                    }
                    r5.Bk6(r7, $const$string, map2);
                    r5.Bt8(r7, $const$string, true);
                    r7.A05.put(1, "memory_encoded");
                    r12.BkJ(1.0f);
                    r12.Bgg(r8, 1);
                    AnonymousClass1NY.A04(r8);
                } catch (Throwable th) {
                    AnonymousClass1PS.A05(Ab8);
                    throw th;
                }
            } else if (r7.A08.mValue >= C23531Pw.ENCODED_MEMORY_CACHE.mValue) {
                if (r5.C3Q(r7, $const$string)) {
                    map = C22665B6t.A01("cached_value_found", "false");
                } else {
                    map = null;
                }
                r5.Bk6(r7, $const$string, map);
                r5.Bt8(r7, $const$string, false);
                r7.A05.put(1, "memory_encoded");
                r12.Bgg(null, 1);
            } else {
                C23851Re r11 = new C23851Re(r12, this.A01, A05, r7.A09.A0F, r7.A06.A0F.A02);
                if (r5.C3Q(r7, $const$string)) {
                    map2 = C22665B6t.A01("cached_value_found", "false");
                }
                r5.Bk6(r7, $const$string, map2);
                this.A02.ByS(r11, r7);
            }
            AnonymousClass1PS.A05(Ab8);
        } finally {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
        }
    }

    public AnonymousClass1Q7(C23311Pa r1, C22601Mc r2, AnonymousClass1Q3 r3) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = r3;
    }
}
