package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tD  reason: invalid class name and case insensitive filesystem */
public final class C36491tD extends C36471tB {
    public static final Class A04 = C36491tD.class;
    private static volatile C36491tD A05;
    private final AnonymousClass06A A00;
    private final AnonymousClass18M A01;
    private final AnonymousClass18N A02;
    private final C04310Tq A03;

    public synchronized void A0D(ThreadKey threadKey, String str) {
        if (!A0C()) {
            C010708t.A05(A04, "Stored procedure sender not available to create multi-endpoint thread");
        } else if (this.A02.A03()) {
            C010708t.A05(A04, "Invalid device id");
        } else {
            AwF awF = new AwF(ImmutableList.of(Long.valueOf(threadKey.A01)), Boolean.TRUE);
            Preconditions.checkNotNull(threadKey);
            byte[] bytes = C36471tB.A03(threadKey.A0J()).getBytes(C36471tB.A05);
            C22036AnC anC = C22036AnC.A05;
            C22335Aw3 aw3 = new C22335Aw3();
            aw3.setField_ = 20;
            aw3.value_ = awF;
            A0B(C22030An2.A01(C22039AnF.A01(new C22338Aw9(Long.valueOf(threadKey.A01), null), new C22338Aw9(Long.valueOf(Long.parseLong((String) this.A03.get())), this.A02.A02()), this.A00.now() * 1000, anC, aw3, bytes, null)));
            AnonymousClass18M r3 = this.A01;
            HashMap hashMap = new HashMap();
            hashMap.put("thread_key", threadKey.toString());
            hashMap.put("nonce_content", AnonymousClass18M.A01(bytes));
            hashMap.put("trigger_reason", str);
            AnonymousClass18M.A03(r3, "tincan_thread_create_attempt", hashMap);
        }
    }

    private C36491tD(AnonymousClass18N r2, AnonymousClass06A r3, C04310Tq r4, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, AnonymousClass18M r6) {
        super(17, deprecatedAnalyticsLogger);
        this.A02 = r2;
        this.A00 = r3;
        this.A03 = r4;
        this.A01 = r6;
    }

    public static final C36491tD A00(AnonymousClass1XY r8) {
        if (A05 == null) {
            synchronized (C36491tD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        A05 = new C36491tD(AnonymousClass18N.A00(applicationInjector), AnonymousClass067.A0A(applicationInjector), C10580kT.A04(applicationInjector), C06920cI.A00(applicationInjector), AnonymousClass18M.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static void A01(C36491tD r1, ThreadKey threadKey, AnonymousClass94R r3, byte[] bArr) {
        for (C22035AnB A012 : r1.A01) {
            A012.A01(threadKey, r3, bArr);
        }
    }
}
