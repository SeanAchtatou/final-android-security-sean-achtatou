package X;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.facebook.inject.InjectorModule;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Logger;

@InjectorModule
/* renamed from: X.0UX  reason: invalid class name */
public final class AnonymousClass0UX extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static final Object A02 = new Object();
    private static final Object A03 = new Object();
    private static final Object A04 = new Object();
    private static final Object A05 = new Object();
    private static final Object A06 = new Object();
    private static final Object A07 = new Object();
    private static final Object A08 = new Object();
    private static final Object A09 = new Object();
    private static final Object A0A = new Object();
    private static final Object A0B = new Object();
    private static final Object A0C = new Object();
    private static final Object A0D = new Object();
    private static final Object A0E = new Object();
    private static final Object A0F = new Object();
    private static final Object A0G = new Object();
    private static final Object A0H = new Object();
    private static final Object A0I = new Object();
    private static final Object A0J = new Object();
    private static final Object A0K = new Object();
    private static volatile HandlerThread A0L;
    private static volatile AnonymousClass1YG A0M;
    private static volatile AnonymousClass1YG A0N;
    private static volatile AnonymousClass1YG A0O;
    private static volatile C05160Xw A0P;
    private static volatile AnonymousClass0VL A0Q;
    private static volatile AnonymousClass0VL A0R;
    private static volatile AnonymousClass0VL A0S;
    private static volatile AnonymousClass0VL A0T;
    private static volatile AnonymousClass0VL A0U;
    private static volatile AnonymousClass0VL A0V;
    private static volatile AnonymousClass0VK A0W;
    private static volatile AnonymousClass0VK A0X;
    private static volatile AnonymousClass0VK A0Y;
    private static volatile AnonymousClass0VK A0Z;
    private static volatile ExecutorService A0a;
    private static volatile ExecutorService A0b;
    private static volatile ScheduledExecutorService A0c;
    private static volatile ScheduledExecutorService A0d;
    private static volatile ScheduledExecutorService A0e;
    private static volatile ScheduledExecutorService A0f;

    static {
        synchronized (AnonymousClass0UY.class) {
            if (!AnonymousClass0UY.A00.getAndSet(true)) {
                Logger.getLogger(C04330Ua.class.getName()).addHandler(AnonymousClass0UY.A01);
            }
        }
    }

    public static final HandlerThread A03(AnonymousClass1XY r5) {
        if (A0L == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0L, r5);
                if (A002 != null) {
                    try {
                        HandlerThread A022 = AnonymousClass0V4.A00(r5.getApplicationInjector()).A02("BgHandler", AnonymousClass0V7.NORMAL);
                        A022.start();
                        A0L = A022;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0L;
    }

    public static final AnonymousClass1YG A08(AnonymousClass1XY r3) {
        if (A0M == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0M, r3);
                if (A002 != null) {
                    try {
                        A0M = (AnonymousClass1YG) A0K(r3.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0M;
    }

    public static final AnonymousClass1YG A09(AnonymousClass1XY r3) {
        if (A0N == null) {
            synchronized (A02) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0N, r3);
                if (A002 != null) {
                    try {
                        A0N = (AnonymousClass1YG) A0L(r3.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0N;
    }

    public static final AnonymousClass1YG A0A(AnonymousClass1XY r3) {
        if (A0O == null) {
            synchronized (A03) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0O, r3);
                if (A002 != null) {
                    try {
                        A0O = (AnonymousClass1YG) A0N(r3.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0O;
    }

    public static final C37051uX A0B() {
        return null;
    }

    public static final C05160Xw A0F(AnonymousClass1XY r5) {
        if (A0P == null) {
            synchronized (A04) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0P, r5);
                if (A002 != null) {
                    try {
                        A0P = AnonymousClass0VM.A00(r5.getApplicationInjector()).A04(AnonymousClass0VS.URGENT, "InboxLoader");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0P;
    }

    public static final AnonymousClass0VL A0I(AnonymousClass1XY r6) {
        if (A0Q == null) {
            synchronized (A05) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0Q, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A0Q = AnonymousClass0VM.A00(applicationInjector).A02(AnonymousClass0VN.A00(applicationInjector).A00, AnonymousClass0VS.FOREGROUND, "RichMediaUpload");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0Q;
    }

    public static final AnonymousClass0VL A0J(AnonymousClass1XY r3) {
        if (A0R == null) {
            synchronized (A06) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0R, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A0R = new AnonymousClass0WK();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0R;
    }

    public static final AnonymousClass0VL A0K(AnonymousClass1XY r6) {
        if (A0S == null) {
            synchronized (A07) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0S, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A0S = AnonymousClass0VM.A00(applicationInjector).A02(AnonymousClass0VN.A00(applicationInjector).A00, AnonymousClass0VS.BACKGROUND, "BackgroundExecutor");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0S;
    }

    public static final AnonymousClass0VL A0L(AnonymousClass1XY r6) {
        if (A0T == null) {
            synchronized (A08) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0T, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A0T = AnonymousClass0VM.A00(applicationInjector).A02(AnonymousClass0VN.A00(applicationInjector).A01, AnonymousClass0VS.FOREGROUND, "ForegroundExecutor");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0T;
    }

    public static final AnonymousClass0VL A0M(AnonymousClass1XY r6) {
        if (A0U == null) {
            synchronized (A09) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0U, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A0U = AnonymousClass0VM.A00(applicationInjector).A02(AnonymousClass0VN.A00(applicationInjector).A02, AnonymousClass0VS.IMPORTANT, "ImportantExecutor");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0U;
    }

    public static final AnonymousClass0VL A0O(AnonymousClass1XY r6) {
        if (A0V == null) {
            synchronized (A0A) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0V, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass0VM A003 = AnonymousClass0VM.A00(applicationInjector);
                        AnonymousClass0VN.A00(applicationInjector);
                        A0V = A003.A02(8, AnonymousClass0VS.NORMAL, "DefaultExecutor");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0V;
    }

    public static final AnonymousClass0VK A0P(AnonymousClass1XY r7) {
        if (A0W == null) {
            synchronized (A0B) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0W, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        AnonymousClass0VE r4 = new AnonymousClass0VE(applicationInjector);
                        A0W = new C42412Aj(new AnonymousClass0VH(new Handler(Looper.getMainLooper()), r4), AnonymousClass067.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0W;
    }

    public static final AnonymousClass0VK A0Q(AnonymousClass1XY r5) {
        if (A0X == null) {
            synchronized (A0C) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0X, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        Looper A052 = A05(applicationInjector);
                        A0X = new AnonymousClass0VH(new Handler(A052), new AnonymousClass0VE(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0X;
    }

    public static final AnonymousClass0VK A0R(AnonymousClass1XY r5) {
        if (A0Y == null) {
            synchronized (A0D) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0Y, r5);
                if (A002 != null) {
                    try {
                        A0Y = new AnonymousClass0VH(new Handler(Looper.getMainLooper()), new AnonymousClass0VE(r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0Y;
    }

    public static final AnonymousClass0VK A0S(AnonymousClass1XY r5) {
        if (A0Z == null) {
            synchronized (A0E) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0Z, r5);
                if (A002 != null) {
                    try {
                        A0Z = new AnonymousClass1MO(new Handler(Looper.getMainLooper()), new AnonymousClass0VE(r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0Z;
    }

    public static final ExecutorService A0V(AnonymousClass1XY r7) {
        if (A0a == null) {
            synchronized (A0F) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0a, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass0VM A003 = AnonymousClass0VM.A00(r7.getApplicationInjector());
                        A0a = (AnonymousClass1TI) ((AnonymousClass0VT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ALd, A003.A00)).A01(1, AnonymousClass0VS.BACKGROUND, "AnalyticsThread", null);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0a;
    }

    public static final ExecutorService A0Y(AnonymousClass1XY r7) {
        if (A0b == null) {
            synchronized (A0G) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0b, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass0VM A003 = AnonymousClass0VM.A00(r7.getApplicationInjector());
                        A0b = ((AnonymousClass0VT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ALd, A003.A00)).A01(2, AnonymousClass0VS.IMPORTANT, "LightSharedPrefExecutor", null);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0b;
    }

    public static final ScheduledExecutorService A0g(AnonymousClass1XY r6) {
        ScheduledExecutorService A032;
        if (A0c == null) {
            synchronized (A0H) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0c, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass0VM A003 = AnonymousClass0VM.A00(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.ASK, applicationInjector);
                        if (AnonymousClass00J.A01(AnonymousClass1YA.A02(applicationInjector)).A2G) {
                            A032 = (ScheduledExecutorService) A004.get();
                        } else {
                            A032 = A003.A03(AnonymousClass0VS.BACKGROUND, "Shared");
                        }
                        A0c = A032;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0c;
    }

    public static final ScheduledExecutorService A0h(AnonymousClass1XY r6) {
        ScheduledExecutorService A032;
        if (A0d == null) {
            synchronized (A0I) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0d, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass0VM A003 = AnonymousClass0VM.A00(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.Am5, applicationInjector);
                        if (AnonymousClass00J.A01(AnonymousClass1YA.A02(applicationInjector)).A2H) {
                            A032 = (ScheduledExecutorService) A004.get();
                        } else {
                            A032 = A003.A03(AnonymousClass0VS.FOREGROUND, "Shared");
                        }
                        A0d = A032;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0d;
    }

    public static final ScheduledExecutorService A0i(AnonymousClass1XY r6) {
        ScheduledExecutorService A032;
        if (A0e == null) {
            synchronized (A0J) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0e, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass0VM A003 = AnonymousClass0VM.A00(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.A4I, applicationInjector);
                        if (AnonymousClass00J.A01(AnonymousClass1YA.A02(applicationInjector)).A2I) {
                            A032 = (ScheduledExecutorService) A004.get();
                        } else {
                            A032 = A003.A03(AnonymousClass0VS.IMPORTANT, "Shared");
                        }
                        A0e = A032;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0e;
    }

    public static final ScheduledExecutorService A0k(AnonymousClass1XY r6) {
        ScheduledExecutorService A032;
        if (A0f == null) {
            synchronized (A0K) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0f, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass0VM A003 = AnonymousClass0VM.A00(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.AZx, applicationInjector);
                        if (AnonymousClass00J.A01(AnonymousClass1YA.A02(applicationInjector)).A2J) {
                            A032 = (ScheduledExecutorService) A004.get();
                        } else {
                            A032 = A003.A03(AnonymousClass0VS.NORMAL, "Shared");
                        }
                        A0f = A032;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0f;
    }

    public static final Handler A00() {
        return new Handler(Looper.getMainLooper());
    }

    public static final Handler A01(AnonymousClass1XY r1) {
        return new Handler(A05(r1));
    }

    public static final Handler A02(AnonymousClass1XY r1) {
        return new Handler(A05(r1));
    }

    public static final Looper A04() {
        return Looper.getMainLooper();
    }

    public static final Looper A05(AnonymousClass1XY r0) {
        return A03(r0).getLooper();
    }

    public static final Looper A06(AnonymousClass1XY r0) {
        return A03(r0).getLooper();
    }

    public static final AnonymousClass1Y6 A07(AnonymousClass1XY r0) {
        return C04340Ub.A00(r0);
    }

    public static final C05160Xw A0C(AnonymousClass1XY r0) {
        return C21181Fn.A00(A0M(r0));
    }

    public static final C05160Xw A0D(AnonymousClass1XY r0) {
        return C21181Fn.A00(A0a(r0));
    }

    public static final C05160Xw A0E(AnonymousClass1XY r0) {
        return C21181Fn.A00(A0a(r0));
    }

    public static final C05160Xw A0G(AnonymousClass1XY r2) {
        return AnonymousClass0VM.A00(r2).A04(AnonymousClass0VS.URGENT, "MqttClientSingleThreadExecutorService");
    }

    public static final AnonymousClass0VL A0H(AnonymousClass1XY r3) {
        return AnonymousClass0VM.A00(r3).A02(1, AnonymousClass0VS.URGENT, "UrgentSingleThreadExecutorService");
    }

    public static final AnonymousClass0VL A0N(AnonymousClass1XY r0) {
        return A0O(r0);
    }

    public static final Executor A0T(AnonymousClass1XY r0) {
        return A0Q(r0);
    }

    public static final Executor A0U(AnonymousClass1XY r0) {
        return A0R(r0);
    }

    public static final ExecutorService A0W(AnonymousClass1XY r0) {
        return A0Q(r0);
    }

    public static final ExecutorService A0X(AnonymousClass1XY r0) {
        return A0R(r0);
    }

    public static final ExecutorService A0Z(AnonymousClass1XY r0) {
        return A0K(r0);
    }

    public static final ExecutorService A0a(AnonymousClass1XY r0) {
        return A0O(r0);
    }

    public static final ScheduledExecutorService A0b(AnonymousClass1XY r2) {
        return AnonymousClass0VM.A00(r2).A04(AnonymousClass0VS.BACKGROUND, "SingleBackgdSch");
    }

    public static final ScheduledExecutorService A0c(AnonymousClass1XY r2) {
        return AnonymousClass0VM.A00(r2).A04(AnonymousClass0VS.FOREGROUND, "ForegroundSingleSch");
    }

    public static final ScheduledExecutorService A0d(AnonymousClass1XY r2) {
        return AnonymousClass0VM.A00(r2).A04(AnonymousClass0VS.NORMAL, "SingleSch");
    }

    public static final ScheduledExecutorService A0e(AnonymousClass1XY r0) {
        return A0R(r0);
    }

    public static final ScheduledExecutorService A0f(AnonymousClass1XY r0) {
        return A0g(r0);
    }

    public static final ScheduledExecutorService A0j(AnonymousClass1XY r0) {
        return A0k(r0);
    }
}
