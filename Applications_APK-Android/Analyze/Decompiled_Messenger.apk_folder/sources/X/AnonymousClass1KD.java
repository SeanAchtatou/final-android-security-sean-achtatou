package X;

import com.facebook.yoga.YogaNative;

/* renamed from: X.1KD  reason: invalid class name */
public abstract class AnonymousClass1KD extends C24001Ru {
    public long A00;

    public AnonymousClass1KD() {
        this(YogaNative.jni_YGConfigNewJNI());
    }

    private AnonymousClass1KD(long j) {
        if (j != 0) {
            this.A00 = j;
            return;
        }
        throw new IllegalStateException("Failed to allocate native memory");
    }
}
