package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1FG  reason: invalid class name */
public final class AnonymousClass1FG {
    private static volatile AnonymousClass1FG A01;
    public final AnonymousClass0US A00;

    public static final AnonymousClass1FG A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1FG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1FG(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass1FG(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0UQ.A00(AnonymousClass1Y3.BNF, r2);
    }
}
