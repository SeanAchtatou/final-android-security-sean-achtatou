package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.inboxads.common.InboxAdsData;

/* renamed from: X.1sG  reason: invalid class name and case insensitive filesystem */
public final class C36061sG implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxAdsData(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxAdsData[i];
    }
}
