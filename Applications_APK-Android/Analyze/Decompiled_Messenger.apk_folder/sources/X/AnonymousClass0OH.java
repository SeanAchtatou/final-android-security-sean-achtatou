package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.0OH  reason: invalid class name */
public final class AnonymousClass0OH {
    public static final Class A00 = AnonymousClass0OH.class;
    private static volatile Boolean A01;

    private static File A01(Context context) {
        File cacheDir = context.getCacheDir();
        if (C009508d.A00 && cacheDir.getName().equals("browser_proc")) {
            cacheDir = cacheDir.getParentFile();
        }
        return new File(cacheDir, "ClassTraces/");
    }

    public static boolean A03(Context context) {
        if (A01 == null) {
            A01 = Boolean.valueOf(AnonymousClass08X.A07(context, "mdcd_multiprocess_enable"));
        }
        return A01.booleanValue();
    }

    private AnonymousClass0OH() {
    }

    public static File A00(Context context) {
        File file = new File(A01(context), AnonymousClass08S.A0A("v", AnonymousClass0ZP.A00(context), "/"));
        if (file.exists() || file.mkdirs()) {
            return file;
        }
        return null;
    }

    public static void A02(Context context) {
        if (A03(context)) {
            File A012 = A01(context);
            if (A012.exists()) {
                File A002 = A00(context);
                File[] listFiles = A012.listFiles();
                if (listFiles != null) {
                    for (File file : listFiles) {
                        if (!file.equals(A002)) {
                            AnonymousClass9AZ.A02(file);
                        }
                    }
                }
            }
        }
    }
}
