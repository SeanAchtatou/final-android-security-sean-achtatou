package X;

import java.io.Serializable;

/* renamed from: X.0k7  reason: invalid class name and case insensitive filesystem */
public final class C10440k7 implements Serializable {
    private static final long serialVersionUID = 1;
    public C10310jt _rootNames;

    public synchronized C10230jl findRootName(Class cls, C10470kA r6) {
        String str;
        C29041fm r3 = new C29041fm(cls);
        C10310jt r0 = this._rootNames;
        if (r0 == null) {
            this._rootNames = new C10310jt(20, AnonymousClass1Y3.A1c);
        } else {
            C10230jl r02 = (C10230jl) r0.get(r3);
            if (r02 != null) {
                return r02;
            }
        }
        C87974Hw findRootName = r6.getAnnotationIntrospector().findRootName(r6.introspectClassAnnotations(r6.constructType(cls)).getClassInfo());
        if (findRootName != null) {
            str = findRootName._simpleName;
            boolean z = false;
            if (str.length() > 0) {
                z = true;
            }
            if (z) {
                C10230jl r1 = new C10230jl(str);
                this._rootNames.put(r3, r1);
                return r1;
            }
        }
        str = cls.getSimpleName();
        C10230jl r12 = new C10230jl(str);
        this._rootNames.put(r3, r12);
        return r12;
    }
}
