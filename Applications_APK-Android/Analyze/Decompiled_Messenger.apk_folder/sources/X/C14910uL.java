package X;

import android.content.res.ColorStateList;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.view.View;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.0uL  reason: invalid class name and case insensitive filesystem */
public final class C14910uL extends C17770zR {
    public static final int A0o = A0t.getStyle();
    public static final ColorStateList A0p = new ColorStateList(new int[][]{new int[]{0}}, new int[]{C15320v6.MEASURED_STATE_MASK});
    public static final Path A0q = new Path();
    public static final Rect A0r = new Rect();
    public static final RectF A0s = new RectF();
    public static final Typeface A0t = Typeface.DEFAULT;
    public static final Typeface A0u = A0t;
    public static final C14920uM A0v = C14920uM.TOP;
    @Comparable(type = 0)
    public float A00;
    @Comparable(type = 0)
    public float A01;
    @Comparable(type = 0)
    public float A02 = Float.MAX_VALUE;
    @Comparable(type = 0)
    public float A03;
    @Comparable(type = 0)
    public float A04;
    @Comparable(type = 0)
    public float A05;
    @Comparable(type = 0)
    public float A06 = 1.0f;
    @Comparable(type = 3)
    public int A07 = 0;
    @Comparable(type = 3)
    public int A08;
    @Comparable(type = 3)
    public int A09 = -1;
    @Comparable(type = 3)
    public int A0A = -1;
    @Comparable(type = 3)
    public int A0B = 0;
    @Comparable(type = 3)
    public int A0C = 0;
    @Comparable(type = 3)
    public int A0D = -16776961;
    @Comparable(type = 3)
    public int A0E = -1;
    @Comparable(type = 3)
    public int A0F = Integer.MAX_VALUE;
    @Comparable(type = 3)
    public int A0G = Integer.MAX_VALUE;
    @Comparable(type = 3)
    public int A0H = -1;
    @Comparable(type = 3)
    public int A0I = Integer.MIN_VALUE;
    @Comparable(type = 3)
    public int A0J = 0;
    @Comparable(type = 3)
    public int A0K;
    @Comparable(type = 3)
    public int A0L = -7829368;
    @Comparable(type = 3)
    public int A0M = 0;
    @Comparable(type = 3)
    public int A0N = -1;
    @Comparable(type = 3)
    public int A0O = A0o;
    @Comparable(type = 13)
    public ColorStateList A0P = A0p;
    @Comparable(type = 13)
    public Typeface A0Q = A0u;
    @Comparable(type = 13)
    public Layout.Alignment A0R;
    public Layout A0S;
    public Layout A0T;
    @Comparable(type = 13)
    public TextUtils.TruncateAt A0U;
    @Comparable(type = 13)
    public C22311Kv A0V;
    @Comparable(type = AnonymousClass1Y3.A02)
    public AnonymousClass10N A0W;
    @Comparable(type = 13)
    public AnonymousClass1JS A0X;
    @Comparable(type = 13)
    public C14920uM A0Y = A0v;
    @Comparable(type = 13)
    public AnonymousClass2CC A0Z;
    @Comparable(type = 13)
    public CharSequence A0a;
    public CharSequence A0b;
    @Comparable(type = 13)
    public CharSequence A0c;
    public Float A0d;
    public Integer A0e;
    public Integer A0f;
    @Comparable(type = 3)
    public boolean A0g;
    @Comparable(type = 3)
    public boolean A0h = true;
    @Comparable(type = 3)
    public boolean A0i = false;
    @Comparable(type = 3)
    public boolean A0j;
    @Comparable(type = 3)
    public boolean A0k;
    @Comparable(type = 3)
    public boolean A0l = true;
    public ClickableSpan[] A0m;
    public ImageSpan[] A0n;

    public static ComponentBuilderCBuilderShape0_0S0300000 A02(AnonymousClass0p4 r1) {
        return A03(r1, 0, 0);
    }

    public int A0M() {
        return 30;
    }

    public C14910uL() {
        super("Text");
    }

    public static ComponentBuilderCBuilderShape0_0S0300000 A03(AnonymousClass0p4 r2, int i, int i2) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(3);
        ComponentBuilderCBuilderShape0_0S0300000.A03(componentBuilderCBuilderShape0_0S0300000, r2, i, i2, new C14910uL());
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public static AnonymousClass1JS A04(Layout.Alignment alignment, AnonymousClass1JS r1) {
        if (r1 != null) {
            return r1;
        }
        if (alignment != null) {
            int i = AnonymousClass1RT.A00[alignment.ordinal()];
            if (i == 2) {
                return AnonymousClass1JS.TEXT_END;
            }
            if (i == 3) {
                return AnonymousClass1JS.CENTER;
            }
        }
        return AnonymousClass1JS.TEXT_START;
    }

    public static int A00(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE) {
            return Math.min(View.MeasureSpec.getSize(i), i2);
        }
        if (mode == 0) {
            return i2;
        }
        if (mode == 1073741824) {
            return View.MeasureSpec.getSize(i);
        }
        throw new IllegalStateException(AnonymousClass08S.A09("Unexpected size mode: ", View.MeasureSpec.getMode(i)));
    }

    public C17770zR A16() {
        C14910uL r1 = (C14910uL) super.A16();
        r1.A0m = null;
        r1.A0n = null;
        r1.A0S = null;
        r1.A0e = null;
        r1.A0f = null;
        r1.A0b = null;
        r1.A0T = null;
        r1.A0d = null;
        return r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0175, code lost:
        if (r4.BGQ(r5, 0, r5.length()) != false) goto L_0x01a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0180, code lost:
        if (r4.BGQ(r5, 0, r5.length()) != false) goto L_0x015b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0191, code lost:
        if (r1 == r4.BGQ(r5, 0, r5.length())) goto L_0x015b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01a2, code lost:
        if (r1 == r4.BGQ(r5, 0, r5.length())) goto L_0x01a4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.text.Layout A01(X.AnonymousClass0p4 r9, int r10, android.text.TextUtils.TruncateAt r11, boolean r12, int r13, float r14, float r15, float r16, int r17, boolean r18, java.lang.CharSequence r19, int r20, android.content.res.ColorStateList r21, int r22, int r23, float r24, float r25, float r26, int r27, android.graphics.Typeface r28, X.AnonymousClass1JS r29, boolean r30, X.C17660zG r31, int r32, int r33, int r34, int r35, float r36, int r37, int r38, int r39, X.C22311Kv r40, float r41) {
        /*
            r4 = r40
            r7 = r41
            X.1Ld r3 = new X.1Ld
            r3.<init>()
            r2 = 0
            r3.A05 = r2
            int r1 = android.view.View.MeasureSpec.getMode(r10)
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 == r0) goto L_0x01db
            if (r1 == 0) goto L_0x01d8
            r0 = 1073741824(0x40000000, float:2.0)
            if (r1 != r0) goto L_0x0200
            r6 = 1
        L_0x001b:
            if (r11 != 0) goto L_0x0024
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r13 == r0) goto L_0x0024
            android.text.TextUtils$TruncateAt r11 = android.text.TextUtils.TruncateAt.END
        L_0x0024:
            X.1QX r5 = r3.A06
            android.text.TextPaint r0 = r5.A0G
            float r0 = r0.density
            r1 = r36
            int r0 = (r0 > r36 ? 1 : (r0 == r36 ? 0 : -1))
            if (r0 == 0) goto L_0x003c
            r5.A00()
            X.1QX r0 = r3.A06
            android.text.TextPaint r0 = r0.A0G
            r0.density = r1
            r0 = 0
            r3.A04 = r0
        L_0x003c:
            X.1QX r1 = r3.A06
            android.text.TextUtils$TruncateAt r0 = r1.A0H
            if (r0 == r11) goto L_0x0047
            r1.A0H = r11
            r0 = 0
            r3.A04 = r0
        L_0x0047:
            int r0 = r1.A0D
            if (r0 == r13) goto L_0x0050
            r1.A0D = r13
            r0 = 0
            r3.A04 = r0
        L_0x0050:
            r1 = r16
            r0 = r17
            r3.A04(r14, r15, r1, r0)
            X.1QX r1 = r3.A06
            boolean r0 = r1.A0L
            r5 = r18
            if (r0 == r5) goto L_0x0064
            r1.A0L = r5
            r0 = 0
            r3.A04 = r0
        L_0x0064:
            r5 = r19
            r3.A0D(r5)
            int r8 = android.view.View.MeasureSpec.getSize(r10)
            X.1QX r1 = r3.A06
            int r0 = r1.A0F
            if (r0 != r8) goto L_0x0077
            int r0 = r1.A0E
            if (r0 == r6) goto L_0x007e
        L_0x0077:
            r1.A0F = r8
            r1.A0E = r6
            r0 = 0
            r3.A04 = r0
        L_0x007e:
            boolean r0 = r1.A0K
            if (r0 == r12) goto L_0x0087
            r1.A0K = r12
            r0 = 0
            r3.A04 = r0
        L_0x0087:
            r0 = r24
            r3.A03(r0)
            float r0 = r1.A09
            r8 = 2139095039(0x7f7fffff, float:3.4028235E38)
            int r0 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r0 != 0) goto L_0x00a2
            float r0 = r1.A0B
            r6 = r25
            int r0 = (r0 > r25 ? 1 : (r0 == r25 ? 0 : -1))
            if (r0 == 0) goto L_0x00a2
            r1.A0B = r6
            r0 = 0
            r3.A04 = r0
        L_0x00a2:
            android.text.TextPaint r0 = r1.A0G
            int r0 = r0.linkColor
            r6 = r22
            if (r0 == r6) goto L_0x00b6
            r1.A00()
            X.1QX r0 = r3.A06
            android.text.TextPaint r0 = r0.A0G
            r0.linkColor = r6
            r0 = 0
            r3.A04 = r0
        L_0x00b6:
            r0 = r39
            r3.A06(r0)
            X.1QX r1 = r3.A06
            int r0 = r1.A0C
            r6 = r37
            if (r0 == r6) goto L_0x00c8
            r1.A0C = r6
            r0 = 0
            r3.A04 = r0
        L_0x00c8:
            r0 = r38
            r3.A05(r0)
            r6 = -1
            r0 = r23
            if (r0 == r6) goto L_0x01cb
            r3.A08(r0)
        L_0x00d5:
            int r0 = (r41 > r8 ? 1 : (r41 == r8 ? 0 : -1))
            if (r0 == 0) goto L_0x00f6
            X.1QX r1 = r3.A06
            float r0 = r1.A09
            int r0 = (r0 > r41 ? 1 : (r0 == r41 ? 0 : -1))
            if (r0 == 0) goto L_0x00f6
            r1.A09 = r7
            android.text.TextPaint r0 = r1.A0G
            r8 = 0
            float r0 = r0.getFontMetrics(r8)
            float r7 = r41 - r0
            r1.A0A = r7
            X.1QX r1 = r3.A06
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.A0B = r0
            r3.A04 = r8
        L_0x00f6:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0119
            r1 = r26
            X.1QX r0 = r3.A06
            android.text.TextPaint r0 = r0.A0G
            float r0 = r0.getLetterSpacing()
            int r0 = (r0 > r26 ? 1 : (r0 == r26 ? 0 : -1))
            if (r0 == 0) goto L_0x0119
            X.1QX r0 = r3.A06
            r0.A00()
            X.1QX r0 = r3.A06
            android.text.TextPaint r0 = r0.A0G
            r0.setLetterSpacing(r1)
            r0 = 0
            r3.A04 = r0
        L_0x0119:
            r0 = r32
            if (r0 == r6) goto L_0x01c2
            r3.A02 = r0
            r0 = 1
            r3.A03 = r0
        L_0x0122:
            r0 = r33
            if (r0 == r6) goto L_0x01b9
            r3.A00 = r0
            r0 = 1
            r3.A01 = r0
        L_0x012b:
            r0 = r20
            if (r20 == 0) goto L_0x01b2
            r3.A07(r0)
        L_0x0132:
            android.graphics.Typeface r0 = X.C14910uL.A0t
            r1 = r28
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x01aa
            r3.A0B(r1)
        L_0x013f:
            r6 = r31
            if (r40 != 0) goto L_0x0149
            X.0zG r0 = X.C17660zG.RTL
            if (r6 != r0) goto L_0x01a7
            X.1Kv r4 = X.C22181Kf.A02
        L_0x0149:
            X.1QX r1 = r3.A06
            X.1Kv r0 = r1.A0I
            if (r0 == r4) goto L_0x0154
            r1.A0I = r4
            r0 = 0
            r3.A04 = r0
        L_0x0154:
            int r0 = r29.ordinal()
            switch(r0) {
                case 1: goto L_0x01a4;
                case 2: goto L_0x016a;
                case 3: goto L_0x0183;
                case 4: goto L_0x0194;
                case 5: goto L_0x016d;
                case 6: goto L_0x0178;
                default: goto L_0x015b;
            }
        L_0x015b:
            android.text.Layout$Alignment r0 = android.text.Layout.Alignment.ALIGN_NORMAL
        L_0x015d:
            r3.A0C(r0)
            android.text.Layout r3 = r3.A02()
            if (r30 == 0) goto L_0x01ff
            java.lang.Class<com.facebook.litho.widget.TextureWarmer> r1 = com.facebook.litho.widget.TextureWarmer.class
            monitor-enter(r1)
            goto L_0x01de
        L_0x016a:
            android.text.Layout$Alignment r0 = android.text.Layout.Alignment.ALIGN_CENTER
            goto L_0x015d
        L_0x016d:
            int r0 = r5.length()
            boolean r0 = r4.BGQ(r5, r2, r0)
            if (r0 == 0) goto L_0x015b
            goto L_0x01a4
        L_0x0178:
            int r0 = r5.length()
            boolean r0 = r4.BGQ(r5, r2, r0)
            if (r0 == 0) goto L_0x01a4
            goto L_0x015b
        L_0x0183:
            X.0zG r0 = X.C17660zG.RTL
            r1 = 0
            if (r6 != r0) goto L_0x0189
            r1 = 1
        L_0x0189:
            int r0 = r5.length()
            boolean r0 = r4.BGQ(r5, r2, r0)
            if (r1 != r0) goto L_0x01a4
            goto L_0x015b
        L_0x0194:
            X.0zG r0 = X.C17660zG.RTL
            r1 = 0
            if (r6 != r0) goto L_0x019a
            r1 = 1
        L_0x019a:
            int r0 = r5.length()
            boolean r0 = r4.BGQ(r5, r2, r0)
            if (r1 != r0) goto L_0x015b
        L_0x01a4:
            android.text.Layout$Alignment r0 = android.text.Layout.Alignment.ALIGN_OPPOSITE
            goto L_0x015d
        L_0x01a7:
            X.1Kv r4 = X.C22181Kf.A01
            goto L_0x0149
        L_0x01aa:
            android.graphics.Typeface r0 = android.graphics.Typeface.defaultFromStyle(r27)
            r3.A0B(r0)
            goto L_0x013f
        L_0x01b2:
            r0 = r21
            r3.A0A(r0)
            goto L_0x0132
        L_0x01b9:
            r0 = r35
            r3.A00 = r0
            r0 = 2
            r3.A01 = r0
            goto L_0x012b
        L_0x01c2:
            r0 = r34
            r3.A02 = r0
            r0 = 2
            r3.A03 = r0
            goto L_0x0122
        L_0x01cb:
            X.0z4 r1 = r9.A0B
            r0 = 1096810496(0x41600000, float:14.0)
            int r0 = r1.A01(r0)
            r3.A08(r0)
            goto L_0x00d5
        L_0x01d8:
            r6 = 0
            goto L_0x001b
        L_0x01db:
            r6 = 2
            goto L_0x001b
        L_0x01de:
            com.facebook.litho.widget.TextureWarmer r0 = com.facebook.litho.widget.TextureWarmer.A01     // Catch:{ all -> 0x01ec }
            if (r0 != 0) goto L_0x01e9
            com.facebook.litho.widget.TextureWarmer r0 = new com.facebook.litho.widget.TextureWarmer     // Catch:{ all -> 0x01ec }
            r0.<init>()     // Catch:{ all -> 0x01ec }
            com.facebook.litho.widget.TextureWarmer.A01 = r0     // Catch:{ all -> 0x01ec }
        L_0x01e9:
            com.facebook.litho.widget.TextureWarmer r0 = com.facebook.litho.widget.TextureWarmer.A01     // Catch:{ all -> 0x01ec }
            goto L_0x01ef
        L_0x01ec:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x01ef:
            monitor-exit(r1)
            X.5Xs r2 = r0.A00
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference
            r1.<init>(r3)
            r0 = 0
            android.os.Message r0 = r2.obtainMessage(r0, r1)
            r0.sendToTarget()
        L_0x01ff:
            return r3
        L_0x0200:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = "Unexpected size mode: "
            int r0 = android.view.View.MeasureSpec.getMode(r10)
            java.lang.String r0 = X.AnonymousClass08S.A09(r1, r0)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14910uL.A01(X.0p4, int, android.text.TextUtils$TruncateAt, boolean, int, float, float, float, int, boolean, java.lang.CharSequence, int, android.content.res.ColorStateList, int, int, float, float, float, int, android.graphics.Typeface, X.1JS, boolean, X.0zG, int, int, int, int, float, int, int, int, X.1Kv, float):android.text.Layout");
    }
}
