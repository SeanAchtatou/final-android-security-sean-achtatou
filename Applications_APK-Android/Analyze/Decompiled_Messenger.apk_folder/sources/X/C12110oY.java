package X;

import java.util.Comparator;

/* renamed from: X.0oY  reason: invalid class name and case insensitive filesystem */
public final class C12110oY implements Comparator {
    public final /* synthetic */ Integer A00;

    public C12110oY(Integer num) {
        this.A00 = num;
    }

    public int compare(Object obj, Object obj2) {
        C10800ks r10 = (C10800ks) obj;
        C10800ks r11 = (C10800ks) obj2;
        int i = -1;
        if (this.A00 == AnonymousClass07B.A00) {
            i = 1;
        }
        if (r10 != null) {
            if (r11 != null) {
                long j = r10.A00;
                if (j > 0) {
                    long j2 = r11.A00;
                    if (j2 > 0) {
                        if (j < j2) {
                            return -i;
                        }
                        if (j > j2) {
                            return i;
                        }
                        return 0;
                    }
                }
            }
            return -1;
        }
        return 1;
    }
}
