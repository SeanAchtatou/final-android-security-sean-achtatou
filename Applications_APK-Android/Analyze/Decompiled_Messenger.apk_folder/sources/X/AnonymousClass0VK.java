package X;

import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0VK  reason: invalid class name */
public interface AnonymousClass0VK extends AnonymousClass0VL, ScheduledExecutorService {
    AnonymousClass0Y0 C4Y(Runnable runnable, long j, TimeUnit timeUnit);

    AnonymousClass0Y0 C4Z(Callable callable, long j, TimeUnit timeUnit);
}
