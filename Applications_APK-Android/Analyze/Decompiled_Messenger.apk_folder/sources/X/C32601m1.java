package X;

import com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent;

/* renamed from: X.1m1  reason: invalid class name and case insensitive filesystem */
public final class C32601m1 implements AnonymousClass12H {
    public final /* synthetic */ MontageNonUserOmnistoreComponent A00;

    public C32601m1(MontageNonUserOmnistoreComponent montageNonUserOmnistoreComponent) {
        this.A00 = montageNonUserOmnistoreComponent;
    }

    public void BfC() {
        MontageNonUserOmnistoreComponent montageNonUserOmnistoreComponent = this.A00;
        if (((C17350yl) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BRG, montageNonUserOmnistoreComponent.A00)).A0C() && !((C24321Td) montageNonUserOmnistoreComponent.A02.get()).A04(AnonymousClass07B.A01)) {
            ((C24341Tf) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ASv, montageNonUserOmnistoreComponent.A00)).A02(montageNonUserOmnistoreComponent);
        }
    }
}
