package X;

import android.content.Context;
import android.graphics.PointF;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1vA  reason: invalid class name */
public final class AnonymousClass1vA extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C73513gF A01;
    @Comparable(type = 13)
    public StoryCard A02;
    @Comparable(type = 13)
    public C855643z A03;
    @Comparable(type = 13)
    public AnonymousClass4O4 A04;
    @Comparable(type = 13)
    public AnonymousClass46A A05;
    @Comparable(type = 13)
    public AnonymousClass4O9 A06;
    @Comparable(type = 13)
    public String A07;
    @Comparable(type = 13)
    public String A08;

    public static PointF A00(float f, float f2, float f3, float f4, float f5) {
        double radians = Math.toRadians((double) f5);
        double cos = Math.cos(radians);
        double sin = Math.sin(radians);
        double d = (double) (f - f3);
        double d2 = (double) (f2 - f4);
        return new PointF((float) (((d * cos) - (d2 * sin)) + ((double) f3)), (float) ((d * sin) + (d2 * cos) + ((double) f4)));
    }

    public int A0M() {
        return 3;
    }

    public AnonymousClass1vA(Context context) {
        super("StoryViewerTappableStickerComponent");
        this.A00 = new AnonymousClass0UN(13, AnonymousClass1XX.get(context));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006a, code lost:
        if (r11 != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d5, code lost:
        if (r11 != false) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00d7, code lost:
        r1 = r15.getTop() + r15.getHeight();
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C80043rl r13, int[] r14, android.view.View r15, double r16, X.AnonymousClass3SI r18, X.AnonymousClass4OF r19) {
        /*
            X.1I9 r1 = X.AnonymousClass1I9.A02
            r12 = r13
            com.facebook.resources.ui.FbTextView r0 = r13.A0A
            X.C21651Ie.A01(r0, r1)
            r13 = r15
            if (r14 != 0) goto L_0x0111
            r1 = 4599075939470750515(0x3fd3333333333333, double:0.3)
            int r0 = (r16 > r1 ? 1 : (r16 == r1 ? 0 : -1))
            r11 = 1
            if (r0 >= 0) goto L_0x0016
            r11 = 0
        L_0x0016:
            if (r11 == 0) goto L_0x010d
            java.lang.Integer r0 = X.AnonymousClass07B.A00
        L_0x001a:
            r12.A0K(r0)
            r0 = 2
            double[] r4 = new double[r0]
            float r0 = r15.getRotation()
            r5 = 0
            r6 = 1135869952(0x43b40000, float:360.0)
            int r0 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x0101
            float r0 = r15.getRotation()
            float r0 = r0 % r6
        L_0x0030:
            double r2 = (double) r0
            float r1 = r15.getRotation()
            r0 = 1127481344(0x43340000, float:180.0)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x00f0
            float r0 = r15.getRotation()
            float r0 = r0 - r6
        L_0x0040:
            double r2 = (double) r0
        L_0x0041:
            android.graphics.PointF r8 = new android.graphics.PointF
            int r1 = r15.getLeft()
            int r0 = r15.getWidth()
            int r0 = r0 >> 1
            int r1 = r1 + r0
            float r0 = (float) r1
            r8.<init>(r0, r5)
            r7 = 10
            if (r11 == 0) goto L_0x0058
            r7 = -10
        L_0x0058:
            r9 = 4636033603912859648(0x4056800000000000, double:90.0)
            r5 = -4587338432941916160(0xc056800000000000, double:-90.0)
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x00bb
            int r0 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r0 >= 0) goto L_0x00bb
            if (r11 == 0) goto L_0x00d7
        L_0x006c:
            int r1 = r15.getTop()
        L_0x0070:
            float r0 = (float) r1
            r8.y = r0
        L_0x0073:
            float r5 = r8.x
            float r3 = r8.y
            int r0 = r15.getLeft()
            float r2 = (float) r0
            int r0 = r15.getTop()
            float r1 = (float) r0
            float r0 = r15.getRotation()
            android.graphics.PointF r3 = A00(r5, r3, r2, r1, r0)
            r2 = 0
            float r1 = r3.x
            int r0 = r15.getLeft()
            float r0 = (float) r0
            float r1 = r1 - r0
            double r0 = (double) r1
            r4[r2] = r0
            r5 = 1
            float r1 = r3.y
            int r0 = r15.getTop()
            float r0 = (float) r0
            float r1 = r1 - r0
            float r0 = (float) r7
            float r1 = r1 + r0
            double r2 = (double) r1
            r4[r5] = r2
            r0 = 0
            r0 = r4[r0]
            int r14 = (int) r0
            int r15 = (int) r2
        L_0x00a8:
            r16 = 1
            r17 = 1
            r12.A0H(r13, r14, r15, r16, r17)
            r0 = r18
            r12.A0J = r0
            r0 = r19
            r12.A0I = r0
            r12.A0C()
            return
        L_0x00bb:
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x00c8
            r5 = -4582834833314545664(0xc066800000000000, double:-180.0)
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x00d5
        L_0x00c8:
            int r0 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r0 <= 0) goto L_0x00e1
            r5 = 4640537203540230144(0x4066800000000000, double:180.0)
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x00e1
        L_0x00d5:
            if (r11 == 0) goto L_0x006c
        L_0x00d7:
            int r1 = r15.getTop()
            int r0 = r15.getHeight()
            int r1 = r1 + r0
            goto L_0x0070
        L_0x00e1:
            int r1 = r15.getTop()
            int r0 = r15.getHeight()
            int r0 = r0 >> 1
            int r1 = r1 + r0
            float r0 = (float) r1
            r8.y = r0
            goto L_0x0073
        L_0x00f0:
            float r1 = r15.getRotation()
            r0 = -1020002304(0xffffffffc3340000, float:-180.0)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x0041
            float r0 = r15.getRotation()
            float r0 = r0 + r6
            goto L_0x0040
        L_0x0101:
            float r0 = r15.getRotation()
            float r0 = java.lang.Math.abs(r0)
            float r0 = r0 % r6
            float r0 = -r0
            goto L_0x0030
        L_0x010d:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            goto L_0x001a
        L_0x0111:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r12.A0K(r0)
            r0 = 0
            r0 = r14[r0]
            float r4 = (float) r0
            r0 = 1
            r0 = r14[r0]
            float r3 = (float) r0
            int r0 = r15.getLeft()
            float r2 = (float) r0
            int r0 = r15.getTop()
            float r1 = (float) r0
            float r0 = r15.getRotation()
            android.graphics.PointF r2 = A00(r4, r3, r2, r1, r0)
            r0 = 1094713344(0x41400000, float:12.0)
            int r1 = X.AnonymousClass3OI.A00(r0)
            float r0 = r2.x
            int r14 = (int) r0
            float r0 = r2.y
            int r15 = (int) r0
            int r15 = r15 - r1
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1vA.A01(X.3rl, int[], android.view.View, double, X.3SI, X.4OF):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v19 */
    /* JADX WARN: Type inference failed for: r0v26 */
    /* JADX WARN: Type inference failed for: r0v41 */
    /* JADX WARN: Type inference failed for: r0v42 */
    /* JADX WARN: Type inference failed for: r0v47, types: [android.view.View$OnClickListener] */
    /* JADX WARN: Type inference failed for: r19v6, types: [X.4Nx] */
    /* JADX WARN: Type inference failed for: r26v9, types: [X.4Nu] */
    /* JADX WARN: Type inference failed for: r0v49 */
    /* JADX WARN: Type inference failed for: r18v5, types: [X.4O7] */
    /* JADX WARN: Type inference failed for: r21v10, types: [X.4Nv] */
    /* JADX WARN: Type inference failed for: r27v9, types: [X.4ei] */
    /* JADX WARN: Type inference failed for: r0v50 */
    /* JADX WARN: Type inference failed for: r23v10, types: [X.4Nw] */
    /* JADX WARN: Type inference failed for: r28v9, types: [X.4ei] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0461, code lost:
        if (r0 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x046c, code lost:
        if (r0 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0477, code lost:
        if (r0 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0482, code lost:
        if (r0 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x048d, code lost:
        if (r0 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0498, code lost:
        if (r0 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x04a3, code lost:
        if (r0 == false) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x050a, code lost:
        if (r0.equals("StoryOverlayResharedContent") == false) goto L_0x0110;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0515, code lost:
        if (r0.equals("StoryOverlayLinkSticker") == false) goto L_0x0110;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x051a, code lost:
        if (r16 == false) goto L_0x0110;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0525, code lost:
        if (r0.equals("StoryOverlayResharedPost") == false) goto L_0x0110;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0530, code lost:
        if (r0.equals("StoryOverlayLocationSticker") == false) goto L_0x0110;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0268, code lost:
        if (r4.equals("0") != false) goto L_0x026a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x00e2, code lost:
        if (r10.Aem(r2) == false) goto L_0x00e4;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:158:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0j(X.AnonymousClass0p4 r43, java.lang.Object r44) {
        /*
            r42 = this;
            r8 = r44
            r3 = r42
            com.facebook.resources.ui.FbFrameLayout r8 = (com.facebook.resources.ui.FbFrameLayout) r8
            com.facebook.ipc.stories.model.StoryCard r0 = r3.A02
            r41 = r0
            java.lang.String r0 = r3.A08
            r31 = r0
            X.4O4 r15 = r3.A04
            X.4O9 r0 = r3.A06
            r30 = r0
            X.3gF r9 = r3.A01
            java.lang.String r0 = r3.A07
            X.46A r1 = r3.A05
            r34 = r1
            int r2 = X.AnonymousClass1Y3.BPQ
            X.0UN r3 = r3.A00
            r1 = 11
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.4MH r1 = (X.AnonymousClass4MH) r1
            int r4 = X.AnonymousClass1Y3.BIA
            r2 = 12
            java.lang.Object r17 = X.AnonymousClass1XX.A02(r2, r4, r3)
            r2 = r17
            X.3zI r2 = (X.C84353zI) r2
            r17 = r2
            int r4 = X.AnonymousClass1Y3.AZn
            r2 = 2
            java.lang.Object r23 = X.AnonymousClass1XX.A02(r2, r4, r3)
            r2 = r23
            X.3b0 r2 = (X.C70643b0) r2
            r23 = r2
            int r4 = X.AnonymousClass1Y3.AT5
            r2 = 10
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.0pR r7 = (X.C12480pR) r7
            int r4 = X.AnonymousClass1Y3.A1w
            r2 = 4
            java.lang.Object r22 = X.AnonymousClass1XX.A02(r2, r4, r3)
            r2 = r22
            X.4Nt r2 = (X.C89134Nt) r2
            r22 = r2
            int r4 = X.AnonymousClass1Y3.ACH
            r2 = 3
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.4Nj r4 = (X.C89034Nj) r4
            int r5 = X.AnonymousClass1Y3.BLE
            r2 = 5
            java.lang.Object r21 = X.AnonymousClass1XX.A02(r2, r5, r3)
            r2 = r21
            X.4N4 r2 = (X.AnonymousClass4N4) r2
            r21 = r2
            int r5 = X.AnonymousClass1Y3.BH4
            r2 = 9
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r5, r3)
            X.00z r5 = (X.C001500z) r5
            int r6 = X.AnonymousClass1Y3.B7g
            r2 = 8
            java.lang.Object r20 = X.AnonymousClass1XX.A02(r2, r6, r3)
            r2 = r20
            X.4ek r2 = (X.C94544ek) r2
            r20 = r2
            int r6 = X.AnonymousClass1Y3.A7v
            r2 = 1
            java.lang.Object r19 = X.AnonymousClass1XX.A02(r2, r6, r3)
            r2 = r19
            X.6JL r2 = (X.AnonymousClass6JL) r2
            r19 = r2
            int r6 = X.AnonymousClass1Y3.AiX
            r2 = 0
            java.lang.Object r18 = X.AnonymousClass1XX.A02(r2, r6, r3)
            r2 = r18
            X.4OL r2 = (X.AnonymousClass4OL) r2
            r18 = r2
            int r6 = X.AnonymousClass1Y3.B5n
            r2 = 7
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r6, r3)
            X.1tu r6 = (X.C36831tu) r6
            r2 = r41
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = r2.A0J()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = X.C95184fz.A02(r2)
            if (r2 == 0) goto L_0x018a
            java.lang.String r2 = "StoryOverlayTagSticker"
            boolean r16 = r0.equals(r2)
            if (r16 == 0) goto L_0x0534
            com.facebook.graphql.enums.GraphQLStoryOverlayTagType r3 = com.facebook.graphql.enums.GraphQLStoryOverlayTagType.A05
            r2 = 3575610(0x368f3a, float:5.010497E-39)
            java.lang.Enum r3 = r9.A0O(r2, r3)
            com.facebook.graphql.enums.GraphQLStoryOverlayTagType r3 = (com.facebook.graphql.enums.GraphQLStoryOverlayTagType) r3
            com.facebook.graphql.enums.GraphQLStoryOverlayTagType r2 = com.facebook.graphql.enums.GraphQLStoryOverlayTagType.A04
            if (r3 != r2) goto L_0x058e
            r10 = 0
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r10, r3, r2)
            X.1Yd r10 = (X.C25051Yd) r10
            r2 = 283987534941889(0x1024900240ec1, double:1.40308484861928E-309)
        L_0x00de:
            boolean r2 = r10.Aem(r2)
            if (r2 != 0) goto L_0x058e
        L_0x00e4:
            r2 = 0
        L_0x00e5:
            if (r2 == 0) goto L_0x018a
            int r2 = r8.getChildCount()
            r3 = r43
            if (r2 != 0) goto L_0x00f7
            com.facebook.litho.LithoView r2 = new com.facebook.litho.LithoView
            r2.<init>(r3)
            r8.addView(r2)
        L_0x00f7:
            r2 = 0
            android.view.View r2 = r8.getChildAt(r2)
            com.facebook.litho.LithoView r2 = (com.facebook.litho.LithoView) r2
            r8 = 2131300914(0x7f091232, float:1.8219871E38)
            r2.setId(r8)
            int r8 = r0.hashCode()
            r14 = 4
            r13 = 3
            r12 = 2
            r11 = 0
            r10 = 1
            switch(r8) {
                case -1187750771: goto L_0x0529;
                case 464250483: goto L_0x051e;
                case 612587614: goto L_0x0519;
                case 1936822056: goto L_0x050e;
                case 2034282950: goto L_0x0503;
                default: goto L_0x0110;
            }
        L_0x0110:
            r8 = -1
        L_0x0111:
            if (r8 == 0) goto L_0x04ce
            if (r8 == r10) goto L_0x04c9
            if (r8 == r12) goto L_0x04c4
            if (r8 == r13) goto L_0x04bf
            if (r8 == r14) goto L_0x04a7
            r8 = 2131823783(0x7f110ca7, float:1.9280375E38)
        L_0x011e:
            java.lang.String r8 = r3.A0C(r8)
            r2.setContentDescription(r8)
        L_0x0125:
            X.1I9 r8 = X.AnonymousClass1I9.A02
            X.C21651Ie.A01(r2, r8)
            r16 = r41
            r8 = r0
            int r0 = r0.hashCode()
            r10 = 0
            switch(r0) {
                case -2135965385: goto L_0x045a;
                case -1187750771: goto L_0x0465;
                case 464250483: goto L_0x0470;
                case 612587614: goto L_0x047b;
                case 814842842: goto L_0x0486;
                case 1936822056: goto L_0x0491;
                case 2034282950: goto L_0x049c;
                default: goto L_0x0135;
            }
        L_0x0135:
            r8 = -1
        L_0x0136:
            r0 = 0
            switch(r8) {
                case 0: goto L_0x018b;
                case 1: goto L_0x01eb;
                case 2: goto L_0x02d3;
                case 3: goto L_0x0316;
                case 4: goto L_0x03a8;
                case 5: goto L_0x03b8;
                case 6: goto L_0x0431;
                default: goto L_0x013a;
            }
        L_0x013a:
            if (r0 == 0) goto L_0x018a
            X.6Ek r6 = new X.6Ek
            r6.<init>()
            X.4Q5 r7 = new X.4Q5
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r41.A0J()
            int r3 = X.C95184fz.A01(r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r41.A0J()
            int r1 = X.C95184fz.A00(r1)
            r7.<init>(r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = r9.Af1()
            if (r5 == 0) goto L_0x017c
            int r3 = X.AnonymousClass1Y3.AM1
            r1 = r17
            X.0UN r1 = r1.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r10, r3, r1)
            X.4go r4 = (X.C95644go) r4
            android.content.res.Resources r3 = r2.getResources()
            r1 = 2132148265(0x7f160029, float:1.9938503E38)
            int r1 = r3.getDimensionPixelSize(r1)
            int r1 = r1 << 1
            X.4gq r1 = r4.A06(r2, r7, r5, r1)
            X.C95644go.A03(r2, r1)
        L_0x017c:
            r2.setOnClickListener(r0)
            boolean r1 = r0 instanceof X.AnonymousClass4LT
            if (r1 == 0) goto L_0x0591
            X.4LT r0 = (X.AnonymousClass4LT) r0
            r0.A00 = r6
            r2.setOnTouchListener(r0)
        L_0x018a:
            return
        L_0x018b:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r6 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r5 = 596101811(0x2387cab3, float:1.4722576E-17)
            r4 = -34479968(0xfffffffffdf1e0a0, float:-4.0188783E37)
            com.facebook.graphservice.tree.TreeJNI r7 = r9.A0J(r5, r6, r4)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r7 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r7
            if (r7 == 0) goto L_0x013a
            java.lang.String r4 = X.C861849i.A00(r16)
            if (r4 == 0) goto L_0x013a
            r5 = -1796733735(0xffffffff94e804d9, float:-2.3427926E-26)
            r4 = 34273111(0x20af757, float:1.0209622E-37)
            com.facebook.graphservice.tree.TreeJNI r5 = r7.A0J(r5, r6, r4)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            if (r5 == 0) goto L_0x013a
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = r9.Af1()
            if (r4 == 0) goto L_0x013a
            java.lang.String r26 = X.C861849i.A00(r16)
            java.lang.String r27 = r5.A3m()
            double r37 = r4.A0W()
            X.4OK r24 = new X.4OK
            r28 = r18
            r29 = r19
            r30 = r3
            r31 = r23
            r25 = r21
            r24.<init>(r25, r26, r27, r28, r29, r30, r31)
            X.4ei r0 = new X.4ei
            r28 = r0
            r29 = r22
            r30 = r26
            r31 = r1
            r32 = r20
            r33 = r27
            r35 = r15
            r36 = r21
            r39 = r24
            r40 = r3
            r28.<init>(r29, r30, r31, r32, r33, r34, r35, r36, r37, r39, r40)
            goto L_0x013a
        L_0x01eb:
            com.facebook.graphql.enums.GraphQLStoryOverlayTagType r6 = com.facebook.graphql.enums.GraphQLStoryOverlayTagType.A05
            r4 = 3575610(0x368f3a, float:5.010497E-39)
            java.lang.Enum r6 = r9.A0O(r4, r6)
            com.facebook.graphql.enums.GraphQLStoryOverlayTagType r6 = (com.facebook.graphql.enums.GraphQLStoryOverlayTagType) r6
            r4 = -881241120(0xffffffffcb7953e0, float:-1.6339936E7)
            java.lang.String r4 = r9.A0P(r4)
            if (r4 == 0) goto L_0x013a
            if (r6 == 0) goto L_0x013a
            java.lang.String r25 = X.C861849i.A00(r16)
            int r7 = r6.ordinal()
            switch(r7) {
                case 1: goto L_0x0298;
                case 2: goto L_0x022d;
                case 3: goto L_0x020e;
                case 4: goto L_0x020c;
                case 5: goto L_0x020c;
                case 6: goto L_0x022d;
                default: goto L_0x020c;
            }
        L_0x020c:
            goto L_0x013a
        L_0x020e:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r9.Af1()
            com.google.common.base.Preconditions.checkNotNull(r0)
            double r27 = r0.A0W()
            X.4Kq r29 = new X.4Kq
            r29.<init>()
            X.4Nw r0 = new X.4Nw
            r23 = r0
            r24 = r22
            r26 = r15
            r30 = r3
            r23.<init>(r24, r25, r26, r27, r29, r30)
            goto L_0x013a
        L_0x022d:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r8 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r7 = -309425751(0xffffffffed8e89a9, float:-5.5141615E27)
            r1 = 1332447303(0x4f6b8847, float:3.95157683E9)
            com.facebook.graphservice.tree.TreeJNI r1 = r9.A0J(r7, r8, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 == 0) goto L_0x0241
            java.lang.String r0 = r1.A3r()
        L_0x0241:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r9.Af1()
            com.google.common.base.Preconditions.checkNotNull(r1)
            double r35 = r1.A0W()
            r1 = 224242782(0xd5dac5e, float:6.830836E-31)
            java.lang.String r33 = r9.A0P(r1)
            r1 = -724103475(0xffffffffd4d70ecd, float:-7.3893303E12)
            java.lang.String r28 = r9.A0P(r1)
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r4)
            if (r1 != 0) goto L_0x026a
            java.lang.String r1 = "0"
            boolean r1 = r4.equals(r1)
            r37 = 1
            if (r1 == 0) goto L_0x026c
        L_0x026a:
            r37 = 0
        L_0x026c:
            X.4OI r21 = new X.4OI
            r24 = r41
            r23 = r31
            r26 = r4
            r27 = r30
            r29 = r6
            r21.<init>(r22, r23, r24, r25, r26, r27, r28, r29)
            X.4OH r23 = new X.4OH
            r27 = r41
            r24 = r22
            r26 = r31
            r28 = r4
            r29 = r15
            r30 = r3
            r31 = r5
            r32 = r0
            r34 = r6
            r38 = r21
            r23.<init>(r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r37, r38)
            r0 = r23
            goto L_0x013a
        L_0x0298:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r9.Af1()
            com.google.common.base.Preconditions.checkNotNull(r0)
            double r36 = r0.A0W()
            X.4OK r26 = new X.4OK
            r27 = r21
            r28 = r25
            r29 = r4
            r30 = r18
            r31 = r19
            r32 = r3
            r33 = r23
            r26.<init>(r27, r28, r29, r30, r31, r32, r33)
            X.4ei r0 = new X.4ei
            r27 = r0
            r28 = r22
            r29 = r25
            r30 = r1
            r31 = r20
            r32 = r4
            r33 = r34
            r34 = r15
            r35 = r21
            r38 = r26
            r39 = r3
            r27.<init>(r28, r29, r30, r31, r32, r33, r34, r35, r36, r38, r39)
            goto L_0x013a
        L_0x02d3:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = r9.AdW()
            if (r5 == 0) goto L_0x013a
            java.lang.String r20 = r5.A3m()
            if (r20 == 0) goto L_0x013a
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r9.Af1()
            if (r1 == 0) goto L_0x013a
            java.lang.String r28 = r9.Abp()
            if (r28 == 0) goto L_0x013a
            java.lang.String r23 = r5.A42()
            com.google.common.base.Preconditions.checkNotNull(r1)
            double r26 = r1.A0W()
            X.4Kp r18 = new X.4Kp
            r21 = r41
            r22 = r30
            r19 = r4
            r18.<init>(r19, r20, r21, r22, r23)
            X.4Nv r0 = new X.4Nv
            r25 = r41
            r21 = r0
            r22 = r15
            r23 = r4
            r24 = r20
            r29 = r18
            r30 = r3
            r21.<init>(r22, r23, r24, r25, r26, r28, r29, r30)
            goto L_0x013a
        L_0x0316:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r12 = r9.AsN()
            if (r12 == 0) goto L_0x03a8
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r11 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r8 = -1606223187(0xffffffffa042faad, float:-1.6515381E-19)
            r5 = 20882207(0x13ea31f, float:3.5014522E-38)
            com.facebook.graphservice.tree.TreeJNI r12 = r12.A0J(r8, r11, r5)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r12 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r12
            if (r12 == 0) goto L_0x03a8
            java.lang.String r8 = r12.A42()
            if (r8 == 0) goto L_0x03a8
            com.facebook.graphql.enums.GraphQLStoryOverlayLinkStickerStyle r14 = r9.AsO()
            if (r14 == 0) goto L_0x03a8
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r13 = r9.Af1()
            if (r13 == 0) goto L_0x03a8
            java.lang.String r7 = X.C861849i.A00(r16)
            r5 = 1843998832(0x6de93070, float:9.0210706E27)
            r0 = -31612296(0xfffffffffe1da278, float:-5.2383095E37)
            com.google.common.collect.ImmutableList r11 = r12.A0M(r5, r11, r0)
            boolean r0 = X.C013509w.A01(r11)
            java.lang.String r21 = ""
            if (r0 == 0) goto L_0x03a5
            java.lang.Object r5 = r11.get(r10)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            r0 = 1108410966(0x42110256, float:36.25228)
            java.lang.String r21 = r5.A0P(r0)
            java.lang.Object r5 = r11.get(r10)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            r0 = 1806572395(0x6bae1b6b, float:4.2096514E26)
            java.lang.String r22 = r5.A0P(r0)
        L_0x036e:
            java.lang.String r23 = r14.toString()
            double r28 = r13.A0W()
            X.4N2 r5 = new X.4N2
            r0 = r30
            r5.<init>(r4, r7, r0, r8)
            X.3zG r25 = new X.3zG
            r25.<init>()
            X.4O2 r7 = new X.4O2
            r7.<init>(r15)
            X.4OE r4 = new X.4OE
            r4.<init>(r15)
            X.4O7 r0 = new X.4O7
            r18 = r0
            r19 = r6
            r20 = r15
            r24 = r30
            r26 = r7
            r27 = r1
            r30 = r5
            r31 = r3
            r32 = r4
            r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r30, r31, r32)
            goto L_0x013a
        L_0x03a5:
            r22 = r21
            goto L_0x036e
        L_0x03a8:
            r1 = -499995626(0xffffffffe232ac16, float:-8.2398014E20)
            java.lang.String r1 = r9.A0P(r1)
            if (r1 == 0) goto L_0x013a
            X.4Ym r0 = new X.4Ym
            r0.<init>(r1, r7, r3)
            goto L_0x013a
        L_0x03b8:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r9.Af1()
            if (r0 == 0) goto L_0x042e
            java.lang.String r23 = r9.Ajp()
            if (r23 == 0) goto L_0x042e
            X.4O0 r6 = new X.4O0
            r6.<init>(r15)
            X.4OG r35 = new X.4OG
            r35.<init>()
            X.4OD r5 = new X.4OD
            r5.<init>(r15)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r27 = r9.B45()
            if (r27 == 0) goto L_0x03f8
            r8 = r27
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r7 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -2034953805(0xffffffff86b511b3, float:-6.811064E-35)
            r0 = 1178881545(0x46444e09, float:12563.509)
            com.facebook.graphservice.tree.TreeJNI r7 = r8.A0J(r1, r7, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r7 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r7
            if (r7 == 0) goto L_0x03f8
            com.facebook.graphql.enums.GraphQLStoryCardTypes r1 = com.facebook.graphql.enums.GraphQLStoryCardTypes.A0F
            r0 = -2034615233(0xffffffff86ba3c3f, float:-7.005395E-35)
            java.lang.Enum r1 = r7.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLStoryCardTypes r1 = (com.facebook.graphql.enums.GraphQLStoryCardTypes) r1
            if (r1 != 0) goto L_0x03fa
        L_0x03f8:
            com.facebook.graphql.enums.GraphQLStoryCardTypes r1 = com.facebook.graphql.enums.GraphQLStoryCardTypes.A0E
        L_0x03fa:
            com.facebook.graphql.enums.GraphQLStoryCardTypes r0 = com.facebook.graphql.enums.GraphQLStoryCardTypes.A0A
            r25 = 0
            if (r1 != r0) goto L_0x0402
            r25 = 1
        L_0x0402:
            X.4Nr r18 = new X.4Nr
            r20 = r41
            r19 = r4
            r21 = r31
            r22 = r30
            r24 = r6
            r26 = r9
            r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r27)
            X.4Nu r0 = new X.4Nu
            r29 = r41
            r26 = r0
            r27 = r15
            r28 = r4
            r30 = r31
            r31 = r3
            r32 = r9
            r33 = r25
            r34 = r18
            r36 = r5
            r26.<init>(r27, r28, r29, r30, r31, r32, r33, r34, r35, r36)
            goto L_0x013a
        L_0x042e:
            r0 = 0
            goto L_0x013a
        L_0x0431:
            r0 = -761937713(0xffffffffd295c0cf, float:-3.21592459E11)
            java.lang.String r20 = r9.A0P(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r9.Af1()
            double r0 = r0.A0W()
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            com.google.common.base.Preconditions.checkNotNull(r0)
            double r23 = r0.doubleValue()
            X.4Nx r0 = new X.4Nx
            r19 = r0
            r21 = r22
            r22 = r15
            r25 = r3
            r19.<init>(r20, r21, r22, r23, r25)
            goto L_0x013a
        L_0x045a:
            java.lang.String r0 = "StoryOverlayFundraiserSticker"
            boolean r0 = r8.equals(r0)
            r8 = 6
            if (r0 != 0) goto L_0x0136
            goto L_0x0135
        L_0x0465:
            java.lang.String r0 = "StoryOverlayLocationSticker"
            boolean r0 = r8.equals(r0)
            r8 = 0
            if (r0 != 0) goto L_0x0136
            goto L_0x0135
        L_0x0470:
            java.lang.String r0 = "StoryOverlayResharedPost"
            boolean r0 = r8.equals(r0)
            r8 = 2
            if (r0 != 0) goto L_0x0136
            goto L_0x0135
        L_0x047b:
            java.lang.String r0 = "StoryOverlayTagSticker"
            boolean r0 = r8.equals(r0)
            r8 = 1
            if (r0 != 0) goto L_0x0136
            goto L_0x0135
        L_0x0486:
            java.lang.String r0 = "StoryOverlayDynamicBrandSticker"
            boolean r0 = r8.equals(r0)
            r8 = 4
            if (r0 != 0) goto L_0x0136
            goto L_0x0135
        L_0x0491:
            java.lang.String r0 = "StoryOverlayLinkSticker"
            boolean r0 = r8.equals(r0)
            r8 = 3
            if (r0 != 0) goto L_0x0136
            goto L_0x0135
        L_0x049c:
            java.lang.String r0 = "StoryOverlayResharedContent"
            boolean r0 = r8.equals(r0)
            r8 = 5
            if (r0 != 0) goto L_0x0136
            goto L_0x0135
        L_0x04a7:
            com.facebook.graphql.enums.GraphQLStoryOverlayTagType r11 = com.facebook.graphql.enums.GraphQLStoryOverlayTagType.A02
            com.facebook.graphql.enums.GraphQLStoryOverlayTagType r10 = com.facebook.graphql.enums.GraphQLStoryOverlayTagType.A05
            r8 = 3575610(0x368f3a, float:5.010497E-39)
            java.lang.Enum r8 = r9.A0O(r8, r10)
            com.facebook.graphql.enums.GraphQLStoryOverlayTagType r8 = (com.facebook.graphql.enums.GraphQLStoryOverlayTagType) r8
            boolean r8 = r11.equals(r8)
            if (r8 != 0) goto L_0x04c4
            r8 = 2131833665(0x7f113341, float:1.9300418E38)
            goto L_0x011e
        L_0x04bf:
            r8 = 2131826224(0x7f111630, float:1.9285326E38)
            goto L_0x011e
        L_0x04c4:
            r8 = 2131826621(0x7f1117bd, float:1.9286132E38)
            goto L_0x011e
        L_0x04c9:
            r8 = 2131831803(0x7f112bfb, float:1.9296642E38)
            goto L_0x011e
        L_0x04ce:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r13 = r9.B45()
            if (r13 == 0) goto L_0x0500
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r12 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r10 = -1422944994(0xffffffffab2f951e, float:-6.237943E-13)
            r8 = -910078847(0xffffffffc9c14c81, float:-1583504.1)
            com.google.common.collect.ImmutableList r10 = r13.A0M(r10, r12, r8)
            boolean r8 = r10.isEmpty()
            if (r8 != 0) goto L_0x0500
            java.lang.Object r8 = r10.get(r11)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r8 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r8
            java.lang.String r8 = r8.A3r()
        L_0x04f0:
            r10 = 2131831804(0x7f112bfc, float:1.9296644E38)
            java.lang.Object[] r8 = new java.lang.Object[]{r8}
            java.lang.String r8 = r3.A0D(r10, r8)
            r2.setContentDescription(r8)
            goto L_0x0125
        L_0x0500:
            java.lang.String r8 = ""
            goto L_0x04f0
        L_0x0503:
            java.lang.String r8 = "StoryOverlayResharedContent"
            boolean r16 = r0.equals(r8)
            r8 = 0
            if (r16 != 0) goto L_0x0111
            goto L_0x0110
        L_0x050e:
            java.lang.String r8 = "StoryOverlayLinkSticker"
            boolean r16 = r0.equals(r8)
            r8 = 3
            if (r16 != 0) goto L_0x0111
            goto L_0x0110
        L_0x0519:
            r8 = 4
            if (r16 != 0) goto L_0x0111
            goto L_0x0110
        L_0x051e:
            java.lang.String r8 = "StoryOverlayResharedPost"
            boolean r16 = r0.equals(r8)
            r8 = 1
            if (r16 != 0) goto L_0x0111
            goto L_0x0110
        L_0x0529:
            java.lang.String r8 = "StoryOverlayLocationSticker"
            boolean r16 = r0.equals(r8)
            r8 = 2
            if (r16 != 0) goto L_0x0111
            goto L_0x0110
        L_0x0534:
            java.lang.String r2 = "StoryOverlayLinkSticker"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0554
            int r10 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r1.A00
            r2 = 0
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r2, r10, r3)
            X.1Yd r10 = (X.C25051Yd) r10
            r2 = 282089157362787(0x1008f00050463, double:1.393705617172584E-309)
            boolean r2 = r10.Aem(r2)
            if (r2 != 0) goto L_0x0554
            goto L_0x00e4
        L_0x0554:
            java.lang.String r2 = "StoryOverlayLocationSticker"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0574
            int r10 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r1.A00
            r2 = 0
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r2, r10, r3)
            X.1Yd r10 = (X.C25051Yd) r10
            r2 = 286500088454000(0x1049200001b70, double:1.415498512356E-309)
            boolean r2 = r10.Aem(r2)
            if (r2 != 0) goto L_0x0574
            goto L_0x00e4
        L_0x0574:
            java.lang.String r2 = "StoryOverlayFundraiserSticker"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x058e
            int r10 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r1.A00
            r2 = 0
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r2, r10, r3)
            X.1Yd r10 = (X.C25051Yd) r10
            r2 = 286332584729314(0x1046b00001ae2, double:1.41467093399682E-309)
            goto L_0x00de
        L_0x058e:
            r2 = 1
            goto L_0x00e5
        L_0x0591:
            r2.setOnTouchListener(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1vA.A0j(X.0p4, java.lang.Object):void");
    }
}
