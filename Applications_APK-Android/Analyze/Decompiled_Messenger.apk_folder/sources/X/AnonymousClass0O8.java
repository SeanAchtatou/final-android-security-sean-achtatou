package X;

import java.util.regex.Pattern;

/* renamed from: X.0O8  reason: invalid class name */
public interface AnonymousClass0O8 {
    public static final Pattern A00 = AnonymousClass0HT.A00("FbPatterns.Phone.PATTERN", "(\\+[0-9]+[\\- \\.]*)?(\\([0-9]+\\)[\\- \\.]*)?([0-9][0-9\\- \\.]+[0-9])");
}
