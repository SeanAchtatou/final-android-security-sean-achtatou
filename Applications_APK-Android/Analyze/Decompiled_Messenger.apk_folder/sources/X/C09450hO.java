package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0hO  reason: invalid class name and case insensitive filesystem */
public final class C09450hO extends AnonymousClass0UV {
    private static volatile C09400hF A00;

    public static final C09400hF A01(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (C09400hF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new AnonymousClass0hG(C04490Ux.A0I(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C180868aK A02() {
        return new C180868aK();
    }

    public static final C09400hF A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
