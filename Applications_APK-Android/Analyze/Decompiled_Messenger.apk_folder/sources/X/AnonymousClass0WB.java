package X;

import android.os.SystemClock;
import com.facebook.common.util.TriState;
import com.facebook.gk.store.GatekeeperWriter;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: X.0WB  reason: invalid class name */
public final class AnonymousClass0WB implements AnonymousClass1YI, GatekeeperWriter {
    public C05000Xg A00;
    private A63 A01;
    private boolean A02 = false;
    public final AnonymousClass0X4 A03;
    public final AnonymousClass0WH A04;
    public final AnonymousClass0WC A05;
    public final AnonymousClass0X3 A06;
    public final AnonymousClass38T A07;
    public final AnonymousClass0WG A08;

    public static synchronized A63 A01(AnonymousClass0WB r2) {
        A63 a63;
        synchronized (r2) {
            if (r2.A01 == null) {
                r2.A01 = new A63(r2.A05);
            }
            a63 = r2.A01;
        }
        return a63;
    }

    public static void A03(AnonymousClass0WB r8, TriState[] triStateArr, TriState[] triStateArr2, boolean z, boolean z2) {
        ArrayList arrayList;
        C05000Xg r0;
        synchronized (r8) {
            A02(r8);
            arrayList = new ArrayList();
            int length = triStateArr.length;
            for (int i = 0; i < length; i++) {
                if (!z || !r8.A03.A06(i)) {
                    TriState A012 = r8.A03.A01(i);
                    TriState triState = triStateArr[i];
                    if (triState != null) {
                        if (triState == TriState.UNSET) {
                            r8.A03.A03(i);
                        } else {
                            r8.A03.A04(i, triState.asBoolean(false));
                        }
                    }
                    TriState triState2 = triStateArr2[i];
                    if (triState2 != null) {
                        if (triState2 == TriState.UNSET) {
                            r8.A03.A02(i);
                        } else {
                            r8.A03.A05(i, triState2.asBoolean(false));
                        }
                    }
                    if (z2 && A012 != r8.A03.A01(i)) {
                        arrayList.add(Integer.valueOf(i));
                    }
                }
            }
            AnonymousClass0WG r02 = r8.A08;
            if (r02 != null) {
                C005505z.A05("%s.save", r02.A00, 22522486);
            }
            try {
                r8.A04.A05(r8.A03);
            } finally {
                if (r8.A08 != null) {
                    C005505z.A00(-571548417);
                }
            }
        }
        synchronized (r8) {
            r0 = r8.A00;
        }
        if (r0 != null) {
            r0.A00.A04(arrayList, r8, r0.A01);
        }
    }

    public synchronized void A04() {
        AnonymousClass0X4 r2 = this.A03;
        synchronized (r2) {
            int length = r2.A02.length;
            for (int i = 0; i < length; i++) {
                r2.A03(i);
            }
        }
        AnonymousClass0X4 r22 = this.A03;
        int length2 = r22.A02.length;
        for (int i2 = 0; i2 < length2; i2++) {
            r22.A02(i2);
        }
        this.A04.A05(this.A03);
    }

    public synchronized TriState Ab9(int i) {
        A02(this);
        return this.A03.A01(i);
    }

    public synchronized boolean AbO(int i, boolean z) {
        A02(this);
        return this.A03.A01(i).asBoolean(z);
    }

    public void C0L(boolean z) {
    }

    public static void A02(AnonymousClass0WB r6) {
        AnonymousClass38T r0;
        if (!r6.A02) {
            r6.A02 = true;
            AnonymousClass0WG r02 = r6.A08;
            if (r02 != null) {
                SystemClock.uptimeMillis();
                C005505z.A05("%s.load", r02.A00, 464810741);
            }
            try {
                if (!r6.A04.A06(r6.A03) && (r0 = r6.A07) != null) {
                    Map preExistingGatekeeperStates = r0.getPreExistingGatekeeperStates();
                    ArrayList Anu = r6.A05.Anu();
                    int AwA = r6.A05.AwA();
                    for (int i = 0; i < AwA; i++) {
                        Boolean bool = (Boolean) preExistingGatekeeperStates.get((String) Anu.get(i));
                        if (bool != null) {
                            r6.A03.A04(i, bool.booleanValue());
                        }
                    }
                    r6.A04.A05(r6.A03);
                }
            } finally {
                if (r6.A08 != null) {
                    C005505z.A00(-1085128165);
                    SystemClock.uptimeMillis();
                }
            }
        }
    }

    public boolean AUC() {
        AnonymousClass0X4 r3 = this.A03;
        synchronized (r3) {
            int length = r3.A02.length;
            for (int i = 0; i < length; i++) {
                if (r3.A06(i)) {
                    return true;
                }
            }
            return false;
        }
    }

    public C72723er AY7() {
        return new C72713eq(this);
    }

    public AnonymousClass0WB(AnonymousClass0WC r3, AnonymousClass0WH r4, AnonymousClass38T r5, AnonymousClass0WG r6, AnonymousClass0X3 r7) {
        this.A05 = r3;
        this.A04 = r4;
        this.A07 = r5;
        this.A08 = r6;
        this.A06 = r7;
        this.A03 = new AnonymousClass0X4(r3.AwA());
    }

    public static int A00(AnonymousClass0WB r1, String str) {
        int intValue;
        Integer num = (Integer) A01(r1).A00.get(str);
        if (num == null) {
            intValue = -1;
        } else {
            intValue = num.intValue();
        }
        if (intValue != -1) {
            return intValue;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0J("Unknown gatekeeper: ", str));
    }
}
