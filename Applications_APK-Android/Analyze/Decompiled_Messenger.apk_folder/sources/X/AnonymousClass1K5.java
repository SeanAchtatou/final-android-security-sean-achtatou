package X;

import android.graphics.Rect;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.TransformationMethod;
import android.view.View;
import java.util.Locale;

/* renamed from: X.1K5  reason: invalid class name */
public final class AnonymousClass1K5 implements TransformationMethod {
    private boolean A00;
    private final Locale A01;

    public void onFocusChanged(View view, CharSequence charSequence, boolean z, int i, Rect rect) {
    }

    public static final AnonymousClass1K5 A00(AnonymousClass1XY r2) {
        return new AnonymousClass1K5(AnonymousClass0ZS.A00(r2));
    }

    public static final AnonymousClass1K5 A01(AnonymousClass1XY r2) {
        return new AnonymousClass1K5(AnonymousClass0ZS.A00(r2));
    }

    public CharSequence getTransformation(CharSequence charSequence, View view) {
        if (!this.A00) {
            return charSequence;
        }
        if (charSequence == null) {
            return null;
        }
        String upperCase = charSequence.toString().toUpperCase(this.A01);
        if (!(charSequence instanceof Spanned)) {
            return upperCase;
        }
        Spanned spanned = (Spanned) charSequence;
        Object[] spans = spanned.getSpans(0, spanned.length(), Object.class);
        SpannableString spannableString = new SpannableString(upperCase);
        for (Object obj : spans) {
            spannableString.setSpan(obj, spanned.getSpanStart(obj), spanned.getSpanEnd(obj), spanned.getSpanFlags(obj));
        }
        return spannableString;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0054, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005f, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006a, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0075, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0080, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008b, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0096, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a2, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ae, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b9, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c4, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00cf, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00da, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00e5, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f0, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00fb, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0106, code lost:
        if (r0 == false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private AnonymousClass1K5(X.AnonymousClass0ZS r3) {
        /*
            r2 = this;
            r2.<init>()
            java.util.Locale r0 = r3.A06()
            r2.A01 = r0
            java.lang.String r1 = r3.A03()
            if (r1 == 0) goto L_0x001a
            int r0 = r1.hashCode()
            switch(r0) {
                case 92714369: goto L_0x00ff;
                case 94948006: goto L_0x00f4;
                case 95335305: goto L_0x00e9;
                case 95454463: goto L_0x00de;
                case 96586627: goto L_0x00d3;
                case 96646193: goto L_0x00c8;
                case 96646644: goto L_0x00bd;
                case 96795103: goto L_0x00b2;
                case 96795302: goto L_0x00a6;
                case 97688863: goto L_0x009a;
                case 100042431: goto L_0x008e;
                case 100519103: goto L_0x0083;
                case 104600620: goto L_0x0078;
                case 106983531: goto L_0x006d;
                case 106983967: goto L_0x0062;
                case 108860863: goto L_0x0057;
                case 109486495: goto L_0x004c;
                case 109814190: goto L_0x0041;
                case 110439711: goto L_0x0036;
                case 110618591: goto L_0x002b;
                case 112197572: goto L_0x0020;
                default: goto L_0x0016;
            }
        L_0x0016:
            r1 = -1
        L_0x0017:
            switch(r1) {
                case 0: goto L_0x001e;
                case 1: goto L_0x001e;
                case 2: goto L_0x001e;
                case 3: goto L_0x001e;
                case 4: goto L_0x001e;
                case 5: goto L_0x001e;
                case 6: goto L_0x001e;
                case 7: goto L_0x001e;
                case 8: goto L_0x001e;
                case 9: goto L_0x001e;
                case 10: goto L_0x001e;
                case 11: goto L_0x001e;
                case 12: goto L_0x001e;
                case 13: goto L_0x001e;
                case 14: goto L_0x001e;
                case 15: goto L_0x001e;
                case 16: goto L_0x001e;
                case 17: goto L_0x001e;
                case 18: goto L_0x001e;
                case 19: goto L_0x001e;
                case 20: goto L_0x001e;
                default: goto L_0x001a;
            }
        L_0x001a:
            r0 = 0
        L_0x001b:
            r2.A00 = r0
            return
        L_0x001e:
            r0 = 1
            goto L_0x001b
        L_0x0020:
            java.lang.String r0 = "vi_VN"
            boolean r0 = r1.equals(r0)
            r1 = 20
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x002b:
            java.lang.String r0 = "tr_TR"
            boolean r0 = r1.equals(r0)
            r1 = 19
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x0036:
            java.lang.String r0 = "tl_PH"
            boolean r0 = r1.equals(r0)
            r1 = 18
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x0041:
            java.lang.String r0 = "sv_SE"
            boolean r0 = r1.equals(r0)
            r1 = 17
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x004c:
            java.lang.String r0 = "sk_SK"
            boolean r0 = r1.equals(r0)
            r1 = 16
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x0057:
            java.lang.String r0 = "ru_RU"
            boolean r0 = r1.equals(r0)
            r1 = 15
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x0062:
            java.lang.String r0 = "pt_PT"
            boolean r0 = r1.equals(r0)
            r1 = 14
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x006d:
            java.lang.String r0 = "pt_BR"
            boolean r0 = r1.equals(r0)
            r1 = 13
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x0078:
            java.lang.String r0 = "nb_NO"
            boolean r0 = r1.equals(r0)
            r1 = 12
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x0083:
            java.lang.String r0 = "it_IT"
            boolean r0 = r1.equals(r0)
            r1 = 11
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x008e:
            java.lang.String r0 = "id_ID"
            boolean r0 = r1.equals(r0)
            r1 = 10
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x009a:
            java.lang.String r0 = "fr_FR"
            boolean r0 = r1.equals(r0)
            r1 = 9
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00a6:
            java.lang.String r0 = "es_LA"
            boolean r0 = r1.equals(r0)
            r1 = 8
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00b2:
            java.lang.String r0 = "es_ES"
            boolean r0 = r1.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00bd:
            java.lang.String r0 = "en_US"
            boolean r0 = r1.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00c8:
            java.lang.String r0 = "en_GB"
            boolean r0 = r1.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00d3:
            java.lang.String r0 = "el_GR"
            boolean r0 = r1.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00de:
            java.lang.String r0 = "de_DE"
            boolean r0 = r1.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00e9:
            java.lang.String r0 = "da_DK"
            boolean r0 = r1.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00f4:
            java.lang.String r0 = "cs_CZ"
            boolean r0 = r1.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        L_0x00ff:
            java.lang.String r0 = "af_ZA"
            boolean r0 = r1.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0017
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1K5.<init>(X.0ZS):void");
    }
}
