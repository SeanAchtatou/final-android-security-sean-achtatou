package X;

import java.util.Collection;
import java.util.Set;

/* renamed from: X.0IE  reason: invalid class name */
public final class AnonymousClass0IE {
    public static boolean A00(AnonymousClass0V0 r6, AnonymousClass0V0 r7) {
        if (r6.size() == r7.size()) {
            Set keySet = r6.keySet();
            if (keySet.equals(r7.keySet())) {
                for (Object next : keySet) {
                    Collection AbK = r6.AbK(next);
                    Collection AbK2 = r7.AbK(next);
                    if (AbK.size() == AbK2.size()) {
                        if (!AbK.containsAll(AbK2)) {
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
}
