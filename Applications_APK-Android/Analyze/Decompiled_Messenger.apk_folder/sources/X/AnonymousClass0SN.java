package X;

import android.view.animation.Interpolator;

/* renamed from: X.0SN  reason: invalid class name */
public final class AnonymousClass0SN extends C03550Om {
    private static final Interpolator A01 = new AnonymousClass0DY();
    private final C03550Om A00;

    public AnonymousClass0SN(C03550Om r4) {
        super(A01, r4.A00);
        this.A00 = r4;
    }

    public void A00(float f) {
        C03550Om r1 = this.A00;
        r1.A00(r1.A01.getInterpolation(f));
    }
}
