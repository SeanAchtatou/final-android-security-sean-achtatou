package X;

import java.util.Map;

/* renamed from: X.08O  reason: invalid class name */
public final class AnonymousClass08O implements C007306v {
    public final /* synthetic */ Map A00;
    public final /* synthetic */ AnonymousClass09U[] A01;
    public final /* synthetic */ AnonymousClass1ZT[] A02;

    public boolean CDm() {
        return true;
    }

    public boolean CDp() {
        return true;
    }

    public AnonymousClass08O(AnonymousClass09U[] r1, AnonymousClass1ZT[] r2, Map map) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = map;
    }

    public AnonymousClass1ZT[] AjO() {
        return this.A02;
    }

    public Map Ajo() {
        return this.A00;
    }

    public AnonymousClass09U[] AqR() {
        return this.A01;
    }
}
