package X;

import android.app.AlarmManager;
import android.app.PendingIntent;

/* renamed from: X.0E8  reason: invalid class name */
public final class AnonymousClass0E8 {
    public static void A00(AnonymousClass09P r2, AlarmManager alarmManager, int i, long j, PendingIntent pendingIntent) {
        try {
            alarmManager.setExact(i, j, pendingIntent);
        } catch (SecurityException e) {
            if (r2 != null) {
                r2.CGa("AlarmManagerCompat", e);
            }
        }
    }
}
