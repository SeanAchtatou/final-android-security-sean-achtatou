package X;

import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1mm  reason: invalid class name and case insensitive filesystem */
public final class C33061mm extends AnonymousClass11I {
    @Comparable(type = 3)
    public boolean storiesTrayAccessible;

    public void applyStateUpdate(C61322yh r3) {
        if (r3.A00 == 0) {
            C23871Rg r1 = new C23871Rg();
            r1.A00(Boolean.valueOf(this.storiesTrayAccessible));
            r1.A00(Boolean.valueOf(!((Boolean) r1.A00).booleanValue()));
            this.storiesTrayAccessible = ((Boolean) r1.A00).booleanValue();
        }
    }
}
