package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0bF  reason: invalid class name and case insensitive filesystem */
public final class C06280bF {
    private static volatile C06280bF A00;

    public static final C06280bF A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C06280bF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C06280bF();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
