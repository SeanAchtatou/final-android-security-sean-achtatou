package X;

import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.card.payment.BuildConfig;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
/* renamed from: X.1Ui  reason: invalid class name */
public final class AnonymousClass1Ui extends AnonymousClass0YI implements OmnistoreComponent {
    public static final Class A07 = AnonymousClass1Ui.class;
    private static volatile AnonymousClass1Ui A08;
    public AnonymousClass0UN A00;
    public final C31511jo A01;
    public final C24341Tf A02;
    private final AnonymousClass1YI A03;
    private final C05000Xg A04;
    private final AnonymousClass1Uj A05;
    private final C04310Tq A06;

    public void Boz(int i) {
    }

    public String getCollectionLabel() {
        return "fql_user_nux_status";
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public static final AnonymousClass1Ui A00(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass1Ui.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new AnonymousClass1Ui(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        return new IndexedFields();
    }

    public void BVs(List list) {
        AnonymousClass1Uj r6 = this.A05;
        synchronized (r6.A05) {
            if (r6.A02.now() - r6.A00 >= TimeUnit.SECONDS.toMillis(5)) {
                r6.A00 = r6.A02.now();
                r6.A06.schedule(new AnonymousClass8BX(r6), 5, TimeUnit.SECONDS);
            }
        }
    }

    public void onCollectionAvailable(Collection collection) {
        AnonymousClass1Uj r1 = this.A05;
        synchronized (r1) {
            r1.A01 = collection;
        }
    }

    public void onCollectionInvalidated() {
        AnonymousClass1Uj r1 = this.A05;
        synchronized (r1) {
            r1.A01 = null;
        }
    }

    public C32171lK provideSubscriptionInfo(Omnistore omnistore) {
        String str;
        this.A04.A00(this, 234);
        CollectionName.Builder createCollectionNameBuilder = omnistore.createCollectionNameBuilder(getCollectionLabel());
        createCollectionNameBuilder.addSegment((String) this.A06.get());
        createCollectionNameBuilder.addDeviceId();
        CollectionName build = createCollectionNameBuilder.build();
        if (!this.A03.AbO(234, false)) {
            return C32171lK.A03;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
            for (String add : this.A01.A01()) {
                arrayNode.add(add);
            }
            jSONObject2.put("nux_ids", arrayNode);
            jSONObject.put("render_object_list_query_params", jSONObject2);
            str = jSONObject.toString();
        } catch (JSONException e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport(A07.getSimpleName(), e);
            str = BuildConfig.FLAVOR;
        }
        C32151lI r1 = new C32151lI();
        r1.A02 = str;
        r1.A03 = "namespace com.facebook.interstitial.omnistore;\n\ntable UserNuxStatus {\n nux_id:string; \n rank:int; \n nux_data:string;\n fetch_time:long;\n}\n\nroot_type UserNuxStatus;\n";
        return C32171lK.A00(build, new C32161lJ(r1));
    }

    private AnonymousClass1Ui(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A04 = C04990Xf.A00(r3);
        this.A03 = AnonymousClass0WA.A00(r3);
        this.A06 = C10580kT.A04(r3);
        this.A02 = C24341Tf.A00(r3);
        this.A01 = new AnonymousClass1F7(r3);
        this.A05 = AnonymousClass1Uj.A00(r3);
    }
}
