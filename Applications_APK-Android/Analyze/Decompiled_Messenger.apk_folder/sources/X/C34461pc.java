package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.1pc  reason: invalid class name and case insensitive filesystem */
public final class C34461pc implements AnonymousClass0AY {
    public static C36251sl A00(AnonymousClass0CK r45) {
        C36261sm r21;
        byte[] A01;
        AnonymousClass0CJ r0 = r45.A01;
        if (r0 != null) {
            ArrayList arrayList = new ArrayList();
            for (String str : r0.A0K) {
                Integer A00 = AnonymousClass0AI.A00(str);
                if (A00 != null) {
                    arrayList.add(C56602qT.A00(A00.intValue()));
                } else {
                    C010708t.A0P("ThriftPayloadEncoder", "Topic %s does not have an id!", str);
                }
            }
            C36191sf r43 = C36191sf.DEFAULT;
            C36191sf r1 = r0.A01;
            if (r1 != null) {
                r43 = r1;
            }
            Long l = r0.A0C;
            String str2 = r0.A0J;
            Long l2 = r0.A08;
            Long l3 = r0.A0A;
            int i = r0.A00;
            if (i == 0) {
                r21 = C36261sm.RAW;
            } else if (i == 1) {
                r21 = C36261sm.ZLIB;
            } else if (i != 2) {
                r21 = null;
            } else {
                r21 = C36261sm.ZLIB_OPTIONAL;
            }
            Boolean bool = r0.A04;
            Boolean bool2 = r0.A02;
            String str3 = r0.A0H;
            Boolean bool3 = r0.A03;
            Integer num = r0.A07;
            Integer num2 = r0.A06;
            Long l4 = r0.A0B;
            String str4 = r0.A0E;
            String str5 = r0.A0D;
            Long l5 = null;
            if (str5 != null) {
                l5 = Long.valueOf(Long.parseLong(str5));
            }
            String str6 = r0.A0F;
            if (str6 == null) {
                A01 = null;
            } else {
                A01 = C013109s.A01(str6);
            }
            return new C36251sl(l, str2, l2, l3, r21, bool, bool2, str3, bool3, num, num2, l4, null, arrayList, str4, l5, null, A01, r0.A0G, r0.A0I, r0.A05, null, null, null, null, r0.A09, r43, null, null);
        }
        throw new IOException("No user name to fill ClientInfo");
    }

    public static List A01(AnonymousClass0CK r7) {
        List list = r7.A06;
        if (list == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (C04070Rs r4 : r7.A06) {
            arrayList.add(new AxV(r4.A02, Integer.valueOf(r4.A00), r4.A03));
        }
        return arrayList;
    }

    public byte[] AUR(Long l, Boolean bool, Integer num, List list, List list2) {
        LinkedList linkedList;
        LinkedList linkedList2;
        LinkedList linkedList3;
        C36281so r2 = new C36281so(new C36291sp());
        LinkedList linkedList4 = null;
        if (list != null) {
            Iterator it = list.iterator();
            linkedList2 = null;
            linkedList = null;
            while (it.hasNext()) {
                SubscribeTopic subscribeTopic = (SubscribeTopic) it.next();
                Integer A00 = AnonymousClass0AI.A00(subscribeTopic.A01);
                if (A00 != null) {
                    if (linkedList2 == null) {
                        linkedList2 = new LinkedList();
                    }
                    linkedList2.add(A00);
                } else {
                    if (linkedList == null) {
                        linkedList = new LinkedList();
                    }
                    linkedList.add(new C22374Awt(subscribeTopic.A01, Integer.valueOf(subscribeTopic.A00)));
                }
            }
        } else {
            linkedList2 = null;
            linkedList = null;
        }
        if (list2 != null) {
            Iterator it2 = list2.iterator();
            linkedList3 = null;
            while (it2.hasNext()) {
                String str = (String) it2.next();
                Integer A002 = AnonymousClass0AI.A00(str);
                if (A002 != null) {
                    if (linkedList4 == null) {
                        linkedList4 = new LinkedList();
                    }
                    linkedList4.add(A002);
                } else {
                    if (linkedList3 == null) {
                        linkedList3 = new LinkedList();
                    }
                    linkedList3.add(str);
                }
            }
        } else {
            linkedList3 = null;
        }
        C35851rv r3 = new C35851rv(bool, null, num, linkedList2, linkedList, linkedList4, linkedList3, l);
        try {
            byte[] A003 = r2.A00(new C37001uS(null));
            byte[] A004 = r2.A00(r3);
            int length = A003.length;
            int length2 = A004.length;
            byte[] copyOf = Arrays.copyOf(A003, length + length2);
            System.arraycopy(A004, 0, copyOf, length, length2);
            return copyOf;
        } catch (C70863bP unused) {
            return null;
        }
    }

    public List AiC(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            SubscribeTopic subscribeTopic = (SubscribeTopic) it.next();
            if (AnonymousClass0AI.A00(subscribeTopic.A01) != null) {
                arrayList.add(subscribeTopic);
            }
        }
        return arrayList;
    }

    public int BAV(DataOutputStream dataOutputStream, AnonymousClass0P6 r21) {
        AnonymousClass0P6 r0 = r21;
        C01990Ck r1 = r0.A00;
        AnonymousClass0Rv A03 = r0.A03();
        AnonymousClass0CK A02 = r0.A02();
        List A01 = A01(A02);
        try {
            byte[] A00 = AnonymousClass0Q0.A00(new C36281so(new C36291sp()).A00(new C36211sh(A02.A02, A02.A05, A02.A04, A00(A02), A02.A03, null, null, A01, null, A02.A01.A0L, new C36271sn(A02.A00, 0, null))));
            int length = A00.length;
            int i = length + 12;
            DataOutputStream dataOutputStream2 = dataOutputStream;
            dataOutputStream2.writeByte(C04000Rj.A01(r1));
            int A022 = C04000Rj.A02(dataOutputStream2, i) + 1;
            dataOutputStream2.writeByte(0);
            dataOutputStream2.writeByte(6);
            dataOutputStream2.writeByte(77);
            dataOutputStream2.writeByte(81);
            dataOutputStream2.writeByte(84);
            dataOutputStream2.writeByte(84);
            dataOutputStream2.writeByte(111);
            dataOutputStream2.writeByte(84);
            dataOutputStream2.write(A03.A01);
            dataOutputStream2.write(C04000Rj.A00(A03));
            dataOutputStream2.writeShort(A03.A00);
            dataOutputStream2.write(A00, 0, length);
            dataOutputStream2.flush();
            return A022 + i;
        } catch (C70863bP e) {
            throw new IOException(e);
        }
    }
}
