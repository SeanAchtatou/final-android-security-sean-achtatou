package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1rD  reason: invalid class name and case insensitive filesystem */
public final class C35431rD implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.prefetcher.ForegroundThreadMessagesPrefetcher$2";
    public final /* synthetic */ int A00;
    public final /* synthetic */ C33891oJ A01;
    public final /* synthetic */ ImmutableList A02;
    public final /* synthetic */ String A03;

    public C35431rD(C33891oJ r1, String str, ImmutableList immutableList, int i) {
        this.A01 = r1;
        this.A03 = str;
        this.A02 = immutableList;
        this.A00 = i;
    }

    public void run() {
        C005505z.A05("ForegroundThreadMessagesPrefetcher.prefetchThreadsWhenUiIdle %s", this.A03, -459078866);
        try {
            C34011oV r4 = this.A01.A04;
            ImmutableList immutableList = this.A02;
            int i = this.A00;
            String str = this.A03;
            ListenableFuture A032 = C05350Yp.A03(null);
            boolean Aem = ((C25051Yd) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AOJ, r4.A01)).Aem(282686157489991L);
            ImmutableList.Builder builder = ImmutableList.builder();
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                ThreadKey threadKey = (ThreadKey) it.next();
                A032 = AnonymousClass169.A01(A032, new C103974y6(r4, threadKey, i, str, Aem), (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aoc, r4.A01));
                if (A032 != null) {
                    builder.add((Object) A032);
                }
                synchronized (r4.A04) {
                    r4.A02 = true;
                    if (!((String) r4.A00.get(threadKey.A0J().hashCode(), "Unrequested")).equals("Fetched")) {
                        r4.A00.put(threadKey.A0J().hashCode(), "Requested");
                    }
                }
            }
            AnonymousClass169.A01(C05350Yp.A01(builder.build()), new C94694f1(r4), (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aoc, r4.A01));
            C005505z.A00(-1492262896);
        } catch (Throwable th) {
            C005505z.A00(-182610029);
            throw th;
        }
    }
}
