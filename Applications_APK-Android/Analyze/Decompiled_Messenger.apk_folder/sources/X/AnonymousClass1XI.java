package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/* renamed from: X.1XI  reason: invalid class name */
public class AnonymousClass1XI implements Application.ActivityLifecycleCallbacks {
    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (this instanceof AnonymousClass1XQ) {
            ((C14400tF) activity.getFragmentManager().findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag")).A00 = ((AnonymousClass1XQ) this).A00.A03;
        } else if (this instanceof AnonymousClass1XH) {
            C14400tF.A00(activity);
        }
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
        if (this instanceof AnonymousClass1XQ) {
            AnonymousClass1XJ r1 = ((AnonymousClass1XQ) this).A00;
            int i = r1.A00 - 1;
            r1.A00 = i;
            if (i == 0) {
                AnonymousClass00S.A05(r1.A02, r1.A04, 700, 1243086173);
            }
        }
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        boolean z = this instanceof AnonymousClass1XH;
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
        if (!(this instanceof AnonymousClass1XQ)) {
            boolean z = this instanceof AnonymousClass1XH;
            return;
        }
        AnonymousClass1XJ r2 = ((AnonymousClass1XQ) this).A00;
        int i = r2.A01 - 1;
        r2.A01 = i;
        if (i == 0 && r2.A05) {
            r2.A07.A08(C14820uB.ON_STOP);
            r2.A06 = true;
        }
    }
}
