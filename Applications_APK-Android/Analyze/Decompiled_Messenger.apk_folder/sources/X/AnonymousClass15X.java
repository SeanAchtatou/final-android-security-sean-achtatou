package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.15X  reason: invalid class name */
public final class AnonymousClass15X implements Iterator {
    private int A00 = -1;
    private boolean A01;
    public final /* synthetic */ C05180Xy A02;

    public AnonymousClass15X(C05180Xy r2) {
        this.A02 = r2;
    }

    public boolean hasNext() {
        if (this.A00 + 1 >= this.A02.size()) {
            return false;
        }
        return true;
    }

    public void remove() {
        if (!this.A01) {
            this.A02.A00.A08(this.A00);
            this.A01 = true;
            this.A00--;
            return;
        }
        throw new IllegalStateException();
    }

    public Object next() {
        if (hasNext()) {
            this.A01 = false;
            int i = this.A00 + 1;
            this.A00 = i;
            return this.A02.A00.A07(i);
        }
        throw new NoSuchElementException();
    }
}
