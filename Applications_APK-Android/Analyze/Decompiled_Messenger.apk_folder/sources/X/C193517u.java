package X;

import android.content.Intent;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.util.StringLocaleUtil;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.17u  reason: invalid class name and case insensitive filesystem */
public final class C193517u {
    private static volatile C193517u A07;
    public long A00 = -1;
    public long A01 = -1;
    public long A02;
    public AnonymousClass0UN A03;
    public AnonymousClass089 A04 = AnonymousClass089.DISCONNECTED;
    public boolean A05 = false;
    public boolean A06 = false;

    public synchronized long A02() {
        return this.A00;
    }

    public synchronized AnonymousClass089 A03() {
        return this.A04;
    }

    public synchronized boolean A05() {
        boolean z;
        z = false;
        if (this.A04 == AnonymousClass089.CONNECTED) {
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r3.A04 == X.AnonymousClass089.CONNECTING) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A06() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.A05()     // Catch:{ all -> 0x0011 }
            if (r0 != 0) goto L_0x000e
            X.089 r2 = r3.A04     // Catch:{ all -> 0x0011 }
            X.089 r1 = X.AnonymousClass089.CONNECTING     // Catch:{ all -> 0x0011 }
            r0 = 0
            if (r2 != r1) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            monitor-exit(r3)
            return r0
        L_0x0011:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C193517u.A06():boolean");
    }

    public static final C193517u A01(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C193517u.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C193517u(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public void A04(C34581pq r11) {
        AnonymousClass195 r4;
        ((C29291gB) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A3R, this.A03)).A01(new C34431pZ(((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A03)).now(), "Received channel state changed: %s", r11.toString()));
        synchronized (this) {
            long j = r11.A02;
            if (j >= this.A02) {
                this.A02 = j;
                AnonymousClass089 r7 = r11.A03;
                long j2 = r11.A00;
                long j3 = r11.A01;
                boolean z = r11.A04;
                synchronized (this) {
                    if (r7 == AnonymousClass089.CONNECT_SENT) {
                        this.A06 = true;
                        r7 = AnonymousClass089.CONNECTED;
                    } else {
                        this.A06 = false;
                    }
                    AnonymousClass089 r0 = this.A04;
                    this.A04 = r7;
                    this.A00 = j2;
                    this.A01 = j3;
                    this.A05 = z;
                    if (r7 != r0) {
                        synchronized (this) {
                            AnonymousClass089 r3 = this.A04;
                            switch (r3.ordinal()) {
                                case 0:
                                    r4 = AnonymousClass195.CHANNEL_CONNECTING;
                                    break;
                                case 1:
                                default:
                                    throw new IllegalStateException(StringLocaleUtil.A00("Received a state I did not expect %s", r3));
                                case 2:
                                    r4 = AnonymousClass195.CHANNEL_CONNECTED;
                                    break;
                                case 3:
                                    r4 = AnonymousClass195.CHANNEL_DISCONNECTED;
                                    if (this.A05) {
                                        r4.mClockSkewDetected = true;
                                        break;
                                    }
                                    break;
                            }
                            Intent intent = new Intent(TurboLoader.Locator.$const$string(2));
                            intent.putExtra("event", r4.value);
                            ((C34311pM) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AD1, this.A03)).A00.C4x(intent);
                        }
                    }
                }
            }
        }
    }

    private C193517u(AnonymousClass1XY r3) {
        this.A03 = new AnonymousClass0UN(3, r3);
    }

    public static final C193517u A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
