package X;

/* renamed from: X.1mY  reason: invalid class name and case insensitive filesystem */
public final class C32921mY implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.orca.threadlist.ClearAllNotificationsHelper$1";
    public final /* synthetic */ C20971En A00;
    public final /* synthetic */ String A01;

    public C32921mY(C20971En r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    public void run() {
        C20971En r4 = this.A00;
        ((C191016u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AXk, r4.A00)).ASY(this.A01);
        r4.A01 = null;
    }
}
