package X;

import android.net.Uri;

/* renamed from: X.0NR  reason: invalid class name */
public final class AnonymousClass0NR {
    public static boolean A00(Uri uri) {
        if (uri == null) {
            return false;
        }
        if ("fb".equals(uri.getScheme()) || C52652jT.A00.equals(uri.getScheme()) || "fb-messenger-secure".equals(uri.getScheme()) || "fb-messenger-sametask".equals(uri.getScheme()) || "fb-messenger-public".equals(uri.getScheme()) || "fbinternal".equals(uri.getScheme()) || "fb-work".equals(uri.getScheme()) || "dialtone".equals(uri.getScheme()) || "fb-service".equals(uri.getScheme())) {
            return true;
        }
        return false;
    }
}
