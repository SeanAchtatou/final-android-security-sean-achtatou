package X;

import android.os.Build;
import android.view.View;
import java.lang.ref.WeakReference;

/* renamed from: X.1L6  reason: invalid class name */
public final class AnonymousClass1L6 {
    public int A00 = -1;
    public WeakReference A01;

    public void A00() {
        View view = (View) this.A01.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    public void A01(float f) {
        View view = (View) this.A01.get();
        if (view != null) {
            view.animate().alpha(f);
        }
    }

    public void A02(float f) {
        View view = (View) this.A01.get();
        if (view != null) {
            view.animate().translationY(f);
        }
    }

    public void A03(long j) {
        View view = (View) this.A01.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
    }

    public void A04(C28001Dmh dmh) {
        View view = (View) this.A01.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT < 16) {
                view.setTag(2113929216, dmh);
                C27972DmD dmD = new C27972DmD(this);
                if (dmD != null) {
                    view.animate().setListener(new C27990DmW(dmD, view));
                    return;
                }
            } else if (dmh != null) {
                view.animate().setListener(new C27990DmW(dmh, view));
                return;
            }
            view.animate().setListener(null);
        }
    }

    public void A05(AnonymousClass3TT r4) {
        View view = (View) this.A01.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            C80583si r1 = null;
            if (r4 != null) {
                r1 = new C80583si(r4, view);
            }
            view.animate().setUpdateListener(r1);
        }
    }

    public AnonymousClass1L6(View view) {
        this.A01 = new WeakReference(view);
    }
}
