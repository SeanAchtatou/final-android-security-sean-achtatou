package X;

import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1JO  reason: invalid class name */
public final class AnonymousClass1JO extends C17770zR {
    @Comparable(type = 13)
    public C25998CqM A00;
    @Comparable(type = 13)
    public C203679j4 A01;
    @Comparable(type = 14)
    public AnonymousClass5EV A02 = new AnonymousClass5EV();
    @Comparable(type = 13)
    public C121605oW A03;
    @Comparable(type = 13)
    public String A04;

    public AnonymousClass1JO() {
        super("AccountLoginRecPasswordRootComponent");
    }

    public C17770zR A16() {
        AnonymousClass1JO r1 = (AnonymousClass1JO) super.A16();
        r1.A02 = new AnonymousClass5EV();
        return r1;
    }
}
