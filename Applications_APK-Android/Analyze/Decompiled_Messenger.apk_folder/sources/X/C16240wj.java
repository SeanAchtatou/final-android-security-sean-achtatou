package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0wj  reason: invalid class name and case insensitive filesystem */
public interface C16240wj {
    void ANr(View view, int i);

    void AP8(View view, int i, ViewGroup.LayoutParams layoutParams);

    void AXE(int i);

    View Ah1(int i);

    int Ah4();

    C33781o8 Ah9(View view);

    int BCT(View view);

    void BXh(View view);

    void Bca(View view);

    void C1V();

    void C27(int i);
}
