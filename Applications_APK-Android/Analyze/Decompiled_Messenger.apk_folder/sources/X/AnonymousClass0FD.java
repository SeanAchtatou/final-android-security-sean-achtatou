package X;

import android.os.Build;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: X.0FD  reason: invalid class name */
public final class AnonymousClass0FD {
    public static final Object A00 = new Object();
    public static final HashMap A01 = new HashMap();
    public static final Object[] A02 = new Object[2];

    private static Object A00(Class cls, Object obj, String str, int i, boolean z) {
        Field field;
        Object[] objArr;
        Field field2;
        Field field3;
        Field field4;
        Field field5;
        try {
            synchronized (AnonymousClass0FD.class) {
                if (z) {
                    objArr = (Object[]) A01.get(cls);
                    if (objArr == null) {
                        objArr = new Object[6];
                        A01.put(cls, objArr);
                    }
                    Object obj2 = objArr[i];
                    field3 = null;
                    if (obj2 != null) {
                        if (obj2 == A00) {
                            obj2 = null;
                        }
                        field = (Field) obj2;
                    } else {
                        try {
                            field2 = cls.getDeclaredField(str);
                            field2.setAccessible(true);
                            field = field2;
                        } catch (NoSuchFieldException unused) {
                            field2 = null;
                            field = null;
                        }
                        if (field2 == null) {
                            objArr[i] = A00;
                        }
                        objArr[i] = field2;
                    }
                } else {
                    objArr = A02;
                    Object obj3 = objArr[i];
                    field3 = null;
                    if (obj3 != null) {
                        if (obj3 == A00) {
                            obj3 = null;
                        }
                        field = (Field) obj3;
                    } else {
                        try {
                            field4 = cls.getDeclaredField(str);
                            field4.setAccessible(true);
                            field5 = field4;
                        } catch (NoSuchFieldException unused2) {
                            field4 = null;
                            field5 = null;
                        }
                        if (field2 == null) {
                            objArr[i] = A00;
                        }
                        objArr[i] = field2;
                    }
                }
                field = field3;
            }
            if (field != null) {
                return field.get(obj);
            }
            return null;
        } catch (IllegalAccessException unused3) {
            return null;
        }
    }

    public static String A01(Object obj) {
        Object A002;
        Object obj2 = null;
        int i = 100;
        while (true) {
            Object obj3 = obj2;
            obj2 = obj;
            int i2 = i - 1;
            if (i <= 0 || obj == obj3 || (obj instanceof C012409l)) {
                break;
            }
            if (obj instanceof AnonymousClass09S) {
                obj = ((AnonymousClass09S) obj2).getInnerRunnable();
            } else if (obj instanceof FutureTask) {
                int i3 = Build.VERSION.SDK_INT;
                if (i3 <= 26) {
                    Object obj4 = null;
                    if (i3 <= 16 && (A002 = A00(FutureTask.class, obj, "sync", 0, false)) != null) {
                        obj4 = A00(A002.getClass(), A002, "callable", 0, true);
                    }
                    if (obj4 == null) {
                        obj4 = A00(FutureTask.class, obj, "callable", 1, false);
                    }
                    if (obj4 != null) {
                        if (!(obj4 instanceof AnonymousClass09S) && (obj4 instanceof Callable)) {
                            Object A003 = A00(obj4.getClass(), obj4, "task", 5, true);
                            obj = A003;
                            if (A003 != null) {
                            }
                        }
                        obj = obj4;
                    }
                }
            } else {
                try {
                    Class<?> cls = obj.getClass();
                    if (cls.getName().startsWith("com.google.common.util.concurrent.Futures$")) {
                        obj = A00(cls, obj, "function", 1, true);
                        if (obj == null && (obj = A00(cls, obj2, "val$function", 2, true)) == null) {
                            Object A004 = A00(cls, obj2, "val$callback", 3, true);
                            obj = A004;
                            if (A004 == null) {
                                obj = null;
                            }
                        }
                        if (obj == null) {
                            break;
                        }
                    } else {
                        break;
                    }
                } catch (IncompatibleClassChangeError unused) {
                }
            }
            i = i2;
        }
        if (obj2 instanceof C012409l) {
            return ((C012409l) obj2).getRunnableName();
        }
        return AnonymousClass07V.A00(obj2.getClass());
    }
}
