package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.presence.PresenceItem;
import com.facebook.presence.PresenceList;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0yc  reason: invalid class name and case insensitive filesystem */
public final class C17260yc implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C17260yc(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    /* JADX INFO: finally extract failed */
    public void Bl1(Context context, Intent intent, AnonymousClass06Y r16) {
        int A002 = AnonymousClass09Y.A00(-1343625150);
        AnonymousClass0r6 r7 = this.A00;
        try {
            C005505z.A03("PresenceManager:onPresenceReceived", 2135883759);
            r7.A0I.A04 = ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r7.A02)).now();
            String stringExtra = intent.getStringExtra("extra_topic_name");
            boolean booleanExtra = intent.getBooleanExtra("extra_full_list", false);
            ImmutableList immutableList = ((PresenceList) intent.getParcelableExtra("extra_presence_map")).A00;
            ((C07380dK) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BQ3, r7.A02)).A03("presence_mqtt_receive");
            r7.A03 = C13430rQ.PRESENCE_MAP_RECEIVED;
            if (booleanExtra) {
                if (stringExtra == null || !stringExtra.equals("/t_p")) {
                    AnonymousClass0r6.A04(r7);
                } else {
                    int i = AnonymousClass1Y3.BL6;
                    AnonymousClass0UN r5 = r7.A02;
                    AnonymousClass1FR r1 = (AnonymousClass1FR) AnonymousClass1XX.A02(12, i, r5);
                    if ((r1.A01 || r1.A00) && r7.A00 != -1) {
                        ((C07380dK) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BQ3, r7.A02)).A04("android_generic_presence_delay", ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r5)).now() - r7.A00, "counters");
                    }
                    AnonymousClass0r6.A09(r7, C13430rQ.TP_FULL_LIST_RECEIVED);
                    AnonymousClass0r6.A03(r7);
                }
                AnonymousClass0t3 r2 = r7.A0I;
                r2.A02 = r2.A04;
                r2.A00 = immutableList.size();
            }
            long currentTimeMillis = System.currentTimeMillis();
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                PresenceItem presenceItem = (PresenceItem) it.next();
                C37291v0 A01 = AnonymousClass0r6.A01(r7, presenceItem.A02);
                A01.A0A = presenceItem.A06;
                A01.A00 = presenceItem.A00;
                long j = presenceItem.A01;
                if (j >= 0) {
                    A01.A03 = j;
                }
                Long l = presenceItem.A05;
                if (l != null) {
                    A01.A05 = l.longValue();
                } else {
                    A01.A05 = 0;
                }
                Long l2 = presenceItem.A03;
                if (l2 != null) {
                    A01.A01 = l2.longValue();
                } else {
                    A01.A01 = 0;
                }
                Long l3 = presenceItem.A04;
                if (l3 != null) {
                    A01.A02 = l3.longValue();
                } else {
                    A01.A02 = 0;
                }
                A01.A04 = currentTimeMillis;
                AnonymousClass0r6.A0A(r7, presenceItem.A02);
            }
            if (!r7.A07) {
                r7.A0I.A01 = AnonymousClass0r6.A02(r7, -1, -1).size();
            }
            AnonymousClass0r6.A0C(r7, booleanExtra);
            AnonymousClass0r6.A05(r7);
            C005505z.A00(-1830039387);
            AnonymousClass09Y.A01(-95316862, A002);
        } catch (Throwable th) {
            C005505z.A00(-699966109);
            throw th;
        }
    }
}
