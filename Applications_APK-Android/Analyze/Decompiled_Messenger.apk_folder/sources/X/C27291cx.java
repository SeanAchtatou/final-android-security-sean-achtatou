package X;

import android.content.Context;

/* renamed from: X.1cx  reason: invalid class name and case insensitive filesystem */
public final class C27291cx {
    public AnonymousClass0UN A00;
    private Object A01;
    private boolean A02 = false;
    public final C27321d0 A03 = C27321d0.A00;
    private final Context A04;
    private final C72463eK A05 = null;

    public static synchronized void A00(C27291cx r3) {
        synchronized (r3) {
            if (!r3.A02) {
                Context context = r3.A04;
                if (context != null) {
                    r3.A00 = new AnonymousClass0UN(5, AnonymousClass1XX.get(context));
                    r3.A02 = true;
                } else {
                    throw new NullPointerException("If you use @PluginInjectProp, you must put a non-null context in MessengerMsysBootstrapInterface.create() as the second parameter");
                }
            }
        }
    }

    public static boolean A01(C27291cx r6) {
        Boolean bool;
        boolean z;
        if (r6.A01 == null) {
            C27271cv.A02.getAndIncrement();
            r6.A03.A06("com.facebook.messenger.msys.plugins.implementations.messengermsys.msysbootstrap.MessengerMsysBootstrapImpl", "com.facebook.messenger.msys.plugins.interfaces.msysbootstrap.MessengerMsysBootstrapInterfaceSpec");
            try {
                C72463eK r1 = r6.A05;
                if (r1 == null) {
                    bool = null;
                } else {
                    bool = r1.isNeeded("com.facebook.messenger.msys.plugins.messengermsys.MessengerMsysKillSwitch");
                }
                if (bool != null) {
                    z = bool.booleanValue();
                } else {
                    Boolean bool2 = C27351d3.A00;
                    if (bool2 != null) {
                        z = bool2.booleanValue();
                    } else {
                        z = C27351d3.A00(r6.A03);
                    }
                }
                if (z) {
                    C27361d4 r4 = (C27361d4) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AYQ, r6.A00);
                    boolean z2 = false;
                    if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r4.A00)).Aem(282853661214636L) && ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r4.A00)).Aem(282853661280173L)) {
                        z2 = true;
                    }
                    if (z2) {
                        r6.A01 = C27271cv.A00;
                        r6.A03.A02();
                    }
                }
                r6.A01 = C27271cv.A01;
                r6.A03.A02();
            } catch (Exception e) {
                r6.A01 = C27271cv.A01;
                throw e;
            } catch (Throwable th) {
                r6.A03.A02();
                throw th;
            }
        }
        if (r6.A01 == C27271cv.A01) {
            return false;
        }
        return true;
    }

    public C27291cx(Context context) {
        this.A04 = context;
    }
}
