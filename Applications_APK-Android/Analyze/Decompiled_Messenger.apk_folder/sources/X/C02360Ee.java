package X;

import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;
import com.facebook.systrace.TraceDirect;

/* renamed from: X.0Ee  reason: invalid class name and case insensitive filesystem */
public final class C02360Ee implements C02350Ed {
    public void AaI(long j, String str, C02980Hk r18) {
        if (AnonymousClass08Z.A05(j)) {
            C02980Hk r0 = r18;
            String[] strArr = r0.A01;
            int i = r0.A00;
            int i2 = AnonymousClass00n.A08;
            if (i == 0) {
                Logger.writeStandardEntry(i2, 6, 23, 0, 0, -1606012197, 0, 0);
            } else if (TraceEvents.isEnabled(i2)) {
                int i3 = AnonymousClass00n.A08;
                int writeStandardEntry = Logger.writeStandardEntry(i3, 7, 23, 0, 0, -1606012197, 0, 0);
                Logger.writeBytesEntry(i3, 1, 83, writeStandardEntry, str);
                for (int i4 = 1; i4 < i; i4 += 2) {
                    String str2 = strArr[i4 - 1];
                    String str3 = strArr[i4];
                    if (!(str2 == null || str3 == null)) {
                        Logger.writeBytesEntry(i3, 1, 57, Logger.writeBytesEntry(i3, 1, 56, writeStandardEntry, str2), str3);
                    }
                }
            }
            if (!TraceEvents.isEnabled(AnonymousClass00n.A08) && AnonymousClass08Z.A05(j)) {
                if (TraceDirect.checkNative()) {
                    TraceDirect.nativeEndSectionWithArgs(strArr, i);
                    return;
                }
                AnonymousClass09R r4 = new AnonymousClass09R('E');
                StringBuilder sb = r4.A00;
                sb.append('|');
                sb.append('|');
                r4.A04(strArr, i);
                AnonymousClass0HL.A00(r4.toString());
            }
        }
    }
}
