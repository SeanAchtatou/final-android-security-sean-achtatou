package X;

import io.card.payment.BuildConfig;

/* renamed from: X.00f  reason: invalid class name and case insensitive filesystem */
public final class C000100f {
    public static final C000100f A01 = new C000100f(BuildConfig.FLAVOR);
    public final String A00;

    public boolean equals(Object obj) {
        return this == obj || (obj != null && getClass() == obj.getClass() && this.A00.equals(((C000100f) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public C000100f(String str) {
        if (str == null || str.contains(":")) {
            throw new IllegalArgumentException("Invalid name");
        }
        this.A00 = str;
    }

    public String toString() {
        return this.A00;
    }
}
