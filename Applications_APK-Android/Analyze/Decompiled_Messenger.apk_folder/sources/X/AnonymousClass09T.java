package X;

/* renamed from: X.09T  reason: invalid class name */
public final class AnonymousClass09T implements AnonymousClass1Z5 {
    public String getSimpleName() {
        return "BlackBoxOnInitTrigger";
    }

    public synchronized void init() {
        int A03 = C000700l.A03(-833361686);
        AnonymousClass050.A03();
        C000700l.A09(-763122865, A03);
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.A8R, r1);
    }

    public static final AnonymousClass09T A01() {
        return new AnonymousClass09T();
    }

    public AnonymousClass0WW AkC() {
        return AnonymousClass0WW.A01;
    }
}
