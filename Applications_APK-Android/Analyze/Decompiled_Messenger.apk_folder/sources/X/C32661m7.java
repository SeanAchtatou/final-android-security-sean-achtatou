package X;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.collect.ImmutableSet;

/* renamed from: X.1m7  reason: invalid class name and case insensitive filesystem */
public final class C32661m7 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.sms.broadcast.SmsTakeoverStateChecker$2";
    public final /* synthetic */ AnonymousClass13I A00;

    public C32661m7(AnonymousClass13I r1) {
        this.A00 = r1;
    }

    public void run() {
        ((C185814g) this.A00.A07.get()).A01();
        AnonymousClass13J r3 = (AnonymousClass13J) this.A00.A04.get();
        if (!r3.A03) {
            synchronized (r3.A0A) {
                try {
                    if (!r3.A03) {
                        r3.A03 = true;
                        AnonymousClass00S.A04(r3.A08, new AnonymousClass9OS(r3), 1473436062);
                        r3.A02 = new AnonymousClass9OU(r3);
                        r3.A09.C0h(ImmutableSet.A04(C10990lD.A0I), r3.A02);
                    }
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                        break;
                    }
                }
            }
        }
        AnonymousClass13I r32 = this.A00;
        synchronized (AnonymousClass13I.A0E) {
            try {
                boolean Aep = r32.A0A.Aep(C10990lD.A0J, false);
                boolean Aep2 = r32.A0A.Aep(C10990lD.A0K, false);
                boolean A08 = ((C10960l9) r32.A06.get()).A08();
                if (A08) {
                    FbSharedPreferences fbSharedPreferences = r32.A0A;
                    C30281hn edit = fbSharedPreferences.edit();
                    AnonymousClass1Y7 r1 = C10990lD.A0I;
                    if (fbSharedPreferences.BBh(r1)) {
                        edit.C1B(r1);
                    }
                    FbSharedPreferences fbSharedPreferences2 = r32.A0A;
                    AnonymousClass1Y7 r12 = C10990lD.A0G;
                    if (fbSharedPreferences2.BBh(r12)) {
                        edit.C1B(r12);
                    }
                    FbSharedPreferences fbSharedPreferences3 = r32.A0A;
                    AnonymousClass1Y7 r2 = C10990lD.A00;
                    if (!fbSharedPreferences3.Aep(r2, false)) {
                        edit.putBoolean(r2, true);
                    }
                    edit.putBoolean(C10990lD.A08, true);
                    edit.commit();
                }
                boolean A0B = ((C10960l9) r32.A06.get()).A0B();
                if (!A0B) {
                    AnonymousClass7Xn r0 = AnonymousClass13I.A0D;
                    if (r0 != null) {
                        C06790c5 r02 = r0.A00;
                        if (r02 != null) {
                            r02.A01();
                        }
                        AnonymousClass13I.A0D = null;
                    }
                } else if (AnonymousClass13I.A0D == null) {
                    AnonymousClass7Xn r6 = (AnonymousClass7Xn) r32.A0B.get();
                    AnonymousClass13I.A0D = r6;
                    C06600bl BMm = r6.A01.BMm();
                    BMm.A02(C06680bu.A0l, new C158827Xm(r6));
                    C06790c5 A002 = BMm.A00();
                    r6.A00 = A002;
                    A002.A00();
                }
                if (A08 != Aep || A0B != Aep2) {
                    C30281hn edit2 = r32.A0A.edit();
                    if (A08 && !Aep) {
                        edit2.putBoolean(C10990lD.A0D, false);
                        edit2.putBoolean(C10990lD.A0V, false);
                        edit2.putBoolean(C10990lD.A0U, false);
                        edit2.putBoolean(C10990lD.A0M, false);
                        edit2.BzA(C10990lD.A0A, ((AnonymousClass06B) r32.A03.get()).now());
                        if (!r32.A0A.BBh(C10990lD.A0E)) {
                            edit2.BzA(C10990lD.A0E, ((AnonymousClass06B) r32.A03.get()).now());
                        }
                    }
                    edit2.putBoolean(C10990lD.A0J, A08);
                    edit2.putBoolean(C10990lD.A0K, A0B);
                    edit2.commit();
                    C39991zw r22 = (C39991zw) r32.A09.get();
                    if (!A08) {
                        C52622jP r13 = r22.A02;
                        synchronized (r13) {
                            r13.A00.clear();
                        }
                    }
                    ((AnonymousClass7Xb) r22.A03.get()).clearUserData();
                    r22.A01.A03();
                    Intent intent = new Intent(C06680bu.A0t);
                    intent.putExtra("default_sms", A08);
                    intent.putExtra("sms_enabled", A0B);
                    r22.A00.C4x(intent);
                }
                C185914h r23 = (C185914h) r32.A05.get();
                try {
                    ComponentName componentName = new ComponentName(r23.A00, "com.facebook.messenger.intents.SmsShareIntentHandler");
                    PackageManager packageManager = r23.A00.getPackageManager();
                    int i = 2;
                    if (A0B) {
                        i = 1;
                    }
                    packageManager.setComponentEnabledSetting(componentName, i, 1);
                } catch (Exception e) {
                    C010708t.A0T("SmsDefaultAppManager", e, "Failed to enable/disable SmsShareIntentHandler");
                }
            } catch (Throwable th2) {
                while (true) {
                    th = th2;
                }
            }
        }
        ((C197479Ql) this.A00.A08.get()).A01();
        this.A00.A01 = false;
        return;
        throw th;
    }
}
