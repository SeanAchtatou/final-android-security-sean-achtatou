package X;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

/* renamed from: X.00F  reason: invalid class name */
public final class AnonymousClass00F {
    public Message A00;
    public final IBinder A01;
    public final /* synthetic */ AnonymousClass001 A02;

    public AnonymousClass00F(AnonymousClass001 r3, Activity activity) {
        this.A02 = r3;
        try {
            IBinder iBinder = (IBinder) r3.A0O.get(activity);
            this.A01 = iBinder;
            if (iBinder == null) {
                throw new IllegalStateException("activity not bound");
            }
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    public int A00(Message message) {
        IBinder iBinder;
        if (message.what != this.A02.A07) {
            return 0;
        }
        Handler A002 = AnonymousClass009.A00(message);
        AnonymousClass001 r2 = this.A02;
        if (A002 != r2.A0H) {
            return 0;
        }
        if (Build.VERSION.SDK_INT >= 28) {
            iBinder = (IBinder) message.obj;
        } else {
            try {
                iBinder = (IBinder) r2.A0N.get(message.obj);
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
        if (iBinder != this.A01) {
            return 0;
        }
        this.A00 = message;
        return 3;
    }
}
