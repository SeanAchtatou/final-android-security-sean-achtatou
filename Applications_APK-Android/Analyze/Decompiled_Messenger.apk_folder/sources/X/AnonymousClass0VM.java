package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0VM  reason: invalid class name */
public final class AnonymousClass0VM {
    private static volatile AnonymousClass0VM A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass0VM A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0VM.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0VM(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public C04650Vo A02(int i, AnonymousClass0VS r6, String str) {
        int i2 = AnonymousClass1Y3.ALd;
        AnonymousClass0UN r3 = this.A00;
        return ((AnonymousClass0VT) AnonymousClass1XX.A02(0, i2, r3)).A01(i, r6, str, (AnonymousClass0VE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AXf, r3));
    }

    public C04650Vo A03(AnonymousClass0VS r5, String str) {
        int i = AnonymousClass1Y3.ALd;
        AnonymousClass0UN r3 = this.A00;
        return ((AnonymousClass0VT) AnonymousClass1XX.A02(0, i, r3)).A01(2, r5, str, (AnonymousClass0VE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AXf, r3));
    }

    public AnonymousClass1TI A04(AnonymousClass0VS r5, String str) {
        int i = AnonymousClass1Y3.ALd;
        AnonymousClass0UN r3 = this.A00;
        return (AnonymousClass1TI) ((AnonymousClass0VT) AnonymousClass1XX.A02(0, i, r3)).A01(1, r5, str, (AnonymousClass0VE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AXf, r3));
    }

    private AnonymousClass0VM(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public static final AnonymousClass0VM A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
