package X;

import android.content.SharedPreferences;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.0QG  reason: invalid class name */
public final class AnonymousClass0QG implements AnonymousClass0B0 {
    private final SharedPreferences A00;

    public synchronized Map getAll() {
        return this.A00.getAll();
    }

    public AnonymousClass0DD AY8() {
        return new AnonymousClass0QJ(this.A00.edit());
    }

    public boolean contains(String str) {
        return this.A00.contains(str);
    }

    public boolean getBoolean(String str, boolean z) {
        return this.A00.getBoolean(str, z);
    }

    public int getInt(String str, int i) {
        return this.A00.getInt(str, i);
    }

    public long getLong(String str, long j) {
        return this.A00.getLong(str, j);
    }

    public String getString(String str, String str2) {
        return this.A00.getString(str, str2);
    }

    public AnonymousClass0QG(SharedPreferences sharedPreferences) {
        new WeakHashMap();
        this.A00 = sharedPreferences;
    }
}
