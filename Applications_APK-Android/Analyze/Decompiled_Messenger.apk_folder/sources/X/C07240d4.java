package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0d4  reason: invalid class name and case insensitive filesystem */
public final class C07240d4 implements C07250d5 {
    private static volatile C07240d4 A06;
    public long A00 = 0;
    public boolean A01 = false;
    public final AnonymousClass069 A02;
    public final AnonymousClass1YI A03;
    private final C25711aH A04;
    private final AnonymousClass0US A05;

    public synchronized void AQQ(Integer num) {
        boolean z;
        this.A04.AQQ(num);
        synchronized (this) {
            long now = this.A02.now();
            if (now - this.A00 > 600000) {
                this.A01 = this.A03.AbO(182, this.A01);
                this.A00 = now;
            }
            z = this.A01;
        }
        if (z && ((C25501Zw) this.A05.get()).A09()) {
            C005505z.A03("MessengerStartupCompletedLock-waitForWarmStart", -1005881316);
            try {
                ((C25501Zw) this.A05.get()).A06();
                C005505z.A00(1384156701);
            } catch (Throwable th) {
                C005505z.A00(-68487887);
                throw th;
            }
        }
    }

    public static final C07240d4 A00(AnonymousClass1XY r8) {
        if (A06 == null) {
            synchronized (C07240d4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        A06 = new C07240d4(AnonymousClass0VB.A00(AnonymousClass1Y3.AMF, applicationInjector), C25711aH.A00(applicationInjector), AnonymousClass0WA.A00(applicationInjector), AnonymousClass067.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    private C07240d4(AnonymousClass0US r3, C25711aH r4, AnonymousClass1YI r5, AnonymousClass069 r6) {
        this.A05 = r3;
        this.A04 = r4;
        this.A03 = r5;
        this.A02 = r6;
    }
}
