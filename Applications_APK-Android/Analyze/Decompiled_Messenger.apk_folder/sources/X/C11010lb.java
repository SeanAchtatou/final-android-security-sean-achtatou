package X;

import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;

/* renamed from: X.0lb  reason: invalid class name and case insensitive filesystem */
public final class C11010lb {
    public boolean A00 = false;
    public final Bundle A01;
    public final CallerContext A02;
    public final String A03;
    public final String A04;
    public final boolean A05;

    public C11010lb(String str, String str2, Bundle bundle, boolean z, CallerContext callerContext) {
        this.A03 = str;
        this.A04 = str2;
        this.A01 = bundle;
        this.A05 = z;
        this.A02 = callerContext;
    }
}
