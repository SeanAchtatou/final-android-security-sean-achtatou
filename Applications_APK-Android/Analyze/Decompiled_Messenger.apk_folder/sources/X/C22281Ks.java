package X;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.facebook.quickpromotion.model.QuickPromotionDefinition;
import com.facebook.quickpromotion.protocol.QuickPromotionDefinitionsFetchResult;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.1Ks  reason: invalid class name and case insensitive filesystem */
public abstract class C22281Ks extends AnonymousClass1BV implements C32821mO, AnonymousClass1J2, C22081Jv {
    public final C70733bC A00;

    public long A01() {
        return 0;
    }

    public abstract long A02();

    public abstract Intent A04(Context context);

    public abstract String A05();

    public boolean A07() {
        return false;
    }

    private static final C21276A5b A00(QuickPromotionDefinition quickPromotionDefinition) {
        C21279A5m a5m = new C21279A5m(false);
        a5m.A03 = StringFormatUtil.formatStrLocaleSafe("Invalid template for promotion %s: %s", quickPromotionDefinition.promotionId, quickPromotionDefinition.A09());
        return new C21276A5b(a5m);
    }

    public Set A06() {
        return RegularImmutableSet.A05;
    }

    public final Class AmL() {
        return QuickPromotionDefinitionsFetchResult.class;
    }

    public Class Ao8() {
        return AnonymousClass4EE.class;
    }

    public final C70753bE B3h(InterstitialTrigger interstitialTrigger) {
        return this.A00.A05(interstitialTrigger);
    }

    public final ImmutableList B7C() {
        return this.A00.A04;
    }

    public void Bxt(Parcelable parcelable) {
        List<QuickPromotionDefinition> list;
        QuickPromotionDefinitionsFetchResult quickPromotionDefinitionsFetchResult = (QuickPromotionDefinitionsFetchResult) parcelable;
        C70733bC r1 = this.A00;
        if (quickPromotionDefinitionsFetchResult != null) {
            list = quickPromotionDefinitionsFetchResult.mQuickPromotionDefinitions;
        } else {
            list = null;
        }
        C70733bC.A01(r1, list);
    }

    public void Bxu(Object obj) {
        GSTModelShape1S0000000 Aza;
        QuickPromotionDefinition quickPromotionDefinition;
        String obj2;
        AnonymousClass4EE r10 = (AnonymousClass4EE) obj;
        C70733bC r5 = this.A00;
        if (r10 == null || (Aza = r10.Aza()) == null) {
            C70733bC.A01(r5, null);
            return;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        C24971Xv it = Aza.A0M(96356950, GSTModelShape1S0000000.class, 627135294).iterator();
        while (it.hasNext()) {
            GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) it.next();
            if (gSTModelShape1S0000000 != null) {
                try {
                    GSTModelShape1S0000000 A1z = gSTModelShape1S0000000.A1z();
                    if (A1z != null && !A1z.A0M(598509220, GSTModelShape1S0000000.class, -206750890).isEmpty()) {
                        quickPromotionDefinition = new QuickPromotionDefinition(gSTModelShape1S0000000);
                        Preconditions.checkNotNull(quickPromotionDefinition);
                        builder.add((Object) quickPromotionDefinition);
                    }
                } catch (NullPointerException e) {
                    AnonymousClass09P r3 = (AnonymousClass09P) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Amr, r5.A01);
                    StringBuilder sb = new StringBuilder("Error creating QuickPromotionDefinition for QP data ");
                    GSTModelShape1S0000000 A1z2 = gSTModelShape1S0000000.A1z();
                    sb.append(A1z2);
                    if (sb.toString() != null) {
                        obj2 = A1z2.A0P(1962741303);
                    } else {
                        obj2 = gSTModelShape1S0000000.toString();
                    }
                    r3.softReport("QuickPromotionGraphQLInvalid", obj2, e);
                }
            }
            quickPromotionDefinition = null;
            Preconditions.checkNotNull(quickPromotionDefinition);
            builder.add((Object) quickPromotionDefinition);
        }
        C70733bC.A01(r5, builder.build());
    }

    public C22281Ks(C70743bD r2) {
        this.A00 = new C70733bC(r2, this);
    }

    public Intent A03(Context context) {
        return this.A00.A04(A04(context));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0076, code lost:
        if (r1.A00.get(r4) == null) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0088, code lost:
        if (r1.equals(r0) != false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
        if ((r5 instanceof X.C1305569o) != false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C21276A5b CLY(com.facebook.quickpromotion.model.QuickPromotionDefinition r6, com.facebook.interstitial.triggers.InterstitialTrigger r7) {
        /*
            r5 = this;
            com.facebook.quickpromotion.model.QuickPromotionDefinition$TemplateType r1 = r6.A09()
            com.facebook.quickpromotion.model.QuickPromotionDefinition$TemplateType r0 = com.facebook.quickpromotion.model.QuickPromotionDefinition.TemplateType.A0E
            if (r1 != r0) goto L_0x0019
            com.facebook.quickpromotion.model.CustomRenderType r1 = r6.customRenderType
            if (r1 != 0) goto L_0x000e
            com.facebook.quickpromotion.model.CustomRenderType r1 = com.facebook.quickpromotion.model.CustomRenderType.A04
        L_0x000e:
            com.facebook.quickpromotion.model.CustomRenderType r0 = com.facebook.quickpromotion.model.CustomRenderType.A03
            if (r1 != r0) goto L_0x0019
            boolean r0 = r5 instanceof X.C1305569o
            if (r0 == 0) goto L_0x0019
        L_0x0016:
            X.A5b r0 = X.C21276A5b.A05
            return r0
        L_0x0019:
            boolean r0 = r5.A07()
            if (r0 == 0) goto L_0x0053
            com.facebook.quickpromotion.model.QuickPromotionDefinition$TemplateType r4 = r6.A09()
            java.util.Set r3 = r5.A06()
            X.3bC r0 = r5.A00
            int r2 = X.AnonymousClass1Y3.Agd
            X.0UN r1 = r0.A01
            r0 = 6
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.6ug r0 = (X.C148386ug) r0
            java.lang.Class r1 = X.C148386ug.A01(r0, r6)
            r0 = 0
            if (r1 == 0) goto L_0x003c
            r0 = 1
        L_0x003c:
            if (r0 == 0) goto L_0x00bf
            int r0 = r3.size()
            if (r0 == 0) goto L_0x0050
            boolean r0 = r6.A0C()
            if (r0 != 0) goto L_0x0050
            boolean r0 = r3.contains(r4)
            if (r0 == 0) goto L_0x00bf
        L_0x0050:
            X.A5b r0 = X.C21276A5b.A05
            return r0
        L_0x0053:
            java.util.Set r3 = r5.A06()
            X.3bC r0 = r5.A00
            com.facebook.quickpromotion.model.CustomRenderType r4 = r6.customRenderType
            if (r4 != 0) goto L_0x005f
            com.facebook.quickpromotion.model.CustomRenderType r4 = com.facebook.quickpromotion.model.CustomRenderType.A04
        L_0x005f:
            int r2 = X.AnonymousClass1Y3.A0G
            X.0UN r1 = r0.A01
            r0 = 9
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4sf r1 = (X.C101504sf) r1
            com.facebook.quickpromotion.model.CustomRenderType r0 = com.facebook.quickpromotion.model.CustomRenderType.A04
            if (r4 == r0) goto L_0x0078
            java.util.Map r0 = r1.A00
            java.lang.Object r1 = r0.get(r4)
            r0 = 1
            if (r1 != 0) goto L_0x0079
        L_0x0078:
            r0 = 0
        L_0x0079:
            if (r0 != 0) goto L_0x008a
            com.facebook.quickpromotion.model.CustomRenderType r1 = com.facebook.quickpromotion.model.CustomRenderType.A02
            com.facebook.quickpromotion.model.CustomRenderType r0 = r6.customRenderType
            if (r0 != 0) goto L_0x0083
            com.facebook.quickpromotion.model.CustomRenderType r0 = com.facebook.quickpromotion.model.CustomRenderType.A04
        L_0x0083:
            boolean r0 = r1.equals(r0)
            r2 = 1
            if (r0 == 0) goto L_0x008b
        L_0x008a:
            r2 = 0
        L_0x008b:
            com.facebook.quickpromotion.model.QuickPromotionDefinition$TemplateType r0 = r6.A09()
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x00c4
            com.facebook.quickpromotion.model.QuickPromotionDefinition$TemplateType r1 = r6.A09()
            com.facebook.quickpromotion.model.QuickPromotionDefinition$TemplateType r0 = com.facebook.quickpromotion.model.QuickPromotionDefinition.TemplateType.A0E
            if (r1 != r0) goto L_0x0016
            if (r2 == 0) goto L_0x0016
            X.A5m r2 = new X.A5m
            r0 = 0
            r2.<init>(r0)
            java.lang.String r1 = r6.promotionId
            com.facebook.quickpromotion.model.CustomRenderType r0 = r6.customRenderType
            if (r0 != 0) goto L_0x00ad
            com.facebook.quickpromotion.model.CustomRenderType r0 = com.facebook.quickpromotion.model.CustomRenderType.A04
        L_0x00ad:
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Invalid custom render type for promotion %s: %s"
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1)
            r2.A03 = r0
            X.A5b r0 = new X.A5b
            r0.<init>(r2)
            return r0
        L_0x00bf:
            X.A5b r0 = A00(r6)
            return r0
        L_0x00c4:
            X.A5b r0 = A00(r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22281Ks.CLY(com.facebook.quickpromotion.model.QuickPromotionDefinition, com.facebook.interstitial.triggers.InterstitialTrigger):X.A5b");
    }
}
