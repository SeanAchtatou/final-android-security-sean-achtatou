package X;

import android.graphics.Bitmap;
import android.util.SparseArray;
import android.util.SparseIntArray;
import com.facebook.imagepipeline.memory.NativeMemoryChunk;
import com.facebook.imagepipeline.memory.NativeMemoryChunkPool;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

/* renamed from: X.1Nd  reason: invalid class name and case insensitive filesystem */
public abstract class C22871Nd implements C22881Ne {
    public boolean A00;
    public boolean A01;
    public final Set A02;
    public final SparseArray A03;
    public final C14320t5 A04;
    public final C22931Nj A05;
    public final C22931Nj A06;
    public final AnonymousClass1N7 A07;
    public final AnonymousClass1N4 A08;
    private final Class A09;

    private synchronized C22911Nh A00(int i) {
        C22911Nh r1;
        r1 = (C22911Nh) this.A03.get(i);
        if (r1 == null && this.A00) {
            AnonymousClass02w.A0G(2);
            r1 = A06(i);
            this.A03.put(i, r1);
        }
        return r1;
    }

    private synchronized void A01(SparseIntArray sparseIntArray) {
        C05520Zg.A02(sparseIntArray);
        this.A03.clear();
        SparseIntArray sparseIntArray2 = this.A07.A04;
        if (sparseIntArray2 != null) {
            for (int i = 0; i < sparseIntArray2.size(); i++) {
                int keyAt = sparseIntArray2.keyAt(i);
                int valueAt = sparseIntArray2.valueAt(i);
                int i2 = sparseIntArray.get(keyAt, 0);
                SparseArray sparseArray = this.A03;
                A04(keyAt);
                sparseArray.put(keyAt, new C22911Nh(keyAt, valueAt, i2, this.A07.A00));
            }
            this.A00 = false;
        } else {
            this.A00 = true;
        }
    }

    private synchronized boolean A02() {
        boolean z;
        z = false;
        if (this.A06.A01 + this.A05.A01 > this.A07.A03) {
            z = true;
        }
        if (z) {
            this.A08.Bp0();
        }
        return z;
    }

    public int A03(int i) {
        int i2;
        C37521vo r7;
        SparseArray sparseArray;
        if (this instanceof C22921Ni) {
            C22921Ni r0 = (C22921Ni) this;
            if (i > 0) {
                int[] iArr = r0.A00;
                int length = iArr.length;
                int i3 = 0;
                while (i3 < length) {
                    i2 = iArr[i3];
                    if (i2 < i) {
                        i3++;
                    }
                }
                return i;
            }
            throw new AnonymousClass4XD(Integer.valueOf(i));
        } else if (!(this instanceof C22861Nc)) {
            C37381va r72 = (C37381va) this;
            if (!(r72 instanceof C37521vo) || (sparseArray = (r7 = (C37521vo) r72).A03) == null) {
                return i;
            }
            synchronized (sparseArray) {
                int size = r7.A03.size();
                for (int i4 = 0; i4 < size; i4++) {
                    C22911Nh r1 = (C22911Nh) r7.A03.valueAt(i4);
                    int i5 = r1.A01;
                    if (i5 >= i) {
                        boolean z = false;
                        if (r1.A00 + r1.A03.size() > r1.A02) {
                            z = true;
                        }
                        if (!z) {
                            return i5;
                        }
                    }
                }
                return i;
            }
        } else {
            C22861Nc r02 = (C22861Nc) this;
            if (i > 0) {
                int[] iArr2 = r02.A00;
                int length2 = iArr2.length;
                int i6 = 0;
                while (i6 < length2) {
                    i2 = iArr2[i6];
                    if (i2 < i) {
                        i6++;
                    }
                }
                return i;
            }
            throw new AnonymousClass4XD(Integer.valueOf(i));
        }
        return i2;
    }

    public int A04(int i) {
        if (!(this instanceof C22921Ni)) {
            boolean z = this instanceof C22861Nc;
        }
        return i;
    }

    public int A05(Object obj) {
        if (this instanceof C22921Ni) {
            AnonymousClass1NZ r2 = (AnonymousClass1NZ) obj;
            C05520Zg.A02(r2);
            return r2.getSize();
        } else if (!(this instanceof C22861Nc)) {
            Bitmap bitmap = (Bitmap) obj;
            C05520Zg.A02(bitmap);
            return bitmap.getAllocationByteCount();
        } else {
            byte[] bArr = (byte[]) obj;
            C05520Zg.A02(bArr);
            return bArr.length;
        }
    }

    public Object A07(int i) {
        if (this instanceof C22861Nc) {
            return new byte[i];
        }
        if (this instanceof C37381va) {
            return Bitmap.createBitmap(1, (int) Math.ceil(((double) i) / 2.0d), Bitmap.Config.RGB_565);
        }
        C22921Ni r1 = (C22921Ni) this;
        return !(r1 instanceof NativeMemoryChunkPool) ? r1.A0C(i) : new NativeMemoryChunk(i);
    }

    public synchronized Object A08(C22911Nh r3) {
        Object A002;
        A002 = r3.A00();
        if (A002 != null) {
            r3.A00++;
        }
        return A002;
    }

    public synchronized void A09(int i) {
        int i2 = this.A06.A01;
        int i3 = this.A05.A01;
        int min = Math.min((i2 + i3) - i, i3);
        if (min > 0) {
            AnonymousClass02w.A0G(2);
            AnonymousClass02w.A0G(2);
            for (int i4 = 0; i4 < this.A03.size() && min > 0; i4++) {
                C22911Nh r2 = (C22911Nh) this.A03.valueAt(i4);
                while (min > 0) {
                    Object A002 = r2.A00();
                    if (A002 == null) {
                        break;
                    }
                    A0A(A002);
                    int i5 = r2.A01;
                    min -= i5;
                    this.A05.A00(i5);
                }
            }
            AnonymousClass02w.A0G(2);
            AnonymousClass02w.A0G(2);
        }
    }

    public void A0A(Object obj) {
        if (this instanceof C22921Ni) {
            AnonymousClass1NZ r2 = (AnonymousClass1NZ) obj;
            C05520Zg.A02(r2);
            r2.close();
        } else if (!(this instanceof C22861Nc)) {
            Bitmap bitmap = (Bitmap) obj;
            C05520Zg.A02(bitmap);
            bitmap.recycle();
        } else {
            C05520Zg.A02((byte[]) obj);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r5 = A07(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b3, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00b4, code lost:
        monitor-enter(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r8.A06.A00(r4);
        r3 = A00(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00be, code lost:
        if (r3 != null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c0, code lost:
        r2 = r3.A00;
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00c4, code lost:
        if (r2 > 0) goto L_0x00c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00c6, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00c7, code lost:
        X.C05520Zg.A05(r0);
        r3.A00 = r2 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00ce, code lost:
        X.AnonymousClass95E.A01(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00d1, code lost:
        monitor-enter(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        X.C05520Zg.A05(r8.A02.add(r5));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00dc, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00e1, code lost:
        if (A02() == false) goto L_0x00ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00e3, code lost:
        A09(r8.A07.A03);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00eb, code lost:
        r8.A08.BO0(r4);
        X.AnonymousClass02w.A0G(2);
        X.AnonymousClass02w.A0G(2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
        if (r8.A05.A01 == 0) goto L_0x000f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00f7, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00fb, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00fe, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0116, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0118, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object get(int r9) {
        /*
            r8 = this;
            r2 = r8
            monitor-enter(r2)
            boolean r0 = r8.A02()     // Catch:{ all -> 0x0119 }
            if (r0 == 0) goto L_0x000f
            X.1Nj r0 = r8.A05     // Catch:{ all -> 0x0119 }
            int r1 = r0.A01     // Catch:{ all -> 0x0119 }
            r0 = 0
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            r0 = 1
        L_0x0010:
            X.C05520Zg.A05(r0)     // Catch:{ all -> 0x0119 }
            monitor-exit(r2)
            int r4 = r8.A03(r9)
            monitor-enter(r8)
            X.1Nh r2 = r8.A00(r4)     // Catch:{ all -> 0x0116 }
            r7 = 2
            if (r2 == 0) goto L_0x0055
            java.lang.Object r3 = r8.A08(r2)     // Catch:{ all -> 0x0116 }
            if (r3 == 0) goto L_0x0055
            java.util.Set r0 = r8.A02     // Catch:{ all -> 0x0116 }
            boolean r0 = r0.add(r3)     // Catch:{ all -> 0x0116 }
            X.C05520Zg.A05(r0)     // Catch:{ all -> 0x0116 }
            int r2 = r8.A05(r3)     // Catch:{ all -> 0x0116 }
            r8.A04(r2)     // Catch:{ all -> 0x0116 }
            X.1Nj r1 = r8.A06     // Catch:{ all -> 0x0116 }
            int r0 = r1.A00     // Catch:{ all -> 0x0116 }
            int r0 = r0 + 1
            r1.A00 = r0     // Catch:{ all -> 0x0116 }
            int r0 = r1.A01     // Catch:{ all -> 0x0116 }
            int r0 = r0 + r2
            r1.A01 = r0     // Catch:{ all -> 0x0116 }
            X.1Nj r0 = r8.A05     // Catch:{ all -> 0x0116 }
            r0.A00(r2)     // Catch:{ all -> 0x0116 }
            X.1N4 r0 = r8.A08     // Catch:{ all -> 0x0116 }
            r0.BuD(r2)     // Catch:{ all -> 0x0116 }
            X.AnonymousClass02w.A0G(r7)     // Catch:{ all -> 0x0116 }
            X.AnonymousClass02w.A0G(r7)     // Catch:{ all -> 0x0116 }
            monitor-exit(r8)     // Catch:{ all -> 0x0116 }
            return r3
        L_0x0055:
            r8.A04(r4)     // Catch:{ all -> 0x0116 }
            r3 = r8
            monitor-enter(r3)     // Catch:{ all -> 0x0116 }
            boolean r0 = r8.A01     // Catch:{ all -> 0x0113 }
            if (r0 != 0) goto L_0x0093
            X.1N7 r1 = r8.A07     // Catch:{ all -> 0x0113 }
            int r6 = r1.A02     // Catch:{ all -> 0x0113 }
            X.1Nj r0 = r8.A06     // Catch:{ all -> 0x0113 }
            int r5 = r0.A01     // Catch:{ all -> 0x0113 }
            int r0 = r6 - r5
            if (r4 <= r0) goto L_0x0070
            X.1N4 r0 = r8.A08     // Catch:{ all -> 0x0113 }
            r0.BaY()     // Catch:{ all -> 0x0113 }
            goto L_0x0090
        L_0x0070:
            int r1 = r1.A03     // Catch:{ all -> 0x0113 }
            X.1Nj r0 = r8.A05     // Catch:{ all -> 0x0113 }
            int r0 = r0.A01     // Catch:{ all -> 0x0113 }
            int r5 = r5 + r0
            int r0 = r1 - r5
            if (r4 <= r0) goto L_0x007f
            int r1 = r1 - r4
            r8.A09(r1)     // Catch:{ all -> 0x0113 }
        L_0x007f:
            X.1Nj r0 = r8.A06     // Catch:{ all -> 0x0113 }
            int r1 = r0.A01     // Catch:{ all -> 0x0113 }
            X.1Nj r0 = r8.A05     // Catch:{ all -> 0x0113 }
            int r0 = r0.A01     // Catch:{ all -> 0x0113 }
            int r1 = r1 + r0
            int r6 = r6 - r1
            if (r4 <= r6) goto L_0x0093
            X.1N4 r0 = r8.A08     // Catch:{ all -> 0x0113 }
            r0.BaY()     // Catch:{ all -> 0x0113 }
        L_0x0090:
            monitor-exit(r3)     // Catch:{ all -> 0x0116 }
            r0 = 0
            goto L_0x0095
        L_0x0093:
            monitor-exit(r3)     // Catch:{ all -> 0x0116 }
            r0 = 1
        L_0x0095:
            if (r0 == 0) goto L_0x0101
            X.1Nj r1 = r8.A06     // Catch:{ all -> 0x0116 }
            int r0 = r1.A00     // Catch:{ all -> 0x0116 }
            int r0 = r0 + 1
            r1.A00 = r0     // Catch:{ all -> 0x0116 }
            int r0 = r1.A01     // Catch:{ all -> 0x0116 }
            int r0 = r0 + r4
            r1.A01 = r0     // Catch:{ all -> 0x0116 }
            if (r2 == 0) goto L_0x00ac
            int r0 = r2.A00     // Catch:{ all -> 0x0116 }
            int r0 = r0 + 1
            r2.A00 = r0     // Catch:{ all -> 0x0116 }
        L_0x00ac:
            monitor-exit(r8)     // Catch:{ all -> 0x0116 }
            r5 = 0
            java.lang.Object r5 = r8.A07(r4)     // Catch:{ all -> 0x00b3 }
            goto L_0x00d1
        L_0x00b3:
            r6 = move-exception
            monitor-enter(r8)
            X.1Nj r0 = r8.A06     // Catch:{ all -> 0x00fe }
            r0.A00(r4)     // Catch:{ all -> 0x00fe }
            X.1Nh r3 = r8.A00(r4)     // Catch:{ all -> 0x00fe }
            if (r3 == 0) goto L_0x00cd
            int r2 = r3.A00     // Catch:{ all -> 0x00fe }
            r1 = 1
            r0 = 0
            if (r2 <= 0) goto L_0x00c7
            r0 = 1
        L_0x00c7:
            X.C05520Zg.A05(r0)     // Catch:{ all -> 0x00fe }
            int r2 = r2 - r1
            r3.A00 = r2     // Catch:{ all -> 0x00fe }
        L_0x00cd:
            monitor-exit(r8)     // Catch:{ all -> 0x00fe }
            X.AnonymousClass95E.A01(r6)
        L_0x00d1:
            monitor-enter(r8)
            java.util.Set r0 = r8.A02     // Catch:{ all -> 0x00fb }
            boolean r0 = r0.add(r5)     // Catch:{ all -> 0x00fb }
            X.C05520Zg.A05(r0)     // Catch:{ all -> 0x00fb }
            r1 = r8
            monitor-enter(r1)     // Catch:{ all -> 0x00fb }
            boolean r0 = r8.A02()     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x00ea
            X.1N7 r0 = r8.A07     // Catch:{ all -> 0x00f8 }
            int r0 = r0.A03     // Catch:{ all -> 0x00f8 }
            r8.A09(r0)     // Catch:{ all -> 0x00f8 }
        L_0x00ea:
            monitor-exit(r1)     // Catch:{ all -> 0x00fb }
            X.1N4 r0 = r8.A08     // Catch:{ all -> 0x00fb }
            r0.BO0(r4)     // Catch:{ all -> 0x00fb }
            X.AnonymousClass02w.A0G(r7)     // Catch:{ all -> 0x00fb }
            X.AnonymousClass02w.A0G(r7)     // Catch:{ all -> 0x00fb }
            monitor-exit(r8)     // Catch:{ all -> 0x00fb }
            return r5
        L_0x00f8:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00fb }
            throw r0     // Catch:{ all -> 0x00fb }
        L_0x00fb:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00fb }
            goto L_0x0118
        L_0x00fe:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00fe }
            goto L_0x0118
        L_0x0101:
            X.4Wx r3 = new X.4Wx     // Catch:{ all -> 0x0116 }
            X.1N7 r0 = r8.A07     // Catch:{ all -> 0x0116 }
            int r2 = r0.A02     // Catch:{ all -> 0x0116 }
            X.1Nj r0 = r8.A06     // Catch:{ all -> 0x0116 }
            int r1 = r0.A01     // Catch:{ all -> 0x0116 }
            X.1Nj r0 = r8.A05     // Catch:{ all -> 0x0116 }
            int r0 = r0.A01     // Catch:{ all -> 0x0116 }
            r3.<init>(r2, r1, r0, r4)     // Catch:{ all -> 0x0116 }
            throw r3     // Catch:{ all -> 0x0116 }
        L_0x0113:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0116 }
            throw r0     // Catch:{ all -> 0x0116 }
        L_0x0116:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0116 }
        L_0x0118:
            throw r0
        L_0x0119:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22871Nd.get(int):java.lang.Object");
    }

    public void trim(C133746Nn r13) {
        int i;
        ArrayList arrayList;
        synchronized (this) {
            if (this.A07.A00) {
                arrayList = new ArrayList(this.A03.size());
                int size = this.A03.size();
                for (int i2 = 0; i2 < size; i2++) {
                    C22911Nh r1 = (C22911Nh) this.A03.valueAt(i2);
                    int i3 = r1.A01;
                    int i4 = r1.A02;
                    int i5 = r1.A00;
                    if (r1.A03.size() > 0) {
                        arrayList.add(r1);
                    }
                    SparseArray sparseArray = this.A03;
                    A04(i3);
                    sparseArray.setValueAt(i2, new C22911Nh(i3, i4, i5, this.A07.A00));
                }
            } else {
                arrayList = new ArrayList(this.A03.size());
                SparseIntArray sparseIntArray = new SparseIntArray();
                for (int i6 = 0; i6 < this.A03.size(); i6++) {
                    C22911Nh r2 = (C22911Nh) this.A03.valueAt(i6);
                    if (r2.A03.size() > 0) {
                        arrayList.add(r2);
                    }
                    sparseIntArray.put(this.A03.keyAt(i6), r2.A00);
                }
                A01(sparseIntArray);
            }
            C22931Nj r0 = this.A05;
            r0.A00 = 0;
            r0.A01 = 0;
            AnonymousClass02w.A0G(2);
        }
        for (i = 0; i < arrayList.size(); i++) {
            C22911Nh r12 = (C22911Nh) arrayList.get(i);
            while (true) {
                Object A002 = r12.A00();
                if (A002 == null) {
                    break;
                }
                A0A(A002);
            }
        }
    }

    public C22911Nh A06(int i) {
        if (this instanceof C22851Nb) {
            return new AnonymousClass1S4(i, ((C22851Nb) this).A07.A01, 0);
        }
        A04(i);
        return new C22911Nh(i, Integer.MAX_VALUE, 0, this.A07.A00);
    }

    public boolean A0B(Object obj) {
        if (this instanceof C22921Ni) {
            AnonymousClass1NZ r3 = (AnonymousClass1NZ) obj;
            C05520Zg.A02(r3);
            return !r3.isClosed();
        } else if (!(this instanceof C37381va)) {
            C05520Zg.A02(obj);
            return true;
        } else {
            Bitmap bitmap = (Bitmap) obj;
            C05520Zg.A02(bitmap);
            if (bitmap.isRecycled() || !bitmap.isMutable()) {
                return false;
            }
            return true;
        }
    }

    public void C0t(Object obj) {
        C22911Nh r3;
        int i;
        C05520Zg.A02(obj);
        int A052 = A05(obj);
        A04(A052);
        synchronized (this) {
            synchronized (this) {
                r3 = (C22911Nh) this.A03.get(A052);
            }
            if (!this.A02.remove(obj)) {
                AnonymousClass02w.A06(this.A09, "release (free, value unrecognized) (object, size) = (%x, %s)", Integer.valueOf(System.identityHashCode(obj)), Integer.valueOf(A052));
                A0A(obj);
                this.A08.BZs(A052);
            } else {
                if (r3 != null) {
                    boolean z = false;
                    if (r3.A00 + r3.A03.size() > r3.A02) {
                        z = true;
                    }
                    if (!z && !A02() && A0B(obj)) {
                        C05520Zg.A02(obj);
                        boolean z2 = false;
                        if (r3.A04) {
                            i = r3.A00;
                            if (i > 0) {
                                z2 = true;
                            }
                            C05520Zg.A05(z2);
                            r3.A00 = i - 1;
                            r3.A01(obj);
                        } else {
                            i = r3.A00;
                            if (i <= 0) {
                                Object[] objArr = {obj};
                                AnonymousClass02v r1 = AnonymousClass02w.A00;
                                if (r1.BFj(6)) {
                                    r1.AY3("BUCKET", String.format(null, "Tried to release value %s from an empty bucket!", objArr));
                                }
                            }
                            r3.A00 = i - 1;
                            r3.A01(obj);
                        }
                        C22931Nj r12 = this.A05;
                        r12.A00++;
                        r12.A01 += A052;
                        this.A06.A00(A052);
                        this.A08.BuA(A052);
                        AnonymousClass02w.A0G(2);
                    }
                }
                if (r3 != null) {
                    int i2 = r3.A00;
                    boolean z3 = false;
                    if (i2 > 0) {
                        z3 = true;
                    }
                    C05520Zg.A05(z3);
                    r3.A00 = i2 - 1;
                }
                AnonymousClass02w.A0G(2);
                A0A(obj);
                this.A06.A00(A052);
                this.A08.BZs(A052);
            }
            AnonymousClass02w.A0G(2);
        }
    }

    public C22871Nd(C14320t5 r10, AnonymousClass1N7 r11, AnonymousClass1N4 r12) {
        this.A09 = getClass();
        C05520Zg.A02(r10);
        this.A04 = r10;
        C05520Zg.A02(r11);
        this.A07 = r11;
        C05520Zg.A02(r12);
        this.A08 = r12;
        this.A03 = new SparseArray();
        if (this.A07.A00) {
            synchronized (this) {
                SparseIntArray sparseIntArray = this.A07.A04;
                if (sparseIntArray != null) {
                    this.A03.clear();
                    for (int i = 0; i < sparseIntArray.size(); i++) {
                        int keyAt = sparseIntArray.keyAt(i);
                        int valueAt = sparseIntArray.valueAt(i);
                        SparseArray sparseArray = this.A03;
                        A04(keyAt);
                        sparseArray.put(keyAt, new C22911Nh(keyAt, valueAt, 0, this.A07.A00));
                    }
                    this.A00 = false;
                } else {
                    this.A00 = true;
                }
            }
        } else {
            A01(new SparseIntArray(0));
        }
        this.A02 = Collections.newSetFromMap(new IdentityHashMap());
        this.A05 = new C22931Nj();
        this.A06 = new C22931Nj();
    }

    public C22871Nd(C14320t5 r1, AnonymousClass1N7 r2, AnonymousClass1N4 r3, boolean z) {
        this(r1, r2, r3);
        this.A01 = z;
    }
}
