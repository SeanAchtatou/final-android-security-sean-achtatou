package X;

import android.content.Context;

/* renamed from: X.1Xo  reason: invalid class name and case insensitive filesystem */
public final class C24901Xo extends C24831Xg implements C24851Xi, AnonymousClass062 {
    public AnonymousClass0UN A00;
    public boolean A01 = false;
    public final AnonymousClass0U1 A02;

    public C05920aY B9T() {
        synchronized (this) {
            if (!this.A01) {
                this.A00 = new AnonymousClass0UN(0, AnonymousClass1XX.get(this.A02.A00));
            }
            this.A01 = true;
        }
        return (C05920aY) AnonymousClass1XX.A03(AnonymousClass1Y3.BJM, this.A00);
    }

    public Object AYf() {
        return this.A02.A00();
    }

    public void AZH(Object obj) {
        C24811Xe r1 = (C24811Xe) obj;
        r1.A02();
        r1.A03();
    }

    public Context Aq3() {
        return this.A02.A00;
    }

    public C24901Xo(AnonymousClass1XX r2, AnonymousClass0U1 r3) {
        super(r2);
        this.A02 = r3;
    }
}
