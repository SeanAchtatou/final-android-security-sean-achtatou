package X;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0sy  reason: invalid class name and case insensitive filesystem */
public final class C14290sy extends AnonymousClass01W {
    private static volatile C14290sy A00;

    public static final C14290sy A02(AnonymousClass1XY r5) {
        if (A00 == null) {
            synchronized (C14290sy.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A00 = new C14290sy(C04490Ux.A0J(applicationInjector), C04490Ux.A0G(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public C14290sy(PackageManager packageManager, ApplicationInfo applicationInfo) {
        super(packageManager, applicationInfo);
    }
}
