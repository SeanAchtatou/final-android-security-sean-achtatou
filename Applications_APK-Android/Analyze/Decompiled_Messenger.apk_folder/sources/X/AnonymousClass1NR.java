package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1NR  reason: invalid class name */
public final class AnonymousClass1NR {
    private static C05540Zi A02;
    public AnonymousClass0UN A00;
    public final C04310Tq A01;

    public static final AnonymousClass1NR A00(AnonymousClass1XY r4) {
        AnonymousClass1NR r0;
        synchronized (AnonymousClass1NR.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass1NR((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass1NR) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
        if (r2 == X.AnonymousClass105.A04) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0037, code lost:
        if (r1 == false) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
        if (r0 != false) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        if (r7.A0Z == X.AnonymousClass105.A02) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer A01(com.facebook.messaging.model.threads.ThreadSummary r7) {
        /*
            r6 = this;
            boolean r0 = r7.A0z
            if (r0 != 0) goto L_0x000b
            X.105 r2 = r7.A0Z
            X.105 r1 = X.AnonymousClass105.A02
            r0 = 0
            if (r2 != r1) goto L_0x000c
        L_0x000b:
            r0 = 1
        L_0x000c:
            if (r0 == 0) goto L_0x0011
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            return r0
        L_0x0011:
            int r1 = X.AnonymousClass1Y3.AmY
            X.0UN r0 = r6.A00
            r3 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1hZ r0 = (X.C30151hZ) r0
            boolean r0 = r0.A01(r7)
            if (r0 != 0) goto L_0x003c
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0S
            boolean r0 = r0.A0O()
            if (r0 == 0) goto L_0x0039
            X.105 r2 = r7.A0Z
            X.105 r0 = X.AnonymousClass105.A03
            if (r2 == r0) goto L_0x0035
            X.105 r0 = X.AnonymousClass105.A04
            r1 = 0
            if (r2 != r0) goto L_0x0036
        L_0x0035:
            r1 = 1
        L_0x0036:
            r0 = 1
            if (r1 != 0) goto L_0x003a
        L_0x0039:
            r0 = 0
        L_0x003a:
            if (r0 == 0) goto L_0x003d
        L_0x003c:
            r3 = 1
        L_0x003d:
            if (r3 == 0) goto L_0x0042
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            return r0
        L_0x0042:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0S
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r0)
            if (r0 == 0) goto L_0x004c
            r0 = 0
            return r0
        L_0x004c:
            com.google.common.collect.ImmutableList r0 = r7.A0m
            X.1Xv r5 = r0.iterator()
        L_0x0052:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x007c
            java.lang.Object r2 = r5.next()
            com.facebook.messaging.model.threads.ThreadParticipant r2 = (com.facebook.messaging.model.threads.ThreadParticipant) r2
            com.facebook.user.model.UserKey r1 = r2.A00()
            X.0Tq r0 = r6.A01
            java.lang.Object r0 = r0.get()
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0052
            long r3 = r2.A01
            long r1 = r7.A0B
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0052
            r0 = 1
        L_0x0077:
            if (r0 == 0) goto L_0x007e
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            return r0
        L_0x007c:
            r0 = 0
            goto L_0x0077
        L_0x007e:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1NR.A01(com.facebook.messaging.model.threads.ThreadSummary):java.lang.Integer");
    }

    private AnonymousClass1NR(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = AnonymousClass0XJ.A0I(r3);
    }
}
