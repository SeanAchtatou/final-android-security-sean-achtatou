package X;

/* renamed from: X.0pt  reason: invalid class name and case insensitive filesystem */
public final class C12740pt {
    public static final C28041eA A00 = new C28041eA("db_server_inconsistency_messages_hash");
    public static final C28041eA A01 = new C28041eA("full_refresh_reason");
    public static final C28041eA A02;
    public static final C28041eA A03;
    public static final C28041eA A04 = new C28041eA("missed_delta_exception_seq_id");
    public static final C28041eA A05 = new C28041eA(AnonymousClass80H.$const$string(512));
    public static final C28041eA A06 = new C28041eA("last_sync_full_refresh_ms");
    public static final C28041eA A07 = new C28041eA("needs_full_refresh");
    public static final C28041eA A08;
    public static final C28041eA A09;
    public static final C28041eA A0A = new C28041eA(AnonymousClass80H.$const$string(192));
    public static final C28041eA A0B;

    static {
        C28041eA r1 = new C28041eA("/sync/");
        A08 = r1;
        A0B = (C28041eA) r1.A09("user_info_fetch_latest_thread_timestamp");
        C28041eA r12 = A08;
        A09 = (C28041eA) r12.A09("sync_latest_user_info_fetch_timestamp_ms");
        A02 = (C28041eA) r12.A09("last_fetch_group_threads_time_ms");
        A03 = (C28041eA) r12.A09("last_fetch_room_threads_time_ms");
    }

    public static C28041eA A00(C10950l8 r2) {
        return (C28041eA) ((C28041eA) A08.A09(r2.dbName)).A09("/threads_table_out_of_date");
    }

    public static C28041eA A01(C10950l8 r2) {
        return (C28041eA) ((C28041eA) A08.A09(r2.dbName)).A09("/last_get_threads_client_time_ms");
    }
}
