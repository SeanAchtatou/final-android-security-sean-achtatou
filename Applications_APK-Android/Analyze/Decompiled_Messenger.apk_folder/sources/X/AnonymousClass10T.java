package X;

import android.content.Context;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.10T  reason: invalid class name */
public final class AnonymousClass10T {
    private static volatile AnonymousClass10T A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass10T A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass10T.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass10T(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static void A01(AnonymousClass10T r6, String str, C55082nV r8, C55092nW r9, String str2, Context context, boolean z) {
        C29500EcC ecC = (C29500EcC) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B9N, r6.A00);
        C23278BbV bbV = ecC.A02;
        C185414b r0 = bbV.A00;
        C08900gA r1 = C23278BbV.A01;
        r0.CH1(r1);
        bbV.A00.AMv(r1, str);
        bbV.A00.AMv(r1, "Remix");
        C23272BbP bbP = new C23272BbP(ecC.A03, str, r9, r8, str2, context);
        bbP.A05 = z;
        ecC.A04.offer(bbP);
        if (ecC.A00) {
            ecC.A00 = false;
            C29500EcC.A01(ecC);
        }
    }

    public void A02(String str, Context context) {
        String str2 = str;
        A01(this, str2, (C122995qs) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AK7, this.A00), new C55092nW(new HashMap()), BuildConfig.FLAVOR, context, false);
    }

    private AnonymousClass10T(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
