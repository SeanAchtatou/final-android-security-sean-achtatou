package X;

import java.lang.reflect.Field;

/* renamed from: X.1R3  reason: invalid class name */
public final class AnonymousClass1R3 extends AnonymousClass1R7 implements AnonymousClass1JR {
    private final int A00;
    private final int A01;

    public int Aoh() {
        return this.A00;
    }

    public int B9v() {
        return this.A01;
    }

    public boolean BBw() {
        C33781o8 r0;
        try {
            if (AnonymousClass1Ri.A18 == null) {
                Field declaredField = AnonymousClass1R7.class.getDeclaredField("mViewHolder");
                AnonymousClass1Ri.A18 = declaredField;
                declaredField.setAccessible(true);
            }
            r0 = (C33781o8) AnonymousClass1Ri.A18.get(this);
        } catch (Exception unused) {
            r0 = null;
        }
        if (r0 == null || r0.A04() != -1) {
            return false;
        }
        return true;
    }

    public AnonymousClass1R3(int i, int i2, int i3, int i4) {
        super(i, i2);
        this.A01 = i3;
        this.A00 = i4;
    }
}
