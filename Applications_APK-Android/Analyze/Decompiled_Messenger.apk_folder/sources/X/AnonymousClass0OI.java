package X;

import com.facebook.common.dextricks.DexStore;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.0OI  reason: invalid class name */
public final class AnonymousClass0OI implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.debug.logcat.raw.LogcatProcess$1";
    public final /* synthetic */ AnonymousClass0OJ A00;

    public AnonymousClass0OI(AnonymousClass0OJ r1) {
        this.A00 = r1;
    }

    public void run() {
        Integer num;
        Integer num2;
        try {
            InputStream errorStream = this.A00.A00.getErrorStream();
            byte[] bArr = new byte[DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED];
            while (errorStream.read(bArr) >= 0) {
                synchronized (this.A00) {
                    num = this.A00.A02;
                    num2 = AnonymousClass07B.A01;
                }
                if (num != num2) {
                    return;
                }
            }
        } catch (IOException unused) {
            this.A00.A00.destroy();
            this.A00.A02 = AnonymousClass07B.A0C;
        }
    }
}
