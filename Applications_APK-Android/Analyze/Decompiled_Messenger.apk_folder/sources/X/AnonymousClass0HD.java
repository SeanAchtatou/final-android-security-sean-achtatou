package X;

import android.content.SharedPreferences;
import android.os.Bundle;

/* renamed from: X.0HD  reason: invalid class name */
public final class AnonymousClass0HD extends AnonymousClass0HB {
    public Object A01(SharedPreferences sharedPreferences, String str, Object obj) {
        int intValue;
        Integer num = (Integer) obj;
        if (num == null) {
            intValue = 0;
        } else {
            intValue = num.intValue();
        }
        return Integer.valueOf(sharedPreferences.getInt(str, intValue));
    }

    public Object A02(Bundle bundle, String str, Object obj) {
        int intValue;
        Integer num = (Integer) obj;
        if (num == null) {
            intValue = 0;
        } else {
            intValue = num.intValue();
        }
        return Integer.valueOf(bundle.getInt(str, intValue));
    }

    public void A03(SharedPreferences.Editor editor, String str, Object obj) {
        editor.putInt(str, ((Integer) obj).intValue());
    }

    public void A04(Bundle bundle, String str, Object obj) {
        bundle.putInt(str, ((Integer) obj).intValue());
    }

    public Class A00() {
        return Integer.class;
    }
}
