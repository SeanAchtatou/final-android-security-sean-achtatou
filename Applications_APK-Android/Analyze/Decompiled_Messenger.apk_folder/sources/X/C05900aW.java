package X;

import android.app.Application;
import android.content.Context;
import com.facebook.auth.viewercontext.ViewerContext;
import com.google.common.base.Preconditions;
import java.util.List;
import java.util.NoSuchElementException;

/* renamed from: X.0aW  reason: invalid class name and case insensitive filesystem */
public final class C05900aW implements C05920aY {
    private ViewerContext A00;
    private ViewerContext A01;
    private ThreadLocal A02 = new C05940aa();
    public final AnonymousClass09P A03;
    private final AnonymousClass0XN A04;
    private final AnonymousClass1YI A05;
    private final boolean A06;

    private ViewerContext A00() {
        List list = (List) this.A02.get();
        if (!list.isEmpty()) {
            return (ViewerContext) list.get(list.size() - 1);
        }
        ViewerContext viewerContext = this.A01;
        if (viewerContext != null) {
            return viewerContext;
        }
        return this.A04.A08();
    }

    public ViewerContext Asn() {
        return this.A04.A08();
    }

    public ViewerContext Awc() {
        if (this.A00 == null) {
            this.A00 = A00();
        }
        return this.A00;
    }

    public ViewerContext Awn() {
        return this.A01;
    }

    public void BxG() {
        List list = (List) this.A02.get();
        if (!list.isEmpty()) {
            list.remove(list.size() - 1);
            return;
        }
        throw new NoSuchElementException();
    }

    public C09830il Byt(ViewerContext viewerContext) {
        if (viewerContext == null) {
            return C09830il.A00;
        }
        ((List) this.A02.get()).add(viewerContext);
        return new AnonymousClass2JY(this, viewerContext);
    }

    public void CA8(ViewerContext viewerContext) {
        boolean z = false;
        if (!this.A06 || this.A05.AbO(AnonymousClass1Y3.A3j, false)) {
            z = true;
        }
        Preconditions.checkState(z, "Cannot override viewer context on the application context");
        this.A01 = viewerContext;
    }

    public C05900aW(AnonymousClass0XN r2, Context context, AnonymousClass09P r4, AnonymousClass1YI r5) {
        this.A04 = r2;
        this.A03 = r4;
        this.A06 = context instanceof Application;
        this.A05 = r5;
    }

    public ViewerContext B9R() {
        ViewerContext A002 = A00();
        if (this.A00 == null) {
            this.A00 = A002;
        }
        return A002;
    }

    public ViewerContext B9S() {
        ViewerContext B9R = B9R();
        if (B9R == Asn()) {
            return null;
        }
        return B9R;
    }
}
