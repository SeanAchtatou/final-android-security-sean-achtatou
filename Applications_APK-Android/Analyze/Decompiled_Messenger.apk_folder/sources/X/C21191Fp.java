package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1Fp  reason: invalid class name and case insensitive filesystem */
public class C21191Fp extends C06490ba implements C04670Vq {
    public final WeakHashMap A00;
    public final int A01;
    public final String A02;
    public final Map A03;
    public final BlockingQueue A04;
    public final Executor A05;
    public final AtomicInteger A06;
    private final C190308tE A07;
    private final Executor A08;
    private final AtomicInteger A09;
    private volatile int A0A;

    private void A01() {
        int size = this.A04.size();
        int i = this.A09.get();
        if (size > i) {
            this.A09.compareAndSet(i, size);
        }
    }

    public static void A02(C21191Fp r3) {
        int i;
        do {
            i = r3.A06.get();
            if (i < r3.A0A) {
            } else {
                return;
            }
        } while (!r3.A06.compareAndSet(i, i + 1));
        AnonymousClass07A.A04(r3.A08, r3.A07, 911056284);
    }

    private void A03(Runnable runnable) {
        if (!this.A04.offer(runnable)) {
            ArrayList arrayList = new ArrayList();
            synchronized (this) {
                arrayList.addAll(this.A00.entrySet());
            }
            throw AnonymousClass8YV.A01(this.A02, this.A0A, arrayList, this.A04, null);
        }
    }

    public AnonymousClass3YX CHb(String str, Callable callable) {
        if (callable != null) {
            synchronized (this) {
                C190288tC r0 = (C190288tC) this.A03.get(str);
                if (r0 != null) {
                    return r0;
                }
                AnonymousClass064.A00(str);
                C190288tC r2 = new C190288tC(str, callable);
                if (this.A01 != Integer.MAX_VALUE) {
                    r2.addListener(new C190298tD(this, r2), this.A05);
                }
                A03(r2);
                this.A03.put(r2.A00, r2);
                A01();
                A02(this);
                return r2;
            }
        }
        throw new NullPointerException("The task is empty");
    }

    public AnonymousClass3YX CII(String str, Callable callable) {
        if (callable != null) {
            AnonymousClass064.A00(str);
            C190288tC r2 = new C190288tC(str, callable);
            if (this.A01 != Integer.MAX_VALUE) {
                r2.addListener(new C190298tD(this, r2), this.A05);
            }
            synchronized (this) {
                C190288tC r1 = (C190288tC) this.A03.get(str);
                if (r1 != null) {
                    this.A04.remove(r1);
                    this.A03.remove(str);
                    r1.cancel(false);
                }
                A03(r2);
                this.A03.put(str, r2);
            }
            A01();
            A02(this);
            return r2;
        }
        throw new NullPointerException("The task is empty");
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        Executor executor = this.A08;
        if (executor instanceof AbstractExecutorService) {
            return ((AbstractExecutorService) executor).awaitTermination(j, timeUnit);
        }
        return super.awaitTermination(j, timeUnit);
    }

    public void execute(Runnable runnable) {
        if (runnable != null) {
            A03(runnable);
            A01();
            A02(this);
            return;
        }
        throw new NullPointerException("runnable parameter is null");
    }

    public boolean isShutdown() {
        Executor executor = this.A08;
        if (executor instanceof AbstractExecutorService) {
            return ((AbstractExecutorService) executor).isShutdown();
        }
        return super.isShutdown();
    }

    public boolean isTerminated() {
        Executor executor = this.A08;
        if (executor instanceof AbstractExecutorService) {
            return ((AbstractExecutorService) executor).isTerminated();
        }
        return super.isTerminated();
    }

    public void shutdown() {
        Executor executor = this.A08;
        if (executor instanceof AbstractExecutorService) {
            ((AbstractExecutorService) executor).shutdown();
        } else {
            super.shutdown();
        }
    }

    public List shutdownNow() {
        Executor executor = this.A08;
        if (executor instanceof AbstractExecutorService) {
            return ((AbstractExecutorService) executor).shutdownNow();
        }
        return super.shutdownNow();
    }

    public C21191Fp(String str, int i, Executor executor, BlockingQueue blockingQueue) {
        if (i > 0) {
            this.A02 = str;
            this.A08 = executor;
            this.A0A = i;
            this.A04 = blockingQueue;
            this.A03 = new HashMap();
            this.A01 = blockingQueue.remainingCapacity();
            this.A05 = C25141Ym.INSTANCE;
            this.A07 = new C190308tE(this);
            this.A06 = new AtomicInteger(0);
            this.A09 = new AtomicInteger(0);
            this.A00 = new WeakHashMap();
            return;
        }
        throw new IllegalArgumentException("max concurrency must be > 0");
    }

    public AnonymousClass0XX A04(Runnable runnable, Object obj) {
        AnonymousClass0XX A042 = super.A04(runnable, obj);
        if (this.A01 != Integer.MAX_VALUE) {
            A042.addListener(new C190298tD(this, A042), this.A05);
        }
        return A042;
    }

    public AnonymousClass0XX A05(Callable callable) {
        AnonymousClass0XX A052 = super.A05(callable);
        if (this.A01 != Integer.MAX_VALUE) {
            A052.addListener(new C190298tD(this, A052), this.A05);
        }
        return A052;
    }
}
