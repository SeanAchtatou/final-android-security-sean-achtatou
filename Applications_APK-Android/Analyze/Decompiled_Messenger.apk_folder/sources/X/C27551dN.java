package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1dN  reason: invalid class name and case insensitive filesystem */
public final class C27551dN extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static final Object A01 = new Object();

    public static final C27311cz A00(AnonymousClass1XY r7) {
        C27311cz r0;
        synchronized (A01) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r7)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C27301cy(AnonymousClass0lw.A09(r02), new C27301cy(C10840kw.A00(r02), new C27331d1()));
                }
                C05540Zi r1 = A00;
                r0 = (C27311cz) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }
}
