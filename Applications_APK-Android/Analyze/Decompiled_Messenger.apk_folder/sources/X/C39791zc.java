package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.ipc.composer.interception.ComposerPagesInterceptionDecisionData;

/* renamed from: X.1zc  reason: invalid class name and case insensitive filesystem */
public final class C39791zc implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ComposerPagesInterceptionDecisionData(parcel);
    }

    public Object[] newArray(int i) {
        return new ComposerPagesInterceptionDecisionData[i];
    }
}
