package X;

import android.util.SparseArray;

/* renamed from: X.1bH  reason: invalid class name and case insensitive filesystem */
public final class C26331bH extends AnonymousClass0f6 {
    public static final SparseArray A03 = new SparseArray();
    public static final AnonymousClass0f7 A04 = C08240eu.A00;
    public boolean A00;
    public final int A01;
    public final int A02;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
        if (r5.A07 == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C26331bH() {
        /*
            r6 = this;
            r6.<init>()
            X.1bC r0 = X.C26281bC.A00()
            X.1bF r5 = r0.A06
            boolean r0 = r5.A00()
            if (r0 == 0) goto L_0x0014
            boolean r0 = r5.A07
            r1 = 1
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            r1 = 0
        L_0x0015:
            r6.A00 = r1
            int r0 = r5.A02
            r6.A01 = r0
            if (r1 == 0) goto L_0x002c
            int r4 = r5.A00
            r3 = 0
            r2 = 0
        L_0x0021:
            r1 = 1
            if (r3 >= r4) goto L_0x002d
            int r0 = r5.A01
            int r0 = r0 + r3
            int r1 = r1 << r0
            r2 = r2 | r1
            int r3 = r3 + 1
            goto L_0x0021
        L_0x002c:
            r2 = -1
        L_0x002d:
            r6.A02 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26331bH.<init>():void");
    }
}
