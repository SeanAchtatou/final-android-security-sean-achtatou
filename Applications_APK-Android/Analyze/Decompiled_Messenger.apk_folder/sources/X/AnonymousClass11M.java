package X;

/* renamed from: X.11M  reason: invalid class name */
public enum AnonymousClass11M {
    READ_IO_BUFFER(AnonymousClass1Y3.AX2),
    WRITE_ENCODING_BUFFER(AnonymousClass1Y3.AX2),
    WRITE_CONCAT_BUFFER(2000),
    BASE64_CODEC_BUFFER(2000);
    
    public final int size;

    private AnonymousClass11M(int i) {
        this.size = i;
    }
}
