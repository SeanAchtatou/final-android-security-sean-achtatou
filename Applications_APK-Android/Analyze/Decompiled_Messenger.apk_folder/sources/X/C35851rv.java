package X;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1rv  reason: invalid class name and case insensitive filesystem */
public final class C35851rv implements C36221si, Serializable, Cloneable {
    private static final C36241sk A00 = new C36241sk("inForegroundApp", (byte) 2, 1);
    private static final C36241sk A01 = new C36241sk("inForegroundDevice", (byte) 2, 2);
    private static final C36241sk A02 = new C36241sk("keepAliveTimeout", (byte) 8, 3);
    private static final C36241sk A03 = new C36241sk("requestId", (byte) 10, 8);
    private static final C36241sk A04 = new C36241sk("subscribeGenericTopics", (byte) 15, 5);
    private static final C36241sk A05 = new C36241sk("subscribeTopics", (byte) 15, 4);
    private static final C36241sk A06 = new C36241sk("unsubscribeGenericTopics", (byte) 15, 7);
    private static final C36241sk A07 = new C36241sk("unsubscribeTopics", (byte) 15, 6);
    private static final C36231sj A08 = new C36231sj("ForegroundState");
    public final Boolean inForegroundApp;
    public final Boolean inForegroundDevice;
    public final Integer keepAliveTimeout;
    public final Long requestId;
    public final List subscribeGenericTopics;
    public final List subscribeTopics;
    public final List unsubscribeGenericTopics;
    public final List unsubscribeTopics;

    public boolean equals(Object obj) {
        C35851rv r5;
        if (obj == null || !(obj instanceof C35851rv) || (r5 = (C35851rv) obj) == null) {
            return false;
        }
        if (this == r5) {
            return true;
        }
        Boolean bool = this.inForegroundApp;
        boolean z = false;
        if (bool != null) {
            z = true;
        }
        Boolean bool2 = r5.inForegroundApp;
        boolean z2 = false;
        if (bool2 != null) {
            z2 = true;
        }
        if ((z || z2) && (!z || !z2 || !bool.equals(bool2))) {
            return false;
        }
        Boolean bool3 = this.inForegroundDevice;
        boolean z3 = false;
        if (bool3 != null) {
            z3 = true;
        }
        Boolean bool4 = r5.inForegroundDevice;
        boolean z4 = false;
        if (bool4 != null) {
            z4 = true;
        }
        if ((z3 || z4) && (!z3 || !z4 || !bool3.equals(bool4))) {
            return false;
        }
        Integer num = this.keepAliveTimeout;
        boolean z5 = false;
        if (num != null) {
            z5 = true;
        }
        Integer num2 = r5.keepAliveTimeout;
        boolean z6 = false;
        if (num2 != null) {
            z6 = true;
        }
        if ((z5 || z6) && (!z5 || !z6 || !num.equals(num2))) {
            return false;
        }
        List list = this.subscribeTopics;
        boolean z7 = false;
        if (list != null) {
            z7 = true;
        }
        List list2 = r5.subscribeTopics;
        boolean z8 = false;
        if (list2 != null) {
            z8 = true;
        }
        if ((z7 || z8) && (!z7 || !z8 || !B36.A0F(list, list2))) {
            return false;
        }
        List list3 = this.subscribeGenericTopics;
        boolean z9 = false;
        if (list3 != null) {
            z9 = true;
        }
        List list4 = r5.subscribeGenericTopics;
        boolean z10 = false;
        if (list4 != null) {
            z10 = true;
        }
        if ((z9 || z10) && (!z9 || !z10 || !B36.A0F(list3, list4))) {
            return false;
        }
        List list5 = this.unsubscribeTopics;
        boolean z11 = false;
        if (list5 != null) {
            z11 = true;
        }
        List list6 = r5.unsubscribeTopics;
        boolean z12 = false;
        if (list6 != null) {
            z12 = true;
        }
        if ((z11 || z12) && (!z11 || !z12 || !B36.A0F(list5, list6))) {
            return false;
        }
        List list7 = this.unsubscribeGenericTopics;
        boolean z13 = false;
        if (list7 != null) {
            z13 = true;
        }
        List list8 = r5.unsubscribeGenericTopics;
        boolean z14 = false;
        if (list8 != null) {
            z14 = true;
        }
        if ((z13 || z14) && (!z13 || !z14 || !B36.A0F(list7, list8))) {
            return false;
        }
        Long l = this.requestId;
        boolean z15 = false;
        if (l != null) {
            z15 = true;
        }
        Long l2 = r5.requestId;
        boolean z16 = false;
        if (l2 != null) {
            z16 = true;
        }
        if (!z15 && !z16) {
            return true;
        }
        if (!z15 || !z16 || !l.equals(l2)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return CJ9(1, true);
    }

    public void CNX(C35781ro r5) {
        r5.A0i(A08);
        Boolean bool = this.inForegroundApp;
        if (bool != null) {
            boolean z = false;
            if (bool != null) {
                z = true;
            }
            if (z) {
                r5.A0e(A00);
                r5.A0l(this.inForegroundApp.booleanValue());
                r5.A0S();
            }
        }
        Boolean bool2 = this.inForegroundDevice;
        if (bool2 != null) {
            boolean z2 = false;
            if (bool2 != null) {
                z2 = true;
            }
            if (z2) {
                r5.A0e(A01);
                r5.A0l(this.inForegroundDevice.booleanValue());
                r5.A0S();
            }
        }
        Integer num = this.keepAliveTimeout;
        if (num != null) {
            boolean z3 = false;
            if (num != null) {
                z3 = true;
            }
            if (z3) {
                r5.A0e(A02);
                r5.A0c(this.keepAliveTimeout.intValue());
                r5.A0S();
            }
        }
        List list = this.subscribeTopics;
        if (list != null) {
            boolean z4 = false;
            if (list != null) {
                z4 = true;
            }
            if (z4) {
                r5.A0e(A05);
                r5.A0f(new C36381sy((byte) 8, this.subscribeTopics.size()));
                for (Integer intValue : this.subscribeTopics) {
                    r5.A0c(intValue.intValue());
                }
                r5.A0U();
                r5.A0S();
            }
        }
        List list2 = this.subscribeGenericTopics;
        if (list2 != null) {
            boolean z5 = false;
            if (list2 != null) {
                z5 = true;
            }
            if (z5) {
                r5.A0e(A04);
                r5.A0f(new C36381sy((byte) 12, this.subscribeGenericTopics.size()));
                for (C22374Awt CNX : this.subscribeGenericTopics) {
                    CNX.CNX(r5);
                }
                r5.A0U();
                r5.A0S();
            }
        }
        List list3 = this.unsubscribeTopics;
        if (list3 != null) {
            boolean z6 = false;
            if (list3 != null) {
                z6 = true;
            }
            if (z6) {
                r5.A0e(A07);
                r5.A0f(new C36381sy((byte) 8, this.unsubscribeTopics.size()));
                for (Integer intValue2 : this.unsubscribeTopics) {
                    r5.A0c(intValue2.intValue());
                }
                r5.A0U();
                r5.A0S();
            }
        }
        List list4 = this.unsubscribeGenericTopics;
        if (list4 != null) {
            boolean z7 = false;
            if (list4 != null) {
                z7 = true;
            }
            if (z7) {
                r5.A0e(A06);
                r5.A0f(new C36381sy((byte) 11, this.unsubscribeGenericTopics.size()));
                for (String A0j : this.unsubscribeGenericTopics) {
                    r5.A0j(A0j);
                }
                r5.A0U();
                r5.A0S();
            }
        }
        Long l = this.requestId;
        if (l != null) {
            boolean z8 = false;
            if (l != null) {
                z8 = true;
            }
            if (z8) {
                r5.A0e(A03);
                r5.A0d(this.requestId.longValue());
                r5.A0S();
            }
        }
        r5.A0T();
        r5.A0X();
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{this.inForegroundApp, this.inForegroundDevice, this.keepAliveTimeout, this.subscribeTopics, this.subscribeGenericTopics, this.unsubscribeTopics, this.unsubscribeGenericTopics, this.requestId});
    }

    public C35851rv(Boolean bool, Boolean bool2, Integer num, List list, List list2, List list3, List list4, Long l) {
        this.inForegroundApp = bool;
        this.inForegroundDevice = bool2;
        this.keepAliveTimeout = num;
        this.subscribeTopics = list;
        this.subscribeGenericTopics = list2;
        this.unsubscribeTopics = list3;
        this.unsubscribeGenericTopics = list4;
        this.requestId = l;
    }

    public String CJ9(int i, boolean z) {
        return B36.A06(this, i, z);
    }
}
