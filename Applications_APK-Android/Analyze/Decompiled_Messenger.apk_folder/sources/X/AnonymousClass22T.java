package X;

/* renamed from: X.22T  reason: invalid class name */
public enum AnonymousClass22T {
    UNPREPARED("unprepared"),
    PREPARED("prepared"),
    ATTEMPT_TO_PLAY("attempt_to_play"),
    PLAYING("playing"),
    SEEKING("seeking"),
    ATTEMPT_TO_PAUSE("attempt_to_pause"),
    PAUSED("paused"),
    PLAYBACK_COMPLETE("playback_complete"),
    ERROR("error");
    
    public final String value;

    public boolean A00() {
        if (this == ATTEMPT_TO_PLAY || this == PLAYING) {
            return true;
        }
        return false;
    }

    private AnonymousClass22T(String str) {
        this.value = str;
    }

    public String toString() {
        return this.value;
    }
}
