package X;

import java.util.Arrays;

/* renamed from: X.1LH  reason: invalid class name */
public final class AnonymousClass1LH {
    public static void A00(Object obj, String str) {
        if (obj == null) {
            IllegalStateException illegalStateException = new IllegalStateException(AnonymousClass08S.A0J(str, " must not be null"));
            A02(illegalStateException);
            throw illegalStateException;
        }
    }

    public static void A01(Object obj, String str) {
        if (obj == null) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException(AnonymousClass08S.A0U("Parameter specified as non-null is null: method ", stackTraceElement.getClassName(), ".", stackTraceElement.getMethodName(), ", parameter ", str));
            A02(illegalArgumentException);
            throw illegalArgumentException;
        }
    }

    private static void A02(Throwable th) {
        String name = AnonymousClass1LH.class.getName();
        StackTraceElement[] stackTrace = th.getStackTrace();
        int length = stackTrace.length;
        int i = -1;
        for (int i2 = 0; i2 < length; i2++) {
            if (name.equals(stackTrace[i2].getClassName())) {
                i = i2;
            }
        }
        th.setStackTrace((StackTraceElement[]) Arrays.copyOfRange(stackTrace, i + 1, length));
    }

    private AnonymousClass1LH() {
    }
}
