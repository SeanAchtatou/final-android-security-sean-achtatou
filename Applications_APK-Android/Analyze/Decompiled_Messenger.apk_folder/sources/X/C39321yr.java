package X;

import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.util.Set;

/* renamed from: X.1yr  reason: invalid class name and case insensitive filesystem */
public final class C39321yr implements CallerContextable {
    public static final CallerContext A03;
    public static final Class A04;
    public static final String __redex_internal_original_name = "com.facebook.contacts.upload.ContinuousContactUploadClient";
    public int A00 = 10000;
    public AnonymousClass0UN A01;
    public Set A02;

    static {
        Class<C39321yr> cls = C39321yr.class;
        A03 = CallerContext.A04(cls);
        A04 = cls;
    }

    public static long A00(C39321yr r3) {
        return ((FbSharedPreferences) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B6q, r3.A01)).At2(C52502jD.A0B, -1);
    }

    public static final C39321yr A01(AnonymousClass1XY r1) {
        return new C39321yr(r1);
    }

    public static C27211cp A02(C39321yr r4, String str) {
        ((AnonymousClass9UW) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BIn, r4.A01)).A02(AnonymousClass07B.A0Y);
        Bundle bundle = new Bundle();
        bundle.putBoolean(AnonymousClass80H.$const$string(481), false);
        bundle.putInt("contactsUploadPhonebookMaxLimit", r4.A00);
        C27211cp CGe = ((BlueServiceOperationFactory) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B1W, r4.A01)).newInstance(str, bundle, 1, A03).CGe();
        C05350Yp.A08(CGe, new AnonymousClass9UV(r4), C25141Ym.INSTANCE);
        return CGe;
    }

    public static void A03(C39321yr r8) {
        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, r8.A01)).now();
        C11670nb r6 = new C11670nb("ccu_upload");
        r6.A0D("data_type", "ccu_upload_age");
        r6.A0A("ccu_last_uploaded_addressbook_age_in_seconds", (now - A00(r8)) / 1000);
        r6.A0D("pigeon_reserved_keyword_module", AnonymousClass80H.$const$string(58));
        ((AnonymousClass9UW) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BIn, r8.A01)).A00.A09(r6);
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B6q, r8.A01)).edit();
        edit.BzA(C52502jD.A0B, now);
        edit.commit();
    }

    public C39321yr(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(9, r3);
    }
}
