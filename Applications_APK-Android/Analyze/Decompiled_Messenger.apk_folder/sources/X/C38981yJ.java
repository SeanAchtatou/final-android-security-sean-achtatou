package X;

import com.facebook.payments.checkout.configuration.model.CheckoutOption;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableList;
import java.util.Iterator;

/* renamed from: X.1yJ  reason: invalid class name and case insensitive filesystem */
public final class C38981yJ implements C183978gI {
    private final C8B A00;

    public static final C38981yJ A00(AnonymousClass1XY r2) {
        return new C38981yJ(C8B.A00(r2));
    }

    public Object BwP(String str, JsonNode jsonNode) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24597C7o c7o = (C24597C7o) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AQF, this.A00.A00);
        Iterator it = jsonNode.iterator();
        while (it.hasNext()) {
            builder.add((Object) ((CheckoutOption) c7o.BwP(str, (JsonNode) it.next())));
        }
        return builder.build();
    }

    private C38981yJ(C8B c8b) {
        this.A00 = c8b;
    }
}
