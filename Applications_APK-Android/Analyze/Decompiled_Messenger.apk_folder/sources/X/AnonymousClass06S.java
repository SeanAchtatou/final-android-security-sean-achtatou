package X;

import android.app.PendingIntent;

/* renamed from: X.06S  reason: invalid class name */
public final class AnonymousClass06S extends C06010ah {
    public final /* synthetic */ AnonymousClass06R A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass06S(AnonymousClass06R r1, int i, CharSequence charSequence, PendingIntent pendingIntent) {
        super(i, charSequence, pendingIntent);
        this.A00 = r1;
    }

    public CharSequence A01() {
        if (this.A00.A00) {
            return "Stop Tracing";
        }
        return "Begin Tracing";
    }
}
