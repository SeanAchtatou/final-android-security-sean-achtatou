package X;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.10H  reason: invalid class name */
public final class AnonymousClass10H extends C17770zR {
    public static final ImageView.ScaleType[] A05 = ImageView.ScaleType.values();
    @Comparable(type = 13)
    public Drawable A00;
    @Comparable(type = 13)
    public ImageView.ScaleType A01;
    public AnonymousClass1AN A02;
    public Integer A03;
    public Integer A04;

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r3) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(1);
        ComponentBuilderCBuilderShape0_0S0300000.A01(componentBuilderCBuilderShape0_0S0300000, r3, 0, 0, new AnonymousClass10H());
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public int A0M() {
        return 30;
    }

    public AnonymousClass10H() {
        super("Image");
    }

    public C17770zR A16() {
        AnonymousClass10H r1 = (AnonymousClass10H) super.A16();
        r1.A03 = null;
        r1.A02 = null;
        r1.A04 = null;
        return r1;
    }
}
