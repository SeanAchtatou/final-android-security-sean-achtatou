package X;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0sU  reason: invalid class name and case insensitive filesystem */
public abstract class C14030sU implements Cloneable {
    private static ThreadLocal A0N = new ThreadLocal();
    private static final C28335Dt1 A0O = new DsW();
    private static final int[] A0P = {2, 1, 3, 4};
    public ArrayList A00 = new ArrayList();
    public long A01 = -1;
    public long A02 = -1;
    public TimeInterpolator A03 = null;
    public C28335Dt1 A04 = A0O;
    public C17130yN A05;
    public C28304DsP A06;
    public C16360wv A07 = null;
    public C28267Drh A08 = new C28267Drh();
    public C28267Drh A09 = new C28267Drh();
    public String A0A = getClass().getName();
    public ArrayList A0B;
    public ArrayList A0C;
    public ArrayList A0D = new ArrayList();
    public ArrayList A0E = new ArrayList();
    public boolean A0F = false;
    public int[] A0G = A0P;
    private int A0H = 0;
    private ArrayList A0I = new ArrayList();
    private ArrayList A0J = null;
    private ArrayList A0K = null;
    private boolean A0L = false;
    private boolean A0M = false;

    public Animator A04(ViewGroup viewGroup, C97334ki r3, C97334ki r4) {
        return null;
    }

    /* renamed from: A05 */
    public C14030sU clone() {
        try {
            C14030sU r1 = (C14030sU) super.clone();
            r1.A0I = new ArrayList();
            r1.A09 = new C28267Drh();
            r1.A08 = new C28267Drh();
            r1.A0C = null;
            r1.A0B = null;
            return r1;
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }

    public abstract void A0W(C97334ki r1);

    public abstract void A0X(C97334ki r1);

    public boolean A0b(C97334ki r6, C97334ki r7) {
        if (r6 == null || r7 == null) {
            return false;
        }
        String[] A0c = A0c();
        if (A0c != null) {
            int length = A0c.length;
            int i = 0;
            while (i < length) {
                if (!A03(r6, r7, A0c[i])) {
                    i++;
                }
            }
            return false;
        }
        for (String A032 : r6.A02.keySet()) {
            if (A03(r6, r7, A032)) {
            }
        }
        return false;
        return true;
    }

    public String[] A0c() {
        return null;
    }

    public static AnonymousClass04a A00() {
        AnonymousClass04a r1 = (AnonymousClass04a) A0N.get();
        if (r1 != null) {
            return r1;
        }
        AnonymousClass04a r12 = new AnonymousClass04a();
        A0N.set(r12);
        return r12;
    }

    private void A01(View view, boolean z) {
        if (view != null) {
            int id = view.getId();
            ArrayList arrayList = this.A0K;
            if (arrayList == null || !arrayList.contains(Integer.valueOf(id))) {
                ArrayList arrayList2 = null;
                if (arrayList2 == null || !arrayList2.contains(view)) {
                    ArrayList arrayList3 = null;
                    if (arrayList3 != null) {
                        int size = arrayList3.size();
                        int i = 0;
                        while (i < size) {
                            ArrayList arrayList4 = null;
                            if (!((Class) arrayList4.get(i)).isInstance(view)) {
                                i++;
                            } else {
                                return;
                            }
                        }
                    }
                    if (view.getParent() instanceof ViewGroup) {
                        C97334ki r1 = new C97334ki(view);
                        if (z) {
                            A0X(r1);
                        } else {
                            A0W(r1);
                        }
                        r1.A01.add(this);
                        A0V(r1);
                        if (z) {
                            A02(this.A09, view, r1);
                        } else {
                            A02(this.A08, view, r1);
                        }
                    }
                    if (view instanceof ViewGroup) {
                        ArrayList arrayList5 = null;
                        if (arrayList5 != null && arrayList5.contains(Integer.valueOf(id))) {
                            return;
                        }
                        if (arrayList5 == null || !arrayList5.contains(view)) {
                            if (arrayList5 != null) {
                                int size2 = arrayList5.size();
                                int i2 = 0;
                                while (i2 < size2) {
                                    ArrayList arrayList6 = null;
                                    if (!((Class) arrayList6.get(i2)).isInstance(view)) {
                                        i2++;
                                    } else {
                                        return;
                                    }
                                }
                            }
                            ViewGroup viewGroup = (ViewGroup) view;
                            for (int i3 = 0; i3 < viewGroup.getChildCount(); i3++) {
                                A01(viewGroup.getChildAt(i3), z);
                            }
                        }
                    }
                }
            }
        }
    }

    private static void A02(C28267Drh drh, View view, C97334ki r7) {
        drh.A02.put(view, r7);
        int id = view.getId();
        if (id >= 0) {
            if (drh.A00.indexOfKey(id) >= 0) {
                drh.A00.put(id, null);
            } else {
                drh.A00.put(id, view);
            }
        }
        String transitionName = C15320v6.getTransitionName(view);
        if (transitionName != null) {
            if (drh.A01.containsKey(transitionName)) {
                drh.A01.put(transitionName, null);
            } else {
                drh.A01.put(transitionName, view);
            }
        }
        if (view.getParent() instanceof ListView) {
            ListView listView = (ListView) view.getParent();
            if (listView.getAdapter().hasStableIds()) {
                long itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(view));
                C17610zB r3 = drh.A03;
                if (r3.A02(itemIdAtPosition) >= 0) {
                    View view2 = (View) r3.A07(itemIdAtPosition);
                    if (view2 != null) {
                        C15320v6.setHasTransientState(view2, false);
                        drh.A03.A0D(itemIdAtPosition, null);
                        return;
                    }
                    return;
                }
                C15320v6.setHasTransientState(view, true);
                drh.A03.A0D(itemIdAtPosition, view);
            }
        }
    }

    private static boolean A03(C97334ki r2, C97334ki r3, String str) {
        Object obj = r2.A02.get(str);
        Object obj2 = r3.A02.get(str);
        if (obj == null && obj2 == null) {
            return false;
        }
        if (obj == null || obj2 == null) {
            return true;
        }
        return true ^ obj.equals(obj2);
    }

    public C14030sU A06(int i) {
        if (i != 0) {
            this.A0D.add(Integer.valueOf(i));
        }
        return this;
    }

    public C14030sU A07(int i, boolean z) {
        ArrayList arrayList = this.A0K;
        if (i > 0) {
            Integer valueOf = Integer.valueOf(i);
            if (z) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                if (!arrayList.contains(valueOf)) {
                    arrayList.add(valueOf);
                }
            } else if (arrayList != null) {
                arrayList.remove(valueOf);
                if (arrayList.isEmpty()) {
                    arrayList = null;
                }
            }
        }
        this.A0K = arrayList;
        return this;
    }

    public C14030sU A0B(View view) {
        this.A0E.add(view);
        return this;
    }

    public C14030sU A0C(View view) {
        this.A0E.remove(view);
        return this;
    }

    public C14030sU A0E(C17160yQ r2) {
        if (this.A0J == null) {
            this.A0J = new ArrayList();
        }
        this.A0J.add(r2);
        return this;
    }

    public C14030sU A0F(C17160yQ r2) {
        ArrayList arrayList = this.A0J;
        if (arrayList != null) {
            arrayList.remove(r2);
            if (this.A0J.size() == 0) {
                this.A0J = null;
            }
        }
        return this;
    }

    public C97334ki A0G(View view, boolean z) {
        ArrayList arrayList;
        ArrayList arrayList2;
        C16360wv r0 = this.A07;
        if (r0 != null) {
            return r0.A0G(view, z);
        }
        if (z) {
            arrayList = this.A0C;
        } else {
            arrayList = this.A0B;
        }
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        int i = -1;
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                break;
            }
            C97334ki r1 = (C97334ki) arrayList.get(i2);
            if (r1 == null) {
                return null;
            }
            if (r1.A00 == view) {
                i = i2;
                break;
            }
            i2++;
        }
        if (i < 0) {
            return null;
        }
        if (z) {
            arrayList2 = this.A0B;
        } else {
            arrayList2 = this.A0C;
        }
        return (C97334ki) arrayList2.get(i);
    }

    public C97334ki A0H(View view, boolean z) {
        C28267Drh drh;
        C16360wv r0 = this.A07;
        if (r0 != null) {
            return r0.A0H(view, z);
        }
        if (z) {
            drh = this.A09;
        } else {
            drh = this.A08;
        }
        return (C97334ki) drh.A02.get(view);
    }

    public void A0J() {
        for (int size = this.A00.size() - 1; size >= 0; size--) {
            ((Animator) this.A00.get(size)).cancel();
        }
        ArrayList arrayList = this.A0J;
        if (arrayList != null && arrayList.size() > 0) {
            ArrayList arrayList2 = (ArrayList) this.A0J.clone();
            int size2 = arrayList2.size();
            for (int i = 0; i < size2; i++) {
                ((C17160yQ) arrayList2.get(i)).Bss(this);
            }
        }
    }

    public void A0K() {
        int i = this.A0H - 1;
        this.A0H = i;
        if (i == 0) {
            ArrayList arrayList = this.A0J;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.A0J.clone();
                int size = arrayList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((C17160yQ) arrayList2.get(i2)).Bst(this);
                }
            }
            int i3 = 0;
            while (true) {
                C17610zB r1 = this.A09.A03;
                if (i3 >= r1.A01()) {
                    break;
                }
                View view = (View) r1.A06(i3);
                if (view != null) {
                    C15320v6.setHasTransientState(view, false);
                }
                i3++;
            }
            int i4 = 0;
            while (true) {
                C17610zB r12 = this.A08.A03;
                if (i4 < r12.A01()) {
                    View view2 = (View) r12.A06(i4);
                    if (view2 != null) {
                        C15320v6.setHasTransientState(view2, false);
                    }
                    i4++;
                } else {
                    this.A0L = true;
                    return;
                }
            }
        }
    }

    public void A0L() {
        if (!(this instanceof C16360wv)) {
            A0M();
            AnonymousClass04a A002 = A00();
            Iterator it = this.A0I.iterator();
            while (it.hasNext()) {
                Animator animator = (Animator) it.next();
                if (A002.containsKey(animator)) {
                    A0M();
                    if (animator != null) {
                        animator.addListener(new C1064057e(this, A002));
                        if (animator == null) {
                            A0K();
                        } else {
                            long j = this.A01;
                            if (j >= 0) {
                                animator.setDuration(j);
                            }
                            long j2 = this.A02;
                            if (j2 >= 0) {
                                animator.setStartDelay(j2);
                            }
                            TimeInterpolator timeInterpolator = this.A03;
                            if (timeInterpolator != null) {
                                animator.setInterpolator(timeInterpolator);
                            }
                            animator.addListener(new AnonymousClass4TE(this));
                            animator.start();
                        }
                    }
                }
            }
            this.A0I.clear();
            A0K();
            return;
        }
        C16360wv r4 = (C16360wv) this;
        if (r4.A01.isEmpty()) {
            r4.A0M();
            r4.A0K();
            return;
        }
        C28306DsR dsR = new C28306DsR(r4);
        Iterator it2 = r4.A01.iterator();
        while (it2.hasNext()) {
            ((C14030sU) it2.next()).A0E(dsR);
        }
        r4.A00 = r4.A01.size();
        if (!r4.A02) {
            for (int i = 1; i < r4.A01.size(); i++) {
                ((C14030sU) r4.A01.get(i - 1)).A0E(new C28307DsS((C14030sU) r4.A01.get(i)));
            }
            C14030sU r0 = (C14030sU) r4.A01.get(0);
            if (r0 != null) {
                r0.A0L();
                return;
            }
            return;
        }
        Iterator it3 = r4.A01.iterator();
        while (it3.hasNext()) {
            ((C14030sU) it3.next()).A0L();
        }
    }

    public void A0M() {
        if (this.A0H == 0) {
            ArrayList arrayList = this.A0J;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.A0J.clone();
                int size = arrayList2.size();
                for (int i = 0; i < size; i++) {
                    ((C17160yQ) arrayList2.get(i)).Bsx(this);
                }
            }
            this.A0L = false;
        }
        this.A0H++;
    }

    public void A0N(View view) {
        if (!this.A0L) {
            AnonymousClass04a A002 = A00();
            int size = A002.size();
            C28504Dw2 A003 = C28250DrP.A00(view);
            for (int i = size - 1; i >= 0; i--) {
                DsD dsD = (DsD) A002.A09(i);
                if (dsD.A01 != null && A003.equals(dsD.A04)) {
                    Animator animator = (Animator) A002.A07(i);
                    if (Build.VERSION.SDK_INT >= 19) {
                        animator.pause();
                    } else {
                        ArrayList<Animator.AnimatorListener> listeners = animator.getListeners();
                        if (listeners != null) {
                            int size2 = listeners.size();
                            for (int i2 = 0; i2 < size2; i2++) {
                                Animator.AnimatorListener animatorListener = listeners.get(i2);
                                if (animatorListener instanceof C28477DvY) {
                                    ((C28477DvY) animatorListener).onAnimationPause(animator);
                                }
                            }
                        }
                    }
                }
            }
            ArrayList arrayList = this.A0J;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.A0J.clone();
                int size3 = arrayList2.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    ((C17160yQ) arrayList2.get(i3)).Bsv(this);
                }
            }
            this.A0M = true;
        }
    }

    public void A0O(View view) {
        if (this.A0M) {
            if (!this.A0L) {
                AnonymousClass04a A002 = A00();
                int size = A002.size();
                C28504Dw2 A003 = C28250DrP.A00(view);
                for (int i = size - 1; i >= 0; i--) {
                    DsD dsD = (DsD) A002.A09(i);
                    if (dsD.A01 != null && A003.equals(dsD.A04)) {
                        Animator animator = (Animator) A002.A07(i);
                        if (Build.VERSION.SDK_INT >= 19) {
                            animator.resume();
                        } else {
                            ArrayList<Animator.AnimatorListener> listeners = animator.getListeners();
                            if (listeners != null) {
                                int size2 = listeners.size();
                                for (int i2 = 0; i2 < size2; i2++) {
                                    Animator.AnimatorListener animatorListener = listeners.get(i2);
                                    if (animatorListener instanceof C28477DvY) {
                                        ((C28477DvY) animatorListener).onAnimationResume(animator);
                                    }
                                }
                            }
                        }
                    }
                }
                ArrayList arrayList = this.A0J;
                if (arrayList != null && arrayList.size() > 0) {
                    ArrayList arrayList2 = (ArrayList) this.A0J.clone();
                    int size3 = arrayList2.size();
                    for (int i3 = 0; i3 < size3; i3++) {
                        ((C17160yQ) arrayList2.get(i3)).Bsw(this);
                    }
                }
            }
            this.A0M = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005b, code lost:
        if (A0b(r15, r14) != false) goto L_0x005d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Q(android.view.ViewGroup r29, X.C28267Drh r30, X.C28267Drh r31, java.util.ArrayList r32, java.util.ArrayList r33) {
        /*
            r28 = this;
            r12 = r28
            boolean r0 = r12 instanceof X.C16360wv
            r27 = r29
            r26 = r31
            r24 = r33
            r25 = r32
            if (r0 != 0) goto L_0x0145
            X.04a r11 = A00()
            android.util.SparseIntArray r10 = new android.util.SparseIntArray
            r10.<init>()
            int r17 = r25.size()
            r3 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r9 = 0
        L_0x0021:
            r0 = r17
            if (r9 >= r0) goto L_0x011b
            r0 = r25
            java.lang.Object r15 = r0.get(r9)
            X.4ki r15 = (X.C97334ki) r15
            r0 = r24
            java.lang.Object r14 = r0.get(r9)
            X.4ki r14 = (X.C97334ki) r14
            if (r15 == 0) goto L_0x0040
            java.util.ArrayList r0 = r15.A01
            boolean r0 = r0.contains(r12)
            if (r0 != 0) goto L_0x0040
            r15 = 0
        L_0x0040:
            if (r14 == 0) goto L_0x004b
            java.util.ArrayList r0 = r14.A01
            boolean r0 = r0.contains(r12)
            if (r0 != 0) goto L_0x004b
            r14 = 0
        L_0x004b:
            if (r15 != 0) goto L_0x0052
            if (r14 != 0) goto L_0x0052
        L_0x004f:
            int r9 = r9 + 1
            goto L_0x0021
        L_0x0052:
            if (r15 == 0) goto L_0x005d
            if (r14 == 0) goto L_0x005d
            boolean r1 = r12.A0b(r15, r14)
            r0 = 0
            if (r1 == 0) goto L_0x005e
        L_0x005d:
            r0 = 1
        L_0x005e:
            if (r0 == 0) goto L_0x004f
            r6 = r27
            android.animation.Animator r8 = r12.A04(r6, r15, r14)
            if (r8 == 0) goto L_0x004f
            if (r14 == 0) goto L_0x00a5
            android.view.View r2 = r14.A00
            java.lang.String[] r13 = r12.A0c()
            if (r13 == 0) goto L_0x00a3
            int r7 = r13.length
            if (r7 <= 0) goto L_0x00a3
            X.4ki r6 = new X.4ki
            r6.<init>(r2)
            r0 = r26
            X.04a r0 = r0.A02
            java.lang.Object r5 = r0.get(r2)
            X.4ki r5 = (X.C97334ki) r5
            if (r5 == 0) goto L_0x00a9
            r1 = 0
        L_0x0087:
            if (r1 >= r7) goto L_0x00a9
            java.util.Map r0 = r6.A02
            r20 = r0
            r16 = r13[r1]
            java.util.Map r0 = r5.A02
            r18 = r0
            r19 = r16
            java.lang.Object r0 = r18.get(r19)
            r18 = r20
            r20 = r0
            r18.put(r19, r20)
            int r1 = r1 + 1
            goto L_0x0087
        L_0x00a3:
            r6 = 0
            goto L_0x00d5
        L_0x00a5:
            android.view.View r2 = r15.A00
            r6 = 0
            goto L_0x00d5
        L_0x00a9:
            int r13 = r11.size()
            r7 = 0
        L_0x00ae:
            if (r7 >= r13) goto L_0x00d5
            java.lang.Object r0 = r11.A07(r7)
            android.animation.Animator r0 = (android.animation.Animator) r0
            java.lang.Object r1 = r11.get(r0)
            X.DsD r1 = (X.DsD) r1
            X.4ki r5 = r1.A03
            if (r5 == 0) goto L_0x0118
            android.view.View r0 = r1.A01
            if (r0 != r2) goto L_0x0118
            java.lang.String r1 = r1.A00
            java.lang.String r0 = r12.A0A
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0118
            boolean r0 = r5.equals(r6)
            if (r0 == 0) goto L_0x0118
            r8 = 0
        L_0x00d5:
            if (r8 == 0) goto L_0x004f
            X.DsP r0 = r12.A06
            if (r0 == 0) goto L_0x00f7
            r18 = r0
            r19 = r27
            r20 = r12
            r21 = r15
            r22 = r14
            long r0 = r18.A00(r19, r20, r21, r22)
            java.util.ArrayList r5 = r12.A0I
            int r7 = r5.size()
            int r5 = (int) r0
            r10.put(r7, r5)
            long r3 = java.lang.Math.min(r0, r3)
        L_0x00f7:
            X.DsD r1 = new X.DsD
            java.lang.String r0 = r12.A0A
            r5 = r27
            X.Dw2 r22 = X.C28250DrP.A00(r5)
            r18 = r1
            r19 = r2
            r20 = r0
            r21 = r12
            r23 = r6
            r18.<init>(r19, r20, r21, r22, r23)
            r11.put(r8, r1)
            java.util.ArrayList r0 = r12.A0I
            r0.add(r8)
            goto L_0x004f
        L_0x0118:
            int r7 = r7 + 1
            goto L_0x00ae
        L_0x011b:
            int r0 = r10.size()
            if (r0 == 0) goto L_0x0186
            r7 = 0
        L_0x0122:
            int r0 = r10.size()
            if (r7 >= r0) goto L_0x0186
            int r1 = r10.keyAt(r7)
            java.util.ArrayList r0 = r12.A0I
            java.lang.Object r2 = r0.get(r1)
            android.animation.Animator r2 = (android.animation.Animator) r2
            int r0 = r10.valueAt(r7)
            long r5 = (long) r0
            long r5 = r5 - r3
            long r0 = r2.getStartDelay()
            long r5 = r5 + r0
            r2.setStartDelay(r5)
            int r7 = r7 + 1
            goto L_0x0122
        L_0x0145:
            r10 = r12
            X.0wv r10 = (X.C16360wv) r10
            long r3 = r10.A02
            java.util.ArrayList r0 = r10.A01
            int r9 = r0.size()
            r8 = 0
        L_0x0151:
            if (r8 >= r9) goto L_0x0186
            java.util.ArrayList r0 = r10.A01
            java.lang.Object r7 = r0.get(r8)
            X.0sU r7 = (X.C14030sU) r7
            r5 = 0
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x0171
            boolean r0 = r10.A02
            if (r0 != 0) goto L_0x0167
            if (r8 != 0) goto L_0x0171
        L_0x0167:
            long r1 = r7.A02
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x0182
            long r1 = r1 + r3
            r7.A09(r1)
        L_0x0171:
            r11 = r7
            r12 = r27
            r14 = r26
            r15 = r25
            r16 = r24
            r13 = r30
            r11.A0Q(r12, r13, r14, r15, r16)
            int r8 = r8 + 1
            goto L_0x0151
        L_0x0182:
            r7.A09(r3)
            goto L_0x0171
        L_0x0186:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14030sU.A0Q(android.view.ViewGroup, X.Drh, X.Drh, java.util.ArrayList, java.util.ArrayList):void");
    }

    public void A0S(C28335Dt1 dt1) {
        if (dt1 == null) {
            this.A04 = A0O;
        } else {
            this.A04 = dt1;
        }
    }

    public void A0V(C97334ki r6) {
        String[] A022;
        if (this.A06 != null && !r6.A02.isEmpty() && (A022 = this.A06.A02()) != null) {
            boolean z = false;
            int i = 0;
            while (true) {
                if (i < A022.length) {
                    if (!r6.A02.containsKey(A022[i])) {
                        break;
                    }
                    i++;
                } else {
                    z = true;
                    break;
                }
            }
            if (!z) {
                this.A06.A01(r6);
            }
        }
    }

    public void A0Y(boolean z) {
        C28267Drh drh;
        if (z) {
            this.A09.A02.clear();
            this.A09.A00.clear();
            drh = this.A09;
        } else {
            this.A08.A02.clear();
            this.A08.A00.clear();
            drh = this.A08;
        }
        drh.A03.A09();
    }

    public String toString() {
        return A0I(BuildConfig.FLAVOR);
    }

    public String A0I(String str) {
        String A0T = AnonymousClass08S.A0T(str, getClass().getSimpleName(), "@", Integer.toHexString(hashCode()), ": ");
        long j = this.A01;
        if (j != -1) {
            A0T = A0T + "dur(" + j + ") ";
        }
        long j2 = this.A02;
        if (j2 != -1) {
            A0T = A0T + "dly(" + j2 + ") ";
        }
        TimeInterpolator timeInterpolator = this.A03;
        if (timeInterpolator != null) {
            A0T = A0T + "interp(" + timeInterpolator + ") ";
        }
        if (this.A0D.size() <= 0 && this.A0E.size() <= 0) {
            return A0T;
        }
        String A0J2 = AnonymousClass08S.A0J(A0T, "tgts(");
        if (this.A0D.size() > 0) {
            for (int i = 0; i < this.A0D.size(); i++) {
                if (i > 0) {
                    A0J2 = AnonymousClass08S.A0J(A0J2, ", ");
                }
                A0J2 = A0J2 + this.A0D.get(i);
            }
        }
        if (this.A0E.size() > 0) {
            for (int i2 = 0; i2 < this.A0E.size(); i2++) {
                if (i2 > 0) {
                    A0J2 = AnonymousClass08S.A0J(A0J2, ", ");
                }
                A0J2 = A0J2 + this.A0E.get(i2);
            }
        }
        return AnonymousClass08S.A0J(A0J2, ")");
    }

    public void A0P(ViewGroup viewGroup) {
        AnonymousClass04a A002 = A00();
        int size = A002.size();
        if (viewGroup != null) {
            C28504Dw2 A003 = C28250DrP.A00(viewGroup);
            for (int i = size - 1; i >= 0; i--) {
                DsD dsD = (DsD) A002.A09(i);
                if (!(dsD.A01 == null || A003 == null || !A003.equals(dsD.A04))) {
                    ((Animator) A002.A07(i)).end();
                }
            }
        }
    }

    public void A0R(ViewGroup viewGroup, boolean z) {
        AnonymousClass04b r0;
        ArrayList arrayList;
        ArrayList arrayList2;
        A0Y(z);
        if ((this.A0D.size() > 0 || this.A0E.size() > 0) && (((arrayList = null) == null || arrayList.isEmpty()) && ((arrayList2 = null) == null || arrayList2.isEmpty()))) {
            for (int i = 0; i < this.A0D.size(); i++) {
                View findViewById = viewGroup.findViewById(((Integer) this.A0D.get(i)).intValue());
                if (findViewById != null) {
                    C97334ki r1 = new C97334ki(findViewById);
                    if (z) {
                        A0X(r1);
                    } else {
                        A0W(r1);
                    }
                    r1.A01.add(this);
                    A0V(r1);
                    if (z) {
                        A02(this.A09, findViewById, r1);
                    } else {
                        A02(this.A08, findViewById, r1);
                    }
                }
            }
            for (int i2 = 0; i2 < this.A0E.size(); i2++) {
                View view = (View) this.A0E.get(i2);
                C97334ki r12 = new C97334ki(view);
                if (z) {
                    A0X(r12);
                } else {
                    A0W(r12);
                }
                r12.A01.add(this);
                A0V(r12);
                if (z) {
                    A02(this.A09, view, r12);
                } else {
                    A02(this.A08, view, r12);
                }
            }
        } else {
            A01(viewGroup, z);
        }
        if (!z && (r0 = null) != null) {
            int size = r0.size();
            ArrayList arrayList3 = new ArrayList(size);
            for (int i3 = 0; i3 < size; i3++) {
                AnonymousClass04b r02 = null;
                arrayList3.add(this.A09.A01.remove((String) r02.A07(i3)));
            }
            for (int i4 = 0; i4 < size; i4++) {
                View view2 = (View) arrayList3.get(i4);
                if (view2 != null) {
                    AnonymousClass04b r03 = null;
                    this.A09.A01.put((String) r03.A09(i4), view2);
                }
            }
        }
    }

    public boolean A0a(View view) {
        ArrayList arrayList;
        ArrayList arrayList2;
        ArrayList arrayList3;
        ArrayList arrayList4;
        int id = view.getId();
        ArrayList arrayList5 = this.A0K;
        if ((arrayList5 != null && arrayList5.contains(Integer.valueOf(id))) || ((arrayList = null) != null && arrayList.contains(view))) {
            return false;
        }
        ArrayList arrayList6 = null;
        if (arrayList6 != null) {
            int size = arrayList6.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                }
                ArrayList arrayList7 = null;
                if (((Class) arrayList7.get(i)).isInstance(view)) {
                    break;
                }
                i++;
            }
        }
        if (!(0 == 0 || C15320v6.getTransitionName(view) == null)) {
            ArrayList arrayList8 = null;
            if (arrayList8.contains(C15320v6.getTransitionName(view))) {
                return false;
            }
        }
        if ((this.A0D.size() == 0 && this.A0E.size() == 0 && (((arrayList3 = null) == null || arrayList3.isEmpty()) && ((arrayList4 = null) == null || arrayList4.isEmpty()))) || this.A0D.contains(Integer.valueOf(id)) || this.A0E.contains(view) || ((arrayList2 = null) != null && arrayList2.contains(C15320v6.getTransitionName(view)))) {
            return true;
        }
        if (0 != 0) {
            int i2 = 0;
            while (true) {
                ArrayList arrayList9 = null;
                if (i2 >= arrayList9.size()) {
                    break;
                }
                ArrayList arrayList10 = null;
                if (((Class) arrayList10.get(i2)).isInstance(view)) {
                    break;
                }
                i2++;
            }
        }
        return true;
        return false;
    }

    public C14030sU A08(long j) {
        this.A01 = j;
        return this;
    }

    public C14030sU A09(long j) {
        this.A02 = j;
        return this;
    }

    public C14030sU A0A(TimeInterpolator timeInterpolator) {
        this.A03 = timeInterpolator;
        return this;
    }

    public C14030sU A0D(ViewGroup viewGroup) {
        return this;
    }

    public void A0T(C17130yN r1) {
        this.A05 = r1;
    }

    public void A0U(C28304DsP dsP) {
        this.A06 = dsP;
    }

    public void A0Z(boolean z) {
        this.A0F = z;
    }
}
