package X;

import android.content.Context;
import com.facebook.common.dextricks.MultiDexClassLoader;

/* renamed from: X.0Xs  reason: invalid class name and case insensitive filesystem */
public final class C05120Xs implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.init.impl.FbAppInitializerInternal$7";
    public final /* synthetic */ AnonymousClass0WR A00;
    public final /* synthetic */ AnonymousClass0Y7 A01;
    public final /* synthetic */ boolean A02;

    public C05120Xs(AnonymousClass0Y7 r1, boolean z, AnonymousClass0WR r3) {
        this.A01 = r1;
        this.A02 = z;
        this.A00 = r3;
    }

    /* JADX INFO: finally extract failed */
    public void run() {
        Context applicationContext;
        if (this.A02) {
            AnonymousClass0Y7 r4 = this.A01;
            C005505z.A03("onColdStartDone", 98055016);
            try {
                int i = AnonymousClass1Y3.B79;
                AnonymousClass0UN r3 = r4.A01;
                ((AnonymousClass0F1) AnonymousClass1XX.A02(16, i, r3)).BSp((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r3));
                Context context = (Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r4.A01);
                if (!(context == null || (applicationContext = context.getApplicationContext()) == null || !(applicationContext instanceof AnonymousClass004))) {
                    ((AnonymousClass004) applicationContext).C50();
                }
                MultiDexClassLoader.notifyCurrentClassLoaderThatColdstartDone();
            } finally {
                C005505z.A00(1930525545);
            }
        }
        AnonymousClass0Y7 r5 = this.A01;
        AnonymousClass0WR r9 = this.A00;
        long now = ((AnonymousClass069) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BBa, r5.A01)).now();
        for (int i2 = 0; i2 < r9.BKI(); i2++) {
            AnonymousClass1YQ BLp = r9.BLp();
            if (BLp != null) {
                C005505z.A03(C05260Yg.A00(BLp.getClass()), -1962010842);
                try {
                    BLp.init();
                    C005505z.A00(-1320668449);
                    if (((AnonymousClass069) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BBa, r5.A01)).now() - now > 5) {
                        AnonymousClass0Y7.A03(r5, r9, false);
                        return;
                    }
                } catch (Throwable th) {
                    C005505z.A00(-1903103643);
                    throw th;
                }
            }
        }
        if (!r5.A05) {
            ((AnonymousClass0Y3) r5.A02.get()).A00 = null;
        }
        r5.A02 = null;
    }
}
