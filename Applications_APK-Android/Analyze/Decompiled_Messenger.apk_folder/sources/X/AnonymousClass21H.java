package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

@UserScoped
/* renamed from: X.21H  reason: invalid class name */
public final class AnonymousClass21H implements C05460Za {
    private static C05540Zi A04;
    public AnonymousClass0UN A00;
    public final Map A01 = Collections.synchronizedMap(new HashMap());
    public final Map A02 = Collections.synchronizedMap(new HashMap());
    public final CopyOnWriteArrayList A03 = new CopyOnWriteArrayList();

    public static final AnonymousClass21H A00(AnonymousClass1XY r4) {
        AnonymousClass21H r0;
        synchronized (AnonymousClass21H.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new AnonymousClass21H((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (AnonymousClass21H) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A02(AnonymousClass21H r1, String str, String str2, String str3) {
        if (!r1.A02.containsKey(str) || !Objects.equal((String) r1.A02.get(str), str2)) {
            r1.A02.put(str, str2);
            r1.A03(str, str3);
        }
    }

    private void A03(String str, String str2) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.A01.get(str);
        if (copyOnWriteArrayList != null) {
            String str3 = (String) this.A02.get(str);
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                if (weakReference.get() != null) {
                    AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ADs, this.A00), new C162007ef(weakReference, str3, str2), -1186576389);
                }
            }
        }
    }

    public void A04(C162017eg r4) {
        ThreadKey A012 = ((C155757Ic) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B1G, this.A00)).A01();
        if (A012 != null) {
            A01(this, A012, r4);
            this.A03.remove(new WeakReference(r4));
        }
    }

    public void A05(C162017eg r6, boolean z) {
        ThreadKey A012 = ((C155757Ic) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B1G, this.A00)).A01();
        if (A012 != null) {
            this.A03.add(new WeakReference(r6));
            if (((AnonymousClass23X) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BMb, this.A00)).A01() && !ThreadKey.A0F(A012)) {
                Preconditions.checkNotNull(A012);
                Preconditions.checkNotNull(r6);
                String A0I = A012.A0I();
                Preconditions.checkNotNull(A0I);
                CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.A01.get(A0I);
                if (copyOnWriteArrayList == null) {
                    copyOnWriteArrayList = new CopyOnWriteArrayList();
                    this.A01.put(A0I, copyOnWriteArrayList);
                }
                copyOnWriteArrayList.add(new WeakReference(r6));
                AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1n, this.A00), new C103134vj(this, A0I), 1188572924);
                if (!z) {
                    return;
                }
                if (this.A02.containsKey(A0I)) {
                    r6.Bcl((String) this.A02.get(A0I), "cache");
                } else {
                    r6.Bcl(null, "cache");
                }
            }
        }
    }

    public void clearUserData() {
        this.A02.clear();
    }

    private AnonymousClass21H(AnonymousClass1XY r4) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(6, r4);
        this.A00 = r2;
        ((AnonymousClass21I) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B8f, r2)).A00 = new AnonymousClass235(this);
    }

    public static void A01(AnonymousClass21H r5, ThreadKey threadKey, C162017eg r7) {
        Preconditions.checkNotNull(threadKey);
        Preconditions.checkNotNull(r7);
        String A0I = threadKey.A0I();
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) r5.A01.get(A0I);
        if (copyOnWriteArrayList != null) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                if (weakReference.get() == null || weakReference.get() == r7) {
                    copyOnWriteArrayList.remove(weakReference);
                }
            }
            if (copyOnWriteArrayList.isEmpty()) {
                r5.A01.remove(A0I);
                ((C75983kl) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ALl, r5.A00)).A04(A0I);
            }
        }
    }
}
