package X;

import com.facebook.common.util.StringLocaleUtil;
import java.util.Iterator;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

/* renamed from: X.0Ot  reason: invalid class name and case insensitive filesystem */
public final class C03620Ot implements AnonymousClass0DW {
    /* JADX WARN: Type inference failed for: r4v1, types: [java.util.regex.Pattern, java.lang.Integer] */
    public Object BDF(AnonymousClass0PE r7) {
        boolean z;
        C04040Ro r2;
        String str;
        String str2 = r7.A00;
        if (str2 != null) {
            C04030Rn r3 = C04030Rn.A01;
            synchronized (r3) {
                try {
                    z = false;
                    if (!r3.A00.isEmpty()) {
                        z = true;
                    }
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            if (z) {
                synchronized (C04030Rn.A01) {
                    try {
                        Iterator it = r3.A00.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                r2 = null;
                                break;
                            }
                            r2 = (C04040Ro) it.next();
                            Pattern pattern = null;
                            if (pattern.matcher(str2).matches()) {
                                break;
                            }
                        }
                    } catch (Throwable th2) {
                        while (true) {
                            th = th2;
                            break;
                        }
                    }
                }
                if (r2 != null) {
                    ? r4 = 0;
                    String simpleName = getClass().getSimpleName();
                    int intValue = r4.intValue();
                    if (1 != intValue) {
                        str = "BLOCK";
                    } else {
                        str = "ALLOW";
                    }
                    StringLocaleUtil.A00("%s(%s)", simpleName, str);
                    StringLocaleUtil.A00("Intercepted '%s' with pattern: %s", str2, r4.toString());
                    switch (intValue) {
                        case 0:
                            throw new AnonymousClass0SV(AnonymousClass0DA.A04);
                        case 1:
                            break;
                        default:
                            throw new AssertionError("Non-exhaustive switch statement on interception");
                    }
                }
            }
        }
        return (Future) r7.A00(str2);
        throw th;
    }
}
