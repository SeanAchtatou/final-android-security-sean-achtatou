package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Pi  reason: invalid class name and case insensitive filesystem */
public final class C23391Pi implements C23411Pk {
    public final List A00;

    public void Bk1(String str, String str2, String str3) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bk1(str, str2, str3);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onIntermediateChunkStart", e);
            }
        }
    }

    public void Bk3(String str, String str2, Map map) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bk3(str, str2, map);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onProducerFinishWithCancellation", e);
            }
        }
    }

    public void Bk5(String str, String str2, Throwable th, Map map) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bk5(str, str2, th, map);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onProducerFinishWithFailure", e);
            }
        }
    }

    public void Bk7(String str, String str2, Map map) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bk7(str, str2, map);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onProducerFinishWithSuccess", e);
            }
        }
    }

    public void Bk9(String str, String str2) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bk9(str, str2);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onProducerStart", e);
            }
        }
    }

    public void Bls(String str) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bls(str);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onRequestCancellation", e);
            }
        }
    }

    public void Bm2(AnonymousClass1Q0 r6, String str, Throwable th, boolean z) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bm2(r6, str, th, z);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onRequestFailure", e);
            }
        }
    }

    public void Bm7(AnonymousClass1Q0 r6, Object obj, String str, boolean z) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bm7(r6, obj, str, z);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onRequestStart", e);
            }
        }
    }

    public void Bm9(AnonymousClass1Q0 r6, String str, boolean z) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bm9(r6, str, z);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onRequestSuccess", e);
            }
        }
    }

    public void Bt9(String str, String str2, boolean z) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((C23411Pk) this.A00.get(i)).Bt9(str, str2, z);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener", "InternalListener exception in onProducerFinishWithSuccess", e);
            }
        }
    }

    public boolean C3R(String str) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            if (((C23411Pk) this.A00.get(i)).C3R(str)) {
                return true;
            }
        }
        return false;
    }

    public C23391Pi(Set set) {
        this.A00 = new ArrayList(set.size());
        Iterator it = set.iterator();
        while (it.hasNext()) {
            C23411Pk r1 = (C23411Pk) it.next();
            if (r1 != null) {
                this.A00.add(r1);
            }
        }
    }

    public C23391Pi(C23411Pk... r5) {
        this.A00 = new ArrayList(r3);
        for (C23411Pk r1 : r5) {
            if (r1 != null) {
                this.A00.add(r1);
            }
        }
    }
}
