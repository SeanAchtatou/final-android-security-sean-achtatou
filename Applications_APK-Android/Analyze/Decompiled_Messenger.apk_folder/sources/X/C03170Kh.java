package X;

import android.content.Context;
import java.io.File;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Kh  reason: invalid class name and case insensitive filesystem */
public final class C03170Kh {
    public static final long A07 = TimeUnit.MINUTES.toMillis(10);
    public long A00 = 15000;
    public long A01 = A07;
    public boolean A02 = false;
    public boolean A03 = true;
    public boolean A04;
    private boolean A05 = false;
    public final File A06;

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0053 */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0057 A[SYNTHETIC, Splitter:B:25:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0061 A[SYNTHETIC, Splitter:B:31:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.C03170Kh r5) {
        /*
            boolean r0 = r5.A05
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            java.io.File r0 = r5.A06
            r0.mkdir()
            java.io.File r2 = new java.io.File
            java.io.File r1 = r5.A06
            java.lang.String r0 = "xprocessconfig"
            r2.<init>(r1, r0)
            boolean r0 = r2.exists()
            r4 = 1
            if (r0 != 0) goto L_0x0020
            r0 = 0
            r5.A04 = r0
        L_0x001d:
            r5.A05 = r4
            return
        L_0x0020:
            r5.A04 = r4
            r1 = 0
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0053 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0053 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0053 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x0053 }
            short r2 = r3.readShort()     // Catch:{ IOException -> 0x0052, all -> 0x005e }
            short r1 = r3.readShort()     // Catch:{ IOException -> 0x0052, all -> 0x005e }
            r0 = 251(0xfb, float:3.52E-43)
            if (r0 != r2) goto L_0x004e
            r0 = 3
            if (r0 != r1) goto L_0x004e
            long r0 = r3.readLong()     // Catch:{ IOException -> 0x0052, all -> 0x005e }
            r5.A01 = r0     // Catch:{ IOException -> 0x0052, all -> 0x005e }
            long r0 = r3.readLong()     // Catch:{ IOException -> 0x0052, all -> 0x005e }
            r5.A00 = r0     // Catch:{ IOException -> 0x0052, all -> 0x005e }
            boolean r0 = r3.readBoolean()     // Catch:{ IOException -> 0x0052, all -> 0x005e }
            r5.A03 = r0     // Catch:{ IOException -> 0x0052, all -> 0x005e }
        L_0x004e:
            r3.close()     // Catch:{ IOException -> 0x001d }
            goto L_0x001d
        L_0x0052:
            r1 = r3
        L_0x0053:
            r5.A02 = r4     // Catch:{ all -> 0x005b }
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x001d }
            goto L_0x001d
        L_0x005b:
            r0 = move-exception
            r3 = r1
            goto L_0x005f
        L_0x005e:
            r0 = move-exception
        L_0x005f:
            if (r3 == 0) goto L_0x0064
            r3.close()     // Catch:{ IOException -> 0x0064 }
        L_0x0064:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03170Kh.A00(X.0Kh):void");
    }

    public String toString() {
        if (!this.A05) {
            return "XProcessConfig{not initialized}";
        }
        return "XProcessConfig{mIsLoggingEnabled=" + this.A04 + ", mReportingIntervalMs=" + this.A01 + ", mInitialDelayMs=" + this.A00 + ", mIsLogWriteBroadcastEnabled=" + this.A03 + '}';
    }

    public C03170Kh(Context context) {
        this.A06 = new File(context.getFilesDir(), "batterymetrics");
    }
}
