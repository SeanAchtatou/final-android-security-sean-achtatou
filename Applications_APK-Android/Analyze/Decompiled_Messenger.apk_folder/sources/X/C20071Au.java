package X;

import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.1Au  reason: invalid class name and case insensitive filesystem */
public final class C20071Au implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.orca.threadlist.ThreadListFragment$22";
    public final /* synthetic */ ThreadListFragment A00;
    public final /* synthetic */ C13880sE A01;
    public final /* synthetic */ Integer A02;

    public C20071Au(ThreadListFragment threadListFragment, C13880sE r2, Integer num) {
        this.A00 = threadListFragment;
        this.A01 = r2;
        this.A02 = num;
    }

    public void run() {
        ThreadListFragment threadListFragment = this.A00;
        threadListFragment.A0R.A0O(ThreadListFragment.A07(threadListFragment, this.A01, this.A02));
    }
}
