package X;

import android.view.animation.Interpolator;
import com.facebook.common.asyncview.SpriteView;

/* renamed from: X.0SU  reason: invalid class name */
public final class AnonymousClass0SU extends C03550Om {
    public final /* synthetic */ float A00;
    public final /* synthetic */ SpriteView.Sprite A01;
    public final /* synthetic */ AnonymousClass01E A02;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0SU(AnonymousClass01E r1, Interpolator interpolator, long j, SpriteView.Sprite sprite, float f) {
        super(interpolator, j);
        this.A02 = r1;
        this.A01 = sprite;
        this.A00 = f;
    }

    public void A00(float f) {
        SpriteView.Sprite sprite = this.A01;
        float f2 = this.A00;
        sprite.setTranslationY(((((this.A02.A08 * 16.0f) + f2) - f2) * f) + f2);
    }
}
