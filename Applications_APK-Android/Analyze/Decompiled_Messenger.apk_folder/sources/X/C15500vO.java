package X;

/* renamed from: X.0vO  reason: invalid class name and case insensitive filesystem */
public final class C15500vO {
    public AnonymousClass0UN A00;

    public static final C15500vO A00(AnonymousClass1XY r1) {
        return new C15500vO(r1);
    }

    public void A01() {
        ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, this.A00)).A03("android_messenger_thread_list_load_threads_success");
    }

    public void A02(String str) {
        ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, this.A00)).A03(str);
    }

    public void A03(String str) {
        ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, this.A00)).A03(AnonymousClass08S.A0J("android_messenger_thread_list_load_more_threads_from_", str));
    }

    public void A04(String str) {
        ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, this.A00)).A03(AnonymousClass08S.A0J("android_messenger_thread_list_load_threads_from_", str));
    }

    public C15500vO(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
