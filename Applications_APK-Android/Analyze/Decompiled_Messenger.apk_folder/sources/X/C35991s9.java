package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.common.calltoaction.model.AdCallToAction;

/* renamed from: X.1s9  reason: invalid class name and case insensitive filesystem */
public final class C35991s9 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new AdCallToAction(parcel);
    }

    public Object[] newArray(int i) {
        return new AdCallToAction[i];
    }
}
