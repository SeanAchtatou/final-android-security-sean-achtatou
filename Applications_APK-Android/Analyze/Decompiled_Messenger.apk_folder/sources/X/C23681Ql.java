package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.List;

/* renamed from: X.1Ql  reason: invalid class name and case insensitive filesystem */
public final class C23681Ql extends C20831Dz {
    public AnonymousClass7G9 A00;
    public List A01 = C04300To.A00();
    public final Context A02;
    public final LayoutInflater A03;
    public final View.OnClickListener A04 = new C55822on(this);
    public final AnonymousClass2Wx A05;
    public final AnonymousClass1Y6 A06;
    public final C55832oo A07;
    public final FbSharedPreferences A08;
    public final C04310Tq A09;

    public static final C23681Ql A00(AnonymousClass1XY r1) {
        return new C23681Ql(r1);
    }

    public int ArU() {
        return this.A01.size() + 1;
    }

    public C23681Ql(AnonymousClass1XY r4) {
        this.A02 = AnonymousClass1YA.A00(r4);
        this.A09 = AnonymousClass0WY.A02(r4);
        this.A03 = C04490Ux.A0e(r4);
        this.A07 = new C55832oo(C04490Ux.A0L(r4), AnonymousClass067.A02());
        this.A05 = new AnonymousClass2Wx(r4);
        this.A08 = FbSharedPreferencesModule.A00(r4);
        this.A06 = AnonymousClass0UX.A07(r4);
    }
}
