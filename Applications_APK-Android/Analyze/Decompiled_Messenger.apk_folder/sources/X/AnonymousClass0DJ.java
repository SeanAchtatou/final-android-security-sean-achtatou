package X;

import android.content.Context;
import android.os.Build;

/* renamed from: X.0DJ  reason: invalid class name */
public final class AnonymousClass0DJ {
    public static String A00(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return Build.SERIAL;
        }
        if (AnonymousClass01R.A01(context, "android.permission.READ_PHONE_STATE") == 0) {
            return Build.getSerial();
        }
        return "PERMISSION_DENIED";
    }
}
