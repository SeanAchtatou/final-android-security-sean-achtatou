package X;

import android.net.Uri;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1H4  reason: invalid class name */
public interface AnonymousClass1H4 {
    String Akc();

    ImmutableList Akd();

    Uri AmV(int i, int i2, int i3);

    Uri ApX(int i, int i2, int i3);

    int Aw7();

    C21381Gs B6D();

    int B6P();

    ImmutableList B8C();

    boolean BHA();
}
