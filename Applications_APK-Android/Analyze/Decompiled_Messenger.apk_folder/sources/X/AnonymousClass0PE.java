package X;

import java.util.List;

/* renamed from: X.0PE  reason: invalid class name */
public final class AnonymousClass0PE {
    public final String A00;
    private final int A01;
    private final List A02;

    public Object A00(String str) {
        if (this.A01 < this.A02.size()) {
            return ((AnonymousClass0DW) this.A02.get(this.A01)).BDF(new AnonymousClass0PE(str, this.A02, this.A01 + 1));
        }
        throw new AssertionError();
    }

    public AnonymousClass0PE(String str, List list, int i) {
        this.A00 = str;
        this.A01 = i;
        this.A02 = list;
    }
}
