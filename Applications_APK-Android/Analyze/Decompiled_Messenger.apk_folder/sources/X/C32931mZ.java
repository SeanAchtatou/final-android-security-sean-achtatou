package X;

import com.facebook.common.callercontext.CallerContextable;

/* renamed from: X.1mZ  reason: invalid class name and case insensitive filesystem */
public final class C32931mZ implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.orca.threadlist.MarkFolderSeenHelper";
    public AnonymousClass0UN A00;
    public boolean A01;

    public static final C32931mZ A00(AnonymousClass1XY r1) {
        return new C32931mZ(r1);
    }

    private C32931mZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
