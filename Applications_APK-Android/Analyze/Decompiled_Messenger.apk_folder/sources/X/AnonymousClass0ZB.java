package X;

import com.facebook.common.stringformat.StringFormatUtil;
import java.util.Set;

/* renamed from: X.0ZB  reason: invalid class name */
public final class AnonymousClass0ZB implements AnonymousClass0Z1 {
    private AnonymousClass0UN A00;

    public String Amj() {
        return "mqtt_client_subscription_data";
    }

    public static final AnonymousClass0ZB A00(AnonymousClass1XY r1) {
        return new AnonymousClass0ZB(r1);
    }

    public String getCustomData(Throwable th) {
        Set set;
        AnonymousClass1FN r1 = (AnonymousClass1FN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AS6, this.A00);
        synchronized (r1) {
            set = r1.A03;
        }
        if (set == null) {
            return "No currently active topics";
        }
        return StringFormatUtil.formatStrLocaleSafe("Currently active topics : %s", set.toString());
    }

    private AnonymousClass0ZB(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
