package X;

import android.os.HandlerThread;

/* renamed from: X.0V5  reason: invalid class name */
public final class AnonymousClass0V5 implements AnonymousClass0V6 {
    public final /* synthetic */ AnonymousClass0V4 A00;

    public AnonymousClass0V5(AnonymousClass0V4 r1) {
        this.A00 = r1;
    }

    public HandlerThread AUc(String str, int i, boolean z) {
        return new AnonymousClass0V9(this.A00, str, i, z);
    }
}
