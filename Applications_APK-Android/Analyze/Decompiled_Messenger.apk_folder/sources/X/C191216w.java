package X;

import android.os.Looper;

/* renamed from: X.16w  reason: invalid class name and case insensitive filesystem */
public final class C191216w {
    public static boolean A00() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
}
