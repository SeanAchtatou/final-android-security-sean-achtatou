package X;

import java.util.Locale;

/* renamed from: X.0GR  reason: invalid class name */
public final class AnonymousClass0GR implements Comparable {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1.equals(r5.A02) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0058
            r2 = 0
            if (r5 == 0) goto L_0x001e
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x001e
            X.0GR r5 = (X.AnonymousClass0GR) r5
            java.lang.String r1 = r4.A02
            if (r1 == 0) goto L_0x001f
            java.lang.String r0 = r5.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0024
        L_0x001e:
            return r2
        L_0x001f:
            java.lang.String r0 = r5.A02
            if (r0 == 0) goto L_0x0024
            return r2
        L_0x0024:
            java.lang.String r1 = r4.A03
            java.lang.String r0 = r5.A03
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001e
            java.lang.String r1 = r4.A01
            java.lang.String r0 = r5.A01
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001e
            java.lang.String r1 = r4.A00
            if (r1 == 0) goto L_0x0045
            java.lang.String r0 = r5.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x004a
            return r2
        L_0x0045:
            java.lang.String r0 = r5.A00
            if (r0 == 0) goto L_0x004a
            return r2
        L_0x004a:
            java.lang.String r1 = r4.A04
            java.lang.String r0 = r5.A04
            if (r1 == 0) goto L_0x0055
            boolean r3 = r1.equals(r0)
            return r3
        L_0x0055:
            if (r0 == 0) goto L_0x0058
            r3 = 0
        L_0x0058:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GR.equals(java.lang.Object):boolean");
    }

    public int compareTo(Object obj) {
        AnonymousClass0GR r3 = (AnonymousClass0GR) obj;
        int compareTo = this.A03.compareTo(r3.A03);
        if (compareTo != 0) {
            return compareTo;
        }
        return this.A01.compareTo(r3.A01);
    }

    public int hashCode() {
        int i;
        int i2;
        String str = this.A02;
        int i3 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int hashCode = ((((i * 31) + this.A03.hashCode()) * 31) + this.A01.hashCode()) * 31;
        String str2 = this.A00;
        if (str2 != null) {
            i2 = str2.hashCode();
        } else {
            i2 = 0;
        }
        int i4 = (hashCode + i2) * 31;
        String str3 = this.A04;
        if (str3 != null) {
            i3 = str3.hashCode();
        }
        return i4 + i3;
    }

    public String toString() {
        return String.format(Locale.US, "[name: %s, hash: %s, id: %s, downloadUri: %s, path: %s]", this.A03, this.A01, this.A02, this.A00, this.A04);
    }

    public AnonymousClass0GR(String str, String str2, String str3, String str4, String str5) {
        this.A03 = str;
        this.A01 = str2;
        this.A02 = str3;
        this.A00 = str4;
        this.A04 = str5;
    }
}
