package X;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.facebook.orca.threadview.ThreadViewFragment;

/* renamed from: X.0qn  reason: invalid class name and case insensitive filesystem */
public final class C13150qn {
    public Fragment A00;
    public Fragment A01;
    public Fragment A02;
    public Fragment A03;
    public Fragment A04;
    public final C16290wo A05;
    public final C55632oP A06;
    private final C13060qW A07;

    public void A00() {
        if (this.A01 == null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("defer_init", true);
            C13450rS r2 = new C13450rS();
            r2.A1P(bundle);
            this.A05.A08(2131300895, r2);
            this.A01 = r2;
        }
    }

    public void A01() {
        if (C16000wK.A01(this.A07)) {
            this.A05.A02();
            C13060qW r1 = this.A07;
            if (!C16000wK.A00(r1)) {
                r1.A0Y();
            }
        }
    }

    public void A02() {
        Fragment fragment = this.A01;
        if (fragment != null) {
            C16290wo r2 = this.A05;
            r2.A07(0, 2130771981);
            r2.A0H(fragment);
        }
    }

    public void A03() {
        Fragment fragment = this.A04;
        if (fragment != null && !fragment.A0b) {
            ((ThreadViewFragment) fragment).A2S();
            C16290wo r2 = this.A05;
            r2.A07(0, 2130771979);
            r2.A0H(this.A04);
        }
    }

    public void A04() {
        Fragment fragment = this.A00;
        if (fragment != null) {
            this.A05.A0J(fragment);
            return;
        }
        Fragment fragment2 = this.A01;
        if (fragment2 == null) {
            A00();
            return;
        }
        C16290wo r2 = this.A05;
        r2.A07(2130771978, 0);
        r2.A0J(fragment2);
    }

    public C13150qn(C13060qW r2, C55632oP r3) {
        this.A07 = r2;
        this.A05 = r2.A0T();
        this.A06 = r3;
        this.A01 = r2.A0O(2131300895);
        this.A04 = r2.A0O(2131301002);
        this.A03 = r2.A0O(2131300410);
        this.A00 = r2.A0O(2131298121);
        this.A02 = r2.A0O(2131299191);
    }
}
