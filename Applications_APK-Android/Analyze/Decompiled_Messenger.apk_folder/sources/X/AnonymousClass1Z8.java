package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.1Z8  reason: invalid class name */
public final class AnonymousClass1Z8 implements FilenameFilter {
    public boolean accept(File file, String str) {
        return str.endsWith(".mctable");
    }
}
