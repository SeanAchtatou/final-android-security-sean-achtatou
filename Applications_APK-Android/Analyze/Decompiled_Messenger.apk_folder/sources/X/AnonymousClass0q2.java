package X;

import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

/* renamed from: X.0q2  reason: invalid class name */
public final class AnonymousClass0q2 {
    public static void A07(byte[] bArr, File file) {
        C192448zs r0 = new C192448zs(file, new C192488zw[0]);
        Preconditions.checkNotNull(bArr);
        C12840q5 A00 = C12840q5.A00();
        try {
            OutputStream A002 = r0.A00();
            A00.A02(A002);
            A002.write(bArr);
            A002.flush();
            A00.close();
        } catch (Throwable th) {
            A00.close();
            throw th;
        }
    }

    public static C07190cq A00(File file, C07160cj r4) {
        C12820q3 r2 = new C12820q3(file);
        C07180co A01 = r4.A01();
        r2.A03(new C99514pC(A01));
        return A01.A09();
    }

    public static String A01(File file, Charset charset) {
        AnonymousClass900 r0 = new AnonymousClass900(new C12820q3(file), charset);
        C12840q5 A00 = C12840q5.A00();
        try {
            Reader A002 = r0.A00();
            A00.A02(A002);
            StringBuilder sb = new StringBuilder();
            Preconditions.checkNotNull(A002);
            Preconditions.checkNotNull(sb);
            CharBuffer allocate = CharBuffer.allocate(2048);
            while (A002.read(allocate) != -1) {
                allocate.flip();
                sb.append((CharSequence) allocate);
                allocate.clear();
            }
            String sb2 = sb.toString();
            A00.close();
            return sb2;
        } catch (Throwable th) {
            A00.close();
            throw th;
        }
    }

    public static void A06(CharSequence charSequence, File file, Charset charset) {
        AnonymousClass901 r0 = new AnonymousClass901(new C192448zs(file, C192488zw.APPEND), charset);
        Preconditions.checkNotNull(charSequence);
        C12840q5 A00 = C12840q5.A00();
        try {
            Writer A002 = r0.A00();
            A00.A02(A002);
            A002.append(charSequence);
            A002.flush();
            A00.close();
        } catch (Throwable th) {
            A00.close();
            throw th;
        }
    }

    public static byte[] A08(File file) {
        return new C12820q3(file).A04();
    }

    public static String A02(String str) {
        Preconditions.checkNotNull(str);
        String name = new File(str).getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf == -1) {
            return BuildConfig.FLAVOR;
        }
        return name.substring(lastIndexOf + 1);
    }

    public static String A03(String str) {
        Preconditions.checkNotNull(str);
        String name = new File(str).getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf != -1) {
            return name.substring(0, lastIndexOf);
        }
        return name;
    }

    public static void A04(File file, File file2) {
        Preconditions.checkArgument(!file.equals(file2), "Source %s and destination %s must be different", file, file2);
        new C12820q3(file).A02(new C192448zs(file2, new C192488zw[0]));
    }

    public static void A05(File file, File file2) {
        Preconditions.checkNotNull(file);
        Preconditions.checkNotNull(file2);
        Preconditions.checkArgument(!file.equals(file2), "Source %s and destination %s must be different", file, file2);
        if (!file.renameTo(file2)) {
            A04(file, file2);
            if (file.delete()) {
                return;
            }
            if (!file2.delete()) {
                throw new IOException("Unable to delete " + file2);
            }
            throw new IOException("Unable to delete " + file);
        }
    }
}
