package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0Bd  reason: invalid class name and case insensitive filesystem */
public final class C01670Bd extends Enum {
    private static final /* synthetic */ C01670Bd[] A00;
    public static final C01670Bd A01;
    public static final C01670Bd A02;
    public static final C01670Bd A03;
    public static final C01670Bd A04;
    public static final C01670Bd A05;
    public static final C01670Bd A06;
    public static final C01670Bd A07;
    public static final C01670Bd A08;
    public static final C01670Bd A09;
    public static final C01670Bd A0A;
    public static final C01670Bd A0B;
    public static final C01670Bd A0C;
    public static final C01670Bd A0D;

    static {
        C01670Bd r2 = new C01670Bd("SUBSCRIBE_STARTED_GQLS", 0);
        A09 = r2;
        C01670Bd r3 = new C01670Bd("SUBSCRIBE_REACHED_LOCAL_CACHE", 1);
        A07 = r3;
        C01670Bd r4 = new C01670Bd("SUBSCRIBE_REACHED_CONNECTION_STREAM", 2);
        A06 = r4;
        C01670Bd r5 = new C01670Bd("SUBSCRIBE_RECEIVED_SERVER_ACK", 3);
        A08 = r5;
        C01670Bd r6 = new C01670Bd("UNSUBSCRIBE_STARTED_GQLS", 4);
        A0D = r6;
        C01670Bd r7 = new C01670Bd("UNSUBSCRIBE_REACHED_LOCAL_CACHE", 5);
        A0B = r7;
        C01670Bd r8 = new C01670Bd("UNSUBSCRIBE_REACHED_CONNECTION_STREAM", 6);
        A0A = r8;
        C01670Bd r9 = new C01670Bd("UNSUBSCRIBE_RECEIVED_SERVER_ACK", 7);
        A0C = r9;
        C01670Bd r10 = new C01670Bd("PUBLISH_RECEIVED_BY_MQTT", 8);
        A05 = r10;
        C01670Bd r11 = new C01670Bd("PUBLISH_DELIVERED_TO_GQLS", 9);
        A03 = r11;
        C01670Bd r12 = new C01670Bd("PUBLISH_FOUND_GRAPHQL_HANDLE", 10);
        A04 = r12;
        C01670Bd r13 = new C01670Bd("DISCONNECTED_WITH_PENDING_SUBSCRIBE", 11);
        A01 = r13;
        C01670Bd r14 = new C01670Bd("DISCONNECTED_WITH_PENDING_UNSUBSCRIBE", 12);
        A02 = r14;
        A00 = new C01670Bd[]{r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, new C01670Bd("UNKNOWN", 13)};
    }

    public static C01670Bd valueOf(String str) {
        return (C01670Bd) Enum.valueOf(C01670Bd.class, str);
    }

    public static C01670Bd[] values() {
        return (C01670Bd[]) A00.clone();
    }

    private C01670Bd(String str, int i) {
    }
}
