package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0O4  reason: invalid class name */
public final class AnonymousClass0O4 extends C03160Kg {
    public long A00() {
        return -153197510099727452L;
    }

    public void A01(AnonymousClass0FM r7, DataOutput dataOutput) {
        C02590Fn r72 = (C02590Fn) r7;
        dataOutput.writeLong(r72.heldTimeMs);
        dataOutput.writeLong(r72.acquiredCount);
        dataOutput.writeBoolean(r72.isAttributionEnabled);
        if (r72.isAttributionEnabled) {
            int size = r72.tagTimeMs.size();
            dataOutput.writeInt(size);
            for (int i = 0; i < size; i++) {
                AnonymousClass04b r0 = r72.tagTimeMs;
                String str = (String) r0.A07(i);
                long longValue = ((Long) r0.A09(i)).longValue();
                dataOutput.writeInt(str.length());
                dataOutput.writeChars(str);
                dataOutput.writeLong(longValue);
            }
        }
    }

    public boolean A03(AnonymousClass0FM r7, DataInput dataInput) {
        C02590Fn r72 = (C02590Fn) r7;
        r72.tagTimeMs.clear();
        r72.heldTimeMs = dataInput.readLong();
        r72.acquiredCount = dataInput.readLong();
        boolean readBoolean = dataInput.readBoolean();
        r72.isAttributionEnabled = readBoolean;
        if (!readBoolean) {
            return true;
        }
        int readInt = dataInput.readInt();
        for (int i = 0; i < readInt; i++) {
            int readInt2 = dataInput.readInt();
            StringBuilder sb = new StringBuilder();
            for (int i2 = 0; i2 < readInt2; i2++) {
                sb.append(dataInput.readChar());
            }
            r72.tagTimeMs.put(sb.toString(), Long.valueOf(dataInput.readLong()));
        }
        return true;
    }
}
