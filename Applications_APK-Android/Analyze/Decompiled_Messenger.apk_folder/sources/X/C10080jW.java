package X;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;

/* renamed from: X.0jW  reason: invalid class name and case insensitive filesystem */
public abstract class C10080jW {
    public abstract AnnotatedElement getAnnotated();

    public abstract Annotation getAnnotation(Class cls);

    public abstract Type getGenericType();

    public abstract String getName();

    public abstract Class getRawType();

    public C10030jR getType(C28311eb r3) {
        return r3._typeFactory._constructType(getGenericType(), r3);
    }

    public final boolean hasAnnotation(Class cls) {
        if (getAnnotation(cls) != null) {
            return true;
        }
        return false;
    }
}
