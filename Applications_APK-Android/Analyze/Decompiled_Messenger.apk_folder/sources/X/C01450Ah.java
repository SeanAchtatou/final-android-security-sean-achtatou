package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0Ah  reason: invalid class name and case insensitive filesystem */
public final class C01450Ah {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "analytics";
            case 2:
                return "fbns_notification_store";
            case 3:
                return "fbns_state";
            case 4:
                return "flags";
            case 5:
                return "ids";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                return "oxygen_fbns_config";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "registrations";
            case 8:
                return "retry";
            case Process.SIGKILL /*9*/:
                return "gk";
            case AnonymousClass1Y3.A01 /*10*/:
                return "mqtt_radio_active_time";
            case AnonymousClass1Y3.A02 /*11*/:
                return "token_store";
            case AnonymousClass1Y3.A03 /*12*/:
                return "runtime_params";
            case 13:
                return "mqtt_debug";
            case 14:
                return "mqtt_config";
            case 15:
                return "mqtt_last_host";
            default:
                return "address";
        }
    }

    public static boolean A01(Integer num) {
        switch (num.intValue()) {
            case 1:
            case 2:
            case 4:
            case 5:
                return true;
            case 3:
            default:
                return false;
        }
    }
}
