package X;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import com.facebook.acra.ACRA;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.google.common.base.Preconditions;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.1pb  reason: invalid class name and case insensitive filesystem */
public final class C34451pb {
    private static final IntentFilter A05 = new IntentFilter(TurboLoader.Locator.$const$string(57));
    private static volatile C34451pb A06;
    public Intent A00;
    private boolean A01 = false;
    private final Context A02;
    private final C14370tC A03;
    private final Object A04 = new Object();

    private synchronized Intent A00() {
        if (this.A01) {
            return this.A00;
        }
        try {
            Intent registerReceiver = this.A02.registerReceiver(new C630034g(this), A05);
            this.A00 = registerReceiver;
            this.A01 = true;
            return registerReceiver;
        } catch (SecurityException e) {
            String message = e.getMessage();
            if (message != null && message.contains("Unable to find app for caller")) {
                return null;
            }
            throw new RuntimeException(e);
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }

    public static final C34451pb A01(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (C34451pb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new C34451pb(AnonymousClass1YA.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public void A07(C101624ss r3) {
        synchronized (this.A04) {
            this.A03.A03(r3);
        }
    }

    public void A08(C101624ss r3, Handler handler) {
        synchronized (this.A04) {
            this.A03.A04(r3, handler);
        }
    }

    public C34451pb(Context context) {
        Preconditions.checkNotNull(context);
        this.A02 = context;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_LOW");
        intentFilter.addAction("android.intent.action.BATTERY_OKAY");
        intentFilter.addAction(TurboLoader.Locator.$const$string(31));
        intentFilter.addAction(TurboLoader.Locator.$const$string(32));
        this.A03 = new C33621ns(this.A02, new C14980uW(), intentFilter);
    }

    public float A02() {
        Intent A002 = A00();
        if (A002 == null) {
            return -1.0f;
        }
        int intExtra = A002.getIntExtra("level", -1);
        int intExtra2 = A002.getIntExtra("scale", -1);
        if (intExtra < 0 || intExtra2 < 0) {
            return -1.0f;
        }
        return ((float) intExtra) / ((float) intExtra2);
    }

    public float A03() {
        Intent A002 = A00();
        if (A002 == null) {
            return -1.0f;
        }
        return (float) A002.getIntExtra(AnonymousClass80H.$const$string(193), -1);
    }

    public Integer A04() {
        Intent A002 = A00();
        if (A002 != null) {
            int intExtra = A002.getIntExtra("status", 1);
            if (intExtra == 2) {
                int intExtra2 = A002.getIntExtra("plugged", -1);
                if (intExtra2 == 1) {
                    return AnonymousClass07B.A0Y;
                }
                if (intExtra2 == 2) {
                    return AnonymousClass07B.A0N;
                }
                if (intExtra2 == 4) {
                    return AnonymousClass07B.A0i;
                }
            } else if (intExtra == 3) {
                return AnonymousClass07B.A01;
            } else {
                if (intExtra == 4) {
                    return AnonymousClass07B.A0C;
                }
                if (intExtra == 5) {
                    return AnonymousClass07B.A0n;
                }
            }
        }
        return AnonymousClass07B.A00;
    }

    public Integer A05() {
        Intent A002 = A00();
        if (A002 != null) {
            switch (A002.getIntExtra("health", 1)) {
                case 2:
                    return AnonymousClass07B.A01;
                case 3:
                    return AnonymousClass07B.A0C;
                case 4:
                    return AnonymousClass07B.A0N;
                case 5:
                    return AnonymousClass07B.A0Y;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    return AnonymousClass07B.A0i;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    return AnonymousClass07B.A0n;
            }
        }
        return AnonymousClass07B.A00;
    }

    public Integer A06() {
        Intent A002 = A00();
        if (A002 == null) {
            return AnonymousClass07B.A00;
        }
        int intExtra = A002.getIntExtra("plugged", 0);
        if (intExtra == 1) {
            return AnonymousClass07B.A0N;
        }
        if (intExtra == 2) {
            return AnonymousClass07B.A0C;
        }
        if (intExtra != 4) {
            return AnonymousClass07B.A01;
        }
        return AnonymousClass07B.A0Y;
    }

    public boolean A09() {
        Intent A002 = A00();
        if (A002 == null) {
            return false;
        }
        int intExtra = A002.getIntExtra("status", 1);
        if (intExtra == 2 || intExtra == 5) {
            return true;
        }
        return false;
    }

    public boolean A0A(int i) {
        int intExtra;
        Intent A002 = A00();
        if (A002 == null || (intExtra = A002.getIntExtra("status", 1)) == 1 || intExtra == 2 || intExtra == 5) {
            return false;
        }
        int intExtra2 = A002.getIntExtra("level", -1);
        int intExtra3 = A002.getIntExtra("scale", -1);
        if (intExtra2 < 0 || intExtra3 < 0 || (((float) intExtra2) / ((float) intExtra3)) * 100.0f >= ((float) i)) {
            return false;
        }
        return true;
    }
}
