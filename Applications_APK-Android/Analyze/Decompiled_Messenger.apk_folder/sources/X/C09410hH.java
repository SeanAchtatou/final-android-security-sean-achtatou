package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0hH  reason: invalid class name and case insensitive filesystem */
public final class C09410hH extends AnonymousClass0Wl {
    private static volatile C09410hH A02;
    public final AnonymousClass0US A00;
    public final FbSharedPreferences A01;

    public String getSimpleName() {
        return "INeedInitForSharedPrefsListenerRegister";
    }

    public static final C09410hH A00(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (C09410hH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A02 = new C09410hH(AnonymousClass0UQ.A00(AnonymousClass1Y3.B5G, applicationInjector), FbSharedPreferencesModule.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C09410hH(AnonymousClass0US r1, FbSharedPreferences fbSharedPreferences) {
        this.A00 = r1;
        this.A01 = fbSharedPreferences;
    }

    /* JADX INFO: finally extract failed */
    public void init() {
        int A03 = C000700l.A03(-135327366);
        C005505z.A03("INeedInitForSharedPrefsListenerRegister-RegisterListeners", 1743351995);
        try {
            C005505z.A03("INeedInitForSharedPrefsListenerRegister-RegisterListeners#construct", 145617354);
            Set<C25941ae> set = (Set) this.A00.get();
            C005505z.A00(-1812863358);
            for (C25941ae r3 : set) {
                FbSharedPreferences fbSharedPreferences = this.A01;
                if (!r3.A03.getAndSet(true)) {
                    Set set2 = r3.A02;
                    if (set2 != null) {
                        fbSharedPreferences.C0h(set2, r3);
                    } else {
                        AnonymousClass1Y7 r0 = r3.A00;
                        if (r0 != null) {
                            fbSharedPreferences.C0f(r0, r3);
                        } else {
                            AnonymousClass1Y7 r02 = r3.A01;
                            if (r02 != null) {
                                fbSharedPreferences.C0i(r02, r3);
                            } else {
                                throw new IllegalArgumentException();
                            }
                        }
                    }
                }
            }
            C005505z.A00(1136494395);
            C000700l.A09(1417234428, A03);
        } catch (Throwable th) {
            C005505z.A00(-532309989);
            throw th;
        }
    }
}
