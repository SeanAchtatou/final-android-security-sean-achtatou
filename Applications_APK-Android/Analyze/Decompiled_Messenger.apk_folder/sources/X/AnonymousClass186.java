package X;

import android.content.Context;
import android.net.Uri;
import com.facebook.messaging.model.send.SendError;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.186  reason: invalid class name */
public final class AnonymousClass186 {
    private static final Class A01 = AnonymousClass186.class;
    private static volatile AnonymousClass186 A02;
    private final C04310Tq A00;

    public static final AnonymousClass186 A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass186.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass186(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private static String A01(AwN awN, String str) {
        return Uri.parse(C1060655e.A01).buildUpon().appendPath(String.valueOf(awN.download_fbid)).appendPath(str).build().toString();
    }

    private void A02(int i, AnonymousClass1TG r4) {
        if (r4.A0R.A02 == C36891u4.A07) {
            r4.A0y = ((Context) this.A00.get()).getResources().getString(i, r4.A0J.A03);
            r4.A0C = AnonymousClass1V7.A04;
            r4.A0R = SendError.A08;
            r4.A08 = new C159757aj().A00();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r23v0, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r7v5, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r21v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r4v16, types: [java.lang.String] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0218  */
    /* JADX WARNING: Removed duplicated region for block: B:87:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.AnonymousClass1TG r25, java.lang.String r26, X.C22339AwB r27) {
        /*
            r24 = this;
            r5 = r27
            r2 = r25
            if (r27 == 0) goto L_0x024e
            X.AnN r6 = r5.type
            if (r6 == 0) goto L_0x024e
            X.AnN r0 = X.C22046AnN.LINK_DELETED
            if (r6 == r0) goto L_0x024e
            X.AnN r0 = X.C22046AnN.PLAIN_TEXT     // Catch:{ IllegalStateException -> 0x0244 }
            r4 = 1
            if (r6 != r0) goto L_0x002e
            X.Avy r6 = r5.body     // Catch:{ IllegalStateException -> 0x0244 }
            if (r6 == 0) goto L_0x002c
            int r1 = r6.setField_     // Catch:{ IllegalStateException -> 0x0244 }
            r0 = 3
            if (r1 != r0) goto L_0x002c
        L_0x001c:
            com.google.common.base.Preconditions.checkState(r4)     // Catch:{ IllegalStateException -> 0x0244 }
            int r4 = r6.setField_     // Catch:{ IllegalStateException -> 0x0244 }
            r0 = 3
            if (r4 != r0) goto L_0x01df
            java.lang.Object r0 = r6.value_     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalStateException -> 0x0244 }
            r2.A0y = r0     // Catch:{ IllegalStateException -> 0x0244 }
            goto L_0x0214
        L_0x002c:
            r4 = 0
            goto L_0x001c
        L_0x002e:
            X.AnN r0 = X.C22046AnN.ATTACHMENT_INFO_LIST     // Catch:{ IllegalStateException -> 0x0244 }
            if (r6 != r0) goto L_0x016b
            X.Avy r3 = r5.body     // Catch:{ IllegalStateException -> 0x0244 }
            if (r3 == 0) goto L_0x0163
            int r1 = r3.setField_     // Catch:{ IllegalStateException -> 0x0244 }
            r0 = 4
            if (r1 != r0) goto L_0x0163
        L_0x003b:
            com.google.common.base.Preconditions.checkState(r4)     // Catch:{ IllegalStateException -> 0x0244 }
            java.util.List r0 = r3.A09()     // Catch:{ IllegalStateException -> 0x0244 }
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ IllegalStateException -> 0x0244 }
            r8.<init>()     // Catch:{ IllegalStateException -> 0x0244 }
            java.util.Iterator r13 = r0.iterator()     // Catch:{ IllegalStateException -> 0x0244 }
        L_0x004b:
            boolean r0 = r13.hasNext()     // Catch:{ IllegalStateException -> 0x0244 }
            if (r0 == 0) goto L_0x0166
            java.lang.Object r0 = r13.next()     // Catch:{ IllegalStateException -> 0x0244 }
            X.AwN r0 = (X.AwN) r0     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Long r1 = r0.download_fbid     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r3 = java.lang.String.valueOf(r1)     // Catch:{ IllegalStateException -> 0x0244 }
            X.0w6 r1 = new X.0w6     // Catch:{ IllegalStateException -> 0x0244 }
            r6 = r26
            r1.<init>(r3, r6)     // Catch:{ IllegalStateException -> 0x0244 }
            r1.A06 = r3     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r3 = r0.suggested_file_name     // Catch:{ IllegalStateException -> 0x0244 }
            r1.A07 = r3     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Long r3 = r0.download_size_bytes     // Catch:{ IllegalStateException -> 0x0244 }
            long r3 = r3.longValue()     // Catch:{ IllegalStateException -> 0x0244 }
            int r3 = X.C06950cM.A00(r3)     // Catch:{ IllegalStateException -> 0x0244 }
            r1.A00 = r3     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r3 = r0.file_mime_type     // Catch:{ IllegalStateException -> 0x0244 }
            r1.A09 = r3     // Catch:{ IllegalStateException -> 0x0244 }
            X.Awf r9 = r0.image_metadata     // Catch:{ IllegalStateException -> 0x0244 }
            r4 = 0
            if (r9 == 0) goto L_0x00d9
            byte[] r7 = r0.thumbnail_data     // Catch:{ IllegalStateException -> 0x0244 }
            if (r7 == 0) goto L_0x0088
            r3 = 0
            java.lang.String r4 = android.util.Base64.encodeToString(r7, r3)     // Catch:{ IllegalStateException -> 0x0244 }
        L_0x0088:
            com.facebook.messaging.model.attachment.ImageData r14 = new com.facebook.messaging.model.attachment.ImageData     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r9.width     // Catch:{ IllegalStateException -> 0x0244 }
            int r15 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r9.height     // Catch:{ IllegalStateException -> 0x0244 }
            int r16 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            X.Awf r7 = r0.image_metadata     // Catch:{ IllegalStateException -> 0x0244 }
            X.1TT r9 = new X.1TT     // Catch:{ IllegalStateException -> 0x0244 }
            r9.<init>()     // Catch:{ IllegalStateException -> 0x0244 }
            X.2Uv r10 = X.C47422Uv.A02     // Catch:{ IllegalStateException -> 0x0244 }
            X.2Uw r11 = new X.2Uw     // Catch:{ IllegalStateException -> 0x0244 }
            r11.<init>()     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r3 = A01(r0, r6)     // Catch:{ IllegalStateException -> 0x0244 }
            r11.A02 = r3     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r7.width     // Catch:{ IllegalStateException -> 0x0244 }
            int r3 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            r11.A01 = r3     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r7.height     // Catch:{ IllegalStateException -> 0x0244 }
            int r3 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            r11.A00 = r3     // Catch:{ IllegalStateException -> 0x0244 }
            com.facebook.messaging.model.attachment.ImageUrl r7 = new com.facebook.messaging.model.attachment.ImageUrl     // Catch:{ IllegalStateException -> 0x0244 }
            r7.<init>(r11)     // Catch:{ IllegalStateException -> 0x0244 }
            java.util.Map r3 = r9.A01     // Catch:{ IllegalStateException -> 0x0244 }
            r3.put(r10, r7)     // Catch:{ IllegalStateException -> 0x0244 }
            com.facebook.messaging.model.attachment.AttachmentImageMap r3 = new com.facebook.messaging.model.attachment.AttachmentImageMap     // Catch:{ IllegalStateException -> 0x0244 }
            r3.<init>(r9)     // Catch:{ IllegalStateException -> 0x0244 }
            r18 = 0
            X.2Ur r19 = X.C47392Ur.NONQUICKCAM     // Catch:{ IllegalStateException -> 0x0244 }
            r20 = 0
            r22 = 0
            r21 = r4
            r17 = r3
            r14.<init>(r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ IllegalStateException -> 0x0244 }
            r4 = r14
        L_0x00d9:
            r1.A03 = r4     // Catch:{ IllegalStateException -> 0x0244 }
            X.Awg r4 = r0.video_metadata     // Catch:{ IllegalStateException -> 0x0244 }
            r7 = 0
            if (r4 == 0) goto L_0x011e
            byte[] r9 = r0.thumbnail_data     // Catch:{ IllegalStateException -> 0x0244 }
            if (r9 == 0) goto L_0x00e9
            r3 = 0
            java.lang.String r7 = android.util.Base64.encodeToString(r9, r3)     // Catch:{ IllegalStateException -> 0x0244 }
        L_0x00e9:
            com.facebook.messaging.model.attachment.VideoData r14 = new com.facebook.messaging.model.attachment.VideoData     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r4.width     // Catch:{ IllegalStateException -> 0x0244 }
            int r15 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r4.height     // Catch:{ IllegalStateException -> 0x0244 }
            int r16 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r4.rotation     // Catch:{ IllegalStateException -> 0x0244 }
            int r17 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r4.duration_ms     // Catch:{ IllegalStateException -> 0x0244 }
            int r3 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            long r3 = (long) r3     // Catch:{ IllegalStateException -> 0x0244 }
            r9 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r9
            int r9 = (int) r3     // Catch:{ IllegalStateException -> 0x0244 }
            r19 = 0
            X.2VB r20 = X.AnonymousClass2VB.VIDEO_ATTACHMENT     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r3 = A01(r0, r6)     // Catch:{ IllegalStateException -> 0x0244 }
            android.net.Uri r21 = android.net.Uri.parse(r3)     // Catch:{ IllegalStateException -> 0x0244 }
            r22 = 0
            r23 = r7
            r18 = r9
            r14.<init>(r15, r16, r17, r18, r19, r20, r21, r22, r23)     // Catch:{ IllegalStateException -> 0x0244 }
            r7 = r14
        L_0x011e:
            r1.A04 = r7     // Catch:{ IllegalStateException -> 0x0244 }
            X.Awh r4 = r0.audio_metadata     // Catch:{ IllegalStateException -> 0x0244 }
            if (r4 != 0) goto L_0x0126
            r12 = 0
            goto L_0x0146
        L_0x0126:
            com.facebook.messaging.model.attachment.AudioData r12 = new com.facebook.messaging.model.attachment.AudioData     // Catch:{ IllegalStateException -> 0x0244 }
            r15 = 0
            java.lang.String r3 = A01(r0, r6)     // Catch:{ IllegalStateException -> 0x0244 }
            android.net.Uri r17 = android.net.Uri.parse(r3)     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Integer r3 = r4.duration_ms     // Catch:{ IllegalStateException -> 0x0244 }
            int r11 = r3.intValue()     // Catch:{ IllegalStateException -> 0x0244 }
            long r3 = (long) r11     // Catch:{ IllegalStateException -> 0x0244 }
            r9 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r9
            int r7 = (int) r3     // Catch:{ IllegalStateException -> 0x0244 }
            r14 = r12
            r16 = r6
            r18 = r7
            r19 = r11
            r14.<init>(r15, r16, r17, r18, r19)     // Catch:{ IllegalStateException -> 0x0244 }
        L_0x0146:
            r1.A02 = r12     // Catch:{ IllegalStateException -> 0x0244 }
            byte[] r4 = r0.secret_key     // Catch:{ IllegalStateException -> 0x0244 }
            r3 = 0
            java.lang.String r3 = android.util.Base64.encodeToString(r4, r3)     // Catch:{ IllegalStateException -> 0x0244 }
            r1.A05 = r3     // Catch:{ IllegalStateException -> 0x0244 }
            byte[] r3 = r0.download_hash     // Catch:{ IllegalStateException -> 0x0244 }
            r1.A0B = r3     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = r0.download_mac     // Catch:{ IllegalStateException -> 0x0244 }
            r1.A08 = r0     // Catch:{ IllegalStateException -> 0x0244 }
            com.facebook.messaging.model.attachment.Attachment r0 = new com.facebook.messaging.model.attachment.Attachment     // Catch:{ IllegalStateException -> 0x0244 }
            r0.<init>(r1)     // Catch:{ IllegalStateException -> 0x0244 }
            r8.add(r0)     // Catch:{ IllegalStateException -> 0x0244 }
            goto L_0x004b
        L_0x0163:
            r4 = 0
            goto L_0x003b
        L_0x0166:
            r2.A08(r8)     // Catch:{ IllegalStateException -> 0x0244 }
            goto L_0x0214
        L_0x016b:
            X.AnN r0 = X.C22046AnN.STICKER_INFO     // Catch:{ IllegalStateException -> 0x0244 }
            if (r6 != r0) goto L_0x01a2
            X.Avy r6 = r5.body     // Catch:{ IllegalStateException -> 0x0244 }
            if (r6 == 0) goto L_0x018e
            int r1 = r6.setField_     // Catch:{ IllegalStateException -> 0x0244 }
            r0 = 6
            if (r1 != r0) goto L_0x018e
        L_0x0178:
            com.google.common.base.Preconditions.checkState(r4)     // Catch:{ IllegalStateException -> 0x0244 }
            int r4 = r6.setField_     // Catch:{ IllegalStateException -> 0x0244 }
            r0 = 6
            if (r4 != r0) goto L_0x0190
            java.lang.Object r0 = r6.value_     // Catch:{ IllegalStateException -> 0x0244 }
            X.Awe r0 = (X.C22359Awe) r0     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.Long r0 = r0.fbid     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ IllegalStateException -> 0x0244 }
            r2.A0x = r0     // Catch:{ IllegalStateException -> 0x0244 }
            goto L_0x0214
        L_0x018e:
            r4 = 0
            goto L_0x0178
        L_0x0190:
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r1 = "Cannot get field 'sticker_info' because union is currently set to "
            X.1sk r0 = r6.getFieldDesc(r4)     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = r0.A01     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ IllegalStateException -> 0x0244 }
            r3.<init>(r0)     // Catch:{ IllegalStateException -> 0x0244 }
            goto L_0x01f0
        L_0x01a2:
            X.AnN r0 = X.C22046AnN.DEVICE_LOCAL_TEXT     // Catch:{ IllegalStateException -> 0x0244 }
            if (r6 != r0) goto L_0x01f1
            X.Avy r6 = r5.body     // Catch:{ IllegalStateException -> 0x0244 }
            if (r6 != 0) goto L_0x01ab
            r4 = 0
        L_0x01ab:
            com.google.common.base.Preconditions.checkState(r4)     // Catch:{ IllegalStateException -> 0x0244 }
            int r4 = r6.setField_     // Catch:{ IllegalStateException -> 0x0244 }
            r0 = 5
            if (r4 != r0) goto L_0x01cd
            java.lang.Object r0 = r6.value_     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalStateException -> 0x0244 }
            r2.A0y = r0     // Catch:{ IllegalStateException -> 0x0244 }
            X.1V7 r0 = X.AnonymousClass1V7.A04     // Catch:{ IllegalStateException -> 0x0244 }
            r2.A0C = r0     // Catch:{ IllegalStateException -> 0x0244 }
            com.facebook.messaging.model.send.SendError r0 = com.facebook.messaging.model.send.SendError.A08     // Catch:{ IllegalStateException -> 0x0244 }
            r2.A0R = r0     // Catch:{ IllegalStateException -> 0x0244 }
            X.7aj r0 = new X.7aj     // Catch:{ IllegalStateException -> 0x0244 }
            r0.<init>()     // Catch:{ IllegalStateException -> 0x0244 }
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r0 = r0.A00()     // Catch:{ IllegalStateException -> 0x0244 }
            r2.A08 = r0     // Catch:{ IllegalStateException -> 0x0244 }
            goto L_0x0214
        L_0x01cd:
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r1 = "Cannot get field 'device_local_text' because union is currently set to "
            X.1sk r0 = r6.getFieldDesc(r4)     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = r0.A01     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ IllegalStateException -> 0x0244 }
            r3.<init>(r0)     // Catch:{ IllegalStateException -> 0x0244 }
            goto L_0x01f0
        L_0x01df:
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r1 = "Cannot get field 'plain_text' because union is currently set to "
            X.1sk r0 = r6.getFieldDesc(r4)     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = r0.A01     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ IllegalStateException -> 0x0244 }
            r3.<init>(r0)     // Catch:{ IllegalStateException -> 0x0244 }
        L_0x01f0:
            throw r3     // Catch:{ IllegalStateException -> 0x0244 }
        L_0x01f1:
            X.AnN r0 = X.C22046AnN.THREAD_SENDER_KEY     // Catch:{ IllegalStateException -> 0x0244 }
            if (r6 != r0) goto L_0x0201
            java.lang.Class r3 = X.AnonymousClass186.A01     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r1 = "Encontered sender key in database"
            java.lang.Object[] r0 = new java.lang.Object[]{r6}     // Catch:{ IllegalStateException -> 0x0244 }
            X.C010708t.A0C(r3, r1, r0)     // Catch:{ IllegalStateException -> 0x0244 }
            goto L_0x0214
        L_0x0201:
            java.lang.Class r3 = X.AnonymousClass186.A01     // Catch:{ IllegalStateException -> 0x0244 }
            java.lang.String r1 = "Received unknown salamander of type %d"
            java.lang.Object[] r0 = new java.lang.Object[]{r6}     // Catch:{ IllegalStateException -> 0x0244 }
            X.C010708t.A0B(r3, r1, r0)     // Catch:{ IllegalStateException -> 0x0244 }
            r1 = r24
            r0 = 2131821340(0x7f11031c, float:1.927542E38)
            r1.A02(r0, r2)     // Catch:{ IllegalStateException -> 0x0244 }
        L_0x0214:
            java.lang.Long r0 = r5.ephemeral_lifetime_micros
            if (r0 == 0) goto L_0x0243
            long r5 = r0.longValue()
            r3 = 0
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0243
            r0 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r0
            r0 = 2147483647(0x7fffffff, double:1.060997895E-314)
            long r0 = java.lang.Math.min(r5, r0)
            long r3 = java.lang.Math.max(r0, r3)
            int r0 = (int) r3
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.A0i = r0
            java.lang.Long r0 = r2.A0j
            if (r0 != 0) goto L_0x0243
            r0 = -1
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.A0j = r0
        L_0x0243:
            return
        L_0x0244:
            r3 = move-exception
            r1 = r24
            r0 = 2131821298(0x7f1102f2, float:1.9275335E38)
            r1.A02(r0, r2)
            throw r3
        L_0x024e:
            r1 = r24
            r0 = 2131821298(0x7f1102f2, float:1.9275335E38)
            r1.A02(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass186.A03(X.1TG, java.lang.String, X.AwB):void");
    }

    private AnonymousClass186(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0VG.A00(AnonymousClass1Y3.BCt, r2);
    }
}
