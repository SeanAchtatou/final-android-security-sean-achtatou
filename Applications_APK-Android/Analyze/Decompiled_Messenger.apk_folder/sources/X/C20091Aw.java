package X;

import android.content.Context;
import android.widget.Toast;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.business.inboxads.common.InboxAdsItem;
import com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo;
import com.google.common.collect.ImmutableList;

@UserScoped
/* renamed from: X.1Aw  reason: invalid class name and case insensitive filesystem */
public final class C20091Aw {
    private static C05540Zi A07;
    public AnonymousClass0UN A00;
    public boolean A01;
    public final C17610zB A02 = new C17610zB();
    public final C17610zB A03 = new C17610zB();
    public final C17610zB A04 = new C17610zB();
    public final AnonymousClass069 A05;
    public final C14500tS A06;

    private void A02(C74283hZ r5, boolean z) {
        String str;
        String str2;
        if (z && r5.A09) {
            r5.A09 = false;
            r5.A04 = this.A05.now();
            this.A06.A03(r5);
            if (((C88014Ib) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Acf, this.A00)).A01().booleanValue()) {
                ImmutableList immutableList = r5.A05;
                if (immutableList != null) {
                    str2 = ((InboxAdsMediaInfo) immutableList.get(0)).A0C;
                } else {
                    str2 = "null ad title";
                }
                A03(this, str2, "Impression Start", C74123hJ.A00(r5.A06));
            }
        }
        if (!z && !r5.A09) {
            this.A06.A04(r5);
            r5.A09 = true;
            if (((C88014Ib) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Acf, this.A00)).A01().booleanValue()) {
                ImmutableList immutableList2 = r5.A05;
                if (immutableList2 != null) {
                    str = ((InboxAdsMediaInfo) immutableList2.get(0)).A0C;
                } else {
                    str = "null ad title";
                }
                A03(this, str, "Impression Stop", C74123hJ.A00(r5.A06));
            }
        }
        ImmutableList immutableList3 = r5.A05;
        if (immutableList3 != null) {
            C24971Xv it = immutableList3.iterator();
            while (it.hasNext()) {
                A04(((C36031sD) it.next()).AcG(), z);
            }
        }
    }

    public static final C20091Aw A00(AnonymousClass1XY r4) {
        C20091Aw r0;
        synchronized (C20091Aw.class) {
            C05540Zi A002 = C05540Zi.A00(A07);
            A07 = A002;
            try {
                if (A002.A03(r4)) {
                    A07.A00 = new C20091Aw((AnonymousClass1XY) A07.A01());
                }
                C05540Zi r1 = A07;
                r0 = (C20091Aw) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A07.A02();
                throw th;
            }
        }
        return r0;
    }

    public static String A01(C36031sD r1) {
        if (r1 instanceof InboxAdsItem) {
            return ((InboxAdsItem) r1).A00.A0I;
        }
        if (r1 instanceof InboxAdsMediaInfo) {
            return ((InboxAdsMediaInfo) r1).A0I;
        }
        return "UNDEFINED";
    }

    public static void A03(C20091Aw r3, String str, String str2, String str3) {
        Toast.makeText((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r3.A00), AnonymousClass08S.A0T(str3, " ", str2, "\n", str), 0).show();
    }

    public void A04(long j, boolean z) {
        String str;
        String str2;
        C74283hZ r4 = (C74283hZ) this.A02.A07(j);
        if (r4 != null) {
            String $const$string = AnonymousClass24B.$const$string(1042);
            if (z && r4.A09) {
                r4.A09 = false;
                r4.A04 = this.A05.now();
                this.A06.A02(r4);
                if (((C88014Ib) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Acf, this.A00)).A01().booleanValue()) {
                    ImmutableList immutableList = r4.A05;
                    if (immutableList != null) {
                        str2 = ((InboxAdsMediaInfo) immutableList.get(0)).A0C;
                    } else {
                        str2 = "null ad title";
                    }
                    A03(this, str2, "Impression Start", $const$string);
                }
            }
            if (!z && !r4.A09) {
                this.A06.A05(r4);
                r4.A09 = true;
                if (((C88014Ib) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Acf, this.A00)).A01().booleanValue()) {
                    ImmutableList immutableList2 = r4.A05;
                    if (immutableList2 != null) {
                        str = ((InboxAdsMediaInfo) immutableList2.get(0)).A0C;
                    } else {
                        str = "null ad title";
                    }
                    A03(this, str, "Impression Stop", $const$string);
                }
            }
        }
        C74283hZ r0 = (C74283hZ) this.A04.A07(j);
        if (r0 != null) {
            A02(r0, z);
        }
        C74283hZ r02 = (C74283hZ) this.A03.A07(j);
        if (r02 != null) {
            A02(r02, z);
        }
    }

    private C20091Aw(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A06 = C14500tS.A00(r3);
        this.A05 = AnonymousClass067.A03(r3);
    }
}
