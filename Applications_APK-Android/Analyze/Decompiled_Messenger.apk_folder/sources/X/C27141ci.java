package X;

import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

/* renamed from: X.1ci  reason: invalid class name and case insensitive filesystem */
public final class C27141ci extends ContentObserver {
    public final /* synthetic */ C10960l9 A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C27141ci(C10960l9 r1, Handler handler) {
        super(handler);
        this.A00 = r1;
    }

    public void onChange(boolean z, Uri uri) {
        super.onChange(z);
        this.A00.A07();
        ((C04460Ut) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AKb, this.A00.A01)).C4x(new Intent(C06680bu.A0s));
    }
}
