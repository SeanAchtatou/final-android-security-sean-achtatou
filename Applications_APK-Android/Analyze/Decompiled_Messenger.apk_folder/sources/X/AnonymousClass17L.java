package X;

import android.database.sqlite.SQLiteDatabase;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.17L  reason: invalid class name */
public final class AnonymousClass17L extends AnonymousClass0W2 {
    private static volatile AnonymousClass17L A00;

    public AnonymousClass17L() {
        super(AnonymousClass80H.$const$string(58), 2);
    }

    public static final AnonymousClass17L A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass17L.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass17L();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public void A08(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        C007406x.A00(-538941706);
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS phone_address_book_snapshot");
        C007406x.A00(1323068826);
        A04(sQLiteDatabase);
    }
}
