package X;

import com.facebook.profilo.logger.Logger;
import java.util.concurrent.Callable;

/* renamed from: X.0NL  reason: invalid class name */
public final class AnonymousClass0NL implements Callable, AnonymousClass09S {
    private int A00;
    private int A01;
    private Callable A02;

    public Object call() {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 14, 0, 0, this.A00, this.A01, 0);
        try {
            return this.A02.call();
        } finally {
            Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 15, 0, 0, this.A00, writeStandardEntry, 0);
        }
    }

    public String toString() {
        return "CallableWrapper for " + this.A02;
    }

    public AnonymousClass0NL(Callable callable, int i, int i2) {
        this.A02 = callable;
        this.A01 = i;
        this.A00 = i2;
    }

    public Object getInnerRunnable() {
        return this.A02;
    }
}
