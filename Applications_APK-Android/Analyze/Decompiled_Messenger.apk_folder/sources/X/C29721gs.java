package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.profilepic.PicSquareUrlWithSize;

/* renamed from: X.1gs  reason: invalid class name and case insensitive filesystem */
public final class C29721gs implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new PicSquareUrlWithSize(parcel);
    }

    public Object[] newArray(int i) {
        return new PicSquareUrlWithSize[i];
    }
}
