package X;

import android.app.NotificationChannel;

/* renamed from: X.0uZ  reason: invalid class name and case insensitive filesystem */
public final class C15010uZ {
    public static final String A00 = "MessengerNotificationChannelHelper";
    public static final long[] A01 = {0, 250, 250, 250};
    public static final long[] A02 = {0, 150, 150, 350};
    public static final long[] A03 = {0, 200, 200, 200};
    public static final long[] A04 = {0, 100};

    public static long[] A02(AnonymousClass16F r0) {
        if (r0 == null) {
            return null;
        }
        switch (r0.ordinal()) {
            case 0:
                return A01;
            case 1:
                return A04;
            case 2:
                return A03;
            case 3:
                return A02;
            default:
                return null;
        }
    }

    public static int A00(NotificationChannel notificationChannel) {
        int i = 0;
        if (notificationChannel.getImportance() != 0) {
            i = 2;
        }
        if (notificationChannel.getImportance() >= 4) {
            i |= 4;
        }
        if (notificationChannel.getSound() != null && notificationChannel.getImportance() >= 3) {
            i |= 16;
        }
        if (notificationChannel.shouldVibrate() && notificationChannel.getImportance() >= 3) {
            i |= 32;
        }
        if (!notificationChannel.canShowBadge() || notificationChannel.getImportance() == 0) {
            return i;
        }
        return i | 64;
    }

    public static String A01(String str) {
        int indexOf;
        if (C06850cB.A0B(str) || (indexOf = str.indexOf(64)) < 0) {
            return str;
        }
        return str.substring(0, indexOf);
    }
}
