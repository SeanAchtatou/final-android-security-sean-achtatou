package X;

/* renamed from: X.0an  reason: invalid class name and case insensitive filesystem */
public final class C06070an {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("before_threads", "INTEGER");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6("id", "TEXT");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("pos", "INTEGER");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("service_subkey", "TEXT");
    public static final AnonymousClass0W6 A04 = new AnonymousClass0W6("service_tertiary_key", "TEXT");
    public static final AnonymousClass0W6 A05 = new AnonymousClass0W6("service_type", "TEXT");
    public static final AnonymousClass0W6 A06 = new AnonymousClass0W6("tree_blob", "TEXT");
    public static final AnonymousClass0W6 A07 = new AnonymousClass0W6("type", "TEXT");
    public static final AnonymousClass0W6 A08 = new AnonymousClass0W6("update_ts_ms", "INTEGER");
}
