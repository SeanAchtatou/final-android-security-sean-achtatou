package X;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;

/* renamed from: X.0QI  reason: invalid class name */
public final class AnonymousClass0QI {
    static {
        C29981hI.A02 = true;
    }

    public static Location A00(LocationManager locationManager, String str) {
        Location location;
        if (!C29981hI.A00()) {
            return locationManager.getLastKnownLocation(str);
        }
        try {
            C29981hI.A01.readLock().lock();
            C29991hJ r2 = C29981hI.A00;
            if (r2.A02 && C25891aZ.A02(r2.A00)) {
                C25891aZ.A01(r2.A00, C25891aZ.A01);
            }
            if (!r2.A01 || !C25891aZ.A02(r2.A00)) {
                location = locationManager.getLastKnownLocation(str);
            } else {
                location = null;
            }
            return location;
        } finally {
            C29981hI.A01.readLock().unlock();
        }
    }

    public static void A01(LocationManager locationManager, LocationListener locationListener) {
        if (C29981hI.A00()) {
            try {
                C29981hI.A01.readLock().lock();
                C29991hJ r1 = C29981hI.A00;
                if (r1 != null) {
                    if (r1.A02 && C25891aZ.A02(r1.A00)) {
                        C25891aZ.A01(r1.A00, C25891aZ.A02);
                    }
                    locationManager.removeUpdates(locationListener);
                }
            } finally {
                C29981hI.A01.readLock().unlock();
            }
        } else {
            locationManager.removeUpdates(locationListener);
        }
    }

    public static void A02(LocationManager locationManager, String str, long j, float f, LocationListener locationListener) {
        if (C29981hI.A00()) {
            try {
                C29981hI.A01.readLock().lock();
                C29991hJ r2 = C29981hI.A00;
                if (r2 != null) {
                    if (r2.A02 && C25891aZ.A02(r2.A00)) {
                        C25891aZ.A01(r2.A00, C25891aZ.A03);
                    }
                    if (!r2.A01 || !C25891aZ.A02(r2.A00)) {
                        locationManager.requestLocationUpdates(str, j, f, locationListener);
                    }
                }
            } finally {
                C29981hI.A01.readLock().unlock();
            }
        } else {
            locationManager.requestLocationUpdates(str, j, f, locationListener);
        }
    }

    public static void A03(LocationManager locationManager, String str, long j, float f, LocationListener locationListener, Looper looper) {
        if (C29981hI.A00()) {
            try {
                C29981hI.A01.readLock().lock();
                C29991hJ r2 = C29981hI.A00;
                if (r2 != null) {
                    if (r2.A02 && C25891aZ.A02(r2.A00)) {
                        C25891aZ.A01(r2.A00, C25891aZ.A03);
                    }
                    if (!r2.A01 || !C25891aZ.A02(r2.A00)) {
                        locationManager.requestLocationUpdates(str, j, f, locationListener, looper);
                    }
                }
            } finally {
                C29981hI.A01.readLock().unlock();
            }
        } else {
            locationManager.requestLocationUpdates(str, j, f, locationListener, looper);
        }
    }
}
