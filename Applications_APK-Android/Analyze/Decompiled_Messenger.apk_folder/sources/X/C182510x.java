package X;

import android.content.Context;
import androidx.fragment.app.Fragment;
import com.facebook.messaging.montage.omnistore.storedprocedures.MontageNewsfeedRefreshStoredProcedureComponent;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.10x  reason: invalid class name and case insensitive filesystem */
public final class C182510x implements AnonymousClass13r {
    private static volatile C182510x A04;
    private AnonymousClass0UN A00;
    private final C16510xE A01;
    private final C17350yl A02;
    private final MontageNewsfeedRefreshStoredProcedureComponent A03;

    public static final C182510x A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C182510x.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C182510x(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public /* bridge */ /* synthetic */ void AP7(Context context, Fragment fragment, C33691nz r4) {
        ((C73953h2) fragment).mListener = new C107175Bc(r4);
    }

    public Fragment AV6() {
        return new C73953h2();
    }

    public C15390vD Ae6() {
        return (C15390vD) AnonymousClass1XX.A03(AnonymousClass1Y3.ACr, this.A00);
    }

    public Class AiR() {
        return C73953h2.class;
    }

    public AnonymousClass10Z B5D(Context context) {
        return new AnonymousClass10Z(AnonymousClass01R.A03(context, ((AnonymousClass1MT) AnonymousClass1XX.A03(AnonymousClass1Y3.AJa, this.A00)).A03(C29631gj.A0y, AnonymousClass07B.A0N)), 22, -1, 28, -1);
    }

    public String B5E(Context context) {
        if (this.A01.A0C()) {
            return context.getString(2131823830);
        }
        return context.getString(2131823828);
    }

    public String B6U(Context context) {
        if (this.A01.A0C()) {
            return context.getString(2131834125);
        }
        return context.getString(2131834122);
    }

    public ImmutableList B6e() {
        return ((C32591m0) AnonymousClass1XX.A03(AnonymousClass1Y3.AF9, this.A00)).A01();
    }

    public boolean BGe(Fragment fragment) {
        C73953h2 r3 = (C73953h2) fragment;
        AnonymousClass5QI r0 = (AnonymousClass5QI) r3.A0Y.get(r3.A07);
        if (r0 == null) {
            return true;
        }
        return r0.A05;
    }

    public void BST(boolean z) {
        if (this.A02.A0F() && !z) {
            this.A03.A01();
        }
    }

    public void C4d(Fragment fragment) {
        C73953h2 r4 = (C73953h2) fragment;
        AnonymousClass5QI r0 = (AnonymousClass5QI) r4.A0Y.get(r4.A07);
        if (r0 != null) {
            r0.A01.A0A.A01(0, true);
        }
    }

    public void CBh(Fragment fragment, C33761o6 r3) {
        ((C73953h2) fragment).A09 = new C110985Qo(r3);
    }

    private C182510x(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A02 = C17350yl.A00(r3);
        this.A01 = C16510xE.A00(r3);
        this.A03 = MontageNewsfeedRefreshStoredProcedureComponent.A00(r3);
    }
}
