package X;

import com.facebook.graphql.query.GraphQlQueryParamSet;
import com.google.common.base.Preconditions;
import java.util.concurrent.Callable;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0mJ  reason: invalid class name */
public final class AnonymousClass0mJ {
    public static final Callable A01 = new C11190mK();
    private static volatile AnonymousClass0mJ A02;
    public AnonymousClass0UN A00;

    public static final AnonymousClass0mJ A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass0mJ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass0mJ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private AnonymousClass0mJ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }

    public static final AnonymousClass0mJ A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public static void A02(GraphQlQueryParamSet graphQlQueryParamSet, String str, String str2, Callable callable) {
        C12240os r2;
        Preconditions.checkNotNull(str);
        if (graphQlQueryParamSet != null && (r2 = graphQlQueryParamSet.A00.A00) != null) {
            boolean z = false;
            int i = 0;
            while (i < r2.A00) {
                if (r2.A0H(i).equals(str)) {
                    Object A0G = r2.A0G(i);
                    if (A0G instanceof C12240os) {
                        C12240os r22 = (C12240os) A0G;
                        int i2 = 0;
                        while (true) {
                            if (i2 >= r22.A00) {
                                break;
                            } else if (r22.A0H(i2).equals(str2)) {
                                z = true;
                                break;
                            } else {
                                i2++;
                            }
                        }
                        if (!z) {
                            try {
                                String str3 = (String) callable.call();
                                if (str3 != null) {
                                    r22.A0K(str2, str3);
                                    return;
                                }
                                return;
                            } catch (Exception unused) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    i++;
                }
            }
        }
    }
}
