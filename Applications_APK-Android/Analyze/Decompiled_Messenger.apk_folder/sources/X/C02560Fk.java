package X;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.CharBuffer;
import java.util.NoSuchElementException;

/* renamed from: X.0Fk  reason: invalid class name and case insensitive filesystem */
public final class C02560Fk {
    public char A00;
    public boolean A01 = true;
    private char A02;
    private int A03;
    private int A04 = -1;
    private RandomAccessFile A05;
    private boolean A06 = false;
    private final String A07;
    private final byte[] A08;

    public void A04() {
        this.A01 = true;
        RandomAccessFile randomAccessFile = this.A05;
        if (randomAccessFile != null) {
            try {
                randomAccessFile.seek(0);
            } catch (IOException unused) {
                A00();
            }
        }
        if (this.A05 == null) {
            try {
                this.A05 = new RandomAccessFile(this.A07, "r");
            } catch (IOException unused2) {
                this.A01 = false;
                A00();
            }
        }
        if (this.A01) {
            this.A04 = -1;
            this.A03 = 0;
            this.A00 = 0;
            this.A02 = 0;
            this.A06 = false;
        }
    }

    private void A00() {
        RandomAccessFile randomAccessFile = this.A05;
        if (randomAccessFile != null) {
            try {
                randomAccessFile.close();
            } catch (IOException unused) {
            } catch (Throwable th) {
                this.A05 = null;
                throw th;
            }
            this.A05 = null;
        }
    }

    private void A02() {
        if (!this.A06) {
            this.A04--;
            this.A00 = this.A02;
            this.A06 = true;
            return;
        }
        throw new AnonymousClass0KX("Can only rewind one step!");
    }

    public long A03() {
        long j = 1;
        long j2 = 0;
        boolean z = true;
        while (true) {
            if (!A08()) {
                break;
            }
            A01();
            if (!Character.isDigit(this.A00)) {
                if (!z) {
                    A02();
                    break;
                } else if (this.A00 == '-') {
                    j = -1;
                } else {
                    throw new AnonymousClass0KX("Couldn't read number!");
                }
            } else {
                j2 = (j2 * 10) + ((long) (this.A00 - '0'));
            }
            z = false;
        }
        if (!z) {
            return j * j2;
        }
        throw new AnonymousClass0KX("Couldn't read number because the file ended!");
    }

    public void A05() {
        boolean z = false;
        while (A08()) {
            A01();
            if (this.A00 == 10) {
                z = true;
            } else if (z) {
                A02();
                return;
            }
        }
    }

    public void A06() {
        boolean z = false;
        while (A08()) {
            A01();
            if (this.A00 == ' ') {
                z = true;
            } else if (z) {
                A02();
                return;
            }
        }
    }

    public boolean A08() {
        RandomAccessFile randomAccessFile;
        if (this.A01 && (randomAccessFile = this.A05) != null) {
            int i = this.A04;
            int i2 = this.A03;
            if (i <= i2 - 1) {
                if (i < i2 - 1) {
                    return true;
                }
                try {
                    this.A03 = randomAccessFile.read(this.A08);
                    this.A04 = -1;
                } catch (IOException unused) {
                    this.A01 = false;
                    A00();
                }
                return A08();
            }
        }
        return false;
    }

    public C02560Fk(String str, int i) {
        this.A07 = str;
        this.A08 = new byte[i];
    }

    private void A01() {
        if (A08()) {
            int i = this.A04 + 1;
            this.A04 = i;
            this.A02 = this.A00;
            this.A00 = (char) this.A08[i];
            this.A06 = false;
            return;
        }
        throw new NoSuchElementException();
    }

    public void A07(CharBuffer charBuffer) {
        charBuffer.clear();
        boolean z = true;
        while (true) {
            if (!A08()) {
                break;
            }
            A01();
            if (!Character.isWhitespace(this.A00)) {
                if (!charBuffer.hasRemaining()) {
                    CharBuffer allocate = CharBuffer.allocate(charBuffer.capacity() << 1);
                    charBuffer.flip();
                    allocate.put(charBuffer);
                    charBuffer = allocate;
                }
                charBuffer.put(this.A00);
                z = false;
            } else if (!z) {
                A02();
            } else {
                throw new AnonymousClass0KX("Couldn't read string!");
            }
        }
        if (!z) {
            charBuffer.flip();
            return;
        }
        throw new AnonymousClass0KX("Couldn't read string because file ended!");
    }

    public void finalize() {
        int A032 = C000700l.A03(2002137234);
        A00();
        C000700l.A09(-1024868264, A032);
    }
}
