package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;

@UserScoped
/* renamed from: X.16Z  reason: invalid class name */
public final class AnonymousClass16Z {
    private static C05540Zi A02;
    public final C14300t0 A00;
    public final C189116b A01;

    public static final AnonymousClass16Z A00(AnonymousClass1XY r4) {
        AnonymousClass16Z r0;
        synchronized (AnonymousClass16Z.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass16Z((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass16Z) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public String A01(ParticipantInfo participantInfo) {
        String str;
        User A03;
        UserKey userKey = participantInfo.A01;
        if (userKey == null || (A03 = this.A00.A03(userKey)) == null) {
            str = participantInfo.A03;
        } else {
            str = A03.A0L.A00();
        }
        if (str == null) {
            return participantInfo.A02;
        }
        return str;
    }

    public String A02(ParticipantInfo participantInfo) {
        User user;
        UserKey userKey = participantInfo.A01;
        C189116b r1 = this.A01;
        if (userKey != null) {
            user = this.A00.A03(userKey);
        } else {
            user = null;
        }
        String A06 = r1.A06(user);
        if (C06850cB.A0B(A06)) {
            return participantInfo.A03;
        }
        return A06;
    }

    private AnonymousClass16Z(AnonymousClass1XY r2) {
        this.A00 = C14300t0.A00(r2);
        this.A01 = C189116b.A01(r2);
    }
}
