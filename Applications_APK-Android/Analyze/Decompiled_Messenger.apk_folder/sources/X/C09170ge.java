package X;

/* renamed from: X.0ge  reason: invalid class name and case insensitive filesystem */
public final class C09170ge extends C09180gf {
    private final AnonymousClass1ZJ A00;

    public C09170ge(AnonymousClass0Td[] r1, C39851zi r2, AnonymousClass1ZJ r3) {
        super(r1, r2);
        this.A00 = r3;
    }

    public static int A00(C09170ge r14, int i, int i2, int i3) {
        long A02 = r14.A02(i);
        int i4 = 0;
        if (A02 == 0 || r14.A03 == null) {
            return 0;
        }
        long j = 1;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            AnonymousClass0Td[] r1 = r14.A03;
            if (i4 < r1.length) {
                if ((A02 & j) != 0) {
                    int i7 = i2;
                    int i8 = i3;
                    if (i8 != 1) {
                        if (i8 == 2) {
                            r1[i4].Bkk(i, i7);
                        } else {
                            throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown listenerMethod: ", i8));
                        }
                    } else if (r1[i4].Bkl(i, i7)) {
                        i6 = (int) (((long) i6) | j);
                    }
                    i5++;
                }
                i4++;
                j <<= 1;
            } else {
                AnonymousClass1ZJ r0 = r14.A00;
                if (!(r0 == null || i5 == 0)) {
                    r0.A05.getAndAdd(i5);
                }
                return i6;
            }
        }
    }
}
