package X;

/* renamed from: X.0eJ  reason: invalid class name and case insensitive filesystem */
public final class C07870eJ implements Cloneable {
    public static final Object A04 = new Object();
    public int A00;
    public boolean A01;
    public int[] A02;
    public Object[] A03;

    public Object A04(int i) {
        Object obj;
        int A022 = AnonymousClass04d.A02(this.A02, this.A00, i);
        if (A022 < 0 || (obj = this.A03[A022]) == A04) {
            return null;
        }
        return obj;
    }

    public static void A00(C07870eJ r8) {
        int i = r8.A00;
        int[] iArr = r8.A02;
        Object[] objArr = r8.A03;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != A04) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        r8.A01 = false;
        r8.A00 = i2;
    }

    public int A01() {
        if (this.A01) {
            A00(this);
        }
        return this.A00;
    }

    public int A02(int i) {
        if (this.A01) {
            A00(this);
        }
        return this.A02[i];
    }

    public Object A05(int i) {
        if (this.A01) {
            A00(this);
        }
        return this.A03[i];
    }

    public void A06() {
        int i = this.A00;
        Object[] objArr = this.A03;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.A00 = 0;
        this.A01 = false;
    }

    public void A07(int i) {
        Object[] objArr;
        Object obj;
        int A022 = AnonymousClass04d.A02(this.A02, this.A00, i);
        if (A022 >= 0 && (objArr = this.A03)[A022] != (obj = A04)) {
            objArr[A022] = obj;
            this.A01 = true;
        }
    }

    public void A08(int i, Object obj) {
        int i2 = this.A00;
        if (i2 == 0 || i > this.A02[i2 - 1]) {
            if (this.A01 && i2 >= this.A02.length) {
                A00(this);
            }
            int i3 = this.A00;
            int[] iArr = this.A02;
            int length = iArr.length;
            if (i3 >= length) {
                int A002 = AnonymousClass04d.A00(i3 + 1);
                int[] iArr2 = new int[A002];
                Object[] objArr = new Object[A002];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                Object[] objArr2 = this.A03;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.A02 = iArr2;
                this.A03 = objArr;
            }
            this.A02[i3] = i;
            this.A03[i3] = obj;
            this.A00 = i3 + 1;
            return;
        }
        A09(i, obj);
    }

    public void A09(int i, Object obj) {
        int[] iArr = this.A02;
        int i2 = this.A00;
        int A022 = AnonymousClass04d.A02(iArr, i2, i);
        if (A022 >= 0) {
            this.A03[A022] = obj;
            return;
        }
        int i3 = A022 ^ -1;
        if (i3 < i2) {
            Object[] objArr = this.A03;
            if (objArr[i3] == A04) {
                iArr[i3] = i;
                objArr[i3] = obj;
                return;
            }
        }
        if (this.A01 && i2 >= iArr.length) {
            A00(this);
            i3 = AnonymousClass04d.A02(iArr, this.A00, i) ^ -1;
        }
        int i4 = this.A00;
        int length = iArr.length;
        if (i4 >= length) {
            int A002 = AnonymousClass04d.A00(i4 + 1);
            int[] iArr2 = new int[A002];
            Object[] objArr2 = new Object[A002];
            System.arraycopy(iArr, 0, iArr2, 0, length);
            Object[] objArr3 = this.A03;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.A02 = iArr2;
            this.A03 = objArr2;
        }
        int i5 = this.A00 - i3;
        if (i5 != 0) {
            int[] iArr3 = this.A02;
            int i6 = i3 + 1;
            System.arraycopy(iArr3, i3, iArr3, i6, i5);
            Object[] objArr4 = this.A03;
            System.arraycopy(objArr4, i3, objArr4, i6, this.A00 - i3);
        }
        this.A02[i3] = i;
        this.A03[i3] = obj;
        this.A00++;
    }

    /* renamed from: A03 */
    public C07870eJ clone() {
        try {
            C07870eJ r1 = (C07870eJ) super.clone();
            r1.A02 = (int[]) this.A02.clone();
            r1.A03 = (Object[]) this.A03.clone();
            return r1;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public String toString() {
        if (A01() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.A00 * 28);
        sb.append('{');
        for (int i = 0; i < this.A00; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(A02(i));
            sb.append('=');
            Object A05 = A05(i);
            if (A05 != this) {
                sb.append(A05);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public C07870eJ() {
        this(10);
    }

    public C07870eJ(int i) {
        this.A01 = false;
        if (i == 0) {
            this.A02 = AnonymousClass04d.A00;
            this.A03 = AnonymousClass04d.A02;
            return;
        }
        int A002 = AnonymousClass04d.A00(i);
        this.A02 = new int[A002];
        this.A03 = new Object[A002];
    }
}
