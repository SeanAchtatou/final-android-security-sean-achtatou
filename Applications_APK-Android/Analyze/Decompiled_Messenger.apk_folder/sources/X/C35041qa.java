package X;

/* renamed from: X.1qa  reason: invalid class name and case insensitive filesystem */
public final class C35041qa implements C35051qb {
    public final String A00;
    public final boolean A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C35041qa) {
                C35041qa r5 = (C35041qa) obj;
                if (this.A01 != r5.A01 || !C28931fb.A07(this.A00, r5.A00)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A03(C28931fb.A04(1, this.A01), this.A00);
    }

    public C35041qa(C35031qZ r3) {
        this.A01 = r3.A01;
        String str = r3.A00;
        C28931fb.A06(str, "text");
        this.A00 = str;
    }
}
