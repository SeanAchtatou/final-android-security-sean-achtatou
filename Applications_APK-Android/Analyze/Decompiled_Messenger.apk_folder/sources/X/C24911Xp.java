package X;

import android.content.Context;
import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.1Xp  reason: invalid class name and case insensitive filesystem */
public final class C24911Xp implements C24891Xn {
    public final Context A00;
    public final C04290Tm A01 = new C04290Tm(new AnonymousClass0U7(this), 16);
    public final AnonymousClass1XX A02;

    public C24811Xe A00() {
        return this.A02.getInjectorThreadStack();
    }

    public C04310Tq C4b(C22916BKm bKm, C04310Tq r3) {
        return new EID(this, r3);
    }

    public C24911Xp(AnonymousClass1XX r4) {
        this.A02 = r4;
        this.A00 = r4.getInjectorThreadStack().A00();
    }
}
