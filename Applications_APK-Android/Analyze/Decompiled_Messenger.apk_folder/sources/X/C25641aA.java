package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.1aA  reason: invalid class name and case insensitive filesystem */
public final class C25641aA implements Runnable {
    public static final String __redex_internal_original_name = "com.google.common.util.concurrent.Futures$NonCancellationPropagatingFuture$1";
    public final /* synthetic */ C25631a9 A00;
    public final /* synthetic */ ListenableFuture A01;

    public C25641aA(C25631a9 r1, ListenableFuture listenableFuture) {
        this.A00 = r1;
        this.A01 = listenableFuture;
    }

    public void run() {
        this.A00.setFuture(this.A01);
    }
}
