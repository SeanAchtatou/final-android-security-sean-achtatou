package X;

import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import io.card.payment.BuildConfig;

@InjectorModule
/* renamed from: X.1Pr  reason: invalid class name and case insensitive filesystem */
public final class C23481Pr extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static volatile AnonymousClass1MI A02;
    private static volatile C31211jK A03;

    public static final AnonymousClass1MI A00(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        new C23491Ps(applicationInjector);
                        A02 = new AnonymousClass1MI(new AnonymousClass1QS(), new AnonymousClass1QT(FbSharedPreferencesModule.A00(new AnonymousClass17X(applicationInjector)), BuildConfig.FLAVOR), AnonymousClass067.A02());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final C31211jK A01(AnonymousClass1XY r11) {
        if (A03 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        C31161jF r3 = new C31161jF(applicationInjector);
                        A03 = new C31211jK(new C31181jH(AnonymousClass0WT.A00(applicationInjector)), new C31191jI(FbSharedPreferencesModule.A00(new C31171jG(applicationInjector)), BuildConfig.FLAVOR), AnonymousClass067.A02(), AnonymousClass0ZD.A03(r3), AnonymousClass0WT.A00(r3));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }
}
