package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.06T  reason: invalid class name */
public final class AnonymousClass06T implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass06R A00;

    public AnonymousClass06T(AnonymousClass06R r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r10) {
        int A002 = AnonymousClass09Y.A00(1396923001);
        synchronized (this.A00) {
            try {
                AnonymousClass06R r1 = this.A00;
                if (r1.A00) {
                    AnonymousClass054 r4 = AnonymousClass054.A07;
                    if (r4 != null) {
                        r4.A0B(AnonymousClass04i.A00, null, 0);
                    }
                } else {
                    AnonymousClass06R.A09(r1);
                }
            } catch (Throwable th) {
                while (true) {
                    AnonymousClass09Y.A01(1704331501, A002);
                    throw th;
                }
            }
        }
        AnonymousClass09Y.A01(453447815, A002);
    }
}
