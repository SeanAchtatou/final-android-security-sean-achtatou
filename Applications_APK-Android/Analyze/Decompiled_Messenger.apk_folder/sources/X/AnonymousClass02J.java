package X;

/* renamed from: X.02J  reason: invalid class name */
public final class AnonymousClass02J {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "lib-zstd";
            case 2:
                return "lib-superpack-xz";
            case 3:
                return "lib-superpack-zstd";
            default:
                return "lib-xzs";
        }
    }
}
