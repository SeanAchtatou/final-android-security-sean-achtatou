package X;

import android.content.Context;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.redex.dynamicanalysis.DynamicAnalysisTraceManager;
import java.util.HashMap;

/* renamed from: X.0DM  reason: invalid class name */
public final class AnonymousClass0DM implements AnonymousClass32S, CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.redex.dynamicanalysis.support.DynamicAnalysisUploadConditionalWorker";
    private AnonymousClass0UN A00;
    private final AnonymousClass0DP A01;

    public static final AnonymousClass0DM A00(AnonymousClass1XY r1) {
        return new AnonymousClass0DM(r1);
    }

    public static final C04310Tq A01(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.ArQ, r1);
    }

    public boolean C4B(C168587q7 r12) {
        C10740km r7 = (C10740km) AnonymousClass1XX.A03(AnonymousClass1Y3.B9i, this.A00);
        if (DynamicAnalysisTraceManager.A03.getAndSet(true)) {
            return false;
        }
        HashMap hashMap = DynamicAnalysisTraceManager.A01;
        if (hashMap.isEmpty()) {
            C010708t.A0J("DYNA|DynamicAnalysisUploadConditionalWorker", "Nothing to upload!");
            return true;
        }
        Context context = C02470Ey.A00;
        for (String str : hashMap.keySet()) {
            AnonymousClass0DR r2 = (AnonymousClass0DR) hashMap.get(str);
            C010708t.A0P("DYNA|DynamicAnalysisUploadConditionalWorker", "About to upload %d raw methods stats: short[%d] ... for interaction %s", Integer.valueOf(r2.A02), Integer.valueOf(r2.A03), str);
            try {
                r7.A06(this.A01, r2, CallerContext.A04(AnonymousClass0DM.class));
                C010708t.A0P("DYNA|DynamicAnalysisUploadConditionalWorker", "Successfully uploaded %d methods stats (short[%d]) for the main process for interaction %s", Integer.valueOf(r2.A02), Integer.valueOf(r2.A03), str);
            } catch (Exception e) {
                C010708t.A0R("DYNA|DynamicAnalysisUploadConditionalWorker", e, "Failed to upload method stats for one trace ...");
            }
            try {
                context.openFileInput(r2.A07);
            } catch (Exception e2) {
                C010708t.A0R("DYNA|DynamicAnalysisUploadConditionalWorker", e2, "Failed to open file in order to delete :|");
            } catch (Throwable th) {
                context.deleteFile(r2.A07);
                throw th;
            }
            context.deleteFile(r2.A07);
        }
        DynamicAnalysisTraceManager.A01 = new HashMap();
        return true;
    }

    private AnonymousClass0DM(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = AnonymousClass0DP.A00(r3);
    }
}
