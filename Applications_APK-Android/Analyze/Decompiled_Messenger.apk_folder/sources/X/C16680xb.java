package X;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.common.banner.BasicBannerNotificationView;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.0xb  reason: invalid class name and case insensitive filesystem */
public final class C16680xb extends C32501lr {
    public AnonymousClass0ZM A00 = new C17010yB(this);
    private AnonymousClass0UN A01;
    public final C16590xN A02;
    public final C14150sh A03;
    public final C16740xh A04;
    public final C16730xg A05;
    public final FbSharedPreferences A06;
    private final Context A07;
    private final LayoutInflater A08;

    private C16680xb(AnonymousClass1XY r3, C16590xN r4, Context context, C16730xg r6, FbSharedPreferences fbSharedPreferences, LayoutInflater layoutInflater, C16740xh r9) {
        super("MuteGlobalWarningNotification");
        this.A01 = new AnonymousClass0UN(1, r3);
        this.A03 = new C14150sh(r3);
        this.A02 = r4;
        this.A07 = context;
        this.A05 = r6;
        this.A06 = fbSharedPreferences;
        this.A08 = layoutInflater;
        this.A04 = r9;
        r9.A00 = new C16760xj(this);
    }

    public static final C16680xb A00(AnonymousClass1XY r8) {
        AnonymousClass1XY r2 = r8;
        return new C16680xb(r2, new C16590xN(r8), AnonymousClass1YA.A00(r8), C190616q.A01(r8), FbSharedPreferencesModule.A00(r8), C04490Ux.A0e(r8), new C16740xh(AnonymousClass0UX.A0e(r2)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0039, code lost:
        if (r8.A03.A01.Aep(X.C05690aA.A04, true) != false) goto L_0x003b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C16680xb r8) {
        /*
            X.0xg r0 = r8.A05
            X.0US r0 = r0.A01
            java.lang.Object r0 = r0.get()
            X.0u1 r0 = (X.C14780u1) r0
            com.facebook.messaging.model.threads.NotificationSetting r0 = r0.A02()
            boolean r4 = r0.A03()
            X.0sh r0 = r8.A03
            com.facebook.prefs.shared.FbSharedPreferences r2 = r0.A01
            X.1Y7 r1 = X.C05690aA.A0y
            r0 = 1
            boolean r0 = r2.Aep(r1, r0)
            r3 = 1
            if (r0 != 0) goto L_0x003b
            X.0sh r0 = r8.A03
            com.facebook.prefs.shared.FbSharedPreferences r2 = r0.A01
            X.1Y7 r1 = X.C05690aA.A03
            r0 = 1
            boolean r0 = r2.Aep(r1, r0)
            if (r0 != 0) goto L_0x003b
            X.0sh r0 = r8.A03
            com.facebook.prefs.shared.FbSharedPreferences r2 = r0.A01
            X.1Y7 r1 = X.C05690aA.A04
            r0 = 1
            boolean r1 = r2.Aep(r1, r0)
            r0 = 1
            if (r1 == 0) goto L_0x003c
        L_0x003b:
            r0 = 0
        L_0x003c:
            if (r4 == 0) goto L_0x0041
            if (r0 == 0) goto L_0x0041
            r4 = 0
        L_0x0041:
            if (r4 == 0) goto L_0x0044
            r3 = 0
        L_0x0044:
            if (r3 == 0) goto L_0x009e
            r2 = 0
            int r1 = X.AnonymousClass1Y3.AaY
            X.0UN r0 = r8.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3AM r0 = (X.AnonymousClass3AM) r0
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 282535833699991(0x100f700010697, double:1.39591249150281E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x009e
            X.0xM r0 = r8.A00
            r0.A06(r8)
            X.0xg r0 = r8.A05
            X.0US r0 = r0.A01
            java.lang.Object r0 = r0.get()
            X.0u1 r0 = (X.C14780u1) r0
            com.facebook.messaging.model.threads.NotificationSetting r1 = r0.A02()
            long r4 = java.lang.System.currentTimeMillis()
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 / r6
            boolean r0 = r1.A03
            if (r0 == 0) goto L_0x009b
            long r2 = r1.A00
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x009b
            long r2 = r2 - r4
        L_0x008c:
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x009a
            X.0xh r1 = r8.A04
            long r2 = r2 * r6
            java.lang.String r0 = "MuteGlobalWarningNotification"
            r1.A02(r0, r2)
        L_0x009a:
            return
        L_0x009b:
            r2 = 0
            goto L_0x008c
        L_0x009e:
            X.0xM r0 = r8.A00
            r0.A05(r8)
            X.0xh r0 = r8.A04
            r0.A01()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16680xb.A01(X.0xb):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View B90(ViewGroup viewGroup) {
        String string;
        BasicBannerNotificationView basicBannerNotificationView = (BasicBannerNotificationView) this.A08.inflate(2132410497, viewGroup, false);
        basicBannerNotificationView.setClickable(true);
        if (((C14780u1) this.A05.A01.get()).A02().A03()) {
            string = this.A07.getString(2131833675);
        } else {
            C14780u1 r4 = (C14780u1) this.A05.A01.get();
            string = r4.A01.getString(2131828767, r4.A06(r4.A02().A00));
        }
        C46862Se r3 = new C46862Se();
        r3.A07 = string;
        r3.A03 = new ColorDrawable(AnonymousClass01R.A00(this.A07, 2132082728));
        r3.A01(this.A07.getString(2131828766));
        basicBannerNotificationView.A0T(r3.A00());
        basicBannerNotificationView.A00 = new C1927491g(this);
        return basicBannerNotificationView;
    }

    public void onResume() {
        this.A06.C0f(C05690aA.A0z, this.A00);
        A01(this);
    }
}
