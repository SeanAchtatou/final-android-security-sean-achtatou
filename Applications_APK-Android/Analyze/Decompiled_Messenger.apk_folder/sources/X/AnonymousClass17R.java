package X;

import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.17R  reason: invalid class name */
public final class AnonymousClass17R extends AnonymousClass17U {
    public final /* synthetic */ AnonymousClass16R A00;

    public AnonymousClass17R(AnonymousClass16R r1) {
        this.A00 = r1;
    }

    public void Bd1(Object obj, Object obj2) {
        String message;
        Throwable th = (Throwable) obj2;
        C20921Ei r7 = (C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, this.A00.A08);
        if (th == null) {
            message = null;
        } else {
            message = th.getMessage();
        }
        if (C20921Ei.A03(r7)) {
            for (int i : C20921Ei.A08) {
                if (message != null) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r7.A00)).markerAnnotate(i, "failure_reason", message);
                }
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r7.A00)).markerEnd(i, 3);
            }
            C20921Ei.A01(r7);
        }
    }

    public void BdJ(Object obj, Object obj2) {
    }
}
