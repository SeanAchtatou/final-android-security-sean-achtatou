package X;

import java.util.List;

/* renamed from: X.0dA  reason: invalid class name and case insensitive filesystem */
public abstract class C07280dA implements C07290dB {
    public void A01(C12240os r7) {
        C07270d9 r5 = (C07270d9) this;
        List A00 = r5.A00.A00();
        try {
            int size = A00.size();
            for (int i = 0; i < size; i++) {
                C07290dB r1 = (C07290dB) A00.get(i);
                if (r1 instanceof C07280dA) {
                    ((C07280dA) r1).A01(r7);
                } else {
                    r1.BY5();
                }
            }
        } finally {
            r5.A00.A01();
        }
    }

    public void BY7(int i) {
        if (this instanceof C07270d9) {
            C07270d9 r4 = (C07270d9) this;
            List A00 = r4.A00.A00();
            try {
                int size = A00.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((C07290dB) A00.get(i2)).BY7(i);
                }
            } finally {
                r4.A00.A01();
            }
        }
    }

    public final void BY5() {
        throw new UnsupportedOperationException("The debug event listener expects only onEventReceivedWithParamsCollectionMap to be called. For production, use EventListener instead.");
    }
}
