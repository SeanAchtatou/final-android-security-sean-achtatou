package X;

import android.os.Trace;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.0QE  reason: invalid class name */
public final class AnonymousClass0QE {
    public static void A00(int i) {
        int i2 = AnonymousClass00n.A08;
        Logger.writeStandardEntry(i2, 6, 23, 0, 0, i, 0, 0);
        if (!TraceEvents.isEnabled(i2)) {
            Trace.endSection();
        }
    }

    public static void A01(String str, int i) {
        if (!TraceEvents.isEnabled(AnonymousClass00n.A08)) {
            Trace.beginSection(str);
            return;
        }
        Logger.writeBytesEntry(AnonymousClass00n.A08, 1, 83, Logger.writeStandardEntry(AnonymousClass00n.A08, 7, 22, 0, 0, i, 0, 0), str);
    }
}
