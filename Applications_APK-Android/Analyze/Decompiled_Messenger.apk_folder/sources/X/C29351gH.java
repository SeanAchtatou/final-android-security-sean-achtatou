package X;

import android.os.Handler;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1gH  reason: invalid class name and case insensitive filesystem */
public final class C29351gH extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile AnonymousClass0VK A01;

    public static final AnonymousClass0VK A00(AnonymousClass1XY r6) {
        if (A01 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        C29361gI r3 = new C29361gI(applicationInjector);
                        Handler A003 = C29321gE.A00(applicationInjector);
                        C04910Wu.A00(r3);
                        A01 = new C29371gJ(r3, "Mqtt_Wakeup", A003);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
