package X;

/* renamed from: X.1ah  reason: invalid class name and case insensitive filesystem */
public final class C25971ah {
    public final String A00;
    public final int[] A01;
    public final C26071ar[] A02;
    public final C25981ai[] A03;

    public C25971ah(String str, int[] iArr, C25981ai[] r5, C26071ar[] r6) {
        if (r6.length != 0) {
            this.A00 = str;
            this.A01 = iArr;
            this.A03 = r5;
            this.A02 = r6;
            return;
        }
        throw new IllegalArgumentException("At least one aggregation must be set for a scenario");
    }
}
