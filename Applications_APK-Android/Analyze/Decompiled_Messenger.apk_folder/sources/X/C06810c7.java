package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0c7  reason: invalid class name and case insensitive filesystem */
public final class C06810c7 extends Handler {
    public final long A00;
    public final AtomicInteger A01 = new AtomicInteger(0);
    public final /* synthetic */ C04480Uv A02;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C06810c7(C04480Uv r3, Looper looper, long j) {
        super(looper);
        this.A02 = r3;
        this.A00 = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        if (r7 >= r9) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0038, code lost:
        r6 = r8[r7];
        r5 = r6.A01.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0044, code lost:
        if (r5.hasNext() == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0046, code lost:
        ((X.C06930cK) r5.next()).A03.onReceive(r10.A00, r6.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0056, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r12) {
        /*
            r11 = this;
            int r1 = r12.what
            r0 = 1
            if (r1 == r0) goto L_0x0009
            super.handleMessage(r12)
            return
        L_0x0009:
            X.0Uv r10 = r11.A02
            long r3 = r11.A00
        L_0x000d:
            java.util.Map r2 = r10.A04
            monitor-enter(r2)
            X.0V0 r0 = r10.A01     // Catch:{ all -> 0x005b }
            int r0 = r0.size()     // Catch:{ all -> 0x005b }
            if (r0 <= 0) goto L_0x0059
            X.0V0 r0 = r10.A01     // Catch:{ all -> 0x005b }
            java.lang.Long r1 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x005b }
            java.util.Collection r0 = r0.AbK(r1)     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0059
            int r9 = r0.size()     // Catch:{ all -> 0x005b }
            if (r9 <= 0) goto L_0x0059
            X.2JZ[] r8 = new X.AnonymousClass2JZ[r9]     // Catch:{ all -> 0x005b }
            r0.toArray(r8)     // Catch:{ all -> 0x005b }
            X.0V0 r0 = r10.A01     // Catch:{ all -> 0x005b }
            r0.C1N(r1)     // Catch:{ all -> 0x005b }
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            r7 = 0
        L_0x0036:
            if (r7 >= r9) goto L_0x000d
            r6 = r8[r7]
            java.util.Collection r0 = r6.A01
            java.util.Iterator r5 = r0.iterator()
        L_0x0040:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0056
            java.lang.Object r0 = r5.next()
            X.0cK r0 = (X.C06930cK) r0
            android.content.BroadcastReceiver r2 = r0.A03
            android.content.Context r1 = r10.A00
            android.content.Intent r0 = r6.A00
            r2.onReceive(r1, r0)
            goto L_0x0040
        L_0x0056:
            int r7 = r7 + 1
            goto L_0x0036
        L_0x0059:
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            return
        L_0x005b:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06810c7.handleMessage(android.os.Message):void");
    }
}
