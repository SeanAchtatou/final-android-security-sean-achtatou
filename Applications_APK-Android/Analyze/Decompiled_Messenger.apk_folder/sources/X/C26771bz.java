package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1bz  reason: invalid class name and case insensitive filesystem */
public final class C26771bz extends Enum implements C26781c0 {
    private static final /* synthetic */ C26771bz[] $VALUES;
    public static final C26771bz ALLOW_FINAL_FIELDS_AS_MUTATORS;
    public static final C26771bz AUTO_DETECT_CREATORS;
    public static final C26771bz AUTO_DETECT_FIELDS;
    public static final C26771bz AUTO_DETECT_GETTERS;
    public static final C26771bz AUTO_DETECT_IS_GETTERS;
    public static final C26771bz AUTO_DETECT_SETTERS;
    public static final C26771bz CAN_OVERRIDE_ACCESS_MODIFIERS;
    public static final C26771bz DEFAULT_VIEW_INCLUSION;
    public static final C26771bz INFER_PROPERTY_MUTATORS;
    public static final C26771bz REQUIRE_SETTERS_FOR_GETTERS;
    public static final C26771bz SORT_PROPERTIES_ALPHABETICALLY;
    public static final C26771bz USE_ANNOTATIONS;
    public static final C26771bz USE_GETTERS_AS_SETTERS;
    public static final C26771bz USE_STATIC_TYPING;
    public static final C26771bz USE_WRAPPER_NAME_AS_PROPERTY_NAME;
    private final boolean _defaultState;

    static {
        C26771bz r15 = new C26771bz("USE_ANNOTATIONS", 0, true);
        USE_ANNOTATIONS = r15;
        C26771bz r13 = new C26771bz("AUTO_DETECT_CREATORS", 1, true);
        AUTO_DETECT_CREATORS = r13;
        C26771bz r12 = new C26771bz("AUTO_DETECT_FIELDS", 2, true);
        AUTO_DETECT_FIELDS = r12;
        C26771bz r11 = new C26771bz("AUTO_DETECT_GETTERS", 3, true);
        AUTO_DETECT_GETTERS = r11;
        C26771bz r10 = new C26771bz("AUTO_DETECT_IS_GETTERS", 4, true);
        AUTO_DETECT_IS_GETTERS = r10;
        C26771bz r9 = new C26771bz("AUTO_DETECT_SETTERS", 5, true);
        AUTO_DETECT_SETTERS = r9;
        C26771bz r8 = new C26771bz("REQUIRE_SETTERS_FOR_GETTERS", 6, false);
        REQUIRE_SETTERS_FOR_GETTERS = r8;
        C26771bz r7 = new C26771bz("USE_GETTERS_AS_SETTERS", 7, true);
        USE_GETTERS_AS_SETTERS = r7;
        C26771bz r6 = new C26771bz("CAN_OVERRIDE_ACCESS_MODIFIERS", 8, true);
        CAN_OVERRIDE_ACCESS_MODIFIERS = r6;
        C26771bz r5 = new C26771bz("INFER_PROPERTY_MUTATORS", 9, true);
        INFER_PROPERTY_MUTATORS = r5;
        C26771bz r4 = new C26771bz("ALLOW_FINAL_FIELDS_AS_MUTATORS", 10, true);
        ALLOW_FINAL_FIELDS_AS_MUTATORS = r4;
        C26771bz r3 = new C26771bz("USE_STATIC_TYPING", 11, false);
        USE_STATIC_TYPING = r3;
        C26771bz r17 = new C26771bz("DEFAULT_VIEW_INCLUSION", 12, true);
        DEFAULT_VIEW_INCLUSION = r17;
        C26771bz r18 = new C26771bz("SORT_PROPERTIES_ALPHABETICALLY", 13, false);
        SORT_PROPERTIES_ALPHABETICALLY = r18;
        C26771bz r182 = new C26771bz("USE_WRAPPER_NAME_AS_PROPERTY_NAME", 14, false);
        USE_WRAPPER_NAME_AS_PROPERTY_NAME = r182;
        C26771bz r26 = r17;
        C26771bz r27 = r18;
        C26771bz r28 = r182;
        C26771bz r20 = r8;
        C26771bz r21 = r7;
        C26771bz r183 = r10;
        C26771bz r19 = r9;
        C26771bz r16 = r12;
        C26771bz r172 = r11;
        C26771bz r14 = r15;
        C26771bz r152 = r13;
        $VALUES = new C26771bz[]{r14, r152, r16, r172, r183, r19, r20, r21, r6, r5, r4, r3, r26, r27, r28};
    }

    public static C26771bz valueOf(String str) {
        return (C26771bz) Enum.valueOf(C26771bz.class, str);
    }

    public static C26771bz[] values() {
        return (C26771bz[]) $VALUES.clone();
    }

    public boolean enabledByDefault() {
        return this._defaultState;
    }

    private C26771bz(String str, int i, boolean z) {
        this._defaultState = z;
    }

    public int getMask() {
        return 1 << ordinal();
    }
}
