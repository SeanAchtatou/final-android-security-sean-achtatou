package X;

import android.graphics.PointF;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;
import androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState;
import java.util.BitSet;

/* renamed from: X.1Eg  reason: invalid class name and case insensitive filesystem */
public final class C20901Eg extends AnonymousClass19T implements AnonymousClass19V {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05 = -1;
    public C197219k A06;
    public C197219k A07;
    public C22750BAu A08;
    public StaggeredGridLayoutManager$SavedState A09;
    public BitSet A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public int[] A0F;
    public C25156Cas[] A0G;
    private boolean A0H;
    public final C860846r A0I;
    private final Rect A0J;
    private final C25159Cav A0K;
    private final Runnable A0L;

    public static int A09(C20901Eg r6, C15930wD r7) {
        C20901Eg r4 = r6;
        if (r6.A0c() == 0) {
            return 0;
        }
        return C52082iR.A01(r7, r6.A06, A0D(r6, false), A0C(r6, false), r4, true, r6.A0E);
    }

    public static int A0A(C20901Eg r5, C15930wD r6) {
        C20901Eg r4 = r5;
        if (r5.A0c() == 0) {
            return 0;
        }
        return C52082iR.A00(r6, r5.A06, A0D(r5, false), A0C(r5, false), r4, true);
    }

    private void A0G(int i, int i2) {
        for (int i3 = 0; i3 < this.A05; i3++) {
            if (!this.A0G[i3].A03.isEmpty()) {
                A0X(this.A0G[i3], i, i2);
            }
        }
    }

    public static void A0Z(C20901Eg r4, int i, C15930wD r6) {
        int A062;
        int i2;
        if (i > 0) {
            A062 = A07(r4);
            i2 = 1;
        } else {
            A062 = A06(r4);
            i2 = -1;
        }
        r4.A0I.A02 = true;
        r4.A0M(A062, r6);
        r4.A0F(i2);
        C860846r r1 = r4.A0I;
        r1.A04 = A062 + r1.A06;
        r1.A00 = Math.abs(i);
    }

    public void A1m(C15740vp r2, C15930wD r3) {
        A0W(r2, r3, true);
    }

    private int A01(int i) {
        int A052 = this.A0G[0].A05(i);
        for (int i2 = 1; i2 < this.A05; i2++) {
            int A053 = this.A0G[i2].A05(i);
            if (A053 > A052) {
                A052 = A053;
            }
        }
        return A052;
    }

    private int A02(int i) {
        int A062 = this.A0G[0].A06(i);
        for (int i2 = 1; i2 < this.A05; i2++) {
            int A063 = this.A0G[i2].A06(i);
            if (A063 < A062) {
                A062 = A063;
            }
        }
        return A062;
    }

    private static int A03(int i, int i2, int i3) {
        int mode;
        if ((i2 != 0 || i3 != 0) && ((mode = View.MeasureSpec.getMode(i)) == Integer.MIN_VALUE || mode == 1073741824)) {
            return View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i) - i2) - i3), mode);
        }
        return i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        if (r7 >= r23.A00()) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0192, code lost:
        r0 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x01e0  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x01fa  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0201  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x021f  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0228  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0231  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0239  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0252  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x032f  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int A05(X.C15740vp r21, X.C860846r r22, X.C15930wD r23) {
        /*
            r20 = this;
            r5 = r20
            java.util.BitSet r1 = r5.A0A
            int r0 = r5.A05
            r4 = 0
            r6 = 1
            r1.set(r4, r0, r6)
            X.46r r0 = r5.A0I
            boolean r0 = r0.A01
            r3 = r22
            if (r0 == 0) goto L_0x031b
            int r0 = r3.A07
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 != r6) goto L_0x001c
            r1 = 2147483647(0x7fffffff, float:NaN)
        L_0x001c:
            int r0 = r3.A07
            r5.A0G(r0, r1)
            boolean r0 = r5.A0E
            if (r0 == 0) goto L_0x0313
            X.19k r0 = r5.A06
            int r8 = r0.A02()
        L_0x002b:
            r9 = 0
        L_0x002c:
            int r7 = r3.A04
            if (r7 < 0) goto L_0x0037
            int r2 = r23.A00()
            r0 = 1
            if (r7 < r2) goto L_0x0038
        L_0x0037:
            r0 = 0
        L_0x0038:
            r18 = -1
            r19 = r21
            if (r0 == 0) goto L_0x032d
            X.46r r0 = r5.A0I
            boolean r0 = r0.A01
            if (r0 != 0) goto L_0x004c
            java.util.BitSet r0 = r5.A0A
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x032d
        L_0x004c:
            int r2 = r3.A04
            r0 = r19
            android.view.View r9 = r0.A04(r2)
            int r2 = r3.A04
            int r0 = r3.A06
            int r2 = r2 + r0
            r3.A04 = r2
            android.view.ViewGroup$LayoutParams r7 = r9.getLayoutParams()
            X.Cat r7 = (X.C25157Cat) r7
            X.1o8 r0 = r7.mViewHolder
            int r10 = r0.A05()
            X.BAu r0 = r5.A08
            int[] r2 = r0.A01
            if (r2 == 0) goto L_0x0310
            int r0 = r2.length
            if (r10 >= r0) goto L_0x0310
            r2 = r2[r10]
        L_0x0072:
            r17 = 0
            r0 = r18
            if (r2 != r0) goto L_0x007a
            r17 = 1
        L_0x007a:
            if (r17 == 0) goto L_0x030a
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x02be
            X.Cas[] r0 = r5.A0G
            r12 = r0[r4]
        L_0x0084:
            X.BAu r2 = r5.A08
            X.C22750BAu.A00(r2, r10)
            int[] r2 = r2.A01
            int r0 = r12.A04
            r2[r10] = r0
        L_0x008f:
            r7.A00 = r12
            int r0 = r3.A07
            if (r0 != r6) goto L_0x02b9
            r5.A16(r9)
        L_0x0098:
            r11 = 0
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x0287
            int r0 = r5.A01
            if (r0 != r6) goto L_0x026d
            int r13 = r5.A00
        L_0x00a3:
            int r14 = r5.A01
            int r4 = r5.A02
            int r2 = r5.A0h()
            int r0 = r5.A0e()
            int r2 = r2 + r0
            int r0 = r7.height
            int r0 = X.AnonymousClass19T.A0I(r14, r4, r2, r0, r6)
            r5.A0Q(r9, r13, r0, r11)
        L_0x00b9:
            int r0 = r3.A07
            if (r0 != r6) goto L_0x00f8
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x00f3
            int r4 = r5.A01(r8)
        L_0x00c5:
            X.19k r0 = r5.A06
            int r2 = r0.A09(r9)
            int r2 = r2 + r4
            if (r17 == 0) goto L_0x0148
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x0148
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r13 = new androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem
            r13.<init>()
            int r0 = r5.A05
            int[] r0 = new int[r0]
            r13.A03 = r0
            r15 = 0
        L_0x00de:
            int r0 = r5.A05
            if (r15 >= r0) goto L_0x013d
            int[] r14 = r13.A03
            X.Cas[] r0 = r5.A0G
            r0 = r0[r15]
            int r0 = r0.A05(r4)
            int r0 = r4 - r0
            r14[r15] = r0
            int r15 = r15 + 1
            goto L_0x00de
        L_0x00f3:
            int r4 = r12.A05(r8)
            goto L_0x00c5
        L_0x00f8:
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x012e
            int r2 = r5.A02(r8)
        L_0x0100:
            X.19k r0 = r5.A06
            int r0 = r0.A09(r9)
            int r4 = r2 - r0
            if (r17 == 0) goto L_0x0148
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x0148
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r14 = new androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem
            r14.<init>()
            int r0 = r5.A05
            int[] r0 = new int[r0]
            r14.A03 = r0
            r15 = 0
        L_0x011a:
            int r0 = r5.A05
            if (r15 >= r0) goto L_0x0133
            int[] r13 = r14.A03
            X.Cas[] r0 = r5.A0G
            r0 = r0[r15]
            int r0 = r0.A06(r2)
            int r0 = r0 - r2
            r13[r15] = r0
            int r15 = r15 + 1
            goto L_0x011a
        L_0x012e:
            int r2 = r12.A06(r8)
            goto L_0x0100
        L_0x0133:
            r14.A00 = r6
            r14.A02 = r10
            X.BAu r0 = r5.A08
            r0.A08(r14)
            goto L_0x0148
        L_0x013d:
            r0 = r18
            r13.A00 = r0
            r13.A02 = r10
            X.BAu r0 = r5.A08
            r0.A08(r13)
        L_0x0148:
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x01a2
            int r13 = r3.A06
            r0 = r18
            if (r13 != r0) goto L_0x01a2
            if (r17 != 0) goto L_0x01a0
            int r0 = r3.A07
            if (r0 != r6) goto L_0x0174
            X.Cas[] r0 = r5.A0G
            r0 = r0[r11]
            r14 = -2147483648(0xffffffff80000000, float:-0.0)
            int r13 = r0.A05(r14)
            r11 = 1
        L_0x0163:
            int r0 = r5.A05
            if (r11 >= r0) goto L_0x0192
            X.Cas[] r0 = r5.A0G
            r0 = r0[r11]
            int r0 = r0.A05(r14)
            if (r0 != r13) goto L_0x0190
            int r11 = r11 + 1
            goto L_0x0163
        L_0x0174:
            X.Cas[] r0 = r5.A0G
            r0 = r0[r11]
            r14 = -2147483648(0xffffffff80000000, float:-0.0)
            int r13 = r0.A06(r14)
            r11 = 1
        L_0x017f:
            int r0 = r5.A05
            if (r11 >= r0) goto L_0x0192
            X.Cas[] r0 = r5.A0G
            r0 = r0[r11]
            int r0 = r0.A06(r14)
            if (r0 != r13) goto L_0x0190
            int r11 = r11 + 1
            goto L_0x017f
        L_0x0190:
            r0 = 0
            goto L_0x0193
        L_0x0192:
            r0 = 1
        L_0x0193:
            r0 = r0 ^ r6
            if (r0 == 0) goto L_0x01a2
            X.BAu r0 = r5.A08
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r0 = r0.A01(r10)
            if (r0 == 0) goto L_0x01a0
            r0.A01 = r6
        L_0x01a0:
            r5.A0H = r6
        L_0x01a2:
            int r10 = r3.A07
            r0 = 1
            if (r10 != r0) goto L_0x01bb
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x01d5
            int r0 = r5.A05
            int r10 = r0 + -1
        L_0x01af:
            if (r10 < 0) goto L_0x01da
            X.Cas[] r0 = r5.A0G
            r0 = r0[r10]
            r0.A0B(r9)
            int r10 = r10 + -1
            goto L_0x01af
        L_0x01bb:
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x01cf
            int r0 = r5.A05
            int r10 = r0 + -1
        L_0x01c3:
            if (r10 < 0) goto L_0x01da
            X.Cas[] r0 = r5.A0G
            r0 = r0[r10]
            r0.A0C(r9)
            int r10 = r10 + -1
            goto L_0x01c3
        L_0x01cf:
            X.Cas r0 = r7.A00
            r0.A0C(r9)
            goto L_0x01da
        L_0x01d5:
            X.Cas r0 = r7.A00
            r0.A0B(r9)
        L_0x01da:
            boolean r0 = r20.A0a()
            if (r0 == 0) goto L_0x024e
            int r0 = r5.A01
            if (r0 != r6) goto L_0x024e
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x023d
            X.19k r0 = r5.A07
            int r0 = r0.A02()
        L_0x01ee:
            X.19k r10 = r5.A07
            int r10 = r10.A09(r9)
            int r10 = r0 - r10
        L_0x01f6:
            int r11 = r5.A01
            if (r11 != r6) goto L_0x0239
            X.AnonymousClass19T.A0N(r9, r10, r4, r0, r2)
        L_0x01fd:
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x0231
            X.46r r0 = r5.A0I
            int r0 = r0.A07
            r5.A0G(r0, r1)
        L_0x0208:
            X.46r r2 = r5.A0I
            r0 = r19
            r5.A0T(r0, r2)
            X.46r r0 = r5.A0I
            boolean r0 = r0.A03
            if (r0 == 0) goto L_0x0224
            boolean r0 = r9.hasFocusable()
            if (r0 == 0) goto L_0x0224
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x0228
            java.util.BitSet r0 = r5.A0A
            r0.clear()
        L_0x0224:
            r9 = 1
            r4 = 0
            goto L_0x002c
        L_0x0228:
            java.util.BitSet r4 = r5.A0A
            int r2 = r12.A04
            r0 = 0
            r4.set(r2, r0)
            goto L_0x0224
        L_0x0231:
            X.46r r0 = r5.A0I
            int r0 = r0.A07
            r5.A0X(r12, r0, r1)
            goto L_0x0208
        L_0x0239:
            X.AnonymousClass19T.A0N(r9, r4, r10, r2, r0)
            goto L_0x01fd
        L_0x023d:
            X.19k r0 = r5.A07
            int r0 = r0.A02()
            int r11 = r5.A05
            int r11 = r11 - r6
            int r10 = r12.A04
            int r11 = r11 - r10
            int r10 = r5.A04
            int r11 = r11 * r10
            int r0 = r0 - r11
            goto L_0x01ee
        L_0x024e:
            boolean r0 = r7.A01
            if (r0 == 0) goto L_0x0260
            X.19k r0 = r5.A07
            int r10 = r0.A06()
        L_0x0258:
            X.19k r0 = r5.A07
            int r0 = r0.A09(r9)
            int r0 = r0 + r10
            goto L_0x01f6
        L_0x0260:
            int r10 = r12.A04
            int r0 = r5.A04
            int r10 = r10 * r0
            X.19k r0 = r5.A07
            int r0 = r0.A06()
            int r10 = r10 + r0
            goto L_0x0258
        L_0x026d:
            int r13 = r5.A04
            int r4 = r5.A05
            int r2 = r5.A0f()
            int r0 = r5.A0g()
            int r2 = r2 + r0
            int r0 = r7.width
            int r2 = X.AnonymousClass19T.A0I(r13, r4, r2, r0, r6)
            int r0 = r5.A00
            r5.A0Q(r9, r2, r0, r11)
            goto L_0x00b9
        L_0x0287:
            int r0 = r5.A01
            if (r0 != r6) goto L_0x0297
            int r4 = r5.A04
            int r2 = r5.A05
            int r0 = r7.width
            int r13 = X.AnonymousClass19T.A0I(r4, r2, r11, r0, r11)
            goto L_0x00a3
        L_0x0297:
            int r13 = r5.A04
            int r4 = r5.A05
            int r2 = r5.A0f()
            int r0 = r5.A0g()
            int r2 = r2 + r0
            int r0 = r7.width
            int r13 = X.AnonymousClass19T.A0I(r13, r4, r2, r0, r6)
            int r4 = r5.A04
            int r2 = r5.A02
            int r0 = r7.height
            int r0 = X.AnonymousClass19T.A0I(r4, r2, r11, r0, r11)
            r5.A0Q(r9, r13, r0, r11)
            goto L_0x00b9
        L_0x02b9:
            r5.A18(r9, r4)
            goto L_0x0098
        L_0x02be:
            int r0 = r3.A07
            boolean r0 = r5.A0b(r0)
            r15 = -1
            if (r0 == 0) goto L_0x02eb
            int r14 = r5.A05
            int r14 = r14 - r6
            r16 = -1
        L_0x02cc:
            int r0 = r3.A07
            r12 = 0
            if (r0 != r6) goto L_0x02f1
            r13 = 2147483647(0x7fffffff, float:NaN)
            X.19k r0 = r5.A06
            int r11 = r0.A06()
        L_0x02da:
            if (r14 == r15) goto L_0x0084
            X.Cas[] r0 = r5.A0G
            r2 = r0[r14]
            int r0 = r2.A05(r11)
            if (r0 >= r13) goto L_0x02e8
            r12 = r2
            r13 = r0
        L_0x02e8:
            int r14 = r14 + r16
            goto L_0x02da
        L_0x02eb:
            r14 = 0
            int r15 = r5.A05
            r16 = 1
            goto L_0x02cc
        L_0x02f1:
            r13 = -2147483648(0xffffffff80000000, float:-0.0)
            X.19k r0 = r5.A06
            int r11 = r0.A02()
        L_0x02f9:
            if (r14 == r15) goto L_0x0084
            X.Cas[] r0 = r5.A0G
            r2 = r0[r14]
            int r0 = r2.A06(r11)
            if (r0 <= r13) goto L_0x0307
            r12 = r2
            r13 = r0
        L_0x0307:
            int r14 = r14 + r16
            goto L_0x02f9
        L_0x030a:
            X.Cas[] r0 = r5.A0G
            r12 = r0[r2]
            goto L_0x008f
        L_0x0310:
            r2 = -1
            goto L_0x0072
        L_0x0313:
            X.19k r0 = r5.A06
            int r8 = r0.A06()
            goto L_0x002b
        L_0x031b:
            int r0 = r3.A07
            if (r0 != r6) goto L_0x0326
            int r1 = r3.A05
            int r0 = r3.A00
            int r1 = r1 + r0
            goto L_0x001c
        L_0x0326:
            int r1 = r3.A08
            int r0 = r3.A00
            int r1 = r1 - r0
            goto L_0x001c
        L_0x032d:
            if (r9 != 0) goto L_0x0336
            X.46r r1 = r5.A0I
            r0 = r19
            r5.A0T(r0, r1)
        L_0x0336:
            X.46r r0 = r5.A0I
            int r1 = r0.A07
            r0 = r18
            if (r1 != r0) goto L_0x0358
            X.19k r0 = r5.A06
            int r0 = r0.A06()
            int r2 = r5.A02(r0)
            X.19k r0 = r5.A06
            int r1 = r0.A06()
            int r1 = r1 - r2
        L_0x034f:
            if (r1 <= 0) goto L_0x0357
            int r0 = r3.A00
            int r4 = java.lang.Math.min(r0, r1)
        L_0x0357:
            return r4
        L_0x0358:
            X.19k r0 = r5.A06
            int r0 = r0.A02()
            int r1 = r5.A01(r0)
            X.19k r0 = r5.A06
            int r0 = r0.A02()
            int r1 = r1 - r0
            goto L_0x034f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20901Eg.A05(X.0vp, X.46r, X.0wD):int");
    }

    public static View A0C(C20901Eg r7, boolean z) {
        int A062 = r7.A06.A06();
        int A022 = r7.A06.A02();
        View view = null;
        for (int A0c = r7.A0c() - 1; A0c >= 0; A0c--) {
            View A0u = r7.A0u(A0c);
            int A0B2 = r7.A06.A0B(A0u);
            int A082 = r7.A06.A08(A0u);
            if (A082 > A062 && A0B2 < A022) {
                if (A082 <= A022 || !z) {
                    return A0u;
                }
                if (view == null) {
                    view = A0u;
                }
            }
        }
        return view;
    }

    public static View A0D(C20901Eg r8, boolean z) {
        int A062 = r8.A06.A06();
        int A022 = r8.A06.A02();
        int A0c = r8.A0c();
        View view = null;
        for (int i = 0; i < A0c; i++) {
            View A0u = r8.A0u(i);
            int A0B2 = r8.A06.A0B(A0u);
            if (r8.A06.A08(A0u) > A062 && A0B2 < A022) {
                if (A0B2 >= A062 || !z) {
                    return A0u;
                }
                if (view == null) {
                    view = A0u;
                }
            }
        }
        return view;
    }

    private void A0E() {
        if (this.A01 == 1 || !A0a()) {
            this.A0E = this.A0D;
        } else {
            this.A0E = !this.A0D;
        }
    }

    private void A0F(int i) {
        C860846r r4 = this.A0I;
        r4.A07 = i;
        boolean z = this.A0E;
        int i2 = 1;
        boolean z2 = false;
        if (i == -1) {
            z2 = true;
        }
        if (z != z2) {
            i2 = -1;
        }
        r4.A06 = i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000e, code lost:
        if (r1 == false) goto L_0x0010;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0M(int r7, X.C15930wD r8) {
        /*
            r6 = this;
            X.46r r0 = r6.A0I
            r3 = 0
            r0.A00 = r3
            r0.A04 = r7
            X.30i r0 = r6.A07
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0.A05
            r0 = 1
            if (r1 != 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            r4 = 1
            if (r0 == 0) goto L_0x0073
            int r2 = r8.A06
            r0 = -1
            if (r2 == r0) goto L_0x0073
            boolean r1 = r6.A0E
            r0 = 0
            if (r2 >= r7) goto L_0x001f
            r0 = 1
        L_0x001f:
            if (r1 != r0) goto L_0x006b
            X.19k r0 = r6.A06
            int r5 = r0.A07()
        L_0x0027:
            r0 = 0
        L_0x0028:
            boolean r1 = r6.A1U()
            if (r1 == 0) goto L_0x005c
            X.46r r2 = r6.A0I
            X.19k r1 = r6.A06
            int r1 = r1.A06()
            int r1 = r1 - r0
            r2.A08 = r1
            X.46r r1 = r6.A0I
            X.19k r0 = r6.A06
            int r0 = r0.A02()
            int r0 = r0 + r5
            r1.A05 = r0
        L_0x0044:
            X.46r r2 = r6.A0I
            r2.A03 = r3
            r2.A02 = r4
            X.19k r1 = r6.A06
            int r0 = r1.A04()
            if (r0 != 0) goto L_0x0059
            int r0 = r1.A01()
            if (r0 != 0) goto L_0x0059
            r3 = 1
        L_0x0059:
            r2.A01 = r3
            return
        L_0x005c:
            X.46r r2 = r6.A0I
            X.19k r1 = r6.A06
            int r1 = r1.A01()
            int r1 = r1 + r5
            r2.A05 = r1
            int r0 = -r0
            r2.A08 = r0
            goto L_0x0044
        L_0x006b:
            X.19k r0 = r6.A06
            int r0 = r0.A07()
            r5 = 0
            goto L_0x0028
        L_0x0073:
            r5 = 0
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20901Eg.A0M(int, X.0wD):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0048, code lost:
        if (X.AnonymousClass19T.A0P(r7.getMeasuredHeight(), r2, r4.height) == false) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0Q(android.view.View r7, int r8, int r9, boolean r10) {
        /*
            r6 = this;
            android.graphics.Rect r0 = r6.A0J
            r6.A1A(r7, r0)
            android.view.ViewGroup$LayoutParams r4 = r7.getLayoutParams()
            X.Cat r4 = (X.C25157Cat) r4
            int r3 = r4.leftMargin
            android.graphics.Rect r2 = r6.A0J
            int r0 = r2.left
            int r3 = r3 + r0
            int r1 = r4.rightMargin
            int r0 = r2.right
            int r1 = r1 + r0
            int r3 = A03(r8, r3, r1)
            int r5 = r4.topMargin
            android.graphics.Rect r2 = r6.A0J
            int r0 = r2.top
            int r5 = r5 + r0
            int r1 = r4.bottomMargin
            int r0 = r2.bottom
            int r1 = r1 + r0
            int r2 = A03(r9, r5, r1)
            if (r10 == 0) goto L_0x0051
            boolean r0 = r6.A0D
            if (r0 == 0) goto L_0x004a
            int r1 = r7.getMeasuredWidth()
            int r0 = r4.width
            boolean r0 = X.AnonymousClass19T.A0P(r1, r3, r0)
            if (r0 == 0) goto L_0x004a
            int r1 = r7.getMeasuredHeight()
            int r0 = r4.height
            boolean r1 = X.AnonymousClass19T.A0P(r1, r2, r0)
            r0 = 0
            if (r1 != 0) goto L_0x004b
        L_0x004a:
            r0 = 1
        L_0x004b:
            if (r0 == 0) goto L_0x0050
            r7.measure(r3, r2)
        L_0x0050:
            return
        L_0x0051:
            boolean r0 = r6.A1X(r7, r3, r2, r4)
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20901Eg.A0Q(android.view.View, int, int, boolean):void");
    }

    private void A0T(C15740vp r5, C860846r r6) {
        int min;
        int min2;
        if (r6.A02 && !r6.A01) {
            if (r6.A00 == 0) {
                if (r6.A07 == -1) {
                    A0R(r5, r6.A05);
                } else {
                    A0S(r5, r6.A08);
                }
            } else if (r6.A07 == -1) {
                int i = r6.A08;
                int A062 = this.A0G[0].A06(i);
                for (int i2 = 1; i2 < this.A05; i2++) {
                    int A063 = this.A0G[i2].A06(i);
                    if (A063 > A062) {
                        A062 = A063;
                    }
                }
                int i3 = i - A062;
                if (i3 < 0) {
                    min2 = r6.A05;
                } else {
                    min2 = r6.A05 - Math.min(i3, r6.A00);
                }
                A0R(r5, min2);
            } else {
                int i4 = r6.A05;
                int A052 = this.A0G[0].A05(i4);
                for (int i5 = 1; i5 < this.A05; i5++) {
                    int A053 = this.A0G[i5].A05(i4);
                    if (A053 < A052) {
                        A052 = A053;
                    }
                }
                int i6 = A052 - r6.A05;
                if (i6 < 0) {
                    min = r6.A08;
                } else {
                    min = Math.min(i6, r6.A00) + r6.A08;
                }
                A0S(r5, min);
            }
        }
    }

    private void A0U(C15740vp r4, C15930wD r5, boolean z) {
        int A022;
        int A012 = A01(Integer.MIN_VALUE);
        if (A012 != Integer.MIN_VALUE && (A022 = this.A06.A02() - A012) > 0) {
            int i = A022 - (-A04(-A022, r4, r5));
            if (z && i > 0) {
                this.A06.A0E(i);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        if (r2 != null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x03b7, code lost:
        if (A1t() != false) goto L_0x03b9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00e5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0W(X.C15740vp r12, X.C15930wD r13, boolean r14) {
        /*
            r11 = this;
            X.Cav r9 = r11.A0K
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r2 = r11.A09
            r1 = -1
            if (r2 != 0) goto L_0x000b
            int r0 = r11.A02
            if (r0 == r1) goto L_0x0018
        L_0x000b:
            int r0 = r13.A00()
            if (r0 != 0) goto L_0x0018
            r11.A1F(r12)
            r9.A00()
            return
        L_0x0018:
            boolean r0 = r9.A00
            r8 = 0
            r7 = 1
            if (r0 == 0) goto L_0x0025
            int r0 = r11.A02
            if (r0 != r1) goto L_0x0025
            r10 = 0
            if (r2 == 0) goto L_0x0026
        L_0x0025:
            r10 = 1
        L_0x0026:
            if (r10 == 0) goto L_0x0109
            r9.A00()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r3 = r11.A09
            if (r3 == 0) goto L_0x00b8
            int r2 = r3.A02
            if (r2 <= 0) goto L_0x0079
            int r0 = r11.A05
            if (r2 != r0) goto L_0x006a
            r3 = 0
        L_0x0038:
            int r0 = r11.A05
            if (r3 >= r0) goto L_0x0079
            X.Cas[] r0 = r11.A0G
            r0 = r0[r3]
            r0.A0A()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r4 = r11.A09
            int[] r0 = r4.A09
            r2 = r0[r3]
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r2 == r0) goto L_0x0058
            boolean r0 = r4.A05
            if (r0 == 0) goto L_0x0063
            X.19k r0 = r11.A06
            int r0 = r0.A02()
        L_0x0057:
            int r2 = r2 + r0
        L_0x0058:
            X.Cas[] r0 = r11.A0G
            r0 = r0[r3]
            r0.A01 = r2
            r0.A00 = r2
            int r3 = r3 + 1
            goto L_0x0038
        L_0x0063:
            X.19k r0 = r11.A06
            int r0 = r0.A06()
            goto L_0x0057
        L_0x006a:
            r0 = 0
            r3.A09 = r0
            r3.A02 = r8
            r3.A01 = r8
            r3.A08 = r0
            r3.A04 = r0
            int r0 = r3.A03
            r3.A00 = r0
        L_0x0079:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r2 = r11.A09
            boolean r0 = r2.A06
            r11.A0C = r0
            boolean r4 = r2.A07
            r0 = 0
            r11.A1r(r0)
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r2 = r11.A09
            if (r2 == 0) goto L_0x008f
            boolean r0 = r2.A07
            if (r0 == r4) goto L_0x008f
            r2.A07 = r4
        L_0x008f:
            r11.A0D = r4
            r11.A0y()
            r11.A0E()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r3 = r11.A09
            int r2 = r3.A00
            r0 = -1
            if (r2 == r0) goto L_0x00b3
            r11.A02 = r2
            boolean r0 = r3.A05
            r9.A04 = r0
        L_0x00a4:
            int r0 = r3.A01
            if (r0 <= r7) goto L_0x00bf
            X.BAu r2 = r11.A08
            int[] r0 = r3.A08
            r2.A01 = r0
            java.util.List r0 = r3.A04
            r2.A00 = r0
            goto L_0x00bf
        L_0x00b3:
            boolean r0 = r11.A0E
            r9.A04 = r0
            goto L_0x00a4
        L_0x00b8:
            r11.A0E()
            boolean r0 = r11.A0E
            r9.A04 = r0
        L_0x00bf:
            boolean r0 = r13.A08
            r5 = 0
            if (r0 != 0) goto L_0x023b
            int r4 = r11.A02
            if (r4 == r1) goto L_0x023b
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r4 < 0) goto L_0x0237
            int r0 = r13.A00()
            if (r4 >= r0) goto L_0x0237
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r2 = r11.A09
            if (r2 == 0) goto L_0x0171
            int r0 = r2.A00
            if (r0 == r1) goto L_0x0171
            int r0 = r2.A02
            if (r0 < r7) goto L_0x0171
            r9.A01 = r3
            r9.A02 = r4
        L_0x00e2:
            r0 = 1
        L_0x00e3:
            if (r0 != 0) goto L_0x0107
            boolean r0 = r11.A0B
            int r5 = r13.A00()
            if (r0 == 0) goto L_0x0158
            int r0 = r11.A0c()
            int r2 = r0 + -1
        L_0x00f3:
            if (r2 < 0) goto L_0x016f
            android.view.View r0 = r11.A0u(r2)
            int r0 = X.AnonymousClass19T.A0L(r0)
            if (r0 < 0) goto L_0x0155
            if (r0 >= r5) goto L_0x0155
        L_0x0101:
            r9.A02 = r0
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r9.A01 = r0
        L_0x0107:
            r9.A00 = r7
        L_0x0109:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r0 = r11.A09
            if (r0 != 0) goto L_0x0126
            int r0 = r11.A02
            if (r0 != r1) goto L_0x0126
            boolean r2 = r9.A04
            boolean r0 = r11.A0B
            if (r2 != r0) goto L_0x011f
            boolean r2 = r11.A0a()
            boolean r0 = r11.A0C
            if (r2 == r0) goto L_0x0126
        L_0x011f:
            X.BAu r0 = r11.A08
            r0.A03()
            r9.A03 = r7
        L_0x0126:
            int r0 = r11.A0c()
            if (r0 <= 0) goto L_0x02c8
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r0 = r11.A09
            if (r0 == 0) goto L_0x0134
            int r0 = r0.A02
            if (r0 >= r7) goto L_0x02c8
        L_0x0134:
            boolean r0 = r9.A03
            if (r0 == 0) goto L_0x023e
            r3 = 0
        L_0x0139:
            int r0 = r11.A05
            if (r3 >= r0) goto L_0x02c8
            X.Cas[] r0 = r11.A0G
            r0 = r0[r3]
            r0.A0A()
            int r2 = r9.A01
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r2 == r0) goto L_0x0152
            X.Cas[] r0 = r11.A0G
            r0 = r0[r3]
            r0.A01 = r2
            r0.A00 = r2
        L_0x0152:
            int r3 = r3 + 1
            goto L_0x0139
        L_0x0155:
            int r2 = r2 + -1
            goto L_0x00f3
        L_0x0158:
            int r3 = r11.A0c()
            r2 = 0
        L_0x015d:
            if (r2 >= r3) goto L_0x016f
            android.view.View r0 = r11.A0u(r2)
            int r0 = X.AnonymousClass19T.A0L(r0)
            if (r0 < 0) goto L_0x016c
            if (r0 >= r5) goto L_0x016c
            goto L_0x0101
        L_0x016c:
            int r2 = r2 + 1
            goto L_0x015d
        L_0x016f:
            r0 = 0
            goto L_0x0101
        L_0x0171:
            android.view.View r4 = r11.A0t(r4)
            if (r4 == 0) goto L_0x0203
            boolean r0 = r11.A0E
            if (r0 == 0) goto L_0x01ad
            int r0 = A07(r11)
        L_0x017f:
            r9.A02 = r0
            int r0 = r11.A03
            if (r0 == r3) goto L_0x01b2
            boolean r0 = r9.A04
            if (r0 == 0) goto L_0x019d
            X.19k r0 = r11.A06
            int r2 = r0.A02()
            int r0 = r11.A03
            int r2 = r2 - r0
            X.19k r0 = r11.A06
            int r0 = r0.A08(r4)
        L_0x0198:
            int r2 = r2 - r0
            r9.A01 = r2
            goto L_0x00e2
        L_0x019d:
            X.19k r0 = r11.A06
            int r2 = r0.A06()
            int r0 = r11.A03
            int r2 = r2 + r0
            X.19k r0 = r11.A06
            int r0 = r0.A0B(r4)
            goto L_0x0198
        L_0x01ad:
            int r0 = A06(r11)
            goto L_0x017f
        L_0x01b2:
            X.19k r0 = r11.A06
            int r2 = r0.A09(r4)
            X.19k r0 = r11.A06
            int r0 = r0.A07()
            if (r2 <= r0) goto L_0x01d6
            boolean r0 = r9.A04
            if (r0 == 0) goto L_0x01cf
            X.19k r0 = r11.A06
            int r0 = r0.A02()
        L_0x01ca:
            r9.A01 = r0
            r0 = 1
            goto L_0x00e3
        L_0x01cf:
            X.19k r0 = r11.A06
            int r0 = r0.A06()
            goto L_0x01ca
        L_0x01d6:
            X.19k r0 = r11.A06
            int r2 = r0.A0B(r4)
            X.19k r0 = r11.A06
            int r0 = r0.A06()
            int r2 = r2 - r0
            if (r2 >= 0) goto L_0x01eb
            int r0 = -r2
            r9.A01 = r0
            r0 = 1
            goto L_0x00e3
        L_0x01eb:
            X.19k r0 = r11.A06
            int r2 = r0.A02()
            X.19k r0 = r11.A06
            int r0 = r0.A08(r4)
            int r2 = r2 - r0
            if (r2 >= 0) goto L_0x01ff
            r9.A01 = r2
            r0 = 1
            goto L_0x00e3
        L_0x01ff:
            r9.A01 = r3
            goto L_0x00e2
        L_0x0203:
            int r0 = r11.A02
            r9.A02 = r0
            int r2 = r11.A03
            if (r2 != r3) goto L_0x021b
            int r0 = r11.A00(r0)
            if (r0 != r7) goto L_0x0212
            r5 = 1
        L_0x0212:
            r9.A04 = r5
            r9.A01()
        L_0x0217:
            r9.A03 = r7
            goto L_0x00e2
        L_0x021b:
            boolean r0 = r9.A04
            if (r0 == 0) goto L_0x022b
            X.1Eg r0 = r9.A06
            X.19k r0 = r0.A06
            int r0 = r0.A02()
            int r0 = r0 - r2
            r9.A01 = r0
            goto L_0x0217
        L_0x022b:
            X.1Eg r0 = r9.A06
            X.19k r0 = r0.A06
            int r0 = r0.A06()
            int r0 = r0 + r2
            r9.A01 = r0
            goto L_0x0217
        L_0x0237:
            r11.A02 = r1
            r11.A03 = r3
        L_0x023b:
            r0 = 0
            goto L_0x00e3
        L_0x023e:
            if (r10 != 0) goto L_0x0247
            X.Cav r0 = r11.A0K
            int[] r0 = r0.A05
            r3 = 0
            if (r0 != 0) goto L_0x02b0
        L_0x0247:
            r6 = 0
        L_0x0248:
            int r0 = r11.A05
            if (r6 >= r0) goto L_0x0289
            X.Cas[] r0 = r11.A0G
            r5 = r0[r6]
            boolean r10 = r11.A0E
            int r4 = r9.A01
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r10 == 0) goto L_0x0284
            int r2 = r5.A05(r3)
        L_0x025c:
            r5.A0A()
            if (r2 == r3) goto L_0x0279
            if (r10 == 0) goto L_0x026d
            X.1Eg r0 = r5.A05
            X.19k r0 = r0.A06
            int r0 = r0.A02()
            if (r2 < r0) goto L_0x0279
        L_0x026d:
            if (r10 != 0) goto L_0x027c
            X.1Eg r0 = r5.A05
            X.19k r0 = r0.A06
            int r0 = r0.A06()
            if (r2 <= r0) goto L_0x027c
        L_0x0279:
            int r6 = r6 + 1
            goto L_0x0248
        L_0x027c:
            if (r4 == r3) goto L_0x027f
            int r2 = r2 + r4
        L_0x027f:
            r5.A00 = r2
            r5.A01 = r2
            goto L_0x0279
        L_0x0284:
            int r2 = r5.A06(r3)
            goto L_0x025c
        L_0x0289:
            X.Cav r10 = r11.A0K
            X.Cas[] r6 = r11.A0G
            int r5 = r6.length
            int[] r0 = r10.A05
            if (r0 == 0) goto L_0x0295
            int r0 = r0.length
            if (r0 >= r5) goto L_0x029e
        L_0x0295:
            X.1Eg r0 = r10.A06
            X.Cas[] r0 = r0.A0G
            int r0 = r0.length
            int[] r0 = new int[r0]
            r10.A05 = r0
        L_0x029e:
            r4 = 0
        L_0x029f:
            if (r4 >= r5) goto L_0x02c8
            int[] r3 = r10.A05
            r2 = r6[r4]
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            int r0 = r2.A06(r0)
            r3[r4] = r0
            int r4 = r4 + 1
            goto L_0x029f
        L_0x02b0:
            int r0 = r11.A05
            if (r3 >= r0) goto L_0x02c8
            X.Cas[] r0 = r11.A0G
            r2 = r0[r3]
            r2.A0A()
            X.Cav r0 = r11.A0K
            int[] r0 = r0.A05
            r0 = r0[r3]
            r2.A01 = r0
            r2.A00 = r0
            int r3 = r3 + 1
            goto L_0x02b0
        L_0x02c8:
            r11.A1E(r12)
            X.46r r0 = r11.A0I
            r0.A02 = r8
            r11.A0H = r8
            X.19k r0 = r11.A07
            int r2 = r0.A07()
            int r0 = r11.A05
            int r0 = r2 / r0
            r11.A04 = r0
            X.19k r0 = r11.A07
            int r0 = r0.A04()
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r0)
            r11.A00 = r0
            int r0 = r9.A02
            r11.A0M(r0, r13)
            boolean r0 = r9.A04
            if (r0 == 0) goto L_0x0343
            r11.A0F(r1)
            X.46r r0 = r11.A0I
            r11.A05(r12, r0, r13)
            r11.A0F(r7)
        L_0x02fd:
            X.46r r2 = r11.A0I
            int r1 = r9.A02
            int r0 = r2.A06
            int r1 = r1 + r0
            r2.A04 = r1
            r11.A05(r12, r2, r13)
            X.19k r0 = r11.A07
            int r1 = r0.A04()
            r0 = 1073741824(0x40000000, float:2.0)
            if (r1 == r0) goto L_0x0381
            int r6 = r11.A0c()
            r5 = 0
            r3 = 0
            r1 = 0
        L_0x031a:
            if (r3 >= r6) goto L_0x034f
            android.view.View r4 = r11.A0u(r3)
            X.19k r0 = r11.A07
            int r0 = r0.A09(r4)
            float r2 = (float) r0
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0340
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            X.Cat r0 = (X.C25157Cat) r0
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x033c
            r0 = 1065353216(0x3f800000, float:1.0)
            float r2 = r2 * r0
            int r0 = r11.A05
            float r0 = (float) r0
            float r2 = r2 / r0
        L_0x033c:
            float r1 = java.lang.Math.max(r1, r2)
        L_0x0340:
            int r3 = r3 + 1
            goto L_0x031a
        L_0x0343:
            r11.A0F(r7)
            X.46r r0 = r11.A0I
            r11.A05(r12, r0, r13)
            r11.A0F(r1)
            goto L_0x02fd
        L_0x034f:
            int r4 = r11.A04
            int r0 = r11.A05
            float r0 = (float) r0
            float r1 = r1 * r0
            int r1 = java.lang.Math.round(r1)
            X.19k r3 = r11.A07
            int r2 = r3.A04()
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r2 != r0) goto L_0x036b
            int r0 = r3.A07()
            int r1 = java.lang.Math.min(r1, r0)
        L_0x036b:
            int r0 = r11.A05
            int r0 = r1 / r0
            r11.A04 = r0
            X.19k r0 = r11.A07
            int r0 = r0.A04()
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r0)
            r11.A00 = r0
            int r0 = r11.A04
            if (r0 != r4) goto L_0x03e2
        L_0x0381:
            int r0 = r11.A0c()
            if (r0 <= 0) goto L_0x0391
            boolean r0 = r11.A0E
            if (r0 == 0) goto L_0x03db
            r11.A0U(r12, r13, r7)
            r11.A0V(r12, r13, r8)
        L_0x0391:
            if (r14 == 0) goto L_0x03d9
            boolean r0 = r13.A08
            if (r0 != 0) goto L_0x03d9
            int r0 = r11.A0c()
            if (r0 <= 0) goto L_0x03d7
            boolean r0 = r11.A0H
            if (r0 != 0) goto L_0x03a7
            android.view.View r0 = r11.A0B()
            if (r0 == 0) goto L_0x03d7
        L_0x03a7:
            r0 = 1
        L_0x03a8:
            if (r0 == 0) goto L_0x03d9
            java.lang.Runnable r1 = r11.A0L
            androidx.recyclerview.widget.RecyclerView r0 = r11.A08
            if (r0 == 0) goto L_0x03b3
            r0.removeCallbacks(r1)
        L_0x03b3:
            boolean r0 = r11.A1t()
            if (r0 == 0) goto L_0x03d9
        L_0x03b9:
            boolean r0 = r13.A08
            if (r0 == 0) goto L_0x03c2
            X.Cav r0 = r11.A0K
            r0.A00()
        L_0x03c2:
            boolean r0 = r9.A04
            r11.A0B = r0
            boolean r0 = r11.A0a()
            r11.A0C = r0
            if (r7 == 0) goto L_0x03d6
            X.Cav r0 = r11.A0K
            r0.A00()
            r11.A0W(r12, r13, r8)
        L_0x03d6:
            return
        L_0x03d7:
            r0 = 0
            goto L_0x03a8
        L_0x03d9:
            r7 = 0
            goto L_0x03b9
        L_0x03db:
            r11.A0V(r12, r13, r7)
            r11.A0U(r12, r13, r8)
            goto L_0x0391
        L_0x03e2:
            if (r5 >= r6) goto L_0x0381
            android.view.View r3 = r11.A0u(r5)
            android.view.ViewGroup$LayoutParams r2 = r3.getLayoutParams()
            X.Cat r2 = (X.C25157Cat) r2
            boolean r0 = r2.A01
            if (r0 != 0) goto L_0x040d
            boolean r0 = r11.A0a()
            if (r0 == 0) goto L_0x0410
            int r0 = r11.A01
            if (r0 != r7) goto L_0x0410
            int r1 = r11.A05
            int r1 = r1 - r7
            X.Cas r0 = r2.A00
            int r0 = r0.A04
            int r1 = r1 - r0
            int r1 = -r1
            int r0 = r11.A04
            int r0 = r0 * r1
            int r1 = r1 * r4
            int r0 = r0 - r1
            r3.offsetLeftAndRight(r0)
        L_0x040d:
            int r5 = r5 + 1
            goto L_0x03e2
        L_0x0410:
            X.Cas r0 = r2.A00
            int r2 = r0.A04
            int r0 = r11.A04
            int r1 = r2 * r0
            int r2 = r2 * r4
            int r0 = r11.A01
            int r1 = r1 - r2
            if (r0 != r7) goto L_0x0422
            r3.offsetLeftAndRight(r1)
            goto L_0x040d
        L_0x0422:
            r3.offsetTopAndBottom(r1)
            goto L_0x040d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20901Eg.A0W(X.0vp, X.0wD, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    private void A0X(C25156Cas cas, int i, int i2) {
        int i3 = cas.A02;
        if (i == -1) {
            int i4 = cas.A01;
            if (i4 == Integer.MIN_VALUE) {
                C25156Cas.A02(cas);
                i4 = cas.A01;
            }
            if (i4 + i3 > i2) {
                return;
            }
        } else {
            int i5 = cas.A00;
            if (i5 == Integer.MIN_VALUE) {
                C25156Cas.A01(cas);
                i5 = cas.A00;
            }
            if (i5 - i3 < i2) {
                return;
            }
        }
        this.A0A.set(cas.A04, false);
    }

    public static void A0Y(C20901Eg r6, int i, int i2, int i3) {
        int A062;
        int i4;
        int i5;
        int A072;
        if (r6.A0E) {
            A062 = A07(r6);
        } else {
            A062 = A06(r6);
        }
        if (i3 == 8) {
            i4 = i2 + 1;
            if (i >= i2) {
                i4 = i + 1;
                i5 = i2;
            }
            i5 = i;
        } else {
            i4 = i + i2;
            i5 = i;
        }
        r6.A08.A05(i5);
        if (i3 == 1) {
            r6.A08.A06(i, i2);
        } else if (i3 == 2) {
            r6.A08.A07(i, i2);
        } else if (i3 == 8) {
            r6.A08.A07(i, 1);
            r6.A08.A06(i2, 1);
        }
        if (i4 > A062) {
            if (r6.A0E) {
                A072 = A06(r6);
            } else {
                A072 = A07(r6);
            }
            if (i5 <= A072) {
                r6.A0y();
            }
        }
    }

    private boolean A0a() {
        if (C15320v6.getLayoutDirection(this.A08) != 1) {
            return false;
        }
        return true;
    }

    private boolean A0b(int i) {
        if (this.A01 == 0) {
            boolean z = false;
            if (i == -1) {
                z = true;
            }
            if (z != this.A0E) {
                return true;
            }
            return false;
        }
        boolean z2 = false;
        if (i == -1) {
            z2 = true;
        }
        boolean z3 = false;
        if (z2 == this.A0E) {
            z3 = true;
        }
        if (z3 == A0a()) {
            return true;
        }
        return false;
    }

    public int A1d(C15740vp r3, C15930wD r4) {
        if (this.A01 == 1) {
            return this.A05;
        }
        return super.A1d(r3, r4);
    }

    public int A1e(C15740vp r2, C15930wD r3) {
        if (this.A01 == 0) {
            return this.A05;
        }
        return super.A1e(r2, r3);
    }

    public AnonymousClass1R7 A1g(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C25157Cat((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C25157Cat(layoutParams);
    }

    public void A1j(int i) {
        StaggeredGridLayoutManager$SavedState staggeredGridLayoutManager$SavedState = this.A09;
        if (!(staggeredGridLayoutManager$SavedState == null || staggeredGridLayoutManager$SavedState.A00 == i)) {
            staggeredGridLayoutManager$SavedState.A09 = null;
            staggeredGridLayoutManager$SavedState.A02 = 0;
            staggeredGridLayoutManager$SavedState.A00 = -1;
            staggeredGridLayoutManager$SavedState.A03 = -1;
        }
        this.A02 = i;
        this.A03 = Integer.MIN_VALUE;
        A0y();
    }

    public void A1q(RecyclerView recyclerView, C15930wD r4, int i) {
        C27360DbK dbK = new C27360DbK(recyclerView.getContext());
        dbK.A00 = i;
        A1I(dbK);
    }

    public void A1r(String str) {
        if (this.A09 == null) {
            super.A1r(str);
        }
    }

    public boolean A1s() {
        if (this.A09 == null) {
            return true;
        }
        return false;
    }

    public C20901Eg(int i, int i2) {
        this.A0D = false;
        this.A0E = false;
        this.A02 = -1;
        this.A03 = Integer.MIN_VALUE;
        this.A08 = new C22750BAu();
        this.A0J = new Rect();
        this.A0K = new C25159Cav(this);
        this.A0H = false;
        this.A0L = new C25162Cay(this);
        this.A01 = i2;
        A1r(null);
        if (i != this.A05) {
            this.A08.A03();
            A0y();
            this.A05 = i;
            this.A0A = new BitSet(i);
            C25156Cas[] casArr = new C25156Cas[i];
            this.A0G = casArr;
            for (int i3 = 0; i3 < i; i3++) {
                casArr[i3] = new C25156Cas(this, i3);
            }
            A0y();
        }
        this.A0I = new C860846r();
        this.A06 = C197219k.A00(this, this.A01);
        this.A07 = C197219k.A00(this, 1 - this.A01);
    }

    private int A00(int i) {
        if (A0c() != 0) {
            boolean z = false;
            if (i < A06(this)) {
                z = true;
            }
            if (z != this.A0E) {
                return -1;
            }
        } else if (this.A0E) {
            return 1;
        } else {
            return -1;
        }
        return 1;
    }

    private int A04(int i, C15740vp r5, C15930wD r6) {
        if (A0c() == 0 || i == 0) {
            return 0;
        }
        A0Z(this, i, r6);
        int A052 = A05(r5, this.A0I, r6);
        if (this.A0I.A00 >= A052) {
            int i2 = i;
            i = A052;
            if (i2 < 0) {
                i = -A052;
            }
        }
        this.A06.A0E(-i);
        this.A0B = this.A0E;
        C860846r r0 = this.A0I;
        r0.A00 = 0;
        A0T(r5, r0);
        return i;
    }

    public static int A06(C20901Eg r2) {
        if (r2.A0c() != 0) {
            return AnonymousClass19T.A0L(r2.A0u(0));
        }
        return 0;
    }

    public static int A07(C20901Eg r1) {
        int A0c = r1.A0c();
        if (A0c == 0) {
            return 0;
        }
        return AnonymousClass19T.A0L(r1.A0u(A0c - 1));
    }

    public static int A08(C20901Eg r4, C15930wD r5) {
        if (r4.A0c() == 0) {
            return 0;
        }
        C197219k r3 = r4.A06;
        View A0D2 = A0D(r4, false);
        View A0C2 = A0C(r4, false);
        if (r4.A0c() == 0 || r5.A00() == 0 || A0D2 == null || A0C2 == null) {
            return 0;
        }
        return Math.min(r3.A07(), r3.A08(A0C2) - r3.A0B(A0D2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001b, code lost:
        if (A0a() == false) goto L_0x001d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0096 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.view.View A0B() {
        /*
            r11 = this;
            int r6 = r11.A0c()
            r2 = 1
            int r6 = r6 - r2
            java.util.BitSet r5 = new java.util.BitSet
            int r1 = r11.A05
            r5.<init>(r1)
            r0 = 0
            r5.set(r0, r1, r2)
            int r0 = r11.A01
            r10 = -1
            if (r0 != r2) goto L_0x001d
            boolean r0 = r11.A0a()
            r9 = 1
            if (r0 != 0) goto L_0x001e
        L_0x001d:
            r9 = -1
        L_0x001e:
            boolean r0 = r11.A0E
            r4 = -1
            if (r0 != 0) goto L_0x0026
            int r4 = r6 + 1
            r6 = 0
        L_0x0026:
            if (r6 >= r4) goto L_0x0029
            r10 = 1
        L_0x0029:
            if (r6 == r4) goto L_0x00e5
            android.view.View r3 = r11.A0u(r6)
            android.view.ViewGroup$LayoutParams r8 = r3.getLayoutParams()
            X.Cat r8 = (X.C25157Cat) r8
            X.Cas r7 = r8.A00
            int r0 = r7.A04
            boolean r0 = r5.get(r0)
            if (r0 == 0) goto L_0x0078
            boolean r0 = r11.A0E
            r2 = 0
            if (r0 == 0) goto L_0x00c9
            int r1 = r7.A00
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 != r0) goto L_0x004f
            X.C25156Cas.A01(r7)
            int r1 = r7.A00
        L_0x004f:
            X.19k r0 = r11.A06
            int r0 = r0.A02()
            if (r1 >= r0) goto L_0x00e3
            java.util.ArrayList r1 = r7.A03
            int r0 = r1.size()
            int r0 = r0 + -1
            java.lang.Object r0 = r1.get(r0)
        L_0x0063:
            android.view.View r0 = (android.view.View) r0
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            X.Cat r0 = (X.C25157Cat) r0
            boolean r0 = r0.A01
            r0 = r0 ^ 1
        L_0x006f:
            if (r0 != 0) goto L_0x0096
            X.Cas r0 = r8.A00
            int r0 = r0.A04
            r5.clear(r0)
        L_0x0078:
            boolean r0 = r8.A01
            if (r0 != 0) goto L_0x00c6
            int r0 = r6 + r10
            if (r0 == r4) goto L_0x00c6
            android.view.View r7 = r11.A0u(r0)
            boolean r0 = r11.A0E
            if (r0 == 0) goto L_0x0097
            X.19k r0 = r11.A06
            int r2 = r0.A08(r3)
            X.19k r0 = r11.A06
            int r1 = r0.A08(r7)
            if (r2 >= r1) goto L_0x00a6
        L_0x0096:
            return r3
        L_0x0097:
            X.19k r0 = r11.A06
            int r2 = r0.A0B(r3)
            X.19k r0 = r11.A06
            int r1 = r0.A0B(r7)
            if (r2 <= r1) goto L_0x00a6
            return r3
        L_0x00a6:
            r0 = 0
            if (r2 != r1) goto L_0x00aa
            r0 = 1
        L_0x00aa:
            if (r0 == 0) goto L_0x00c6
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            X.Cat r1 = (X.C25157Cat) r1
            X.Cas r0 = r8.A00
            int r2 = r0.A04
            X.Cas r0 = r1.A00
            int r0 = r0.A04
            int r2 = r2 - r0
            r1 = 0
            if (r2 >= 0) goto L_0x00bf
            r1 = 1
        L_0x00bf:
            r0 = 0
            if (r9 >= 0) goto L_0x00c3
            r0 = 1
        L_0x00c3:
            if (r1 == r0) goto L_0x00c6
            return r3
        L_0x00c6:
            int r6 = r6 + r10
            goto L_0x0029
        L_0x00c9:
            int r1 = r7.A01
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 != r0) goto L_0x00d4
            X.C25156Cas.A02(r7)
            int r1 = r7.A01
        L_0x00d4:
            X.19k r0 = r11.A06
            int r0 = r0.A06()
            if (r1 <= r0) goto L_0x00e3
            java.util.ArrayList r0 = r7.A03
            java.lang.Object r0 = r0.get(r2)
            goto L_0x0063
        L_0x00e3:
            r0 = 0
            goto L_0x006f
        L_0x00e5:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20901Eg.A0B():android.view.View");
    }

    private void A0R(C15740vp r7, int i) {
        int A0c = A0c() - 1;
        while (A0c >= 0) {
            View A0u = A0u(A0c);
            if (this.A06.A0B(A0u) >= i && this.A06.A0D(A0u) >= i) {
                C25157Cat cat = (C25157Cat) A0u.getLayoutParams();
                if (cat.A01) {
                    int i2 = 0;
                    while (i2 < this.A05) {
                        if (this.A0G[i2].A03.size() != 1) {
                            i2++;
                        } else {
                            return;
                        }
                    }
                    for (int i3 = 0; i3 < this.A05; i3++) {
                        this.A0G[i3].A08();
                    }
                } else if (cat.A00.A03.size() != 1) {
                    cat.A00.A08();
                } else {
                    return;
                }
                A1C(A0u, r7);
                A0c--;
            } else {
                return;
            }
        }
    }

    private void A0S(C15740vp r6, int i) {
        while (A0c() > 0) {
            View A0u = A0u(0);
            if (this.A06.A08(A0u) <= i && this.A06.A0C(A0u) <= i) {
                C25157Cat cat = (C25157Cat) A0u.getLayoutParams();
                if (cat.A01) {
                    int i2 = 0;
                    while (i2 < this.A05) {
                        if (this.A0G[i2].A03.size() != 1) {
                            i2++;
                        } else {
                            return;
                        }
                    }
                    for (int i3 = 0; i3 < this.A05; i3++) {
                        this.A0G[i3].A09();
                    }
                } else if (cat.A00.A03.size() != 1) {
                    cat.A00.A09();
                } else {
                    return;
                }
                A1C(A0u, r6);
            } else {
                return;
            }
        }
    }

    private void A0V(C15740vp r4, C15930wD r5, boolean z) {
        int A062;
        int A022 = A02(Integer.MAX_VALUE);
        if (A022 != Integer.MAX_VALUE && (A062 = A022 - this.A06.A06()) > 0) {
            int A042 = A062 - A04(A062, r4, r5);
            if (z && A042 > 0) {
                this.A06.A0E(-A042);
            }
        }
    }

    public int A1b(int i, C15740vp r3, C15930wD r4) {
        return A04(i, r3, r4);
    }

    public int A1c(int i, C15740vp r3, C15930wD r4) {
        return A04(i, r3, r4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0083, code lost:
        if (r11.A01 == 1) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0088, code lost:
        if (r11.A01 == 0) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x008c, code lost:
        r1 = Integer.MIN_VALUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0097, code lost:
        if (A0a() != false) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a2, code lost:
        if (A0a() != false) goto L_0x00a4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A1f(android.view.View r12, int r13, X.C15740vp r14, X.C15930wD r15) {
        /*
            r11 = this;
            int r0 = r11.A0c()
            r10 = 0
            if (r0 == 0) goto L_0x0141
            android.view.View r2 = r11.A0v(r12)
            if (r2 == 0) goto L_0x0141
            r11.A0E()
            r1 = -1
            r8 = 1
            if (r13 == r8) goto L_0x008f
            r0 = 2
            if (r13 == r0) goto L_0x009a
            r0 = 17
            if (r13 == r0) goto L_0x0086
            r0 = 33
            if (r13 == r0) goto L_0x0081
            r0 = 66
            if (r13 == r0) goto L_0x0079
            r0 = 130(0x82, float:1.82E-43)
            if (r13 != r0) goto L_0x007e
            int r0 = r11.A01
            if (r0 != r8) goto L_0x007e
        L_0x002b:
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r8 == r0) goto L_0x0141
            android.view.ViewGroup$LayoutParams r0 = r2.getLayoutParams()
            X.Cat r0 = (X.C25157Cat) r0
            boolean r9 = r0.A01
            X.Cas r5 = r0.A00
            r7 = 1
            if (r8 != r7) goto L_0x0074
            int r3 = A07(r11)
        L_0x0040:
            r11.A0M(r3, r15)
            r11.A0F(r8)
            X.46r r4 = r11.A0I
            int r0 = r4.A06
            int r0 = r0 + r3
            r4.A04 = r0
            r1 = 1051372203(0x3eaaaaab, float:0.33333334)
            X.19k r0 = r11.A06
            int r0 = r0.A07()
            float r0 = (float) r0
            float r0 = r0 * r1
            int r0 = (int) r0
            r4.A00 = r0
            X.46r r0 = r11.A0I
            r0.A03 = r7
            r4 = 0
            r0.A02 = r4
            r11.A05(r14, r0, r15)
            boolean r0 = r11.A0E
            r11.A0B = r0
            if (r9 != 0) goto L_0x00a6
            android.view.View r0 = r5.A07(r3, r8)
            if (r0 == 0) goto L_0x00a6
            if (r0 == r2) goto L_0x00a6
            return r0
        L_0x0074:
            int r3 = A06(r11)
            goto L_0x0040
        L_0x0079:
            int r0 = r11.A01
            if (r0 != 0) goto L_0x007e
            goto L_0x002b
        L_0x007e:
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x002b
        L_0x0081:
            int r0 = r11.A01
            if (r0 != r8) goto L_0x008c
            goto L_0x008a
        L_0x0086:
            int r0 = r11.A01
            if (r0 != 0) goto L_0x008c
        L_0x008a:
            r8 = r1
            goto L_0x002b
        L_0x008c:
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x008a
        L_0x008f:
            int r0 = r11.A01
            if (r0 == r8) goto L_0x00a4
            boolean r0 = r11.A0a()
            if (r0 == 0) goto L_0x00a4
            goto L_0x002b
        L_0x009a:
            int r0 = r11.A01
            if (r0 == r8) goto L_0x002b
            boolean r0 = r11.A0a()
            if (r0 == 0) goto L_0x002b
        L_0x00a4:
            r8 = -1
            goto L_0x002b
        L_0x00a6:
            boolean r0 = r11.A0b(r8)
            if (r0 == 0) goto L_0x00c1
            int r1 = r11.A05
            int r1 = r1 - r7
        L_0x00af:
            if (r1 < 0) goto L_0x00d6
            X.Cas[] r0 = r11.A0G
            r0 = r0[r1]
            android.view.View r0 = r0.A07(r3, r8)
            if (r0 == 0) goto L_0x00be
            if (r0 == r2) goto L_0x00be
            return r0
        L_0x00be:
            int r1 = r1 + -1
            goto L_0x00af
        L_0x00c1:
            r1 = 0
        L_0x00c2:
            int r0 = r11.A05
            if (r1 >= r0) goto L_0x00d6
            X.Cas[] r0 = r11.A0G
            r0 = r0[r1]
            android.view.View r0 = r0.A07(r3, r8)
            if (r0 == 0) goto L_0x00d3
            if (r0 == r2) goto L_0x00d3
            return r0
        L_0x00d3:
            int r1 = r1 + 1
            goto L_0x00c2
        L_0x00d6:
            boolean r6 = r11.A0D
            r6 = r6 ^ r7
            r1 = -1
            r0 = 0
            if (r8 != r1) goto L_0x00de
            r0 = 1
        L_0x00de:
            r3 = 0
            if (r6 != r0) goto L_0x00e2
            r3 = 1
        L_0x00e2:
            if (r9 != 0) goto L_0x00f8
            if (r3 == 0) goto L_0x00f3
            int r0 = r5.A03()
        L_0x00ea:
            android.view.View r0 = r11.A0t(r0)
            if (r0 == 0) goto L_0x00f8
            if (r0 == r2) goto L_0x00f8
            return r0
        L_0x00f3:
            int r0 = r5.A04()
            goto L_0x00ea
        L_0x00f8:
            boolean r0 = r11.A0b(r8)
            if (r0 == 0) goto L_0x0122
            int r1 = r11.A05
            int r1 = r1 - r7
        L_0x0101:
            if (r1 < 0) goto L_0x0141
            int r0 = r5.A04
            if (r1 == r0) goto L_0x011f
            X.Cas[] r0 = r11.A0G
            r0 = r0[r1]
            if (r3 == 0) goto L_0x011a
            int r0 = r0.A03()
        L_0x0111:
            android.view.View r0 = r11.A0t(r0)
            if (r0 == 0) goto L_0x011f
            if (r0 == r2) goto L_0x011f
            return r0
        L_0x011a:
            int r0 = r0.A04()
            goto L_0x0111
        L_0x011f:
            int r1 = r1 + -1
            goto L_0x0101
        L_0x0122:
            int r0 = r11.A05
            if (r4 >= r0) goto L_0x0141
            X.Cas[] r0 = r11.A0G
            r0 = r0[r4]
            if (r3 == 0) goto L_0x013c
            int r0 = r0.A03()
        L_0x0130:
            android.view.View r0 = r11.A0t(r0)
            if (r0 == 0) goto L_0x0139
            if (r0 == r2) goto L_0x0139
            return r0
        L_0x0139:
            int r4 = r4 + 1
            goto L_0x0122
        L_0x013c:
            int r0 = r0.A04()
            goto L_0x0130
        L_0x0141:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20901Eg.A1f(android.view.View, int, X.0vp, X.0wD):android.view.View");
    }

    public void A1h(int i) {
        super.A1h(i);
        for (int i2 = 0; i2 < this.A05; i2++) {
            C25156Cas cas = this.A0G[i2];
            int i3 = cas.A01;
            if (i3 != Integer.MIN_VALUE) {
                cas.A01 = i3 + i;
            }
            int i4 = cas.A00;
            if (i4 != Integer.MIN_VALUE) {
                cas.A00 = i4 + i;
            }
        }
    }

    public void A1i(int i) {
        super.A1i(i);
        for (int i2 = 0; i2 < this.A05; i2++) {
            C25156Cas cas = this.A0G[i2];
            int i3 = cas.A01;
            if (i3 != Integer.MIN_VALUE) {
                cas.A01 = i3 + i;
            }
            int i4 = cas.A00;
            if (i4 != Integer.MIN_VALUE) {
                cas.A00 = i4 + i;
            }
        }
    }

    public void A1k(Rect rect, int i, int i2) {
        int A0H2;
        int A0H3;
        int A0f = A0f() + A0g();
        int A0h = A0h() + A0e();
        if (this.A01 == 1) {
            A0H3 = AnonymousClass19T.A0H(i2, rect.height() + A0h, C15320v6.getMinimumHeight(this.A08));
            A0H2 = AnonymousClass19T.A0H(i, (this.A04 * this.A05) + A0f, C15320v6.getMinimumWidth(this.A08));
        } else {
            A0H2 = AnonymousClass19T.A0H(i, rect.width() + A0f, C15320v6.getMinimumWidth(this.A08));
            A0H3 = AnonymousClass19T.A0H(i2, (this.A04 * this.A05) + A0h, C15320v6.getMinimumHeight(this.A08));
        }
        this.A08.setMeasuredDimension(A0H2, A0H3);
    }

    public void A1l(AccessibilityEvent accessibilityEvent) {
        super.A1l(accessibilityEvent);
        if (A0c() > 0) {
            View A0D2 = A0D(this, false);
            View A0C2 = A0C(this, false);
            if (A0D2 != null && A0C2 != null) {
                int A0L2 = AnonymousClass19T.A0L(A0D2);
                int A0L3 = AnonymousClass19T.A0L(A0C2);
                if (A0L2 < A0L3) {
                    accessibilityEvent.setFromIndex(A0L2);
                    accessibilityEvent.setToIndex(A0L3);
                    return;
                }
                accessibilityEvent.setFromIndex(A0L3);
                accessibilityEvent.setToIndex(A0L2);
            }
        }
    }

    public void A1n(C15740vp r9, C15930wD r10, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        int i;
        int i2;
        int i3;
        int i4;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof C25157Cat)) {
            super.A1B(view, accessibilityNodeInfoCompat);
            return;
        }
        C25157Cat cat = (C25157Cat) layoutParams;
        if (this.A01 == 0) {
            C25156Cas cas = cat.A00;
            if (cas == null) {
                i3 = -1;
            } else {
                i3 = cas.A04;
            }
            boolean z = cat.A01;
            if (z) {
                i4 = this.A05;
            } else {
                i4 = 1;
            }
            accessibilityNodeInfoCompat.A0Q(AnonymousClass4CJ.A00(i3, i4, -1, -1, z, false));
            return;
        }
        C25156Cas cas2 = cat.A00;
        if (cas2 == null) {
            i = -1;
        } else {
            i = cas2.A04;
        }
        boolean z2 = cat.A01;
        if (z2) {
            i2 = this.A05;
        } else {
            i2 = 1;
        }
        accessibilityNodeInfoCompat.A0Q(AnonymousClass4CJ.A00(-1, -1, i, i2, z2, false));
    }

    public void A1o(C15930wD r2) {
        super.A1o(r2);
        this.A02 = -1;
        this.A03 = Integer.MIN_VALUE;
        this.A09 = null;
        this.A0K.A00();
    }

    public void A1p(RecyclerView recyclerView, C15740vp r4) {
        super.A1p(recyclerView, r4);
        Runnable runnable = this.A0L;
        RecyclerView recyclerView2 = this.A08;
        if (recyclerView2 != null) {
            recyclerView2.removeCallbacks(runnable);
        }
        for (int i = 0; i < this.A05; i++) {
            this.A0G[i].A0A();
        }
        recyclerView.requestLayout();
    }

    public boolean A1t() {
        int A062;
        int A072;
        if (A0c() != 0 && this.A0B) {
            if (this.A0E) {
                A062 = A07(this);
                A072 = A06(this);
            } else {
                A062 = A06(this);
                A072 = A07(this);
            }
            if (A062 == 0 && A0B() != null) {
                this.A08.A03();
            } else if (this.A0H) {
                int i = 1;
                if (this.A0E) {
                    i = -1;
                }
                int i2 = A072 + 1;
                StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem A022 = this.A08.A02(A062, i2, i, true);
                if (A022 == null) {
                    this.A0H = false;
                    this.A08.A04(i2);
                    return false;
                }
                StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem A023 = this.A08.A02(A062, A022.A02, -i, true);
                if (A023 == null) {
                    this.A08.A04(A022.A02);
                } else {
                    this.A08.A04(A023.A02 + 1);
                }
            }
            this.A0F = true;
            A0y();
            return true;
        }
        return false;
    }

    public PointF ATk(int i) {
        int A002 = A00(i);
        PointF pointF = new PointF();
        if (A002 == 0) {
            return null;
        }
        if (this.A01 == 0) {
            pointF.x = (float) A002;
            pointF.y = 0.0f;
            return pointF;
        }
        pointF.x = 0.0f;
        pointF.y = (float) A002;
        return pointF;
    }
}
