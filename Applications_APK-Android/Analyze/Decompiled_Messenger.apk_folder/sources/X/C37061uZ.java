package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1uZ  reason: invalid class name and case insensitive filesystem */
public final class C37061uZ extends C09260gp {
    private static volatile C37061uZ A00;

    public static final C37061uZ A00(AnonymousClass1XY r7) {
        if (A00 == null) {
            synchronized (C37061uZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A00 = new C37061uZ(AnonymousClass1YA.A00(applicationInjector), C04720Vx.A01(applicationInjector), C09280gs.A00(applicationInjector), new C52532jG());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C37061uZ(android.content.Context r7, X.C04740Vz r8, X.C09280gs r9, X.C52532jG r10) {
        /*
            r6 = this;
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r10)
            java.lang.String r5 = "matching_db"
            r0 = r6
            r2 = r8
            r3 = r9
            r1 = r7
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37061uZ.<init>(android.content.Context, X.0Vz, X.0gs, X.2jG):void");
    }
}
