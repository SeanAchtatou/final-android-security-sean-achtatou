package X;

import android.content.Context;
import com.facebook.common.util.TriState;
import com.facebook.mobileconfig.init.MobileConfigInit;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0g1  reason: invalid class name and case insensitive filesystem */
public final class C08830g1 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.mobileconfig.init.MobileConfigInit$2";
    public final /* synthetic */ MobileConfigInit A00;

    public C08830g1(MobileConfigInit mobileConfigInit) {
        this.A00 = mobileConfigInit;
    }

    public void run() {
        MobileConfigInit mobileConfigInit = this.A00;
        if ((mobileConfigInit.A02.get() instanceof C25041Yc) && ((C25051Yd) mobileConfigInit.A02.get()).Aem(285099929245049L)) {
            ((C25041Yc) mobileConfigInit.A02.get()).A0A.set(true);
        }
        MobileConfigInit mobileConfigInit2 = this.A00;
        ((C25051Yd) mobileConfigInit2.A02.get()).Aem(286878045576464L);
        ((C25051Yd) mobileConfigInit2.A02.get()).Aem(282767761868651L);
        ((C25051Yd) mobileConfigInit2.A02.get()).B4C(845717715419316L);
        MobileConfigInit mobileConfigInit3 = this.A00;
        int i = AnonymousClass1Y3.Amd;
        AnonymousClass0UN r2 = mobileConfigInit3.A00;
        if (((TriState) AnonymousClass1XX.A02(8, i, r2)).equals(TriState.YES)) {
            ((ScheduledExecutorService) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ADt, r2)).scheduleWithFixedDelay(new AnonymousClass95f(mobileConfigInit3), 1, 30, TimeUnit.MINUTES);
        } else {
            mobileConfigInit3.A07();
        }
        MobileConfigInit mobileConfigInit4 = this.A00;
        AnonymousClass08X.A04((Context) mobileConfigInit4.A01.get(), "consistencyLoggingInterval", (int) ((C25051Yd) mobileConfigInit4.A02.get()).At0(566574905820877L));
        MobileConfigInit mobileConfigInit5 = this.A00;
        AnonymousClass08X.A05((Context) mobileConfigInit5.A01.get(), "useTranslationTablePerJavaManager", ((C25051Yd) mobileConfigInit5.A02.get()).Aem(285099929310586L));
    }
}
