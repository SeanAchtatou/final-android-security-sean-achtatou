package X;

/* renamed from: X.0jm  reason: invalid class name and case insensitive filesystem */
public interface C10240jm {
    int appendQuotedUTF8(byte[] bArr, int i);

    char[] asQuotedChars();

    byte[] asQuotedUTF8();

    byte[] asUnquotedUTF8();

    String getValue();
}
