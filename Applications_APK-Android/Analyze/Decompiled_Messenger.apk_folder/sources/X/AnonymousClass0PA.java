package X;

import java.io.File;
import java.util.Comparator;

/* renamed from: X.0PA  reason: invalid class name */
public final class AnonymousClass0PA implements Comparator {
    public int compare(Object obj, Object obj2) {
        long lastModified = ((File) obj).lastModified();
        long lastModified2 = ((File) obj2).lastModified();
        if (lastModified == lastModified2) {
            return 0;
        }
        if (lastModified < lastModified2) {
            return 1;
        }
        return -1;
    }
}
