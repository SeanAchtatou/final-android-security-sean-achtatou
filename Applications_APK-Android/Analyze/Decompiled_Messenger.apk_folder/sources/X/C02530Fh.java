package X;

import android.content.Context;

/* renamed from: X.0Fh  reason: invalid class name and case insensitive filesystem */
public final class C02530Fh extends C007907e {
    private boolean A00 = true;
    private final C02540Fi A01;
    private final long[] A02;
    private final long[] A03;

    private static void A00(C02520Fg r4, long[] jArr, int i) {
        r4.mobileBytesTx += jArr[i | 3];
        r4.mobileBytesRx += jArr[i | 2];
        r4.wifiBytesTx += jArr[i | 1];
        r4.wifiBytesRx += jArr[i | 0];
    }

    public AnonymousClass0FM A03() {
        return new C02520Fg();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0065, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(X.AnonymousClass0FM r10) {
        /*
            r9 = this;
            X.0Fg r10 = (X.C02520Fg) r10
            monitor-enter(r9)
            boolean r0 = r9.A00     // Catch:{ all -> 0x0066 }
            r4 = 0
            if (r0 == 0) goto L_0x0064
            X.0Fi r1 = r9.A01     // Catch:{ all -> 0x0066 }
            long[] r0 = r9.A02     // Catch:{ all -> 0x0066 }
            boolean r0 = r1.A03(r0)     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x0064
            long[] r8 = r9.A02     // Catch:{ all -> 0x0066 }
            long[] r7 = r9.A03     // Catch:{ all -> 0x0066 }
            r3 = 0
        L_0x0017:
            int r0 = r8.length     // Catch:{ all -> 0x0066 }
            if (r3 >= r0) goto L_0x003e
            r5 = r8[r3]     // Catch:{ all -> 0x0066 }
            r1 = r7[r3]     // Catch:{ all -> 0x0066 }
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0039
            java.lang.String r3 = "Network Bytes decreased from "
            java.lang.String r2 = java.util.Arrays.toString(r7)     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = " to "
            java.lang.String r0 = java.util.Arrays.toString(r8)     // Catch:{ all -> 0x0066 }
            java.lang.String r2 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = "NetworkMetricsCollector"
            r0 = 0
            X.AnonymousClass0KZ.A00(r1, r2, r0)     // Catch:{ all -> 0x0066 }
            goto L_0x003c
        L_0x0039:
            int r3 = r3 + 1
            goto L_0x0017
        L_0x003c:
            r0 = 0
            goto L_0x0042
        L_0x003e:
            java.lang.System.arraycopy(r8, r4, r7, r4, r0)     // Catch:{ all -> 0x0066 }
            r0 = 1
        L_0x0042:
            r9.A00 = r0     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x0064
            X.0Fi r0 = r9.A01     // Catch:{ all -> 0x0066 }
            boolean r2 = r0.A02()     // Catch:{ all -> 0x0066 }
            r0 = 0
            r10.mobileBytesTx = r0     // Catch:{ all -> 0x0066 }
            r10.mobileBytesRx = r0     // Catch:{ all -> 0x0066 }
            r10.wifiBytesTx = r0     // Catch:{ all -> 0x0066 }
            r10.wifiBytesRx = r0     // Catch:{ all -> 0x0066 }
            long[] r1 = r9.A02     // Catch:{ all -> 0x0066 }
            A00(r10, r1, r4)     // Catch:{ all -> 0x0066 }
            if (r2 == 0) goto L_0x0061
            r0 = 4
            A00(r10, r1, r0)     // Catch:{ all -> 0x0066 }
        L_0x0061:
            r0 = 1
            monitor-exit(r9)
            return r0
        L_0x0064:
            monitor-exit(r9)
            return r4
        L_0x0066:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02530Fh.A04(X.0FM):boolean");
    }

    public C02530Fh(Context context) {
        this.A01 = C02540Fi.A00(context);
        this.A02 = new long[8];
        this.A03 = new long[8];
    }
}
