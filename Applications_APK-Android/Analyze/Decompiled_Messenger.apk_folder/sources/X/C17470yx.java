package X;

import android.animation.StateListAnimator;
import android.content.res.TypedArray;
import android.graphics.PathEffect;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0yx  reason: invalid class name and case insensitive filesystem */
public interface C17470yx extends C17480yy, C17490yz, C31391jc, C22571Lz, C17500z0 {
    void ANp(C17760zQ r1);

    void ANs(List list);

    C17470yx ANw(C14940uO r1);

    C17470yx ANx(C14940uO r1);

    void AOS(C17770zR r1);

    void AOU(C17770zR r1);

    void AOc(TypedArray typedArray);

    boolean AOl();

    void AOw();

    C17470yx API(Drawable drawable);

    C17470yx AQW(AnonymousClass38W r1);

    void AQX(AnonymousClass1IF r1, int[] iArr, float[] fArr);

    void AR7(float f, float f2);

    C17470yx ASH(C17770zR r1);

    C17470yx AWl();

    C17470yx AY1(boolean z);

    C17470yx Aa9(AnonymousClass10X r1);

    C17470yx AaM(AnonymousClass10N r1);

    C17470yx Aac(Drawable drawable);

    C17470yx Aaj(AnonymousClass10N r1);

    int[] Aeu();

    PathEffect Aew();

    float[] Aex();

    C17470yx Ah3(int i);

    int Ah4();

    List Ahs();

    ArrayList Ahu();

    AnonymousClass0p4 AiS();

    C22571Lz AkV();

    AnonymousClass10N AnI();

    Drawable AnL();

    AnonymousClass10N Anq();

    C17770zR AoX();

    int Apg();

    AnonymousClass10N Aqk();

    int Ary();

    float As1();

    float As2();

    int AsD();

    int AsF(AnonymousClass10G r1);

    C17470yx AvV();

    C31401jd Avp();

    AnonymousClass14K AwP();

    C31401jd AwQ();

    C17470yx AxD();

    AnonymousClass1KE Axp();

    StateListAnimator B3m();

    int B3n();

    C17660zG B4P();

    float B4Q();

    float B4V();

    C17770zR B5K();

    String B5X();

    int B6k();

    int B6l();

    int B6m();

    int B6n();

    String B73();

    Integer B74();

    String B75();

    ArrayList B76();

    AnonymousClass10N B7X();

    List B7d();

    AnonymousClass10N B9W();

    AnonymousClass10N B9Y();

    float B9Z();

    float B9b();

    ArrayList BA2();

    AnonymousClass10W BAF();

    boolean BBY();

    boolean BBZ();

    boolean BBs();

    boolean BBu();

    boolean BBv();

    boolean BBx();

    C17470yx BCN(int i);

    C17470yx BDS(AnonymousClass10N r1);

    boolean BER();

    boolean BF5();

    boolean BFL();

    boolean BFQ();

    boolean BFZ();

    boolean BFx();

    C17470yx BHb(C14950uP r1);

    void BJz(AnonymousClass1KE r1);

    void BK0();

    C17470yx Bzu(AnonymousClass0p4 r1, C17770zR r2);

    C17660zG C0A();

    void C0S(C49912d4 r1);

    void C6X(boolean z);

    void C7h(C22571Lz r1);

    void C8l(int i);

    void C8m(float f);

    void C8n(float f);

    void C8r(int i);

    void C9P(AnonymousClass119 r1);

    void C9n(C17470yx r1);

    void C9r(C31401jd r1);

    void CC0(int i);

    void CC1(int i);

    boolean CDz();

    C17470yx CHg(StateListAnimator stateListAnimator);

    C17470yx CHh(int i);

    C17470yx CIv(String str);

    C17470yx CJD(AnonymousClass10G r1, int i);

    C17470yx CJN(String str, String str2);

    C17470yx CJO(Integer num);

    C17470yx CJe(AnonymousClass10N r1);

    C17470yx CLh(AnonymousClass10N r1);

    C17470yx CLi(AnonymousClass10N r1);

    C17470yx CLj(float f);

    C17470yx CLk(float f);

    C17470yx CNU(AnonymousClass6I0 r1);

    C17470yx CNW();

    String getSimpleName();
}
