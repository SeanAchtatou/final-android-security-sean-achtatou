package X;

import java.util.LinkedHashMap;
import java.util.Locale;

/* renamed from: X.0wE  reason: invalid class name and case insensitive filesystem */
public class C15940wE {
    public int A00;
    private int A01;
    private int A02;
    private int A03;
    private int A04;
    private int A05;
    public final LinkedHashMap A06;

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0060, code lost:
        throw new java.lang.IllegalStateException(X.AnonymousClass08S.A0J(r4.getClass().getName(), ".sizeOf() is reporting inconsistent results!"));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C15940wE r4, int r5) {
        /*
        L_0x0000:
            monitor-enter(r4)
            int r0 = r4.A00     // Catch:{ all -> 0x0061 }
            if (r0 < 0) goto L_0x004d
            java.util.LinkedHashMap r0 = r4.A06     // Catch:{ all -> 0x0061 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0061 }
            if (r0 == 0) goto L_0x0011
            int r0 = r4.A00     // Catch:{ all -> 0x0061 }
            if (r0 != 0) goto L_0x004d
        L_0x0011:
            int r0 = r4.A00     // Catch:{ all -> 0x0061 }
            if (r0 <= r5) goto L_0x004b
            java.util.LinkedHashMap r0 = r4.A06     // Catch:{ all -> 0x0061 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0061 }
            if (r0 != 0) goto L_0x004b
            java.util.LinkedHashMap r0 = r4.A06     // Catch:{ all -> 0x0061 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x0061 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0061 }
            java.lang.Object r0 = r0.next()     // Catch:{ all -> 0x0061 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x0061 }
            java.lang.Object r3 = r0.getKey()     // Catch:{ all -> 0x0061 }
            java.lang.Object r2 = r0.getValue()     // Catch:{ all -> 0x0061 }
            java.util.LinkedHashMap r0 = r4.A06     // Catch:{ all -> 0x0061 }
            r0.remove(r3)     // Catch:{ all -> 0x0061 }
            int r1 = r4.A00     // Catch:{ all -> 0x0061 }
            int r0 = A00(r4, r3, r2)     // Catch:{ all -> 0x0061 }
            int r1 = r1 - r0
            r4.A00 = r1     // Catch:{ all -> 0x0061 }
            int r1 = r4.A01     // Catch:{ all -> 0x0061 }
            r0 = 1
            int r1 = r1 + r0
            r4.A01 = r1     // Catch:{ all -> 0x0061 }
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            goto L_0x0000
        L_0x004b:
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            return
        L_0x004d:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0061 }
            java.lang.Class r0 = r4.getClass()     // Catch:{ all -> 0x0061 }
            java.lang.String r1 = r0.getName()     // Catch:{ all -> 0x0061 }
            java.lang.String r0 = ".sizeOf() is reporting inconsistent results!"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0061 }
            r2.<init>(r0)     // Catch:{ all -> 0x0061 }
            throw r2     // Catch:{ all -> 0x0061 }
        L_0x0061:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15940wE.A01(X.0wE, int):void");
    }

    public int A02(Object obj, Object obj2) {
        if ((this instanceof C17530z3) && (obj2 instanceof String)) {
            return ((String) obj2).length();
        }
        return 1;
    }

    public final synchronized String toString() {
        int i;
        int i2;
        int i3;
        i = this.A02;
        i2 = this.A04;
        int i4 = i + i2;
        if (i4 != 0) {
            i3 = (i * 100) / i4;
        } else {
            i3 = 0;
        }
        return String.format(Locale.US, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", Integer.valueOf(this.A03), Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
    }

    public final Object A03(Object obj) {
        if (obj != null) {
            synchronized (this) {
                Object obj2 = this.A06.get(obj);
                if (obj2 != null) {
                    this.A02++;
                    return obj2;
                }
                this.A04++;
                return null;
            }
        }
        throw new NullPointerException("key == null");
    }

    public final void A04(Object obj, Object obj2) {
        if (obj == null || obj2 == null) {
            throw new NullPointerException("key == null || value == null");
        }
        synchronized (this) {
            this.A05++;
            this.A00 += A00(this, obj, obj2);
            Object put = this.A06.put(obj, obj2);
            if (put != null) {
                this.A00 -= A00(this, obj, put);
            }
        }
        A01(this, this.A03);
    }

    public C15940wE(int i) {
        if (i > 0) {
            this.A03 = i;
            this.A06 = new LinkedHashMap(0, 0.75f, true);
            return;
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }

    public static int A00(C15940wE r2, Object obj, Object obj2) {
        int A022 = r2.A02(obj, obj2);
        if (A022 >= 0) {
            return A022;
        }
        throw new IllegalStateException("Negative size: " + obj + "=" + obj2);
    }
}
