package X;

import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1GV  reason: invalid class name */
public final class AnonymousClass1GV extends C16070wR {
    @Comparable(type = AnonymousClass1Y3.A01)
    public C17770zR A00;
    @Comparable(type = 13)
    public Boolean A01;
    @Comparable(type = 13)
    public Boolean A02;
    @Comparable(type = 13)
    public Integer A03;

    public AnonymousClass1GV() {
        super("SingleComponentSection");
    }

    public static C33981oS A06() {
        C33981oS r1 = new C33981oS();
        AnonymousClass1GV r0 = new AnonymousClass1GV();
        r1.A00 = r0;
        r1.A00 = r0;
        r1.A01.clear();
        return r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (X.AnonymousClass07c.enableRenderInfoDebugging == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A07(X.C33121my r3, java.util.Map r4, X.AnonymousClass1GA r5, X.C33111mx r6) {
        /*
            boolean r0 = X.AnonymousClass07c.isDebugModeEnabled
            if (r0 == 0) goto L_0x0009
            boolean r1 = X.AnonymousClass07c.enableRenderInfoDebugging
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x0023
            X.0wR r1 = r5.A0K()
            java.lang.String r0 = "SONAR_SECTIONS_DEBUG_INFO"
            r3.A02(r0, r1)
            java.lang.Object r1 = r6.A01
            java.lang.String r0 = "SCS_DATA_INFO_PREV"
            r3.A02(r0, r1)
            java.lang.Object r1 = r6.A00
            java.lang.String r0 = "SCS_DATA_INFO_NEXT"
            r3.A02(r0, r1)
        L_0x0023:
            if (r4 == 0) goto L_0x0047
            java.util.Set r0 = r4.entrySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x002d:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0047
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.getValue()
            r3.A01(r1, r0)
            goto L_0x002d
        L_0x0047:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1GV.A07(X.1my, java.util.Map, X.1GA, X.1mx):void");
    }

    public C16070wR A0T(boolean z) {
        C17770zR r0;
        AnonymousClass1GV r1 = (AnonymousClass1GV) super.A0T(z);
        C17770zR r02 = r1.A00;
        if (r02 != null) {
            r0 = r02.A16();
        } else {
            r0 = null;
        }
        r1.A00 = r0;
        return r1;
    }
}
