package X;

/* renamed from: X.0Gr  reason: invalid class name and case insensitive filesystem */
public final class C02860Gr implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        C02570Fl r62 = (C02570Fl) r6;
        long j = r62.realtimeMs;
        if (j != 0) {
            r7.AMV("realtime_ms", j);
        }
        long j2 = r62.uptimeMs;
        if (j2 != 0) {
            r7.AMV("uptime_ms", j2);
        }
    }
}
