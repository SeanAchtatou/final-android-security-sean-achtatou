package X;

import android.net.Uri;
import com.facebook.messaging.browser.model.MessengerWebViewParams;
import com.facebook.messaging.business.common.calltoaction.model.AdCallToAction;
import com.facebook.messaging.business.common.calltoaction.model.CTABrandedCameraParams;
import com.facebook.messaging.business.common.calltoaction.model.CTAInformationIdentify;
import com.facebook.messaging.business.common.calltoaction.model.CTAPaymentInfo;
import com.facebook.messaging.business.common.calltoaction.model.CTAUserConfirmation;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.business.common.calltoaction.model.CallToActionTarget;
import com.facebook.messaging.business.mdotme.model.PlatformRefParams;
import com.google.common.base.Platform;

/* renamed from: X.15m  reason: invalid class name and case insensitive filesystem */
public class C188515m {
    public Uri A00;
    public Uri A01;
    public MessengerWebViewParams A02;
    public CTABrandedCameraParams A03;
    public CTAInformationIdentify A04;
    public CTAPaymentInfo A05;
    public CTAUserConfirmation A06;
    public C30101hU A07;
    public C16550xJ A08;
    public CallToActionTarget A09;
    public PlatformRefParams A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;

    public CallToAction A00() {
        if (!(this instanceof C35981s8)) {
            return new CallToAction(this);
        }
        return new AdCallToAction((C35981s8) this);
    }

    public void A01(String str) {
        Uri uri;
        if (!Platform.stringIsNullOrEmpty(str)) {
            uri = Uri.parse(str);
        } else {
            uri = null;
        }
        this.A00 = uri;
    }

    public void A02(String str) {
        Uri uri;
        if (!Platform.stringIsNullOrEmpty(str)) {
            uri = Uri.parse(str);
        } else {
            uri = null;
        }
        this.A01 = uri;
    }

    public C188515m() {
    }

    public C188515m(CallToAction callToAction) {
        this.A0B = callToAction.A0B;
        this.A0E = callToAction.A0E;
        this.A00 = callToAction.A00;
        this.A01 = callToAction.A01;
        this.A08 = callToAction.A08;
        this.A09 = callToAction.A09;
        this.A0G = callToAction.A0G;
        this.A0F = callToAction.A0F;
        this.A06 = callToAction.A06;
        this.A05 = callToAction.A05;
        this.A04 = callToAction.A04;
        this.A0A = callToAction.A0A;
        this.A0C = callToAction.A0C;
        this.A02 = callToAction.A02;
        this.A07 = callToAction.A07;
        this.A0H = callToAction.A0H;
        this.A03 = callToAction.A03;
        this.A0D = callToAction.A0D;
    }
}
