package X;

import com.facebook.auth.viewercontext.ViewerContext;

/* renamed from: X.20S  reason: invalid class name */
public final class AnonymousClass20S implements C177128Fu {
    private AnonymousClass0UN A00;

    public static final AnonymousClass20S A00(AnonymousClass1XY r1) {
        return new AnonymousClass20S(r1);
    }

    public void C6d(C04810Wg r4) {
        ((AnonymousClass20Q) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AW0, this.A00)).C6d(r4);
    }

    public void CGl(String str, ViewerContext viewerContext) {
        ((AnonymousClass20Q) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AW0, this.A00)).CGl(str, viewerContext);
    }

    public void stop() {
        ((AnonymousClass20Q) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AW0, this.A00)).stop();
    }

    private AnonymousClass20S(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
