package X;

/* renamed from: X.1d8  reason: invalid class name and case insensitive filesystem */
public final class C27401d8 {
    public static final C27411d9 A00 = new C27411d9();
    public static volatile AnonymousClass8t3 A01;
    public static volatile AnonymousClass1XT A02;
    public static volatile C30231Erz A03;

    static {
        new C27401d8();
    }

    public static AnonymousClass1XV A00(AnonymousClass1XV r3, int i) {
        return A00.A02(r3, r3.A06, 0, i);
    }

    public static AnonymousClass1XV A01(String str, int i) {
        AnonymousClass1XV A032 = A00.A03(str, i);
        if (A032 != null) {
            A032.close();
        }
        return A032;
    }

    private C27401d8() {
    }
}
