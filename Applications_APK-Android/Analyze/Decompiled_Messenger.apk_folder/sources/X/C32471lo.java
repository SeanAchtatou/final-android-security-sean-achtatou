package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1lo  reason: invalid class name and case insensitive filesystem */
public final class C32471lo {
    private static volatile C32471lo A01;
    public final AnonymousClass1FN A00;

    public static final C32471lo A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C32471lo.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C32471lo(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C32471lo(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1FN.A00(r2);
    }
}
