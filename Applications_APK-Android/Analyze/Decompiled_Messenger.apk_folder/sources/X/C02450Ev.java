package X;

import android.content.Context;

/* renamed from: X.0Ev  reason: invalid class name and case insensitive filesystem */
public final class C02450Ev implements AnonymousClass1YQ, C06670bt {
    private final Context A00;
    private final C25051Yd A01;

    public String getSimpleName() {
        return "BSODConfigUpdater";
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.Ajl, r1);
    }

    public static final C02450Ev A01(AnonymousClass1XY r1) {
        return new C02450Ev(r1);
    }

    private void A02() {
        C25051Yd r2 = this.A01;
        AnonymousClass0XE r3 = AnonymousClass0XE.A05;
        AnonymousClass08X.A05(this.A00, "bsod_halt_exception_handler", r2.Aer(284584533037420L, r3));
        AnonymousClass08X.A05(this.A00, "bsod_show_fg", this.A01.Aer(284584533168494L, r3));
        AnonymousClass08X.A05(this.A00, "bsod_show_cta_storage_manager", this.A01.Aer(284584533102957L, r3));
    }

    private C02450Ev(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0WT.A00(r2);
        this.A00 = AnonymousClass1YA.A00(r2);
    }

    public int Ai5() {
        return 724;
    }

    public void init() {
        int A03 = C000700l.A03(65877887);
        A02();
        C000700l.A09(1097472146, A03);
    }

    public void BTE(int i) {
        A02();
    }
}
