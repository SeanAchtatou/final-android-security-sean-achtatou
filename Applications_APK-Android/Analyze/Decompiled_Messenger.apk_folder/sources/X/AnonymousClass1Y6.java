package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.1Y6  reason: invalid class name */
public interface AnonymousClass1Y6 {
    void AMf(ListenableFuture listenableFuture, C04810Wg r2);

    void AOx();

    void AOy(String str);

    void AOz();

    void AP0(String str);

    AnonymousClass1GT AZ9(AnonymousClass1GT r1, Object... objArr);

    boolean BHM();

    void BxQ(Runnable runnable, long j);

    void BxR(Runnable runnable);

    void BxS(Runnable runnable, long j);

    void C1d(Runnable runnable);

    void C4C(Runnable runnable);

    void CGN(long j);
}
