package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.util.TriState;
import com.facebook.fbservice.ops.DefaultBlueServiceOperation$3;
import com.facebook.fbservice.service.BlueService;
import com.facebook.fbservice.service.IBlueService;
import com.facebook.fbservice.service.OperationResult;
import com.google.common.base.Preconditions;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1cl  reason: invalid class name and case insensitive filesystem */
public final class C27171cl implements AnonymousClass0lL {
    public Handler A00;
    public C27231cr A01;
    public TriState A02 = TriState.UNSET;
    public C55182nf A03;
    public C27181cm A04 = C27181cm.INIT;
    public C156107Jr A05;
    public C34711q3 A06;
    public IBlueService A07;
    public AnonymousClass0UN A08;
    public String A09;
    public boolean A0A;
    public boolean A0B = false;
    public boolean A0C;
    public final int A0D;
    public final Context A0E;
    public final Bundle A0F;
    public final C04460Ut A0G;
    public final AnonymousClass0lQ A0H;
    public final CallerContext A0I;
    public final AnonymousClass09P A0J;
    public final C27201co A0K;
    public final AnonymousClass0lN A0L;
    public final AnonymousClass0US A0M;
    public final AnonymousClass0US A0N;
    public final String A0O;
    private final C05920aY A0P;
    private final ExecutorService A0Q;

    public C27211cp CGe() {
        return A00(true);
    }

    public C27211cp CHd() {
        return A00(false);
    }

    private C27211cp A00(boolean z) {
        C27181cm r3 = this.A04;
        boolean z2 = false;
        if (r3 == C27181cm.INIT) {
            z2 = true;
        }
        Preconditions.checkState(z2, "Incorrect operation state %s", r3);
        this.A04 = C27181cm.READY_TO_QUEUE;
        if (Looper.myLooper() != null) {
            this.A00 = new Handler();
        }
        C156107Jr r0 = this.A05;
        if (r0 != null) {
            r0.APp();
        }
        A04(this, !z);
        return this.A0L;
    }

    public static void A01(C27171cl r7) {
        C005505z.A06("%s.maybeStartAndRegister(%s)", "DefaultBlueServiceOperation", r7.A0O, -1618859090);
        try {
            if (r7.A04 == C27181cm.READY_TO_QUEUE) {
                boolean z = false;
                if (r7.A0O != null) {
                    z = true;
                }
                Preconditions.checkState(z, "Null operation type");
                boolean z2 = false;
                if (r7.A09 == null) {
                    z2 = true;
                }
                Preconditions.checkState(z2, "Non-null operation id");
                boolean z3 = false;
                if (r7.A0D == 1) {
                    z3 = true;
                }
                r7.A09 = r7.A07.CHO(r7.A0O, r7.A0F, z3, r7.A0B, new DefaultBlueServiceOperation$3(r7), r7.A0I);
                if (r7.A07 != null) {
                    r7.A04 = C27181cm.OPERATION_QUEUED;
                } else {
                    throw new RemoteException("mBlueService is null");
                }
            } else {
                AnonymousClass09P r2 = r7.A0J;
                r2.CGS("DefaultBlueServiceOperation_START_AND_REGISTER_CALLED_UNEXPECTINGLY", "maybeStartAndRegister called in wrong state. triedBindingLocally=" + r7.A02 + ", state=" + r7.A04.toString() + ", operationType=" + r7.A0O);
            }
        } catch (RemoteException e) {
            r7.A05(OperationResult.A02(C14880uI.A0C, AnonymousClass08S.A0J("BlueService.startOperationWithCompletionHandler failed due to ", e.getMessage())));
        } catch (Throwable th) {
            C005505z.A00(1369040460);
            throw th;
        }
        C005505z.A00(-240975518);
    }

    public static void A02(C27171cl r4) {
        C34711q3 r1;
        if (r4.A0A) {
            try {
                AnonymousClass0lQ r0 = r4.A0H;
                C006406k.A01(r0.A00, r4.A0K, -810719460);
            } catch (IllegalArgumentException unused) {
                C010708t.A01.BFj(4);
            }
            r4.A0A = false;
        }
        synchronized (r4) {
            r1 = r4.A06;
            r4.A06 = null;
        }
        if (r1 != null) {
            r1.A01();
        }
    }

    public String AwN() {
        return this.A0O;
    }

    public Bundle Ax6() {
        return new Bundle(this.A0F);
    }

    public boolean BEO() {
        return this.A01.BEO();
    }

    public AnonymousClass0lL CA5(C156107Jr r3) {
        C156107Jr r0;
        C27181cm r1 = this.A04;
        if ((r1 == C27181cm.READY_TO_QUEUE || r1 == C27181cm.OPERATION_QUEUED) && (r0 = this.A05) != null) {
            r0.CI0();
        }
        this.A05 = r3;
        C27181cm r12 = this.A04;
        if ((r12 == C27181cm.READY_TO_QUEUE || r12 == C27181cm.OPERATION_QUEUED) && r3 != null) {
            r3.APp();
        }
        return this;
    }

    public C27211cp CHL() {
        boolean z = false;
        if (this.A04 == C27181cm.INIT) {
            z = true;
        }
        Preconditions.checkState(z, "Incorrect operation state");
        this.A04 = C27181cm.READY_TO_QUEUE;
        this.A00 = new Handler(Looper.getMainLooper());
        C156107Jr r0 = this.A05;
        if (r0 != null) {
            r0.APp();
        }
        A03(this, "BindToService(false)", new C70883bR(this));
        return this.A0L;
    }

    public boolean isRunning() {
        C27181cm r2 = this.A04;
        if (r2 == C27181cm.INIT || r2 == C27181cm.COMPLETED) {
            return false;
        }
        return true;
    }

    public C27171cl(AnonymousClass1XY r4, Context context, AnonymousClass0US r6, ExecutorService executorService, AnonymousClass0lQ r8, AnonymousClass09P r9, String str, Bundle bundle, int i, CallerContext callerContext, C05920aY r14, AnonymousClass0US r15, C04460Ut r16) {
        ViewerContext B9S;
        this.A08 = new AnonymousClass0UN(1, r4);
        this.A0E = context;
        this.A0K = new C27201co(this);
        this.A0M = r6;
        this.A0Q = executorService;
        this.A0L = new AnonymousClass0lN(this);
        this.A0H = r8;
        this.A0J = r9;
        Preconditions.checkNotNull(str);
        this.A0O = str;
        Preconditions.checkNotNull(bundle);
        Bundle bundle2 = new Bundle(bundle);
        this.A0F = bundle2;
        this.A0D = i;
        this.A0I = callerContext;
        this.A0P = r14;
        this.A01 = new C27221cq(this);
        this.A0N = r15;
        this.A0G = r16;
        if (!bundle2.containsKey("overridden_viewer_context") && (B9S = this.A0P.B9S()) != null) {
            this.A0F.putParcelable("overridden_viewer_context", B9S);
        }
        this.A0F.putString("calling_process_name", AnonymousClass00M.A00().A01);
        C27241cs r1 = (C27241cs) AnonymousClass065.A00(context, C27241cs.class);
        if (r1 != null) {
            r1.C0V(this.A01);
        }
    }

    public static void A03(C27171cl r2, String str, Runnable runnable) {
        C005505z.A03(str, -95851102);
        try {
            Handler handler = r2.A00;
            if (handler != null) {
                AnonymousClass00S.A04(handler, runnable, 1815990501);
            } else {
                AnonymousClass07A.A04(r2.A0Q, runnable, 272456122);
            }
        } finally {
            C005505z.A00(-958053720);
        }
    }

    public static void A04(C27171cl r5, boolean z) {
        if (!r5.BEO() && r5.A04 == C27181cm.READY_TO_QUEUE) {
            if (((C27251ct) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AW9, r5.A08)).A02) {
                z = true;
            }
            Intent intent = new Intent(r5.A0E, BlueService.class);
            if (!z || ((C27251ct) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AW9, r5.A08)).A01(intent)) {
                r5.A02 = TriState.NO;
                try {
                    if (r5.A0H.A01(intent, r5.A0K, 1)) {
                        r5.A0A = true;
                    } else {
                        r5.A05(OperationResult.A02(C14880uI.A0C, "Bind to BlueService failed"));
                    }
                } catch (RuntimeException e) {
                    throw new RuntimeException(AnonymousClass08S.A0P("Binding BlueService for `", r5.A0O, "` threw an exception."), e);
                }
            } else {
                r5.A02 = TriState.YES;
                r5.A07 = (IBlueService) r5.A0M.get();
                int i = AnonymousClass1Y3.AW9;
                AnonymousClass0UN r1 = r5.A08;
                if (((C27251ct) AnonymousClass1XX.A02(0, i, r1)).A02) {
                    C34711q3 r0 = (C34711q3) AnonymousClass1XX.A03(AnonymousClass1Y3.ABI, r1);
                    synchronized (r5) {
                        r5.A06 = r0;
                        r0.A02();
                    }
                }
                A01(r5);
            }
        }
    }

    public void A05(OperationResult operationResult) {
        C27181cm r0;
        if (!BEO() && this.A04 != (r0 = C27181cm.COMPLETED)) {
            this.A04 = r0;
            this.A09 = null;
            A02(this);
            if (this.A0C) {
                this.A01.A03();
                return;
            }
            String str = this.A0O;
            A03(this, AnonymousClass08S.A0J("ReportCompleted-", str), new C17340yk(this, "DefaultBlueServiceOperation-Completed", str, operationResult));
        }
    }

    public AnonymousClass0lL C6o(boolean z) {
        this.A0B = z;
        return this;
    }

    public AnonymousClass0lL C81(boolean z) {
        this.A0C = z;
        return this;
    }

    public AnonymousClass0lL CA2(C55182nf r1) {
        this.A03 = r1;
        return this;
    }

    static {
        AnonymousClass0TG.A07();
    }
}
