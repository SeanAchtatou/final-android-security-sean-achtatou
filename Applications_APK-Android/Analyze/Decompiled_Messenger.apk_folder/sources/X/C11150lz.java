package X;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.util.TriState;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.database.threads.MessageCursorUtil;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadCriteria;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.service.model.FetchThreadParams;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.0lz  reason: invalid class name and case insensitive filesystem */
public class C11150lz extends AnonymousClass0mF {
    public OperationResult A0V(C11060ln r24, C27311cz r25) {
        FetchThreadResult A0H;
        OperationResult operationResult;
        int i;
        Map map;
        FetchThreadResult fetchThreadResult;
        boolean z;
        FetchThreadResult fetchThreadResult2;
        ThreadKey threadKey;
        OperationResult A00;
        C11060ln r6 = r24;
        C27311cz r5 = r25;
        if (this instanceof C27561dO) {
            C27561dO r7 = (C27561dO) this;
            ThreadKey A01 = ((FetchThreadParams) r6.A00.getParcelable("fetchThreadParams")).A04.A01();
            C50452e3 r4 = null;
            if (A01 != null && ((int) A01.A0G()) == -102) {
                r4 = C50452e3.BUSINESS;
            }
            if (r4 == null) {
                return r5.BAz(r6);
            }
            ThreadSummary A012 = C27561dO.A01(r7, r4, r5);
            HashMap hashMap = new HashMap();
            C33881oI r1 = new C33881oI();
            r1.A00 = A01;
            r1.A01(RegularImmutableList.A02);
            r1.A03 = true;
            r1.A02 = true;
            MessagesCollection A002 = r1.A00();
            C61222yX A003 = FetchThreadResult.A00();
            A003.A01 = DataFetchDisposition.A0F;
            A003.A00 = ((C32761mI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AnC, r7.A00)).now();
            A003.A04 = A012;
            A003.A02 = A002;
            A003.A06 = ImmutableList.copyOf(hashMap.values());
            return OperationResult.A04(A003.A00());
        } else if (this instanceof C12440pM) {
            C12440pM r42 = (C12440pM) this;
            C005505z.A03("SmsServiceHandler.handleFetchThread", 338443844);
            try {
                FetchThreadParams fetchThreadParams = (FetchThreadParams) r6.A00.getParcelable("fetchThreadParams");
                return OperationResult.A04(C12440pM.A01(r42, fetchThreadParams.A04.A01().A0G(), fetchThreadParams.A01));
            } finally {
                C005505z.A00(1848059140);
            }
        } else if (this instanceof C12370pE) {
            C12370pE r43 = (C12370pE) this;
            ((C60222wn) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B4i, r43.A00)).A04();
            Bundle bundle = r6.A00;
            CallerContext callerContext = r6.A01;
            FetchThreadParams fetchThreadParams2 = (FetchThreadParams) bundle.getParcelable("fetchThreadParams");
            C08770fv.A04(r43.A02, fetchThreadParams2.A04.A01().A0J().hashCode(), "db_thread");
            ((C12450pN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AwM, r43.A00)).A03(C12500pT.A05, "fetchThread (DSH). " + fetchThreadParams2.A04.A01());
            C09510hU r12 = fetchThreadParams2.A02;
            int i2 = fetchThreadParams2.A01;
            ThreadCriteria threadCriteria = fetchThreadParams2.A04;
            C005505z.A03("DbServiceHandler.handleFetchThread", 406675370);
            try {
                long now = AnonymousClass06A.A00.now();
                A0H = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r43.A00)).A0H(threadCriteria, i2);
                long now2 = AnonymousClass06A.A00.now() - now;
                HashMap hashMap2 = new HashMap();
                hashMap2.put("fetch_location", C52172id.A00(AnonymousClass07B.A0C));
                hashMap2.put("thread_db_duration", Long.toString(now2));
                A0H.A00 = hashMap2;
                if (((Boolean) r43.A03.get()).booleanValue() && A0H.A05 != null && A0H.A03.A09(i2) && A0H.A05.A14) {
                    AnonymousClass162 r13 = new AnonymousClass162();
                    r13.A07 = AnonymousClass0u3.LOCAL_DISK_CACHE;
                    r13.A02 = TriState.YES;
                    TriState triState = TriState.NO;
                    r13.A04 = triState;
                    r13.A06 = triState;
                    DataFetchDisposition dataFetchDisposition = new DataFetchDisposition(r13);
                    C61222yX r3 = new C61222yX(A0H);
                    r3.A01 = dataFetchDisposition;
                    r3.A07 = hashMap2;
                    r3.A00 = r43.A01.now();
                    operationResult = OperationResult.A04(r3.A00());
                    C12370pE.A03(operationResult);
                    i = -2120951300;
                } else if (ThreadKey.A0C(threadCriteria.A01())) {
                    operationResult = OperationResult.A04(A0H);
                    C12370pE.A03(operationResult);
                    i = 1654596062;
                } else if (FetchThreadResult.A01(fetchThreadParams2, A0H)) {
                    operationResult = C12370pE.A00(r43, r6, r5, A0H, now2, i2);
                    i = 602688578;
                } else if (r12 == C09510hU.DO_NOT_CHECK_SERVER) {
                    operationResult = OperationResult.A04(A0H);
                    C12370pE.A03(operationResult);
                    i = 1787418756;
                } else {
                    ThreadSummary threadSummary = A0H.A05;
                    MessagesCollection messagesCollection = A0H.A03;
                    map = A0H.A00;
                    fetchThreadResult = null;
                    if (threadSummary != null && messagesCollection != null && !messagesCollection.A08() && !threadSummary.A0O.A01()) {
                        C09510hU r14 = fetchThreadParams2.A02;
                        if (r14 == C09510hU.STALE_DATA_OKAY) {
                            fetchThreadResult = A0H;
                        } else {
                            if (r14 == C09510hU.CHECK_SERVER_FOR_NEW_DATA) {
                                z = true;
                            } else {
                                z = A0H.A02.A04.asBoolean(false);
                            }
                            if (!z) {
                                fetchThreadResult2 = A0H;
                            } else {
                                MessagesCollection messagesCollection2 = A0H.A03;
                                Message A02 = C12370pE.A02(messagesCollection2);
                                C60292wu r15 = new C60292wu();
                                r15.A00(fetchThreadParams2);
                                r15.A01 = C09510hU.CHECK_SERVER_FOR_NEW_DATA;
                                FetchThreadParams fetchThreadParams3 = new FetchThreadParams(r15);
                                Bundle bundle2 = new Bundle();
                                bundle2.putParcelable("fetchThreadParams", fetchThreadParams3);
                                FetchThreadResult fetchThreadResult3 = (FetchThreadResult) r5.BAz(new C11060ln("fetch_thread", bundle2, null, null, callerContext, null)).A08();
                                SQLiteDatabase A06 = ((C25771aN) r43.A05.get()).A06();
                                C007406x.A01(A06, 1213790269);
                                try {
                                    C12370pE.A05(r43, A02, fetchThreadResult3);
                                    ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r43.A00)).A0X(A0H, fetchThreadResult3, "fetchMoreRecentMessagesFromServerIfNeeded");
                                    A06.setTransactionSuccessful();
                                    C007406x.A02(A06, -1103719485);
                                    if (messagesCollection2 == null) {
                                        messagesCollection2 = fetchThreadResult3.A03;
                                        if (messagesCollection2 == null) {
                                            th = new NullPointerException();
                                            throw th;
                                        }
                                    } else {
                                        MessagesCollection messagesCollection3 = fetchThreadResult3.A03;
                                        if (messagesCollection3 != null) {
                                            messagesCollection2 = AnonymousClass0mB.A00((AnonymousClass0mB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BFT, r43.A00), messagesCollection3, messagesCollection2, true);
                                        }
                                    }
                                    C61222yX A004 = FetchThreadResult.A00();
                                    A004.A01 = DataFetchDisposition.A0G;
                                    A004.A04 = fetchThreadResult3.A05;
                                    A004.A02 = messagesCollection2;
                                    A004.A06 = fetchThreadResult3.A07;
                                    A004.A00 = r43.A01.now();
                                    fetchThreadResult2 = A004.A00();
                                } catch (Throwable th) {
                                    th = th;
                                    C007406x.A02(A06, -1301736220);
                                }
                            }
                            C12370pE.A06(r43, fetchThreadParams2, fetchThreadResult2, r5);
                            C61222yX r16 = new C61222yX(((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r43.A00)).A0F(threadSummary.A0S, fetchThreadParams2.A01));
                            r16.A01 = DataFetchDisposition.A0G;
                            fetchThreadResult = r16.A00();
                        }
                    }
                    if (fetchThreadResult != null) {
                        operationResult = OperationResult.A04(fetchThreadResult);
                        C12370pE.A03(operationResult);
                        i = -226719118;
                    } else {
                        operationResult = C12370pE.A00(r43, r6, r5, A0H, now2, i2);
                        i = -355813987;
                    }
                }
            } catch (IOException e) {
                if (fetchThreadParams2.A03 != C09510hU.CHECK_SERVER_FOR_NEW_DATA) {
                    C61222yX A005 = FetchThreadResult.A00();
                    A005.A01 = DataFetchDisposition.A09;
                    A005.A04 = A0H.A05;
                    A005.A02 = A0H.A03;
                    A005.A07 = map;
                    A005.A06 = A0H.A07;
                    A005.A00 = A0H.A01;
                    fetchThreadResult = A005.A00();
                } else {
                    throw e;
                }
            } catch (Throwable th2) {
                C005505z.A00(210285950);
                throw th2;
            }
            C005505z.A00(i);
            return operationResult;
        } else if (!(this instanceof C11130lx)) {
            return r5.BAz(r6);
        } else {
            C11130lx r8 = (C11130lx) this;
            FetchThreadParams fetchThreadParams4 = (FetchThreadParams) AnonymousClass0ls.A00(r6, "fetchThreadParams");
            ((C12450pN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AwM, r8.A00)).A03(C12500pT.A03, "fetchThread (CSH). " + fetchThreadParams4.A04.A01());
            ImmutableList immutableList = fetchThreadParams4.A05;
            if (immutableList != null) {
                ((C14300t0) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AxE, r8.A00)).A07(immutableList, true);
            }
            HashMap hashMap3 = new HashMap();
            hashMap3.put("fetch_location", C52172id.A00(AnonymousClass07B.A00));
            long now3 = AnonymousClass06A.A00.now();
            C11230mQ r11 = r8.A04;
            Bundle bundle3 = r6.A00;
            FetchThreadParams fetchThreadParams5 = (FetchThreadParams) bundle3.getParcelable("fetchThreadParams");
            ThreadSummary A03 = r11.A03(fetchThreadParams5.A04);
            FetchThreadResult fetchThreadResult4 = null;
            if (A03 != null) {
                threadKey = A03.A0S;
            } else {
                threadKey = null;
            }
            C60292wu r142 = new C60292wu();
            r142.A00(fetchThreadParams5);
            C09510hU r152 = ((C12540pX) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BDk, r11.A00)).A02(threadKey, fetchThreadParams5.A02).A00;
            C09510hU r132 = fetchThreadParams5.A02;
            if (r152 != r132) {
                r142.A01 = r152;
                r142.A02 = r132;
            }
            FetchThreadParams fetchThreadParams6 = new FetchThreadParams(r142);
            bundle3.putParcelable("fetchThreadParams", fetchThreadParams6);
            if (C11230mQ.A01(r11, A03, fetchThreadParams6.A02, fetchThreadParams6.A01)) {
                fetchThreadResult4 = C11230mQ.A00(r11, A03);
            }
            hashMap3.put("thread_cache_duration", Long.toString(AnonymousClass06A.A00.now() - now3));
            if (FetchThreadResult.A01(fetchThreadParams4, fetchThreadResult4) || fetchThreadResult4 == null) {
                A00 = C11130lx.A00(r8, r6, r5);
            } else {
                hashMap3.put("fetch_location", C52172id.A00(AnonymousClass07B.A01));
                A00 = OperationResult.A04(fetchThreadResult4);
                Bundle bundle4 = A00.resultDataBundle;
                if (bundle4 != null) {
                    bundle4.putString("source", "cache");
                }
            }
            Map map2 = ((FetchThreadResult) A00.A07()).A00;
            if (map2 != null) {
                hashMap3.putAll(map2);
            }
            C11670nb r17 = new C11670nb("fetch_thread");
            C11670nb.A02(r17, hashMap3, false);
            ((AnonymousClass0oE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Are, r8.A00)).A00.A09(r17);
            return A00;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0465, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0466, code lost:
        if (r6 != null) goto L_0x0468;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:159:0x046b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0f(X.C11060ln r19, X.C27311cz r20) {
        /*
            r18 = this;
            r1 = r18
            boolean r0 = r1 instanceof X.C27561dO
            r8 = r19
            r7 = r20
            if (r0 != 0) goto L_0x0474
            boolean r0 = r1 instanceof X.C12440pM
            if (r0 != 0) goto L_0x026c
            boolean r0 = r1 instanceof X.C12370pE
            if (r0 != 0) goto L_0x024c
            boolean r0 = r1 instanceof X.C11130lx
            if (r0 == 0) goto L_0x0267
            r6 = r1
            X.0lx r6 = (X.C11130lx) r6
            android.os.Bundle r11 = r8.A00
            java.lang.String r10 = "markThreadsParams"
            android.os.Parcelable r3 = r11.getParcelable(r10)
            com.facebook.messaging.service.model.MarkThreadsParams r3 = (com.facebook.messaging.service.model.MarkThreadsParams) r3
            java.lang.Integer r1 = r3.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x008d
            X.3YF r5 = new X.3YF
            r5.<init>()
            r5.A00 = r1
            boolean r0 = r3.A03
            r5.A01 = r0
            com.google.common.collect.ImmutableList r0 = r3.A00
            X.1Xv r12 = r0.iterator()
        L_0x003a:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x0088
            java.lang.Object r9 = r12.next()
            com.facebook.messaging.service.model.MarkThreadFields r9 = (com.facebook.messaging.service.model.MarkThreadFields) r9
            X.3YC r4 = new X.3YC
            r4.<init>()
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r9.A06
            r4.A05 = r3
            boolean r2 = r9.A07
            r4.A06 = r2
            long r0 = r9.A02
            r4.A01 = r0
            long r0 = r9.A04
            r4.A03 = r0
            r0 = -1
            r4.A02 = r0
            X.0l8 r0 = r9.A05
            r4.A04 = r0
            long r0 = r9.A01
            r4.A00 = r0
            if (r2 == 0) goto L_0x007d
            X.0mQ r0 = r6.A04
            X.0m6 r0 = r0.A01
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A0R(r3)
            if (r0 == 0) goto L_0x007d
            long r2 = r0.A07
            long r0 = r9.A04
            int r9 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r9 <= 0) goto L_0x007d
            r4.A03 = r2
        L_0x007d:
            com.facebook.messaging.service.model.MarkThreadFields r1 = new com.facebook.messaging.service.model.MarkThreadFields
            r1.<init>(r4)
            com.google.common.collect.ImmutableList$Builder r0 = r5.A02
            r0.add(r1)
            goto L_0x003a
        L_0x0088:
            com.facebook.messaging.service.model.MarkThreadsParams r3 = new com.facebook.messaging.service.model.MarkThreadsParams
            r3.<init>(r5)
        L_0x008d:
            com.google.common.collect.HashMultimap r5 = new com.google.common.collect.HashMultimap
            r5.<init>()
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder
            r9.<init>()
            com.google.common.collect.ImmutableMap$Builder r4 = new com.google.common.collect.ImmutableMap$Builder
            r4.<init>()
            com.google.common.collect.ImmutableList r0 = r3.A00
            X.1Xv r14 = r0.iterator()
        L_0x00a2:
            boolean r0 = r14.hasNext()
            if (r0 == 0) goto L_0x00eb
            java.lang.Object r1 = r14.next()
            com.facebook.messaging.service.model.MarkThreadFields r1 = (com.facebook.messaging.service.model.MarkThreadFields) r1
            X.0l8 r2 = r1.A05
            if (r2 != 0) goto L_0x00b4
            X.0l8 r2 = X.C10950l8.A05
        L_0x00b4:
            r5.Byx(r2, r1)
            r13 = 11
            int r12 = X.AnonymousClass1Y3.BAo
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r13, r12, r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00d2
            X.0l8 r0 = X.C10950l8.A05
            if (r2 != r0) goto L_0x00d2
            X.0l8 r0 = X.C10950l8.A0B
            r5.Byx(r0, r1)
        L_0x00d2:
            boolean r0 = r1.A07
            if (r0 == 0) goto L_0x00a2
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A06
            r9.add(r0)
            X.0l8 r0 = X.C10950l8.A06
            if (r2 != r0) goto L_0x00a2
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r1.A06
            long r0 = r1.A04
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r4.put(r2, r0)
            goto L_0x00a2
        L_0x00eb:
            java.lang.Integer r2 = r3.A02
            boolean r17 = X.C11130lx.A01(r6)
            java.util.Set r0 = r5.keySet()     // Catch:{ all -> 0x0239 }
            java.util.Iterator r16 = r0.iterator()     // Catch:{ all -> 0x0239 }
        L_0x00f9:
            boolean r0 = r16.hasNext()     // Catch:{ all -> 0x0239 }
            if (r0 == 0) goto L_0x0175
            java.lang.Object r13 = r16.next()     // Catch:{ all -> 0x0239 }
            X.0l8 r13 = (X.C10950l8) r13     // Catch:{ all -> 0x0239 }
            X.0US r0 = r6.A01     // Catch:{ all -> 0x0239 }
            java.lang.Object r12 = r0.get()     // Catch:{ all -> 0x0239 }
            X.0u9 r12 = (X.C14800u9) r12     // Catch:{ all -> 0x0239 }
            X.3YF r14 = new X.3YF     // Catch:{ all -> 0x0239 }
            r14.<init>()     // Catch:{ all -> 0x0239 }
            r14.A00 = r2     // Catch:{ all -> 0x0239 }
            java.util.Set r0 = r5.AbK(r13)     // Catch:{ all -> 0x0239 }
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x0239 }
            com.google.common.collect.ImmutableList$Builder r0 = r14.A02     // Catch:{ all -> 0x0239 }
            r0.addAll(r1)     // Catch:{ all -> 0x0239 }
            com.facebook.messaging.service.model.MarkThreadsParams r1 = new com.facebook.messaging.service.model.MarkThreadsParams     // Catch:{ all -> 0x0239 }
            r1.<init>(r14)     // Catch:{ all -> 0x0239 }
            java.lang.Integer r14 = r1.A02     // Catch:{ all -> 0x0239 }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0239 }
            if (r14 != r0) goto L_0x0146
            com.google.common.collect.ImmutableList r0 = r1.A00     // Catch:{ all -> 0x0239 }
            X.1Xv r15 = r0.iterator()     // Catch:{ all -> 0x0239 }
        L_0x0132:
            boolean r0 = r15.hasNext()     // Catch:{ all -> 0x0239 }
            if (r0 == 0) goto L_0x00f9
            java.lang.Object r14 = r15.next()     // Catch:{ all -> 0x0239 }
            com.facebook.messaging.service.model.MarkThreadFields r14 = (com.facebook.messaging.service.model.MarkThreadFields) r14     // Catch:{ all -> 0x0239 }
            X.0m6 r13 = r12.A01     // Catch:{ all -> 0x0239 }
            r0 = -1
            r13.A0d(r14, r0)     // Catch:{ all -> 0x0239 }
            goto L_0x0132
        L_0x0146:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0239 }
            if (r14 == r0) goto L_0x014e
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x0239 }
            if (r14 != r0) goto L_0x00f9
        L_0x014e:
            com.google.common.collect.ImmutableList$Builder r14 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ all -> 0x0239 }
            r14.<init>()     // Catch:{ all -> 0x0239 }
            com.google.common.collect.ImmutableList r0 = r1.A00     // Catch:{ all -> 0x0239 }
            X.1Xv r1 = r0.iterator()     // Catch:{ all -> 0x0239 }
        L_0x0159:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0239 }
            if (r0 == 0) goto L_0x016b
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0239 }
            com.facebook.messaging.service.model.MarkThreadFields r0 = (com.facebook.messaging.service.model.MarkThreadFields) r0     // Catch:{ all -> 0x0239 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A06     // Catch:{ all -> 0x0239 }
            r14.add(r0)     // Catch:{ all -> 0x0239 }
            goto L_0x0159
        L_0x016b:
            X.0m6 r1 = r12.A01     // Catch:{ all -> 0x0239 }
            com.google.common.collect.ImmutableList r0 = r14.build()     // Catch:{ all -> 0x0239 }
            r1.A0X(r13, r0)     // Catch:{ all -> 0x0239 }
            goto L_0x00f9
        L_0x0175:
            com.google.common.collect.ImmutableList r13 = r9.build()     // Catch:{ all -> 0x0239 }
            boolean r0 = r13.isEmpty()     // Catch:{ all -> 0x0239 }
            r9 = 3
            if (r0 != 0) goto L_0x01cb
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0239 }
            java.lang.String r12 = "CacheServiceHandler.handleMarkThreads"
            if (r2 != r0) goto L_0x01b6
            com.google.common.collect.ImmutableList r1 = r3.A00     // Catch:{ all -> 0x0239 }
            r0 = 0
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x0239 }
            com.facebook.messaging.service.model.MarkThreadFields r0 = (com.facebook.messaging.service.model.MarkThreadFields) r0     // Catch:{ all -> 0x0239 }
            boolean r0 = r0.A07     // Catch:{ all -> 0x0239 }
            if (r0 != 0) goto L_0x01a1
            int r1 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x0239 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0239 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x0239 }
            X.16c r0 = (X.C189216c) r0     // Catch:{ all -> 0x0239 }
            r0.A0M(r13, r12)     // Catch:{ all -> 0x0239 }
            goto L_0x01cb
        L_0x01a1:
            int r1 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x0239 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0239 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x0239 }
            X.16c r2 = (X.C189216c) r2     // Catch:{ all -> 0x0239 }
            java.lang.String r1 = X.C06680bu.A0b     // Catch:{ all -> 0x0239 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0239 }
            r0.<init>(r13)     // Catch:{ all -> 0x0239 }
            X.C189216c.A05(r2, r1, r0, r12)     // Catch:{ all -> 0x0239 }
            goto L_0x01cb
        L_0x01b6:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0239 }
            if (r2 == r0) goto L_0x01be
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x0239 }
            if (r2 != r0) goto L_0x01cb
        L_0x01be:
            int r1 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x0239 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0239 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x0239 }
            X.16c r0 = (X.C189216c) r0     // Catch:{ all -> 0x0239 }
            r0.A0L(r13, r12)     // Catch:{ all -> 0x0239 }
        L_0x01cb:
            boolean r0 = r5.isEmpty()     // Catch:{ all -> 0x0239 }
            if (r0 != 0) goto L_0x01f1
            int r1 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x0239 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0239 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x0239 }
            X.16c r0 = (X.C189216c) r0     // Catch:{ all -> 0x0239 }
            r0.A08()     // Catch:{ all -> 0x0239 }
            X.0l8 r0 = X.C10950l8.A06     // Catch:{ all -> 0x0239 }
            boolean r0 = r5.containsKey(r0)     // Catch:{ all -> 0x0239 }
            if (r0 == 0) goto L_0x01f1
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0239 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x0239 }
            X.16c r0 = (X.C189216c) r0     // Catch:{ all -> 0x0239 }
            r0.A09()     // Catch:{ all -> 0x0239 }
        L_0x01f1:
            com.google.common.collect.ImmutableMap r1 = r4.build()     // Catch:{ all -> 0x0239 }
            boolean r0 = r1.isEmpty()     // Catch:{ all -> 0x0239 }
            if (r0 != 0) goto L_0x0220
            X.0US r0 = r6.A01     // Catch:{ all -> 0x0239 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0239 }
            X.0u9 r0 = (X.C14800u9) r0     // Catch:{ all -> 0x0239 }
            java.util.Set r4 = r0.A01(r1)     // Catch:{ all -> 0x0239 }
            boolean r0 = r4.isEmpty()     // Catch:{ all -> 0x0239 }
            if (r0 != 0) goto L_0x0220
            int r1 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x0239 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0239 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x0239 }
            X.16c r2 = (X.C189216c) r2     // Catch:{ all -> 0x0239 }
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.copyOf(r4)     // Catch:{ all -> 0x0239 }
            java.lang.String r0 = "CacheServiceHandler.maybeHandleMarkThreadsForMontage"
            r2.A0K(r1, r0)     // Catch:{ all -> 0x0239 }
        L_0x0220:
            r11.putParcelable(r10, r3)     // Catch:{ all -> 0x0239 }
            com.facebook.fbservice.service.OperationResult r3 = r7.BAz(r8)     // Catch:{ all -> 0x0239 }
            if (r17 == 0) goto L_0x0238
            int r2 = X.AnonymousClass1Y3.BBF
            X.0UN r1 = r6.A00
            r0 = 12
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x0238:
            return r3
        L_0x0239:
            r3 = move-exception
            if (r17 == 0) goto L_0x024b
            int r2 = X.AnonymousClass1Y3.BBF
            X.0UN r1 = r6.A00
            r0 = 12
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x024b:
            throw r3
        L_0x024c:
            r4 = r1
            X.0pE r4 = (X.C12370pE) r4
            android.os.Bundle r1 = r8.A00
            java.lang.String r0 = "markThreadsParams"
            android.os.Parcelable r3 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.MarkThreadsParams r3 = (com.facebook.messaging.service.model.MarkThreadsParams) r3
            int r2 = X.AnonymousClass1Y3.BJD
            X.0UN r1 = r4.A00
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2ij r0 = (X.C52232ij) r0
            r0.A0Z(r3)
        L_0x0267:
            com.facebook.fbservice.service.OperationResult r0 = r7.BAz(r8)
            return r0
        L_0x026c:
            r0 = r1
            X.0pM r0 = (X.C12440pM) r0
            r17 = r0
            java.lang.String r1 = "SmsServiceHandler.handleMarkThreads"
            r0 = -1956813237(0xffffffff8b5d664b, float:-4.2640008E-32)
            X.C005505z.A03(r1, r0)
            android.os.Bundle r1 = r8.A00     // Catch:{ all -> 0x046c }
            java.lang.String r0 = "markThreadsParams"
            android.os.Parcelable r7 = r1.getParcelable(r0)     // Catch:{ all -> 0x046c }
            com.facebook.messaging.service.model.MarkThreadsParams r7 = (com.facebook.messaging.service.model.MarkThreadsParams) r7     // Catch:{ all -> 0x046c }
            java.lang.Integer r1 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x046c }
            java.lang.Integer r0 = r7.A02     // Catch:{ all -> 0x046c }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x046c }
            if (r0 == 0) goto L_0x02b8
            com.google.common.collect.ImmutableList r0 = r7.A00     // Catch:{ all -> 0x046c }
            int r0 = r0.size()     // Catch:{ all -> 0x046c }
            r3 = 1
            if (r0 != r3) goto L_0x02da
            com.google.common.collect.ImmutableList r1 = r7.A00     // Catch:{ all -> 0x046c }
            r0 = 0
            java.lang.Object r4 = r1.get(r0)     // Catch:{ all -> 0x046c }
            com.facebook.messaging.service.model.MarkThreadFields r4 = (com.facebook.messaging.service.model.MarkThreadFields) r4     // Catch:{ all -> 0x046c }
            boolean r0 = r4.A07     // Catch:{ all -> 0x046c }
            if (r0 == 0) goto L_0x02bc
            int r1 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x046c }
            r0 = r17
            X.0UN r0 = r0.A01     // Catch:{ all -> 0x046c }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x046c }
            X.2e4 r2 = (X.C50462e4) r2     // Catch:{ all -> 0x046c }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r4.A06     // Catch:{ all -> 0x046c }
            long r0 = r0.A0G()     // Catch:{ all -> 0x046c }
            r2.A0F(r0)     // Catch:{ all -> 0x046c }
        L_0x02b8:
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A00     // Catch:{ all -> 0x046c }
            goto L_0x045c
        L_0x02bc:
            int r1 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x046c }
            r0 = r17
            X.0UN r0 = r0.A01     // Catch:{ all -> 0x046c }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x046c }
            X.2e4 r2 = (X.C50462e4) r2     // Catch:{ all -> 0x046c }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r4.A06     // Catch:{ all -> 0x046c }
            long r0 = r0.A0G()     // Catch:{ all -> 0x046c }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x046c }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r0)     // Catch:{ all -> 0x046c }
            r2.A0I(r0)     // Catch:{ all -> 0x046c }
            goto L_0x02b8
        L_0x02da:
            java.util.HashSet r4 = new java.util.HashSet     // Catch:{ all -> 0x046c }
            com.google.common.collect.ImmutableList r0 = r7.A00     // Catch:{ all -> 0x046c }
            int r0 = r0.size()     // Catch:{ all -> 0x046c }
            r4.<init>(r0)     // Catch:{ all -> 0x046c }
            com.google.common.collect.ImmutableList r0 = r7.A00     // Catch:{ all -> 0x046c }
            X.1Xv r2 = r0.iterator()     // Catch:{ all -> 0x046c }
        L_0x02eb:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x046c }
            if (r0 == 0) goto L_0x0309
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x046c }
            com.facebook.messaging.service.model.MarkThreadFields r1 = (com.facebook.messaging.service.model.MarkThreadFields) r1     // Catch:{ all -> 0x046c }
            boolean r0 = r1.A07     // Catch:{ all -> 0x046c }
            if (r0 == 0) goto L_0x02eb
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A06     // Catch:{ all -> 0x046c }
            long r0 = r0.A0G()     // Catch:{ all -> 0x046c }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x046c }
            r4.add(r0)     // Catch:{ all -> 0x046c }
            goto L_0x02eb
        L_0x0309:
            boolean r0 = r4.isEmpty()     // Catch:{ all -> 0x046c }
            if (r0 != 0) goto L_0x0421
            int r1 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x046c }
            r0 = r17
            X.0UN r0 = r0.A01     // Catch:{ all -> 0x046c }
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x046c }
            X.2e4 r9 = (X.C50462e4) r9     // Catch:{ all -> 0x046c }
            java.lang.String r12 = "1"
            java.lang.String r11 = "seen"
            java.lang.String r5 = "0"
            java.lang.String r10 = "read"
            int r6 = X.AnonymousClass1Y3.B8R     // Catch:{ all -> 0x046c }
            X.0UN r1 = r9.A00     // Catch:{ all -> 0x046c }
            r0 = 18
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r6, r1)     // Catch:{ all -> 0x046c }
            X.2e8 r1 = (X.C50502e8) r1     // Catch:{ all -> 0x046c }
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r1.A02     // Catch:{ all -> 0x046c }
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r0.writeLock()     // Catch:{ all -> 0x046c }
            r0.lock()     // Catch:{ all -> 0x046c }
            X.2e9 r6 = r1.A01     // Catch:{ all -> 0x046c }
            int r0 = r4.size()     // Catch:{ all -> 0x0463 }
            r14 = 0
            if (r0 != r3) goto L_0x0355
            java.lang.Long r0 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x0463 }
            java.lang.Object r0 = X.AnonymousClass0j4.A06(r4, r0)     // Catch:{ all -> 0x0463 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x0463 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x0463 }
            r9.A0F(r0)     // Catch:{ all -> 0x0463 }
            goto L_0x0419
        L_0x0355:
            int r1 = X.AnonymousClass1Y3.BOk     // Catch:{ all -> 0x0463 }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x0463 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0463 }
            X.0l9 r0 = (X.C10960l9) r0     // Catch:{ all -> 0x0463 }
            boolean r0 = r0.A08()     // Catch:{ all -> 0x0463 }
            if (r0 != 0) goto L_0x0375
            r5 = 7
            int r1 = X.AnonymousClass1Y3.ABY     // Catch:{ all -> 0x0463 }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x0463 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0463 }
            X.2jN r0 = (X.C52602jN) r0     // Catch:{ all -> 0x0463 }
            r0.A06(r4)     // Catch:{ all -> 0x0463 }
            goto L_0x0419
        L_0x0375:
            X.0av r1 = X.C06160ax.A03(r10, r5)     // Catch:{ all -> 0x0463 }
            r8 = 0
            X.0av r0 = X.C06160ax.A03(r11, r5)     // Catch:{ all -> 0x0463 }
            X.0av[] r0 = new X.C06140av[]{r1, r0}     // Catch:{ all -> 0x0463 }
            X.1a6 r1 = X.C06160ax.A02(r0)     // Catch:{ all -> 0x0463 }
            java.lang.String r0 = "thread_id"
            X.0av r0 = X.C06160ax.A04(r0, r4)     // Catch:{ all -> 0x0463 }
            X.0av[] r0 = new X.C06140av[]{r1, r0}     // Catch:{ all -> 0x0463 }
            X.1a6 r5 = X.C06160ax.A01(r0)     // Catch:{ all -> 0x0463 }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x0463 }
            int r0 = r4.size()     // Catch:{ all -> 0x0463 }
            r2.<init>(r0)     // Catch:{ all -> 0x0463 }
            java.util.Iterator r16 = r4.iterator()     // Catch:{ all -> 0x0463 }
        L_0x03a1:
            boolean r0 = r16.hasNext()     // Catch:{ all -> 0x0463 }
            if (r0 == 0) goto L_0x03ed
            java.lang.Object r0 = r16.next()     // Catch:{ all -> 0x0463 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x0463 }
            if (r0 == 0) goto L_0x03a1
            long r0 = r0.longValue()     // Catch:{ all -> 0x0463 }
            int r13 = (r0 > r14 ? 1 : (r0 == r14 ? 0 : -1))
            if (r13 >= 0) goto L_0x03c7
            r15 = 9
            int r14 = X.AnonymousClass1Y3.A6D     // Catch:{ all -> 0x0463 }
            X.0UN r13 = r9.A00     // Catch:{ all -> 0x0463 }
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r15, r14, r13)     // Catch:{ all -> 0x0463 }
            X.9NV r13 = (X.AnonymousClass9NV) r13     // Catch:{ all -> 0x0463 }
            r13.A01(r0)     // Catch:{ all -> 0x0463 }
            goto L_0x03ea
        L_0x03c7:
            android.net.Uri r13 = X.C50472e5.A00     // Catch:{ all -> 0x0463 }
            android.net.Uri r0 = android.content.ContentUris.withAppendedId(r13, r0)     // Catch:{ all -> 0x0463 }
            android.content.ContentProviderOperation$Builder r13 = android.content.ContentProviderOperation.newUpdate(r0)     // Catch:{ all -> 0x0463 }
            java.lang.String r1 = X.C50462e4.A06(r9, r5)     // Catch:{ all -> 0x0463 }
            java.lang.String[] r0 = X.C50462e4.A09(r9, r5)     // Catch:{ all -> 0x0463 }
            android.content.ContentProviderOperation$Builder r0 = r13.withSelection(r1, r0)     // Catch:{ all -> 0x0463 }
            r0.withValue(r10, r12)     // Catch:{ all -> 0x0463 }
            r0.withValue(r11, r12)     // Catch:{ all -> 0x0463 }
            android.content.ContentProviderOperation r0 = r0.build()     // Catch:{ all -> 0x0463 }
            r2.add(r0)     // Catch:{ all -> 0x0463 }
        L_0x03ea:
            r14 = 0
            goto L_0x03a1
        L_0x03ed:
            int r1 = X.AnonymousClass1Y3.BCt     // Catch:{ OperationApplicationException | RemoteException -> 0x0401 }
            X.0UN r0 = r9.A00     // Catch:{ OperationApplicationException | RemoteException -> 0x0401 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ OperationApplicationException | RemoteException -> 0x0401 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ OperationApplicationException | RemoteException -> 0x0401 }
            android.content.ContentResolver r1 = r0.getContentResolver()     // Catch:{ OperationApplicationException | RemoteException -> 0x0401 }
            java.lang.String r0 = "mms-sms"
            r1.applyBatch(r0, r2)     // Catch:{ OperationApplicationException | RemoteException -> 0x0401 }
            goto L_0x0419
        L_0x0401:
            r5 = move-exception
            r0 = 48
            java.lang.String r2 = X.AnonymousClass24B.$const$string(r0)     // Catch:{ all -> 0x0463 }
            java.lang.String r1 = "sms/mms thread mark read failed. # threads = %d"
            int r0 = r4.size()     // Catch:{ all -> 0x0463 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0463 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x0463 }
            X.C010708t.A0U(r2, r5, r1, r0)     // Catch:{ all -> 0x0463 }
        L_0x0419:
            if (r6 == 0) goto L_0x041e
            r6.close()     // Catch:{ all -> 0x046c }
        L_0x041e:
            r4.clear()     // Catch:{ all -> 0x046c }
        L_0x0421:
            com.google.common.collect.ImmutableList r0 = r7.A00     // Catch:{ all -> 0x046c }
            X.1Xv r2 = r0.iterator()     // Catch:{ all -> 0x046c }
        L_0x0427:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x046c }
            if (r0 == 0) goto L_0x0445
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x046c }
            com.facebook.messaging.service.model.MarkThreadFields r1 = (com.facebook.messaging.service.model.MarkThreadFields) r1     // Catch:{ all -> 0x046c }
            boolean r0 = r1.A07     // Catch:{ all -> 0x046c }
            if (r0 != 0) goto L_0x0427
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A06     // Catch:{ all -> 0x046c }
            long r0 = r0.A0G()     // Catch:{ all -> 0x046c }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x046c }
            r4.add(r0)     // Catch:{ all -> 0x046c }
            goto L_0x0427
        L_0x0445:
            boolean r0 = r4.isEmpty()     // Catch:{ all -> 0x046c }
            if (r0 != 0) goto L_0x02b8
            int r1 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x046c }
            r0 = r17
            X.0UN r0 = r0.A01     // Catch:{ all -> 0x046c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x046c }
            X.2e4 r0 = (X.C50462e4) r0     // Catch:{ all -> 0x046c }
            r0.A0I(r4)     // Catch:{ all -> 0x046c }
            goto L_0x02b8
        L_0x045c:
            r0 = -720617970(0xffffffffd50c3e0e, float:-9.6373844E12)
            X.C005505z.A00(r0)
            return r1
        L_0x0463:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0465 }
        L_0x0465:
            r0 = move-exception
            if (r6 == 0) goto L_0x046b
            r6.close()     // Catch:{ all -> 0x046b }
        L_0x046b:
            throw r0     // Catch:{ all -> 0x046c }
        L_0x046c:
            r1 = move-exception
            r0 = 336434122(0x140d93ca, float:7.147827E-27)
            X.C005505z.A00(r0)
            throw r1
        L_0x0474:
            r2 = r1
            X.1dO r2 = (X.C27561dO) r2
            android.os.Bundle r1 = r8.A00
            java.lang.String r0 = "markThreadsParams"
            android.os.Parcelable r0 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.MarkThreadsParams r0 = (com.facebook.messaging.service.model.MarkThreadsParams) r0
            com.facebook.fbservice.service.OperationResult r1 = r7.BAz(r8)
            com.google.common.collect.ImmutableList r0 = r0.A01
            X.C27561dO.A05(r2, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11150lz.A0f(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
    public OperationResult A0y(C11060ln r27, C27311cz r28) {
        Cursor query;
        C61182yT r8;
        ImmutableList immutableList;
        String str;
        MessagesCollection messagesCollection;
        Integer num;
        C11060ln r9 = r27;
        if (!(this instanceof C12440pM)) {
            C27311cz r21 = r28;
            if (!(this instanceof C11130lx)) {
                return r21.BAz(r9);
            }
            C11130lx r6 = (C11130lx) this;
            FetchThreadParams fetchThreadParams = (FetchThreadParams) AnonymousClass0ls.A00(r9, "fetchThreadParams");
            ArrayList arrayList = new ArrayList();
            HashSet hashSet = new HashSet();
            HashMap hashMap = new HashMap();
            FetchThreadParams fetchThreadParams2 = (FetchThreadParams) r9.A00.getParcelable("fetchThreadParams");
            ImmutableSet immutableSet = fetchThreadParams2.A04.A00;
            AnonymousClass04b r12 = new AnonymousClass04b();
            ImmutableList immutableList2 = fetchThreadParams2.A05;
            if (immutableList2 != null) {
                ((C14300t0) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AxE, r6.A00)).A07(immutableList2, true);
            }
            long now = AnonymousClass06A.A00.now();
            C11230mQ r15 = r6.A04;
            C09510hU r14 = fetchThreadParams2.A02;
            int i = fetchThreadParams2.A01;
            AnonymousClass04b r3 = new AnonymousClass04b();
            C24971Xv it = immutableSet.iterator();
            while (it.hasNext()) {
                ThreadKey threadKey = (ThreadKey) it.next();
                ThreadSummary A0R = r15.A01.A0R(threadKey);
                FetchThreadResult fetchThreadResult = null;
                if (C11230mQ.A01(r15, A0R, r14, i)) {
                    fetchThreadResult = C11230mQ.A00(r15, A0R);
                }
                r3.put(threadKey, fetchThreadResult);
            }
            long now2 = (AnonymousClass06A.A00.now() - now) / ((long) Math.max(immutableSet.size(), 1));
            C24971Xv it2 = immutableSet.iterator();
            while (it2.hasNext()) {
                ThreadKey threadKey2 = (ThreadKey) it2.next();
                FetchThreadResult fetchThreadResult2 = (FetchThreadResult) r3.get(threadKey2);
                HashMap hashMap2 = new HashMap();
                hashMap2.put("thread_cache_duration", Long.toString(now2));
                if (fetchThreadResult2 != null) {
                    num = AnonymousClass07B.A01;
                } else {
                    num = AnonymousClass07B.A00;
                }
                hashMap2.put("fetch_location", C52172id.A00(num));
                hashMap.put(threadKey2, hashMap2);
                r12.put(threadKey2, fetchThreadResult2);
            }
            int size = r12.size();
            for (int i2 = 0; i2 < size; i2++) {
                ThreadKey threadKey3 = (ThreadKey) r12.A07(i2);
                FetchThreadResult fetchThreadResult3 = (FetchThreadResult) r12.A09(i2);
                if (fetchThreadResult3 == null || FetchThreadResult.A01(fetchThreadParams, fetchThreadResult3)) {
                    hashSet.add(threadKey3);
                } else {
                    arrayList.add(fetchThreadResult3);
                }
            }
            if (!hashSet.isEmpty()) {
                C207529qM r32 = new C207529qM();
                r32.A01(r9);
                C60292wu r2 = new C60292wu();
                r2.A00(fetchThreadParams);
                r2.A03 = new ThreadCriteria(fetchThreadParams.A04.A01, hashSet);
                r32.A00.putParcelable("fetchThreadParams", new FetchThreadParams(r2));
                C11060ln A00 = r32.A00();
                boolean A01 = C11130lx.A01(r6);
                try {
                    ArrayList<FetchThreadResult> A0A = r21.BAz(A00).A0A();
                    if (A0A != null) {
                        Iterator it3 = A0A.iterator();
                        while (it3.hasNext()) {
                            FetchThreadResult fetchThreadResult4 = (FetchThreadResult) it3.next();
                            Map map = fetchThreadResult4.A00;
                            ThreadSummary threadSummary = fetchThreadResult4.A05;
                            if (!(map == null || threadSummary == null)) {
                                ThreadKey threadKey4 = threadSummary.A0S;
                                Object obj = (Map) hashMap.get(threadKey4);
                                if (obj == null) {
                                    obj = new HashMap();
                                    hashMap.put(threadKey4, obj);
                                }
                                obj.putAll(map);
                            }
                        }
                    }
                    if (A0A != null) {
                        for (FetchThreadResult fetchThreadResult5 : A0A) {
                            if (fetchThreadResult5.A02.A08) {
                                ThreadSummary threadSummary2 = fetchThreadResult5.A05;
                                boolean z = false;
                                if (threadSummary2 != null) {
                                    z = true;
                                }
                                ThreadSummary threadSummary3 = null;
                                if (z) {
                                    threadSummary3 = r6.A03.A0R(threadSummary2.A0S);
                                }
                                if (z && threadSummary3 != null) {
                                    long j = threadSummary2.A09;
                                    if (j != -1 && j < threadSummary3.A09) {
                                    }
                                }
                                if (threadSummary2 != null) {
                                    ThreadKey threadKey5 = threadSummary2.A0S;
                                    if (ThreadKey.A0E(threadKey5) && !AnonymousClass1Gw.A00(threadKey5) && (messagesCollection = fetchThreadResult5.A03) != null && messagesCollection.A08()) {
                                        ((C14300t0) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AxE, r6.A00)).A06(fetchThreadResult5.A07);
                                    }
                                }
                                ((C14800u9) r6.A01.get()).A03(((FetchThreadParams) A00.A00.getParcelable("fetchThreadParams")).A01, fetchThreadResult5);
                            }
                        }
                    }
                    if (A0A != null) {
                        arrayList.addAll(A0A);
                    }
                } finally {
                    if (A01) {
                        ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r6.A00)).A03();
                    }
                }
            }
            AnonymousClass0oE r4 = (AnonymousClass0oE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Are, r6.A00);
            for (Map A02 : hashMap.values()) {
                C11670nb r1 = new C11670nb("fetch_thread");
                C11670nb.A02(r1, A02, false);
                r4.A00.A09(r1);
            }
            return OperationResult.A05(arrayList);
        }
        C12440pM r13 = (C12440pM) this;
        C005505z.A03("SmsServiceHandler.handleFetchThreads", 2018936351);
        try {
            FetchThreadParams fetchThreadParams3 = (FetchThreadParams) r9.A00.getParcelable("fetchThreadParams");
            ImmutableSet immutableSet2 = fetchThreadParams3.A04.A00;
            C07410dQ r5 = new C07410dQ();
            C24971Xv it4 = immutableSet2.iterator();
            while (it4.hasNext()) {
                r5.A01(Long.valueOf(((ThreadKey) it4.next()).A0G()));
            }
            ImmutableSet A04 = r5.build();
            int i3 = fetchThreadParams3.A01;
            ArrayList arrayList2 = new ArrayList();
            C17610zB r0 = new C17610zB();
            C50462e4 r62 = (C50462e4) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKa, r13.A01);
            ImmutableSet immutableSet3 = A04;
            C17610zB r132 = new C17610zB();
            C005505z.A03("SmsThreadManager.getThreadSummaries", 853515176);
            try {
                ArrayList arrayList3 = new ArrayList();
                C24971Xv it5 = immutableSet3.iterator();
                while (it5.hasNext()) {
                    arrayList3.add(String.valueOf((Long) it5.next()));
                }
                C06140av A042 = C06160ax.A04("_id", arrayList3);
                query = ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, r62.A00)).getContentResolver().query(C50462e4.A05, C50462e4.A06, C50462e4.A06(r62, A042), C50462e4.A09(r62, A042), null);
                if (query != null) {
                    int columnIndex = query.getColumnIndex("_id");
                    while (query.moveToNext()) {
                        long j2 = query.getLong(columnIndex);
                        Map map2 = (Map) r0.A07(j2);
                        if (map2 == null) {
                            map2 = new HashMap();
                            r0.A0D(j2, map2);
                        }
                        ThreadSummary A043 = C50462e4.A04(r62, query, map2);
                        if (A043 == null) {
                            A043 = C50462e4.A02(r62, j2, map2);
                        }
                        if (A043 == null) {
                            A043 = C50462e4.A03(r62, j2, map2);
                        }
                        r132.A0D(j2, A043);
                    }
                    query.close();
                }
                C005505z.A00(1399440468);
                C17610zB r11 = new C17610zB();
                AnonymousClass9NU r42 = (AnonymousClass9NU) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AJF, r13.A01);
                C17610zB r7 = new C17610zB();
                C17610zB r63 = new C17610zB();
                C07410dQ r52 = new C07410dQ();
                C24971Xv it6 = A04.iterator();
                while (it6.hasNext()) {
                    r52.A01(ThreadKey.A03(((Long) it6.next()).longValue()).A0J());
                }
                C06140av A044 = C06160ax.A04("thread_key", r52.build());
                MessageCursorUtil messageCursorUtil = r42.A02;
                SQLiteDatabase A06 = ((C25771aN) r42.A04.get()).A06();
                String[] strArr = MessageCursorUtil.A0F;
                String A022 = A044.A02();
                String[] A045 = A044.A04();
                String num2 = Integer.toString(i3);
                SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
                sQLiteQueryBuilder.setTables(C61162yR.A01(strArr, A022, "timestamp_ms DESC"));
                r8 = new C61182yT(messageCursorUtil, sQLiteQueryBuilder.query(A06, strArr, A022, A045, null, null, "timestamp_ms DESC", num2), messageCursorUtil.A0D);
                while (true) {
                    Message A002 = r8.A00();
                    if (A002 == null) {
                        break;
                    }
                    ThreadKey threadKey6 = A002.A0U;
                    ArrayList arrayList4 = (ArrayList) r63.A07(threadKey6.A0G());
                    if (arrayList4 == null) {
                        arrayList4 = new ArrayList();
                        r63.A0D(threadKey6.A0G(), arrayList4);
                    }
                    arrayList4.add(A002);
                }
                r8.A1K.close();
                C17610zB r82 = new C17610zB();
                for (int i4 = 0; i4 < r63.A01(); i4++) {
                    r82.A0D(r63.A04(i4), ImmutableList.copyOf((Collection) r63.A06(i4)));
                }
                C24971Xv it7 = A04.iterator();
                while (it7.hasNext()) {
                    long longValue = ((Long) it7.next()).longValue();
                    ImmutableList immutableList3 = (ImmutableList) r82.A07(longValue);
                    if (immutableList3 == null) {
                        immutableList3 = RegularImmutableList.A02;
                    }
                    ImmutableList of = ImmutableList.of(AnonymousClass9NU.A06(r42, longValue, i3, -1), immutableList3);
                    AnonymousClass9OC r92 = new AnonymousClass9OC();
                    Preconditions.checkNotNull(of, "iterables");
                    Preconditions.checkNotNull(r92, "comparator");
                    ImmutableList copyOf = ImmutableList.copyOf(new C196909Nx(new C196819No(of, r92)));
                    r7.A0D(longValue, copyOf.subList(0, Math.min(i3, copyOf.size())));
                }
                C24971Xv it8 = A04.iterator();
                while (it8.hasNext()) {
                    long longValue2 = ((Long) it8.next()).longValue();
                    ImmutableList immutableList4 = (ImmutableList) r7.A07(longValue2);
                    if (immutableList4 == null) {
                        immutableList4 = RegularImmutableList.A02;
                    }
                    C33881oI r64 = new C33881oI();
                    r64.A00 = ThreadKey.A03(longValue2);
                    r64.A01(immutableList4);
                    boolean z2 = false;
                    if (immutableList4.size() < i3) {
                        z2 = true;
                    }
                    r64.A03 = z2;
                    r64.A02 = true;
                    r11.A0D(longValue2, r64.A00());
                }
                C24971Xv it9 = A04.iterator();
                while (it9.hasNext()) {
                    long longValue3 = ((Long) it9.next()).longValue();
                    Map map3 = (Map) r0.A07(longValue3);
                    ThreadSummary threadSummary4 = (ThreadSummary) r132.A07(longValue3);
                    MessagesCollection messagesCollection2 = (MessagesCollection) r11.A07(longValue3);
                    if (!(threadSummary4 == null || messagesCollection2 == null)) {
                        Message A05 = messagesCollection2.A05();
                        if (A05 != null) {
                            String str2 = A05.A10;
                            boolean z3 = false;
                            if (!C28841fS.A0M(A05) || Platform.stringIsNullOrEmpty(str2)) {
                                int i5 = AnonymousClass1Y3.AnD;
                                if (((C57262rh) AnonymousClass1XX.A02(0, i5, r13.A01)).A0D(A05)) {
                                    str = ((C57262rh) AnonymousClass1XX.A02(0, i5, r13.A01)).A08(A05, threadSummary4.A0e);
                                } else if (Platform.stringIsNullOrEmpty(str2)) {
                                    str = C57262rh.A04((C57262rh) AnonymousClass1XX.A02(0, i5, r13.A01), A05, threadSummary4.A0e, true);
                                } else {
                                    str = null;
                                }
                            } else {
                                str = str2;
                            }
                            C17920zh A003 = ThreadSummary.A00();
                            A003.A02(threadSummary4);
                            A003.A0s = str2;
                            A003.A0l = str;
                            A003.A0P = A05.A0K;
                            if (A05.A0S.A02 != C36891u4.A07) {
                                z3 = true;
                            }
                            A003.A0y = z3;
                            threadSummary4 = A003.A00();
                        }
                        C61222yX A004 = FetchThreadResult.A00();
                        A004.A01 = DataFetchDisposition.A0F;
                        A004.A00 = ((AnonymousClass06B) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AgK, r13.A01)).now();
                        A004.A04 = threadSummary4;
                        A004.A02 = messagesCollection2;
                        if (map3 == null) {
                            immutableList = RegularImmutableList.A02;
                        } else {
                            immutableList = ImmutableList.copyOf(map3.values());
                        }
                        A004.A06 = immutableList;
                        arrayList2.add(A004.A00());
                    }
                }
                if (!A04.isEmpty() && arrayList2.isEmpty()) {
                    arrayList2.add(FetchThreadResult.A09);
                }
                OperationResult A052 = OperationResult.A05(arrayList2);
                C005505z.A00(329974901);
                return A052;
            } catch (Throwable th) {
                th = th;
                C005505z.A00(-1061403717);
                throw th;
            }
        } catch (Throwable th2) {
            C005505z.A00(-1366183929);
            throw th2;
        }
    }

    public OperationResult A0z(C11060ln r2, C27311cz r3) {
        return r3.BAz(r2);
    }

    public C11150lz(String str) {
        super(str);
    }
}
