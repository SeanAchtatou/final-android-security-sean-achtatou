package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1QW  reason: invalid class name */
public final class AnonymousClass1QW extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C101074rt A01;
    @Comparable(type = 13)
    public C101034rp A02;
    @Comparable(type = 13)
    public MigColorScheme A03;

    public AnonymousClass1QW(Context context) {
        super("M4AboutPreferenceLayout");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
