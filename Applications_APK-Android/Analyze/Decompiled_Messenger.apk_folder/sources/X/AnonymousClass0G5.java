package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0G5  reason: invalid class name */
public final class AnonymousClass0G5 {
    private static volatile AnonymousClass0G5 A04;
    public final AnonymousClass03g A00;
    public final AnonymousClass0G7 A01 = new AnonymousClass0G7(this.A02, new AnonymousClass0G6(this));
    private final Context A02;
    private final C25051Yd A03;

    public static final AnonymousClass0G5 A01(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass0G5.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass0G5(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static boolean A02(AnonymousClass0G5 r2) {
        return r2.A03.Aem(281938833507191L);
    }

    private AnonymousClass0G5(AnonymousClass1XY r4) {
        this.A02 = AnonymousClass1YA.A00(r4);
        AnonymousClass0WA.A00(r4);
        this.A00 = AnonymousClass03g.A00(r4);
        this.A03 = AnonymousClass0WT.A00(r4);
    }

    public static final AnonymousClass0G5 A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
