package X;

import android.content.Context;
import android.util.BoostFramework;
import io.card.payment.BuildConfig;
import org.json.JSONObject;

/* renamed from: X.1pt  reason: invalid class name and case insensitive filesystem */
public final class C34611pt implements C26381bM {
    private static boolean A01;
    private static boolean A02;
    private final Context A00;

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0059, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[ExcHandler: Error | Exception (unused java.lang.Throwable), SYNTHETIC, Splitter:B:1:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00() {
        /*
            r6 = 0
            java.lang.Class<android.util.BoostFramework> r3 = android.util.BoostFramework.class
            r3.toString()     // Catch:{ Error | Exception -> 0x0059 }
            java.lang.String r2 = "perfLockAcquire"
            r7 = 2
            java.lang.Class[] r1 = new java.lang.Class[r7]     // Catch:{ Error | Exception -> 0x0059 }
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ Error | Exception -> 0x0059 }
            r1[r6] = r0     // Catch:{ Error | Exception -> 0x0059 }
            java.lang.Class<int[]> r0 = int[].class
            r5 = 1
            r1[r5] = r0     // Catch:{ Error | Exception -> 0x0059 }
            boolean r0 = X.C34061oa.A01(r3, r2, r1)     // Catch:{ Error | Exception -> 0x0059 }
            if (r0 == 0) goto L_0x0058
            java.lang.Class<android.util.BoostFramework> r2 = android.util.BoostFramework.class
            java.lang.String r1 = "perfLockRelease"
            java.lang.Class[] r0 = new java.lang.Class[r6]     // Catch:{ Error | Exception -> 0x0059 }
            boolean r0 = X.C34061oa.A01(r3, r1, r0)     // Catch:{ Error | Exception -> 0x0059 }
            if (r0 == 0) goto L_0x0058
            java.lang.Class<android.util.BoostFramework> r4 = android.util.BoostFramework.class
            java.lang.String r3 = "perfHint"
            r0 = 4
            java.lang.Class[] r2 = new java.lang.Class[r0]     // Catch:{ Error | Exception -> 0x0059 }
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ Error | Exception -> 0x0059 }
            r2[r6] = r0     // Catch:{ Error | Exception -> 0x0059 }
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            r2[r5] = r0     // Catch:{ Error | Exception -> 0x0059 }
            java.lang.Class r1 = java.lang.Integer.TYPE     // Catch:{ Error | Exception -> 0x0059 }
            r2[r7] = r1     // Catch:{ Error | Exception -> 0x0059 }
            r0 = 3
            r2[r0] = r1     // Catch:{ Error | Exception -> 0x0059 }
            boolean r0 = X.C34061oa.A01(r4, r3, r2)     // Catch:{ Error | Exception -> 0x0059 }
            X.C34611pt.A01 = r0     // Catch:{ Error | Exception -> 0x0059 }
            java.lang.Class<android.util.BoostFramework> r3 = android.util.BoostFramework.class
            java.lang.Class[] r2 = new java.lang.Class[r5]     // Catch:{ Error | Exception -> 0x0059 }
            java.lang.Class<android.content.Context> r0 = android.content.Context.class
            r2[r6] = r0     // Catch:{ Error | Exception -> 0x0059 }
            r1 = 0
            if (r4 == 0) goto L_0x0051
            java.lang.reflect.Constructor r1 = r4.getConstructor(r2)     // Catch:{ Exception -> 0x0051, Error | Exception -> 0x0059 }
        L_0x0051:
            r0 = 0
            if (r1 == 0) goto L_0x0055
            r0 = 1
        L_0x0055:
            X.C34611pt.A02 = r0     // Catch:{ Error | Exception -> 0x0059 }
            return r5
        L_0x0058:
            return r6
        L_0x0059:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34611pt.A00():boolean");
    }

    public int AyN() {
        return 1;
    }

    public int AyO() {
        return 1;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            String A0J = AnonymousClass08S.A0J(A01 ? "perfHint" : BuildConfig.FLAVOR, A02 ? "useContext" : BuildConfig.FLAVOR);
            jSONObject.put("name", "qualcomm");
            jSONObject.put("framework", "BoostFramework");
            jSONObject.put("extra", A0J);
            return jSONObject.toString();
        } catch (Exception unused) {
            return BuildConfig.FLAVOR;
        }
    }

    public C34611pt(Context context) {
        this.A00 = context;
    }

    public AnonymousClass0f7 AUy(C26401bO r5, C26501bY r6) {
        BoostFramework boostFramework;
        int[] Aet = r5.Aet(r6);
        if (Aet == null || Aet.length == 0) {
            return null;
        }
        if (A02) {
            boostFramework = new BoostFramework(this.A00);
        } else {
            boostFramework = new BoostFramework();
        }
        return new C34621pu(boostFramework, r6.A01, Aet);
    }
}
