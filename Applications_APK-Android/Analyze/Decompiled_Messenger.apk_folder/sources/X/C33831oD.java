package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.util.Comparator;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1oD  reason: invalid class name and case insensitive filesystem */
public final class C33831oD implements Comparator {
    public int compare(Object obj, Object obj2) {
        return A00((C32631m4) obj) - A00((C32631m4) obj2);
    }

    private static int A00(C32631m4 r2) {
        switch (r2.ordinal()) {
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return 1;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return 2;
            case 8:
                return 3;
            case Process.SIGKILL:
                return 4;
            default:
                return 0;
        }
    }
}
