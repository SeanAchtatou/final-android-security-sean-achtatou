package X;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrength;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.facebook.acra.ACRA;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.forker.Process;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.card.payment.BuildConfig;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.1k0  reason: invalid class name and case insensitive filesystem */
public final class C31631k0 {
    private static volatile C31631k0 A0A;
    public SubscriptionManager A00 = null;
    public C29911hB A01 = null;
    public AnonymousClass0UN A02;
    public String A03 = BuildConfig.FLAVOR;
    public String A04 = "UNKNOWN";
    private ObjectNode A05 = null;
    private boolean A06 = false;
    public final AtomicBoolean A07 = new AtomicBoolean();
    public volatile boolean A08 = false;
    public volatile boolean A09 = true;

    private static boolean A06(int i) {
        return i != Integer.MAX_VALUE;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0185, code lost:
        if (r7 != null) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        if (r6.equals("UNKNOWN") == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007f, code lost:
        if (r6.equals("4G") == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0089, code lost:
        if (r6.equals("3G") == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0093, code lost:
        if (r6.equals("2G") == false) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A07(X.C29911hB r16) {
        /*
            r15 = this;
            r9 = 0
            r3 = r16
            if (r16 == 0) goto L_0x0189
            android.telephony.TelephonyManager r0 = r3.A00
            boolean r0 = r0.isNetworkRoaming()
            r15.A06 = r0
            android.telephony.TelephonyManager r0 = r3.A00
            int r5 = r0.getPhoneType()     // Catch:{ NotFoundException | SecurityException -> 0x0014 }
            goto L_0x0015
        L_0x0014:
            r5 = -1
        L_0x0015:
            java.lang.String r6 = r15.A04
            int r1 = r6.hashCode()
            r0 = 1621(0x655, float:2.272E-42)
            r4 = 2
            r2 = 1
            if (r1 == r0) goto L_0x008c
            r0 = 1652(0x674, float:2.315E-42)
            if (r1 == r0) goto L_0x0082
            r0 = 1683(0x693, float:2.358E-42)
            if (r1 == r0) goto L_0x0078
            r0 = 433141802(0x19d1382a, float:2.1632778E-23)
            if (r1 != r0) goto L_0x0037
            java.lang.String r0 = "UNKNOWN"
            boolean r0 = r6.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x0038
        L_0x0037:
            r1 = -1
        L_0x0038:
            if (r1 == 0) goto L_0x0075
            if (r1 == r2) goto L_0x0069
            if (r1 != r4) goto L_0x0073
            if (r5 != r2) goto L_0x006e
            java.lang.Class<android.telephony.CellInfoGsm> r5 = android.telephony.CellInfoGsm.class
        L_0x0042:
            java.util.List r0 = r15.A04(r3)
            if (r5 == 0) goto L_0x0189
            if (r0 == 0) goto L_0x0189
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.Iterator r2 = r0.iterator()
        L_0x0053:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0096
            java.lang.Object r1 = r2.next()
            android.telephony.CellInfo r1 = (android.telephony.CellInfo) r1
            boolean r0 = r5.isInstance(r1)
            if (r0 == 0) goto L_0x0053
            r4.add(r1)
            goto L_0x0053
        L_0x0069:
            if (r5 != r2) goto L_0x006e
            java.lang.Class<android.telephony.CellInfoWcdma> r5 = android.telephony.CellInfoWcdma.class
            goto L_0x0042
        L_0x006e:
            if (r5 != r4) goto L_0x0073
            java.lang.Class<android.telephony.CellInfoCdma> r5 = android.telephony.CellInfoCdma.class
            goto L_0x0042
        L_0x0073:
            r5 = 0
            goto L_0x0042
        L_0x0075:
            java.lang.Class<android.telephony.CellInfoLte> r5 = android.telephony.CellInfoLte.class
            goto L_0x0042
        L_0x0078:
            java.lang.String r0 = "4G"
            boolean r0 = r6.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0038
            goto L_0x0037
        L_0x0082:
            java.lang.String r0 = "3G"
            boolean r0 = r6.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0038
            goto L_0x0037
        L_0x008c:
            java.lang.String r0 = "2G"
            boolean r0 = r6.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0038
            goto L_0x0037
        L_0x0096:
            long r1 = java.lang.System.nanoTime()
            int r0 = r4.size()
            r8 = 1
            if (r0 != r8) goto L_0x00ae
            java.lang.Object r7 = r4.get(r9)
            android.telephony.CellInfo r7 = (android.telephony.CellInfo) r7
        L_0x00a7:
            com.fasterxml.jackson.databind.node.ObjectNode r0 = A02(r7, r1)
            r15.A05 = r0
            return r8
        L_0x00ae:
            int r0 = r4.size()
            if (r0 <= r8) goto L_0x0189
            java.lang.Object r0 = r4.get(r9)
            boolean r0 = r0 instanceof android.telephony.CellInfoCdma
            if (r0 != 0) goto L_0x0189
            r7 = 0
            if (r16 == 0) goto L_0x0185
            android.telephony.TelephonyManager r0 = r3.A00
            boolean r0 = r0.isNetworkRoaming()
            if (r0 == 0) goto L_0x017c
            android.telephony.TelephonyManager r0 = r3.A00
            java.lang.String r10 = r0.getNetworkOperator()
        L_0x00cd:
            int r3 = android.os.Build.VERSION.SDK_INT
            r0 = 18
            if (r3 < r0) goto L_0x0185
            java.util.Iterator r13 = r4.iterator()
            r14 = r7
            r12 = 0
        L_0x00d9:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x0184
            java.lang.Object r6 = r13.next()
            android.telephony.CellInfo r6 = (android.telephony.CellInfo) r6
            r5 = 0
            if (r6 == 0) goto L_0x0171
            boolean r0 = r6 instanceof android.telephony.CellInfoCdma
            if (r0 != 0) goto L_0x0171
            boolean r0 = r6 instanceof android.telephony.CellInfoGsm
            r4 = 2147483647(0x7fffffff, float:NaN)
            if (r0 == 0) goto L_0x013a
            r3 = r6
            android.telephony.CellInfoGsm r3 = (android.telephony.CellInfoGsm) r3
            android.telephony.CellIdentityGsm r0 = r3.getCellIdentity()
            int r11 = r0.getMcc()
            android.telephony.CellIdentityGsm r0 = r3.getCellIdentity()
            int r3 = r0.getMnc()
        L_0x0106:
            if (r11 == r4) goto L_0x0174
            if (r3 == r4) goto L_0x0174
            r0 = 999(0x3e7, float:1.4E-42)
            r4 = 4
            if (r11 > r0) goto L_0x0125
            r4 = 3
            java.lang.String r0 = r10.substring(r9, r4)     // Catch:{ IndexOutOfBoundsException | NumberFormatException -> 0x0174 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ IndexOutOfBoundsException | NumberFormatException -> 0x0174 }
            if (r11 != r0) goto L_0x0174
            java.lang.String r0 = r10.substring(r4)     // Catch:{ IndexOutOfBoundsException | NumberFormatException -> 0x0174 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ IndexOutOfBoundsException | NumberFormatException -> 0x0174 }
            if (r3 != r0) goto L_0x0174
            goto L_0x0173
        L_0x0125:
            java.lang.String r0 = r10.substring(r9, r4)     // Catch:{ IndexOutOfBoundsException | NumberFormatException -> 0x0174 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ IndexOutOfBoundsException | NumberFormatException -> 0x0174 }
            if (r11 != r0) goto L_0x0174
            java.lang.String r0 = r10.substring(r4)     // Catch:{ IndexOutOfBoundsException | NumberFormatException -> 0x0174 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ IndexOutOfBoundsException | NumberFormatException -> 0x0174 }
            if (r3 != r0) goto L_0x0174
            goto L_0x0173
        L_0x013a:
            boolean r0 = r6 instanceof android.telephony.CellInfoLte
            if (r0 == 0) goto L_0x0152
            r3 = r6
            android.telephony.CellInfoLte r3 = (android.telephony.CellInfoLte) r3
            android.telephony.CellIdentityLte r0 = r3.getCellIdentity()
            int r11 = r0.getMcc()
            android.telephony.CellIdentityLte r0 = r3.getCellIdentity()
            int r3 = r0.getMnc()
            goto L_0x0106
        L_0x0152:
            boolean r0 = r6 instanceof android.telephony.CellInfoWcdma
            if (r0 == 0) goto L_0x016a
            r3 = r6
            android.telephony.CellInfoWcdma r3 = (android.telephony.CellInfoWcdma) r3
            android.telephony.CellIdentityWcdma r0 = r3.getCellIdentity()
            int r11 = r0.getMcc()
            android.telephony.CellIdentityWcdma r0 = r3.getCellIdentity()
            int r3 = r0.getMnc()
            goto L_0x0106
        L_0x016a:
            r3 = 2147483647(0x7fffffff, float:NaN)
            r11 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x0106
        L_0x0171:
            r5 = 0
            goto L_0x0174
        L_0x0173:
            r5 = 1
        L_0x0174:
            if (r5 == 0) goto L_0x00d9
            if (r12 != 0) goto L_0x0185
            r14 = r6
            r12 = 1
            goto L_0x00d9
        L_0x017c:
            android.telephony.TelephonyManager r0 = r3.A00
            java.lang.String r10 = r0.getSimOperator()
            goto L_0x00cd
        L_0x0184:
            r7 = r14
        L_0x0185:
            if (r7 == 0) goto L_0x0189
            goto L_0x00a7
        L_0x0189:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31631k0.A07(X.1hB):boolean");
    }

    public static final C31631k0 A00(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (C31631k0.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new C31631k0(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    private static ArrayNode A01(List list, long j) {
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                arrayNode.add(A02((CellInfo) it.next(), j));
            }
        }
        return arrayNode;
    }

    private static ObjectNode A02(CellInfo cellInfo, long j) {
        int level;
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        if (cellInfo instanceof CellInfoCdma) {
            objectNode.put("network_type", "cdma");
            CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
            CellIdentityCdma cellIdentity = cellInfoCdma.getCellIdentity();
            int basestationId = cellIdentity.getBasestationId();
            int latitude = cellIdentity.getLatitude();
            int longitude = cellIdentity.getLongitude();
            int networkId = cellIdentity.getNetworkId();
            int systemId = cellIdentity.getSystemId();
            if (A06(basestationId)) {
                objectNode.put("cdma_base_station_id", basestationId);
            }
            if (A06(latitude)) {
                objectNode.put("cdma_base_station_latitude", latitude);
            }
            if (A06(longitude)) {
                objectNode.put("cdma_base_station_longitude", longitude);
            }
            if (A06(networkId)) {
                objectNode.put("cdma_network_id", networkId);
            }
            if (A06(systemId)) {
                objectNode.put("cdma_system_id", systemId);
            }
            CellSignalStrengthCdma cellSignalStrength = cellInfoCdma.getCellSignalStrength();
            objectNode.put("signal_asu_level", cellSignalStrength.getAsuLevel());
            objectNode.put("signal_dbm", cellSignalStrength.getDbm());
            objectNode.put("signal_level", cellSignalStrength.getLevel());
            objectNode.put("signal_cdma_dbm", cellSignalStrength.getCdmaDbm());
            objectNode.put("signal_cdma_ecio", cellSignalStrength.getCdmaEcio());
            objectNode.put("signal_cdma_level", cellSignalStrength.getCdmaLevel());
            objectNode.put("signal_evdo_dbm", cellSignalStrength.getEvdoDbm());
            objectNode.put("signal_evdo_ecio", cellSignalStrength.getEvdoEcio());
            objectNode.put("signal_evdo_level", cellSignalStrength.getEvdoLevel());
            objectNode.put("signal_evdo_snr", cellSignalStrength.getEvdoSnr());
        } else {
            if (cellInfo instanceof CellInfoGsm) {
                objectNode.put("network_type", "gsm");
                CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
                CellIdentityGsm cellIdentity2 = cellInfoGsm.getCellIdentity();
                int cid = cellIdentity2.getCid();
                if (A06(cid)) {
                    objectNode.put("gsm_cid", cid);
                }
                int mcc = cellIdentity2.getMcc();
                if (A06(mcc)) {
                    objectNode.put("gsm_mcc", mcc);
                }
                int mnc = cellIdentity2.getMnc();
                if (A06(mnc)) {
                    objectNode.put("gsm_mnc", mnc);
                }
                int lac = cellIdentity2.getLac();
                if (A06(lac)) {
                    objectNode.put("gsm_lac", lac);
                }
                if (Build.VERSION.SDK_INT >= 24) {
                    int arfcn = cellIdentity2.getArfcn();
                    if (A06(arfcn)) {
                        objectNode.put("gsm_arfcn", arfcn);
                    }
                }
                CellSignalStrengthGsm cellSignalStrength2 = cellInfoGsm.getCellSignalStrength();
                objectNode.put("signal_asu_level", cellSignalStrength2.getAsuLevel());
                objectNode.put("signal_dbm", cellSignalStrength2.getDbm());
                level = cellSignalStrength2.getLevel();
            } else if (cellInfo instanceof CellInfoLte) {
                objectNode.put("network_type", "lte");
                CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
                CellIdentityLte cellIdentity3 = cellInfoLte.getCellIdentity();
                int ci = cellIdentity3.getCi();
                if (A06(ci)) {
                    objectNode.put("lte_ci", ci);
                }
                int mcc2 = cellIdentity3.getMcc();
                if (A06(mcc2)) {
                    objectNode.put("lte_mcc", mcc2);
                }
                int mnc2 = cellIdentity3.getMnc();
                if (A06(mnc2)) {
                    objectNode.put("lte_mnc", mnc2);
                }
                int pci = cellIdentity3.getPci();
                if (A06(pci)) {
                    objectNode.put("lte_pci", pci);
                }
                int tac = cellIdentity3.getTac();
                if (A06(tac)) {
                    objectNode.put("lte_tac", tac);
                }
                int i = Build.VERSION.SDK_INT;
                if (i >= 24) {
                    int earfcn = cellIdentity3.getEarfcn();
                    if (A06(earfcn)) {
                        objectNode.put("lte_earfcn", earfcn);
                    }
                }
                if (i >= 28) {
                    int bandwidth = cellIdentity3.getBandwidth();
                    if (A06(bandwidth)) {
                        objectNode.put("lte_bandwidth", bandwidth);
                    }
                }
                CellSignalStrengthLte cellSignalStrength3 = cellInfoLte.getCellSignalStrength();
                objectNode.put("signal_asu_level", cellSignalStrength3.getAsuLevel());
                objectNode.put("signal_dbm", cellSignalStrength3.getDbm());
                objectNode.put("signal_level", cellSignalStrength3.getLevel());
                objectNode.put("signal_lte_timing_advance", cellSignalStrength3.getTimingAdvance());
                if (Build.VERSION.SDK_INT >= 24) {
                    objectNode.put("lte_rsrq", cellSignalStrength3.getRsrq());
                    objectNode.put("lte_rssnr", cellSignalStrength3.getRssnr());
                }
            } else if (cellInfo instanceof CellInfoWcdma) {
                objectNode.put("network_type", "wcdma");
                CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
                CellIdentityWcdma cellIdentity4 = cellInfoWcdma.getCellIdentity();
                int cid2 = cellIdentity4.getCid();
                if (A06(cid2)) {
                    objectNode.put("wcdma_cid", cid2);
                }
                int mcc3 = cellIdentity4.getMcc();
                if (A06(mcc3)) {
                    objectNode.put("wcdma_mcc", mcc3);
                }
                int mnc3 = cellIdentity4.getMnc();
                if (A06(mnc3)) {
                    objectNode.put("wcdma_mnc", mnc3);
                }
                int psc = cellIdentity4.getPsc();
                if (A06(psc)) {
                    objectNode.put("wcdma_psc", psc);
                }
                int lac2 = cellIdentity4.getLac();
                if (A06(lac2)) {
                    objectNode.put("wcdma_lac", lac2);
                }
                if (Build.VERSION.SDK_INT >= 24) {
                    int uarfcn = cellIdentity4.getUarfcn();
                    if (A06(uarfcn)) {
                        objectNode.put("wcdma_uarfcn", uarfcn);
                    }
                }
                CellSignalStrengthWcdma cellSignalStrength4 = cellInfoWcdma.getCellSignalStrength();
                objectNode.put("signal_asu_level", cellSignalStrength4.getAsuLevel());
                objectNode.put("signal_dbm", cellSignalStrength4.getDbm());
                level = cellSignalStrength4.getLevel();
            }
            objectNode.put("signal_level", level);
        }
        objectNode.put("freshness", j - cellInfo.getTimeStamp());
        return objectNode;
    }

    public static String A03(String str) {
        if (str != null) {
            String lowerCase = str.toLowerCase(Locale.US);
            char c = 65535;
            switch (lowerCase.hashCode()) {
                case -1324722188:
                    if (lowerCase.equals("dc_hspap")) {
                        c = 13;
                        break;
                    }
                    break;
                case -1291358803:
                    if (lowerCase.equals("evdo_0")) {
                        c = 15;
                        break;
                    }
                    break;
                case -1291358754:
                    if (lowerCase.equals("evdo_a")) {
                        c = 16;
                        break;
                    }
                    break;
                case -1291358753:
                    if (lowerCase.equals("evdo_b")) {
                        c = 17;
                        break;
                    }
                    break;
                case -1092835250:
                    if (lowerCase.equals("lte-ca")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1092787200:
                    if (lowerCase.equals("lte_ca")) {
                        c = 4;
                        break;
                    }
                    break;
                case -698359411:
                    if (lowerCase.equals("cdma - 1xrtt")) {
                        c = 28;
                        break;
                    }
                    break;
                case -650813115:
                    if (lowerCase.equals("cdma - ehrpd")) {
                        c = 6;
                        break;
                    }
                    break;
                case 1684:
                    if (lowerCase.equals("3g")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1715:
                    if (lowerCase.equals("4g")) {
                        c = 0;
                        break;
                    }
                    break;
                case 102657:
                    if (lowerCase.equals("gsm")) {
                        c = ' ';
                        break;
                    }
                    break;
                case 107485:
                    if (lowerCase.equals("lte")) {
                        c = 2;
                        break;
                    }
                    break;
                case 3008352:
                    if (lowerCase.equals("axgp")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3048885:
                    if (lowerCase.equals("cdma")) {
                        c = 26;
                        break;
                    }
                    break;
                case 3108285:
                    if (lowerCase.equals("edge")) {
                        c = 30;
                        break;
                    }
                    break;
                case 3179754:
                    if (lowerCase.equals("gprs")) {
                        c = 31;
                        break;
                    }
                    break;
                case 3212348:
                    if (lowerCase.equals("hspa")) {
                        c = 19;
                        break;
                    }
                    break;
                case 3594007:
                    if (lowerCase.equals("umts")) {
                        c = 23;
                        break;
                    }
                    break;
                case 48940715:
                    if (lowerCase.equals("1xrtt")) {
                        c = 25;
                        break;
                    }
                    break;
                case 96487011:
                    if (lowerCase.equals("ehrpd")) {
                        c = 14;
                        break;
                    }
                    break;
                case 99571818:
                    if (lowerCase.equals("hsdpa")) {
                        c = 18;
                        break;
                    }
                    break;
                case 99582831:
                    if (lowerCase.equals("hspa+")) {
                        c = 21;
                        break;
                    }
                    break;
                case 99582900:
                    if (lowerCase.equals("hspap")) {
                        c = 20;
                        break;
                    }
                    break;
                case 99588155:
                    if (lowerCase.equals("hsupa")) {
                        c = 22;
                        break;
                    }
                    break;
                case 112947884:
                    if (lowerCase.equals("wcdma")) {
                        c = 24;
                        break;
                    }
                    break;
                case 399615685:
                    if (lowerCase.equals("cdma - 1x")) {
                        c = 27;
                        break;
                    }
                    break;
                case 401368199:
                    if (lowerCase.equals("cdma evdo")) {
                        c = 10;
                        break;
                    }
                    break;
                case 635052210:
                    if (lowerCase.equals("cdma 1x")) {
                        c = 29;
                        break;
                    }
                    break;
                case 1489923733:
                    if (lowerCase.equals("dchspap")) {
                        c = 12;
                        break;
                    }
                    break;
                case 1538787489:
                    if (lowerCase.equals("dc-hspa+")) {
                        c = 11;
                        break;
                    }
                    break;
                case 1870782017:
                    if (lowerCase.equals("cdma - evdo rev. 0")) {
                        c = 7;
                        break;
                    }
                    break;
                case 1870782066:
                    if (lowerCase.equals("cdma - evdo rev. a")) {
                        c = 8;
                        break;
                    }
                    break;
                case 1870782067:
                    if (lowerCase.equals("cdma - evdo rev. b")) {
                        c = 9;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    return "4G";
                case 5:
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case 8:
                case Process.SIGKILL:
                case AnonymousClass1Y3.A01:
                case AnonymousClass1Y3.A02:
                case AnonymousClass1Y3.A03:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case Process.SIGCONT:
                case Process.SIGSTOP:
                case 20:
                case AnonymousClass1Y3.A05:
                case AnonymousClass1Y3.A06:
                case 23:
                case AnonymousClass1Y3.A07:
                    return "3G";
                case 25:
                case AnonymousClass1Y3.A08:
                case AnonymousClass1Y3.A09:
                case 28:
                case 29:
                case AnonymousClass1Y3.A0A:
                case AnonymousClass1Y3.A0B:
                case ' ':
                    return "2G";
            }
        }
        return "UNKNOWN";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005b, code lost:
        if (r1 == 0) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005e, code lost:
        if (r2 != 0) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0061, code lost:
        if (r1 != false) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0080, code lost:
        if (r2 != 0) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009b, code lost:
        if (r3 != 535) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b7, code lost:
        if (r1 == 0) goto L_0x005d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List A04(X.C29911hB r10) {
        /*
            r9 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r3 = 0
            r0 = 17
            if (r1 < r0) goto L_0x00bb
            if (r10 == 0) goto L_0x00bb
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AWJ
            X.0UN r0 = r9.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pP r1 = (X.C12460pP) r1
            r0 = 10
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            boolean r0 = r1.A08(r0)
            if (r0 == 0) goto L_0x00bb
            java.lang.String r0 = "CellDiagnosticsSerializer"
            java.util.List r1 = r10.A03(r0)
            if (r1 == 0) goto L_0x00bb
            java.util.ArrayList r6 = new java.util.ArrayList
            r0 = 1
            r6.<init>(r0)
            java.util.Iterator r8 = r1.iterator()
        L_0x0032:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x00ba
            java.lang.Object r5 = r8.next()
            android.telephony.CellInfo r5 = (android.telephony.CellInfo) r5
            boolean r0 = r5.isRegistered()
            if (r0 == 0) goto L_0x0032
            r0 = r5
            if (r5 == 0) goto L_0x0064
            boolean r1 = r5 instanceof android.telephony.CellInfoGsm
            r7 = 0
            if (r1 == 0) goto L_0x006a
            r1 = r5
            android.telephony.CellInfoGsm r1 = (android.telephony.CellInfoGsm) r1
            android.telephony.CellIdentityGsm r2 = r1.getCellIdentity()
            int r1 = r2.getMcc()
            int r2 = r2.getMnc()
            if (r1 != 0) goto L_0x0060
        L_0x005d:
            r1 = 1
            if (r2 == 0) goto L_0x0061
        L_0x0060:
            r1 = 0
        L_0x0061:
            if (r1 == 0) goto L_0x0064
        L_0x0063:
            r0 = r7
        L_0x0064:
            if (r0 == 0) goto L_0x0032
            r6.add(r5)
            goto L_0x0032
        L_0x006a:
            boolean r1 = r5 instanceof android.telephony.CellInfoLte
            if (r1 == 0) goto L_0x009e
            r1 = r5
            android.telephony.CellInfoLte r1 = (android.telephony.CellInfoLte) r1
            android.telephony.CellIdentityLte r3 = r1.getCellIdentity()
            int r1 = r3.getMcc()
            int r2 = r3.getMnc()
            if (r1 != 0) goto L_0x0082
            r1 = 1
            if (r2 == 0) goto L_0x0083
        L_0x0082:
            r1 = 0
        L_0x0083:
            if (r1 != 0) goto L_0x0063
            int r2 = r3.getCi()
            int r4 = r3.getMcc()
            int r3 = r3.getMnc()
            r1 = 17575755(0x10c2f4b, float:2.574787E-38)
            if (r2 != r1) goto L_0x0060
            r2 = 535(0x217, float:7.5E-43)
            if (r4 != r2) goto L_0x0060
            r1 = 1
            if (r3 == r2) goto L_0x0061
            goto L_0x0060
        L_0x009e:
            int r2 = android.os.Build.VERSION.SDK_INT
            r1 = 18
            if (r2 < r1) goto L_0x0064
            boolean r1 = r5 instanceof android.telephony.CellInfoWcdma
            if (r1 == 0) goto L_0x0064
            r1 = r5
            android.telephony.CellInfoWcdma r1 = (android.telephony.CellInfoWcdma) r1
            android.telephony.CellIdentityWcdma r2 = r1.getCellIdentity()
            int r1 = r2.getMcc()
            int r2 = r2.getMnc()
            if (r1 != 0) goto L_0x0060
            goto L_0x005d
        L_0x00ba:
            return r6
        L_0x00bb:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31631k0.A04(X.1hB):java.util.List");
    }

    private void A05(C29911hB r5, ObjectNode objectNode) {
        String str;
        int i;
        if (r5 != null && objectNode != null) {
            objectNode.put("sim_operator_mcc_mnc", r5.A00.getSimOperator());
            objectNode.put("network_operator_mcc_mnc", r5.A00.getNetworkOperator());
            objectNode.put("is_network_roaming", r5.A00.isNetworkRoaming());
            int dataActivity = r5.A00.getDataActivity();
            if (dataActivity == 1) {
                str = "IN";
            } else if (dataActivity == 2) {
                str = "OUT";
            } else if (dataActivity == 3) {
                str = "INOUT";
            } else if (dataActivity != 4) {
                str = "NONE";
            } else {
                str = "DORMANT";
            }
            objectNode.put("data_activity", str);
            if (((C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, this.A02)).A08("android.permission.READ_PHONE_STATE")) {
                objectNode.put("data_state", C37511vn.A00(r5.A00.getDataState()));
            }
            objectNode.put("network_type", C37511vn.A01(r5.A00.getNetworkType()));
            if (Build.VERSION.SDK_INT >= 24 && ((C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, this.A02)).A08("android.permission.READ_PHONE_STATE")) {
                try {
                    i = r5.A00.getDataNetworkType();
                } catch (SecurityException unused) {
                    i = 0;
                }
                objectNode.put("data_network_type", C37511vn.A01(i));
            }
        }
    }

    public int A08() {
        if (((C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, this.A02)).A08("android.permission.READ_PHONE_STATE") && ((C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, this.A02)).A08(TurboLoader.Locator.$const$string(11))) {
            C29911hB r3 = (C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02);
            if (r3 == null) {
                return 0;
            }
            if (Build.VERSION.SDK_INT < 17) {
                CellLocation A022 = r3.A02("CellDiagnosticsSerializer");
                if (A022 != null) {
                    if (A022 instanceof CdmaCellLocation) {
                        return ((CdmaCellLocation) A022).getBaseStationId();
                    }
                    if (A022 instanceof GsmCellLocation) {
                        return ((GsmCellLocation) A022).getCid();
                    }
                }
            } else {
                List<CellInfo> A032 = r3.A03("CellDiagnosticsSerializer");
                if (A032 != null) {
                    for (CellInfo cellInfo : A032) {
                        if (cellInfo.isRegistered()) {
                            if (cellInfo instanceof CellInfoGsm) {
                                CellIdentityGsm cellIdentity = ((CellInfoGsm) cellInfo).getCellIdentity();
                                if (cellIdentity.getCid() != Integer.MAX_VALUE) {
                                    return cellIdentity.getCid();
                                }
                            } else if (cellInfo instanceof CellInfoCdma) {
                                CellIdentityCdma cellIdentity2 = ((CellInfoCdma) cellInfo).getCellIdentity();
                                if (cellIdentity2.getBasestationId() != Integer.MAX_VALUE) {
                                    return cellIdentity2.getBasestationId();
                                }
                            } else if (cellInfo instanceof CellInfoLte) {
                                CellIdentityLte cellIdentity3 = ((CellInfoLte) cellInfo).getCellIdentity();
                                if (cellIdentity3.getCi() != Integer.MAX_VALUE) {
                                    return cellIdentity3.getCi();
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }

    public CellSignalStrength A09(int i) {
        C29911hB r0;
        Integer num;
        CellSignalStrength cellSignalStrength;
        if (Build.VERSION.SDK_INT >= 18 && (r0 = (C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)) != null) {
            switch (i) {
                case 1:
                case 2:
                case 3:
                case AnonymousClass1Y3.A02:
                    num = AnonymousClass07B.A01;
                    break;
                case 4:
                case 5:
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case AnonymousClass1Y3.A03:
                case 14:
                    num = AnonymousClass07B.A00;
                    break;
                case 8:
                case Process.SIGKILL:
                case AnonymousClass1Y3.A01:
                case 15:
                    num = AnonymousClass07B.A0N;
                    break;
                case 13:
                    num = AnonymousClass07B.A0C;
                    break;
                default:
                    num = AnonymousClass07B.A0Y;
                    break;
            }
            List<CellInfo> A042 = A04(r0);
            if (A042 != null) {
                CellSignalStrength cellSignalStrength2 = null;
                for (CellInfo cellInfo : A042) {
                    if ((cellInfo instanceof CellInfoCdma) && num == AnonymousClass07B.A00) {
                        cellSignalStrength = ((CellInfoCdma) cellInfo).getCellSignalStrength();
                    } else if ((cellInfo instanceof CellInfoGsm) && num == AnonymousClass07B.A01) {
                        cellSignalStrength = ((CellInfoGsm) cellInfo).getCellSignalStrength();
                    } else if ((cellInfo instanceof CellInfoLte) && num == AnonymousClass07B.A0C) {
                        cellSignalStrength = ((CellInfoLte) cellInfo).getCellSignalStrength();
                    } else if (!(cellInfo instanceof CellInfoWcdma) || num != AnonymousClass07B.A0N) {
                        cellSignalStrength = null;
                    } else {
                        cellSignalStrength = ((CellInfoWcdma) cellInfo).getCellSignalStrength();
                    }
                    if (cellSignalStrength2 == null || (cellSignalStrength != null && cellSignalStrength2.getDbm() < cellSignalStrength.getDbm())) {
                        cellSignalStrength2 = cellSignalStrength;
                    }
                }
                return cellSignalStrength2;
            }
        }
        return null;
    }

    public Map A0A(long j) {
        AnonymousClass0Ud r0;
        int i;
        CellLocation cellLocation;
        SubscriptionManager from;
        List<SubscriptionInfo> activeSubscriptionInfoList;
        ObjectNode objectNode;
        int i2 = AnonymousClass1Y3.ADo;
        AnonymousClass0UN r2 = this.A02;
        if (((C29911hB) AnonymousClass1XX.A02(0, i2, r2)) == null || (r0 = (AnonymousClass0Ud) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B5j, r2)) == null || r0.A0G()) {
            return null;
        }
        TreeMap treeMap = new TreeMap();
        String A012 = C37511vn.A01(((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getNetworkType());
        treeMap.put("network_type", A012);
        treeMap.put("network_generation", A03(A012));
        try {
            i = ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getPhoneType();
        } catch (Resources.NotFoundException | SecurityException unused) {
            i = -1;
        }
        treeMap.put("phone_type", C37511vn.A02(i));
        treeMap.put("sim_country_iso", ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getSimCountryIso());
        treeMap.put("sim_operator_mcc_mnc", ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getSimOperator());
        treeMap.put(AnonymousClass80H.$const$string(25), ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getSimOperatorName());
        treeMap.put("has_icc_card", Boolean.valueOf(((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.hasIccCard()));
        treeMap.put("timestamp", Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, this.A02)).now()));
        if (!((C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, this.A02)).A08("android.permission.READ_PHONE_STATE") || !((C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, this.A02)).A08(TurboLoader.Locator.$const$string(11))) {
            cellLocation = null;
        } else {
            cellLocation = ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A02("CellDiagnosticsSerializer");
        }
        if (cellLocation != null) {
            if (cellLocation instanceof CdmaCellLocation) {
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                int baseStationId = cdmaCellLocation.getBaseStationId();
                int baseStationLatitude = cdmaCellLocation.getBaseStationLatitude();
                int baseStationLongitude = cdmaCellLocation.getBaseStationLongitude();
                int networkId = cdmaCellLocation.getNetworkId();
                int systemId = cdmaCellLocation.getSystemId();
                if (baseStationId != -1) {
                    treeMap.put("cdma_base_station_id", Integer.valueOf(baseStationId));
                }
                if (baseStationLatitude != Integer.MAX_VALUE) {
                    treeMap.put("cdma_base_station_latitude", Integer.valueOf(baseStationLatitude));
                }
                if (baseStationLongitude != Integer.MAX_VALUE) {
                    treeMap.put("cdma_base_station_longitude", Integer.valueOf(baseStationLongitude));
                }
                if (networkId != -1) {
                    treeMap.put("cdma_network_id", Integer.valueOf(networkId));
                }
                if (systemId != -1) {
                    treeMap.put("cdma_system_id", Integer.valueOf(systemId));
                }
            } else if (cellLocation instanceof GsmCellLocation) {
                treeMap.put("network_country_iso", ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getNetworkCountryIso());
                treeMap.put("network_operator_mcc_mnc", ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getNetworkOperator());
                treeMap.put(AnonymousClass80H.$const$string(22), ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getNetworkOperatorName());
                treeMap.put("is_network_roaming", Boolean.valueOf(((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.isNetworkRoaming()));
            }
        }
        if (Build.VERSION.SDK_INT >= 18) {
            List<CellInfo> A042 = A04((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02));
            if (Build.VERSION.SDK_INT >= 18 && A042 != null) {
                for (CellInfo cellInfo : A042) {
                    if (cellInfo instanceof CellInfoCdma) {
                        CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
                        CellSignalStrengthCdma cellSignalStrength = cellInfoCdma.getCellSignalStrength();
                        treeMap.put("signal_asu_level", Integer.valueOf(cellSignalStrength.getAsuLevel()));
                        treeMap.put("signal_dbm", Integer.valueOf(cellSignalStrength.getDbm()));
                        treeMap.put("signal_level", Integer.valueOf(cellSignalStrength.getLevel()));
                        treeMap.put("signal_cdma_dbm", Integer.valueOf(cellSignalStrength.getCdmaDbm()));
                        treeMap.put("signal_cdma_ecio", Integer.valueOf(cellSignalStrength.getCdmaEcio()));
                        treeMap.put("signal_cdma_level", Integer.valueOf(cellSignalStrength.getCdmaLevel()));
                        treeMap.put("signal_evdo_dbm", Integer.valueOf(cellSignalStrength.getEvdoDbm()));
                        treeMap.put("signal_evdo_ecio", Integer.valueOf(cellSignalStrength.getEvdoEcio()));
                        treeMap.put("signal_evdo_level", Integer.valueOf(cellSignalStrength.getEvdoLevel()));
                        treeMap.put("signal_evdo_snr", Integer.valueOf(cellSignalStrength.getEvdoSnr()));
                        CellIdentityCdma cellIdentity = cellInfoCdma.getCellIdentity();
                        int basestationId = cellIdentity.getBasestationId();
                        int latitude = cellIdentity.getLatitude();
                        int longitude = cellIdentity.getLongitude();
                        int networkId2 = cellIdentity.getNetworkId();
                        int systemId2 = cellIdentity.getSystemId();
                        if (A06(basestationId)) {
                            treeMap.put("cdma_base_station_id", Integer.valueOf(basestationId));
                        }
                        if (A06(latitude)) {
                            treeMap.put("cdma_base_station_latitude", Integer.valueOf(latitude));
                        }
                        if (A06(longitude)) {
                            treeMap.put("cdma_base_station_longitude", Integer.valueOf(longitude));
                        }
                        if (A06(networkId2)) {
                            treeMap.put("cdma_network_id", Integer.valueOf(networkId2));
                        }
                        if (A06(systemId2)) {
                            treeMap.put("cdma_system_id", Integer.valueOf(systemId2));
                        }
                    } else if (cellInfo instanceof CellInfoGsm) {
                        CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
                        CellSignalStrengthGsm cellSignalStrength2 = cellInfoGsm.getCellSignalStrength();
                        treeMap.put("signal_asu_level", Integer.valueOf(cellSignalStrength2.getAsuLevel()));
                        treeMap.put("signal_dbm", Integer.valueOf(cellSignalStrength2.getDbm()));
                        treeMap.put("signal_level", Integer.valueOf(cellSignalStrength2.getLevel()));
                        CellIdentityGsm cellIdentity2 = cellInfoGsm.getCellIdentity();
                        int cid = cellIdentity2.getCid();
                        if (A06(cid)) {
                            treeMap.put("gsm_cid", Integer.valueOf(cid));
                        }
                        int mcc = cellIdentity2.getMcc();
                        if (A06(mcc)) {
                            treeMap.put("gsm_mcc", Integer.valueOf(mcc));
                        }
                        int mnc = cellIdentity2.getMnc();
                        if (A06(mnc)) {
                            treeMap.put("gsm_mnc", Integer.valueOf(mnc));
                        }
                        int lac = cellIdentity2.getLac();
                        if (A06(lac)) {
                            treeMap.put("gsm_lac", Integer.valueOf(lac));
                        }
                        if (Build.VERSION.SDK_INT >= 24) {
                            int arfcn = cellInfoGsm.getCellIdentity().getArfcn();
                            if (A06(arfcn)) {
                                treeMap.put("gsm_arfcn", Integer.valueOf(arfcn));
                            }
                        }
                    } else if (cellInfo instanceof CellInfoLte) {
                        CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
                        CellSignalStrengthLte cellSignalStrength3 = cellInfoLte.getCellSignalStrength();
                        treeMap.put("signal_asu_level", Integer.valueOf(cellSignalStrength3.getAsuLevel()));
                        treeMap.put("signal_dbm", Integer.valueOf(cellSignalStrength3.getDbm()));
                        treeMap.put("signal_level", Integer.valueOf(cellSignalStrength3.getLevel()));
                        treeMap.put("signal_lte_timing_advance", Integer.valueOf(cellSignalStrength3.getTimingAdvance()));
                        CellIdentityLte cellIdentity3 = cellInfoLte.getCellIdentity();
                        int ci = cellIdentity3.getCi();
                        if (A06(ci)) {
                            treeMap.put("lte_ci", Integer.valueOf(ci));
                        }
                        int mcc2 = cellIdentity3.getMcc();
                        if (A06(mcc2)) {
                            treeMap.put("lte_mcc", Integer.valueOf(mcc2));
                        }
                        int mnc2 = cellIdentity3.getMnc();
                        if (A06(mnc2)) {
                            treeMap.put("lte_mnc", Integer.valueOf(mnc2));
                        }
                        int pci = cellIdentity3.getPci();
                        if (A06(pci)) {
                            treeMap.put("lte_pci", Integer.valueOf(pci));
                        }
                        int tac = cellIdentity3.getTac();
                        if (A06(tac)) {
                            treeMap.put("lte_tac", Integer.valueOf(tac));
                        }
                        if (Build.VERSION.SDK_INT >= 24) {
                            int earfcn = cellInfoLte.getCellIdentity().getEarfcn();
                            if (A06(earfcn)) {
                                treeMap.put("lte_earfcn", Integer.valueOf(earfcn));
                            }
                        }
                        if (Build.VERSION.SDK_INT >= 28) {
                            int bandwidth = cellInfoLte.getCellIdentity().getBandwidth();
                            if (A06(bandwidth)) {
                                treeMap.put("lte_bandwidth", Integer.valueOf(bandwidth));
                            }
                        }
                        if (Build.VERSION.SDK_INT >= 24) {
                            treeMap.put("lte_rsrq", Integer.valueOf(cellSignalStrength3.getRsrq()));
                            treeMap.put("lte_rssnr", Integer.valueOf(cellSignalStrength3.getRssnr()));
                        }
                    } else if (cellInfo instanceof CellInfoWcdma) {
                        CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
                        CellSignalStrengthWcdma cellSignalStrength4 = cellInfoWcdma.getCellSignalStrength();
                        treeMap.put("signal_asu_level", Integer.valueOf(cellSignalStrength4.getAsuLevel()));
                        treeMap.put("signal_dbm", Integer.valueOf(cellSignalStrength4.getDbm()));
                        treeMap.put("signal_level", Integer.valueOf(cellSignalStrength4.getLevel()));
                        CellIdentityWcdma cellIdentity4 = cellInfoWcdma.getCellIdentity();
                        int cid2 = cellIdentity4.getCid();
                        if (A06(cid2)) {
                            treeMap.put("wcdma_cid", Integer.valueOf(cid2));
                        }
                        int mcc3 = cellIdentity4.getMcc();
                        if (A06(mcc3)) {
                            treeMap.put("wcdma_mcc", Integer.valueOf(mcc3));
                        }
                        int mnc3 = cellIdentity4.getMnc();
                        if (A06(mnc3)) {
                            treeMap.put("wcdma_mnc", Integer.valueOf(mnc3));
                        }
                        int psc = cellIdentity4.getPsc();
                        if (A06(psc)) {
                            treeMap.put("wcdma_psc", Integer.valueOf(psc));
                        }
                        int lac2 = cellIdentity4.getLac();
                        if (A06(lac2)) {
                            treeMap.put("wcdma_lac", Integer.valueOf(lac2));
                        }
                        if (Build.VERSION.SDK_INT >= 24) {
                            int uarfcn = cellInfoWcdma.getCellIdentity().getUarfcn();
                            if (A06(uarfcn)) {
                                treeMap.put("wcdma_uarfcn", Integer.valueOf(uarfcn));
                            }
                        }
                    }
                }
            }
        }
        ObjectNode objectNode2 = new ObjectNode(JsonNodeFactory.instance);
        if ((1 & j) != 0) {
            int i3 = AnonymousClass1Y3.BSv;
            int A052 = ((AnonymousClass3B9) AnonymousClass1XX.A02(3, i3, this.A02)).A05();
            int A043 = ((AnonymousClass3B9) AnonymousClass1XX.A02(3, i3, this.A02)).A04();
            int i4 = 0;
            List A044 = A04((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02));
            if (A044 != null) {
                i4 = A044.size();
            }
            objectNode2.put("phone_count", A052);
            objectNode2.put("active_subscription_count", A043);
            objectNode2.put("registered_cell_count", i4);
            objectNode2.put("sdk_version", Build.VERSION.SDK_INT);
        }
        if ((j & 2) != 0) {
            long nanoTime = System.nanoTime();
            if (Build.VERSION.SDK_INT >= 24 && ((C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, this.A02)).A08("android.permission.READ_PHONE_STATE")) {
                ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
                int i5 = AnonymousClass1Y3.BCt;
                AnonymousClass0UN r9 = this.A02;
                if (!(((Context) AnonymousClass1XX.A02(7, i5, r9)) == null || !((C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, r9)).A08("android.permission.READ_PHONE_STATE") || (from = SubscriptionManager.from((Context) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BCt, this.A02))) == null || (activeSubscriptionInfoList = from.getActiveSubscriptionInfoList()) == null)) {
                    for (SubscriptionInfo next : activeSubscriptionInfoList) {
                        int i6 = AnonymousClass1Y3.ADo;
                        if (((C29911hB) AnonymousClass1XX.A02(0, i6, this.A02)) == null) {
                            objectNode = null;
                        } else {
                            objectNode = new ObjectNode(JsonNodeFactory.instance);
                            int subscriptionId = next.getSubscriptionId();
                            C29911hB r14 = (C29911hB) AnonymousClass1XX.A02(0, i6, this.A02);
                            C29911hB r4 = new C29911hB(r14.A00.createForSubscriptionId(subscriptionId), r14.A02, r14.A01, r14.A03);
                            objectNode.put("registered_cells", A01(A04(r4), nanoTime));
                            objectNode.put("subscription_id", subscriptionId);
                            A05(r4, objectNode);
                        }
                        if (objectNode != null) {
                            arrayNode.add(objectNode);
                        }
                    }
                }
                objectNode2.put("active_subscriptions", arrayNode);
                objectNode2.put("default_data_subscription_id", SubscriptionManager.getDefaultDataSubscriptionId());
                objectNode2.put("default_subscription_id", SubscriptionManager.getDefaultSubscriptionId());
            } else if (Build.VERSION.SDK_INT >= 18) {
                JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
                ArrayNode arrayNode2 = new ArrayNode(jsonNodeFactory);
                ObjectNode objectNode3 = new ObjectNode(jsonNodeFactory);
                int i7 = AnonymousClass1Y3.ADo;
                objectNode3.put("registered_cells", A01(A04((C29911hB) AnonymousClass1XX.A02(0, i7, this.A02)), nanoTime));
                objectNode3.put("subscription_id", Integer.MAX_VALUE);
                A05((C29911hB) AnonymousClass1XX.A02(0, i7, this.A02), objectNode3);
                arrayNode2.add(objectNode3);
                objectNode2.put("active_subscriptions", arrayNode2);
            }
        }
        treeMap.put("extra", objectNode2.toString());
        return treeMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0056, code lost:
        if (r0 == false) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0058, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d4, code lost:
        if (r0 != false) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0B(java.util.Map r8) {
        /*
            r7 = this;
            int r2 = X.AnonymousClass1Y3.AWJ
            X.0UN r1 = r7.A02
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0pP r1 = (X.C12460pP) r1
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            boolean r0 = r1.A08(r0)
            if (r0 == 0) goto L_0x013e
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 24
            if (r1 < r0) goto L_0x00c0
            boolean r0 = r7.A08
            r1 = 1
            if (r0 == 0) goto L_0x009d
            r0 = 1
        L_0x001f:
            r6 = 0
            if (r0 == 0) goto L_0x0026
            boolean r0 = r7.A09
            if (r0 == 0) goto L_0x004e
        L_0x0026:
            int r2 = android.telephony.SubscriptionManager.getDefaultDataSubscriptionId()
            r0 = -1
            if (r2 == r0) goto L_0x009b
            int r1 = X.AnonymousClass1Y3.ADo
            X.0UN r0 = r7.A02
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.1hB r5 = (X.C29911hB) r5
            if (r5 == 0) goto L_0x009b
            android.telephony.TelephonyManager r0 = r5.A00
            android.telephony.TelephonyManager r4 = r0.createForSubscriptionId(r2)
            X.1hB r3 = new X.1hB
            X.1hH r2 = r5.A02
            X.1hK r1 = r5.A01
            X.1hF r0 = r5.A03
            r3.<init>(r4, r2, r1, r0)
            r7.A01 = r3
            r7.A09 = r6
        L_0x004e:
            X.1hB r0 = r7.A01
            if (r0 != 0) goto L_0x0080
            r0 = 1
            r7.A09 = r0
            r0 = 0
        L_0x0056:
            if (r0 == 0) goto L_0x00c0
        L_0x0058:
            r0 = 1
        L_0x0059:
            if (r0 == 0) goto L_0x007f
            com.fasterxml.jackson.databind.node.ObjectNode r0 = r7.A05
            if (r0 == 0) goto L_0x007f
            java.lang.String r1 = r7.A03
            java.lang.String r0 = "network_type_info"
            r8.put(r0, r1)
            java.lang.String r1 = r7.A04
            java.lang.String r0 = "network_generation"
            r8.put(r0, r1)
            com.fasterxml.jackson.databind.node.ObjectNode r1 = r7.A05
            java.lang.String r0 = "network_params"
            r8.put(r0, r1)
            boolean r0 = r7.A06
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "is_network_roaming"
            r8.put(r0, r1)
        L_0x007f:
            return
        L_0x0080:
            android.telephony.TelephonyManager r0 = r0.A00     // Catch:{ SecurityException -> 0x0087 }
            int r0 = r0.getDataNetworkType()     // Catch:{ SecurityException -> 0x0087 }
            goto L_0x0088
        L_0x0087:
            r0 = 0
        L_0x0088:
            java.lang.String r0 = X.C37511vn.A01(r0)
            r7.A03 = r0
            java.lang.String r0 = A03(r0)
            r7.A04 = r0
            X.1hB r0 = r7.A01
            boolean r0 = r7.A07(r0)
            goto L_0x0056
        L_0x009b:
            r0 = 0
            goto L_0x0056
        L_0x009d:
            java.util.concurrent.atomic.AtomicBoolean r0 = r7.A07
            boolean r0 = r0.getAndSet(r1)
            if (r0 != 0) goto L_0x00bc
            r2 = 8
            int r1 = X.AnonymousClass1Y3.Abm
            X.0UN r0 = r7.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            android.os.Handler r2 = (android.os.Handler) r2
            X.1k1 r1 = new X.1k1
            r1.<init>(r7)
            r0 = 1910893043(0x71e5e9f3, float:2.2769566E30)
            X.AnonymousClass00S.A04(r2, r1, r0)
        L_0x00bc:
            boolean r0 = r7.A08
            goto L_0x001f
        L_0x00c0:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 18
            if (r1 < r0) goto L_0x013e
            int r0 = X.AnonymousClass1Y3.ADo
            X.0UN r2 = r7.A02
            r4 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r0, r2)
            X.1hB r0 = (X.C29911hB) r0
            if (r0 != 0) goto L_0x00d7
            r0 = 0
        L_0x00d4:
            if (r0 == 0) goto L_0x013e
            goto L_0x0058
        L_0x00d7:
            com.fasterxml.jackson.databind.node.ObjectNode r5 = new com.fasterxml.jackson.databind.node.ObjectNode
            com.fasterxml.jackson.databind.node.JsonNodeFactory r0 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance
            r5.<init>(r0)
            int r1 = X.AnonymousClass1Y3.AeN
            r0 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r1, r2)
            X.0h3 r0 = (X.C09340h3) r0
            if (r0 == 0) goto L_0x0100
            java.lang.String r1 = r0.A0J()
            java.lang.String r0 = "none"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0100
            java.lang.String r0 = "connection_subtype"
            r5.put(r0, r1)
            java.lang.String r0 = A03(r1)
            r7.A04 = r0
        L_0x0100:
            int r1 = X.AnonymousClass1Y3.ADo
            X.0UN r0 = r7.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1hB r0 = (X.C29911hB) r0
            android.telephony.TelephonyManager r0 = r0.A00
            int r0 = r0.getNetworkType()
            java.lang.String r2 = X.C37511vn.A01(r0)
            java.lang.String r0 = "network_type"
            r5.put(r0, r2)
            java.lang.String r1 = r7.A04
            java.lang.String r0 = "UNKNOWN"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0129
            java.lang.String r0 = A03(r2)
            r7.A04 = r0
        L_0x0129:
            java.lang.String r0 = r5.toString()
            r7.A03 = r0
            int r1 = X.AnonymousClass1Y3.ADo
            X.0UN r0 = r7.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1hB r0 = (X.C29911hB) r0
            boolean r0 = r7.A07(r0)
            goto L_0x00d4
        L_0x013e:
            r0 = 0
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31631k0.A0B(java.util.Map):void");
    }

    public void A0C(Map map) {
        C12460pP r1;
        String str;
        int i = AnonymousClass1Y3.ADo;
        AnonymousClass0UN r2 = this.A02;
        if (((C29911hB) AnonymousClass1XX.A02(0, i, r2)) != null && (r1 = (C12460pP) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AWJ, r2)) != null && r1.A08("android.permission.READ_PHONE_STATE")) {
            if (Build.VERSION.SDK_INT >= 26) {
                try {
                    str = ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getImei();
                } catch (SecurityException unused) {
                    str = null;
                }
            } else {
                str = ((C29911hB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADo, this.A02)).A00.getDeviceId();
            }
            if (str != null && str.length() >= 8) {
                map.put("device_tac", str.substring(0, 8));
            }
        }
    }

    private C31631k0(AnonymousClass1XY r4) {
        this.A02 = new AnonymousClass0UN(9, r4);
    }
}
