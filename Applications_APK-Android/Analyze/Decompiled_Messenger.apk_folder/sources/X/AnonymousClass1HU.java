package X;

import com.facebook.user.model.User;
import java.util.Comparator;

/* renamed from: X.1HU  reason: invalid class name */
public final class AnonymousClass1HU implements Comparator {
    public int compare(Object obj, Object obj2) {
        float f = ((User) obj).A04;
        float f2 = ((User) obj2).A04;
        if (f > f2) {
            return -1;
        }
        if (f < f2) {
            return 1;
        }
        return 0;
    }
}
