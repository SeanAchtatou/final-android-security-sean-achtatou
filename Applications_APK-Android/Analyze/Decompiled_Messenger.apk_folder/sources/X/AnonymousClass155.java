package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.zero.common.ZeroTrafficEnforcementConfig;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.155  reason: invalid class name */
public final class AnonymousClass155 {
    public static final AnonymousClass1Y8 A05 = AnonymousClass158.A00;
    public static final AnonymousClass1Y8 A06 = C05730aE.A0H;
    public static final Class A07 = AnonymousClass155.class;
    private static volatile AnonymousClass155 A08;
    public final FbSharedPreferences A00;
    public final Map A01 = new HashMap();
    public final C04310Tq A02;
    private final AnonymousClass06B A03;
    private final Map A04 = new HashMap();

    public static AnonymousClass1Y8 A00(AnonymousClass157 r1) {
        if (r1 == AnonymousClass157.DIALTONE) {
            return A05;
        }
        return A06;
    }

    public static final AnonymousClass155 A01(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass155.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new AnonymousClass155(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public static AnonymousClass157 A02(AnonymousClass155 r0) {
        return A03((String) r0.A02.get());
    }

    public static AnonymousClass157 A03(String str) {
        if ("normal".equals(str)) {
            return AnonymousClass157.NORMAL;
        }
        if ("dialtone".equals(str)) {
            return AnonymousClass157.DIALTONE;
        }
        throw new RuntimeException("calling getBaseToken() with unsupported FbZeroToken.Type");
    }

    public static ImmutableList A04(AnonymousClass155 r3, String str) {
        if (r3.A04.containsKey(str)) {
            return ImmutableList.copyOf((Collection) r3.A04.get(str));
        }
        try {
            ImmutableList A002 = C47882Yl.A00(str);
            if (r3.A04.size() < 20) {
                r3.A04.put(str, A002);
                return A002;
            }
            C010708t.A06(A07, "Too many cached deserialized rewrite rules.");
            return A002;
        } catch (IOException e) {
            C010708t.A0E(A07, e, "Error deserializing backup rewrite rules: %s", e.getMessage());
            return RegularImmutableList.A02;
        }
    }

    public static String A05(AnonymousClass155 r2, AnonymousClass157 r3) {
        return r2.A00.B4F(A00(r3).A09("rewrite_rules"), BuildConfig.FLAVOR);
    }

    public ZeroTrafficEnforcementConfig A06(AnonymousClass157 r4) {
        return C101764t7.A00(this.A00.B4F(A00(r4).A09("zero_traffic_enforcement_config"), BuildConfig.FLAVOR));
    }

    public String A08() {
        return this.A00.B4F(A00(A02(this)).A09("campaign"), BuildConfig.FLAVOR);
    }

    public String A09(AnonymousClass157 r4) {
        return this.A00.B4F(A00(r4).A09("campaign"), BuildConfig.FLAVOR);
    }

    public String A0A(AnonymousClass157 r4) {
        return this.A00.B4F(A00(r4).A09("carrier_id"), BuildConfig.FLAVOR);
    }

    public String A0B(AnonymousClass157 r4) {
        return this.A00.B4F(A00(r4).A09("eligibility_hash"), null);
    }

    public String A0C(AnonymousClass157 r4) {
        return this.A00.B4F(A00(r4).A09("fast_hash"), BuildConfig.FLAVOR);
    }

    public String A0D(AnonymousClass157 r4) {
        return this.A00.B4F(A00(r4).A09("token_hash"), BuildConfig.FLAVOR);
    }

    public String A0E(AnonymousClass157 r4) {
        return this.A00.B4F(A00(r4).A09("enabled_ui_features"), BuildConfig.FLAVOR);
    }

    public String A0F(AnonymousClass157 r4, String str) {
        return this.A00.B4F(A00(r4).A09(AnonymousClass80H.$const$string(17)), str);
    }

    public String A0G(AnonymousClass157 r4, String str) {
        return this.A00.B4F(A00(r4).A09("reg_status"), str);
    }

    public String A0H(AnonymousClass157 r4, String str) {
        return this.A00.B4F(A00(r4).A09("current_zero_rating_status"), str);
    }

    public String A0I(AnonymousClass157 r4, String str) {
        return this.A00.B4F(A00(r4).A09("unregistered_reason"), str);
    }

    public void A0J() {
        C30281hn edit = this.A00.edit();
        for (AnonymousClass157 A002 : AnonymousClass157.values()) {
            edit.C24(A00(A002));
        }
        edit.commit();
    }

    public void A0K(AnonymousClass157 r6) {
        long now = this.A03.now();
        C30281hn edit = this.A00.edit();
        edit.BzA(A00(r6).A09("last_time_checked"), now);
        edit.commit();
    }

    private AnonymousClass155(AnonymousClass1XY r2) {
        this.A00 = FbSharedPreferencesModule.A00(r2);
        this.A03 = AnonymousClass067.A02();
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.Ay6, r2);
    }

    public String A07() {
        return this.A00.B4F(A00(A02(this)).A09("backup_rewrite_rules"), BuildConfig.FLAVOR);
    }
}
