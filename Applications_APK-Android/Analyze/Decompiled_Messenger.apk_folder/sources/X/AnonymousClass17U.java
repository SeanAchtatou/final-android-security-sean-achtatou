package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.graphql.query.GQSQStringShape2S0000000_I2;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.ExecutorService;

/* renamed from: X.17U  reason: invalid class name */
public class AnonymousClass17U implements C20021Ap {
    public void Bd1(Object obj, Object obj2) {
    }

    public void BdJ(Object obj, Object obj2) {
    }

    public void BdU(Object obj, ListenableFuture listenableFuture) {
    }

    public void Bgh(Object obj, Object obj2) {
        if (this instanceof AnonymousClass17R) {
            AnonymousClass17R r6 = (AnonymousClass17R) this;
            C34801qC r15 = (C34801qC) obj2;
            C20921Ei r4 = (C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, r6.A00.A08);
            int size = r15.A00.size();
            if (C20921Ei.A03(r4)) {
                synchronized (r4.A06) {
                    try {
                        r4.A02 = true;
                    } catch (Throwable th) {
                        while (true) {
                            th = th;
                        }
                    }
                }
                for (int i : C20921Ei.A08) {
                    if (i == 5505198 || (i == 5505218 && size > 0)) {
                        C21061Ew withMarker = ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r4.A00)).withMarker(i);
                        withMarker.A03("stories_loaded");
                        withMarker.A05("card_count", size);
                        withMarker.BK9();
                    }
                }
                if (C20921Ei.A02(r4) && size == 0 && C20921Ei.A02(r4)) {
                    if ("no_data" != 0) {
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r4.A00)).markerAnnotate(5505218, "cancel_reason", "no_data");
                    }
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r4.A00)).markerEnd(5505218, 4);
                    synchronized (r4.A06) {
                        try {
                            r4.A02 = true;
                        } catch (Throwable th2) {
                            while (true) {
                                th = th2;
                                break;
                            }
                        }
                    }
                }
            }
            C34801qC r1 = r6.A00.A09;
            if (r1 == null || !r1.equals(r15.A00)) {
                C75353jh r42 = (C75353jh) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ALa, r6.A00.A08);
                int size2 = r15.A00.size();
                long now = r42.A02.now();
                long j = r42.A00;
                if (now - j >= 1800000 || j == -1) {
                    USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r42.A01.A01("montage_amount_stories_in_tray"), AnonymousClass1Y3.A3g);
                    if (uSLEBaseShape0S0000000.A0G()) {
                        uSLEBaseShape0S0000000.A0B("amount", Integer.valueOf(size2));
                        boolean z = false;
                        if (r42.A00 == -1) {
                            z = true;
                        }
                        uSLEBaseShape0S0000000.A08("is_first_load", Boolean.valueOf(z));
                        uSLEBaseShape0S0000000.A06();
                    }
                    r42.A00 = r42.A02.now();
                }
                AnonymousClass16R r0 = r6.A00;
                r0.A09 = r15;
                if (((C17350yl) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BRG, r0.A08)).A0A()) {
                    int i2 = AnonymousClass1Y3.Aqj;
                    AnonymousClass16R r2 = r6.A00;
                    ((AnonymousClass12I) AnonymousClass1XX.A02(19, i2, r2.A08)).A06(r2.A09.A00);
                }
                if (((C17350yl) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BRG, r6.A00.A08)).A0B()) {
                    C75293jb r7 = (C75293jb) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AId, r6.A00.A08);
                    long At2 = r7.A03.At2(AnonymousClass129.A0H, 0);
                    long now2 = r7.A01.now();
                    if (At2 == 0 || ((double) (now2 - At2)) >= 2.0d * 3600000.0d) {
                        C26931cN A00 = C26931cN.A00(new GQSQStringShape2S0000000_I2(4));
                        A00.A09(0);
                        C05350Yp.A08(AnonymousClass169.A01(((C11170mD) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ASp, r7.A00)).A04(A00), new C87164Dp(r7), (ExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BKH, r7.A00)), new C87144Dn(r7, now2), (ExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BKH, r7.A00));
                    }
                }
                if (r6.A00.A0R.A00.Aem(282325382268221L)) {
                    C86644Bk r43 = (C86644Bk) AnonymousClass1XX.A02(14, AnonymousClass1Y3.BCA, r6.A00.A08);
                    AnonymousClass07A.A04((AnonymousClass0VL) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ASK, r43.A00), new C86634Bj(r43, r15.A00), 87375944);
                }
                Integer num = AnonymousClass07B.A0n;
                if (!AnonymousClass16R.A08(r6.A00, num, "MontageInbox")) {
                    AnonymousClass16R.A04(r6.A00, num, "MontageInbox");
                    return;
                }
                return;
            }
            return;
        }
        return;
        throw th;
    }
}
