package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1fi  reason: invalid class name and case insensitive filesystem */
public final class C29001fi {
    private static C05540Zi A00;

    public static final C29001fi A01(AnonymousClass1XY r4) {
        C29001fi r0;
        synchronized (C29001fi.class) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r4)) {
                    A00.A00 = new C29001fi((AnonymousClass1XY) A00.A01());
                }
                C05540Zi r1 = A00;
                r0 = (C29001fi) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    private C29001fi(AnonymousClass1XY r3) {
        new AnonymousClass0UN(1, r3);
    }

    public static final C29001fi A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
