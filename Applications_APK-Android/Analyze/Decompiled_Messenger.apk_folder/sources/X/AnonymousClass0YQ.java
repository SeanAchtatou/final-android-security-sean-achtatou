package X;

import com.facebook.gk.sessionless.GkSessionlessModule;

/* renamed from: X.0YQ  reason: invalid class name */
public final class AnonymousClass0YQ implements AnonymousClass0WR {
    private final AnonymousClass0WS A00;
    private final AnonymousClass0US[] A01;

    public static final AnonymousClass0YQ A00(AnonymousClass1XY r17) {
        AnonymousClass1XY r0 = r17;
        C001500z A05 = AnonymousClass0UU.A05(r0);
        AnonymousClass1YI A002 = AnonymousClass0WA.A00(r0);
        GkSessionlessModule.A00(r0);
        return new AnonymousClass0YQ(A05, A002, AnonymousClass0VB.A00(AnonymousClass1Y3.AL8, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.A7u, r0), AnonymousClass0UQ.A00(AnonymousClass1Y3.Av4, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.AM4, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.B5K, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.BPf, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.A9K, r0), AnonymousClass0UQ.A00(AnonymousClass1Y3.BJP, r0), AnonymousClass0UQ.A00(AnonymousClass1Y3.Aqo, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.ATb, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.AGf, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.BUA, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.Aen, r0));
    }

    public int BKI() {
        return this.A00.A04.length;
    }

    public AnonymousClass1YQ BLp() {
        AnonymousClass0US A002 = this.A00.A00();
        if (A002 != null) {
            return (AnonymousClass1YQ) A002.get();
        }
        return null;
    }

    private AnonymousClass0YQ(C001500z r22, AnonymousClass1YI r23, AnonymousClass0US r24, AnonymousClass0US r25, AnonymousClass0US r26, AnonymousClass0US r27, AnonymousClass0US r28, AnonymousClass0US r29, AnonymousClass0US r30, AnonymousClass0US r31, AnonymousClass0US r32, AnonymousClass0US r33, AnonymousClass0US r34, AnonymousClass0US r35, AnonymousClass0US r36) {
        AnonymousClass0US r6 = r26;
        AnonymousClass0US r5 = r25;
        AnonymousClass0US r4 = r24;
        AnonymousClass0US r13 = r33;
        AnonymousClass0US r12 = r32;
        AnonymousClass0US r11 = r31;
        this.A01 = new AnonymousClass0US[]{r4, r5, r6, r27, r28, r29, r30, r11, r12, r13, r34, r35, r36};
        AnonymousClass0US[] r14 = this.A01;
        this.A00 = new AnonymousClass0WS(r14, new C001500z[]{null, null, null, null, null, null, null, null, null, null, null, null, null}, r22, new Integer[]{null, null, null, null, null, null, null, null, null, null, null, null, null}, new boolean[]{true, true, true, true, true, true, true, true, true, true, true, true, true}, new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false}, r23);
    }
}
