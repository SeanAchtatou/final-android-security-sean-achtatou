package X;

import com.facebook.webpsupport.WebpBitmapFactoryImpl;

/* renamed from: X.1PU  reason: invalid class name */
public final class AnonymousClass1PU {
    public boolean A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final long A08;
    public final C23111Og A09;
    public final C23111Og A0A;
    public final AnonymousClass1PO A0B;
    public final C84013yd A0C;
    public final WebpBitmapFactoryImpl A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;
    public final boolean A0I;
    public final boolean A0J;
    public final boolean A0K;

    public AnonymousClass1PU(AnonymousClass1PJ r3) {
        this.A0K = r3.A0K;
        this.A0C = r3.A08;
        this.A0E = r3.A0B;
        this.A0D = r3.A09;
        this.A0J = r3.A0J;
        this.A0I = r3.A0I;
        this.A06 = r3.A02;
        this.A05 = r3.A01;
        this.A00 = r3.A0A;
        this.A07 = r3.A03;
        this.A0H = r3.A0H;
        AnonymousClass1PO r0 = r3.A07;
        if (r0 == null) {
            this.A0B = new AnonymousClass1PN();
        } else {
            this.A0B = r0;
        }
        this.A09 = r3.A05;
        this.A0G = r3.A0F;
        this.A04 = r3.A00;
        this.A0A = r3.A06;
        this.A0F = r3.A0E;
        this.A08 = r3.A04;
        this.A03 = r3.A0G;
        this.A01 = r3.A0C;
        this.A02 = r3.A0D;
    }
}
