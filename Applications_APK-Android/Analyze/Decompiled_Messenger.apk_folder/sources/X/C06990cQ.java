package X;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.util.ArrayList;
import java.util.Set;
import java.util.SortedSet;

/* renamed from: X.0cQ  reason: invalid class name and case insensitive filesystem */
public final class C06990cQ implements C07420dS, C06150aw {
    public final /* synthetic */ C06880cE A00;

    public C06990cQ(C06880cE r1) {
        this.A00 = r1;
    }

    public void BiF(C06130au r10, Integer num) {
        Message obtain;
        if (!this.A00.A03.isEmpty()) {
            C06880cE r5 = this.A00;
            synchronized (r5) {
                obtain = Message.obtain((Handler) null, 1000000000);
                Bundle data = obtain.getData();
                ArrayList A002 = C04300To.A00();
                ArrayList A003 = C04300To.A00();
                ArrayList A004 = C04300To.A00();
                C24971Xv it = r5.A03.iterator();
                while (it.hasNext()) {
                    C06750c1 r1 = (C06750c1) it.next();
                    A003.add(r1.A01);
                    A004.add(Integer.valueOf(r1.A00));
                    Bundle bundle = new Bundle();
                    r1.A03(bundle);
                    A002.add(bundle);
                }
                data.putParcelableArrayList("__BASE_URIS__", A003);
                data.putIntegerArrayList("__PRIORITIES__", A004);
                data.putParcelableArrayList("__ROLES_DATA__", A002);
            }
            C07930eP r3 = this.A00.A02;
            obtain.arg1 = r3.A07.A01;
            AnonymousClass00S.A04(r3.A01, new C10590kV(r3, r10, obtain), 1670626840);
        }
    }

    public void BiG(C06130au r10) {
        Set<C06750c1> set;
        synchronized (this.A00) {
            set = (Set) this.A00.A06.get(r10);
            if (set != null && !set.isEmpty()) {
                for (C06750c1 r7 : set) {
                    r7.A00();
                    SortedSet sortedSet = (SortedSet) this.A00.A05.get(r7.A01);
                    if (sortedSet == null) {
                        C010708t.A0B(this.A00.A08, "Invalid state: there should be roles for base uri %s when %s disconnected.", r7.A01, r10.A02);
                        int i = AnonymousClass1Y3.Amr;
                        C06880cE r1 = this.A00;
                        String simpleName = r1.A08.getSimpleName();
                        ((AnonymousClass09P) AnonymousClass1XX.A02(0, i, r1.A00)).CGS(simpleName, "Invalid state: there should be roles for base uri " + r7.A01 + " when " + r10.A02 + " disconnected.");
                    } else {
                        sortedSet.remove(r7);
                        if (sortedSet.isEmpty()) {
                            this.A00.A05.remove(r7.A01);
                        }
                    }
                }
            }
            this.A00.A06.remove(r10);
        }
        if (set != null) {
            for (C06750c1 r0 : set) {
                C06880cE.A01(this.A00, Uri.withAppendedPath(r0.A01, TurboLoader.Locator.$const$string(24)), false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004a, code lost:
        if (r0 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004c, code lost:
        X.C06880cE.A01(r5, r4, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Bex(X.C06130au r11, android.os.Message r12) {
        /*
            r10 = this;
            android.os.Bundle r8 = r12.getData()
            X.0cE r0 = r10.A00
            java.lang.ClassLoader r0 = r0.A04
            if (r0 == 0) goto L_0x000d
            r8.setClassLoader(r0)
        L_0x000d:
            int r0 = r12.what
            switch(r0) {
                case 1000000000: goto L_0x0050;
                case 1000000001: goto L_0x0013;
                default: goto L_0x0012;
            }
        L_0x0012:
            return
        L_0x0013:
            X.0cE r5 = r10.A00
            java.lang.String r0 = "__STATE_URI__"
            android.os.Parcelable r4 = r8.getParcelable(r0)
            android.net.Uri r4 = (android.net.Uri) r4
            monitor-enter(r5)
            java.util.Map r0 = r5.A06     // Catch:{ all -> 0x00a0 }
            java.lang.Object r0 = r0.get(r11)     // Catch:{ all -> 0x00a0 }
            java.util.Set r0 = (java.util.Set) r0     // Catch:{ all -> 0x00a0 }
            if (r0 != 0) goto L_0x002a
            monitor-exit(r5)     // Catch:{ all -> 0x00a0 }
            goto L_0x009f
        L_0x002a:
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x00a0 }
        L_0x002e:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x00a0 }
            r2 = 0
            if (r0 == 0) goto L_0x0048
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x00a0 }
            X.0c1 r1 = (X.C06750c1) r1     // Catch:{ all -> 0x00a0 }
            android.net.Uri r0 = r1.A01     // Catch:{ all -> 0x00a0 }
            boolean r0 = X.C06880cE.A03(r4, r0)     // Catch:{ all -> 0x00a0 }
            if (r0 == 0) goto L_0x002e
            r1.A02(r8)     // Catch:{ all -> 0x00a0 }
            r0 = 1
            goto L_0x0049
        L_0x0048:
            r0 = 0
        L_0x0049:
            monitor-exit(r5)     // Catch:{ all -> 0x00a0 }
            if (r0 == 0) goto L_0x0012
            X.C06880cE.A01(r5, r4, r2)
            return
        L_0x0050:
            X.0cE r9 = r10.A00
            monitor-enter(r9)
            java.lang.String r0 = "__BASE_URIS__"
            java.util.ArrayList r7 = r8.getParcelableArrayList(r0)     // Catch:{ all -> 0x009c }
            java.lang.String r0 = "__PRIORITIES__"
            java.util.ArrayList r6 = r8.getIntegerArrayList(r0)     // Catch:{ all -> 0x009c }
            java.lang.String r0 = "__ROLES_DATA__"
            java.util.ArrayList r5 = r8.getParcelableArrayList(r0)     // Catch:{ all -> 0x009c }
            java.util.HashSet r4 = X.C25011Xz.A03()     // Catch:{ all -> 0x009c }
            r3 = 0
        L_0x006a:
            int r0 = r7.size()     // Catch:{ all -> 0x009c }
            if (r3 >= r0) goto L_0x0097
            X.0ab r2 = r9.A01     // Catch:{ all -> 0x009c }
            java.lang.Object r1 = r7.get(r3)     // Catch:{ all -> 0x009c }
            android.net.Uri r1 = (android.net.Uri) r1     // Catch:{ all -> 0x009c }
            java.lang.Object r0 = r6.get(r3)     // Catch:{ all -> 0x009c }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x009c }
            int r0 = r0.intValue()     // Catch:{ all -> 0x009c }
            X.0c1 r1 = r2.A01(r1, r0)     // Catch:{ all -> 0x009c }
            if (r1 == 0) goto L_0x0094
            java.lang.Object r0 = r5.get(r3)     // Catch:{ all -> 0x009c }
            android.os.Bundle r0 = (android.os.Bundle) r0     // Catch:{ all -> 0x009c }
            r1.A02(r0)     // Catch:{ all -> 0x009c }
            r4.add(r1)     // Catch:{ all -> 0x009c }
        L_0x0094:
            int r3 = r3 + 1
            goto L_0x006a
        L_0x0097:
            X.C06880cE.A02(r9, r11, r4)     // Catch:{ all -> 0x009c }
            monitor-exit(r9)
            return
        L_0x009c:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x009f:
            return
        L_0x00a0:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00a0 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06990cQ.Bex(X.0au, android.os.Message):void");
    }
}
