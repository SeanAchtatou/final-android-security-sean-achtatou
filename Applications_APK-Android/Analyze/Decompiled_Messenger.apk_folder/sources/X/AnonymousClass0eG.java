package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eG  reason: invalid class name */
public final class AnonymousClass0eG extends C08190ep {
    private static volatile AnonymousClass0eG A01;
    public AnonymousClass0UN A00;

    public AnonymousClass0Ti AsW() {
        return AnonymousClass0Ti.A00(5505028, 5505026, 5505027);
    }

    public static final AnonymousClass0eG A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0eG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0eG(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass0eG(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
