package X;

/* renamed from: X.0Cs  reason: invalid class name and case insensitive filesystem */
public final class C02070Cs extends AnonymousClass0CV {
    public int A00;
    public int A01;
    public C01950Cg A02;

    public void A00() {
        C01950Cg r1 = this.A02;
        if (r1 != null) {
            r1.cancel(false);
            this.A02 = null;
        }
        super.A00();
    }

    public void A01() {
        C01950Cg r1 = this.A02;
        if (r1 != null) {
            r1.cancel(false);
            this.A02 = null;
        }
        super.A01();
    }

    public void A03(Throwable th) {
        C01950Cg r1 = this.A02;
        if (r1 != null) {
            r1.cancel(false);
            this.A02 = null;
        }
        super.A03(th);
    }

    public String toString() {
        return "MqttRetriableOperation{mResponseType=" + this.A04 + ", mOperationId=" + this.A01 + ", mCreationTime=" + this.A02 + '}';
    }

    public C02070Cs(AnonymousClass0C8 r1, String str, AnonymousClass0CL r3, int i, long j, int i2, int i3) {
        super(r1, str, r3, i, j);
        this.A00 = i2;
        this.A01 = i3;
    }
}
