package X;

import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.common.time.AwakeTimeSinceBootClock;
import java.util.IllegalFormatException;

/* renamed from: X.0Dw  reason: invalid class name and case insensitive filesystem */
public final class C02280Dw {
    public static final AnonymousClass07J A08;
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public Integer A05;
    public String A06;
    public Object[] A07;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public static String A00(long j) {
        long round = (long) Math.round((float) j);
        return AnonymousClass08S.A07(Long.toString(Math.max(0L, (round / 1000) % 60) + 100).substring(1, 3), '.', Long.toString(Math.max(round % 1000, 0L) + 1000).substring(1, 4));
    }

    static {
        Class<C02280Dw> cls = C02280Dw.class;
        AnonymousClass06c r2 = new AnonymousClass06c(null, cls, AwakeTimeSinceBootClock.INSTANCE);
        r2.A03 = new C02290Dx(cls);
        A08 = r2.A00();
    }

    public static String A01(long j) {
        StringBuilder sb = new StringBuilder(10);
        long round = (long) Math.round((float) j);
        if (round < 10) {
            sb.append("____");
        } else if (round < 100) {
            sb.append("___");
        } else if (round < 1000) {
            sb.append("__");
        } else if (round < StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS) {
            sb.append("_");
        }
        sb.append(round);
        return sb.toString();
    }

    public String A02() {
        Object[] objArr = this.A07;
        if (objArr != null) {
            try {
                this.A06 = StringFormatUtil.formatStrLocaleSafe(this.A06, objArr);
                this.A07 = null;
            } catch (IllegalFormatException e) {
                C010708t.A0N("TraceEvent", "Bad format string", e);
                this.A07 = null;
            }
        }
        return this.A06;
    }

    public String toString() {
        return A02();
    }
}
