package X;

import android.content.Context;
import android.widget.Toast;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.15O  reason: invalid class name */
public final class AnonymousClass15O implements AnonymousClass1LY {
    private static volatile AnonymousClass15O A02;
    private final Context A00;
    private final FbSharedPreferences A01;

    public static final AnonymousClass15O A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass15O.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass15O(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public void BgD(String str, String str2, Map map) {
        if (this.A01.Aep(C08720fq.A0B, false)) {
            Context context = this.A00;
            Toast.makeText(context, str + " to " + str2 + " via " + map.get("click_point"), 0).show();
        }
    }

    private AnonymousClass15O(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1YA.A00(r2);
        this.A01 = FbSharedPreferencesModule.A00(r2);
    }
}
