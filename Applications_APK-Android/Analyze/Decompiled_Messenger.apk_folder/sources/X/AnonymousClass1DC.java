package X;

import com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams;
import com.facebook.payments.contactinfo.model.ContactInfoFormInput;

/* renamed from: X.1DC  reason: invalid class name */
public final class AnonymousClass1DC extends C06020ai {
    public final /* synthetic */ ContactInfoCommonFormParams A00;
    public final /* synthetic */ C54212m3 A01;
    public final /* synthetic */ ContactInfoFormInput A02;
    public final /* synthetic */ boolean A03;

    public AnonymousClass1DC(C54212m3 r1, ContactInfoCommonFormParams contactInfoCommonFormParams, ContactInfoFormInput contactInfoFormInput, boolean z) {
        this.A01 = r1;
        this.A00 = contactInfoCommonFormParams;
        this.A02 = contactInfoFormInput;
        this.A03 = z;
    }
}
