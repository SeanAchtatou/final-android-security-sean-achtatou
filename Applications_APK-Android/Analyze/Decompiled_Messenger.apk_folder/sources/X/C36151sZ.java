package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.facebook.proxygen.ConnectionParams;
import com.facebook.proxygen.MQTTClient;
import com.facebook.proxygen.MQTTClientError;
import com.facebook.proxygen.MQTTClientFactory;
import com.facebook.proxygen.ProxygenRadioMeter;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import io.card.payment.BuildConfig;
import java.io.EOFException;
import java.io.IOException;
import java.net.SocketException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.zip.DataFormatException;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1sZ  reason: invalid class name and case insensitive filesystem */
public final class C36151sZ implements AnonymousClass0C7 {
    public AnonymousClass0CB A00;
    public String A01;
    public String A02;
    private C36181se A03;
    private AnonymousClass2FD A04;
    private final C36161sa A05;

    public byte AhL() {
        return 4;
    }

    public String AiA() {
        return BuildConfig.FLAVOR;
    }

    public void C5P(int i, Object obj) {
    }

    public static /* synthetic */ C02020Cn A00(AnonymousClass0CL r4, int i) {
        C01990Ck r3 = new C01990Ck(r4);
        C02000Cl r2 = new C02000Cl(i);
        switch (r4.ordinal()) {
            case 3:
                return new C02010Cm(r3, r2);
            case 8:
                return new AnonymousClass0P3(r3, r2, null);
            default:
                return new C02020Cn(r3, r2, null);
        }
    }

    public static /* synthetic */ Throwable A01(MQTTClientError mQTTClientError) {
        String str = mQTTClientError.mErrMsg;
        switch (mQTTClientError.mErrType.ordinal()) {
            case 0:
            case 5:
            case Process.SIGKILL:
                return new DataFormatException(str);
            case 1:
            case 3:
                return new SocketException(str);
            case 2:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case 8:
            default:
                return new RuntimeException(str);
            case 4:
                return new IOException(str);
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return new EOFException(str);
        }
    }

    public void ASP() {
        C36181se r0 = this.A03;
        AnonymousClass07A.A04(r0.A01, new C30164Eqj(r0), 2014353877);
        C36181se r02 = this.A03;
        AnonymousClass07A.A04(r02.A01, new C30163Eqi(r02), -1614072133);
        this.A00.A03(AnonymousClass089.DISCONNECTED);
        this.A00.A00();
        this.A01 = null;
    }

    public void AU1(String str, int i, boolean z, AnonymousClass0CK r23, int i2, boolean z2) {
        C01570At r0;
        AnonymousClass0CK r5 = r23;
        try {
            byte[] A002 = new C36281so(new C36291sp()).A00(new C36211sh(r5.A02, r5.A05, r5.A04, C34461pc.A00(r5), r5.A03, null, null, C34461pc.A01(r5), null, r5.A01.A0L, new C36271sn(r5.A00, 0, null)));
            C36181se r4 = this.A03;
            String str2 = str;
            int i3 = i;
            AnonymousClass07A.A04(r4.A01, new C36391sz(r4, str2, i3, A002, 0, A002.length, z2), 1632024476);
            this.A00.A06(AnonymousClass0CL.A03.name(), BuildConfig.FLAVOR);
            C36161sa r2 = this.A05;
            if (!(r2 == null || (r0 = r2.A01) == null)) {
                r2.A00 = r0.A05();
            }
            this.A02 = AnonymousClass08S.A0P(str2, ":", String.valueOf(i3));
        } catch (C70863bP e) {
            throw new IOException(e);
        } catch (IOException e2) {
            C010708t.A0V("WhistleClientCore", e2, "Failed to encode connectPayload=%s", r5);
            this.A00.A02(new AnonymousClass0CR(AnonymousClass0SB.A07, e2));
        }
    }

    public String B0g() {
        String str = this.A01;
        if (str == null) {
            return BuildConfig.FLAVOR;
        }
        return str;
    }

    public void C5M() {
        C36181se r0 = this.A03;
        AnonymousClass07A.A04(r0.A01, new C30165Eqk(r0), 616351661);
    }

    public void C5N() {
        C36181se r0 = this.A03;
        AnonymousClass07A.A04(r0.A01, new C30166Eql(r0), -204845959);
    }

    public void C5Q(String str, byte[] bArr, int i, int i2) {
        C36181se r3 = this.A03;
        AnonymousClass07A.A04(r3.A01, new C32251lS(r3, str, bArr, i, i2), -832102773);
    }

    public void C6f(AnonymousClass0CB r2, AnonymousClass0CA r3) {
        this.A00 = r2;
        AnonymousClass2FD r0 = this.A04;
        if (r0 != null) {
            r0.A00 = r3;
        }
    }

    public void CHp() {
        C36181se r0 = this.A03;
        AnonymousClass07A.A04(r0.A01, new C30167Eqm(r0), -602911279);
    }

    public C36151sZ(int i, int i2, MQTTClientFactory mQTTClientFactory, boolean z, ProxygenRadioMeter proxygenRadioMeter, C36161sa r10, Executor executor) {
        ConnectionParams.PublishFormat publishFormat;
        C36171sb r3 = new C36171sb(this);
        this.A05 = r10;
        ConnectionParams connectionParams = new ConnectionParams();
        if (i == 1) {
            publishFormat = ConnectionParams.PublishFormat.ZLIB;
        } else if (i != 2) {
            publishFormat = ConnectionParams.PublishFormat.RAW;
        } else {
            publishFormat = ConnectionParams.PublishFormat.ZLIB_OPTIONAL;
        }
        connectionParams.publishFormat = publishFormat;
        connectionParams.keepaliveSecs = i2;
        connectionParams.clientId = BuildConfig.FLAVOR;
        connectionParams.enableTopicEncoding = true;
        MQTTClient mQTTClient = new MQTTClient(mQTTClientFactory, r3, connectionParams);
        if (proxygenRadioMeter != null) {
            mQTTClient.mRadioMeter = proxygenRadioMeter;
        }
        if (z) {
            mQTTClient.mLogger = r10;
            AnonymousClass2FD r0 = new AnonymousClass2FD();
            this.A04 = r0;
            mQTTClient.mByteEventLogger = r0;
        }
        mQTTClient.init();
        this.A03 = new C36181se(mQTTClient, executor);
    }

    public String AWi(C02040Cp r2) {
        return r2.A02().A01;
    }

    public void C5T(List list, int i) {
        String[] strArr = new String[list.size()];
        int[] iArr = new int[list.size()];
        for (int i2 = 0; i2 < list.size(); i2++) {
            SubscribeTopic subscribeTopic = (SubscribeTopic) list.get(i2);
            strArr[i2] = subscribeTopic.A01;
            iArr[i2] = subscribeTopic.A00;
        }
        C36181se r0 = this.A03;
        AnonymousClass07A.A04(r0.A01, new C30161Eqg(r0, strArr, iArr), 1604649037);
    }

    public void C5U(List list, int i) {
        String[] strArr = new String[list.size()];
        list.toArray(strArr);
        Arrays.toString(strArr);
        C36181se r0 = this.A03;
        AnonymousClass07A.A04(r0.A01, new C30162Eqh(r0, strArr, i), -1060034033);
    }
}
