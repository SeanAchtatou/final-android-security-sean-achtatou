package X;

/* renamed from: X.0FZ  reason: invalid class name */
public final class AnonymousClass0FZ extends AnonymousClass0FM {
    public float batteryLevelPct;
    public long batteryRealtimeMs;
    public long chargingRealtimeMs;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            AnonymousClass0FZ r8 = (AnonymousClass0FZ) obj;
            return this.batteryLevelPct == r8.batteryLevelPct && this.batteryRealtimeMs == r8.batteryRealtimeMs && this.chargingRealtimeMs == r8.chargingRealtimeMs;
        }
        return false;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r3) {
        AnonymousClass0FZ r32 = (AnonymousClass0FZ) r3;
        this.batteryLevelPct = r32.batteryLevelPct;
        this.batteryRealtimeMs = r32.batteryRealtimeMs;
        this.chargingRealtimeMs = r32.chargingRealtimeMs;
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        AnonymousClass0FZ r52 = (AnonymousClass0FZ) r5;
        AnonymousClass0FZ r62 = (AnonymousClass0FZ) r6;
        if (r62 == null) {
            r62 = new AnonymousClass0FZ();
        }
        if (r52 == null) {
            r62.batteryLevelPct = this.batteryLevelPct;
            r62.batteryRealtimeMs = this.batteryRealtimeMs;
            r62.chargingRealtimeMs = this.chargingRealtimeMs;
            return r62;
        }
        r62.batteryLevelPct = this.batteryLevelPct - r52.batteryLevelPct;
        r62.batteryRealtimeMs = this.batteryRealtimeMs - r52.batteryRealtimeMs;
        r62.chargingRealtimeMs = this.chargingRealtimeMs - r52.chargingRealtimeMs;
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        AnonymousClass0FZ r52 = (AnonymousClass0FZ) r5;
        AnonymousClass0FZ r62 = (AnonymousClass0FZ) r6;
        if (r62 == null) {
            r62 = new AnonymousClass0FZ();
        }
        if (r52 == null) {
            r62.batteryLevelPct = this.batteryLevelPct;
            r62.batteryRealtimeMs = this.batteryRealtimeMs;
            r62.chargingRealtimeMs = this.chargingRealtimeMs;
            return r62;
        }
        r62.batteryLevelPct = this.batteryLevelPct + r52.batteryLevelPct;
        r62.batteryRealtimeMs = this.batteryRealtimeMs + r52.batteryRealtimeMs;
        r62.chargingRealtimeMs = this.chargingRealtimeMs + r52.chargingRealtimeMs;
        return r62;
    }

    public int hashCode() {
        int i;
        float f = this.batteryLevelPct;
        if (f != 0.0f) {
            i = Float.floatToIntBits(f);
        } else {
            i = 0;
        }
        long j = this.batteryRealtimeMs;
        long j2 = this.chargingRealtimeMs;
        return (((i * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "DeviceBatteryMetrics{batteryLevelPct=" + this.batteryLevelPct + ", batteryRealtimeMs=" + this.batteryRealtimeMs + ", chargingRealtimeMs=" + this.chargingRealtimeMs + '}';
    }
}
