package X;

import com.facebook.proxygen.TraceFieldType;

/* renamed from: X.18v  reason: invalid class name and case insensitive filesystem */
public final class C195818v {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("attachments_encrypted", "BLOB");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6("client_expiration_time_ms", "INTEGER");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("encrypted_content", "BLOB");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("expired", "INTEGER");
    public static final AnonymousClass0W6 A04 = new AnonymousClass0W6("facebook_hmac", "BLOB");
    public static final AnonymousClass0W6 A05 = new AnonymousClass0W6(TraceFieldType.MsgId, "TEXT");
    public static final AnonymousClass0W6 A06 = new AnonymousClass0W6(TraceFieldType.MsgType, "INTEGER");
    public static final AnonymousClass0W6 A07 = new AnonymousClass0W6("offline_threading_id", "TEXT");
    public static final AnonymousClass0W6 A08 = new AnonymousClass0W6("pending_send_media_attachment", "BLOB");
    public static final AnonymousClass0W6 A09 = new AnonymousClass0W6(C99084oO.$const$string(25), "INTEGER");
    public static final AnonymousClass0W6 A0A = new AnonymousClass0W6("send_error", "STRING");
    public static final AnonymousClass0W6 A0B = new AnonymousClass0W6("send_error_message", "STRING");
    public static final AnonymousClass0W6 A0C = new AnonymousClass0W6("send_error_timestamp_ms", "INTEGER");
    public static final AnonymousClass0W6 A0D = new AnonymousClass0W6("thread_encryption_key_version", "INTEGER");
    public static final AnonymousClass0W6 A0E = new AnonymousClass0W6("thread_key", "TEXT");
    public static final AnonymousClass0W6 A0F = new AnonymousClass0W6("timestamp_ms", "INTEGER");
    public static final AnonymousClass0W6 A0G = new AnonymousClass0W6("timestamp_sent_ms", "INTEGER");
}
