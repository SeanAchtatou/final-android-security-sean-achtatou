package X;

import com.facebook.mobileconfig.MobileConfigCxxChangeListener;
import com.facebook.mobileconfig.MobileConfigEmergencyPushChangeListener;
import com.facebook.tigon.iface.TigonServiceHolder;
import java.util.Map;

/* renamed from: X.1Yb  reason: invalid class name and case insensitive filesystem */
public final class C25031Yb implements AnonymousClass0WV {
    public C25051Yd A00;
    private volatile AnonymousClass0WV A01 = new AnonymousClass0WX();

    public synchronized AnonymousClass0WV A00() {
        return this.A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1 = java.util.Collections.unmodifiableSet(new java.util.HashSet(r4.A07));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        if (r1 == null) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        if ((r3 instanceof X.C25041Yc) == false) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        r6 = (X.AnonymousClass1ZM) ((X.C25041Yc) r3).A06(0);
        r5 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        if (r5.hasNext() == false) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        r3 = (android.util.Pair) r5.next();
        r6.A08(((java.lang.Long) r3.first).longValue(), (X.AnonymousClass10F) r3.second);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0057, code lost:
        r2 = r4.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0059, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r0 = java.util.Collections.unmodifiableSet(new java.util.HashSet(r4.A06));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0065, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0066, code lost:
        r3 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006e, code lost:
        if (r3.hasNext() == false) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0070, code lost:
        r0 = (X.C31811kQ) r3.next();
        r15.logExposure(r0.A01, r0.A00, r0.A02);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0080, code lost:
        r2 = r4.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0082, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r0 = java.util.Collections.unmodifiableList(new java.util.ArrayList(r4.A05));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008e, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008f, code lost:
        if (r0 == null) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0091, code lost:
        r1 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0099, code lost:
        if (r1.hasNext() == false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009b, code lost:
        r0 = (X.C30358Eui) r1.next();
        r7.logShadowResult(r0.A02, r0.A00, r0.A01, r0.A04, r0.A05, r0.A03);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b1, code lost:
        r15.isValid();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b4, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b5, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00bb, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        r4 = (X.AnonymousClass1Z7) r4;
        r2 = r4.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        monitor-enter(r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass0WV r15, X.C25051Yd r16) {
        /*
            r14 = this;
            r3 = r16
            monitor-enter(r14)
            X.0WV r4 = r14.A00()     // Catch:{ all -> 0x00c3 }
            r7 = r15
            r14.A01 = r15     // Catch:{ all -> 0x00c3 }
            r14.A00 = r3     // Catch:{ all -> 0x00c3 }
            boolean r0 = r4 instanceof X.AnonymousClass1Z7     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x00be
            X.0WV r0 = r14.A01     // Catch:{ all -> 0x00c3 }
            boolean r0 = r0 instanceof com.facebook.mobileconfig.MobileConfigManagerHolderImpl     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x00be
            monitor-exit(r14)     // Catch:{ all -> 0x00c3 }
            X.1Z7 r4 = (X.AnonymousClass1Z7) r4
            java.util.Set r2 = r4.A07
            monitor-enter(r2)
            java.util.HashSet r1 = new java.util.HashSet     // Catch:{ all -> 0x00bb }
            java.util.Set r0 = r4.A07     // Catch:{ all -> 0x00bb }
            r1.<init>(r0)     // Catch:{ all -> 0x00bb }
            java.util.Set r1 = java.util.Collections.unmodifiableSet(r1)     // Catch:{ all -> 0x00bb }
            monitor-exit(r2)     // Catch:{ all -> 0x00bb }
            if (r1 == 0) goto L_0x0057
            boolean r0 = r3 instanceof X.C25041Yc
            if (r0 == 0) goto L_0x0057
            X.1Yc r3 = (X.C25041Yc) r3
            r0 = 0
            X.0Wc r6 = r3.A06(r0)
            X.1ZM r6 = (X.AnonymousClass1ZM) r6
            java.util.Iterator r5 = r1.iterator()
        L_0x003b:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0057
            java.lang.Object r3 = r5.next()
            android.util.Pair r3 = (android.util.Pair) r3
            java.lang.Object r0 = r3.first
            java.lang.Long r0 = (java.lang.Long) r0
            long r1 = r0.longValue()
            java.lang.Object r0 = r3.second
            X.10F r0 = (X.AnonymousClass10F) r0
            r6.A08(r1, r0)
            goto L_0x003b
        L_0x0057:
            java.util.Set r2 = r4.A06
            monitor-enter(r2)
            java.util.HashSet r1 = new java.util.HashSet     // Catch:{ all -> 0x00b8 }
            java.util.Set r0 = r4.A06     // Catch:{ all -> 0x00b8 }
            r1.<init>(r0)     // Catch:{ all -> 0x00b8 }
            java.util.Set r0 = java.util.Collections.unmodifiableSet(r1)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r2)     // Catch:{ all -> 0x00b8 }
            java.util.Iterator r3 = r0.iterator()
        L_0x006a:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0080
            java.lang.Object r0 = r3.next()
            X.1kQ r0 = (X.C31811kQ) r0
            java.lang.String r2 = r0.A01
            java.lang.String r1 = r0.A00
            java.lang.String r0 = r0.A02
            r15.logExposure(r2, r1, r0)
            goto L_0x006a
        L_0x0080:
            java.util.List r2 = r4.A05
            monitor-enter(r2)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x00b5 }
            java.util.List r0 = r4.A05     // Catch:{ all -> 0x00b5 }
            r1.<init>(r0)     // Catch:{ all -> 0x00b5 }
            java.util.List r0 = java.util.Collections.unmodifiableList(r1)     // Catch:{ all -> 0x00b5 }
            monitor-exit(r2)     // Catch:{ all -> 0x00b5 }
            if (r0 == 0) goto L_0x00b1
            java.util.Iterator r1 = r0.iterator()
        L_0x0095:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00b1
            java.lang.Object r0 = r1.next()
            X.Eui r0 = (X.C30358Eui) r0
            java.lang.String r8 = r0.A02
            java.lang.String r9 = r0.A00
            java.lang.String r10 = r0.A01
            java.lang.String r11 = r0.A04
            java.lang.String r12 = r0.A05
            java.lang.String r13 = r0.A03
            r7.logShadowResult(r8, r9, r10, r11, r12, r13)
            goto L_0x0095
        L_0x00b1:
            r15.isValid()
            return
        L_0x00b5:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00b5 }
            goto L_0x00c5
        L_0x00b8:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00b8 }
            goto L_0x00c5
        L_0x00bb:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00bb }
            goto L_0x00c5
        L_0x00be:
            r15.isValid()     // Catch:{ all -> 0x00c3 }
            monitor-exit(r14)     // Catch:{ all -> 0x00c3 }
            return
        L_0x00c3:
            r0 = move-exception
            monitor-exit(r14)     // Catch:{ all -> 0x00c3 }
        L_0x00c5:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25031Yb.A01(X.0WV, X.1Yd):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r0 = X.AnonymousClass1Z7.A00(null, getLatestHandle());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003f, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0045, code lost:
        if (r0.equals(r6) == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0047, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(java.lang.String r6) {
        /*
            r5 = this;
            X.1Yd r4 = r5.A00
            r3 = 0
            if (r4 != 0) goto L_0x0006
            return r3
        L_0x0006:
            monitor-enter(r4)
            X.1Yd r1 = r5.A00     // Catch:{ all -> 0x0049 }
            boolean r0 = r1 instanceof X.C25041Yc     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x0035
            X.1Yc r1 = (X.C25041Yc) r1     // Catch:{ all -> 0x0049 }
            X.0Wc r0 = r1.A0J     // Catch:{ all -> 0x0049 }
            boolean r0 = r0 instanceof X.AnonymousClass1ZL     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x002c
            X.0Wc r0 = r1.A0J     // Catch:{ all -> 0x0049 }
            X.1ZL r0 = (X.AnonymousClass1ZL) r0     // Catch:{ all -> 0x0049 }
            X.1ZN r2 = r0.A03     // Catch:{ all -> 0x0049 }
            if (r2 == 0) goto L_0x002c
            r0 = 4
            int r1 = r2.A02(r0)     // Catch:{ all -> 0x0049 }
            if (r1 == 0) goto L_0x002c
            int r0 = r2.A00     // Catch:{ all -> 0x0049 }
            int r1 = r1 + r0
            java.lang.String r0 = r2.A05(r1)     // Catch:{ all -> 0x0049 }
            goto L_0x002d
        L_0x002c:
            r0 = 0
        L_0x002d:
            if (r0 == 0) goto L_0x0035
            boolean r0 = r0.equals(r6)     // Catch:{ all -> 0x0049 }
            monitor-exit(r4)     // Catch:{ all -> 0x0049 }
            return r0
        L_0x0035:
            monitor-exit(r4)     // Catch:{ all -> 0x0049 }
            X.0b2 r1 = r5.getLatestHandle()     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x0048 }
            r0 = 0
            java.lang.String r0 = X.AnonymousClass1Z7.A00(r0, r1)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x0048 }
            if (r0 == 0) goto L_0x0048
            boolean r0 = r0.equals(r6)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x0048 }
            if (r0 == 0) goto L_0x0048
            r3 = 1
        L_0x0048:
            return r3
        L_0x0049:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0049 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25031Yb.A02(java.lang.String):boolean");
    }

    public void clearAlternativeUpdater() {
        this.A01.clearAlternativeUpdater();
    }

    public void clearOverrides() {
        this.A01.clearOverrides();
    }

    public void deleteOldUserData(int i) {
        this.A01.deleteOldUserData(i);
    }

    public String getConsistencyLoggingFlagsJSON() {
        return this.A01.getConsistencyLoggingFlagsJSON();
    }

    public String getFrameworkStatus() {
        return this.A01.getFrameworkStatus();
    }

    public C06190b2 getLatestHandle() {
        return this.A01.getLatestHandle();
    }

    public C25851aV getNewOverridesTable() {
        return this.A01.getNewOverridesTable();
    }

    public C25851aV getNewOverridesTableIfExists() {
        return this.A01.getNewOverridesTableIfExists();
    }

    public boolean isConsistencyLoggingNeeded(AnonymousClass8ZO r2) {
        return this.A01.isConsistencyLoggingNeeded(r2);
    }

    public boolean isFetchNeeded() {
        return this.A01.isFetchNeeded();
    }

    public boolean isTigonServiceSet() {
        return this.A01.isTigonServiceSet();
    }

    public boolean isValid() {
        return this.A01.isValid();
    }

    public void logConfigs(String str, AnonymousClass8ZO r3, Map map) {
        this.A01.logConfigs(str, r3, map);
    }

    public void logExposure(String str, String str2, String str3) {
        this.A01.logExposure(str, str2, str3);
    }

    public void logShadowResult(String str, String str2, String str3, String str4, String str5, String str6) {
        this.A01.logShadowResult(str, str2, str3, str4, str5, str6);
    }

    public void logStorageConsistency() {
        this.A01.logStorageConsistency();
    }

    public boolean registerConfigChangeListener(MobileConfigCxxChangeListener mobileConfigCxxChangeListener) {
        return this.A01.registerConfigChangeListener(mobileConfigCxxChangeListener);
    }

    public boolean setEpHandler(MobileConfigEmergencyPushChangeListener mobileConfigEmergencyPushChangeListener) {
        return this.A01.setEpHandler(mobileConfigEmergencyPushChangeListener);
    }

    public boolean setSandboxURL(String str) {
        return this.A01.setSandboxURL(str);
    }

    public void setTigonService(TigonServiceHolder tigonServiceHolder, boolean z) {
        this.A01.setTigonService(tigonServiceHolder, z);
    }

    public String syncFetchReason() {
        return this.A01.syncFetchReason();
    }

    public boolean tryUpdateConfigsSynchronously(int i) {
        return this.A01.tryUpdateConfigsSynchronously(i);
    }

    public boolean updateConfigs() {
        return this.A01.updateConfigs();
    }

    public boolean updateConfigsSynchronouslyWithDefaultUpdater(int i) {
        return this.A01.updateConfigsSynchronouslyWithDefaultUpdater(i);
    }

    public boolean updateEmergencyPushConfigs() {
        return this.A01.updateEmergencyPushConfigs();
    }

    public boolean updateEmergencyPushConfigsSynchronously(int i) {
        return this.A01.updateEmergencyPushConfigsSynchronously(i);
    }
}
