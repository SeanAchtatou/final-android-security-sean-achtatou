package X;

import android.content.Context;

/* renamed from: X.1hr  reason: invalid class name and case insensitive filesystem */
public final class C30321hr {
    private AnonymousClass0UN A00;
    private final C30341ht A01;

    public static final C30321hr A00(AnonymousClass1XY r1) {
        return new C30321hr(r1);
    }

    public C30451i4 A01() {
        if (this.A01.A01(C22298Ase.$const$string(5))) {
            return (AoH) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AnR, this.A00);
        }
        return (C30441i3) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AXC, this.A00);
    }

    public C30431i2 A02() {
        if (this.A01.A01(C22298Ase.$const$string(5))) {
            return (C22089AoG) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ACT, this.A00);
        }
        return (C193917y) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AI2, this.A00);
    }

    private C30321hr(AnonymousClass1XY r4) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(5, r4);
        this.A00 = r2;
        this.A01 = C30341ht.A00(C22298Ase.$const$string(24), (Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r2));
    }
}
