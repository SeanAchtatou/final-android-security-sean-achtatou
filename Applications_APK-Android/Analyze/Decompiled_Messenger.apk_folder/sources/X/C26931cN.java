package X;

import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.http.interfaces.RequestPriority;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.http.Header;

/* renamed from: X.1cN  reason: invalid class name and case insensitive filesystem */
public final class C26931cN extends C07440dX {
    private static final AtomicInteger A02 = new AtomicInteger(0);
    public C09300gy A00 = C09300gy.A00;
    private C16890xw A01;

    public static C26931cN A00(C10880l0 r5) {
        if (!(r5 instanceof C07450dZ)) {
            if (r5.A0C("profile_image_small_size")) {
                r5.A07("profile_image_small_size", Integer.valueOf(C26901cK.A01()));
            }
            if (r5.A0C("profile_image_big_size")) {
                r5.A07("profile_image_big_size", Integer.valueOf(C26901cK.A00()));
            }
            if (r5.A0C("scale")) {
                r5.A06("scale", C26901cK.A03());
            }
            C26951cP.A00.incrementAndGet();
            return new C26931cN(r5);
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0T("Trying to create a ", "GraphQLRequest", " from a ", "TypedGraphQLMutationString", ". Use createMutationRequest() instead."));
    }

    public static C21899AkD A01(C07450dZ r1) {
        return new C21899AkD(r1);
    }

    public void A09(long j) {
        this.A00 = j * 1000;
    }

    public void A0C(RequestPriority requestPriority) {
        C16890xw r0 = this.A01;
        if (requestPriority != null) {
            r0.A04 = requestPriority;
        }
    }

    public final void A0D(ImmutableList immutableList) {
        if (immutableList != null) {
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                Header header = (Header) it.next();
                String name = header.getName();
                String value = header.getValue();
                Preconditions.checkNotNull(name);
                if (value != null) {
                    this.A0A.put(name, value);
                } else {
                    this.A0A.remove(name);
                }
            }
        }
    }

    private C26931cN(C10880l0 r4) {
        super(r4);
        Preconditions.checkNotNull(r4);
        r4.A0C = true;
        String str = r4.A06;
        this.A01 = new C16890xw(str == null ? "unknown" : str, null);
        A02.incrementAndGet();
    }

    public /* bridge */ /* synthetic */ C07440dX A02(ViewerContext viewerContext) {
        super.A02(viewerContext);
        return this;
    }

    public /* bridge */ /* synthetic */ C07440dX A03(C09290gx r1) {
        A0B(r1);
        return this;
    }

    public /* bridge */ /* synthetic */ C07440dX A04(String str) {
        super.A04(str);
        return this;
    }

    public /* bridge */ /* synthetic */ C07440dX A05(String str) {
        super.A05(str);
        return this;
    }

    public /* bridge */ /* synthetic */ C07440dX A06(boolean z) {
        super.A06(z);
        return this;
    }

    public /* bridge */ /* synthetic */ C07440dX A07(boolean z) {
        super.A07(z);
        return this;
    }

    public /* bridge */ /* synthetic */ C07440dX A08(boolean z) {
        super.A08(z);
        return this;
    }

    public void A0A(ViewerContext viewerContext) {
        super.A02(viewerContext);
    }

    public void A0B(C09290gx r1) {
        super.A03(r1);
    }

    public void A0E(String str) {
        super.A04(str);
    }

    public void A0F(String str) {
        super.A05(str);
    }

    public void A0G(boolean z) {
        super.A06(z);
    }

    public void A0H(boolean z) {
        super.A07(z);
    }

    public void A0I(boolean z) {
        super.A08(z);
    }
}
