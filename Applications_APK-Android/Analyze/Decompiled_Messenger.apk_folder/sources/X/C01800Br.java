package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.0Br  reason: invalid class name and case insensitive filesystem */
public final class C01800Br extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0AH A00;

    public C01800Br(AnonymousClass0AH r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(-1377825427);
        if (intent == null) {
            C000700l.A0D(intent, 1947763625, A01);
        } else if (!AnonymousClass0A2.A00(intent.getAction(), "android.os.action.POWER_SAVE_MODE_CHANGED")) {
            C000700l.A0D(intent, 1008913196, A01);
        } else {
            AnonymousClass0AH.A03(this.A00, intent);
            C000700l.A0D(intent, -642791542, A01);
        }
    }
}
