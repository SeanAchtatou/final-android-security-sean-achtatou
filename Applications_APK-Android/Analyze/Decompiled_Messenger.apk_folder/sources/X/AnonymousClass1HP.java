package X;

import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.messaging.montage.model.MontageInboxNuxItem;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1HP  reason: invalid class name */
public final class AnonymousClass1HP {
    public final InboxMontageItem A00;
    public final InboxUnitMontageActiveNowItem A01;
    public final MontageInboxNuxItem A02;
    public final MigColorScheme A03;
    public final Integer A04;
    public final boolean A05;

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "MONTAGE_COMPOSE";
            case 2:
                return "MY_MONTAGE";
            case 3:
                return "MONTAGE";
            case 4:
                return "NUX";
            case 5:
                return "LOAD_MORE";
            default:
                return "ACTIVE_NOW";
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r1.A0F(r5.A01) == false) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x005a
            r2 = 0
            if (r5 == 0) goto L_0x002e
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x002e
            X.1HP r5 = (X.AnonymousClass1HP) r5
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r4.A03
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r5.A03
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x002e
            java.lang.Integer r1 = r4.A04
            java.lang.Integer r0 = r5.A04
            if (r1 != r0) goto L_0x002e
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r1 = r4.A01
            if (r1 == 0) goto L_0x002f
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r0 = r5.A01
            boolean r0 = r1.A0F(r0)
            if (r0 != 0) goto L_0x0034
        L_0x002e:
            return r2
        L_0x002f:
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r0 = r5.A01
            if (r0 == 0) goto L_0x0034
            return r2
        L_0x0034:
            com.facebook.messaging.montage.model.MontageInboxNuxItem r1 = r4.A02
            if (r1 == 0) goto L_0x0041
            com.facebook.messaging.montage.model.MontageInboxNuxItem r0 = r5.A02
            boolean r0 = r1.A00(r0)
            if (r0 != 0) goto L_0x0046
            return r2
        L_0x0041:
            com.facebook.messaging.montage.model.MontageInboxNuxItem r0 = r5.A02
            if (r0 == 0) goto L_0x0046
            return r2
        L_0x0046:
            boolean r1 = r4.A05
            boolean r0 = r5.A05
            if (r1 != r0) goto L_0x002e
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r1 = r4.A00
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r0 = r5.A00
            if (r1 == 0) goto L_0x0057
            boolean r3 = r1.A0F(r0)
            return r3
        L_0x0057:
            if (r0 == 0) goto L_0x005a
            r3 = 0
        L_0x005a:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1HP.equals(java.lang.Object):boolean");
    }

    public static AnonymousClass1HP A00(InboxMontageItem inboxMontageItem, MigColorScheme migColorScheme) {
        Integer num;
        if (inboxMontageItem.A02.A08) {
            num = AnonymousClass07B.A0C;
        } else {
            num = AnonymousClass07B.A0N;
        }
        return new AnonymousClass1HP(num, null, inboxMontageItem, migColorScheme);
    }

    public int hashCode() {
        int i;
        int i2;
        Integer num = this.A04;
        int i3 = 0;
        if (num != null) {
            i = A01(num).hashCode() + num.intValue();
        } else {
            i = 0;
        }
        int i4 = i * 31;
        InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem = this.A01;
        if (inboxUnitMontageActiveNowItem != null) {
            i2 = inboxUnitMontageActiveNowItem.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 31;
        InboxMontageItem inboxMontageItem = this.A00;
        if (inboxMontageItem != null) {
            i3 = inboxMontageItem.hashCode();
        }
        return i5 + i3;
    }

    public String toString() {
        String str;
        BasicMontageThreadInfo basicMontageThreadInfo;
        StringBuilder sb = new StringBuilder("[type = ");
        Integer num = this.A04;
        if (num != null) {
            str = A01(num);
        } else {
            str = "null";
        }
        sb.append(str);
        sb.append(", user = ");
        InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem = this.A01;
        if (inboxUnitMontageActiveNowItem != null) {
            if (inboxUnitMontageActiveNowItem.A0J() != null) {
                sb.append(this.A01.A0J().A09());
                sb.append(", ");
            }
            sb.append(this.A01.A0L());
            sb.append(", ");
            InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem2 = this.A01;
            sb.append(inboxUnitMontageActiveNowItem2.A00.A03);
            sb.append(", ");
            sb.append(inboxUnitMontageActiveNowItem2.A04);
        } else {
            InboxMontageItem inboxMontageItem = this.A00;
            if (!(inboxMontageItem == null || (basicMontageThreadInfo = inboxMontageItem.A02) == null)) {
                sb.append(basicMontageThreadInfo.A06);
                sb.append(", ");
                sb.append(String.valueOf(basicMontageThreadInfo.A03));
                sb.append(", ");
                sb.append(this.A00.A01.A03);
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public AnonymousClass1HP(Integer num, InboxMontageItem inboxMontageItem, MigColorScheme migColorScheme) {
        this.A04 = num;
        this.A01 = null;
        this.A00 = inboxMontageItem;
        this.A02 = inboxMontageItem.A03;
        this.A03 = migColorScheme;
        this.A05 = false;
    }

    public AnonymousClass1HP(Integer num, InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem, InboxMontageItem inboxMontageItem, MigColorScheme migColorScheme) {
        this.A04 = num;
        this.A01 = inboxUnitMontageActiveNowItem;
        this.A00 = inboxMontageItem;
        this.A02 = null;
        this.A03 = migColorScheme;
        this.A05 = false;
    }

    public AnonymousClass1HP(Integer num, InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem, InboxMontageItem inboxMontageItem, MigColorScheme migColorScheme, boolean z) {
        this.A04 = num;
        this.A01 = inboxUnitMontageActiveNowItem;
        this.A00 = inboxMontageItem;
        this.A02 = null;
        this.A03 = migColorScheme;
        this.A05 = z;
    }
}
