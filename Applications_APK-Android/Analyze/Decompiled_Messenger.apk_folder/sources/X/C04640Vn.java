package X;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.0Vn  reason: invalid class name and case insensitive filesystem */
public class C04640Vn implements C04650Vo {
    private WeakHashMap A00;
    public final AnonymousClass0VF A01;
    public final AnonymousClass0VS A02;
    public final AnonymousClass0VT A03;
    public final C04610Vk A04;
    private final C04550Vd A05;
    private final C04590Vi A06;

    private AnonymousClass3YX A00(String str, Callable callable, AnonymousClass0VS r6, boolean z) {
        AnonymousClass3YW r2;
        boolean z2;
        synchronized (this) {
            AnonymousClass3YW r22 = null;
            WeakHashMap weakHashMap = this.A00;
            if (weakHashMap == null) {
                this.A00 = new WeakHashMap();
            } else {
                WeakReference weakReference = (WeakReference) weakHashMap.get(str);
                if (weakReference != null) {
                    r22 = (AnonymousClass3YW) weakReference.get();
                }
            }
            if (r22 == null) {
                r2 = new AnonymousClass3YW(callable);
            } else if (z) {
                synchronized (r22) {
                    if (!r22.A00) {
                        r22.cancel(false);
                    }
                }
                r2 = new AnonymousClass3YW(callable);
            } else {
                synchronized (r22) {
                    z2 = !r22.A00;
                }
                if (z2) {
                    return r22;
                }
                r2 = new AnonymousClass3YW(callable);
            }
            this.A00.put(str, new WeakReference(r2));
            A04(r2, r6);
            return r2;
        }
    }

    private void A04(Runnable runnable, AnonymousClass0VS r11) {
        Runnable runnable2 = runnable;
        AnonymousClass0VF r0 = this.A01;
        if (r0 != null) {
            runnable2 = r0.Bms(runnable);
        }
        C04590Vi r1 = this.A06;
        C04610Vk r5 = this.A04;
        AnonymousClass0VV r02 = r1.A01;
        if (!(r02 == null || runnable2 == null)) {
            runnable2 = r02.onCreateRunnable(runnable2);
        }
        r1.AYZ(new C07680dx(runnable2, r11, r5, r1.BLs(), r1.A02));
    }

    private AnonymousClass0Y0 A01(Runnable runnable, Object obj, Callable callable, AnonymousClass0VS r23, long j, long j2, TimeUnit timeUnit, Integer num) {
        AnonymousClass0VF r2;
        AnonymousClass0VF r22;
        Runnable runnable2 = runnable;
        long j3 = j;
        Callable callable2 = callable;
        if (j < 0) {
            j3 = 0;
        }
        if (!(runnable == null || (r22 = this.A01) == null)) {
            runnable2 = r22.Bms(runnable2);
        }
        if (!(callable == null || (r2 = this.A01) == null)) {
            callable2 = r2.BQi(callable2);
        }
        C04590Vi r15 = this.A06;
        C04610Vk r14 = this.A04;
        long BLs = r15.BLs();
        AnonymousClass0VV r24 = r15.A01;
        if (r24 != null) {
            if (runnable2 != null) {
                runnable2 = r24.onCreateRunnable(runnable2);
            }
            if (callable2 != null) {
                callable2 = r24.onCreateCallable(callable2);
            }
        }
        TimeUnit timeUnit2 = timeUnit;
        C07690e0 r4 = new C07690e0(runnable2, obj, callable2, r23, timeUnit2.toNanos(j3), timeUnit2.toNanos(j2), num, r14, r15, BLs, r15.A02);
        r15.AYZ(r4);
        return r4;
    }

    /* renamed from: C4Y */
    public AnonymousClass0Y0 schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        return A01(runnable, null, null, this.A02, j, 0, timeUnit, AnonymousClass07B.A00);
    }

    /* renamed from: C4Z */
    public AnonymousClass0Y0 schedule(Callable callable, long j, TimeUnit timeUnit) {
        return A01(null, null, callable, this.A02, j, 0, timeUnit, AnonymousClass07B.A00);
    }

    public AnonymousClass3YX CHb(String str, Callable callable) {
        return A00(str, callable, this.A02, false);
    }

    /* renamed from: CIC */
    public ListenableFuture submit(Runnable runnable) {
        return A01(runnable, null, null, this.A02, 0, 0, TimeUnit.NANOSECONDS, AnonymousClass07B.A00);
    }

    public ListenableFuture CID(Runnable runnable, AnonymousClass0VS r13) {
        return A01(runnable, null, null, r13, 0, 0, TimeUnit.NANOSECONDS, AnonymousClass07B.A00);
    }

    /* renamed from: CIE */
    public ListenableFuture submit(Runnable runnable, Object obj) {
        return A01(runnable, obj, null, this.A02, 0, 0, TimeUnit.NANOSECONDS, AnonymousClass07B.A00);
    }

    /* renamed from: CIF */
    public ListenableFuture submit(Callable callable) {
        return A01(null, null, callable, this.A02, 0, 0, TimeUnit.NANOSECONDS, AnonymousClass07B.A00);
    }

    public AnonymousClass3YX CII(String str, Callable callable) {
        return A00(str, callable, this.A02, true);
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        boolean z;
        C04550Vd r5 = this.A05;
        C04610Vk r6 = this.A04;
        r5.A06.A00();
        try {
            Preconditions.checkNotNull(r6.A00);
            long nanos = timeUnit.toNanos(j);
            while (true) {
                boolean z2 = false;
                if (r6.A0C.compareTo((Enum) C04620Vl.TERMINATED) >= 0) {
                    z2 = true;
                }
                if (z2) {
                    z = true;
                    break;
                } else if (nanos <= 0) {
                    z = false;
                    break;
                } else {
                    nanos = r6.A00.A01(nanos);
                }
            }
            return z;
        } finally {
            r5.A06.A02();
        }
    }

    public void execute(Runnable runnable) {
        A04(runnable, this.A02);
    }

    public boolean isShutdown() {
        if (this.A04.A0C.compareTo((Enum) C04620Vl.SHUTTING_DOWN) >= 0) {
            return true;
        }
        return false;
    }

    public boolean isTerminated() {
        if (this.A04.A0C.compareTo((Enum) C04620Vl.TERMINATED) >= 0) {
            return true;
        }
        return false;
    }

    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        return A01(runnable, null, null, this.A02, j, j2, timeUnit, AnonymousClass07B.A01);
    }

    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        return A01(runnable, null, null, this.A02, j, j2, timeUnit, AnonymousClass07B.A0C);
    }

    public void shutdown() {
        C04550Vd r2 = this.A05;
        C04610Vk r1 = this.A04;
        r2.A06.A00();
        try {
            r2.A06.A01();
            r1.A0H(r2.A06);
        } finally {
            r2.A06.A02();
        }
    }

    /* JADX INFO: finally extract failed */
    public List shutdownNow() {
        C04550Vd r4 = this.A05;
        C04610Vk r8 = this.A04;
        r4.A06.A00();
        try {
            r4.A06.A01();
            ArrayList<C07820eD> arrayList = new ArrayList<>();
            ArrayList<C04610Vk> arrayList2 = new ArrayList<>();
            arrayList2.add(r8);
            r8.A0D(arrayList2);
            for (C04610Vk r9 : arrayList2) {
                C42812By r7 = new C42812By(r9);
                int A042 = r9.A04(arrayList, r7, true);
                if (A042 > 0) {
                    AnonymousClass0VY r5 = r9.A0A;
                    int i = r5.A00;
                    boolean z = false;
                    if (i >= A042) {
                        z = true;
                    }
                    Preconditions.checkState(z);
                    r5.A00 = i - A042;
                    r9.A07();
                }
                AnonymousClass0VZ.A00(arrayList, r9.A0A.A05, r7, true);
                Preconditions.checkState(true);
            }
            r8.A0H(r4.A06);
            r4.A06.A02();
            for (C07820eD r1 : arrayList) {
                C179878Tk A002 = C25201Ys.A00(r4.A08, r1);
                if (A002 != null) {
                    A002.onTaskCanceled();
                }
                if (r1 instanceof C07690e0) {
                    ((C07690e0) r1).A01();
                }
            }
            return new ArrayList(arrayList);
        } catch (Throwable th) {
            r4.A06.A02();
            throw th;
        }
    }

    public C04640Vn(AnonymousClass0VT r1, C04590Vi r2, C04550Vd r3, C04610Vk r4, AnonymousClass0VS r5, AnonymousClass0VF r6) {
        this.A03 = r1;
        this.A06 = r2;
        this.A05 = r3;
        this.A04 = r4;
        this.A01 = r6;
        this.A02 = r5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r2.A03.unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d1, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d2, code lost:
        r2.A03.unlock();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object A02(java.util.Collection r11, boolean r12, long r13) {
        /*
            r10 = this;
            com.google.common.base.Preconditions.checkNotNull(r11)
            int r1 = r11.size()
            r0 = 0
            if (r1 <= 0) goto L_0x000b
            r0 = 1
        L_0x000b:
            com.google.common.base.Preconditions.checkArgument(r0)
            X.ERZ r2 = new X.ERZ
            r2.<init>(r1)
            r3 = 0
            java.util.Iterator r9 = r11.iterator()     // Catch:{ all -> 0x00ec }
            java.lang.Object r0 = r9.next()     // Catch:{ all -> 0x00ec }
            java.util.concurrent.Callable r0 = (java.util.concurrent.Callable) r0     // Catch:{ all -> 0x00ec }
            com.google.common.util.concurrent.ListenableFuture r0 = r10.submit(r0)     // Catch:{ all -> 0x00ec }
            X.0e0 r0 = (X.C07690e0) r0     // Catch:{ all -> 0x00ec }
            r2.A01(r0)     // Catch:{ all -> 0x00ec }
            int r8 = r1 + -1
            r7 = 1
        L_0x002a:
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.lock()     // Catch:{ all -> 0x00ec }
            java.util.Queue r0 = r2.A01     // Catch:{ all -> 0x00de }
            java.lang.Object r4 = r0.poll()     // Catch:{ all -> 0x00de }
            X.0e0 r4 = (X.C07690e0) r4     // Catch:{ all -> 0x00de }
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.unlock()     // Catch:{ all -> 0x00ec }
            if (r4 != 0) goto L_0x00ad
            if (r8 <= 0) goto L_0x0052
            int r8 = r8 + -1
            java.lang.Object r0 = r9.next()     // Catch:{ all -> 0x00ec }
            java.util.concurrent.Callable r0 = (java.util.concurrent.Callable) r0     // Catch:{ all -> 0x00ec }
            com.google.common.util.concurrent.ListenableFuture r0 = r10.submit(r0)     // Catch:{ all -> 0x00ec }
            X.0e0 r0 = (X.C07690e0) r0     // Catch:{ all -> 0x00ec }
            r2.A01(r0)     // Catch:{ all -> 0x00ec }
            goto L_0x00ab
        L_0x0052:
            if (r7 == 0) goto L_0x00d8
            if (r12 == 0) goto L_0x008f
            long r4 = X.AnonymousClass0Y1.A00()     // Catch:{ all -> 0x00ec }
            long r0 = r13 - r4
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00ec }
            long r0 = r4.toNanos(r0)     // Catch:{ all -> 0x00ec }
            java.util.concurrent.locks.ReentrantLock r4 = r2.A03     // Catch:{ all -> 0x00ec }
            r4.lock()     // Catch:{ all -> 0x00ec }
        L_0x0067:
            java.util.Queue r4 = r2.A01     // Catch:{ all -> 0x00e5 }
            java.lang.Object r4 = r4.poll()     // Catch:{ all -> 0x00e5 }
            X.0e0 r4 = (X.C07690e0) r4     // Catch:{ all -> 0x00e5 }
            if (r4 == 0) goto L_0x0072
            goto L_0x0080
        L_0x0072:
            r5 = 0
            int r4 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r4 > 0) goto L_0x0079
            goto L_0x0086
        L_0x0079:
            java.util.concurrent.locks.Condition r4 = r2.A02     // Catch:{ all -> 0x00e5 }
            long r0 = r4.awaitNanos(r0)     // Catch:{ all -> 0x00e5 }
            goto L_0x0067
        L_0x0080:
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.unlock()     // Catch:{ all -> 0x00ec }
            goto L_0x008c
        L_0x0086:
            r4 = 0
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.unlock()     // Catch:{ all -> 0x00ec }
        L_0x008c:
            if (r4 != 0) goto L_0x00ad
            goto L_0x00cb
        L_0x008f:
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.lock()     // Catch:{ all -> 0x00ec }
        L_0x0094:
            java.util.Queue r0 = r2.A01     // Catch:{ all -> 0x00d1 }
            java.lang.Object r4 = r0.poll()     // Catch:{ all -> 0x00d1 }
            X.0e0 r4 = (X.C07690e0) r4     // Catch:{ all -> 0x00d1 }
            if (r4 == 0) goto L_0x009f
            goto L_0x00a5
        L_0x009f:
            java.util.concurrent.locks.Condition r0 = r2.A02     // Catch:{ all -> 0x00d1 }
            r0.await()     // Catch:{ all -> 0x00d1 }
            goto L_0x0094
        L_0x00a5:
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.unlock()     // Catch:{ all -> 0x00ec }
            goto L_0x00ad
        L_0x00ab:
            int r7 = r7 + 1
        L_0x00ad:
            if (r4 == 0) goto L_0x002a
            int r7 = r7 + -1
            r0 = 0
            if (r7 < 0) goto L_0x00b5
            r0 = 1
        L_0x00b5:
            com.google.common.base.Preconditions.checkState(r0)     // Catch:{ all -> 0x00ec }
            java.lang.Object r0 = r4.get()     // Catch:{ ExecutionException -> 0x00c8, RuntimeException -> 0x00c0 }
            r2.A00()
            return r0
        L_0x00c0:
            r0 = move-exception
            java.util.concurrent.ExecutionException r3 = new java.util.concurrent.ExecutionException     // Catch:{ all -> 0x00ec }
            r3.<init>(r0)     // Catch:{ all -> 0x00ec }
            goto L_0x002a
        L_0x00c8:
            r3 = move-exception
            goto L_0x002a
        L_0x00cb:
            java.util.concurrent.TimeoutException r0 = new java.util.concurrent.TimeoutException     // Catch:{ all -> 0x00ec }
            r0.<init>()     // Catch:{ all -> 0x00ec }
            throw r0     // Catch:{ all -> 0x00ec }
        L_0x00d1:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.unlock()     // Catch:{ all -> 0x00ec }
            goto L_0x00eb
        L_0x00d8:
            com.google.common.base.Preconditions.checkNotNull(r3)     // Catch:{ all -> 0x00ec }
            java.util.concurrent.ExecutionException r3 = (java.util.concurrent.ExecutionException) r3     // Catch:{ all -> 0x00ec }
            throw r3     // Catch:{ all -> 0x00ec }
        L_0x00de:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.unlock()     // Catch:{ all -> 0x00ec }
            goto L_0x00eb
        L_0x00e5:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantLock r0 = r2.A03     // Catch:{ all -> 0x00ec }
            r0.unlock()     // Catch:{ all -> 0x00ec }
        L_0x00eb:
            throw r1     // Catch:{ all -> 0x00ec }
        L_0x00ec:
            r0 = move-exception
            r2.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04640Vn.A02(java.util.Collection, boolean, long):java.lang.Object");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List A03(java.util.Collection r9, boolean r10, long r11) {
        /*
            r8 = this;
            com.google.common.base.Preconditions.checkNotNull(r9)
            java.util.ArrayList r4 = new java.util.ArrayList
            int r0 = r9.size()
            r4.<init>(r0)
            java.util.Iterator r1 = r9.iterator()     // Catch:{ all -> 0x005f }
        L_0x0010:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x0024
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x005f }
            java.util.concurrent.Callable r0 = (java.util.concurrent.Callable) r0     // Catch:{ all -> 0x005f }
            com.google.common.util.concurrent.ListenableFuture r0 = r8.submit(r0)     // Catch:{ all -> 0x005f }
            r4.add(r0)     // Catch:{ all -> 0x005f }
            goto L_0x0010
        L_0x0024:
            r7 = 0
        L_0x0025:
            int r0 = r4.size()     // Catch:{ all -> 0x005f }
            if (r7 >= r0) goto L_0x005e
            java.lang.Object r3 = r4.get(r7)     // Catch:{ all -> 0x005f }
            java.util.concurrent.Future r3 = (java.util.concurrent.Future) r3     // Catch:{ all -> 0x005f }
            boolean r0 = r3.isDone()     // Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0048
            if (r10 == 0) goto L_0x0045
            long r5 = X.AnonymousClass0Y1.A00()     // Catch:{ CancellationException | ExecutionException -> 0x0048, TimeoutException -> 0x004b }
            long r1 = r11 - r5
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ CancellationException | ExecutionException -> 0x0048, TimeoutException -> 0x004b }
            r3.get(r1, r0)     // Catch:{ CancellationException | ExecutionException -> 0x0048, TimeoutException -> 0x004b }
            goto L_0x0048
        L_0x0045:
            r3.get()     // Catch:{ CancellationException | ExecutionException -> 0x0048, TimeoutException -> 0x004b }
        L_0x0048:
            int r7 = r7 + 1
            goto L_0x0025
        L_0x004b:
            int r0 = r4.size()     // Catch:{ all -> 0x005f }
            if (r7 >= r0) goto L_0x005e
            java.lang.Object r1 = r4.get(r7)     // Catch:{ all -> 0x005f }
            java.util.concurrent.Future r1 = (java.util.concurrent.Future) r1     // Catch:{ all -> 0x005f }
            r0 = 1
            r1.cancel(r0)     // Catch:{ all -> 0x005f }
            int r7 = r7 + 1
            goto L_0x004b
        L_0x005e:
            return r4
        L_0x005f:
            r3 = move-exception
            r2 = 0
        L_0x0061:
            int r0 = r4.size()
            if (r2 >= r0) goto L_0x0074
            java.lang.Object r1 = r4.get(r2)
            java.util.concurrent.Future r1 = (java.util.concurrent.Future) r1
            r0 = 1
            r1.cancel(r0)
            int r2 = r2 + 1
            goto L_0x0061
        L_0x0074:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04640Vn.A03(java.util.Collection, boolean, long):java.util.List");
    }

    public List invokeAll(Collection collection) {
        return A03(collection, false, 0);
    }

    public List invokeAll(Collection collection, long j, TimeUnit timeUnit) {
        return A03(collection, true, AnonymousClass0Y1.A00() + timeUnit.toNanos(j));
    }

    public Object invokeAny(Collection collection) {
        try {
            return A02(collection, false, 0);
        } catch (TimeoutException unused) {
            Preconditions.checkState(false);
            throw new RuntimeException();
        }
    }

    public Object invokeAny(Collection collection, long j, TimeUnit timeUnit) {
        return A02(collection, true, AnonymousClass0Y1.A00() + timeUnit.toNanos(j));
    }
}
