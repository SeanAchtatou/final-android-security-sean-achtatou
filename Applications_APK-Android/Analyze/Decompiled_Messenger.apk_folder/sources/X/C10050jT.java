package X;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;

/* renamed from: X.0jT  reason: invalid class name and case insensitive filesystem */
public class C10050jT extends C10060jU implements Serializable {
    public static final C10110jZ BOOLEAN_DESC;
    public static final C10110jZ INT_DESC;
    public static final C10110jZ LONG_DESC;
    public static final C10110jZ STRING_DESC;
    public static final C10050jT instance = new C10050jT();
    private static final long serialVersionUID = 1;

    public /* bridge */ /* synthetic */ C10120ja forCreation(C10490kF r7, C10030jR r8, C26761by r9) {
        C10030jR r2 = r8;
        C10110jZ _findCachedDesc = _findCachedDesc(r8);
        if (_findCachedDesc != null) {
            return _findCachedDesc;
        }
        return C10110jZ.forDeserialization(collectProperties(r7, r2, r9, false, "set"));
    }

    public /* bridge */ /* synthetic */ C10120ja forDeserializationWithBuilder(C10490kF r9, C10030jR r10, C26761by r11) {
        C10140jc r1;
        String str;
        C10490kF r3 = r9;
        BM3 bm3 = null;
        if (r9.isEnabled(C26771bz.USE_ANNOTATIONS)) {
            r1 = r9.getAnnotationIntrospector();
        } else {
            r1 = null;
        }
        C10030jR r5 = r10;
        C10070jV construct = C10070jV.construct(r10._class, r1, r11);
        if (r1 != null) {
            bm3 = r1.findPOJOBuilderConfig(construct);
        }
        if (bm3 == null) {
            str = "with";
        } else {
            str = bm3.withPrefix;
        }
        C183412k r2 = new C183412k(r3, false, r5, construct, str);
        r2.collect();
        return C10110jZ.forDeserialization(r2);
    }

    static {
        Class<String> cls = String.class;
        STRING_DESC = new C10110jZ(null, C10020jP.constructUnsafe(cls), C10070jV.constructWithoutSuperTypes(cls, null, null), Collections.emptyList());
        Class cls2 = Boolean.TYPE;
        BOOLEAN_DESC = new C10110jZ(null, C10020jP.constructUnsafe(cls2), C10070jV.constructWithoutSuperTypes(cls2, null, null), Collections.emptyList());
        Class cls3 = Integer.TYPE;
        INT_DESC = new C10110jZ(null, C10020jP.constructUnsafe(cls3), C10070jV.constructWithoutSuperTypes(cls3, null, null), Collections.emptyList());
        Class cls4 = Long.TYPE;
        LONG_DESC = new C10110jZ(null, C10020jP.constructUnsafe(cls4), C10070jV.constructWithoutSuperTypes(cls4, null, null), Collections.emptyList());
    }

    public static C10110jZ _findCachedDesc(C10030jR r1) {
        Class<String> cls = r1._class;
        if (cls == String.class) {
            return STRING_DESC;
        }
        if (cls == Boolean.TYPE) {
            return BOOLEAN_DESC;
        }
        if (cls == Integer.TYPE) {
            return INT_DESC;
        }
        if (cls == Long.TYPE) {
            return LONG_DESC;
        }
        return null;
    }

    private C183412k collectProperties(C10470kA r8, C10030jR r9, C26761by r10, boolean z, String str) {
        C10140jc r0;
        C10470kA r2 = r8;
        boolean isEnabled = r8.isEnabled(C26771bz.USE_ANNOTATIONS);
        C10030jR r4 = r9;
        Class cls = r9._class;
        if (isEnabled) {
            r0 = r8.getAnnotationIntrospector();
        } else {
            r0 = null;
        }
        C183412k r1 = new C183412k(r2, z, r4, C10070jV.construct(cls, r0, r10), str);
        r1.collect();
        return r1;
    }

    public static C10110jZ forDirectClassAnnotations(C10470kA r3, C10030jR r4, C26761by r5) {
        boolean isEnabled = r3.isEnabled(C26771bz.USE_ANNOTATIONS);
        C10140jc annotationIntrospector = r3.getAnnotationIntrospector();
        Class cls = r4._class;
        if (!isEnabled) {
            annotationIntrospector = null;
        }
        return new C10110jZ(r3, r4, C10070jV.constructWithoutSuperTypes(cls, annotationIntrospector, r5), Collections.emptyList());
    }

    public /* bridge */ /* synthetic */ C10120ja forClassAnnotations(C10470kA r4, C10030jR r5, C26761by r6) {
        C10140jc r0;
        boolean isEnabled = r4.isEnabled(C26771bz.USE_ANNOTATIONS);
        Class cls = r5._class;
        if (isEnabled) {
            r0 = r4.getAnnotationIntrospector();
        } else {
            r0 = null;
        }
        return new C10110jZ(r4, r5, C10070jV.construct(cls, r0, r6), Collections.emptyList());
    }

    public C10110jZ forDeserialization(C10490kF r7, C10030jR r8, C26761by r9) {
        C10030jR r2 = r8;
        C10110jZ _findCachedDesc = _findCachedDesc(r8);
        if (_findCachedDesc != null) {
            return _findCachedDesc;
        }
        return C10110jZ.forDeserialization(collectProperties(r7, r2, r9, false, "set"));
    }

    public C10110jZ forSerialization(C10450k8 r10, C10030jR r11, C26761by r12) {
        C183512m r0;
        C10030jR r5 = r11;
        C10110jZ _findCachedDesc = _findCachedDesc(r11);
        if (_findCachedDesc == null) {
            C183412k collectProperties = collectProperties(r10, r5, r12, true, "set");
            _findCachedDesc = new C10110jZ(collectProperties);
            _findCachedDesc._jsonValueMethod = collectProperties.getJsonValueMethod();
            LinkedList linkedList = collectProperties._anyGetters;
            if (linkedList != null) {
                if (linkedList.size() > 1) {
                    C183412k.reportProblem(collectProperties, "Multiple 'any-getters' defined (" + collectProperties._anyGetters.get(0) + " vs " + collectProperties._anyGetters.get(1) + ")");
                }
                r0 = (C183512m) collectProperties._anyGetters.getFirst();
            } else {
                r0 = null;
            }
            _findCachedDesc._anyGetter = r0;
        }
        return _findCachedDesc;
    }
}
