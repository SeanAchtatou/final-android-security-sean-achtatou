package X;

import android.content.Context;
import com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.17x  reason: invalid class name and case insensitive filesystem */
public final class C193817x implements C15520vQ {
    private static volatile C193817x A02;
    private AnonymousClass0UN A00;
    private final AnonymousClass1MT A01;

    public String B5H() {
        return "CameraButton";
    }

    public static final C193817x A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C193817x.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C193817x(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public C15390vD Ae6() {
        return C32521lt.A00;
    }

    public AnonymousClass10Z Ap6(Context context) {
        return AnonymousClass10Z.A00(AnonymousClass01R.A03(context, this.A01.A02(C34891qL.A0C, AnonymousClass07B.A01)));
    }

    public void BSD(Context context, C33691nz r14, C13060qW r15) {
        ((C13410rO) AnonymousClass1XX.A03(AnonymousClass1Y3.Ajr, this.A00)).A01.A01.A0D("topRightCamera");
        C05720aD r6 = (C05720aD) AnonymousClass1XX.A03(AnonymousClass1Y3.Az1, this.A00);
        C414225k r4 = (C414225k) AnonymousClass1XX.A03(AnonymousClass1Y3.AbV, this.A00);
        C191617a r1 = (C191617a) AnonymousClass1XX.A03(AnonymousClass1Y3.AdZ, this.A00);
        AnonymousClass1XX.A03(AnonymousClass1Y3.AyH, this.A00);
        C73473g8 r11 = (C73473g8) AnonymousClass1XX.A03(AnonymousClass1Y3.ASj, this.A00);
        C73443g5 r10 = C73443g5.A06;
        String str = "free_messenger_open_camera_interstitial";
        C33691nz r8 = r14;
        if (r6.A08("semi_free_messenger_open_camera_interstitial") || r6.A08(str)) {
            if (r6.A08("semi_free_messenger_open_camera_interstitial")) {
                str = "semi_free_messenger_open_camera_interstitial";
            }
            AnonymousClass5BF r5 = new AnonymousClass5BF(r6, str, r8, context, r10, r11);
            if (r6.A07(AnonymousClass24B.$const$string(6))) {
                r1.A04(context, str, r5, null);
                return;
            }
            r4.A06(str, context.getResources().getString(2131832736), context.getResources().getString(2131832735), r5);
            r4.A0B(str, r15, null);
            return;
        }
        r14.BvS(context, NavigationTrigger.A00(C99084oO.$const$string(605)), MontageComposerFragmentParams.A01(r10, C73453g6.A00(r10), r11));
    }

    private C193817x(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = AnonymousClass1MT.A00(r3);
    }

    public String AiM(Context context) {
        return context.getString(2131822335);
    }
}
