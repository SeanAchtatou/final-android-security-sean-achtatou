package X;

/* renamed from: X.1g7  reason: invalid class name and case insensitive filesystem */
public final class C29251g7 {
    public String A00 = "UNKNOWN";
    public String A01;
    public String A02;
    public boolean A03;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        if (r7 != 2) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.String r8 = "UNKNOWN"
            if (r10 == 0) goto L_0x002d
            r7 = -1
            int r6 = r10.hashCode()
            r0 = -1164782942(0xffffffffba92d2a2, float:-0.001120169)
            java.lang.String r5 = "AUDIO_GROUP_CALL"
            java.lang.String r4 = "NO_ONGOING_CALL"
            java.lang.String r3 = "VIDEO_GROUP_CALL"
            r2 = 2
            r1 = 1
            if (r6 == r0) goto L_0x003d
            r0 = 298203776(0x11c63a80, float:3.1274945E-28)
            if (r6 == r0) goto L_0x0035
            r0 = 703640679(0x29f0b467, float:1.0689436E-13)
            if (r6 != r0) goto L_0x0027
            boolean r0 = r10.equals(r5)
            if (r0 == 0) goto L_0x0027
            r7 = 1
        L_0x0027:
            if (r7 == 0) goto L_0x0033
            if (r7 == r1) goto L_0x0031
            if (r7 == r2) goto L_0x002e
        L_0x002d:
            r3 = r8
        L_0x002e:
            r9.A00 = r3
            return
        L_0x0031:
            r3 = r5
            goto L_0x002e
        L_0x0033:
            r3 = r4
            goto L_0x002e
        L_0x0035:
            boolean r0 = r10.equals(r4)
            if (r0 == 0) goto L_0x0027
            r7 = 0
            goto L_0x0027
        L_0x003d:
            boolean r0 = r10.equals(r3)
            if (r0 == 0) goto L_0x0027
            r7 = 2
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29251g7.A00(java.lang.String):void");
    }
}
