package X;

/* renamed from: X.1O3  reason: invalid class name */
public final class AnonymousClass1O3 {
    public static final AnonymousClass1O3 A02 = new AnonymousClass1O3("UNKNOWN", null);
    public final String A00;
    public final String A01;

    public String toString() {
        return this.A01;
    }

    public AnonymousClass1O3(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }
}
