package X;

import android.content.Context;
import java.lang.reflect.Constructor;

/* renamed from: X.1pw  reason: invalid class name and case insensitive filesystem */
public final class C34641pw extends C34061oa {
    public static C34651px A01 = new C34651px();
    public static boolean A02;
    public Object A00 = null;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r1.A03 == null) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C26381bM A02(android.content.Context r2) {
        /*
            X.1px r1 = X.C34641pw.A01
            java.lang.Class r0 = r1.A00
            if (r0 == 0) goto L_0x000f
            java.lang.reflect.Method r0 = r1.A02
            if (r0 == 0) goto L_0x000f
            java.lang.reflect.Method r1 = r1.A03
            r0 = 1
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 == 0) goto L_0x0018
            X.376 r0 = new X.376
            r0.<init>(r2)
            return r0
        L_0x0018:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34641pw.A02(android.content.Context):X.1bM");
    }

    public C34641pw() {
        Class cls = A01.A00;
        Object obj = null;
        if (cls != null) {
            try {
                obj = cls.newInstance();
            } catch (Exception unused) {
            }
        }
        this.A00 = obj;
    }

    public C34641pw(Context context) {
        Constructor constructor = A01.A01;
        Object[] objArr = {context};
        Object obj = null;
        if (constructor != null) {
            try {
                obj = constructor.newInstance(objArr);
            } catch (Exception unused) {
            }
        }
        this.A00 = obj;
    }
}
