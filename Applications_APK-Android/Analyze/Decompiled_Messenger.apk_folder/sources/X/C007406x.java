package X;

import android.database.sqlite.SQLiteDatabase;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.06x  reason: invalid class name and case insensitive filesystem */
public final class C007406x {
    public static final ThreadLocal A00 = new C007506y();

    public static void A00(int i) {
        if (A00.get() == Boolean.FALSE) {
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 21, 0, 0, i, 0, 0);
        }
    }

    public static void A01(SQLiteDatabase sQLiteDatabase, int i) {
        Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 22, 0, 0, i, 0, 0);
        sQLiteDatabase.beginTransaction();
        A00.set(Boolean.TRUE);
    }

    public static void A02(SQLiteDatabase sQLiteDatabase, int i) {
        int i2 = i;
        try {
            sQLiteDatabase.endTransaction();
            if (!sQLiteDatabase.inTransaction()) {
                A00.set(Boolean.FALSE);
            }
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 23, 0, 0, i2, 0, 0);
        } catch (Throwable th) {
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 23, 0, 0, i2, 0, 0);
            throw th;
        }
    }
}
