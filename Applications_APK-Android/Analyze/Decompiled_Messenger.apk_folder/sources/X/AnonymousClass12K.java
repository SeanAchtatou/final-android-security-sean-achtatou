package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.12K  reason: invalid class name */
public final class AnonymousClass12K implements C15520vQ {
    private static volatile AnonymousClass12K A01;
    private final AnonymousClass1MT A00;

    public String B5H() {
        return "AllContactsButton";
    }

    public static final AnonymousClass12K A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass12K.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass12K(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public C15390vD Ae6() {
        return C32521lt.A00;
    }

    public AnonymousClass10Z Ap6(Context context) {
        return AnonymousClass10Z.A00(AnonymousClass01R.A03(context, this.A00.A02(C34891qL.A0I, AnonymousClass07B.A01)));
    }

    private AnonymousClass12K(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1MT.A00(r2);
    }

    public String AiM(Context context) {
        return context.getString(2131821431);
    }

    public void BSD(Context context, C33691nz r2, C13060qW r3) {
        r2.Bvw(context);
    }
}
