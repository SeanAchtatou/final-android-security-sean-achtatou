package X;

import java.net.Socket;
import java.util.concurrent.Callable;

/* renamed from: X.0ST  reason: invalid class name */
public final class AnonymousClass0ST implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass0SS A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ Socket A03;

    public AnonymousClass0ST(AnonymousClass0SS r1, Socket socket, String str, int i) {
        this.A01 = r1;
        this.A03 = socket;
        this.A02 = str;
        this.A00 = i;
    }

    public Object call() {
        return this.A01.A00(this.A03, this.A02, this.A00);
    }
}
