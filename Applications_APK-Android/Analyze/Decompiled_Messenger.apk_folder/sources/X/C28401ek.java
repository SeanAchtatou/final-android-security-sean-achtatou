package X;

import android.database.Cursor;
import android.util.Log;

/* renamed from: X.1ek  reason: invalid class name and case insensitive filesystem */
public abstract class C28401ek implements C28411el {
    private Exception A00;
    public final Cursor A01;
    private volatile boolean A02;

    private void A00() {
        if (this.A01.isClosed()) {
            String stackTraceString = Log.getStackTraceString(this.A00);
            C010708t.A0I("AbstractDAOItem", stackTraceString);
            throw new IllegalStateException(AnonymousClass08S.A0J("Can't access DAO when it is already closed: ", stackTraceString));
        }
    }

    public void close() {
        this.A01.close();
        this.A00 = new Exception();
    }

    public C28401ek(Cursor cursor) {
        if (cursor != null) {
            this.A01 = cursor;
            return;
        }
        throw new IllegalArgumentException("cursor is null");
    }

    public int getCount() {
        A00();
        return this.A01.getCount();
    }

    public boolean moveToFirst() {
        A00();
        return this.A01.moveToFirst();
    }

    public boolean moveToNext() {
        A00();
        return this.A01.moveToNext();
    }
}
