package X;

import java.util.Collections;
import java.util.Map;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0CL  reason: invalid class name */
public final class AnonymousClass0CL extends Enum {
    public static final Map A00 = Collections.unmodifiableMap(new AnonymousClass0CM());
    private static final /* synthetic */ AnonymousClass0CL[] A01;
    public static final AnonymousClass0CL A02;
    public static final AnonymousClass0CL A03;
    public static final AnonymousClass0CL A04;
    public static final AnonymousClass0CL A05;
    public static final AnonymousClass0CL A06;
    public static final AnonymousClass0CL A07;
    public static final AnonymousClass0CL A08;
    public static final AnonymousClass0CL A09;
    public static final AnonymousClass0CL A0A;
    public static final AnonymousClass0CL A0B;
    public static final AnonymousClass0CL A0C;
    public final int mValue;

    static {
        AnonymousClass0CL r14 = new AnonymousClass0CL("CONNECT", 0, 1);
        A03 = r14;
        AnonymousClass0CL r13 = new AnonymousClass0CL("CONNACK", 1, 2);
        A02 = r13;
        AnonymousClass0CL r12 = new AnonymousClass0CL("PUBLISH", 2, 3);
        A08 = r12;
        AnonymousClass0CL r11 = new AnonymousClass0CL("PUBACK", 3, 4);
        A07 = r11;
        AnonymousClass0CL r0 = new AnonymousClass0CL("PUBREC", 4, 5);
        AnonymousClass0CL r15 = new AnonymousClass0CL("PUBREL", 5, 6);
        AnonymousClass0CL r10 = new AnonymousClass0CL("PUBCOMP", 6, 7);
        AnonymousClass0CL r9 = new AnonymousClass0CL("SUBSCRIBE", 7, 8);
        A0A = r9;
        AnonymousClass0CL r8 = new AnonymousClass0CL("SUBACK", 8, 9);
        A09 = r8;
        AnonymousClass0CL r7 = new AnonymousClass0CL("UNSUBSCRIBE", 9, 10);
        A0C = r7;
        AnonymousClass0CL r6 = new AnonymousClass0CL("UNSUBACK", 10, 11);
        A0B = r6;
        AnonymousClass0CL r5 = new AnonymousClass0CL("PINGREQ", 11, 12);
        A05 = r5;
        AnonymousClass0CL r4 = new AnonymousClass0CL("PINGRESP", 12, 13);
        A06 = r4;
        AnonymousClass0CL r2 = new AnonymousClass0CL("DISCONNECT", 13, 14);
        A04 = r2;
        AnonymousClass0CL r25 = r5;
        AnonymousClass0CL r24 = r6;
        AnonymousClass0CL r23 = r7;
        AnonymousClass0CL r22 = r8;
        AnonymousClass0CL r21 = r9;
        AnonymousClass0CL r20 = r10;
        AnonymousClass0CL r19 = r15;
        AnonymousClass0CL r18 = r0;
        AnonymousClass0CL r17 = r11;
        AnonymousClass0CL r16 = r12;
        AnonymousClass0CL r152 = r13;
        A01 = new AnonymousClass0CL[]{r14, r152, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r4, r2};
    }

    public static AnonymousClass0CL valueOf(String str) {
        return (AnonymousClass0CL) Enum.valueOf(AnonymousClass0CL.class, str);
    }

    public static AnonymousClass0CL[] values() {
        return (AnonymousClass0CL[]) A01.clone();
    }

    private AnonymousClass0CL(String str, int i, int i2) {
        this.mValue = i2;
    }
}
