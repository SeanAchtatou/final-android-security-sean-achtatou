package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.games.pushnotification.model.GamesPushNotificationSettings;

/* renamed from: X.13o  reason: invalid class name and case insensitive filesystem */
public final class C184613o implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new GamesPushNotificationSettings(parcel);
    }

    public Object[] newArray(int i) {
        return new GamesPushNotificationSettings[i];
    }
}
