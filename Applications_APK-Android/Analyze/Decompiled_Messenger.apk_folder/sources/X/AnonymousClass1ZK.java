package X;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.google.common.base.Preconditions;

/* renamed from: X.1ZK  reason: invalid class name */
public class AnonymousClass1ZK extends C04450Us {
    public final Context A00;
    public final String A01;

    public void C4x(Intent intent) {
        this.A00.sendBroadcast(intent, this.A01);
    }

    public AnonymousClass1ZK(Context context, String str, Handler handler) {
        super(handler);
        Preconditions.checkNotNull(context);
        this.A00 = context;
        Preconditions.checkNotNull(str);
        this.A01 = str;
    }
}
