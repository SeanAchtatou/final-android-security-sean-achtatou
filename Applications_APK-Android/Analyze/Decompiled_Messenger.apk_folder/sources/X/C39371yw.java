package X;

import com.facebook.compactdisk.current.Factory;
import com.facebook.compactdisk.current.FileCacheConfig;

/* renamed from: X.1yw  reason: invalid class name and case insensitive filesystem */
public final class C39371yw implements Factory {
    public final /* synthetic */ AIY A00;

    public C39371yw(AIY aiy) {
        this.A00 = aiy;
    }

    public Object create() {
        return new FileCacheConfig.Builder().setName(AnonymousClass80H.$const$string(AnonymousClass1Y3.A43)).setScope(this.A00.A02.A00()).setParentDirectory(this.A00.A04).setStoreInCacheDirectory(true).setVersionID("1").setMaxSize(20971520).build();
    }
}
