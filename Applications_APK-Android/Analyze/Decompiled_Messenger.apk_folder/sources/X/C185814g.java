package X;

import android.content.Context;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.14g  reason: invalid class name and case insensitive filesystem */
public final class C185814g {
    public static final String[] A06 = {TurboLoader.Locator.$const$string(7), TurboLoader.Locator.$const$string(4)};
    private static volatile C185814g A07;
    public final C12460pP A00;
    private final Context A01;
    private final C10960l9 A02;
    private final C21451Ha A03;
    private final C185914h A04;
    private final FbSharedPreferences A05;

    public static final C185814g A00(AnonymousClass1XY r9) {
        if (A07 == null) {
            synchronized (C185814g.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r9);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r9.getApplicationInjector();
                        A07 = new C185814g(applicationInjector, AnonymousClass1YA.A00(applicationInjector), C12460pP.A01(applicationInjector), C185914h.A01(applicationInjector), FbSharedPreferencesModule.A00(applicationInjector), C10960l9.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public void A01() {
        boolean z;
        if (this.A02.A0B() && !A02()) {
            C10960l9 r1 = this.A02;
            if (C10960l9.A03(r1)) {
                C30281hn edit = r1.A06.A00.edit();
                edit.BzC(C10990lD.A0W, BuildConfig.FLAVOR);
                edit.commit();
                z = true;
            } else {
                z = false;
            }
            if (z) {
                C22361La A002 = C21451Ha.A00(this.A03, "sms_takeover_clear_sms_white_list");
                if (A002.A0B()) {
                    A002.A06("call_context", "sms_permission_revoked");
                    A002.A0A();
                }
            }
            this.A04.A08(C138436dA.A0C, this.A01, false);
            C30281hn edit2 = this.A05.edit();
            edit2.C1B(C10990lD.A0I);
            edit2.commit();
        }
    }

    public boolean A02() {
        return this.A00.A09(A06);
    }

    private C185814g(AnonymousClass1XY r2, Context context, C12460pP r4, C185914h r5, FbSharedPreferences fbSharedPreferences, C10960l9 r7) {
        this.A03 = C21451Ha.A02(r2);
        this.A01 = context.getApplicationContext();
        this.A00 = r4;
        this.A04 = r5;
        this.A05 = fbSharedPreferences;
        this.A02 = r7;
    }
}
