package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1zi  reason: invalid class name and case insensitive filesystem */
public final class C39851zi {
    public final AnonymousClass09O A00;
    public final C39861zj A01;
    public final Random A02;
    public final AtomicBoolean A03 = new AtomicBoolean(false);
    private final int A04;
    private final int A05;
    private final C25401Zm A06;
    private final AtomicInteger A07;
    private final AtomicInteger A08;
    private final AtomicInteger A09;

    public static boolean A00(C39851zi r3) {
        if (r3.A05 == 0 || r3.A09.incrementAndGet() % r3.A05 != 0) {
            return false;
        }
        return true;
    }

    public Ed3 A01(int i) {
        if (this.A06.BzU(this.A04) == Integer.MAX_VALUE) {
            return null;
        }
        Ed3 ed3 = (Ed3) Ed3.A0B.A01();
        ed3.A00 = i;
        ed3.A02 = this.A04;
        ed3.A04 = this.A00.nowNanos();
        return ed3;
    }

    public void A02(Ed3 ed3) {
        int i;
        if (ed3.A06 || (i = ed3.A01) <= 1 || this.A02.nextInt(i) == 0) {
            long nowNanos = this.A00.nowNanos() - ed3.A04;
            C39861zj r3 = this.A01;
            PerformanceLoggingEvent A022 = C39861zj.A02(r3, "MARKER_START_TIME", ed3, nowNanos);
            if (!C39861zj.A04(r3)) {
                PerformanceLoggingEvent.A0c.A02(A022);
            } else {
                A022.A0C("is_sampled", ed3.A06);
                A022.A0C("is_restarted", ed3.A05);
                C39861zj.A03(r3, ed3, A022);
                r3.A01.A00(A022);
                r3.A02.set(false);
            }
            Ed3.A0B.A02(ed3);
        }
    }

    public void A03(Ed3 ed3) {
        if (ed3 != null) {
            C39861zj r2 = this.A01;
            PerformanceLoggingEvent A022 = C39861zj.A02(r2, "MARKER_ANNOTATE_TIME", ed3, this.A00.nowNanos() - ed3.A04);
            if (!C39861zj.A04(r2)) {
                PerformanceLoggingEvent.A0c.A02(A022);
            } else {
                r2.A01.A00(A022);
                r2.A02.set(false);
            }
            Ed3.A0B.A02(ed3);
        }
    }

    public void A04(Ed3 ed3) {
        if (ed3 != null) {
            C39861zj r2 = this.A01;
            PerformanceLoggingEvent A022 = C39861zj.A02(r2, "MARKER_POINT_TIME", ed3, this.A00.nowNanos() - ed3.A04);
            if (!C39861zj.A04(r2)) {
                PerformanceLoggingEvent.A0c.A02(A022);
            } else {
                C39861zj.A03(r2, ed3, A022);
                r2.A01.A00(A022);
                r2.A02.set(false);
            }
            Ed3.A0B.A02(ed3);
        }
    }

    public void A05(boolean z) {
        int i;
        int incrementAndGet = this.A07.incrementAndGet();
        if (z) {
            i = this.A08.incrementAndGet();
        } else if (incrementAndGet == 5000) {
            i = this.A08.get();
        } else {
            i = 0;
        }
        if (incrementAndGet == 5000) {
            this.A01.A06(27787269, "accuracy", (int) ((((float) i) / ((float) incrementAndGet)) * 100.0f));
        }
    }

    public C39851zi(AnonymousClass09O r6, int i, C25401Zm r8, Random random, C39861zj r10) {
        this.A00 = r6;
        this.A05 = i;
        this.A06 = r8;
        this.A02 = random;
        this.A01 = r10;
        this.A09 = new AtomicInteger((int) (Math.random() * ((double) i)));
        this.A07 = new AtomicInteger();
        this.A08 = new AtomicInteger();
        this.A04 = r8.B1q(27787270);
    }
}
