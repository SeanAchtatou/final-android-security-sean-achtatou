package X;

/* renamed from: X.0wO  reason: invalid class name and case insensitive filesystem */
public enum C16040wO {
    INBOX,
    CONTACTS,
    DISCOVER,
    FRIENDS,
    FILES_VIEW;

    public String A01() {
        return AnonymousClass08S.A0J("tab_", name());
    }

    public static String A00(C16040wO r0) {
        switch (r0.ordinal()) {
            case 1:
            case 3:
                return "contacts";
            case 2:
                return "discovery";
            case 4:
                return "files_view";
            default:
                return null;
        }
    }
}
