package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.facebook.rti.push.service.FbnsService;
import java.util.ArrayList;

/* renamed from: X.0H6  reason: invalid class name */
public final class AnonymousClass0H6 extends ArrayList<SubscribeTopic> {
    public AnonymousClass0H6() {
        add(new SubscribeTopic("/fbns_reg_resp", 1));
        add(new SubscribeTopic("/fbns_exp_logging", 1));
        addAll(FbnsService.A0A);
    }
}
