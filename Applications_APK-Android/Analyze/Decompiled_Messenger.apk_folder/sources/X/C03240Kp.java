package X;

import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.mmapbuf.MmapBufferManager;

/* renamed from: X.0Kp  reason: invalid class name and case insensitive filesystem */
public final class C03240Kp extends AnonymousClass04v {
    private final MmapBufferManager A00;

    private void A00() {
        AnonymousClass054 r0 = AnonymousClass054.A07;
        if (r0 != null) {
            long j = 0;
            long j2 = 0;
            int i = 0;
            int i2 = 0;
            for (TraceContext traceContext : r0.A08()) {
                i |= traceContext.A02;
                if (traceContext.A03 != 2) {
                    i2 = (int) traceContext.A04;
                    j = traceContext.A05;
                } else {
                    j2 = traceContext.A05;
                }
            }
            this.A00.nativeUpdateHeader(i, i2, j, j2);
        }
    }

    public C03240Kp(MmapBufferManager mmapBufferManager) {
        this.A00 = mmapBufferManager;
    }

    public void onTraceAbort(TraceContext traceContext) {
        A00();
    }

    public void onTraceStart(TraceContext traceContext) {
        A00();
    }

    public void onTraceStop(TraceContext traceContext) {
        A00();
    }
}
