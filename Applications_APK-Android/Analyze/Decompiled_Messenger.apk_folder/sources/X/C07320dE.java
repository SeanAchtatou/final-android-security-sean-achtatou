package X;

import android.content.Context;

/* renamed from: X.0dE  reason: invalid class name and case insensitive filesystem */
public final class C07320dE {
    private static String A0R;
    public C06290bG A00;
    public C06300bH A01;
    public C1936795w A02;
    public C1936795w A03;
    public C07250d5 A04;
    public C07290dB A05;
    public C07290dB A06;
    public C06830c9 A07;
    public C06320bJ A08;
    public C06320bJ A09;
    public C06840cA A0A;
    public C06260bD A0B;
    public C07300dC A0C;
    public C07300dC A0D;
    public C07140ch A0E;
    public C07130cg A0F;
    public C07340dG A0G;
    public C07330dF A0H;
    public C11970oK A0I;
    public C07660dv A0J;
    public C07310dD A0K;
    public AnonymousClass0d3 A0L;
    public Class A0M;
    public Class A0N;
    public Class A0O;
    public Class A0P;
    public final Context A0Q;

    public C07320dE(Context context) {
        if (context != null) {
            this.A0Q = context.getApplicationContext();
            if (A0R != null) {
                C010708t.A0K("Analytics2Logger.Builder", AnonymousClass08S.A0J(A0R, A00("currentStack: ")));
            }
            A0R = A00("previousStack: ");
            return;
        }
        throw new IllegalArgumentException("context");
    }

    private static String A00(String str) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        StringBuilder sb = new StringBuilder("A2 is created\n");
        for (StackTraceElement stackTraceElement : stackTrace) {
            sb.append(str);
            sb.append(stackTraceElement.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
}
