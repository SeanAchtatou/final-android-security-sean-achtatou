package X;

import com.facebook.mobileconfig.MobileConfigCrashReportUtils;
import java.util.Random;

/* renamed from: X.06M  reason: invalid class name */
public final class AnonymousClass06M extends AnonymousClass0Wl {
    private AnonymousClass0UN A00;
    private final MobileConfigCrashReportUtils A01 = MobileConfigCrashReportUtils.getInstance();

    public String getSimpleName() {
        return "AppDataBridge";
    }

    public static final AnonymousClass06M A00(AnonymousClass1XY r1) {
        return new AnonymousClass06M(r1);
    }

    public static final AnonymousClass0US A01(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.AkX, r1);
    }

    private AnonymousClass06M(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
    }

    public void init() {
        int A03 = C000700l.A03(674380065);
        int i = AnonymousClass1Y3.B5j;
        AnonymousClass0UN r3 = this.A00;
        AnonymousClass06N r4 = new AnonymousClass06N((AnonymousClass0Ud) AnonymousClass1XX.A02(0, i, r3), (C06230bA) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BTX, r3), (C09340h3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AeN, r3), (AnonymousClass1ZE) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Ap7, r3), this.A01, (AnonymousClass14M) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A86, r3), (Random) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A8J, r3));
        synchronized (AnonymousClass01P.A0Z) {
            if (AnonymousClass01P.A0Y == null) {
                C010708t.A0J("AppStateLoggerCore", "Application needs to be registered before setting app state manager bridge");
            } else {
                C001901i r1 = AnonymousClass01P.A0Y.A0B;
                synchronized (r1) {
                    r1.A08 = r4;
                }
            }
        }
        C000700l.A09(1585644279, A03);
    }
}
