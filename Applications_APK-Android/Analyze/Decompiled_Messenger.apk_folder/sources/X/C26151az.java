package X;

import android.content.Context;
import android.os.Handler;
import com.facebook.proxygen.LigerSamplePolicy;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.1az  reason: invalid class name and case insensitive filesystem */
public final class C26151az implements Runnable, C08020eY {
    public static final String __redex_internal_original_name = "com.facebook.mobileboost.apps.analytics.OptimizationLogger";
    public Context A00;
    public Handler A01;
    public C08010eX A02;
    public volatile boolean A03 = false;

    public void AOF() {
        if (this.A03) {
            AnonymousClass00S.A02(this.A01, this);
            this.A03 = false;
        }
        AnonymousClass00S.A05(this.A01, this, LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT, 953278287);
        this.A03 = true;
    }

    public void AOG() {
        if (this.A03) {
            AnonymousClass00S.A02(this.A01, this);
            this.A03 = false;
        }
    }

    public void run() {
        C08290f0 r6;
        C08010eX r0 = this.A02;
        C08010eX.A01(r0);
        for (int i : r0.A01) {
            C08010eX r1 = this.A02;
            C08040ea r02 = (C08040ea) r1.A01.get(i);
            if (r02 == null) {
                r02 = r1.A06;
            }
            for (C07210ct r4 : new ArrayList(r02.A04)) {
                if (r4.A04 == 1 && r4 != null && (r6 = r4.A01) != null && (r6 instanceof C48792b9)) {
                    C48792b9 r62 = (C48792b9) r6;
                    if (r62.A00 != 0) {
                        if (r4.A05 == null) {
                            HashMap hashMap = new HashMap();
                            hashMap.put("optimization_type", "CPU-Boost-without-parameters");
                            String A002 = C03020Ho.A00(r4.A03);
                            long j = r62.A00;
                            long j2 = j;
                            C185628jU.A01(A002, j2, r62.A02, r62.A01, this.A00, new HashMap(), hashMap);
                        }
                        HashMap hashMap2 = new HashMap();
                        hashMap2.put("cpu_level", Long.valueOf((long) r4.A05.A03));
                        hashMap2.put("gpu_level", -1L);
                        hashMap2.put("platform", Long.valueOf((long) r4.A05.A00));
                        HashMap hashMap3 = new HashMap();
                        hashMap3.put("optimization_type", "CPU-Boost");
                        C185628jU.A01(C03020Ho.A00(r4.A03), r62.A00, r62.A02, r62.A01, this.A00, hashMap2, hashMap3);
                        r62.A00 = 0;
                        r62.A01 = 0;
                        r62.A02 = 0;
                    }
                }
            }
        }
        this.A03 = false;
    }
}
