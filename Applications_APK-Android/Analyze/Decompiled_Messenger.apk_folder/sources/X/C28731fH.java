package X;

import android.database.Cursor;
import java.util.Iterator;

/* renamed from: X.1fH  reason: invalid class name and case insensitive filesystem */
public final class C28731fH implements Iterable, AutoCloseable {
    private final Cursor A00;

    public void close() {
        this.A00.close();
    }

    public Iterator iterator() {
        return new C28741fI(this.A00);
    }

    public C28731fH(Cursor cursor) {
        this.A00 = cursor;
    }
}
