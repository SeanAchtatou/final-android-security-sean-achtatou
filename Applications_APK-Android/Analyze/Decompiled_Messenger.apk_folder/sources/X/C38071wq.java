package X;

import android.net.Uri;

/* renamed from: X.1wq  reason: invalid class name and case insensitive filesystem */
public final class C38071wq implements AnonymousClass9QS, C197229Pk {
    public static int A0C = 8000;
    public static int A0D = 8000;
    public static String A0E;
    public static final ACJ A0F = new ACJ(AnonymousClass1Y3.A0t, 12);
    public int A00 = -1;
    public AnonymousClass9QQ A01;
    private C38081wr A02;
    public final C21382ABz A03;
    public final A85 A04;
    public final A9c A05;
    public final C197179Pd A06;
    public final A7z A07;
    public final String A08;
    public final boolean A09;
    public final boolean A0A;
    private final boolean A0B;

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long BvJ(X.C21297A8a r14) {
        /*
            r13 = this;
            monitor-enter(r13)
            X.1wr r2 = new X.1wr     // Catch:{ all -> 0x0135 }
            X.9Pd r0 = r13.A06     // Catch:{ all -> 0x0135 }
            java.lang.String r1 = r0.A03     // Catch:{ all -> 0x0135 }
            android.net.Uri r0 = r14.A04     // Catch:{ all -> 0x0135 }
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0135 }
            r13.A02 = r2     // Catch:{ all -> 0x0135 }
            X.ACJ r1 = X.C38071wq.A0F     // Catch:{ all -> 0x0135 }
            monitor-enter(r1)     // Catch:{ all -> 0x0135 }
            X.1wr r0 = r13.A02     // Catch:{ all -> 0x0132 }
            X.9QQ r2 = r1.A00(r0)     // Catch:{ all -> 0x0132 }
            if (r2 != 0) goto L_0x0096
            X.5L8 r0 = X.AnonymousClass5L8.A01     // Catch:{ all -> 0x0132 }
            if (r0 != 0) goto L_0x0030
            java.lang.Class<X.5L8> r3 = X.AnonymousClass5L8.class
            monitor-enter(r3)     // Catch:{ all -> 0x0132 }
            X.5L8 r0 = X.AnonymousClass5L8.A01     // Catch:{ all -> 0x002d }
            if (r0 != 0) goto L_0x002b
            X.5L8 r0 = new X.5L8     // Catch:{ all -> 0x002d }
            r0.<init>()     // Catch:{ all -> 0x002d }
            X.AnonymousClass5L8.A01 = r0     // Catch:{ all -> 0x002d }
        L_0x002b:
            monitor-exit(r3)     // Catch:{ all -> 0x002d }
            goto L_0x0030
        L_0x002d:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002d }
            throw r0     // Catch:{ all -> 0x0132 }
        L_0x0030:
            X.5L8 r4 = X.AnonymousClass5L8.A01     // Catch:{ all -> 0x0132 }
            android.net.Uri r0 = r14.A04     // Catch:{ all -> 0x0132 }
            java.util.regex.Pattern r3 = X.AnonymousClass5L8.A02     // Catch:{ all -> 0x0132 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0132 }
            java.util.regex.Matcher r7 = r3.matcher(r0)     // Catch:{ all -> 0x0132 }
            boolean r0 = r7.matches()     // Catch:{ all -> 0x0132 }
            r5 = 0
            if (r0 == 0) goto L_0x0087
            r0 = 5
            java.lang.String r0 = r7.group(r0)     // Catch:{ NumberFormatException -> 0x0087 }
            long r8 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x0087 }
            android.util.LruCache r3 = r4.A00     // Catch:{ NumberFormatException -> 0x0087 }
            java.lang.Long r0 = java.lang.Long.valueOf(r8)     // Catch:{ NumberFormatException -> 0x0087 }
            java.lang.Object r6 = r3.get(r0)     // Catch:{ NumberFormatException -> 0x0087 }
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ NumberFormatException -> 0x0087 }
            if (r6 == 0) goto L_0x0087
            r0 = 2
            java.lang.String r4 = r7.group(r0)     // Catch:{ all -> 0x0132 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0132 }
            r3.<init>()     // Catch:{ all -> 0x0132 }
            java.lang.String r0 = "$1/live-dash/ID/live-ll-"
            r3.append(r0)     // Catch:{ all -> 0x0132 }
            r3.append(r4)     // Catch:{ all -> 0x0132 }
            java.lang.String r0 = "-$3/$4-"
            r3.append(r0)     // Catch:{ all -> 0x0132 }
            r3.append(r6)     // Catch:{ all -> 0x0132 }
            java.lang.String r0 = ".m4$6"
            r3.append(r0)     // Catch:{ all -> 0x0132 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0132 }
            java.lang.String r0 = r7.replaceAll(r0)     // Catch:{ all -> 0x0132 }
            android.net.Uri r5 = android.net.Uri.parse(r0)     // Catch:{  }
        L_0x0087:
            if (r5 == 0) goto L_0x0096
            X.1wr r2 = new X.1wr     // Catch:{ all -> 0x0132 }
            X.9Pd r0 = r13.A06     // Catch:{ all -> 0x0132 }
            java.lang.String r0 = r0.A03     // Catch:{ all -> 0x0132 }
            r2.<init>(r0, r5)     // Catch:{ all -> 0x0132 }
            X.9QQ r2 = r1.A00(r2)     // Catch:{ all -> 0x0132 }
        L_0x0096:
            r5 = 1
            r6 = 0
            if (r2 != 0) goto L_0x00e8
            X.ABz r3 = r13.A03     // Catch:{ all -> 0x0132 }
            X.9Pd r0 = r13.A06     // Catch:{ all -> 0x0132 }
            java.lang.String r2 = r0.A03     // Catch:{ all -> 0x0132 }
            android.net.Uri r0 = r14.A04     // Catch:{ all -> 0x0132 }
            byte[] r4 = r3.A01(r2, r0)     // Catch:{ all -> 0x0132 }
            if (r4 != 0) goto L_0x00d7
            X.A9U r3 = new X.A9U     // Catch:{ all -> 0x0132 }
            X.9Pd r4 = r13.A06     // Catch:{ all -> 0x0132 }
            X.A85 r5 = r13.A04     // Catch:{ all -> 0x0132 }
            java.lang.String r6 = r13.A08     // Catch:{ all -> 0x0132 }
            X.A8b r0 = r14.A05     // Catch:{ all -> 0x0132 }
            boolean r2 = r0.A0E     // Catch:{ all -> 0x0132 }
            boolean r0 = r13.A0A     // Catch:{ all -> 0x0132 }
            if (r0 == 0) goto L_0x00b9
            goto L_0x00bc
        L_0x00b9:
            int r7 = X.C38071wq.A0C     // Catch:{ all -> 0x0132 }
            goto L_0x00c0
        L_0x00bc:
            if (r2 == 0) goto L_0x00b9
            r7 = 8500(0x2134, float:1.1911E-41)
        L_0x00c0:
            if (r0 == 0) goto L_0x00c7
            if (r2 == 0) goto L_0x00c7
            r8 = 8500(0x2134, float:1.1911E-41)
            goto L_0x00c9
        L_0x00c7:
            int r8 = X.C38071wq.A0D     // Catch:{ all -> 0x0132 }
        L_0x00c9:
            X.A9c r9 = r13.A05     // Catch:{ all -> 0x0132 }
            X.A7z r10 = r13.A07     // Catch:{ all -> 0x0132 }
            java.lang.String r11 = X.C38071wq.A0E     // Catch:{ all -> 0x0132 }
            boolean r12 = r13.A09     // Catch:{ all -> 0x0132 }
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x0132 }
            r13.A01 = r3     // Catch:{ all -> 0x0132 }
            goto L_0x00e6
        L_0x00d7:
            X.1wt r3 = new X.1wt     // Catch:{ all -> 0x0132 }
            int r2 = r4.length     // Catch:{ all -> 0x0132 }
            X.A9c r0 = r13.A05     // Catch:{ all -> 0x0132 }
            r3.<init>(r4, r2, r0, r6)     // Catch:{ all -> 0x0132 }
            r13.A01 = r3     // Catch:{ all -> 0x0132 }
            X.1wr r0 = r13.A02     // Catch:{ all -> 0x0132 }
            r1.A02(r0, r3)     // Catch:{ all -> 0x0132 }
        L_0x00e6:
            r5 = 0
            goto L_0x00fd
        L_0x00e8:
            boolean r0 = r2 instanceof X.C38101wt     // Catch:{ all -> 0x0132 }
            if (r0 == 0) goto L_0x00ff
            X.1wt r2 = (X.C38101wt) r2     // Catch:{ all -> 0x0132 }
            boolean r5 = r2.A03     // Catch:{ all -> 0x0132 }
            X.1wt r4 = new X.1wt     // Catch:{ all -> 0x0132 }
            byte[] r3 = r2.A04     // Catch:{ all -> 0x0132 }
            int r2 = r2.A02     // Catch:{ all -> 0x0132 }
            X.A9c r0 = r13.A05     // Catch:{ all -> 0x0132 }
            r4.<init>(r3, r2, r0, r5)     // Catch:{ all -> 0x0132 }
            r13.A01 = r4     // Catch:{ all -> 0x0132 }
        L_0x00fd:
            monitor-exit(r1)     // Catch:{ all -> 0x0132 }
            goto L_0x0126
        L_0x00ff:
            X.A9V r2 = (X.A9V) r2     // Catch:{ all -> 0x0132 }
            boolean r0 = r2.A02()     // Catch:{ all -> 0x0132 }
            if (r0 == 0) goto L_0x011a
            X.1wt r4 = new X.1wt     // Catch:{ all -> 0x0132 }
            byte[] r3 = r2.A04     // Catch:{ all -> 0x0132 }
            int r2 = r2.A01     // Catch:{ all -> 0x0132 }
            X.A9c r0 = r13.A05     // Catch:{ all -> 0x0132 }
            r4.<init>(r3, r2, r0, r5)     // Catch:{ all -> 0x0132 }
            r13.A01 = r4     // Catch:{ all -> 0x0132 }
            X.1wr r0 = r13.A02     // Catch:{ all -> 0x0132 }
            r1.A02(r0, r4)     // Catch:{ all -> 0x0132 }
            goto L_0x00fd
        L_0x011a:
            X.1wr r0 = r13.A02     // Catch:{ all -> 0x0132 }
            r1.A01(r0)     // Catch:{ all -> 0x0132 }
            X.A9c r0 = r13.A05     // Catch:{ all -> 0x0132 }
            r2.A02 = r0     // Catch:{ all -> 0x0132 }
            r13.A01 = r2     // Catch:{ all -> 0x0132 }
            goto L_0x00fd
        L_0x0126:
            X.A8a r1 = A00(r13, r14, r5)     // Catch:{ all -> 0x0135 }
            X.9QQ r0 = r13.A01     // Catch:{ all -> 0x0135 }
            long r0 = r0.BvJ(r1)     // Catch:{ all -> 0x0135 }
            monitor-exit(r13)
            return r0
        L_0x0132:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0132 }
            throw r0     // Catch:{ all -> 0x0135 }
        L_0x0135:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38071wq.BvJ(X.A8a):long");
    }

    public synchronized void close() {
        byte[] bArr;
        int i;
        AnonymousClass9QQ r0 = this.A01;
        if (r0 != null) {
            r0.close();
            AnonymousClass9QQ r7 = this.A01;
            if (r7 instanceof A9T) {
                A9T a9t = (A9T) r7;
                if (a9t.A02() && (bArr = a9t.A04) != null && bArr.length >= (i = a9t.A01)) {
                    if (i <= 0) {
                        i = a9t.A00;
                    }
                    ACJ acj = A0F;
                    synchronized (acj) {
                        acj.A02(this.A02, new C38101wt(a9t.A04, i, null, true));
                    }
                    C21382ABz aBz = this.A03;
                    C38081wr r02 = this.A02;
                    aBz.A00(r02.A01, r02.A00, a9t.A04, i);
                }
            }
        }
        this.A01 = null;
    }

    public synchronized int read(byte[] bArr, int i, int i2) {
        AnonymousClass9QQ r0 = this.A01;
        if (r0 == null) {
            return -1;
        }
        return r0.read(bArr, i, i2);
    }

    public static C21297A8a A00(C38071wq r15, C21297A8a a8a, boolean z) {
        A8Z a8z = new A8Z(r15.A06.A03, z);
        C21297A8a a8a2 = a8a;
        Uri uri = a8a2.A04;
        byte[] bArr = a8a2.A07;
        long j = a8a2.A01;
        long j2 = a8a2.A03;
        long j3 = a8a2.A02;
        String str = a8a2.A06;
        int i = a8a2.A00;
        C21298A8b a8b = a8a2.A05;
        int i2 = r15.A00;
        if (i2 < 0) {
            i2 = a8b.A01;
        }
        return new C21297A8a(uri, bArr, j, j2, j3, str, i, new C21298A8b(a8b, i2, a8z));
    }

    public static boolean A01(String str, Uri uri) {
        boolean z;
        C38081wr r0 = new C38081wr(str, uri);
        ACJ acj = A0F;
        synchronized (acj) {
            AnonymousClass9QQ A002 = acj.A00(r0);
            z = false;
            if (A002 != null) {
                z = true;
            }
        }
        return z;
    }

    public void CKe(int i) {
        if (!this.A0B) {
            synchronized (this) {
                this.A00 = i;
                AnonymousClass9QQ r1 = this.A01;
                if (r1 != null && (r1 instanceof AnonymousClass9QS)) {
                    ((AnonymousClass9QS) r1).CKe(i);
                }
            }
            return;
        }
        this.A00 = i;
        AnonymousClass9QQ r12 = this.A01;
        if (r12 != null && (r12 instanceof AnonymousClass9QS)) {
            ((AnonymousClass9QS) r12).CKe(i);
        }
    }

    public void cancel() {
        AnonymousClass9QQ r0 = this.A01;
        if (r0 != null) {
            r0.cancel();
        }
    }

    public C38071wq(C197179Pd r2, String str, C21382ABz aBz, A9c a9c, A7z a7z, A85 a85, boolean z, boolean z2, boolean z3) {
        C21430ADw.A01(a9c);
        this.A06 = r2;
        this.A08 = str;
        this.A03 = aBz;
        this.A05 = a9c;
        this.A07 = a7z;
        this.A04 = a85;
        this.A01 = null;
        this.A0A = z;
        this.A0B = z2;
        this.A09 = z3;
    }
}
