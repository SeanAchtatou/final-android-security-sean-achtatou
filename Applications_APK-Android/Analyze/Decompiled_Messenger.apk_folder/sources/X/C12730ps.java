package X;

import android.content.Context;
import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.0ps  reason: invalid class name and case insensitive filesystem */
public final class C12730ps {
    private static C05540Zi A02;
    public final Context A00;
    public final C04310Tq A01;

    public static final C12730ps A00(AnonymousClass1XY r4) {
        C12730ps r0;
        synchronized (C12730ps.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new C12730ps((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (C12730ps) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    private C12730ps(AnonymousClass1XY r2) {
        this.A01 = C10580kT.A03(r2);
        this.A00 = AnonymousClass1YA.A00(r2);
    }
}
