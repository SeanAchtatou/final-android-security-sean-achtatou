package X;

import android.content.Context;
import android.net.Uri;
import com.google.common.collect.ImmutableMap;
import java.io.File;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0PX  reason: invalid class name */
public final class AnonymousClass0PX implements C08210er {
    private static volatile AnonymousClass0PX A01;
    private AnonymousClass0UN A00;

    public String getName() {
        return "ProfiloConfig";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    public void prepareDataForWriting() {
    }

    public static final AnonymousClass0PX A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0PX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0PX(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public boolean shouldSendAsync() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aeo(281651072663842L, false);
    }

    private AnonymousClass0PX(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public Map getExtraFileFromWorkerThread(File file) {
        ImmutableMap.Builder builder = ImmutableMap.builder();
        File file2 = new File(new AnonymousClass09G((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, this.A00)).A02, "ProfiloInitFileConfig.json");
        if (file2.exists()) {
            File file3 = new File(file, file2.getName());
            AnonymousClass0q2.A04(file2, file3);
            builder.put(file2.getName(), Uri.fromFile(file3).toString());
        }
        return builder.build();
    }
}
