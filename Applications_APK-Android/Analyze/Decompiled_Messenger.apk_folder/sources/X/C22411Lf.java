package X;

import java.util.ArrayList;

/* renamed from: X.1Lf  reason: invalid class name and case insensitive filesystem */
public abstract class C22411Lf {
    public AnonymousClass0VJ A00;
    public C117635hk A01 = null;
    public C117635hk A02 = null;
    public final AnonymousClass15R A03;
    public final AnonymousClass15Q A04;
    public final C22491Lo A05;
    public final AnonymousClass1T3 A06;
    public final Object A07 = new Object();
    public final boolean A08;
    public final boolean A09;

    public void A00(int i, int i2, boolean z, long j) {
        ArrayList arrayList;
        AnonymousClass1T0 r4 = ((C22481Ln) this).A00;
        C005505z.A03("FrameRateLogger.logFrameInfo", -435756667);
        try {
            AnonymousClass1T4 r2 = r4.A0O;
            int max = Math.max(Math.round(((float) i) / ((float) i2)) - 1, 0);
            int i3 = r4.A01;
            AnonymousClass15Q r22 = r2.A00;
            int min = Math.min(max, (z ? r22.A00 : 1) * i3);
            int i4 = min;
            AnonymousClass1T0.A01(r4, r4.A0N, Math.min(max, (z ? r22.A00 : 1) * 100), i2, z);
            AnonymousClass1T0.A01(r4, r4.A0M, min, i2, z);
            AnonymousClass1T2 r1 = r4.A0H;
            synchronized (r1.A03) {
                arrayList = new ArrayList(r1.A07.size() / 2);
                for (C30442EwX ewX : r1.A07) {
                    long ordinal = (long) (1 << C859646c.A01.ordinal());
                    if ((j & ordinal) == ordinal) {
                        arrayList.add(ewX);
                    }
                }
            }
            int i5 = 0;
            if (min <= 0) {
                int i6 = z ? r4.A0G.A00 : 1;
                while (i5 < arrayList.size()) {
                    AnonymousClass1T0.A00(i6, z, (C30442EwX) arrayList.get(i5), r4.A0C);
                    i5++;
                }
            } else {
                C30442EwX ewX2 = null;
                for (int i7 = 0; i7 < arrayList.size(); i7++) {
                    C30442EwX ewX3 = (C30442EwX) arrayList.get(i7);
                    if (ewX2 != null) {
                        C859646c r12 = C859646c.A01;
                        if (r12.compareTo((Enum) r12) >= 0) {
                        }
                    }
                    ewX2 = ewX3;
                }
                if (ewX2 == null) {
                    C005505z.A03("FrameRateLogger.updateBlameMarker.notBlamed", -1316843892);
                    C005505z.A00(-875455052);
                } else {
                    AnonymousClass1T0.A00(min, z, ewX2, r4.A0A);
                    AnonymousClass15Q r23 = r4.A0G;
                    AnonymousClass15Q.A02(r23);
                    boolean z2 = false;
                    if (AnonymousClass15Q.A00(r23, min, z) >= 4) {
                        z2 = true;
                    }
                    if (z2) {
                        AnonymousClass1T0.A00(min, z, ewX2, r4.A09);
                    }
                    AnonymousClass15Q r0 = r4.A0G;
                    AnonymousClass15Q.A02(r0);
                    int i8 = min + (z ? r0.A00 : 1);
                    while (i5 < arrayList.size()) {
                        C30442EwX ewX4 = (C30442EwX) arrayList.get(i5);
                        AnonymousClass1T0.A00(i4, z, ewX4, r4.A0B);
                        AnonymousClass15Q r24 = r4.A0G;
                        AnonymousClass15Q.A02(r24);
                        boolean z3 = false;
                        if (AnonymousClass15Q.A00(r24, i4, z) >= 4) {
                            z3 = true;
                        }
                        if (z3) {
                            AnonymousClass1T0.A00(i4, z, ewX4, r4.A08);
                        }
                        AnonymousClass1T0.A00(i8, z, ewX4, r4.A0C);
                        i5++;
                    }
                }
            }
            C005505z.A00(1146126928);
        } catch (Throwable th) {
            C005505z.A00(342912312);
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x002a, code lost:
        if (r6.equals("news_feed_scroll") == false) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C22411Lf(X.AnonymousClass1T3 r3, X.AnonymousClass15Q r4, android.os.Looper r5, java.lang.String r6, boolean r7, boolean r8) {
        /*
            r2 = this;
            r2.<init>()
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r2.A07 = r0
            r0 = 0
            r2.A02 = r0
            r2.A01 = r0
            r2.A06 = r3
            r2.A04 = r4
            X.1Lo r0 = new X.1Lo
            r0.<init>(r2, r5)
            r2.A05 = r0
            int r1 = r6.hashCode()
            r0 = -1870441182(0xffffffff90835522, float:-5.180156E-29)
            if (r1 != r0) goto L_0x002c
            java.lang.String r0 = "news_feed_scroll"
            boolean r0 = r6.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x002d
        L_0x002c:
            r1 = -1
        L_0x002d:
            if (r1 == 0) goto L_0x0038
            X.15R r0 = X.AnonymousClass15R.UNKNOWN
        L_0x0031:
            r2.A03 = r0
            r2.A09 = r7
            r2.A08 = r8
            return
        L_0x0038:
            X.15R r0 = X.AnonymousClass15R.NEWSFEED
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22411Lf.<init>(X.1T3, X.15Q, android.os.Looper, java.lang.String, boolean, boolean):void");
    }
}
