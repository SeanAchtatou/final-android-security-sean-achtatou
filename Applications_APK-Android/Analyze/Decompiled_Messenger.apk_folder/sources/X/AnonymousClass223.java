package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.util.TriState;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.video.downloadmanager.db.SavedVideoDbHelper;
import com.facebook.video.engine.api.VideoDataSource;
import com.facebook.video.engine.api.VideoPlayerParams;
import com.facebook.video.heroplayer.common.MosScoreCalculation;
import com.facebook.video.heroplayer.ipc.ParcelableFormat;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.card.payment.BuildConfig;
import java.lang.ref.WeakReference;

/* renamed from: X.223  reason: invalid class name */
public final class AnonymousClass223 implements Handler.Callback {
    public int A00;
    public int A01;
    public int A02;
    public long A03 = -1;
    public long A04 = -1;
    public long A05 = -1;
    public long A06 = -1;
    public long A07 = -1;
    public long A08 = -1;
    public long A09 = -1;
    public long A0A = -1;
    public USLEBaseShape0S0000000 A0B;
    public TriState A0C = TriState.UNSET;
    public C176628Dq A0D = C176628Dq.A02;
    public AnonymousClass8SH A0E;
    public C53752lJ A0F = C53752lJ.A0w;
    public AnonymousClass8Q6 A0G = AnonymousClass8Q6.A04;
    public C54152lx A0H = C54152lx.A0P;
    public AnonymousClass8SP A0I = AnonymousClass8SP.A02;
    public ParcelableFormat A0J;
    public ParcelableFormat A0K;
    public Integer A0L = AnonymousClass07B.A00;
    public String A0M = BuildConfig.FLAVOR;
    public String A0N = null;
    public String A0O;
    public WeakReference A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    private long A0T;
    public final int A0U;
    public final Handler A0V;
    public final AnonymousClass069 A0W;
    public final AnonymousClass0US A0X;
    public final AnonymousClass8Q5 A0Y;
    public final C54102ls A0Z;
    public final C76053ks A0a;
    public final VideoPlayerParams A0b;
    public final C400420b A0c;
    public final AnonymousClass8O4 A0d;
    public final AnonymousClass8MX A0e;
    public final Runnable A0f = new AnonymousClass8SO(this);
    private final Thread A0g;
    private final boolean A0h;
    private final boolean A0i;
    private final boolean A0j;
    private final boolean A0k;
    public volatile int A0l;
    public volatile C53752lJ A0m;
    public volatile C53752lJ A0n;
    public volatile Integer A0o = AnonymousClass07B.A00;
    public volatile boolean A0p;

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0027 A[SYNTHETIC, Splitter:B:17:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0039 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A03(X.AnonymousClass223 r4) {
        /*
            r3 = 0
            boolean r0 = r4.A0j     // Catch:{ Exception -> 0x002c }
            if (r0 != 0) goto L_0x000c
            boolean r0 = r4.A0k     // Catch:{ Exception -> 0x002c }
            if (r0 != 0) goto L_0x000c
            r2 = r3
            r1 = r3
            goto L_0x002e
        L_0x000c:
            java.lang.Integer r1 = r4.A0o     // Catch:{ Exception -> 0x002c }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ Exception -> 0x002c }
            if (r1 != r0) goto L_0x001b
            java.lang.String r0 = r4.A0O     // Catch:{ Exception -> 0x002c }
            if (r0 == 0) goto L_0x0024
            android.net.Uri r2 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x002c }
            goto L_0x0025
        L_0x001b:
            com.facebook.video.engine.api.VideoPlayerParams r0 = r4.A0b     // Catch:{ Exception -> 0x002c }
            com.facebook.video.engine.api.VideoDataSource r0 = r0.A0I     // Catch:{ Exception -> 0x002c }
            if (r0 == 0) goto L_0x0024
            android.net.Uri r2 = r0.A03     // Catch:{ Exception -> 0x002c }
            goto L_0x0025
        L_0x0024:
            r2 = r3
        L_0x0025:
            if (r2 == 0) goto L_0x002d
            java.lang.String r1 = r2.getHost()     // Catch:{ Exception -> 0x002d }
            goto L_0x002e
        L_0x002c:
            r2 = r3
        L_0x002d:
            r1 = r3
        L_0x002e:
            boolean r0 = r4.A0k
            if (r0 == 0) goto L_0x0039
            if (r2 == 0) goto L_0x0038
            java.lang.String r3 = r2.toString()
        L_0x0038:
            return r3
        L_0x0039:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass223.A03(X.223):java.lang.String");
    }

    public static float A00(AnonymousClass223 r2) {
        AnonymousClass8BB r0;
        WeakReference weakReference = r2.A0P;
        if (weakReference == null) {
            r0 = null;
        } else {
            r0 = (AnonymousClass8BB) weakReference.get();
        }
        ParcelableFormat parcelableFormat = r2.A0K;
        if (parcelableFormat == null || r0 == null) {
            return -1.0f;
        }
        return MosScoreCalculation.A01(parcelableFormat.fbPlaybackResolutionMos, r0.A02(), r0.A01());
    }

    public static int A01(AnonymousClass223 r1) {
        ParcelableFormat parcelableFormat = r1.A0K;
        if (parcelableFormat != null) {
            return parcelableFormat.bitrate;
        }
        return r1.A02;
    }

    public static long A02(AnonymousClass223 r5) {
        AnonymousClass8O4 r0 = r5.A0d;
        if (r0 != null) {
            long j = r0.A06.A0H;
            if (j > 0 && j != r5.A0T) {
                r5.A0T = j;
            }
        }
        return r5.A0T;
    }

    public static void A05(AnonymousClass223 r0, String str, Object... objArr) {
        if (r0.A0i) {
            C010708t.A0P("FbHeroPlayerLogger", str, objArr);
        }
    }

    public String A06() {
        String A002;
        String str;
        TriState triState = this.A0C;
        if (triState == TriState.UNSET) {
            if (!this.A0b.A0Y) {
                return C179028Oa.A00(this.A0L);
            }
            A002 = C179028Oa.A00(this.A0L);
            str = "_groot_eligible";
        } else if (triState == TriState.YES) {
            A002 = C179028Oa.A00(this.A0L);
            str = "_only_core_plugins";
        } else {
            A002 = C179028Oa.A00(this.A0L);
            str = "_other_plugins";
        }
        return AnonymousClass08S.A0J(A002, str);
    }

    public void A08() {
        if (this.A0h && this.A0U != 0) {
            A02(this);
            this.A0V.removeMessages(1);
        }
    }

    public void A09(String str) {
        if (this.A0h && this.A0U != 0) {
            A08();
            A02(this);
            Handler handler = this.A0V;
            handler.sendMessageDelayed(handler.obtainMessage(1, str), (long) this.A0U);
        }
    }

    public boolean handleMessage(Message message) {
        AnonymousClass8BB r2;
        String A002;
        int i;
        int i2;
        Message message2 = message;
        if (message2.what == 1) {
            String str = (String) message2.obj;
            if (this.A0p || !this.A0d.isPlaying() || !this.A0d.BH6()) {
                A08();
                return true;
            }
            WeakReference weakReference = this.A0P;
            if (weakReference == null) {
                r2 = null;
            } else {
                r2 = (AnonymousClass8BB) weakReference.get();
            }
            C400420b r31 = this.A0c;
            AnonymousClass8Q6 r30 = this.A0G;
            String A012 = C80833tF.A01(this.A0o);
            int i3 = this.A01;
            VideoPlayerParams videoPlayerParams = this.A0b;
            String str2 = videoPlayerParams.A0Q;
            ArrayNode arrayNode = videoPlayerParams.A0K;
            Boolean valueOf = Boolean.valueOf(videoPlayerParams.A0g);
            int currentPosition = this.A0d.getCurrentPosition();
            C54152lx r27 = this.A0H;
            String str3 = this.A0N;
            VideoPlayerParams videoPlayerParams2 = this.A0b;
            String str4 = this.A0Z.value;
            String str5 = this.A0F.value;
            String A062 = A06();
            String str6 = this.A0I.value;
            if (r2 == null) {
                A002 = null;
            } else {
                A002 = AnonymousClass09V.A00(r2.A00);
            }
            long j = this.A0A;
            if (r2 != null) {
                i = r2.A02();
            } else {
                i = 0;
            }
            if (r2 != null) {
                i2 = r2.A01();
            } else {
                i2 = 0;
            }
            boolean z = this.A0S;
            boolean z2 = this.A0Q;
            boolean z3 = this.A0d.A0A;
            C11670nb r22 = new C11670nb(C178548Ls.A00(AnonymousClass07B.A0Z));
            r22.A0D("streaming_format", A012);
            r22.A09("available_qualities", i3);
            boolean z4 = true;
            if (i3 <= 1) {
                z4 = false;
            }
            r22.A0E("is_abr_enabled", z4);
            r22.A0D(TraceFieldType.StreamType, str4);
            r22.A0D("fbvp_session_id", str3);
            r22.A08("video_time_position", (double) (((float) currentPosition) / 1000.0f));
            C400420b r11 = r31;
            r22.A0D("video_play_reason", C400420b.A03(r11, str, str5, "logVideoHeartbeatEvent"));
            r22.A0D("player_version", A062);
            r22.A09("video_player_width", i);
            r22.A09("video_player_height", i2);
            r22.A0E("is_templated_manifest", z);
            r22.A0E("is_fbms", z2);
            r22.A0E(AnonymousClass80H.$const$string(20), z3);
            C400420b.A0F(r11, r22, str6, A002, j);
            C400420b.A07(r22, videoPlayerParams2, str2);
            C400420b.A0E(r11, r22, str2, arrayNode, valueOf.booleanValue(), r27, r30, false);
            if (!this.A0p) {
                Handler handler = this.A0V;
                handler.sendMessageDelayed(handler.obtainMessage(1, str), (long) this.A0U);
            }
        }
        return true;
    }

    public static void A04(AnonymousClass223 r2, Runnable runnable) {
        if (Thread.currentThread() == r2.A0g) {
            runnable.run();
        } else {
            AnonymousClass00S.A04(r2.A0V, runnable, -1135744410);
        }
    }

    public void A07() {
        A02(this);
        this.A0p = true;
    }

    public AnonymousClass223(C400420b r4, VideoPlayerParams videoPlayerParams, Integer num, AnonymousClass069 r7, AnonymousClass0US r8, AnonymousClass0US r9, Looper looper, AnonymousClass8O4 r11, AnonymousClass8Q5 r12, AnonymousClass0US r13, C194749Cr r14, C76053ks r15, boolean z, boolean z2, boolean z3) {
        C54102ls r2;
        C53752lJ r0 = C53752lJ.A0v;
        this.A0n = r0;
        this.A0m = r0;
        this.A0c = r4;
        this.A0b = videoPlayerParams;
        this.A0o = num;
        this.A0W = r7;
        this.A0E = new AnonymousClass8SH(r7, r8, r9);
        Handler handler = new Handler(looper, this);
        this.A0V = handler;
        this.A0g = handler.getLooper().getThread();
        this.A0U = (int) ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r12.A00)).At0(567609996544042L);
        this.A0h = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r12.A00)).Aem(2306129144235825626L);
        this.A0d = r11;
        if (!((SavedVideoDbHelper) r13.get()).A0F(videoPlayerParams.A0Q) || !((SavedVideoDbHelper) r13.get()).A0G(videoPlayerParams.A0Q)) {
            r2 = C54102ls.FROM_STREAM;
        } else {
            r2 = C54102ls.FROM_SAVED_OFFLINE_LOCAL_FILE;
        }
        VideoDataSource videoDataSource = videoPlayerParams.A0I;
        if (videoDataSource == null) {
            r2 = C54102ls.FROM_STREAM;
        } else if (r2 != C54102ls.FROM_SAVED_OFFLINE_LOCAL_FILE) {
            r2 = videoDataSource.A04;
        }
        this.A0Z = r2;
        this.A0a = r15;
        if (r15.A0I == null) {
            r15.A0I = new AnonymousClass8SN(looper, r15.A0E);
        }
        this.A0Y = r12;
        this.A0j = z;
        this.A0k = z2;
        this.A0i = z3;
        if (this.A0b.A0c && ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, r12.A00)).AbO(401, false)) {
            this.A0e = new AnonymousClass8MX(videoPlayerParams, r4, r14);
        }
        this.A0X = r9;
    }
}
