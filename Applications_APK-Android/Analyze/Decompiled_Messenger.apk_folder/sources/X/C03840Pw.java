package X;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Pw  reason: invalid class name and case insensitive filesystem */
public final class C03840Pw {
    public AnonymousClass06B A00;
    public C004405h A01 = new C004105a().Anp().B5B();
    private final Context A02;
    private final FbSharedPreferences A03;
    private final C04310Tq A04;
    private final boolean A05;

    public static synchronized void A06(C03840Pw r6, long j) {
        synchronized (r6) {
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            long seconds = timeUnit.toSeconds(r6.A00.now()) - A01(r6, timeUnit.toSeconds(r6.A00.now()));
            if (seconds >= r6.A01.B7n() || j != 0) {
                long B7n = seconds / r6.A01.B7n();
                long A002 = A00(r6);
                long B7l = r6.A01.B7l();
                Long.signum(B7n);
                A05(r6, A002 + (B7l * B7n) + j);
                A04(r6, A01(r6, TimeUnit.MILLISECONDS.toSeconds(r6.A00.now())) + (B7n * r6.A01.B7n()));
            }
        }
    }

    public static long A00(C03840Pw r3) {
        return r3.A03.At2(C005706b.A02, 0);
    }

    public static long A01(C03840Pw r1, long j) {
        return r1.A03.At2(C005706b.A00, j);
    }

    public static final C03840Pw A03(AnonymousClass1XY r5) {
        return new C03840Pw(r5, AnonymousClass1YA.A00(r5), AnonymousClass067.A02(), FbSharedPreferencesModule.A00(r5), C04750Wa.A05(r5));
    }

    public static void A04(C03840Pw r1, long j) {
        C30281hn edit = r1.A03.edit();
        edit.BzA(C005706b.A00, j);
        edit.commit();
    }

    public static void A05(C03840Pw r4, long j) {
        long min = Math.min(j, r4.A01.B7m());
        C30281hn edit = r4.A03.edit();
        edit.BzA(C005706b.A02, min);
        edit.commit();
    }

    public boolean A07() {
        NetworkInfo activeNetworkInfo;
        A06(this, 0);
        ConnectivityManager connectivityManager = (ConnectivityManager) this.A02.getSystemService("connectivity");
        if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected() || (this.A04.get() != TriState.YES && !this.A05 && A00(this) <= 0)) {
            return false;
        }
        return true;
    }

    private C03840Pw(AnonymousClass1XY r4, Context context, AnonymousClass06B r6, FbSharedPreferences fbSharedPreferences, C04310Tq r8) {
        AnonymousClass1YI A002 = AnonymousClass0WA.A00(r4);
        this.A02 = context;
        this.A00 = r6;
        this.A03 = fbSharedPreferences;
        this.A04 = r8;
        this.A05 = A002.AbO(AnonymousClass1Y3.A12, false);
    }

    public static final C03840Pw A02(AnonymousClass1XY r0) {
        return A03(r0);
    }
}
