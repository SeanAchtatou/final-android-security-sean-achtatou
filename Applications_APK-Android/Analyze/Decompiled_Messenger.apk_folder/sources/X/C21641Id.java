package X;

import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Id  reason: invalid class name and case insensitive filesystem */
public final class C21641Id extends C17770zR {
    @Comparable(type = 0)
    public float A00 = -1.0f;
    @Comparable(type = 3)
    public int A01;

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r3) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(2);
        ComponentBuilderCBuilderShape0_0S0300000.A02(componentBuilderCBuilderShape0_0S0300000, r3, 0, 0, new C21641Id());
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public C21641Id() {
        super("SolidColor");
    }
}
