package X;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0CO  reason: invalid class name */
public final class AnonymousClass0CO implements Future {
    public static final AnonymousClass0CO A01 = new AnonymousClass0CO(null);
    private final Object A00;

    public boolean cancel(boolean z) {
        return false;
    }

    public boolean isCancelled() {
        return false;
    }

    public boolean isDone() {
        return true;
    }

    public AnonymousClass0CO(Object obj) {
        this.A00 = obj;
    }

    public Object get() {
        return this.A00;
    }

    public Object get(long j, TimeUnit timeUnit) {
        return get();
    }
}
