package X;

import com.facebook.payments.shipping.model.ShippingParams;

/* renamed from: X.1zg  reason: invalid class name and case insensitive filesystem */
public final class C39831zg implements Bs5 {
    public final /* synthetic */ C24533C4p A00;

    public void BRC() {
    }

    public C39831zg(C24533C4p c4p) {
        this.A00 = c4p;
    }

    public void BgQ() {
        C24533C4p c4p = this.A00;
        c4p.A0A.A09(c4p.A0F.B2y().paymentsLoggingSessionData, "button_name", "do_not_save");
        C23904BpH bpH = c4p.A0A;
        ShippingParams shippingParams = c4p.A0F;
        bpH.A04(shippingParams.B2y().paymentsLoggingSessionData, shippingParams.B2y().paymentsFlowStep, "payflows_click");
        this.A00.A2J().finish();
    }

    public void Bgv() {
        C24533C4p c4p = this.A00;
        c4p.A0A.A09(c4p.A0F.B2y().paymentsLoggingSessionData, "button_name", "save");
        C23904BpH bpH = c4p.A0A;
        ShippingParams shippingParams = c4p.A0F;
        bpH.A04(shippingParams.B2y().paymentsLoggingSessionData, shippingParams.B2y().paymentsFlowStep, "payflows_click");
        C5E c5e = this.A00.A0C;
        if (c5e != null) {
            c5e.Bmw();
        }
    }
}
