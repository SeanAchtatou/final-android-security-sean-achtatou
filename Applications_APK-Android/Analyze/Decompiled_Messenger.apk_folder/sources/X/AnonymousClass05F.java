package X;

import com.facebook.common.dextricks.stats.ClassLoadingStats;
import com.facebook.profilo.logger.Logger;
import com.facebook.profilo.provider.systemcounters.SystemCounterThread;

/* renamed from: X.05F  reason: invalid class name */
public final class AnonymousClass05F implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.profilo.init.ClassStatsPeriodicRunnable";

    public void run() {
        ClassLoadingStats.SnapshotStats A01 = ClassLoadingStats.A00().A01();
        int i = SystemCounterThread.PROVIDER_SYSTEM_COUNTERS;
        Logger.writeStandardEntry(i, 6, 44, 0, 0, 9240583, 0, (long) A01.A00);
        Logger.writeStandardEntry(i, 6, 44, 0, 0, 9240584, 0, (long) A01.A02);
    }
}
