package X;

import android.content.Context;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.resources.impl.qt.QTStringResourcesProvider;
import com.facebook.resources.impl.qt.model.QTStringResources;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.SettableFuture;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0fw  reason: invalid class name and case insensitive filesystem */
public final class C08780fw implements C04810Wg {
    public final /* synthetic */ C05290Yj A00;
    public final /* synthetic */ C09770if A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ AtomicReference A03;
    public final /* synthetic */ AtomicReference A04;

    public C08780fw(C05290Yj r1, AtomicReference atomicReference, String str, C09770if r4, AtomicReference atomicReference2) {
        this.A00 = r1;
        this.A03 = atomicReference;
        this.A02 = str;
        this.A01 = r4;
        this.A04 = atomicReference2;
    }

    public void BYh(Throwable th) {
        String str;
        this.A03.set(null);
        C05290Yj r6 = this.A00;
        ((C12120oZ) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADC, r6.A00)).A02(this.A02, this.A01, false, th);
        C35871rx r5 = (C35871rx) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AhK, r6.A00);
        QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r5.A00);
        if (th.getMessage() != null) {
            str = th.getMessage();
        } else {
            str = "null";
        }
        quickPerformanceLogger.markerAnnotate(30474250, "error_message", str);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r5.A00)).markerEnd(30474250, 3);
        SettableFuture settableFuture = r6.A02;
        Preconditions.checkNotNull(settableFuture);
        settableFuture.set(new C08730fr(false, th));
    }

    public void Bqf(Object obj) {
        Optional absent;
        C08650fi r15 = (C08650fi) obj;
        this.A03.set(null);
        C05290Yj r4 = this.A00;
        String str = this.A02;
        C09770if r3 = this.A01;
        AtomicReference atomicReference = this.A04;
        synchronized (r4) {
            QTStringResourcesProvider qTStringResourcesProvider = (QTStringResourcesProvider) AnonymousClass1XX.A02(10, AnonymousClass1Y3.ADX, r4.A00);
            Context context = r3.A02;
            String locale = r3.A07.toString();
            String str2 = (String) qTStringResourcesProvider.A05.get();
            if (str2 == null) {
                C010708t.A0J("com.facebook.resources.impl.qt.QTStringResourcesProvider", "Failed to get logged-in user id");
                absent = Optional.absent();
            } else {
                absent = Optional.absent();
                try {
                    C08710fp r2 = qTStringResourcesProvider.A04;
                    C08710fp.A03(r2, 4456456, new C08820g0(r2, locale, str2));
                    C12750pu r22 = (C12750pu) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ALx, qTStringResourcesProvider.A00);
                    ByteBuffer wrap = ByteBuffer.wrap(AnonymousClass0q2.A08(r22.A00.A02(context, qTStringResourcesProvider.A02.A01(), locale, str2).A02));
                    C172437xH r23 = new C172437xH();
                    wrap.order(ByteOrder.LITTLE_ENDIAN);
                    r23.A00 = wrap.getInt(wrap.position()) + wrap.position();
                    r23.A01 = wrap;
                    absent = Optional.of(new QTStringResources(new AnonymousClass0lk(r23), new AnonymousClass8N6(qTStringResourcesProvider.A03, str2, qTStringResourcesProvider.A01), str2, qTStringResourcesProvider.A05));
                    qTStringResourcesProvider.A04.A00.markerEnd(4456456, 2);
                } catch (Exception e) {
                    throw new C12780px(e);
                } catch (Exception e2) {
                    C08710fp.A02(qTStringResourcesProvider.A04, 4456456, e2);
                }
            }
            if (absent.isPresent()) {
                r15 = new AnonymousClass8N9((C08650fi) absent.get(), r15);
            }
            atomicReference.set(r15);
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, ((C12120oZ) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADC, r4.A00)).A00)).A01("fbresources_loading_success"), AnonymousClass1Y3.A1K);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0D("locale", r3.A07.toString());
                uSLEBaseShape0S0000000.A0Q(str);
                uSLEBaseShape0S0000000.A0D("file_format", r3.A04.mValue);
                uSLEBaseShape0S0000000.A06();
            }
            Boolean valueOf = Boolean.valueOf(r4.A0A());
            C35871rx r6 = (C35871rx) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AhK, r4.A00);
            boolean booleanValue = valueOf.booleanValue();
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r6.A00)).markerAnnotate(30474250, "is_ready", booleanValue);
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r6.A00)).markerEnd(30474250, 2);
            if (booleanValue) {
                SettableFuture settableFuture = r4.A02;
                Preconditions.checkNotNull(settableFuture);
                settableFuture.set(new C08730fr(true, null));
            }
            C05290Yj.A03(r4);
        }
    }
}
