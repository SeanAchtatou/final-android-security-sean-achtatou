package X;

import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.threads.NotificationSetting;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.FetchThreadListResult;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.List;

/* renamed from: X.161  reason: invalid class name */
public final class AnonymousClass161 {
    public long A00;
    public long A01;
    public DataFetchDisposition A02;
    public FolderCounts A03;
    public C10950l8 A04;
    public NotificationSetting A05;
    public ThreadsCollection A06 = ThreadsCollection.A02;
    public ImmutableList A07;
    public ImmutableList A08;
    public List A09;

    public void A00(FetchThreadListResult fetchThreadListResult) {
        this.A02 = fetchThreadListResult.A02;
        this.A04 = fetchThreadListResult.A04;
        this.A06 = fetchThreadListResult.A06;
        this.A08 = fetchThreadListResult.A08;
        this.A09 = fetchThreadListResult.A09;
        this.A07 = fetchThreadListResult.A07;
        this.A03 = fetchThreadListResult.A03;
        this.A05 = fetchThreadListResult.A05;
        this.A00 = fetchThreadListResult.A00;
        this.A01 = fetchThreadListResult.A01;
    }

    public AnonymousClass161() {
        ImmutableList immutableList = RegularImmutableList.A02;
        this.A08 = immutableList;
        this.A09 = immutableList;
        this.A07 = immutableList;
        this.A03 = FolderCounts.A03;
        this.A00 = -1;
        this.A01 = -1;
    }
}
