package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1Gy  reason: invalid class name and case insensitive filesystem */
public final class C21431Gy extends AnonymousClass0UV {
    public static final AnonymousClass13Q A00(AnonymousClass1XY r0) {
        return AnonymousClass13O.A00(r0).A02();
    }

    public static final AnonymousClass13Q A01(AnonymousClass1XY r0) {
        return AnonymousClass13O.A00(r0).A01();
    }

    public static final AnonymousClass13Q A02(AnonymousClass1XY r0) {
        return AnonymousClass13O.A00(r0).A07;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0026, code lost:
        if (com.google.common.base.Objects.equal(r2.B4F(X.C25951af.A0J, "facebook.com"), "facebook.com") != false) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A03(X.AnonymousClass1XY r3) {
        /*
            com.facebook.prefs.shared.FbSharedPreferences r2 = com.facebook.prefs.shared.FbSharedPreferencesModule.A00(r3)
            X.C31901kl.A00(r3)
            boolean r0 = r2.BFQ()
            if (r0 == 0) goto L_0x0028
            X.1Y7 r0 = X.C25951af.A0J
            java.lang.String r1 = "facebook.com"
            java.lang.String r0 = r2.B4F(r0, r1)
            boolean r0 = com.google.common.base.Objects.equal(r0, r1)
            if (r0 != 0) goto L_0x0028
            X.1Y7 r0 = X.C25951af.A0J
            java.lang.String r0 = r2.B4F(r0, r1)
            boolean r1 = com.google.common.base.Objects.equal(r0, r1)
            r0 = 1
            if (r1 == 0) goto L_0x0029
        L_0x0028:
            r0 = 0
        L_0x0029:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21431Gy.A03(X.1XY):java.lang.Boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r1.A01 != X.C001400y.PUBLIC) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A04(X.AnonymousClass1XY r2) {
        /*
            java.lang.Boolean r0 = X.C184313a.A02(r2)
            X.00x r1 = X.AnonymousClass0UU.A02(r2)
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0015
            X.00y r2 = r1.A01
            X.00y r1 = X.C001400y.PUBLIC
            r0 = 0
            if (r2 == r1) goto L_0x0016
        L_0x0015:
            r0 = 1
        L_0x0016:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21431Gy.A04(X.1XY):java.lang.Boolean");
    }

    public static final String A05(AnonymousClass1XY r0) {
        return AnonymousClass13O.A00(r0).A05.A02();
    }
}
