package X;

import com.facebook.auth.userscope.UserScoped;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@UserScoped
/* renamed from: X.12S  reason: invalid class name */
public final class AnonymousClass12S {
    private static C05540Zi A02;
    public ImmutableList A00 = RegularImmutableList.A02;
    public final List A01 = new ArrayList();

    public static final AnonymousClass12S A00(AnonymousClass1XY r3) {
        AnonymousClass12S r0;
        synchronized (AnonymousClass12S.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r3)) {
                    A02.A01();
                    A02.A00 = new AnonymousClass12S();
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass12S) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public void A01(C16960y6 r4) {
        synchronized (this.A01) {
            this.A01.add(r4);
            Collections.sort(this.A01, new C32611m2());
            this.A00 = ImmutableList.copyOf((Collection) this.A01);
        }
    }

    public void A02(C16960y6 r3) {
        synchronized (this.A01) {
            this.A01.remove(r3);
            this.A00 = ImmutableList.copyOf((Collection) this.A01);
        }
    }
}
