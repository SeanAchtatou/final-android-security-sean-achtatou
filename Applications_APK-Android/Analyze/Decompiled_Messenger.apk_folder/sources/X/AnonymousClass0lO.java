package X;

import android.content.Intent;
import android.content.res.Resources;
import com.facebook.fbservice.service.BlueService;
import com.facebook.fbservice.service.BlueServiceLogic;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.quicklog.QuickPerformanceLoggerProvider;

/* renamed from: X.0lO  reason: invalid class name */
public abstract class AnonymousClass0lO extends AnonymousClass0lP implements C04500Uy {
    public AnonymousClass0UN A00;
    private QuickPerformanceLogger A01;
    private final C11560nN A02 = new C11560nN();

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0050, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0051, code lost:
        if (r2 != null) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0056 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0044 A[SYNTHETIC, Splitter:B:14:0x0044] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A0f(android.content.Intent r5, int r6, int r7) {
        /*
            r4 = this;
            com.facebook.quicklog.QuickPerformanceLogger r2 = r4.A01
            if (r2 == 0) goto L_0x000c
            r1 = 43712514(0x29b0002, float:2.2775208E-37)
            java.lang.String r0 = "do-start-command"
            r2.markerPoint(r1, r0)
        L_0x000c:
            java.lang.Class r0 = r4.getClass()
            java.lang.String r2 = r0.getSimpleName()
            r1 = -184829448(0xfffffffff4fbb9f8, float:-1.5955059E32)
            java.lang.String r0 = "FbService[%s].doStartCommand"
            X.C005505z.A05(r0, r2, r1)
            if (r5 == 0) goto L_0x003c
            java.lang.String r1 = "overridden_viewer_context"
            boolean r0 = r5.hasExtra(r1)     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x003c
            android.os.Parcelable r3 = r5.getParcelableExtra(r1)     // Catch:{ all -> 0x0057 }
            com.facebook.auth.viewercontext.ViewerContext r3 = (com.facebook.auth.viewercontext.ViewerContext) r3     // Catch:{ all -> 0x0057 }
            r2 = 0
            int r1 = X.AnonymousClass1Y3.BJQ     // Catch:{ all -> 0x0057 }
            X.0UN r0 = r4.A00     // Catch:{ all -> 0x0057 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0057 }
            X.0aY r0 = (X.C05920aY) r0     // Catch:{ all -> 0x0057 }
            X.0il r2 = r0.Byt(r3)     // Catch:{ all -> 0x0057 }
            goto L_0x003e
        L_0x003c:
            X.0il r2 = X.C09830il.A00     // Catch:{ all -> 0x0057 }
        L_0x003e:
            int r1 = r4.A0j(r5, r6, r7)     // Catch:{ all -> 0x004e }
            if (r2 == 0) goto L_0x0047
            r2.close()     // Catch:{ all -> 0x0057 }
        L_0x0047:
            r0 = 1480735786(0x58423c2a, float:8.5425463E14)
            X.C005505z.A00(r0)
            return r1
        L_0x004e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0050 }
        L_0x0050:
            r0 = move-exception
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch:{ all -> 0x0056 }
        L_0x0056:
            throw r0     // Catch:{ all -> 0x0057 }
        L_0x0057:
            r1 = move-exception
            r0 = 949757169(0x389c24f1, float:7.4455405E-5)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0lO.A0f(android.content.Intent, int, int):int");
    }

    public int A0j(Intent intent, int i, int i2) {
        if (!(this instanceof BlueService)) {
            int A04 = C000700l.A04(-187472485);
            int A0f = super.A0f(intent, i, i2);
            C000700l.A0A(1735465075, A04);
            return A0f;
        }
        BlueService blueService = (BlueService) this;
        int A042 = C000700l.A04(331459390);
        if (intent == null) {
            C000700l.A0A(-1467436766, A042);
            return 2;
        }
        synchronized (blueService) {
            try {
                if ("Orca.DRAIN".equals(intent.getAction())) {
                    ((BlueServiceLogic) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AVT, blueService.A00)).A02();
                }
            } catch (Throwable th) {
                while (true) {
                    C000700l.A0A(1109176657, A042);
                    throw th;
                }
            }
        }
        C000700l.A0A(-1061573329, A042);
        return 2;
    }

    public Object Aze(Object obj) {
        return this.A02.A00(obj);
    }

    public void CB0(Object obj, Object obj2) {
        this.A02.A01(obj, obj2);
    }

    public final void A0g() {
        QuickPerformanceLogger qPLInstance = QuickPerformanceLoggerProvider.getQPLInstance();
        this.A01 = qPLInstance;
        if (qPLInstance != null) {
            qPLInstance.markerStart(43712514, "class-name", getClass().getSimpleName());
            this.A01.markerPoint(43712514, "do-create");
        }
        C005505z.A05("FbService[%s].doCreate", getClass().getSimpleName(), 458237556);
        try {
            this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(this));
            A0k();
        } finally {
            C005505z.A00(1500442934);
        }
    }

    public final void A0h() {
        A0l();
        QuickPerformanceLogger quickPerformanceLogger = this.A01;
        if (quickPerformanceLogger != null) {
            quickPerformanceLogger.markerEnd(43712514, 2);
        }
    }

    public void A0k() {
        int A04 = C000700l.A04(1183151674);
        super.A0g();
        C000700l.A0A(-1357109961, A04);
    }

    public void A0l() {
        int A04 = C000700l.A04(-237846158);
        super.A0h();
        C000700l.A0A(1185030550, A04);
    }

    public Resources getResources() {
        return getApplicationContext().getResources();
    }
}
