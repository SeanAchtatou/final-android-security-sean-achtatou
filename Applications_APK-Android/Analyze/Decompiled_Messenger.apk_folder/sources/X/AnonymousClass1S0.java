package X;

import android.content.Context;
import android.text.format.DateFormat;
import com.facebook.acra.ErrorReporter;
import com.facebook.inject.ContextScoped;
import io.card.payment.BuildConfig;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@ContextScoped
/* renamed from: X.1S0  reason: invalid class name */
public final class AnonymousClass1S0 {
    private static C04470Uu A03;
    public final Context A00;
    public final AnonymousClass1HK A01;
    public final AnonymousClass06B A02 = AnonymousClass067.A02();

    public String A06(long j) {
        String[] strArr = new String[2];
        long now = (this.A02.now() - j) / 86400000;
        Date date = new Date(j);
        if (now < 1) {
            strArr[0] = BuildConfig.FLAVOR;
        } else if (now < 180) {
            strArr[0] = this.A01.A0B().format(date);
        } else {
            AnonymousClass1S7 r3 = this.A01.A00;
            SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A0B.get();
            if (simpleDateFormat == null) {
                simpleDateFormat = (SimpleDateFormat) r3.A01().clone();
                AnonymousClass1S7.A00(simpleDateFormat, "EEEE, MMMM d, yyyy", r3.A0C);
                r3.A0B.set(simpleDateFormat);
            }
            strArr[0] = simpleDateFormat.format(date);
        }
        String format = DateFormat.getTimeFormat(this.A00).format(date);
        strArr[1] = format;
        StringBuilder sb = new StringBuilder(100);
        String str = strArr[0];
        if (!str.isEmpty()) {
            sb.append(str);
            sb.append(", ");
        }
        sb.append(format);
        return sb.toString();
    }

    public static final AnonymousClass1S0 A00(AnonymousClass1XY r4) {
        AnonymousClass1S0 r0;
        synchronized (AnonymousClass1S0.class) {
            C04470Uu A002 = C04470Uu.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new AnonymousClass1S0((AnonymousClass1XY) A03.A01());
                }
                C04470Uu r1 = A03;
                r0 = (AnonymousClass1S0) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static String A01(AnonymousClass1S0 r7, long j, boolean z) {
        Context context;
        int i;
        Object[] objArr;
        SimpleDateFormat A06;
        SimpleDateFormat A032;
        Date date = new Date(j);
        if (A03(j)) {
            return DateFormat.getTimeFormat(r7.A00).format(date);
        }
        int now = ((int) ((((r7.A02.now() - j) / 1000) / 60) / 60)) / 24;
        if (now < 4) {
            Context context2 = r7.A00;
            if (z) {
                A032 = r7.A01.A09();
            } else {
                A032 = r7.A01.A03();
            }
            return context2.getString(2131823620, A032.format(date), DateFormat.getTimeFormat(r7.A00).format(date));
        }
        if (now < 180) {
            context = r7.A00;
            i = 2131823620;
            objArr = new Object[2];
            A06 = r7.A01.A04();
        } else {
            context = r7.A00;
            i = 2131823620;
            objArr = new Object[2];
            A06 = r7.A01.A06();
        }
        objArr[0] = A06.format(date);
        objArr[1] = DateFormat.getTimeFormat(r7.A00).format(date);
        return context.getString(i, objArr);
    }

    public String A04(long j) {
        SimpleDateFormat A06;
        int now = ((int) ((((this.A02.now() - j) / 1000) / 60) / 60)) / 24;
        Date date = new Date(j);
        if (now < 180) {
            A06 = this.A01.A04();
        } else {
            A06 = this.A01.A06();
        }
        return AnonymousClass08S.A0P(A06.format(date), " ", DateFormat.getTimeFormat(this.A00).format(date));
    }

    public String A05(long j) {
        String format;
        String format2;
        String format3;
        int now = (int) ((((this.A02.now() - j) / 1000) / 60) / 60);
        if (now < 24) {
            return DateFormat.getTimeFormat(this.A00).format(Long.valueOf(j));
        }
        Date date = new Date(j);
        int i = now / 24;
        if (i < 4) {
            SimpleDateFormat A09 = this.A01.A09();
            synchronized (A09) {
                try {
                    format3 = A09.format(date);
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
            return format3;
        } else if (i < 180) {
            SimpleDateFormat A04 = this.A01.A04();
            synchronized (A04) {
                try {
                    format2 = A04.format(date);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
            return format2;
        } else {
            SimpleDateFormat A06 = this.A01.A06();
            synchronized (A06) {
                try {
                    format = A06.format(date);
                } catch (Throwable th3) {
                    th = th3;
                    throw th;
                }
            }
            return format;
        }
    }

    public boolean A07(long j) {
        long now = this.A02.now();
        if (j > now || now - j > ErrorReporter.MAX_REPORT_AGE) {
            return false;
        }
        return true;
    }

    private AnonymousClass1S0(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1YA.A00(r2);
        this.A01 = AnonymousClass1HK.A00(r2);
    }

    public static boolean A02(long j) {
        Calendar instance = GregorianCalendar.getInstance();
        instance.setTimeInMillis(j);
        Calendar instance2 = GregorianCalendar.getInstance();
        if (instance.get(1) == instance2.get(1) && instance.get(2) == instance2.get(2)) {
            return true;
        }
        return false;
    }

    public static boolean A03(long j) {
        Calendar instance = GregorianCalendar.getInstance();
        instance.setTimeInMillis(j);
        Calendar instance2 = GregorianCalendar.getInstance();
        if (instance.get(1) == instance2.get(1) && instance.get(2) == instance2.get(2) && instance.get(5) == instance2.get(5)) {
            return true;
        }
        return false;
    }
}
