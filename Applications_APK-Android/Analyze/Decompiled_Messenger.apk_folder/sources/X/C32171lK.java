package X;

import com.facebook.omnistore.CollectionName;

/* renamed from: X.1lK  reason: invalid class name and case insensitive filesystem */
public final class C32171lK {
    public static final C32171lK A03 = new C32171lK(AnonymousClass07B.A00, null, null);
    public final CollectionName A00;
    public final C32161lJ A01;
    public final Integer A02;

    public static C32171lK A00(CollectionName collectionName, C32161lJ r3) {
        Integer num = AnonymousClass07B.A0C;
        AnonymousClass064.A00(collectionName);
        return new C32171lK(num, collectionName, r3);
    }

    private C32171lK(Integer num, CollectionName collectionName, C32161lJ r3) {
        this.A02 = num;
        this.A00 = collectionName;
        this.A01 = r3;
    }
}
