package X;

import android.content.Intent;
import android.net.Uri;
import com.facebook.content.SecureContextHelper;
import com.facebook.messaging.business.share.util.PlatformShareExtras;
import com.facebook.user.model.User;

/* renamed from: X.1zn  reason: invalid class name and case insensitive filesystem */
public final class C39901zn implements C45922Ob {
    public final /* synthetic */ C135496Vg A00;
    public final /* synthetic */ User A01;

    public C39901zn(C135496Vg r1, User user) {
        this.A00 = r1;
        this.A01 = user;
    }

    public boolean Bc1() {
        User user = this.A01;
        if (user == null || !user.A0F()) {
            return false;
        }
        C93354cZ r4 = this.A00.A03;
        String str = user.A0j;
        Intent intent = new Intent(C60982y9.A03);
        intent.setData(Uri.parse(C52652jT.A0W));
        intent.putExtra("ShareType", "ShareType.platformItem");
        C39931zq r0 = new C39931zq();
        r0.A02 = str;
        intent.putExtra(C22298Ase.$const$string(4), new PlatformShareExtras(r0));
        ((SecureContextHelper) r4.A01.get()).startFacebookActivity(intent, r4.A00);
        return true;
    }
}
