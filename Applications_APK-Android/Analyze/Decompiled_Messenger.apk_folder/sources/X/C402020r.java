package X;

/* renamed from: X.20r  reason: invalid class name and case insensitive filesystem */
public final class C402020r {
    public C13460rT A00;
    public AnonymousClass2Wj A01;
    public AnonymousClass2XE A02;
    public C47652Wu A03;
    public String A04;
    public String A05;
    public String A06;
    public boolean A07;

    public C402120s A00() {
        C13460rT r3 = this.A00;
        if (r3 != null) {
            if (this.A04 == null) {
                this.A04 = this.A05;
            }
            C402120s r2 = new C402120s(r3, this.A05, this.A04, this.A06, this.A02, this.A03, this.A01, this.A07);
            r2.A07.A02 = new C43862Fz(r2);
            return r2;
        }
        throw new IllegalArgumentException("Host is a mandatory param.");
    }
}
