package X;

import android.content.Context;
import com.facebook.flexiblesampling.SamplingPolicyConfig;

/* renamed from: X.0bT  reason: invalid class name and case insensitive filesystem */
public final class C06420bT implements C06430bU {
    public final Context A00;
    public final Class A01;

    public void CLP(AnonymousClass23I r1) {
    }

    public void onBackground() {
    }

    public void onForeground() {
    }

    public void CLQ() {
        SamplingPolicyConfig samplingPolicyConfig;
        String name = this.A01.getName();
        Context context = this.A00;
        if (name != null) {
            AnonymousClass15C A002 = AnonymousClass15C.A00(context);
            samplingPolicyConfig = (SamplingPolicyConfig) AnonymousClass15C.A01(A002, A002.A00, name);
        } else {
            samplingPolicyConfig = null;
        }
        if (samplingPolicyConfig != null) {
            samplingPolicyConfig.BDO();
        }
    }

    public C06420bT(Class cls, Context context) {
        AnonymousClass064.A01(cls, "Sampling policy config should not be null");
        this.A01 = cls;
        this.A00 = context;
    }
}
