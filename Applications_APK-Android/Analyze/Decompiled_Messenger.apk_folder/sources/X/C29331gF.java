package X;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1gF  reason: invalid class name and case insensitive filesystem */
public final class C29331gF implements C29341gG {
    private static volatile C29331gF A04;
    public int A00 = 0;
    public ScheduledFuture A01;
    public final ScheduledExecutorService A02;
    public final AtomicBoolean A03 = new AtomicBoolean(false);

    public static final C29331gF A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C29331gF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C29331gF(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public boolean BDY() {
        return this.A03.get();
    }

    private C29331gF(AnonymousClass1XY r3) {
        this.A02 = AnonymousClass0UX.A0e(r3);
    }
}
