package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.proxygen.TraceFieldType;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@UserScoped
/* renamed from: X.1eD  reason: invalid class name and case insensitive filesystem */
public final class C28071eD extends AnonymousClass0j5 {
    private static C05540Zi A02;
    private static final AnonymousClass0jA A03;
    private static final ImmutableMap A04;
    private static final ImmutableMap A05;
    private static final String A06 = A02(A04.values());
    private static final String A07 = A02(A05.values());
    private AnonymousClass06B A00;
    public final C04310Tq A01;

    public Cursor A08(SQLiteDatabase sQLiteDatabase, String[] strArr, String str, String[] strArr2, String str2) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables(A01(this, strArr, str, str2));
        return sQLiteQueryBuilder.query(sQLiteDatabase, strArr, str, strArr2, null, null, str2, null);
    }

    static {
        ImmutableMap.Builder builder = new ImmutableMap.Builder();
        builder.put("latest_montage_preview_message_id", TraceFieldType.MsgId);
        builder.put("latest_montage_preview_attachments", "attachments");
        builder.put("latest_montage_preview_pending_send_attachment", "pending_send_media_attachment");
        builder.put("latest_montage_preview_sticker_id", "sticker_id");
        builder.put("latest_montage_preview_text", "text");
        builder.put("latest_montage_message_timestamp_ms", "timestamp_ms");
        builder.put("latest_montage_message_sender", "sender");
        builder.put("latest_montage_preview_message_type", TraceFieldType.MsgType);
        A05 = builder.build();
        ImmutableMap.Builder builder2 = new ImmutableMap.Builder();
        builder2.put("inbox_to_montage_preview_message_id", TraceFieldType.MsgId);
        builder2.put("inbox_to_montage_preview_attachments", "attachments");
        builder2.put("inbox_to_montage_preview_sticker_id", "sticker_id");
        builder2.put("inbox_to_montage_preview_text", "text");
        builder2.put("inbox_to_montage_latest_message_timestamp_ms", "timestamp_ms");
        builder2.put("inbox_to_montage_sender", "sender");
        A04 = builder2.build();
        C09560i1 r2 = new C09560i1();
        r2.A00("_id", "threads", "_id");
        r2.A00("thread_key", "threads", "thread_key");
        r2.A00("legacy_thread_id", "threads", "legacy_thread_id");
        r2.A00("sequence_id", "threads", "sequence_id");
        r2.A00("name", "threads", "name");
        r2.A00("senders", "threads", "senders");
        r2.A00("snippet", "threads", "snippet");
        r2.A00("snippet_sender", "threads", "snippet_sender");
        r2.A00("admin_snippet", "threads", "admin_snippet");
        r2.A00("timestamp_ms", "threads", "timestamp_ms");
        r2.A00("last_read_timestamp_ms", "threads", "last_read_timestamp_ms");
        r2.A00("approx_total_message_count", "threads", "approx_total_message_count");
        r2.A00("unread_message_count", "threads", "unread_message_count");
        r2.A00("pic_hash", "threads", "pic_hash");
        r2.A00("can_reply_to", "threads", "can_reply_to");
        r2.A00("cannot_reply_reason", "threads", "cannot_reply_reason");
        r2.A00("last_message_admin_text_type", "threads", "last_message_admin_text_type");
        r2.A00("job_application_time", "threads", "job_application_time");
        r2.A00("pic", "threads", "pic");
        r2.A00("is_subscribed", "threads", "is_subscribed");
        r2.A00("folder", "threads", "folder");
        r2.A00("draft", "threads", "draft");
        r2.A00("last_fetch_time_ms", "threads", "last_fetch_time_ms");
        r2.A00("missed_call_status", "threads", "missed_call_status");
        r2.A00("mute_until", "threads", "mute_until");
        r2.A00("timestamp_in_folder_ms", "folders", "timestamp_ms");
        r2.A00("group_chat_rank", "threads", "group_chat_rank");
        r2.A00("initial_fetch_complete", "threads", "initial_fetch_complete");
        r2.A00("custom_like_emoji", "threads", "custom_like_emoji");
        r2.A00("outgoing_message_lifetime", "threads", "outgoing_message_lifetime");
        r2.A00("custom_nicknames", "threads", "custom_nicknames");
        r2.A00("invite_uri", "threads", "invite_uri");
        r2.A00("last_message_id_if_sponsored", "threads", "last_message_id_if_sponsored");
        r2.A00("is_joinable", "threads", "is_joinable");
        r2.A00("requires_approval", "threads", "requires_approval");
        r2.A00("rtc_call_info", "threads", "rtc_call_info");
        r2.A00("last_message_commerce_message_type", "threads", "last_message_commerce_message_type");
        r2.A00("is_thread_queue_enabled", "threads", "is_thread_queue_enabled");
        r2.A00("group_description", "threads", "group_description");
        r2.A00("media_preview", "threads", "media_preview");
        r2.A00("booking_requests", "threads", "booking_requests");
        r2.A00("last_call_ms", "threads", "last_call_ms");
        r2.A00("is_discoverable", "threads", "is_discoverable");
        r2.A00("last_sponsored_message_call_to_action", "threads", "last_sponsored_message_call_to_action");
        r2.A00("montage_thread_key", "threads", "montage_thread_key");
        r2.A00("inbox_to_montage_thread_read_watermark_timestamp_ms", "inbox_to_montage_threads", "last_read_timestamp_ms");
        r2.A00("room_privacy_mode", "threads", "room_privacy_mode");
        r2.A00("room_associated_fb_group_id", "threads", "room_associated_fb_group_id");
        r2.A00("room_associated_fb_group_name", "threads", "room_associated_fb_group_name");
        r2.A00("room_associated_photo_uri", "threads", "room_associated_photo_uri");
        r2.A00("group_associated_fb_group_visibility", "threads", "group_associated_fb_group_visibility");
        r2.A00("has_work_multi_company_associated_group", "threads", "has_work_multi_company_associated_group");
        r2.A00("approval_toggleable", "threads", "approval_toggleable");
        r2.A00("video_room_mode", "threads", "video_room_mode");
        r2.A00("marketplace_data", "threads", "marketplace_data");
        r2.A00("room_creation_time", "threads", "room_creation_time");
        r2.A00("group_thread_category", "threads", "group_thread_category");
        r2.A00("are_admins_supported", "threads", "are_admins_supported");
        r2.A00("group_thread_add_mode", "threads", "group_thread_add_mode");
        r2.A00("group_thread_offline_threading_id", "threads", "group_thread_offline_threading_id");
        r2.A00("personal_group_invite_link", "threads", "personal_group_invite_link");
        r2.A00("optimistic_group_state", "threads", "optimistic_group_state");
        r2.A00("ad_context_data", "threads", "ad_context_data");
        r2.A00("use_existing_group", "threads", "use_existing_group");
        r2.A00("thread_associated_object_type", "threads", "thread_associated_object_type");
        r2.A00("thread_streak_data", "threads", "thread_streak_data");
        r2.A00("games_push_notification_settings", "threads", "games_push_notification_settings");
        r2.A00("can_participants_claim_admin", "threads", "can_participants_claim_admin");
        r2.A00("group_approval_mode", "threads", "group_approval_mode");
        r2.A00("unopened_montage_directs", "threads", "unopened_montage_directs");
        r2.A00("synced_fb_group_id", "threads", "synced_fb_group_id");
        r2.A00("synced_fb_group_status", "threads", "synced_fb_group_status");
        r2.A00("synced_fb_group_is_work_multi_company_group", "threads", "synced_fb_group_is_work_multi_company_group");
        r2.A00("video_chat_link", "threads", "video_chat_link");
        r2.A00("theme_id", "threads", "theme_id");
        r2.A00("theme_fallback_color", "threads", "theme_fallback_color");
        r2.A00("theme_gradient_colors", "threads", "theme_gradient_colors");
        r2.A00("theme_accessibility_label", "threads", "theme_accessibility_label");
        r2.A00("is_fuss_red_page", "threads", "is_fuss_red_page");
        r2.A00("is_thread_pinned", "threads", "is_thread_pinned");
        r2.A00("thread_pin_timestamp", "threads", "thread_pin_timestamp");
        r2.A00("animated_thread_activity_banner", "threads", "animated_thread_activity_banner");
        r2.A00("last_message_breadcrumb_type", "threads", "last_message_breadcrumb_type");
        r2.A00("last_message_breadcrumb_cta", "threads", "last_message_breadcrumb_cta");
        r2.A00("thread_connectivity_data", "threads", "thread_connectivity_data");
        r2.A00("unsendability_status", "threads", "unsendability_status");
        r2.A00("work_sync_group_data", "threads", "work_sync_group_data");
        r2.A00("group_thread_subtype", "threads", "group_thread_subtype");
        r2.A00("is_page_follow_up", "threads", "is_page_follow_up");
        r2.A00("last_message_id", "threads", "last_message_id");
        r2.A00("ads_qp_update_data", "threads", "ads_qp_update_data");
        r2.A00("last_message_timestamp_ms", "threads", "last_message_timestamp_ms");
        r2.A00("messenger_request_appointment_data", "threads", "messenger_request_appointment_data");
        r2.A00("related_page_thread_data", "threads", "related_page_thread_data");
        r2.A00("thread_message_assigned_page_admin", "threads", "thread_message_assigned_page_admin");
        r2.A00("has_non_admin_message", "threads", "has_non_admin_message");
        A03(r2, A05.entrySet(), "montage_latest_messages");
        A03(r2, A04.entrySet(), "inbox_to_montage_messages");
        A03 = new AnonymousClass0jA(r2.A00.build());
    }

    public static final C28071eD A00(AnonymousClass1XY r5) {
        C28071eD r0;
        synchronized (C28071eD.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r5)) {
                    A02.A00 = new C28071eD(C25771aN.A02((AnonymousClass1XY) A02.A01()), AnonymousClass067.A02());
                }
                C05540Zi r1 = A02;
                r0 = (C28071eD) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    private static String A02(ImmutableCollection immutableCollection) {
        StringBuilder sb = new StringBuilder();
        C24971Xv it = immutableCollection.iterator();
        while (it.hasNext()) {
            sb.append((String) it.next());
            if (it.hasNext()) {
                sb.append(',');
            }
        }
        return sb.toString();
    }

    private C28071eD(C04310Tq r1, AnonymousClass06B r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public static String A01(C28071eD r21, String[] strArr, String str, String str2) {
        String str3;
        String str4;
        C07410dQ A012 = ImmutableSet.A01();
        A012.A03(strArr);
        String nullToEmpty = Strings.nullToEmpty(str);
        String nullToEmpty2 = Strings.nullToEmpty(str2);
        C24971Xv it = A03.A00.keySet().iterator();
        while (it.hasNext()) {
            String str5 = (String) it.next();
            if (nullToEmpty.contains(str5) || nullToEmpty2.contains(str5)) {
                A012.A01(str5);
            }
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb.append("t._ROWID_ AS _id");
        sb2.append("threads AS t");
        C24971Xv it2 = A012.build().iterator();
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        while (it2.hasNext()) {
            String str6 = (String) it2.next();
            if (!"_id".equals(str6)) {
                C09550i0 r4 = (C09550i0) A03.A00.get(str6);
                if (r4 != null) {
                    String str7 = r4.A02;
                    if ("threads".equals(str7)) {
                        str3 = ", t.";
                    } else if ("folders".equals(str7)) {
                        if (!z) {
                            sb2.append(" INNER JOIN folders AS f ON t.thread_key = f.thread_key");
                            z = true;
                        }
                        str3 = ", f.";
                    } else if ("group_conversations".equals(str7)) {
                        if (!z2) {
                            sb2.append(" INNER JOIN group_conversations AS g ON t.thread_key = g.thread_key");
                            z2 = true;
                        }
                        str3 = ", g.";
                    } else {
                        str3 = ".";
                        C28071eD r10 = r21;
                        if ("inbox_to_montage_messages".equals(str7) || "inbox_to_montage_threads".equals(str7)) {
                            if (!z3) {
                                sb2.append(" LEFT JOIN ");
                                sb2.append("threads");
                                sb2.append(" AS mt ON mt.thread_key = t.");
                                sb2.append("montage_thread_key");
                                sb2.append(" AND mt.folder='" + C10950l8.A06 + "'");
                                sb2.append(" LEFT JOIN");
                                sb2.append(" (SELECT ");
                                sb2.append(A06);
                                sb2.append(",thread_key, max(timestamp_ms) FROM ");
                                sb2.append("messages");
                                sb2.append(" WHERE msg_type=");
                                sb2.append(AnonymousClass1V7.A0K.dbKeyValue);
                                sb2.append(" AND timestamp_ms > ");
                                sb2.append(r10.A00.now() - 86400000);
                                sb2.append(" AND (montage_story_type IS NULL OR montage_story_type NOT IN('group', 'event')) ");
                                sb2.append(" GROUP BY thread_key)");
                                sb2.append(" AS mm ON mm.thread_key = mt.thread_key");
                                z3 = true;
                            }
                            if ("inbox_to_montage_messages".equals(r4.A02)) {
                                str4 = ", mm";
                            } else {
                                str4 = ", mt";
                            }
                            sb.append(str4);
                        } else if ("montage_latest_messages".equals(str7)) {
                            if (!z4) {
                                sb2.append(" LEFT JOIN (SELECT ");
                                sb2.append(A07);
                                sb2.append(",thread_key, max(timestamp_ms) FROM ");
                                sb2.append("messages");
                                sb2.append(" WHERE msg_type IN(");
                                sb2.append(AnonymousClass1V7.A0K.dbKeyValue);
                                sb2.append(",");
                                sb2.append(AnonymousClass1V7.A0J.dbKeyValue);
                                sb2.append(",");
                                sb2.append(AnonymousClass1V7.A0A.dbKeyValue);
                                sb2.append(") AND timestamp_ms > ");
                                sb2.append(r10.A00.now() - 86400000);
                                sb2.append(" AND (montage_story_type IS NULL OR montage_story_type NOT IN('group', 'event')) ");
                                sb2.append(" GROUP BY thread_key)");
                                sb2.append(" AS ");
                                sb2.append("latest");
                                sb2.append(" ON ");
                                sb2.append("latest");
                                sb2.append(".thread_key = t.thread_key");
                                z4 = true;
                            }
                            sb.append(", ");
                            sb.append("latest");
                            sb.append(str3);
                            sb.append(r4.A01);
                            sb.append(" AS ");
                            sb.append(r4.A00);
                        }
                    }
                    sb.append(AnonymousClass08S.A0S(str3, r4.A01, " AS ", r4.A00));
                } else {
                    throw new IllegalArgumentException(AnonymousClass08S.A0J("Unknown field: ", str6));
                }
            }
        }
        return AnonymousClass08S.A0T("(SELECT ", sb.toString(), " FROM ", sb2.toString(), ")");
    }

    private static void A03(C09560i1 r3, Set set, String str) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            r3.A00((String) entry.getKey(), str, (String) entry.getValue());
        }
    }
}
