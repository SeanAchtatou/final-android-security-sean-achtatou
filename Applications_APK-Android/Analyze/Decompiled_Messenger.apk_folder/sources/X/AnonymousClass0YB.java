package X;

import android.os.Process;

/* renamed from: X.0YB  reason: invalid class name */
public final class AnonymousClass0YB implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.executors.NamedThreadFactory$1";
    public final /* synthetic */ C04570Vg A00;
    public final /* synthetic */ Runnable A01;

    public AnonymousClass0YB(C04570Vg r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    public void run() {
        AnonymousClass05x.A00();
        try {
            Process.setThreadPriority(this.A00.A00);
        } catch (Throwable unused) {
        }
        this.A01.run();
    }
}
