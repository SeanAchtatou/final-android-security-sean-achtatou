package X;

import android.content.ContentResolver;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.netchecker.NetChecker;
import com.facebook.proxygen.LigerSamplePolicy;
import com.google.common.base.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1o2  reason: invalid class name and case insensitive filesystem */
public final class C33721o2 implements C33701o0 {
    private static volatile C33721o2 A05;
    public AnonymousClass0UN A00;
    public C33731o3 A01 = C33731o3.CONNECTED;
    public ScheduledFuture A02;
    private Optional A03 = Optional.absent();
    private final C11330mk A04;

    public static final C33721o2 A01(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C33721o2.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C33721o2(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static void A02(C33721o2 r5, C33731o3 r6) {
        String str;
        r5.A03 = Optional.of(r5.A01);
        r5.A01 = r6;
        ((C04460Ut) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AKb, r5.A00)).C4x(new Intent("com.facebook.orca.CONNECTIVITY_CHANGED"));
        String name = r5.A01.name();
        Optional optional = r5.A03;
        if (optional.isPresent()) {
            str = ((C33731o3) optional.get()).name();
        } else {
            str = "UNKNOWN";
        }
        r5.A04.BIp("connection_status_monitor", AnonymousClass08S.A0S("send connectivity_changed broadcast; current_state=", name, "; previous_state=", str));
    }

    public static void A03(C33721o2 r5, C33731o3 r6) {
        ScheduledFuture scheduledFuture = r5.A02;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
        }
        if (r5.A01 == C33731o3.CONNECTED) {
            r5.A02 = ((ScheduledExecutorService) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Ae9, r5.A00)).schedule(new EWV(r5, r6), (long) LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT, TimeUnit.MILLISECONDS);
            return;
        }
        A02(r5, r6);
    }

    public C33731o3 AiE() {
        if (this.A01 == C33731o3.CONNECTING && ((NetChecker) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Adl, this.A00)).A0B == C57162rX.CAPTIVE_PORTAL) {
            return C33731o3.CONNECTED_CAPTIVE_PORTAL;
        }
        return this.A01;
    }

    public boolean BDc() {
        int i = Build.VERSION.SDK_INT;
        String $const$string = TurboLoader.Locator.$const$string(20);
        if (i < 17) {
            if (Settings.System.getInt((ContentResolver) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A4v, this.A00), $const$string, 0) != 0) {
                return true;
            }
            return false;
        } else if (Settings.Global.getInt((ContentResolver) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A4v, this.A00), $const$string, 0) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean CN2() {
        Optional optional = this.A03;
        if (!optional.isPresent() || optional.get() != C33731o3.CONNECTED) {
            return false;
        }
        return true;
    }

    public void init() {
        C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AKb, this.A00)).BMm();
        BMm.A02("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED", new EWS(this));
        BMm.A02(TurboLoader.Locator.$const$string(2), new EWU(this));
        BMm.A02(AnonymousClass24B.$const$string(211), new EWT(this));
        BMm.A00().A00();
        if (!((C09340h3) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AeN, this.A00)).A0Q()) {
            A02(this, C33731o3.NO_INTERNET);
        } else if (((C193517u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B1g, this.A00)).A03() == AnonymousClass089.DISCONNECTED) {
            A02(this, C33731o3.WAITING_TO_CONNECT);
        }
    }

    public boolean isConnected() {
        if (this.A01 == C33731o3.CONNECTED) {
            return true;
        }
        return false;
    }

    private C33721o2(AnonymousClass1XY r4) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(7, r4);
        this.A00 = r2;
        this.A04 = ((AnonymousClass1CU) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AuL, r2)).A01("mqtt_instance");
    }

    public static /* synthetic */ C33731o3 A00(AnonymousClass089 r0) {
        switch (r0.ordinal()) {
            case 0:
                return C33731o3.CONNECTING;
            case 1:
            default:
                return C33731o3.WAITING_TO_CONNECT;
            case 2:
                return C33731o3.CONNECTED;
        }
    }
}
