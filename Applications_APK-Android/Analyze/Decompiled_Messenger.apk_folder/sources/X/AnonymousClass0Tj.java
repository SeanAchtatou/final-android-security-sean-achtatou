package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0Tj  reason: invalid class name */
public final class AnonymousClass0Tj {
    public int A00 = 0;
    public int A01 = -1;
    public int A02 = 0;
    public int A03 = 0;
    public String A04 = null;
    public ArrayList A05 = new ArrayList();
    public ArrayList A06 = new ArrayList();
    public byte[] A07 = new byte[20];
    public double[] A08 = new double[15];
    public long[] A09 = new long[15];

    public void A04() {
        this.A03 = 0;
        this.A05.clear();
        this.A06.clear();
        this.A02 = 0;
        this.A00 = 0;
        this.A04 = null;
        this.A01 = -1;
    }

    public static void A00(AnonymousClass0Tj r5, byte b) {
        int i = r5.A03;
        byte[] bArr = r5.A07;
        int length = bArr.length;
        if (i == length) {
            r5.A07 = Arrays.copyOf(bArr, (int) (((double) length) * 1.4d));
        }
        byte[] bArr2 = r5.A07;
        int i2 = r5.A03;
        r5.A03 = i2 + 1;
        bArr2[i2] = b;
    }

    public static void A01(AnonymousClass0Tj r5, long j) {
        int i = r5.A02;
        long[] jArr = r5.A09;
        int length = jArr.length;
        if (i == length) {
            r5.A09 = Arrays.copyOf(jArr, (int) (((double) length) * 1.4d));
        }
        long[] jArr2 = r5.A09;
        int i2 = r5.A02;
        r5.A02 = i2 + 1;
        jArr2[i2] = j;
    }

    public List A02() {
        int i;
        int i2 = this.A03;
        if (i2 == 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(i2);
        for (int i3 = 0; i3 < i2; i3++) {
            byte b = this.A07[i3];
            switch (b) {
                case 1:
                    i = 1;
                    break;
                case 2:
                case 3:
                    i = 2;
                    break;
                case 4:
                    i = 3;
                    break;
                case 5:
                case AnonymousClass1Y3.A01 /*10*/:
                    i = 4;
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    i = 5;
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    i = 6;
                    break;
                case 8:
                    i = 7;
                    break;
                case Process.SIGKILL:
                    i = 8;
                    break;
                default:
                    throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown type ", b));
            }
            arrayList.add(Integer.valueOf(i));
        }
        return arrayList;
    }

    public List A03() {
        int i;
        String str;
        int i2;
        int i3 = this.A03;
        if (i3 == 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(i3 << 1);
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i4 < this.A03) {
            byte b = this.A07[i4];
            switch (b) {
                case 1:
                    i = i5 + 1;
                    str = (String) this.A06.get(i5);
                    continue;
                    arrayList.add(this.A05.get(i4));
                    arrayList.add(str);
                    i4++;
                    i5 = i;
                case 2:
                    i2 = i6 + 1;
                    str = Integer.toString((int) this.A09[i6]);
                    break;
                case 3:
                    i2 = i6 + 1;
                    str = Long.toString(this.A09[i6]);
                    break;
                case 4:
                    i = i5 + 1;
                    str = AnonymousClass36y.A03((String[]) this.A06.get(i5));
                    continue;
                    arrayList.add(this.A05.get(i4));
                    arrayList.add(str);
                    i4++;
                    i5 = i;
                case 5:
                    i = i5 + 1;
                    str = AnonymousClass36y.A01((int[]) this.A06.get(i5));
                    continue;
                    arrayList.add(this.A05.get(i4));
                    arrayList.add(str);
                    i4++;
                    i5 = i;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    str = Double.toString(this.A08[i7]);
                    i = i5;
                    i7++;
                    continue;
                    arrayList.add(this.A05.get(i4));
                    arrayList.add(str);
                    i4++;
                    i5 = i;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    i = i5 + 1;
                    str = AnonymousClass36y.A00((double[]) this.A06.get(i5));
                    continue;
                    arrayList.add(this.A05.get(i4));
                    arrayList.add(str);
                    i4++;
                    i5 = i;
                case 8:
                    i2 = i6 + 1;
                    boolean z = false;
                    if (this.A09[i6] != 0) {
                        z = true;
                    }
                    str = Boolean.toString(z);
                    break;
                case Process.SIGKILL:
                    i = i5 + 1;
                    str = AnonymousClass36y.A04((boolean[]) this.A06.get(i5));
                    continue;
                    arrayList.add(this.A05.get(i4));
                    arrayList.add(str);
                    i4++;
                    i5 = i;
                case AnonymousClass1Y3.A01 /*10*/:
                    i = i5 + 1;
                    str = AnonymousClass36y.A02((long[]) this.A06.get(i5));
                    continue;
                    arrayList.add(this.A05.get(i4));
                    arrayList.add(str);
                    i4++;
                    i5 = i;
                default:
                    throw new UnsupportedOperationException(AnonymousClass08S.A09("Unsupported type ", b));
            }
            i = i5;
            i6 = i2;
            arrayList.add(this.A05.get(i4));
            arrayList.add(str);
            i4++;
            i5 = i;
        }
        return arrayList;
    }
}
