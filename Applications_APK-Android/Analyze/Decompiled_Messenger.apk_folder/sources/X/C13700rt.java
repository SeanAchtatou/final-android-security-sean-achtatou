package X;

import com.facebook.messaging.model.messages.MessageDraft;
import com.facebook.messaging.model.messages.ProfileRange;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

/* renamed from: X.0rt  reason: invalid class name and case insensitive filesystem */
public final class C13700rt {
    private final AnonymousClass0jD A00;
    private final C11920oF A01;

    public static final C13700rt A00(AnonymousClass1XY r1) {
        return new C13700rt(r1);
    }

    public String A02(MessageDraft messageDraft) {
        if (messageDraft == null) {
            return null;
        }
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        objectNode.put("text", messageDraft.A03);
        objectNode.put("cursorPosition", messageDraft.A00);
        if (!messageDraft.A04.isEmpty()) {
            objectNode.put("attachmentData", this.A01.A03(messageDraft.A04));
        }
        if (!C06850cB.A0B(messageDraft.A02)) {
            objectNode.put("offlineMessageId", messageDraft.A02);
        }
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        C24971Xv it = messageDraft.A01.iterator();
        while (it.hasNext()) {
            arrayNode.add(((ProfileRange) it.next()).A02());
        }
        objectNode.put("profile_ranges", arrayNode);
        return objectNode.toString();
    }

    public C13700rt(AnonymousClass1XY r2) {
        this.A01 = C11920oF.A00(r2);
        this.A00 = AnonymousClass0jD.A00(r2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.MessageDraft A01(java.lang.String r11) {
        /*
            r10 = this;
            boolean r0 = X.C06850cB.A0B(r11)
            r9 = 0
            if (r0 == 0) goto L_0x0008
            return r9
        L_0x0008:
            X.0jD r0 = r10.A00
            com.fasterxml.jackson.databind.JsonNode r4 = r0.A02(r11)
            java.lang.String r0 = "text"
            com.fasterxml.jackson.databind.JsonNode r0 = r4.get(r0)
            java.lang.String r5 = r0.textValue()
            java.lang.String r1 = "cursorPosition"
            boolean r0 = r4.has(r1)
            if (r0 == 0) goto L_0x0063
            com.fasterxml.jackson.databind.JsonNode r0 = r4.get(r1)
            int r7 = r0.intValue()
        L_0x0028:
            java.lang.String r1 = "offlineMessageId"
            boolean r0 = r4.has(r1)
            if (r0 == 0) goto L_0x0038
            com.fasterxml.jackson.databind.JsonNode r0 = r4.get(r1)
            java.lang.String r9 = r0.textValue()
        L_0x0038:
            java.lang.String r1 = "profile_ranges"
            boolean r0 = r4.has(r1)
            if (r0 == 0) goto L_0x0065
            com.fasterxml.jackson.databind.JsonNode r3 = r4.get(r1)
            boolean r0 = r3.isArray()
            if (r0 == 0) goto L_0x0065
            com.google.common.collect.ImmutableList$Builder r2 = com.google.common.collect.ImmutableList.builder()
            r1 = 0
        L_0x004f:
            int r0 = r3.size()
            if (r1 >= r0) goto L_0x0069
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)
            com.facebook.messaging.model.messages.ProfileRange r0 = com.facebook.messaging.model.messages.ProfileRange.A00(r0)
            r2.add(r0)
            int r1 = r1 + 1
            goto L_0x004f
        L_0x0063:
            r7 = 0
            goto L_0x0028
        L_0x0065:
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.RegularImmutableList.A02
            r6 = r2
            goto L_0x006e
        L_0x0069:
            com.google.common.collect.ImmutableList r2 = r2.build()
            r6 = r2
        L_0x006e:
            java.lang.String r1 = "attachmentData"
            boolean r0 = r4.has(r1)
            if (r0 == 0) goto L_0x008a
            com.fasterxml.jackson.databind.JsonNode r0 = r4.get(r1)
            java.lang.String r1 = r0.textValue()
            X.0oF r0 = r10.A01
            java.util.List r8 = r0.A04(r1)
            com.facebook.messaging.model.messages.MessageDraft r4 = new com.facebook.messaging.model.messages.MessageDraft
            r4.<init>(r5, r6, r7, r8, r9)
            return r4
        L_0x008a:
            com.facebook.messaging.model.messages.MessageDraft r0 = new com.facebook.messaging.model.messages.MessageDraft
            r0.<init>(r5, r2, r7, r9)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13700rt.A01(java.lang.String):com.facebook.messaging.model.messages.MessageDraft");
    }
}
