package X;

import java.util.Random;

/* renamed from: X.0Vt  reason: invalid class name and case insensitive filesystem */
public final class C04690Vt implements C04700Vu {
    public static final AnonymousClass1Y7 A05 = ((AnonymousClass1Y7) C04350Ue.A03.A09("version"));
    public final AnonymousClass1YI A00;
    public final AnonymousClass0US A01;
    public final C04310Tq A02;
    private final C04710Vv A03;
    private final Random A04;

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:97:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BIW(java.util.Map r15) {
        /*
            r14 = this;
            r2 = 0
            java.lang.String r1 = "FbSharedPreferencesDbStorage.queryDb"
            r0 = -895089415(0xffffffffcaa604f9, float:-5440124.5)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x0199 }
            X.0Vv r0 = r14.A03     // Catch:{ all -> 0x0191 }
            android.database.sqlite.SQLiteDatabase r3 = r0.A06()     // Catch:{ all -> 0x0191 }
            java.lang.String r4 = "preferences"
            java.lang.String[] r5 = X.C07470dc.A00     // Catch:{ all -> 0x0191 }
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0191 }
            r0 = 1103132266(0x41c0766a, float:24.05782)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0199 }
            if (r2 != 0) goto L_0x0034
            X.0US r0 = r14.A01     // Catch:{ all -> 0x0199 }
            java.lang.Object r3 = r0.get()     // Catch:{ all -> 0x0199 }
            X.09P r3 = (X.AnonymousClass09P) r3     // Catch:{ all -> 0x0199 }
            java.lang.String r1 = "FbSharedPreferencesDbStorage_NULL_CURSOR"
            java.lang.String r0 = "Null cursor."
            r3.CGY(r1, r0)     // Catch:{ all -> 0x0199 }
            return
        L_0x0034:
            java.lang.String r1 = "FbSharedPreferencesDbStorage.loadPrefsFromCursor"
            r0 = -661848484(0xffffffffd88cfe5c, float:-1.24019274E15)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x0199 }
            X.C07480dd.A01(r2, r15)     // Catch:{ SQLiteDatabaseCorruptException -> 0x005f, IllegalStateException -> 0x0046 }
            r0 = 1682318965(0x64462675, float:1.4620906E22)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0199 }
            goto L_0x007a
        L_0x0046:
            r4 = move-exception
            X.0US r0 = r14.A01     // Catch:{ all -> 0x0189 }
            java.lang.Object r3 = r0.get()     // Catch:{ all -> 0x0189 }
            X.09P r3 = (X.AnonymousClass09P) r3     // Catch:{ all -> 0x0189 }
            java.lang.String r1 = "fbsharedprefs_db_illegal_state_on_load"
            java.lang.String r0 = "Database was reported as containing bad key values. Attempting to clear the DB."
            r3.softReport(r1, r0, r4)     // Catch:{ all -> 0x0189 }
            X.0Vv r0 = r14.A03     // Catch:{ all -> 0x0189 }
            r0.A09()     // Catch:{ all -> 0x0189 }
            r0 = -1193453733(0xffffffffb8dd575b, float:-1.05543724E-4)
            goto L_0x0077
        L_0x005f:
            r4 = move-exception
            X.0US r0 = r14.A01     // Catch:{ all -> 0x0189 }
            java.lang.Object r3 = r0.get()     // Catch:{ all -> 0x0189 }
            X.09P r3 = (X.AnonymousClass09P) r3     // Catch:{ all -> 0x0189 }
            java.lang.String r1 = "fbsharedprefs_db_corrupted_on_load"
            java.lang.String r0 = "Database was reported as malformed when loading prefs. Attempting to clear the DB."
            r3.softReport(r1, r0, r4)     // Catch:{ all -> 0x0189 }
            X.0Vv r0 = r14.A03     // Catch:{ all -> 0x0189 }
            r0.A09()     // Catch:{ all -> 0x0189 }
            r0 = 234523568(0xdfa8bb0, float:1.5441068E-30)
        L_0x0077:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0199 }
        L_0x007a:
            r2.close()
            boolean r0 = r15.isEmpty()
            if (r0 != 0) goto L_0x0188
            X.1Y7 r0 = X.C04690Vt.A05
            java.lang.Object r1 = r15.get(r0)
            boolean r0 = r1 instanceof java.lang.Integer
            if (r0 == 0) goto L_0x00bd
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r8 = r1.intValue()
        L_0x0093:
            r7 = 2
            if (r8 == r7) goto L_0x0188
            com.google.common.collect.HashMultimap r6 = new com.google.common.collect.HashMultimap
            r6.<init>()
            X.0Tq r0 = r14.A02
            java.lang.Object r0 = r0.get()
            java.util.Set r0 = (java.util.Set) r0
            java.util.Iterator r2 = r0.iterator()
        L_0x00a7:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00bf
            java.lang.Object r1 = r2.next()
            X.A3c r1 = (X.C21243A3c) r1
            int r0 = r1.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r6.Byx(r0, r1)
            goto L_0x00a7
        L_0x00bd:
            r8 = 0
            goto L_0x0093
        L_0x00bf:
            if (r8 >= r7) goto L_0x0170
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            java.util.Collection r0 = r6.AbK(r0)
            java.util.Iterator r13 = r0.iterator()
        L_0x00cd:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x0166
            java.lang.Object r11 = r13.next()
            X.A3c r11 = (X.C21243A3c) r11
            java.util.HashSet r4 = new java.util.HashSet
            r4.<init>()
            com.facebook.gk.store.GatekeeperWriter r0 = r11.A02
            X.3er r9 = r0.AY7()
            java.util.Set r0 = r15.entrySet()
            java.util.Iterator r12 = r0.iterator()
        L_0x00ec:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x0143
            java.lang.Object r3 = r12.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r10 = r3.getKey()
            X.1Y7 r10 = (X.AnonymousClass1Y7) r10
            X.1Y7 r0 = r11.A03
            boolean r0 = r10.A07(r0)
            if (r0 == 0) goto L_0x00ec
            java.lang.Object r0 = r3.getValue()
            boolean r0 = r0 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x013f
            X.1Y7 r0 = r11.A03
            java.lang.String r2 = r10.A06(r0)
            X.0WB r0 = r11.A01
            X.A63 r0 = X.AnonymousClass0WB.A01(r0)
            java.util.Map r0 = r0.A00
            java.lang.Object r1 = r0.get(r2)
            java.lang.Integer r1 = (java.lang.Integer) r1
            r0 = 0
            if (r1 == 0) goto L_0x0126
            r0 = 1
        L_0x0126:
            if (r0 == 0) goto L_0x013f
            java.lang.Object r0 = r3.getValue()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            com.facebook.common.util.TriState r3 = com.facebook.common.util.TriState.valueOf(r0)
            monitor-enter(r9)
            int r2 = r9.A00(r2)     // Catch:{ all -> 0x016d }
            r1 = r9
            monitor-enter(r1)     // Catch:{ all -> 0x016d }
            com.facebook.common.util.TriState[] r0 = r9.A01     // Catch:{ all -> 0x016a }
            r0[r2] = r3     // Catch:{ all -> 0x016a }
            monitor-exit(r1)     // Catch:{ all -> 0x016d }
            monitor-exit(r9)
        L_0x013f:
            r4.add(r10)
            goto L_0x00ec
        L_0x0143:
            r9.A01()
            java.util.Iterator r1 = r4.iterator()
        L_0x014a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x015a
            java.lang.Object r0 = r1.next()
            X.1Y7 r0 = (X.AnonymousClass1Y7) r0
            r15.remove(r0)
            goto L_0x014a
        L_0x015a:
            java.util.Map r0 = java.util.Collections.emptyMap()
            r14.CNe(r0, r4)
            r4.size()
            goto L_0x00cd
        L_0x0166:
            int r8 = r8 + 1
            goto L_0x00bf
        L_0x016a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x016d }
            throw r0     // Catch:{ all -> 0x016d }
        L_0x016d:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0170:
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            X.1Y7 r1 = X.C04690Vt.A05
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)
            r2.put(r1, r0)
            r15.putAll(r2)
            java.util.Set r0 = java.util.Collections.emptySet()
            r14.CNe(r2, r0)
        L_0x0188:
            return
        L_0x0189:
            r1 = move-exception
            r0 = 1573221884(0x5dc575fc, float:1.77856946E18)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0199 }
            throw r1     // Catch:{ all -> 0x0199 }
        L_0x0191:
            r1 = move-exception
            r0 = 338397878(0x142b8ab6, float:8.66065E-27)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0199 }
            throw r1     // Catch:{ all -> 0x0199 }
        L_0x0199:
            r0 = move-exception
            if (r2 == 0) goto L_0x019f
            r2.close()
        L_0x019f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04690Vt.BIW(java.util.Map):void");
    }

    public static final C04690Vt A00(AnonymousClass1XY r6) {
        return new C04690Vt(C04710Vv.A00(r6), AnonymousClass0UQ.A00(AnonymousClass1Y3.Amr, r6), AnonymousClass0W9.A01(), AnonymousClass0VG.A00(AnonymousClass1Y3.Aie, r6), AnonymousClass0WA.A00(r6));
    }

    private C04690Vt(C04710Vv r1, AnonymousClass0US r2, Random random, C04310Tq r4, AnonymousClass1YI r5) {
        this.A03 = r1;
        this.A01 = r2;
        this.A04 = random;
        this.A02 = r4;
        this.A00 = r5;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:27|(3:29|30|31)|32|33|34|35|36|48|37) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00d9 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void CNe(java.util.Map r14, java.util.Collection r15) {
        /*
            r13 = this;
            boolean r0 = r14.isEmpty()
            if (r0 == 0) goto L_0x000d
            boolean r0 = r15.isEmpty()
            if (r0 == 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            java.util.Random r1 = r13.A04
            r0 = 1000(0x3e8, float:1.401E-42)
            int r0 = r1.nextInt(r0)
            r5 = 0
            r4 = 1
            r12 = 0
            if (r0 >= r4) goto L_0x001b
            r12 = 1
        L_0x001b:
            X.0Vv r0 = r13.A03
            android.database.sqlite.SQLiteDatabase r2 = r0.A06()
            r11 = 0
            r3 = r11
            r10 = 0
        L_0x0024:
            r0 = 10
            if (r5 >= r0) goto L_0x00f0
            r1 = 439325917(0x1a2f94dd, float:3.6309403E-23)
            java.lang.String r0 = "FbSharedPreferencesDbStorage.writePrefChangesAttempt"
            X.C005505z.A03(r0, r1)
            r0 = 1687205420(0x6490b62c, float:2.1355664E22)
            X.C007406x.A01(r2, r0)
            if (r10 != 0) goto L_0x0080
            boolean r0 = r14.isEmpty()     // Catch:{ SQLiteException -> 0x00ab }
            if (r0 != 0) goto L_0x007f
            android.content.ContentValues r8 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00ab }
            r8.<init>()     // Catch:{ SQLiteException -> 0x00ab }
            java.util.Set r0 = r14.entrySet()     // Catch:{ SQLiteException -> 0x00ab }
            java.util.Iterator r9 = r0.iterator()     // Catch:{ SQLiteException -> 0x00ab }
        L_0x004b:
            boolean r0 = r9.hasNext()     // Catch:{ SQLiteException -> 0x00ab }
            if (r0 == 0) goto L_0x007f
            java.lang.Object r0 = r9.next()     // Catch:{ SQLiteException -> 0x00ab }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ SQLiteException -> 0x00ab }
            java.lang.Object r7 = r0.getKey()     // Catch:{ SQLiteException -> 0x00ab }
            X.1Y7 r7 = (X.AnonymousClass1Y7) r7     // Catch:{ SQLiteException -> 0x00ab }
            java.lang.Object r6 = r0.getValue()     // Catch:{ SQLiteException -> 0x00ab }
            X.1YI r3 = r13.A00     // Catch:{ SQLiteException -> 0x00ab }
            r1 = 324(0x144, float:4.54E-43)
            r0 = 0
            boolean r0 = r3.AbO(r1, r0)     // Catch:{ SQLiteException -> 0x00ab }
            X.C07480dd.A00(r8, r7, r6, r0)     // Catch:{ SQLiteException -> 0x00ab }
            r0 = -1750399044(0xffffffff97ab07bc, float:-1.1052568E-24)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x00ab }
            java.lang.String r0 = "preferences"
            r2.replaceOrThrow(r0, r11, r8)     // Catch:{ SQLiteException -> 0x00ab }
            r0 = 1351350244(0x508bf7e4, float:1.87862303E10)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x00ab }
            goto L_0x004b
        L_0x007f:
            r10 = 1
        L_0x0080:
            boolean r0 = r15.isEmpty()     // Catch:{ SQLiteException -> 0x00ab }
            if (r0 != 0) goto L_0x00a7
            java.lang.String[] r6 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00ab }
            java.util.Iterator r3 = r15.iterator()     // Catch:{ SQLiteException -> 0x00ab }
        L_0x008c:
            boolean r0 = r3.hasNext()     // Catch:{ SQLiteException -> 0x00ab }
            if (r0 == 0) goto L_0x00a7
            java.lang.Object r0 = r3.next()     // Catch:{ SQLiteException -> 0x00ab }
            X.1Y7 r0 = (X.AnonymousClass1Y7) r0     // Catch:{ SQLiteException -> 0x00ab }
            r1 = 0
            java.lang.String r0 = r0.A05()     // Catch:{ SQLiteException -> 0x00ab }
            r6[r1] = r0     // Catch:{ SQLiteException -> 0x00ab }
            java.lang.String r1 = "preferences"
            java.lang.String r0 = "key = ?"
            r2.delete(r1, r0, r6)     // Catch:{ SQLiteException -> 0x00ab }
            goto L_0x008c
        L_0x00a7:
            r2.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x00ab }
            goto L_0x00f2
        L_0x00ab:
            r3 = move-exception
            if (r12 == 0) goto L_0x00d3
            java.lang.String r7 = "Writing preferences failed."
            java.lang.String r6 = "FbSharedPreferencesDbStorage_PROVIDER_SQLITE_EXCEPTION"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0105 }
            java.lang.String r0 = "Attempt #%d. %s."
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1, r7)     // Catch:{ all -> 0x0105 }
            X.06G r0 = X.AnonymousClass06F.A02(r6, r0)     // Catch:{ all -> 0x0105 }
            r0.A00 = r4     // Catch:{ all -> 0x0105 }
            r0.A03 = r3     // Catch:{ all -> 0x0105 }
            X.06F r1 = r0.A00()     // Catch:{ all -> 0x0105 }
            X.0US r0 = r13.A01     // Catch:{ all -> 0x0105 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0105 }
            X.09P r0 = (X.AnonymousClass09P) r0     // Catch:{ all -> 0x0105 }
            r0.CGQ(r1)     // Catch:{ all -> 0x0105 }
        L_0x00d3:
            r0 = 30
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x00d9 }
            goto L_0x00e0
        L_0x00d9:
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0105 }
            r0.interrupt()     // Catch:{ all -> 0x0105 }
        L_0x00e0:
            r0 = 567338283(0x21d0e52b, float:1.4155288E-18)
            X.C007406x.A02(r2, r0)
            r0 = 113090650(0x6bda05a, float:7.1329514E-35)
            X.C005505z.A00(r0)
            int r5 = r5 + 1
            goto L_0x0024
        L_0x00f0:
            r11 = r3
            goto L_0x00fe
        L_0x00f2:
            r0 = 1227064222(0x4923839e, float:669753.9)
            X.C007406x.A02(r2, r0)
            r0 = -2110871216(0xffffffff822ea950, float:-1.2832091E-37)
            X.C005505z.A00(r0)
        L_0x00fe:
            if (r11 == 0) goto L_0x000c
            java.lang.RuntimeException r0 = com.google.common.base.Throwables.propagate(r11)
            throw r0
        L_0x0105:
            r1 = move-exception
            r0 = 945893798(0x386131a6, float:5.369042E-5)
            X.C007406x.A02(r2, r0)
            r0 = 985517572(0x3abdce04, float:0.0014480953)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04690Vt.CNe(java.util.Map, java.util.Collection):void");
    }
}
