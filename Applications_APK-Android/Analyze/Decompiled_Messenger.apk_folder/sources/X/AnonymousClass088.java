package X;

import android.view.Choreographer;

/* renamed from: X.088  reason: invalid class name */
public final class AnonymousClass088 implements AnonymousClass05V {
    public long A00 = -1;
    public long A01 = -1;
    public AnonymousClass05U A02;
    public boolean A03 = false;
    public final Choreographer A04;
    private final Choreographer.FrameCallback A05;

    public void disable() {
        this.A03 = false;
        this.A04.removeFrameCallback(this.A05);
    }

    public void enable() {
        if (!this.A03) {
            this.A00 = -1;
        }
        this.A03 = true;
        this.A04.postFrameCallback(this.A05);
    }

    public AnonymousClass088(Choreographer choreographer, AnonymousClass05U r4) {
        this.A04 = choreographer;
        this.A02 = r4;
        this.A05 = new C192817n(this);
    }
}
