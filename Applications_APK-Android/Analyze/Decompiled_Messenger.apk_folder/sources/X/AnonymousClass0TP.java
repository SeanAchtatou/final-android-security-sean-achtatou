package X;

import java.util.Arrays;
import java.util.HashSet;

/* renamed from: X.0TP  reason: invalid class name */
public final class AnonymousClass0TP extends HashSet<AnonymousClass0TO> {
    public AnonymousClass0TP() {
        addAll(Arrays.asList(AnonymousClass0TN.A01, AnonymousClass0TN.A04, AnonymousClass0TN.A06, AnonymousClass0TN.A0N, AnonymousClass0TN.A0L, AnonymousClass0TN.A0P, AnonymousClass0TN.A0I, AnonymousClass0TN.A0Y));
        addAll(AnonymousClass0TN.A0r);
    }
}
