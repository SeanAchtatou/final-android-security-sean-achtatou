package X;

import android.os.Build;
import android.os.PowerManager;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import io.card.payment.BuildConfig;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.1pX  reason: invalid class name and case insensitive filesystem */
public final class C34411pX {
    private boolean A00 = false;
    public final PowerManager A01;
    public final FbSharedPreferences A02;
    public final List A03 = new LinkedList();
    private final C34451pb A04;
    private final C09340h3 A05;

    private synchronized void A01() {
        this.A03.clear();
        String B4F = this.A02.B4F(C10930l6.A03, BuildConfig.FLAVOR);
        if (!B4F.matches("([01][01])*")) {
            B4F = BuildConfig.FLAVOR;
        }
        int i = 0;
        while (true) {
            boolean z = true;
            if (i < ((B4F.length() / 2) << 1)) {
                boolean z2 = false;
                if (B4F.charAt(i) == '1') {
                    z2 = true;
                }
                if (B4F.charAt(i + 1) != '1') {
                    z = false;
                }
                this.A03.add(new C34421pY(z2, z));
                i += 2;
            } else {
                this.A00 = true;
            }
        }
    }

    public synchronized Integer A02() {
        Integer num;
        if (!this.A00) {
            A01();
        }
        if (this.A03.size() < 50) {
            num = AnonymousClass07B.A0C;
        } else {
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            for (C34421pY r1 : this.A03) {
                if (r1.A00) {
                    i3++;
                    if (r1.A01) {
                        i4++;
                    }
                } else {
                    i++;
                    if (r1.A01) {
                        i2++;
                    }
                }
            }
            if (i < 10) {
                num = AnonymousClass07B.A0C;
            } else {
                double d = (double) i2;
                Double.isNaN(d);
                double d2 = (double) i;
                Double.isNaN(d2);
                double d3 = (d * 1.0d) / d2;
                if (d3 > 0.18d) {
                    num = AnonymousClass07B.A01;
                } else if (i3 < 10) {
                    num = AnonymousClass07B.A0C;
                } else {
                    double d4 = (double) i4;
                    Double.isNaN(d4);
                    double d5 = (double) i3;
                    Double.isNaN(d5);
                    double d6 = (d4 * 1.0d) / d5;
                    if (d6 < 0.5d) {
                        num = AnonymousClass07B.A0C;
                    } else if (d6 / d3 > 5.0d) {
                        num = AnonymousClass07B.A00;
                    } else {
                        num = AnonymousClass07B.A0C;
                    }
                }
            }
        }
        return num;
    }

    public synchronized void A03(boolean z, boolean z2) {
        if (!this.A00) {
            A01();
        }
        if (!this.A05.A0O()) {
            if (Build.VERSION.SDK_INT < 21 || !this.A01.isPowerSaveMode()) {
                if (this.A04.A02() >= 0.0f && !this.A04.A0A(20)) {
                    this.A03.add(new C34421pY(z, z2));
                    if (this.A03.size() > 100) {
                        this.A03.remove(0);
                    }
                    synchronized (this) {
                        StringBuilder sb = new StringBuilder();
                        for (C34421pY r3 : this.A03) {
                            char c = '1';
                            char c2 = '0';
                            if (r3.A00) {
                                c2 = '1';
                            }
                            sb.append(c2);
                            if (!r3.A01) {
                                c = '0';
                            }
                            sb.append(c);
                        }
                        C30281hn edit = this.A02.edit();
                        edit.BzC(C10930l6.A03, sb.toString());
                        edit.commit();
                    }
                }
            }
        }
    }

    public static final C34411pX A00(AnonymousClass1XY r1) {
        return new C34411pX(r1);
    }

    public C34411pX(AnonymousClass1XY r2) {
        this.A02 = FbSharedPreferencesModule.A00(r2);
        this.A05 = C09340h3.A01(r2);
        this.A01 = C04490Ux.A0Y(r2);
        this.A04 = C34451pb.A01(r2);
    }
}
