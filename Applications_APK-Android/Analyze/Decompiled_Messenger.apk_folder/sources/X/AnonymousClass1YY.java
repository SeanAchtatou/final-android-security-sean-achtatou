package X;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1YY  reason: invalid class name */
public final class AnonymousClass1YY {
    private static volatile AnonymousClass1YY A04;
    public AnonymousClass1YU A00;
    public final ConcurrentHashMap A01 = new ConcurrentHashMap();
    public final ConcurrentHashMap A02 = new ConcurrentHashMap();
    public final ConcurrentSkipListSet A03 = new ConcurrentSkipListSet();

    public static final AnonymousClass1YY A00(AnonymousClass1XY r3) {
        if (A04 == null) {
            synchronized (AnonymousClass1YY.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A04 = new AnonymousClass1YY();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static void A01(int i, int i2, ConcurrentHashMap concurrentHashMap) {
        Integer valueOf = Integer.valueOf(i);
        ConcurrentSkipListSet concurrentSkipListSet = (ConcurrentSkipListSet) concurrentHashMap.get(valueOf);
        if (concurrentSkipListSet == null) {
            concurrentSkipListSet = new ConcurrentSkipListSet();
        }
        concurrentSkipListSet.add(Integer.valueOf(i2));
        concurrentHashMap.putIfAbsent(valueOf, concurrentSkipListSet);
    }
}
