package X;

import android.net.NetworkInfo;
import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1lO  reason: invalid class name and case insensitive filesystem */
public final class C32211lO {
    private static volatile C32211lO A02;
    public AnonymousClass0UN A00;
    private final C04310Tq A01;

    public static final C32211lO A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C32211lO.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C32211lO(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public boolean A01() {
        Boolean bool = Boolean.FALSE;
        int i = AnonymousClass1Y3.BAo;
        AnonymousClass0UN r1 = this.A00;
        if (!bool.equals((Boolean) AnonymousClass1XX.A02(5, i, r1)) || !((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, r1)).AbO(514, false)) {
            return false;
        }
        return true;
    }

    public boolean A02() {
        if (((Boolean) this.A01.get()).booleanValue() || !A03()) {
            return false;
        }
        if (!((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A00)).Aep(C71143bt.A00, true)) {
            return true;
        }
        NetworkInfo A0D = ((C09340h3) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AeN, this.A00)).A0D();
        boolean z = true;
        if (A0D == null || !A0D.isConnected() || A0D.getType() != 1) {
            z = false;
        }
        if (!z) {
            return true;
        }
        return false;
    }

    private C32211lO(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
        AnonymousClass0VG.A00(AnonymousClass1Y3.Ard, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.Amx, r3);
    }

    public boolean A03() {
        if (!A01() || !((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A00)).Aep(C71143bt.A01, false)) {
            return false;
        }
        return true;
    }
}
