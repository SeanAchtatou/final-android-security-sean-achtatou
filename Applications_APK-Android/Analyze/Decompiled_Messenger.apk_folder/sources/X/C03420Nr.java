package X;

import java.io.ByteArrayOutputStream;

/* renamed from: X.0Nr  reason: invalid class name and case insensitive filesystem */
public final class C03420Nr extends ByteArrayOutputStream {
    public byte[] toByteArray() {
        int i = this.count;
        byte[] bArr = this.buf;
        if (i == bArr.length) {
            return bArr;
        }
        return super.toByteArray();
    }

    public C03420Nr(int i) {
        super(i);
    }
}
