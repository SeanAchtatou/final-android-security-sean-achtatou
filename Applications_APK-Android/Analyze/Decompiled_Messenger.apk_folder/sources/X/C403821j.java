package X;

/* renamed from: X.21j  reason: invalid class name and case insensitive filesystem */
public final class C403821j implements C58062t6 {
    public static final C43872Ga A07 = new C61292ye();
    public static final C58072t7 A08 = new C61282yd().A00();
    public int A00 = Integer.MAX_VALUE;
    public int A01 = 1;
    public int A02 = Integer.MIN_VALUE;
    public C43872Ga A03 = A07;
    public C58072t7 A04 = A08;
    public boolean A05 = false;
    private int A06 = 1;

    /* renamed from: A00 */
    public C58032t3 AQc() {
        C58032t3 r3 = new C58032t3(this.A01, this.A05, this.A02, this.A04, this.A03);
        r3.A00 = this.A00;
        r3.A01 = this.A06;
        int B3M = r3.B3M();
        if (r3.AwY() != 1 || B3M == Integer.MIN_VALUE || B3M == -1) {
            return r3;
        }
        throw new UnsupportedOperationException("Only snap to start is implemented for vertical lists");
    }

    public /* bridge */ /* synthetic */ C58062t6 C0C(C58072t7 r1) {
        this.A04 = r1;
        return this;
    }

    public C403821j() {
    }

    public C403821j(C58032t3 r3) {
        this.A01 = r3.A02;
        this.A05 = r3.A06;
        this.A02 = r3.A03;
        this.A04 = r3.A05;
        this.A03 = r3.A04;
        this.A00 = r3.A00;
        this.A06 = r3.A01;
    }
}
