package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1pP  reason: invalid class name and case insensitive filesystem */
public final class C34331pP implements AnonymousClass0AT {
    private static volatile C34331pP A04;
    private final C34321pO A00;
    private final Map A01 = new ConcurrentHashMap();
    private final Map A02 = new ConcurrentHashMap();
    private final Map A03 = new ConcurrentHashMap();

    public static final C34331pP A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C34331pP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C34331pP(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void A01(String str) {
        SubscribeTopic subscribeTopic = (SubscribeTopic) this.A03.get(str);
        this.A03.size();
        if (subscribeTopic != null) {
            this.A00.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A03, ImmutableList.of(subscribeTopic));
        }
    }

    public void BKd(String str) {
        SubscribeTopic subscribeTopic = (SubscribeTopic) this.A03.get(str);
        this.A03.size();
        if (subscribeTopic != null) {
            this.A00.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A05, ImmutableList.of(subscribeTopic));
        }
    }

    public void BKs(int i) {
        List list = (List) this.A01.remove(Integer.valueOf(i));
        if (list != null) {
            this.A00.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A08, list);
        }
    }

    public void BKt(int i) {
        List list = (List) this.A02.remove(Integer.valueOf(i));
        if (list != null) {
            this.A00.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A0C, list);
        }
    }

    public void Bfm(String str) {
        String str2;
        Iterator it = this.A01.values().iterator();
        while (true) {
            str2 = str;
            if (!it.hasNext()) {
                break;
            }
            this.A00.A02("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A01, (List) it.next(), str2);
        }
        for (List A022 : this.A02.values()) {
            this.A00.A02("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A02, A022, str2);
        }
    }

    public void BsK(List list, int i) {
        if (list != null) {
            this.A01.put(Integer.valueOf(i), list);
            Iterator it = list.iterator();
            while (it.hasNext()) {
                SubscribeTopic subscribeTopic = (SubscribeTopic) it.next();
                this.A03.put(subscribeTopic.A01, subscribeTopic);
            }
        }
    }

    public void BsL(List list, int i) {
        if (list != null) {
            this.A02.put(Integer.valueOf(i), list);
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.A03.remove(((SubscribeTopic) it.next()).A01);
            }
        }
    }

    public void C3a() {
        this.A01.clear();
        this.A02.clear();
        this.A03.clear();
    }

    private C34331pP(AnonymousClass1XY r2) {
        this.A00 = C34321pO.A00(r2);
    }
}
