package X;

import com.facebook.profilo.core.ProvidersRegistry;
import java.util.List;
import java.util.TreeMap;

/* renamed from: X.07p  reason: invalid class name and case insensitive filesystem */
public final class C008507p {
    public int A00;
    public TreeMap A01;
    public TreeMap A02;
    public TreeMap A03;
    public final int A04;
    private final List A05;

    public C008507p(int i, List list, TreeMap treeMap, TreeMap treeMap2, TreeMap treeMap3) {
        if (i > 0) {
            this.A04 = i;
            this.A05 = list;
            this.A03 = treeMap;
            this.A01 = treeMap2;
            this.A02 = treeMap3;
            if (list != null && !list.isEmpty()) {
                this.A00 = ProvidersRegistry.A00.A00(this.A05);
                return;
            }
            return;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0A("coinflip_sample_rate (", i, ") <= 0"));
    }
}
