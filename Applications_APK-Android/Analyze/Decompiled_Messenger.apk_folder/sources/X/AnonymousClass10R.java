package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.10R  reason: invalid class name */
public final class AnonymousClass10R {
    private static volatile AnonymousClass10R A01;
    public final C25051Yd A00;

    public static final AnonymousClass10R A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass10R.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass10R(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static boolean A01(AnonymousClass10R r2, boolean z) {
        if (z) {
            return r2.A00.Aem(282325383120200L);
        }
        return r2.A00.Aer(282325383120200L, AnonymousClass0XE.A07);
    }

    public boolean A02() {
        return this.A00.Aem(285774239242113L);
    }

    public boolean A03(boolean z) {
        if (z) {
            return this.A00.Aem(282325382726980L);
        }
        return this.A00.Aer(282325382726980L, AnonymousClass0XE.A07);
    }

    private AnonymousClass10R(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
