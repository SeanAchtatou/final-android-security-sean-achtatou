package X;

import android.database.Cursor;

/* renamed from: X.0cY  reason: invalid class name and case insensitive filesystem */
public final class C07050cY extends AnonymousClass0UT {
    public C42712Bn A00(Cursor cursor, boolean z) {
        C13660rp r3 = new C13660rp(this);
        C13670rq r4 = new C13670rq(this);
        C42722Bo r5 = new C42722Bo(this);
        new C13690rs(this);
        return new C42712Bn(r3, r4, r5, cursor, z);
    }

    public C07050cY(AnonymousClass1XY r1) {
        super(r1);
    }
}
