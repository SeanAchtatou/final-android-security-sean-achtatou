package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0Aj  reason: invalid class name and case insensitive filesystem */
public final class C01470Aj {
    public static final IntentFilter A07;
    public C01780Bp A00;
    public final BroadcastReceiver A01;
    public final Context A02;
    public final Handler A03;
    public final AtomicLong A04 = new AtomicLong(-1);
    public final AtomicReference A05 = new AtomicReference(null);
    private final C01420Ad A06;

    static {
        IntentFilter intentFilter = new IntentFilter();
        A07 = intentFilter;
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        IntentFilter intentFilter2 = A07;
        intentFilter2.addAction("android.intent.action.SCREEN_OFF");
        intentFilter2.setPriority(999);
    }

    public boolean A00() {
        Boolean bool = (Boolean) this.A05.get();
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            C01540Aq A002 = this.A06.A00("power", PowerManager.class);
            if (!A002.A02()) {
                return false;
            }
            if (Build.VERSION.SDK_INT >= 20) {
                return ((PowerManager) A002.A01()).isInteractive();
            }
            return ((PowerManager) A002.A01()).isScreenOn();
        } catch (RuntimeException unused) {
            return false;
        }
    }

    public C01470Aj(Context context, C01420Ad r5, Handler handler) {
        this.A02 = context;
        this.A06 = r5;
        this.A03 = handler;
        this.A01 = new C01480Ak(this);
    }
}
