package X;

import android.content.Context;
import android.view.View;
import com.facebook.messaging.attribution.MediaResourceView;
import com.facebook.resources.ui.FbTextView;
import com.facebook.widget.CustomFrameLayout;

/* renamed from: X.20D  reason: invalid class name */
public final class AnonymousClass20D extends CustomFrameLayout {
    public View A00 = C012809p.A01(this, 2131298447);
    public View A01 = C012809p.A01(this, 2131300484);
    public BHQ A02;
    public MediaResourceView A03 = ((MediaResourceView) C012809p.A01(this, 2131298959));
    public FbTextView A04 = ((FbTextView) C012809p.A01(this, 2131296986));
    public FbTextView A05 = ((FbTextView) C012809p.A01(this, 2131297584));
    public FbTextView A06 = ((FbTextView) C012809p.A01(this, 2131301101));

    public AnonymousClass20D(Context context) {
        super(context);
        A0L(2132410959);
        this.A00.setOnClickListener(new BHR(this));
        this.A01.setOnClickListener(new BHO(this));
        this.A04.setOnClickListener(new BHS(this));
    }
}
