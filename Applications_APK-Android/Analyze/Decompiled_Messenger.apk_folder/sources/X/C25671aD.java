package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.model.Name;

/* renamed from: X.1aD  reason: invalid class name and case insensitive filesystem */
public final class C25671aD implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new Name(parcel);
    }

    public Object[] newArray(int i) {
        return new Name[i];
    }
}
