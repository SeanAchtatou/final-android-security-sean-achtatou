package X;

import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent;

/* renamed from: X.1TP  reason: invalid class name */
public final class AnonymousClass1TP implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.loading.MontageLoadingServiceImpl$1";
    public final /* synthetic */ C34041oY A00;

    public AnonymousClass1TP(C34041oY r1) {
        this.A00 = r1;
    }

    public void run() {
        int i;
        C33161n7 r3 = (C33161n7) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ANX, this.A00.A00);
        C005505z.A03("MontageOmnistoreServiceImpl.loadInventory", -292038243);
        try {
            int i2 = AnonymousClass1Y3.ANr;
            if (((MontageOmnistoreComponent) AnonymousClass1XX.A02(0, i2, r3.A00)).A05()) {
                OperationResult.A02(C14880uI.A0B, "the collection is already available");
                i = 1467046723;
            } else {
                ((MontageOmnistoreComponent) AnonymousClass1XX.A02(0, i2, r3.A00)).A04();
                i = 201562569;
            }
            C005505z.A00(i);
        } catch (Throwable th) {
            C005505z.A00(-1178370968);
            throw th;
        }
    }
}
