package X;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: X.1Fn  reason: invalid class name and case insensitive filesystem */
public final class C21181Fn extends C21191Fp implements C05160Xw {
    public synchronized void execute(Runnable runnable) {
        super.execute(runnable);
    }

    private C21181Fn(Executor executor) {
        super("SerialExecutor", 1, executor, new LinkedBlockingQueue());
    }

    public static C05160Xw A00(Executor executor) {
        if (!(executor instanceof C04640Vn)) {
            return new C21181Fn(executor);
        }
        return (AnonymousClass1TI) ((C04640Vn) executor).A03.A02((C04650Vo) executor, 1, "SerialExecutor", null);
    }
}
