package X;

import android.graphics.Rect;

/* renamed from: X.117  reason: invalid class name */
public final class AnonymousClass117 {
    public float A00;
    public float A01;
    public float A02;
    public C17770zR A03;
    public AnonymousClass10N A04;
    public AnonymousClass10N A05;
    public AnonymousClass10N A06;
    public AnonymousClass10N A07;
    public AnonymousClass10N A08;
    public AnonymousClass10N A09;
    public final Rect A0A = new Rect();

    public String A00() {
        C17770zR r0 = this.A03;
        if (r0 == null) {
            return null;
        }
        return r0.A06;
    }
}
