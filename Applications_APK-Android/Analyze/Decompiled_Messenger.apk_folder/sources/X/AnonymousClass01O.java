package X;

import java.io.Writer;

/* renamed from: X.01O  reason: invalid class name */
public final class AnonymousClass01O implements AnonymousClass01J {
    public boolean ANg(Writer writer, AnonymousClass0HM r8) {
        String A00 = AnonymousClass00V.A00("/proc/self/oom_adj");
        String A002 = AnonymousClass00V.A00("/proc/self/oom_score");
        String A003 = AnonymousClass00V.A00("/proc/self/oom_score_adj");
        AnonymousClass023 r2 = null;
        if (!(A00 == null || A002 == null || A003 == null)) {
            AnonymousClass023 r1 = new AnonymousClass023();
            try {
                r1.A00 = Integer.parseInt(A00.trim());
                r1.A01 = Integer.parseInt(A002.trim());
                r1.A02 = Integer.parseInt(A003.trim());
                r2 = r1;
            } catch (NumberFormatException unused) {
            }
        }
        if (r2 == null) {
            return false;
        }
        writer.append((CharSequence) "\"oom_adj\":");
        writer.append((CharSequence) Integer.toString(r2.A00));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"oom_score\":");
        writer.append((CharSequence) Integer.toString(r2.A01));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"oom_score_adj\":");
        writer.append((CharSequence) Integer.toString(r2.A02));
        return true;
    }
}
