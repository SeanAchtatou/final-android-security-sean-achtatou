package X;

/* renamed from: X.09j  reason: invalid class name and case insensitive filesystem */
public final class C012209j extends AnonymousClass009 {
    public final /* synthetic */ AnonymousClass001 A00;

    public C012209j(AnonymousClass001 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A07() {
        /*
            r3 = this;
            X.001 r1 = r3.A00
            java.util.ArrayList r0 = r1.A0b
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x002e
            java.util.ArrayList r0 = r1.A0c
            java.util.Iterator r2 = r0.iterator()
        L_0x0010:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0030
            java.lang.Object r1 = r2.next()
            com.facebook.base.app.SplashScreenActivity r1 = (com.facebook.base.app.SplashScreenActivity) r1
            boolean r0 = r1.isFinishing()
            if (r0 != 0) goto L_0x002b
            boolean r0 = r1.A03
            if (r0 != 0) goto L_0x002b
            boolean r1 = r1.A04
            r0 = 0
            if (r1 == 0) goto L_0x002c
        L_0x002b:
            r0 = 1
        L_0x002c:
            if (r0 != 0) goto L_0x0010
        L_0x002e:
            r0 = 0
            return r0
        L_0x0030:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C012209j.A07():boolean");
    }
}
