package X;

import android.content.Context;

/* renamed from: X.0xi  reason: invalid class name and case insensitive filesystem */
public final class C16750xi {
    public final int A00;

    public static final C16750xi A00(AnonymousClass1XY r2) {
        return new C16750xi(r2, AnonymousClass1YA.A02(r2));
    }

    public C16750xi(AnonymousClass1XY r3, Context context) {
        new AnonymousClass0UN(1, r3);
        this.A00 = AnonymousClass01R.A00(context, 2132082725);
    }
}
