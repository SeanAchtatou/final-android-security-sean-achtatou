package X;

/* renamed from: X.0zB  reason: invalid class name and case insensitive filesystem */
public final class C17610zB implements Cloneable {
    private static final Object A04 = new Object();
    private int A00;
    private boolean A01;
    private long[] A02;
    private Object[] A03;

    public Object A07(long j) {
        return A08(j, null);
    }

    private void A00() {
        int i = this.A00;
        long[] jArr = this.A02;
        Object[] objArr = this.A03;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != A04) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.A01 = false;
        this.A00 = i2;
    }

    public int A01() {
        if (this.A01) {
            A00();
        }
        return this.A00;
    }

    public int A02(long j) {
        if (this.A01) {
            A00();
        }
        return AnonymousClass04d.A03(this.A02, this.A00, j);
    }

    public int A03(Object obj) {
        if (this.A01) {
            A00();
        }
        for (int i = 0; i < this.A00; i++) {
            if (this.A03[i] == obj) {
                return i;
            }
        }
        return -1;
    }

    public long A04(int i) {
        if (this.A01) {
            A00();
        }
        return this.A02[i];
    }

    public Object A06(int i) {
        if (this.A01) {
            A00();
        }
        return this.A03[i];
    }

    public Object A08(long j, Object obj) {
        Object obj2;
        int A032 = AnonymousClass04d.A03(this.A02, this.A00, j);
        if (A032 < 0 || (obj2 = this.A03[A032]) == A04) {
            return obj;
        }
        return obj2;
    }

    public void A09() {
        int i = this.A00;
        Object[] objArr = this.A03;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.A00 = 0;
        this.A01 = false;
    }

    public void A0A(int i) {
        Object[] objArr = this.A03;
        Object obj = objArr[i];
        Object obj2 = A04;
        if (obj != obj2) {
            objArr[i] = obj2;
            this.A01 = true;
        }
    }

    public void A0B(long j) {
        Object[] objArr;
        Object obj;
        int A032 = AnonymousClass04d.A03(this.A02, this.A00, j);
        if (A032 >= 0 && (objArr = this.A03)[A032] != (obj = A04)) {
            objArr[A032] = obj;
            this.A01 = true;
        }
    }

    public void A0C(long j, Object obj) {
        int i = this.A00;
        if (i == 0 || j > this.A02[i - 1]) {
            if (this.A01 && i >= this.A02.length) {
                A00();
            }
            int i2 = this.A00;
            long[] jArr = this.A02;
            int length = jArr.length;
            if (i2 >= length) {
                int A012 = AnonymousClass04d.A01(i2 + 1);
                long[] jArr2 = new long[A012];
                Object[] objArr = new Object[A012];
                System.arraycopy(jArr, 0, jArr2, 0, length);
                Object[] objArr2 = this.A03;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.A02 = jArr2;
                this.A03 = objArr;
            }
            this.A02[i2] = j;
            this.A03[i2] = obj;
            this.A00 = i2 + 1;
            return;
        }
        A0D(j, obj);
    }

    public void A0D(long j, Object obj) {
        long[] jArr = this.A02;
        int i = this.A00;
        int A032 = AnonymousClass04d.A03(jArr, i, j);
        if (A032 >= 0) {
            this.A03[A032] = obj;
            return;
        }
        int i2 = A032 ^ -1;
        if (i2 < i) {
            Object[] objArr = this.A03;
            if (objArr[i2] == A04) {
                jArr[i2] = j;
                objArr[i2] = obj;
                return;
            }
        }
        if (this.A01 && i >= jArr.length) {
            A00();
            i2 = AnonymousClass04d.A03(jArr, this.A00, j) ^ -1;
        }
        int i3 = this.A00;
        int length = jArr.length;
        if (i3 >= length) {
            int A012 = AnonymousClass04d.A01(i3 + 1);
            long[] jArr2 = new long[A012];
            Object[] objArr2 = new Object[A012];
            System.arraycopy(jArr, 0, jArr2, 0, length);
            Object[] objArr3 = this.A03;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.A02 = jArr2;
            this.A03 = objArr2;
        }
        int i4 = this.A00 - i2;
        if (i4 != 0) {
            long[] jArr3 = this.A02;
            int i5 = i2 + 1;
            System.arraycopy(jArr3, i2, jArr3, i5, i4);
            Object[] objArr4 = this.A03;
            System.arraycopy(objArr4, i2, objArr4, i5, this.A00 - i2);
        }
        this.A02[i2] = j;
        this.A03[i2] = obj;
        this.A00++;
    }

    /* renamed from: A05 */
    public C17610zB clone() {
        try {
            C17610zB r1 = (C17610zB) super.clone();
            r1.A02 = (long[]) this.A02.clone();
            r1.A03 = (Object[]) this.A03.clone();
            return r1;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public String toString() {
        if (A01() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.A00 * 28);
        sb.append('{');
        for (int i = 0; i < this.A00; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(A04(i));
            sb.append('=');
            Object A06 = A06(i);
            if (A06 != this) {
                sb.append(A06);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public C17610zB() {
        this(10);
    }

    public C17610zB(int i) {
        this.A01 = false;
        if (i == 0) {
            this.A02 = AnonymousClass04d.A01;
            this.A03 = AnonymousClass04d.A02;
            return;
        }
        int A012 = AnonymousClass04d.A01(i);
        this.A02 = new long[A012];
        this.A03 = new Object[A012];
    }
}
