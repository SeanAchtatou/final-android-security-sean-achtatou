package X;

import android.os.Handler;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: X.1k5  reason: invalid class name and case insensitive filesystem */
public final class C31681k5 extends C188315h {
    private final Object A00 = new Object();
    private final ExecutorService A01 = Executors.newFixedThreadPool(4, new C17380yo());
    private volatile Handler A02;

    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0042 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(java.lang.Runnable r8) {
        /*
            r7 = this;
            android.os.Handler r0 = r7.A02
            if (r0 != 0) goto L_0x0051
            java.lang.Object r5 = r7.A00
            monitor-enter(r5)
            android.os.Handler r0 = r7.A02     // Catch:{ all -> 0x004e }
            if (r0 != 0) goto L_0x001b
            android.os.Looper r6 = android.os.Looper.getMainLooper()     // Catch:{ all -> 0x004e }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x004e }
            r0 = 28
            if (r1 < r0) goto L_0x001d
            android.os.Handler r0 = android.os.Handler.createAsync(r6)     // Catch:{ all -> 0x004e }
        L_0x0019:
            r7.A02 = r0     // Catch:{ all -> 0x004e }
        L_0x001b:
            monitor-exit(r5)     // Catch:{ all -> 0x004e }
            goto L_0x0051
        L_0x001d:
            r0 = 16
            if (r1 < r0) goto L_0x0048
            java.lang.Class<android.os.Handler> r4 = android.os.Handler.class
            java.lang.Class<android.os.Looper> r2 = android.os.Looper.class
            java.lang.Class<android.os.Handler$Callback> r1 = android.os.Handler.Callback.class
            r3 = 1
            java.lang.Class r0 = java.lang.Boolean.TYPE     // Catch:{ IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0048, InvocationTargetException -> 0x0042 }
            java.lang.Class[] r0 = new java.lang.Class[]{r2, r1, r0}     // Catch:{ IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0048, InvocationTargetException -> 0x0042 }
            java.lang.reflect.Constructor r2 = r4.getDeclaredConstructor(r0)     // Catch:{ IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0048, InvocationTargetException -> 0x0042 }
            r1 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)     // Catch:{ IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0048, InvocationTargetException -> 0x0042 }
            java.lang.Object[] r0 = new java.lang.Object[]{r6, r1, r0}     // Catch:{ IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0048, InvocationTargetException -> 0x0042 }
            java.lang.Object r0 = r2.newInstance(r0)     // Catch:{ IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0048, InvocationTargetException -> 0x0042 }
            android.os.Handler r0 = (android.os.Handler) r0     // Catch:{ IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0048, InvocationTargetException -> 0x0042 }
            goto L_0x0019
        L_0x0042:
            android.os.Handler r0 = new android.os.Handler     // Catch:{ all -> 0x004e }
            r0.<init>(r6)     // Catch:{ all -> 0x004e }
            goto L_0x0019
        L_0x0048:
            android.os.Handler r0 = new android.os.Handler     // Catch:{ all -> 0x004e }
            r0.<init>(r6)     // Catch:{ all -> 0x004e }
            goto L_0x0019
        L_0x004e:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x004e }
            throw r0
        L_0x0051:
            android.os.Handler r1 = r7.A02
            r0 = -532479415(0xffffffffe0430249, float:-5.6207496E19)
            X.AnonymousClass00S.A04(r1, r8, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31681k5.A01(java.lang.Runnable):void");
    }
}
