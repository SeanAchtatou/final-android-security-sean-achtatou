package X;

import com.facebook.common.stringformat.StringFormatUtil;

/* renamed from: X.0XH  reason: invalid class name */
public final class AnonymousClass0XH {
    public static int A00(int i) {
        if (i == 0) {
            return 0;
        }
        int i2 = 1;
        if (i != 1) {
            i2 = 2;
            if (i != 2) {
                i2 = 3;
                if (i != 3) {
                    i2 = 4;
                    if (i != 4) {
                        throw new IllegalArgumentException(StringFormatUtil.formatStrLocaleSafe("%d is not a MobileConfigParamType", Integer.valueOf(i)));
                    }
                }
            }
        }
        return i2;
    }
}
