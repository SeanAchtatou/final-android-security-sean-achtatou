package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.18j  reason: invalid class name and case insensitive filesystem */
public final class C194918j {
    private static volatile C194918j A01;
    private final C04310Tq A00;

    public static final C194918j A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C194918j.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C194918j(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public int A01() {
        String A05 = ((C30491i8) this.A00.get()).A05(AnonymousClass197.A00);
        if (A05 == null) {
            return -1;
        }
        try {
            return Integer.parseInt(A05);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    private C194918j(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0VG.A00(AnonymousClass1Y3.AUe, r2);
        AnonymousClass0WT.A00(r2);
    }
}
