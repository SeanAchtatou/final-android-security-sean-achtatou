package X;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import io.card.payment.BuildConfig;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1f7  reason: invalid class name and case insensitive filesystem */
public final class C28631f7 implements C04310Tq {
    public final /* synthetic */ C28611f5 A00;

    public C28631f7(C28611f5 r1) {
        this.A00 = r1;
    }

    public Object get() {
        String str;
        int i = AnonymousClass1Y3.Aot;
        AnonymousClass0UN r3 = this.A00.A00;
        C21514AHw aHw = (C21514AHw) AnonymousClass1XX.A02(1, i, r3);
        ExecutorService executorService = (ExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BKH, r3);
        C21515AHx aHx = new C21515AHx();
        C09400hF r1 = (C09400hF) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BMV, r3);
        aHx.A05 = r1.A02();
        aHx.A00 = r1.A01();
        C001300x r12 = (C001300x) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B3a, r3);
        aHx.A01 = r12.A04;
        aHx.A02 = r12.A06;
        AnonymousClass00M r0 = (AnonymousClass00M) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AsP, r3);
        if (r0 != null) {
            str = r0.A01;
        } else {
            str = BuildConfig.FLAVOR;
        }
        aHx.A04 = str;
        aHx.A03 = String.valueOf(((ActivityManager) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AOn, r3)).getMemoryClass());
        return new C38921yD(aHw, executorService, new C21513AHv(aHx), (Application) ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, this.A00.A00)));
    }
}
