package X;

import android.content.Context;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.facebook.proxygen.EventBase;
import com.facebook.proxygen.NetworkStatusMonitor;
import com.facebook.proxygen.NetworkType;
import com.facebook.proxygen.ProxygenRadioMeter;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1kG  reason: invalid class name and case insensitive filesystem */
public final class C31731kG extends NetworkStatusMonitor implements C16810xo {
    public C04460Ut A00;
    private int A01;
    private int A02;
    private int A03;
    private int A04;
    private AnonymousClass0Ud A05;
    public final C09340h3 A06;
    private final int A07;
    private final int A08;
    private final Context A09;
    private final AnonymousClass125 A0A;
    private final C31631k0 A0B;

    public void A01(NetworkInfo networkInfo) {
        this.A04 = -1;
        this.A03 = 0;
        NetworkType.ConnectivityType connectivityType = NetworkType.ConnectivityType.NOCONN;
        NetworkType.CellularType cellularType = NetworkType.CellularType.NOT_CELLULAR;
        if (networkInfo != null && networkInfo.isConnected()) {
            this.A04 = networkInfo.getType();
            int subtype = networkInfo.getSubtype();
            this.A03 = subtype;
            int i = this.A04;
            if (i == 0) {
                connectivityType = NetworkType.ConnectivityType.CELLULAR;
            } else if (i == 1) {
                connectivityType = NetworkType.ConnectivityType.WIFI;
            } else if (i != 6) {
                connectivityType = NetworkType.ConnectivityType.OTHER;
            } else {
                connectivityType = NetworkType.ConnectivityType.CELLULAR;
                cellularType = NetworkType.CellularType.G4;
            }
            if (i == 0) {
                switch (subtype) {
                    case 1:
                    case 2:
                    case 4:
                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    case AnonymousClass1Y3.A02:
                    case 16:
                        cellularType = NetworkType.CellularType.G2;
                        break;
                    case 3:
                    case 5:
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    case 8:
                    case Process.SIGKILL:
                    case AnonymousClass1Y3.A01:
                    case AnonymousClass1Y3.A03:
                    case 14:
                    case 15:
                        cellularType = NetworkType.CellularType.G3;
                        break;
                    case 13:
                    case Process.SIGCONT:
                        cellularType = NetworkType.CellularType.G4;
                        break;
                    case 17:
                    default:
                        cellularType = NetworkType.CellularType.UNKNOWN;
                        break;
                }
            }
        }
        setNetworkType(connectivityType.value, cellularType.value);
    }

    public void C7c(C16810xo r1) {
    }

    public boolean isEnabled() {
        return true;
    }

    private void A00() {
        try {
            if (Build.VERSION.SDK_INT >= 17) {
                this.A01 = Settings.Global.getInt(this.A09.getContentResolver(), "auto_time");
                this.A02 = Settings.Global.getInt(this.A09.getContentResolver(), "auto_time_zone");
                return;
            }
            this.A01 = Settings.System.getInt(this.A09.getContentResolver(), "auto_time");
            this.A02 = Settings.System.getInt(this.A09.getContentResolver(), "auto_time_zone");
        } catch (Exception unused) {
        }
    }

    public int AfY() {
        return this.A07;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0057, code lost:
        if (((java.lang.Long) r9.get()).longValue() >= ((long) (r3.A08 * X.AnonymousClass1Y3.A87))) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003a, code lost:
        if (((java.lang.Long) r10.get()).longValue() >= ((long) (r3.A08 * X.AnonymousClass1Y3.A87))) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass123 AvW() {
        /*
            r42 = this;
            r3 = r42
            com.facebook.proxygen.NetworkStatus r0 = r42.getNetworkStatus()
            r21 = 0
            if (r0 != 0) goto L_0x000b
            return r21
        L_0x000b:
            X.0h3 r1 = r3.A06
            com.google.common.base.Optional r10 = r1.A0G()
            X.125 r8 = r3.A0A
            long r6 = r8.A00
            r4 = -9223372036854775808
            int r1 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r1 != 0) goto L_0x014d
            com.google.common.base.Optional r9 = com.google.common.base.Optional.absent()
        L_0x001f:
            boolean r1 = r10.isPresent()
            r2 = 1
            r8 = 0
            if (r1 == 0) goto L_0x003c
            java.lang.Object r1 = r10.get()
            java.lang.Long r1 = (java.lang.Long) r1
            long r6 = r1.longValue()
            int r1 = r3.A08
            int r1 = r1 * 1000
            long r4 = (long) r1
            int r1 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            r18 = 1
            if (r1 < 0) goto L_0x003e
        L_0x003c:
            r18 = 0
        L_0x003e:
            boolean r1 = r9.isPresent()
            if (r1 == 0) goto L_0x0059
            java.lang.Object r1 = r9.get()
            java.lang.Long r1 = (java.lang.Long) r1
            long r6 = r1.longValue()
            int r1 = r3.A08
            int r1 = r1 * 1000
            long r4 = (long) r1
            int r1 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            r17 = 1
            if (r1 < 0) goto L_0x005b
        L_0x0059:
            r17 = 0
        L_0x005b:
            int r1 = r3.A04
            if (r1 != r2) goto L_0x0131
            X.0h3 r1 = r3.A06
            android.net.wifi.WifiInfo r5 = r1.A0F()
            if (r5 == 0) goto L_0x012c
            int r4 = r5.getRssi()
            int r2 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            if (r2 < r1) goto L_0x0075
            int r8 = r5.getFrequency()
        L_0x0075:
            int r16 = r5.getLinkSpeed()
            r15 = r8
            r8 = r4
        L_0x007b:
            X.0h3 r1 = r3.A06
            int r14 = r1.A0A()
        L_0x0081:
            com.facebook.proxygen.BandwidthEstimate r1 = r42.getBandwidthEstimate()
            r10 = -1
            if (r1 == 0) goto L_0x0126
            long r4 = r1.ttfbMs
            long r6 = r1.bandwidthBps
        L_0x008d:
            X.123 r19 = new X.123
            long r1 = r0.mRttAvg
            int r9 = (r1 > r10 ? 1 : (r1 == r10 ? 0 : -1))
            if (r9 == 0) goto L_0x0122
            java.lang.Long r20 = java.lang.Long.valueOf(r1)
        L_0x0099:
            r9 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            double r1 = r0.mRttStdDev
            double r9 = r9 - r1
            double r12 = java.lang.Math.abs(r9)
            r9 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r10 = java.lang.Math.ulp(r9)
            int r9 = (r12 > r10 ? 1 : (r12 == r10 ? 0 : -1))
            if (r9 <= 0) goto L_0x00b0
            java.lang.Double r21 = java.lang.Double.valueOf(r1)
        L_0x00b0:
            java.lang.Boolean r22 = java.lang.Boolean.valueOf(r18)
            java.lang.Boolean r23 = java.lang.Boolean.valueOf(r17)
            long r1 = r0.mOpenedConn
            java.lang.Long r24 = java.lang.Long.valueOf(r1)
            java.lang.Integer r25 = java.lang.Integer.valueOf(r14)
            java.lang.Integer r26 = java.lang.Integer.valueOf(r8)
            java.lang.Integer r27 = java.lang.Integer.valueOf(r15)
            java.lang.Integer r28 = java.lang.Integer.valueOf(r16)
            X.0Ud r1 = r3.A05
            boolean r1 = r1.A0G()
            java.lang.Boolean r29 = java.lang.Boolean.valueOf(r1)
            X.0Ud r1 = r3.A05
            long r1 = r1.A08()
            java.lang.Long r30 = java.lang.Long.valueOf(r1)
            X.0Ud r1 = r3.A05
            long r1 = r1.A07()
            java.lang.Long r31 = java.lang.Long.valueOf(r1)
            boolean r1 = r0.mMayHaveNetwork
            java.lang.Boolean r32 = java.lang.Boolean.valueOf(r1)
            long r1 = r0.mReqBwIngressAvg
            java.lang.Long r33 = java.lang.Long.valueOf(r1)
            double r1 = r0.mReqBwIngressStdDev
            java.lang.Double r34 = java.lang.Double.valueOf(r1)
            long r1 = r0.mReqBwEgressAvg
            java.lang.Long r35 = java.lang.Long.valueOf(r1)
            double r1 = r0.mReqBwEgressStdDev
            java.lang.Double r36 = java.lang.Double.valueOf(r1)
            java.lang.String r2 = r0.mLatencyQuality
            java.lang.String r1 = r0.mUploadBwQuality
            java.lang.String r0 = r0.mDownloadBwQuality
            java.lang.Long r40 = java.lang.Long.valueOf(r4)
            java.lang.Long r41 = java.lang.Long.valueOf(r6)
            r37 = r2
            r38 = r1
            r39 = r0
            r19.<init>(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41)
            return r19
        L_0x0122:
            r20 = r21
            goto L_0x0099
        L_0x0126:
            r4 = -1
            r6 = -1
            goto L_0x008d
        L_0x012c:
            r15 = 0
            r16 = 0
            goto L_0x007b
        L_0x0131:
            if (r1 != 0) goto L_0x014a
            X.1k0 r2 = r3.A0B
            int r1 = r3.A03
            android.telephony.CellSignalStrength r1 = r2.A09(r1)
            if (r1 == 0) goto L_0x014a
            int r14 = r1.getLevel()
            int r8 = r1.getDbm()
            r15 = 0
        L_0x0146:
            r16 = 0
            goto L_0x0081
        L_0x014a:
            r15 = 0
            r14 = 0
            goto L_0x0146
        L_0x014d:
            r4 = 3
            int r2 = X.AnonymousClass1Y3.BBa
            X.0UN r1 = r8.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.069 r1 = (X.AnonymousClass069) r1
            long r4 = r1.now()
            long r1 = r8.A00
            long r4 = r4 - r1
            java.lang.Long r1 = java.lang.Long.valueOf(r4)
            com.google.common.base.Optional r9 = com.google.common.base.Optional.of(r1)
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31731kG.AvW():X.123");
    }

    public int B9x() {
        return this.A08;
    }

    public C31731kG(Context context, EventBase eventBase, int i, int i2, AnonymousClass125 r6, C09340h3 r7, C04460Ut r8, C31631k0 r9, AnonymousClass0Ud r10, ProxygenRadioMeter proxygenRadioMeter) {
        super(eventBase);
        this.A09 = context;
        this.A08 = i;
        this.A07 = i2;
        this.A0A = r6;
        this.A06 = r7;
        this.A00 = r8;
        this.A0B = r9;
        this.A05 = r10;
        setRadioMeter(proxygenRadioMeter);
        setCalcBandwidthOnEvbThreadEnabled(false);
        this.mCacheDurationInSeconds = i2;
        init(i);
        A01(this.A06.A0E());
        A00();
    }

    public AnonymousClass126 B6E() {
        A00();
        return new AnonymousClass126(Integer.valueOf(this.A01), Integer.valueOf(this.A02));
    }
}
