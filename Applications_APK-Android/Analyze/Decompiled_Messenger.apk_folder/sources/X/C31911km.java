package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.1km  reason: invalid class name and case insensitive filesystem */
public final class C31911km implements AnonymousClass0ZM {
    public final /* synthetic */ AnonymousClass13O A00;

    public C31911km(AnonymousClass13O r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r5) {
        AnonymousClass13O r2 = this.A00;
        synchronized (r2) {
            if (r2.A01 != null) {
                r2.A01 = null;
                r2.A03.C4y("com.facebook.config.server.ACTION_SERVER_CONFIG_CHANGED");
            }
        }
    }
}
