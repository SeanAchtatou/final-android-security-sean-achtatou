package X;

import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.common.dextricks.DexStore;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.User;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Ai  reason: invalid class name and case insensitive filesystem */
public abstract class C19951Ai extends C16260wl {
    public static final ImmutableSet A05 = ImmutableSet.A05(2048, Integer.valueOf((int) DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED));
    public View A00;
    public C19961Aj A01;
    public ScheduledFuture A02;
    private ScheduledExecutorService A03;
    private final Runnable A04 = new C19971Ak(this);

    public long A0J() {
        return 1500;
    }

    public C19961Aj A0K() {
        C19941Ah r1 = (C19941Ah) this;
        r1.A00 = new LinkedHashMap();
        return new AnonymousClass16X(r1);
    }

    public CharSequence A0L() {
        C19941Ah r1 = (C19941Ah) this;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (ThreadSummary threadSummary : r1.A00.values()) {
            String str = threadSummary.A0t;
            if (str != null) {
                linkedHashSet.add(str);
            } else {
                ThreadKey threadKey = threadSummary.A0S;
                C28711fF r2 = threadKey.A05;
                if (r2 == C28711fF.ONE_TO_ONE) {
                    linkedHashSet.add(threadSummary.A04(ThreadKey.A07(threadKey)).A04.A03);
                } else if (r2 == C28711fF.GROUP) {
                    ParticipantInfo participantInfo = threadSummary.A0Q;
                    if (participantInfo != null) {
                        linkedHashSet.add(participantInfo.A03);
                    } else {
                        linkedHashSet.add(null);
                    }
                }
            }
        }
        return linkedHashSet.iterator().next() == null ? r1.A00.getResources().getQuantityString(2131689640, linkedHashSet.size()) : linkedHashSet.size() == 1 ? r1.A00.getResources().getString(2131829615, linkedHashSet.iterator().next()) : r1.A00.getResources().getQuantityString(2131689639, linkedHashSet.size() - 1, linkedHashSet.iterator().next(), Integer.valueOf(linkedHashSet.size() - 1));
    }

    public boolean A0O(Object obj) {
        C19941Ah r2 = (C19941Ah) this;
        ThreadSummary threadSummary = (ThreadSummary) obj;
        if (threadSummary == null) {
            return false;
        }
        ParticipantInfo participantInfo = threadSummary.A0Q;
        if ((participantInfo == null || !Objects.equal(participantInfo.A01, ((User) r2.A01.get()).A0Q)) && threadSummary.A0B()) {
            return !r2.A00.containsKey(threadSummary.A0S) || ((ThreadSummary) r2.A00.get(threadSummary.A0S)).A0B < threadSummary.A0B;
        }
        return false;
    }

    public static void A00(C19951Ai r5) {
        if (!r5.A01.isEmpty()) {
            ScheduledFuture scheduledFuture = r5.A02;
            if (scheduledFuture != null) {
                scheduledFuture.cancel(false);
            }
            r5.A02 = r5.A03.schedule(r5.A04, r5.A0J(), TimeUnit.MILLISECONDS);
        }
    }

    public boolean A0N(int i) {
        if (!A05.contains(Integer.valueOf(i)) || this.A01.isEmpty()) {
            return false;
        }
        return true;
    }

    public C19951Ai(RecyclerView recyclerView, ScheduledExecutorService scheduledExecutorService) {
        super(recyclerView);
        this.A03 = scheduledExecutorService;
        this.A01 = A0K();
    }

    public void A0A(View view, int i) {
        if (!A0N(i)) {
            super.A0A(view, i);
        }
    }

    public void A0C(View view, AccessibilityEvent accessibilityEvent) {
        if (!A0N(accessibilityEvent.getEventType())) {
            super.A0C(view, accessibilityEvent);
        }
    }

    public void A0M(Object obj) {
        if (A0O(obj)) {
            this.A01.add(obj);
            A00(this);
        }
    }
}
