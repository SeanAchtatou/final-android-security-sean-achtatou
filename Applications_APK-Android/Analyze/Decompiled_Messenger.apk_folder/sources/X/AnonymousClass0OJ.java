package X;

import android.util.Log;
import java.util.List;

/* renamed from: X.0OJ  reason: invalid class name */
public final class AnonymousClass0OJ {
    public Process A00;
    public List A01;
    public volatile Integer A02 = AnonymousClass07B.A00;

    public void A00() {
        synchronized (this) {
            if (this.A02 == AnonymousClass07B.A01) {
                this.A02 = AnonymousClass07B.A0C;
            } else {
                throw new IllegalStateException("Cannot stop non-running logcat process");
            }
        }
        this.A00.destroy();
    }

    public AnonymousClass0OJ(List list) {
        this.A01 = list;
    }

    public void finalize() {
        int A03 = C000700l.A03(-158468130);
        super.finalize();
        try {
            if (this.A02 == AnonymousClass07B.A01) {
                A00();
                Log.e("LogcatProcess", "child process still alive when finalize() called");
            }
        } catch (RuntimeException unused) {
        }
        C000700l.A09(-347084073, A03);
    }
}
