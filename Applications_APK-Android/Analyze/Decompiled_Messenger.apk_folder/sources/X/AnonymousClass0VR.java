package X;

import java.io.File;
import java.io.FileFilter;

/* renamed from: X.0VR  reason: invalid class name */
public final class AnonymousClass0VR implements FileFilter {
    public boolean accept(File file) {
        String name = file.getName();
        if (name.startsWith("cpu")) {
            int i = 3;
            while (i < name.length()) {
                if (Character.isDigit(name.charAt(i))) {
                    i++;
                }
            }
            return true;
        }
        return false;
    }
}
