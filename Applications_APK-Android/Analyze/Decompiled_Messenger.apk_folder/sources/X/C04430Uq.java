package X;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0Uq  reason: invalid class name and case insensitive filesystem */
public final class C04430Uq extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static final Object A02 = new Object();
    private static volatile Handler A03;
    private static volatile Looper A04;
    private static volatile AnonymousClass0VK A05;

    public static final Handler A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new Handler(A01(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final Looper A01(AnonymousClass1XY r5) {
        if (A04 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r5);
                if (A002 != null) {
                    try {
                        HandlerThread A022 = AnonymousClass0V4.A00(r5.getApplicationInjector()).A02("BackgroundBroadcastHandler", AnonymousClass0V7.NORMAL);
                        A022.start();
                        A04 = A022.getLooper();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final AnonymousClass0VK A03(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (A02) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new AnonymousClass0VI(A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final C04460Ut A02(AnonymousClass1XY r0) {
        return C04440Ur.A00(r0);
    }
}
