package X;

import android.content.Context;
import com.facebook.inject.InjectorModule;
import com.google.common.base.Preconditions;

@InjectorModule
/* renamed from: X.0VA  reason: invalid class name */
public final class AnonymousClass0VA extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile AnonymousClass0VT A01;
    private static volatile AnonymousClass0VV A02;
    private static volatile C37041uW A03;
    private static volatile C37041uW A04;

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r8.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009f, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.AnonymousClass0VT A00(X.AnonymousClass1XY r10) {
        /*
            X.0VT r0 = X.AnonymousClass0VA.A01
            if (r0 != 0) goto L_0x00a8
            java.lang.Class<X.0VT> r9 = X.AnonymousClass0VT.class
            monitor-enter(r9)
            X.0VT r0 = X.AnonymousClass0VA.A01     // Catch:{ all -> 0x00a5 }
            X.0WD r8 = X.AnonymousClass0WD.A00(r0, r10)     // Catch:{ all -> 0x00a5 }
            if (r8 == 0) goto L_0x00a3
            X.1XY r3 = r10.getApplicationInjector()     // Catch:{ all -> 0x009b }
            int r0 = X.AnonymousClass1Y3.BMP     // Catch:{ all -> 0x009b }
            X.0VB r1 = X.AnonymousClass0VB.A00(r0, r3)     // Catch:{ all -> 0x009b }
            X.0VV r7 = A01(r3)     // Catch:{ all -> 0x009b }
            X.00z r2 = X.AnonymousClass0UU.A05(r3)     // Catch:{ all -> 0x009b }
            android.content.Context r6 = X.AnonymousClass1YA.A00(r3)     // Catch:{ all -> 0x009b }
            X.00K r5 = X.AnonymousClass00J.A01(r6)     // Catch:{ all -> 0x009b }
            boolean r0 = r5.A0y     // Catch:{ all -> 0x009b }
            if (r0 == 0) goto L_0x0045
            java.lang.Object r4 = r1.get()     // Catch:{ all -> 0x009b }
            com.google.common.base.Preconditions.checkNotNull(r4)     // Catch:{ all -> 0x009b }
            X.1uW r4 = (X.C37041uW) r4     // Catch:{ all -> 0x009b }
        L_0x0036:
            boolean r3 = X.AnonymousClass0VW.A00()     // Catch:{ all -> 0x009b }
            r1 = 1
            r3 = r3 ^ r1
            X.00z r0 = X.C001500z.A01     // Catch:{ all -> 0x009b }
            if (r2 == r0) goto L_0x0048
            boolean r0 = r5.A0x     // Catch:{ all -> 0x009b }
            if (r0 != 0) goto L_0x0048
            goto L_0x0047
        L_0x0045:
            r4 = 0
            goto L_0x0036
        L_0x0047:
            r1 = 0
        L_0x0048:
            X.0VX r2 = new X.0VX     // Catch:{ all -> 0x009b }
            r2.<init>()     // Catch:{ all -> 0x009b }
            r0 = 4
            r2.A05 = r0     // Catch:{ all -> 0x009b }
            r0 = 20
            r2.A02 = r0     // Catch:{ all -> 0x009b }
            r0 = 15
            r2.A04 = r0     // Catch:{ all -> 0x009b }
            r0 = 10
            r2.A00 = r0     // Catch:{ all -> 0x009b }
            r2.A07 = r4     // Catch:{ all -> 0x009b }
            r2.A09 = r3     // Catch:{ all -> 0x009b }
            int r0 = r5.A0A     // Catch:{ all -> 0x009b }
            r2.A03 = r0     // Catch:{ all -> 0x009b }
            r2.A06 = r7     // Catch:{ all -> 0x009b }
            int r0 = r5.A08     // Catch:{ all -> 0x009b }
            r2.A01 = r0     // Catch:{ all -> 0x009b }
            boolean r0 = r5.A0w     // Catch:{ all -> 0x009b }
            r2.A08 = r0     // Catch:{ all -> 0x009b }
            r2.A0A = r1     // Catch:{ all -> 0x009b }
            X.00K r1 = X.AnonymousClass00J.A01(r6)     // Catch:{ all -> 0x009b }
            int r0 = r1.A0d     // Catch:{ all -> 0x009b }
            if (r0 <= 0) goto L_0x007a
            r2.A05 = r0     // Catch:{ all -> 0x009b }
        L_0x007a:
            int r0 = r1.A0b     // Catch:{ all -> 0x009b }
            if (r0 <= 0) goto L_0x0080
            r2.A02 = r0     // Catch:{ all -> 0x009b }
        L_0x0080:
            int r0 = r1.A0c     // Catch:{ all -> 0x009b }
            if (r0 <= 0) goto L_0x0086
            r2.A04 = r0     // Catch:{ all -> 0x009b }
        L_0x0086:
            X.0VT r3 = new X.0VT     // Catch:{ all -> 0x009b }
            r3.<init>(r2)     // Catch:{ all -> 0x009b }
            java.lang.Class<X.0Vs> r2 = X.C04680Vs.class
            monitor-enter(r2)     // Catch:{ all -> 0x009b }
            java.util.WeakHashMap r1 = X.C04680Vs.A00     // Catch:{ all -> 0x0098 }
            r0 = 0
            r1.put(r3, r0)     // Catch:{ all -> 0x0098 }
            monitor-exit(r2)     // Catch:{ all -> 0x009b }
            X.AnonymousClass0VA.A01 = r3     // Catch:{ all -> 0x009b }
            goto L_0x00a0
        L_0x0098:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x009b }
            throw r0     // Catch:{ all -> 0x009b }
        L_0x009b:
            r0 = move-exception
            r8.A01()     // Catch:{ all -> 0x00a5 }
            throw r0     // Catch:{ all -> 0x00a5 }
        L_0x00a0:
            r8.A01()     // Catch:{ all -> 0x00a5 }
        L_0x00a3:
            monitor-exit(r9)     // Catch:{ all -> 0x00a5 }
            goto L_0x00a8
        L_0x00a5:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x00a5 }
            throw r0
        L_0x00a8:
            X.0VT r0 = X.AnonymousClass0VA.A01
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VA.A00(X.1XY):X.0VT");
    }

    public static final AnonymousClass0VV A01(AnonymousClass1XY r3) {
        if (A02 == null) {
            synchronized (AnonymousClass0VV.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A02 = null;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final C37041uW A02(AnonymousClass1XY r6) {
        C37041uW r2;
        if (A03 == null) {
            synchronized (C37041uW.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        C17440yu A004 = C17440yu.A00(applicationInjector);
                        AnonymousClass00K A012 = AnonymousClass00J.A01(A003);
                        AnonymousClass00K A013 = AnonymousClass00J.A01(A003);
                        if (A012.A0y || A013.A0v) {
                            r2 = new C37041uW(A004, 100, AnonymousClass1Y3.A87);
                        } else {
                            r2 = null;
                        }
                        A03 = r2;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final C37041uW A03(AnonymousClass1XY r5) {
        C37041uW r0;
        if (A04 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        AnonymousClass0VB A003 = AnonymousClass0VB.A00(AnonymousClass1Y3.BMP, applicationInjector);
                        if (!AnonymousClass00J.A01(AnonymousClass1YA.A00(applicationInjector)).A0v) {
                            r0 = null;
                        } else {
                            Object obj = A003.get();
                            Preconditions.checkNotNull(obj);
                            r0 = (C37041uW) obj;
                        }
                        A04 = r0;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }
}
