package X;

/* renamed from: X.17j  reason: invalid class name and case insensitive filesystem */
public final class C192417j {
    public final /* synthetic */ C07380dK A00;
    public final /* synthetic */ String A01;

    public C192417j(C07380dK r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0022, code lost:
        if (r1.isEmpty() != false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        r6 = new com.fasterxml.jackson.databind.node.ObjectNode(com.fasterxml.jackson.databind.node.JsonNodeFactory.instance);
        r7 = r1.entrySet().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0037, code lost:
        if (r7.hasNext() == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        r0 = (java.util.Map.Entry) r7.next();
        r4 = (X.C193417t) r0.getValue();
        r3 = new com.fasterxml.jackson.databind.node.ObjectNode(com.fasterxml.jackson.databind.node.JsonNodeFactory.instance);
        r3.put("count", r4.A00);
        r3.put("sum", r4.A02);
        r3.put("s_sum", r4.A01);
        r6.put((java.lang.String) r0.getKey(), r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006b, code lost:
        return r6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fasterxml.jackson.databind.JsonNode A00(boolean r9) {
        /*
            r8 = this;
            if (r9 == 0) goto L_0x006f
            X.0dK r0 = r8.A00
            java.lang.String r1 = r8.A01
            java.util.Map r2 = r0.A01
            monitor-enter(r2)
            java.util.Map r0 = r0.A01     // Catch:{ all -> 0x006c }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x006c }
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ all -> 0x006c }
            r6 = 0
            if (r0 != 0) goto L_0x0016
            monitor-exit(r2)     // Catch:{ all -> 0x006c }
            return r6
        L_0x0016:
            com.google.common.collect.ImmutableMap r1 = com.google.common.collect.ImmutableMap.copyOf(r0)     // Catch:{ all -> 0x006c }
            r0.clear()     // Catch:{ all -> 0x006c }
            monitor-exit(r2)     // Catch:{ all -> 0x006c }
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x006b
            com.fasterxml.jackson.databind.node.ObjectNode r6 = new com.fasterxml.jackson.databind.node.ObjectNode
            com.fasterxml.jackson.databind.node.JsonNodeFactory r0 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance
            r6.<init>(r0)
            com.google.common.collect.ImmutableSet r0 = r1.entrySet()
            X.1Xv r7 = r0.iterator()
        L_0x0033:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x006b
            java.lang.Object r0 = r7.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r5 = r0.getKey()
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r4 = r0.getValue()
            X.17t r4 = (X.C193417t) r4
            com.fasterxml.jackson.databind.node.ObjectNode r3 = new com.fasterxml.jackson.databind.node.ObjectNode
            com.fasterxml.jackson.databind.node.JsonNodeFactory r0 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance
            r3.<init>(r0)
            int r1 = r4.A00
            java.lang.String r0 = "count"
            r3.put(r0, r1)
            long r1 = r4.A02
            java.lang.String r0 = "sum"
            r3.put(r0, r1)
            long r1 = r4.A01
            java.lang.String r0 = "s_sum"
            r3.put(r0, r1)
            r6.put(r5, r3)
            goto L_0x0033
        L_0x006b:
            return r6
        L_0x006c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x006c }
            goto L_0x008e
        L_0x006f:
            X.0dK r3 = r8.A00
            java.lang.String r2 = r8.A01
            java.util.Map r1 = r3.A01
            monitor-enter(r1)
            java.util.Map r0 = r3.A01     // Catch:{ all -> 0x008c }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x008c }
            if (r0 == 0) goto L_0x0089
            java.util.Map r0 = r3.A01     // Catch:{ all -> 0x008c }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x008c }
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ all -> 0x008c }
            r0.clear()     // Catch:{ all -> 0x008c }
        L_0x0089:
            monitor-exit(r1)     // Catch:{ all -> 0x008c }
            r0 = 0
            return r0
        L_0x008c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x008c }
        L_0x008e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C192417j.A00(boolean):com.fasterxml.jackson.databind.JsonNode");
    }
}
