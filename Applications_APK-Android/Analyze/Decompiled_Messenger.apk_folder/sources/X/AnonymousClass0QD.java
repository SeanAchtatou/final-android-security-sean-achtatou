package X;

import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.0QD  reason: invalid class name */
public final class AnonymousClass0QD {
    public static void A00(int i) {
        Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 23, 0, 0, i, 0, 0);
        ADV.A00();
    }

    public static void A01(String str, int i) {
        if (TraceEvents.isEnabled(AnonymousClass00n.A08)) {
            Logger.writeBytesEntry(AnonymousClass00n.A08, 1, 83, Logger.writeStandardEntry(AnonymousClass00n.A08, 7, 22, 0, 0, i, 0, 0), str);
        }
        ADV.A01(str);
    }
}
