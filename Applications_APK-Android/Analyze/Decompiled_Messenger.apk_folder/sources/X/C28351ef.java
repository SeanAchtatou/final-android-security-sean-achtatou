package X;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$BigDecimalDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$BigIntegerDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$BooleanDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$ByteDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$CharacterDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$DoubleDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$FloatDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$IntegerDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$LongDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$NumberDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers$ShortDeserializer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;

/* renamed from: X.1ef  reason: invalid class name and case insensitive filesystem */
public final class C28351ef {
    private static final HashSet _classNames = new HashSet();

    static {
        Class[] clsArr = {Boolean.class, Byte.class, Short.class, Character.class, Integer.class, Long.class, Float.class, Double.class, Number.class, BigDecimal.class, BigInteger.class};
        for (int i = 0; i < 11; i++) {
            _classNames.add(clsArr[i].getName());
        }
    }

    public static JsonDeserializer find(Class cls, String str) {
        if (cls.isPrimitive()) {
            if (cls == Integer.TYPE) {
                return NumberDeserializers$IntegerDeserializer.primitiveInstance;
            }
            if (cls == Boolean.TYPE) {
                return NumberDeserializers$BooleanDeserializer.primitiveInstance;
            }
            if (cls == Long.TYPE) {
                return NumberDeserializers$LongDeserializer.primitiveInstance;
            }
            if (cls == Double.TYPE) {
                return NumberDeserializers$DoubleDeserializer.primitiveInstance;
            }
            if (cls == Character.TYPE) {
                return NumberDeserializers$CharacterDeserializer.primitiveInstance;
            }
            if (cls == Byte.TYPE) {
                return NumberDeserializers$ByteDeserializer.primitiveInstance;
            }
            if (cls == Short.TYPE) {
                return NumberDeserializers$ShortDeserializer.primitiveInstance;
            }
            if (cls == Float.TYPE) {
                return NumberDeserializers$FloatDeserializer.primitiveInstance;
            }
        } else if (!_classNames.contains(str)) {
            return null;
        } else {
            if (cls == Integer.class) {
                return NumberDeserializers$IntegerDeserializer.wrapperInstance;
            }
            if (cls == Boolean.class) {
                return NumberDeserializers$BooleanDeserializer.wrapperInstance;
            }
            if (cls == Long.class) {
                return NumberDeserializers$LongDeserializer.wrapperInstance;
            }
            if (cls == Double.class) {
                return NumberDeserializers$DoubleDeserializer.wrapperInstance;
            }
            if (cls == Character.class) {
                return NumberDeserializers$CharacterDeserializer.wrapperInstance;
            }
            if (cls == Byte.class) {
                return NumberDeserializers$ByteDeserializer.wrapperInstance;
            }
            if (cls == Short.class) {
                return NumberDeserializers$ShortDeserializer.wrapperInstance;
            }
            if (cls == Float.class) {
                return NumberDeserializers$FloatDeserializer.wrapperInstance;
            }
            if (cls == Number.class) {
                return NumberDeserializers$NumberDeserializer.instance;
            }
            if (cls == BigDecimal.class) {
                return NumberDeserializers$BigDecimalDeserializer.instance;
            }
            if (cls == BigInteger.class) {
                return NumberDeserializers$BigIntegerDeserializer.instance;
            }
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0J("Internal error: can't find deserializer for ", cls.getName()));
    }
}
