package X;

import android.os.RemoteException;
import com.facebook.fbservice.service.ICompletionHandler;
import com.facebook.fbservice.service.OperationResult;
import java.util.List;

/* renamed from: X.0ld  reason: invalid class name and case insensitive filesystem */
public final class C11030ld {
    public final /* synthetic */ C11020lc A00;
    public final /* synthetic */ C11000la A01;

    public C11030ld(C11000la r1, C11020lc r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void A00(OperationResult operationResult) {
        C11000la r2 = this.A01;
        C11020lc r0 = this.A00;
        synchronized (r2) {
            List<ICompletionHandler> list = r0.A06;
            if (list != null) {
                for (ICompletionHandler BhG : list) {
                    try {
                        BhG.BhG(operationResult);
                    } catch (RemoteException unused) {
                    }
                }
            }
        }
    }
}
