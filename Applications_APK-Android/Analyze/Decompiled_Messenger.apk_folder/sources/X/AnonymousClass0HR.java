package X;

import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.os.RemoteException;
import java.util.Arrays;

/* renamed from: X.0HR  reason: invalid class name */
public final class AnonymousClass0HR extends C02540Fi {
    private final NetworkStats.Bucket A00 = new NetworkStats.Bucket();
    private final NetworkStatsManager A01;

    public boolean A02() {
        return true;
    }

    private void A01(long[] jArr, int i, int i2, long j, long j2) {
        NetworkStats querySummary = this.A01.querySummary(i, null, j, j2);
        while (querySummary.hasNextBucket()) {
            querySummary.getNextBucket(this.A00);
            int i3 = 4;
            if (this.A00.getState() == 2) {
                i3 = 0;
            }
            int i4 = i2 | 0 | i3;
            jArr[i4] = jArr[i4] + this.A00.getRxBytes();
            int i5 = i3 | i2 | 1;
            jArr[i5] = jArr[i5] + this.A00.getTxBytes();
        }
        querySummary.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(long[], long):void}
     arg types: [long[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(long[], long):void} */
    public boolean A03(long[] jArr) {
        long[] jArr2 = jArr;
        try {
            Arrays.fill(jArr, 0L);
            A01(jArr2, 0, 2, Long.MIN_VALUE, Long.MAX_VALUE);
            A01(jArr2, 1, 0, Long.MIN_VALUE, Long.MAX_VALUE);
            return true;
        } catch (RemoteException | IllegalArgumentException | NullPointerException e) {
            AnonymousClass0KZ.A00("NetworkStatsManagerBytesCollector", "Unable to get bytes transferred", e);
            return false;
        }
    }

    public AnonymousClass0HR(Context context) {
        this.A01 = (NetworkStatsManager) context.getSystemService(NetworkStatsManager.class);
    }
}
