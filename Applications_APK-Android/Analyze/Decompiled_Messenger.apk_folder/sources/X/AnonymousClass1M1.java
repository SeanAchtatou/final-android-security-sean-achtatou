package X;

/* renamed from: X.1M1  reason: invalid class name */
public final /* synthetic */ class AnonymousClass1M1 {
    public static final /* synthetic */ int[] A00;

    /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0024 */
    static {
        /*
            android.widget.ImageView$ScaleType[] r0 = android.widget.ImageView.ScaleType.values()
            int r0 = r0.length
            int[] r2 = new int[r0]
            X.AnonymousClass1M1.A00 = r2
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_XY     // Catch:{ NoSuchFieldError -> 0x0012 }
            int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
            r0 = 1
            r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0012 }
        L_0x0012:
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_START     // Catch:{ NoSuchFieldError -> 0x001b }
            int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x001b }
            r0 = 2
            r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x001b }
        L_0x001b:
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_CENTER     // Catch:{ NoSuchFieldError -> 0x0024 }
            int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0024 }
            r0 = 3
            r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0024 }
        L_0x0024:
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_END     // Catch:{ NoSuchFieldError -> 0x002d }
            int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x002d }
            r0 = 4
            r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x002d }
        L_0x002d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1M1.<clinit>():void");
    }
}
