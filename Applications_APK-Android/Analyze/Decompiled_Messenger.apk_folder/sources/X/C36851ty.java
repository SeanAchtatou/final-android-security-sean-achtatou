package X;

import java.nio.ByteBuffer;

/* renamed from: X.1ty  reason: invalid class name and case insensitive filesystem */
public final class C36851ty extends C06220b5 {
    public AnonymousClass2UX A06() {
        AnonymousClass2UX r2 = new AnonymousClass2UX();
        int A02 = A02(6);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public C36861tz A07() {
        C36861tz r2 = new C36861tz();
        int A02 = A02(4);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }
}
