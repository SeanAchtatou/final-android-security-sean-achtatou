package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0U1  reason: invalid class name */
public final class AnonymousClass0U1 implements C24891Xn {
    public final Context A00;
    private final AnonymousClass1XX A01;
    private final C24901Xo A02 = new C24901Xo(this.A01, this);

    public C24811Xe A00() {
        C24811Xe injectorThreadStack = this.A01.getInjectorThreadStack();
        injectorThreadStack.A01.add(injectorThreadStack.A00);
        injectorThreadStack.A02.add(this.A02);
        return injectorThreadStack;
    }

    public C04310Tq C4b(C22916BKm bKm, C04310Tq r3) {
        return new EIF(this, r3);
    }

    public AnonymousClass0U1(AnonymousClass1XX r3) {
        this.A01 = r3;
        this.A00 = r3.getInjectorThreadStack().A00();
    }
}
