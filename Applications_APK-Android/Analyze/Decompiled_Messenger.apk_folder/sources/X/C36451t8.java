package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1t8  reason: invalid class name and case insensitive filesystem */
public final class C36451t8 {
    private static volatile C36451t8 A01;
    public final AnonymousClass1ZE A00;

    public static final C36451t8 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C36451t8.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C36451t8(AnonymousClass1ZD.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C36451t8(AnonymousClass1ZE r1) {
        this.A00 = r1;
    }
}
