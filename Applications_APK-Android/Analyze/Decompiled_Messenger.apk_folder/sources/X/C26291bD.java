package X;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/* renamed from: X.1bD  reason: invalid class name and case insensitive filesystem */
public final class C26291bD {
    private static int A00 = -1;

    private static int[] A04(int i) {
        int i2;
        int i3;
        try {
            File file = new File(A03(false, i));
            if (!file.exists()) {
                i2 = -2;
            } else {
                try {
                    i2 = Integer.parseInt(A02(file));
                } catch (NumberFormatException unused) {
                    i2 = -1;
                }
            }
            File file2 = new File(A03(true, i));
            if (!file2.exists()) {
                i3 = -2;
            } else {
                try {
                    i3 = Integer.parseInt(A02(file2));
                } catch (NumberFormatException unused2) {
                    i3 = -1;
                }
            }
            return new int[]{i2, i3};
        } catch (IOException unused3) {
            return new int[]{-1, -1};
        }
    }

    public static int A00() {
        int i;
        int i2 = A00;
        if (i2 == -1 || i2 == -2) {
            File file = new File("/sys/devices/system/cpu/possible");
            if (!file.exists()) {
                i = -2;
            } else {
                try {
                    String A02 = A02(file);
                    int indexOf = A02.indexOf(45);
                    if (indexOf != -1) {
                        i = Integer.parseInt(A02.substring(indexOf + 1)) + 1;
                    } else {
                        i = Integer.parseInt(A02) + 1;
                    }
                } catch (Exception unused) {
                    i = -1;
                }
            }
            A00 = i;
        }
        return A00;
    }

    public static C26311bF A01() {
        C26301bE r2;
        C26301bE r8 = new C26301bE();
        int A002 = A00();
        if (A002 > 0) {
            if (A002 == 1) {
                int[] A04 = A04(0);
                C26311bF r0 = r8.A00;
                r0.A02 = 1;
                r0.A0B = A04;
            } else {
                int i = A002 - 1;
                int[] A042 = A04(0);
                int i2 = i - 1;
                int[] A043 = A04(i);
                int i3 = 1;
                while (i3 < i2) {
                    int i4 = A042[0];
                    if (i4 >= 0 && A043[0] >= 0) {
                        break;
                    }
                    if (i4 < 0) {
                        A042 = A04(i3);
                        i3++;
                    }
                    if (A043[0] < 0) {
                        A043 = A04(i2);
                        i2--;
                    }
                }
                int i5 = A042[0];
                if (i5 < 0 && A043[0] < 0) {
                    C26311bF r02 = r8.A00;
                    r02.A02 = A002;
                    r02.A0B = new int[]{-1, -1};
                } else if (i5 < 0) {
                    C26311bF r03 = r8.A00;
                    r03.A02 = A002;
                    r03.A0B = A043;
                } else {
                    if (A043[0] >= 0) {
                        int i6 = A042[1];
                        int i7 = A043[1];
                        boolean z = false;
                        if (i6 != i7) {
                            z = true;
                        }
                        if (z) {
                            int i8 = A002 >> 1;
                            if (i6 > i7) {
                                int i9 = i8;
                                int i10 = 0;
                                if (i3 - 1 > i2 + 1) {
                                    i10 = i8;
                                    i9 = 0;
                                }
                                int[] iArr = {i10, i9};
                                r2 = new C26301bE();
                                r2.A01(i8, A042, iArr[0]);
                                r2.A02(i8, A043, iArr[1]);
                            } else {
                                int i11 = i8;
                                int i12 = 0;
                                if (i2 + 1 > i3 - 1) {
                                    i12 = i8;
                                    i11 = 0;
                                }
                                int[] iArr2 = {i12, i11};
                                r2 = new C26301bE();
                                r2.A01(i8, A043, iArr2[0]);
                                r2.A02(i8, A042, iArr2[1]);
                            }
                            return r2.A00();
                        }
                    }
                    C26311bF r04 = r8.A00;
                    r04.A02 = A002;
                    r04.A0B = A042;
                }
            }
        }
        return r8.A00();
    }

    private static String A02(File file) {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        try {
            return bufferedReader.readLine();
        } finally {
            bufferedReader.close();
        }
    }

    private static String A03(boolean z, int i) {
        StringBuilder sb = new StringBuilder("/sys/devices/system/cpu/cpu");
        sb.append(i);
        if (z) {
            sb.append("/cpufreq/cpuinfo_max_freq");
        } else {
            sb.append("/cpufreq/cpuinfo_min_freq");
        }
        return sb.toString();
    }
}
