package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tR  reason: invalid class name and case insensitive filesystem */
public final class C36621tR extends C36471tB {
    public static final Class A03 = C36621tR.class;
    private static volatile C36621tR A04;
    public final AnonymousClass06A A00;
    public final AnonymousClass18N A01;
    public final C04310Tq A02;

    private C36621tR(AnonymousClass18N r2, AnonymousClass06A r3, C04310Tq r4, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        super(13, deprecatedAnalyticsLogger);
        this.A01 = r2;
        this.A00 = r3;
        this.A02 = r4;
    }

    public static final C36621tR A00(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (C36621tR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A04 = new C36621tR(AnonymousClass18N.A00(applicationInjector), AnonymousClass067.A0A(applicationInjector), C10580kT.A04(applicationInjector), C06920cI.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }
}
