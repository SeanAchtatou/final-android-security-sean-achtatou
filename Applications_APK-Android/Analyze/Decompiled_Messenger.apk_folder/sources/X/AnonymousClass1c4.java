package X;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.BuilderBasedDeserializer;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1c4  reason: invalid class name */
public final class AnonymousClass1c4 extends AnonymousClass1c5 implements Serializable {
    public static final Class[] INIT_CAUSE_PARAMS = {Throwable.class};
    private static final Class[] NO_VIEWS = new Class[0];
    public static final AnonymousClass1c4 instance = new AnonymousClass1c4(new AnonymousClass1c8());
    private static final long serialVersionUID = 1;

    private CZw constructSettableProperty(C26791c3 r11, C10120ja r12, C29111ft r13, Type type) {
        CZw ca6;
        C29111ft r5 = r13;
        C183512m mutator = r13.getMutator();
        if (r11.getConfig().canOverrideAccessModifiers()) {
            C29081fq.checkAndFixAccess(mutator.getMember());
        }
        C10030jR resolveType = r12.resolveType(type);
        r13.getName();
        r13.getWrapperName();
        r12.getClassAnnotations();
        r13.isRequired();
        C10030jR resolveType2 = resolveType(r11, r12, resolveType, mutator);
        JsonDeserializer findDeserializerFromAnnotation = AnonymousClass1c5.findDeserializerFromAnnotation(r11, mutator);
        C10030jR modifyTypeByAnnotation = AnonymousClass1c5.modifyTypeByAnnotation(r11, mutator, resolveType2);
        C64433Bp r7 = (C64433Bp) modifyTypeByAnnotation.getTypeHandler();
        if (mutator instanceof C29141fw) {
            ca6 = new C25118Ca8(r5, modifyTypeByAnnotation, r7, r12.getClassAnnotations(), (C29141fw) mutator);
        } else {
            ca6 = new C25116Ca6(r5, modifyTypeByAnnotation, r7, r12.getClassAnnotations(), (C29091fr) mutator);
        }
        if (findDeserializerFromAnnotation != null) {
            ca6 = ca6.withValueDeserializer(findDeserializerFromAnnotation);
        }
        C860046h findReferenceType = r13.findReferenceType();
        if (findReferenceType != null) {
            boolean z = false;
            if (findReferenceType._type == AnonymousClass46g.MANAGED_REFERENCE) {
                z = true;
            }
            if (z) {
                ca6._managedReferenceName = findReferenceType._name;
            }
        }
        return ca6;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:74:0x01ae */
    /* JADX WARN: Type inference failed for: r0v20, types: [X.CZw] */
    /* JADX WARN: Type inference failed for: r0v26, types: [X.CZw] */
    /* JADX WARN: Type inference failed for: r0v35 */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00db, code lost:
        if (r4.getConfig().isEnabled(X.C26771bz.AUTO_DETECT_GETTERS) == false) goto L_0x00dd;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0186 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01de  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addBeanProps(X.C26791c3 r23, X.C10120ja r24, X.C25113CZz r25) {
        /*
            r22 = this;
            r6 = r25
            X.3Bx r1 = r6._valueInstantiator
            r4 = r23
            X.0kF r0 = r4._config
            X.CZw[] r3 = r1.getFromObjectArguments(r0)
            X.0jc r1 = r4.getAnnotationIntrospector()
            r5 = r24
            X.0jV r0 = r5.getClassInfo()
            java.lang.Boolean r0 = r1.findIgnoreUnknownProperties(r0)
            if (r0 == 0) goto L_0x0022
            boolean r0 = r0.booleanValue()
            r6._ignoreAllUnknown = r0
        L_0x0022:
            X.0jV r0 = r5.getClassInfo()
            java.lang.String[] r0 = r1.findPropertiesToIgnore(r0)
            java.util.HashSet r1 = X.C11830o1.arrayToSet(r0)
            java.util.Iterator r2 = r1.iterator()
        L_0x0032:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0042
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            r6.addIgnorable(r0)
            goto L_0x0032
        L_0x0042:
            X.1fw r9 = r5.findAnySetter()
            r7 = r22
            if (r9 == 0) goto L_0x00a7
            X.0kA r0 = r4.getConfig()
            boolean r0 = r0.canOverrideAccessModifiers()
            if (r0 == 0) goto L_0x005b
            java.lang.reflect.Member r0 = r9.getMember()
            X.C29081fq.checkAndFixAccess(r0)
        L_0x005b:
            X.1eb r8 = r5.bindingsForBeanType()
            r0 = 1
            java.lang.reflect.Type r2 = r9.getGenericParameterType(r0)
            X.0js r0 = r8._typeFactory
            X.0jR r13 = r0._constructType(r2, r8)
            X.CaI r11 = new X.CaI
            java.lang.String r12 = r9.getName()
            r14 = 0
            X.0jY r15 = r5.getClassAnnotations()
            r17 = 0
            r16 = r9
            r11.<init>(r12, r13, r14, r15, r16, r17)
            X.0jR r10 = r7.resolveType(r4, r5, r13, r9)
            com.fasterxml.jackson.databind.JsonDeserializer r2 = X.AnonymousClass1c5.findDeserializerFromAnnotation(r4, r9)
            if (r2 == 0) goto L_0x0099
            X.Ca1 r8 = new X.Ca1
            java.lang.reflect.Method r0 = r9._method
            r8.<init>(r11, r0, r10, r2)
        L_0x008d:
            X.Ca1 r0 = r6._anySetter
            if (r0 == 0) goto L_0x00a5
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "_anySetter already set to non-null"
            r1.<init>(r0)
            throw r1
        L_0x0099:
            X.0jR r10 = X.AnonymousClass1c5.modifyTypeByAnnotation(r4, r9, r10)
            X.Ca1 r8 = new X.Ca1
            java.lang.reflect.Method r0 = r9._method
            r8.<init>(r11, r0, r10, r14)
            goto L_0x008d
        L_0x00a5:
            r6._anySetter = r8
        L_0x00a7:
            if (r9 != 0) goto L_0x00c3
            java.util.Set r0 = r5.getIgnoredPropertyNames()
            if (r0 == 0) goto L_0x00c3
            java.util.Iterator r2 = r0.iterator()
        L_0x00b3:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00c3
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            r6.addIgnorable(r0)
            goto L_0x00b3
        L_0x00c3:
            X.1bz r2 = X.C26771bz.USE_GETTERS_AS_SETTERS
            X.0kA r0 = r4.getConfig()
            boolean r0 = r0.isEnabled(r2)
            r10 = 0
            if (r0 == 0) goto L_0x00dd
            X.1bz r2 = X.C26771bz.AUTO_DETECT_GETTERS
            X.0kA r0 = r4.getConfig()
            boolean r0 = r0.isEnabled(r2)
            r15 = 1
            if (r0 != 0) goto L_0x00de
        L_0x00dd:
            r15 = 0
        L_0x00de:
            java.util.List r9 = r5.findProperties()
            java.util.ArrayList r2 = new java.util.ArrayList
            int r8 = r9.size()
            r0 = 4
            int r0 = java.lang.Math.max(r0, r8)
            r2.<init>(r0)
            java.util.HashMap r12 = new java.util.HashMap
            r12.<init>()
            java.util.Iterator r14 = r9.iterator()
        L_0x00f9:
            boolean r0 = r14.hasNext()
            if (r0 == 0) goto L_0x0165
            java.lang.Object r11 = r14.next()
            X.1ft r11 = (X.C29111ft) r11
            java.lang.String r9 = r11.getName()
            boolean r0 = r1.contains(r9)
            if (r0 != 0) goto L_0x00f9
            boolean r0 = r11.hasConstructorParameter()
            if (r0 != 0) goto L_0x0161
            r13 = 0
            boolean r0 = r11.hasSetter()
            if (r0 == 0) goto L_0x0152
            X.1fw r8 = r11.getSetter()
            java.lang.Class r13 = r8.getRawParameterType(r10)
        L_0x0124:
            if (r13 == 0) goto L_0x0161
            X.0kF r8 = r4._config
            java.lang.Object r0 = r12.get(r13)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 != 0) goto L_0x0148
            X.0jR r0 = r8.constructType(r13)
            X.0ja r0 = r8.introspectClassAnnotations(r0)
            X.0jc r8 = r8.getAnnotationIntrospector()
            X.0jV r0 = r0.getClassInfo()
            java.lang.Boolean r0 = r8.isIgnorableType(r0)
            if (r0 != 0) goto L_0x0148
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
        L_0x0148:
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0161
            r6.addIgnorable(r9)
            goto L_0x00f9
        L_0x0152:
            boolean r0 = r11.hasField()
            if (r0 == 0) goto L_0x0124
            X.1fr r0 = r11.getField()
            java.lang.Class r13 = r0.getRawType()
            goto L_0x0124
        L_0x0161:
            r2.add(r11)
            goto L_0x00f9
        L_0x0165:
            X.1c8 r1 = r7._factoryConfig
            boolean r0 = r1.hasDeserializerModifiers()
            if (r0 == 0) goto L_0x0182
            X.1c9[] r1 = r1._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x0178:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0182
            r1.next()
            goto L_0x0178
        L_0x0182:
            java.util.Iterator r12 = r2.iterator()
        L_0x0186:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x0289
            java.lang.Object r9 = r12.next()
            X.1ft r9 = (X.C29111ft) r9
            r0 = 0
            boolean r1 = r9.hasConstructorParameter()
            if (r1 == 0) goto L_0x01ca
            java.lang.String r8 = r9.getName()
            if (r3 == 0) goto L_0x01ae
            int r11 = r3.length
            r9 = 0
        L_0x01a1:
            if (r9 >= r11) goto L_0x01ae
            r2 = r3[r9]
            java.lang.String r1 = r2._propName
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x01c7
            r0 = r2
        L_0x01ae:
            if (r0 != 0) goto L_0x01f7
            java.lang.String r3 = "Could not find creator property with name '"
            java.lang.String r2 = "' (in class "
            X.0jR r0 = r5._type
            java.lang.Class r0 = r0._class
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = ")"
            java.lang.String r0 = X.AnonymousClass08S.A0T(r3, r8, r2, r1, r0)
            X.1w6 r0 = r4.mappingException(r0)
            throw r0
        L_0x01c7:
            int r9 = r9 + 1
            goto L_0x01a1
        L_0x01ca:
            boolean r1 = r9.hasSetter()
            if (r1 == 0) goto L_0x0216
            X.1fw r0 = r9.getSetter()
            java.lang.reflect.Type r0 = r0.getGenericParameterType(r10)
        L_0x01d8:
            X.CZw r0 = r7.constructSettableProperty(r4, r5, r9, r0)
        L_0x01dc:
            if (r0 == 0) goto L_0x0186
            java.lang.Class[] r8 = r9.findViews()
            if (r8 != 0) goto L_0x01f2
            X.1bz r2 = X.C26771bz.DEFAULT_VIEW_INCLUSION
            X.0kA r1 = r4.getConfig()
            boolean r1 = r1.isEnabled(r2)
            if (r1 != 0) goto L_0x01f2
            java.lang.Class[] r8 = X.AnonymousClass1c4.NO_VIEWS
        L_0x01f2:
            if (r8 != 0) goto L_0x01fb
            r1 = 0
            r0._viewMatcher = r1
        L_0x01f7:
            r6.addProperty(r0)
            goto L_0x0186
        L_0x01fb:
            if (r8 == 0) goto L_0x0213
            int r2 = r8.length
            if (r2 == 0) goto L_0x0213
            r1 = 1
            if (r2 == r1) goto L_0x020b
            X.Caa r2 = new X.Caa
            r2.<init>(r8)
        L_0x0208:
            r0._viewMatcher = r2
            goto L_0x01f7
        L_0x020b:
            X.Cab r2 = new X.Cab
            r1 = r8[r10]
            r2.<init>(r1)
            goto L_0x0208
        L_0x0213:
            X.Cac r2 = X.C25143Cac.instance
            goto L_0x0208
        L_0x0216:
            boolean r1 = r9.hasField()
            if (r1 == 0) goto L_0x0225
            X.1fr r0 = r9.getField()
            java.lang.reflect.Type r0 = r0.getGenericType()
            goto L_0x01d8
        L_0x0225:
            if (r15 == 0) goto L_0x01dc
            boolean r1 = r9.hasGetter()
            if (r1 == 0) goto L_0x01dc
            X.1fw r1 = r9.getGetter()
            java.lang.Class r2 = r1.getRawType()
            java.lang.Class<java.util.Collection> r1 = java.util.Collection.class
            boolean r1 = r1.isAssignableFrom(r2)
            if (r1 != 0) goto L_0x0245
            java.lang.Class<java.util.Map> r1 = java.util.Map.class
            boolean r1 = r1.isAssignableFrom(r2)
            if (r1 == 0) goto L_0x01dc
        L_0x0245:
            X.1fw r8 = r9.getGetter()
            X.0kA r0 = r4.getConfig()
            boolean r0 = r0.canOverrideAccessModifiers()
            if (r0 == 0) goto L_0x025a
            java.lang.reflect.Member r0 = r8.getMember()
            X.C29081fq.checkAndFixAccess(r0)
        L_0x025a:
            X.1eb r0 = r5.bindingsForBeanType()
            X.0jR r0 = r8.getType(r0)
            com.fasterxml.jackson.databind.JsonDeserializer r2 = X.AnonymousClass1c5.findDeserializerFromAnnotation(r4, r8)
            X.0jR r18 = X.AnonymousClass1c5.modifyTypeByAnnotation(r4, r8, r0)
            java.lang.Object r1 = r18.getTypeHandler()
            X.3Bp r1 = (X.C64433Bp) r1
            X.Ca4 r0 = new X.Ca4
            X.0jY r20 = r5.getClassAnnotations()
            r21 = r8
            r16 = r0
            r17 = r9
            r19 = r1
            r16.<init>(r17, r18, r19, r20, r21)
            if (r2 == 0) goto L_0x01dc
            X.CZw r0 = r0.withValueDeserializer(r2)
            goto L_0x01dc
        L_0x0289:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1c4.addBeanProps(X.1c3, X.0ja, X.CZz):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:171:0x028d, code lost:
        if (r3.startsWith("org.hibernate.proxy.") != false) goto L_0x028f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x0309, code lost:
        if (r2.canCreateFromBoolean() != false) goto L_0x030b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x029e  */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x033a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fasterxml.jackson.databind.JsonDeserializer createBeanDeserializer(X.C26791c3 r8, X.C10030jR r9, X.C10120ja r10) {
        /*
            r7 = this;
            X.0kF r2 = r8._config
            X.1c8 r0 = r7._factoryConfig
            X.1c7[] r1 = r0._additionalDeserializers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x000f:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0024
            java.lang.Object r0 = r1.next()
            X.1c7 r0 = (X.AnonymousClass1c7) r0
            com.fasterxml.jackson.databind.JsonDeserializer r0 = r0.findBeanDeserializer(r9, r2, r10)
            if (r0 == 0) goto L_0x000f
        L_0x0021:
            if (r0 == 0) goto L_0x0026
            return r0
        L_0x0024:
            r0 = 0
            goto L_0x0021
        L_0x0026:
            java.lang.Class<java.lang.Throwable> r1 = java.lang.Throwable.class
            java.lang.Class r0 = r9._class
            boolean r0 = r1.isAssignableFrom(r0)
            if (r0 == 0) goto L_0x00c6
            X.CZz r3 = new X.CZz
            X.0kF r0 = r8._config
            r3.<init>(r10, r0)
            X.3Bx r0 = r7.findValueInstantiator(r8, r10)
            r3._valueInstantiator = r0
            r7.addBeanProps(r8, r10, r3)
            java.lang.Class[] r1 = X.AnonymousClass1c4.INIT_CAUSE_PARAMS
            java.lang.String r0 = "initCause"
            X.1fw r4 = r10.findMethod(r0, r1)
            if (r4 == 0) goto L_0x0068
            X.0kF r0 = r8._config
            java.lang.String r2 = "cause"
            X.CZY r1 = new X.CZY
            if (r0 != 0) goto L_0x0094
            r0 = 0
        L_0x0053:
            r1.<init>(r4, r2, r0)
            r0 = 0
            java.lang.reflect.Type r0 = r4.getGenericParameterType(r0)
            X.CZw r2 = r7.constructSettableProperty(r8, r10, r1, r0)
            if (r2 == 0) goto L_0x0068
            java.util.Map r1 = r3._properties
            java.lang.String r0 = r2._propName
            r1.put(r0, r2)
        L_0x0068:
            java.lang.String r0 = "localizedMessage"
            r3.addIgnorable(r0)
            java.lang.String r0 = "suppressed"
            r3.addIgnorable(r0)
            java.lang.String r0 = "message"
            r3.addIgnorable(r0)
            X.1c8 r1 = r7._factoryConfig
            boolean r0 = r1.hasDeserializerModifiers()
            if (r0 == 0) goto L_0x0099
            X.1c9[] r1 = r1._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x008a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0099
            r1.next()
            goto L_0x008a
        L_0x0094:
            X.0jc r0 = r0.getAnnotationIntrospector()
            goto L_0x0053
        L_0x0099:
            com.fasterxml.jackson.databind.JsonDeserializer r2 = r3.build()
            boolean r0 = r2 instanceof com.fasterxml.jackson.databind.deser.BeanDeserializer
            if (r0 == 0) goto L_0x00a9
            com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer r0 = new com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer
            com.fasterxml.jackson.databind.deser.BeanDeserializer r2 = (com.fasterxml.jackson.databind.deser.BeanDeserializer) r2
            r0.<init>(r2)
            r2 = r0
        L_0x00a9:
            X.1c8 r1 = r7._factoryConfig
            boolean r0 = r1.hasDeserializerModifiers()
            if (r0 == 0) goto L_0x0339
            X.1c9[] r1 = r1._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x00bc:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0339
            r1.next()
            goto L_0x00bc
        L_0x00c6:
            boolean r0 = r9.isAbstract()
            if (r0 == 0) goto L_0x00e3
            X.1c8 r0 = r7._factoryConfig
            X.1cA[] r1 = r0._abstractTypeResolvers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x00d9:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00e3
            r1.next()
            goto L_0x00d9
        L_0x00e3:
            java.lang.Class r1 = r9._class
            java.lang.String r2 = r1.getName()
            boolean r0 = r1.isPrimitive()
            if (r0 != 0) goto L_0x0192
            java.lang.String r0 = "java."
            boolean r0 = r2.startsWith(r0)
            if (r0 != 0) goto L_0x0192
            java.lang.String r0 = "com.fasterxml."
            boolean r0 = r2.startsWith(r0)
            if (r0 == 0) goto L_0x0265
            java.lang.Class<X.0no> r0 = X.C11700no.class
            if (r1 != r0) goto L_0x0186
            com.fasterxml.jackson.databind.deser.std.JacksonDeserializers$TokenBufferDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JacksonDeserializers$TokenBufferDeserializer.instance
        L_0x0105:
            if (r3 != 0) goto L_0x0128
            java.lang.Class r0 = r9._class
            java.lang.Class<java.util.concurrent.atomic.AtomicReference> r1 = java.util.concurrent.atomic.AtomicReference.class
            boolean r0 = r1.isAssignableFrom(r0)
            if (r0 == 0) goto L_0x0130
            X.0js r0 = r8.getTypeFactory()
            X.0jR[] r2 = r0.findTypeParameters(r9, r1)
            if (r2 == 0) goto L_0x012b
            int r1 = r2.length
            r0 = 1
            if (r1 < r0) goto L_0x012b
            r0 = 0
            r1 = r2[r0]
        L_0x0122:
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$AtomicReferenceDeserializer r3 = new com.fasterxml.jackson.databind.deser.std.JdkDeserializers$AtomicReferenceDeserializer
            r0 = 0
            r3.<init>(r1, r0)
        L_0x0128:
            if (r3 == 0) goto L_0x026c
            return r3
        L_0x012b:
            X.0jR r1 = X.C10300js.unknownType()
            goto L_0x0122
        L_0x0130:
            X.8WH r5 = X.AnonymousClass8WH.instance
            X.0kF r4 = r8._config
            java.lang.Class r2 = r9._class
            java.lang.String r0 = r2.getName()
            java.lang.String r1 = "javax.xml."
            boolean r0 = r0.startsWith(r1)
            r3 = 0
            if (r0 != 0) goto L_0x0171
            boolean r0 = X.AnonymousClass8WH.hasSupertypeStartingWith(r5, r2, r1)
            if (r0 != 0) goto L_0x0171
            java.lang.String r1 = "org.w3c.dom.Node"
            boolean r0 = X.AnonymousClass8WH.doesImplement(r5, r2, r1)
            if (r0 == 0) goto L_0x015c
            java.lang.String r0 = "com.fasterxml.jackson.databind.ext.DOMDeserializer$DocumentDeserializer"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception | LinkageError -> 0x016d }
            java.lang.Object r3 = r0.newInstance()     // Catch:{ Exception | LinkageError -> 0x016d }
            goto L_0x016e
        L_0x015c:
            boolean r0 = X.AnonymousClass8WH.doesImplement(r5, r2, r1)
            if (r0 == 0) goto L_0x0128
            java.lang.String r0 = "com.fasterxml.jackson.databind.ext.DOMDeserializer$NodeDeserializer"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception | LinkageError -> 0x016d }
            java.lang.Object r3 = r0.newInstance()     // Catch:{ Exception | LinkageError -> 0x016d }
            goto L_0x016e
        L_0x016d:
            r3 = 0
        L_0x016e:
            com.fasterxml.jackson.databind.JsonDeserializer r3 = (com.fasterxml.jackson.databind.JsonDeserializer) r3
            goto L_0x0128
        L_0x0171:
            java.lang.String r0 = "com.fasterxml.jackson.databind.ext.CoreXMLDeserializers"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception | LinkageError -> 0x017c }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Exception | LinkageError -> 0x017c }
            goto L_0x017d
        L_0x017c:
            r0 = 0
        L_0x017d:
            if (r0 == 0) goto L_0x0128
            X.1c7 r0 = (X.AnonymousClass1c7) r0
            com.fasterxml.jackson.databind.JsonDeserializer r3 = r0.findBeanDeserializer(r9, r4, r10)
            goto L_0x0128
        L_0x0186:
            java.lang.Class<X.0jR> r0 = X.C10030jR.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x0265
            com.fasterxml.jackson.databind.deser.std.JacksonDeserializers$JavaTypeDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JacksonDeserializers$JavaTypeDeserializer.instance
            goto L_0x0105
        L_0x0192:
            java.lang.Class r0 = X.AnonymousClass1c5.CLASS_OBJECT
            if (r1 != r0) goto L_0x019a
            com.fasterxml.jackson.databind.deser.std.UntypedObjectDeserializer r3 = com.fasterxml.jackson.databind.deser.std.UntypedObjectDeserializer.instance
            goto L_0x0105
        L_0x019a:
            java.lang.Class r0 = X.AnonymousClass1c5.CLASS_STRING
            if (r1 == r0) goto L_0x0268
            java.lang.Class r0 = X.AnonymousClass1c5.CLASS_CHAR_BUFFER
            if (r1 == r0) goto L_0x0268
            java.lang.Class r0 = X.AnonymousClass1c5.CLASS_ITERABLE
            if (r1 != r0) goto L_0x01c2
            int r0 = r9.containedTypeCount()
            if (r0 <= 0) goto L_0x01bd
            r0 = 0
            X.0jR r1 = r9.containedType(r0)
        L_0x01b1:
            java.lang.Class<java.util.Collection> r0 = java.util.Collection.class
            X.1ed r0 = X.C28331ed.construct(r0, r1)
            com.fasterxml.jackson.databind.JsonDeserializer r3 = r7.createCollectionDeserializer(r8, r0, r10)
            goto L_0x0105
        L_0x01bd:
            X.0jR r1 = X.C10300js.unknownType()
            goto L_0x01b1
        L_0x01c2:
            com.fasterxml.jackson.databind.JsonDeserializer r3 = X.C28351ef.find(r1, r2)
            if (r3 != 0) goto L_0x0105
            java.util.HashSet r0 = X.AnonymousClass9M5._classNames
            boolean r0 = r0.contains(r2)
            if (r0 != 0) goto L_0x01e3
            r3 = 0
        L_0x01d1:
            if (r3 != 0) goto L_0x0105
            java.util.HashSet r0 = X.C182918e9._classNames
            boolean r0 = r0.contains(r2)
            if (r0 == 0) goto L_0x0265
            java.lang.Class<java.net.URI> r0 = java.net.URI.class
            if (r1 != r0) goto L_0x020d
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$URIDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$URIDeserializer.instance
            goto L_0x0105
        L_0x01e3:
            java.lang.Class<java.util.Calendar> r0 = java.util.Calendar.class
            if (r1 != r0) goto L_0x01ea
            com.fasterxml.jackson.databind.deser.std.DateDeserializers$CalendarDeserializer r3 = com.fasterxml.jackson.databind.deser.std.DateDeserializers$CalendarDeserializer.instance
            goto L_0x01d1
        L_0x01ea:
            java.lang.Class<java.util.Date> r0 = java.util.Date.class
            if (r1 != r0) goto L_0x01f1
            com.fasterxml.jackson.databind.deser.std.DateDeserializers$DateDeserializer r3 = com.fasterxml.jackson.databind.deser.std.DateDeserializers$DateDeserializer.instance
            goto L_0x01d1
        L_0x01f1:
            java.lang.Class<java.sql.Date> r0 = java.sql.Date.class
            if (r1 != r0) goto L_0x01f8
            com.fasterxml.jackson.databind.deser.std.DateDeserializers$SqlDateDeserializer r3 = com.fasterxml.jackson.databind.deser.std.DateDeserializers$SqlDateDeserializer.instance
            goto L_0x01d1
        L_0x01f8:
            java.lang.Class<java.sql.Timestamp> r0 = java.sql.Timestamp.class
            if (r1 != r0) goto L_0x01ff
            com.fasterxml.jackson.databind.deser.std.DateDeserializers$TimestampDeserializer r3 = com.fasterxml.jackson.databind.deser.std.DateDeserializers$TimestampDeserializer.instance
            goto L_0x01d1
        L_0x01ff:
            java.lang.Class<java.util.TimeZone> r0 = java.util.TimeZone.class
            if (r1 != r0) goto L_0x0206
            com.fasterxml.jackson.databind.deser.std.DateDeserializers$TimeZoneDeserializer r3 = com.fasterxml.jackson.databind.deser.std.DateDeserializers$TimeZoneDeserializer.instance
            goto L_0x01d1
        L_0x0206:
            java.lang.Class<java.util.GregorianCalendar> r0 = java.util.GregorianCalendar.class
            if (r1 != r0) goto L_0x036c
            com.fasterxml.jackson.databind.deser.std.DateDeserializers$CalendarDeserializer r3 = com.fasterxml.jackson.databind.deser.std.DateDeserializers$CalendarDeserializer.gregorianInstance
            goto L_0x01d1
        L_0x020d:
            java.lang.Class<java.net.URL> r0 = java.net.URL.class
            if (r1 != r0) goto L_0x0215
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$URLDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$URLDeserializer.instance
            goto L_0x0105
        L_0x0215:
            java.lang.Class<java.io.File> r0 = java.io.File.class
            if (r1 != r0) goto L_0x021d
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$FileDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$FileDeserializer.instance
            goto L_0x0105
        L_0x021d:
            java.lang.Class<java.util.UUID> r0 = java.util.UUID.class
            if (r1 != r0) goto L_0x0225
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$UUIDDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$UUIDDeserializer.instance
            goto L_0x0105
        L_0x0225:
            java.lang.Class<java.util.Currency> r0 = java.util.Currency.class
            if (r1 != r0) goto L_0x022d
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$CurrencyDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$CurrencyDeserializer.instance
            goto L_0x0105
        L_0x022d:
            java.lang.Class<java.util.regex.Pattern> r0 = java.util.regex.Pattern.class
            if (r1 != r0) goto L_0x0235
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$PatternDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$PatternDeserializer.instance
            goto L_0x0105
        L_0x0235:
            java.lang.Class<java.util.Locale> r0 = java.util.Locale.class
            if (r1 != r0) goto L_0x023d
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$LocaleDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$LocaleDeserializer.instance
            goto L_0x0105
        L_0x023d:
            java.lang.Class<java.net.InetAddress> r0 = java.net.InetAddress.class
            if (r1 != r0) goto L_0x0245
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$InetAddressDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$InetAddressDeserializer.instance
            goto L_0x0105
        L_0x0245:
            java.lang.Class<java.nio.charset.Charset> r0 = java.nio.charset.Charset.class
            if (r1 != r0) goto L_0x024d
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$CharsetDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$CharsetDeserializer.instance
            goto L_0x0105
        L_0x024d:
            java.lang.Class<java.lang.Class> r0 = java.lang.Class.class
            if (r1 != r0) goto L_0x0255
            com.fasterxml.jackson.databind.deser.std.ClassDeserializer r3 = com.fasterxml.jackson.databind.deser.std.ClassDeserializer.instance
            goto L_0x0105
        L_0x0255:
            java.lang.Class<java.lang.StackTraceElement> r0 = java.lang.StackTraceElement.class
            if (r1 != r0) goto L_0x025d
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$StackTraceElementDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$StackTraceElementDeserializer.instance
            goto L_0x0105
        L_0x025d:
            java.lang.Class<java.util.concurrent.atomic.AtomicBoolean> r0 = java.util.concurrent.atomic.AtomicBoolean.class
            if (r1 != r0) goto L_0x0360
            com.fasterxml.jackson.databind.deser.std.JdkDeserializers$AtomicBooleanDeserializer r3 = com.fasterxml.jackson.databind.deser.std.JdkDeserializers$AtomicBooleanDeserializer.instance
            goto L_0x0105
        L_0x0265:
            r3 = 0
            goto L_0x0105
        L_0x0268:
            com.fasterxml.jackson.databind.deser.std.StringDeserializer r3 = com.fasterxml.jackson.databind.deser.std.StringDeserializer.instance
            goto L_0x0105
        L_0x026c:
            java.lang.Class r6 = r9._class
            java.lang.String r3 = X.C29081fq.canBeABeanType(r6)
            java.lang.String r5 = ") as a Bean"
            java.lang.String r4 = " (of type "
            java.lang.String r2 = "Can not deserialize Class "
            if (r3 != 0) goto L_0x0352
            java.lang.String r3 = r6.getName()
            java.lang.String r0 = "net.sf.cglib.proxy."
            boolean r0 = r3.startsWith(r0)
            if (r0 != 0) goto L_0x028f
            java.lang.String r0 = "org.hibernate.proxy."
            boolean r1 = r3.startsWith(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0290
        L_0x028f:
            r0 = 1
        L_0x0290:
            if (r0 != 0) goto L_0x0344
            java.lang.reflect.Method r0 = r6.getEnclosingMethod()     // Catch:{ NullPointerException | SecurityException -> 0x029b }
            if (r0 == 0) goto L_0x029b
            java.lang.String r0 = "local/anonymous"
            goto L_0x029c
        L_0x029b:
            r0 = 0
        L_0x029c:
            if (r0 != 0) goto L_0x033a
            X.3Bx r2 = r7.findValueInstantiator(r8, r10)
            X.CZz r3 = new X.CZz
            X.0kF r0 = r8._config
            r3.<init>(r10, r0)
            r3._valueInstantiator = r2
            r7.addBeanProps(r8, r10, r3)
            addObjectIdReader(r8, r10, r3)
            r7.addReferenceProperties(r8, r10, r3)
            addInjectables(r8, r10, r3)
            X.1c8 r1 = r7._factoryConfig
            boolean r0 = r1.hasDeserializerModifiers()
            if (r0 == 0) goto L_0x02d4
            X.1c9[] r1 = r1._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x02ca:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x02d4
            r1.next()
            goto L_0x02ca
        L_0x02d4:
            boolean r0 = r9.isAbstract()
            if (r0 == 0) goto L_0x0334
            boolean r0 = r2.canCreateUsingDefault()
            if (r0 != 0) goto L_0x030b
            boolean r0 = r2.canCreateUsingDelegate()
            if (r0 != 0) goto L_0x030b
            boolean r0 = r2.canCreateFromObjectWith()
            if (r0 != 0) goto L_0x030b
            boolean r0 = r2.canCreateFromString()
            if (r0 != 0) goto L_0x030b
            boolean r0 = r2.canCreateFromInt()
            if (r0 != 0) goto L_0x030b
            boolean r0 = r2.canCreateFromLong()
            if (r0 != 0) goto L_0x030b
            boolean r0 = r2.canCreateFromDouble()
            if (r0 != 0) goto L_0x030b
            boolean r1 = r2.canCreateFromBoolean()
            r0 = 0
            if (r1 == 0) goto L_0x030c
        L_0x030b:
            r0 = 1
        L_0x030c:
            if (r0 != 0) goto L_0x0334
            com.fasterxml.jackson.databind.deser.AbstractDeserializer r2 = new com.fasterxml.jackson.databind.deser.AbstractDeserializer
            X.0ja r1 = r3._beanDesc
            java.util.HashMap r0 = r3._backRefProperties
            r2.<init>(r3, r1, r0)
        L_0x0317:
            X.1c8 r1 = r7._factoryConfig
            boolean r0 = r1.hasDeserializerModifiers()
            if (r0 == 0) goto L_0x0339
            X.1c9[] r1 = r1._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x032a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0339
            r1.next()
            goto L_0x032a
        L_0x0334:
            com.fasterxml.jackson.databind.JsonDeserializer r2 = r3.build()
            goto L_0x0317
        L_0x0339:
            return r2
        L_0x033a:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = X.AnonymousClass08S.A0T(r2, r3, r4, r0, r5)
            r1.<init>(r0)
            throw r1
        L_0x0344:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Can not deserialize Proxy class "
            java.lang.String r0 = " as a Bean"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r3, r0)
            r2.<init>(r0)
            throw r2
        L_0x0352:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r6.getName()
            java.lang.String r0 = X.AnonymousClass08S.A0T(r2, r0, r4, r3, r5)
            r1.<init>(r0)
            throw r1
        L_0x0360:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Internal error: can't find deserializer for "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r2)
            r1.<init>(r0)
            throw r1
        L_0x036c:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Internal error: can't find deserializer for "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r2)
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1c4.createBeanDeserializer(X.1c3, X.0jR, X.0ja):com.fasterxml.jackson.databind.JsonDeserializer");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.Ca7.<init>(X.CaE, boolean):void
     arg types: [X.CaE, int]
     candidates:
      X.Ca7.<init>(X.Ca7, com.fasterxml.jackson.databind.JsonDeserializer):void
      X.Ca7.<init>(X.Ca7, java.lang.String):void
      X.Ca7.<init>(X.CaE, boolean):void */
    public JsonDeserializer createBuilderBasedDeserializer(C26791c3 r14, C10030jR r15, C10120ja r16, Class cls) {
        String str;
        C10030jR constructType = r14._config.constructType(cls);
        C10490kF r1 = r14._config;
        C10120ja forDeserializationWithBuilder = r1._base._classIntrospector.forDeserializationWithBuilder(r1, constructType, r1);
        C64473Bx findValueInstantiator = findValueInstantiator(r14, forDeserializationWithBuilder);
        C10490kF r12 = r14._config;
        C10490kF r7 = r12;
        C25113CZz cZz = new C25113CZz(forDeserializationWithBuilder, r12);
        cZz._valueInstantiator = findValueInstantiator;
        addBeanProps(r14, forDeserializationWithBuilder, cZz);
        addObjectIdReader(r14, forDeserializationWithBuilder, cZz);
        addReferenceProperties(r14, forDeserializationWithBuilder, cZz);
        addInjectables(r14, forDeserializationWithBuilder, cZz);
        BM3 findPOJOBuilderConfig = forDeserializationWithBuilder.findPOJOBuilderConfig();
        if (findPOJOBuilderConfig == null) {
            str = "build";
        } else {
            str = findPOJOBuilderConfig.buildMethodName;
        }
        C29141fw findMethod = forDeserializationWithBuilder.findMethod(str, null);
        if (findMethod != null && r7.canOverrideAccessModifiers()) {
            C29081fq.checkAndFixAccess(findMethod._method);
        }
        cZz._buildMethod = findMethod;
        cZz._builderConfig = findPOJOBuilderConfig;
        AnonymousClass1c8 r13 = this._factoryConfig;
        if (r13.hasDeserializerModifiers()) {
            Iterator it = new C29161fy(r13._modifiers).iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        C29141fw r0 = cZz._buildMethod;
        if (r0 != null) {
            Class<?> returnType = r0._method.getReturnType();
            if (r15._class.isAssignableFrom(returnType)) {
                Collection values = cZz._properties.values();
                Ca0 ca0 = new Ca0(values);
                ca0.assignIndexes();
                boolean z = !cZz._defaultViewInclusion;
                if (!z) {
                    Iterator it2 = values.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        boolean z2 = false;
                        if (((CZw) it2.next())._viewMatcher != null) {
                            z2 = true;
                            continue;
                        }
                        if (z2) {
                            z = true;
                            break;
                        }
                    }
                }
                C25121CaE caE = cZz._objectIdReader;
                if (caE != null) {
                    ca0 = ca0.withProperty(new C25117Ca7(caE, true));
                }
                BuilderBasedDeserializer builderBasedDeserializer = new BuilderBasedDeserializer(cZz, cZz._beanDesc, ca0, cZz._backRefProperties, cZz._ignorableProps, cZz._ignoreAllUnknown, z);
                AnonymousClass1c8 r17 = this._factoryConfig;
                if (r17.hasDeserializerModifiers()) {
                    Iterator it3 = new C29161fy(r17._modifiers).iterator();
                    while (it3.hasNext()) {
                        it3.next();
                    }
                }
                return builderBasedDeserializer;
            }
            throw new IllegalArgumentException(AnonymousClass08S.A0V("Build method '", cZz._buildMethod.getFullName(), " has bad return type (", returnType.getName(), "), not compatible with POJO type (", r15._class.getName(), ")"));
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0T("Builder class ", cZz._beanDesc._type._class.getName(), " does not have build method '", str, "()'"));
    }

    public AnonymousClass1c6 withConfig(AnonymousClass1c8 r6) {
        if (this._factoryConfig == r6) {
            return this;
        }
        Class<?> cls = getClass();
        if (cls == AnonymousClass1c4.class) {
            return new AnonymousClass1c4(r6);
        }
        throw new IllegalStateException(AnonymousClass08S.A0S("Subtype of BeanDeserializerFactory (", cls.getName(), ") has not properly overridden method 'withAdditionalDeserializers': can not instantiate subtype with ", "additional deserializer definitions"));
    }

    private static void addInjectables(C26791c3 r9, C10120ja r10, C25113CZz cZz) {
        Map findInjectables = r10.findInjectables();
        if (findInjectables != null) {
            boolean canOverrideAccessModifiers = r9.getConfig().canOverrideAccessModifiers();
            for (Map.Entry entry : findInjectables.entrySet()) {
                C183512m r8 = (C183512m) entry.getValue();
                if (canOverrideAccessModifiers) {
                    C29081fq.checkAndFixAccess(r8.getMember());
                }
                String name = r8.getName();
                C10030jR resolveType = r10.resolveType(r8.getGenericType());
                C10100jY classAnnotations = r10.getClassAnnotations();
                Object key = entry.getKey();
                if (cZz._injectables == null) {
                    cZz._injectables = new ArrayList();
                }
                cZz._injectables.add(new C25123CaH(name, resolveType, classAnnotations, r8, key));
            }
        }
    }

    private static void addObjectIdReader(C26791c3 r7, C10120ja r8, C25113CZz cZz) {
        C10030jR r2;
        CZw cZw;
        C25128CaN objectIdGeneratorInstance;
        CZj objectIdInfo = r8.getObjectIdInfo();
        if (objectIdInfo != null) {
            Class<C25132CaR> cls = objectIdInfo._generator;
            if (cls == C25132CaR.class) {
                String str = objectIdInfo._propertyName;
                cZw = (CZw) cZz._properties.get(str);
                if (cZw != null) {
                    r2 = cZw.getType();
                    objectIdGeneratorInstance = new CZo(objectIdInfo._scope);
                } else {
                    throw new IllegalArgumentException(AnonymousClass08S.A0T("Invalid Object Id definition for ", r8._type._class.getName(), ": can not find property with name '", str, "'"));
                }
            } else {
                r2 = r7.getTypeFactory().findTypeParameters(r7._config.constructType(cls), C25128CaN.class)[0];
                cZw = null;
                objectIdGeneratorInstance = r7.objectIdGeneratorInstance(r8.getClassInfo(), objectIdInfo);
            }
            cZz._objectIdReader = new C25121CaE(r2, objectIdInfo._propertyName, objectIdGeneratorInstance, r7.findRootValueDeserializer(r2), cZw);
        }
    }

    private void addReferenceProperties(C26791c3 r8, C10120ja r9, C25113CZz cZz) {
        Type rawType;
        C10140jc annotationIntrospector;
        Map findBackReferenceProperties = r9.findBackReferenceProperties();
        if (findBackReferenceProperties != null) {
            for (Map.Entry entry : findBackReferenceProperties.entrySet()) {
                String str = (String) entry.getKey();
                C183512m r5 = (C183512m) entry.getValue();
                if (r5 instanceof C29141fw) {
                    rawType = ((C29141fw) r5).getGenericParameterType(0);
                } else {
                    rawType = r5.getRawType();
                }
                C10490kF r0 = r8._config;
                String name = r5.getName();
                if (r0 == null) {
                    annotationIntrospector = null;
                } else {
                    annotationIntrospector = r0.getAnnotationIntrospector();
                }
                CZw constructSettableProperty = constructSettableProperty(r8, r9, new CZY(r5, name, annotationIntrospector), rawType);
                if (cZz._backRefProperties == null) {
                    cZz._backRefProperties = new HashMap(4);
                }
                cZz._backRefProperties.put(str, constructSettableProperty);
                Map map = cZz._properties;
                if (map != null) {
                    map.remove(constructSettableProperty._propName);
                }
            }
        }
    }

    private AnonymousClass1c4(AnonymousClass1c8 r1) {
        super(r1);
    }
}
