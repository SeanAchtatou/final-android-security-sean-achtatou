package X;

import com.google.common.util.concurrent.ListenableFuture;
import io.card.payment.BuildConfig;

/* renamed from: X.1FC  reason: invalid class name */
public final class AnonymousClass1FC implements C20021Ap {
    public final /* synthetic */ AnonymousClass16R A00;

    public AnonymousClass1FC(AnonymousClass16R r1) {
        this.A00 = r1;
    }

    public void Bd1(Object obj, Object obj2) {
        ((C14500tS) AnonymousClass1XX.A02(23, AnonymousClass1Y3.A1i, this.A00.A08)).A0C(BuildConfig.FLAVOR, ((Throwable) obj2).getMessage(), AnonymousClass07B.A0i);
    }

    public void Bgh(Object obj, Object obj2) {
        int i = AnonymousClass1Y3.BQP;
        AnonymousClass16R r2 = this.A00;
        ((AnonymousClass1G8) AnonymousClass1XX.A02(22, i, r2.A08)).A01 = (AnonymousClass101) obj2;
        AnonymousClass16R.A04(r2, AnonymousClass07B.A0Y, "InboxAds");
    }

    public void BdJ(Object obj, Object obj2) {
    }

    public void BdU(Object obj, ListenableFuture listenableFuture) {
    }
}
