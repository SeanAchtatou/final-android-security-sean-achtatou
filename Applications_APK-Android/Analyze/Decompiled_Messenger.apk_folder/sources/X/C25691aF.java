package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.model.User;

/* renamed from: X.1aF  reason: invalid class name and case insensitive filesystem */
public final class C25691aF implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new User(parcel);
    }

    public Object[] newArray(int i) {
        return new User[i];
    }
}
