package X;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/* renamed from: X.0oR  reason: invalid class name and case insensitive filesystem */
public class C12040oR {
    public static C12040oR A02;
    private C31061j5 A00;
    private Comparator A01;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r0 == false) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        if (r0 == false) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        if (r0 == false) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        if (r0 == false) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0054, code lost:
        if (r0 == false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C34171oq A01(X.C30971iw r8) {
        /*
            r7 = this;
            boolean r0 = r7 instanceof X.AnonymousClass15S
            if (r0 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            r4 = r7
            X.15S r4 = (X.AnonymousClass15S) r4
            java.lang.String r6 = r8.A02()
            int r0 = r6.hashCode()
            r5 = 1
            r1 = 4
            r3 = 3
            r2 = 2
            switch(r0) {
                case 351608024: goto L_0x0025;
                case 382486303: goto L_0x002f;
                case 408072700: goto L_0x0039;
                case 1738660166: goto L_0x0043;
                case 1934313696: goto L_0x004d;
                default: goto L_0x0018;
            }
        L_0x0018:
            r6 = -1
        L_0x0019:
            if (r6 == 0) goto L_0x0085
            if (r6 == r5) goto L_0x007a
            if (r6 == r2) goto L_0x006f
            if (r6 == r3) goto L_0x0063
            if (r6 == r1) goto L_0x0057
            r0 = 0
            return r0
        L_0x0025:
            java.lang.String r0 = "version"
            boolean r0 = r6.equals(r0)
            r6 = 3
            if (r0 != 0) goto L_0x0019
            goto L_0x0018
        L_0x002f:
            java.lang.String r0 = "eviction"
            boolean r0 = r6.equals(r0)
            r6 = 4
            if (r0 != 0) goto L_0x0019
            goto L_0x0018
        L_0x0039:
            java.lang.String r0 = "max_size"
            boolean r0 = r6.equals(r0)
            r6 = 1
            if (r0 != 0) goto L_0x0019
            goto L_0x0018
        L_0x0043:
            java.lang.String r0 = "stale_removal"
            boolean r0 = r6.equals(r0)
            r6 = 2
            if (r0 != 0) goto L_0x0019
            goto L_0x0018
        L_0x004d:
            java.lang.String r0 = "user_scope"
            boolean r0 = r6.equals(r0)
            r6 = 0
            if (r0 != 0) goto L_0x0019
            goto L_0x0018
        L_0x0057:
            r2 = 6
            int r1 = X.AnonymousClass1Y3.AjB
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.storage.cask.fbapps.controllers.FBEvictionPluginController r0 = (com.facebook.storage.cask.fbapps.controllers.FBEvictionPluginController) r0
            return r0
        L_0x0063:
            r2 = 5
            int r1 = X.AnonymousClass1Y3.AW3
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.EHS r0 = (X.EHS) r0
            return r0
        L_0x006f:
            int r2 = X.AnonymousClass1Y3.AW2
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r2, r0)
            com.facebook.storage.cask.fbapps.controllers.FBStaleRemovalPluginController r0 = (com.facebook.storage.cask.fbapps.controllers.FBStaleRemovalPluginController) r0
            return r0
        L_0x007a:
            int r1 = X.AnonymousClass1Y3.B0f
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            com.facebook.storage.cask.fbapps.controllers.FBMaxSizePluginController r0 = (com.facebook.storage.cask.fbapps.controllers.FBMaxSizePluginController) r0
            return r0
        L_0x0085:
            int r1 = X.AnonymousClass1Y3.AMZ
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1oo r0 = (X.C34151oo) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12040oR.A01(X.1iw):X.1oq");
    }

    public File A03(C31071j6 r10) {
        File file;
        File file2;
        File file3;
        File file4;
        File file5;
        C31061j5 r0 = this.A00;
        if (r0 != null) {
            File file6 = r0.A00;
            if (file6 == null || (file = r0.A01) == null || (file2 = r0.A02) == null || (file3 = r0.A03) == null || (file4 = r0.A04) == null) {
                throw new IllegalStateException(AnonymousClass08S.A09("Base folder null with storeInCaches = ", r10.A00));
            }
            int i = r10.A00;
            String str = r10.A03;
            if (i == 1) {
                file5 = new File(file3, str);
            } else if (i == 2) {
                file5 = new File(file4, str);
            } else if (i == 3) {
                file5 = new File(file6, str);
            } else if (i == 4) {
                file5 = new File(file, AnonymousClass08S.A0J("app_", str));
            } else if (i == 5) {
                file5 = new File(file2, str);
            } else {
                throw new IllegalStateException(AnonymousClass08S.A09("Invalid location value provided = ", i));
            }
            ArrayList<C30971iw> arrayList = new ArrayList<>(r10.A02.values());
            if (!arrayList.isEmpty()) {
                LinkedList<Pair> linkedList = new LinkedList<>();
                for (C30971iw r5 : arrayList) {
                    C34171oq A012 = A01(r5);
                    if (A012 == null) {
                        AnonymousClass02w.A0C("Cask", AnonymousClass08S.A0S("PathConfig of '", r10.A03, "' tried to getWithoutInit with unhandled plugin : ", r5.A02()));
                    } else if (A012 instanceof C34181or) {
                        linkedList.add(new Pair(r5, (C34181or) A012));
                    }
                }
                Collections.sort(linkedList, this.A01);
                for (Pair pair : linkedList) {
                    String BwW = ((C34181or) pair.second).BwW(r10, (C30971iw) pair.first);
                    if (!TextUtils.isEmpty(BwW)) {
                        file5 = new File(file5, BwW);
                    }
                }
            }
            return file5;
        }
        throw new IllegalStateException(AnonymousClass08S.A0J("Base Cask not initialized ", r10.A03));
    }

    public File A04(File file, C31071j6 r8) {
        ArrayList<C30971iw> arrayList = new ArrayList<>(r8.A02.values());
        if (!arrayList.isEmpty()) {
            for (C30971iw r4 : arrayList) {
                C34171oq A012 = A01(r4);
                if (A012 == null) {
                    AnonymousClass02w.A0C("Cask", AnonymousClass08S.A0S("PathConfig of '", r8.A03, "' tried to registerPath with unhandled plugin : ", r4.A02()));
                } else {
                    A012.Bhy(r8, r4, file);
                }
            }
        }
        return file;
    }

    public void A05(Context context) {
        this.A00 = new C31061j5(context);
        this.A01 = new AnonymousClass10Y();
    }

    public File A02(C31071j6 r3) {
        File A03 = A03(r3);
        File file = r3.A01;
        if (file != null) {
            C31061j5.A00(A03, file);
        }
        if (!A03.isDirectory()) {
            A03.mkdirs();
        }
        A04(A03, r3);
        return A03;
    }

    public C12040oR() {
    }

    public C12040oR(Context context) {
        A05(context);
    }
}
