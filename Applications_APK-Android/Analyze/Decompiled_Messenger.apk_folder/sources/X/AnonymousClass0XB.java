package X;

import com.facebook.acra.CustomReportDataSupplier;

/* renamed from: X.0XB  reason: invalid class name */
public final class AnonymousClass0XB implements CustomReportDataSupplier {
    public final /* synthetic */ AnonymousClass0Z1 A00;

    public AnonymousClass0XB(AnonymousClass0Z1 r1) {
        this.A00 = r1;
    }

    public String getCustomData(Throwable th) {
        return this.A00.getCustomData(th);
    }
}
