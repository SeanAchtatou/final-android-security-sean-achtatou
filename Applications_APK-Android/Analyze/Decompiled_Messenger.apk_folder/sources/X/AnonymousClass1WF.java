package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1WF  reason: invalid class name */
public final class AnonymousClass1WF extends AnonymousClass0UV {
    private static volatile C31691k6 A00;

    public static final C31691k6 A00(AnonymousClass1XY r5) {
        if (A00 == null) {
            synchronized (C31691k6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A00 = new C31591jw(AnonymousClass1YA.A00(applicationInjector), new C013209t(AnonymousClass1YA.A00(applicationInjector))).A00();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C31591jw A01(AnonymousClass1XY r3) {
        return new C31591jw(AnonymousClass1YA.A00(r3), new C013209t(AnonymousClass1YA.A00(r3)));
    }

    public static final C013209t A02(AnonymousClass1XY r1) {
        return new C013209t(AnonymousClass1YA.A00(r1));
    }
}
