package X;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import com.facebook.acra.util.StatFsUtil;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;

/* renamed from: X.0VQ  reason: invalid class name */
public final class AnonymousClass0VQ {
    public static final FileFilter A00 = new AnonymousClass0VR();

    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0053 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00() {
        /*
            r8 = 0
            r3 = -1
            r7 = 0
            r5 = -1
        L_0x0004:
            int r0 = A01()     // Catch:{ IOException -> 0x007c }
            if (r7 >= r0) goto L_0x0059
            java.lang.String r1 = "/sys/devices/system/cpu/cpu"
            java.lang.String r0 = "/cpufreq/cpuinfo_max_freq"
            java.lang.String r0 = X.AnonymousClass08S.A0A(r1, r7, r0)     // Catch:{ IOException -> 0x007c }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x007c }
            r1.<init>(r0)     // Catch:{ IOException -> 0x007c }
            boolean r0 = r1.exists()     // Catch:{ IOException -> 0x007c }
            if (r0 == 0) goto L_0x0056
            boolean r0 = r1.canRead()     // Catch:{ IOException -> 0x007c }
            if (r0 == 0) goto L_0x0056
            r0 = 128(0x80, float:1.794E-43)
            r6 = 128(0x80, float:1.794E-43)
            byte[] r4 = new byte[r0]     // Catch:{ IOException -> 0x007c }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x007c }
            r2.<init>(r1)     // Catch:{ IOException -> 0x007c }
            r2.read(r4)     // Catch:{ NumberFormatException -> 0x0053, all -> 0x0076 }
            r1 = 0
        L_0x0032:
            byte r0 = r4[r1]     // Catch:{ NumberFormatException -> 0x0053, all -> 0x0076 }
            boolean r0 = java.lang.Character.isDigit(r0)     // Catch:{ NumberFormatException -> 0x0053, all -> 0x0076 }
            if (r0 == 0) goto L_0x003f
            if (r1 >= r6) goto L_0x003f
            int r1 = r1 + 1
            goto L_0x0032
        L_0x003f:
            java.lang.String r0 = new java.lang.String     // Catch:{ NumberFormatException -> 0x0053, all -> 0x0076 }
            r0.<init>(r4, r8, r1)     // Catch:{ NumberFormatException -> 0x0053, all -> 0x0076 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0053, all -> 0x0076 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ NumberFormatException -> 0x0053, all -> 0x0076 }
            int r0 = r0.intValue()     // Catch:{ NumberFormatException -> 0x0053, all -> 0x0076 }
            if (r0 <= r5) goto L_0x0053
            r5 = r0
        L_0x0053:
            r2.close()     // Catch:{ IOException -> 0x007c }
        L_0x0056:
            int r7 = r7 + 1
            goto L_0x0004
        L_0x0059:
            if (r5 != r3) goto L_0x007b
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x007c }
            java.lang.String r0 = "/proc/cpuinfo"
            r1.<init>(r0)     // Catch:{ IOException -> 0x007c }
            java.lang.String r0 = "cpu MHz"
            int r0 = A03(r0, r1)     // Catch:{ all -> 0x0071 }
            int r0 = r0 * 1000
            if (r0 > r5) goto L_0x006d
            r0 = -1
        L_0x006d:
            r1.close()     // Catch:{ IOException -> 0x007c }
            return r0
        L_0x0071:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x007c }
            goto L_0x007a
        L_0x0076:
            r0 = move-exception
            r2.close()     // Catch:{ IOException -> 0x007c }
        L_0x007a:
            throw r0     // Catch:{ IOException -> 0x007c }
        L_0x007b:
            return r5
        L_0x007c:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VQ.A00():int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003c A[SYNTHETIC, Splitter:B:17:0x003c] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0044 A[SYNTHETIC, Splitter:B:26:0x0044] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int A02(java.lang.String r4) {
        /*
            r1 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0040, all -> 0x0038 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0040, all -> 0x0038 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            java.lang.String r1 = r2.readLine()     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            r2.close()     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            if (r1 == 0) goto L_0x0031
            java.lang.String r0 = "0-[\\d]+$"
            boolean r0 = r1.matches(r0)     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            if (r0 == 0) goto L_0x0031
            r0 = 2
            java.lang.String r0 = r1.substring(r0)     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            int r0 = r0.intValue()     // Catch:{ IOException -> 0x0041, all -> 0x0036 }
            int r0 = r0 + 1
            goto L_0x0032
        L_0x0031:
            r0 = -1
        L_0x0032:
            r3.close()     // Catch:{ IOException -> 0x0047 }
            return r0
        L_0x0036:
            r0 = move-exception
            goto L_0x003a
        L_0x0038:
            r0 = move-exception
            r3 = r1
        L_0x003a:
            if (r3 == 0) goto L_0x003f
            r3.close()     // Catch:{ IOException -> 0x003f }
        L_0x003f:
            throw r0
        L_0x0040:
            r3 = r1
        L_0x0041:
            r0 = -1
            if (r3 == 0) goto L_0x0047
            r3.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0047:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VQ.A02(java.lang.String):int");
    }

    public static int A01() {
        if (Build.VERSION.SDK_INT <= 10) {
            return 1;
        }
        try {
            int A02 = A02("/sys/devices/system/cpu/possible");
            if (A02 == -1) {
                A02 = A02(AnonymousClass80H.$const$string(205));
            }
            if (A02 == -1) {
                return new File(TurboLoader.Locator.$const$string(95)).listFiles(A00).length;
            }
            return A02;
        } catch (NullPointerException | SecurityException unused) {
            return -1;
        }
    }

    private static int A03(String str, FileInputStream fileInputStream) {
        byte[] bArr = new byte[1024];
        try {
            int read = fileInputStream.read(bArr);
            int i = 0;
            while (i < read) {
                byte b = bArr[i];
                if (b == 10 || i == 0) {
                    if (b == 10) {
                        i++;
                    }
                    int i2 = i;
                    while (i2 < read) {
                        int i3 = i2 - i;
                        if (bArr[i2] != str.charAt(i3)) {
                            continue;
                            break;
                        } else if (i3 == str.length() - 1) {
                            while (i2 < 1024) {
                                byte b2 = bArr[i2];
                                if (b2 == 10) {
                                    return -1;
                                }
                                if (Character.isDigit(b2)) {
                                    int i4 = i2 + 1;
                                    while (i4 < 1024 && Character.isDigit(bArr[i4])) {
                                        i4++;
                                    }
                                    return Integer.parseInt(new String(bArr, 0, i2, i4 - i2));
                                }
                                i2++;
                            }
                            return -1;
                        } else {
                            i2++;
                        }
                    }
                    continue;
                }
                i++;
            }
            return -1;
        } catch (IOException | NumberFormatException unused) {
            return -1;
        }
    }

    public static long A04(Context context) {
        FileInputStream fileInputStream;
        if (Build.VERSION.SDK_INT >= 16) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
            return memoryInfo.totalMem;
        }
        try {
            fileInputStream = new FileInputStream(TurboLoader.Locator.$const$string(41));
            long A03 = ((long) A03("MemTotal", fileInputStream)) * StatFsUtil.IN_KILO_BYTE;
            fileInputStream.close();
            return A03;
        } catch (IOException unused) {
            return -1;
        } catch (Throwable th) {
            fileInputStream.close();
            throw th;
        }
    }
}
