package X;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/* renamed from: X.0fg  reason: invalid class name and case insensitive filesystem */
public final class C08630fg {
    public static ByteBuffer A00(InputStream inputStream) {
        if (!(inputStream instanceof FileInputStream)) {
            return ByteBuffer.wrap(C76163l4.A03(inputStream));
        }
        FileChannel channel = ((FileInputStream) inputStream).getChannel();
        return channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
    }
}
