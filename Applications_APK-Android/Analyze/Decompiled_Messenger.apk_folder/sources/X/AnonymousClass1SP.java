package X;

/* renamed from: X.1SP  reason: invalid class name */
public final class AnonymousClass1SP extends AnonymousClass1RO {
    public boolean A00 = false;
    public final AnonymousClass1QK A01;
    public final AnonymousClass1RU A02;
    public final C23351Pe A03;
    public final boolean A04;
    public final /* synthetic */ AnonymousClass1QB A05;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass1SP(AnonymousClass1QB r5, C23581Qb r6, AnonymousClass1QK r7, boolean z, C23351Pe r9) {
        super(r6);
        this.A05 = r5;
        this.A01 = r7;
        Boolean bool = r7.A09.A0D;
        this.A04 = bool != null ? bool.booleanValue() : z;
        this.A03 = r9;
        this.A02 = new AnonymousClass1RU(r5.A01, new C33231nF(this), 100);
        r7.A06(new AnonymousClass1QY(this, r6));
    }
}
