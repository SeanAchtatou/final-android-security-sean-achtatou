package X;

import android.content.Context;
import com.facebook.compactdisk.analytics.DiskSizeReporter;
import com.facebook.compactdisk.analytics.DiskSizeReporterHolder;
import com.facebook.compactdisk.common.AnalyticsEventReporterHolder;
import com.facebook.compactdisk.common.DependencyManager;
import com.facebook.compactdisk.common.ExperimentationConfigItem;
import com.facebook.compactdisk.common.PrivacyGuard;
import com.facebook.compactdisk.common.XAnalyticsLogger;
import com.facebook.compactdisk.current.CompactDiskManager;
import com.facebook.compactdisk.current.NoOpCompactDiskManager;
import com.facebook.compactdiskmodule.CompactDiskExperimentationConfig;
import com.facebook.compactdiskmodule.DiskAccessTrackerWrapper;
import com.facebook.compactdiskmodule.analytics.AndroidXAnalyticsLogger;
import com.facebook.inject.InjectorModule;
import com.facebook.quicklog.QuickPerformanceLogger;
import io.card.payment.BuildConfig;
import java.io.File;
import java.util.concurrent.ScheduledExecutorService;

@InjectorModule
/* renamed from: X.1i5  reason: invalid class name and case insensitive filesystem */
public final class C30461i5 extends AnonymousClass0UV {
    private static volatile DiskSizeReporter A00;
    private static volatile DiskSizeReporterHolder A01;
    private static volatile AnalyticsEventReporterHolder A02;
    private static volatile DependencyManager A03;
    private static volatile C30511iA A04;
    private static volatile PrivacyGuard A05;
    private static volatile CompactDiskManager A06;
    private static volatile C30541iE A07;
    private static volatile AnonymousClass29D A08;

    public static final DiskSizeReporter A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (DiskSizeReporter.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new DiskSizeReporter(A01(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final DiskSizeReporterHolder A01(AnonymousClass1XY r7) {
        DiskSizeReporterHolder diskSizeReporterHolder;
        if (A01 == null) {
            synchronized (DiskSizeReporterHolder.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        AnalyticsEventReporterHolder A022 = A02(applicationInjector);
                        ScheduledExecutorService A0j = AnonymousClass0UX.A0j(applicationInjector);
                        File filesDir = A003.getFilesDir();
                        if (filesDir == null) {
                            diskSizeReporterHolder = null;
                        } else {
                            diskSizeReporterHolder = new DiskSizeReporterHolder(A022, new File(filesDir, ".compactdisk_extended_attributes_single_file").getAbsolutePath(), A0j);
                        }
                        A01 = diskSizeReporterHolder;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final AnalyticsEventReporterHolder A02(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnalyticsEventReporterHolder.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnalyticsEventReporterHolder(AndroidXAnalyticsLogger.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.compactdisk.common.ExperimentationConfigItem.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.facebook.compactdisk.common.ExperimentationConfigItem.<init>(java.lang.String, long):void
      com.facebook.compactdisk.common.ExperimentationConfigItem.<init>(java.lang.String, boolean):void */
    public static final DependencyManager A03(AnonymousClass1XY r49) {
        QuickPerformanceLogger A032;
        String str;
        DependencyManager dependencyManager;
        String str2;
        long j;
        XAnalyticsLogger xAnalyticsLogger = null;
        if (A03 == null) {
            synchronized (DependencyManager.class) {
                AnonymousClass1XY r1 = r49;
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r1);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r1.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        AnonymousClass0WA.A00(applicationInjector);
                        A032 = AnonymousClass0ZD.A03(applicationInjector);
                        ScheduledExecutorService A0j = AnonymousClass0UX.A0j(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.Aog, applicationInjector);
                        CompactDiskExperimentationConfig A005 = CompactDiskExperimentationConfig.A00(applicationInjector);
                        A032.markerStart(10420333);
                        String str3 = BuildConfig.FLAVOR;
                        String str4 = BuildConfig.FLAVOR;
                        String str5 = BuildConfig.FLAVOR;
                        ExperimentationConfigItem[] experimentationConfigItemArr = new ExperimentationConfigItem[0];
                        File dir = A003.getDir("compactdisk", 0);
                        if (dir.exists() || dir.mkdirs()) {
                            str = dir.getPath();
                        } else {
                            str = null;
                        }
                        if (str == null) {
                            dependencyManager = new DependencyManager(BuildConfig.FLAVOR, str3, str4, str5, A0j, null, false, experimentationConfigItemArr);
                        } else {
                            String path = A003.getDir("compactdisk_internal", 0).getPath();
                            if (path == null) {
                                dependencyManager = new DependencyManager(BuildConfig.FLAVOR, str3, str4, str5, A0j, null, false, experimentationConfigItemArr);
                            } else {
                                File cacheDir = A003.getCacheDir();
                                if (cacheDir == null) {
                                    dependencyManager = new DependencyManager(BuildConfig.FLAVOR, str3, str4, str5, A0j, null, false, experimentationConfigItemArr);
                                } else {
                                    File file = new File(cacheDir, "compactdisk");
                                    if (file.exists() || file.mkdirs()) {
                                        str2 = file.getPath();
                                    } else {
                                        str2 = null;
                                    }
                                    if (str2 == null) {
                                        dependencyManager = new DependencyManager(BuildConfig.FLAVOR, str3, str4, str5, A0j, null, false, experimentationConfigItemArr);
                                    } else {
                                        if (AnonymousClass299.A06) {
                                            xAnalyticsLogger = (XAnalyticsLogger) A004.get();
                                        }
                                        String str6 = str2;
                                        ExperimentationConfigItem experimentationConfigItem = new ExperimentationConfigItem("enable_deletion_on_logout", false);
                                        ExperimentationConfigItem experimentationConfigItem2 = new ExperimentationConfigItem("use_cache_level_lock", A005.A00.Aem(287943198711315L));
                                        ExperimentationConfigItem experimentationConfigItem3 = new ExperimentationConfigItem("enable_no_manifest_lru", true);
                                        ExperimentationConfigItem experimentationConfigItem4 = new ExperimentationConfigItem("delete_caches_on_stale_days", A005.A01.At1(18583842453719919L, 0));
                                        if (AnonymousClass299.A04) {
                                            j = 1;
                                        } else {
                                            j = 2;
                                        }
                                        ScheduledExecutorService scheduledExecutorService = A0j;
                                        XAnalyticsLogger xAnalyticsLogger2 = xAnalyticsLogger;
                                        dependencyManager = new DependencyManager(str, path, str6, str2, scheduledExecutorService, xAnalyticsLogger2, false, new ExperimentationConfigItem[]{experimentationConfigItem, experimentationConfigItem2, experimentationConfigItem3, experimentationConfigItem4, new ExperimentationConfigItem("cache_stats_source", j), new ExperimentationConfigItem(AnonymousClass24B.$const$string(980), AnonymousClass299.A06), new ExperimentationConfigItem("diskcache_stale_removal", true), new ExperimentationConfigItem("enable_soft_error_reporter", false), new ExperimentationConfigItem("soft_error_reporter_sampling", A005.A01.At0(18583842453785456L)), new ExperimentationConfigItem("filecache_stale_removal", true), new ExperimentationConfigItem("enable_cache_stats_sampling", A005.A00.Aem(287943197793804L)), new ExperimentationConfigItem("cache_stats_stale_duration", A005.A00.At0(569418174368615L)), new ExperimentationConfigItem("cache_stats_sample_rate", A005.A00.At0(569418174172004L)), new ExperimentationConfigItem("enable_global_cache_config", false)});
                                    }
                                }
                            }
                        }
                        A032.markerEnd(10420333, 2);
                        A03 = dependencyManager;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final C30511iA A04(AnonymousClass1XY r3) {
        if (A04 == null) {
            synchronized (C30511iA.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A04 = new C30511iA();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final PrivacyGuard A05(AnonymousClass1XY r5) {
        if (A05 == null) {
            synchronized (PrivacyGuard.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A05 = new PrivacyGuard(AnonymousClass1YA.A00(applicationInjector), A04(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final CompactDiskManager A06(AnonymousClass1XY r12) {
        CompactDiskManager compactDiskManager;
        if (A06 == null) {
            synchronized (CompactDiskManager.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r12);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r12.getApplicationInjector();
                        AnonymousClass1YA.A00(applicationInjector);
                        DependencyManager A032 = A03(applicationInjector);
                        AnonymousClass0WA.A00(applicationInjector);
                        QuickPerformanceLogger A033 = AnonymousClass0ZD.A03(applicationInjector);
                        C30511iA A042 = A04(applicationInjector);
                        AnonymousClass0UX.A0j(applicationInjector);
                        AnonymousClass29D A082 = A08(applicationInjector);
                        new DiskAccessTrackerWrapper(applicationInjector);
                        AnonymousClass0WT.A00(applicationInjector);
                        AnonymousClass29F A003 = AnonymousClass29F.A00(applicationInjector);
                        if (!A032.mHasValidPaths) {
                            compactDiskManager = new NoOpCompactDiskManager();
                        } else {
                            A033.markerStart(10420332);
                            try {
                                compactDiskManager = new CompactDiskManager(A032, A033, A082, null);
                                A003.A01(compactDiskManager);
                                synchronized (A042) {
                                    A042.A00.add(compactDiskManager);
                                }
                                A033.markerEnd(10420332, 2);
                            } catch (Throwable th) {
                                A033.markerEnd(10420332, 2);
                                throw th;
                            }
                        }
                        A06 = compactDiskManager;
                        A002.A01();
                    } catch (Throwable th2) {
                        A002.A01();
                        throw th2;
                    }
                }
            }
        }
        return A06;
    }

    public static final C30541iE A07(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C30541iE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C30541iE(A05(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static final AnonymousClass29D A08(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass29D.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r4.getApplicationInjector();
                        C04750Wa.A01(applicationInjector);
                        AnonymousClass0UQ.A00(AnonymousClass1Y3.AcD, applicationInjector);
                        AnonymousClass0UQ.A00(AnonymousClass1Y3.AR0, applicationInjector);
                        A08 = null;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }
}
