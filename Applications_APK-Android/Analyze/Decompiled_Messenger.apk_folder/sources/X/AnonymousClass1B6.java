package X;

import com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1B6  reason: invalid class name */
public interface AnonymousClass1B6 {
    void BN9();

    void BWP(DiscoverTabAttachmentItem discoverTabAttachmentItem);

    void BZY(C64663Ct r1);

    void Bc3(InboxUnitItem inboxUnitItem);

    void Bd9();

    void Bf5();

    void BfQ(long j);

    void BfU(InboxMontageItem inboxMontageItem);

    void Bfa(ImmutableList immutableList);

    void Bfc(ImmutableList immutableList);

    void Bh6(C149266wB r1, ThreadKey threadKey);

    void Bh7();

    void BhA(ThreadKey threadKey, NavigationTrigger navigationTrigger, C13220qv r3);

    void BhB(ThreadSummary threadSummary, int i, NavigationTrigger navigationTrigger, C13220qv r4);

    void BhP(UserKey userKey, long j, InboxMontageItem inboxMontageItem);

    void Bpe(ImmutableList immutableList, long j, String str, C114765cl r5);
}
