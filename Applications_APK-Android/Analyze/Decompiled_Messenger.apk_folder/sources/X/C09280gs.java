package X;

import com.facebook.acra.LogCatCollector;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.common.util.TriState;
import com.facebook.user.model.User;
import com.google.common.io.Closeables;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0gs  reason: invalid class name and case insensitive filesystem */
public final class C09280gs {
    private static volatile C09280gs A03;
    public Map A00 = AnonymousClass0TG.A04();
    private final C04310Tq A01;
    private final C04310Tq A02;

    public static final C09280gs A00(AnonymousClass1XY r6) {
        if (A03 == null) {
            synchronized (C09280gs.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A03 = new C09280gs(AnonymousClass0WY.A01(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.BP6, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public void A01(String str) {
        this.A00.remove(str.substring(str.lastIndexOf(47) + 1));
        new File(AnonymousClass08S.A0J(str, "-uid")).delete();
    }

    public void A02(String str) {
        if (TriState.YES.equals(this.A01.get())) {
            User user = (User) this.A02.get();
            int lastIndexOf = str.lastIndexOf(47) + 1;
            String substring = str.substring(lastIndexOf);
            String str2 = (String) this.A00.get(substring);
            if (str2 == null) {
                File file = new File(AnonymousClass08S.A0J(str, "-uid"));
                if (!file.exists()) {
                    str2 = null;
                } else {
                    try {
                        str2 = new String(AnonymousClass0q2.A08(file), LogCatCollector.UTF_8_ENCODING);
                        this.A00.put(substring, str2);
                    } catch (IOException e) {
                        throw new IllegalStateException("Cannot Read From UID File", e);
                    }
                }
            }
            if (user == null) {
                if (str2 != null) {
                    throw new C37671w3("Expected uId to be null");
                }
            } else if (str2 == null) {
                String str3 = user.A0j;
                this.A00.put(str.substring(lastIndexOf), str3);
                RandomAccessFile randomAccessFile = null;
                try {
                    RandomAccessFile randomAccessFile2 = new RandomAccessFile(new File(AnonymousClass08S.A0J(str, "-uid")), "rw");
                    try {
                        randomAccessFile2.setLength(0);
                        randomAccessFile2.write(str3.getBytes(LogCatCollector.UTF_8_ENCODING));
                    } catch (IOException e2) {
                        e = e2;
                        randomAccessFile = randomAccessFile2;
                        try {
                            throw new C37671w3("Error when writing to a file", e);
                        } catch (Throwable th) {
                            th = th;
                            Closeables.A00(randomAccessFile, true);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        randomAccessFile = randomAccessFile2;
                        Closeables.A00(randomAccessFile, true);
                        throw th;
                    }
                    try {
                        Closeables.A00(randomAccessFile2, false);
                    } catch (IOException e3) {
                        throw new C37671w3("Error when closing file", e3);
                    }
                } catch (IOException e4) {
                    e = e4;
                    throw new C37671w3("Error when writing to a file", e);
                }
            } else if (!str2.equals(user.A0j)) {
                throw new C37671w3("Expected uId to be equal to loggedInUser");
            }
        }
    }

    private C09280gs(@LoggedInUser C04310Tq r2, C04310Tq r3) {
        this.A02 = r2;
        this.A01 = r3;
    }
}
