package X;

import android.os.Handler;

/* renamed from: X.0VH  reason: invalid class name */
public class AnonymousClass0VH extends AnonymousClass0VI {
    private final AnonymousClass0VE A00;

    public Runnable A01(Runnable runnable) {
        AnonymousClass0VE r0 = this.A00;
        super.A01(runnable);
        return r0.Bms(runnable);
    }

    public AnonymousClass0VH(Handler handler, AnonymousClass0VE r2) {
        super(handler);
        this.A00 = r2;
    }
}
