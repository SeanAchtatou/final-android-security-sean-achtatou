package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0XZ  reason: invalid class name */
public final class AnonymousClass0XZ {
    private static volatile AnonymousClass0XZ A02;
    public long A00;
    public final AnonymousClass069 A01;

    public static final AnonymousClass0XZ A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass0XZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass0XZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private AnonymousClass0XZ(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass067.A03(r2);
    }
}
