package X;

import com.facebook.common.time.AwakeTimeSinceBootClock;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.0Zu  reason: invalid class name */
public final class AnonymousClass0Zu {
    public static final AnonymousClass07J A02;
    public C05920aY A00;
    public ConcurrentMap A01;

    static {
        Class<AnonymousClass0Zu> cls = AnonymousClass0Zu.class;
        AnonymousClass06c r2 = new AnonymousClass06c(null, cls, AwakeTimeSinceBootClock.INSTANCE);
        r2.A03 = new C25461Zs(cls);
        A02 = r2.A00();
    }
}
