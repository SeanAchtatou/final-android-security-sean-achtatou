package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.0j2  reason: invalid class name */
public final class AnonymousClass0j2 {
    public final C09500hT A00;
    public final ListenableFuture A01;

    public AnonymousClass0j2(C09500hT r1, ListenableFuture listenableFuture) {
        this.A00 = r1;
        this.A01 = listenableFuture;
    }
}
