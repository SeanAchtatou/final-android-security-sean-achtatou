package X;

import android.content.Context;
import android.util.Log;
import java.io.File;

/* renamed from: X.01C  reason: invalid class name */
public final class AnonymousClass01C {
    public final int A00;
    public final long A01;

    public static File A00(Context context, boolean z) {
        if (z) {
            return new File(context.getApplicationInfo().dataDir, "insta_crash_remedy_log");
        }
        return new File(context.getApplicationInfo().dataDir, "remedy_log");
    }

    public AnonymousClass01C(long j, int i) {
        this.A01 = j;
        this.A00 = i;
    }

    public static void A01(Context context, boolean z) {
        if (!A00(context, z).delete()) {
            Log.w("CrashLoopRemedyLog", AnonymousClass08S.A0X("unable to delete remedy log, instaCrash: ", z));
        }
    }
}
