package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1O2  reason: invalid class name */
public final class AnonymousClass1O2 {
    public List A00;
    public Map A01;

    public void A00(AnonymousClass1O3 r2, AnonymousClass1O4 r3, C22761Ms r4) {
        if (this.A00 == null) {
            this.A00 = new ArrayList();
        }
        this.A00.add(r3);
        if (this.A01 == null) {
            this.A01 = new HashMap();
        }
        this.A01.put(r2, r4);
    }
}
