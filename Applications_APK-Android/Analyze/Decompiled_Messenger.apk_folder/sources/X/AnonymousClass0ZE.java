package X;

import android.util.SparseIntArray;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.0ZE  reason: invalid class name */
public final class AnonymousClass0ZE {
    public final AnonymousClass09P A00;
    public final AnonymousClass0US A01;
    public final C06510bc A02;

    public static C25451Zr A00(String str, AnonymousClass0jJ r11, AnonymousClass09P r12) {
        int i;
        SparseIntArray sparseIntArray = new SparseIntArray();
        SparseIntArray sparseIntArray2 = new SparseIntArray();
        Iterator fields = r11.readTree(str).fields();
        int i2 = 0;
        while (fields.hasNext()) {
            Map.Entry entry = (Map.Entry) fields.next();
            String str2 = (String) entry.getKey();
            JsonNode jsonNode = (JsonNode) entry.getValue();
            if (jsonNode.isLong()) {
                r12.CGY("qpl", "We do not support 64 bit integer samples/metadata, change code to support it");
            }
            if ("*".equals(str2)) {
                i2 = jsonNode.asInt();
            } else {
                short parseShort = Short.parseShort(str2);
                if (jsonNode.isNumber()) {
                    i = jsonNode.asInt();
                } else {
                    Iterator fields2 = jsonNode.fields();
                    i = -1;
                    while (fields2.hasNext()) {
                        Map.Entry entry2 = (Map.Entry) fields2.next();
                        String str3 = (String) entry2.getKey();
                        JsonNode jsonNode2 = (JsonNode) entry2.getValue();
                        if (jsonNode2.isLong()) {
                            r12.CGS("qpl", "We do not support 64 bit integer samples/metadata, change code to support it");
                        }
                        int asInt = jsonNode2.asInt();
                        if ("*".equals(str3)) {
                            i = asInt;
                        } else {
                            sparseIntArray2.put((parseShort << 16) | Short.parseShort(str3), asInt);
                        }
                    }
                }
                if (i != -1) {
                    sparseIntArray.put(parseShort, i);
                }
            }
        }
        return new C25451Zr(i2, sparseIntArray, sparseIntArray2);
    }

    public AnonymousClass0ZE(AnonymousClass0US r1, C06510bc r2, AnonymousClass09P r3) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = r3;
    }
}
