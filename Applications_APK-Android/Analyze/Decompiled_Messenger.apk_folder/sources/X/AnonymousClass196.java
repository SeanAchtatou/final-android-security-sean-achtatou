package X;

import com.facebook.crypto.keychain.KeyChain;

/* renamed from: X.196  reason: invalid class name */
public final class AnonymousClass196 implements KeyChain {
    private final KeyChain A00;
    private final Integer A01;

    private static void A00(byte[] bArr, int i, String str) {
        int length = bArr.length;
        if (length != i) {
            throw new IllegalStateException(AnonymousClass08S.A0N(str, " should be ", i, " bytes long but is ", length));
        }
    }

    public byte[] getCipherKey() {
        byte[] cipherKey = this.A00.getCipherKey();
        A00(cipherKey, AnonymousClass189.A03(this.A01), "Key");
        return cipherKey;
    }

    public byte[] getMacKey() {
        byte[] macKey = this.A00.getMacKey();
        A00(macKey, 64, "Mac");
        return macKey;
    }

    public byte[] getNewIV() {
        byte[] newIV = this.A00.getNewIV();
        A00(newIV, AnonymousClass189.A02(this.A01), "IV");
        return newIV;
    }

    public AnonymousClass196(KeyChain keyChain, Integer num) {
        this.A00 = keyChain;
        this.A01 = num;
    }
}
