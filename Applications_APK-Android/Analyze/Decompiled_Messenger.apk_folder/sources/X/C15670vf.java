package X;

/* renamed from: X.0vf  reason: invalid class name and case insensitive filesystem */
public final class C15670vf extends C191817c {
    public boolean A0S(C33781o8 r3, C33781o8 r4, int i, int i2, int i3, int i4) {
        if (r3 != r4) {
            A0B(r3);
        }
        A0B(r4);
        return true;
    }

    public C15670vf() {
        this.A00 = false;
    }

    public boolean A0P(C33781o8 r2) {
        A0K(r2);
        A0J(r2);
        A0B(r2);
        return true;
    }

    public boolean A0Q(C33781o8 r2) {
        A0O(r2);
        A0N(r2);
        A0B(r2);
        return true;
    }

    public boolean A0R(C33781o8 r2, int i, int i2, int i3, int i4) {
        A0M(r2);
        A0L(r2);
        A0B(r2);
        return true;
    }
}
