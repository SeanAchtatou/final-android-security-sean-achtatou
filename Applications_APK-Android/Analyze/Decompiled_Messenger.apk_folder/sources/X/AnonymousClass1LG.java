package X;

/* renamed from: X.1LG  reason: invalid class name */
public abstract class AnonymousClass1LG {
    public AnonymousClass1LI A00() {
        AnonymousClass1LF r0 = (AnonymousClass1LF) this;
        return new AnonymousClass1LI(r0.A01, r0.A04, r0.A02);
    }

    public String A01() {
        return ((AnonymousClass1LF) this).A00;
    }

    public String A02() {
        return ((AnonymousClass1LF) this).A01;
    }

    public String A03() {
        return ((AnonymousClass1LF) this).A03;
    }
}
