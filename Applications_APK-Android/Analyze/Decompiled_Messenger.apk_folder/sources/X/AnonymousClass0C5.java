package X;

import android.util.Pair;
import io.card.payment.BuildConfig;

/* renamed from: X.0C5  reason: invalid class name */
public final class AnonymousClass0C5 extends Pair {
    public static final AnonymousClass0C5 A01 = new AnonymousClass0C5(BuildConfig.FLAVOR, BuildConfig.FLAVOR, Long.MAX_VALUE);
    public final long A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0C5(String str, String str2, long j) {
        super(str == null ? BuildConfig.FLAVOR : str, str2 == null ? BuildConfig.FLAVOR : str2);
        this.A00 = j;
    }

    public String toString() {
        return "MqttDeviceIdAndSecret{id=" + ((String) this.first) + "secret=" + ((String) this.second) + "mTimestamp=" + this.A00 + '}';
    }
}
