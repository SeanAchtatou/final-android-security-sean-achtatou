package X;

import java.util.Random;

/* renamed from: X.0Oi  reason: invalid class name and case insensitive filesystem */
public final class C03520Oi implements C01840Bv {
    public int A00;
    public int A01 = 0;
    public final int A02;
    private final int A03;
    private final int A04;
    private final Random A05 = new Random();

    public boolean BBa(boolean z) {
        if (this.A01 < Integer.MAX_VALUE) {
            return true;
        }
        return false;
    }

    public int BLm(boolean z) {
        int i;
        this.A01++;
        int i2 = this.A00;
        if (z || i2 >= (i = this.A03)) {
            i = i2;
        }
        int nextFloat = (int) ((((double) this.A05.nextFloat()) + 0.5d) * ((double) Math.min(i << 1, this.A04)));
        this.A00 = nextFloat;
        return nextFloat;
    }

    public String toString() {
        return String.format(null, "BackoffRetryStrategy: attempt:%d/Infinite, delay:%d seconds", Integer.valueOf(this.A01), Integer.valueOf(this.A00));
    }

    public C03520Oi(int i, int i2, int i3) {
        this.A02 = i;
        this.A03 = i2;
        this.A04 = i3;
        this.A00 = i;
    }

    public C008107l B48() {
        return C008107l.BACK_OFF;
    }
}
