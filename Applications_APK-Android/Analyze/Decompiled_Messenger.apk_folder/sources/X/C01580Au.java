package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

/* renamed from: X.0Au  reason: invalid class name and case insensitive filesystem */
public final class C01580Au extends BroadcastReceiver {
    public final /* synthetic */ C01570At A00;

    public C01580Au(C01570At r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int i;
        int A01 = C000700l.A01(2071197917);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            C01570At.A00(this.A00, (NetworkInfo) intent.getParcelableExtra("networkInfo"));
            if (isInitialStickyBroadcast()) {
                C000700l.A0D(intent, -1812383513, A01);
                return;
            }
            C01570At r3 = this.A00;
            synchronized (r3) {
                NetworkInfo A03 = r3.A03();
                if (A03 == null || !A03.isConnected()) {
                    i = -1;
                } else {
                    i = A03.getType();
                }
                r3.A05();
                Intent intent2 = new Intent("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED");
                intent2.putExtra("com.facebook.mqtt.EXTRA_NETWORK_TYPE", i);
                for (AnonymousClass0AL BgN : r3.A07) {
                    BgN.BgN(intent2);
                }
            }
        }
        C000700l.A0D(intent, 893513987, A01);
    }
}
