package X;

/* renamed from: X.1Jh  reason: invalid class name and case insensitive filesystem */
public final class C21941Jh extends AnonymousClass1LQ {
    public static int A00(int[] iArr, int i, int i2, int i3) {
        if (i3 <= 8264) {
            while (i < i2) {
                if (i3 > iArr[i]) {
                    i += 2;
                }
            }
            return -2;
        }
        int i4 = i2 - 1;
        while (i <= i4) {
            int i5 = ((i + i4) >>> 2) << 1;
            int i6 = iArr[i5];
            if (i6 < i3) {
                i = i5 + 2;
            } else if (i6 <= i3) {
                return iArr[i5 + 1];
            } else {
                i4 = i5 - 2;
            }
        }
        if (i >= i2) {
            return -2;
        }
        return iArr[i + 1];
    }

    public C21941Jh(int[] iArr, int i, int i2) {
        super(iArr, i, i2);
    }
}
