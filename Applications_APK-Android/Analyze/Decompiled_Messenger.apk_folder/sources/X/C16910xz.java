package X;

import java.util.ArrayList;

/* renamed from: X.0xz  reason: invalid class name and case insensitive filesystem */
public final class C16910xz extends C12250ot {
    public final ArrayList A00;

    public static void A00(C16910xz r1, Object obj) {
        if (r1.A05) {
            r1.A00.add(obj);
            return;
        }
        throw new IllegalStateException("Expected object to be mutable");
    }

    public C16910xz A0E() {
        C16910xz A01 = this.A01.A01();
        A0G(A01);
        return A01;
    }

    public C12240os A0F() {
        C12240os A02 = this.A01.A02();
        A0G(A02);
        return A02;
    }

    public void A0G(C12250ot r3) {
        C000300h.A02(r3, "subParams cannot be null!");
        if (this.A05) {
            r3.A07();
            A00(this, r3);
            r3.A07();
            r3.A00 = this;
            return;
        }
        throw new IllegalStateException("Expected object to be mutable");
    }

    public C16910xz(int i) {
        this.A00 = new ArrayList(i);
    }
}
