package X;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.08Y  reason: invalid class name */
public final class AnonymousClass08Y {
    public int A00 = 0;
    public final ArrayList A01 = new ArrayList();

    public int A00(Iterable iterable) {
        int i = 0;
        if (iterable == null) {
            return 0;
        }
        synchronized (this.A01) {
            for (Object A012 : iterable) {
                i |= A01(A012);
            }
        }
        return i;
    }

    public int A01(Object obj) {
        int i;
        synchronized (this.A01) {
            Iterator it = this.A01.iterator();
            i = 0;
            int i2 = 0;
            while (it.hasNext()) {
                if (it.next().equals(obj)) {
                    i |= 1 << i2;
                }
                i2++;
            }
        }
        return i;
    }

    public int A02(Object obj) {
        int i;
        synchronized (this.A01) {
            if (this.A00 < 32) {
                this.A01.add(obj);
                int i2 = this.A00;
                i = 1 << i2;
                this.A00 = i2 + 1;
            } else {
                throw new IllegalStateException("Attempting to newEntry more than 32 entries.");
            }
        }
        return i;
    }

    public Set A03(int i) {
        HashSet hashSet = new HashSet();
        synchronized (this.A01) {
            Iterator it = this.A01.iterator();
            int i2 = 1;
            while (it.hasNext()) {
                Object next = it.next();
                if ((i2 & i) != 0) {
                    hashSet.add(next);
                }
                i2 <<= 1;
            }
        }
        return hashSet;
    }
}
