package X;

import android.util.Log;

/* renamed from: X.0E0  reason: invalid class name */
public abstract class AnonymousClass0E0 extends AnonymousClass0E1 {
    public AnonymousClass0E7 dequeueWork() {
        try {
            return super.dequeueWork();
        } catch (SecurityException e) {
            if (e.getMessage().contains("Caller no longer running")) {
                Log.e("CustomJobIntentService", AnonymousClass08S.A0J("Captured a \"Caller no longer running\" failure for ", getClass().getSimpleName()), e);
                return null;
            }
            throw e;
        }
    }
}
