package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.1q5  reason: invalid class name and case insensitive filesystem */
public final class C34731q5 {
    public static C34731q5 A0F;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public C34761q8 A05;
    public C34761q8 A06 = null;
    public Integer A07 = null;
    public Runnable A08 = null;
    public Runnable A09 = null;
    public Runnable A0A = null;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public final Handler A0E = new Handler(Looper.getMainLooper());

    public void A00() {
        if (this.A0D) {
            C35061qc r4 = new C35061qc(this);
            this.A08 = r4;
            AnonymousClass00S.A05(this.A0E, r4, (long) this.A04, 903454483);
        }
        if (this.A0C) {
            C60802xr r42 = new C60802xr(this);
            this.A0A = r42;
            AnonymousClass00S.A05(this.A0E, r42, (long) this.A02, -611326556);
        }
        if (this.A0B) {
            AAQ aaq = new AAQ(this);
            this.A09 = aaq;
            AnonymousClass00S.A05(this.A0E, aaq, (long) this.A00, -1699351423);
        }
    }

    public void A01() {
        Runnable runnable = this.A08;
        if (runnable != null) {
            AnonymousClass00S.A02(this.A0E, runnable);
            this.A08 = null;
        }
        Runnable runnable2 = this.A09;
        if (runnable2 != null) {
            AnonymousClass00S.A02(this.A0E, runnable2);
            this.A09 = null;
        }
        Runnable runnable3 = this.A0A;
        if (runnable3 != null) {
            AnonymousClass00S.A02(this.A0E, runnable3);
            this.A0A = null;
        }
        AnonymousClass00S.A04(this.A0E, new C35181qo(this), 2004757807);
    }
}
