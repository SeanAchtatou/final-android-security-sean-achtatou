package X;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import com.facebook.common.stringformat.StringFormatUtil;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

/* renamed from: X.09p  reason: invalid class name and case insensitive filesystem */
public final class C012809p {
    public static View A00(Activity activity, int i) {
        Preconditions.checkNotNull(activity);
        View findViewById = activity.findViewById(i);
        A04(findViewById, activity.getResources(), i);
        return findViewById;
    }

    public static View A01(View view, int i) {
        Preconditions.checkNotNull(view);
        View findViewById = view.findViewById(i);
        A04(findViewById, view.getResources(), i);
        return findViewById;
    }

    public static Optional A02(Activity activity, int i) {
        Preconditions.checkNotNull(activity);
        return Optional.fromNullable(activity.findViewById(i));
    }

    public static Optional A03(View view, int i) {
        Preconditions.checkNotNull(view);
        return Optional.fromNullable(view.findViewById(i));
    }

    private static void A04(View view, Resources resources, int i) {
        Preconditions.checkNotNull(resources);
        if (view == null) {
            throw new IllegalStateException(StringFormatUtil.formatStrLocaleSafe("Required view with ID %s not found. Either your layout is missing the ID you requested, or you want to use getOptionalView. Only use getOptionalView if you're sure that you need logic that depends on whether a particular child view exists.", resources.getResourceEntryName(i)));
        }
    }
}
