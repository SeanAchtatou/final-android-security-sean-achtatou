package X;

import android.os.SystemClock;
import android.util.SparseArray;

/* renamed from: X.0FO  reason: invalid class name */
public final class AnonymousClass0FO extends C007907e {
    public final SparseArray A00 = new SparseArray();
    public final AnonymousClass0D7 A01 = new AnonymousClass0D7();
    public final AnonymousClass0D7 A02 = new AnonymousClass0D7();

    public AnonymousClass0FM A03() {
        return new AnonymousClass0FL();
    }

    public boolean A04(AnonymousClass0FM r8) {
        long j;
        long j2;
        AnonymousClass0FL r82 = (AnonymousClass0FL) r8;
        synchronized (this) {
            C02740Gd.A00(r82, "Null value passed to getSnapshot!");
            AnonymousClass0D7 r1 = this.A01;
            r82.bleScanCount = r1.A00;
            AnonymousClass0D7 r6 = this.A02;
            r82.bleOpportunisticScanCount = r6.A00;
            long j3 = r1.A02;
            if (r1.A01 > 0) {
                j = SystemClock.uptimeMillis() - r1.A03;
            } else {
                j = 0;
            }
            r82.bleScanDurationMs = j3 + j;
            long j4 = r6.A02;
            if (r6.A01 > 0) {
                j2 = SystemClock.uptimeMillis() - r6.A03;
            } else {
                j2 = 0;
            }
            r82.bleOpportunisticScanDurationMs = j4 + j2;
        }
        return true;
    }
}
