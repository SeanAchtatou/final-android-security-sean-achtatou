package X;

import android.content.SharedPreferences;
import android.os.Bundle;

/* renamed from: X.0HF  reason: invalid class name */
public final class AnonymousClass0HF extends AnonymousClass0HB {
    public Object A01(SharedPreferences sharedPreferences, String str, Object obj) {
        long longValue;
        Long l = (Long) obj;
        if (l == null) {
            longValue = Long.MAX_VALUE;
        } else {
            longValue = l.longValue();
        }
        return Long.valueOf(sharedPreferences.getLong(str, longValue));
    }

    public Object A02(Bundle bundle, String str, Object obj) {
        long longValue;
        Long l = (Long) obj;
        if (l == null) {
            longValue = Long.MAX_VALUE;
        } else {
            longValue = l.longValue();
        }
        return Long.valueOf(bundle.getLong(str, longValue));
    }

    public void A03(SharedPreferences.Editor editor, String str, Object obj) {
        editor.putLong(str, ((Long) obj).longValue());
    }

    public void A04(Bundle bundle, String str, Object obj) {
        bundle.putLong(str, ((Long) obj).longValue());
    }

    public Class A00() {
        return Long.class;
    }
}
