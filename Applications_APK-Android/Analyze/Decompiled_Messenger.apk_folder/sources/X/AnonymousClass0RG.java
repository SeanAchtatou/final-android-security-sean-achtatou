package X;

import io.card.payment.BuildConfig;
import org.json.JSONObject;

/* renamed from: X.0RG  reason: invalid class name */
public final class AnonymousClass0RG {
    public Long A00 = 0L;
    public String A01 = BuildConfig.FLAVOR;
    public String A02 = BuildConfig.FLAVOR;
    public String A03 = BuildConfig.FLAVOR;
    public boolean A04 = false;

    public static AnonymousClass0RG A00(String str) {
        AnonymousClass0RG r3 = new AnonymousClass0RG();
        if (str == null) {
            return r3;
        }
        JSONObject jSONObject = new JSONObject(str);
        r3.A01 = jSONObject.optString("app_id");
        r3.A02 = jSONObject.optString("pkg_name");
        r3.A03 = jSONObject.optString("token");
        r3.A00 = Long.valueOf(jSONObject.optLong("time"));
        r3.A04 = jSONObject.optBoolean("invalid");
        return r3;
    }

    public String A01() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.putOpt("app_id", this.A01);
        jSONObject.putOpt("pkg_name", this.A02);
        jSONObject.putOpt("token", this.A03);
        jSONObject.putOpt("time", this.A00);
        jSONObject.putOpt("invalid", Boolean.valueOf(this.A04));
        return jSONObject.toString();
    }
}
