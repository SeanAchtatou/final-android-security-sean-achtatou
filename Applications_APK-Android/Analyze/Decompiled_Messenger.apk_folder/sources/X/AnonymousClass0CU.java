package X;

import android.os.PowerManager;

/* renamed from: X.0CU  reason: invalid class name */
public final class AnonymousClass0CU {
    public static final AnonymousClass0CU A01 = new AnonymousClass0CU();
    public final PowerManager.WakeLock A00;

    public void A00() {
        try {
            PowerManager.WakeLock wakeLock = this.A00;
            if (wakeLock != null) {
                C009007v.A01(wakeLock);
            }
        } catch (Throwable unused) {
        }
    }

    private AnonymousClass0CU() {
        this.A00 = null;
    }

    public AnonymousClass0CU(PowerManager powerManager) {
        this.A00 = C009007v.A00(powerManager, 1, "RtiWakeLock");
    }
}
