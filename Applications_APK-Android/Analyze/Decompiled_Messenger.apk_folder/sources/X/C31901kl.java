package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1kl  reason: invalid class name and case insensitive filesystem */
public final class C31901kl {
    private static volatile C31901kl A00;

    public static final C31901kl A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C31901kl.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C31901kl();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
