package X;

import android.content.Context;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.google.common.base.Platform;
import com.google.common.collect.ImmutableList;
import java.util.HashMap;

/* renamed from: X.0pM  reason: invalid class name and case insensitive filesystem */
public final class C12440pM extends C11150lz {
    public Context A00;
    public AnonymousClass0UN A01;

    public C12440pM(AnonymousClass1XY r3) {
        super("SmsServiceHandler");
        this.A01 = new AnonymousClass0UN(9, r3);
        this.A00 = AnonymousClass1YA.A00(r3);
    }

    public static MessagesCollection A00(C12440pM r7, long j, int i, long j2) {
        ImmutableList A0A = ((AnonymousClass9NU) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AJF, r7.A01)).A0A(j, i, j2);
        C33881oI r2 = new C33881oI();
        r2.A00 = ThreadKey.A03(j);
        r2.A01(A0A);
        boolean z = false;
        if (A0A.size() < i) {
            z = true;
        }
        r2.A03 = z;
        r2.A02 = true;
        return r2.A00();
    }

    public static FetchThreadResult A01(C12440pM r16, long j, int i) {
        long j2 = j;
        C12440pM r10 = r16;
        MessagesCollection A002 = A00(r10, j2, i, -1);
        HashMap hashMap = new HashMap();
        boolean z = true;
        String str = null;
        ThreadSummary A0B = ((C50462e4) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKa, r10.A01)).A0B(j2, hashMap);
        Message A05 = A002.A05();
        if (A05 != null) {
            String str2 = A05.A10;
            if (!C28841fS.A0M(A05) || Platform.stringIsNullOrEmpty(str2)) {
                int i2 = AnonymousClass1Y3.AnD;
                if (((C57262rh) AnonymousClass1XX.A02(0, i2, r10.A01)).A0D(A05)) {
                    str = ((C57262rh) AnonymousClass1XX.A02(0, i2, r10.A01)).A08(A05, A0B.A0e);
                } else if (Platform.stringIsNullOrEmpty(str2)) {
                    str = C57262rh.A04((C57262rh) AnonymousClass1XX.A02(0, i2, r10.A01), A05, A0B.A0e, true);
                }
            } else {
                str = str2;
            }
            C17920zh A003 = ThreadSummary.A00();
            A003.A02(A0B);
            A003.A0s = str2;
            A003.A0l = str;
            A003.A0P = A05.A0K;
            if (A05.A0S.A02 == C36891u4.A07) {
                z = false;
            }
            A003.A0y = z;
            A0B = A003.A00();
        }
        C61222yX A004 = FetchThreadResult.A00();
        A004.A01 = DataFetchDisposition.A0F;
        A004.A00 = ((AnonymousClass06B) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AgK, r10.A01)).now();
        A004.A04 = A0B;
        A004.A02 = A002;
        A004.A06 = ImmutableList.copyOf(hashMap.values());
        return A004.A00();
    }

    public static final C12440pM A02(AnonymousClass1XY r1) {
        return new C12440pM(r1);
    }
}
