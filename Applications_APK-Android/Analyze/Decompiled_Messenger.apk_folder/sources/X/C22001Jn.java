package X;

import com.facebook.common.dextricks.DexStore;
import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.1Jn  reason: invalid class name and case insensitive filesystem */
public abstract class C22001Jn {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "SIZE_36";
            case 2:
                return "SIZE_40";
            case 3:
                return "SIZE_62";
            default:
                return "SIZE_32";
        }
    }

    public int A01(int i) {
        switch (i) {
            case 65536:
                return 2131231186;
            case 65537:
                return 2131231187;
            case DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP:
                return 2132345033;
            case 131073:
                return 2132345034;
            case 196608:
                return 2131231188;
            case 196609:
                return 2131231189;
            case 262145:
                return 2132345035;
            case 327681:
                return 2131231191;
            case 393216:
                return 2131231192;
            case 393217:
                return 2131231193;
            case 458752:
                return 2132345036;
            case 458753:
                return 2132345037;
            case DexStore.LOAD_RESULT_WITH_VDEX_ODEX:
                return 2131231194;
            case 589824:
                return 2132345038;
            case 589825:
                return 2132345039;
            case 655360:
                return 2132345040;
            case 655361:
                return 2132345041;
            case 720896:
                return 2132345042;
            case 720897:
                return 2132345043;
            case 786432:
                return 2131231195;
            case 786433:
                return 2131231196;
            case 851968:
                return 2131231197;
            case 851969:
                return 2131231198;
            case 917504:
                return 2132345044;
            case 917505:
                return 2132345045;
            case 917506:
                return 2132345046;
            case 983042:
                return 2132345048;
            case 1048576:
                return 2131231199;
            case 1048577:
                return 2131231200;
            case 1114114:
                return 2132345049;
            case 1179648:
                return 2132345050;
            case 1179649:
                return 2132345051;
            case 1245184:
                return 2132345052;
            case 1245185:
                return 2132345053;
            case 1310720:
                return 2131231201;
            case 1376256:
                return 2131231202;
            case 1376257:
                return 2131231203;
            case 1441795:
                return 2131231204;
            case 1507328:
                return 2132345054;
            case 1507329:
                return 2132345055;
            case 1572864:
                return 2131231205;
            case 1638400:
                return 2132345059;
            case 1703936:
                return 2131231206;
            case 1703937:
                return 2131231207;
            case 1769472:
                return 2131231208;
            case 1769473:
                return 2131231209;
            case 1835008:
                return 2131231210;
            case 1835009:
                return 2131231211;
            case 1900544:
                return 2131231212;
            case 1900545:
                return 2131231213;
            case 1966080:
                return 2131231214;
            case 1966081:
                return 2131231215;
            case 2031616:
                return 2131231216;
            case 2031617:
                return 2131231217;
            case 2097152:
                return 2132345060;
            case 2097153:
                return 2132345061;
            case 2162688:
                return 2132345062;
            case 2162689:
                return 2132345063;
            case 2228226:
                return 2131231218;
            case 2293760:
                return 2132345064;
            case 2293761:
                return 2132345065;
            case 2359296:
                return 2132345066;
            case 2359297:
                return 2132345067;
            case 2424832:
                return 2131231219;
            case 2424833:
                return 2131231220;
            case 2490369:
                return 2131231221;
            case 2555904:
                return 2131231222;
            case 2555905:
                return 2131231223;
            case 2621440:
                return 2131231224;
            case 2686976:
                return 2132345068;
            case 2686977:
                return 2132345069;
            case 2752512:
                return 2131231225;
            case 2752513:
                return 2131231226;
            case 2818048:
                return 2132345070;
            case 2818049:
                return 2132345071;
            case 2883584:
                return 2132345072;
            case 2883585:
                return 2132345073;
            case 2949121:
                return 2132345074;
            case 3014656:
                return 2132345075;
            case 3014657:
                return 2132345076;
            case 3080192:
                return 2132345077;
            case 3080193:
                return 2132345078;
            case 3145728:
                return 2132345079;
            case 3145729:
                return 2131231227;
            case 3211264:
                return 2132345080;
            case 3211265:
                return 2132345081;
            case 3276800:
                return 2132345082;
            case 3276801:
                return 2132345083;
            case 3342336:
                return 2131231228;
            case 3342337:
                return 2131231229;
            case 3342338:
                return 2131231230;
            case 3407872:
                return 2132345084;
            case 3407873:
                return 2132345085;
            case 3473411:
                return 2131231231;
            case 3538944:
                return 2132345086;
            case 3538945:
                return 2132345087;
            case 3604480:
                return 2132345088;
            case 3604481:
                return 2132345089;
            case 3670016:
                return 2132345090;
            case 3670017:
                return 2132345091;
            case 3670018:
                return 2132345092;
            case 3735553:
                return 2131231232;
            case 3801088:
                return 2132345093;
            case 3801089:
                return 2132345094;
            case 3866624:
                return 2132345095;
            case 3866625:
                return 2132345096;
            case 3932160:
                return 2132345097;
            case 3997697:
                return 2131231233;
            case 4063232:
                return 2132345098;
            case 4063233:
                return 2132345099;
            case 4128768:
                return 2131231234;
            case 4128769:
                return 2131231235;
            case 4194304:
                return 2132345100;
            case 4194305:
                return 2132345101;
            case 4259840:
                return 2132345102;
            case 4259841:
                return 2132345103;
            case 4325379:
                return 2131231236;
            case 4390912:
                return 2132345104;
            case 4390913:
                return 2132345105;
            case 4456448:
                return 2131231237;
            case 4456449:
                return 2131231238;
            case 4521984:
                return 2131231239;
            case 4521985:
                return 2131231240;
            case 4587520:
                return 2132345106;
            case 4587521:
                return 2132345107;
            case 4653056:
                return 2132345108;
            case 4653057:
                return 2132345109;
            case 4718593:
                return 2131231241;
            case 4784128:
                return 2131231242;
            case 4784129:
                return 2131231243;
            case 4849664:
                return 2132345110;
            case 4849665:
                return 2132345111;
            case 4915200:
                return 2132345112;
            case 4915201:
                return 2132345113;
            case 4980736:
                return 2132345114;
            case 5046273:
                return 2131231244;
            case 5111808:
                return 2132345115;
            case 5111809:
                return 2132345116;
            default:
                return 0;
        }
    }

    public final int A02(C34891qL r9, Integer num) {
        String str;
        int ordinal = r9.ordinal() << 16;
        int A01 = A01(num.intValue() | ordinal);
        if (A01 != 0) {
            return A01;
        }
        ArrayList arrayList = new ArrayList();
        for (Integer num2 : AnonymousClass07B.A00(4)) {
            r9.ordinal();
            boolean z = false;
            if (A01(num2.intValue() | ordinal) != 0) {
                z = true;
            }
            if (z) {
                arrayList.add(A00(num2));
            }
        }
        String arrays = Arrays.toString(arrayList.toArray());
        StringBuilder sb = new StringBuilder("MIGIconName '");
        sb.append(r9);
        sb.append("' is not present in size '");
        if (num != null) {
            str = A00(num);
        } else {
            str = "null";
        }
        sb.append(str);
        sb.append("'. The available sizes for this icon are: ");
        sb.append(arrays);
        sb.append(". Please find the icon in the Asset Manager and follow the ");
        sb.append("instructions there to add it to the asset bundle.");
        throw new IllegalArgumentException(sb.toString());
    }
}
