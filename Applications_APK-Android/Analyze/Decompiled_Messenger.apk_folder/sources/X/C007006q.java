package X;

import io.card.payment.BuildConfig;
import java.util.List;
import java.util.Locale;

/* renamed from: X.06q  reason: invalid class name and case insensitive filesystem */
public final class C007006q {
    public static String A00(Locale locale, boolean z) {
        String displayName;
        String str;
        if (z) {
            displayName = locale.getDisplayName();
        } else {
            displayName = locale.getDisplayName(locale);
        }
        String locale2 = locale.toString();
        if ("fb".equals(locale2)) {
            displayName = "FB Hash";
        } else if ("qz".equals(locale2)) {
            Locale locale3 = new Locale("my");
            String displayName2 = locale3.getDisplayName(locale3);
            if (displayName2 == null || displayName2.isEmpty() || displayName2.equals("မြန်မာ")) {
                str = "ျမန္မာ";
            } else {
                str = "ဗမာ";
            }
            displayName = AnonymousClass08S.A0J(str, " (Zawgyi)");
        } else if ("mp".equalsIgnoreCase(locale2)) {
            displayName = "ꯃꯅꯤꯄꯨꯔꯤ";
        }
        return C06850cB.A04(displayName);
    }

    public static Locale A01(String str) {
        List A08 = C06850cB.A08(str, '_');
        if (A08.size() == 1) {
            A08 = C06850cB.A08(str, '-');
        }
        return new Locale((String) AnonymousClass0j4.A06(A08, BuildConfig.FLAVOR), (String) AnonymousClass0j4.A04(A08, 1, BuildConfig.FLAVOR), (String) AnonymousClass0j4.A04(A08, 2, BuildConfig.FLAVOR));
    }
}
