package X;

import android.text.TextUtils;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.1ud  reason: invalid class name */
public final class AnonymousClass1ud implements AnonymousClass0Z1 {
    public final /* synthetic */ C05820aO A00;

    public String Amj() {
        return null;
    }

    public AnonymousClass1ud(C05820aO r1) {
        this.A00 = r1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public String getCustomData(Throwable th) {
        if (this.A00.A01.isEmpty()) {
            return null;
        }
        String join = TextUtils.join(", ", this.A00.A01);
        C010708t.A0O("ForegroundServiceTracker", "pending foreground services: %s", join);
        String name = th.getClass().getName();
        for (String hashCode : this.A00.A01) {
            int hashCode2 = hashCode.hashCode();
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00.A00)).markerAnnotate(27721729, hashCode2, "exception_type", name);
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00.A00)).markerAnnotate(27721729, hashCode2, "exception_msg", th.getMessage());
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00.A00)).markerEnd(27721729, hashCode2, (short) 3);
        }
        return join;
    }
}
