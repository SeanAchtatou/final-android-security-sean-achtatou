package X;

import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1cH  reason: invalid class name and case insensitive filesystem */
public final class C26871cH {
    private static volatile C26871cH A07;
    public AnonymousClass0UN A00;
    public boolean A01;
    public final AnonymousClass0XN A02;
    public final C04480Uv A03;
    public final C12140oc A04;
    public final Executor A05;
    public final Executor A06;

    public static final C26871cH A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C26871cH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C26871cH(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        if (r0 == r9) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(long r9) {
        /*
            r8 = this;
            X.0XN r0 = r8.A02
            com.facebook.user.model.User r0 = r0.A09()
            if (r0 != 0) goto L_0x0028
            r0 = 0
        L_0x000a:
            r7 = 0
            r6 = 1
            r4 = 0
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 == 0) goto L_0x0017
            int r3 = (r0 > r9 ? 1 : (r0 == r9 ? 0 : -1))
            r2 = 0
            if (r3 != 0) goto L_0x0018
        L_0x0017:
            r2 = 1
        L_0x0018:
            com.google.common.base.Preconditions.checkArgument(r2)
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 != 0) goto L_0x0050
            X.0XN r0 = r8.A02
            com.facebook.user.model.User r1 = r0.A09()
            if (r1 != 0) goto L_0x0031
            return r7
        L_0x0028:
            X.0XN r0 = r8.A02
            com.facebook.user.model.User r0 = r0.A09()
            long r0 = r0.A0D
            goto L_0x000a
        L_0x0031:
            X.0cv r0 = new X.0cv
            r0.<init>()
            r0.A04(r1)
            r0.A0C = r9
            X.0XN r1 = r8.A02
            com.facebook.user.model.User r0 = r0.A02()
            r1.A0E(r0)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r0 = X.C06680bu.A0h
            r1.<init>(r0)
            X.0Uv r0 = r8.A03
            r0.A04(r1)
        L_0x0050:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26871cH.A01(long):boolean");
    }

    private C26871cH(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A05 = AnonymousClass0UX.A0T(r3);
        this.A06 = AnonymousClass0UX.A0U(r3);
        this.A03 = C04490Ux.A0n(r3);
        this.A02 = AnonymousClass0XN.A00(r3);
        this.A04 = C12140oc.A00(r3);
    }
}
