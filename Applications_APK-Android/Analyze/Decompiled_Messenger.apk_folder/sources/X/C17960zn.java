package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.GroupThreadData;

/* renamed from: X.0zn  reason: invalid class name and case insensitive filesystem */
public final class C17960zn implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new GroupThreadData(parcel);
    }

    public Object[] newArray(int i) {
        return new GroupThreadData[i];
    }
}
