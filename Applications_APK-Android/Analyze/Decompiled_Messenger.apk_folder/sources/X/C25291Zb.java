package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Zb  reason: invalid class name and case insensitive filesystem */
public final class C25291Zb implements ListenableFuture {
    public AnonymousClass1ZY A00;
    public AnonymousClass1YX A01;
    public boolean A02 = false;
    public final C04330Ua A03 = new C04330Ua();
    public final CountDownLatch A04 = new CountDownLatch(1);
    public volatile ListenableFuture A05 = null;

    public boolean cancel(boolean z) {
        AnonymousClass1ZX r1;
        this.A02 = true;
        if (this.A05 != null) {
            return this.A05.cancel(z);
        }
        AnonymousClass1YX r4 = this.A01;
        if (r4 == null) {
            return false;
        }
        int i = this.A00.A02;
        Iterator it = r4.A00.A03.iterator();
        while (true) {
            if (!it.hasNext()) {
                r1 = null;
                break;
            }
            r1 = (AnonymousClass1ZX) it.next();
            if (r1.A02 == i) {
                break;
            }
        }
        if (r1 != null) {
            return r4.A00.A03.remove(r1);
        }
        return false;
    }

    public void addListener(Runnable runnable, Executor executor) {
        this.A03.A02(runnable, executor);
    }

    public boolean isCancelled() {
        if (this.A02) {
            return true;
        }
        if (this.A05 == null || !this.A05.isCancelled()) {
            return false;
        }
        return true;
    }

    public boolean isDone() {
        if (this.A05 == null || !this.A05.isDone()) {
            return false;
        }
        return true;
    }

    public C25291Zb(AnonymousClass1ZY r3) {
        this.A00 = r3;
    }

    public Object get() {
        if (this.A05 == null) {
            try {
                this.A04.await();
            } catch (InterruptedException unused) {
            }
        }
        if (this.A05 != null) {
            return this.A05.get();
        }
        return null;
    }

    public Object get(long j, TimeUnit timeUnit) {
        if (this.A05 == null) {
            try {
                this.A04.await(j, timeUnit);
            } catch (InterruptedException unused) {
            }
        }
        if (this.A05 != null) {
            return this.A05.get(j, timeUnit);
        }
        return null;
    }
}
