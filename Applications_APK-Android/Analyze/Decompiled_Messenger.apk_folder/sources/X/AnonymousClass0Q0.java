package X;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/* renamed from: X.0Q0  reason: invalid class name */
public final class AnonymousClass0Q0 {
    public static byte[] A00(byte[] bArr) {
        Deflater deflater = new Deflater(9);
        deflater.setInput(bArr);
        deflater.finish();
        int length = bArr.length + 32;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(length);
        byte[] bArr2 = new byte[length];
        while (!deflater.finished()) {
            byteArrayOutputStream.write(bArr2, 0, deflater.deflate(bArr2));
        }
        try {
            byteArrayOutputStream.close();
        } catch (IOException e) {
            C010708t.A0T("ZlibCompressionUtil", e, "got io exception closing ByteArrayOutputStream");
        }
        deflater.end();
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] A01(byte[] bArr) {
        Inflater inflater = new Inflater();
        int length = bArr.length;
        inflater.setInput(bArr, 0, length);
        int i = length << 1;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(i);
        byte[] bArr2 = new byte[i];
        while (!inflater.finished()) {
            byteArrayOutputStream.write(bArr2, 0, inflater.inflate(bArr2, 0, i));
        }
        byteArrayOutputStream.close();
        inflater.end();
        return byteArrayOutputStream.toByteArray();
    }
}
