package X;

import android.content.Context;
import com.facebook.common.stringformat.StringFormatUtil;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ap  reason: invalid class name and case insensitive filesystem */
public final class C06090ap {
    private static volatile C06090ap A01;
    private final AnonymousClass010 A00;

    public static final C06090ap A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C06090ap.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C06090ap(AnonymousClass0UU.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static String A01(Context context, String str) {
        if (C90764Uy.A01 == null) {
            C90764Uy.A01 = new C90764Uy(context);
        }
        return C90764Uy.A01.A00.A02(str);
    }

    public String A02(String str) {
        return StringFormatUtil.formatStrLocaleSafe("com.facebook.intent.action.%s.%s", this.A00.name().toLowerCase(), str);
    }

    private C06090ap(AnonymousClass010 r1) {
        C05520Zg.A02(r1);
        this.A00 = r1;
    }
}
