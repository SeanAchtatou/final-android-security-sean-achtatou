package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.auth.viewercontext.ViewerContext;

/* renamed from: X.1Yq  reason: invalid class name and case insensitive filesystem */
public final class C25181Yq implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ViewerContext(parcel);
    }

    public Object[] newArray(int i) {
        return new ViewerContext[i];
    }
}
