package X;

import android.content.res.Resources;

/* renamed from: X.0Zf  reason: invalid class name and case insensitive filesystem */
public final class C05510Zf implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.fbui.drawable.CustomDrawablesCache$1";
    public final /* synthetic */ C07020cT A00;

    public C05510Zf(C07020cT r1) {
        this.A00 = r1;
    }

    public void run() {
        C005505z.A03("init", -1658058162);
        try {
            C07020cT r0 = this.A00;
            Resources resources = r0.A00;
            C05170Xx r02 = r0.A01;
            C07020cT.A04 = new C25821aS(resources, r02.A04, r02.A05);
        } finally {
            C005505z.A00(106552113);
            C07020cT.A05 = null;
            C07020cT.A03.countDown();
        }
    }
}
