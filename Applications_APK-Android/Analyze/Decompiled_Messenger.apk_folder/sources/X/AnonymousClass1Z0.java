package X;

import java.util.HashSet;

/* renamed from: X.1Z0  reason: invalid class name */
public final class AnonymousClass1Z0 {
    public HashSet A00;
    public final C25261Yy A01;
    public final String A02;
    public final int[] A03;

    public synchronized void A00(int i) {
        this.A00.add(Integer.valueOf(i));
    }

    public AnonymousClass1Z0(String str, C25261Yy r2, int[] iArr) {
        this.A02 = str;
        this.A01 = r2;
        this.A03 = iArr;
    }
}
