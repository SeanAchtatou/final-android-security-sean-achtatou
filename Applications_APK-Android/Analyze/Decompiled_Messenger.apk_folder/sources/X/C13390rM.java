package X;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.facebook.messaging.accountswitch.SwitchAccountActivity;

/* renamed from: X.0rM  reason: invalid class name and case insensitive filesystem */
public final class C13390rM implements DialogInterface.OnClickListener {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C13210qu A01;
    public final /* synthetic */ String A02;

    public C13390rM(C13210qu r1, Context context, String str) {
        this.A01 = r1;
        this.A00 = context;
        this.A02 = str;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        C417626w.A06(new Intent(this.A00, SwitchAccountActivity.class).putExtra(AnonymousClass24B.$const$string(235), "home").putExtra(AnonymousClass24B.$const$string(AnonymousClass1Y3.A1p), this.A02), this.A00);
    }
}
