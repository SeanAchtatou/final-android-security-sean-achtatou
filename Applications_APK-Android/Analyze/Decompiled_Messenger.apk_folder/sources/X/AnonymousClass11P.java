package X;

/* renamed from: X.11P  reason: invalid class name */
public enum AnonymousClass11P {
    TOKEN_BUFFER(2000),
    CONCAT_BUFFER(2000),
    TEXT_BUFFER(AnonymousClass1Y3.A1c),
    NAME_COPY_BUFFER(AnonymousClass1Y3.A1c);
    
    public final int size;

    private AnonymousClass11P(int i) {
        this.size = i;
    }
}
