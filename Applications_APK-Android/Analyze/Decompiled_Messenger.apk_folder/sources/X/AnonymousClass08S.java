package X;

/* renamed from: X.08S  reason: invalid class name */
public class AnonymousClass08S {
    public static String A00(char c, String str) {
        return c + str;
    }

    public static String A01(int i, String str) {
        return i + str;
    }

    public static String A02(int i, String str, int i2) {
        return i + str + i2;
    }

    public static String A03(int i, String str, String str2) {
        return i + str + str2;
    }

    public static String A04(long j, String str) {
        return j + str;
    }

    public static String A05(long j, String str, long j2) {
        return j + str + j2;
    }

    public static String A06(String str, char c) {
        return str + c;
    }

    public static String A07(String str, char c, String str2) {
        return str + c + str2;
    }

    public static String A08(String str, float f) {
        return str + f;
    }

    public static String A09(String str, int i) {
        return str + i;
    }

    public static String A0A(String str, int i, String str2) {
        return str + i + str2;
    }

    public static String A0B(String str, int i, String str2, int i2) {
        return str + i + str2 + i2;
    }

    public static String A0C(String str, int i, String str2, int i2, String str3) {
        return str + i + str2 + i2 + str3;
    }

    public static String A0D(String str, int i, String str2, int i2, String str3, int i3) {
        return str + i + str2 + i2 + str3 + i3;
    }

    public static String A0E(String str, int i, String str2, int i2, String str3, String str4) {
        return str + i + str2 + i2 + str3 + str4;
    }

    public static String A0F(String str, int i, String str2, String str3) {
        return str + i + str2 + str3;
    }

    public static String A0G(String str, long j) {
        return str + j;
    }

    public static String A0H(String str, long j, String str2) {
        return str + j + str2;
    }

    public static String A0I(String str, long j, String str2, long j2) {
        return str + j + str2 + j2;
    }

    public static String A0J(String str, String str2) {
        return str + str2;
    }

    public static String A0K(String str, String str2, char c) {
        return str + str2 + c;
    }

    public static String A0L(String str, String str2, int i) {
        return str + str2 + i;
    }

    public static String A0M(String str, String str2, int i, String str3) {
        return str + str2 + i + str3;
    }

    public static String A0N(String str, String str2, int i, String str3, int i2) {
        return str + str2 + i + str3 + i2;
    }

    public static String A0O(String str, String str2, long j) {
        return str + str2 + j;
    }

    public static String A0P(String str, String str2, String str3) {
        return str + str2 + str3;
    }

    public static String A0Q(String str, String str2, String str3, int i) {
        return str + str2 + str3 + i;
    }

    public static String A0R(String str, String str2, String str3, int i, String str4) {
        return str + str2 + str3 + i + str4;
    }

    public static String A0S(String str, String str2, String str3, String str4) {
        return str + str2 + str3 + str4;
    }

    public static String A0T(String str, String str2, String str3, String str4, String str5) {
        return str + str2 + str3 + str4 + str5;
    }

    public static String A0U(String str, String str2, String str3, String str4, String str5, String str6) {
        return str + str2 + str3 + str4 + str5 + str6;
    }

    public static String A0V(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        return str + str2 + str3 + str4 + str5 + str6 + str7;
    }

    public static String A0W(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        return str + str2 + str3 + str4 + str5 + str6 + str7 + str8 + str9;
    }

    public static String A0X(String str, boolean z) {
        return str + z;
    }

    public static String A0Y(String str, boolean z, String str2) {
        return str + z + str2;
    }
}
