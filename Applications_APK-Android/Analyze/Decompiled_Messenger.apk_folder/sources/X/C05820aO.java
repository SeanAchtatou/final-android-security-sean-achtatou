package X;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0aO  reason: invalid class name and case insensitive filesystem */
public final class C05820aO extends AnonymousClass0Wl implements C011409b {
    private static volatile C05820aO A02;
    public AnonymousClass0UN A00;
    public final Set A01 = Collections.synchronizedSet(new HashSet());

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public synchronized void CGz(Service service) {
        String name = service.getClass().getName();
        if (this.A01.contains(name)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(27721729, name.hashCode(), (short) 2);
        }
        this.A01.remove(name);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int, long, boolean):void
     arg types: [int, int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int, long, java.util.concurrent.TimeUnit):void
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int, java.lang.String, java.lang.String):void
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, java.lang.String, java.lang.String, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int, long, boolean):void */
    public synchronized void CH0(ComponentName componentName, Intent intent) {
        if (componentName == null && intent != null) {
            componentName = intent.getComponent();
        }
        if (componentName != null) {
            String className = componentName.getClassName();
            C010708t.A0O("ForegroundServiceTracker", "startForegroundService: %s", className);
            this.A01.add(className);
            int i = AnonymousClass1Y3.BBd;
            int hashCode = className.hashCode();
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerStart(27721729, hashCode, -1L, false);
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerAnnotate(27721729, hashCode, TurboLoader.Locator.$const$string(86), className);
        }
    }

    public String getSimpleName() {
        return "ForegroundServiceTracker";
    }

    public static final C05820aO A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C05820aO.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C05820aO(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static void A01(C05820aO r3) {
        ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r3.A00)).Bz9("pending_foreground_services", new AnonymousClass1ud(r3));
        C013309u.A00 = r3;
    }

    private C05820aO(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }

    public void BUe(Service service) {
        String name = service.getClass().getName();
        if (this.A01.contains(name)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerPoint(27721729, name.hashCode(), "onCreate");
        }
    }

    public void BpW(Service service) {
        String name = service.getClass().getName();
        if (this.A01.contains(name)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerPoint(27721729, name.hashCode(), "onStartCommand");
        }
    }

    public void init() {
        int A03 = C000700l.A03(266152910);
        ((C05000Xg) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B2R, this.A00)).A00(new C05610Zr(this), 96);
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B50, this.A00)).AbO(96, false)) {
            A01(this);
        }
        C000700l.A09(1829952306, A03);
    }
}
