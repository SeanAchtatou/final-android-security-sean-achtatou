package X;

import android.graphics.drawable.Drawable;
import com.facebook.acra.ACRA;
import com.facebook.litho.annotations.Comparable;
import java.util.List;

/* renamed from: X.1Ju  reason: invalid class name and case insensitive filesystem */
public final class C22071Ju extends C17770zR {
    public static final int A08;
    public static final int A09;
    public static final int A0A;
    public static final int A0B;
    @Comparable(type = 13)
    public Drawable A00;
    @Comparable(type = AnonymousClass1Y3.A01)
    public C17770zR A01;
    @Comparable(type = 13)
    public AnonymousClass1BY A02;
    @Comparable(type = 13)
    public AnonymousClass1MF A03;
    @Comparable(type = 13)
    public AnonymousClass1MD A04;
    @Comparable(type = 13)
    public String A05;
    @Comparable(type = ACRA.MULTI_SIGNAL_ANR_DETECTOR)
    public List A06;
    @Comparable(type = ACRA.MULTI_SIGNAL_ANR_DETECTOR)
    public List A07;

    static {
        AnonymousClass1JQ r2 = AnonymousClass1JQ.XLARGE;
        A09 = r2.B3A();
        AnonymousClass1JQ r1 = AnonymousClass1JQ.MEDIUM;
        A08 = r1.B3A();
        A0B = r1.B3A();
        A0A = r2.B3A();
    }

    public C22071Ju() {
        super("SwipeableListItemRowComponent");
    }

    public static C17770zR A00(AnonymousClass0p4 r6, List list, int i, int i2) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        C22041Jr r5 = new C22041Jr();
        C17540z4 r4 = r6.A0B;
        C17770zR r2 = r6.A04;
        if (r2 != null) {
            r5.A07 = r2.A06;
        }
        r5.A00 = r4.A00((float) AnonymousClass1JQ.MEDIUM.B3A());
        r5.A14().BwM(AnonymousClass10G.START, r4.A00((float) i));
        r5.A14().BwM(AnonymousClass10G.END, r4.A00((float) i2));
        if (list != null) {
            if (r5.A01.isEmpty()) {
                r5.A01 = list;
            } else {
                r5.A01.addAll(list);
                return r5;
            }
        }
        return r5;
    }

    public C17770zR A16() {
        C17770zR r0;
        C22071Ju r1 = (C22071Ju) super.A16();
        C17770zR r02 = r1.A01;
        if (r02 != null) {
            r0 = r02.A16();
        } else {
            r0 = null;
        }
        r1.A01 = r0;
        return r1;
    }
}
