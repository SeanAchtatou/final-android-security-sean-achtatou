package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.mms.MmsData;

/* renamed from: X.1u9  reason: invalid class name and case insensitive filesystem */
public final class C36921u9 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new MmsData(parcel);
    }

    public Object[] newArray(int i) {
        return new MmsData[i];
    }
}
