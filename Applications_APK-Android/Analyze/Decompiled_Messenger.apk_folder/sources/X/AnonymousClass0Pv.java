package X;

import com.facebook.inject.InjectorModule;
import java.util.concurrent.Executors;

@InjectorModule
/* renamed from: X.0Pv  reason: invalid class name */
public final class AnonymousClass0Pv extends AnonymousClass0UV {
    public static final AnonymousClass0Qc A00(AnonymousClass1XY r1) {
        return AnonymousClass0Qc.A00(r1).A00(Executors.newSingleThreadExecutor(new C03820Pt()));
    }
}
