package X;

/* renamed from: X.12O  reason: invalid class name */
public final class AnonymousClass12O implements C15390vD {
    private final C15390vD A00;
    private final C15390vD A01;

    public C15400vE Ae7() {
        C15400vE Ae7 = this.A00.Ae7();
        boolean z = false;
        if (Ae7.A00 <= 0) {
            z = true;
        }
        if (!z) {
            return Ae7;
        }
        return this.A01.Ae7();
    }

    public void BUD() {
        this.A00.BUD();
        this.A01.BUD();
    }

    public void CIL(C15960wG r2) {
        this.A00.CIL(r2);
        this.A01.CIL(r2);
    }

    public void CK1(C15960wG r2) {
        this.A00.CK1(r2);
        this.A01.CK1(r2);
    }

    public AnonymousClass12O(C15390vD r1, C15390vD r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
