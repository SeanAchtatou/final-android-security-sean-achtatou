package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem;

/* renamed from: X.1np  reason: invalid class name and case insensitive filesystem */
public final class C33591np implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new RankingLoggingItem(parcel);
    }

    public Object[] newArray(int i) {
        return new RankingLoggingItem[i];
    }
}
