package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1be  reason: invalid class name and case insensitive filesystem */
public final class C26561be extends AnonymousClass0f8 {
    private static volatile C26561be A00;

    public String Azg() {
        return "yoga_stats";
    }

    public static final C26561be A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C26561be.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C26561be();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        ELA ela = (ELA) obj;
        ELA ela2 = (ELA) obj2;
        if (!(ela == null || ela2 == null)) {
            performanceLoggingEvent.A06("yoga_measure_callbacks", ela2.A00 - ela.A00);
            long[] jArr = ela2.A01;
            int length = jArr.length;
            String[] strArr = new String[length];
            for (int i = 0; i < length; i++) {
                strArr[i] = Long.toString(jArr[i] - ela.A01[i]);
            }
            if (performanceLoggingEvent.A0K == null) {
                performanceLoggingEvent.A0K = new AnonymousClass1GD();
            }
            AnonymousClass1GD.A00(performanceLoggingEvent.A0K, "yoga_measure_callback_reasons_count", strArr);
        }
        performanceLoggingEvent.A04("yoga_node_count", 0);
        performanceLoggingEvent.A04("yoga_node_count_litho", 0);
        performanceLoggingEvent.A04("yoga_node_count_react_native_classic", 0);
    }

    public long Azh() {
        return C08350fD.A0H;
    }

    public Class B3O() {
        return ELA.class;
    }

    public Object CGO() {
        return new ELA();
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A0D;
    }
}
