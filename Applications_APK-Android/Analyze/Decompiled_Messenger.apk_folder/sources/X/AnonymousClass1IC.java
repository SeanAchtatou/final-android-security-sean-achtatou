package X;

import com.facebook.payments.logging.PaymentsLoggingSessionData;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1IC  reason: invalid class name */
public final class AnonymousClass1IC extends C20831Dz {
    public AnonymousClass0UN A00;
    public PaymentsLoggingSessionData A01;
    public ImmutableList A02;

    public static final AnonymousClass1IC A00(AnonymousClass1XY r1) {
        return new AnonymousClass1IC(r1);
    }

    public int ArU() {
        ImmutableList immutableList = this.A02;
        if (immutableList == null) {
            return 0;
        }
        return immutableList.size();
    }

    private AnonymousClass1IC(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
