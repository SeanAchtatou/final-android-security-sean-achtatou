package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.Map;

@UserScoped
/* renamed from: X.1Zv  reason: invalid class name and case insensitive filesystem */
public final class C25491Zv {
    private static C05540Zi A05;
    public C05160Xw A00;
    public C05650a2 A01;
    public C25501Zw A02;
    public C04310Tq A03;
    private final Map A04 = new AnonymousClass04a();

    public static final C25491Zv A00(AnonymousClass1XY r4) {
        C25491Zv r0;
        synchronized (C25491Zv.class) {
            C05540Zi A002 = C05540Zi.A00(A05);
            A05 = A002;
            try {
                if (A002.A03(r4)) {
                    A05.A00 = new C25491Zv((AnonymousClass1XY) A05.A01());
                }
                C05540Zi r1 = A05;
                r0 = (C25491Zv) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A05.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x006d, code lost:
        if (r6.equals("MESSENGER_DISCOVERY_GAMES_M4") == false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008d, code lost:
        if (r6.equals("MESSENGER_DISCOVERY_BUSINESSES") == false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0097, code lost:
        if (r6.equals("MESSENGER_DISCOVERY_FOR_YOU") == false) goto L_0x006f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C05530Zh A01(java.lang.String r23, java.lang.String r24, java.lang.String r25, java.lang.Long r26, java.lang.String r27, java.lang.String r28) {
        /*
            r22 = this;
            r7 = r24
            r6 = r23
            r8 = r25
            r9 = r26
            r11 = r28
            r10 = r27
            int r0 = X.AnonymousClass07K.A05(r6, r7, r8, r9, r10, r11)
            long r0 = (long) r0
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            r2 = r22
            java.util.Map r0 = r2.A04
            java.lang.Object r0 = r0.get(r1)
            X.0Zh r0 = (X.C05530Zh) r0
            if (r0 == 0) goto L_0x0022
            return r0
        L_0x0022:
            X.0a2 r13 = r2.A01
            X.1Zx r12 = new X.1Zx
            X.1Zy r14 = new X.1Zy
            r14.<init>(r13)
            X.0a8 r0 = new X.0a8
            r0.<init>(r13)
            X.0a9 r15 = X.C05680a9.A00(r13)
            r16 = r6
            r17 = r7
            r18 = r8
            r19 = r9
            r20 = r10
            r21 = r11
            r12.<init>(r13, r14, r15, r16, r17, r18, r19, r20, r21)
            X.0Zh r9 = new X.0Zh
            X.1Zw r10 = r2.A02
            X.0Xw r11 = r2.A00
            X.0Tq r0 = r2.A03
            java.lang.Object r13 = r0.get()
            X.0Ut r13 = (X.C04460Ut) r13
            int r5 = r6.hashCode()
            r0 = -1275542162(0xffffffffb3f8c56e, float:-1.1584312E-7)
            r4 = 2
            r3 = 1
            if (r5 == r0) goto L_0x0090
            r0 = -201186231(0xfffffffff4022449, float:-4.1243563E31)
            if (r5 == r0) goto L_0x0086
            r0 = 1148238464(0x4470ba80, float:962.91406)
            if (r5 != r0) goto L_0x006f
            java.lang.String r0 = "MESSENGER_DISCOVERY_GAMES_M4"
            boolean r0 = r6.equals(r0)
            r5 = 2
            if (r0 != 0) goto L_0x0070
        L_0x006f:
            r5 = -1
        L_0x0070:
            if (r5 == 0) goto L_0x0083
            if (r5 == r3) goto L_0x0081
            if (r5 == r4) goto L_0x0081
            java.lang.String r14 = X.C06680bu.A0E
        L_0x0078:
            r9.<init>(r10, r11, r12, r13, r14)
            java.util.Map r0 = r2.A04
            r0.put(r1, r9)
            return r9
        L_0x0081:
            r14 = 0
            goto L_0x0078
        L_0x0083:
            java.lang.String r14 = X.C06680bu.A0A
            goto L_0x0078
        L_0x0086:
            java.lang.String r0 = "MESSENGER_DISCOVERY_BUSINESSES"
            boolean r0 = r6.equals(r0)
            r5 = 1
            if (r0 != 0) goto L_0x0070
            goto L_0x006f
        L_0x0090:
            java.lang.String r0 = "MESSENGER_DISCOVERY_FOR_YOU"
            boolean r0 = r6.equals(r0)
            r5 = 0
            if (r0 != 0) goto L_0x0070
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25491Zv.A01(java.lang.String, java.lang.String, java.lang.String, java.lang.Long, java.lang.String, java.lang.String):X.0Zh");
    }

    private C25491Zv(AnonymousClass1XY r2) {
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.AKb, r2);
        this.A01 = new C05650a2(r2);
        this.A02 = C25501Zw.A00(r2);
        this.A00 = AnonymousClass0UX.A0F(r2);
    }
}
