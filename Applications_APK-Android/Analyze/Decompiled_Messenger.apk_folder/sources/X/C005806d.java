package X;

/* renamed from: X.06d  reason: invalid class name and case insensitive filesystem */
public class C005806d implements C005906e {
    public Class A00;

    public void BO1(Object obj) {
    }

    public void BlQ(Object obj) {
    }

    public Object create() {
        try {
            return this.A00.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            C010708t.A08(AnonymousClass07J.A0A, "Couldn't instantiate object", e);
            return null;
        }
    }

    public C005806d(Class cls) {
        this.A00 = cls;
    }
}
