package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.11N  reason: invalid class name */
public final class AnonymousClass11N extends Handler implements C31551js {
    public boolean BHG() {
        return false;
    }

    public void BxL(Runnable runnable, String str) {
        AnonymousClass00S.A04(this, runnable, 1416545395);
    }

    public void BxM(Runnable runnable, String str) {
        AnonymousClass00S.A03(this, runnable, 1085177248);
    }

    public AnonymousClass11N(Looper looper) {
        super(looper);
    }

    public void C1H(Runnable runnable) {
        AnonymousClass00S.A02(this, runnable);
    }
}
