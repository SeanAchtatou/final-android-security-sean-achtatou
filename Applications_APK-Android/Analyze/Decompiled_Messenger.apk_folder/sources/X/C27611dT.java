package X;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;
import com.facebook.surfaces.SurfaceDataCache$SurfaceContextLifecycleObserver;

/* renamed from: X.1dT  reason: invalid class name and case insensitive filesystem */
public final class C27611dT implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.surfaces.SurfaceDataCache$3";
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ C12290p1 A01;
    public final /* synthetic */ C27581dQ A02;
    public final /* synthetic */ Runnable A03;

    public C27611dT(C12290p1 r1, Activity activity, C27581dQ r3, Runnable runnable) {
        this.A01 = r1;
        this.A00 = activity;
        this.A02 = r3;
        this.A03 = runnable;
    }

    public void run() {
        AnonymousClass0qM AsK = ((FragmentActivity) this.A00).AsK();
        AsK.A06(new SurfaceDataCache$SurfaceContextLifecycleObserver(AsK, this.A02, this.A01, this.A03));
    }
}
