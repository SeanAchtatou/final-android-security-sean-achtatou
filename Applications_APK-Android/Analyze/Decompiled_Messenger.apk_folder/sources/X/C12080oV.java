package X;

import java.io.IOException;

/* renamed from: X.0oV  reason: invalid class name and case insensitive filesystem */
public final class C12080oV {
    public final AnonymousClass09P A00;
    public final AnonymousClass0US A01;
    private final AnonymousClass0jE A02;

    public C58012t1 A02(String str) {
        if (str == null) {
            return null;
        }
        try {
            return (C58012t1) this.A02.readValue(str, C58012t1.class);
        } catch (C37561vs | C37701w6 e) {
            throw new IllegalArgumentException(e);
        } catch (IOException unused) {
            this.A00.CGS("XMA", "IO Exception when reading XMA from JSON string.");
            return null;
        }
    }

    public static final C12080oV A00(AnonymousClass1XY r4) {
        return new C12080oV(C05040Xk.A00(), C04750Wa.A01(r4), AnonymousClass0VB.A00(AnonymousClass1Y3.AeU, r4));
    }

    public static final C12080oV A01(AnonymousClass1XY r4) {
        return new C12080oV(C05040Xk.A00(), C04750Wa.A01(r4), AnonymousClass0VB.A00(AnonymousClass1Y3.AeU, r4));
    }

    public String A03(C61192yU r3) {
        if (r3 == null) {
            return null;
        }
        try {
            return this.A02.writeValueAsString(C58012t1.A02(r3, C05850aR.A02()));
        } catch (C37571vt e) {
            throw new IllegalArgumentException(e);
        }
    }

    private C12080oV(AnonymousClass0jE r1, AnonymousClass09P r2, AnonymousClass0US r3) {
        this.A02 = r1;
        this.A00 = r2;
        this.A01 = r3;
    }
}
