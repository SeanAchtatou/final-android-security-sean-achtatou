package X;

import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1We  reason: invalid class name and case insensitive filesystem */
public final class C24591We {
    public final C24691Ws A00;
    public final String A01;

    public static String A00(Set set) {
        StringBuilder sb = new StringBuilder();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            C24571Wc r1 = (C24571Wc) it.next();
            sb.append(r1.A00());
            sb.append('/');
            sb.append(r1.A01());
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    public C24591We(Set set, C24691Ws r3) {
        this.A01 = A00(set);
        this.A00 = r3;
    }
}
