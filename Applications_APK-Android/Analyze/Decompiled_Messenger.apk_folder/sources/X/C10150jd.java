package X;

import com.facebook.acra.ACRA;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import java.io.Serializable;

@JsonAutoDetect(creatorVisibility = C10180jg.ANY, fieldVisibility = C10180jg.PUBLIC_ONLY, getterVisibility = C10180jg.PUBLIC_ONLY, isGetterVisibility = C10180jg.PUBLIC_ONLY, setterVisibility = C10180jg.ANY)
/* renamed from: X.0jd  reason: invalid class name and case insensitive filesystem */
public final class C10150jd implements C10160je, Serializable {
    public static final C10150jd DEFAULT = new C10150jd((JsonAutoDetect) C10150jd.class.getAnnotation(JsonAutoDetect.class));
    private static final long serialVersionUID = -7073939237187922755L;
    public final C10180jg _creatorMinLevel;
    public final C10180jg _fieldMinLevel;
    public final C10180jg _getterMinLevel;
    public final C10180jg _isGetterMinLevel;
    public final C10180jg _setterMinLevel;

    public boolean isFieldVisible(C29091fr r3) {
        return this._fieldMinLevel.isVisible(r3._field);
    }

    public boolean isGetterVisible(C29141fw r3) {
        return this._getterMinLevel.isVisible(r3._method);
    }

    public boolean isIsGetterVisible(C29141fw r3) {
        return this._isGetterMinLevel.isVisible(r3._method);
    }

    public boolean isSetterVisible(C29141fw r3) {
        return this._setterMinLevel.isVisible(r3._method);
    }

    public String toString() {
        return "[Visibility:" + " getter: " + this._getterMinLevel + ", isGetter: " + this._isGetterMinLevel + ", setter: " + this._setterMinLevel + ", creator: " + this._creatorMinLevel + ", field: " + this._fieldMinLevel + "]";
    }

    public /* bridge */ /* synthetic */ C10160je with(JsonAutoDetect jsonAutoDetect) {
        if (jsonAutoDetect != null) {
            return withGetterVisibility(jsonAutoDetect.getterVisibility()).withIsGetterVisibility(jsonAutoDetect.isGetterVisibility()).withSetterVisibility(jsonAutoDetect.setterVisibility()).withCreatorVisibility(jsonAutoDetect.creatorVisibility()).withFieldVisibility(jsonAutoDetect.fieldVisibility());
        }
        return this;
    }

    public boolean isCreatorVisible(C183512m r3) {
        return this._creatorMinLevel.isVisible(r3.getMember());
    }

    public /* bridge */ /* synthetic */ C10160je withVisibility(AnonymousClass0o9 r2, C10180jg r3) {
        switch (r2.ordinal()) {
            case 0:
                return withGetterVisibility(r3);
            case 1:
                return withSetterVisibility(r3);
            case 2:
                return withCreatorVisibility(r3);
            case 3:
                return withFieldVisibility(r3);
            case 4:
                return withIsGetterVisibility(r3);
            case 5:
            default:
                return this;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                if (r3 == C10180jg.DEFAULT) {
                    return DEFAULT;
                }
                return new C10150jd(r3);
        }
    }

    private C10150jd(C10180jg r3) {
        if (r3 == C10180jg.DEFAULT) {
            C10150jd r1 = DEFAULT;
            this._getterMinLevel = r1._getterMinLevel;
            this._isGetterMinLevel = r1._isGetterMinLevel;
            this._setterMinLevel = r1._setterMinLevel;
            this._creatorMinLevel = r1._creatorMinLevel;
            this._fieldMinLevel = r1._fieldMinLevel;
            return;
        }
        this._getterMinLevel = r3;
        this._isGetterMinLevel = r3;
        this._setterMinLevel = r3;
        this._creatorMinLevel = r3;
        this._fieldMinLevel = r3;
    }

    private C10150jd(C10180jg r1, C10180jg r2, C10180jg r3, C10180jg r4, C10180jg r5) {
        this._getterMinLevel = r1;
        this._isGetterMinLevel = r2;
        this._setterMinLevel = r3;
        this._creatorMinLevel = r4;
        this._fieldMinLevel = r5;
    }

    private C10150jd(JsonAutoDetect jsonAutoDetect) {
        this._getterMinLevel = jsonAutoDetect.getterVisibility();
        this._isGetterMinLevel = jsonAutoDetect.isGetterVisibility();
        this._setterMinLevel = jsonAutoDetect.setterVisibility();
        this._creatorMinLevel = jsonAutoDetect.creatorVisibility();
        this._fieldMinLevel = jsonAutoDetect.fieldVisibility();
    }

    /* access modifiers changed from: private */
    public C10150jd withCreatorVisibility(C10180jg r7) {
        C10180jg r4 = r7;
        if (r7 == C10180jg.DEFAULT) {
            r4 = DEFAULT._creatorMinLevel;
        }
        if (this._creatorMinLevel == r4) {
            return this;
        }
        return new C10150jd(this._getterMinLevel, this._isGetterMinLevel, this._setterMinLevel, r4, this._fieldMinLevel);
    }

    /* access modifiers changed from: private */
    public C10150jd withFieldVisibility(C10180jg r7) {
        C10180jg r5 = r7;
        if (r7 == C10180jg.DEFAULT) {
            r5 = DEFAULT._fieldMinLevel;
        }
        if (this._fieldMinLevel == r5) {
            return this;
        }
        return new C10150jd(this._getterMinLevel, this._isGetterMinLevel, this._setterMinLevel, this._creatorMinLevel, r5);
    }

    /* access modifiers changed from: private */
    public C10150jd withGetterVisibility(C10180jg r7) {
        C10180jg r1 = r7;
        if (r7 == C10180jg.DEFAULT) {
            r1 = DEFAULT._getterMinLevel;
        }
        if (this._getterMinLevel == r1) {
            return this;
        }
        return new C10150jd(r1, this._isGetterMinLevel, this._setterMinLevel, this._creatorMinLevel, this._fieldMinLevel);
    }

    /* access modifiers changed from: private */
    public C10150jd withIsGetterVisibility(C10180jg r7) {
        C10180jg r2 = r7;
        if (r7 == C10180jg.DEFAULT) {
            r2 = DEFAULT._isGetterMinLevel;
        }
        if (this._isGetterMinLevel == r2) {
            return this;
        }
        return new C10150jd(this._getterMinLevel, r2, this._setterMinLevel, this._creatorMinLevel, this._fieldMinLevel);
    }

    /* access modifiers changed from: private */
    public C10150jd withSetterVisibility(C10180jg r7) {
        C10180jg r3 = r7;
        if (r7 == C10180jg.DEFAULT) {
            r3 = DEFAULT._setterMinLevel;
        }
        if (this._setterMinLevel == r3) {
            return this;
        }
        return new C10150jd(this._getterMinLevel, this._isGetterMinLevel, r3, this._creatorMinLevel, this._fieldMinLevel);
    }
}
