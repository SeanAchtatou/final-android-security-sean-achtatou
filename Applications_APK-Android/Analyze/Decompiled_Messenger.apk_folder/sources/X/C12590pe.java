package X;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

/* renamed from: X.0pe  reason: invalid class name and case insensitive filesystem */
public class C12590pe extends Fragment implements DialogInterface.OnDismissListener, DialogInterface.OnCancelListener {
    public static final String __redex_internal_original_name = "androidx.fragment.app.DialogFragment";
    public int A00 = -1;
    public int A01 = 0;
    public int A02 = 0;
    public DialogInterface.OnCancelListener A03 = new C46572Qo(this);
    public DialogInterface.OnDismissListener A04 = new AnonymousClass2QY(this);
    public boolean A05 = true;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public Dialog A09;
    public boolean A0A = true;
    private Handler A0B;
    private Runnable A0C = new C46452Qc(this);

    public int A1z(C16290wo r3, String str) {
        this.A06 = false;
        this.A07 = true;
        r3.A0C(this, str);
        this.A08 = false;
        int A022 = r3.A02();
        this.A00 = A022;
        return A022;
    }

    public void A21() {
        A07(false, false);
    }

    public void A22() {
        A07(true, false);
    }

    public void A24(Dialog dialog, int i) {
        if (!(i == 1 || i == 2)) {
            if (i == 3) {
                dialog.getWindow().addFlags(24);
            } else {
                return;
            }
        }
        dialog.requestWindowFeature(1);
    }

    public void A25(C13060qW r2, String str) {
        this.A06 = false;
        this.A07 = true;
        C16290wo A0T = r2.A0T();
        A0T.A0C(this, str);
        A0T.A02();
    }

    public void onCancel(DialogInterface dialogInterface) {
    }

    private void A07(boolean z, boolean z2) {
        if (!this.A06) {
            this.A06 = true;
            this.A07 = false;
            Dialog dialog = this.A09;
            if (dialog != null) {
                dialog.setOnDismissListener(null);
                this.A09.dismiss();
                if (!z2) {
                    if (Looper.myLooper() == this.A0B.getLooper()) {
                        onDismiss(this.A09);
                    } else {
                        AnonymousClass00S.A04(this.A0B, this.A0C, -1984533926);
                    }
                }
            }
            this.A08 = true;
            if (this.A00 >= 0) {
                C13060qW A18 = A18();
                int i = this.A00;
                if (i >= 0) {
                    A18.A0w(new AnonymousClass6ZC(A18, null, i, 1), false);
                    this.A00 = -1;
                    return;
                }
                throw new IllegalArgumentException(AnonymousClass08S.A09("Bad id: ", i));
            }
            C16290wo A0T = A18().A0T();
            A0T.A0I(this);
            if (z) {
                A0T.A03();
            } else {
                A0T.A02();
            }
        }
    }

    public LayoutInflater A1d(Bundle bundle) {
        Context context;
        if (!this.A0A) {
            return super.A1d(bundle);
        }
        Dialog A20 = A20(bundle);
        this.A09 = A20;
        if (A20 != null) {
            A24(A20, this.A01);
            context = this.A09.getContext();
        } else {
            context = this.A0N.A01;
        }
        return (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int A1y() {
        return this.A02;
    }

    public Dialog A20(Bundle bundle) {
        return new Dialog(A11(), A1y());
    }

    public void A23(int i, int i2) {
        this.A01 = i;
        if (i == 2 || i == 3) {
            this.A02 = 16973913;
        }
        if (i2 != 0) {
            this.A02 = i2;
        }
    }

    public void A26(boolean z) {
        this.A05 = z;
        Dialog dialog = this.A09;
        if (dialog != null) {
            dialog.setCancelable(z);
        }
    }

    public void onDismiss(DialogInterface dialogInterface) {
        if (!this.A08) {
            A07(true, true);
        }
    }

    public void A1h(Bundle bundle) {
        int A022 = C000700l.A02(1208297191);
        super.A1h(bundle);
        this.A0B = new Handler();
        boolean z = false;
        if (this.A0D == 0) {
            z = true;
        }
        this.A0A = z;
        if (bundle != null) {
            this.A01 = bundle.getInt("android:style", 0);
            this.A02 = bundle.getInt("android:theme", 0);
            this.A05 = bundle.getBoolean("android:cancelable", true);
            this.A0A = bundle.getBoolean("android:showsDialog", this.A0A);
            this.A00 = bundle.getInt("android:backStackId", -1);
        }
        C000700l.A08(-441591193, A022);
    }

    public void A1m() {
        int A022 = C000700l.A02(-563155941);
        super.A1m();
        Dialog dialog = this.A09;
        if (dialog != null) {
            this.A08 = true;
            dialog.setOnDismissListener(null);
            this.A09.dismiss();
            if (!this.A06) {
                onDismiss(this.A09);
            }
            this.A09 = null;
        }
        C000700l.A08(618176553, A022);
    }

    public void A1n() {
        int A022 = C000700l.A02(-1242039940);
        super.A1n();
        if (!this.A07 && !this.A06) {
            this.A06 = true;
        }
        C000700l.A08(336420265, A022);
    }

    public void A1q() {
        int A022 = C000700l.A02(-105500898);
        super.A1q();
        Dialog dialog = this.A09;
        if (dialog != null) {
            this.A08 = false;
            dialog.show();
        }
        C000700l.A08(-212315428, A022);
    }

    public void A1r() {
        int A022 = C000700l.A02(88987751);
        super.A1r();
        Dialog dialog = this.A09;
        if (dialog != null) {
            dialog.hide();
        }
        C000700l.A08(969999624, A022);
    }

    public void A1s(Context context) {
        super.A1s(context);
        if (!this.A07) {
            this.A06 = false;
        }
    }

    public void A1t(Bundle bundle) {
        Bundle bundle2;
        int A022 = C000700l.A02(1684930327);
        super.A1t(bundle);
        if (!this.A0A) {
            C000700l.A08(1880406405, A022);
            return;
        }
        View view = this.A0I;
        if (view != null) {
            if (view.getParent() == null) {
                this.A09.setContentView(view);
            } else {
                IllegalStateException illegalStateException = new IllegalStateException("DialogFragment can not be attached to a container view");
                C000700l.A08(1317062338, A022);
                throw illegalStateException;
            }
        }
        FragmentActivity A15 = A15();
        if (A15 != null) {
            this.A09.setOwnerActivity(A15);
        }
        this.A09.setCancelable(this.A05);
        this.A09.setOnCancelListener(this.A03);
        this.A09.setOnDismissListener(this.A04);
        if (!(bundle == null || (bundle2 = bundle.getBundle("android:savedDialogState")) == null)) {
            this.A09.onRestoreInstanceState(bundle2);
        }
        C000700l.A08(-1542410601, A022);
    }

    public void A1u(Bundle bundle) {
        Bundle onSaveInstanceState;
        super.A1u(bundle);
        Dialog dialog = this.A09;
        if (!(dialog == null || (onSaveInstanceState = dialog.onSaveInstanceState()) == null)) {
            bundle.putBundle("android:savedDialogState", onSaveInstanceState);
        }
        int i = this.A01;
        if (i != 0) {
            bundle.putInt("android:style", i);
        }
        int i2 = this.A02;
        if (i2 != 0) {
            bundle.putInt("android:theme", i2);
        }
        boolean z = this.A05;
        if (!z) {
            bundle.putBoolean("android:cancelable", z);
        }
        boolean z2 = this.A0A;
        if (!z2) {
            bundle.putBoolean("android:showsDialog", z2);
        }
        int i3 = this.A00;
        if (i3 != -1) {
            bundle.putInt("android:backStackId", i3);
        }
    }
}
