package X;

/* renamed from: X.1z0  reason: invalid class name and case insensitive filesystem */
public final class C39411z0 implements Runnable {
    public static final String __redex_internal_original_name = "com.google.android.play.core.tasks.d";
    private final /* synthetic */ C51412ga A00;
    private final /* synthetic */ C39391yy A01;

    public C39411z0(C39391yy r1, C51412ga r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public final void run() {
        synchronized (this.A01.A01) {
            AnonymousClass90J r1 = this.A01.A00;
            if (r1 != null) {
                r1.BYd(this.A00.A04());
            }
        }
    }
}
