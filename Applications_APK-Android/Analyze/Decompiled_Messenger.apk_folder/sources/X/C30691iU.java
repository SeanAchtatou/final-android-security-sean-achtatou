package X;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* renamed from: X.1iU  reason: invalid class name and case insensitive filesystem */
public final class C30691iU implements C22761Ms {
    public AnonymousClass1S9 AWg(AnonymousClass1NY r5, int i, C33271nJ r7, C23541Px r8) {
        InputStream inputStream;
        try {
            inputStream = r5.A09();
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                AnonymousClass0h4.A00(inputStream, byteArrayOutputStream);
                C74663iH A00 = C74663iH.A00(ByteBuffer.wrap(byteArrayOutputStream.toByteArray()), false);
                AnonymousClass2R2.A01(inputStream);
                return A00;
            } catch (Exception e) {
                e = e;
                try {
                    e.printStackTrace();
                    AnonymousClass2R2.A01(inputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    AnonymousClass2R2.A01(inputStream);
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            inputStream = null;
            e.printStackTrace();
            AnonymousClass2R2.A01(inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            AnonymousClass2R2.A01(inputStream);
            throw th;
        }
    }
}
