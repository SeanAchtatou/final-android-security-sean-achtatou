package X;

import libcore.io.ErrnoException;
import libcore.io.OsConstants;

/* renamed from: X.0MW  reason: invalid class name */
public final class AnonymousClass0MW {
    public static int A00(Throwable th) {
        if (th instanceof ErrnoException) {
            return ((ErrnoException) th).errno;
        }
        return -1;
    }

    public static String A01(int i) {
        return OsConstants.errnoName(i);
    }
}
