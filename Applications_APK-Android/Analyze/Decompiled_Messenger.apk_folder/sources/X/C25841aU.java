package X;

import android.util.SparseArray;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.1aU  reason: invalid class name and case insensitive filesystem */
public final class C25841aU extends C08190ep {
    public final SparseArray A00;
    public final List A01;
    public final C04310Tq A02;
    private final AnonymousClass0Ti A03;

    public static void A00(C25841aU r16, C04270Tg r17) {
        String sb;
        int i;
        C04270Tg r7 = r17;
        List list = (List) r16.A00.get(r7.A02);
        if (list != null) {
            for (int i2 = 0; i2 < list.size(); i2++) {
                C26111av r9 = (C26111av) list.get(i2);
                C25981ai[] r10 = r9.A00.A03;
                if (r10 == null) {
                    sb = BuildConfig.FLAVOR;
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    boolean z = false;
                    for (C25981ai r1 : r10) {
                        if (z) {
                            sb2.append(":");
                        } else {
                            z = true;
                        }
                        sb2.append(r1.AkW(r7));
                    }
                    sb = sb2.toString();
                }
                C72933fE r4 = (C72933fE) r9.A01.get(sb);
                if (r4 == null) {
                    r4 = new C72933fE(r9.A00, r7);
                    r9.A01.put(sb, r4);
                }
                int i3 = 0;
                int i4 = 0;
                int i5 = 0;
                for (C26071ar r92 : r9.A00.A02) {
                    if (r92 instanceof C26081as) {
                        int[] iArr = r4.A03;
                        iArr[i3] = iArr[i3] + ((C26031an) ((C26081as) r92).A00).B8E(r7);
                        i3++;
                    } else {
                        if (r92 instanceof C72943fF) {
                            i = i4 + 1;
                            double[] dArr = r4.A01;
                            dArr[i4] = dArr[i4] + ((C72953fG) ((C72943fF) r92).A00).getValue(r7);
                        } else if (r92 instanceof C72963fH) {
                            int i6 = i5 + 1;
                            C26051ap r93 = (C26051ap) ((C72963fH) r92).A00;
                            String[] strArr = r4.A05;
                            String A012 = r7.A01(r93.A00);
                            if (A012 == null) {
                                A012 = r93.A00;
                            }
                            strArr[i5] = A012;
                            i5 = i6;
                        } else if (r92 instanceof C72973fI) {
                            i = i4 + 1;
                            double[] dArr2 = r4.A01;
                            dArr2[i4] = ((dArr2[i4] * ((double) r4.A00)) + ((double) ((C26031an) ((C72973fI) r92).A00).B8E(r7))) / ((double) (r4.A00 + 1));
                        } else if (r92 instanceof C72983fJ) {
                            i = i4 + 1;
                            double[] dArr3 = r4.A01;
                            double d = dArr3[i4];
                            int i7 = r4.A00;
                            dArr3[i4] = ((d * ((double) i7)) + ((C72953fG) ((C72983fJ) r92).A00).getValue(r7)) / ((double) (i7 + 1));
                        } else {
                            throw new UnsupportedOperationException(AnonymousClass08S.A0J("Unsupported Aggregation: ", r92.getClass().getName()));
                        }
                        i4 = i;
                    }
                }
                r4.A00++;
            }
        }
    }

    public AnonymousClass0Ti AsW() {
        return this.A03;
    }

    public C25841aU(Set set, C12210op r14, C04310Tq r15) {
        SparseArray sparseArray = new SparseArray();
        this.A00 = sparseArray;
        this.A02 = r15;
        if (r14.BFh()) {
            ArrayList arrayList = new ArrayList();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                if (((C25961ag) it.next()).A00) {
                    C25971ah r4 = new C25971ah("images_outliers_android", new int[]{42663937}, new C25981ai[]{new C26011al(), new C26041ao(), new C26051ap(AnonymousClass24B.$const$string(1226), BuildConfig.FLAVOR)}, new C26071ar[]{new C26081as(new C26091at(new C26101au(), AnonymousClass1Y3.A87, 1, 0)), new C26081as(new C26091at(new C26101au(), AnonymousClass1Y3.A1c, 1, 0))});
                    C26111av r5 = new C26111av(r4);
                    arrayList.add(r5);
                    for (int i : r4.A01) {
                        Object obj = (List) sparseArray.get(i);
                        if (obj == null) {
                            obj = new ArrayList();
                            sparseArray.put(i, obj);
                        }
                        obj.add(r5);
                    }
                }
            }
            this.A01 = arrayList;
            int size = this.A00.size() + 1;
            int[] iArr = new int[size];
            iArr[0] = 46333953;
            for (int i2 = 1; i2 < size; i2++) {
                iArr[i2] = this.A00.keyAt(i2 - 1);
            }
            this.A03 = AnonymousClass0Ti.A00(iArr);
            return;
        }
        this.A01 = Collections.emptyList();
        this.A03 = AnonymousClass0Ti.A04;
    }
}
