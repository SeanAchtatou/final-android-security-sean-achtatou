package X;

import java.util.concurrent.Executor;

/* renamed from: X.1p7  reason: invalid class name and case insensitive filesystem */
public final class C34231p7 implements Executor {
    private final int A00;
    private final C08270ey A01;
    private final C07970eT A02;
    private final Executor A03;
    private final boolean A04;

    public void execute(Runnable runnable) {
        C07980eU r5;
        if (this.A04) {
            r5 = this.A02;
        } else {
            r5 = this.A01;
        }
        AnonymousClass07A.A04(this.A03, new C34251p9(runnable, r5, (long) this.A00), 170791165);
    }

    public C34231p7(AnonymousClass1XY r4, Executor executor, int i) {
        this.A01 = C08270ey.A00(r4);
        this.A02 = C07970eT.A00(r4);
        C25051Yd A002 = AnonymousClass0WT.A00(r4);
        this.A03 = executor;
        this.A00 = i;
        this.A04 = A002.Aem(287371966881189L);
    }
}
