package X;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;

/* renamed from: X.00A  reason: invalid class name */
public final class AnonymousClass00A extends AnonymousClass009 {
    public final /* synthetic */ AnonymousClass001 A00;

    public AnonymousClass00A(AnonymousClass001 r4) {
        this.A00 = r4;
        if (AnonymousClass001.A0l) {
            A05();
        }
        try {
            Class.forName("android.app.ActivityThread$ReceiverData").getDeclaredField("intent").setAccessible(true);
        } catch (ClassNotFoundException e) {
            Log.w("SplashScreenApplication", "Error trying to access class ActivityThread.ReceiverData", e);
        } catch (NoSuchFieldException e2) {
            Log.w("SplashScreenApplication", "No such field 'intent' on ReceiverDataClass", e2);
        }
    }

    public void A06(Handler handler, Message message) {
        boolean A04 = AnonymousClass001.A04(this.A00, this, handler, message);
        if (A04) {
            AnonymousClass001 r1 = this.A00;
            if (r1.A0U == null) {
                r1.A0U = new ArrayList();
            }
            r1.A0U.add(message);
            synchronized (AnonymousClass001.A0j) {
                AnonymousClass001.A0h = true;
            }
        }
        if (!A04) {
            super.A06(handler, message);
        }
    }
}
