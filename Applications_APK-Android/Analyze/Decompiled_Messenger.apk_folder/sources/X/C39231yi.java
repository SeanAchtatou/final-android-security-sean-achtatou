package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1yi  reason: invalid class name and case insensitive filesystem */
public final class C39231yi {
    private AnonymousClass0UN A00;

    public static final C39231yi A00(AnonymousClass1XY r1) {
        return new C39231yi(r1);
    }

    public boolean A01(String str) {
        boolean z;
        String str2;
        boolean z2 = false;
        C90164Sc r1 = (C90164Sc) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AfC, this.A00);
        if (!r1.A00.containsKey(str) || (str2 = (String) r1.A00.get(str)) == null) {
            z = false;
        } else {
            z = str2.equals(BuildConfig.FLAVOR);
        }
        if (!z) {
            z2 = true;
            if (!((C90164Sc) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AfC, this.A00)).A00.containsKey(str)) {
                if (((AnonymousClass5OV) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AFl, this.A00)).A01() == 2131825466) {
                    return true;
                }
                return false;
            }
        }
        return z2;
    }

    private C39231yi(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
