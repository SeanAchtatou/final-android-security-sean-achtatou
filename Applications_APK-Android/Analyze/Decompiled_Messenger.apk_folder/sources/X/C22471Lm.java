package X;

import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1Lm  reason: invalid class name and case insensitive filesystem */
public final class C22471Lm implements AnonymousClass1NH {
    public final /* synthetic */ AnonymousClass1BE A00;

    public C22471Lm(AnonymousClass1BE r1) {
        this.A00 = r1;
    }

    public void Bri(InboxUnitThreadItem inboxUnitThreadItem, C21361Gm r3) {
        AnonymousClass1BE r0 = this.A00;
        if (r0 != null) {
            r0.Bg1(inboxUnitThreadItem);
        }
    }
}
