package X;

import com.facebook.common.perftest.PerfTestConfig;
import com.facebook.common.perftest.base.PerfTestConfigBase;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Zt  reason: invalid class name */
public final class AnonymousClass0Zt implements C25531Zz {
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    private static volatile AnonymousClass0Zt A03;
    private FbSharedPreferences A00;

    static {
        AnonymousClass1Y7 r1 = C04350Ue.A06;
        A01 = (AnonymousClass1Y7) r1.A09("perfmarker_to_logcat");
        A02 = (AnonymousClass1Y7) r1.A09("perfmarker_send_all");
    }

    public static final AnonymousClass0Zt A00(AnonymousClass1XY r5) {
        if (A03 == null) {
            synchronized (AnonymousClass0Zt.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        FbSharedPreferences A003 = FbSharedPreferencesModule.A00(applicationInjector);
                        PerfTestConfig.A01(applicationInjector);
                        A03 = new AnonymousClass0Zt(A003);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public boolean BDf() {
        return PerfTestConfigBase.A02;
    }

    public TriState BG3() {
        if (!this.A00.BFQ()) {
            return TriState.UNSET;
        }
        if (this.A00.Aep(A01, false) || Boolean.valueOf(AnonymousClass00I.A02("perfmarker_to_logcat")).booleanValue()) {
            return TriState.YES;
        }
        return TriState.NO;
    }

    public boolean BG7() {
        return PerfTestConfigBase.A03;
    }

    public TriState BGm() {
        if (!this.A00.BFQ()) {
            return TriState.UNSET;
        }
        if (this.A00.Aep(A02, false) || Boolean.valueOf(AnonymousClass00I.A02("perfmarker_send_all")).booleanValue()) {
            return TriState.YES;
        }
        return TriState.NO;
    }

    private AnonymousClass0Zt(FbSharedPreferences fbSharedPreferences) {
        this.A00 = fbSharedPreferences;
    }

    public boolean BGR() {
        return PerfTestConfigBase.A00();
    }
}
