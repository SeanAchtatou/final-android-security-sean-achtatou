package X;

import com.fasterxml.jackson.core.json.PackageVersion;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.Writer;
import java.lang.ref.SoftReference;

/* renamed from: X.0jz  reason: invalid class name and case insensitive filesystem */
public class C10370jz implements AnonymousClass0jN, Serializable {
    public static final int DEFAULT_FACTORY_FEATURE_FLAGS;
    public static final int DEFAULT_GENERATOR_FEATURE_FLAGS;
    public static final int DEFAULT_PARSER_FEATURE_FLAGS;
    private static final C10240jm DEFAULT_ROOT_VALUE_SEPARATOR = C10200ji.DEFAULT_ROOT_VALUE_SEPARATOR;
    public static final ThreadLocal _recyclerRef = new ThreadLocal();
    private static final long serialVersionUID = 8726401676402117450L;
    public C21221A1y _characterEscapes;
    public int _factoryFeatures;
    public int _generatorFeatures;
    public AnonymousClass5LM _inputDecorator;
    public C09980jK _objectCodec;
    public AnonymousClass6O9 _outputDecorator;
    public int _parserFeatures;
    public final transient C26721bu _rootByteSymbols;
    public final transient C10410k3 _rootCharSymbols;
    public C10240jm _rootValueSeparator;

    private C11710np _createGenerator(Writer writer, AnonymousClass11K r5) {
        C48722aw r2 = new C48722aw(r5, this._generatorFeatures, this._objectCodec, writer);
        C21221A1y a1y = this._characterEscapes;
        if (a1y != null) {
            r2._characterEscapes = a1y;
            if (a1y == null) {
                r2._outputEscapes = C48732ax.sOutputEscapes;
            } else {
                r2._outputEscapes = a1y.getEscapeCodesForAscii();
            }
        }
        C10240jm r1 = this._rootValueSeparator;
        if (r1 != DEFAULT_ROOT_VALUE_SEPARATOR) {
            r2._rootValueSeparator = r1;
        }
        return r2;
    }

    public static C28271eX _createParser(C10370jz r7, InputStream inputStream, AnonymousClass11K r9) {
        return new C48382aL(r9, inputStream).constructParser(r7._parserFeatures, r7._objectCodec, r7._rootByteSymbols, r7._rootCharSymbols, r7.isEnabled(C10380k0.CANONICALIZE_FIELD_NAMES), r7.isEnabled(C10380k0.INTERN_FIELD_NAMES));
    }

    private C11710np _createUTF8Generator(OutputStream outputStream, AnonymousClass11K r5) {
        C21219A1w a1w = new C21219A1w(r5, this._generatorFeatures, this._objectCodec, outputStream);
        C21221A1y a1y = this._characterEscapes;
        if (a1y != null) {
            a1w._characterEscapes = a1y;
            if (a1y == null) {
                a1w._outputEscapes = C48732ax.sOutputEscapes;
            } else {
                a1w._outputEscapes = a1y.getEscapeCodesForAscii();
            }
        }
        C10240jm r1 = this._rootValueSeparator;
        if (r1 != DEFAULT_ROOT_VALUE_SEPARATOR) {
            a1w._rootValueSeparator = r1;
        }
        return a1w;
    }

    public static AnonymousClass11L _getBufferRecycler() {
        AnonymousClass11L r0;
        SoftReference softReference = (SoftReference) _recyclerRef.get();
        if (softReference == null) {
            r0 = null;
        } else {
            r0 = (AnonymousClass11L) softReference.get();
        }
        if (r0 != null) {
            return r0;
        }
        AnonymousClass11L r02 = new AnonymousClass11L();
        _recyclerRef.set(new SoftReference(r02));
        return r02;
    }

    private final boolean isEnabled(C10380k0 r4) {
        if (((1 << r4.ordinal()) & this._factoryFeatures) != 0) {
            return true;
        }
        return false;
    }

    public C09980jK getCodec() {
        return this._objectCodec;
    }

    public Object readResolve() {
        return new C10370jz(this, this._objectCodec);
    }

    public C11780nw version() {
        return PackageVersion.VERSION;
    }

    static {
        int i = 0;
        for (C10380k0 r1 : C10380k0.values()) {
            if (r1._defaultState) {
                i |= 1 << r1.ordinal();
            }
        }
        DEFAULT_FACTORY_FEATURE_FLAGS = i;
        int i2 = 0;
        for (C10390k1 r12 : C10390k1.values()) {
            if (r12._defaultState) {
                i2 |= 1 << r12.ordinal();
            }
        }
        DEFAULT_PARSER_FEATURE_FLAGS = i2;
        int i3 = 0;
        for (C10400k2 r13 : C10400k2.values()) {
            if (r13._defaultState) {
                i3 |= r13._mask;
            }
        }
        DEFAULT_GENERATOR_FEATURE_FLAGS = i3;
    }

    public String getFormatName() {
        if (getClass() == C10370jz.class) {
            return "JSON";
        }
        return null;
    }

    public C10370jz() {
        this(null);
    }

    public C10370jz(C09980jK r5) {
        this._rootCharSymbols = C10410k3.createRoot();
        long currentTimeMillis = System.currentTimeMillis();
        this._rootByteSymbols = new C26721bu(64, true, (((int) currentTimeMillis) + ((int) (currentTimeMillis >>> 32))) | 1);
        this._factoryFeatures = DEFAULT_FACTORY_FEATURE_FLAGS;
        this._parserFeatures = DEFAULT_PARSER_FEATURE_FLAGS;
        this._generatorFeatures = DEFAULT_GENERATOR_FEATURE_FLAGS;
        this._rootValueSeparator = DEFAULT_ROOT_VALUE_SEPARATOR;
        this._objectCodec = r5;
    }

    private C10370jz(C10370jz r5, C09980jK r6) {
        this._rootCharSymbols = C10410k3.createRoot();
        long currentTimeMillis = System.currentTimeMillis();
        this._rootByteSymbols = new C26721bu(64, true, (((int) currentTimeMillis) + ((int) (currentTimeMillis >>> 32))) | 1);
        this._factoryFeatures = DEFAULT_FACTORY_FEATURE_FLAGS;
        this._parserFeatures = DEFAULT_PARSER_FEATURE_FLAGS;
        this._generatorFeatures = DEFAULT_GENERATOR_FEATURE_FLAGS;
        this._rootValueSeparator = DEFAULT_ROOT_VALUE_SEPARATOR;
        this._objectCodec = null;
        this._factoryFeatures = r5._factoryFeatures;
        this._parserFeatures = r5._parserFeatures;
        this._generatorFeatures = r5._generatorFeatures;
        this._characterEscapes = r5._characterEscapes;
        this._inputDecorator = r5._inputDecorator;
        this._outputDecorator = r5._outputDecorator;
        this._rootValueSeparator = r5._rootValueSeparator;
    }

    public C11710np createGenerator(File file, C48392aM r6) {
        Writer outputStreamWriter;
        OutputStream fileOutputStream = new FileOutputStream(file);
        AnonymousClass11K r2 = new AnonymousClass11K(_getBufferRecycler(), fileOutputStream, true);
        r2._encoding = r6;
        C48392aM r0 = C48392aM.UTF8;
        if (r6 == r0) {
            AnonymousClass6O9 r02 = this._outputDecorator;
            if (r02 != null) {
                fileOutputStream = r02.decorate(r2, fileOutputStream);
            }
            return _createUTF8Generator(fileOutputStream, r2);
        }
        if (r6 == r0) {
            outputStreamWriter = new C1935494z(r2, fileOutputStream);
        } else {
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, r6._javaName);
        }
        AnonymousClass6O9 r03 = this._outputDecorator;
        if (r03 != null) {
            outputStreamWriter = r03.decorate(r2, outputStreamWriter);
        }
        return _createGenerator(outputStreamWriter, r2);
    }

    public C11710np createGenerator(OutputStream outputStream, C48392aM r5) {
        Writer outputStreamWriter;
        AnonymousClass11K r2 = new AnonymousClass11K(_getBufferRecycler(), outputStream, false);
        r2._encoding = r5;
        C48392aM r0 = C48392aM.UTF8;
        if (r5 == r0) {
            AnonymousClass6O9 r02 = this._outputDecorator;
            if (r02 != null) {
                outputStream = r02.decorate(r2, outputStream);
            }
            return _createUTF8Generator(outputStream, r2);
        }
        if (r5 == r0) {
            outputStreamWriter = new C1935494z(r2, outputStream);
        } else {
            outputStreamWriter = new OutputStreamWriter(outputStream, r5._javaName);
        }
        AnonymousClass6O9 r03 = this._outputDecorator;
        if (r03 != null) {
            outputStreamWriter = r03.decorate(r2, outputStreamWriter);
        }
        return _createGenerator(outputStreamWriter, r2);
    }

    public C11710np createGenerator(Writer writer) {
        AnonymousClass11K r1 = new AnonymousClass11K(_getBufferRecycler(), writer, false);
        AnonymousClass6O9 r0 = this._outputDecorator;
        if (r0 != null) {
            writer = r0.decorate(r1, writer);
        }
        return _createGenerator(writer, r1);
    }

    public C28271eX createParser(InputStream inputStream) {
        AnonymousClass11K r1 = new AnonymousClass11K(_getBufferRecycler(), inputStream, false);
        AnonymousClass5LM r0 = this._inputDecorator;
        if (r0 != null) {
            inputStream = r0.decorate(r1, inputStream);
        }
        return _createParser(this, inputStream, r1);
    }

    public C28271eX createParser(String str) {
        Reader stringReader = new StringReader(str);
        AnonymousClass11K r4 = new AnonymousClass11K(_getBufferRecycler(), stringReader, true);
        AnonymousClass5LM r0 = this._inputDecorator;
        if (r0 != null) {
            stringReader = r0.decorate(r4, stringReader);
        }
        return new C28241eU(r4, this._parserFeatures, stringReader, this._objectCodec, this._rootCharSymbols.makeChild(isEnabled(C10380k0.CANONICALIZE_FIELD_NAMES), isEnabled(C10380k0.INTERN_FIELD_NAMES)));
    }

    public C28271eX createParser(byte[] bArr) {
        InputStream decorate;
        AnonymousClass11K r3 = new AnonymousClass11K(_getBufferRecycler(), bArr, true);
        AnonymousClass5LM r2 = this._inputDecorator;
        if (r2 == null || (decorate = r2.decorate(r3, bArr, 0, bArr.length)) == null) {
            return new C48382aL(r3, bArr, 0, bArr.length).constructParser(this._parserFeatures, this._objectCodec, this._rootByteSymbols, this._rootCharSymbols, isEnabled(C10380k0.CANONICALIZE_FIELD_NAMES), isEnabled(C10380k0.INTERN_FIELD_NAMES));
        }
        return _createParser(this, decorate, r3);
    }
}
