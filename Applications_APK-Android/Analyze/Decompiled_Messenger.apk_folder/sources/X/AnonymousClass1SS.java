package X;

import java.io.Closeable;
import java.nio.ByteBuffer;

/* renamed from: X.1SS  reason: invalid class name */
public interface AnonymousClass1SS extends Closeable {
    ByteBuffer getByteBuffer();

    long getNativePtr();

    boolean isClosed();

    byte read(int i);

    int read(int i, byte[] bArr, int i2, int i3);

    int size();
}
