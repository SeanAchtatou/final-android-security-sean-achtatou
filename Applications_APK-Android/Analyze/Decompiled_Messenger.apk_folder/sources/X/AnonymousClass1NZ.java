package X;

import java.nio.ByteBuffer;

/* renamed from: X.1NZ  reason: invalid class name */
public interface AnonymousClass1NZ {
    void close();

    void copy(int i, AnonymousClass1NZ r2, int i2, int i3);

    ByteBuffer getByteBuffer();

    long getNativePtr();

    int getSize();

    long getUniqueId();

    boolean isClosed();

    byte read(int i);

    int read(int i, byte[] bArr, int i2, int i3);

    int write(int i, byte[] bArr, int i2, int i3);
}
