package X;

import android.util.SparseArray;

/* renamed from: X.05Z  reason: invalid class name */
public final class AnonymousClass05Z implements AnonymousClass057 {
    public int[] A00;
    public final SparseArray A01;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        if (r1 <= 0) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C008507p A00(int r3) {
        /*
            r2 = this;
            android.util.SparseArray r0 = r2.A01
            if (r0 == 0) goto L_0x000b
            int r1 = r0.size()
            r0 = 1
            if (r1 > 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            if (r0 != 0) goto L_0x0010
            r0 = 0
            return r0
        L_0x0010:
            android.util.SparseArray r0 = r2.A01
            java.lang.Object r0 = r0.get(r3)
            X.07p r0 = (X.C008507p) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass05Z.A00(int):X.07p");
    }

    public AnonymousClass05Z(SparseArray sparseArray) {
        this.A01 = sparseArray;
        if (sparseArray != null && sparseArray.size() > 0) {
            this.A00 = new int[this.A01.size()];
            for (int i = 0; i < this.A01.size(); i++) {
                this.A00[i] = this.A01.keyAt(i);
            }
        }
    }
}
