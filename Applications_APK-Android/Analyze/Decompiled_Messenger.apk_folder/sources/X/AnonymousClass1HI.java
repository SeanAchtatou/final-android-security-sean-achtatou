package X;

import android.os.Build;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1HI  reason: invalid class name */
public final class AnonymousClass1HI extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile Boolean A01;

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0050, code lost:
        if (r1 != false) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A00(X.AnonymousClass1XY r6) {
        /*
            java.lang.Boolean r0 = X.AnonymousClass1HI.A01
            if (r0 != 0) goto L_0x0067
            java.lang.Object r5 = X.AnonymousClass1HI.A00
            monitor-enter(r5)
            java.lang.Boolean r0 = X.AnonymousClass1HI.A01     // Catch:{ all -> 0x0064 }
            X.0WD r4 = X.AnonymousClass0WD.A00(r0, r6)     // Catch:{ all -> 0x0064 }
            if (r4 == 0) goto L_0x0062
            X.1XY r0 = r6.getApplicationInjector()     // Catch:{ all -> 0x005a }
            X.1HJ r3 = X.AnonymousClass1HJ.A00(r0)     // Catch:{ all -> 0x005a }
            com.facebook.common.util.TriState r1 = r3.A00     // Catch:{ all -> 0x005a }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET     // Catch:{ all -> 0x005a }
            if (r1 != r0) goto L_0x0027
            boolean r0 = com.facebook.jni.CpuCapabilitiesJni.nativeDeviceSupportsNeon()     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x002f
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x005a }
        L_0x0025:
            r3.A00 = r0     // Catch:{ all -> 0x005a }
        L_0x0027:
            com.facebook.common.util.TriState r1 = r3.A00     // Catch:{ all -> 0x005a }
            com.facebook.common.util.TriState r2 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x005a }
            r0 = 0
            if (r1 != r2) goto L_0x0033
            goto L_0x0032
        L_0x002f:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO     // Catch:{ all -> 0x005a }
            goto L_0x0025
        L_0x0032:
            r0 = 1
        L_0x0033:
            if (r0 != 0) goto L_0x0052
            com.facebook.common.util.TriState r1 = r3.A01     // Catch:{ all -> 0x005a }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET     // Catch:{ all -> 0x005a }
            if (r1 != r0) goto L_0x0045
            boolean r0 = com.facebook.jni.CpuCapabilitiesJni.nativeDeviceSupportsX86()     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0048
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x005a }
        L_0x0043:
            r3.A01 = r0     // Catch:{ all -> 0x005a }
        L_0x0045:
            com.facebook.common.util.TriState r0 = r3.A01     // Catch:{ all -> 0x005a }
            goto L_0x004b
        L_0x0048:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO     // Catch:{ all -> 0x005a }
            goto L_0x0043
        L_0x004b:
            r1 = 0
            if (r0 != r2) goto L_0x004f
            r1 = 1
        L_0x004f:
            r0 = 0
            if (r1 == 0) goto L_0x0053
        L_0x0052:
            r0 = 1
        L_0x0053:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x005a }
            X.AnonymousClass1HI.A01 = r0     // Catch:{ all -> 0x005a }
            goto L_0x005f
        L_0x005a:
            r0 = move-exception
            r4.A01()     // Catch:{ all -> 0x0064 }
            throw r0     // Catch:{ all -> 0x0064 }
        L_0x005f:
            r4.A01()     // Catch:{ all -> 0x0064 }
        L_0x0062:
            monitor-exit(r5)     // Catch:{ all -> 0x0064 }
            goto L_0x0067
        L_0x0064:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0064 }
            throw r0
        L_0x0067:
            java.lang.Boolean r0 = X.AnonymousClass1HI.A01
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1HI.A00(X.1XY):java.lang.Boolean");
    }

    public static final Boolean A01(AnonymousClass1XY r2) {
        return Boolean.valueOf(AnonymousClass0WT.A00(r2).Aem(2306126069033470122L));
    }

    public static final Boolean A02(AnonymousClass1XY r2) {
        return Boolean.valueOf(AnonymousClass0WT.A00(r2).Aem(2306126069033535659L));
    }

    public static final Boolean A03(AnonymousClass1XY r8) {
        C001500z r1;
        Boolean A002 = A00(r8);
        C25051Yd A003 = AnonymousClass0WT.A00(r8);
        C001300x A02 = AnonymousClass0UU.A02(r8);
        C33841oE A004 = C33841oE.A00(r8);
        boolean z = false;
        if (Build.VERSION.SDK_INT < 9 || (((r1 = A02.A02) != C001500z.A07 && r1 != C001500z.A0A && r1 != C001500z.A01 && r1 != C001500z.A0C) || !A004.A01(ECX.$const$string(30)))) {
            return false;
        }
        if (A002.booleanValue() && A003.Aem(2306126069033339048L)) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0020, code lost:
        if (r1.A01("android.hardware.camera.front") != false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A04(X.AnonymousClass1XY r6) {
        /*
            java.lang.Boolean r5 = A00(r6)
            X.1Yd r4 = X.AnonymousClass0WT.A00(r6)
            X.1oE r1 = X.C33841oE.A00(r6)
            r0 = 373(0x175, float:5.23E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            boolean r0 = r1.A01(r0)
            r3 = 0
            if (r0 != 0) goto L_0x0022
            java.lang.String r0 = "android.hardware.camera.front"
            boolean r0 = r1.A01(r0)
            r2 = 0
            if (r0 == 0) goto L_0x0023
        L_0x0022:
            r2 = 1
        L_0x0023:
            r0 = 2306126069033404585(0x20010171000108a9, double:1.5854223321087633E-154)
            boolean r0 = r4.Aem(r0)
            if (r0 == 0) goto L_0x0037
            if (r2 == 0) goto L_0x0037
            boolean r0 = r5.booleanValue()
            if (r0 == 0) goto L_0x0037
            r3 = 1
        L_0x0037:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1HI.A04(X.1XY):java.lang.Boolean");
    }
}
