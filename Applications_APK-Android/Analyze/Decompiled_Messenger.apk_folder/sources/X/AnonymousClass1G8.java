package X;

/* renamed from: X.1G8  reason: invalid class name */
public final class AnonymousClass1G8 {
    public AnonymousClass101 A00;
    public AnonymousClass101 A01;
    public final AnonymousClass1G9 A02;
    public final C19891Ac A03;
    public final C25051Yd A04;

    public static final AnonymousClass1G8 A00(AnonymousClass1XY r1) {
        return new AnonymousClass1G8(r1);
    }

    private AnonymousClass1G8(AnonymousClass1XY r2) {
        this.A03 = C19891Ac.A00(r2);
        this.A02 = AnonymousClass1G9.A00(r2);
        this.A04 = AnonymousClass0WT.A00(r2);
    }
}
