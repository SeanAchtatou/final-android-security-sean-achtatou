package X;

import com.facebook.payments.logging.PaymentsLoggingSessionData;
import com.facebook.payments.model.PaymentItemType;
import com.facebook.payments.shipping.model.ShippingAddressFormInput;

/* renamed from: X.1Cl  reason: invalid class name and case insensitive filesystem */
public final class C20441Cl extends C06020ai {
    public final /* synthetic */ PaymentsLoggingSessionData A00;
    public final /* synthetic */ PaymentItemType A01;
    public final /* synthetic */ C55122nZ A02;
    public final /* synthetic */ ShippingAddressFormInput A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ boolean A05;

    public C20441Cl(C55122nZ r1, PaymentsLoggingSessionData paymentsLoggingSessionData, String str, ShippingAddressFormInput shippingAddressFormInput, boolean z, PaymentItemType paymentItemType) {
        this.A02 = r1;
        this.A00 = paymentsLoggingSessionData;
        this.A04 = str;
        this.A03 = shippingAddressFormInput;
        this.A05 = z;
        this.A01 = paymentItemType;
    }
}
