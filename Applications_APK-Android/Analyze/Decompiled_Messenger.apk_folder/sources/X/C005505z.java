package X;

import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.05z  reason: invalid class name and case insensitive filesystem */
public final class C005505z {
    public static void A00(int i) {
        int i2 = AnonymousClass00n.A08;
        Logger.writeStandardEntry(i2, 6, 23, 0, 0, i, 0, 0);
        if (!TraceEvents.isEnabled(i2)) {
            AnonymousClass060.A00();
        }
    }

    public static void A01(int i) {
        int i2 = AnonymousClass00n.A08;
        Logger.writeStandardEntry(i2, 6, 23, 0, 0, i, 0, 0);
        if (!TraceEvents.isEnabled(i2)) {
            AnonymousClass060.A00();
        }
    }

    public static void A02(long j, int i) {
        int i2 = AnonymousClass00n.A08;
        Logger.writeStandardEntry(i2, 6, 23, 0, 0, i, 0, 0);
        if (!TraceEvents.isEnabled(i2)) {
            AnonymousClass060.A00();
        }
    }

    public static void A03(String str, int i) {
        String str2 = str;
        if (!TraceEvents.isEnabled(AnonymousClass00n.A08)) {
            AnonymousClass060.A01(str2, 0, null, null, null, null, null);
        } else {
            A04(str, i);
        }
    }

    public static void A04(String str, int i) {
        if (TraceEvents.isEnabled(AnonymousClass00n.A08)) {
            int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A08, 7, 22, 0, 0, i, 0, 0);
            if (str != null) {
                Logger.writeBytesEntry(AnonymousClass00n.A08, 1, 83, writeStandardEntry, str);
            }
        }
    }

    public static void A05(String str, Object obj, int i) {
        String str2 = str;
        Object obj2 = obj;
        if (!TraceEvents.isEnabled(AnonymousClass00n.A08)) {
            AnonymousClass060.A01(str2, 1, obj2, null, null, null, null);
        } else {
            A04(StringFormatUtil.formatStrLocaleSafe(str, obj), i);
        }
    }

    public static void A06(String str, Object obj, Object obj2, int i) {
        Object obj3 = obj;
        Object obj4 = obj2;
        String str2 = str;
        if (!TraceEvents.isEnabled(AnonymousClass00n.A08)) {
            AnonymousClass060.A01(str2, 2, obj3, obj4, null, null, null);
        } else {
            A04(StringFormatUtil.formatStrLocaleSafe(str, obj, obj2), i);
        }
    }
}
