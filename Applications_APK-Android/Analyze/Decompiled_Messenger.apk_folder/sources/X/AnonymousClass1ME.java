package X;

import android.view.View;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1ME  reason: invalid class name */
public final class AnonymousClass1ME implements AnonymousClass1MF {
    public final /* synthetic */ InboxUnitThreadItem A00;
    public final /* synthetic */ AnonymousClass1BG A01;
    public final /* synthetic */ AnonymousClass1BY A02;
    public final /* synthetic */ String A03;

    public AnonymousClass1ME(AnonymousClass1BY r1, AnonymousClass1BG r2, InboxUnitThreadItem inboxUnitThreadItem, String str) {
        this.A02 = r1;
        this.A01 = r2;
        this.A00 = inboxUnitThreadItem;
        this.A03 = str;
    }

    public void Bdx(View view) {
        this.A02.A02(this.A03);
    }

    public void onClick(View view) {
        this.A02.A01();
        AnonymousClass1BG r1 = this.A01;
        if (r1 != null) {
            r1.Bbw(this.A00);
        }
    }
}
