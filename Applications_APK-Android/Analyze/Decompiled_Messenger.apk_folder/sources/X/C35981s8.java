package X;

import com.facebook.messaging.business.common.calltoaction.model.AdCallToAction;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import io.card.payment.BuildConfig;

/* renamed from: X.1s8  reason: invalid class name and case insensitive filesystem */
public final class C35981s8 extends C188515m {
    public String A00;
    public String A01 = BuildConfig.FLAVOR;

    public C35981s8(CallToAction callToAction) {
        super(callToAction);
        if (callToAction instanceof AdCallToAction) {
            AdCallToAction adCallToAction = (AdCallToAction) callToAction;
            this.A00 = adCallToAction.A00;
            this.A01 = adCallToAction.A01;
        }
    }
}
