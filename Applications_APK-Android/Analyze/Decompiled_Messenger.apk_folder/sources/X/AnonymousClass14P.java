package X;

import android.content.pm.PackageInfo;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.14P  reason: invalid class name */
public final class AnonymousClass14P {
    public final AnonymousClass06B A00 = AnonymousClass067.A02();
    public final AnonymousClass14Q A01;
    public final C25051Yd A02;
    public final FbSharedPreferences A03;
    private final C14290sy A04;

    public static final AnonymousClass14P A00(AnonymousClass1XY r1) {
        return new AnonymousClass14P(r1);
    }

    public static boolean A01(AnonymousClass14P r8, long j) {
        String str;
        long At2 = r8.A03.At2(AnonymousClass5Z4.A01, -1);
        String B4F = r8.A03.B4F(AnonymousClass5Z4.A02, null);
        PackageInfo A042 = r8.A04.A04(TurboLoader.Locator.$const$string(17), 0);
        if (A042 != null) {
            str = A042.versionName;
        } else {
            str = null;
        }
        if (At2 != -1 && B4F != null && j == At2) {
            return B4F.equals(str);
        }
        C30281hn edit = r8.A03.edit();
        edit.BzA(AnonymousClass5Z4.A01, j);
        edit.BzC(AnonymousClass5Z4.A02, str);
        edit.commit();
        return true;
    }

    public AnonymousClass14P(AnonymousClass1XY r2) {
        this.A01 = new AnonymousClass14Q(r2);
        this.A02 = AnonymousClass0WT.A00(r2);
        this.A03 = FbSharedPreferencesModule.A00(r2);
        this.A04 = C14290sy.A02(r2);
    }
}
