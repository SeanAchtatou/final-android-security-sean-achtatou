package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0kh  reason: invalid class name and case insensitive filesystem */
public final class C10690kh extends AnonymousClass0UV {
    private static volatile AnonymousClass1Vu A00;

    public static final AnonymousClass1Vu A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass1Vu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass1Vu();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
