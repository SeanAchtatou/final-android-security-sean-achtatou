package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1OK  reason: invalid class name */
public final class AnonymousClass1OK {
    private static volatile AnonymousClass1OK A03;
    private C04980Xe A00;
    private AnonymousClass069 A01;
    private AnonymousClass0US A02;

    public static final AnonymousClass1OK A00(AnonymousClass1XY r7) {
        if (A03 == null) {
            synchronized (AnonymousClass1OK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A03 = new AnonymousClass1OK(AnonymousClass0VB.A00(AnonymousClass1Y3.Ac2, applicationInjector), AnonymousClass0Xd.A00(applicationInjector), AnonymousClass067.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public AnonymousClass1OS A01(String str) {
        return new AnonymousClass1OS(this.A02, str, this.A00, this.A01);
    }

    private AnonymousClass1OK(AnonymousClass0US r1, C04980Xe r2, AnonymousClass069 r3) {
        this.A02 = r1;
        this.A00 = r2;
        this.A01 = r3;
    }
}
