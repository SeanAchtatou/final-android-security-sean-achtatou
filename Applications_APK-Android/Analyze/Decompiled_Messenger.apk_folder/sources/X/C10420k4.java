package X;

/* renamed from: X.0k4  reason: invalid class name and case insensitive filesystem */
public final class C10420k4 {
    public final int _length;
    public final C10420k4 _next;
    public final String _symbol;

    public C10420k4(String str, C10420k4 r4) {
        this._symbol = str;
        this._next = r4;
        this._length = r4 != null ? 1 + r4._length : 1;
    }
}
