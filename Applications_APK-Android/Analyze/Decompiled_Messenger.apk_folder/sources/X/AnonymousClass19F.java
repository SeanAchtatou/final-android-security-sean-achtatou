package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.19F  reason: invalid class name */
public final class AnonymousClass19F extends AnonymousClass0W4 {
    private static final C06060am A00 = new C06050al(ImmutableList.of(C193717w.A0F));
    private static final ImmutableList A01 = ImmutableList.of(C193717w.A0F, C193717w.A09, C193717w.A08, C193717w.A0G, C193717w.A0H, C193717w.A05, C193717w.A0B, C193717w.A02, C193717w.A0C, C193717w.A0D, C193717w.A00, C193717w.A01, C193717w.A0A, C193717w.A04, C193717w.A03, C193717w.A06, C193717w.A07, C193717w.A0E);

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public AnonymousClass19F() {
        super("threads", A01, A00);
    }
}
