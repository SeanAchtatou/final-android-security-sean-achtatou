package X;

import android.util.Pair;
import java.io.Closeable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1Q9  reason: invalid class name */
public abstract class AnonymousClass1Q9 implements AnonymousClass1Q3 {
    public final AnonymousClass1Q3 A00;
    public final String A01;
    public final Map A02 = new HashMap();
    public final boolean A03;

    public Closeable A00(Closeable closeable) {
        return !(this instanceof AnonymousClass1Q8) ? AnonymousClass1PS.A00((AnonymousClass1PS) closeable) : AnonymousClass1NY.A03((AnonymousClass1NY) closeable);
    }

    public Object A01(AnonymousClass1QK r4) {
        return Pair.create(!(this instanceof AnonymousClass1Q8) ? ((AnonymousClass1QE) this).A00.A04(r4.A09, r4.A0A) : ((AnonymousClass1Q8) this).A00.A05(r4.A09, r4.A0A), r4.A08);
    }

    public synchronized void A02(Object obj, AnonymousClass1R4 r3) {
        if (this.A02.get(obj) == r3) {
            this.A02.remove(obj);
        }
    }

    public AnonymousClass1Q9(AnonymousClass1Q3 r2, String str, boolean z) {
        this.A00 = r2;
        this.A03 = z;
        this.A01 = str;
    }

    public void ByS(C23581Qb r11, AnonymousClass1QK r12) {
        boolean z;
        AnonymousClass1R4 r5;
        AnonymousClass1R4 r0;
        boolean z2;
        try {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("MultiplexProducer#produceResults");
            }
            r12.A07.Bk8(r12, this.A01);
            Object A012 = A01(r12);
            do {
                z = false;
                synchronized (this) {
                    try {
                        synchronized (this) {
                            r5 = (AnonymousClass1R4) this.A02.get(A012);
                        }
                        if (r5 == null) {
                            synchronized (this) {
                                r5 = new AnonymousClass1R4(this, A012);
                                this.A02.put(A012, r5);
                            }
                            z = true;
                        }
                    } catch (Throwable th) {
                        while (true) {
                            th = th;
                        }
                        throw th;
                    }
                }
                Pair create = Pair.create(r11, r12);
                synchronized (r5) {
                    try {
                        AnonymousClass1Q9 r2 = r5.A07;
                        Object obj = r5.A05;
                        synchronized (r2) {
                            r0 = (AnonymousClass1R4) r2.A02.get(obj);
                        }
                        if (r0 != r5) {
                            z2 = false;
                            continue;
                        } else {
                            r5.A06.add(create);
                            List A022 = AnonymousClass1R4.A02(r5);
                            List A032 = AnonymousClass1R4.A03(r5);
                            List A013 = AnonymousClass1R4.A01(r5);
                            Closeable closeable = r5.A04;
                            float f = r5.A00;
                            int i = r5.A01;
                            AnonymousClass1QK.A01(A022);
                            AnonymousClass1QK.A02(A032);
                            AnonymousClass1QK.A00(A013);
                            synchronized (create) {
                                try {
                                    synchronized (r5) {
                                        if (closeable != r5.A04) {
                                            closeable = null;
                                        } else if (closeable != null) {
                                            closeable = r5.A07.A00(closeable);
                                        }
                                    }
                                    if (closeable != null) {
                                        if (f > 0.0f) {
                                            r11.BkJ(f);
                                        }
                                        r11.Bgg(closeable, i);
                                        AnonymousClass1R4.A05(closeable);
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    throw th;
                                }
                            }
                            r12.A06(new AnonymousClass1R5(r5, create));
                            z2 = true;
                            continue;
                        }
                    } catch (Throwable th3) {
                        while (true) {
                            th = th3;
                            break;
                        }
                    }
                }
            } while (!z2);
            if (z) {
                AnonymousClass1R4.A04(r5);
            }
        } finally {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
        }
    }
}
