package X;

import android.content.ComponentName;
import android.content.Intent;

/* renamed from: X.0E3  reason: invalid class name */
public abstract class AnonymousClass0E3 {
    public int A00;
    public boolean A01;
    public final ComponentName A02;

    public void A00() {
    }

    public void A01() {
    }

    public void A02() {
    }

    public abstract void A04(Intent intent);

    public void A03(int i) {
        if (!this.A01) {
            this.A01 = true;
            this.A00 = i;
            return;
        }
        int i2 = this.A00;
        if (i2 != i) {
            throw new IllegalArgumentException(AnonymousClass08S.A0B("Given job ID ", i, " is different than previous ", i2));
        }
    }

    public AnonymousClass0E3(ComponentName componentName) {
        this.A02 = componentName;
    }
}
