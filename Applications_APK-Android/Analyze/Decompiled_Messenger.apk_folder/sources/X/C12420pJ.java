package X;

/* renamed from: X.0pJ  reason: invalid class name and case insensitive filesystem */
public final class C12420pJ {
    public final AnonymousClass1YI A00;
    private final AnonymousClass0XN A01;
    private final boolean A02;

    public static final C12420pJ A00(AnonymousClass1XY r3) {
        return new C12420pJ(r3, AnonymousClass0UU.A05(r3), AnonymousClass0UU.A08(r3));
    }

    public static final C12420pJ A01(AnonymousClass1XY r3) {
        return new C12420pJ(r3, AnonymousClass0UU.A05(r3), AnonymousClass0UU.A08(r3));
    }

    public boolean A02() {
        AnonymousClass0XN r0;
        if (!this.A02 || (r0 = this.A01) == null || !r0.A0I() || !this.A00.AbO(AnonymousClass1Y3.A28, false)) {
            return false;
        }
        return true;
    }

    private C12420pJ(AnonymousClass1XY r4, C001500z r5, Boolean bool) {
        boolean z;
        this.A00 = AnonymousClass0WA.A00(r4);
        this.A01 = C25481Zu.A00(r4);
        if (r5 != C001500z.A07 || (bool.booleanValue() && !this.A00.AbO(849, false))) {
            z = false;
        } else {
            z = true;
        }
        this.A02 = z;
    }
}
