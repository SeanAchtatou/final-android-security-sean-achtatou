package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Iv  reason: invalid class name and case insensitive filesystem */
public final class C21821Iv {
    public final Map A00 = new HashMap();

    public boolean A00(String str, C17770zR r4) {
        int i;
        String A0P = AnonymousClass08S.A0P(str, "_", r4.A06);
        if (this.A00.containsKey(A0P)) {
            i = ((Integer) this.A00.get(A0P)).intValue();
        } else {
            i = 0;
        }
        if (i != 1) {
            return false;
        }
        return true;
    }
}
