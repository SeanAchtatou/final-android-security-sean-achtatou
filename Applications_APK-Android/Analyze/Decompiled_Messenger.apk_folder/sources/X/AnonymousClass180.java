package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.180  reason: invalid class name */
public final class AnonymousClass180 implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass180(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r11) {
        C193617v r3;
        int A002 = AnonymousClass09Y.A00(1072046156);
        ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("multiple_thread_keys");
        C05180Xy r5 = new C05180Xy();
        Iterator it = parcelableArrayListExtra.iterator();
        boolean z = false;
        while (it.hasNext()) {
            ThreadKey threadKey = (ThreadKey) it.next();
            ThreadSummary A08 = ((C26681bq) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AuB, this.A00.A02)).A08(threadKey);
            if (A08 != null) {
                if (A08.A0O == C10950l8.A06) {
                    z = true;
                } else {
                    r5.add(threadKey);
                }
            }
        }
        if (z) {
            C33741o4 r0 = this.A00;
            AnonymousClass1FD r32 = AnonymousClass1FD.RECEIPTS_REFRESH;
            C193617v r02 = r0.A03;
            if (r02 != null) {
                r02.BtV(r32, "threadsReadReceiver", false);
            }
        }
        if (!r5.isEmpty() && (r3 = this.A00.A03) != null) {
            r3.BtY(intent.getAction(), AnonymousClass1FD.AUTOMATIC_REFRESH, r5, intent.getStringExtra("calling_class"));
        }
        AnonymousClass09Y.A01(1215638516, A002);
    }
}
