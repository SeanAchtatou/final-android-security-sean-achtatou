package X;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/* renamed from: X.0QH  reason: invalid class name */
public final class AnonymousClass0QH {
    public static void A00(SensorManager sensorManager, SensorEventListener sensorEventListener) {
        C02670Fv.A00.A06(sensorEventListener, null);
        sensorManager.unregisterListener(sensorEventListener);
    }

    public static boolean A01(SensorManager sensorManager, SensorEventListener sensorEventListener, Sensor sensor, int i) {
        boolean registerListener = sensorManager.registerListener(sensorEventListener, sensor, i);
        if (registerListener) {
            C02670Fv.A00.A05(sensorEventListener, sensor);
        }
        return registerListener;
    }
}
