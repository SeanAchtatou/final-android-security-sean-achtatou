package X;

import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1IM  reason: invalid class name */
public final class AnonymousClass1IM extends C17770zR {
    public static final MigColorScheme A03 = C17190yT.A00();
    public static final Integer A04 = AnonymousClass07B.A00;
    @Comparable(type = 3)
    public int A00 = 0;
    @Comparable(type = 13)
    public MigColorScheme A01 = A03;
    @Comparable(type = 13)
    public Integer A02 = A04;

    public static ComponentBuilderCBuilderShape0_0S0100000 A00(AnonymousClass0p4 r3) {
        ComponentBuilderCBuilderShape0_0S0100000 componentBuilderCBuilderShape0_0S0100000 = new ComponentBuilderCBuilderShape0_0S0100000(2);
        ComponentBuilderCBuilderShape0_0S0100000.A02(componentBuilderCBuilderShape0_0S0100000, r3, 0, 0, new AnonymousClass1IM());
        return componentBuilderCBuilderShape0_0S0100000;
    }

    public AnonymousClass1IM() {
        super("MigDivider");
    }
}
