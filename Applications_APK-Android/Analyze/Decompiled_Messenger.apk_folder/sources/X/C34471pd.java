package X;

import android.content.Context;
import com.facebook.proxygen.EventBase;
import com.facebook.proxygen.MQTTClientFactory;
import com.facebook.proxygen.MQTTClientSettings;
import com.facebook.proxygen.ProxygenRadioMeter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1pd  reason: invalid class name and case insensitive filesystem */
public final class C34471pd extends AnonymousClass0AZ {
    public static EventBase A0D;
    public static Thread A0E;
    public static final AtomicInteger A0F = new AtomicInteger();
    public MQTTClientFactory A00;
    private AnonymousClass0US A01;
    private EventBase A02;
    private MQTTClientSettings.Builder A03;
    private ProxygenRadioMeter A04;
    private boolean A05 = false;
    private final Context A06;
    private final EWY A07;
    private final ExecutorService A08;
    private final boolean A09;
    private final boolean A0A;
    private final boolean A0B;
    private final boolean A0C;

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x024d, code lost:
        if (r3.equals(io.card.payment.BuildConfig.FLAVOR) != false) goto L_0x024f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0249 A[SYNTHETIC, Splitter:B:81:0x0249] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0253 A[Catch:{ all -> 0x0269, all -> 0x02b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0259 A[Catch:{ all -> 0x0269, all -> 0x02b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x025f A[Catch:{ all -> 0x0269, all -> 0x02b1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0C7 A00(X.AnonymousClass0C6 r13) {
        /*
            r12 = this;
            boolean r0 = r12.A0A
            r11 = 0
            if (r0 == 0) goto L_0x02d6
            boolean r0 = r12.A05
            java.lang.String r2 = "WhistleCoreBuilder"
            r7 = 1
            if (r0 != 0) goto L_0x0042
            java.lang.String r0 = "fb"
            X.AnonymousClass01q.A08(r0)     // Catch:{ all -> 0x001e }
            java.lang.String r0 = "liger"
            X.AnonymousClass01q.A08(r0)     // Catch:{ all -> 0x001e }
            java.lang.String r0 = "whistle"
            X.AnonymousClass01q.A08(r0)     // Catch:{ all -> 0x001e }
            r12.A05 = r7
            goto L_0x0042
        L_0x001e:
            r4 = move-exception
            java.lang.String r1 = "JNI load failed"
            X.C010708t.A0N(r2, r1, r4)
            X.0B6 r3 = r12.A01
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r0 = "reason"
            r2.put(r0, r1)
            java.lang.String r1 = r4.toString()
            java.lang.String r0 = "throwable"
            r2.put(r0, r1)
            java.lang.String r0 = "whistle_failure"
            r3.A07(r0, r2)
            java.lang.String r0 = "LF"
            goto L_0x02d3
        L_0x0042:
            com.facebook.proxygen.MQTTClientFactory r0 = r12.A00     // Catch:{ all -> 0x02b1 }
            if (r0 != 0) goto L_0x028e
            boolean r0 = r12.A0B     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x0060
            X.0US r0 = r12.A01     // Catch:{ all -> 0x02b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x0060
            X.0US r0 = r12.A01     // Catch:{ all -> 0x02b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x02b1 }
            X.0ka r0 = (X.C10630ka) r0     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.HTTPClient r0 = r0.A04     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.EventBase r0 = r0.mEventBase     // Catch:{ all -> 0x02b1 }
            r12.A02 = r0     // Catch:{ all -> 0x02b1 }
        L_0x0060:
            com.facebook.proxygen.EventBase r0 = r12.A02     // Catch:{ all -> 0x02b1 }
            if (r0 != 0) goto L_0x00b3
            java.lang.Class<X.1pd> r5 = X.C34471pd.class
            monitor-enter(r5)     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.EventBase r0 = X.C34471pd.A0D     // Catch:{ all -> 0x026c }
            if (r0 != 0) goto L_0x00a3
            com.facebook.proxygen.HTTPThread r4 = new com.facebook.proxygen.HTTPThread     // Catch:{ all -> 0x026c }
            r4.<init>()     // Catch:{ all -> 0x026c }
            java.lang.Thread r3 = new java.lang.Thread     // Catch:{ all -> 0x026c }
            java.lang.String r1 = "Whistle-EVB-"
            java.util.concurrent.atomic.AtomicInteger r0 = X.C34471pd.A0F     // Catch:{ all -> 0x026c }
            int r0 = r0.incrementAndGet()     // Catch:{ all -> 0x026c }
            java.lang.String r0 = X.AnonymousClass08S.A09(r1, r0)     // Catch:{ all -> 0x026c }
            r3.<init>(r4, r0)     // Catch:{ all -> 0x026c }
            int r0 = r13.A0A     // Catch:{ all -> 0x026c }
            r3.setPriority(r0)     // Catch:{ all -> 0x026c }
            r3.start()     // Catch:{ all -> 0x026c }
            r4.waitForInitialization()     // Catch:{ all -> 0x026c }
            com.facebook.proxygen.EventBase r0 = r4.getEventBase()     // Catch:{ all -> 0x026c }
            android.util.Pair r1 = android.util.Pair.create(r0, r3)     // Catch:{ all -> 0x026c }
            java.lang.Object r0 = r1.first     // Catch:{ all -> 0x026c }
            com.facebook.proxygen.EventBase r0 = (com.facebook.proxygen.EventBase) r0     // Catch:{ all -> 0x026c }
            X.C34471pd.A0D = r0     // Catch:{ all -> 0x026c }
            java.lang.Object r0 = r1.second     // Catch:{ all -> 0x026c }
            java.lang.Thread r0 = (java.lang.Thread) r0     // Catch:{ all -> 0x026c }
            X.C34471pd.A0E = r0     // Catch:{ all -> 0x026c }
        L_0x00a0:
            com.facebook.proxygen.EventBase r0 = X.C34471pd.A0D     // Catch:{ all -> 0x026c }
            goto L_0x00b0
        L_0x00a3:
            java.lang.Thread r0 = X.C34471pd.A0E     // Catch:{ all -> 0x026c }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x026c }
            java.lang.Thread r1 = X.C34471pd.A0E     // Catch:{ all -> 0x026c }
            int r0 = r13.A0A     // Catch:{ all -> 0x026c }
            r1.setPriority(r0)     // Catch:{ all -> 0x026c }
            goto L_0x00a0
        L_0x00b0:
            monitor-exit(r5)     // Catch:{ all -> 0x02b1 }
            r12.A02 = r0     // Catch:{ all -> 0x02b1 }
        L_0x00b3:
            com.facebook.proxygen.MQTTClientSettings$Builder r8 = new com.facebook.proxygen.MQTTClientSettings$Builder     // Catch:{ all -> 0x02b1 }
            r8.<init>()     // Catch:{ all -> 0x02b1 }
            r12.A03 = r8     // Catch:{ all -> 0x02b1 }
            boolean r0 = r12.A0C     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x00ef
            X.EWY r0 = r12.A07     // Catch:{ all -> 0x02b1 }
            r6 = 0
            java.lang.String r9 = ""
            if (r0 == 0) goto L_0x01fe
            X.EWa r3 = r0.A01()     // Catch:{ all -> 0x02b1 }
            X.EWh r0 = r3.A01     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x01f8
            java.lang.String r1 = r0.A01     // Catch:{ all -> 0x02b1 }
            int r0 = r0.A00     // Catch:{ all -> 0x02b1 }
            r8.secureProxyAddress = r1     // Catch:{ all -> 0x02b1 }
            r8.secureProxyPort = r0     // Catch:{ all -> 0x02b1 }
        L_0x00d5:
            X.EWh r0 = r3.A00     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x01f2
            java.lang.String r1 = r0.A01     // Catch:{ all -> 0x02b1 }
            int r0 = r0.A00     // Catch:{ all -> 0x02b1 }
            r8.proxyAddress = r1     // Catch:{ all -> 0x02b1 }
            r8.proxyPort = r0     // Catch:{ all -> 0x02b1 }
        L_0x00e1:
            com.google.common.collect.ImmutableList r0 = r3.A02     // Catch:{ all -> 0x02b1 }
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = ","
            java.lang.String r0 = X.C06850cB.A06(r0, r1)     // Catch:{ all -> 0x02b1 }
            r8.bypassProxyDomains = r0     // Catch:{ all -> 0x02b1 }
        L_0x00ef:
            com.facebook.proxygen.MQTTClientSettings$Builder r1 = r12.A03     // Catch:{ all -> 0x02b1 }
            r1.zlibCompression = r7     // Catch:{ all -> 0x02b1 }
            r1.verifyCertificates = r7     // Catch:{ all -> 0x02b1 }
            int r0 = r13.A03     // Catch:{ all -> 0x02b1 }
            int r0 = r0 * 1000
            r1.connectTimeout = r0     // Catch:{ all -> 0x02b1 }
            r0 = 0
            r1.pingRespTimeout = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.MQTTClientSettings r7 = r1.build()     // Catch:{ all -> 0x02b1 }
            boolean r6 = r13.A0P     // Catch:{ all -> 0x02b1 }
            java.lang.String r4 = r13.A0H     // Catch:{ all -> 0x02b1 }
            boolean r3 = r13.A0L     // Catch:{ all -> 0x02b1 }
            boolean r1 = r13.A0M     // Catch:{ all -> 0x02b1 }
            int r0 = r13.A04     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.SPDYTransportSettings r5 = new com.facebook.proxygen.SPDYTransportSettings     // Catch:{ all -> 0x02b1 }
            r5.<init>()     // Catch:{ all -> 0x02b1 }
            r5.enableSPDYTransport = r6     // Catch:{ all -> 0x02b1 }
            r5.mergeHostCname = r4     // Catch:{ all -> 0x02b1 }
            r5.enableConnectionMerge = r3     // Catch:{ all -> 0x02b1 }
            r5.enableCustomTransactionTimeout = r1     // Catch:{ all -> 0x02b1 }
            r5.customTransactionTimeoutInSeconds = r0     // Catch:{ all -> 0x02b1 }
            X.1sX r3 = new X.1sX     // Catch:{ all -> 0x02b1 }
            r3.<init>()     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.MQTTClientFactory r4 = new com.facebook.proxygen.MQTTClientFactory     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.EventBase r1 = r12.A02     // Catch:{ all -> 0x02b1 }
            java.util.concurrent.ExecutorService r0 = r12.A08     // Catch:{ all -> 0x02b1 }
            r4.<init>(r1, r0, r7, r3)     // Catch:{ all -> 0x02b1 }
            android.content.Context r0 = r12.A06     // Catch:{ all -> 0x02b1 }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x02b1 }
            java.io.File r1 = r0.getCacheDir()     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = "WhistleTls.store"
            r3.<init>(r1, r0)     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.PersistentSSLCacheSettings$Builder r1 = new com.facebook.proxygen.PersistentSSLCacheSettings$Builder     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x02b1 }
            r1.<init>(r0)     // Catch:{ all -> 0x02b1 }
            r0 = 10
            r1.cacheCapacity = r0     // Catch:{ all -> 0x02b1 }
            r0 = 150(0x96, float:2.1E-43)
            r1.syncInterval = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.PersistentSSLCacheSettings r0 = r1.build()     // Catch:{ all -> 0x02b1 }
            r4.mPersistentSSLCacheSettings = r0     // Catch:{ all -> 0x02b1 }
            android.content.Context r0 = r12.A06     // Catch:{ all -> 0x02b1 }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x02b1 }
            java.io.File r1 = r0.getCacheDir()     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = "WhistleDns.store"
            r3.<init>(r1, r0)     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.PersistentSSLCacheSettings$Builder r1 = new com.facebook.proxygen.PersistentSSLCacheSettings$Builder     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x02b1 }
            r1.<init>(r0)     // Catch:{ all -> 0x02b1 }
            r0 = 20
            r1.cacheCapacity = r0     // Catch:{ all -> 0x02b1 }
            r0 = 150(0x96, float:2.1E-43)
            r1.syncInterval = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.PersistentSSLCacheSettings r0 = r1.build()     // Catch:{ all -> 0x02b1 }
            r4.mPersistentDNSCacheSettings = r0     // Catch:{ all -> 0x02b1 }
            boolean r1 = r13.A0N     // Catch:{ all -> 0x02b1 }
            android.content.Context r0 = r12.A06     // Catch:{ all -> 0x02b1 }
            if (r1 == 0) goto L_0x01e5
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x02b1 }
            java.io.File r1 = r0.getCacheDir()     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = "WhistleFizz.store"
            r3.<init>(r1, r0)     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.PersistentSSLCacheSettings$Builder r1 = new com.facebook.proxygen.PersistentSSLCacheSettings$Builder     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x02b1 }
            r1.<init>(r0)     // Catch:{ all -> 0x02b1 }
            r0 = 30
            r1.cacheCapacity = r0     // Catch:{ all -> 0x02b1 }
            r0 = 150(0x96, float:2.1E-43)
            r1.syncInterval = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.PersistentSSLCacheSettings r3 = r1.build()     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.FizzSettings$Builder r1 = new com.facebook.proxygen.FizzSettings$Builder     // Catch:{ all -> 0x02b1 }
            r1.<init>()     // Catch:{ all -> 0x02b1 }
            r0 = 1
            r1.enabled = r0     // Catch:{ all -> 0x02b1 }
            r1.persistentCacheEnabled = r0     // Catch:{ all -> 0x02b1 }
            r1.cacheSettings = r3     // Catch:{ all -> 0x02b1 }
            r1.sendEarlyData = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.FizzSettings r0 = r1.build()     // Catch:{ all -> 0x02b1 }
        L_0x01a9:
            r4.mFizzSettings = r0     // Catch:{ all -> 0x02b1 }
            r4.mSPDYTransportSettings = r5     // Catch:{ all -> 0x02b1 }
            boolean r0 = r13.A0O     // Catch:{ all -> 0x02b1 }
            r4.mEnableLargePayload = r0     // Catch:{ all -> 0x02b1 }
            r12.A00 = r4     // Catch:{ all -> 0x02b1 }
            X.0US r0 = r12.A01     // Catch:{ all -> 0x02b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x01d7
            com.facebook.proxygen.MQTTClientFactory r1 = r12.A00     // Catch:{ all -> 0x02b1 }
            X.0US r0 = r12.A01     // Catch:{ all -> 0x02b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x02b1 }
            X.0ka r0 = (X.C10630ka) r0     // Catch:{ all -> 0x02b1 }
            java.util.concurrent.Executor r0 = r0.A06     // Catch:{ all -> 0x02b1 }
            r1.mPersistentCachesExecutor = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.MQTTClientFactory r1 = r12.A00     // Catch:{ all -> 0x02b1 }
            X.0US r0 = r12.A01     // Catch:{ all -> 0x02b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x02b1 }
            X.0ka r0 = (X.C10630ka) r0     // Catch:{ all -> 0x02b1 }
            java.util.concurrent.Executor r0 = r0.A05     // Catch:{ all -> 0x02b1 }
            r1.mDnsResolverExecutor = r0     // Catch:{ all -> 0x02b1 }
        L_0x01d7:
            com.facebook.proxygen.MQTTClientFactory r0 = r12.A00     // Catch:{ all -> 0x02b1 }
            r0.init()     // Catch:{ all -> 0x02b1 }
            X.0At r3 = r12.A02     // Catch:{ all -> 0x02b1 }
            X.1sY r1 = new X.1sY     // Catch:{ all -> 0x02b1 }
            r1.<init>(r12)     // Catch:{ all -> 0x02b1 }
            monitor-enter(r3)     // Catch:{ all -> 0x02b1 }
            goto L_0x0263
        L_0x01e5:
            com.facebook.proxygen.FizzSettings$Builder r1 = new com.facebook.proxygen.FizzSettings$Builder     // Catch:{ all -> 0x02b1 }
            r1.<init>()     // Catch:{ all -> 0x02b1 }
            r0 = 0
            r1.enabled = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.FizzSettings r0 = r1.build()     // Catch:{ all -> 0x02b1 }
            goto L_0x01a9
        L_0x01f2:
            r8.proxyAddress = r9     // Catch:{ all -> 0x02b1 }
            r8.proxyPort = r6     // Catch:{ all -> 0x02b1 }
            goto L_0x00e1
        L_0x01f8:
            r8.secureProxyAddress = r9     // Catch:{ all -> 0x02b1 }
            r8.secureProxyPort = r6     // Catch:{ all -> 0x02b1 }
            goto L_0x00d5
        L_0x01fe:
            java.lang.String r0 = "http.nonProxyHosts"
            java.lang.String r5 = java.lang.System.getProperty(r0)     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = "http.proxyHost"
            java.lang.String r4 = java.lang.System.getProperty(r0)     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = "http.proxyPort"
            java.lang.String r0 = java.lang.System.getProperty(r0)     // Catch:{ NumberFormatException -> 0x0215 }
            int r10 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0215 }
            goto L_0x0217
        L_0x0215:
            r4 = r9
            r10 = 0
        L_0x0217:
            java.lang.String r0 = "https.proxyHost"
            java.lang.String r3 = java.lang.System.getProperty(r0)     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = "https.proxyPort"
            java.lang.String r0 = java.lang.System.getProperty(r0)     // Catch:{ NumberFormatException -> 0x0228 }
            int r1 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0228 }
            goto L_0x022a
        L_0x0228:
            r3 = r9
            r1 = 0
        L_0x022a:
            if (r4 == 0) goto L_0x0233
            boolean r0 = r4.equals(r9)     // Catch:{ all -> 0x02b1 }
            if (r0 != 0) goto L_0x0233
            goto L_0x0246
        L_0x0233:
            java.lang.String r0 = "proxyHost"
            java.lang.String r4 = java.lang.System.getProperty(r0)     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = "proxyPort"
            java.lang.String r0 = java.lang.System.getProperty(r0)     // Catch:{ NumberFormatException -> 0x0244 }
            int r6 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0244 }
            goto L_0x0247
        L_0x0244:
            r4 = r9
            goto L_0x0247
        L_0x0246:
            r6 = r10
        L_0x0247:
            if (r3 == 0) goto L_0x024f
            boolean r0 = r3.equals(r9)     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x0251
        L_0x024f:
            r1 = r6
            r3 = r4
        L_0x0251:
            if (r4 == 0) goto L_0x0257
            r8.proxyAddress = r4     // Catch:{ all -> 0x02b1 }
            r8.proxyPort = r6     // Catch:{ all -> 0x02b1 }
        L_0x0257:
            if (r3 == 0) goto L_0x025d
            r8.secureProxyAddress = r3     // Catch:{ all -> 0x02b1 }
            r8.secureProxyPort = r1     // Catch:{ all -> 0x02b1 }
        L_0x025d:
            if (r5 == 0) goto L_0x00ef
            r8.bypassProxyDomains = r5     // Catch:{ all -> 0x02b1 }
            goto L_0x00ef
        L_0x0263:
            java.util.Set r0 = r3.A07     // Catch:{ all -> 0x0269 }
            r0.add(r1)     // Catch:{ all -> 0x0269 }
            goto L_0x026f
        L_0x0269:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x02b1 }
            goto L_0x026e
        L_0x026c:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x02b1 }
        L_0x026e:
            throw r0     // Catch:{ all -> 0x02b1 }
        L_0x026f:
            monitor-exit(r3)     // Catch:{ all -> 0x02b1 }
            X.0US r0 = r12.A01     // Catch:{ all -> 0x02b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x028e
            X.0US r0 = r12.A01     // Catch:{ all -> 0x02b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x02b1 }
            X.0ka r0 = (X.C10630ka) r0     // Catch:{ all -> 0x02b1 }
            int r3 = X.AnonymousClass1Y3.ADv     // Catch:{ all -> 0x02b1 }
            X.0UN r1 = r0.A01     // Catch:{ all -> 0x02b1 }
            r0 = 11
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r1)     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.ProxygenRadioMeter r0 = (com.facebook.proxygen.ProxygenRadioMeter) r0     // Catch:{ all -> 0x02b1 }
            r12.A04 = r0     // Catch:{ all -> 0x02b1 }
        L_0x028e:
            java.lang.String r0 = "W"
            r12.A03 = r0     // Catch:{ all -> 0x02b1 }
            X.1sZ r3 = new X.1sZ     // Catch:{ all -> 0x02b1 }
            int r4 = r13.A08     // Catch:{ all -> 0x02b1 }
            java.util.concurrent.atomic.AtomicInteger r0 = r13.A0K     // Catch:{ all -> 0x02b1 }
            int r5 = r0.get()     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.MQTTClientFactory r6 = r12.A00     // Catch:{ all -> 0x02b1 }
            boolean r7 = r12.A09     // Catch:{ all -> 0x02b1 }
            com.facebook.proxygen.ProxygenRadioMeter r8 = r12.A04     // Catch:{ all -> 0x02b1 }
            X.1sa r9 = new X.1sa     // Catch:{ all -> 0x02b1 }
            X.0B6 r1 = r12.A01     // Catch:{ all -> 0x02b1 }
            X.0At r0 = r12.A02     // Catch:{ all -> 0x02b1 }
            r9.<init>(r1, r13, r0)     // Catch:{ all -> 0x02b1 }
            java.util.concurrent.ExecutorService r10 = r12.A08     // Catch:{ all -> 0x02b1 }
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x02b1 }
            return r3
        L_0x02b1:
            r4 = move-exception
            java.lang.String r1 = "Failed to create whistle factory"
            X.C010708t.A0N(r2, r1, r4)
            X.0B6 r3 = r12.A01
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r0 = "reason"
            r2.put(r0, r1)
            java.lang.String r1 = r4.toString()
            java.lang.String r0 = "throwable"
            r2.put(r0, r1)
            java.lang.String r0 = "whistle_failure"
            r3.A07(r0, r2)
            java.lang.String r0 = "FC"
        L_0x02d3:
            r12.A03 = r0
            return r11
        L_0x02d6:
            java.lang.String r0 = "D"
            r12.A03 = r0
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34471pd.A00(X.0C6):X.0C7");
    }

    public C34471pd(Context context, ExecutorService executorService, boolean z, boolean z2, boolean z3, AnonymousClass0US r7, boolean z4, EWY ewy) {
        this.A06 = context;
        this.A0A = z;
        this.A08 = executorService;
        this.A09 = z2;
        this.A0B = z3;
        this.A01 = r7;
        this.A0C = z4;
        this.A07 = ewy;
    }
}
