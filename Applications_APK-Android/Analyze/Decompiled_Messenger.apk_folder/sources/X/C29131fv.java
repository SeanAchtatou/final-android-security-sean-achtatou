package X;

/* renamed from: X.1fv  reason: invalid class name and case insensitive filesystem */
public final class C29131fv {
    public final String explicitName;
    public final boolean isMarkedIgnored;
    public final boolean isVisible;
    public final C29131fv next;
    public final Object value;

    public static C29131fv append(C29131fv r1, C29131fv r2) {
        C29131fv r0 = r1.next;
        if (r0 != null) {
            r2 = append(r0, r2);
        }
        return withNext(r1, r2);
    }

    public static C29131fv withNext(C29131fv r5, C29131fv r6) {
        C29131fv r2 = r6;
        if (r6 == r5.next) {
            return r5;
        }
        return new C29131fv(r5.value, r2, r5.explicitName, r5.isVisible, r5.isMarkedIgnored);
    }

    public String toString() {
        String str = this.value.toString() + "[visible=" + this.isVisible + "]";
        C29131fv r0 = this.next;
        if (r0 != null) {
            return AnonymousClass08S.A0P(str, ", ", r0.toString());
        }
        return str;
    }

    public C29131fv trimByVisibility() {
        C29131fv r0 = this.next;
        if (r0 == null) {
            return this;
        }
        C29131fv trimByVisibility = r0.trimByVisibility();
        if (this.explicitName != null) {
            if (trimByVisibility.explicitName == null) {
                return withNext(this, null);
            }
        } else if (trimByVisibility.explicitName != null) {
            return trimByVisibility;
        } else {
            boolean z = this.isVisible;
            if (z != trimByVisibility.isVisible) {
                if (z) {
                    return withNext(this, null);
                }
                return trimByVisibility;
            }
        }
        return withNext(this, trimByVisibility);
    }

    public C29131fv withValue(Object obj) {
        Object obj2 = obj;
        if (obj == this.value) {
            return this;
        }
        return new C29131fv(obj2, this.next, this.explicitName, this.isVisible, this.isMarkedIgnored);
    }

    public C29131fv withoutIgnored() {
        C29131fv withoutIgnored;
        if (this.isMarkedIgnored) {
            C29131fv r0 = this.next;
            if (r0 == null) {
                return null;
            }
            return r0.withoutIgnored();
        }
        C29131fv r02 = this.next;
        if (r02 == null || (withoutIgnored = r02.withoutIgnored()) == this.next) {
            return this;
        }
        return withNext(this, withoutIgnored);
    }

    public C29131fv withoutNonVisible() {
        C29131fv withoutNonVisible;
        C29131fv r0 = this.next;
        if (r0 == null) {
            withoutNonVisible = null;
        } else {
            withoutNonVisible = r0.withoutNonVisible();
        }
        if (this.isVisible) {
            return withNext(this, withoutNonVisible);
        }
        return withoutNonVisible;
    }

    public C29131fv(Object obj, C29131fv r4, String str, boolean z, boolean z2) {
        this.value = obj;
        this.next = r4;
        String str2 = null;
        if (!(str == null || str.length() == 0)) {
            str2 = str;
        }
        this.explicitName = str2;
        this.isVisible = z;
        this.isMarkedIgnored = z2;
    }
}
