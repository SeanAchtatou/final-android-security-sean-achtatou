package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.1pE  reason: invalid class name and case insensitive filesystem */
public final class C34261pE extends AnonymousClass0AH {
    public static final Map A06 = Collections.unmodifiableMap(new C34291pH());
    public static final Map A07 = Collections.unmodifiableMap(new C34281pG());
    public C33821oC A00;
    public C21041Eu A01;
    public C34411pX A02;
    public C34561po A03;
    public C01690Bg A04;
    public volatile C34321pO A05;

    public C01540Aq A0U(String str, byte[] bArr, Integer num, AnonymousClass0CZ r15, int i, long j, String str2, AnonymousClass0PF r20) {
        return A00(str, bArr, num, r15, i, j, str2, 0, r20);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002d, code lost:
        if ("/t_sm_b".equals(r1) != false) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List A0W(X.AnonymousClass0C8 r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0052 }
            r5.<init>()     // Catch:{ all -> 0x0052 }
            X.1po r0 = r8.A03     // Catch:{ all -> 0x0052 }
            java.util.Collection r0 = r0.A01()     // Catch:{ all -> 0x0052 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x0052 }
        L_0x0010:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x0050
            java.lang.Object r6 = r7.next()     // Catch:{ all -> 0x0052 }
            X.Bfk r6 = (X.C23467Bfk) r6     // Catch:{ all -> 0x0052 }
            java.lang.String r1 = r6.A08     // Catch:{ all -> 0x0052 }
            java.lang.String r0 = "/t_sm"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0052 }
            if (r0 != 0) goto L_0x002f
            java.lang.String r0 = "/t_sm_b"
            boolean r1 = r0.equals(r1)     // Catch:{ all -> 0x0052 }
            r0 = 0
            if (r1 == 0) goto L_0x0030
        L_0x002f:
            r0 = 1
        L_0x0030:
            if (r0 == 0) goto L_0x0010
            java.util.concurrent.atomic.AtomicInteger r0 = r6.A09     // Catch:{ all -> 0x0052 }
            r0.incrementAndGet()     // Catch:{ all -> 0x0052 }
            X.0Rs r4 = new X.0Rs     // Catch:{ all -> 0x0052 }
            java.lang.String r3 = r6.A08     // Catch:{ all -> 0x0052 }
            java.util.concurrent.atomic.AtomicInteger r0 = X.AnonymousClass0C8.A0e     // Catch:{ all -> 0x0052 }
            int r2 = r0.getAndIncrement()     // Catch:{ all -> 0x0052 }
            r0 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r0
            byte[] r1 = r6.A0A     // Catch:{ all -> 0x0052 }
            int r0 = r6.A01     // Catch:{ all -> 0x0052 }
            r4.<init>(r3, r2, r1, r0)     // Catch:{ all -> 0x0052 }
            r5.add(r4)     // Catch:{ all -> 0x0052 }
            goto L_0x0010
        L_0x0050:
            monitor-exit(r8)
            return r5
        L_0x0052:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34261pE.A0W(X.0C8):java.util.List");
    }

    public boolean A0d(List list) {
        return A0f(list, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ExcHandler: 0CI | UnsupportedOperationException (unused java.lang.Throwable), SYNTHETIC, Splitter:B:5:0x000d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0e(java.util.List r19) {
        /*
            r18 = this;
            r7 = 0
            r4 = r19
            if (r19 == 0) goto L_0x0090
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x0090
            r2 = r18
            java.util.List r0 = X.C02430Et.A00(r4)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r6.<init>()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r5.<init>()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
        L_0x001f:
            boolean r0 = r3.hasNext()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            if (r0 == 0) goto L_0x0039
            java.lang.Object r1 = r3.next()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            java.lang.Integer r0 = X.AnonymousClass0AI.A00(r1)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            if (r0 == 0) goto L_0x0035
            r6.add(r0)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            goto L_0x001f
        L_0x0035:
            r5.add(r1)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            goto L_0x001f
        L_0x0039:
            X.AwR r3 = new X.AwR     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r3.<init>(r6, r5)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            X.1so r1 = new X.1so     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            X.1sp r0 = new X.1sp     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r0.<init>()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r1.<init>(r0)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            byte[] r10 = r1.A00(r3)     // Catch:{ 3bP -> 0x0076, 0CI | UnsupportedOperationException -> 0x007d }
            java.lang.String r9 = "/unsubscribe"
            java.lang.Integer r11 = X.AnonymousClass07B.A01     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r12 = 0
            int r13 = r2.A07()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r14 = 0
            r16 = 0
            r17 = 0
            r8 = r2
            X.0Aq r0 = r8.A0U(r9, r10, r11, r12, r13, r14, r16, r17)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            boolean r7 = r0.A02()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            if (r7 == 0) goto L_0x007d
            X.0AT r1 = r2.A0p     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            java.lang.Object r0 = r0.A01()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            X.0CW r0 = (X.AnonymousClass0CW) r0     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            int r0 = r0.AwM()     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r1.BsL(r4, r0)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            goto L_0x007d
        L_0x0076:
            r1 = move-exception
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            r0.<init>(r1)     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
            throw r0     // Catch:{ 0CI | UnsupportedOperationException -> 0x007d }
        L_0x007d:
            if (r7 != 0) goto L_0x0083
            boolean r7 = super.A0e(r4)
        L_0x0083:
            if (r7 == 0) goto L_0x0090
            X.1pO r3 = r2.A05
            X.0Bd r2 = X.C01670Bd.A0A
            java.lang.String r1 = "graphql_subscriptions_reach_mqtt_client_checkpoint"
            java.lang.String r0 = "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log"
            r3.A01(r1, r0, r2, r4)
        L_0x0090:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34261pE.A0e(java.util.List):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0086, code lost:
        if ("/t_sm_b".equals(r15) != false) goto L_0x0088;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.C01540Aq A00(java.lang.String r25, byte[] r26, java.lang.Integer r27, X.AnonymousClass0CZ r28, int r29, long r30, java.lang.String r32, int r33, X.AnonymousClass0PF r34) {
        /*
            r24 = this;
            java.lang.String r0 = "/send_message2"
            r15 = r25
            boolean r0 = r0.equals(r15)
            r2 = r24
            if (r0 != 0) goto L_0x0014
            java.lang.String r0 = "/t_sm"
            boolean r0 = r0.equals(r15)
            if (r0 == 0) goto L_0x0029
        L_0x0014:
            X.0B7 r1 = r2.A0A
            java.lang.Class<X.08I> r0 = X.AnonymousClass08I.class
            X.0By r1 = r1.A07(r0)
            X.08I r1 = (X.AnonymousClass08I) r1
            X.08J r0 = X.AnonymousClass08J.A07
            java.lang.Object r0 = r1.A00(r0)
            java.util.concurrent.atomic.AtomicLong r0 = (java.util.concurrent.atomic.AtomicLong) r0
            r0.incrementAndGet()
        L_0x0029:
            X.0C8 r4 = r2.A0n
            r20 = r30
            r11 = r20
            r1 = r33
            r6 = r15
            java.util.Map r0 = X.C34261pE.A06
            java.lang.Object r0 = r0.get(r15)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r33 <= 0) goto L_0x0040
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
        L_0x0040:
            r10 = r29
            r9 = r28
            r8 = r27
            r22 = r32
            r7 = r26
            if (r0 == 0) goto L_0x00b6
            int r3 = r0.intValue()
            if (r3 <= 0) goto L_0x00b6
            X.1po r5 = r2.A03
            java.util.Map r0 = X.C34261pE.A07
            java.lang.Object r14 = r0.get(r15)
            java.lang.Long r14 = (java.lang.Long) r14
            r13 = r22
            X.Bfk r1 = r5.A00(r6, r7, r8, r9, r10, r11, r13, r14)
            if (r4 == 0) goto L_0x006d
            boolean r0 = r4.A09()
            if (r0 == 0) goto L_0x006d
            r2.A06(r4, r1, r3)
        L_0x006d:
            if (r1 != 0) goto L_0x00b1
            if (r4 == 0) goto L_0x0077
            boolean r0 = r4.A0A()
            if (r0 != 0) goto L_0x00b8
        L_0x0077:
            java.lang.String r0 = "/t_sm"
            boolean r0 = r0.equals(r15)
            if (r0 != 0) goto L_0x0088
            java.lang.String r0 = "/t_sm_b"
            boolean r1 = r0.equals(r15)
            r0 = 0
            if (r1 == 0) goto L_0x0089
        L_0x0088:
            r0 = 1
        L_0x0089:
            if (r0 == 0) goto L_0x00b8
            X.0Bg r0 = r2.A04
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00b8
            X.1po r14 = r2.A03
            java.util.Map r0 = X.C34261pE.A07
            java.lang.Object r0 = r0.get(r15)
            java.lang.Long r0 = (java.lang.Long) r0
            r16 = r7
            r17 = r8
            r18 = r9
            r19 = r10
            r23 = r0
            X.Bfk r1 = r14.A00(r15, r16, r17, r18, r19, r20, r22, r23)
        L_0x00b1:
            X.0Aq r0 = X.C01540Aq.A00(r1)
            return r0
        L_0x00b6:
            r1 = 0
            goto L_0x006d
        L_0x00b8:
            r14 = r2
            r23 = r34
            r16 = r7
            r17 = r8
            r18 = r9
            r19 = r10
            X.0Aq r0 = super.A0U(r15, r16, r17, r18, r19, r20, r22, r23)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34261pE.A00(java.lang.String, byte[], java.lang.Integer, X.0CZ, int, long, java.lang.String, int, X.0PF):X.0Aq");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ba, code lost:
        if (r2 == false) goto L_0x00bc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A06(X.AnonymousClass0C8 r23, X.C23467Bfk r24, int r25) {
        /*
            r22 = this;
            r0 = r22
            java.lang.String r4 = "MqttConnectionManager"
            X.0Cs r11 = new X.0Cs
            r1 = r24
            java.lang.String r13 = r1.A08
            X.0CL r14 = X.AnonymousClass0CL.A07
            java.util.concurrent.atomic.AtomicInteger r2 = X.AnonymousClass0C8.A0e
            int r15 = r2.getAndIncrement()
            r2 = 65535(0xffff, float:9.1834E-41)
            r15 = r15 & r2
            long r16 = android.os.SystemClock.elapsedRealtime()
            int r18 = r1.AwM()
            java.util.concurrent.atomic.AtomicInteger r2 = r1.A09
            int r19 = r2.get()
            r12 = r23
            r11.<init>(r12, r13, r14, r15, r16, r18, r19)
            java.lang.Integer r3 = r1.A06
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            r10 = 1
            if (r3 != r2) goto L_0x0073
            X.0BN r9 = r0.A0L
            int r5 = r1.A02
            X.2Mv r8 = new X.2Mv
            r8.<init>(r0, r11, r1)
            java.util.Map r6 = r9.A04
            monitor-enter(r6)
            java.util.Map r3 = r9.A04     // Catch:{ all -> 0x0070 }
            int r2 = r11.A01     // Catch:{ all -> 0x0070 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0070 }
            boolean r2 = r3.containsKey(r2)     // Catch:{ all -> 0x0070 }
            if (r2 == 0) goto L_0x004c
            monitor-exit(r6)     // Catch:{ all -> 0x0070 }
            goto L_0x00b9
        L_0x004c:
            java.util.Map r3 = r9.A04     // Catch:{ all -> 0x0070 }
            int r2 = r11.A01     // Catch:{ all -> 0x0070 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0070 }
            r3.put(r2, r11)     // Catch:{ all -> 0x0070 }
            monitor-exit(r6)     // Catch:{ all -> 0x0070 }
            X.0Ap r7 = r9.A03
            X.0SG r6 = new X.0SG
            r6.<init>(r9, r11)
            long r2 = (long) r5
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS
            X.0Cf r3 = r7.schedule(r6, r2, r5)
            r11.A02(r3)
            X.0Ao r2 = r9.A02
            r3.addListener(r8, r2)
            r2 = 1
            goto L_0x00ba
        L_0x0070:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0070 }
            throw r0
        L_0x0073:
            r3 = r25
            if (r25 <= 0) goto L_0x0089
            X.1po r2 = r0.A03
            X.EY2 r7 = new X.EY2
            r7.<init>(r0, r11)
            X.0An r6 = r2.A01
            long r2 = (long) r3
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.MILLISECONDS
            X.0Cf r2 = r6.C4X(r7, r2, r5)
            r11.A02 = r2
        L_0x0089:
            X.0Al r2 = r0.A07
            X.0CU r21 = r2.A00()
            java.lang.String r13 = r1.A08     // Catch:{ 0CI -> 0x00bd }
            byte[] r14 = r1.A0A     // Catch:{ 0CI -> 0x00bd }
            java.lang.Integer r15 = r1.A06     // Catch:{ 0CI -> 0x00bd }
            int r7 = r11.A01     // Catch:{ 0CI -> 0x00bd }
            X.0CZ r6 = r1.A05     // Catch:{ 0CI -> 0x00bd }
            long r2 = r1.A04     // Catch:{ 0CI -> 0x00bd }
            java.lang.String r5 = r1.A07     // Catch:{ 0CI -> 0x00bd }
            r17 = r6
            r18 = r2
            r20 = r5
            r16 = r7
            r12.A07(r13, r14, r15, r16, r17, r18, r20, r21)     // Catch:{ 0CI -> 0x00bd }
            java.lang.Integer r3 = r1.A06
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            if (r3 == r2) goto L_0x00bc
            r11.A00()
            X.1po r2 = r0.A03
            int r0 = r1.A01
            r2.A02(r0)
            return r10
        L_0x00b9:
            r2 = 0
        L_0x00ba:
            if (r2 != 0) goto L_0x0073
        L_0x00bc:
            return r10
        L_0x00bd:
            r2 = move-exception
            java.lang.String r1 = "exception/publish"
            X.C010708t.A0S(r4, r2, r1)
            X.0CE r2 = X.AnonymousClass0CE.A0J
            X.0FG r1 = X.AnonymousClass0FG.CONNECTION_LOST
            r0.A0L(r12, r2, r1)
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34261pE.A06(X.0C8, X.Bfk, int):boolean");
    }

    public Long A0V(Boolean bool) {
        long A012;
        C21041Eu r3 = this.A01;
        if (!r3.A04.A00.AbO(AnonymousClass1Y3.A6h, false) || bool == null) {
            return null;
        }
        if (bool.booleanValue()) {
            A012 = r3.A02(false);
        } else {
            A012 = r3.A01(false);
        }
        return Long.valueOf(A012);
    }

    public void A0Z(List list) {
        AnonymousClass0C8 r4 = this.A0n;
        if (r4 != null) {
            HashSet hashSet = new HashSet();
            if (list != null) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    hashSet.add(Integer.valueOf(((C04070Rs) it.next()).A01));
                }
            }
            for (C23467Bfk bfk : this.A03.A01()) {
                if (!hashSet.contains(Integer.valueOf(bfk.AwM()))) {
                    bfk.A09.incrementAndGet();
                    if (!A06(r4, bfk, 0)) {
                        return;
                    }
                }
            }
        }
    }

    public void A0b(List list, List list2) {
        this.A05.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A07, list);
        this.A05.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A0B, list2);
    }

    public boolean A0f(List list, int i) {
        boolean z = false;
        List<SubscribeTopic> list2 = list;
        if (list != null && !list2.isEmpty()) {
            try {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                for (SubscribeTopic subscribeTopic : list2) {
                    Integer A002 = AnonymousClass0AI.A00(subscribeTopic.A01);
                    if (A002 != null) {
                        arrayList.add(A002);
                    } else {
                        arrayList2.add(new C22374Awt(subscribeTopic.A01, Integer.valueOf(subscribeTopic.A00)));
                    }
                }
                byte[] A003 = new C36281so(new C36291sp()).A00(new C22373Aws(arrayList, arrayList2));
                if (A003 != null) {
                    C01540Aq A004 = A00("/subscribe", A003, AnonymousClass07B.A01, null, A07(), 0, null, i, null);
                    z = A004.A02();
                    if (z) {
                        this.A0p.BsK(list2, ((AnonymousClass0CW) A004.A01()).AwM());
                    }
                }
            } catch (C70863bP e) {
                throw new UnsupportedOperationException(e);
            } catch (UnsupportedOperationException unused) {
            } catch (AnonymousClass0CI e2) {
                if (C010708t.A01.BFj(2)) {
                    C010708t.A01.CLU("MqttConnectionManager", "exception/MqttException", e2);
                }
            }
            if (!z) {
                z = super.A0d(list2);
            }
            if (z) {
                this.A05.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A06, list2);
            }
        }
        return z;
    }

    public void A0X(Boolean bool) {
        super.A0X(bool);
        if (bool != null) {
            this.A02.A03(bool.booleanValue(), false);
        }
    }

    public void A0Y(Boolean bool) {
        long A012;
        String str;
        super.A0Y(bool);
        if (bool != null) {
            C34411pX r0 = this.A02;
            boolean booleanValue = bool.booleanValue();
            r0.A03(booleanValue, false);
            C33821oC r3 = this.A00;
            String A002 = C206729on.A00(this.A02.A02());
            C21041Eu r02 = r3.A01;
            if (booleanValue) {
                A012 = r02.A02(false);
            } else {
                A012 = r02.A01(false);
            }
            Map A013 = C33821oC.A01(A012);
            A013.put("is_background_data_restricted", A002);
            if (booleanValue) {
                str = "client_presence_app_foregrounded_publish_failed";
            } else {
                str = "client_presence_app_backgrounded_publish_failed";
            }
            C33821oC.A02(r3, str, A013);
        }
    }

    public void A0a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.A03.A02(((C04070Rs) it.next()).A01);
        }
    }

    public void A0c(List list, List list2, Boolean bool) {
        long A012;
        String str;
        super.A0c(list, list2, bool);
        this.A05.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A06, list);
        this.A05.A01("graphql_subscriptions_reach_mqtt_client_checkpoint", "graphql_subscriptions_reach_mqtt_client_checkpoint_force_log", C01670Bd.A0A, list2);
        if (bool != null) {
            C34411pX r1 = this.A02;
            boolean booleanValue = bool.booleanValue();
            r1.A03(booleanValue, true);
            String A002 = C206729on.A00(this.A02.A02());
            C33821oC r2 = this.A00;
            C21041Eu r0 = r2.A01;
            if (booleanValue) {
                A012 = r0.A02(false);
            } else {
                A012 = r0.A01(false);
            }
            Map A013 = C33821oC.A01(A012);
            A013.put("is_background_data_restricted", A002);
            if (booleanValue) {
                str = "client_presence_app_foregrounded_published";
            } else {
                str = "client_presence_app_backgrounded_published";
            }
            C33821oC.A02(r2, str, A013);
        }
    }
}
