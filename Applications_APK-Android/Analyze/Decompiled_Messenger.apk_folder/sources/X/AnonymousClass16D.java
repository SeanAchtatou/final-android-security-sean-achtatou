package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.user.model.User;

/* renamed from: X.16D  reason: invalid class name */
public final class AnonymousClass16D {
    private AnonymousClass0UN A00;

    public static final AnonymousClass16D A00(AnonymousClass1XY r1) {
        return new AnonymousClass16D(r1);
    }

    public void A01(User user) {
        String[] strArr;
        String str;
        if (!((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aer(282754876966759L, AnonymousClass0XE.A07) && !((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, this.A00)).Aep(C17170yR.A00, false)) {
            C07220cv r0 = new C07220cv();
            r0.A04(user);
            User A02 = r0.A02();
            String str2 = A02.A0k;
            if (str2 != null && !"unknown".equals(str2) && !"unset_or_unrecognized_enum_value".equals(str2)) {
                if (A02.A1c) {
                    if (A02.A1Q) {
                        strArr = new String[1];
                        str = "deactivated_allowed_on_messenger";
                    } else {
                        boolean z = A02.A1X;
                        strArr = new String[1];
                        if (z) {
                            str = "messenger_only_deactivated";
                        } else {
                            str = "messenger_only_confirmed";
                        }
                    }
                    strArr[0] = str;
                } else {
                    strArr = new String[]{"facebook_confirmed", "facebook_pending"};
                }
                C17180yS A002 = C17180yS.A00(strArr);
                if (!A002.contains(str2.toLowerCase())) {
                    C010708t.A0O("MessengerAccountStatusVerificationUtil", "validateLoggedInUserStatus: local status is inconsistent, expectedAccountStatuses=%s, localAccountStatus=%s", A002, str2);
                    C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, this.A00)).edit();
                    edit.putBoolean(C17170yR.A00, true);
                    edit.commit();
                    ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, this.A00)).At2(C17170yR.A01, 0);
                }
            }
        }
    }

    private AnonymousClass16D(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
