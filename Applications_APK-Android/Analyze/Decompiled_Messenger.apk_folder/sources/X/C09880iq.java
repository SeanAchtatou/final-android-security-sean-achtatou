package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import java.io.File;

/* renamed from: X.0iq  reason: invalid class name and case insensitive filesystem */
public abstract class C09880iq {
    public final String A00;

    public File A00() {
        C09870ip r3 = (C09870ip) this;
        FbSharedPreferences fbSharedPreferences = (FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r3.A00);
        if (r3.A01 == null) {
            r3.A01 = r3.A04("location");
        }
        String B4F = fbSharedPreferences.B4F(r3.A01, null);
        if (B4F != null) {
            return new File(B4F);
        }
        return null;
    }

    public String A01() {
        C09870ip r3 = (C09870ip) this;
        FbSharedPreferences fbSharedPreferences = (FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r3.A00);
        if (r3.A02 == null) {
            r3.A02 = r3.A04("md5");
        }
        return fbSharedPreferences.B4F(r3.A02, null);
    }

    public void A02(File file) {
        C09870ip r4 = (C09870ip) this;
        String absolutePath = file.getAbsolutePath();
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r4.A00)).edit();
        if (r4.A01 == null) {
            r4.A01 = r4.A04("location");
        }
        edit.BzC(r4.A01, absolutePath);
        edit.commit();
    }

    public void A03(String str) {
        C09870ip r3 = (C09870ip) this;
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r3.A00)).edit();
        if (r3.A02 == null) {
            r3.A02 = r3.A04("md5");
        }
        edit.BzC(r3.A02, str);
        edit.commit();
    }

    public C09880iq(String str) {
        this.A00 = str;
    }
}
