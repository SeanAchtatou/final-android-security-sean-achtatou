package X;

import com.facebook.common.dextricks.DexStore;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1JA  reason: invalid class name */
public final class AnonymousClass1JA {
    public final List A00;
    public final Map A01;
    public final /* synthetic */ C05720aD A02;

    public AnonymousClass1JA(C05720aD r16) {
        this.A02 = r16;
        ImmutableMap.Builder builder = new ImmutableMap.Builder();
        builder.put("free_messenger_send_video_interstitial", 1);
        builder.put("semi_free_messenger_send_gifs_interstitial", 1);
        String $const$string = C99084oO.$const$string(220);
        builder.put($const$string, 3);
        builder.put(AnonymousClass80H.$const$string(73), 1);
        builder.put("free_messenger_nux", 1);
        String $const$string2 = C99084oO.$const$string(5);
        builder.put($const$string2, 1);
        builder.put("semi_free_messenger_open_camera_interstitial", 1);
        builder.put("free_messenger_sending_free_tooltip", 3);
        String $const$string3 = AnonymousClass80H.$const$string(62);
        builder.put($const$string3, 1);
        String $const$string4 = AnonymousClass24B.$const$string(DexStore.LOAD_RESULT_OATMEAL_QUICKENED);
        builder.put($const$string4, 1);
        String $const$string5 = AnonymousClass24B.$const$string(AnonymousClass1Y3.A2e);
        builder.put($const$string5, 1);
        String $const$string6 = AnonymousClass24B.$const$string(32);
        builder.put($const$string6, 1);
        String $const$string7 = C99084oO.$const$string(20);
        builder.put($const$string7, 1);
        builder.put("free_messenger_open_camera_interstitial", 1);
        builder.put("url_interstitial", 1);
        builder.put(AnonymousClass24B.$const$string(0), 3);
        this.A01 = builder.build();
        ImmutableList.Builder builder2 = new ImmutableList.Builder();
        builder2.add((Object) "free_messenger_open_camera_interstitial");
        builder2.add((Object) $const$string7);
        builder2.add((Object) "url_interstitial");
        builder2.add((Object) $const$string6);
        builder2.add((Object) $const$string5);
        builder2.add((Object) $const$string4);
        builder2.add((Object) $const$string3);
        builder2.add((Object) "semi_free_messenger_open_camera_interstitial");
        builder2.add((Object) $const$string2);
        builder2.add((Object) "semi_free_messenger_send_gifs_interstitial");
        builder2.add((Object) "free_messenger_send_video_interstitial");
        builder2.add((Object) $const$string);
        builder2.add((Object) "dialtone_toggle_interstitial");
        builder2.add((Object) "dialtone_video_interstitial");
        builder2.add((Object) "dialtone_photo_interstitial");
        this.A00 = builder2.build();
    }
}
