package X;

/* renamed from: X.0VB  reason: invalid class name */
public final class AnonymousClass0VB extends AnonymousClass0VC implements AnonymousClass0US {
    public final int A00;

    public static AnonymousClass0VB A00(int i, AnonymousClass1XY r2) {
        return new AnonymousClass0VB(i, r2);
    }

    private AnonymousClass0VB(int i, AnonymousClass1XY r2) {
        super(r2);
        this.A00 = i;
    }
}
