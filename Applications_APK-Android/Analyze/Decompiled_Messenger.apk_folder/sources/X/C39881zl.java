package X;

import android.graphics.PointF;
import com.mapbox.mapboxsdk.geometry.LatLng;

/* renamed from: X.1zl  reason: invalid class name and case insensitive filesystem */
public final class C39881zl implements C28501Dvy {
    public final /* synthetic */ C39751zY A00;

    public C39881zl(C39751zY r1) {
        this.A00 = r1;
    }

    public void BgR(Object obj) {
        LatLng latLng = (LatLng) obj;
        C39751zY r2 = this.A00;
        if (!r2.A04) {
            r2.A0F.A04(r2.A0E, C28222Dqt.A01(latLng), null);
            r2.A07.A00.A0F.BQw();
            if (r2.A03) {
                PointF pixelForLatLng = r2.A0E.A08.A02.pixelForLatLng(latLng);
                C411224f r0 = r2.A0E.A0A;
                r0.A01 = pixelForLatLng;
                r0.A0H.BZR(pixelForLatLng);
                r2.A03 = false;
            }
        }
    }
}
