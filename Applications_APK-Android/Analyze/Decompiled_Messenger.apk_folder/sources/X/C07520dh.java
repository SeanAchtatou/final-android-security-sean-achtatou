package X;

import java.util.concurrent.Executor;

/* renamed from: X.0dh  reason: invalid class name and case insensitive filesystem */
public final class C07520dh {
    public static final C07520dh A03 = new C07520dh(null, null);
    public C07520dh A00;
    public final Runnable A01;
    public final Executor A02;

    public C07520dh(Runnable runnable, Executor executor) {
        this.A01 = runnable;
        this.A02 = executor;
    }
}
