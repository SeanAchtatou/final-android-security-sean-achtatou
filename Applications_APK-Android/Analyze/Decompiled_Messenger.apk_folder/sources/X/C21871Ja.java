package X;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1Ja  reason: invalid class name and case insensitive filesystem */
public final class C21871Ja {
    public static final AtomicReference A00 = new AtomicReference();

    public static Typeface A00(Context context, C32031l0 r6) {
        Typeface A002;
        int[] iArr = C32041l1.A00;
        int ordinal = r6.ordinal();
        int i = iArr[ordinal];
        switch (ordinal) {
            case 0:
                A002 = Typeface.create(C99084oO.$const$string(AnonymousClass1Y3.A1i), 0);
                break;
            case 1:
            default:
                if (i != 3) {
                    if (i == 4) {
                        A002 = Typeface.create("sans-serif", 1);
                        break;
                    } else {
                        A002 = null;
                        break;
                    }
                } else {
                    A002 = Typeface.create("sans-serif", 0);
                    break;
                }
            case 2:
                A002 = C22231Kk.A00(context);
                if (A002 == null && (A002 = (Typeface) A00.get()) == null) {
                    Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
                    if (createFromAsset == null) {
                        createFromAsset = Typeface.create("sans-serif", 0);
                    }
                    AtomicReference atomicReference = A00;
                    atomicReference.compareAndSet(null, createFromAsset);
                    A002 = (Typeface) atomicReference.get();
                    break;
                }
        }
        if (A002 == null) {
            C010708t.A0D(C21871Ja.class, "Unable to create roboto typeface: %s", r6.name());
        }
        return A002;
    }

    public static Typeface A01(Context context, Integer num, C32031l0 r3, Typeface typeface) {
        C32031l0 r0;
        if (num == AnonymousClass07B.A01) {
            return typeface;
        }
        if (r3 != C32031l0.UNSET) {
            return A00(context, r3);
        }
        if (typeface == null || !typeface.isBold()) {
            r0 = C32031l0.REGULAR;
        } else {
            r0 = C32031l0.BOLD;
        }
        return A00(context, r0);
    }

    public static void A02(TextView textView, Integer num, C32031l0 r3, Typeface typeface) {
        Typeface A01 = A01(textView.getContext(), num, r3, typeface);
        if (A01 != typeface) {
            textView.setTypeface(A01);
        }
    }
}
