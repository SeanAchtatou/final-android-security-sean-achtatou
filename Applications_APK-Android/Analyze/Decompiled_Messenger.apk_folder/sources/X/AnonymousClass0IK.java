package X;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;

/* renamed from: X.0IK  reason: invalid class name */
public abstract class AnonymousClass0IK extends D3J {
    private Fragment A00;
    private C16290wo A01;
    private ArrayList A02;
    private ArrayList A03;
    private final int A04;
    private final C13060qW A05;

    public abstract Fragment A0J(int i);

    public Parcelable A09() {
        Bundle bundle;
        if (this.A03.size() > 0) {
            bundle = new Bundle();
            Fragment.SavedState[] savedStateArr = new Fragment.SavedState[this.A03.size()];
            this.A03.toArray(savedStateArr);
            bundle.putParcelableArray("states", savedStateArr);
        } else {
            bundle = null;
        }
        for (int i = 0; i < this.A02.size(); i++) {
            Fragment fragment = (Fragment) this.A02.get(i);
            if (fragment != null && fragment.A1X()) {
                if (bundle == null) {
                    bundle = new Bundle();
                }
                this.A05.A0d(bundle, AnonymousClass08S.A09("f", i), fragment);
            }
        }
        return bundle;
    }

    public void A0B(Parcelable parcelable, ClassLoader classLoader) {
        if (parcelable != null) {
            Bundle bundle = (Bundle) parcelable;
            bundle.setClassLoader(classLoader);
            Parcelable[] parcelableArray = bundle.getParcelableArray("states");
            this.A03.clear();
            this.A02.clear();
            if (parcelableArray != null) {
                for (Parcelable parcelable2 : parcelableArray) {
                    this.A03.add((Fragment.SavedState) parcelable2);
                }
            }
            for (String str : bundle.keySet()) {
                if (str.startsWith("f")) {
                    int parseInt = Integer.parseInt(str.substring(1));
                    Fragment A0P = this.A05.A0P(bundle, str);
                    if (A0P == null) {
                        Log.w("FragmentStatePagerAdapt", AnonymousClass08S.A0J("Bad fragment at key ", str));
                    } else {
                        while (this.A02.size() <= parseInt) {
                            this.A02.add(null);
                        }
                        A0P.A1U(false);
                        this.A02.set(parseInt, A0P);
                    }
                }
            }
        }
    }

    public void A0C(ViewGroup viewGroup) {
        C16290wo r0 = this.A01;
        if (r0 != null) {
            try {
                r0.A05();
            } catch (IllegalStateException unused) {
                this.A01.A03();
            }
            this.A01 = null;
        }
    }

    public boolean A0E(View view, Object obj) {
        if (((Fragment) obj).A0I == view) {
            return true;
        }
        return false;
    }

    public Object A0G(ViewGroup viewGroup, int i) {
        Fragment A0J;
        Fragment.SavedState savedState;
        if (this.A02.size() <= i || (A0J = (Fragment) this.A02.get(i)) == null) {
            if (this.A01 == null) {
                this.A01 = this.A05.A0T();
            }
            A0J = A0J(i);
            if (this.A03.size() > i && (savedState = (Fragment.SavedState) this.A03.get(i)) != null) {
                A0J.A1S(savedState);
            }
            while (this.A02.size() <= i) {
                this.A02.add(null);
            }
            A0J.A1U(false);
            if (this.A04 == 0) {
                A0J.A1W(false);
            }
            this.A02.set(i, A0J);
            this.A01.A08(viewGroup.getId(), A0J);
            if (this.A04 == 1) {
                this.A01.A0K(A0J, AnonymousClass1XL.STARTED);
                return A0J;
            }
        }
        return A0J;
    }

    public void A0H(ViewGroup viewGroup, int i, Object obj) {
        Fragment.SavedState savedState;
        Fragment fragment = (Fragment) obj;
        if (this.A01 == null) {
            this.A01 = this.A05.A0T();
        }
        while (this.A03.size() <= i) {
            this.A03.add(null);
        }
        ArrayList arrayList = this.A03;
        if (fragment.A1X()) {
            savedState = this.A05.A0N(fragment);
        } else {
            savedState = null;
        }
        arrayList.set(i, savedState);
        this.A02.set(i, null);
        this.A01.A0I(fragment);
        if (fragment.equals(this.A00)) {
            this.A00 = null;
        }
    }

    public void A0I(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        Fragment fragment2 = this.A00;
        if (fragment != fragment2) {
            if (fragment2 != null) {
                fragment2.A1U(false);
                if (this.A04 == 1) {
                    if (this.A01 == null) {
                        this.A01 = this.A05.A0T();
                    }
                    this.A01.A0K(this.A00, AnonymousClass1XL.STARTED);
                } else {
                    this.A00.A1W(false);
                }
            }
            fragment.A1U(true);
            if (this.A04 == 1) {
                if (this.A01 == null) {
                    this.A01 = this.A05.A0T();
                }
                this.A01.A0K(fragment, AnonymousClass1XL.RESUMED);
            } else {
                fragment.A1W(true);
            }
            this.A00 = fragment;
        }
    }

    public void A0D(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            throw new IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }

    public AnonymousClass0IK(C13060qW r2) {
        this(r2, 0);
    }

    private AnonymousClass0IK(C13060qW r3, int i) {
        this.A01 = null;
        this.A03 = new ArrayList();
        this.A02 = new ArrayList();
        this.A00 = null;
        this.A05 = r3;
        this.A04 = i;
    }
}
