package X;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0jZ  reason: invalid class name and case insensitive filesystem */
public final class C10110jZ extends C10120ja {
    public final C10140jc _annotationIntrospector;
    public C183512m _anyGetter;
    public C29141fw _anySetterMethod;
    public C28311eb _bindings;
    public final C10070jV _classInfo;
    public final C10470kA _config;
    public Set _ignoredPropertyNames;
    public Map _injectables;
    public C29141fw _jsonValueMethod;
    public CZj _objectIdInfo;
    public final List _properties;

    private C422428v _createConverter(Object obj) {
        if (obj != null) {
            if (obj instanceof C422428v) {
                return (C422428v) obj;
            }
            if (obj instanceof Class) {
                Class<C422228t> cls = (Class) obj;
                if (!(cls == C422328u.class || cls == C422228t.class)) {
                    if (C422428v.class.isAssignableFrom(cls)) {
                        return (C422428v) C29081fq.createInstance(cls, this._config.canOverrideAccessModifiers());
                    }
                    throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned Class ", cls.getName(), "; expected Class<Converter>"));
                }
            } else {
                throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned Converter definition of type ", obj.getClass().getName(), "; expected type Converter or Class<Converter> instead"));
            }
        }
        return null;
    }

    public static C10110jZ forDeserialization(C183412k r5) {
        C29141fw r0;
        C10110jZ r2 = new C10110jZ(r5);
        LinkedList linkedList = r5._anySetters;
        if (linkedList != null) {
            if (linkedList.size() > 1) {
                C183412k.reportProblem(r5, "Multiple 'any-setters' defined (" + r5._anySetters.get(0) + " vs " + r5._anySetters.get(1) + ")");
            }
            r0 = (C29141fw) r5._anySetters.getFirst();
        } else {
            r0 = null;
        }
        r2._anySetterMethod = r0;
        r2._ignoredPropertyNames = r5._ignoredPropertyNames;
        r2._injectables = r5._injectables;
        r2._jsonValueMethod = r5.getJsonValueMethod();
        return r2;
    }

    private boolean isFactoryMethod(C29141fw r5) {
        if (!this._type._class.isAssignableFrom(r5._method.getReturnType()) || (!this._annotationIntrospector.hasCreatorAnnotation(r5) && !"valueOf".equals(r5.getName()))) {
            return false;
        }
        return true;
    }

    public C28311eb bindingsForBeanType() {
        if (this._bindings == null) {
            C10300js r3 = this._config._base._typeFactory;
            C10030jR r2 = this._type;
            this._bindings = new C28311eb(r3, null, r2._class, r2);
        }
        return this._bindings;
    }

    public C183512m findAnyGetter() {
        C183512m r0 = this._anyGetter;
        if (r0 != null) {
            if (!Map.class.isAssignableFrom(r0.getRawType())) {
                throw new IllegalArgumentException(AnonymousClass08S.A0P("Invalid 'any-getter' annotation on method ", this._anyGetter.getName(), "(): return type is not instance of java.util.Map"));
            }
        }
        return this._anyGetter;
    }

    public C29141fw findAnySetter() {
        Class<String> rawParameterType;
        C29141fw r1 = this._anySetterMethod;
        if (r1 == null || (rawParameterType = r1.getRawParameterType(0)) == String.class || rawParameterType == Object.class) {
            return this._anySetterMethod;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0S("Invalid 'any-setter' annotation on method ", this._anySetterMethod.getName(), "(): first argument not of type String or Object, but ", rawParameterType.getName()));
    }

    public Map findBackReferenceProperties() {
        C860046h findReferenceType;
        HashMap hashMap = null;
        for (C29111ft mutator : this._properties) {
            C183512m mutator2 = mutator.getMutator();
            if (!(mutator2 == null || (findReferenceType = this._annotationIntrospector.findReferenceType(mutator2)) == null)) {
                boolean z = false;
                if (findReferenceType._type == AnonymousClass46g.BACK_REFERENCE) {
                    z = true;
                }
                if (!z) {
                    continue;
                } else {
                    if (hashMap == null) {
                        hashMap = new HashMap();
                    }
                    String str = findReferenceType._name;
                    if (hashMap.put(str, mutator2) != null) {
                        throw new IllegalArgumentException(AnonymousClass08S.A0P("Multiple back-reference properties with name '", str, "'"));
                    }
                }
            }
        }
        return hashMap;
    }

    public C29151fx findDefaultConstructor() {
        C10070jV r1 = this._classInfo;
        if (!r1._creatorsResolved) {
            C10070jV.resolveCreators(r1);
        }
        return r1._defaultConstructor;
    }

    public C422428v findDeserializationConverter() {
        C10140jc r1 = this._annotationIntrospector;
        if (r1 == null) {
            return null;
        }
        return _createConverter(r1.findDeserializationConverter(this._classInfo));
    }

    public CX6 findExpectedFormat(CX6 cx6) {
        CX6 findFormat;
        C10140jc r1 = this._annotationIntrospector;
        if (r1 == null || (findFormat = r1.findFormat(this._classInfo)) == null) {
            return cx6;
        }
        return findFormat;
    }

    public Method findFactoryMethod(Class... clsArr) {
        C10070jV r1 = this._classInfo;
        if (!r1._creatorsResolved) {
            C10070jV.resolveCreators(r1);
        }
        for (C29141fw r4 : r1._creatorMethods) {
            if (isFactoryMethod(r4)) {
                Class rawParameterType = r4.getRawParameterType(0);
                for (Class isAssignableFrom : clsArr) {
                    if (rawParameterType.isAssignableFrom(isAssignableFrom)) {
                        return r4._method;
                    }
                }
                continue;
            }
        }
        return null;
    }

    public Map findInjectables() {
        return this._injectables;
    }

    public C29141fw findJsonValueMethod() {
        return this._jsonValueMethod;
    }

    public C29141fw findMethod(String str, Class[] clsArr) {
        C10070jV r1 = this._classInfo;
        if (r1._memberMethods == null) {
            C10070jV.resolveMemberMethods(r1);
        }
        LinkedHashMap linkedHashMap = r1._memberMethods._methods;
        if (linkedHashMap == null) {
            return null;
        }
        return (C29141fw) linkedHashMap.get(new C184212z(str, clsArr));
    }

    public Class findPOJOBuilder() {
        C10140jc r1 = this._annotationIntrospector;
        if (r1 == null) {
            return null;
        }
        return r1.findPOJOBuilder(this._classInfo);
    }

    public BM3 findPOJOBuilderConfig() {
        C10140jc r1 = this._annotationIntrospector;
        if (r1 == null) {
            return null;
        }
        return r1.findPOJOBuilderConfig(this._classInfo);
    }

    public List findProperties() {
        return this._properties;
    }

    public C422428v findSerializationConverter() {
        C10140jc r1 = this._annotationIntrospector;
        if (r1 == null) {
            return null;
        }
        return _createConverter(r1.findSerializationConverter(this._classInfo));
    }

    public AnonymousClass0oC findSerializationInclusion(AnonymousClass0oC r3) {
        C10140jc r1 = this._annotationIntrospector;
        if (r1 == null) {
            return r3;
        }
        return r1.findSerializationInclusion(this._classInfo, r3);
    }

    public Constructor findSingleArgConstructor(Class... clsArr) {
        Class<?> cls;
        C10070jV r1 = this._classInfo;
        if (!r1._creatorsResolved) {
            C10070jV.resolveCreators(r1);
        }
        for (C29151fx r4 : r1._constructors) {
            if (r4._constructor.getParameterTypes().length == 1) {
                Class<?>[] parameterTypes = r4._constructor.getParameterTypes();
                if (0 >= parameterTypes.length) {
                    cls = null;
                } else {
                    cls = parameterTypes[0];
                }
                for (Class cls2 : clsArr) {
                    if (cls2 == cls) {
                        return r4._constructor;
                    }
                }
                continue;
            }
        }
        return null;
    }

    public C10100jY getClassAnnotations() {
        C10070jV r1 = this._classInfo;
        if (r1._classAnnotations == null) {
            C10070jV.resolveClassAnnotations(r1);
        }
        return r1._classAnnotations;
    }

    public C10070jV getClassInfo() {
        return this._classInfo;
    }

    public List getConstructors() {
        C10070jV r1 = this._classInfo;
        if (!r1._creatorsResolved) {
            C10070jV.resolveCreators(r1);
        }
        return r1._constructors;
    }

    public List getFactoryMethods() {
        C10070jV r1 = this._classInfo;
        if (!r1._creatorsResolved) {
            C10070jV.resolveCreators(r1);
        }
        List<C29141fw> list = r1._creatorMethods;
        if (list.isEmpty()) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        for (C29141fw r12 : list) {
            if (isFactoryMethod(r12)) {
                arrayList.add(r12);
            }
        }
        return arrayList;
    }

    public Set getIgnoredPropertyNames() {
        Set set = this._ignoredPropertyNames;
        if (set == null) {
            return Collections.emptySet();
        }
        return set;
    }

    public CZj getObjectIdInfo() {
        return this._objectIdInfo;
    }

    public boolean hasKnownClassAnnotations() {
        int size;
        C10070jV r1 = this._classInfo;
        if (r1._classAnnotations == null) {
            C10070jV.resolveClassAnnotations(r1);
        }
        HashMap hashMap = r1._classAnnotations._annotations;
        if (hashMap == null) {
            size = 0;
        } else {
            size = hashMap.size();
        }
        if (size > 0) {
            return true;
        }
        return false;
    }

    public Object instantiateBean(boolean z) {
        C10070jV r1 = this._classInfo;
        if (!r1._creatorsResolved) {
            C10070jV.resolveCreators(r1);
        }
        C29151fx r12 = r1._defaultConstructor;
        if (r12 == null) {
            return null;
        }
        if (z) {
            C29081fq.checkAndFixAccess(r12.getMember());
        }
        try {
            return r12._constructor.newInstance(new Object[0]);
        } catch (Exception e) {
            e = e;
            while (e.getCause() != null) {
                e = e.getCause();
            }
            if (e instanceof Error) {
                throw ((Error) e);
            } else if (e instanceof RuntimeException) {
                throw ((RuntimeException) e);
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A0U("Failed to instantiate bean of type ", this._classInfo._class.getName(), ": (", e.getClass().getName(), ") ", e.getMessage()), e);
            }
        }
    }

    public C10030jR resolveType(Type type) {
        if (type == null) {
            return null;
        }
        C28311eb bindingsForBeanType = bindingsForBeanType();
        return bindingsForBeanType._typeFactory._constructType(type, bindingsForBeanType);
    }

    public C10110jZ(C10470kA r2, C10030jR r3, C10070jV r4, List list) {
        super(r3);
        C10140jc annotationIntrospector;
        this._config = r2;
        if (r2 == null) {
            annotationIntrospector = null;
        } else {
            annotationIntrospector = r2.getAnnotationIntrospector();
        }
        this._annotationIntrospector = annotationIntrospector;
        this._classInfo = r4;
        this._properties = list;
    }

    public C10110jZ(C183412k r6) {
        this(r6._config, r6._type, r6._classDef, new ArrayList(r6._properties.values()));
        CZj findObjectIdInfo;
        C10140jc r1 = r6._annotationIntrospector;
        if (r1 == null) {
            findObjectIdInfo = null;
        } else {
            findObjectIdInfo = r1.findObjectIdInfo(r6._classDef);
            if (findObjectIdInfo != null) {
                findObjectIdInfo = r6._annotationIntrospector.findObjectReferenceInfo(r6._classDef, findObjectIdInfo);
            }
        }
        this._objectIdInfo = findObjectIdInfo;
    }
}
