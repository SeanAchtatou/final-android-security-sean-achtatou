package X;

/* renamed from: X.0SI  reason: invalid class name */
public final class AnonymousClass0SI implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$CallbackHandler$7";
    public final /* synthetic */ AnonymousClass0CC A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ Throwable A03;

    public AnonymousClass0SI(AnonymousClass0CC r1, String str, String str2, Throwable th) {
        this.A00 = r1;
        this.A01 = str;
        this.A02 = str2;
        this.A03 = th;
    }

    public void run() {
        this.A00.A02.A0I.AYn(this.A01, this.A02, this.A03);
    }
}
