package X;

import android.os.Build;
import android.system.ErrnoException;
import android.util.Log;
import java.lang.reflect.Field;

/* renamed from: X.0KA  reason: invalid class name */
public final class AnonymousClass0KA {
    public static Class A00;
    public static Field A01;
    public static boolean A02;

    public static int A00(Throwable th) {
        if (th == null) {
            return -1;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            return A01(th);
        }
        if (!A02) {
            synchronized (AnonymousClass0KA.class) {
                if (!A02) {
                    A02 = true;
                    try {
                        Class<?> cls = Class.forName("libcore.io.ErrnoException");
                        Field declaredField = cls.getDeclaredField("errno");
                        A00 = cls;
                        A01 = declaredField;
                    } catch (Exception e) {
                        Log.e("ErrnoExtractor", "Error loading errno exception class", e);
                    }
                }
            }
        }
        Class cls2 = A00;
        if (cls2 == null || !th.getClass().equals(cls2)) {
            return -1;
        }
        try {
            return A01.getInt(th);
        } catch (IllegalAccessException e2) {
            Log.e("ErrnoExtractor", "Error accessing errno field", e2);
            return -1;
        }
    }

    public static int A01(Throwable th) {
        if (!(th instanceof ErrnoException)) {
            return -1;
        }
        return ((ErrnoException) th).errno;
    }

    private AnonymousClass0KA() {
    }
}
