package X;

import com.google.common.base.Predicate;
import java.util.Map;

/* renamed from: X.1Fk  reason: invalid class name and case insensitive filesystem */
public final class C21151Fk<K, V> extends C21091Fe<K, V> {
    public final Predicate A00;

    public boolean containsKey(Object obj) {
        if (!this.A01.containsKey(obj) || !this.A00.apply(obj)) {
            return false;
        }
        return true;
    }

    public C21151Fk(Map map, Predicate predicate, Predicate predicate2) {
        super(map, predicate2);
        this.A00 = predicate;
    }
}
