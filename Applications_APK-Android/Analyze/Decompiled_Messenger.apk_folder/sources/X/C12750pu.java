package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0pu  reason: invalid class name and case insensitive filesystem */
public final class C12750pu {
    private static volatile C12750pu A01;
    public final C12760pv A00;

    public static final C12750pu A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C12750pu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C12750pu(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C12750pu(AnonymousClass1XY r2) {
        this.A00 = C12760pv.A00(r2);
    }
}
