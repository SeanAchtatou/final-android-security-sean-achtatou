package X;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

/* renamed from: X.0XQ  reason: invalid class name */
public final class AnonymousClass0XQ {
    public final File A00;
    private final int A01;
    private final Map A02 = new HashMap();
    private final Executor A03;

    public synchronized C05770aI A00(String str) {
        C05770aI r2;
        r2 = (C05770aI) this.A02.get(str);
        if (r2 == null) {
            File file = new File(this.A00, str);
            File parentFile = file.getParentFile();
            C000300h.A02(parentFile, "expecting a file which is always under some dir");
            if (parentFile.exists() && !parentFile.isDirectory()) {
                C010708t.A0P("LightSharedPreferencesFactory", "cannot create directory %s, a file already exists with that name", parentFile.getAbsolutePath());
            }
            if (!parentFile.exists()) {
                parentFile.mkdirs();
            }
            r2 = new C05770aI(file, this.A03, this.A01);
            this.A02.put(str, r2);
        }
        return r2;
    }

    public AnonymousClass0XQ(Executor executor, File file, int i) {
        this.A03 = executor;
        this.A00 = file;
        this.A01 = i;
    }
}
