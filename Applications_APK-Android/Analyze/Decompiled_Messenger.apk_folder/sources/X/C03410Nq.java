package X;

import android.database.DatabaseUtils;
import com.google.common.base.Function;

/* renamed from: X.0Nq  reason: invalid class name and case insensitive filesystem */
public final class C03410Nq implements Function {
    public Object apply(Object obj) {
        String obj2;
        if (obj == null) {
            obj2 = "null";
        } else {
            obj2 = obj.toString();
        }
        return DatabaseUtils.sqlEscapeString(obj2);
    }
}
