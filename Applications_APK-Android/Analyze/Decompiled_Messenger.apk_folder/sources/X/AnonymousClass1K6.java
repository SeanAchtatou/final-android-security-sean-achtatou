package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1K6  reason: invalid class name */
public final class AnonymousClass1K6 extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C15350v9 A01;
    @Comparable(type = 5)
    public ImmutableList A02;

    public AnonymousClass1K6(Context context) {
        super("NavigationButtons");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }
}
