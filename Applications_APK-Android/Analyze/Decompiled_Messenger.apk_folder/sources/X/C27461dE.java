package X;

/* renamed from: X.1dE  reason: invalid class name and case insensitive filesystem */
public final class C27461dE {
    public static volatile C27431dB A00;

    public static Object A00(String str) {
        C27431dB r2 = A00;
        if (r2 != null && C25231Yv.A01()) {
            return C27401d8.A01(AnonymousClass08S.A0J(r2.A01, str), r2.A00);
        }
        return null;
    }

    public static Object A01(String str, Object obj) {
        C27431dB r1 = A00;
        if (r1 == null || obj == null || !C25231Yv.A01() || !(obj instanceof AnonymousClass1XV)) {
            return null;
        }
        String A0J = AnonymousClass08S.A0J(r1.A01, str);
        int i = r1.A00;
        return C27401d8.A00.A02((AnonymousClass1XV) obj, A0J, 1, i);
    }

    public static void A02(Object obj) {
        if (A00 != null && obj != null && (obj instanceof AnonymousClass1XV)) {
            ((AnonymousClass1XV) obj).close();
        }
    }
}
