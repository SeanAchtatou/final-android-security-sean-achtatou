package X;

/* renamed from: X.17m  reason: invalid class name and case insensitive filesystem */
public final class C192717m implements AnonymousClass05U {
    public final /* synthetic */ C192017f A00;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void BZp(long j) {
        C192017f r10 = this.A00;
        double d = r10.A04;
        long max = Math.max(Math.round(((double) j) / d), 1L);
        long min = Math.min(max - 1, 100L);
        double d2 = (double) min;
        r10.A01 += d2;
        if (min > 4) {
            r10.A00 += d2 / 4.0d;
        }
        r10.A02 = (long) (((double) r10.A02) + (d * ((double) max)));
    }

    public C192717m(C192017f r1) {
        this.A00 = r1;
    }
}
