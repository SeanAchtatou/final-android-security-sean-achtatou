package X;

/* renamed from: X.1pQ  reason: invalid class name and case insensitive filesystem */
public final class C34341pQ {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;

    static {
        AnonymousClass1Y7 r1 = C04350Ue.A06;
        A00 = (AnonymousClass1Y7) r1.A09("mobile_online_availability");
        A01 = (AnonymousClass1Y7) r1.A09("vsc_messenger_presence_availability");
        A02 = (AnonymousClass1Y7) r1.A09("vsc_migrated");
    }
}
