package X;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import com.facebook.litho.ComponentTree;
import com.facebook.litho.LithoView;

/* renamed from: X.1Kq  reason: invalid class name and case insensitive filesystem */
public final class C22261Kq extends FrameLayout {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public ComponentTree A06;
    public AnonymousClass1BY A07;
    public AnonymousClass1MD A08;
    public Integer A09;
    public Integer A0A = AnonymousClass07B.A0C;
    public String A0B;
    public boolean A0C;
    public boolean A0D;
    private float A0E;
    private ObjectAnimator A0F;
    public final LithoView A0G;
    private final float A0H;

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        if (r3.A0G.getTranslationX() < ((float) r3.A03)) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001a, code lost:
        if (r3.A0G.getTranslationX() > ((float) r3.A03)) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A04(float r4) {
        /*
            r3 = this;
            r2 = 0
            r0 = 0
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x0021
            int r1 = r3.A05
            r0 = 0
            if (r1 <= 0) goto L_0x000c
            r0 = 1
        L_0x000c:
            if (r0 != 0) goto L_0x001f
            com.facebook.litho.LithoView r0 = r3.A0G
            float r1 = r0.getTranslationX()
            int r0 = r3.A03
            float r0 = (float) r0
            int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x001d
        L_0x001c:
            r0 = 1
        L_0x001d:
            if (r0 == 0) goto L_0x0020
        L_0x001f:
            r2 = 1
        L_0x0020:
            return r2
        L_0x0021:
            int r1 = r3.A04
            r0 = 0
            if (r1 <= 0) goto L_0x0027
            r0 = 1
        L_0x0027:
            if (r0 != 0) goto L_0x001f
            com.facebook.litho.LithoView r0 = r3.A0G
            float r1 = r0.getTranslationX()
            int r0 = r3.A03
            float r0 = (float) r0
            int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            r0 = 0
            if (r1 >= 0) goto L_0x001d
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22261Kq.A04(float):boolean");
    }

    private void A00() {
        ComponentTree componentTree = this.A0G.A03;
        ComponentTree componentTree2 = this.A06;
        if (componentTree != componentTree2) {
            AnonymousClass10L r2 = new AnonymousClass10L();
            componentTree2.A0O(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0), r2);
            this.A02 = r2.A01;
            this.A01 = r2.A00;
            this.A0G.A0b(this.A06);
            int i = -this.A04;
            this.A03 = i;
            this.A0G.setTranslationX((float) i);
        }
    }

    private void A01() {
        AnonymousClass1BY r2 = this.A07;
        if (r2 != null) {
            for (C22261Kq r0 : r2.A01) {
                if (r0 != this) {
                    r0.A05();
                }
            }
            r2.A00 = this;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void A02(float f, boolean z, Integer num, Integer num2) {
        if (!this.A0C && this.A0A == AnonymousClass07B.A00) {
            f = Math.min(f, 0.0f);
        } else if (!this.A0D && this.A0A == AnonymousClass07B.A01) {
            f = Math.max(f, (float) (this.A03 - this.A05));
        }
        if (this.A0G.getX() == f) {
            return;
        }
        if (z) {
            this.A09 = num;
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.A0G, C99084oO.$const$string(27), f);
            this.A0F = ofFloat;
            ofFloat.addListener(new AnonymousClass4T6(this, num2));
            this.A0F.setDuration(300L);
            this.A0F.setInterpolator(new DecelerateInterpolator());
            this.A0F.start();
            return;
        }
        this.A09 = num2;
        this.A0G.setTranslationX(f);
    }

    public static void A03(C22261Kq r2) {
        ObjectAnimator objectAnimator = r2.A0F;
        if (objectAnimator != null && objectAnimator.isRunning()) {
            r2.A0F.cancel();
            r2.A0F = null;
        }
        r2.A03 = 0;
        r2.A0G.setTranslationX((float) 0);
        r2.A09 = AnonymousClass07B.A0Y;
    }

    public void A05() {
        Integer num;
        Integer num2 = this.A09;
        Integer num3 = AnonymousClass07B.A0Y;
        if (num2 != num3 && num2 != (num = AnonymousClass07B.A0i)) {
            int i = -this.A04;
            this.A03 = i;
            A02((float) i, true, num, num3);
        }
    }

    public void onMeasure(int i, int i2) {
        this.A0G.measure(View.MeasureSpec.makeMeasureSpec(this.A02, 1073741824), View.MeasureSpec.makeMeasureSpec(this.A01, 1073741824));
        setMeasuredDimension(View.MeasureSpec.getSize(i), View.MeasureSpec.getSize(i2));
    }

    public C22261Kq(Context context) {
        super(context);
        LithoView lithoView = new LithoView(context);
        this.A0G = lithoView;
        addView(lithoView);
        this.A0H = (float) ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public void A06() {
        A00();
        A02((float) (this.A03 - this.A05), true, AnonymousClass07B.A0N, AnonymousClass07B.A0C);
        AnonymousClass1MD r0 = this.A08;
        if (r0 != null) {
            r0.Bmj();
        }
        A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        if (A04(r2) == false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r5) {
        /*
            r4 = this;
            float r3 = r5.getRawX()
            int r1 = r5.getAction()
            if (r1 == 0) goto L_0x0026
            r0 = 2
            if (r1 != r0) goto L_0x0022
            float r0 = r4.A0E
            float r2 = r3 - r0
            float r1 = java.lang.Math.abs(r2)
            float r0 = r4.A0H
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0022
            boolean r0 = r4.A04(r2)
            r1 = 1
            if (r0 != 0) goto L_0x0023
        L_0x0022:
            r1 = 0
        L_0x0023:
            r4.A00 = r3
            return r1
        L_0x0026:
            r4.A0E = r3
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22261Kq.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r1 != 3) goto L_0x0018;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x008e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r11) {
        /*
            r10 = this;
            r0 = 359162967(0x15686457, float:4.693118E-26)
            int r5 = X.C000700l.A05(r0)
            float r4 = r11.getRawX()
            int r1 = r11.getAction()
            r3 = 1
            if (r1 == r3) goto L_0x004b
            r0 = 2
            if (r1 == r0) goto L_0x0022
            r0 = 3
            if (r1 == r0) goto L_0x004b
        L_0x0018:
            r3 = 0
        L_0x0019:
            r10.A00 = r4
            r0 = -1455410779(0xffffffffa94031a5, float:-4.2675624E-14)
            X.C000700l.A0B(r0, r5)
            return r3
        L_0x0022:
            r10.requestDisallowInterceptTouchEvent(r3)
            float r0 = r10.A00
            float r1 = r4 - r0
            boolean r0 = r10.A04(r1)
            if (r0 == 0) goto L_0x0019
            r10.A00()
            r0 = 0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0048
            java.lang.Integer r0 = X.AnonymousClass07B.A00
        L_0x0039:
            r10.A0A = r0
            com.facebook.litho.LithoView r0 = r10.A0G
            float r2 = r0.getTranslationX()
            float r2 = r2 + r1
            r1 = 0
            r0 = 0
            r10.A02(r2, r1, r0, r0)
            goto L_0x0019
        L_0x0048:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            goto L_0x0039
        L_0x004b:
            int r0 = r10.A03
            float r6 = (float) r0
            com.facebook.litho.LithoView r0 = r10.A0G
            float r1 = r0.getTranslationX()
            int r0 = r10.A03
            float r0 = (float) r0
            float r1 = r1 - r0
            float r1 = java.lang.Math.abs(r1)
            r3 = 0
            r2 = 1
            r0 = 1120403456(0x42c80000, float:100.0)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x00b4
            java.lang.Integer r1 = r10.A0A
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x009b
            com.facebook.litho.LithoView r0 = r10.A0G
            float r1 = r0.getTranslationX()
            int r0 = r10.A03
            int r0 = r0 + 100
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x009b
            java.lang.Integer r8 = X.AnonymousClass07B.A01
            java.lang.Integer r7 = X.AnonymousClass07B.A00
            r1 = 0
            r10.A02(r1, r2, r8, r7)
            X.1MD r0 = r10.A08
            if (r0 == 0) goto L_0x0088
            r0.BcX()
        L_0x0088:
            r10.A01()
        L_0x008b:
            r0 = 1
        L_0x008c:
            if (r0 != 0) goto L_0x0092
            r0 = 0
            r10.A02(r6, r2, r0, r0)
        L_0x0092:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            r10.A0A = r0
            r10.requestDisallowInterceptTouchEvent(r3)
            goto L_0x0018
        L_0x009b:
            java.lang.Integer r1 = r10.A0A
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            if (r1 != r0) goto L_0x00b4
            com.facebook.litho.LithoView r0 = r10.A0G
            float r1 = r0.getTranslationX()
            int r0 = r10.A03
            int r0 = r0 + -100
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x00b4
            r10.A06()
            goto L_0x008b
        L_0x00b4:
            r0 = 0
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22261Kq.onTouchEvent(android.view.MotionEvent):boolean");
    }
}
