package X;

/* renamed from: X.0Ff  reason: invalid class name and case insensitive filesystem */
public final class C02510Ff extends C007907e {
    public C02500Fe A00 = new C02500Fe(true);

    public static C03130Kd A00(C02510Ff r2, String str) {
        C03130Kd r1 = (C03130Kd) r2.A00.tagLocationDetails.get(str);
        if (r1 != null) {
            return r1;
        }
        C03130Kd r12 = new C03130Kd();
        r2.A00.tagLocationDetails.put(str, r12);
        return r12;
    }

    public AnonymousClass0FM A03() {
        return new C02500Fe(true);
    }

    public boolean A04(AnonymousClass0FM r2) {
        C02500Fe r22 = (C02500Fe) r2;
        synchronized (this) {
            r22.A09(this.A00);
        }
        return true;
    }
}
