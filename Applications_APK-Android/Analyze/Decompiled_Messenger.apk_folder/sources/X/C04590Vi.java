package X;

import com.google.common.base.Preconditions;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0Vi  reason: invalid class name and case insensitive filesystem */
public final class C04590Vi extends ThreadPoolExecutor implements C04600Vj {
    public final int A00;
    public final AnonymousClass0VV A01;
    public final boolean A02;
    private final C04550Vd A03;
    private final AnonymousClass2S5 A04;
    private final C37041uW A05;
    private final AtomicLong A06 = new AtomicLong();
    private final boolean A07;

    public C04590Vi(AnonymousClass0VX r9, C04550Vd r10, ThreadFactory threadFactory, C37041uW r12) {
        super(r9.A05, Integer.MAX_VALUE, (long) r9.A04, TimeUnit.SECONDS, r10, threadFactory);
        AnonymousClass2S5 r0;
        Preconditions.checkArgument(r9.A05 > 0);
        this.A03 = r10;
        this.A05 = r12;
        this.A00 = r9.A00;
        this.A07 = r9.A09;
        this.A01 = r9.A06;
        int i = r9.A01;
        if (i > 0) {
            r0 = new AnonymousClass2S5(i);
        } else {
            r0 = null;
        }
        this.A04 = r0;
        this.A02 = r9.A08;
    }

    private void A00(C07820eD r41, Integer num, long j, long j2, long j3, Boolean bool) {
        Preconditions.checkNotNull(this.A05);
        C52952jx ATQ = r41.ATQ();
        if (ATQ != null) {
            C37041uW r14 = this.A05;
            Integer num2 = AnonymousClass07B.A00;
            String C4H = r41.C4H();
            String str = r41.AZE().A0B;
            Integer C4I = r41.C4I();
            AnonymousClass0VS r12 = ATQ.A02;
            long j4 = ATQ.A09;
            Integer num3 = num;
            Boolean bool2 = bool;
            long j5 = j2;
            long j6 = j4;
            AnonymousClass0VS r19 = r12;
            String str2 = str;
            r14.A02(num2, C4H, str2, C4I, r19, num3, bool2, j, j5, j6, j3, (long) ATQ.A05, (long) ATQ.A04, (long) ATQ.A03, (long) ATQ.A07, (long) ATQ.A06);
        }
    }

    public void A01(C07820eD r11) {
        if (this.A05 != null) {
            A00(r11, AnonymousClass07B.A0C, 0, 0, 0, null);
        }
    }

    public void AP4(C07690e0 r4) {
        C04550Vd r2 = this.A03;
        Preconditions.checkNotNull(r2.A0E);
        r2.A0E.execute(new AnonymousClass2X6(r2, r4));
    }

    public AnonymousClass0VV ATR() {
        return this.A01;
    }

    public void AYZ(C07820eD r4) {
        C04550Vd r2;
        C04560Ve r22;
        try {
            C179878Tk A002 = C25201Ys.A00(this.A01, r4);
            if (A002 != null) {
                A002.onTaskCreated(r4);
            }
            r2 = this.A03;
            if (r4.AZE().A0E()) {
                r2.A06.A00();
                C04550Vd.A03(r2, r4);
                r2.A06.A02();
                return;
            }
            r22 = r2.A06;
            r22.A02.add(r4);
            if (r22.A05.tryLock()) {
                if (r22.A05.getHoldCount() > 1) {
                    r22.A01();
                }
                r22.A02();
            }
        } catch (RejectedExecutionException e) {
            A01(r4);
            C179878Tk A003 = C25201Ys.A00(this.A01, r4);
            if (A003 != null) {
                A003.onTaskRejected();
            }
            throw e;
        } catch (Throwable th) {
            r2.A06.A02();
            throw th;
        }
    }

    public long BLs() {
        return this.A06.getAndIncrement();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006d, code lost:
        if (r0 == false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00d4, code lost:
        if (r4 == X.AnonymousClass07B.A01) goto L_0x00d6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void afterExecute(java.lang.Runnable r21, java.lang.Throwable r22) {
        /*
            r20 = this;
            r2 = r21
            r11 = r2
            X.0eD r11 = (X.C07820eD) r11
            boolean r0 = r2 instanceof X.C07690e0
            if (r0 == 0) goto L_0x0032
            X.0e0 r2 = (X.C07690e0) r2
        L_0x000b:
            r5 = r20
            X.2S5 r0 = r5.A04
            if (r0 == 0) goto L_0x0016
            java.util.concurrent.ConcurrentHashMap r0 = r0.A02
            r0.remove(r11)
        L_0x0016:
            X.0Vd r3 = r5.A03
            r4 = 1
            r6 = 0
            if (r22 == 0) goto L_0x001d
            r6 = 1
        L_0x001d:
            java.lang.ThreadLocal r0 = r3.A09
            java.lang.Object r1 = r0.get()
            r0 = 0
            if (r1 != 0) goto L_0x0027
            r0 = 1
        L_0x0027:
            com.google.common.base.Preconditions.checkState(r0)
            if (r6 == 0) goto L_0x0055
            X.0Ve r0 = r3.A06
            r0.A00()
            goto L_0x0034
        L_0x0032:
            r2 = 0
            goto L_0x000b
        L_0x0034:
            X.0Vk r0 = r11.AZE()     // Catch:{ all -> 0x004e }
            X.0Vc r0 = r0.A08     // Catch:{ all -> 0x004e }
            int r1 = r0.A01     // Catch:{ all -> 0x004e }
            r0 = 1
            if (r1 != r0) goto L_0x0045
            X.0Vk r0 = r11.AZE()     // Catch:{ all -> 0x004e }
            r0.A04 = r4     // Catch:{ all -> 0x004e }
        L_0x0045:
            X.C04550Vd.A02(r3, r11)     // Catch:{ all -> 0x004e }
            X.1YF r0 = r3.A05     // Catch:{ all -> 0x004e }
            r0.A02()     // Catch:{ all -> 0x004e }
            goto L_0x005b
        L_0x004e:
            r1 = move-exception
            X.0Ve r0 = r3.A06
            r0.A02()
            throw r1
        L_0x0055:
            java.lang.ThreadLocal r0 = r3.A09
            r0.set(r11)
            goto L_0x0060
        L_0x005b:
            X.0Ve r0 = r3.A06
            r0.A02()
        L_0x0060:
            boolean r0 = r5.A07
            if (r0 == 0) goto L_0x006f
            android.os.Looper r1 = android.os.Looper.myLooper()
            r0 = 0
            if (r1 == 0) goto L_0x006c
            r0 = 1
        L_0x006c:
            r9 = 1
            if (r0 != 0) goto L_0x0070
        L_0x006f:
            r9 = 0
        L_0x0070:
            if (r9 == 0) goto L_0x008e
            X.0Vk r0 = r11.AZE()
            java.lang.String r6 = r0.A0B
            java.lang.String r1 = r11.C4H()
            java.lang.String r3 = "Do not create a Looper on a threadpool thread. executor: %s task: %s."
            r0 = 6
            boolean r0 = X.C010708t.A0X(r0)
            if (r0 == 0) goto L_0x008e
            java.lang.Object[] r1 = new java.lang.Object[]{r6, r1}
            java.lang.String r0 = "ComTP"
            X.C010708t.A0O(r0, r3, r1)
        L_0x008e:
            X.2jx r6 = r11.ATQ()
            if (r6 == 0) goto L_0x00bd
            long r0 = r6.A01
            r7 = 0
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 != 0) goto L_0x009d
            r4 = 0
        L_0x009d:
            com.google.common.base.Preconditions.checkState(r4)
            long r15 = X.AnonymousClass0Y1.A00()
            if (r22 != 0) goto L_0x00c1
            java.lang.Integer r12 = X.AnonymousClass07B.A00
        L_0x00a8:
            long r3 = r6.A08
            long r13 = r0 - r3
            long r15 = r15 - r0
            long r17 = android.os.SystemClock.currentThreadTimeMillis()
            long r0 = r6.A00
            long r17 = r17 - r0
            java.lang.Boolean r19 = java.lang.Boolean.valueOf(r9)
            r10 = r5
            r10.A00(r11, r12, r13, r15, r17, r19)
        L_0x00bd:
            if (r2 == 0) goto L_0x0110
            monitor-enter(r2)
            goto L_0x00c4
        L_0x00c1:
            java.lang.Integer r12 = X.AnonymousClass07B.A01
            goto L_0x00a8
        L_0x00c4:
            boolean r0 = r2.A02     // Catch:{ all -> 0x0108 }
            if (r0 != 0) goto L_0x00ca
            monitor-exit(r2)     // Catch:{ all -> 0x0108 }
            goto L_0x0110
        L_0x00ca:
            java.lang.Integer r4 = r2.A06     // Catch:{ all -> 0x0108 }
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x0108 }
            r3 = 0
            if (r4 == r0) goto L_0x00d6
            java.lang.Integer r1 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0108 }
            r0 = 0
            if (r4 != r1) goto L_0x00d7
        L_0x00d6:
            r0 = 1
        L_0x00d7:
            com.google.common.base.Preconditions.checkState(r0)     // Catch:{ all -> 0x0108 }
            r2.A02 = r3     // Catch:{ all -> 0x0108 }
            r0 = 0
            r2.A01 = r0     // Catch:{ all -> 0x0108 }
            X.0Vj r0 = r2.A05     // Catch:{ all -> 0x0108 }
            long r0 = r0.BLs()     // Catch:{ all -> 0x0108 }
            r2.A0D = r0     // Catch:{ all -> 0x0108 }
            java.lang.Integer r0 = r2.A06     // Catch:{ all -> 0x0108 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x0108 }
            switch(r0) {
                case 0: goto L_0x0104;
                case 1: goto L_0x00fc;
                case 2: goto L_0x00f2;
                default: goto L_0x00f0;
            }     // Catch:{ all -> 0x0108 }
        L_0x00f0:
            monitor-exit(r2)     // Catch:{ all -> 0x0108 }
            goto L_0x010b
        L_0x00f2:
            long r3 = X.AnonymousClass0Y1.A00()     // Catch:{ all -> 0x0108 }
            long r0 = r2.A04     // Catch:{ all -> 0x0108 }
            long r3 = r3 + r0
            r2.A00 = r3     // Catch:{ all -> 0x0108 }
            goto L_0x00f0
        L_0x00fc:
            long r3 = r2.A00     // Catch:{ all -> 0x0108 }
            long r0 = r2.A04     // Catch:{ all -> 0x0108 }
            long r3 = r3 + r0
            r2.A00 = r3     // Catch:{ all -> 0x0108 }
            goto L_0x00f0
        L_0x0104:
            com.google.common.base.Preconditions.checkState(r3)     // Catch:{ all -> 0x0108 }
            goto L_0x00f0
        L_0x0108:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0108 }
            throw r0
        L_0x010b:
            X.0Vj r0 = r2.A05
            r0.AYZ(r2)
        L_0x0110:
            int r0 = r5.A00     // Catch:{ RuntimeException -> 0x0116 }
            android.os.Process.setThreadPriority(r0)     // Catch:{ RuntimeException -> 0x0116 }
            goto L_0x011c
        L_0x0116:
            r1 = move-exception
            java.lang.String r0 = "Unable to reset thread priority"
            X.AnonymousClass57O.A00(r1, r0)
        L_0x011c:
            if (r9 != 0) goto L_0x011f
            return
        L_0x011f:
            X.1vv r0 = new X.1vv
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04590Vi.afterExecute(java.lang.Runnable, java.lang.Throwable):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r3.mThreadPriority == null) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void beforeExecute(java.lang.Thread r9, java.lang.Runnable r10) {
        /*
            r8 = this;
            X.0eD r10 = (X.C07820eD) r10
            X.0Vd r0 = r8.A03
            java.lang.ThreadLocal r0 = r0.A09
            java.lang.Object r1 = r0.get()
            r0 = 0
            if (r1 != 0) goto L_0x000e
            r0 = 1
        L_0x000e:
            com.google.common.base.Preconditions.checkState(r0)
            X.0VS r3 = r10.By8()
            int r5 = r3.mAndroidThreadPriority     // Catch:{ RuntimeException -> 0x0039 }
            r4 = 0
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r5 == r2) goto L_0x0021
            X.0V7 r1 = r3.mThreadPriority     // Catch:{ RuntimeException -> 0x0039 }
            r0 = 0
            if (r1 != 0) goto L_0x0022
        L_0x0021:
            r0 = 1
        L_0x0022:
            com.google.common.base.Preconditions.checkState(r0)     // Catch:{ RuntimeException -> 0x0039 }
            if (r5 > r2) goto L_0x002b
            X.0V7 r0 = r3.mThreadPriority     // Catch:{ RuntimeException -> 0x0039 }
            if (r0 == 0) goto L_0x002c
        L_0x002b:
            r4 = 1
        L_0x002c:
            com.google.common.base.Preconditions.checkState(r4)     // Catch:{ RuntimeException -> 0x0039 }
            X.0V7 r0 = r3.mThreadPriority     // Catch:{ RuntimeException -> 0x0039 }
            if (r0 == 0) goto L_0x0035
            int r5 = r0.mAndroidThreadPriority     // Catch:{ RuntimeException -> 0x0039 }
        L_0x0035:
            android.os.Process.setThreadPriority(r5)     // Catch:{ RuntimeException -> 0x0039 }
            goto L_0x003f
        L_0x0039:
            r1 = move-exception
            java.lang.String r0 = "Unable to set thread priority"
            X.AnonymousClass57O.A00(r1, r0)
        L_0x003f:
            X.2S5 r5 = r8.A04
            if (r5 == 0) goto L_0x0082
            long r6 = android.os.SystemClock.uptimeMillis()
            java.util.concurrent.ConcurrentHashMap r1 = r5.A02
            java.lang.Long r0 = java.lang.Long.valueOf(r6)
            r1.put(r10, r0)
            android.os.Handler r0 = r5.A04
            if (r0 != 0) goto L_0x0070
            monitor-enter(r5)
            android.os.Handler r0 = r5.A04     // Catch:{ all -> 0x006d }
            if (r0 != 0) goto L_0x006b
            android.os.HandlerThread r0 = r5.A01     // Catch:{ all -> 0x006d }
            r0.start()     // Catch:{ all -> 0x006d }
            X.2dn r1 = new X.2dn     // Catch:{ all -> 0x006d }
            android.os.HandlerThread r0 = r5.A01     // Catch:{ all -> 0x006d }
            android.os.Looper r0 = r0.getLooper()     // Catch:{ all -> 0x006d }
            r1.<init>(r5, r0)     // Catch:{ all -> 0x006d }
            r5.A04 = r1     // Catch:{ all -> 0x006d }
        L_0x006b:
            monitor-exit(r5)     // Catch:{ all -> 0x006d }
            goto L_0x0070
        L_0x006d:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x006d }
            throw r0
        L_0x0070:
            java.util.concurrent.atomic.AtomicBoolean r1 = r5.A03
            r4 = 1
            r0 = 0
            boolean r0 = r1.compareAndSet(r0, r4)
            if (r0 == 0) goto L_0x0082
            android.os.Handler r2 = r5.A04
            int r0 = r5.A00
            long r0 = (long) r0
            r2.sendEmptyMessageDelayed(r4, r0)
        L_0x0082:
            X.2jx r2 = r10.ATQ()
            if (r2 == 0) goto L_0x0096
            long r0 = X.AnonymousClass0Y1.A00()
            r2.A01 = r0
            long r0 = android.os.SystemClock.currentThreadTimeMillis()
            r2.A00 = r0
            r2.A02 = r3
        L_0x0096:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04590Vi.beforeExecute(java.lang.Thread, java.lang.Runnable):void");
    }
}
