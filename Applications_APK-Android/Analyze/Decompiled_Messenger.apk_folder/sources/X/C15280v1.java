package X;

import android.view.View;

/* renamed from: X.0v1  reason: invalid class name and case insensitive filesystem */
public final class C15280v1 implements AnonymousClass1M8 {
    public String getName() {
        return "scale";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (r1 == false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float Ab1(X.C17730zN r5) {
        /*
            r4 = this;
            X.1jd r3 = r5.A0A
            if (r3 == 0) goto L_0x0010
            int r2 = r3.A0A
            r0 = 524288(0x80000, float:7.34684E-40)
            r2 = r2 & r0
            r1 = 0
            if (r2 == 0) goto L_0x000d
            r1 = 1
        L_0x000d:
            r0 = 1
            if (r1 != 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            if (r0 == 0) goto L_0x0018
            if (r3 == 0) goto L_0x0018
            float r0 = r3.A04
            return r0
        L_0x0018:
            r0 = 1065353216(0x3f800000, float:1.0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15280v1.Ab1(X.0zN):float");
    }

    public float Ab2(Object obj) {
        View A02 = C17700zK.A02(obj, this);
        float scaleX = A02.getScaleX();
        if (scaleX == A02.getScaleY()) {
            return scaleX;
        }
        throw new RuntimeException("Tried to get scale of view, but scaleX and scaleY are different");
    }

    public void C3Y(Object obj) {
        View A02 = C17700zK.A02(obj, this);
        A02.setScaleX(1.0f);
        A02.setScaleY(1.0f);
    }

    public void C5f(Object obj, float f) {
        View A02 = C17700zK.A02(obj, this);
        A02.setScaleX(f);
        A02.setScaleY(f);
    }
}
