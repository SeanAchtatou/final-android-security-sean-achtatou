package X;

import com.google.common.collect.AbstractMapBasedMultimap;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.118  reason: invalid class name */
public final class AnonymousClass118 extends AbstractMapBasedMultimap<K, V>.WrappedList implements RandomAccess {
    public AnonymousClass118(AbstractMapBasedMultimap abstractMapBasedMultimap, Object obj, List list, AnonymousClass11B r4) {
        super(abstractMapBasedMultimap, obj, list, r4);
    }
}
