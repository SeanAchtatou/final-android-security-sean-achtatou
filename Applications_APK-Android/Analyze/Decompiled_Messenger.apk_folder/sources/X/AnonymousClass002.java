package X;

import android.app.Application;
import android.content.res.Resources;
import android.os.SystemClock;
import android.util.Log;
import com.facebook.base.app.ApplicationLike;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.002  reason: invalid class name */
public abstract class AnonymousClass002 extends Application implements AnonymousClass003 {
    public static boolean A01;
    public ApplicationLike A00;

    public abstract ApplicationLike A05();

    public synchronized void A07() {
        if (this.A00 == null) {
            this.A00 = A05();
            AnonymousClass08R.A00.open();
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:42|(2:44|45)|46|47) */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00e3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e4, code lost:
        if (r6 != null) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x010d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x010e, code lost:
        r1 = r2;
        r0 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0111, code lost:
        if (r0 == -1) goto L_0x0113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0115, code lost:
        r0 = X.AnonymousClass08T.A00(r1);
        r1 = r1.getCause();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x011e, code lost:
        if (r0 != -1) goto L_0x0120;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0120, code lost:
        r1 = X.AnonymousClass08T.A01(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x012a, code lost:
        if ("ENOSPC".equals(r1) != false) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x012c, code lost:
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x012d, code lost:
        if (r1 != 1) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x012f, code lost:
        A09(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0132, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0139, code lost:
        if ("ENOENT".equals(r1) != false) goto L_0x013b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x013b, code lost:
        r1 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x013d, code lost:
        r0 = "EROFS".equals(r1);
        r1 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0144, code lost:
        if (r0 == false) goto L_0x0146;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0146, code lost:
        r1 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0149, code lost:
        if (r1 == 2) goto L_0x014b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x014b, code lost:
        A0A(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x014e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0150, code lost:
        if (r1 == 3) goto L_0x0152;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0159, code lost:
        throw new java.lang.RuntimeException("readOnlyFileSystem", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x015a, code lost:
        r3 = e;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:46:0x00e9 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x00f2 */
    /* JADX WARNING: Removed duplicated region for block: B:138:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0148  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(int r12) {
        /*
            r11 = this;
            r4 = 1
            r0 = 0
            X.025[] r8 = new X.AnonymousClass025[]{r0}
            r3 = 0
            r5 = 1
            X.00c r7 = r11.A06()     // Catch:{ 08U -> 0x015a, RuntimeException -> 0x010d }
            java.lang.Class<X.01n> r10 = X.C002301n.class
            monitor-enter(r10)     // Catch:{ 08U -> 0x015a, RuntimeException -> 0x010d }
            boolean r0 = X.C002301n.A02     // Catch:{ all -> 0x010a }
            if (r0 != 0) goto L_0x0108
            android.os.StrictMode$ThreadPolicy r9 = android.os.StrictMode.allowThreadDiskWrites()     // Catch:{ all -> 0x010a }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x00f3 }
            java.lang.String r0 = "/data/local/tmp/ctscan_test_running"
            r1.<init>(r0)     // Catch:{ IOException -> 0x00f3 }
            boolean r0 = r1.exists()     // Catch:{ IOException -> 0x00f3 }
            if (r0 == 0) goto L_0x0026
            r12 = r12 & -3
        L_0x0026:
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x00f3 }
            android.content.pm.ApplicationInfo r0 = r11.getApplicationInfo()     // Catch:{ IOException -> 0x00f3 }
            java.lang.String r1 = r0.dataDir     // Catch:{ IOException -> 0x00f3 }
            java.lang.String r0 = "/app_libs"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ IOException -> 0x00f3 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00f3 }
            X.C002401o.A00(r2)     // Catch:{ IOException -> 0x00f3 }
            X.AnonymousClass01q.A06(r11, r12)     // Catch:{ IOException -> 0x00f3 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ IOException -> 0x00f3 }
            r0.<init>()     // Catch:{ IOException -> 0x00f3 }
            X.C002301n.A01 = r0     // Catch:{ IOException -> 0x00f3 }
            r2 = 0
        L_0x0045:
            if (r2 >= r4) goto L_0x005a
            r0 = r8[r2]     // Catch:{ IOException -> 0x00f3 }
            if (r0 == 0) goto L_0x0057
            X.AnonymousClass01q.A07(r0)     // Catch:{ IOException -> 0x00f3 }
            java.util.ArrayList r1 = X.C002301n.A01     // Catch:{ IOException -> 0x00f3 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00f3 }
            r1.add(r0)     // Catch:{ IOException -> 0x00f3 }
        L_0x0057:
            int r2 = r2 + 1
            goto L_0x0045
        L_0x005a:
            X.01z r0 = new X.01z     // Catch:{ IOException -> 0x00f3 }
            java.lang.String r1 = "lib-assets"
            r0.<init>(r11, r1)     // Catch:{ IOException -> 0x00f3 }
            X.AnonymousClass01q.A07(r0)     // Catch:{ IOException -> 0x00f3 }
            java.util.ArrayList r0 = X.C002301n.A01     // Catch:{ IOException -> 0x00f3 }
            r0.add(r1)     // Catch:{ IOException -> 0x00f3 }
            r0 = 34603021(0x210000d, float:1.0579464E-37)
            X.02K r8 = r7.AOr(r0)     // Catch:{ IOException -> 0x00f3 }
            java.lang.Integer r2 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x00ea }
            X.02N r0 = new X.02N     // Catch:{ all -> 0x00ea }
            r0.<init>(r11, r2)     // Catch:{ all -> 0x00ea }
            X.AnonymousClass01q.A07(r0)     // Catch:{ all -> 0x00ea }
            java.util.ArrayList r1 = X.C002301n.A01     // Catch:{ all -> 0x00ea }
            java.lang.String r0 = X.AnonymousClass02J.A00(r2)     // Catch:{ all -> 0x00ea }
            r1.add(r0)     // Catch:{ all -> 0x00ea }
            java.lang.Integer r2 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x00ea }
            X.02N r0 = new X.02N     // Catch:{ all -> 0x00ea }
            r0.<init>(r11, r2)     // Catch:{ all -> 0x00ea }
            X.AnonymousClass01q.A07(r0)     // Catch:{ all -> 0x00ea }
            java.util.ArrayList r1 = X.C002301n.A01     // Catch:{ all -> 0x00ea }
            java.lang.String r0 = X.AnonymousClass02J.A00(r2)     // Catch:{ all -> 0x00ea }
            r1.add(r0)     // Catch:{ all -> 0x00ea }
            r0 = 34603019(0x210000b, float:1.0579461E-37)
            X.02K r6 = r7.AOr(r0)     // Catch:{ all -> 0x00ea }
            java.lang.Integer r2 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x00e1 }
            X.02N r0 = new X.02N     // Catch:{ all -> 0x00e1 }
            r0.<init>(r11, r2)     // Catch:{ all -> 0x00e1 }
            X.AnonymousClass01q.A07(r0)     // Catch:{ all -> 0x00e1 }
            if (r6 == 0) goto L_0x00ac
            r6.close()     // Catch:{ all -> 0x00ea }
        L_0x00ac:
            java.util.ArrayList r1 = X.C002301n.A01     // Catch:{ all -> 0x00ea }
            java.lang.String r0 = X.AnonymousClass02J.A00(r2)     // Catch:{ all -> 0x00ea }
            r1.add(r0)     // Catch:{ all -> 0x00ea }
            r0 = 34603020(0x210000c, float:1.0579463E-37)
            X.02K r6 = r7.AOr(r0)     // Catch:{ all -> 0x00ea }
            java.lang.Integer r2 = X.AnonymousClass07B.A0N     // Catch:{ all -> 0x00e1 }
            X.02N r0 = new X.02N     // Catch:{ all -> 0x00e1 }
            r0.<init>(r11, r2)     // Catch:{ all -> 0x00e1 }
            X.AnonymousClass01q.A07(r0)     // Catch:{ all -> 0x00e1 }
            if (r6 == 0) goto L_0x00cb
            r6.close()     // Catch:{ all -> 0x00ea }
        L_0x00cb:
            java.util.ArrayList r1 = X.C002301n.A01     // Catch:{ all -> 0x00ea }
            java.lang.String r0 = X.AnonymousClass02J.A00(r2)     // Catch:{ all -> 0x00ea }
            r1.add(r0)     // Catch:{ all -> 0x00ea }
            if (r8 == 0) goto L_0x00d9
            r8.close()     // Catch:{ IOException -> 0x00f3 }
        L_0x00d9:
            X.C002301n.A00 = r11     // Catch:{ IOException -> 0x00f3 }
            android.os.StrictMode.setThreadPolicy(r9)     // Catch:{ all -> 0x010a }
            X.C002301n.A02 = r4     // Catch:{ all -> 0x010a }
            goto L_0x0108
        L_0x00e1:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00e3 }
        L_0x00e3:
            r0 = move-exception
            if (r6 == 0) goto L_0x00e9
            r6.close()     // Catch:{ all -> 0x00e9 }
        L_0x00e9:
            throw r0     // Catch:{ all -> 0x00ea }
        L_0x00ea:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ec }
        L_0x00ec:
            r0 = move-exception
            if (r8 == 0) goto L_0x00f2
            r8.close()     // Catch:{ all -> 0x00f2 }
        L_0x00f2:
            throw r0     // Catch:{ IOException -> 0x00f3 }
        L_0x00f3:
            r2 = move-exception
            java.lang.String r1 = "FbSoLoader"
            java.lang.String r0 = "IOException during init"
            android.util.Log.e(r1, r0, r2)     // Catch:{ all -> 0x0101 }
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x0101 }
            r0.<init>(r2)     // Catch:{ all -> 0x0101 }
            throw r0     // Catch:{ all -> 0x0101 }
        L_0x0101:
            r0 = move-exception
            android.os.StrictMode.setThreadPolicy(r9)     // Catch:{ all -> 0x010a }
            X.C002301n.A02 = r4     // Catch:{ all -> 0x010a }
            throw r0     // Catch:{ all -> 0x010a }
        L_0x0108:
            monitor-exit(r10)     // Catch:{ 08U -> 0x015a, RuntimeException -> 0x010d }
            goto L_0x015b
        L_0x010a:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ 08U -> 0x015a, RuntimeException -> 0x010d }
            throw r0     // Catch:{ 08U -> 0x015a, RuntimeException -> 0x010d }
        L_0x010d:
            r2 = move-exception
            r4 = -1
            r1 = r2
            r0 = -1
        L_0x0111:
            if (r0 != r4) goto L_0x011e
            if (r1 == 0) goto L_0x011e
            int r0 = X.AnonymousClass08T.A00(r1)
            java.lang.Throwable r1 = r1.getCause()
            goto L_0x0111
        L_0x011e:
            if (r0 == r4) goto L_0x0146
            java.lang.String r1 = X.AnonymousClass08T.A01(r0)
            java.lang.String r0 = "ENOSPC"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0133
            r1 = 1
        L_0x012d:
            if (r1 != r5) goto L_0x0148
            r11.A09(r2)
            return
        L_0x0133:
            java.lang.String r0 = "ENOENT"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x013d
            r1 = 2
            goto L_0x012d
        L_0x013d:
            java.lang.String r0 = "EROFS"
            boolean r0 = r0.equals(r1)
            r1 = 3
            if (r0 != 0) goto L_0x012d
        L_0x0146:
            r1 = -1
            goto L_0x012d
        L_0x0148:
            r0 = 2
            if (r1 != r0) goto L_0x014f
            r11.A0A(r2)
            return
        L_0x014f:
            r0 = 3
            if (r1 != r0) goto L_0x015b
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "readOnlyFileSystem"
            r1.<init>(r0, r2)
            throw r1
        L_0x015a:
            r3 = move-exception
        L_0x015b:
            if (r3 != 0) goto L_0x01c9
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = X.AnonymousClass01q.A06
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.lock()
            X.025[] r0 = X.AnonymousClass01q.A04     // Catch:{ all -> 0x01b2 }
            if (r0 == 0) goto L_0x01bd
            java.lang.String[] r7 = X.C002401o.A02()     // Catch:{ all -> 0x01b2 }
            r6 = 0
        L_0x016f:
            X.025[] r1 = X.AnonymousClass01q.A04     // Catch:{ all -> 0x01b2 }
            int r0 = r1.length     // Catch:{ all -> 0x01b2 }
            if (r6 >= r0) goto L_0x01a7
            r0 = r1[r6]     // Catch:{ all -> 0x01b2 }
            java.lang.String[] r5 = r0.A0B()     // Catch:{ all -> 0x01b2 }
            r4 = 0
        L_0x017b:
            int r0 = r5.length     // Catch:{ all -> 0x01b2 }
            if (r4 >= r0) goto L_0x0196
            r2 = 0
            r1 = 0
        L_0x0180:
            int r0 = r7.length     // Catch:{ all -> 0x01b2 }
            if (r2 >= r0) goto L_0x0190
            if (r1 != 0) goto L_0x0190
            r1 = r5[r4]     // Catch:{ all -> 0x01b2 }
            r0 = r7[r2]     // Catch:{ all -> 0x01b2 }
            boolean r1 = r1.equals(r0)     // Catch:{ all -> 0x01b2 }
            int r2 = r2 + 1
            goto L_0x0180
        L_0x0190:
            if (r1 != 0) goto L_0x0193
            goto L_0x0199
        L_0x0193:
            int r4 = r4 + 1
            goto L_0x017b
        L_0x0196:
            int r6 = r6 + 1
            goto L_0x016f
        L_0x0199:
            java.lang.String r2 = "SoLoader"
            java.lang.String r1 = "abi not supported: "
            r0 = r5[r4]     // Catch:{ all -> 0x01b2 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x01b2 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x01b2 }
            goto L_0x01bd
        L_0x01a7:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = X.AnonymousClass01q.A06
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            r0 = 1
            goto L_0x01c7
        L_0x01b2:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = X.AnonymousClass01q.A06
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            throw r1
        L_0x01bd:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = X.AnonymousClass01q.A06
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            r0 = 0
        L_0x01c7:
            if (r0 != 0) goto L_0x01cc
        L_0x01c9:
            r11.A0B(r3)
        L_0x01cc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass002.A08(int):void");
    }

    public void A0D() {
    }

    public boolean A0E() {
        return true;
    }

    public boolean A0F() {
        return false;
    }

    public void A09(Throwable th) {
        throw new RuntimeException("diskFullError", th);
    }

    public void A0A(Throwable th) {
        throw new RuntimeException("fileNotFoundError", th);
    }

    public void A0B(Throwable th) {
        throw new RuntimeException("unsupportedDsoAbiError", th);
    }

    public void A0C() {
        this.A00.A02();
    }

    public Resources getResources() {
        ApplicationLike applicationLike = this.A00;
        if (applicationLike instanceof AnonymousClass04W) {
            if (this instanceof AnonymousClass005) {
                Resources Awo = ((AnonymousClass04W) applicationLike).Awo();
                if (Awo != null) {
                    return Awo;
                }
            } else {
                throw new IllegalStateException(AnonymousClass08S.A0J(getClass().getName(), " illegally implements HasOverridingResources without HasBaseResourcesAccess."));
            }
        }
        return super.getResources();
    }

    public void onCreate() {
        int i = AnonymousClass00n.A04;
        int writeStandardEntry = Logger.writeStandardEntry(i, 6, 24, 0, 0, 1134386476, 0, 0);
        super.onCreate();
        A07();
        A0C();
        Logger.writeStandardEntry(i, 6, 25, 0, 0, -749386219, writeStandardEntry, 0);
    }

    public void registerActivityLifecycleCallbacks(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        ApplicationLike applicationLike = this.A00;
        if (applicationLike instanceof AnonymousClass04Z) {
            ((AnonymousClass04Z) applicationLike).registerActivityLifecycleCallbacks(this, activityLifecycleCallbacks);
        } else {
            super.registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
        }
    }

    public void unregisterActivityLifecycleCallbacks(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        ApplicationLike applicationLike = this.A00;
        if (applicationLike instanceof AnonymousClass04Z) {
            ((AnonymousClass04Z) applicationLike).unregisterActivityLifecycleCallbacks(this, activityLifecycleCallbacks);
        } else {
            super.unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
        }
    }

    static {
        SystemClock.uptimeMillis();
    }

    public AnonymousClass002() {
        synchronized (AnonymousClass002.class) {
            if (A01) {
                Log.e("DelegatingApplication", "Multiple instances of the Application object were created.");
                System.exit(222);
            }
            A01 = true;
        }
    }

    public C000000c A06() {
        return AnonymousClass08Q.A00;
    }

    public final Resources AeF() {
        return super.getResources();
    }

    public Object Aq4() {
        A07();
        return this.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0075, code lost:
        if (r2.exists() != false) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0026, code lost:
        if (X.AnonymousClass00I.A00("debug.fbsystrace.tags", 0) != 0) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        if (r0 != false) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void attachBaseContext(android.content.Context r7) {
        /*
            r6 = this;
            super.attachBaseContext(r7)
            android.app.ActivityThread r0 = android.app.ActivityThread.currentActivityThread()
            X.AnonymousClass00d.A00 = r0
            android.app.Application r0 = X.AnonymousClass00e.A00
            if (r0 != 0) goto L_0x00c7
            X.AnonymousClass00e.A00 = r6
            boolean r0 = r6.A0E()
            if (r0 != 0) goto L_0x002b
            boolean r0 = X.AnonymousClass00H.A01()
            if (r0 != 0) goto L_0x0028
            java.lang.String r2 = "debug.fbsystrace.tags"
            r0 = 0
            long r4 = X.AnonymousClass00I.A00(r2, r0)
            int r1 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            r0 = 0
            if (r1 == 0) goto L_0x0029
        L_0x0028:
            r0 = 1
        L_0x0029:
            if (r0 == 0) goto L_0x002f
        L_0x002b:
            r0 = 0
            r6.A08(r0)
        L_0x002f:
            boolean r0 = r6.A0F()
            if (r0 == 0) goto L_0x00bb
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 27
            if (r1 > r0) goto L_0x0042
            if (r1 != r0) goto L_0x0080
            int r1 = android.os.Build.VERSION.PREVIEW_SDK_INT
            r0 = 2
            if (r1 < r0) goto L_0x0080
        L_0x0042:
            r0 = 1
        L_0x0043:
            r4 = 0
            if (r0 == 0) goto L_0x0078
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "/data/local/tmp/non_sdk_strict_mode.fb"
            r1.<init>(r0)
            boolean r0 = r1.exists()
            r3 = 1
            if (r0 != 0) goto L_0x0077
            android.content.pm.ApplicationInfo r0 = r6.getApplicationInfo()
            if (r0 == 0) goto L_0x007e
            android.content.pm.ApplicationInfo r0 = r6.getApplicationInfo()
            java.lang.String r0 = r0.dataDir
            if (r0 == 0) goto L_0x007e
            java.io.File r2 = new java.io.File
            android.content.pm.ApplicationInfo r0 = r6.getApplicationInfo()
            java.lang.String r1 = r0.dataDir
            java.lang.String r0 = "non_sdk_strict_mode.fb"
            r2.<init>(r1, r0)
        L_0x006f:
            if (r2 == 0) goto L_0x0078
            boolean r0 = r2.exists()
            if (r0 == 0) goto L_0x0078
        L_0x0077:
            r4 = r3
        L_0x0078:
            if (r4 == 0) goto L_0x00aa
            java.lang.Class<X.00O> r4 = X.AnonymousClass00O.class
            monitor-enter(r4)
            goto L_0x0082
        L_0x007e:
            r2 = 0
            goto L_0x006f
        L_0x0080:
            r0 = 0
            goto L_0x0043
        L_0x0082:
            boolean r0 = X.AnonymousClass00O.A00     // Catch:{ all -> 0x00a6 }
            if (r0 != 0) goto L_0x00a9
            android.os.StrictMode$VmPolicy$Builder r3 = new android.os.StrictMode$VmPolicy$Builder     // Catch:{ all -> 0x00a6 }
            r3.<init>()     // Catch:{ all -> 0x00a6 }
            android.os.StrictMode$VmPolicy$Builder r2 = r3.detectNonSdkApiUsage()     // Catch:{ all -> 0x00a6 }
            java.util.concurrent.ExecutorService r1 = java.util.concurrent.Executors.newSingleThreadExecutor()     // Catch:{ all -> 0x00a6 }
            X.0KV r0 = new X.0KV     // Catch:{ all -> 0x00a6 }
            r0.<init>()     // Catch:{ all -> 0x00a6 }
            r2.penaltyListener(r1, r0)     // Catch:{ all -> 0x00a6 }
            android.os.StrictMode$VmPolicy r0 = r3.build()     // Catch:{ all -> 0x00a6 }
            android.os.StrictMode.setVmPolicy(r0)     // Catch:{ all -> 0x00a6 }
            r0 = 1
            X.AnonymousClass00O.A00 = r0     // Catch:{ all -> 0x00a6 }
            goto L_0x00a9
        L_0x00a6:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x00a9:
            monitor-exit(r4)
        L_0x00aa:
            java.lang.String r2 = "DelegatingApplication"
            java.lang.String r0 = "android.os.AsyncTask"
            java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x00b2 }
            goto L_0x00b8
        L_0x00b2:
            r1 = move-exception
            java.lang.String r0 = "Exception trying to initialize AsyncTask"
            android.util.Log.w(r2, r0, r1)
        L_0x00b8:
            X.AnonymousClass00M.A00()
        L_0x00bb:
            r6.A0D()
            android.os.ConditionVariable r0 = X.AnonymousClass08R.A00
            r0.close()
            r6.A07()
            return
        L_0x00c7:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "ApplicationHolder#set previously called"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass002.attachBaseContext(android.content.Context):void");
    }

    public void onLowMemory() {
        super.onLowMemory();
        ApplicationLike applicationLike = this.A00;
        if (applicationLike != null) {
            applicationLike.A01();
        }
    }

    public void onTrimMemory(int i) {
        super.onTrimMemory(i);
        ApplicationLike applicationLike = this.A00;
        if (applicationLike != null) {
            applicationLike.A03(i);
        }
    }
}
