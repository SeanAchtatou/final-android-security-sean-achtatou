package X;

import android.util.SparseArray;

/* renamed from: X.0FT  reason: invalid class name */
public final class AnonymousClass0FT implements AnonymousClass0FU {
    public void BXr(Object obj) {
        AnonymousClass0FR r2 = AnonymousClass0FQ.A00;
        synchronized (r2) {
            if (r2.A02) {
                AnonymousClass0FR.A02(obj);
                int identityHashCode = System.identityHashCode(obj);
                r2.A03.delete(identityHashCode);
                r2.A04.delete(identityHashCode);
            }
        }
    }

    public void Bgz(Object obj) {
        AnonymousClass0FR r2 = AnonymousClass0FQ.A00;
        synchronized (r2) {
            if (r2.A02) {
                AnonymousClass0FR.A02(obj);
                AnonymousClass0FR.A01(System.identityHashCode(obj), r2.A03);
            }
        }
    }

    public void BlQ(Object obj) {
        boolean z;
        AnonymousClass0FR r5 = AnonymousClass0FQ.A00;
        synchronized (r5) {
            if (r5.A02) {
                AnonymousClass0FR.A02(obj);
                int identityHashCode = System.identityHashCode(obj);
                SparseArray sparseArray = r5.A03;
                synchronized (AnonymousClass0FR.class) {
                    Long l = (Long) sparseArray.get(identityHashCode);
                    z = false;
                    if (l != null) {
                        z = true;
                    }
                }
                if (z) {
                    r5.A00 += AnonymousClass0FR.A00(identityHashCode, r5.A03);
                }
            }
        }
    }

    public void Bpi(Object obj) {
        AnonymousClass0FR r2 = AnonymousClass0FQ.A00;
        synchronized (r2) {
            if (r2.A02) {
                AnonymousClass0FR.A02(obj);
                AnonymousClass0FR.A01(System.identityHashCode(obj), r2.A04);
            }
        }
    }

    public void Bq9(Object obj) {
        AnonymousClass0FR r4 = AnonymousClass0FQ.A00;
        synchronized (r4) {
            if (r4.A02) {
                AnonymousClass0FR.A02(obj);
                r4.A01 += AnonymousClass0FR.A00(System.identityHashCode(obj), r4.A04);
            }
        }
    }
}
