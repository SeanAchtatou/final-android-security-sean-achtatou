package X;

import android.os.Build;
import android.os.Process;
import com.facebook.acra.LogCatCollector;
import com.facebook.acra.constants.ReportField;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;

/* renamed from: X.1hj  reason: invalid class name and case insensitive filesystem */
public final class C30241hj {
    private static boolean A00;
    public static final AnonymousClass18E A01 = new AnonymousClass18E();
    private static final Object A02 = new Object();

    public static SecureRandom A00() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17 && i <= 18) {
            synchronized (A02) {
                if (!A00) {
                    if (!"robolectric".equals(Build.FINGERPRINT)) {
                        try {
                            Class.forName("org.apache.harmony.xnet.provider.jsse.NativeCrypto").getMethod("RAND_seed", byte[].class).invoke(null, A01());
                            int intValue = ((Integer) Class.forName("org.apache.harmony.xnet.provider.jsse.NativeCrypto").getMethod("RAND_load_file", String.class, Long.TYPE).invoke(null, "/dev/urandom", 1024)).intValue();
                            if (intValue != 1024) {
                                throw new IOException(AnonymousClass08S.A09("Unexpected number of bytes read from Linux PRNG: ", intValue));
                            }
                        } catch (Exception e) {
                            throw new SecurityException("Failed to seed OpenSSL PRNG", e);
                        }
                    }
                    A00 = true;
                }
            }
        }
        if (i <= 18) {
            return new C56222pZ();
        }
        return new SecureRandom();
    }

    public static byte[] A01() {
        String str;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeLong(System.currentTimeMillis());
            dataOutputStream.writeLong(System.nanoTime());
            dataOutputStream.writeInt(Process.myPid());
            dataOutputStream.writeInt(Process.myUid());
            StringBuilder sb = new StringBuilder();
            String str2 = Build.FINGERPRINT;
            if (str2 != null) {
                sb.append(str2);
            }
            try {
                str = (String) Build.class.getField(ReportField.SERIAL).get(null);
            } catch (Exception unused) {
                str = null;
            }
            if (str != null) {
                sb.append(str);
            }
            dataOutputStream.write(sb.toString().getBytes(LogCatCollector.UTF_8_ENCODING));
            dataOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (UnsupportedEncodingException unused2) {
            throw new RuntimeException("UTF-8 encoding not supported");
        } catch (IOException e) {
            throw new SecurityException("Failed to generate seed", e);
        }
    }
}
