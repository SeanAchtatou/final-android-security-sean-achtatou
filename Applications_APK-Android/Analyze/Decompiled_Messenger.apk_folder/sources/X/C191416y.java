package X;

import java.util.HashSet;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.16y  reason: invalid class name and case insensitive filesystem */
public final class C191416y {
    private static volatile C191416y A04;
    public final C10760ko A00;
    public final C191516z A01;
    public final Set A02;
    public final Set A03;

    public static final C191416y A00(AnonymousClass1XY r6) {
        if (A04 == null) {
            synchronized (C191416y.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A04 = new C191416y(C10760ko.A01(applicationInjector), C25481Zu.A00(applicationInjector), C191516z.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private C191416y(C10760ko r4, AnonymousClass0XN r5, C191516z r6) {
        HashSet hashSet = new HashSet();
        this.A03 = hashSet;
        HashSet hashSet2 = new HashSet();
        this.A02 = hashSet2;
        this.A00 = r4;
        this.A01 = r6;
        C08770fv r0 = r6.A05;
        r0.A09 = hashSet;
        r0.A08 = hashSet2;
        if (!r5.A0I()) {
            this.A00.A02(false);
        }
    }
}
