package X;

import com.google.common.base.Preconditions;
import java.util.PriorityQueue;

/* renamed from: X.0Vm  reason: invalid class name and case insensitive filesystem */
public final class C04630Vm {
    public int A00;
    public final PriorityQueue A01;

    public void A00(C07820eD r4) {
        boolean z = true;
        this.A00++;
        PriorityQueue priorityQueue = this.A01;
        if (priorityQueue != null) {
            priorityQueue.offer(r4);
            if (this.A00 != this.A01.size()) {
                z = false;
            }
            Preconditions.checkState(z);
        }
    }

    public void A01(C07820eD r4) {
        boolean z = true;
        this.A00--;
        PriorityQueue priorityQueue = this.A01;
        if (priorityQueue != null) {
            Preconditions.checkState(priorityQueue.remove(r4));
            if (this.A00 != this.A01.size()) {
                z = false;
            }
            Preconditions.checkState(z);
        }
    }

    public C04630Vm(int i) {
        if (i < Integer.MAX_VALUE) {
            this.A01 = new PriorityQueue(i, C04530Vb.A01);
        }
    }
}
