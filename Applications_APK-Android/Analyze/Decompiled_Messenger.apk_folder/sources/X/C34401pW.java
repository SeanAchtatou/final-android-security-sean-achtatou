package X;

import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1pW  reason: invalid class name and case insensitive filesystem */
public final class C34401pW extends AtomicReference {
    private static volatile C34401pW A00;

    public static final C34401pW A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C34401pW.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C34401pW();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
