package X;

import io.card.payment.BuildConfig;

/* renamed from: X.0is  reason: invalid class name and case insensitive filesystem */
public final class C09900is {
    public static final C09900is A03 = new C09900is(BuildConfig.FLAVOR, false, 0);
    public final long A00;
    public final String A01;
    public final boolean A02;

    public C09900is(String str, boolean z, long j) {
        this.A01 = str;
        this.A02 = z;
        this.A00 = j;
    }
}
