package X;

import android.graphics.Rect;
import android.view.View;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1jm  reason: invalid class name and case insensitive filesystem */
public final class C31491jm {
    public static final Comparator A06 = new EVK();
    public static final Comparator A07 = new EVJ();
    public int A00;
    public int A01;
    public final ArrayList A02 = new ArrayList();
    public final ArrayList A03 = new ArrayList();
    public final Map A04 = new HashMap();
    private final View A05;

    private static void A00(Map map, Map map2) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry key : map.entrySet()) {
            String str = (String) key.getKey();
            if (!map2.containsKey(str)) {
                ((C31481jl) map.get(str)).BYG();
                arrayList.add(str);
            }
        }
        for (int i = 0; i < arrayList.size(); i++) {
            map.remove(arrayList.get(i));
        }
        for (Map.Entry key2 : map2.entrySet()) {
            String str2 = (String) key2.getKey();
            if (!map.containsKey(str2)) {
                ((C31481jl) map2.get(str2)).BXg();
            }
            map.put(str2, map2.get(str2));
        }
    }

    private static boolean A01(Rect rect, C31481jl r3) {
        int i = rect.bottom;
        float f = (float) i;
        if (f < r3.AlT()) {
            return true;
        }
        if (f != r3.AlT() || r3.Af0().top < i) {
            return false;
        }
        return true;
    }

    private static boolean A02(Rect rect, C31481jl r3) {
        int i = rect.top;
        float f = (float) i;
        if (f < r3.AlS()) {
            return true;
        }
        if (f != r3.AlS() || r3.Af0().bottom <= i) {
            return false;
        }
        return true;
    }

    public void A03() {
        for (String str : this.A04.keySet()) {
            ((C31481jl) this.A04.get(str)).BYG();
        }
        this.A04.clear();
        this.A03.clear();
        this.A02.clear();
    }

    public void A04(boolean z, List list, List list2, Rect rect, Rect rect2) {
        if (rect == null) {
            return;
        }
        if (!z && !this.A03.isEmpty() && !this.A02.isEmpty()) {
            int size = this.A03.size();
            if (rect.top >= 0 || rect2.top >= 0) {
                while (true) {
                    int i = this.A00;
                    if (i < size && (!A02(rect, (C31481jl) this.A02.get(i)))) {
                        ((C31481jl) this.A02.get(this.A00)).BYG();
                        this.A00++;
                    }
                }
                while (true) {
                    int i2 = this.A00;
                    if (i2 <= 0 || !A02(rect, (C31481jl) this.A02.get(i2 - 1))) {
                        break;
                    }
                    int i3 = this.A00 - 1;
                    this.A00 = i3;
                    ((C31481jl) this.A02.get(i3)).BXg();
                }
            }
            int height = this.A05.getHeight();
            if (rect.bottom <= height || rect2.bottom <= height) {
                while (true) {
                    int i4 = this.A01;
                    if (i4 < size && (!A01(rect, (C31481jl) this.A03.get(i4)))) {
                        ((C31481jl) this.A03.get(this.A01)).BXg();
                        this.A01++;
                    }
                }
                while (true) {
                    int i5 = this.A01;
                    if (i5 > 0 && A01(rect, (C31481jl) this.A03.get(i5 - 1))) {
                        int i6 = this.A01 - 1;
                        this.A01 = i6;
                        ((C31481jl) this.A03.get(i6)).BYG();
                    } else {
                        return;
                    }
                }
            }
        } else if (rect != null) {
            this.A03.clear();
            this.A02.clear();
            int i7 = 0;
            if (list == null || list2 == null) {
                A00(this.A04, new HashMap(0));
                return;
            }
            this.A03.addAll(list);
            this.A02.addAll(list2);
            HashMap hashMap = new HashMap();
            int size2 = this.A03.size();
            this.A01 = size2;
            int i8 = 0;
            while (true) {
                if (i8 >= size2) {
                    break;
                } else if (A01(rect, (C31481jl) this.A03.get(i8))) {
                    this.A01 = i8;
                    break;
                } else {
                    hashMap.put(((C31481jl) this.A03.get(i8)).getId(), this.A03.get(i8));
                    i8++;
                }
            }
            this.A00 = size2;
            while (true) {
                if (i7 >= size2) {
                    break;
                } else if (A02(rect, (C31481jl) this.A02.get(i7))) {
                    this.A00 = i7;
                    break;
                } else {
                    hashMap.remove(((C31481jl) this.A02.get(i7)).getId());
                    i7++;
                }
            }
            while (true) {
                int i9 = this.A00;
                if (i9 >= size2 - 1 || ((C31481jl) this.A02.get(i9)).AlS() != ((C31481jl) this.A02.get(this.A00 + 1)).AlS()) {
                    A00(this.A04, hashMap);
                } else {
                    this.A00++;
                }
            }
            A00(this.A04, hashMap);
        }
    }

    public C31491jm(View view) {
        this.A05 = view;
    }
}
