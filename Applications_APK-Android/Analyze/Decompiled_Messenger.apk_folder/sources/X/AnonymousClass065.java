package X;

import android.content.Context;
import android.content.ContextWrapper;

/* renamed from: X.065  reason: invalid class name */
public final class AnonymousClass065 {
    public static Object A00(Context context, Class cls) {
        Context baseContext;
        while (!cls.isInstance(context)) {
            if (!(context instanceof ContextWrapper) || context == (baseContext = ((ContextWrapper) context).getBaseContext())) {
                return null;
            }
            context = baseContext;
        }
        return context;
    }
}
