package X;

import com.facebook.proxygen.TraceFieldType;
import java.util.Arrays;
import java.util.Random;

/* renamed from: X.1FM  reason: invalid class name */
public final class AnonymousClass1FM {
    public static final Random A09 = new Random();
    public int A00 = 0;
    public int A01 = -1;
    public long A02;
    public long A03;
    public C06230bA A04;
    public String A05;
    public String A06 = Integer.toString(Math.abs(A09.nextInt()), 36);
    public int[] A07;
    public final C24451Tr A08;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public static AnonymousClass4WG A00(AnonymousClass1FM r6, long j, int i) {
        int i2;
        AnonymousClass4WG r3;
        if (r6.A07 == null) {
            r3 = null;
        } else {
            long j2 = r6.A03;
            if (j > j2) {
                i2 = (int) Math.min(64L, (j - r6.A02) + 1);
            } else {
                i2 = (int) ((j2 - r6.A02) + 1);
            }
            r3 = new AnonymousClass4WG(r6.A08, "time_spent_bit_array");
            r3.A00.A0D("tos_id", r6.A06);
            r3.A00.A0A(TraceFieldType.StartTime, r6.A02);
            r3.A00.A0D("tos_array", Arrays.toString(r6.A07));
            r3.A00.A09("tos_len", i2);
            r3.A00.A09("tos_seq", r6.A01);
            r3.A00.A09("tos_cum", r6.A00);
            r3.A00.A0D("start_session_id", r6.A05);
            if (i == 3) {
                r3.A00.A0D("trigger", "clock_change");
            }
        }
        r6.A07 = null;
        r6.A03 = 0;
        return r3;
    }

    public AnonymousClass1FM(C24451Tr r3, C06230bA r4) {
        this.A08 = r3;
        this.A04 = r4;
        this.A07 = null;
    }
}
