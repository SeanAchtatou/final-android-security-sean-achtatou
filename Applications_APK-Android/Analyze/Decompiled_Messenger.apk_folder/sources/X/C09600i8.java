package X;

import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.0i8  reason: invalid class name and case insensitive filesystem */
public final class C09600i8 implements AnonymousClass03a {
    public static QuickPerformanceLogger A00;

    static {
        C001000r.A00(new C09600i8());
    }

    public void Bse() {
        if (A00 != null && AnonymousClass08Z.A05(4)) {
            A00.updateListenerMarkers();
        }
    }

    public void Bsf() {
        QuickPerformanceLogger quickPerformanceLogger = A00;
        if (quickPerformanceLogger != null) {
            quickPerformanceLogger.updateListenerMarkers();
        }
    }

    private C09600i8() {
    }
}
