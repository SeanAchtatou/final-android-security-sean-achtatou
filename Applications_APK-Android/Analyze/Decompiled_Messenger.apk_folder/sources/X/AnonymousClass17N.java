package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.concurrent.Executor;

@UserScoped
/* renamed from: X.17N  reason: invalid class name */
public final class AnonymousClass17N {
    private static C05540Zi A04;
    public AnonymousClass06B A00 = AnonymousClass067.A02();
    public C05680a9 A01;
    public C25491Zv A02;
    public Executor A03;

    public AnonymousClass17O A01(String str) {
        return new AnonymousClass17O(this.A02.A01(str, null, null, null, null, null), this.A00, this.A03, this.A01.A01(str));
    }

    public static final AnonymousClass17N A00(AnonymousClass1XY r4) {
        AnonymousClass17N r0;
        synchronized (AnonymousClass17N.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new AnonymousClass17N((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (AnonymousClass17N) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass17N(AnonymousClass1XY r2) {
        this.A03 = AnonymousClass0UX.A0S(r2);
        this.A02 = C25491Zv.A00(r2);
        this.A01 = C05680a9.A00(r2);
    }
}
