package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0Jw  reason: invalid class name */
public final class AnonymousClass0Jw implements FilenameFilter {
    public boolean accept(File file, String str) {
        return str.endsWith("_lib");
    }
}
