package X;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.HashMap;

/* renamed from: X.12y  reason: invalid class name and case insensitive filesystem */
public abstract class C184112y extends C183512m {
    private static final long serialVersionUID = 1;
    public final C10090jX[] _paramAnnotations;

    public abstract Object call();

    public abstract Object call(Object[] objArr);

    public abstract Object call1(Object obj);

    public abstract Type getGenericParameterType(int i);

    public final Annotation getAnnotation(Class cls) {
        HashMap hashMap = this._annotations._annotations;
        if (hashMap == null) {
            return null;
        }
        return (Annotation) hashMap.get(cls);
    }

    public final AnonymousClass137 getParameter(int i) {
        C10090jX r0;
        Type genericParameterType = getGenericParameterType(i);
        C10090jX[] r1 = this._paramAnnotations;
        if (r1 == null || i < 0 || i > r1.length) {
            r0 = null;
        } else {
            r0 = r1[i];
        }
        return new AnonymousClass137(this, genericParameterType, r0, i);
    }

    public C10030jR getType(C28311eb r8, TypeVariable[] typeVariableArr) {
        C10030jR _constructType;
        if (typeVariableArr != null && (r6 = typeVariableArr.length) > 0) {
            C28311eb r5 = new C28311eb(r8._typeFactory, r8, r8._contextClass, r8._contextType);
            r8 = r5;
            for (TypeVariable typeVariable : typeVariableArr) {
                r5._addPlaceholder(typeVariable.getName());
                Type type = typeVariable.getBounds()[0];
                if (type == null) {
                    _constructType = C10300js.unknownType();
                } else {
                    _constructType = r5._typeFactory._constructType(type, r5);
                }
                r5.addBinding(typeVariable.getName(), _constructType);
            }
        }
        return r8._typeFactory._constructType(getGenericType(), r8);
    }

    public C184112y(C10090jX r1, C10090jX[] r2) {
        super(r1);
        this._paramAnnotations = r2;
    }
}
