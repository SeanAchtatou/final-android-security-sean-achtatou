package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Looper;

/* renamed from: X.0yh  reason: invalid class name and case insensitive filesystem */
public abstract class C17310yh {
    public static final Class A05 = C17310yh.class;
    public boolean A00;
    public final BroadcastReceiver A01 = new C17320yi(this);
    public final C04480Uv A02;
    private final IntentFilter A03;
    private final Looper A04;

    public void A01(Context context, Intent intent) {
        ((C17300yg) this).A00.A01.finish();
    }

    public void A00() {
        if (this.A00) {
            C010708t.A05(A05, "Called registerNotificationReceiver twice.");
            return;
        }
        this.A02.A03(this.A01, this.A03, this.A04);
        this.A00 = true;
    }

    public C17310yh(Context context, IntentFilter intentFilter, Looper looper) {
        this.A02 = C04480Uv.A00(context);
        this.A03 = intentFilter;
        this.A04 = looper;
    }
}
