package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1IY  reason: invalid class name */
public final class AnonymousClass1IY extends Enum {
    private static final /* synthetic */ AnonymousClass1IY[] A00;
    public static final AnonymousClass1IY A01;
    public static final AnonymousClass1IY A02;
    public static final AnonymousClass1IY A03;
    public static final AnonymousClass1IY A04;
    public static final AnonymousClass1IY A05;
    public static final AnonymousClass1IY A06;
    public static final AnonymousClass1IY A07;
    public static final AnonymousClass1IY A08;
    public static final AnonymousClass1IY A09;
    public static final AnonymousClass1IY A0A;
    public static final AnonymousClass1IY A0B;
    public static final AnonymousClass1IY A0C;
    public static final AnonymousClass1IY A0D;
    public static final AnonymousClass1IY A0E;
    public static final AnonymousClass1IY A0F;
    public static final AnonymousClass1IY A0G;
    public static final AnonymousClass1IY A0H;
    public static final AnonymousClass1IY A0I;
    public static final AnonymousClass1IY A0J;
    public static final AnonymousClass1IY A0K;
    public static final AnonymousClass1IY A0L;
    public static final AnonymousClass1IY A0M;
    public static final AnonymousClass1IY A0N;
    public static final AnonymousClass1IY A0O;
    public static final AnonymousClass1IY A0P;
    public static final AnonymousClass1IY A0Q;
    public static final AnonymousClass1IY A0R;
    public static final AnonymousClass1IY A0S;
    public static final AnonymousClass1IY A0T;
    public static final AnonymousClass1IY A0U;
    public static final AnonymousClass1IY A0V;
    public static final AnonymousClass1IY A0W;
    public static final AnonymousClass1IY A0X;
    public static final AnonymousClass1IY A0Y;
    public static final AnonymousClass1IY A0Z;
    public static final AnonymousClass1IY A0a;
    public static final AnonymousClass1IY A0b;

    static {
        AnonymousClass1IY r0 = new AnonymousClass1IY("THREAD", 0);
        A0b = r0;
        AnonymousClass1IY r02 = new AnonymousClass1IY("CONVERSATION_STARTER", 1);
        A06 = r02;
        AnonymousClass1IY r03 = new AnonymousClass1IY("SECTION_HEADER", 2);
        A0Z = r03;
        AnonymousClass1IY r04 = new AnonymousClass1IY("NON_INBOX_SERVICE_SECTION_HEADER", 3);
        A0X = r04;
        AnonymousClass1IY r05 = new AnonymousClass1IY("MESSAGE_REQUEST_DISCLAIMER", 4);
        A0M = r05;
        AnonymousClass1IY r06 = new AnonymousClass1IY("MESSAGE_REQUEST_HEADER", 5);
        A0N = r06;
        AnonymousClass1IY r07 = new AnonymousClass1IY("ACTIVE_NOW_PRESENCE_DISABLED_UPSELL", 6);
        A01 = r07;
        AnonymousClass1IY r08 = new AnonymousClass1IY("LOAD_MORE_THREADS_PLACEHOLDER", 7);
        A0K = r08;
        AnonymousClass1IY r09 = new AnonymousClass1IY("MONTAGE_AND_ACTIVE_NOW", 8);
        A0T = r09;
        AnonymousClass1IY r2 = new AnonymousClass1IY("MONTAGE_COMPOSER_HEADER", 9);
        A0U = r2;
        AnonymousClass1IY r1 = new AnonymousClass1IY("MONTAGE_COMPOSER_HEADER_ITEM", 10);
        A0V = r1;
        AnonymousClass1IY r010 = new AnonymousClass1IY("BYMM_PAGE", 11);
        A03 = r010;
        AnonymousClass1IY r011 = new AnonymousClass1IY("BYMM_VERTICAL", 12);
        A04 = r011;
        AnonymousClass1IY r012 = new AnonymousClass1IY("BUSINESS_VCARD", 13);
        A02 = r012;
        AnonymousClass1IY r4 = new AnonymousClass1IY("DISCOVER_VERTICAL_UNIT", 14);
        A0F = r4;
        AnonymousClass1IY r42 = new AnonymousClass1IY("DISCOVER_VERTICAL_COMPACT_ITEM", 15);
        A0E = r42;
        AnonymousClass1IY r43 = new AnonymousClass1IY("DISCOVER_GRID_UNIT", 16);
        A0C = r43;
        AnonymousClass1IY r44 = new AnonymousClass1IY("DISCOVER_GENERIC_ITEM", 17);
        A0B = r44;
        AnonymousClass1IY r45 = new AnonymousClass1IY("DISCOVER_GAME_VERTICAL_ITEM_WITH_SUBATTACHMENTS", 18);
        A0A = r45;
        AnonymousClass1IY r46 = new AnonymousClass1IY("DISCOVER_GAME_NUX_FOOTER", 19);
        A09 = r46;
        AnonymousClass1IY r47 = new AnonymousClass1IY("DISCOVER_GAME_MEDIA_CARD", 20);
        A07 = r47;
        AnonymousClass1IY r48 = new AnonymousClass1IY("DISCOVER_GAME_MEDIA_CARD_WITH_TOS", 21);
        A08 = r48;
        AnonymousClass1IY r49 = new AnonymousClass1IY("DISCOVER_HORIZONTAL_CARD", 22);
        A0D = r49;
        AnonymousClass1IY r15 = new AnonymousClass1IY("MORE_FOOTER", 23);
        A0W = r15;
        AnonymousClass1IY r14 = new AnonymousClass1IY("SEE_ALL_FOOTER", 24);
        A0a = r14;
        AnonymousClass1IY r3 = new AnonymousClass1IY("CONTACTS_YOU_MAY_KNOW", 25);
        A05 = r3;
        AnonymousClass1IY r5 = new AnonymousClass1IY("CONTACTS_YOU_MAY_KNOW_ITEM", 26);
        AnonymousClass1IY r52 = new AnonymousClass1IY("HORIZONTAL_TILE_ITEM", 27);
        A0I = r52;
        AnonymousClass1IY r13 = new AnonymousClass1IY("HORIZONTAL_TILES_UNIT_ITEM", 28);
        A0H = r13;
        AnonymousClass1IY r53 = new AnonymousClass1IY("MESSENGER_ADS_SINGLE_LARGE_UNIT", 29);
        AnonymousClass1IY r12 = new AnonymousClass1IY("MESSENGER_ADS_ITEM", 30);
        A0O = r12;
        AnonymousClass1IY r54 = new AnonymousClass1IY("DIRECT_M", 31);
        AnonymousClass1IY r11 = new AnonymousClass1IY("MESSENGER_DISCOVERY_CATEGORY", 32);
        A0Q = r11;
        AnonymousClass1IY r10 = new AnonymousClass1IY("MESSENGER_DISCOVERY_BANNER", 33);
        A0P = r10;
        AnonymousClass1IY r9 = new AnonymousClass1IY("MESSENGER_DISCOVERY_LOCATION_UPSELL", 34);
        A0R = r9;
        AnonymousClass1IY r8 = new AnonymousClass1IY("MESSENGER_EXTENSION_ITEM", 35);
        A0S = r8;
        AnonymousClass1IY r7 = new AnonymousClass1IY("INVITE_COWORKERS", 36);
        A0J = r7;
        AnonymousClass1IY r16 = new AnonymousClass1IY("CHAT_SUGGESTION", 37);
        AnonymousClass1IY r452 = new AnonymousClass1IY("GROUP_FOR_CHAT_SUGGESTION", 38);
        AnonymousClass1IY r6 = new AnonymousClass1IY("MARKETPLACE_FOLDER", 39);
        A0L = r6;
        AnonymousClass1IY r453 = new AnonymousClass1IY("BUSINESS_FOLDER", 40);
        AnonymousClass1IY r55 = new AnonymousClass1IY("DIVIDER", 41);
        A0G = r55;
        AnonymousClass1IY r454 = new AnonymousClass1IY("PINNED_THREADS_HEADER", 42);
        A0Y = r454;
        AnonymousClass1IY[] r410 = new AnonymousClass1IY[43];
        System.arraycopy(new AnonymousClass1IY[]{r0, r02, r03, r04, r05, r06, r07, r08, r09, r2, r1, r010, r011, r012, r4, r42, r43, r44, r45, r46, r47, r48, r49, r15, r14, r3, r5}, 0, r410, 0, 27);
        System.arraycopy(new AnonymousClass1IY[]{r52, r13, r53, r12, r54, r11, r10, r9, r8, r7, r16, r452, r6, r453, r55, r454}, 0, r410, 27, 16);
        A00 = r410;
        values();
    }

    public static AnonymousClass1IY valueOf(String str) {
        return (AnonymousClass1IY) Enum.valueOf(AnonymousClass1IY.class, str);
    }

    public static AnonymousClass1IY[] values() {
        return (AnonymousClass1IY[]) A00.clone();
    }

    private AnonymousClass1IY(String str, int i) {
    }
}
