package X;

import android.graphics.drawable.Drawable;
import android.view.ViewOutlineProvider;
import com.facebook.common.dextricks.DexStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;

/* renamed from: X.11F  reason: invalid class name */
public abstract class AnonymousClass11F implements Cloneable {
    public C17770zR A00;
    public AnonymousClass0p4 A01;
    public C17540z4 A02;

    public void A22(int i) {
        A24(this.A02.A06(i, 0));
    }

    public abstract AnonymousClass11F A2x();

    public abstract C17770zR A31();

    public abstract void A32(C17770zR r1);

    public static void A0C(int i, BitSet bitSet, String[] strArr) {
        if (bitSet != null) {
            if (bitSet.nextClearBit(0) < i) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < i; i2++) {
                    if (!bitSet.get(i2)) {
                        arrayList.add(strArr[i2]);
                    }
                }
                throw new IllegalStateException(AnonymousClass08S.A0J("The following props are not marked as optional and were not supplied: ", Arrays.toString(arrayList.toArray())));
            }
        }
    }

    public AnonymousClass11F A1h(float f) {
        this.A00.A14().A04(f);
        A2x();
        return this;
    }

    public AnonymousClass11F A1i(boolean z) {
        this.A00.A14().A0Z(z);
        A2x();
        return this;
    }

    public void A1j() {
        this.A00.A14().A08 = true;
        A2x();
    }

    public void A1k(float f) {
        this.A00.A14().AOt(f);
        A2x();
    }

    public void A1l(float f) {
        this.A00.A14().Aa6(f);
        A2x();
    }

    public void A1m(float f) {
        this.A00.A14().Aa8(this.A02.A00(f));
        A2x();
    }

    public void A1n(float f) {
        this.A00.A14().AaB(f);
        A2x();
    }

    public void A1o(float f) {
        this.A00.A14().AaD(f);
        A2x();
    }

    public void A1p(float f) {
        A27(this.A02.A00(f));
    }

    public void A1q(float f) {
        this.A00.A14().BC1(f);
        A2x();
    }

    public void A1r(float f) {
        A2B(this.A02.A00(f));
    }

    public void A1s(float f) {
        this.A00.A14().BKK(f);
        A2x();
    }

    public void A1t(float f) {
        A2C(this.A02.A00(f));
    }

    public void A1u(float f) {
        A2D(this.A02.A00(f));
    }

    public void A1v(float f) {
        this.A00.A14().BL9(f);
        A2x();
    }

    public void A1w(float f) {
        this.A00.A14().A05(f);
        A2x();
    }

    public void A1x(float f) {
        this.A00.A14().A06(f);
        A2x();
    }

    public void A1y(float f) {
        this.A00.A14().A07((float) this.A02.A00(f));
        A2x();
    }

    public void A1z(float f) {
        A2F(this.A02.A00(f));
    }

    public void A20(float f) {
        this.A00.A14().CNJ(f);
        A2x();
    }

    public void A21(int i) {
        String string = this.A01.A03().getString(i);
        C31401jd A012 = AnonymousClass11G.A01(this.A00.A14());
        A012.A0A |= 16777216;
        A012.A0R = string;
        A2x();
    }

    public void A24(int i) {
        if (i == 0) {
            A2H(null);
        } else {
            A2H(AnonymousClass01R.A03(this.A01.A09, i));
        }
    }

    public void A25(int i) {
        A2z(this.A01.A09.getResources().getString(i));
    }

    public void A26(int i) {
        if (i == 0) {
            A2I(null);
        } else {
            A2I(AnonymousClass01R.A03(this.A01.A09, i));
        }
    }

    public void A27(int i) {
        this.A00.A14().BC2(i);
        A2x();
    }

    public void A28(int i) {
        A27(this.A02.A03(i));
    }

    public void A29(int i) {
        this.A00.A14().A0A(i);
        A2x();
    }

    public void A2A(int i) {
        this.A00.A14().BKH(i);
        A2x();
    }

    public void A2B(int i) {
        this.A00.A14().BKL(i);
        A2x();
    }

    public void A2C(int i) {
        this.A00.A14().BL8(i);
        A2x();
    }

    public void A2D(int i) {
        this.A00.A14().BLA(i);
        A2x();
    }

    public void A2E(int i) {
        A2D(this.A02.A03(i));
    }

    public void A2F(int i) {
        this.A00.A14().CNK(i);
        A2x();
    }

    public void A2G(int i) {
        A2F(this.A02.A03(i));
    }

    public void A2H(Drawable drawable) {
        this.A00.A14().A0C(drawable);
        A2x();
    }

    public void A2I(Drawable drawable) {
        this.A00.A14().A0D(drawable);
        A2x();
    }

    public void A2J(ViewOutlineProvider viewOutlineProvider) {
        this.A00.A14().A0E(viewOutlineProvider);
        A2x();
    }

    public void A2K(AnonymousClass38W r2) {
        this.A00.A14().A0F(r2);
        A2x();
    }

    public void A2L(AnonymousClass0p4 r2, int i, int i2, C17770zR r5) {
        this.A02 = r2.A0B;
        this.A00 = r5;
        this.A01 = r2;
        C17770zR r0 = r2.A04;
        if (r0 != null) {
            r5.A07 = r0.A06;
        }
        if (i != 0 || i2 != 0) {
            r5.A14().A0B(i, i2);
            r5.A0d(r2, i, i2);
        }
    }

    public void A2M(AnonymousClass10N r2) {
        this.A00.A14().A0H(r2);
        A2x();
    }

    public void A2N(AnonymousClass10N r3) {
        C14930uN A002 = AnonymousClass11G.A00(this.A00.A14());
        A002.A03 |= 16;
        A002.A08 = r3;
        A2x();
    }

    public void A2O(AnonymousClass10N r3) {
        C14930uN A002 = AnonymousClass11G.A00(this.A00.A14());
        A002.A03 |= 32;
        A002.A09 = r3;
        A2x();
    }

    public void A2P(AnonymousClass10N r4) {
        C31401jd A012 = AnonymousClass11G.A01(this.A00.A14());
        A012.A0A |= DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED;
        A012.A0H = r4;
        A2x();
    }

    public void A2Q(AnonymousClass10N r2) {
        this.A00.A14().A0I(r2);
        A2x();
    }

    public void A2R(AnonymousClass10N r2) {
        this.A00.A14().A0J(r2);
        A2x();
    }

    public void A2S(AnonymousClass10N r2) {
        this.A00.A14().A0K(r2);
        A2x();
    }

    public void A2T(AnonymousClass10N r2) {
        this.A00.A14().A0L(r2);
        A2x();
    }

    public void A2U(AnonymousClass10N r2) {
        this.A00.A14().A0M(r2);
        A2x();
    }

    public void A2V(C14940uO r2) {
        this.A00.A14().ANz(r2);
        A2x();
    }

    public void A2W(AnonymousClass10G r2) {
        this.A00.A14().BJt(r2);
        A2x();
    }

    public void A2X(AnonymousClass10G r2, float f) {
        A2e(r2, this.A02.A00(f));
    }

    public void A2Y(AnonymousClass10G r2, float f) {
        this.A00.A14().BJv(r2, f);
        A2x();
    }

    public void A2Z(AnonymousClass10G r2, float f) {
        A2g(r2, this.A02.A00(f));
    }

    public void A2a(AnonymousClass10G r2, float f) {
        this.A00.A14().BwL(r2, f);
        A2x();
    }

    public void A2b(AnonymousClass10G r2, float f) {
        A2i(r2, this.A02.A00(f));
    }

    public void A2c(AnonymousClass10G r2, float f) {
        this.A00.A14().BxH(r2, f);
        A2x();
    }

    public void A2d(AnonymousClass10G r3, float f) {
        this.A00.A14().A0N(r3, this.A02.A00(f));
        A2x();
    }

    public void A2e(AnonymousClass10G r2, int i) {
        this.A00.A14().BJx(r2, i);
        A2x();
    }

    public void A2f(AnonymousClass10G r2, int i) {
        A2e(r2, this.A02.A03(i));
    }

    public void A2g(AnonymousClass10G r2, int i) {
        this.A00.A14().BwM(r2, i);
        A2x();
    }

    public void A2h(AnonymousClass10G r2, int i) {
        A2g(r2, this.A02.A03(i));
    }

    public void A2i(AnonymousClass10G r2, int i) {
        this.A00.A14().BxI(r2, i);
        A2x();
    }

    public void A2j(AnonymousClass10G r2, int i) {
        A2i(r2, this.A02.A03(i));
    }

    public void A2k(AnonymousClass1LC r2) {
        this.A00.A14().BxJ(r2);
        A2x();
    }

    public void A2l(Integer num) {
        if (num != null) {
            this.A00.A14().A0P(num);
            A2x();
            return;
        }
        throw new IllegalArgumentException(AnonymousClass24B.$const$string(50));
    }

    public void A2m(Object obj) {
        this.A00.A14().A0Q(obj);
        A2x();
    }

    public void A2n(String str) {
        this.A00.A14().A0R(str);
        A2x();
    }

    public void A2o(String str) {
        String str2;
        if (str == null) {
            C17770zR r0 = this.A01.A04;
            if (r0 != null) {
                str2 = r0.A1A();
            } else {
                str2 = "unknown component";
            }
            C09070gU.A01(AnonymousClass07B.A01, "Component:NullKeySet", AnonymousClass08S.A0P("Setting a null key from ", str2, C99084oO.$const$string(0)));
            str = "null";
        }
        this.A00.A1I(str);
        A2x();
    }

    public void A2p(String str) {
        this.A00.A14().A0S(str);
        A2x();
    }

    public void A2q(String str) {
        C17770zR r2 = this.A00;
        r2.A14().A0T(str, r2.A07);
        if (r2.A14().A03() == null) {
            A2l(C17760zQ.A02);
        }
        A2x();
    }

    public void A2r(boolean z) {
        C31401jd A012 = AnonymousClass11G.A01(this.A00.A14());
        if (z) {
            A012.A06 = 1;
        } else {
            A012.A06 = 2;
        }
        A2x();
    }

    public void A2s(boolean z) {
        this.A00.A14().A0U(z);
        A2x();
    }

    public void A2t(boolean z) {
        C31401jd A012 = AnonymousClass11G.A01(this.A00.A14());
        A012.A0A |= 65536;
        A012.A0W = z;
        A2x();
    }

    public void A2u(boolean z) {
        this.A00.A14().A0W(z);
        A2x();
    }

    public void A2v(boolean z) {
        this.A00.A14().A0X(z);
        A2x();
    }

    public void A2w(boolean z) {
        this.A00.A14().A0Y(z);
        A2x();
    }

    public AnonymousClass11F A2y(AnonymousClass10N r2) {
        this.A00.A14().A0G(r2);
        A2x();
        return this;
    }

    public AnonymousClass11F A2z(CharSequence charSequence) {
        this.A00.A14().A0O(charSequence);
        A2x();
        return this;
    }

    public AnonymousClass11F A30(boolean z) {
        this.A00.A14().A0V(z);
        A2x();
        return this;
    }

    public void A23(int i) {
        A2H(C16990y9.A00(i));
    }

    public Object clone() {
        try {
            AnonymousClass11F r1 = (AnonymousClass11F) super.clone();
            C17770zR A16 = this.A00.A16();
            r1.A00 = A16;
            r1.A32(A16);
            return r1;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
