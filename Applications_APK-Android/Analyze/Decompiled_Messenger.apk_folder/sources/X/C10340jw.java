package X;

/* renamed from: X.0jw  reason: invalid class name and case insensitive filesystem */
public final class C10340jw {
    public static final C10350jx MIME;
    public static final C10350jx MIME_NO_LINEFEEDS;
    public static final C10350jx MODIFIED_FOR_URL;
    public static final C10350jx PEM = new C10350jx(MIME, "PEM", true, '=', 64);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jx.<init>(java.lang.String, java.lang.String, boolean, char, int):void
     arg types: [java.lang.String, java.lang.String, int, int, int]
     candidates:
      X.0jx.<init>(X.0jx, java.lang.String, boolean, char, int):void
      X.0jx.<init>(java.lang.String, java.lang.String, boolean, char, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jx.<init>(X.0jx, java.lang.String, boolean, char, int):void
     arg types: [X.0jx, java.lang.String, int, int, int]
     candidates:
      X.0jx.<init>(java.lang.String, java.lang.String, boolean, char, int):void
      X.0jx.<init>(X.0jx, java.lang.String, boolean, char, int):void */
    static {
        C10350jx r1 = new C10350jx("MIME", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", true, '=', 76);
        MIME = r1;
        MIME_NO_LINEFEEDS = new C10350jx(r1, "MIME-NO-LINEFEEDS", r1._usesPadding, r1._paddingChar, Integer.MAX_VALUE);
        StringBuffer stringBuffer = new StringBuffer("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
        stringBuffer.setCharAt(stringBuffer.indexOf("+"), '-');
        stringBuffer.setCharAt(stringBuffer.indexOf("/"), '_');
        MODIFIED_FOR_URL = new C10350jx("MODIFIED-FOR-URL", stringBuffer.toString(), false, 0, Integer.MAX_VALUE);
    }
}
