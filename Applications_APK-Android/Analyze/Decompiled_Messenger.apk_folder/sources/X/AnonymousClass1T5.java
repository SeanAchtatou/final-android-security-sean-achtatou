package X;

import java.io.OutputStream;

/* renamed from: X.1T5  reason: invalid class name */
public interface AnonymousClass1T5 {
    boolean canResize(AnonymousClass1NY r1, AnonymousClass1Q1 r2, AnonymousClass36w r3);

    boolean canTranscode(AnonymousClass1O3 r1);

    String getIdentifier();

    C80293sC transcode(AnonymousClass1NY r1, OutputStream outputStream, AnonymousClass1Q1 r3, AnonymousClass36w r4, AnonymousClass1O3 r5, Integer num);
}
