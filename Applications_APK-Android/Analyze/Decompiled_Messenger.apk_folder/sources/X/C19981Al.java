package X;

import java.util.ArrayList;

/* renamed from: X.1Al  reason: invalid class name and case insensitive filesystem */
public final class C19981Al implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.analytics.tracker.PassiveImpressionTracker$1";
    public final /* synthetic */ C15480vM A00;

    public C19981Al(C15480vM r1) {
        this.A00 = r1;
    }

    public void run() {
        C15480vM r4 = this.A00;
        C15480vM.A01(r4);
        C19881Ab r5 = r4.A05;
        ArrayList arrayList = new ArrayList();
        int A01 = r4.A03.A01();
        for (int i = 0; i < A01; i++) {
            arrayList.add(r4.A03.A06(i));
        }
        r5.BJd(arrayList);
        for (int A012 = r4.A03.A01() - 1; A012 >= 0; A012--) {
            C17610zB r1 = r4.A03;
            C24201Sr r2 = (C24201Sr) r1.A06(A012);
            if (r2.A08) {
                r2.A02 = 0;
            } else {
                r1.A0A(A012);
                C24191Sq.A00.A02(r2);
            }
        }
    }
}
