package X;

/* renamed from: X.1rR  reason: invalid class name and case insensitive filesystem */
public final class C35571rR extends C35581rS implements C31551js {
    private final C31551js A00;

    public C35571rR(C31551js r2, String str) {
        super(str, 1);
        this.A00 = r2;
    }

    public void C1H(Runnable runnable) {
        Runnable runnable2 = (Runnable) this.A00.remove(runnable);
        if (runnable2 != null) {
            runnable = runnable2;
        }
        this.A00.C1H(runnable);
    }

    public boolean BHG() {
        return C25231Yv.A01();
    }

    public void BxL(Runnable runnable, String str) {
        this.A00.BxL(A00(runnable, str), str);
    }

    public void BxM(Runnable runnable, String str) {
        this.A00.BxM(A00(runnable, str), str);
    }
}
