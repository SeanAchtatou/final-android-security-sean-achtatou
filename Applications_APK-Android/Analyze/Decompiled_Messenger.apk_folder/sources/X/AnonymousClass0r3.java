package X;

import android.content.DialogInterface;

/* renamed from: X.0r3  reason: invalid class name */
public final class AnonymousClass0r3 implements DialogInterface.OnClickListener {
    public final /* synthetic */ C10960l9 A00;
    public final /* synthetic */ C21451Ha A01;
    public final /* synthetic */ C57362rr A02;
    public final /* synthetic */ String A03;

    public AnonymousClass0r3(C10960l9 r1, String str, C21451Ha r3, C57362rr r4) {
        this.A00 = r1;
        this.A03 = str;
        this.A01 = r3;
        this.A02 = r4;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.A00.A0C(this.A03)) {
            this.A01.A0F("turn_on_sms_dialog");
        }
        this.A02.A03();
    }
}
