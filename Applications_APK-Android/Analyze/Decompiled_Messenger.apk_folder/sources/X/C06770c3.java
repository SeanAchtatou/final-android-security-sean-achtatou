package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableSortedSet;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

/* renamed from: X.0c3  reason: invalid class name and case insensitive filesystem */
public final class C06770c3 extends C07410dQ {
    private final Comparator A00;

    /* renamed from: A05 */
    public ImmutableSortedSet build() {
        RegularImmutableSortedSet regularImmutableSortedSet;
        ImmutableList regularImmutableList;
        Object[] objArr = this.A02;
        Comparator comparator = this.A00;
        int i = this.A00;
        if (i == 0) {
            regularImmutableSortedSet = ImmutableSortedSet.A0C(comparator);
        } else {
            for (int i2 = 0; i2 < i; i2++) {
                AnonymousClass0U8.A00(objArr[i2], i2);
            }
            Arrays.sort(objArr, 0, i, comparator);
            int i3 = 1;
            for (int i4 = 1; i4 < i; i4++) {
                Object obj = objArr[i4];
                if (comparator.compare(obj, objArr[i3 - 1]) != 0) {
                    objArr[i3] = obj;
                    i3++;
                }
            }
            Arrays.fill(objArr, i3, i, (Object) null);
            if (i3 < (objArr.length >> 1)) {
                objArr = Arrays.copyOf(objArr, i3);
            }
            if (i3 == 0) {
                regularImmutableList = RegularImmutableList.A02;
            } else {
                regularImmutableList = new RegularImmutableList(objArr, i3);
            }
            regularImmutableSortedSet = new RegularImmutableSortedSet(regularImmutableList, comparator);
        }
        this.A00 = regularImmutableSortedSet.size();
        this.A01 = true;
        return regularImmutableSortedSet;
    }

    public C06770c3(Comparator comparator) {
        Preconditions.checkNotNull(comparator);
        this.A00 = comparator;
    }

    public /* bridge */ /* synthetic */ C07410dQ A00(Iterable iterable) {
        super.A00(iterable);
        return this;
    }

    public /* bridge */ /* synthetic */ C07410dQ A01(Object obj) {
        super.A01(obj);
        return this;
    }

    public /* bridge */ /* synthetic */ C07410dQ A02(Iterator it) {
        super.A02(it);
        return this;
    }

    public /* bridge */ /* synthetic */ C07410dQ A03(Object[] objArr) {
        super.A03(objArr);
        return this;
    }

    public void A06(Object obj) {
        super.A01(obj);
    }

    public void A07(Object... objArr) {
        super.A03(objArr);
    }

    public /* bridge */ /* synthetic */ C24991Xx add(Object[] objArr) {
        super.A03(objArr);
        return this;
    }

    public /* bridge */ /* synthetic */ C24991Xx addAll(Iterable iterable) {
        super.A00(iterable);
        return this;
    }

    public /* bridge */ /* synthetic */ C24991Xx addAll(Iterator it) {
        super.A02(it);
        return this;
    }
}
