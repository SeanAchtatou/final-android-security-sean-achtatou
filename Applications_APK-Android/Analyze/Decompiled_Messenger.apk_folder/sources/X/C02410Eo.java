package X;

/* renamed from: X.0Eo  reason: invalid class name and case insensitive filesystem */
public final class C02410Eo {
    public static AnonymousClass0Ep A00;

    /* JADX WARNING: Can't wrap try/catch for region: R(6:5|6|7|(1:9)|10|11) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0014 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized X.AnonymousClass0Ep A00(android.content.Context r4) {
        /*
            java.lang.Class<X.0Eo> r3 = X.C02410Eo.class
            monitor-enter(r3)
            X.0Ep r0 = X.C02410Eo.A00     // Catch:{ all -> 0x0021 }
            if (r0 != 0) goto L_0x001d
            android.content.pm.ApplicationInfo r0 = r4.getApplicationInfo()     // Catch:{ all -> 0x0021 }
            java.lang.String r2 = r0.dataDir     // Catch:{ all -> 0x0021 }
            java.lang.String r0 = com.facebook.common.dextricks.DalvikInternals.realpath(r2)     // Catch:{ IOException | UnsatisfiedLinkError -> 0x0014 }
            if (r0 == 0) goto L_0x0014
            r2 = r0
        L_0x0014:
            X.0Ep r1 = new X.0Ep     // Catch:{ all -> 0x0021 }
            java.lang.String r0 = "modules"
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0021 }
            X.C02410Eo.A00 = r1     // Catch:{ all -> 0x0021 }
        L_0x001d:
            X.0Ep r0 = X.C02410Eo.A00     // Catch:{ all -> 0x0021 }
            monitor-exit(r3)
            return r0
        L_0x0021:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02410Eo.A00(android.content.Context):X.0Ep");
    }
}
