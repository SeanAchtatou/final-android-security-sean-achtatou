package X;

import android.app.Activity;

/* renamed from: X.0tT  reason: invalid class name */
public final class AnonymousClass0tT {
    public final int A00;
    public final long A01;
    public final Activity A02;
    public final /* synthetic */ AnonymousClass0t4 A03;

    public AnonymousClass0tT(AnonymousClass0t4 r1, int i, Activity activity, long j) {
        this.A03 = r1;
        this.A00 = i;
        this.A02 = activity;
        this.A01 = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0073, code lost:
        if (r1 == null) goto L_0x0075;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00() {
        /*
            r8 = this;
            int r1 = r8.A00
            if (r1 == 0) goto L_0x00a4
            r0 = 1
            if (r1 == r0) goto L_0x003e
            r0 = 2
            if (r1 == r0) goto L_0x0023
            r0 = 3
            if (r1 == r0) goto L_0x002b
            r0 = 4
            if (r1 != r0) goto L_0x002a
            android.app.Activity r1 = r8.A02
            boolean r0 = r1 instanceof X.AnonymousClass16P
            if (r0 == 0) goto L_0x001c
            r0 = r1
            X.16P r0 = (X.AnonymousClass16P) r0
            r0.clearViewPools()
        L_0x001c:
            X.0t4 r0 = r8.A03
            com.facebook.common.activitycleaner.ActivityStackManager r0 = r0.A01
            r0.A04(r1)
        L_0x0023:
            X.0t4 r0 = r8.A03
            com.facebook.common.activitycleaner.ActivityStackManager r0 = r0.A01
            r0.A03()
        L_0x002a:
            return
        L_0x002b:
            X.0t4 r1 = r8.A03
            android.app.Activity r0 = r8.A02
            X.AnonymousClass0t4.A02(r1, r0)
            android.app.Activity r1 = r8.A02
            boolean r0 = r1 instanceof X.AnonymousClass16P
            if (r0 == 0) goto L_0x002a
            X.16P r1 = (X.AnonymousClass16P) r1
            r1.clearViewPools()
            return
        L_0x003e:
            X.0t4 r0 = r8.A03
            com.facebook.common.activitycleaner.ActivityStackManager r7 = r0.A01
            android.app.Activity r1 = r8.A02
            long r3 = r8.A01
            java.util.LinkedList r5 = r7.A04
            monitor-enter(r5)
            java.util.Map r0 = r7.A05     // Catch:{ all -> 0x00b0 }
            java.lang.Object r6 = r0.get(r1)     // Catch:{ all -> 0x00b0 }
            X.0tp r6 = (X.C14690tp) r6     // Catch:{ all -> 0x00b0 }
            if (r6 != 0) goto L_0x007d
            X.0tp r6 = new X.0tp     // Catch:{ all -> 0x00b0 }
            r6.<init>(r1)     // Catch:{ all -> 0x00b0 }
            java.util.Map r0 = r7.A05     // Catch:{ all -> 0x00b0 }
            r0.put(r1, r6)     // Catch:{ all -> 0x00b0 }
            java.util.LinkedList r0 = r7.A04     // Catch:{ all -> 0x00b0 }
            r0.add(r6)     // Catch:{ all -> 0x00b0 }
            int r0 = r7.A00     // Catch:{ all -> 0x00b0 }
            int r0 = r0 + 1
            r7.A00 = r0     // Catch:{ all -> 0x00b0 }
            android.app.Activity r0 = r6.A00()     // Catch:{ all -> 0x00b0 }
            if (r0 == 0) goto L_0x0075
            android.content.ComponentName r1 = r0.getCallingActivity()     // Catch:{ all -> 0x00b0 }
            r0 = 1
            if (r1 != 0) goto L_0x0076
        L_0x0075:
            r0 = 0
        L_0x0076:
            if (r0 == 0) goto L_0x007d
            java.util.HashSet r0 = r7.A03     // Catch:{ all -> 0x00b0 }
            r0.add(r6)     // Catch:{ all -> 0x00b0 }
        L_0x007d:
            r1 = -9223372036854775808
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0087
            monitor-enter(r6)     // Catch:{ all -> 0x00b0 }
            r6.A00 = r3     // Catch:{ all -> 0x00ad }
            monitor-exit(r6)     // Catch:{ all -> 0x00ad }
        L_0x0087:
            java.util.LinkedList r1 = r7.A04     // Catch:{ all -> 0x00b0 }
            java.util.Comparator r0 = X.C14690tp.A02     // Catch:{ all -> 0x00b0 }
            java.util.Collections.sort(r1, r0)     // Catch:{ all -> 0x00b0 }
            monitor-exit(r5)     // Catch:{ all -> 0x00b0 }
            X.0t4 r0 = r8.A03
            com.facebook.common.activitycleaner.ActivityStackManager r0 = r0.A01
            r0.A03()
            X.0t4 r2 = r8.A03
            boolean r0 = r2.A04
            if (r0 != 0) goto L_0x002a
            int r1 = r2.A00
            android.app.Activity r0 = r8.A02
            X.AnonymousClass0t4.A01(r2, r1, r0)
            goto L_0x0023
        L_0x00a4:
            X.0t4 r1 = r8.A03
            android.app.Activity r0 = r8.A02
            X.AnonymousClass0t4.A02(r1, r0)
            goto L_0x0023
        L_0x00ad:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00ad }
            throw r0     // Catch:{ all -> 0x00b0 }
        L_0x00b0:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00b0 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0tT.A00():void");
    }
}
