package X;

import com.fasterxml.jackson.databind.JsonNode;

/* renamed from: X.1et  reason: invalid class name and case insensitive filesystem */
public final class C28491et {
    public final AnonymousClass0jD A00;

    public static final C28491et A00(AnonymousClass1XY r1) {
        return new C28491et(r1);
    }

    public static String A01(JsonNode jsonNode, String str) {
        if (jsonNode == null || jsonNode.get(str) == null || jsonNode.get(str).getNodeType() == C11980oL.NULL) {
            return null;
        }
        return jsonNode.get(str).asText();
    }

    public C28491et(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0jD.A00(r2);
    }
}
