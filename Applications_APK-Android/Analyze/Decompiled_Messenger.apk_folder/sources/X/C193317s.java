package X;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.17s  reason: invalid class name and case insensitive filesystem */
public final class C193317s implements Iterator {
    private int A00;
    private int A01;
    private int A02;
    private boolean A03 = false;
    private final C13770s2 A04;
    private final Object[] A05;

    public final boolean hasNext() {
        if (this.A00 >= 0) {
            return true;
        }
        return false;
    }

    public final Object next() {
        if (this.A01 == this.A04.A00) {
            int i = this.A00;
            if (i >= 0) {
                Object[] objArr = this.A05;
                Object obj = objArr[i];
                if (obj == C13770s2.A03) {
                    obj = null;
                }
                this.A02 = i;
                this.A03 = true;
                for (int i2 = i + 1; i2 < objArr.length; i2++) {
                    if (objArr[i2] != null) {
                        this.A00 = i2;
                        return obj;
                    }
                }
                this.A00 = -1;
                return obj;
            }
            throw new NoSuchElementException();
        }
        throw new ConcurrentModificationException();
    }

    public final void remove() {
        int i = this.A01;
        C13770s2 r2 = this.A04;
        if (i != r2.A00) {
            throw new ConcurrentModificationException();
        } else if (this.A03) {
            this.A03 = false;
            r2.remove(this.A05[this.A02]);
            this.A01++;
            int i2 = this.A02;
            while (true) {
                Object[] objArr = this.A05;
                if (i2 >= objArr.length) {
                    this.A00 = -1;
                    return;
                } else if (objArr[i2] != null) {
                    this.A00 = i2;
                    return;
                } else {
                    i2++;
                }
            }
        } else {
            throw new IllegalStateException();
        }
    }

    public C193317s(C13770s2 r2) {
        this.A04 = r2;
        this.A05 = r2.A02;
        this.A01 = r2.A00;
        int A022 = r2.A02(-1);
        this.A00 = A022;
        this.A02 = A022;
    }
}
