package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.base.Preconditions;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0yG  reason: invalid class name and case insensitive filesystem */
public final class C17060yG implements AnonymousClass1YQ, C05460Za, CallerContextable {
    private static volatile C17060yG A04 = null;
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.MessengerLauncherBadgesController";
    public AnonymousClass0UN A00;
    private C07930eP A01;
    public final C04310Tq A02;
    private final C04310Tq A03;

    public String getSimpleName() {
        return "MessengerLauncherBadgesController";
    }

    public static Message A00(C17060yG r4, int i) {
        Message obtain = Message.obtain((Handler) null, 10001);
        Bundle bundle = new Bundle();
        bundle.putString("key_message_action", "action_badge_count_update");
        bundle.putString("key_user_id", (String) r4.A03.get());
        bundle.putInt("key_messenger_badge_count", i);
        obtain.setData(bundle);
        return obtain;
    }

    public static C07930eP A01(C17060yG r5) {
        if (r5.A01 == null) {
            int i = AnonymousClass1Y3.Asw;
            AnonymousClass0UN r4 = r5.A00;
            r5.A01 = ((C06030aj) AnonymousClass1XX.A02(4, i, r4)).A01("messenger_diode_badge_sync_action", (C04460Ut) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A2N, r4), false);
        }
        return r5.A01;
    }

    public static final C17060yG A02(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C17060yG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C17060yG(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public int A03() {
        return ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A00)).AqN(C05690aA.A02, -1);
    }

    public void clearUserData() {
        Message obtain = Message.obtain((Handler) null, 10001);
        Bundle bundle = new Bundle();
        bundle.putString("key_message_action", "action_messenger_user_log_out");
        obtain.setData(bundle);
        A01(this).A06(obtain);
    }

    private C17060yG(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(8, r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.BAz, r3);
        this.A03 = AnonymousClass0WY.A02(r3);
    }

    public void init() {
        int A032 = C000700l.A03(-2075168994);
        AnonymousClass9SM r2 = new AnonymousClass9SM(this);
        C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKb, this.A00)).BMm();
        BMm.A02(C06680bu.A0I, r2);
        BMm.A00().A00();
        C07930eP A012 = A01(this);
        AnonymousClass9SR r22 = new AnonymousClass9SR();
        Preconditions.checkNotNull(r22);
        A012.A0C.put(r22, true);
        A012.A05(10001, new AnonymousClass9SN(this));
        A012.init();
        C000700l.A09(9704550, A032);
    }
}
