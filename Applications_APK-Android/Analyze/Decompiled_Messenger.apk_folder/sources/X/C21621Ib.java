package X;

/* renamed from: X.1Ib  reason: invalid class name and case insensitive filesystem */
public final class C21621Ib extends C21631Ic {
    public final C17770zR A00;

    public static C33121my A00() {
        return new C33121my();
    }

    public String getName() {
        return this.A00.A1A();
    }

    public C21621Ib(C33121my r3) {
        super(r3);
        C17770zR r0 = r3.A00;
        if (r0 != null) {
            this.A00 = r0;
            return;
        }
        throw new IllegalStateException("Component must be provided.");
    }

    public static C21681Ih A01() {
        C33121my A002 = A00();
        A002.A00 = new AnonymousClass1II();
        return A002.A03();
    }
}
