package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;

/* renamed from: X.0Ay  reason: invalid class name and case insensitive filesystem */
public abstract class C01620Ay implements C01630Az {
    private BroadcastReceiver A00;
    public final C008007h A01;
    public final Integer A02;
    public final Context A03;
    public final AnonymousClass09P A04;
    public final AnonymousClass0AW A05;
    public volatile String A06;
    public volatile String A07;

    public abstract String A00();

    public abstract String A01();

    public abstract void A04(String str, String str2);

    public void A02() {
        if (this.A00 == null) {
            C01820Bt r3 = new C01820Bt(this);
            this.A00 = r3;
            this.A03.registerReceiver(r3, new IntentFilter(A01()));
        }
    }

    public void A03() {
        BroadcastReceiver broadcastReceiver = this.A00;
        if (broadcastReceiver != null) {
            try {
                this.A03.unregisterReceiver(broadcastReceiver);
            } catch (IllegalArgumentException e) {
                C010708t.A0S(A00(), e, "Failed to unregister broadcast receiver");
            }
            this.A00 = null;
        }
    }

    public boolean A05(String str) {
        if (str == null || str.endsWith(".facebook.com") || str.endsWith(".workplace.com") || str.endsWith(".pushnotifs.com")) {
            return true;
        }
        return false;
    }

    public C01620Ay(Context context, C008007h r2, Integer num, AnonymousClass09P r4, AnonymousClass0AW r5) {
        this.A03 = context;
        this.A01 = r2;
        this.A02 = num;
        this.A04 = r4;
        this.A05 = r5;
    }

    public String Acq() {
        return this.A06;
    }

    public String Av5() {
        return this.A07;
    }
}
