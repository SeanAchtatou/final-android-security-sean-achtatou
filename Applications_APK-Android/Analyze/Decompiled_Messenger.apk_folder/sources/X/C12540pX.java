package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threadkey.ThreadKey;

@UserScoped
/* renamed from: X.0pX  reason: invalid class name and case insensitive filesystem */
public final class C12540pX {
    private static C05540Zi A01;
    private final C26681bq A00;

    public synchronized C12550pY A01(C10950l8 r3, C09510hU r4) {
        C12550pY r1;
        if (C010708t.A0X(2) && r4 != null) {
            r4.toString();
        }
        if (r4 == C09510hU.DO_NOT_CHECK_SERVER || r4 == C09510hU.STALE_DATA_OKAY || r4 == C09510hU.CHECK_SERVER_FOR_NEW_DATA) {
            r1 = new C12550pY(r4);
        } else if (!this.A00.A0F(r3)) {
            r1 = new C12550pY(C09510hU.PREFER_CACHE_IF_UP_TO_DATE);
        } else {
            r1 = new C12550pY(r4);
        }
        return r1;
    }

    public synchronized C12550pY A02(ThreadKey threadKey, C09510hU r4) {
        C12550pY r1;
        if (threadKey != null) {
            if (!(r4 == C09510hU.DO_NOT_CHECK_SERVER || r4 == C09510hU.STALE_DATA_OKAY || r4 == C09510hU.CHECK_SERVER_FOR_NEW_DATA)) {
                if (!C26681bq.A02(this.A00, threadKey).A0k(threadKey, 20)) {
                    r1 = new C12550pY(C09510hU.PREFER_CACHE_IF_UP_TO_DATE);
                } else {
                    r1 = new C12550pY(r4);
                }
            }
        }
        r1 = new C12550pY(r4);
        return r1;
    }

    public static final C12540pX A00(AnonymousClass1XY r4) {
        C12540pX r0;
        synchronized (C12540pX.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C12540pX((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C12540pX) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C12540pX(AnonymousClass1XY r2) {
        this.A00 = C26681bq.A00(r2);
    }
}
