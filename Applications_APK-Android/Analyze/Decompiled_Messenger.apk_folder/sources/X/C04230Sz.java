package X;

import android.app.Activity;
import android.os.Build;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.util.StringLocaleUtil;
import com.facebook.runtimepermissions.RequestPermissionsConfig;
import com.facebook.runtimepermissions.RuntimePermissionsNeverAskAgainDialogFragment;
import com.google.common.base.Preconditions;
import java.util.ArrayList;

/* renamed from: X.0Sz  reason: invalid class name and case insensitive filesystem */
public final class C04230Sz implements AnonymousClass0T0 {
    public Activity A00;
    public AnonymousClass0UN A01;
    public RequestPermissionsConfig A02;
    public C73233fj A03;
    public RuntimePermissionsNeverAskAgainDialogFragment A04;
    public boolean A05;
    private C04220Sy A06 = new C04220Sy(this);

    public static void A00(C04230Sz r6, String[] strArr) {
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str = strArr[i];
            String $const$string = TurboLoader.Locator.$const$string(1);
            if (!$const$string.equals(str) || !((C12460pP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWJ, r6.A01)).A08($const$string)) {
                i++;
            } else {
                ((AnonymousClass6AZ) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A2Q, r6.A01)).A04(new C79663qz());
                return;
            }
        }
    }

    public static void A01(C04230Sz r5, String[] strArr) {
        int i = Build.VERSION.SDK_INT;
        boolean z = false;
        if (i >= 23) {
            z = true;
        }
        Preconditions.checkArgument(z, StringLocaleUtil.A00("Unsupported API Level: minimum %d required but %d found", 23, Integer.valueOf(i)));
        String[] A052 = A05(r5, strArr);
        if (A052.length != 0) {
            r5.A00.requestPermissions(A052, 0);
        }
    }

    public static void A02(C04230Sz r6, String[] strArr, String str) {
        C73213fh r5 = (C73213fh) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AkV, r6.A01);
        Activity activity = r6.A00;
        for (String A012 : strArr) {
            r5.A01(str, "NEVER_ASK_AGAIN", A012, activity);
        }
    }

    public static void A03(C04230Sz r7, String[] strArr, boolean z) {
        String str;
        Integer num;
        C73213fh r6 = (C73213fh) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AkV, r7.A01);
        if (z) {
            str = "PRIMARY";
        } else {
            str = "SECONDARY";
        }
        Activity activity = r7.A00;
        for (String A012 : strArr) {
            r6.A01(str, "RATIONALE", A012, activity);
        }
        C73223fi r4 = (C73223fi) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AAN, r7.A01);
        if (z) {
            num = AnonymousClass07B.A01;
        } else {
            num = AnonymousClass07B.A0C;
        }
        for (String A022 : strArr) {
            r4.A02(num, A022, "facebook");
        }
    }

    public static String[] A04(C04230Sz r7, String[] strArr) {
        C12460pP r6 = (C12460pP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWJ, r7.A01);
        Activity activity = r7.A00;
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (r6.A07(activity, str)) {
                arrayList.add(str);
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public static String[] A05(C04230Sz r7, String[] strArr) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (!((C12460pP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWJ, r7.A01)).A08(str)) {
                arrayList.add(str);
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public void ATv(String str, C73233fj r3) {
        ATu(str, AnonymousClass0T0.A00, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00fa, code lost:
        if (r0 == false) goto L_0x00fc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ATw(java.lang.String[] r9, com.facebook.runtimepermissions.RequestPermissionsConfig r10, X.C73233fj r11) {
        /*
            r8 = this;
            r8.A02 = r10
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 0
            r0 = 23
            if (r1 >= r0) goto L_0x001d
            boolean r0 = r8.BBf(r9)
            if (r0 == 0) goto L_0x0013
            r11.BiO()
            return
        L_0x0013:
            java.lang.String[] r1 = A05(r8, r9)
            java.lang.String[] r0 = new java.lang.String[r2]
            r11.BiP(r1, r0)
            return
        L_0x001d:
            r8.A03 = r11
            java.lang.String[] r0 = A04(r8, r9)
            int r1 = r0.length
            r0 = 0
            if (r1 <= 0) goto L_0x0028
            r0 = 1
        L_0x0028:
            r8.A05 = r0
            android.app.Activity r1 = r8.A00
            boolean r0 = r1 instanceof com.facebook.base.activity.FbFragmentActivity
            if (r0 == 0) goto L_0x0036
            com.facebook.base.activity.FbFragmentActivity r1 = (com.facebook.base.activity.FbFragmentActivity) r1
            X.0Sy r0 = r8.A06
            r1.A03 = r0
        L_0x0036:
            boolean r0 = r8.BBf(r9)
            if (r0 == 0) goto L_0x0042
            X.3fj r0 = r8.A03
            r0.BiO()
            return
        L_0x0042:
            android.app.Activity r0 = r8.A00
            boolean r0 = r0.isFinishing()
            if (r0 == 0) goto L_0x0054
            java.lang.String[] r1 = A05(r8, r9)
            java.lang.String[] r0 = new java.lang.String[r2]
            r11.BiP(r1, r0)
            return
        L_0x0054:
            com.facebook.runtimepermissions.RequestPermissionsConfig r0 = r8.A02
            int r2 = r0.A00
            r1 = 3
            r0 = 0
            if (r2 != r1) goto L_0x005d
            r0 = 1
        L_0x005d:
            r6 = 1
            if (r0 == 0) goto L_0x00cb
            int r5 = r9.length
            r4 = 0
            r3 = 0
        L_0x0063:
            if (r3 >= r5) goto L_0x00c9
            r2 = r9[r3]
            int r1 = X.AnonymousClass1Y3.AWJ
            X.0UN r0 = r8.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pP r0 = (X.C12460pP) r0
            boolean r0 = r0.A08(r2)
            if (r0 != 0) goto L_0x00c6
            X.0UN r0 = r8.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pP r1 = (X.C12460pP) r1
            android.app.Activity r0 = r8.A00
            boolean r0 = r1.A06(r0, r2)
            if (r0 != 0) goto L_0x00c6
            r0 = 0
        L_0x0088:
            r0 = r0 ^ r6
        L_0x0089:
            if (r0 == 0) goto L_0x012e
            com.facebook.runtimepermissions.RequestPermissionsConfig r3 = r8.A02
            android.os.Bundle r1 = new android.os.Bundle
            r1.<init>()
            java.lang.String r0 = "permissions"
            r1.putStringArray(r0, r9)
            java.lang.String r0 = "config"
            r1.putParcelable(r0, r3)
            com.facebook.runtimepermissions.RuntimePermissionsRationaleDialogFragment r6 = new com.facebook.runtimepermissions.RuntimePermissionsRationaleDialogFragment
            r6.<init>()
            r6.A1P(r1)
            X.2Be r0 = new X.2Be
            r0.<init>(r8, r9)
            r6.A03 = r0
            int r2 = X.AnonymousClass1Y3.AAN
            X.0UN r1 = r8.A01
            r0 = 2
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.3fi r4 = (X.C73223fi) r4
            java.lang.Integer r3 = X.AnonymousClass07B.A00
            java.lang.String r2 = "facebook"
            int r7 = r9.length
            r1 = 0
        L_0x00bc:
            if (r1 >= r7) goto L_0x0105
            r0 = r9[r1]
            r4.A02(r3, r0, r2)
            int r1 = r1 + 1
            goto L_0x00bc
        L_0x00c6:
            int r3 = r3 + 1
            goto L_0x0063
        L_0x00c9:
            r0 = 1
            goto L_0x0088
        L_0x00cb:
            r1 = 2
            r0 = 0
            if (r2 != r1) goto L_0x00d0
            r0 = 1
        L_0x00d0:
            if (r0 == 0) goto L_0x0103
            int r5 = r9.length
            r4 = 0
            r3 = 0
        L_0x00d5:
            if (r3 >= r5) goto L_0x0101
            r2 = r9[r3]
            int r1 = X.AnonymousClass1Y3.AWJ
            X.0UN r0 = r8.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pP r0 = (X.C12460pP) r0
            boolean r0 = r0.A08(r2)
            if (r0 != 0) goto L_0x00fe
            X.0UN r0 = r8.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pP r1 = (X.C12460pP) r1
            android.app.Activity r0 = r8.A00
            boolean r0 = r1.A07(r0, r2)
            if (r0 != 0) goto L_0x00fe
            r0 = 0
        L_0x00fa:
            if (r0 != 0) goto L_0x0103
        L_0x00fc:
            r0 = r6
            goto L_0x0089
        L_0x00fe:
            int r3 = r3 + 1
            goto L_0x00d5
        L_0x0101:
            r0 = 1
            goto L_0x00fa
        L_0x0103:
            r6 = 0
            goto L_0x00fc
        L_0x0105:
            int r2 = X.AnonymousClass1Y3.AkV
            X.0UN r1 = r8.A01
            r0 = 3
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.3fh r5 = (X.C73213fh) r5
            android.app.Activity r4 = r8.A00
            java.lang.String r3 = "IMPRESSION"
            java.lang.String r2 = "RATIONALE"
            r1 = 0
        L_0x0117:
            if (r1 >= r7) goto L_0x0121
            r0 = r9[r1]
            r5.A01(r3, r2, r0, r4)
            int r1 = r1 + 1
            goto L_0x0117
        L_0x0121:
            android.app.Activity r0 = r8.A00
            androidx.fragment.app.FragmentActivity r0 = (androidx.fragment.app.FragmentActivity) r0
            X.0qW r1 = r0.B4m()
            r0 = 0
            r6.A25(r1, r0)
            return
        L_0x012e:
            A01(r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04230Sz.ATw(java.lang.String[], com.facebook.runtimepermissions.RequestPermissionsConfig, X.3fj):void");
    }

    public void ATx(String[] strArr, C73233fj r3) {
        ATw(strArr, AnonymousClass0T0.A00, r3);
    }

    public boolean BBd(String str) {
        return ((C12460pP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWJ, this.A01)).A08(str);
    }

    public boolean BBf(String[] strArr) {
        return ((C12460pP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWJ, this.A01)).A09(strArr);
    }

    public C04230Sz(AnonymousClass1XY r3, Activity activity) {
        this.A01 = new AnonymousClass0UN(4, r3);
        this.A00 = activity;
    }

    public void ATu(String str, RequestPermissionsConfig requestPermissionsConfig, C73233fj r4) {
        ATw(new String[]{str}, requestPermissionsConfig, r4);
    }
}
