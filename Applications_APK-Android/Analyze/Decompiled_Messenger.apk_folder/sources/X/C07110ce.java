package X;

import android.app.Application;
import android.content.Context;

/* renamed from: X.0ce  reason: invalid class name and case insensitive filesystem */
public final class C07110ce implements C04310Tq {
    public final /* synthetic */ C28611f5 A00;

    public C07110ce(C28611f5 r1) {
        this.A00 = r1;
    }

    public Object get() {
        return (Application) ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, this.A00.A00));
    }
}
