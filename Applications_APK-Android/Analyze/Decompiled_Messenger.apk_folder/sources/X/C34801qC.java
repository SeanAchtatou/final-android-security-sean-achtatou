package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;

/* renamed from: X.1qC  reason: invalid class name and case insensitive filesystem */
public final class C34801qC {
    public static final C34801qC A03;
    public final ImmutableList A00;
    public final ImmutableList A01;
    public final String A02;

    static {
        ImmutableList immutableList = RegularImmutableList.A02;
        A03 = new C34801qC(BuildConfig.FLAVOR, immutableList, immutableList);
    }

    public C34801qC(String str, ImmutableList immutableList, ImmutableList immutableList2) {
        this.A02 = str;
        this.A00 = immutableList;
        this.A01 = immutableList2;
        Preconditions.checkArgument(immutableList.size() == this.A01.size());
    }
}
