package X;

import java.lang.annotation.Annotation;
import java.util.HashMap;

/* renamed from: X.1fs  reason: invalid class name and case insensitive filesystem */
public final class C29101fs extends C29111ft implements Comparable {
    public final C10140jc _annotationIntrospector;
    public C29131fv _ctorParameters;
    public C29131fv _fields;
    public final boolean _forSerialization;
    public C29131fv _getters;
    public final String _internalName;
    public final String _name;
    public C29131fv _setters;

    private static boolean _anyExplicitNames(C29131fv r1) {
        while (r1 != null) {
            String str = r1.explicitName;
            if (str != null && str.length() > 0) {
                return true;
            }
            r1 = r1.next;
        }
        return false;
    }

    public static C10090jX _mergeAnnotations(C29101fs r5, int i, C29131fv... r7) {
        HashMap hashMap;
        HashMap hashMap2;
        C10090jX r4 = ((C183512m) r7[i].value)._annotations;
        do {
            i++;
            if (i >= r7.length) {
                return r4;
            }
        } while (r7[i] == null);
        C10090jX _mergeAnnotations = _mergeAnnotations(r5, i, r7);
        if (r4 == null || (hashMap = r4._annotations) == null || hashMap.isEmpty()) {
            return _mergeAnnotations;
        }
        if (_mergeAnnotations == null || (hashMap2 = _mergeAnnotations._annotations) == null || hashMap2.isEmpty()) {
            return r4;
        }
        HashMap hashMap3 = new HashMap();
        for (Annotation annotation : _mergeAnnotations._annotations.values()) {
            hashMap3.put(annotation.annotationType(), annotation);
        }
        for (Annotation annotation2 : r4._annotations.values()) {
            hashMap3.put(annotation2.annotationType(), annotation2);
        }
        return new C10090jX(hashMap3);
    }

    public static C29131fv _removeNonVisible(C29131fv r3) {
        C29131fv withoutNonVisible;
        if (r3 == null) {
            return r3;
        }
        C29131fv r2 = r3.next;
        if (r2 == null) {
            withoutNonVisible = null;
        } else {
            C29131fv r0 = r2.next;
            if (r0 == null) {
                withoutNonVisible = null;
            } else {
                withoutNonVisible = r0.withoutNonVisible();
            }
            if (r2.isVisible) {
                withoutNonVisible = C29131fv.withNext(r2, withoutNonVisible);
            }
        }
        if (r3.isVisible) {
            return C29131fv.withNext(r3, withoutNonVisible);
        }
        return withoutNonVisible;
    }

    public static C29131fv findRenamed(C29101fs r3, C29131fv r4, C29131fv r5) {
        while (r4 != null) {
            String str = r4.explicitName;
            if (str != null && !str.equals(r3._name)) {
                if (r5 == null) {
                    r5 = r4;
                } else {
                    String str2 = r5.explicitName;
                    if (!str.equals(str2)) {
                        throw new IllegalStateException("Conflicting property name definitions: '" + str2 + "' (for " + r5.value + ") vs '" + r4.explicitName + "' (for " + r4.value + ")");
                    }
                }
            }
            r4 = r4.next;
        }
        return r5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        if (r0 != null) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r0 != null) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object fromMemberAnnotations(X.C30295EtZ r3) {
        /*
            r2 = this;
            X.0jc r0 = r2._annotationIntrospector
            r1 = 0
            if (r0 == 0) goto L_0x0023
            boolean r0 = r2._forSerialization
            if (r0 == 0) goto L_0x0024
            X.1fv r0 = r2._getters
            if (r0 == 0) goto L_0x0015
        L_0x000d:
            java.lang.Object r0 = r0.value
            X.12m r0 = (X.C183512m) r0
            java.lang.Object r1 = r3.withMember(r0)
        L_0x0015:
            if (r1 != 0) goto L_0x0023
            X.1fv r0 = r2._fields
            if (r0 == 0) goto L_0x0023
            java.lang.Object r0 = r0.value
            X.12m r0 = (X.C183512m) r0
            java.lang.Object r1 = r3.withMember(r0)
        L_0x0023:
            return r1
        L_0x0024:
            X.1fv r0 = r2._ctorParameters
            if (r0 == 0) goto L_0x0030
            java.lang.Object r0 = r0.value
            X.12m r0 = (X.C183512m) r0
            java.lang.Object r1 = r3.withMember(r0)
        L_0x0030:
            if (r1 != 0) goto L_0x0015
            X.1fv r0 = r2._setters
            if (r0 == 0) goto L_0x0015
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29101fs.fromMemberAnnotations(X.EtZ):java.lang.Object");
    }

    public void addAll(C29101fs r3) {
        C29131fv r0 = this._fields;
        C29131fv r1 = r3._fields;
        if (r0 == null) {
            r0 = r1;
        } else if (r1 != null) {
            r0 = C29131fv.append(r0, r1);
        }
        this._fields = r0;
        C29131fv r02 = this._ctorParameters;
        C29131fv r12 = r3._ctorParameters;
        if (r02 == null) {
            r02 = r12;
        } else if (r12 != null) {
            r02 = C29131fv.append(r02, r12);
        }
        this._ctorParameters = r02;
        C29131fv r03 = this._getters;
        C29131fv r13 = r3._getters;
        if (r03 == null) {
            r03 = r13;
        } else if (r13 != null) {
            r03 = C29131fv.append(r03, r13);
        }
        this._getters = r03;
        C29131fv r14 = this._setters;
        C29131fv r04 = r3._setters;
        if (r14 != null) {
            if (r04 == null) {
                r04 = r14;
            } else {
                r04 = C29131fv.append(r14, r04);
            }
        }
        this._setters = r04;
    }

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C29101fs r3 = (C29101fs) obj;
        if (this._ctorParameters != null) {
            if (r3._ctorParameters == null) {
                return -1;
            }
        } else if (r3._ctorParameters != null) {
            return 1;
        }
        return getName().compareTo(r3.getName());
    }

    public C860046h findReferenceType() {
        return (C860046h) fromMemberAnnotations(new C30294EtY(this));
    }

    public Class[] findViews() {
        return (Class[]) fromMemberAnnotations(new C30291EtV(this));
    }

    public AnonymousClass137 getConstructorParameter() {
        C29131fv r3 = this._ctorParameters;
        C29131fv r2 = r3;
        if (r3 == null) {
            return null;
        }
        do {
            AnonymousClass137 r1 = (AnonymousClass137) r3.value;
            if (r1._owner instanceof C29151fx) {
                return r1;
            }
            r3 = r3.next;
        } while (r3 != null);
        return (AnonymousClass137) r2.value;
    }

    public C29091fr getField() {
        C29131fv r0 = this._fields;
        if (r0 == null) {
            return null;
        }
        C29091fr r5 = (C29091fr) r0.value;
        C29131fv r4 = r0.next;
        while (r4 != null) {
            C29091fr r3 = (C29091fr) r4.value;
            Class declaringClass = r5.getDeclaringClass();
            Class declaringClass2 = r3.getDeclaringClass();
            if (declaringClass != declaringClass2) {
                if (declaringClass.isAssignableFrom(declaringClass2)) {
                    r5 = r3;
                } else if (declaringClass2.isAssignableFrom(declaringClass)) {
                }
                r4 = r4.next;
            }
            throw new IllegalArgumentException(AnonymousClass08S.A0U("Multiple fields representing property \"", getName(), "\": ", r5.getFullName(), " vs ", r3.getFullName()));
        }
        return r5;
    }

    public C29141fw getGetter() {
        C29131fv r0 = this._getters;
        if (r0 == null) {
            return null;
        }
        C29141fw r5 = (C29141fw) r0.value;
        C29131fv r4 = r0.next;
        while (r4 != null) {
            C29141fw r3 = (C29141fw) r4.value;
            Class declaringClass = r5.getDeclaringClass();
            Class declaringClass2 = r3.getDeclaringClass();
            if (declaringClass != declaringClass2) {
                if (declaringClass.isAssignableFrom(declaringClass2)) {
                    r5 = r3;
                } else if (declaringClass2.isAssignableFrom(declaringClass)) {
                }
                r4 = r4.next;
            }
            throw new IllegalArgumentException(AnonymousClass08S.A0U("Conflicting getter definitions for property \"", getName(), "\": ", r5.getFullName(), " vs ", r3.getFullName()));
        }
        return r5;
    }

    public String getName() {
        return this._name;
    }

    public C29141fw getSetter() {
        C29131fv r0 = this._setters;
        if (r0 == null) {
            return null;
        }
        C29141fw r5 = (C29141fw) r0.value;
        C29131fv r4 = r0.next;
        while (r4 != null) {
            C29141fw r3 = (C29141fw) r4.value;
            Class declaringClass = r5.getDeclaringClass();
            Class declaringClass2 = r3.getDeclaringClass();
            if (declaringClass != declaringClass2) {
                if (declaringClass.isAssignableFrom(declaringClass2)) {
                    r5 = r3;
                } else if (declaringClass2.isAssignableFrom(declaringClass)) {
                }
                r4 = r4.next;
            }
            throw new IllegalArgumentException(AnonymousClass08S.A0U("Conflicting setter definitions for property \"", getName(), "\": ", r5.getFullName(), " vs ", r3.getFullName()));
        }
        return r5;
    }

    public C87974Hw getWrapperName() {
        C183512m mutator;
        if (this._forSerialization) {
            mutator = getAccessor();
        } else {
            mutator = getMutator();
        }
        if (mutator == null) {
            return null;
        }
        C10140jc r0 = this._annotationIntrospector;
        return null;
    }

    public boolean hasConstructorParameter() {
        if (this._ctorParameters != null) {
            return true;
        }
        return false;
    }

    public boolean hasField() {
        if (this._fields != null) {
            return true;
        }
        return false;
    }

    public boolean hasGetter() {
        if (this._getters != null) {
            return true;
        }
        return false;
    }

    public boolean hasSetter() {
        if (this._setters != null) {
            return true;
        }
        return false;
    }

    public boolean isExplicitlyIncluded() {
        if (_anyExplicitNames(this._fields) || _anyExplicitNames(this._getters) || _anyExplicitNames(this._setters) || _anyExplicitNames(this._ctorParameters)) {
            return true;
        }
        return false;
    }

    public boolean isRequired() {
        Boolean bool = (Boolean) fromMemberAnnotations(new C30292EtW(this));
        if (bool == null || !bool.booleanValue()) {
            return false;
        }
        return true;
    }

    public boolean isTypeId() {
        Boolean bool = (Boolean) fromMemberAnnotations(new C30293EtX(this));
        if (bool == null || !bool.booleanValue()) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "[Property '" + this._name + "'; ctors: " + this._ctorParameters + ", field(s): " + this._fields + ", getter(s): " + this._getters + ", setter(s): " + this._setters + "]";
    }

    public C183512m getAccessor() {
        C29141fw getter = getGetter();
        if (getter == null) {
            return getField();
        }
        return getter;
    }

    public C183512m getMutator() {
        AnonymousClass137 constructorParameter = getConstructorParameter();
        if (constructorParameter != null) {
            return constructorParameter;
        }
        C29141fw setter = getSetter();
        if (setter == null) {
            return getField();
        }
        return setter;
    }

    public C29101fs(C29101fs r2, String str) {
        this._internalName = r2._internalName;
        this._name = str;
        this._annotationIntrospector = r2._annotationIntrospector;
        this._fields = r2._fields;
        this._ctorParameters = r2._ctorParameters;
        this._getters = r2._getters;
        this._setters = r2._setters;
        this._forSerialization = r2._forSerialization;
    }

    public C29101fs(String str, C10140jc r2, boolean z) {
        this._internalName = str;
        this._name = str;
        this._annotationIntrospector = r2;
        this._forSerialization = z;
    }
}
