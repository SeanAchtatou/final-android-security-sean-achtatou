package X;

import com.facebook.redex.dynamicanalysis.DynamicAnalysis;
import com.facebook.redex.dynamicanalysis.DynamicAnalysisTraceManager;
import io.card.payment.BuildConfig;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0Oy  reason: invalid class name and case insensitive filesystem */
public final class C03660Oy implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.redex.dynamicanalysis.DynamicAnalysisTraceManager$1";
    public final /* synthetic */ String A00;

    public C03660Oy(String str) {
        this.A00 = str;
    }

    public void run() {
        String str;
        String str2;
        boolean z;
        int i;
        short[][] sArr;
        String str3 = this.A00;
        if (str3 == "ColdStart") {
            str = DynamicAnalysis.A04;
            int i2 = DynamicAnalysis.A01;
            if (i2 != 0) {
                str = AnonymousClass08S.A0S("coldStartDDOrder:", Integer.toString(i2), ",", str);
            }
        } else {
            str = BuildConfig.FLAVOR;
        }
        String str4 = DynamicAnalysis.A05;
        if (0 == 1) {
            str2 = "TTI";
        } else if (0 == 2) {
            str2 = "DD";
        } else if (0 != 3) {
            str2 = BuildConfig.FLAVOR;
        } else {
            str2 = "TTI;DD";
        }
        if (!str2.isEmpty()) {
            str4 = AnonymousClass08S.A0P(str4, ";", str2);
        }
        try {
            Class.forName("com.facebook.zstd.ZstdOutputStream");
            z = true;
        } catch (Throwable unused) {
            z = false;
        }
        int i3 = DynamicAnalysis.sNumStaticallyInstrumented;
        if (i3 == 0) {
            i = 0;
        } else {
            int i4 = 0;
            for (short[] length : DynamicAnalysis.sMethodStatsArray) {
                i4 += length.length;
            }
            i = i4 / i3;
        }
        AnonymousClass0DR r4 = new AnonymousClass0DR(null, i3, i, AnonymousClass08S.A0P("dyna_", str3, ".txt"), str4, DynamicAnalysis.A03, DynamicAnalysis.A02, str, str3, z);
        if (DynamicAnalysis.A07) {
            sArr = DynamicAnalysis.sMethodStatsArray;
        } else {
            C010708t.A0J("DYNA", "Cold start data is not ready or already consumed");
            sArr = new short[0][];
        }
        if (new AnonymousClass0DS(sArr, r4.A02, r4.A03, r4.A07, r4.A09).A00()) {
            C010708t.A0P("DYNA|TraceManager", "Adding the parameters to the queue for %s", this.A00);
            DynamicAnalysisTraceManager.A01.put(this.A00, r4);
        } else {
            C010708t.A0P("DYNA|TraceManager", "There was a problem while writing data for %s", this.A00);
        }
        DynamicAnalysis.A06 = new AtomicInteger(0);
        DynamicAnalysis.A01 = 0;
        DynamicAnalysis.A05 = BuildConfig.FLAVOR;
        DynamicAnalysis.A03 = 0;
        DynamicAnalysis.A02 = 0;
        DynamicAnalysis.A04 = BuildConfig.FLAVOR;
        int i5 = 0;
        while (true) {
            short[][] sArr2 = DynamicAnalysis.sMethodStatsArray;
            if (i5 < sArr2.length) {
                int i6 = 0;
                while (true) {
                    short[] sArr3 = sArr2[i5];
                    if (i6 >= sArr3.length) {
                        break;
                    }
                    sArr3[i6] = 0;
                    i6++;
                }
                i5++;
            } else {
                DynamicAnalysisTraceManager.A02.release();
                return;
            }
        }
    }
}
