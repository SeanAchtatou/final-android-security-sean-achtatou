package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.common.util.TriState;
import com.facebook.contacts.protocol.push.ContactsMessengerUserMap;
import com.facebook.user.model.UserKey;

/* renamed from: X.0uG  reason: invalid class name and case insensitive filesystem */
public final class C14860uG implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C14860uG(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r11) {
        int A002 = AnonymousClass09Y.A00(1958091782);
        AnonymousClass0r6 r6 = this.A00;
        if (intent.hasExtra("extra_on_messenger_map")) {
            ContactsMessengerUserMap contactsMessengerUserMap = (ContactsMessengerUserMap) intent.getParcelableExtra("extra_on_messenger_map");
            C24971Xv it = contactsMessengerUserMap.A00.keySet().iterator();
            boolean z = false;
            while (it.hasNext()) {
                UserKey userKey = (UserKey) it.next();
                AnonymousClass0r6.A01(r6, userKey).A06 = TriState.valueOf(((Boolean) contactsMessengerUserMap.A00.get(userKey)).booleanValue());
                AnonymousClass0r6.A0A(r6, userKey);
                z = true;
            }
            if (z) {
                AnonymousClass0r6.A0C(r6, false);
            }
        }
        AnonymousClass09Y.A01(-228809048, A002);
    }
}
