package X;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

/* renamed from: X.0cc  reason: invalid class name and case insensitive filesystem */
public abstract class C07090cc {
    private static final String[] A02 = {"value"};
    public final ContentResolver A00;
    public final Uri A01;

    public String A02(AnonymousClass063 r10) {
        Cursor query = this.A00.query(this.A01, A02, "key=?", new String[]{r10.A05()}, null);
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    return query.getString(0);
                }
            } finally {
                query.close();
            }
        }
        if (query != null) {
            query.close();
        }
        return null;
    }

    public void A03(AnonymousClass063 r5) {
        this.A00.delete(this.A01, "key = ?", new String[]{r5.A05()});
    }

    public void A05(AnonymousClass063 r4, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("key", r4.A05());
        contentValues.put("value", str);
        this.A00.insert(this.A01, contentValues);
    }

    public C07090cc(ContentResolver contentResolver, Uri uri) {
        this.A00 = contentResolver;
        this.A01 = uri;
    }

    public long A01(AnonymousClass063 r3, long j) {
        String A022 = A02(r3);
        if (A022 == null) {
            return j;
        }
        try {
            return Long.parseLong(A022);
        } catch (NumberFormatException unused) {
            return j;
        }
    }

    public void A04(AnonymousClass063 r2, long j) {
        A05(r2, Long.toString(j));
    }

    public boolean A06(AnonymousClass063 r7, boolean z) {
        String A022 = A02(r7);
        if (A022 == null) {
            return z;
        }
        try {
            if (Long.parseLong(A022) != 0) {
                return true;
            }
            return false;
        } catch (NumberFormatException unused) {
            return z;
        }
    }
}
