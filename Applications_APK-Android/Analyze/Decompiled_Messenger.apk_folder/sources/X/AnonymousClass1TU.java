package X;

import com.facebook.inject.ContextScoped;
import com.facebook.messaging.attachments.VideoAttachmentData;
import com.facebook.messaging.model.attachment.Attachment;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MontageFeedbackOverlay;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.montagemetadata.MontageMetadata;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.MontageThreadPreview;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.messaging.montage.model.MontageBucketKey;
import com.facebook.messaging.montage.model.MontageCard;
import com.facebook.messaging.montage.model.MontageThreadInfo;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@ContextScoped
/* renamed from: X.1TU  reason: invalid class name */
public final class AnonymousClass1TU {
    private static C04470Uu A02;
    public AnonymousClass0UN A00;
    private final C04310Tq A01;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        if (r1.equals(r4.A0q) == false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(com.google.common.collect.ImmutableList r7, com.facebook.messaging.model.messages.Message r8) {
        /*
            r6 = -1
            if (r7 == 0) goto L_0x0039
            if (r8 == 0) goto L_0x0039
            r5 = 0
        L_0x0006:
            int r0 = r7.size()
            if (r5 >= r0) goto L_0x0039
            java.lang.Object r0 = r7.get(r5)
            com.facebook.messaging.montage.model.MontageCard r0 = (com.facebook.messaging.montage.model.MontageCard) r0
            com.facebook.messaging.model.messages.Message r4 = r0.A02
            java.lang.String r1 = r8.A0q
            r3 = 1
            if (r1 == 0) goto L_0x0022
            java.lang.String r0 = r4.A0q
            boolean r0 = r1.equals(r0)
            r2 = 1
            if (r0 != 0) goto L_0x0023
        L_0x0022:
            r2 = 0
        L_0x0023:
            java.lang.String r1 = r8.A0w
            if (r1 == 0) goto L_0x0036
            java.lang.String r0 = r4.A0w
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0036
        L_0x002f:
            if (r2 != 0) goto L_0x0038
            if (r3 != 0) goto L_0x0038
            int r5 = r5 + 1
            goto L_0x0006
        L_0x0036:
            r3 = 0
            goto L_0x002f
        L_0x0038:
            return r5
        L_0x0039:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TU.A00(com.google.common.collect.ImmutableList, com.facebook.messaging.model.messages.Message):int");
    }

    public static int A01(List list) {
        for (int i = 0; i < list.size(); i++) {
            if (((MontageCard) list.get(i)).A08) {
                return i;
            }
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r1 != false) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C47342Ul A04(X.AnonymousClass1TU r4, com.facebook.messaging.model.messages.Message r5) {
        /*
            r3 = 0
            if (r5 == 0) goto L_0x009d
            com.google.common.collect.ImmutableList r0 = r5.A0c
            if (r0 == 0) goto L_0x000e
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 == 0) goto L_0x000f
        L_0x000e:
            r0 = 0
        L_0x000f:
            if (r0 != 0) goto L_0x009d
            X.2yU r0 = r5.A06
            if (r0 == 0) goto L_0x001b
            boolean r0 = X.C28841fS.A0O(r5)
            if (r0 == 0) goto L_0x009d
        L_0x001b:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0U
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r0)
            if (r0 != 0) goto L_0x009d
            r2 = 5
            int r1 = X.AnonymousClass1Y3.BRG
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0yl r0 = (X.C17350yl) r0
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 282501482350111(0x100ef0081061f, double:1.39574277328417E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0057
            com.facebook.messaging.model.montagemetadata.MontageMetadata r1 = r5.A0O
            if (r1 == 0) goto L_0x0057
            java.lang.String r0 = r1.Auy()
            if (r0 == 0) goto L_0x0057
            java.lang.String r0 = r1.Auy()
            X.2Ul r0 = X.C47342Ul.valueOf(r0)
            return r0
        L_0x0057:
            int r2 = X.AnonymousClass1Y3.BPD
            X.0UN r1 = r4.A00
            r0 = 4
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.18P r0 = (X.AnonymousClass18P) r0
            X.2VE r0 = r0.A02(r5)
            int r0 = r0.ordinal()
            r1 = 1
            switch(r0) {
                case 5: goto L_0x0082;
                case 6: goto L_0x0088;
                case 7: goto L_0x006e;
                case 8: goto L_0x0085;
                case 9: goto L_0x006e;
                case 10: goto L_0x006f;
                default: goto L_0x006e;
            }
        L_0x006e:
            return r3
        L_0x006f:
            com.google.common.collect.ImmutableList r0 = r5.A0b
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x009d
            com.google.common.collect.ImmutableList r0 = r5.A0X
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x009d
            X.2Ul r3 = X.C47342Ul.TEXT
            return r3
        L_0x0082:
            X.2Ul r0 = X.C47342Ul.STICKER
            return r0
        L_0x0085:
            X.2Ul r0 = X.C47342Ul.VIDEO
            return r0
        L_0x0088:
            com.google.common.collect.ImmutableList r0 = r5.A0b
            int r0 = r0.size()
            if (r0 > r1) goto L_0x0099
            com.google.common.collect.ImmutableList r0 = r5.A0X
            int r0 = r0.size()
            if (r0 > r1) goto L_0x0099
            r1 = 0
        L_0x0099:
            if (r1 != 0) goto L_0x009d
            X.2Ul r3 = X.C47342Ul.PHOTO
        L_0x009d:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TU.A04(X.1TU, com.facebook.messaging.model.messages.Message):X.2Ul");
    }

    public static boolean A0B(AnonymousClass1TU r4, Message message, long j) {
        if (message != null) {
            boolean z = false;
            if (A04(r4, message) != null) {
                z = true;
            }
            if (z) {
                boolean z2 = true;
                if (message != null && message.A03 + A03(message) > j) {
                    z2 = false;
                }
                return !z2;
            }
        }
        return false;
    }

    public static boolean A0C(MediaResource mediaResource) {
        if (mediaResource != null) {
            switch (mediaResource.A0L.ordinal()) {
                case 0:
                case 1:
                    return true;
            }
        }
        return false;
    }

    public MontageThreadPreview A0F(ImmutableList immutableList) {
        Message A0E;
        C42752Br A002;
        MediaResource mediaResource;
        Attachment attachment = null;
        if (immutableList == null) {
            A0E = null;
        } else {
            A0E = A0E(immutableList, false);
        }
        if (A0E == null || A0E.A0q == null || (A002 = MontageThreadPreview.A00(A0E.A0D)) == null) {
            return null;
        }
        if (C28841fS.A0O(A0E)) {
            mediaResource = C28841fS.A07(A0E);
        } else if (!A0E.A0b.isEmpty()) {
            mediaResource = (MediaResource) A0E.A0b.get(0);
        } else {
            mediaResource = null;
        }
        MontageMetadata montageMetadata = A0E.A0O;
        if (montageMetadata == null) {
            montageMetadata = null;
        }
        AnonymousClass2VF r6 = new AnonymousClass2VF(A0E.A0q, A002, A0E.A03, A0E.A0K);
        r6.A06 = A0E.A0z;
        r6.A07 = A0E.A10;
        if (!A0E.A0X.isEmpty()) {
            attachment = (Attachment) A0E.A0X.get(0);
        }
        r6.A01 = attachment;
        r6.A04 = mediaResource;
        r6.A03 = montageMetadata;
        return new MontageThreadPreview(r6);
    }

    public boolean A0L(ThreadSummary threadSummary, ImmutableList immutableList) {
        if (!(threadSummary == null || immutableList == null || immutableList.isEmpty())) {
            long now = ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, this.A00)).now();
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                Message message = (Message) it.next();
                if (A0B(this, message, now) && MontageCard.A00(message)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static long A03(Message message) {
        Integer num = message.A0l;
        if (num == null || message.A14) {
            C010708t.A0I("MontageMessagesHelper", "Message with no MessageLifetime, defaulting to 1 day");
            return 86400000;
        }
        int intValue = num.intValue();
        if (intValue == 0) {
            return 3144960000000L;
        }
        return (long) intValue;
    }

    public static final AnonymousClass1TU A05(AnonymousClass1XY r4) {
        AnonymousClass1TU r0;
        synchronized (AnonymousClass1TU.class) {
            C04470Uu A002 = C04470Uu.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass1TU((AnonymousClass1XY) A02.A01());
                }
                C04470Uu r1 = A02;
                r0 = (AnonymousClass1TU) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    private ImmutableList.Builder A06(ImmutableList immutableList, boolean z) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, this.A00)).now();
        ArrayList arrayList = new ArrayList();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            Message message = (Message) it.next();
            if (A0B(this, message, now)) {
                arrayList.add(message);
            }
        }
        Collections.reverse(arrayList);
        C24971Xv it2 = ImmutableList.copyOf((Collection) arrayList).iterator();
        while (it2.hasNext()) {
            Message message2 = (Message) it2.next();
            if ((!C06850cB.A0D(message2.A0u, "group", "event")) && (z || !C32741mG.A03.toString().equals(message2.A0u))) {
                builder.add((Object) message2);
            }
        }
        return builder;
    }

    public static boolean A07(Message message) {
        if (message.A0s == null || !C98894o1.MESSAGE_REACTION.equals(message.A0F)) {
            return false;
        }
        return true;
    }

    public static boolean A08(Message message) {
        MontageMetadata montageMetadata = message.A0O;
        if (montageMetadata == null || !montageMetadata.ArM().booleanValue()) {
            return false;
        }
        return true;
    }

    public static boolean A09(MontageCard montageCard) {
        ImmutableList immutableList = montageCard.A02.A0Z;
        if (C013509w.A02(immutableList)) {
            return false;
        }
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            if (((MontageFeedbackOverlay) it.next()).A00 != null) {
                return true;
            }
        }
        return false;
    }

    public static boolean A0A(MontageCard montageCard) {
        ImmutableList immutableList = montageCard.A02.A0Z;
        if (C013509w.A02(immutableList)) {
            return false;
        }
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            if (((MontageFeedbackOverlay) it.next()).A01 != null) {
                return true;
            }
        }
        return false;
    }

    public Message A0D(MontageThreadInfo montageThreadInfo) {
        if (montageThreadInfo == null) {
            return null;
        }
        return A0E(montageThreadInfo.A02, false);
    }

    public Message A0E(ImmutableList immutableList, boolean z) {
        Message message;
        Message message2;
        if (immutableList == null || immutableList.isEmpty()) {
            return null;
        }
        ImmutableList.Builder A06 = A06(immutableList, z);
        if (!((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, ((C17350yl) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BRG, this.A00)).A00)).Aem(282501480121858L) || ((C12130oa) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B8a, this.A00)).A04(((Message) immutableList.get(0)).A0U)) {
            ImmutableList<Message> build = A06.build();
            message = null;
            if (build != null) {
                message2 = null;
                for (Message message3 : build) {
                    if (message2 == null || message3.A03 > message2.A03) {
                        message2 = message3;
                    }
                    if (A08(message3) && (message == null || message3.A03 > message.A03)) {
                        message = message3;
                    }
                }
                if (message != null) {
                    return message;
                }
            }
            return message;
        }
        ImmutableList<Message> build2 = A06.build();
        message2 = null;
        if (build2 != null) {
            message = null;
            for (Message message4 : build2) {
                if (message == null || message4.A03 < message.A03) {
                    message = message4;
                }
                if (A08(message4) && (message2 == null || message4.A03 < message2.A03)) {
                    message2 = message4;
                }
            }
            if (message2 != null) {
                return message2;
            }
            return message;
        }
        return message2;
    }

    public BasicMontageThreadInfo A0G(MontageThreadInfo montageThreadInfo, boolean z, List list, boolean z2, int i) {
        Message A0E;
        long j;
        ImmutableList copyOf;
        long j2;
        ImmutableList copyOf2;
        ImmutableList immutableList;
        MontageThreadInfo montageThreadInfo2 = montageThreadInfo;
        if (montageThreadInfo == null) {
            A0E = null;
        } else {
            A0E = A0E(montageThreadInfo2.A02, z2);
        }
        boolean z3 = false;
        boolean z4 = z;
        int i2 = i;
        if (((C16510xE) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ADT, this.A00)).A0L()) {
            if (A0E != null) {
                String str = A0E.A0K.A03;
                if (!z && MontageCard.A00(A0E)) {
                    z3 = true;
                }
                long A012 = ((C16510xE) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ADT, this.A00)).A01();
                ImmutableList immutableList2 = null;
                if (!(montageThreadInfo == null || (immutableList = montageThreadInfo2.A02) == null || immutableList.isEmpty())) {
                    long min = Math.min(A012, (long) immutableList.size());
                    ImmutableList.Builder A06 = A06(immutableList, false);
                    ImmutableList.Builder builder = new ImmutableList.Builder();
                    TreeMap treeMap = new TreeMap(Collections.reverseOrder());
                    TreeMap treeMap2 = new TreeMap(Collections.reverseOrder());
                    C24971Xv it = A06.build().iterator();
                    while (it.hasNext()) {
                        Message message = (Message) it.next();
                        if (A08(message)) {
                            treeMap.put(Long.valueOf(message.A03), message);
                        } else {
                            treeMap2.put(Long.valueOf(message.A03), message);
                        }
                    }
                    Iterator it2 = treeMap.entrySet().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            for (Map.Entry value : treeMap2.entrySet()) {
                                builder.add(value.getValue());
                                min--;
                                if (min <= 0) {
                                    break;
                                }
                            }
                        } else {
                            builder.add(((Map.Entry) it2.next()).getValue());
                            min--;
                            if (min <= 0) {
                                break;
                            }
                        }
                    }
                    immutableList2 = builder.build();
                }
                UserKey userKey = A0E.A0K.A01;
                ThreadKey threadKey = A0E.A0U;
                if (threadKey == null) {
                    j2 = 0;
                } else {
                    j2 = threadKey.A03;
                }
                MontageBucketKey montageBucketKey = new MontageBucketKey(j2);
                if (list == null) {
                    copyOf2 = null;
                } else {
                    copyOf2 = ImmutableList.copyOf((Collection) list);
                }
                return new BasicMontageThreadInfo(userKey, montageBucketKey, A0E, immutableList2, copyOf2, str, z3, z4, i2);
            }
        } else if (A0E != null) {
            ParticipantInfo participantInfo = A0E.A0K;
            String str2 = participantInfo.A03;
            if (!z && MontageCard.A00(A0E)) {
                z3 = true;
            }
            UserKey userKey2 = participantInfo.A01;
            ThreadKey threadKey2 = A0E.A0U;
            if (threadKey2 == null) {
                j = 0;
            } else {
                j = threadKey2.A03;
            }
            MontageBucketKey montageBucketKey2 = new MontageBucketKey(j);
            if (list == null) {
                copyOf = null;
            } else {
                copyOf = ImmutableList.copyOf((Collection) list);
            }
            return new BasicMontageThreadInfo(userKey2, montageBucketKey2, A0E, null, copyOf, str2, z3, z4, i2);
        }
        return null;
    }

    public boolean A0K(Message message) {
        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, this.A00)).now();
        if (message == null || message.A03 + A03(message) <= now) {
            return true;
        }
        return false;
    }

    public boolean A0M(MontageThreadInfo montageThreadInfo) {
        if (montageThreadInfo == null || !A0L(montageThreadInfo.A01, montageThreadInfo.A02)) {
            return false;
        }
        return true;
    }

    public boolean A0N(ImmutableList immutableList) {
        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, this.A00)).now();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            if (A0B(this, (Message) it.next(), now)) {
                return true;
            }
        }
        return false;
    }

    private AnonymousClass1TU(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(11, r3);
        this.A01 = AnonymousClass0XJ.A0I(r3);
    }

    private long A02(Message message) {
        C47342Ul A04 = A04(this, message);
        if (A04 == null) {
            return 0;
        }
        switch (A04.ordinal()) {
            case 0:
            case 2:
            case 3:
                return 6000;
            case 1:
                VideoAttachmentData A0C = ((C416926p) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANn, this.A00)).A0C(message);
                if (A0C != null) {
                    return (long) A0C.A00;
                }
                return 6000;
            default:
                return 0;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0018, code lost:
        if (r3 == false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.montage.model.MontageCard A0H(com.facebook.messaging.model.messages.Message r8, boolean r9) {
        /*
            r7 = this;
            X.2Ul r4 = A04(r7, r8)
            X.AnonymousClass0qX.A00(r4)
            long r1 = r7.A02(r8)
            com.facebook.messaging.model.montagemetadata.MontageMetadata r0 = r8.A0O
            if (r0 == 0) goto L_0x001a
            java.lang.Boolean r0 = r0.ArB()
            boolean r3 = r0.booleanValue()
            r0 = 1
            if (r3 != 0) goto L_0x001b
        L_0x001a:
            r0 = 0
        L_0x001b:
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r0)
            X.3uj r5 = new X.3uj
            r5.<init>()
            r5.A03 = r4
            r5.A02 = r8
            java.lang.String r0 = r8.A0q
            r5.A07 = r0
            long r3 = r8.A03
            r5.A01 = r3
            com.google.common.collect.ImmutableMultimap r0 = r8.A0j
            r5.A06 = r0
            boolean r0 = r6.booleanValue()
            r5.A08 = r0
            r5.A00 = r1
            r5.A09 = r9
            com.facebook.messaging.montage.model.MontageCard r0 = r5.A00()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TU.A0H(com.facebook.messaging.model.messages.Message, boolean):com.facebook.messaging.montage.model.MontageCard");
    }

    public ImmutableList A0I(Message message, ThreadSummary threadSummary) {
        if (C28841fS.A14(message) || C28841fS.A0V(message)) {
            return RegularImmutableList.A02;
        }
        return ImmutableList.copyOf((Collection) C626632v.A00(message, threadSummary, (UserKey) this.A01.get()).first);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v3, types: [com.facebook.messaging.montage.model.MontageCard] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A0J(com.facebook.messaging.montage.model.MontageThreadInfo r14) {
        /*
            r13 = this;
            com.google.common.collect.ImmutableList$Builder r5 = com.google.common.collect.ImmutableList.builder()
            com.google.common.collect.ImmutableList r0 = r14.A02
            X.1Xv r12 = r0.iterator()
        L_0x000a:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x00ee
            java.lang.Object r10 = r12.next()
            com.facebook.messaging.model.messages.Message r10 = (com.facebook.messaging.model.messages.Message) r10
            com.facebook.messaging.model.threads.ThreadSummary r8 = r14.A01
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r10.A0U
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0S
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            com.google.common.base.Preconditions.checkArgument(r0)
            X.2Ul r6 = A04(r13, r10)
            r9 = 0
            if (r6 == 0) goto L_0x005c
            long r1 = r13.A02(r10)
            r3 = 0
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x0067
            int r4 = X.AnonymousClass1Y3.Amr
            X.0UN r3 = r13.A00
            r0 = 3
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r4, r3)
            X.09P r4 = (X.AnonymousClass09P) r4
            java.lang.String r8 = r10.A0q
            com.facebook.messaging.model.threadkey.ThreadKey r7 = r10.A0U
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r10.A0K
            if (r0 != 0) goto L_0x0062
            java.lang.String r3 = "unknown"
        L_0x0049:
            java.lang.Long r0 = java.lang.Long.valueOf(r1)
            java.lang.Object[] r1 = new java.lang.Object[]{r6, r8, r7, r3, r0}
            java.lang.String r0 = "Encountered %s message (id %s of thread %s by user %s) with invalid duration %d."
            java.lang.String r1 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1)
            java.lang.String r0 = "MontageMessagesHelper"
            r4.CGS(r0, r1)
        L_0x005c:
            if (r9 == 0) goto L_0x000a
            r5.add(r9)
            goto L_0x000a
        L_0x0062:
            java.lang.String r3 = r0.A00()
            goto L_0x0049
        L_0x0067:
            r4 = 10
            int r3 = X.AnonymousClass1Y3.B8a
            X.0UN r0 = r13.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r0)
            X.0oa r3 = (X.C12130oa) r3
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0U
            boolean r3 = r3.A04(r0)
            X.3uj r4 = new X.3uj
            r4.<init>()
            r4.A03 = r6
            r4.A02 = r10
            java.lang.String r0 = r10.A0q
            r4.A07 = r0
            long r6 = r10.A03
            r4.A01 = r6
            com.google.common.collect.ImmutableMultimap r0 = r10.A0j
            r4.A06 = r0
            r4.A00 = r1
            boolean r0 = com.facebook.messaging.montage.model.MontageCard.A00(r10)
            r4.A09 = r0
            r4.A08 = r3
            if (r3 == 0) goto L_0x009e
            com.google.common.collect.ImmutableList r9 = r13.A0I(r10, r8)
        L_0x009e:
            r4.A04 = r9
            if (r3 == 0) goto L_0x00df
            boolean r0 = X.C28841fS.A14(r10)
            if (r0 != 0) goto L_0x00df
            boolean r0 = X.C28841fS.A0V(r10)
            if (r0 != 0) goto L_0x00df
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            com.google.common.collect.ImmutableList r0 = r8.A0m
            X.1Xv r2 = r0.iterator()
        L_0x00b9:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00e2
            java.lang.Object r0 = r2.next()
            com.facebook.messaging.model.threads.ThreadParticipant r0 = (com.facebook.messaging.model.threads.ThreadParticipant) r0
            com.facebook.user.model.UserKey r1 = r0.A00()
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r0.A04
            java.lang.String r0 = r0.A03
            if (r0 != 0) goto L_0x00de
            java.lang.String r0 = ""
        L_0x00d1:
            X.44g r1 = com.facebook.messaging.montage.model.MontageUser.A00(r1, r0)
            com.facebook.messaging.montage.model.MontageUser r0 = new com.facebook.messaging.montage.model.MontageUser
            r0.<init>(r1)
            r3.add(r0)
            goto L_0x00b9
        L_0x00de:
            goto L_0x00d1
        L_0x00df:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            goto L_0x00e6
        L_0x00e2:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r3)
        L_0x00e6:
            r4.A05 = r0
            com.facebook.messaging.montage.model.MontageCard r9 = r4.A00()
            goto L_0x005c
        L_0x00ee:
            com.google.common.collect.ImmutableList r0 = r5.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TU.A0J(com.facebook.messaging.montage.model.MontageThreadInfo):com.google.common.collect.ImmutableList");
    }
}
