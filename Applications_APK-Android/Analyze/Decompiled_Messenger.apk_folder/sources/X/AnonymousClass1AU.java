package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.inbox2.items.InboxTrackableItem;

@UserScoped
/* renamed from: X.1AU  reason: invalid class name */
public final class AnonymousClass1AU implements AnonymousClass1AZ {
    private static C05540Zi A01;
    private final C20091Aw A00;

    public void BMG(C24201Sr r1) {
    }

    public void BMH(C24201Sr r1) {
    }

    public boolean CEL() {
        return false;
    }

    public static final AnonymousClass1AU A00(AnonymousClass1XY r4) {
        AnonymousClass1AU r0;
        synchronized (AnonymousClass1AU.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new AnonymousClass1AU((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass1AU) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public C33901oK Apl() {
        return C33901oK.A0M;
    }

    public void C8h(boolean z) {
        C20091Aw r0 = this.A00;
        r0.A01 = z;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, r0.A06.A00)).A01("inbox_ad_surface_visibility"), 283);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A08("is_surface_visible", Boolean.valueOf(z));
            uSLEBaseShape0S0000000.A0O("messenger_inbox_ads");
            uSLEBaseShape0S0000000.A06();
        }
    }

    public void CKW(C24201Sr r4, boolean z) {
        this.A00.A04(((InboxTrackableItem) r4.A05).ArZ(), z);
    }

    private AnonymousClass1AU(AnonymousClass1XY r2) {
        this.A00 = C20091Aw.A00(r2);
    }
}
