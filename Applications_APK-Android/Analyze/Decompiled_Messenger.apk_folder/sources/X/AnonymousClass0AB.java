package X;

import android.content.Context;
import android.text.TextUtils;

/* renamed from: X.0AB  reason: invalid class name */
public final class AnonymousClass0AB {
    /* JADX WARNING: Can't wrap try/catch for region: R(6:70|69|71|72|73|74) */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0113, code lost:
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x00ff */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass0AC A00(android.content.Context r7, java.lang.String r8, int r9, X.C009207y r10) {
        /*
            java.lang.String r4 = "RtiGracefulSystemMethodHelper"
            X.0AC r2 = new X.0AC
            r2.<init>(r8)
            android.content.pm.PackageManager r0 = r7.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r8, r9)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            r2.A01 = r0     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x00c4
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x00c4
            boolean r0 = r0.enabled     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x00a3
            java.lang.String r0 = "com.facebook.services"
            boolean r3 = r0.equals(r8)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            r0 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r3 != 0) goto L_0x0081
            java.lang.String r0 = "com.facebook.services.dev"
            boolean r0 = r0.equals(r8)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 != 0) goto L_0x0081
            android.content.Intent r5 = new android.content.Intent     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            java.lang.String r0 = "com.facebook.rti.fbns.intent.RECEIVE"
            r5.<init>(r0)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            android.content.pm.PackageManager r3 = r7.getPackageManager()     // Catch:{ RuntimeException -> 0x0041 }
            r0 = 0
            java.util.List r3 = r3.queryBroadcastReceivers(r5, r0)     // Catch:{ RuntimeException -> 0x0041 }
            goto L_0x0059
        L_0x0041:
            r6 = move-exception
            java.lang.String r0 = "Failed to queryBroadcastReceivers"
            X.C010708t.A0R(r4, r6, r0)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            X.09P r3 = r10.A00     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r3 == 0) goto L_0x0050
            java.lang.String r0 = "queryBroadcastReceivers"
            r3.softReport(r4, r0, r6)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
        L_0x0050:
            java.lang.Throwable r0 = r6.getCause()     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            boolean r0 = r0 instanceof android.os.DeadObjectException     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x005c
            r3 = 0
        L_0x0059:
            if (r3 == 0) goto L_0x0094
            goto L_0x005d
        L_0x005c:
            throw r6     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
        L_0x005d:
            boolean r0 = r3.isEmpty()     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 != 0) goto L_0x0094
            java.util.Iterator r3 = r3.iterator()     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
        L_0x0067:
            boolean r0 = r3.hasNext()     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x008e
            java.lang.Object r0 = r3.next()     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x0067
            android.content.pm.ActivityInfo r0 = r0.activityInfo     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x0067
            java.lang.String r0 = r0.packageName     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            boolean r0 = r8.equals(r0)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x0067
        L_0x0081:
            java.lang.Boolean r0 = java.lang.Boolean.TRUE     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            boolean r0 = r0.equals(r1)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x0096
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            r2.A02 = r0     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            goto L_0x00c4
        L_0x008e:
            r0 = 0
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            goto L_0x0081
        L_0x0094:
            r1 = 0
            goto L_0x0081
        L_0x0096:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            boolean r0 = r0.equals(r1)     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            if (r0 == 0) goto L_0x00c4
            java.lang.Integer r0 = X.AnonymousClass07B.A0N     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            r2.A02 = r0     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            goto L_0x00c4
        L_0x00a3:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            r2.A02 = r0     // Catch:{ NameNotFoundException -> 0x00c0, RuntimeException -> 0x00a8 }
            goto L_0x00c4
        L_0x00a8:
            r3 = move-exception
            java.lang.String r0 = "Failed to getPackageInfo"
            X.C010708t.A0R(r4, r3, r0)
            X.09P r1 = r10.A00
            if (r1 == 0) goto L_0x00b7
            java.lang.String r0 = "getPackageInfo"
            r1.softReport(r4, r0, r3)
        L_0x00b7:
            java.lang.Throwable r0 = r3.getCause()
            boolean r0 = r0 instanceof android.os.DeadObjectException
            if (r0 != 0) goto L_0x00c4
            throw r3
        L_0x00c0:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r2.A02 = r0
        L_0x00c4:
            r0 = r9 & 64
            if (r0 == 0) goto L_0x012c
            java.lang.Integer r1 = r2.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            if (r1 != r0) goto L_0x012c
            android.content.pm.PackageInfo r0 = r2.A01
            if (r0 == 0) goto L_0x012c
            android.content.pm.PackageInfo r0 = r2.A01
            android.content.pm.Signature[] r3 = r0.signatures
            if (r3 == 0) goto L_0x0115
            int r1 = r3.length
            r0 = 1
            if (r1 != r0) goto L_0x0115
            r0 = 0
            r0 = r3[r0]
            byte[] r6 = r0.toByteArray()
            java.lang.String r5 = "SHA-1"
            java.security.MessageDigest r3 = java.security.MessageDigest.getInstance(r5)     // Catch:{ NoSuchAlgorithmException -> 0x00ea }
            goto L_0x0103
        L_0x00ea:
            java.lang.String r0 = "org.apache.harmony.security.fortress.Services"
            java.lang.Class r3 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x00ff }
            java.lang.String r1 = "setNeedRefresh"
            r4 = 0
            java.lang.Class[] r0 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x00ff }
            java.lang.reflect.Method r3 = r3.getMethod(r1, r0)     // Catch:{ Exception -> 0x00ff }
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x00ff }
            r3.invoke(r1, r0)     // Catch:{ Exception -> 0x00ff }
        L_0x00ff:
            java.security.MessageDigest r3 = java.security.MessageDigest.getInstance(r5)     // Catch:{ NoSuchAlgorithmException -> 0x0113 }
        L_0x0103:
            r1 = 0
            int r0 = r6.length     // Catch:{ NoSuchAlgorithmException -> 0x0113 }
            r3.update(r6, r1, r0)     // Catch:{ NoSuchAlgorithmException -> 0x0113 }
            byte[] r1 = r3.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0113 }
            r0 = 11
            java.lang.String r1 = android.util.Base64.encodeToString(r1, r0)     // Catch:{ NoSuchAlgorithmException -> 0x0113 }
            goto L_0x0116
        L_0x0113:
            r1 = 0
            goto L_0x0116
        L_0x0115:
            r1 = 0
        L_0x0116:
            X.0Aw r0 = X.C01600Aw.A00(r7)
            boolean r0 = r0.A02
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x012d
            java.util.Set r0 = X.C03030Hp.A01
        L_0x0122:
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x0130
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            r2.A02 = r0
        L_0x012c:
            return r2
        L_0x012d:
            java.util.Set r0 = X.C03030Hp.A00
            goto L_0x0122
        L_0x0130:
            java.lang.Integer r0 = X.AnonymousClass07B.A0i
            r2.A02 = r0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AB.A00(android.content.Context, java.lang.String, int, X.07y):X.0AC");
    }

    public static boolean A01(Context context, String str, C009207y r5) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (!str.equals(context.getPackageName()) && A00(context, str, 64, r5).A02 != AnonymousClass07B.A0n) {
            return false;
        }
        return true;
    }
}
