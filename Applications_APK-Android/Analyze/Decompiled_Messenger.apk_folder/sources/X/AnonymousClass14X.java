package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;

/* renamed from: X.14X  reason: invalid class name */
public abstract class AnonymousClass14X extends C08880g8 implements ListenableFuture {
    public abstract ListenableFuture A03();

    public void addListener(Runnable runnable, Executor executor) {
        A03().addListener(runnable, executor);
    }
}
