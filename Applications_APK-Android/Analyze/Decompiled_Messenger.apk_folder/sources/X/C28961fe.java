package X;

import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.1fe  reason: invalid class name and case insensitive filesystem */
public final class C28961fe extends C14680to {
    private static final BigDecimal MAX_INTEGER = BigDecimal.valueOf(2147483647L);
    private static final BigDecimal MAX_LONG = BigDecimal.valueOf(Long.MAX_VALUE);
    private static final BigDecimal MIN_INTEGER = BigDecimal.valueOf(-2147483648L);
    private static final BigDecimal MIN_LONG = BigDecimal.valueOf(Long.MIN_VALUE);
    public static final C28961fe ZERO = new C28961fe(BigDecimal.ZERO);
    public final BigDecimal _value;

    public boolean equals(Object obj) {
        if (obj != this) {
            return obj != null && obj.getClass() == getClass() && ((C28961fe) obj)._value.compareTo(this._value) == 0;
        }
        return true;
    }

    public String asText() {
        return this._value.toString();
    }

    public C182811d asToken() {
        return C182811d.VALUE_NUMBER_FLOAT;
    }

    public BigInteger bigIntegerValue() {
        return this._value.toBigInteger();
    }

    public boolean canConvertToInt() {
        if (this._value.compareTo(MIN_INTEGER) < 0 || this._value.compareTo(MAX_INTEGER) > 0) {
            return false;
        }
        return true;
    }

    public BigDecimal decimalValue() {
        return this._value;
    }

    public double doubleValue() {
        return this._value.doubleValue();
    }

    public float floatValue() {
        return this._value.floatValue();
    }

    public int hashCode() {
        return Double.valueOf(this._value.doubleValue()).hashCode();
    }

    public int intValue() {
        return this._value.intValue();
    }

    public long longValue() {
        return this._value.longValue();
    }

    public C29501gW numberType() {
        return C29501gW.BIG_DECIMAL;
    }

    public Number numberValue() {
        return this._value;
    }

    public final void serialize(C11710np r2, C11260mU r3) {
        if (!r3.isEnabled(C10480kE.WRITE_BIGDECIMAL_AS_PLAIN) || (r2 instanceof C11700no)) {
            r2.writeNumber(this._value);
        } else {
            r2.writeNumber(this._value.toPlainString());
        }
    }

    public C28961fe(BigDecimal bigDecimal) {
        this._value = bigDecimal;
    }
}
