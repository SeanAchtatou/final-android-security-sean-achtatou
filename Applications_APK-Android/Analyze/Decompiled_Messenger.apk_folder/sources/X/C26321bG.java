package X;

/* renamed from: X.1bG  reason: invalid class name and case insensitive filesystem */
public final class C26321bG {
    public C26461bU A00;
    public C26461bU A01;
    public C26461bU A02;
    public C26461bU A03;
    public C26461bU A04;
    public C26461bU A05;
    private final C26161b0 A06;

    public static void A00(C26321bG r1, C26461bU r2, Short sh) {
        short s;
        if (r2 != null) {
            try {
                C26161b0 r12 = r1.A06;
                if (sh == null) {
                    s = 2;
                } else {
                    s = sh.shortValue();
                }
                r2.A00(s);
                r12.BJ9(r2);
            } catch (Exception e) {
                C010708t.A0I("MBLog", e.getLocalizedMessage());
            }
        }
    }

    public static boolean A01(C26321bG r1) {
        C26161b0 r0 = r1.A06;
        if (r0 == null || !r0.A00) {
            return true;
        }
        return false;
    }

    public C26321bG(C26161b0 r1) {
        this.A06 = r1;
    }
}
