package X;

/* renamed from: X.1nL  reason: invalid class name and case insensitive filesystem */
public final class C33291nL extends AnonymousClass1S9 {
    public C80973tY A00;
    public boolean A01;

    public synchronized C80973tY A04() {
        return this.A00;
    }

    public synchronized int getHeight() {
        int i;
        if (A02()) {
            i = 0;
        } else {
            i = this.A00.A03.getHeight();
        }
        return i;
    }

    public synchronized int getWidth() {
        int i;
        if (A02()) {
            i = 0;
        } else {
            i = this.A00.A03.getWidth();
        }
        return i;
    }

    public C33291nL(C80973tY r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }
}
