package X;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.messaging.customthreads.model.ThreadThemeInfo;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.FetchThreadListParams;
import com.facebook.messaging.service.model.FetchThreadListResult;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.1dO  reason: invalid class name and case insensitive filesystem */
public final class C27561dO extends C11150lz implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.sms.business.SmsAggregationCacheServiceHandler";
    public AnonymousClass0UN A00;

    public static C50452e3 A00(C27561dO r6, ThreadKey threadKey) {
        if (threadKey != null) {
            long A0G = threadKey.A0G();
            C50542eC r0 = (C50542eC) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ASZ, r6.A00);
            C50542eC.A01(r0);
            List list = (List) r0.A01.A07(A0G);
            if (list != null && list.size() == 1) {
                if (((C50532eB) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AjV, r6.A00)).A06((String) list.get(0))) {
                    return C50452e3.BUSINESS;
                }
            }
        }
        return null;
    }

    public static ThreadSummary A01(C27561dO r22, C50452e3 r23, C27311cz r24) {
        boolean z;
        ThreadSummary threadSummary;
        ThreadSummary threadSummary2;
        String join;
        String string;
        C27561dO r3 = r22;
        C50652eN r1 = (C50652eN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BPw, r3.A00);
        synchronized (r1) {
            z = C50652eN.A00(r1, r23).A02;
        }
        if (z) {
            C50652eN r12 = (C50652eN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BPw, r3.A00);
            synchronized (r12) {
                threadSummary = C50652eN.A00(r12, r23).A01;
            }
        } else {
            if (r23.ordinal() != 0) {
                C010708t.A0K("SmsAggregationCacheServiceHandler", "unhandled sms aggregation type.");
                threadSummary = null;
            } else {
                C10950l8 r13 = C10950l8.A0C;
                C10680kg r4 = new C10680kg();
                r4.A02 = C09510hU.PREFER_CACHE_IF_UP_TO_DATE;
                r4.A04 = r13;
                r4.A05 = C10700ki.SMS;
                FetchThreadListParams fetchThreadListParams = new FetchThreadListParams(r4);
                Bundle bundle = new Bundle();
                bundle.putParcelable("fetchThreadListParams", fetchThreadListParams);
                C50532eB r8 = (C50532eB) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AjV, r3.A00);
                ThreadsCollection threadsCollection = ((FetchThreadListResult) r24.BAz(new C11060ln("fetch_thread_list", bundle, null, null, CallerContext.A07(C27561dO.class, "sms_aggregation"), null)).A07()).A06;
                if (threadsCollection == null || threadsCollection.A00.isEmpty()) {
                    threadSummary2 = null;
                } else {
                    C52722ja r132 = (C52722ja) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZZ, r8.A00);
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    ImmutableList immutableList = threadsCollection.A00;
                    C52732jb r10 = new C52732jb();
                    C24971Xv it = immutableList.iterator();
                    boolean z2 = false;
                    String str = BuildConfig.FLAVOR;
                    int i = 0;
                    while (it.hasNext()) {
                        ThreadSummary threadSummary3 = (ThreadSummary) it.next();
                        boolean A0B = threadSummary3.A0B();
                        if (A0B) {
                            i++;
                            str = threadSummary3.A0w;
                        }
                        if (arrayList2.size() < 3) {
                            long A0G = threadSummary3.A0S.A0G();
                            C50542eC r14 = (C50542eC) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ASZ, r132.A00);
                            C50542eC.A01(r14);
                            List list = (List) r14.A01.A07(A0G);
                            if (list != null && list.size() == 1) {
                                String A08 = ((C52422j4) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ALq, r132.A00)).A06((String) list.get(0)).A08();
                                arrayList.add(A08);
                                if (A0B) {
                                    arrayList2.add(A08);
                                }
                            }
                        }
                    }
                    if (!arrayList2.isEmpty()) {
                        join = TextUtils.join(", ", arrayList2.subList(0, Math.min(arrayList2.size(), 3)));
                        if (arrayList2.size() == 1 && !TextUtils.isEmpty(str)) {
                            join = ((Resources) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AEg, r132.A00)).getString(2131833828, join, str);
                        }
                    } else {
                        join = !arrayList.isEmpty() ? TextUtils.join(", ", arrayList.subList(0, Math.min(arrayList.size(), 3))) : BuildConfig.FLAVOR;
                    }
                    r10.A01 = join;
                    r10.A00 = i;
                    if (i > 0) {
                        z2 = true;
                    }
                    r10.A02 = z2;
                    int i2 = i;
                    Resources resources = (Resources) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AEg, r8.A00);
                    if (i == 0) {
                        string = resources.getString(2131833109);
                    } else {
                        string = resources.getString(2131833108, Integer.valueOf(i));
                    }
                    long j = threadsCollection.A03(0).A0B;
                    AnonymousClass12U A002 = ThreadThemeInfo.A00();
                    A002.A00 = ((C21401Gu) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B4t, r8.A00)).A01();
                    ThreadThemeInfo A003 = A002.A00();
                    C17920zh A004 = ThreadSummary.A00();
                    A004.A0N = C10950l8.A05;
                    A004.A0R = C34001oU.A00;
                    A004.A0x = false;
                    A004.A0A = j;
                    A004.A07 = j;
                    A004.A0B = (long) i2;
                    if (z2) {
                        j = 0;
                    }
                    A004.A06 = j;
                    ImmutableList.Builder builder = ImmutableList.builder();
                    ParticipantInfo participantInfo = new ParticipantInfo(new UserKey(C25651aB.A02, BuildConfig.FLAVOR), string);
                    C28831fR r15 = new C28831fR();
                    r15.A04 = participantInfo;
                    builder.add((Object) new ThreadParticipant(r15));
                    ParticipantInfo A05 = ((C52422j4) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ALq, ((C52722ja) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZZ, r8.A00)).A00)).A05();
                    if (A05 != null) {
                        C28831fR r16 = new C28831fR();
                        r16.A04 = A05;
                        builder.add((Object) new ThreadParticipant(r16));
                    }
                    A004.A03(builder.build());
                    C05520Zg.A02(A003);
                    A004.A0L = A003;
                    A004.A0s = join;
                    threadSummary2 = A004.A00();
                }
                threadSummary = threadSummary2;
                if (threadSummary2 == null) {
                    ((C50652eN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BPw, r3.A00)).A02(C50452e3.BUSINESS);
                }
            }
            if (threadSummary != null) {
                C50652eN r2 = (C50652eN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BPw, r3.A00);
                long j2 = threadSummary.A0B;
                synchronized (r2) {
                    C50662eP A005 = C50652eN.A00(r2, r23);
                    A005.A01 = threadSummary;
                    A005.A00 = j2;
                    A005.A02 = true;
                }
                C52742jc r17 = (C52742jc) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ARU, r3.A00);
                String r102 = r23.toString();
                if (r17.A05()) {
                    C52742jc.A01(r17, new C80313sE(r17.A00.now(), "sms_aggregation_row_create", null, r102));
                    return threadSummary;
                }
            }
        }
        return threadSummary;
    }

    public C27561dO(AnonymousClass1XY r3) {
        super("SmsAggregationRowCacheServiceHandler");
        this.A00 = new AnonymousClass0UN(7, r3);
    }

    public static final C27561dO A03(AnonymousClass1XY r1) {
        return new C27561dO(r1);
    }

    public static void A04(C27561dO r3) {
        ((C50652eN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BPw, r3.A00)).A02(C50452e3.BUSINESS);
    }

    public static void A05(C27561dO r12, ImmutableList immutableList) {
        HashSet<C50452e3> hashSet = new HashSet<>();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            C50452e3 A002 = A00(r12, (ThreadKey) it.next());
            if (A002 != null) {
                hashSet.add(A002);
            }
        }
        for (C50452e3 r4 : hashSet) {
            C50652eN r1 = (C50652eN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BPw, r12.A00);
            synchronized (r1) {
                r1.A00.remove(r4);
            }
            C52742jc r13 = (C52742jc) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ARU, r12.A00);
            String r11 = r4.toString();
            if (r13.A05()) {
                C52742jc.A01(r13, new C80313sE(r13.A00.now(), "sms_aggregation_row_reset", null, r11));
            }
        }
        if (hashSet.contains(C50452e3.BUSINESS)) {
            int i = AnonymousClass1Y3.B7s;
            ((C189216c) AnonymousClass1XX.A02(4, i, r12.A00)).A0K(immutableList, "SmsAggregationCacheServiceHandler");
            ((C189216c) AnonymousClass1XX.A02(4, i, r12.A00)).A0G(ThreadKey.A03(-102), "SmsAggregationCacheServiceHandler");
            ((C189216c) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B7s, r12.A00)).A08();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
    public static ThreadsCollection A02(List list, ThreadsCollection threadsCollection, int i) {
        if (!list.isEmpty() && list.size() > 1) {
            Collections.sort(list, new AnonymousClass4JB());
        }
        ThreadsCollection threadsCollection2 = new ThreadsCollection(ImmutableList.copyOf((Collection) list), true);
        if (threadsCollection2.A00.isEmpty()) {
            return threadsCollection;
        }
        ThreadsCollection A002 = AnonymousClass170.A00(ImmutableList.of(threadsCollection2, threadsCollection));
        if (A002.A02() > i) {
            return new ThreadsCollection(A002.A00.subList(0, i), false);
        }
        return A002;
    }
}
