package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.10Q  reason: invalid class name */
public final class AnonymousClass10Q {
    private static volatile AnonymousClass10Q A01;
    public final Map A00 = Collections.synchronizedMap(new HashMap());

    public static final AnonymousClass10Q A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass10Q.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new AnonymousClass10Q();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
