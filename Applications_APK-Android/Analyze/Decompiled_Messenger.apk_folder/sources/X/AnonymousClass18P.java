package X;

import java.util.Map;
import java.util.WeakHashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.18P  reason: invalid class name */
public final class AnonymousClass18P {
    private static volatile AnonymousClass18P A04;
    private AnonymousClass0UN A00;
    public final C12090oW A01;
    private final C28841fS A02;
    private final Map A03 = new WeakHashMap();

    public static final AnonymousClass18P A01(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass18P.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass18P(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004a, code lost:
        if (X.AnonymousClass9QJ.A03(X.C28841fS.A07(r6)) == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (X.AnonymousClass9QJ.A04(X.C28841fS.A07(r6)) == false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008e, code lost:
        if (r1.startsWith("audio/") == false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0092, code lost:
        if (r0 == false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c1, code lost:
        if (r2 == X.C421828p.ENCRYPTED_AUDIO) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00c9, code lost:
        if (r0 == false) goto L_0x00a4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass2VE A02(com.facebook.messaging.model.messages.Message r6) {
        /*
            r5 = this;
            java.util.Map r3 = r5.A03
            monitor-enter(r3)
            java.util.Map r0 = r5.A03     // Catch:{ all -> 0x0115 }
            java.lang.Object r2 = r0.get(r6)     // Catch:{ all -> 0x0115 }
            X.2VE r2 = (X.AnonymousClass2VE) r2     // Catch:{ all -> 0x0115 }
            if (r2 != 0) goto L_0x0113
            X.1V7 r4 = r6.A0D     // Catch:{ all -> 0x0115 }
            int r0 = r4.ordinal()     // Catch:{ all -> 0x0115 }
            switch(r0) {
                case 1: goto L_0x002b;
                case 2: goto L_0x010b;
                case 3: goto L_0x010b;
                case 4: goto L_0x0107;
                case 5: goto L_0x0103;
                case 6: goto L_0x00ff;
                case 7: goto L_0x00ff;
                case 8: goto L_0x0103;
                case 9: goto L_0x0026;
                case 10: goto L_0x00f7;
                case 11: goto L_0x0026;
                case 12: goto L_0x0026;
                case 13: goto L_0x0026;
                case 14: goto L_0x00fb;
                case 15: goto L_0x00fb;
                case 16: goto L_0x00fb;
                case 17: goto L_0x0026;
                case 18: goto L_0x0026;
                case 19: goto L_0x0026;
                case 20: goto L_0x00f3;
                case 21: goto L_0x00f3;
                case 22: goto L_0x0026;
                case 23: goto L_0x00f7;
                case 24: goto L_0x0016;
                case 25: goto L_0x002b;
                case 26: goto L_0x002b;
                default: goto L_0x0016;
            }     // Catch:{ all -> 0x0115 }
        L_0x0016:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0115 }
            java.lang.String r1 = "Unexpected message type: "
            java.lang.String r0 = r4.name()     // Catch:{ all -> 0x0115 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0115 }
            r2.<init>(r0)     // Catch:{ all -> 0x0115 }
            throw r2     // Catch:{ all -> 0x0115 }
        L_0x0026:
            X.2VE r1 = X.AnonymousClass2VE.A01     // Catch:{ all -> 0x0115 }
            r2 = r1
            goto L_0x010e
        L_0x002b:
            java.lang.String r0 = r6.A0z     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x0033
            X.2VE r1 = X.AnonymousClass2VE.A0B     // Catch:{ all -> 0x0115 }
            goto L_0x00f1
        L_0x0033:
            X.0oW r0 = r5.A01     // Catch:{ all -> 0x0115 }
            boolean r0 = r0.A03(r6)     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x00ef
            boolean r0 = X.C28841fS.A0O(r6)     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x004c
            com.facebook.ui.media.attachments.model.MediaResource r0 = X.C28841fS.A07(r6)     // Catch:{ all -> 0x0115 }
            boolean r1 = X.AnonymousClass9QJ.A03(r0)     // Catch:{ all -> 0x0115 }
            r0 = 1
            if (r1 != 0) goto L_0x004d
        L_0x004c:
            r0 = 0
        L_0x004d:
            if (r0 != 0) goto L_0x00ef
            boolean r0 = X.C12090oW.A02(r6)     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x00ec
            boolean r0 = X.C28841fS.A0O(r6)     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x0066
            com.facebook.ui.media.attachments.model.MediaResource r0 = X.C28841fS.A07(r6)     // Catch:{ all -> 0x0115 }
            boolean r1 = X.AnonymousClass9QJ.A04(r0)     // Catch:{ all -> 0x0115 }
            r0 = 1
            if (r1 != 0) goto L_0x0067
        L_0x0066:
            r0 = 0
        L_0x0067:
            if (r0 != 0) goto L_0x00ec
            com.google.common.collect.ImmutableList r0 = r6.A0X     // Catch:{ all -> 0x0115 }
            int r0 = r0.size()     // Catch:{ all -> 0x0115 }
            r4 = 1
            r2 = 0
            if (r0 != r4) goto L_0x00a4
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r6.A0U     // Catch:{ all -> 0x0115 }
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r0)     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x0094
            com.google.common.collect.ImmutableList r0 = r6.A0X     // Catch:{ all -> 0x0115 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0115 }
            com.facebook.messaging.model.attachment.Attachment r0 = (com.facebook.messaging.model.attachment.Attachment) r0     // Catch:{ all -> 0x0115 }
            java.lang.String r1 = r0.A0C     // Catch:{ all -> 0x0115 }
            if (r1 == 0) goto L_0x0090
            java.lang.String r0 = "audio/"
            boolean r1 = r1.startsWith(r0)     // Catch:{ all -> 0x0115 }
            r0 = 1
            if (r1 != 0) goto L_0x0091
        L_0x0090:
            r0 = 0
        L_0x0091:
            r1 = 1
            if (r0 != 0) goto L_0x0095
        L_0x0094:
            r1 = 0
        L_0x0095:
            com.google.common.collect.ImmutableList r0 = r6.A0X     // Catch:{ all -> 0x0115 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0115 }
            com.facebook.messaging.model.attachment.Attachment r0 = (com.facebook.messaging.model.attachment.Attachment) r0     // Catch:{ all -> 0x0115 }
            boolean r0 = X.AnonymousClass18S.A02(r0)     // Catch:{ all -> 0x0115 }
            if (r1 != 0) goto L_0x00cb
            goto L_0x00c9
        L_0x00a4:
            com.google.common.collect.ImmutableList r0 = r6.A04()     // Catch:{ all -> 0x0115 }
            int r0 = r0.size()     // Catch:{ all -> 0x0115 }
            if (r0 != r4) goto L_0x00c7
            com.google.common.collect.ImmutableList r0 = r6.A04()     // Catch:{ all -> 0x0115 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0115 }
            com.facebook.ui.media.attachments.model.MediaResource r0 = (com.facebook.ui.media.attachments.model.MediaResource) r0     // Catch:{ all -> 0x0115 }
            X.28p r2 = r0.A0L     // Catch:{ all -> 0x0115 }
            X.28p r0 = X.C421828p.AUDIO     // Catch:{ all -> 0x0115 }
            if (r2 == r0) goto L_0x00c3
            X.28p r1 = X.C421828p.ENCRYPTED_AUDIO     // Catch:{ all -> 0x0115 }
            r0 = 0
            if (r2 != r1) goto L_0x00c4
        L_0x00c3:
            r0 = 1
        L_0x00c4:
            if (r0 == 0) goto L_0x00c7
            goto L_0x00cb
        L_0x00c7:
            r0 = 0
            goto L_0x00cc
        L_0x00c9:
            if (r0 == 0) goto L_0x00a4
        L_0x00cb:
            r0 = 1
        L_0x00cc:
            if (r0 == 0) goto L_0x00d1
            X.2VE r1 = X.AnonymousClass2VE.A02     // Catch:{ all -> 0x0115 }
            goto L_0x00f1
        L_0x00d1:
            X.0oW r0 = r5.A01     // Catch:{ all -> 0x0115 }
            boolean r0 = r0.A05(r6)     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x00dc
            X.2VE r1 = X.AnonymousClass2VE.A09     // Catch:{ all -> 0x0115 }
            goto L_0x00f1
        L_0x00dc:
            com.facebook.messaging.business.commerce.model.retail.CommerceData r1 = r6.Ahe()     // Catch:{ all -> 0x0115 }
            r0 = 0
            if (r1 == 0) goto L_0x00e4
            r0 = 1
        L_0x00e4:
            if (r0 == 0) goto L_0x00e9
            X.2VE r1 = X.AnonymousClass2VE.A04     // Catch:{ all -> 0x0115 }
            goto L_0x00f1
        L_0x00e9:
            X.2VE r1 = X.AnonymousClass2VE.A08     // Catch:{ all -> 0x0115 }
            goto L_0x00f1
        L_0x00ec:
            X.2VE r1 = X.AnonymousClass2VE.A0E     // Catch:{ all -> 0x0115 }
            goto L_0x00f1
        L_0x00ef:
            X.2VE r1 = X.AnonymousClass2VE.A0A     // Catch:{ all -> 0x0115 }
        L_0x00f1:
            r2 = r1
            goto L_0x010e
        L_0x00f3:
            X.2VE r1 = X.AnonymousClass2VE.A0C     // Catch:{ all -> 0x0115 }
            r2 = r1
            goto L_0x010e
        L_0x00f7:
            X.2VE r1 = X.AnonymousClass2VE.A03     // Catch:{ all -> 0x0115 }
            r2 = r1
            goto L_0x010e
        L_0x00fb:
            X.2VE r1 = X.AnonymousClass2VE.A0F     // Catch:{ all -> 0x0115 }
            r2 = r1
            goto L_0x010e
        L_0x00ff:
            X.2VE r1 = X.AnonymousClass2VE.A0D     // Catch:{ all -> 0x0115 }
            r2 = r1
            goto L_0x010e
        L_0x0103:
            X.2VE r1 = X.AnonymousClass2VE.A05     // Catch:{ all -> 0x0115 }
            r2 = r1
            goto L_0x010e
        L_0x0107:
            X.2VE r1 = X.AnonymousClass2VE.A07     // Catch:{ all -> 0x0115 }
            r2 = r1
            goto L_0x010e
        L_0x010b:
            X.2VE r1 = X.AnonymousClass2VE.A06     // Catch:{ all -> 0x0115 }
            r2 = r1
        L_0x010e:
            java.util.Map r0 = r5.A03     // Catch:{ all -> 0x0115 }
            r0.put(r6, r1)     // Catch:{ all -> 0x0115 }
        L_0x0113:
            monitor-exit(r3)     // Catch:{ all -> 0x0115 }
            return r2
        L_0x0115:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0115 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18P.A02(com.facebook.messaging.model.messages.Message):X.2VE");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a2, code lost:
        if (r1.startsWith("video/") == false) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b1, code lost:
        if (X.AnonymousClass18S.A03(r2) != false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d9, code lost:
        if (r2.A04() != false) goto L_0x00db;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A03(com.facebook.messaging.model.messages.Message r9) {
        /*
            r8 = this;
            com.google.common.collect.ImmutableList r0 = r9.A0c
            java.lang.String r3 = "h"
            if (r0 == 0) goto L_0x0034
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0034
        L_0x000c:
            int r2 = X.AnonymousClass1Y3.B8a
            X.0UN r1 = r8.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0oa r1 = (X.C12130oa) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0U
            boolean r0 = r1.A04(r0)
            if (r0 == 0) goto L_0x0025
            java.lang.String r0 = "m"
            java.lang.String r3 = X.AnonymousClass08S.A0J(r0, r3)
        L_0x0025:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0U
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0D(r0)
            if (r0 == 0) goto L_0x0033
            java.lang.String r0 = "p"
            java.lang.String r3 = X.AnonymousClass08S.A0J(r0, r3)
        L_0x0033:
            return r3
        L_0x0034:
            com.facebook.messaging.model.share.SentShareAttachment r0 = r9.A0T
            if (r0 != 0) goto L_0x000c
            java.lang.String r0 = r9.A0z
            boolean r0 = X.C06850cB.A0A(r0)
            if (r0 != 0) goto L_0x0074
            java.lang.String r1 = r9.A0z
            java.lang.String r0 = "227878347358915"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0071
            r0 = 108(0x6c, float:1.51E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0071
            r0 = 29
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0071
            r0 = 30
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0071
            java.lang.String r3 = "s"
            goto L_0x000c
        L_0x0071:
            java.lang.String r3 = "l"
            goto L_0x000c
        L_0x0074:
            com.google.common.collect.ImmutableList r0 = r9.A0X
            java.lang.String r3 = "i"
            java.lang.String r7 = "f"
            java.lang.String r6 = "g"
            java.lang.String r5 = "v"
            java.lang.String r4 = "a"
            r1 = 0
            if (r0 == 0) goto L_0x00b5
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00b5
            com.google.common.collect.ImmutableList r0 = r9.A0X
            java.lang.Object r2 = r0.get(r1)
            com.facebook.messaging.model.attachment.Attachment r2 = (com.facebook.messaging.model.attachment.Attachment) r2
            boolean r0 = X.AnonymousClass18S.A02(r2)
            if (r0 != 0) goto L_0x00e4
            java.lang.String r1 = r2.A0C
            if (r1 == 0) goto L_0x00a4
            java.lang.String r0 = "video/"
            boolean r1 = r1.startsWith(r0)
            r0 = 1
            if (r1 != 0) goto L_0x00a5
        L_0x00a4:
            r0 = 0
        L_0x00a5:
            if (r0 != 0) goto L_0x00e1
            boolean r0 = X.AnonymousClass18S.A01(r2)
            if (r0 != 0) goto L_0x00db
            boolean r0 = X.AnonymousClass18S.A03(r2)
            if (r0 == 0) goto L_0x00de
            goto L_0x000c
        L_0x00b5:
            com.google.common.collect.ImmutableList r0 = r9.A0b
            if (r0 == 0) goto L_0x00e7
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00e7
            com.google.common.collect.ImmutableList r0 = r9.A0b
            java.lang.Object r2 = r0.get(r1)
            com.facebook.ui.media.attachments.model.MediaResource r2 = (com.facebook.ui.media.attachments.model.MediaResource) r2
            X.28p r1 = r2.A0L
            X.28p r0 = X.C421828p.AUDIO
            if (r1 == r0) goto L_0x00e4
            X.28p r0 = X.C421828p.VIDEO
            if (r1 == r0) goto L_0x00e1
            X.28p r0 = X.C421828p.PHOTO
            if (r1 != r0) goto L_0x00de
            boolean r0 = r2.A04()
            if (r0 == 0) goto L_0x000c
        L_0x00db:
            r3 = r6
            goto L_0x000c
        L_0x00de:
            r3 = r7
            goto L_0x000c
        L_0x00e1:
            r3 = r5
            goto L_0x000c
        L_0x00e4:
            r3 = r4
            goto L_0x000c
        L_0x00e7:
            boolean r0 = X.C112015Ux.A04(r9)
            if (r0 == 0) goto L_0x00f1
            java.lang.String r3 = "lwa"
            goto L_0x000c
        L_0x00f1:
            java.lang.String r3 = "t"
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18P.A03(com.facebook.messaging.model.messages.Message):java.lang.String");
    }

    private AnonymousClass18P(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A02 = C28841fS.A04(r3);
        this.A01 = C12090oW.A00(r3);
        AnonymousClass18S.A00(r3);
    }

    public static final AnonymousClass18P A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0043, code lost:
        if (X.C28841fS.A0q(r7) != false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0057, code lost:
        if (r1 == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0067, code lost:
        if (r7.A0g.containsValue("chex:c2c") == false) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0077, code lost:
        if (r7.A0g.containsValue("marketplace:care") == false) goto L_0x0079;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(com.facebook.messaging.model.messages.Message r7) {
        /*
            r6 = this;
            X.2VE r3 = r6.A02(r7)
            X.2VE r0 = X.AnonymousClass2VE.A06
            r5 = 1
            if (r3 == r0) goto L_0x0081
            X.2VE r0 = X.AnonymousClass2VE.A07
            if (r3 == r0) goto L_0x0081
            X.2VE r0 = X.AnonymousClass2VE.A05
            if (r3 == r0) goto L_0x0081
            boolean r0 = X.C28841fS.A0c(r7)
            r4 = 0
            if (r0 != 0) goto L_0x0082
            boolean r0 = X.C28841fS.A0b(r7)
            if (r0 != 0) goto L_0x0082
            X.1fS r0 = r6.A02
            boolean r0 = r0.A1J(r7)
            if (r0 != 0) goto L_0x0082
            boolean r0 = X.C28841fS.A0N(r7)
            if (r0 != 0) goto L_0x0082
            boolean r0 = X.C28841fS.A0r(r7)
            if (r0 != 0) goto L_0x0045
            boolean r0 = X.C28841fS.A0s(r7)
            if (r0 != 0) goto L_0x0045
            boolean r0 = X.C28841fS.A0t(r7)
            if (r0 != 0) goto L_0x0045
            boolean r1 = X.C28841fS.A0q(r7)
            r0 = 0
            if (r1 == 0) goto L_0x0046
        L_0x0045:
            r0 = 1
        L_0x0046:
            if (r0 != 0) goto L_0x0082
            if (r7 == 0) goto L_0x0059
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r0 = r7.A09
            if (r0 == 0) goto L_0x0059
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r2 = r0.A0C
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r0 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A2N
            r1 = 0
            if (r2 != r0) goto L_0x0056
            r1 = 1
        L_0x0056:
            r0 = 1
            if (r1 != 0) goto L_0x005a
        L_0x0059:
            r0 = 0
        L_0x005a:
            if (r0 != 0) goto L_0x0082
            if (r7 == 0) goto L_0x0069
            com.google.common.collect.ImmutableMap r1 = r7.A0g
            java.lang.String r0 = "chex:c2c"
            boolean r1 = r1.containsValue(r0)
            r0 = 1
            if (r1 != 0) goto L_0x006a
        L_0x0069:
            r0 = 0
        L_0x006a:
            if (r0 != 0) goto L_0x0082
            if (r7 == 0) goto L_0x0079
            com.google.common.collect.ImmutableMap r1 = r7.A0g
            java.lang.String r0 = "marketplace:care"
            boolean r1 = r1.containsValue(r0)
            r0 = 1
            if (r1 != 0) goto L_0x007a
        L_0x0079:
            r0 = 0
        L_0x007a:
            if (r0 != 0) goto L_0x0082
            X.2VE r0 = X.AnonymousClass2VE.A01
            if (r3 == r0) goto L_0x0081
            r5 = 0
        L_0x0081:
            return r5
        L_0x0082:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18P.A04(com.facebook.messaging.model.messages.Message):boolean");
    }
}
