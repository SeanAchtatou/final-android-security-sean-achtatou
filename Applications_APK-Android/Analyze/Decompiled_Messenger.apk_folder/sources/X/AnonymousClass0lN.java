package X;

import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import com.facebook.fbservice.service.IBlueService;
import com.facebook.fbservice.service.OperationResult;
import com.google.common.base.Preconditions;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0lN  reason: invalid class name */
public final class AnonymousClass0lN extends C27211cp {
    public final /* synthetic */ C27171cl A00;

    public AnonymousClass0lN(C27171cl r1) {
        this.A00 = r1;
    }

    public /* bridge */ /* synthetic */ boolean set(Object obj) {
        return super.set((OperationResult) obj);
    }

    private void A00() {
        if (!isDone()) {
            boolean z = true;
            boolean z2 = false;
            if (Looper.myLooper() != Looper.getMainLooper()) {
                z2 = true;
            }
            Preconditions.checkState(z2, "Cannot call get on main thread for unfinished operation");
            Handler handler = this.A00.A00;
            if (handler != null && handler.getLooper() == Looper.myLooper()) {
                z = false;
            }
            Preconditions.checkState(z, "Cannot call get on the operation's handler thread for unfinished operation");
        }
    }

    public boolean A02() {
        boolean z;
        if (!isDone()) {
            try {
                C27171cl r0 = this.A00;
                IBlueService iBlueService = r0.A07;
                String str = r0.A09;
                if (iBlueService == null || str == null) {
                    z = false;
                } else {
                    z = iBlueService.ARd(str);
                }
                if (z) {
                    super.cancel(false);
                    return true;
                }
            } catch (RemoteException e) {
                C010708t.A0M("DefaultBlueServiceOperation", "Could not cancel operation", e);
                return false;
            }
        }
        return false;
    }

    public void interruptTask() {
        if (!isDone()) {
            try {
                C27171cl r0 = this.A00;
                IBlueService iBlueService = r0.A07;
                String str = r0.A09;
                if (iBlueService != null && str != null) {
                    iBlueService.ARd(str);
                }
            } catch (RemoteException e) {
                C010708t.A0M("DefaultBlueServiceOperation", "Could not cancel operation", e);
            }
        }
    }

    public void A03(OperationResult operationResult) {
        super.set(operationResult);
    }

    public Object get() {
        A00();
        return (OperationResult) super.get();
    }

    public Object get(long j, TimeUnit timeUnit) {
        A00();
        return (OperationResult) super.get(j, timeUnit);
    }
}
