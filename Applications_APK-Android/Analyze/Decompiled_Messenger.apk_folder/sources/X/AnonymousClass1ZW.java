package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1ZW  reason: invalid class name */
public final class AnonymousClass1ZW extends Enum {
    private static final /* synthetic */ AnonymousClass1ZW[] A00;
    public static final AnonymousClass1ZW A01;
    public static final AnonymousClass1ZW A02;
    public static final AnonymousClass1ZW A03;

    static {
        AnonymousClass1ZW r2 = new AnonymousClass1ZW("DEFERRABLE", 0);
        A01 = r2;
        AnonymousClass1ZW r3 = new AnonymousClass1ZW("IMMEDIATE", 1);
        A02 = r3;
        AnonymousClass1ZW r4 = new AnonymousClass1ZW("NEEDS_UI", 2);
        A03 = r4;
        A00 = new AnonymousClass1ZW[]{r2, r3, r4, new AnonymousClass1ZW("SURFACE_STORIES_TRAY", 3), new AnonymousClass1ZW("USES_IO", 4), new AnonymousClass1ZW("USES_NETWORK", 5), new AnonymousClass1ZW(AnonymousClass80H.$const$string(12), 6)};
    }

    public static AnonymousClass1ZW valueOf(String str) {
        return (AnonymousClass1ZW) Enum.valueOf(AnonymousClass1ZW.class, str);
    }

    public static AnonymousClass1ZW[] values() {
        return (AnonymousClass1ZW[]) A00.clone();
    }

    private AnonymousClass1ZW(String str, int i) {
    }
}
