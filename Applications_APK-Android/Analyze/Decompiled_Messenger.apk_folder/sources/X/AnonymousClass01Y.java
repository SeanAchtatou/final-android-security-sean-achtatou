package X;

/* renamed from: X.01Y  reason: invalid class name */
public enum AnonymousClass01Y {
    ACTIVITY_RESUMED('r', "ActivityResumed"),
    IN_FOREGROUND('f', "InForeground"),
    ACTIVITY_STARTED('s', "ActivityStarted"),
    ACTIVITY_CREATED('c', "ActivityCreated"),
    ACTIVITY_PAUSED('p', "ActivityPaused"),
    ACTIVITY_STOPPED('t', "ActivityStopped"),
    ACTIVITY_DESTROYED('d', "ActivityDestroyed"),
    IN_BACKGROUND('b', "InBackground"),
    IN_BACKGROUND_DUE_TO_LOW_IMPORTANCE('l', "InBackgroundLowImportance"),
    INITIAL_STATE('i', "InitialState"),
    BYTE_NOT_PRESENT('?', "ByteNotPresent"),
    BYTE_NOT_USED('x', "ByteNotUsed");
    
    public final char mLogSymbol;
    private final String mStringValue;

    private AnonymousClass01Y(char c, String str) {
        this.mLogSymbol = c;
        this.mStringValue = str;
    }

    public String toString() {
        return this.mStringValue;
    }
}
