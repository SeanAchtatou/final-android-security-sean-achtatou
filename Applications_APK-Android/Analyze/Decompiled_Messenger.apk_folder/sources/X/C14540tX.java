package X;

import com.facebook.graphql.enums.GraphQLThreadConnectivityStatus;
import com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleType;
import com.facebook.messaging.model.threads.ThreadConnectivityData;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0tX  reason: invalid class name and case insensitive filesystem */
public final class C14540tX {
    public GraphQLThreadConnectivityStatus A00;
    public GraphQLThreadConnectivityStatusSubtitleType A01;
    public ImmutableList A02;
    public String A03;
    public Set A04;

    public C14540tX() {
        this.A04 = new HashSet();
        this.A02 = RegularImmutableList.A02;
        this.A03 = BuildConfig.FLAVOR;
    }

    public C14540tX(ThreadConnectivityData threadConnectivityData) {
        this.A04 = new HashSet();
        C28931fb.A05(threadConnectivityData);
        if (threadConnectivityData instanceof ThreadConnectivityData) {
            this.A00 = threadConnectivityData.A00;
            this.A02 = threadConnectivityData.A02;
            this.A01 = threadConnectivityData.A01;
            this.A03 = threadConnectivityData.A03;
            this.A04 = new HashSet(threadConnectivityData.A04);
            return;
        }
        GraphQLThreadConnectivityStatus A002 = threadConnectivityData.A00();
        this.A00 = A002;
        C28931fb.A06(A002, "connectivityStatus");
        this.A04.add("connectivityStatus");
        ImmutableList immutableList = threadConnectivityData.A02;
        this.A02 = immutableList;
        C28931fb.A06(immutableList, "contextParams");
        GraphQLThreadConnectivityStatusSubtitleType A012 = threadConnectivityData.A01();
        this.A01 = A012;
        C28931fb.A06(A012, "contextType");
        this.A04.add("contextType");
        String str = threadConnectivityData.A03;
        this.A03 = str;
        C28931fb.A06(str, "firstSenderId");
    }
}
