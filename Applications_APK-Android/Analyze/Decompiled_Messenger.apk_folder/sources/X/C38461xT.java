package X;

import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.facebook.maps.FbMapViewDelegate;
import com.facebook.maps.ttrc.FbMapboxTTRC;

/* renamed from: X.1xT  reason: invalid class name and case insensitive filesystem */
public final class C38461xT implements C28486Dvj {
    public final /* synthetic */ FbMapViewDelegate A00;

    public C38461xT(FbMapViewDelegate fbMapViewDelegate) {
        this.A00 = fbMapViewDelegate;
    }

    public void BQx(int i) {
        int i2 = AnonymousClass1Y3.Akn;
        FbMapViewDelegate fbMapViewDelegate = this.A00;
        ((AnonymousClass1Z4) AnonymousClass1XX.A02(0, i2, fbMapViewDelegate.A01)).A04(AnonymousClass07B.A08, fbMapViewDelegate.A04);
        if (i == 1) {
            synchronized (FbMapboxTTRC.class) {
                C23610BiW biW = FbMapboxTTRC.sTTRCTrace;
                if (biW == null) {
                    FbMapboxTTRC.clearTrace();
                } else {
                    biW.BK6("success_reason", "pan_map");
                    FbMapboxTTRC.sTTRCTrace.CHi(C22298Ase.$const$string(334));
                    FbMapboxTTRC.sTTRCTrace.CHi(C22298Ase.$const$string(273));
                    FbMapboxTTRC.sTTRCTrace.CHi(C22298Ase.$const$string(262));
                    FbMapboxTTRC.cancel(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS);
                }
            }
        }
    }
}
