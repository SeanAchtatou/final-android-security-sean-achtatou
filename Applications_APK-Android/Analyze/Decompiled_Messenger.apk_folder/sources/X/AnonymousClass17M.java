package X;

import android.database.sqlite.SQLiteDatabase;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.17M  reason: invalid class name */
public final class AnonymousClass17M extends AnonymousClass0W2 {
    private static volatile AnonymousClass17M A00;

    public void A08(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i != 1) {
            C007406x.A00(-484979387);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS contacts_upload_snapshot");
            C007406x.A00(-957834505);
            A04(sQLiteDatabase);
            return;
        }
        C007406x.A00(-1951427704);
        sQLiteDatabase.execSQL("ALTER TABLE contacts_upload_snapshot ADD COLUMN contact_extra_fields_hash TEXT;");
        C007406x.A00(31054244);
    }

    public AnonymousClass17M() {
        super("new_ccu_upload", 2);
    }

    public static final AnonymousClass17M A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass17M.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass17M();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
