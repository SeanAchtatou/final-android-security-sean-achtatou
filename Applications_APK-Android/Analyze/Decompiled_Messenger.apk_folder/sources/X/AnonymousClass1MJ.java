package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1MJ  reason: invalid class name */
public final class AnonymousClass1MJ implements AnonymousClass1MK {
    private final List A00;

    public void Bk0(AnonymousClass1QK r6, String str, String str2) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bk0(r6, str, str2);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onIntermediateChunkStart", e);
            }
        }
    }

    public void Bk2(AnonymousClass1QK r6, String str, Map map) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bk2(r6, str, map);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onProducerFinishWithCancellation", e);
            }
        }
    }

    public void Bk4(AnonymousClass1QK r6, String str, Throwable th, Map map) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bk4(r6, str, th, map);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onProducerFinishWithFailure", e);
            }
        }
    }

    public void Bk6(AnonymousClass1QK r6, String str, Map map) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bk6(r6, str, map);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onProducerFinishWithSuccess", e);
            }
        }
    }

    public void Bk8(AnonymousClass1QK r6, String str) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bk8(r6, str);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onProducerStart", e);
            }
        }
    }

    public void Blr(AnonymousClass1QK r6) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Blr(r6);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onRequestCancellation", e);
            }
        }
    }

    public void Bm1(AnonymousClass1QK r6, Throwable th) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bm1(r6, th);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onRequestFailure", e);
            }
        }
    }

    public void Bm6(AnonymousClass1QK r6) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bm6(r6);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onRequestStart", e);
            }
        }
    }

    public void Bm8(AnonymousClass1QK r6) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bm8(r6);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onRequestSuccess", e);
            }
        }
    }

    public void Bt8(AnonymousClass1QK r6, String str, boolean z) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            try {
                ((AnonymousClass1MK) this.A00.get(i)).Bt8(r6, str, z);
            } catch (Exception e) {
                AnonymousClass02w.A0D("ForwardingRequestListener2", "InternalListener exception in onProducerFinishWithSuccess", e);
            }
        }
    }

    public boolean C3Q(AnonymousClass1QK r5, String str) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            if (((AnonymousClass1MK) this.A00.get(i)).C3Q(r5, str)) {
                return true;
            }
        }
        return false;
    }

    public AnonymousClass1MJ(Set set) {
        this.A00 = new ArrayList(set.size());
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AnonymousClass1MK r1 = (AnonymousClass1MK) it.next();
            if (r1 != null) {
                this.A00.add(r1);
            }
        }
    }
}
