package X;

import android.app.ActivityThread;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import io.card.payment.BuildConfig;

/* renamed from: X.00M  reason: invalid class name */
public final class AnonymousClass00M {
    private static volatile AnonymousClass00M A02;
    public final C000100f A00;
    public final String A01;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        String str = this.A01;
        String str2 = ((AnonymousClass00M) obj).A01;
        if (str == null) {
            return str2 == null;
        }
        return str.equals(str2);
    }

    public static AnonymousClass00M A00() {
        AnonymousClass00M A012;
        AnonymousClass00M r0 = A02;
        if (r0 != null) {
            return r0;
        }
        ActivityThread activityThread = AnonymousClass00d.A00;
        if (activityThread == null) {
            activityThread = ActivityThread.currentActivityThread();
            AnonymousClass00d.A00 = activityThread;
        }
        AnonymousClass00M A013 = A01(activityThread.getProcessName());
        A02 = A013;
        if (!TextUtils.isEmpty(A013.A01)) {
            return A013;
        }
        String A002 = AnonymousClass00V.A00("/proc/self/cmdline");
        if (TextUtils.isEmpty(A002)) {
            A012 = null;
        } else {
            A012 = A01(A002);
        }
        if (A012 == null) {
            return A02;
        }
        A02 = A012;
        return A012;
    }

    public static AnonymousClass00M A01(String str) {
        String str2;
        C000100f r0;
        if (str == null) {
            return new AnonymousClass00M(null, null);
        }
        String[] split = str.split(":");
        if (split.length > 1) {
            str2 = split[1];
        } else {
            str2 = BuildConfig.FLAVOR;
        }
        if (str2 != null) {
            if (BuildConfig.FLAVOR.equals(str2)) {
                r0 = C000100f.A01;
            } else {
                r0 = new C000100f(str2);
            }
            return new AnonymousClass00M(str, r0);
        }
        throw new IllegalArgumentException("Invalid name");
    }

    public static boolean A02() {
        int myUid;
        if (Build.VERSION.SDK_INT < 16 || 99000 > (myUid = Process.myUid() % 100000) || myUid > 99999) {
            return false;
        }
        return true;
    }

    public String A03() {
        boolean z = false;
        if (this.A01 == null) {
            z = true;
        }
        if (z) {
            return "<unknown>";
        }
        if (A05()) {
            return "<default>";
        }
        C000100f r0 = this.A00;
        if (r0 != null) {
            return r0.A00;
        }
        return null;
    }

    public String A04() {
        C000100f r0 = this.A00;
        if (r0 != null) {
            return r0.A00;
        }
        return null;
    }

    public boolean A05() {
        return C000100f.A01.equals(this.A00);
    }

    public int hashCode() {
        String str = this.A01;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        String str = this.A01;
        if (str == null) {
            return "<unknown>";
        }
        return str;
    }

    public AnonymousClass00M() {
        this(null, null);
    }

    private AnonymousClass00M(String str, C000100f r2) {
        this.A01 = str;
        this.A00 = r2;
    }
}
