package X;

import android.os.SystemClock;

/* renamed from: X.086  reason: invalid class name */
public final class AnonymousClass086 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.common.concurrent.SerialExecutor$RunnableWrapper";
    private final Runnable A00;
    private volatile long A01;
    private volatile long A02;
    public final /* synthetic */ AnonymousClass0BZ A03;

    public AnonymousClass086(AnonymousClass0BZ r1, Runnable runnable) {
        this.A03 = r1;
        this.A00 = runnable;
        SystemClock.uptimeMillis();
    }

    public void run() {
        SystemClock.uptimeMillis();
        SystemClock.currentThreadTimeMillis();
        this.A00.run();
        SystemClock.currentThreadTimeMillis();
        SystemClock.uptimeMillis();
        synchronized (this.A03) {
            this.A03.A00 = false;
        }
        AnonymousClass0BZ.A01(this.A03);
    }
}
