package X;

import android.os.Bundle;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.model.threadkey.ThreadKey;
import java.util.Collection;
import java.util.Iterator;

@UserScoped
/* renamed from: X.0kw  reason: invalid class name and case insensitive filesystem */
public final class C10840kw extends AnonymousClass0mF implements CallerContextable {
    private static C05540Zi A02 = null;
    public static final Class A03 = C10840kw.class;
    public static final String __redex_internal_original_name = "com.facebook.messaging.tincan.messenger.service.TincanMessengerServiceHandler";
    public AnonymousClass0UN A00;
    @LoggedInUser
    public final C04310Tq A01;

    private C10840kw(AnonymousClass1XY r3) {
        super("TincanMessengerServiceHandler");
        this.A00 = new AnonymousClass0UN(13, r3);
        this.A01 = AnonymousClass0WY.A01(r3);
    }

    public static final C10840kw A00(AnonymousClass1XY r4) {
        C10840kw r0;
        synchronized (C10840kw.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new C10840kw((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (C10840kw) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A01(C10840kw r6, Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("thread_key", (ThreadKey) it.next());
            ((BlueServiceOperationFactory) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B1W, r6.A00)).newInstance("TincanDeleteThread", bundle, 1, CallerContext.A04(C10840kw.class)).CGe();
        }
    }

    public OperationResult A0z(C11060ln r2, C27311cz r3) {
        throw C000300h.A00();
    }
}
