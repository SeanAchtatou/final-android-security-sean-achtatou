package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.facebook.messaging.zombification.MessengerOnlyPhoneReconfirmationActivity;

/* renamed from: X.14w  reason: invalid class name and case insensitive filesystem */
public final class C187314w implements C186714q {
    private final AnonymousClass0XN A00;
    private final C04310Tq A01;

    public boolean CE5() {
        return false;
    }

    public static final C187314w A00(AnonymousClass1XY r3) {
        return new C187314w(AnonymousClass0VG.A00(AnonymousClass1Y3.AQv, r3), AnonymousClass0XM.A00(r3));
    }

    public Integer AeZ() {
        return AnonymousClass07B.A0N;
    }

    public Intent AqX(Activity activity) {
        return new Intent(activity, MessengerOnlyPhoneReconfirmationActivity.class);
    }

    public boolean BDw(Context context) {
        if (!this.A00.A0I()) {
            return false;
        }
        return ((Boolean) this.A01.get()).booleanValue();
    }

    public boolean CEE(Activity activity) {
        if (!(activity instanceof C27891dv)) {
            if (!C138546dM.class.isAssignableFrom(activity.getClass())) {
                return true;
            }
        }
        return false;
    }

    private C187314w(C04310Tq r1, AnonymousClass0XN r2) {
        this.A01 = r1;
        this.A00 = r2;
    }
}
