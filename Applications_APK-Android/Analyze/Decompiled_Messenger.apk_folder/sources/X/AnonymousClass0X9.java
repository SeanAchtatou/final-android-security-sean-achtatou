package X;

import com.facebook.acra.CrashReportData;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.0X9  reason: invalid class name */
public final class AnonymousClass0X9 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.errorreporting.FbErrorReporterImpl$2";
    public final /* synthetic */ AnonymousClass06D A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ Throwable A03;

    public AnonymousClass0X9(AnonymousClass06D r1, String str, String str2, Throwable th) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = str2;
        this.A03 = th;
    }

    public void run() {
        CrashReportData crashReportData = new CrashReportData();
        crashReportData.put(ErrorReportingConstants.SOFT_ERROR_CATEGORY, this.A02);
        crashReportData.put(TurboLoader.Locator.$const$string(87), this.A01);
        ((ErrorReporter) this.A00.A05.get()).handleExceptionDelayed(this.A03, crashReportData);
    }
}
