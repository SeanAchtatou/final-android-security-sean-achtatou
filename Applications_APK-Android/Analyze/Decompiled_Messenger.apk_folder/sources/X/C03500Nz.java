package X;

import io.card.payment.BuildConfig;

/* renamed from: X.0Nz  reason: invalid class name and case insensitive filesystem */
public final class C03500Nz implements AnonymousClass0AQ {
    private C01830Bu A00;
    public final AnonymousClass0AW A01;

    public void ASh() {
    }

    public String AhN() {
        return "device_auth";
    }

    public String AiB() {
        return BuildConfig.FLAVOR;
    }

    public synchronized C01830Bu Arn() {
        return this.A00;
    }

    public void C73(String str) {
    }

    public synchronized boolean CKA(C01830Bu r4) {
        if (this.A00.equals(r4)) {
            return false;
        }
        AnonymousClass0DD AY8 = this.A01.AbP(AnonymousClass07B.A0i).AY8();
        AY8.BzD("/settings/mqtt/id/connection_key", (String) r4.first);
        AY8.BzD("/settings/mqtt/id/connection_secret", (String) r4.second);
        AY8.commit();
        this.A00 = r4;
        return true;
    }

    public synchronized void clear() {
        CKA(C01830Bu.A00);
    }

    public C03500Nz(AnonymousClass0AW r5) {
        this.A01 = r5;
        AnonymousClass0B0 AbP = r5.AbP(AnonymousClass07B.A0i);
        this.A00 = C01830Bu.A00(AbP.getString("/settings/mqtt/id/connection_key", BuildConfig.FLAVOR), AbP.getString("/settings/mqtt/id/connection_secret", BuildConfig.FLAVOR));
    }
}
