package X;

import java.util.ArrayList;

/* renamed from: X.1Gl  reason: invalid class name and case insensitive filesystem */
public final class C21351Gl {
    public static C21361Gm[] A00(boolean z, boolean z2, boolean z3, boolean z4) {
        C21361Gm r0;
        ArrayList arrayList = new ArrayList();
        if (z) {
            arrayList.add(C21361Gm.MORE);
        }
        if (z2) {
            if (z3) {
                r0 = C21361Gm.NOTIFICATION_ON;
            } else {
                r0 = C21361Gm.NOTIFICATION_OFF;
            }
            arrayList.add(r0);
        }
        if (z4) {
            arrayList.add(C21361Gm.DELETE);
        }
        return (C21361Gm[]) arrayList.toArray(new C21361Gm[arrayList.size()]);
    }

    public static C21361Gm[] A01(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        C21361Gm r0;
        ArrayList arrayList = new ArrayList();
        if (z4) {
            if (!z5) {
                r0 = C21361Gm.PIN_THREAD;
            } else {
                r0 = C21361Gm.UNPIN_THREAD;
            }
            arrayList.add(r0);
        } else {
            if (z) {
                arrayList.add(C21361Gm.CAMERA);
            }
            if (z2) {
                arrayList.add(C21361Gm.AUDIO_CALL);
            }
            if (z3) {
                arrayList.add(C21361Gm.VIDEO_CALL);
            }
        }
        return (C21361Gm[]) arrayList.toArray(new C21361Gm[arrayList.size()]);
    }
}
