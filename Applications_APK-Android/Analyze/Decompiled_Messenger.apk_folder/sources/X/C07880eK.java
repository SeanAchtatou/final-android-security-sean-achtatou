package X;

import android.os.Process;
import io.card.payment.BuildConfig;
import java.util.Arrays;
import java.util.HashSet;

/* renamed from: X.0eK  reason: invalid class name and case insensitive filesystem */
public abstract class C07880eK extends C08190ep {
    public int A00 = 0;
    private AnonymousClass09P A01 = null;
    private boolean A02 = false;
    private int[] A03 = new int[0];

    public long A02() {
        if (this instanceof C07850eH) {
            return 285490771465933L;
        }
        if (!(this instanceof C07890eL)) {
            return !(this instanceof C07960eS) ? 285490771138250L : 287371966815652L;
        }
        return 286457138846535L;
    }

    public long A03() {
        if (this instanceof C07850eH) {
            return 566965747910449L;
        }
        if (!(this instanceof C07890eL)) {
            return this instanceof C07960eS ? 568846943652399L : 566965747910449L;
        }
        return 567932115683473L;
    }

    public long A04() {
        if (this instanceof C07850eH) {
            return 848440724685372L;
        }
        if (!(this instanceof C07890eL)) {
            return this instanceof C07960eS ? 850321920426707L : 848440724685372L;
        }
        return 849407092458107L;
    }

    public String A05() {
        return !(this instanceof C07850eH) ? !(this instanceof C07890eL) ? !(this instanceof C07960eS) ? "LithoLayoutThreadBoost" : "QPLInteractionLatch" : "RenderThreadBoostQPLListener" : "SurfaceThreadBoost";
    }

    public void A07(C04270Tg r6) {
        Integer num;
        if (this instanceof C07850eH) {
            C07850eH r4 = (C07850eH) this;
            if (!C07850eH.A01(r4) && (num = r4.A02) != null) {
                try {
                    Process.setThreadPriority(r4.A00, num.intValue());
                } catch (IllegalArgumentException e) {
                    ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r4.A01)).softReport("SurfaceThreadBoost", "SurfaceDataFetch thread unexpectedly died", e);
                }
                r4.A02 = null;
            }
        } else if (this instanceof C07890eL) {
            C34131oh.A01();
        } else if (!(this instanceof C07960eS)) {
            C07950eR r42 = (C07950eR) this;
            C07950eR.A01(r42);
            Integer num2 = r42.A02;
            if (num2 != null) {
                try {
                    Process.setThreadPriority(r42.A00, num2.intValue());
                } catch (IllegalArgumentException e2) {
                    ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r42.A01)).softReport("LithoLayoutThreadBoost", "Layout thread unexpectedly died", e2);
                }
                r42.A02 = null;
            }
        } else {
            ((C07960eS) this).A00.A01();
        }
    }

    public void A08(C04270Tg r6) {
        if (this instanceof C07850eH) {
            C07850eH r4 = (C07850eH) this;
            if (!C07850eH.A01(r4)) {
                try {
                    r4.A02 = Integer.valueOf(Process.getThreadPriority(r4.A00));
                    Process.setThreadPriority(r4.A00, r4.A00);
                } catch (IllegalArgumentException e) {
                    ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r4.A01)).softReport("SurfaceThreadBoost", "SurfaceDataFetch thread unexpectedly died", e);
                }
            }
        } else if (this instanceof C07890eL) {
            C34131oh.A02(((C07890eL) this).A00);
        } else if (!(this instanceof C07960eS)) {
            C07950eR r42 = (C07950eR) this;
            C07950eR.A01(r42);
            try {
                r42.A02 = Integer.valueOf(Process.getThreadPriority(r42.A00));
                Process.setThreadPriority(r42.A00, r42.A00);
            } catch (IllegalArgumentException e2) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r42.A01)).softReport("LithoLayoutThreadBoost", "Layout thread unexpectedly died", e2);
            }
        } else {
            ((C07960eS) this).A00.A02();
        }
    }

    public void A06(C25051Yd r9, AnonymousClass09P r10) {
        this.A01 = r10;
        if (r9.Aem(A02())) {
            int AqL = r9.AqL(A03(), 0);
            this.A00 = AqL;
            if (AqL < -19 || AqL > 19) {
                this.A01.CGS(A05(), AnonymousClass08S.A09("Invalid priority ", AqL));
                return;
            }
            String B4E = r9.B4E(A04(), BuildConfig.FLAVOR);
            String[] split = B4E.split(",");
            int size = new HashSet(Arrays.asList(split)).size();
            int length = split.length;
            boolean z = false;
            if (size < length) {
                z = true;
            }
            if (z) {
                AnonymousClass09P r3 = this.A01;
                String A05 = A05();
                r3.CGS(A05, "Has duplicate marker ids " + split);
            }
            int[] iArr = new int[length];
            this.A03 = iArr;
            int i = 0;
            while (i < length) {
                try {
                    iArr[i] = Integer.parseInt(split[i]);
                    i++;
                } catch (NumberFormatException unused) {
                    this.A01.CGS(A05(), AnonymousClass08S.A0J("Invalid qpl ids ", B4E));
                    this.A03 = new int[0];
                }
            }
        }
        this.A02 = r9.Aem(286289635056296L);
    }

    public AnonymousClass0Ti AsW() {
        if (this.A02) {
            return new AnonymousClass0Ti(null, this.A03, null);
        }
        return AnonymousClass0Ti.A00(this.A03);
    }
}
