package X;

import com.google.common.collect.ImmutableList;
import java.util.List;

/* renamed from: X.1P0  reason: invalid class name */
public final class AnonymousClass1P0 implements AnonymousClass1P1 {
    private int A00;
    private List A01;
    private boolean A02;
    public final /* synthetic */ C25051Yd A03;

    public synchronized int Ao3() {
        if (!this.A02) {
            this.A00 = this.A03.AqL(563542658908658L, 5);
            this.A02 = true;
        }
        return this.A00;
    }

    public AnonymousClass1P0(C25051Yd r1) {
        this.A03 = r1;
    }

    public List B1w() {
        if (this.A01 == null) {
            this.A01 = ImmutableList.of(2, Integer.valueOf(Ao3()));
        }
        return this.A01;
    }
}
