package X;

import com.facebook.auth.viewercontext.ViewerContext;
import java.util.concurrent.Callable;

/* renamed from: X.0VE  reason: invalid class name */
public final class AnonymousClass0VE implements AnonymousClass0VF {
    private final C04310Tq A00;

    public static final AnonymousClass0VE A00(AnonymousClass1XY r1) {
        return new AnonymousClass0VE(r1);
    }

    public Callable BQi(Callable callable) {
        C05920aY r2 = (C05920aY) this.A00.get();
        ViewerContext B9S = r2.B9S();
        if (B9S == null) {
            return callable;
        }
        return new C21868AjU(callable, r2, B9S);
    }

    public Runnable Bms(Runnable runnable) {
        C05920aY r2 = (C05920aY) this.A00.get();
        ViewerContext B9S = r2.B9S();
        if (B9S == null) {
            return runnable;
        }
        return new C21867AjT(runnable, r2, B9S);
    }

    public AnonymousClass0VE(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0VG.A00(AnonymousClass1Y3.BJQ, r2);
    }
}
