package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;

/* renamed from: X.1CY  reason: invalid class name */
public abstract class AnonymousClass1CY {
    public C16070wR A00;

    public AnonymousClass1CY A01() {
        return !(this instanceof C21311Gb) ? !(this instanceof AnonymousClass1GX) ? (C33981oS) this : (AnonymousClass1GX) this : (C21311Gb) this;
    }

    public C16070wR A03() {
        if (this instanceof C21311Gb) {
            C21311Gb r3 = (C21311Gb) this;
            A00(3, r3.A01, r3.A02);
            return r3.A00;
        } else if (!(this instanceof AnonymousClass1GX)) {
            return ((C33981oS) this).A04();
        } else {
            AnonymousClass1GX r32 = (AnonymousClass1GX) this;
            A00(2, r32.A01, r32.A02);
            return r32.A00;
        }
    }

    public static void A00(int i, BitSet bitSet, String[] strArr) {
        if (bitSet != null) {
            if (bitSet.nextClearBit(0) < i) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < i; i2++) {
                    if (!bitSet.get(i2)) {
                        arrayList.add(strArr[i2]);
                    }
                }
                throw new IllegalStateException(AnonymousClass08S.A0J("The following props are not marked as optional and were not supplied: ", Arrays.toString(arrayList.toArray())));
            }
        }
    }

    public AnonymousClass1CY A02(String str) {
        this.A00.A05 = str;
        A01();
        return this;
    }
}
