package X;

import android.net.Uri;
import com.facebook.common.time.RealtimeSinceBootClock;

/* renamed from: X.1Qc  reason: invalid class name and case insensitive filesystem */
public final class C23591Qc implements C23601Qd {
    private final int A00;
    private final C23601Qd A01;
    private final C23541Px A02;
    private final AnonymousClass36w A03;
    private final AnonymousClass1Q1 A04;
    private final String A05;
    private final String A06;

    public String toString() {
        return String.format(null, "%s_%s_%s_%s_%s_%s_%d", this.A06, this.A03, this.A04, this.A02, this.A01, this.A05, Integer.valueOf(this.A00));
    }

    public String B7w() {
        return this.A06;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C23591Qc)) {
            return false;
        }
        C23591Qc r4 = (C23591Qc) obj;
        if (this.A00 != r4.A00 || !this.A06.equals(r4.A06) || !C14620th.A01(this.A03, r4.A03) || !C14620th.A01(this.A04, r4.A04) || !C14620th.A01(this.A02, r4.A02) || !C14620th.A01(this.A01, r4.A01) || !C14620th.A01(this.A05, r4.A05)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A00;
    }

    public C23591Qc(String str, AnonymousClass36w r9, AnonymousClass1Q1 r10, C23541Px r11, C23601Qd r12, String str2) {
        int i;
        C05520Zg.A02(str);
        this.A06 = str;
        this.A03 = r9;
        this.A04 = r10;
        this.A02 = r11;
        this.A01 = r12;
        String str3 = str2;
        this.A05 = str2;
        Integer valueOf = Integer.valueOf(str.hashCode());
        if (r9 != null) {
            i = r9.hashCode();
        } else {
            i = 0;
        }
        this.A00 = AnonymousClass07K.A05(valueOf, Integer.valueOf(i), Integer.valueOf(r10.hashCode()), this.A02, this.A01, str3);
        RealtimeSinceBootClock.A00.now();
    }

    public boolean AUG(Uri uri) {
        return B7w().contains(uri.toString());
    }
}
