package X;

import android.app.Activity;
import android.app.ActivityManager;
import android.graphics.BitmapFactory;
import java.lang.ref.WeakReference;

/* renamed from: X.0ti  reason: invalid class name and case insensitive filesystem */
public final class C14630ti implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.base.activity.TaskDescriptionUtil$1";
    public final /* synthetic */ WeakReference A00;

    public C14630ti(WeakReference weakReference) {
        this.A00 = weakReference;
    }

    public void run() {
        ActivityManager.TaskDescription taskDescription;
        Activity activity = (Activity) this.A00.get();
        if (activity != null) {
            try {
                taskDescription = new ActivityManager.TaskDescription(C121665oc.A03(activity.getResources()), BitmapFactory.decodeResource(activity.getResources(), 2131230775), AnonymousClass2T7.A00(activity, AnonymousClass1L3.A1D) | C15320v6.MEASURED_STATE_MASK);
            } catch (RuntimeException e) {
                C010708t.A0N("setTaskDescription", "Exception trying to set TaskDescription", e);
                taskDescription = null;
            }
            if (!activity.isFinishing() && !activity.isDestroyed()) {
                activity.runOnUiThread(new AnonymousClass4Ju(activity, taskDescription));
            }
        }
    }
}
