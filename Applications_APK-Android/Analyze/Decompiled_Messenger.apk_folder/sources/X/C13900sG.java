package X;

import android.content.Context;
import com.facebook.orca.threadlist.ThreadListFragment;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0sG  reason: invalid class name and case insensitive filesystem */
public abstract class C13900sG {
    public final AtomicBoolean A00 = new AtomicBoolean(false);

    public void A01() {
        if (this instanceof C13870sD) {
            C13870sD r3 = (C13870sD) this;
            try {
                new ThreadListFragment().A2S((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, r3.A00));
            } catch (Exception e) {
                C010708t.A0T("ThreadListFragmentPreInjector", e, "exception while background-injecting fragment");
            }
            synchronized (r3) {
            }
        }
    }
}
