package X;

import com.google.android.exoplayer2.Format;

/* renamed from: X.1wm  reason: invalid class name and case insensitive filesystem */
public final class C38031wm {
    public final int A00;
    public final int A01;
    public final int A02;
    public final long A03;
    public final long A04;
    public final Format A05;
    public final Object A06;

    public C38031wm(int i, int i2, Format format, int i3, Object obj, long j, long j2) {
        this.A00 = i;
        this.A02 = i2;
        this.A05 = format;
        this.A01 = i3;
        this.A06 = obj;
        this.A04 = j;
        this.A03 = j2;
    }
}
