package X;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;

/* renamed from: X.0e2  reason: invalid class name and case insensitive filesystem */
public final class C07710e2 implements AnonymousClass06U {
    public final /* synthetic */ C09340h3 A00;

    public C07710e2(C09340h3 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(1699540552);
        C09340h3.A06(this.A00, !r8.isInitialStickyBroadcast());
        if (Build.VERSION.SDK_INT >= 23) {
            boolean isDeviceIdleMode = ((PowerManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8E, this.A00.A03)).isDeviceIdleMode();
            synchronized (this.A00.A0D) {
                try {
                    this.A00.A04 = Boolean.valueOf(isDeviceIdleMode);
                } finally {
                    AnonymousClass09Y.A01(-1114957652, A002);
                }
            }
        }
    }
}
