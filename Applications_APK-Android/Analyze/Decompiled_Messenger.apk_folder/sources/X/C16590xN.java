package X;

import android.content.Context;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import java.util.Map;

/* renamed from: X.0xN  reason: invalid class name and case insensitive filesystem */
public final class C16590xN {
    private final Context A00;
    private final DeprecatedAnalyticsLogger A01;

    public static final C16590xN A00(AnonymousClass1XY r1) {
        return new C16590xN(r1);
    }

    public void A01(String str, String str2, String str3, Map map) {
        C11670nb r2 = new C11670nb(str);
        r2.A0D("pigeon_reserved_keyword_module", "MessengerBannerNotifications");
        r2.A0D("pigeon_reserved_keyword_obj_type", str2);
        r2.A0D("pigeon_reserved_keyword_obj_id", str3);
        Context context = this.A00;
        if (context instanceof C11510nI) {
            r2.A0D("NotificationLocationActivity", ((C11510nI) context).Act());
        }
        if (map != null) {
            C11670nb.A02(r2, map, false);
        }
        this.A01.A09(r2);
    }

    public C16590xN(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1YA.A00(r2);
        this.A01 = C06920cI.A00(r2);
    }
}
