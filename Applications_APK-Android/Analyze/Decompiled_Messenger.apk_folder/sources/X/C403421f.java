package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.21f  reason: invalid class name and case insensitive filesystem */
public final class C403421f extends Drawable.ConstantState {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;

    public int getChangingConfigurations() {
        return 0;
    }

    public Drawable newDrawable() {
        return new C113975bS(this);
    }

    public C403421f(int i, int i2, int i3, int i4) {
        this.A03 = i;
        this.A02 = i2;
        this.A01 = i3;
        this.A00 = i4;
    }
}
