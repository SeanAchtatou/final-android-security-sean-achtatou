package X;

/* renamed from: X.00h  reason: invalid class name and case insensitive filesystem */
public final class C000300h {
    public static AssertionError A00() {
        throw new AssertionError();
    }

    public static void A01(Object obj) {
        if (obj == null) {
            throw new AssertionError();
        }
    }

    public static void A02(Object obj, String str) {
        if (obj == null) {
            throw new AssertionError(str);
        }
    }

    public static void A03(boolean z) {
        if (!z) {
            throw new AssertionError();
        }
    }
}
