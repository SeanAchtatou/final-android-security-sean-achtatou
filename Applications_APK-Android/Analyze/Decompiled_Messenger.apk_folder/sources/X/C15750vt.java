package X;

import android.view.View;
import androidx.appcompat.widget.ViewStubCompat;
import com.google.common.base.Preconditions;

/* renamed from: X.0vt  reason: invalid class name and case insensitive filesystem */
public final class C15750vt {
    public View A00;
    public C82943wn A01;
    private ViewStubCompat A02;
    private AnonymousClass3DM A03;

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r4.A02.A01 == 0) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A01() {
        /*
            r4 = this;
            android.view.View r0 = r4.A00
            if (r0 != 0) goto L_0x005e
            androidx.appcompat.widget.ViewStubCompat r0 = r4.A02
            if (r0 == 0) goto L_0x005e
            android.content.res.Resources r0 = r0.getResources()
            if (r0 == 0) goto L_0x0015
            androidx.appcompat.widget.ViewStubCompat r0 = r4.A02
            int r0 = r0.A01
            r3 = 1
            if (r0 != 0) goto L_0x0016
        L_0x0015:
            r3 = 0
        L_0x0016:
            if (r3 == 0) goto L_0x002e
            androidx.appcompat.widget.ViewStubCompat r0 = r4.A02
            android.content.res.Resources r1 = r0.getResources()
            androidx.appcompat.widget.ViewStubCompat r0 = r4.A02
            int r0 = r0.A01
            java.lang.String r2 = r1.getResourceName(r0)
            r1 = 780004362(0x2e7dec0a, float:5.7735185E-11)
            java.lang.String r0 = "getView: inflate(%s)"
            X.C005505z.A05(r0, r2, r1)
        L_0x002e:
            androidx.appcompat.widget.ViewStubCompat r0 = r4.A02     // Catch:{ all -> 0x0037 }
            android.view.View r0 = r0.A00()     // Catch:{ all -> 0x0037 }
            r4.A00 = r0     // Catch:{ all -> 0x0037 }
            goto L_0x0041
        L_0x0037:
            r1 = move-exception
            if (r3 == 0) goto L_0x0040
            r0 = 1836384802(0x6d750222, float:4.7391504E27)
            X.C005505z.A00(r0)
        L_0x0040:
            throw r1
        L_0x0041:
            if (r3 == 0) goto L_0x0049
            r0 = -713338911(0xffffffffd57b4fe1, float:-1.7270031E13)
            X.C005505z.A00(r0)
        L_0x0049:
            X.3DM r1 = r4.A03
            if (r1 == 0) goto L_0x0052
            android.view.View r0 = r4.A00
            r1.Bb9(r0)
        L_0x0052:
            X.3wn r0 = r4.A01
            if (r0 == 0) goto L_0x0059
            r0.Bom()
        L_0x0059:
            r0 = 0
            r4.A02 = r0
            r4.A03 = r0
        L_0x005e:
            android.view.View r0 = r4.A00
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15750vt.A01():android.view.View");
    }

    public View A02() {
        View view = this.A00;
        if (view != null) {
            return view;
        }
        return this.A02;
    }

    public boolean A06() {
        if (this.A00 != null) {
            return true;
        }
        return false;
    }

    public C15750vt(ViewStubCompat viewStubCompat) {
        this.A02 = viewStubCompat;
    }

    public static C15750vt A00(ViewStubCompat viewStubCompat) {
        Preconditions.checkNotNull(viewStubCompat);
        return new C15750vt(viewStubCompat);
    }

    public void A03() {
        if (A06()) {
            this.A00.setVisibility(8);
            C82943wn r0 = this.A01;
            if (r0 != null) {
                r0.Bae();
            }
        }
    }

    public void A04() {
        A01().setVisibility(0);
        C82943wn r0 = this.A01;
        if (r0 != null) {
            r0.Bom();
        }
    }

    public boolean A07() {
        if (!A06() || this.A00.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    public void A05(AnonymousClass3DM r1) {
        this.A03 = r1;
    }
}
