package X;

import com.facebook.auth.viewercontext.ViewerContext;

/* renamed from: X.0bD  reason: invalid class name and case insensitive filesystem */
public abstract class C06260bD {
    public AnonymousClass23I A00;
    public boolean A01;
    public final C06270bE A02 = new C06270bE();

    public final synchronized void A02(AnonymousClass23I r2) {
        this.A01 = true;
        this.A00 = r2;
        this.A02.A03(r2);
    }

    public AnonymousClass23I A01() {
        ViewerContext A08;
        if (!(this instanceof C06250bC)) {
            throw new IllegalStateException(ECX.$const$string(115));
        }
        C06250bC r4 = (C06250bC) this;
        int i = AnonymousClass1Y3.Aug;
        AnonymousClass0UN r3 = r4.A00;
        String str = null;
        if (((C11950oI) AnonymousClass1XX.A02(1, i, r3)).A01 && (A08 = ((AnonymousClass0XN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AXA, r3)).A08()) != null) {
            str = A08.A00;
        }
        String str2 = (String) r4.A01.get();
        if (str2 == null) {
            return null;
        }
        return new AnonymousClass23I(str2, str2, str);
    }
}
