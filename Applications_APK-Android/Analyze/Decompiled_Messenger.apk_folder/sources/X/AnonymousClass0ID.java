package X;

import java.util.regex.Pattern;

/* renamed from: X.0ID  reason: invalid class name */
public interface AnonymousClass0ID {
    public static final Pattern A00 = AnonymousClass0HT.A00("FbPatterns.EMail.PATTERN", "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");
}
