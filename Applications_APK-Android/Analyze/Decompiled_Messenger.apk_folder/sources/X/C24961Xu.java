package X;

import java.util.ListIterator;

/* renamed from: X.1Xu  reason: invalid class name and case insensitive filesystem */
public abstract class C24961Xu extends C24971Xv implements ListIterator {
    public final void add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final void set(Object obj) {
        throw new UnsupportedOperationException();
    }
}
