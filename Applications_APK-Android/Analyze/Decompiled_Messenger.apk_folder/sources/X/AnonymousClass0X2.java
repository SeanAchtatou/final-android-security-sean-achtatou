package X;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;

/* renamed from: X.0X2  reason: invalid class name */
public final class AnonymousClass0X2 {
    private RandomAccessFile A00;
    private FileLock A01;
    private final File A02;

    public void A01() {
        FileLock fileLock = this.A01;
        RandomAccessFile randomAccessFile = this.A00;
        this.A01 = null;
        this.A00 = null;
        try {
            fileLock.release();
        } catch (IOException e) {
            C010708t.A0V("GatekeeperRepository", e, "Cannot release a lock to file %s", this.A02);
        }
        A00(this.A02, randomAccessFile);
    }

    public boolean A02() {
        RandomAccessFile randomAccessFile;
        FileLock fileLock;
        File file = this.A02;
        try {
            randomAccessFile = new RandomAccessFile(file, "rw");
        } catch (IOException e) {
            C010708t.A0U("GatekeeperRepository", e, "Cannot create file %s", file);
            randomAccessFile = null;
        }
        if (randomAccessFile == null) {
            return false;
        }
        File file2 = this.A02;
        try {
            fileLock = randomAccessFile.getChannel().lock();
        } catch (IOException e2) {
            C010708t.A0U("GatekeeperRepository", e2, "Cannot acquire a lock to file %s", file2);
            A00(file2, randomAccessFile);
            fileLock = null;
        }
        if (fileLock == null) {
            A00(this.A02, randomAccessFile);
            return false;
        }
        this.A00 = randomAccessFile;
        this.A01 = fileLock;
        return true;
    }

    public AnonymousClass0X2(File file) {
        this.A02 = file;
    }

    private static void A00(File file, RandomAccessFile randomAccessFile) {
        try {
            randomAccessFile.close();
        } catch (IOException e) {
            C010708t.A0V("GatekeeperRepository", e, "Cannot close file %s", file);
        }
    }
}
