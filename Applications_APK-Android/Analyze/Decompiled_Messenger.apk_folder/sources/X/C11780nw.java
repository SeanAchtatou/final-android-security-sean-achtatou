package X;

import io.card.payment.BuildConfig;
import java.io.Serializable;

/* renamed from: X.0nw  reason: invalid class name and case insensitive filesystem */
public final class C11780nw implements Comparable, Serializable {
    public static final C11780nw UNKNOWN_VERSION = new C11780nw(0, 0, 0, null, null, null);
    private static final long serialVersionUID = 1;
    public final String _artifactId;
    public final String _groupId;
    public final int _majorVersion;
    public final int _minorVersion;
    public final int _patchLevel;
    public final String _snapshotInfo;

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj == null || obj.getClass() != getClass()) {
                return false;
            }
            C11780nw r5 = (C11780nw) obj;
            if (!(r5._majorVersion == this._majorVersion && r5._minorVersion == this._minorVersion && r5._patchLevel == this._patchLevel && r5._artifactId.equals(this._artifactId) && r5._groupId.equals(this._groupId))) {
                return false;
            }
        }
        return true;
    }

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C11780nw r3 = (C11780nw) obj;
        if (r3 == this) {
            return 0;
        }
        int compareTo = this._groupId.compareTo(r3._groupId);
        if (compareTo != 0) {
            return compareTo;
        }
        int compareTo2 = this._artifactId.compareTo(r3._artifactId);
        if (compareTo2 != 0) {
            return compareTo2;
        }
        int i = this._majorVersion - r3._majorVersion;
        if (i != 0) {
            return i;
        }
        int i2 = this._minorVersion - r3._minorVersion;
        if (i2 == 0) {
            return this._patchLevel - r3._patchLevel;
        }
        return i2;
    }

    public int hashCode() {
        return this._artifactId.hashCode() ^ (((this._groupId.hashCode() + this._majorVersion) - this._minorVersion) + this._patchLevel);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0025, code lost:
        if (r2.length() <= 0) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r4 = this;
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r0 = r4._majorVersion
            r3.append(r0)
            r1 = 46
            r3.append(r1)
            int r0 = r4._minorVersion
            r3.append(r0)
            r3.append(r1)
            int r0 = r4._patchLevel
            r3.append(r0)
            java.lang.String r2 = r4._snapshotInfo
            if (r2 == 0) goto L_0x0027
            int r1 = r2.length()
            r0 = 1
            if (r1 > 0) goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            if (r0 == 0) goto L_0x0032
            r0 = 45
            r3.append(r0)
            r3.append(r2)
        L_0x0032:
            java.lang.String r0 = r3.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11780nw.toString():java.lang.String");
    }

    public C11780nw(int i, int i2, int i3, String str, String str2, String str3) {
        this._majorVersion = i;
        this._minorVersion = i2;
        this._patchLevel = i3;
        this._snapshotInfo = str;
        String str4 = BuildConfig.FLAVOR;
        this._groupId = str2 == null ? str4 : str2;
        this._artifactId = str3 != null ? str3 : str4;
    }
}
