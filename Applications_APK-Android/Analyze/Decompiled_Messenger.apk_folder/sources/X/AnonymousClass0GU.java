package X;

import android.content.Context;
import android.util.Pair;
import com.facebook.analytics.appstatelogger.AppStateLogFile;
import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0GU  reason: invalid class name */
public final class AnonymousClass0GU implements AnonymousClass1YQ, C06670bt {
    private static volatile AnonymousClass0GU A03;
    private AnonymousClass0UN A00;
    private final Context A01;
    private final C25051Yd A02;

    public String getSimpleName() {
        return "AppStateLoggerEnabler";
    }

    public static final AnonymousClass0GU A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass0GU.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass0GU(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final AnonymousClass0US A01(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.BFj, r1);
    }

    private void A03() {
        C25051Yd r3 = this.A02;
        AnonymousClass0XE r0 = AnonymousClass0XE.A05;
        boolean Aer = r3.Aer(281582352466047L, r0);
        boolean Aer2 = this.A02.Aer(281582352400510L, r0);
        AnonymousClass08X.A05(this.A01, "android_foreground_app_death_logging", Aer);
        AnonymousClass08X.A05(this.A01, "android_background_app_death_logging", Aer2);
        C25051Yd r2 = this.A02;
        AnonymousClass0XE r7 = AnonymousClass0XE.A05;
        int At3 = (int) r2.At3(563057328586796L, 30000, r7);
        int At32 = (int) this.A02.At3(563057328521259L, 0, r7);
        AnonymousClass0XE r13 = r7;
        int At33 = (int) this.A02.At3(563057328652333L, 1000, r13);
        boolean Aer3 = this.A02.Aer(2306124591566028925L, r7);
        boolean Aer4 = this.A02.Aer(281582351614069L, r7);
        boolean Aer5 = this.A02.Aer(281582351745143L, r13);
        boolean Aer6 = this.A02.Aer(281582351548532L, r13);
        C25051Yd r9 = this.A02;
        AnonymousClass0XE r11 = AnonymousClass0XE.A05;
        boolean Aer7 = r9.Aer(281582352007288L, r13);
        AnonymousClass08X.A04(this.A01, "app_state_file_writing_maximum_time_between_writes_foreground_ms", At3);
        AnonymousClass08X.A04(this.A01, "app_state_file_writing_maximum_time_between_writes_background_ms", At32);
        AnonymousClass08X.A04(this.A01, "app_state_file_writing_minimum_time_between_writes_ms", At33);
        AnonymousClass08X.A05(this.A01, "app_state_file_writing_non_critical_writes_lower_priority", Aer3);
        AnonymousClass08X.A05(this.A01, "app_state_log_uncaught_exceptions", Aer4);
        AnonymousClass08X.A05(this.A01, "app_state_log_vm_oom", Aer5);
        AnonymousClass08X.A05(this.A01, "app_state_log_self_sigkill", Aer6);
        AnonymousClass08X.A05(this.A01, "monitor_home_task_switcher_event", Aer7);
        AnonymousClass08X.A05(this.A01, "app_state_log_private_dirty_mem_usage", this.A02.Aer(281582351482995L, r13));
        int i = 0;
        AnonymousClass08X.A05(this.A01, "app_state_report_healthy_app_state", this.A02.Aes(281582350958702L, false, r13));
        long At34 = this.A02.At3(563057327734824L, 0, r11);
        if (At34 <= 2147483647L) {
            i = (int) At34;
        }
        AnonymousClass08X.A04(this.A01, "app_state_report_healthy_app_state_rate", i);
        AnonymousClass08X.A05(this.A01, "app_state_native_late_init", this.A02.Aer(281582351089775L, r13));
        AnonymousClass08X.A05(this.A01, "app_state_join_logger_thread_on_device_shutdown", this.A02.Aer(281582350893165L, r13));
        AnonymousClass08X.A05(this.A01, "app_state_log_serialize_fg_anr_info", this.A02.Aer(281582352728195L, r13));
        AnonymousClass08X.A04(this.A01, "app_state_log_write_policy", (int) this.A02.At4(563057327931434L, r13));
        AnonymousClass08X.A05(this.A01, "app_state_log_write_free_internal_disk_space", this.A02.Aer(281582351351921L, r13));
        AnonymousClass08X.A05(this.A01, "report_all_process_memory_usage", this.A02.Aer(281582351286384L, r13));
        AnonymousClass08X.A05(this.A01, "monitor_pending_stops", this.A02.Aer(281582352138362L, r13));
        AnonymousClass08X.A05(this.A01, "log_finalizer_latency", this.A02.Aer(281582351417458L, r13));
        AnonymousClass08X.A05(this.A01, "log_video", this.A02.Aer(281582351679606L, r13));
        AnonymousClass08X.A05(this.A01, "monitor_pending_launches", this.A02.Aer(281582352072825L, r13));
        C25051Yd r02 = this.A02;
        AnonymousClass0XE r5 = AnonymousClass0XE.A05;
        AnonymousClass08X.A04(this.A01, "foreground_state_initialization_policy", (int) r02.At3(563057327865897L, 0, r5));
        AnonymousClass08X.A05(this.A01, "monitor_process_importance", this.A02.Aer(281582352203899L, r13));
        AnonymousClass08X.A05(this.A01, "monitor_native_library", this.A02.Aer(281582352269436L, r13));
        AnonymousClass08X.A04(this.A01, "time_between_importance_queries", (int) this.A02.At3(563057329504302L, 0, r5));
        AnonymousClass08X.A05(this.A01, "write_process_importance_field", this.A02.Aer(281582352859268L, r13));
    }

    private AnonymousClass0GU(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A02 = AnonymousClass0WT.A00(r3);
        this.A01 = AnonymousClass1YA.A00(r3);
    }

    private void A02() {
        List<Pair> list;
        C002001j A002 = AnonymousClass01P.A00();
        if (A002 != null) {
            AnonymousClass0Nd r5 = new AnonymousClass0Nd((AnonymousClass09P) AnonymousClass1XX.A03(AnonymousClass1Y3.Amr, this.A00), (ExecutorService) AnonymousClass1XX.A03(AnonymousClass1Y3.Aoc, this.A00));
            synchronized (A002) {
                A002.A01 = r5;
                list = A002.A02;
                A002.A02 = null;
            }
            if (list != null) {
                for (Pair pair : list) {
                    AnonymousClass07A.A04(r5.A01, new AnonymousClass0Js(r5, (Throwable) pair.second, (String) pair.first), 1111228372);
                }
            }
        }
    }

    public int Ai5() {
        return 25;
    }

    public void init() {
        int A032 = C000700l.A03(1056604152);
        AppStateLogFile.init();
        A03();
        A02();
        C000700l.A09(-964946070, A032);
    }

    public void BTE(int i) {
        A03();
    }
}
