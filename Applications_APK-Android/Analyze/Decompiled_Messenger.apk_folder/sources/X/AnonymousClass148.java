package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.concurrent.CancellationException;

/* renamed from: X.148  reason: invalid class name */
public final class AnonymousClass148 extends AnonymousClass0lj {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C55502oB A01;

    public AnonymousClass148(C55502oB r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void A03(CancellationException cancellationException) {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, this.A01.A00)).markerEnd(5505173, this.A00, (short) 4);
    }
}
