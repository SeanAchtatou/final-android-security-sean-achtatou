package X;

import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1I3  reason: invalid class name */
public final class AnonymousClass1I3 implements AnonymousClass1GU {
    public final /* synthetic */ MigColorScheme A00;
    public final /* synthetic */ AnonymousClass10J A01;

    public AnonymousClass1I3(MigColorScheme migColorScheme, AnonymousClass10J r2) {
        this.A00 = migColorScheme;
        this.A01 = r2;
    }

    public int Adt() {
        return AnonymousClass1KA.A00(this.A00.AfS());
    }

    public C16930y3 Amq() {
        return AnonymousClass1JZ.A0K;
    }

    public C16930y3 Amr() {
        return this.A00.B2C();
    }

    public int Ao0() {
        return this.A00.B2B();
    }

    public int Ayy() {
        return AnonymousClass1KA.A01(this.A00.AfS());
    }

    public C16930y3 B5c() {
        return this.A01.B5d(this.A00);
    }

    public AnonymousClass1JL B5i() {
        return this.A01.mTextSize;
    }

    public AnonymousClass10M B7S() {
        return this.A01.B7S();
    }
}
