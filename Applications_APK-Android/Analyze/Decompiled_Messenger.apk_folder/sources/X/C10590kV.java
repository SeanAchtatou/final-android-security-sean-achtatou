package X;

import android.os.DeadObjectException;
import android.os.Message;
import android.os.RemoteException;

/* renamed from: X.0kV  reason: invalid class name and case insensitive filesystem */
public final class C10590kV implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.multiprocess.peer.PeerProcessManagerImpl$6";
    public final /* synthetic */ Message A00;
    public final /* synthetic */ C06130au A01;
    public final /* synthetic */ C07930eP A02;

    public C10590kV(C07930eP r1, C06130au r2, Message message) {
        this.A02 = r1;
        this.A01 = r2;
        this.A00 = message;
    }

    public void run() {
        try {
            this.A01.A00.send(this.A00);
        } catch (RemoteException e) {
            if (e instanceof DeadObjectException) {
                C07930eP.A03(this.A02, this.A01);
                return;
            }
            AnonymousClass09P r3 = this.A02.A06;
            r3.softReport("PeerProcessManagerImpl", "RemoteException occurred when sending the message to peer " + this.A01.A02, e);
        }
    }
}
