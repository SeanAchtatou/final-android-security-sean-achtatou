package X;

import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.18N  reason: invalid class name */
public final class AnonymousClass18N implements C05460Za {
    private static volatile AnonymousClass18N A04;
    public String A00 = null;
    public final AnonymousClass09P A01;
    public final C04310Tq A02;
    private final C001500z A03;

    public void clearUserData() {
        this.A00 = null;
    }

    public static final AnonymousClass18N A01(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (AnonymousClass18N.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        AnonymousClass0VG A003 = AnonymousClass0VG.A00(AnonymousClass1Y3.As6, applicationInjector);
                        C001500z A05 = AnonymousClass0UU.A05(applicationInjector);
                        AnonymousClass09P A012 = C04750Wa.A01(applicationInjector);
                        AnonymousClass0WT.A00(applicationInjector);
                        A04 = new AnonymousClass18N(A003, A05, A012);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public String A02() {
        String A06;
        if (this.A03 == C001500z.A07) {
            String str = this.A00;
            if (str == null) {
                C48942bO r0 = (C48942bO) this.A02.get();
                str = "deviceidinvalid";
                if (r0 == null) {
                    this.A01.CGS("tincan", "failed to get id-key store");
                    C010708t.A0K("TincanDeviceIdHolder", "Could not retrieve a valid identity key store when generating Tincan device ID");
                } else {
                    C60872xy ApI = r0.ApI();
                    if (ApI == null) {
                        A06 = null;
                    } else {
                        A06 = AnonymousClass18G.A01.A05().A06(ApI.A00.A00.A00());
                    }
                    if (A06 == null) {
                        this.A01.CGS("tincan", "failed to device id");
                        C010708t.A0I("TincanDeviceIdHolder", "Could not retrieve a valid identity key to go into Tincan device ID");
                    } else {
                        str = A06;
                    }
                }
                this.A00 = str;
            }
            return str.replaceAll("-", BuildConfig.FLAVOR);
        }
        this.A01.CGS("tincan", "accessed_in_other_app");
        return "deviceidinvalid";
    }

    private AnonymousClass18N(C04310Tq r2, C001500z r3, AnonymousClass09P r4) {
        this.A02 = r2;
        this.A03 = r3;
        this.A01 = r4;
    }

    public static final AnonymousClass18N A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public boolean A03() {
        if (A02() == "deviceidinvalid") {
            return true;
        }
        return false;
    }
}
