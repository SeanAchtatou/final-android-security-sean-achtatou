package X;

import com.facebook.messaging.media.mediapicker.dialog.params.PickMediaDialogParams;
import com.facebook.share.model.ComposerAppAttribution;
import com.facebook.share.model.ShareItem;
import java.util.List;

/* renamed from: X.20d  reason: invalid class name and case insensitive filesystem */
public final class C400620d {
    public PickMediaDialogParams A00;
    public ComposerAppAttribution A01;
    public ShareItem A02;
    public String A03;
    public List A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public final C88974Na A0B;

    public C400620d(C88974Na r1) {
        this.A0B = r1;
    }
}
