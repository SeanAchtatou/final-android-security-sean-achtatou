package X;

import android.content.Context;
import android.text.TextUtils;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Rc  reason: invalid class name and case insensitive filesystem */
public final class C03950Rc implements C06670bt {
    private static volatile C03950Rc A03;
    private AnonymousClass0UN A00;
    private final AnonymousClass0US A01;
    private final AnonymousClass0US A02;

    public synchronized void BTE(int i) {
        A03(this);
    }

    public static final C03950Rc A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C03950Rc.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C03950Rc(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private String A01() {
        String B4K = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).B4K(846456450056495L, null, AnonymousClass0XE.A05);
        String A022 = A02((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, this.A00), "com.facebook.loom.config.file");
        if (!TextUtils.isEmpty(A022)) {
            return A022;
        }
        return B4K;
    }

    public static boolean A03(C03950Rc r5) {
        boolean AbO = ((AnonymousClass1YI) r5.A02.get()).AbO(AnonymousClass1Y3.A4a, false);
        String A012 = r5.A01();
        if (A012 != null && (!AbO || !BuildConfig.FLAVOR.equals(A012))) {
            boolean z = false;
            if (C004505k.A0F.get() != null) {
                z = true;
            }
            if (z) {
                C004505k.A00().A09(new AnonymousClass09G((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, r5.A00), A012, r5));
                return true;
            }
        }
        return false;
    }

    public void A04(boolean z) {
        if (!z) {
            ((AnonymousClass09P) this.A01.get()).CGS("ProfiloConfigUpdater", "Could not remove init file based config");
        }
    }

    public void A05(boolean z) {
        if (!z) {
            ((AnonymousClass09P) this.A01.get()).CGS("ProfiloConfigUpdater", "Could not read config file");
        }
    }

    public void A06(boolean z) {
        if (!z) {
            ((AnonymousClass09P) this.A01.get()).CGS("ProfiloConfigUpdater", "Failure in parsing json (v2) from MobileConfig.");
        }
    }

    public void A07(boolean z) {
        if (!z) {
            ((AnonymousClass09P) this.A01.get()).CGS("ProfiloConfigUpdater", "Could not write init file based config");
        }
    }

    private C03950Rc(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = C04750Wa.A03(r3);
        this.A02 = AnonymousClass0UQ.A00(AnonymousClass1Y3.AcD, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001e, code lost:
        if (r1.exists() == false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A02(android.content.Context r5, java.lang.String r6) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            r4 = 0
            if (r0 != 0) goto L_0x0020
            java.lang.String r2 = X.AnonymousClass00I.A02(r6)
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x0020
            java.io.File r1 = new java.io.File
            java.io.File r0 = r5.getCacheDir()
            r1.<init>(r0, r2)
            boolean r0 = r1.exists()
            if (r0 != 0) goto L_0x0021
        L_0x0020:
            r1 = r4
        L_0x0021:
            if (r1 != 0) goto L_0x0024
            return r4
        L_0x0024:
            java.nio.charset.Charset r0 = com.google.common.base.Charsets.UTF_8     // Catch:{ IOException -> 0x002b }
            java.lang.String r0 = X.AnonymousClass0q2.A01(r1, r0)     // Catch:{ IOException -> 0x002b }
            return r0
        L_0x002b:
            r3 = move-exception
            java.lang.String r0 = r1.getName()
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            java.lang.String r1 = "Prflo/OverrideCnfigRdr"
            java.lang.String r0 = "Failed to read Profilo config from file %s"
            X.C010708t.A0U(r1, r3, r0, r2)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03950Rc.A02(android.content.Context, java.lang.String):java.lang.String");
    }

    public int Ai5() {
        return 473;
    }
}
