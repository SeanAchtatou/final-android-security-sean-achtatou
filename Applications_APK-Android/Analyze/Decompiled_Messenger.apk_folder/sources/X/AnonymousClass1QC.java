package X;

import java.util.concurrent.Executor;

/* renamed from: X.1QC  reason: invalid class name */
public final class AnonymousClass1QC implements AnonymousClass1Q3 {
    public final C22971No A00;
    public final C22761Ms A01;
    public final Executor A02;
    public final boolean A03;
    public final boolean A04;
    private final int A05;
    private final C23301Oz A06;
    private final C22861Nc A07;
    private final AnonymousClass1Q3 A08;
    private final boolean A09;

    public AnonymousClass1QC(C22861Nc r1, Executor executor, C22761Ms r3, C23301Oz r4, boolean z, boolean z2, boolean z3, AnonymousClass1Q3 r8, int i, C22971No r10) {
        C05520Zg.A02(r1);
        this.A07 = r1;
        C05520Zg.A02(executor);
        this.A02 = executor;
        C05520Zg.A02(r3);
        this.A01 = r3;
        C05520Zg.A02(r4);
        this.A06 = r4;
        this.A03 = z;
        this.A04 = z2;
        C05520Zg.A02(r8);
        this.A08 = r8;
        this.A09 = z3;
        this.A05 = i;
        this.A00 = r10;
    }

    public void ByS(C23581Qb r10, AnonymousClass1QK r11) {
        C23581Qb r1;
        try {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("DecodeProducer#produceResults");
            }
            AnonymousClass1QK r4 = r11;
            C23581Qb r3 = r10;
            if (!C006206h.A06(r11.A09.A02)) {
                r1 = new C52882jq(this, r3, r4, this.A09, this.A05);
            } else {
                r1 = new AnonymousClass1RR(this, r3, r4, new AnonymousClass1RP(this.A07), this.A06, this.A09, this.A05);
            }
            this.A08.ByS(r1, r11);
        } finally {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
        }
    }
}
