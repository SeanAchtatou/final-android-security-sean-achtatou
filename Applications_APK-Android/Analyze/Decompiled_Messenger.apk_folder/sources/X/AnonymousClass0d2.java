package X;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.view.MotionEvent;
import com.facebook.litho.TextContent;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0d2  reason: invalid class name */
public final class AnonymousClass0d2 extends Drawable implements Drawable.Callback, TextContent, AnonymousClass1MS {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public ColorStateList A06;
    public Path A07;
    public Path A08;
    public Handler A09;
    public Layout A0A;
    public C133086Ku A0B;
    public C88314Jt A0C;
    public AnonymousClass2CC A0D;
    public CharSequence A0E;
    public String A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public ClickableSpan[] A0K;
    public ImageSpan[] A0L;
    private Paint A0M;

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    private int A00(int i, int i2) {
        float paragraphLeft;
        float lineMax;
        int lineForVertical = this.A0A.getLineForVertical(i2);
        if (this.A0A.getAlignment() == Layout.Alignment.ALIGN_CENTER) {
            paragraphLeft = this.A0A.getLineLeft(lineForVertical);
            lineMax = this.A0A.getLineRight(lineForVertical);
        } else {
            boolean z = false;
            if (this.A0A.getParagraphDirection(lineForVertical) == -1) {
                z = true;
            }
            Layout layout = this.A0A;
            if (z) {
                paragraphLeft = ((float) layout.getWidth()) - this.A0A.getLineMax(lineForVertical);
            } else {
                paragraphLeft = (float) layout.getParagraphLeft(lineForVertical);
            }
            if (z) {
                lineMax = (float) this.A0A.getParagraphRight(lineForVertical);
            } else {
                lineMax = this.A0A.getLineMax(lineForVertical);
            }
        }
        float f = (float) i;
        if (f >= paragraphLeft && f <= lineMax) {
            try {
                return this.A0A.getOffsetForHorizontal(lineForVertical, f);
            } catch (ArrayIndexOutOfBoundsException unused) {
            }
        }
        return -1;
    }

    private void A02() {
        Handler handler = this.A09;
        if (handler != null) {
            AnonymousClass00S.A02(handler, this.A0B);
            this.A0B = null;
        }
        this.A0H = false;
    }

    public static void A03(AnonymousClass0d2 r2, int i, int i2) {
        if (Color.alpha(r2.A02) == 0) {
            return;
        }
        if (r2.A04 != i || r2.A03 != i2) {
            r2.A04 = i;
            r2.A03 = i2;
            Paint paint = r2.A0M;
            if (paint == null) {
                Paint paint2 = new Paint();
                r2.A0M = paint2;
                paint2.setColor(r2.A02);
            } else {
                paint.setColor(r2.A02);
            }
            r2.A0I = true;
            r2.invalidateSelf();
        }
    }

    private boolean A04(MotionEvent motionEvent) {
        if (this.A0C == null || motionEvent.getActionMasked() != 0 || !getBounds().contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
            return false;
        }
        return true;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r4v8, types: [android.view.View] */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02f3, code lost:
        if (r0 == false) goto L_0x02f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
        if (r24.getAction() == 0) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r0 != false) goto L_0x001e;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean BsU(android.view.MotionEvent r26, android.view.View r27) {
        /*
            r25 = this;
            r24 = r26
            r10 = r25
            r0 = r24
            boolean r0 = r10.A05(r0)
            if (r0 != 0) goto L_0x001e
            boolean r0 = r10.A0J
            if (r0 == 0) goto L_0x001b
            android.os.Handler r0 = r10.A09
            if (r0 == 0) goto L_0x001b
            int r1 = r24.getAction()
            r0 = 1
            if (r1 != 0) goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            if (r0 == 0) goto L_0x0415
        L_0x001e:
            int r15 = r24.getActionMasked()
            r0 = 3
            if (r15 != r0) goto L_0x0031
            r0 = 0
            A03(r10, r0, r0)
            r10.A02()
            r0 = 0
        L_0x002d:
            if (r0 == 0) goto L_0x0415
            r0 = 1
            return r0
        L_0x0031:
            r0 = 2
            if (r15 != r0) goto L_0x006f
            boolean r0 = r10.A0H
            if (r0 != 0) goto L_0x006f
            X.6Ku r0 = r10.A0B
            if (r0 == 0) goto L_0x006f
            r6 = r24
            android.graphics.Rect r5 = r10.getBounds()
            float r0 = r6.getX()
            int r1 = (int) r0
            float r0 = r6.getY()
            int r0 = (int) r0
            boolean r0 = r5.contains(r1, r0)
            if (r0 == 0) goto L_0x006c
            float r0 = r6.getX()
            int r2 = (int) r0
            int r0 = r5.left
            int r2 = r2 - r0
            float r0 = r6.getY()
            int r1 = (int) r0
            int r0 = r5.top
            int r1 = r1 - r0
            android.text.style.ClickableSpan r1 = r10.A01(r2, r1)
            X.6Ku r0 = r10.A0B
            X.3an r0 = r0.A00
            if (r0 == r1) goto L_0x006f
        L_0x006c:
            r10.A02()
        L_0x006f:
            boolean r14 = r10.A0H
            r12 = 1
            r14 = r14 ^ r12
            if (r15 != r12) goto L_0x0078
            r10.A02()
        L_0x0078:
            android.graphics.Rect r4 = r10.getBounds()
            r2 = r24
            float r0 = r2.getX()
            int r1 = (int) r0
            float r0 = r2.getY()
            int r0 = (int) r0
            boolean r0 = r4.contains(r1, r0)
            if (r0 != 0) goto L_0x0090
            r0 = 0
            goto L_0x002d
        L_0x0090:
            float r0 = r24.getX()
            int r2 = (int) r0
            int r0 = r4.left
            int r2 = r2 - r0
            float r0 = r24.getY()
            int r1 = (int) r0
            int r0 = r4.top
            int r1 = r1 - r0
            android.text.style.ClickableSpan r8 = r10.A01(r2, r1)
            if (r8 != 0) goto L_0x0121
            float r5 = r10.A00
            r0 = 0
            int r0 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0121
            float r6 = (float) r2
            float r4 = (float) r1
            android.graphics.Region r13 = new android.graphics.Region
            r13.<init>()
            android.graphics.Region r3 = new android.graphics.Region
            r3.<init>()
            android.graphics.Path r0 = r10.A08
            if (r0 != 0) goto L_0x00c4
            android.graphics.Path r0 = new android.graphics.Path
            r0.<init>()
            r10.A08 = r0
        L_0x00c4:
            android.text.Layout r0 = r10.A0A
            int r2 = X.C22561Ly.A01(r0)
            android.text.Layout r0 = r10.A0A
            int r1 = X.C22561Ly.A00(r0)
            r0 = 0
            r3.set(r0, r0, r2, r1)
            android.graphics.Path r0 = r10.A08
            r0.reset()
            android.graphics.Path r1 = r10.A08
            android.graphics.Path$Direction r0 = android.graphics.Path.Direction.CW
            r1.addCircle(r6, r4, r5, r0)
            android.graphics.Path r0 = r10.A08
            r13.setPath(r0, r3)
            android.text.style.ClickableSpan[] r11 = r10.A0K
            int r9 = r11.length
            r8 = 0
            r17 = r8
            r7 = 0
        L_0x00ec:
            if (r7 >= r9) goto L_0x011f
            r16 = r11[r7]
            java.lang.CharSequence r6 = r10.A0E
            android.text.Spanned r6 = (android.text.Spanned) r6
            android.text.Layout r5 = r10.A0A
            r4 = r16
            android.graphics.Region r2 = new android.graphics.Region
            r2.<init>()
            android.graphics.Path r1 = new android.graphics.Path
            r1.<init>()
            int r0 = r6.getSpanStart(r4)
            int r4 = r6.getSpanEnd(r4)
            r5.getSelectionPath(r0, r4, r1)
            r2.setPath(r1, r3)
            android.graphics.Region$Op r0 = android.graphics.Region.Op.INTERSECT
            boolean r0 = r2.op(r13, r0)
            if (r0 == 0) goto L_0x011c
            if (r17 != 0) goto L_0x0121
            r17 = r16
        L_0x011c:
            int r7 = r7 + 1
            goto L_0x00ec
        L_0x011f:
            r8 = r17
        L_0x0121:
            if (r8 != 0) goto L_0x012a
            r0 = 0
            A03(r10, r0, r0)
            r0 = 0
            goto L_0x002d
        L_0x012a:
            r1 = r27
            if (r15 != r12) goto L_0x03e7
            r0 = 0
            A03(r10, r0, r0)
            if (r14 == 0) goto L_0x02f8
            X.2CC r3 = r10.A0D
            if (r3 == 0) goto L_0x02f5
            boolean r0 = r8 instanceof X.C70513am
            r4 = 4
            if (r0 == 0) goto L_0x0329
            int r2 = X.AnonymousClass1Y3.AKw
            X.3Em r0 = r3.A04
            X.0UN r0 = r0.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r4, r2, r0)
            X.6Ll r7 = (X.C133246Ll) r7
            X.3Wl r9 = r3.A05
            X.3Fh r6 = r3.A03
            android.content.Context r5 = r3.A00
            X.0qW r0 = r3.A02
            r17 = r0
            X.3Lm r0 = r3.A06
            r18 = r0
            r0 = r8
            X.3am r0 = (X.C70513am) r0
            java.lang.String r0 = r0.A00
            r21 = r0
            android.text.Spannable r0 = r3.A01
            r22 = r0
            X.6Kn r14 = X.C133026Kn.A0K
            r15 = r1
            com.facebook.messaging.model.messages.Message r4 = r9.A04
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r4.A0U
            com.facebook.user.model.UserKey r11 = com.facebook.messaging.model.threadkey.ThreadKey.A07(r0)
            int r3 = X.AnonymousClass1Y3.AxE
            X.0UN r2 = r7.A00
            r0 = 8
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r2)
            X.0t0 r0 = (X.C14300t0) r0
            com.facebook.user.model.User r11 = r0.A03(r11)
            if (r11 == 0) goto L_0x01d5
            boolean r0 = r11.A0F()
            if (r0 == 0) goto L_0x01d5
            r3 = 10
            int r2 = X.AnonymousClass1Y3.B4H
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.5VP r0 = (X.AnonymousClass5VP) r0
            r3 = r21
            X.1ZE r2 = r0.A02
            java.lang.String r0 = "messenger_business_integrity_message_url_click"
            X.0bW r12 = r2.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 368(0x170, float:5.16E-43)
            r2.<init>(r12, r0)
            boolean r0 = r2.A0G()
            if (r0 == 0) goto L_0x01d5
            r0 = 65
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r2.A0D(r0, r3)
            java.lang.String r0 = r11.A0j
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r3 = r2.A0N(r0)
            java.lang.String r0 = "messenger_business_integrity_message_url_click"
            r3.A0L(r0)
            java.lang.String r2 = r4.A0q
            java.lang.String r0 = "message_id"
            r3.A0D(r0, r2)
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r4.A0K
            java.lang.String r2 = r0.A00()
            r0 = 316(0x13c, float:4.43E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r3.A0D(r0, r2)
            r3.A06()
        L_0x01d5:
            r3 = r21
            com.facebook.messaging.model.messages.Message r0 = r9.A04
            com.facebook.messaging.model.share.Share r2 = X.C28841fS.A06(r0)
            if (r2 == 0) goto L_0x0326
            java.lang.String r0 = r2.A09
            boolean r0 = com.google.common.base.Objects.equal(r3, r0)
            if (r0 == 0) goto L_0x0326
            if (r6 == 0) goto L_0x0326
            r6.BoU(r2)
            r0 = 1
        L_0x01ed:
            if (r0 != 0) goto L_0x02f2
            android.net.Uri r13 = android.net.Uri.parse(r21)
            X.6Lo r12 = X.C133276Lo.A00(r13)
            r3 = 11
            int r2 = X.AnonymousClass1Y3.AIn
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.6yy r0 = (X.C150956yy) r0
            X.6uH r11 = r0.A00
            if (r11 == 0) goto L_0x022e
            boolean r0 = r11.A01()
            if (r0 == 0) goto L_0x022e
            X.3Iw r4 = X.C66123Iw.A0Z
            int r0 = r12.ordinal()
            switch(r0) {
                case 0: goto L_0x0322;
                case 1: goto L_0x0216;
                case 2: goto L_0x031e;
                case 3: goto L_0x031a;
                default: goto L_0x0216;
            }
        L_0x0216:
            X.3Iw r0 = X.C66123Iw.A0Z
            if (r4 == r0) goto L_0x022e
            X.6yt r3 = new X.6yt
            r3.<init>()
            java.lang.Integer r2 = X.AnonymousClass07B.A0A
            X.6yx r0 = X.C150946yx.A01
            r3.A01(r2, r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A0B
            r3.A01(r0, r4)
            r11.A00(r3)
        L_0x022e:
            r3 = 4
            int r2 = X.AnonymousClass1Y3.BIh
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.3Qg r0 = (X.C67803Qg) r0
            boolean r0 = r0.A04(r5, r13)
            if (r0 != 0) goto L_0x02f2
            X.6Lo r0 = X.C133276Lo.A05
            if (r12 != r0) goto L_0x0315
            boolean r0 = r1 instanceof android.widget.TextView
            if (r0 == 0) goto L_0x0308
            android.widget.TextView r15 = (android.widget.TextView) r15
            java.lang.CharSequence r0 = r15.getText()
            android.text.Spanned r0 = (android.text.Spanned) r0
            int r4 = r0.getSpanStart(r8)
            int r3 = r0.getSpanEnd(r8)
            if (r4 < 0) goto L_0x0306
            if (r3 < 0) goto L_0x0306
            android.text.Layout r2 = r15.getLayout()
            int r0 = r2.getLineForOffset(r4)
            float r9 = r2.getPrimaryHorizontal(r4)
            float r4 = r2.getPrimaryHorizontal(r3)
            android.graphics.Rect r3 = new android.graphics.Rect
            r3.<init>()
            r2.getLineBounds(r0, r3)
            int r0 = r3.left
            float r2 = (float) r0
            int r0 = r15.getCompoundPaddingLeft()
            float r0 = (float) r0
            float r0 = r0 + r9
            float r2 = r2 + r0
            int r0 = (int) r2
            r3.left = r0
            int r2 = r15.getCompoundPaddingLeft()
            int r0 = (int) r4
            int r2 = r2 + r0
            r3.right = r2
            int r2 = r3.top
            int r0 = r15.getCompoundPaddingTop()
            int r2 = r2 + r0
            r3.top = r2
            int r2 = r3.bottom
            int r0 = r15.getCompoundPaddingTop()
            int r2 = r2 + r0
            r3.bottom = r2
        L_0x029a:
            if (r6 == 0) goto L_0x0304
            if (r3 == 0) goto L_0x0304
            android.view.View r4 = r6.BjW(r3, r15)
        L_0x02a2:
            if (r4 == 0) goto L_0x02a5
            r15 = r4
        L_0x02a5:
            X.70z r3 = new X.70z
            r3.<init>(r5, r15)
            X.27P r6 = new X.27P
            android.content.Context r0 = r3.A02
            r6.<init>(r0)
            r2 = 2131558427(0x7f0d001b, float:1.874217E38)
            X.DlV r0 = r3.A03
            r6.inflate(r2, r0)
            java.lang.String r9 = r13.getSchemeSpecificPart()
            r6 = 9
            int r2 = X.AnonymousClass1Y3.AaZ
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.2eE r0 = (X.C50562eE) r0
            com.facebook.user.model.User r19 = r0.A05(r9)
            if (r19 != 0) goto L_0x02fb
            X.DlV r2 = r3.A03
            r0 = 2131299039(0x7f090adf, float:1.8216068E38)
            r2.removeItem(r0)
        L_0x02d7:
            X.6Ln r0 = new X.6Ln
            r15 = r14
            r16 = r18
            r17 = r8
            r18 = r22
            r11 = r0
            r12 = r7
            r14 = r5
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)
            r3.A01 = r0
            X.4Jz r0 = new X.4Jz
            r0.<init>(r4)
            r3.A00 = r0
            r3.A00()
        L_0x02f2:
            r0 = 1
        L_0x02f3:
            if (r0 != 0) goto L_0x02f8
        L_0x02f5:
            r8.onClick(r1)
        L_0x02f8:
            r0 = 1
            goto L_0x002d
        L_0x02fb:
            X.DlV r2 = r3.A03
            r0 = 2131299035(0x7f090adb, float:1.821606E38)
            r2.removeItem(r0)
            goto L_0x02d7
        L_0x0304:
            r4 = 0
            goto L_0x02a2
        L_0x0306:
            r3 = 0
            goto L_0x029a
        L_0x0308:
            r15 = r7
            r16 = r5
            r19 = r9
            r20 = r8
            r23 = r14
            r15.A04(r16, r17, r18, r19, r20, r21, r22, r23)
            goto L_0x02f2
        L_0x0315:
            boolean r0 = X.C133246Ll.A03(r7, r13, r5, r14)
            goto L_0x02f3
        L_0x031a:
            X.3Iw r4 = X.C66123Iw.A0J
            goto L_0x0216
        L_0x031e:
            X.3Iw r4 = X.C66123Iw.A0K
            goto L_0x0216
        L_0x0322:
            X.3Iw r4 = X.C66123Iw.A0G
            goto L_0x0216
        L_0x0326:
            r0 = 0
            goto L_0x01ed
        L_0x0329:
            boolean r0 = r8 instanceof X.C96604j2
            if (r0 == 0) goto L_0x03e4
            int r2 = X.AnonymousClass1Y3.AKw
            X.3Em r0 = r3.A04
            X.0UN r0 = r0.A00
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r4, r2, r0)
            X.6Ll r13 = (X.C133246Ll) r13
            r0 = r8
            X.4j2 r0 = (X.C96604j2) r0
            java.lang.String r14 = r0.A00
            X.0qW r6 = r3.A02
            X.5wJ r15 = new X.5wJ
            r15.<init>(r3)
            int r2 = X.AnonymousClass1Y3.AC5
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r2, r0)
            X.3Pm r0 = (X.C67603Pm) r0
            int r2 = X.AnonymousClass1Y3.BQ3
            X.0UN r0 = r0.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r0)
            X.0dK r2 = (X.C07380dK) r2
            java.lang.String r0 = "mention_clicked"
            r2.A03(r0)
            int r2 = X.AnonymousClass1Y3.AxE
            X.0UN r0 = r13.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r0)
            X.0t0 r3 = (X.C14300t0) r3
            com.facebook.user.model.UserKey r2 = new com.facebook.user.model.UserKey
            X.1aB r0 = X.C25651aB.A03
            r2.<init>(r0, r14)
            com.facebook.user.model.User r5 = r3.A03(r2)
            if (r5 == 0) goto L_0x02f2
            X.6Lf r3 = new X.6Lf
            r3.<init>()
            java.lang.String r0 = r5.A09()
            r3.A02 = r0
            X.52u r2 = new X.52u
            r2.<init>()
            r0 = 10
            r2.A02 = r0
            r0 = 2131827217(0x7f111a11, float:1.928734E38)
            r2.A03 = r0
            com.facebook.messaging.dialog.MenuDialogItem r0 = r2.A00()
            r3.A00(r0)
            java.lang.Boolean r0 = r13.A01
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x03b6
            boolean r0 = r5.A1c
            if (r0 != 0) goto L_0x03b6
            X.52u r2 = new X.52u
            r2.<init>()
            r0 = 11
            r2.A02 = r0
            r0 = 2131827218(0x7f111a12, float:1.9287342E38)
            r2.A03 = r0
            com.facebook.messaging.dialog.MenuDialogItem r0 = r2.A00()
            r3.A00(r0)
        L_0x03b6:
            r4 = 1
            r3.A04 = r12
            com.facebook.messaging.dialog.MenuDialogParams r0 = new com.facebook.messaging.dialog.MenuDialogParams
            r0.<init>(r3)
            com.facebook.messaging.dialog.MenuDialogFragment r3 = com.facebook.messaging.dialog.MenuDialogFragment.A00(r0)
            int r2 = X.AnonymousClass1Y3.B4J
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r2, r0)
            X.664 r0 = (X.AnonymousClass664) r0
            X.2IM r12 = new X.2IM
            r17 = r5
            r18 = r3
            r16 = r0
            r12.<init>(r13, r14, r15, r16, r17, r18)
            r3.A00 = r12
            X.0wo r2 = r6.A0T()
            java.lang.String r0 = "message_mention_menu_dialog"
            r3.A27(r2, r0, r4)
            goto L_0x02f2
        L_0x03e4:
            r0 = 0
            goto L_0x02f3
        L_0x03e7:
            if (r15 != 0) goto L_0x02f8
            boolean r0 = r8 instanceof X.C70523an
            if (r0 == 0) goto L_0x0404
            r2 = r8
            X.3an r2 = (X.C70523an) r2
            X.6Ku r4 = new X.6Ku
            r4.<init>(r10, r2, r1)
            r10.A0B = r4
            android.os.Handler r3 = r10.A09
            int r0 = android.view.ViewConfiguration.getLongPressTimeout()
            long r1 = (long) r0
            r0 = 481994743(0x1cbaa7f7, float:1.2351865E-21)
            X.AnonymousClass00S.A05(r3, r4, r1, r0)
        L_0x0404:
            java.lang.CharSequence r0 = r10.A0E
            android.text.Spanned r0 = (android.text.Spanned) r0
            int r1 = r0.getSpanStart(r8)
            int r0 = r0.getSpanEnd(r8)
            A03(r10, r1, r0)
            goto L_0x02f8
        L_0x0415:
            r0 = r24
            boolean r0 = r10.A04(r0)
            if (r0 == 0) goto L_0x0451
            android.graphics.Rect r3 = r10.getBounds()
            float r0 = r24.getX()
            int r2 = (int) r0
            int r0 = r3.left
            int r2 = r2 - r0
            float r0 = r24.getY()
            int r1 = (int) r0
            int r0 = r3.top
            int r1 = r1 - r0
            int r1 = r10.A00(r2, r1)
            if (r1 < 0) goto L_0x0451
            java.lang.CharSequence r0 = r10.A0E
            int r0 = r0.length()
            if (r1 > r0) goto L_0x0451
            X.4Jt r0 = r10.A0C
            X.10N r2 = r0.A00
            X.Evs r1 = new X.Evs
            r1.<init>()
            X.0zV r0 = r2.A00
            X.0zT r0 = r0.Alq()
            r0.AXa(r2, r1)
        L_0x0451:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0d2.BsU(android.view.MotionEvent, android.view.View):boolean");
    }

    public void draw(Canvas canvas) {
        if (this.A0A != null) {
            int save = canvas.save();
            Rect bounds = getBounds();
            if (this.A0G) {
                canvas.clipRect(bounds);
            }
            canvas.translate((float) bounds.left, ((float) bounds.top) + this.A01);
            try {
                Layout layout = this.A0A;
                Path path = null;
                if (!(this.A04 == this.A03 || Color.alpha(this.A02) == 0)) {
                    if (this.A0I) {
                        if (this.A07 == null) {
                            this.A07 = new Path();
                        }
                        this.A0A.getSelectionPath(this.A04, this.A03, this.A07);
                        this.A0I = false;
                    }
                    path = this.A07;
                }
                layout.draw(canvas, path, this.A0M, 0);
                canvas.restoreToCount(save);
            } catch (ArrayIndexOutOfBoundsException e) {
                String message = e.getMessage();
                StringBuilder sb = new StringBuilder(" [");
                sb.append(this.A0F);
                sb.append("] ");
                CharSequence charSequence = this.A0E;
                if (charSequence instanceof SpannableStringBuilder) {
                    Object[] spans = ((SpannableStringBuilder) charSequence).getSpans(0, charSequence.length(), Object.class);
                    sb.append("spans: ");
                    for (Object obj : spans) {
                        sb.append(obj.getClass().getSimpleName());
                        sb.append(", ");
                    }
                }
                sb.append("ellipsizedWidth: ");
                sb.append(this.A0A.getEllipsizedWidth());
                sb.append(", lineCount: ");
                sb.append(this.A0A.getLineCount());
                throw new ArrayIndexOutOfBoundsException(AnonymousClass08S.A0J(message, sb.toString()));
            }
        }
    }

    public List getTextItems() {
        CharSequence charSequence = this.A0E;
        if (charSequence != null) {
            return Collections.singletonList(charSequence);
        }
        return Collections.emptyList();
    }

    public boolean isStateful() {
        if (this.A06 != null) {
            return true;
        }
        return false;
    }

    public boolean onStateChange(int[] iArr) {
        Layout layout;
        int colorForState;
        if (!(this.A06 == null || (layout = this.A0A) == null || (colorForState = this.A06.getColorForState(iArr, this.A05)) == layout.getPaint().getColor())) {
            this.A0A.getPaint().setColor(colorForState);
            invalidateSelf();
        }
        return super.onStateChange(iArr);
    }

    private ClickableSpan A01(int i, int i2) {
        ClickableSpan[] clickableSpanArr;
        int A002 = A00(i, i2);
        if (A002 < 0 || (clickableSpanArr = (ClickableSpan[]) ((Spanned) this.A0E).getSpans(A002, A002, ClickableSpan.class)) == null || clickableSpanArr.length <= 0) {
            return null;
        }
        return clickableSpanArr[0];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        if (r5 == 0) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A05(android.view.MotionEvent r7) {
        /*
            r6 = this;
            int r5 = r7.getActionMasked()
            r4 = 0
            r0 = 1
            if (r5 == r0) goto L_0x000b
            r3 = 0
            if (r5 != 0) goto L_0x000c
        L_0x000b:
            r3 = 1
        L_0x000c:
            boolean r0 = r6.A0J
            if (r0 == 0) goto L_0x0026
            android.graphics.Rect r2 = r6.getBounds()
            float r0 = r7.getX()
            int r1 = (int) r0
            float r0 = r7.getY()
            int r0 = (int) r0
            boolean r0 = r2.contains(r1, r0)
            if (r0 == 0) goto L_0x0026
            if (r3 != 0) goto L_0x0029
        L_0x0026:
            r0 = 3
            if (r5 != r0) goto L_0x002a
        L_0x0029:
            r4 = 1
        L_0x002a:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0d2.A05(android.view.MotionEvent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r3.getAction() == 0) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean CE9(android.view.MotionEvent r3) {
        /*
            r2 = this;
            boolean r0 = r2.A05(r3)
            if (r0 != 0) goto L_0x001f
            boolean r0 = r2.A0J
            if (r0 == 0) goto L_0x0015
            android.os.Handler r0 = r2.A09
            if (r0 == 0) goto L_0x0015
            int r1 = r3.getAction()
            r0 = 1
            if (r1 != 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            if (r0 != 0) goto L_0x001f
            boolean r1 = r2.A04(r3)
            r0 = 0
            if (r1 == 0) goto L_0x0020
        L_0x001f:
            r0 = 1
        L_0x0020:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0d2.CE9(android.view.MotionEvent):boolean");
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }
}
