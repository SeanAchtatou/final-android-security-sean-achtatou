package X;

import android.content.BroadcastReceiver;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;

@UserScoped
/* renamed from: X.1oJ  reason: invalid class name and case insensitive filesystem */
public final class C33891oJ {
    private static C05540Zi A06;
    public AnonymousClass0UN A00;
    public boolean A01;
    public final BroadcastReceiver A02;
    public final C05180Xy A03 = new C05180Xy();
    public final C34011oV A04;
    public volatile ImmutableSet A05 = RegularImmutableSet.A05;

    public static final C33891oJ A00(AnonymousClass1XY r5) {
        C33891oJ r0;
        synchronized (C33891oJ.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r3 = (AnonymousClass1XY) A06.A01();
                    A06.A00 = new C33891oJ(r3, new C35421rC(r3));
                }
                C05540Zi r1 = A06;
                r0 = (C33891oJ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A01(C33891oJ r5, ImmutableList immutableList, int i, String str) {
        C35431rD r4 = new C35431rD(r5, str, immutableList, i);
        if (!((C25051Yd) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AOJ, r5.A00)).Aem(282372624876906L)) {
            ((AnonymousClass0WP) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A8a, r5.A00)).CIG(AnonymousClass08S.A0J("foregroundPrefetchThreads:", str), r4, AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE, AnonymousClass07B.A00);
            return;
        }
        AnonymousClass108 r1 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, r5.A00);
        r1.A02(r4);
        r1.A02 = "ForegroundPrefetchThreads";
        r1.A04 = immutableList.toString();
        r1.A03("ForUiThread");
        ((AnonymousClass0g4) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ASI, r5.A00)).A04(r1.A01(), "KeepExisting");
    }

    public String A02(ThreadKey threadKey) {
        String str;
        C34011oV r1 = this.A04;
        synchronized (r1.A04) {
            if (r1.A02) {
                str = (String) r1.A00.get(threadKey.A0J().hashCode(), "Unrequested");
            } else {
                str = "NoPrefetchInvoked";
            }
        }
        return str;
    }

    private C33891oJ(AnonymousClass1XY r4, C35421rC r5) {
        C46302Pn r0;
        this.A00 = new AnonymousClass0UN(7, r4);
        this.A04 = new C34011oV(r5, this);
        if (((C35221qs) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AP0, this.A00)).A02() > 0) {
            r0 = new C46302Pn(this);
        } else {
            r0 = null;
        }
        this.A02 = r0;
    }
}
