package X;

import com.google.common.base.Preconditions;

/* renamed from: X.1G6  reason: invalid class name */
public final class AnonymousClass1G6 {
    public final AnonymousClass100 A00;
    public final boolean A01;

    public AnonymousClass1G6(AnonymousClass100 r1, boolean z) {
        Preconditions.checkNotNull(r1);
        this.A00 = r1;
        this.A01 = z;
    }
}
