package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0j7  reason: invalid class name */
public abstract class AnonymousClass0j7 {
    public int A00 = 0;
    public C25817Cmi A01;
    public final C09820ik A02;
    public final AtomicReference A03 = new AtomicReference();
    public final AtomicReference A04 = new AtomicReference();
    private final C09800ii A05;

    public void A01() {
        AnonymousClass0j6 r3 = (AnonymousClass0j6) this;
        ((C16330ws) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Au3, r3.A00)).A01.put(r3, true);
    }

    public void A02() {
        AnonymousClass0j6 r3 = (AnonymousClass0j6) this;
        ((C16330ws) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Au3, r3.A00)).A01.remove(r3);
    }

    public Object A00() {
        File file;
        boolean z;
        ListenableFuture CIF;
        int i = this.A00;
        if (i == 2) {
            return this.A03.get();
        }
        if (i == 0) {
            A03();
            i = this.A00;
        }
        ListenableFuture listenableFuture = (ListenableFuture) this.A04.get();
        if (listenableFuture == null) {
            AtomicReference atomicReference = this.A04;
            C09800ii r3 = this.A05;
            C09820ik r1 = this.A02;
            if (r1.A02 == null) {
                synchronized (r1) {
                    if (r1.A02 == null) {
                        r1.A02 = r1.A0C();
                    }
                }
            }
            AnonymousClass1LI r2 = r1.A02;
            if (AnonymousClass1LJ.A01.contains(r2)) {
                CIF = null;
            } else {
                AnonymousClass1LJ.A01.add(r2);
                CIF = r3.A03().CIF(new AnonymousClass1LK(r3, r2));
            }
            if (atomicReference.compareAndSet(null, CIF)) {
                this.A05.A01 = this;
                A01();
            }
        } else if (listenableFuture.isDone()) {
            try {
                C22371Lb r32 = (C22371Lb) listenableFuture.get();
                if (!(r32 == null || (file = r32.A00) == null)) {
                    Object AUP = this.A02.A0D().AUP(file);
                    synchronized (this.A03) {
                        if (i == this.A00) {
                            this.A03.set(AUP);
                            this.A00 = 2;
                            z = true;
                        } else {
                            z = false;
                        }
                    }
                    if (z) {
                        if (!r32.A01) {
                            A02();
                        }
                    }
                }
            } catch (InterruptedException | ExecutionException unused) {
            }
        }
        return this.A03.get();
    }

    public void A03() {
        if (this.A00 == 0) {
            C09800ii r3 = this.A05;
            File A06 = r3.A00.A06();
            if (A06 != null && A06.isFile()) {
                r3.A00.A0A(5);
            }
            Object AUP = this.A02.A0D().AUP(A06);
            synchronized (this.A03) {
                if (this.A00 == 0) {
                    this.A00 = 1;
                    if (AUP != null) {
                        this.A03.compareAndSet(null, AUP);
                    }
                }
            }
        }
    }

    public AnonymousClass0j7(C09820ik r2, C09800ii r3) {
        this.A02 = r2;
        this.A05 = r3;
    }
}
