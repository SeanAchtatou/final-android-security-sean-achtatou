package X;

import android.view.View;
import com.facebook.litho.annotations.Comparable;
import com.facebook.payments.paymentmethods.model.AltPayPaymentMethod;

/* renamed from: X.1vH  reason: invalid class name */
public final class AnonymousClass1vH extends C17770zR {
    @Comparable(type = 13)
    public View.OnClickListener A00;
    @Comparable(type = 13)
    public C17840zZ A01;
    @Comparable(type = 13)
    public AltPayPaymentMethod A02;
    @Comparable(type = 13)
    public String A03;
    @Comparable(type = 13)
    public String A04;

    public AnonymousClass1vH() {
        super("AltPayComponent");
    }
}
