package X;

/* renamed from: X.0FV  reason: invalid class name */
public final class AnonymousClass0FV extends AnonymousClass0FM {
    public double childSystemTimeS;
    public double childUserTimeS;
    public double systemTimeS;
    public double userTimeS;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass0FV r6 = (AnonymousClass0FV) obj;
            if (!(Double.compare(r6.systemTimeS, this.systemTimeS) == 0 && Double.compare(r6.userTimeS, this.userTimeS) == 0 && Double.compare(r6.childSystemTimeS, this.childSystemTimeS) == 0 && Double.compare(r6.childUserTimeS, this.childUserTimeS) == 0)) {
                return false;
            }
        }
        return true;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A0B((AnonymousClass0FV) r1);
        return this;
    }

    /* renamed from: A09 */
    public AnonymousClass0FV A07(AnonymousClass0FV r5, AnonymousClass0FV r6) {
        if (r6 == null) {
            r6 = new AnonymousClass0FV();
        }
        if (r5 == null) {
            r6.A0B(this);
            return r6;
        }
        r6.systemTimeS = this.systemTimeS - r5.systemTimeS;
        r6.userTimeS = this.userTimeS - r5.userTimeS;
        r6.childSystemTimeS = this.childSystemTimeS - r5.childSystemTimeS;
        r6.childUserTimeS = this.childUserTimeS - r5.childUserTimeS;
        return r6;
    }

    /* renamed from: A0A */
    public AnonymousClass0FV A08(AnonymousClass0FV r5, AnonymousClass0FV r6) {
        if (r6 == null) {
            r6 = new AnonymousClass0FV();
        }
        if (r5 == null) {
            r6.A0B(this);
            return r6;
        }
        r6.systemTimeS = this.systemTimeS + r5.systemTimeS;
        r6.userTimeS = this.userTimeS + r5.userTimeS;
        r6.childSystemTimeS = this.childSystemTimeS + r5.childSystemTimeS;
        r6.childUserTimeS = this.childUserTimeS + r5.childUserTimeS;
        return r6;
    }

    public void A0B(AnonymousClass0FV r3) {
        this.userTimeS = r3.userTimeS;
        this.systemTimeS = r3.systemTimeS;
        this.childUserTimeS = r3.childUserTimeS;
        this.childSystemTimeS = r3.childSystemTimeS;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.systemTimeS);
        int i = (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        long doubleToLongBits2 = Double.doubleToLongBits(this.userTimeS);
        int i2 = (i * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)));
        long doubleToLongBits3 = Double.doubleToLongBits(this.childSystemTimeS);
        int i3 = (i2 * 31) + ((int) (doubleToLongBits3 ^ (doubleToLongBits3 >>> 32)));
        long doubleToLongBits4 = Double.doubleToLongBits(this.childUserTimeS);
        return (i3 * 31) + ((int) (doubleToLongBits4 ^ (doubleToLongBits4 >>> 32)));
    }

    public String toString() {
        return "CpuMetrics{userTimeS=" + this.userTimeS + ", systemTimeS=" + this.systemTimeS + ", childUserTimeS=" + this.childUserTimeS + ", childSystemTimeS=" + this.childSystemTimeS + '}';
    }
}
