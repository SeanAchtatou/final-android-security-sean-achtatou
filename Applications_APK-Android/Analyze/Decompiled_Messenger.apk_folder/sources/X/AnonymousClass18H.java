package X;

import com.google.common.base.Preconditions;

/* renamed from: X.18H  reason: invalid class name */
public final class AnonymousClass18H extends AnonymousClass18I {
    public AnonymousClass18H(C30481i7 r4, Character ch) {
        super(r4, ch);
        Preconditions.checkArgument(r4.A05.length == 64);
    }
}
