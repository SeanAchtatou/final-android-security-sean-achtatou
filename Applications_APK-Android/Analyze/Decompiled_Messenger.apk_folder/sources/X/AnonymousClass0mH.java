package X;

import com.facebook.graphservice.interfaces.Summary;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;
import java.util.Collection;
import java.util.Map;

/* renamed from: X.0mH  reason: invalid class name */
public class AnonymousClass0mH {
    public final long A00;
    public final AnonymousClass102 A01;
    public final Summary A02;
    public final Object A03;

    public Collection A00() {
        Object obj = this.A03;
        if (obj == null) {
            return RegularImmutableSet.A05;
        }
        if (obj instanceof Map) {
            return ((Map) obj).values();
        }
        if (obj instanceof Collection) {
            return (Collection) obj;
        }
        return ImmutableSet.A04(obj);
    }

    public AnonymousClass0mH(Object obj, AnonymousClass102 r2, long j, Summary summary) {
        this.A03 = obj;
        this.A01 = r2;
        this.A00 = j;
        this.A02 = summary;
    }
}
