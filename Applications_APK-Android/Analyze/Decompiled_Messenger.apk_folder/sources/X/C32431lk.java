package X;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import com.google.common.collect.ImmutableSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1lk  reason: invalid class name and case insensitive filesystem */
public final class C32431lk {
    public static final ImmutableSet A0A = ImmutableSet.A07("050_default", "750_high", "800_medium", "900_low");
    public static final Map A0B;
    public static final Map A0C;
    public static final Map A0D;
    public static final Map A0E;
    public static final Set A0F;
    public static final Set A0G;
    private static volatile C32431lk A0H;
    public AnonymousClass0UN A00;
    public C32441ll A01;
    public C32441ll A02;
    public C32441ll A03;
    public final Context A04;
    public final AnonymousClass32J A05;
    public final Map A06 = new HashMap();
    public final Map A07 = new HashMap();
    public final Map A08 = new HashMap();
    public final Map A09 = new HashMap();

    static {
        HashSet hashSet = new HashSet();
        A0G = hashSet;
        hashSet.add("messenger_orca_050_messaging");
        HashSet hashSet2 = new HashSet();
        A0F = hashSet2;
        hashSet2.add("messenger_orca_050_messaging");
        Set set = A0F;
        set.add("messenger_orca_100_mentioned");
        set.add("messenger_orca_400_stories");
        set.add("messenger_orca_700_other");
        HashMap hashMap = new HashMap();
        A0B = hashMap;
        hashMap.put("messenger_orca_050_messaging", "messenger_orca_10_group_notifications");
        Map map = A0B;
        map.put("messenger_orca_100_mentioned", "messenger_orca_10_group_notifications");
        map.put("messenger_orca_400_stories", "messenger_orca_10_group_notifications");
        map.put("messenger_orca_700_other", "messenger_orca_10_group_notifications");
        map.put("messenger_orca_710_silent_messaging", "messenger_orca_10_group_notifications");
        map.put("messenger_orca_749_voip_incoming", "messenger_orca_50_group_activity_indicators");
        map.put("messenger_orca_750_voip", "messenger_orca_50_group_activity_indicators");
        map.put("messenger_orca_800_live_location", "messenger_orca_50_group_activity_indicators");
        map.put("messenger_orca_900_chathead_active", "messenger_orca_50_group_activity_indicators");
        HashMap hashMap2 = new HashMap();
        A0C = hashMap2;
        hashMap2.put(10000, "messenger_orca_050_messaging");
        Map map2 = A0C;
        map2.put(10001, "messenger_orca_050_messaging");
        map2.put(10003, "messenger_orca_050_messaging");
        map2.put(10004, "messenger_orca_700_other");
        map2.put(10007, "messenger_orca_700_other");
        Integer valueOf = Integer.valueOf((int) AnonymousClass1Y3.BNE);
        map2.put(valueOf, "messenger_orca_050_messaging");
        map2.put(10011, "messenger_orca_700_other");
        map2.put(10014, "messenger_orca_700_other");
        map2.put(10015, "messenger_orca_050_messaging");
        Integer valueOf2 = Integer.valueOf((int) AnonymousClass1Y3.BNG);
        map2.put(valueOf2, "messenger_orca_700_other");
        Integer valueOf3 = Integer.valueOf((int) AnonymousClass1Y3.BNH);
        map2.put(valueOf3, "messenger_orca_700_other");
        Integer valueOf4 = Integer.valueOf((int) AnonymousClass1Y3.BNI);
        map2.put(valueOf4, "messenger_orca_700_other");
        Integer valueOf5 = Integer.valueOf((int) AnonymousClass1Y3.BNJ);
        map2.put(valueOf5, "messenger_orca_700_other");
        Integer valueOf6 = Integer.valueOf((int) AnonymousClass1Y3.BNO);
        map2.put(valueOf6, "messenger_orca_700_other");
        Integer valueOf7 = Integer.valueOf((int) AnonymousClass1Y3.BNQ);
        map2.put(valueOf7, "messenger_orca_700_other");
        map2.put(10029, "messenger_orca_700_other");
        Integer valueOf8 = Integer.valueOf((int) AnonymousClass1Y3.BNS);
        map2.put(valueOf8, "messenger_orca_700_other");
        map2.put(10032, "messenger_orca_050_messaging");
        map2.put(10034, "messenger_orca_050_messaging");
        map2.put(10035, "messenger_orca_050_messaging");
        Integer valueOf9 = Integer.valueOf((int) AnonymousClass1Y3.BNT);
        map2.put(valueOf9, "messenger_orca_100_mentioned");
        map2.put(10037, "messenger_orca_400_stories");
        Integer valueOf10 = Integer.valueOf((int) AnonymousClass1Y3.BNU);
        map2.put(valueOf10, "messenger_orca_400_stories");
        Integer valueOf11 = Integer.valueOf((int) AnonymousClass1Y3.BNV);
        map2.put(valueOf11, "messenger_orca_400_stories");
        Integer valueOf12 = Integer.valueOf((int) AnonymousClass1Y3.BNW);
        map2.put(valueOf12, "messenger_orca_400_stories");
        Integer valueOf13 = Integer.valueOf((int) AnonymousClass1Y3.BNX);
        map2.put(valueOf13, "messenger_orca_700_other");
        Integer valueOf14 = Integer.valueOf((int) AnonymousClass1Y3.BNY);
        map2.put(valueOf14, "messenger_orca_700_other");
        Integer valueOf15 = Integer.valueOf((int) AnonymousClass1Y3.BNZ);
        map2.put(valueOf15, "messenger_orca_050_messaging");
        map2.put(10044, "messenger_orca_050_messaging");
        Integer valueOf16 = Integer.valueOf((int) AnonymousClass1Y3.BNa);
        map2.put(valueOf16, "messenger_orca_700_other");
        map2.put(10047, "messenger_orca_050_messaging");
        Integer valueOf17 = Integer.valueOf((int) AnonymousClass1Y3.BNb);
        map2.put(valueOf17, "messenger_orca_700_other");
        Integer valueOf18 = Integer.valueOf((int) AnonymousClass1Y3.BNc);
        map2.put(valueOf18, "messenger_orca_400_stories");
        map2.put(10051, "messenger_orca_050_messaging");
        Integer valueOf19 = Integer.valueOf((int) AnonymousClass1Y3.BNf);
        map2.put(valueOf19, "messenger_orca_400_stories");
        map2.put(50001, "messenger_orca_700_other");
        Integer valueOf20 = Integer.valueOf((int) AnonymousClass1Y3.BNg);
        map2.put(valueOf20, "messenger_orca_750_voip");
        Integer valueOf21 = Integer.valueOf((int) AnonymousClass1Y3.BNh);
        map2.put(valueOf21, "messenger_orca_700_other");
        map2.put(10056, "messenger_orca_700_other");
        map2.put(10059, "messenger_orca_400_stories");
        map2.put(20001, "messenger_orca_900_chathead_active");
        map2.put(20023, "messenger_orca_749_voip_incoming");
        map2.put(20002, "messenger_orca_750_voip");
        map2.put(10064, "messenger_orca_050_messaging");
        map2.put(20009, "messenger_orca_800_live_location");
        HashMap hashMap3 = new HashMap();
        A0D = hashMap3;
        hashMap3.put("messenger_orca_050_messaging", "messenger_orca_10_group_notifications");
        Map map3 = A0D;
        map3.put("messenger_orca_300_people", "messenger_orca_10_group_notifications");
        map3.put("messenger_orca_400_stories", "messenger_orca_10_group_notifications");
        map3.put("messenger_orca_700_other", "messenger_orca_10_group_notifications");
        map3.put("messenger_orca_749_voip_incoming", "messenger_orca_50_group_activity_indicators");
        map3.put("messenger_orca_750_voip", "messenger_orca_50_group_activity_indicators");
        map3.put("messenger_orca_800_live_location", "messenger_orca_50_group_activity_indicators");
        map3.put("messenger_orca_900_chathead_active", "messenger_orca_50_group_activity_indicators");
        HashMap hashMap4 = new HashMap();
        A0E = hashMap4;
        hashMap4.put(10000, "messenger_orca_050_messaging");
        Map map4 = A0E;
        map4.put(10001, "messenger_orca_050_messaging");
        map4.put(10003, "messenger_orca_300_people");
        map4.put(10004, "messenger_orca_700_other");
        map4.put(10007, "messenger_orca_700_other");
        map4.put(valueOf, "messenger_orca_050_messaging");
        map4.put(10011, "messenger_orca_700_other");
        map4.put(10014, "messenger_orca_700_other");
        map4.put(10015, "messenger_orca_300_people");
        map4.put(valueOf2, "messenger_orca_700_other");
        map4.put(valueOf3, "messenger_orca_700_other");
        map4.put(valueOf4, "messenger_orca_700_other");
        map4.put(valueOf5, "messenger_orca_700_other");
        map4.put(valueOf6, "messenger_orca_700_other");
        map4.put(valueOf7, "messenger_orca_700_other");
        map4.put(10029, "messenger_orca_700_other");
        map4.put(valueOf8, "messenger_orca_700_other");
        map4.put(10032, "messenger_orca_050_messaging");
        map4.put(10034, "messenger_orca_050_messaging");
        map4.put(10035, "messenger_orca_050_messaging");
        map4.put(valueOf9, "messenger_orca_050_messaging");
        map4.put(10037, "messenger_orca_400_stories");
        map4.put(valueOf10, "messenger_orca_400_stories");
        map4.put(valueOf11, "messenger_orca_400_stories");
        map4.put(valueOf12, "messenger_orca_400_stories");
        map4.put(valueOf13, "messenger_orca_700_other");
        map4.put(valueOf14, "messenger_orca_700_other");
        map4.put(valueOf15, "messenger_orca_050_messaging");
        map4.put(10044, "messenger_orca_050_messaging");
        map4.put(valueOf16, "messenger_orca_700_other");
        map4.put(10047, "messenger_orca_050_messaging");
        map4.put(valueOf17, "messenger_orca_700_other");
        map4.put(valueOf18, "messenger_orca_400_stories");
        map4.put(10051, "messenger_orca_050_messaging");
        map4.put(valueOf19, "messenger_orca_400_stories");
        map4.put(50001, "messenger_orca_700_other");
        map4.put(valueOf20, "messenger_orca_750_voip");
        map4.put(valueOf21, "messenger_orca_700_other");
        map4.put(10056, "messenger_orca_700_other");
        map4.put(20001, "messenger_orca_900_chathead_active");
        map4.put(20023, "messenger_orca_749_voip_incoming");
        map4.put(20002, "messenger_orca_750_voip");
        map4.put(10064, "messenger_orca_050_messaging");
        map4.put(20009, "messenger_orca_800_live_location");
    }

    private C32431lk(AnonymousClass1XY r23) {
        AnonymousClass16F r13;
        AnonymousClass1XY r3 = r23;
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A04 = AnonymousClass1YA.A02(r3);
        this.A05 = AnonymousClass32J.A00(r3);
        if (Build.VERSION.SDK_INT >= 26) {
            HashMap hashMap = new HashMap();
            hashMap.put("messenger_orca_10_group_notifications", new C15110uk("messenger_orca_10_group_notifications", this.A04.getString(2131828580)));
            hashMap.put("messenger_orca_50_group_activity_indicators", new C15110uk("messenger_orca_50_group_activity_indicators", this.A04.getString(2131828579)));
            HashMap hashMap2 = new HashMap();
            String string = this.A04.getString(2131828583);
            C15120ul r11 = C15120ul.DEFAULT_LIGHT;
            if (((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aem(282789236901749L)) {
                r13 = AnonymousClass16F.NOTIFY_VIBRATE_IMESSAGE;
            } else if (((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aem(282789236836212L)) {
                r13 = AnonymousClass16F.NOTIFY_VIBRATE_LONG;
            } else {
                r13 = AnonymousClass16F.NOTIFY_VIBRATE_SHORT;
            }
            hashMap2.put("messenger_orca_050_messaging", new C14100sc("messenger_orca_050_messaging", string, 4, r11, true, r13, this.A05.A04(), (String) A0B.get("messenger_orca_050_messaging")));
            String string2 = this.A04.getString(2131828582);
            C15120ul r17 = C15120ul.DEFAULT_LIGHT;
            AnonymousClass16F r19 = AnonymousClass16F.NOTIFY_VIBRATE_SHORT;
            hashMap2.put("messenger_orca_100_mentioned", new C14100sc("messenger_orca_100_mentioned", string2, 4, r17, true, r19, this.A05.A03(), (String) A0B.get("messenger_orca_100_mentioned")));
            if (!((Boolean) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BAo, this.A00)).booleanValue()) {
                hashMap2.put("messenger_orca_400_stories", new C14100sc("messenger_orca_400_stories", this.A04.getString(2131828588), 4, r11, true, AnonymousClass16F.NOTIFY_VIBRATE_SHORT, this.A05.A04(), (String) A0B.get("messenger_orca_400_stories")));
            }
            hashMap2.put("messenger_orca_700_other", new C14100sc("messenger_orca_700_other", this.A04.getString(2131828584), 3, r17, true, r19, this.A05.A04(), (String) A0B.get("messenger_orca_700_other")));
            if (A02()) {
                hashMap2.put("messenger_orca_749_voip_incoming", new C14100sc("messenger_orca_749_voip_incoming", this.A04.getString(2131828590), 4, C15120ul.INVALID_LIGHT, false, null, null, (String) A0B.get("messenger_orca_749_voip_incoming")));
            }
            hashMap2.put("messenger_orca_750_voip", new C14100sc("messenger_orca_750_voip", this.A04.getString(2131828589), 2, C15120ul.INVALID_LIGHT, false, null, null, (String) A0B.get("messenger_orca_750_voip")));
            hashMap2.put("messenger_orca_800_live_location", new C14100sc("messenger_orca_800_live_location", this.A04.getString(2131828581), 2, C15120ul.INVALID_LIGHT, false, null, null, (String) A0B.get("messenger_orca_800_live_location")));
            C14100sc r6 = new C14100sc("messenger_orca_900_chathead_active", this.A04.getString(2131828578), 1, C15120ul.INVALID_LIGHT, false, null, null, (String) A0B.get("messenger_orca_900_chathead_active"));
            r6.mShowBadge = false;
            hashMap2.put("messenger_orca_900_chathead_active", r6);
            this.A01 = new C32441ll(hashMap, hashMap2, new HashMap(A0B), new HashMap(A0C), 1);
            HashMap hashMap3 = new HashMap();
            hashMap3.put("messenger_orca_10_group_notifications", new C15110uk("messenger_orca_10_group_notifications", this.A04.getString(2131828580)));
            hashMap3.put("messenger_orca_50_group_activity_indicators", new C15110uk("messenger_orca_50_group_activity_indicators", this.A04.getString(2131828579)));
            HashMap hashMap4 = new HashMap();
            String string3 = this.A04.getString(2131828583);
            C15120ul r9 = C15120ul.DEFAULT_LIGHT;
            AnonymousClass16F r112 = AnonymousClass16F.NOTIFY_VIBRATE_SHORT;
            hashMap4.put("messenger_orca_050_messaging", new C14100sc("messenger_orca_050_messaging", string3, 4, r9, true, r112, this.A05.A04(), (String) A0D.get("messenger_orca_050_messaging")));
            String string4 = this.A04.getString(2131828586);
            C15120ul r16 = C15120ul.DEFAULT_LIGHT;
            AnonymousClass16F r18 = AnonymousClass16F.NOTIFY_VIBRATE_SHORT;
            hashMap4.put("messenger_orca_300_people", new C14100sc("messenger_orca_300_people", string4, 4, r16, true, r18, this.A05.A04(), (String) A0D.get("messenger_orca_300_people")));
            if (!((Boolean) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BAo, this.A00)).booleanValue()) {
                hashMap4.put("messenger_orca_400_stories", new C14100sc("messenger_orca_400_stories", this.A04.getString(2131828588), 4, r9, true, r112, this.A05.A04(), (String) A0D.get("messenger_orca_400_stories")));
            }
            hashMap4.put("messenger_orca_700_other", new C14100sc("messenger_orca_700_other", this.A04.getString(2131828584), 3, r16, true, r18, this.A05.A04(), (String) A0D.get("messenger_orca_700_other")));
            if (A02()) {
                hashMap4.put("messenger_orca_749_voip_incoming", new C14100sc("messenger_orca_749_voip_incoming", this.A04.getString(2131828590), 4, C15120ul.INVALID_LIGHT, false, null, null, (String) A0D.get("messenger_orca_749_voip_incoming")));
            }
            hashMap4.put("messenger_orca_750_voip", new C14100sc("messenger_orca_750_voip", this.A04.getString(2131828589), 2, C15120ul.INVALID_LIGHT, false, null, null, (String) A0D.get("messenger_orca_750_voip")));
            hashMap4.put("messenger_orca_800_live_location", new C14100sc("messenger_orca_800_live_location", this.A04.getString(2131828581), 2, C15120ul.INVALID_LIGHT, false, null, null, (String) A0D.get("messenger_orca_800_live_location")));
            C14100sc r62 = new C14100sc("messenger_orca_900_chathead_active", this.A04.getString(2131828578), 1, C15120ul.INVALID_LIGHT, false, null, null, (String) A0D.get("messenger_orca_900_chathead_active"));
            r62.mShowBadge = false;
            hashMap4.put("messenger_orca_900_chathead_active", r62);
            this.A02 = new C32441ll(hashMap3, hashMap4, new HashMap(A0D), new HashMap(A0E), 1);
            HashMap hashMap5 = new HashMap();
            hashMap5.put("messenger_orca_10_group_notifications", new C15110uk("messenger_orca_10_group_notifications", this.A04.getString(2131828580)));
            hashMap5.put("messenger_orca_50_group_activity_indicators", new C15110uk("messenger_orca_50_group_activity_indicators", this.A04.getString(2131828579)));
            HashMap hashMap6 = new HashMap();
            String string5 = this.A04.getString(2131828583);
            C15120ul r7 = C15120ul.DEFAULT_LIGHT;
            AnonymousClass16F r92 = AnonymousClass16F.NOTIFY_VIBRATE_SHORT;
            hashMap6.put("messenger_orca_050_messaging", new C14100sc("messenger_orca_050_messaging", string5, 4, r7, true, r92, this.A05.A04(), (String) A0B.get("messenger_orca_050_messaging")));
            hashMap6.put("messenger_orca_100_mentioned", new C14100sc("messenger_orca_100_mentioned", this.A04.getString(2131828582), 4, r7, true, r92, this.A05.A03(), (String) A0B.get("messenger_orca_100_mentioned")));
            hashMap6.put("messenger_orca_700_other", new C14100sc("messenger_orca_700_other", this.A04.getString(2131828584), 3, r7, true, r92, this.A05.A04(), (String) A0B.get("messenger_orca_700_other")));
            if (A02()) {
                hashMap6.put("messenger_orca_749_voip_incoming", new C14100sc("messenger_orca_749_voip_incoming", this.A04.getString(2131828590), 4, C15120ul.INVALID_LIGHT, false, null, null, (String) A0B.get("messenger_orca_749_voip_incoming")));
            }
            String string6 = this.A04.getString(2131828589);
            C15120ul r72 = C15120ul.INVALID_LIGHT;
            Map map = A0B;
            hashMap6.put("messenger_orca_750_voip", new C14100sc("messenger_orca_750_voip", string6, 2, r72, false, null, null, (String) map.get("messenger_orca_750_voip")));
            this.A03 = new C32441ll(hashMap5, hashMap6, new HashMap(map), new HashMap(A0C), 1);
            A05();
        }
    }

    public static NotificationChannel A00(NotificationChannel notificationChannel) {
        if (notificationChannel == null) {
            return null;
        }
        String id = notificationChannel.getId();
        CharSequence name = notificationChannel.getName();
        int importance = notificationChannel.getImportance();
        boolean shouldShowLights = notificationChannel.shouldShowLights();
        Integer valueOf = Integer.valueOf(notificationChannel.getLightColor());
        boolean shouldVibrate = notificationChannel.shouldVibrate();
        long[] vibrationPattern = notificationChannel.getVibrationPattern();
        Uri sound = notificationChannel.getSound();
        String group = notificationChannel.getGroup();
        boolean canShowBadge = notificationChannel.canShowBadge();
        NotificationChannel notificationChannel2 = new NotificationChannel(id, name, importance);
        if (!C06850cB.A0B(group)) {
            notificationChannel2.setGroup(group);
        }
        if (shouldShowLights) {
            notificationChannel2.enableLights(true);
            notificationChannel2.setLightColor(valueOf.intValue());
        } else {
            notificationChannel2.enableLights(false);
        }
        if (shouldVibrate) {
            notificationChannel2.setVibrationPattern(vibrationPattern);
            notificationChannel2.enableVibration(true);
        } else {
            notificationChannel2.enableVibration(false);
        }
        notificationChannel2.setSound(sound, Notification.AUDIO_ATTRIBUTES_DEFAULT);
        notificationChannel2.setShowBadge(canShowBadge);
        return notificationChannel2;
    }

    public static final C32431lk A01(AnonymousClass1XY r4) {
        if (A0H == null) {
            synchronized (C32431lk.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0H, r4);
                if (A002 != null) {
                    try {
                        A0H = new C32431lk(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0H;
    }

    public NotificationChannel A03(String str) {
        return A00((NotificationChannel) this.A08.get(str));
    }

    public String A04(int i) {
        if (this.A09.keySet().isEmpty() && ((C15090ui) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCf, this.A00)).A02()) {
            C010708t.A0K("MessagesNotificationChannelModels", "Active Notification Id-To-Channel Map is empty");
            A05();
        }
        Map map = this.A09;
        Integer valueOf = Integer.valueOf(i);
        if (map.containsKey(valueOf)) {
            return (String) this.A09.get(valueOf);
        }
        C010708t.A0O("MessagesNotificationChannelModels", "Notification id: %s has not been associated with a notification channel", valueOf);
        if (!this.A09.keySet().isEmpty() || Build.VERSION.SDK_INT >= 27) {
            return "messenger_orca_700_other";
        }
        return "miscellaneous";
    }

    public void A05() {
        String B4C;
        C32441ll r0;
        String str;
        C15090ui r1 = (C15090ui) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCf, this.A00);
        if (!r1.A03()) {
            B4C = null;
        } else {
            B4C = r1.A01.B4C(845580276400285L);
        }
        Integer num = null;
        if (B4C != null) {
            String[] split = B4C.split("&");
            if (split.length >= 1) {
                String[] split2 = split[0].split("=");
                if (split2.length >= 2 && (str = split2[1]) != null) {
                    if (str.equals("baseline")) {
                        num = AnonymousClass07B.A00;
                    } else if (str.equals("fewer_channel")) {
                        num = AnonymousClass07B.A01;
                    } else if (str.equals("separate_growth")) {
                        num = AnonymousClass07B.A0C;
                    } else if (str.equals("protect_message")) {
                        num = AnonymousClass07B.A0N;
                    }
                }
            }
        }
        if (((C15090ui) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCf, this.A00)).A04()) {
            r0 = this.A03;
        } else if (num == null) {
            r0 = this.A01;
        } else if (2 - num.intValue() != 0) {
            A06(this.A01);
            return;
        } else {
            A06(this.A02);
            return;
        }
        A06(r0);
    }

    public void A06(C32441ll r15) {
        Uri parse;
        this.A09.clear();
        this.A09.putAll(r15.mNotifIdToChannelMap);
        this.A07.clear();
        for (C15110uk r0 : r15.mChannelGroupSetting.values()) {
            Map map = this.A07;
            String str = r0.mId;
            map.put(str, new NotificationChannelGroup(str, r0.mName));
        }
        this.A08.clear();
        for (C14100sc r3 : r15.mChannelSettings.values()) {
            Map map2 = this.A08;
            String str2 = r3.mChannelId;
            C15120ul r02 = r3.mLight;
            Integer num = null;
            if (r02 != null && r02.ordinal() == 0) {
                num = -16711936;
            }
            long[] A022 = C15010uZ.A02(r3.mNotifyVibrate);
            String str3 = r3.mName;
            int i = r3.mImportance;
            boolean z = false;
            if (num != null) {
                z = true;
            }
            boolean z2 = r3.mShouldVibrate;
            String str4 = r3.mSoundUri;
            if (str4 == null) {
                parse = null;
            } else {
                parse = Uri.parse(str4);
            }
            String str5 = r3.mGroupId;
            boolean z3 = r3.mShowBadge;
            NotificationChannel notificationChannel = new NotificationChannel(str2, str3, i);
            if (!C06850cB.A0B(str5)) {
                notificationChannel.setGroup(str5);
            }
            if (z) {
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(num.intValue());
            } else {
                notificationChannel.enableLights(false);
            }
            if (z2) {
                notificationChannel.setVibrationPattern(A022);
                notificationChannel.enableVibration(true);
            } else {
                notificationChannel.enableVibration(false);
            }
            notificationChannel.setSound(parse, Notification.AUDIO_ATTRIBUTES_DEFAULT);
            notificationChannel.setShowBadge(z3);
            map2.put(str2, notificationChannel);
        }
        this.A06.clear();
        this.A06.putAll(r15.mChannelToGroupMap);
    }

    private boolean A02() {
        if (C188988qA.A00() || ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aer(283081294481582L, AnonymousClass0XE.A07) || ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aer(283167193827536L, AnonymousClass0XE.A07)) {
            return true;
        }
        return false;
    }
}
