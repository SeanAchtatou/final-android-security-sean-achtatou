package X;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.WeakHashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0cE  reason: invalid class name and case insensitive filesystem */
public final class C06880cE extends AnonymousClass0Wl implements AnonymousClass1YQ, C05460Za {
    private static volatile C06880cE A09;
    public AnonymousClass0UN A00;
    public final C05950ab A01;
    public final C07930eP A02;
    public final ImmutableSet A03;
    public final ClassLoader A04 = C07400dP.class.getClassLoader();
    public final Map A05;
    public final Map A06;
    private final Map A07;
    public final Class A08 = getClass();

    public static synchronized void A02(C06880cE r5, C06130au r6, Set set) {
        synchronized (r5) {
            r5.A06.put(r6, set);
            Iterator it = set.iterator();
            while (it.hasNext()) {
                C06750c1 r3 = (C06750c1) it.next();
                Object obj = (SortedSet) r5.A05.get(r3.A01);
                if (obj == null) {
                    Comparator comparator = C06750c1.A02;
                    Preconditions.checkNotNull(comparator);
                    obj = new TreeSet(comparator);
                }
                obj.add(r3);
                r5.A05.put(r3.A01, obj);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004b, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Object A04(android.net.Uri r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.Map r0 = r3.A05     // Catch:{ all -> 0x004f }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x004f }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x004f }
        L_0x000b:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x004c
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x004f }
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1     // Catch:{ all -> 0x004f }
            java.lang.Object r0 = r1.getKey()     // Catch:{ all -> 0x004f }
            android.net.Uri r0 = (android.net.Uri) r0     // Catch:{ all -> 0x004f }
            boolean r0 = A03(r4, r0)     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x000b
            X.2oE r2 = new X.2oE     // Catch:{ all -> 0x004f }
            r2.<init>()     // Catch:{ all -> 0x004f }
            java.lang.Object r0 = r1.getValue()     // Catch:{ all -> 0x004f }
            java.util.SortedSet r0 = (java.util.SortedSet) r0     // Catch:{ all -> 0x004f }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x004f }
        L_0x0032:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x0048
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x004f }
            X.0c1 r0 = (X.C06750c1) r0     // Catch:{ all -> 0x004f }
            r0.A01(r4, r2)     // Catch:{ all -> 0x004f }
            boolean r0 = r2.A01     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x0032
            java.lang.Object r0 = r2.A00     // Catch:{ all -> 0x004f }
            goto L_0x004a
        L_0x0048:
            java.lang.Object r0 = r2.A00     // Catch:{ all -> 0x004f }
        L_0x004a:
            monitor-exit(r3)
            return r0
        L_0x004c:
            r0 = 0
            monitor-exit(r3)
            return r0
        L_0x004f:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06880cE.A04(android.net.Uri):java.lang.Object");
    }

    public synchronized void A05(Uri uri, AnonymousClass2NX r4) {
        WeakHashMap weakHashMap = (WeakHashMap) this.A07.get(uri);
        if (weakHashMap == null) {
            weakHashMap = new WeakHashMap();
        }
        weakHashMap.put(r4, true);
        this.A07.put(uri, weakHashMap);
    }

    public void clearUserData() {
        synchronized (this) {
            this.A06.clear();
            this.A05.clear();
            C24971Xv it = this.A03.iterator();
            while (it.hasNext()) {
                ((C06750c1) it.next()).A00();
            }
            A02(this, this.A02.A07, this.A03);
        }
        this.A02.clearUserData();
    }

    public String getSimpleName() {
        return "StatefulPeerManagerImpl";
    }

    public static final C06880cE A00(AnonymousClass1XY r9) {
        if (A09 == null) {
            synchronized (C06880cE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r9);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r9.getApplicationInjector();
                        A09 = new C06880cE(applicationInjector, C05950ab.A00(applicationInjector), 1, C06030aj.A00(applicationInjector), AnonymousClass0UU.A02(applicationInjector), C05880aU.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public void A06(Uri uri, Object obj) {
        C06750c1 r3;
        boolean A042;
        if (!this.A03.isEmpty()) {
            C24971Xv it = this.A03.iterator();
            while (true) {
                if (!it.hasNext()) {
                    r3 = null;
                    break;
                }
                r3 = (C06750c1) it.next();
                if (A03(uri, r3.A01)) {
                    break;
                }
            }
            if (r3 != null) {
                Message obtain = Message.obtain((Handler) null, 1000000001);
                Bundle data = obtain.getData();
                synchronized (this) {
                    A042 = r3.A04(uri, obj);
                    if (A042) {
                        r3.A03(data);
                    }
                }
                if (A042) {
                    data.putParcelable("__STATE_URI__", uri);
                    this.A02.A06(obtain);
                    A01(this, uri, true);
                    return;
                }
                return;
            }
            return;
        }
        throw new IllegalStateException("Current process " + this.A02.A07.A02 + " is not a stateful peer.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00ce, code lost:
        if (r5 == 0) goto L_0x00d0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C06880cE(X.AnonymousClass1XY r8, X.C05950ab r9, java.lang.Integer r10, X.C06030aj r11, X.C001300x r12, X.C04460Ut r13) {
        /*
            r7 = this;
            r7.<init>()
            java.lang.Class r0 = r7.getClass()
            r7.A08 = r0
            java.lang.Class<X.0dP> r0 = X.C07400dP.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            r7.A04 = r0
            X.0UN r0 = new X.0UN
            r6 = 2
            r0.<init>(r6, r8)
            r7.A00 = r0
            r7.A01 = r9
            X.0dQ r2 = com.google.common.collect.ImmutableSet.A01()
            int r5 = r10.intValue()
            r3 = 4
            r4 = 0
            r1 = 1
            if (r5 == r3) goto L_0x00db
            if (r5 != r1) goto L_0x00a6
            android.net.Uri r3 = X.C06980cP.A03
            r0 = 499(0x1f3, float:6.99E-43)
            X.0c1 r0 = r9.A01(r3, r0)
            r2.A01(r0)
            android.net.Uri r0 = X.C06980cP.A09
            X.0c1 r0 = r9.A01(r0, r4)
            r2.A01(r0)
            android.net.Uri r0 = X.C06980cP.A07
            X.0c1 r0 = r9.A01(r0, r4)
            r2.A01(r0)
            android.net.Uri r0 = X.C06980cP.A04
        L_0x0049:
            X.0c1 r0 = r9.A01(r0, r4)
            r2.A01(r0)
        L_0x0050:
            com.google.common.collect.ImmutableSet r0 = r2.build()
            r7.A03 = r0
            java.util.HashMap r0 = X.AnonymousClass0TG.A04()
            r7.A06 = r0
            java.util.HashMap r0 = X.AnonymousClass0TG.A04()
            r7.A05 = r0
            java.util.HashMap r0 = X.AnonymousClass0TG.A04()
            r7.A07 = r0
            java.lang.String r2 = "com.facebook.messaging.ipc.peers"
            X.010 r0 = r12.A00
            java.lang.String r0 = r0.name()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r2, r0)
            X.0eP r0 = r11.A01(r0, r13, r1)
            r7.A02 = r0
            X.0cQ r2 = new X.0cQ
            r2.<init>(r7)
            com.google.common.base.Preconditions.checkNotNull(r2)
            java.util.concurrent.ConcurrentMap r1 = r0.A0C
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r1.put(r2, r0)
            X.0eP r1 = r7.A02
            r0 = 1000000000(0x3b9aca00, float:0.0047237873)
            r1.A05(r0, r2)
            X.0eP r1 = r7.A02
            r0 = 1000000001(0x3b9aca01, float:0.004723788)
            r1.A05(r0, r2)
            X.0eP r0 = r7.A02
            X.0au r1 = r0.A07
            com.google.common.collect.ImmutableSet r0 = r7.A03
            A02(r7, r1, r0)
            return
        L_0x00a6:
            if (r5 != r6) goto L_0x00bb
            android.net.Uri r0 = X.C06980cP.A09
            X.0c1 r0 = r9.A01(r0, r1)
            r2.A01(r0)
            android.net.Uri r0 = X.C06980cP.A07
            X.0c1 r0 = r9.A01(r0, r1)
            r2.A01(r0)
            goto L_0x0050
        L_0x00bb:
            r4 = 3
            if (r5 == r4) goto L_0x00d0
            r6 = 5
            if (r5 == r6) goto L_0x00db
            r0 = 6
            if (r5 != r0) goto L_0x00ce
            android.net.Uri r0 = X.C06980cP.A09
            X.0c1 r0 = r9.A01(r0, r3)
            r2.A01(r0)
            goto L_0x0050
        L_0x00ce:
            if (r5 != 0) goto L_0x0050
        L_0x00d0:
            android.net.Uri r0 = X.C06980cP.A09
            X.0c1 r0 = r9.A01(r0, r6)
            r2.A01(r0)
            goto L_0x0050
        L_0x00db:
            android.net.Uri r0 = X.C06980cP.A09
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06880cE.<init>(X.1XY, X.0ab, java.lang.Integer, X.0aj, X.00x, X.0Ut):void");
    }

    public static void A01(C06880cE r4, Uri uri, boolean z) {
        ArrayList<AnonymousClass2NX> A002 = C04300To.A00();
        synchronized (r4) {
            for (Map.Entry entry : r4.A07.entrySet()) {
                if (A03(uri, (Uri) entry.getKey())) {
                    A002.addAll(((WeakHashMap) entry.getValue()).keySet());
                }
            }
        }
        for (AnonymousClass2NX BRd : A002) {
            BRd.BRd(uri, z, r4);
        }
    }

    public static boolean A03(Uri uri, Uri uri2) {
        if (uri.getAuthority().equals(uri2.getAuthority())) {
            List<String> pathSegments = uri.getPathSegments();
            List<String> pathSegments2 = uri2.getPathSegments();
            if (pathSegments.size() >= pathSegments2.size()) {
                int i = 0;
                while (i < pathSegments2.size()) {
                    if (pathSegments.get(i).equals(pathSegments2.get(i))) {
                        i++;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public void init() {
        int A032 = C000700l.A03(496201854);
        this.A02.init();
        C000700l.A09(-1302262126, A032);
    }
}
