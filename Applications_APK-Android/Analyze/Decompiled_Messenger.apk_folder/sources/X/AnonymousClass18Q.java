package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.18Q  reason: invalid class name */
public final class AnonymousClass18Q implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass18Q(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        C193617v r1;
        int A002 = AnonymousClass09Y.A00(338931539);
        if (C10950l8.A0A.toString().equals(intent.getStringExtra("folder_name")) && (r1 = this.A00.A03) != null) {
            r1.BtU("folder count changed");
        }
        AnonymousClass09Y.A01(1194860680, A002);
    }
}
