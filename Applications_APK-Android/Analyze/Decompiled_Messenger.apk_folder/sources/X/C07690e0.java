package X;

import com.google.common.base.Preconditions;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0e0  reason: invalid class name and case insensitive filesystem */
public final class C07690e0 extends C05090Xp implements C07820eD, AnonymousClass0Y0 {
    public static final String __redex_internal_original_name = "com.facebook.common.combinedthreadpool.queue.CombinedTimedTask";
    public long A00;
    public C52952jx A01;
    public boolean A02;
    private String A03;
    public final long A04;
    public final C04600Vj A05;
    public final Integer A06;
    private final AnonymousClass0VS A07;
    private final C04610Vk A08;
    private final C05190Xz A09;
    private final Integer A0A;
    private final Object A0B;
    private final boolean A0C;
    public volatile long A0D;

    public synchronized long A00() {
        return this.A00;
    }

    public void A01() {
        super.cancel(false);
    }

    public synchronized C52952jx ATQ() {
        return this.A01;
    }

    public synchronized void C6z(C52952jx r2) {
        Preconditions.checkNotNull(r2);
        this.A01 = r2;
    }

    public void run() {
        boolean z;
        String str;
        synchronized (this) {
            try {
                z = false;
                boolean z2 = false;
                if (!this.A02) {
                    z2 = true;
                }
                Preconditions.checkState(z2);
            } catch (Throwable th) {
                while (true) {
                    e = th;
                    break;
                }
            }
        }
        int i = C14640tj.A00[this.A06.intValue()];
        if (i == 1) {
            try {
                super.run();
            } catch (Exception e) {
                e = e;
                str = "Crash task %s";
                if (C010708t.A0X(6)) {
                    C010708t.A0U("ComTP", e, str, this);
                }
                throw e;
            }
        } else if (i == 2 || i == 3) {
            try {
                boolean runAndReset = runAndReset();
                synchronized (this) {
                    if (!this.A02) {
                        z = true;
                    }
                    Preconditions.checkState(z);
                    this.A02 = runAndReset;
                }
            } catch (Exception e2) {
                e = e2;
                str = "Crash repeating task %s";
            }
        }
    }

    public C04610Vk AZE() {
        return this.A08;
    }

    public AnonymousClass0VS By8() {
        return this.A07;
    }

    public Object C4G() {
        return this.A0B;
    }

    public String C4H() {
        String str;
        if (!this.A0C) {
            return AnonymousClass0FD.A01(this.A0B);
        }
        synchronized (this) {
            if (this.A03 == null) {
                this.A03 = AnonymousClass0FD.A01(this.A0B);
            }
            str = this.A03;
        }
        return str;
    }

    public Integer C4I() {
        return this.A0A;
    }

    public long C5W() {
        return this.A0D;
    }

    public int compareTo(Object obj) {
        Delayed delayed = (Delayed) obj;
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        long delay = getDelay(timeUnit) - delayed.getDelay(timeUnit);
        if (delay != 0) {
            if (delay < 0) {
                return -1;
            }
            return 1;
        } else if (!(delayed instanceof C07690e0)) {
            return 0;
        } else {
            return C04530Vb.A00(this, (C07690e0) delayed);
        }
    }

    public boolean cancel(boolean z) {
        boolean cancel = super.cancel(z);
        if (cancel) {
            C179878Tk A002 = C25201Ys.A00(this.A05.ATR(), this);
            if (A002 != null) {
                A002.onTaskCanceled();
            }
            if (getDelay(TimeUnit.NANOSECONDS) > 0) {
                this.A05.AP4(this);
            }
        }
        return cancel;
    }

    public void done() {
        super.done();
        this.A09.A01(this);
    }

    public long getDelay(TimeUnit timeUnit) {
        return timeUnit.convert(A00() - AnonymousClass0Y1.A00(), TimeUnit.NANOSECONDS);
    }

    public String toString() {
        return AnonymousClass24S.A00(this);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0004, code lost:
        if (r8 == 0) goto L_0x0006;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000d, code lost:
        if (r8 != 0) goto L_0x000f;
     */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C07690e0(java.lang.Runnable r6, java.lang.Object r7, java.util.concurrent.Callable r8, X.AnonymousClass0VS r9, long r10, long r12, java.lang.Integer r14, X.C04610Vk r15, X.C04600Vj r16, long r17, boolean r19) {
        /*
            r5 = this;
            r1 = 0
            if (r6 == 0) goto L_0x0006
            r0 = 0
            if (r8 != 0) goto L_0x0007
        L_0x0006:
            r0 = 1
        L_0x0007:
            com.google.common.base.Preconditions.checkArgument(r0)
            if (r6 != 0) goto L_0x000f
            r0 = 0
            if (r8 == 0) goto L_0x0010
        L_0x000f:
            r0 = 1
        L_0x0010:
            com.google.common.base.Preconditions.checkArgument(r0)
            if (r8 == 0) goto L_0x00ea
            if (r7 != 0) goto L_0x0018
            r1 = 1
        L_0x0018:
            com.google.common.base.Preconditions.checkArgument(r1)
            r0 = r8
        L_0x001c:
            r5.<init>(r0)
            r0 = 0
            r5.A03 = r0
            r4 = 1
            r2 = 0
            int r1 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            r0 = 0
            if (r1 < 0) goto L_0x002b
            r0 = 1
        L_0x002b:
            com.google.common.base.Preconditions.checkArgument(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r14 != r0) goto L_0x003a
            int r0 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0037
            r4 = 0
        L_0x0037:
            com.google.common.base.Preconditions.checkArgument(r4)
        L_0x003a:
            int r0 = r14.intValue()
            r3 = 0
            switch(r0) {
                case 0: goto L_0x0068;
                case 1: goto L_0x0058;
                case 2: goto L_0x0048;
                default: goto L_0x0042;
            }
        L_0x0042:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x0048:
            r0 = 0
            if (r8 != 0) goto L_0x004c
            r0 = 1
        L_0x004c:
            com.google.common.base.Preconditions.checkArgument(r0)
            if (r7 != 0) goto L_0x0052
            r3 = 1
        L_0x0052:
            com.google.common.base.Preconditions.checkArgument(r3)
            java.lang.Integer r0 = X.AnonymousClass07B.A0o
            goto L_0x007a
        L_0x0058:
            r0 = 0
            if (r8 != 0) goto L_0x005c
            r0 = 1
        L_0x005c:
            com.google.common.base.Preconditions.checkArgument(r0)
            if (r7 != 0) goto L_0x0062
            r3 = 1
        L_0x0062:
            com.google.common.base.Preconditions.checkArgument(r3)
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            goto L_0x007a
        L_0x0068:
            r1 = 0
            int r0 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x00c9
            if (r6 == 0) goto L_0x00aa
            if (r7 != 0) goto L_0x00aa
            if (r8 != 0) goto L_0x0075
            r3 = 1
        L_0x0075:
            com.google.common.base.Preconditions.checkArgument(r3)
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
        L_0x007a:
            r5.A0A = r0
            r5.A07 = r9
            com.google.common.base.Preconditions.checkNotNull(r15)
            r5.A08 = r15
            r0 = r17
            r5.A0D = r0
            if (r8 == 0) goto L_0x008a
            r6 = r8
        L_0x008a:
            r5.A0B = r6
            long r0 = X.AnonymousClass0Y1.A00()
            long r0 = r0 + r10
            r5.A00 = r0
            r5.A04 = r12
            r5.A06 = r14
            r0 = r16
            com.google.common.base.Preconditions.checkNotNull(r0)
            r5.A05 = r0
            X.0Xz r0 = new X.0Xz
            r0.<init>(r5)
            r5.A09 = r0
            r0 = r19
            r5.A0C = r0
            return
        L_0x00aa:
            if (r6 == 0) goto L_0x00b7
            if (r7 == 0) goto L_0x00b7
            if (r8 != 0) goto L_0x00b1
            r3 = 1
        L_0x00b1:
            com.google.common.base.Preconditions.checkArgument(r3)
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            goto L_0x007a
        L_0x00b7:
            if (r8 == 0) goto L_0x0042
            r0 = 0
            if (r6 != 0) goto L_0x00bd
            r0 = 1
        L_0x00bd:
            com.google.common.base.Preconditions.checkArgument(r0)
            if (r7 != 0) goto L_0x00c3
            r3 = 1
        L_0x00c3:
            com.google.common.base.Preconditions.checkArgument(r3)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            goto L_0x007a
        L_0x00c9:
            if (r6 == 0) goto L_0x00db
            r0 = 0
            if (r8 != 0) goto L_0x00cf
            r0 = 1
        L_0x00cf:
            com.google.common.base.Preconditions.checkArgument(r0)
            if (r7 != 0) goto L_0x00d5
            r3 = 1
        L_0x00d5:
            com.google.common.base.Preconditions.checkArgument(r3)
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            goto L_0x007a
        L_0x00db:
            if (r8 == 0) goto L_0x0042
            r0 = 1
            com.google.common.base.Preconditions.checkArgument(r0)
            if (r7 != 0) goto L_0x00e4
            r3 = 1
        L_0x00e4:
            com.google.common.base.Preconditions.checkArgument(r3)
            java.lang.Integer r0 = X.AnonymousClass07B.A0i
            goto L_0x007a
        L_0x00ea:
            java.util.concurrent.Callable r0 = java.util.concurrent.Executors.callable(r6, r7)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07690e0.<init>(java.lang.Runnable, java.lang.Object, java.util.concurrent.Callable, X.0VS, long, long, java.lang.Integer, X.0Vk, X.0Vj, long, boolean):void");
    }

    public Object get() {
        this.A09.A00();
        return super.get();
    }

    public Object get(long j, TimeUnit timeUnit) {
        this.A09.A00();
        return super.get(j, timeUnit);
    }
}
