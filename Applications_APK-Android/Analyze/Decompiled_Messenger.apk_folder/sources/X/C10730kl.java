package X;

/* renamed from: X.0kl  reason: invalid class name and case insensitive filesystem */
public final class C10730kl implements AnonymousClass0lE {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01 = ((AnonymousClass1Y7) A04.A09("async_logout_clear_data_in_progress"));
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03 = ((AnonymousClass1Y7) A04.A09("auth_machine_id"));
    public static final AnonymousClass1Y7 A04 = ((AnonymousClass1Y7) C04350Ue.A05.A09("auth/"));
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07 = ((AnonymousClass1Y7) A05.A09("fb_me_user"));
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A;
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C;
    public static final AnonymousClass1Y7 A0D;
    public static final AnonymousClass1Y7 A0E;
    public static final AnonymousClass1Y7 A0F;
    public static final AnonymousClass1Y7 A0G;
    public static final AnonymousClass1Y8 A0H = C04350Ue.A0A.A09("auth/");
    public static final AnonymousClass1Y8 A0I = A0H.A09("session_permananence_info_stored_in_am");

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) A04.A09("user_data/");
        A05 = r1;
        A0C = (AnonymousClass1Y7) r1.A09("fb_uid");
        AnonymousClass1Y7 r12 = A05;
        A0B = (AnonymousClass1Y7) r12.A09("fb_token");
        A08 = (AnonymousClass1Y7) r12.A09("fb_session_cookies_string");
        AnonymousClass1Y7 r13 = A05;
        A0A = (AnonymousClass1Y7) r13.A09("fb_session_secret");
        A09 = (AnonymousClass1Y7) r13.A09("fb_session_key");
        A0D = (AnonymousClass1Y7) r13.A09("fb_username");
        A0E = (AnonymousClass1Y7) r13.A09("in_login_flow");
        r13.A09("is_logged_in_as_profile");
        AnonymousClass1Y7 r14 = A04;
        A02 = (AnonymousClass1Y7) r14.A09("auth_device_based_login_credentials");
        r14.A09("auth_device_based_login_profile_credentials");
        r14.A09("dbl_nux_counter");
        r14.A09("dbl_nux_last_shown_ts");
        r14.A09("dbl_last_shown_nux");
        r14.A09("dbl_nux_cooldown_factor");
        r14.A09("dbl_force_show_nux");
        r14.A09("dbl_force_show_nux_source");
        r14.A09("dbl_local_auth_confirmation_status");
        r14.A09("dbl_sso_blacklist_prefix");
        r14.A09("fb_show_dbl_change_passcode");
        A04.A09("me_user_version");
        AnonymousClass1Y7 r15 = C04350Ue.A07;
        A0G = (AnonymousClass1Y7) r15.A09("logged_in_after_last_auth");
        r15.A09("fb_sign_verification");
        AnonymousClass1Y7 r16 = A04;
        A06 = (AnonymousClass1Y7) r16.A09("dbl_password_account_credentials");
        A00 = (AnonymousClass1Y7) r16.A09("account_switch_in_progress");
        r16.A09("login_logging_id");
        r16.A09("shown_onetime_smartlock_v2");
        r16.A09("shown_count_smartlock");
        A0F = (AnonymousClass1Y7) r16.A09("last_account_switch_timestamp");
    }
}
