package X;

import android.os.Build;
import android.os.Bundle;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeProvider;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0uv  reason: invalid class name and case insensitive filesystem */
public class C15220uv {
    private static final View.AccessibilityDelegate A02 = new View.AccessibilityDelegate();
    public final View.AccessibilityDelegate A00;
    private final View.AccessibilityDelegate A01;

    public void A0A(View view, int i) {
        this.A01.sendAccessibilityEvent(view, i);
    }

    public void A0B(View view, AccessibilityEvent accessibilityEvent) {
        this.A01.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    public void A0C(View view, AccessibilityEvent accessibilityEvent) {
        this.A01.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }

    public boolean A0E(View view, AccessibilityEvent accessibilityEvent) {
        return this.A01.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    public boolean A0F(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.A01.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    public AnonymousClass38I A0G(View view) {
        AccessibilityNodeProvider accessibilityNodeProvider;
        if (Build.VERSION.SDK_INT < 16 || (accessibilityNodeProvider = this.A01.getAccessibilityNodeProvider(view)) == null) {
            return null;
        }
        return new AnonymousClass38I(accessibilityNodeProvider);
    }

    public void A0H(View view, AccessibilityEvent accessibilityEvent) {
        this.A01.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    public void A0I(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        this.A01.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.A02);
    }

    public boolean A0D(View view, int i, Bundle bundle) {
        WeakReference weakReference;
        boolean z;
        ClickableSpan[] clickableSpanArr;
        String name;
        List list = (List) view.getTag(2131300900);
        if (list == null) {
            list = Collections.emptyList();
        }
        boolean z2 = false;
        int i2 = 0;
        while (true) {
            if (i2 >= list.size()) {
                break;
            }
            C25973Cpq cpq = (C25973Cpq) list.get(i2);
            if (cpq.A00() != i) {
                i2++;
            } else if (cpq.A02 != null) {
                AnonymousClass2M9 r3 = null;
                Class cls = cpq.A03;
                if (cls != null) {
                    try {
                        r3 = (AnonymousClass2M9) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                    } catch (Exception e) {
                        Class cls2 = cpq.A03;
                        if (cls2 == null) {
                            name = "null";
                        } else {
                            name = cls2.getName();
                        }
                        Log.e("A11yActionCompat", AnonymousClass08S.A0J("Failed to execute command with argument class ViewCommandArgument: ", name), e);
                    }
                }
                z2 = cpq.A02.perform(view, r3);
            }
        }
        if (!z2 && Build.VERSION.SDK_INT >= 16) {
            z2 = this.A01.performAccessibilityAction(view, i, bundle);
        }
        if (z2 || i != 2131296275) {
            return z2;
        }
        int i3 = bundle.getInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", -1);
        SparseArray sparseArray = (SparseArray) view.getTag(2131300901);
        if (sparseArray == null || (weakReference = (WeakReference) sparseArray.get(i3)) == null) {
            return false;
        }
        ClickableSpan clickableSpan = (ClickableSpan) weakReference.get();
        if (clickableSpan != null) {
            CharSequence text = view.createAccessibilityNodeInfo().getText();
            if (text instanceof Spanned) {
                clickableSpanArr = (ClickableSpan[]) ((Spanned) text).getSpans(0, text.length(), ClickableSpan.class);
            } else {
                clickableSpanArr = null;
            }
            int i4 = 0;
            while (true) {
                if (clickableSpanArr == null || i4 >= clickableSpanArr.length) {
                    break;
                } else if (clickableSpan.equals(clickableSpanArr[i4])) {
                    z = true;
                    break;
                } else {
                    i4++;
                }
            }
        }
        z = false;
        if (!z) {
            return false;
        }
        clickableSpan.onClick(view);
        return true;
    }

    public C15220uv() {
        this(A02);
    }

    public C15220uv(View.AccessibilityDelegate accessibilityDelegate) {
        this.A01 = accessibilityDelegate;
        this.A00 = new C29701gq(this);
    }
}
