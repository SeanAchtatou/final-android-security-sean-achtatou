package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.logger.Logger;
import java.util.Set;

/* renamed from: X.05w  reason: invalid class name and case insensitive filesystem */
public final class C005405w extends AnonymousClass04v {
    public void BkP(int i) {
        Set<String> A03 = ProvidersRegistry.A00.A03(i);
        StringBuilder sb = new StringBuilder();
        for (String str : A03) {
            if (sb.length() != 0) {
                sb.append(",");
            }
            sb.append(str);
        }
        String sb2 = sb.toString();
        int writeStandardEntry = Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126514, 0, 0);
        if ("Active providers" != 0) {
            writeStandardEntry = Logger.writeBytesEntry(0, 1, 56, writeStandardEntry, "Active providers");
        }
        Logger.writeBytesEntry(0, 1, 57, writeStandardEntry, sb2);
    }

    public void BkO() {
        long nanoTime = System.nanoTime();
        Logger.writeBytesEntry(0, 1, 83, Logger.writeStandardEntry(0, 5, 22, nanoTime, 0, 0, 0, 0), "Profilo.ProvidersInitialized");
        Logger.writeStandardEntry(0, 5, 23, nanoTime, 0, 0, 0, 0);
    }
}
