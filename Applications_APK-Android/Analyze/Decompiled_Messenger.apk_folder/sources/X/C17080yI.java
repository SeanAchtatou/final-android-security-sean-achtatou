package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import com.facebook.messenger.neue.MainActivity;

/* renamed from: X.0yI  reason: invalid class name and case insensitive filesystem */
public final class C17080yI extends View.AccessibilityDelegate {
    public final /* synthetic */ MainActivity A00;

    public C17080yI(MainActivity mainActivity) {
        this.A00 = mainActivity;
    }

    public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        ((C52072iQ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANZ, this.A00.A00)).A01(accessibilityEvent);
        return super.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }
}
