package X;

import com.google.common.base.Predicate;

/* renamed from: X.1I1  reason: invalid class name */
public final class AnonymousClass1I1 implements Predicate {
    private final long A00;

    public boolean apply(Object obj) {
        if (((Long) obj).longValue() >= this.A00) {
            return true;
        }
        return false;
    }

    public AnonymousClass1I1(long j) {
        this.A00 = j;
    }
}
