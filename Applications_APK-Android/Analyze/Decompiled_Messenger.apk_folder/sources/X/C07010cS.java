package X;

import android.database.sqlite.SQLiteDatabase;
import com.facebook.common.time.AwakeTimeSinceBootClock;
import java.lang.ref.WeakReference;

/* renamed from: X.0cS  reason: invalid class name and case insensitive filesystem */
public final class C07010cS {
    public long A00 = AwakeTimeSinceBootClock.INSTANCE.now();
    public WeakReference A01;

    public C07010cS(SQLiteDatabase sQLiteDatabase) {
        this.A01 = new WeakReference(sQLiteDatabase);
    }
}
