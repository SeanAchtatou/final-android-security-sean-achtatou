package X;

/* renamed from: X.05e  reason: invalid class name */
public final class AnonymousClass05e {
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0091, code lost:
        if (r3.B5B().BFq() == false) goto L_0x0093;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(android.content.Context r12, X.AnonymousClass04t r13, java.lang.String r14, boolean r15, X.C000900o[] r16, android.util.SparseArray r17, X.AnonymousClass09H[] r18, java.io.File r19) {
        /*
            r10 = r13
            r9 = r12
            if (r12 == 0) goto L_0x0151
            if (r13 == 0) goto L_0x0020
            X.05d r0 = r13.Anp()
            if (r0 == 0) goto L_0x0149
            X.05d r0 = r13.Anp()
            X.05h r0 = r0.B5B()
            if (r0 == 0) goto L_0x0149
            X.05d r0 = r13.Anp()
            X.05i r0 = r0.AiX()
            if (r0 == 0) goto L_0x0149
        L_0x0020:
            r12 = r14
            boolean r0 = android.text.TextUtils.isEmpty(r14)
            if (r0 != 0) goto L_0x0141
            r11 = r16
            if (r16 == 0) goto L_0x0139
            int r0 = r11.length
            r4 = 1
            if (r0 < r4) goto L_0x0139
            r5 = r17
            if (r17 == 0) goto L_0x0131
            int r0 = r5.size()
            if (r0 < r4) goto L_0x0131
            if (r13 != 0) goto L_0x0040
            X.05a r10 = new X.05a
            r10.<init>()
        L_0x0040:
            X.05k r8 = new X.05k
            r14 = r19
            r13 = r15
            r8.<init>(r9, r10, r11, r12, r13, r14)
            java.util.concurrent.atomic.AtomicReference r1 = X.C004505k.A0F
            r0 = 0
            boolean r0 = r1.compareAndSet(r0, r8)
            if (r0 == 0) goto L_0x0129
            monitor-enter(r8)
            X.04t r0 = r8.A01     // Catch:{ all -> 0x0126 }
            X.05d r3 = r0.Anp()     // Catch:{ all -> 0x0126 }
            monitor-exit(r8)     // Catch:{ all -> 0x0126 }
            X.054 r0 = X.AnonymousClass054.A07
            if (r0 != 0) goto L_0x011e
            java.lang.Class<X.054> r2 = X.AnonymousClass054.class
            monitor-enter(r2)
            X.054 r0 = X.AnonymousClass054.A07     // Catch:{ all -> 0x011b }
            if (r0 != 0) goto L_0x0113
            X.054 r0 = new X.054     // Catch:{ all -> 0x011b }
            r0.<init>(r5, r3, r8)     // Catch:{ all -> 0x011b }
            X.AnonymousClass054.A07 = r0     // Catch:{ all -> 0x011b }
            monitor-exit(r2)     // Catch:{ all -> 0x011b }
            monitor-enter(r8)
            X.05m r0 = r8.A03     // Catch:{ all -> 0x0110 }
            java.io.File r6 = r0.A03     // Catch:{ all -> 0x0110 }
            boolean r1 = r8.A0B     // Catch:{ all -> 0x0110 }
            if (r1 == 0) goto L_0x0081
            X.05h r0 = r3.B5B()     // Catch:{ all -> 0x0110 }
            int r5 = r0.AfE()     // Catch:{ all -> 0x0110 }
            r0 = -1
            if (r5 != r0) goto L_0x0086
            goto L_0x0084
        L_0x0081:
            r5 = 1000(0x3e8, float:1.401E-42)
            goto L_0x0086
        L_0x0084:
            r5 = 5000(0x1388, float:7.006E-42)
        L_0x0086:
            if (r1 == 0) goto L_0x0093
            X.05h r0 = r3.B5B()     // Catch:{ all -> 0x0110 }
            boolean r1 = r0.BFq()     // Catch:{ all -> 0x0110 }
            r0 = 1
            if (r1 != 0) goto L_0x0094
        L_0x0093:
            r0 = 0
        L_0x0094:
            if (r0 == 0) goto L_0x00b1
            com.facebook.profilo.mmapbuf.MmapBufferManager r7 = new com.facebook.profilo.mmapbuf.MmapBufferManager     // Catch:{ all -> 0x0110 }
            long r1 = r3.Ai2()     // Catch:{ all -> 0x0110 }
            X.05m r0 = r8.A03     // Catch:{ all -> 0x0110 }
            java.io.File r0 = r0.A05     // Catch:{ all -> 0x0110 }
            r7.<init>(r1, r0, r9)     // Catch:{ all -> 0x0110 }
            r8.A04 = r7     // Catch:{ all -> 0x0110 }
            X.0Kp r1 = new X.0Kp     // Catch:{ all -> 0x0110 }
            r1.<init>(r7)     // Catch:{ all -> 0x0110 }
            X.05n r0 = r8.A09     // Catch:{ all -> 0x0110 }
            java.util.concurrent.CopyOnWriteArrayList r0 = r0.A00     // Catch:{ all -> 0x0110 }
            r0.add(r1)     // Catch:{ all -> 0x0110 }
        L_0x00b1:
            java.lang.String r1 = r8.A0A     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.mmapbuf.MmapBufferManager r2 = r8.A04     // Catch:{ all -> 0x0110 }
            java.lang.String r0 = "profilo"
            X.AnonymousClass01q.A08(r0)     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.core.TraceEvents.sInitialized = r4     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.logger.Logger.sInitialized = r4     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.logger.Logger.sTraceDirectory = r6     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.logger.Logger.sFilePrefix = r1     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.logger.Logger.sLoggerCallbacks = r8     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.logger.Logger.sNativeTraceWriterCallbacks = r8     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.logger.Logger.sRingBufferSize = r5     // Catch:{ all -> 0x0110 }
            java.util.concurrent.atomic.AtomicReference r1 = new java.util.concurrent.atomic.AtomicReference     // Catch:{ all -> 0x0110 }
            r0 = 0
            r1.<init>(r0)     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.logger.Logger.sWorker = r1     // Catch:{ all -> 0x0110 }
            com.facebook.profilo.logger.Logger.sMmapBufferManager = r2     // Catch:{ all -> 0x0110 }
            X.C004505k.A04(r8, r3)     // Catch:{ all -> 0x0110 }
            X.05m r4 = r8.A03     // Catch:{ all -> 0x0110 }
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.DAYS     // Catch:{ all -> 0x0110 }
            r0 = 1
            long r2 = r2.toSeconds(r0)     // Catch:{ all -> 0x0110 }
            r0 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r0
            r4.A01 = r2     // Catch:{ all -> 0x0110 }
            X.05m r1 = r8.A03     // Catch:{ all -> 0x0110 }
            r0 = 10
            r1.A00 = r0     // Catch:{ all -> 0x0110 }
            X.05n r0 = r8.A09     // Catch:{ all -> 0x0110 }
            X.05w r1 = new X.05w     // Catch:{ all -> 0x0110 }
            r1.<init>()     // Catch:{ all -> 0x0110 }
            java.util.concurrent.CopyOnWriteArrayList r0 = r0.A00     // Catch:{ all -> 0x0110 }
            r0.add(r1)     // Catch:{ all -> 0x0110 }
            monitor-exit(r8)     // Catch:{ all -> 0x0110 }
            r4 = r18
            if (r18 == 0) goto L_0x010f
            int r3 = r4.length
            r2 = 0
        L_0x00fd:
            if (r2 >= r3) goto L_0x010f
            r1 = r18[r2]
            X.05k r0 = X.C004505k.A00()
            X.05n r0 = r0.A09
            java.util.concurrent.CopyOnWriteArrayList r0 = r0.A00
            r0.add(r1)
            int r2 = r2 + 1
            goto L_0x00fd
        L_0x010f:
            return
        L_0x0110:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0110 }
            goto L_0x0128
        L_0x0113:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x011b }
            java.lang.String r0 = "TraceControl already initialized"
            r1.<init>(r0)     // Catch:{ all -> 0x011b }
            throw r1     // Catch:{ all -> 0x011b }
        L_0x011b:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x011b }
            goto L_0x0128
        L_0x011e:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "TraceControl already initialized"
            r1.<init>(r0)
            throw r1
        L_0x0126:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0126 }
        L_0x0128:
            throw r0
        L_0x0129:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Orchestrator already initialized"
            r1.<init>(r0)
            throw r1
        L_0x0131:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Null or empty list of controllers"
            r1.<init>(r0)
            throw r1
        L_0x0139:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Null or empty list of trace providers"
            r1.<init>(r0)
            throw r1
        L_0x0141:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Null or empty process name"
            r1.<init>(r0)
            throw r1
        L_0x0149:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Non-null config provider with null sub-configs"
            r1.<init>(r0)
            throw r1
        L_0x0151:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Null Context"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass05e.A00(android.content.Context, X.04t, java.lang.String, boolean, X.00o[], android.util.SparseArray, X.09H[], java.io.File):void");
    }
}
