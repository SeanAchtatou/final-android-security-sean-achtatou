package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.SortedMap;

@UserScoped
/* renamed from: X.22R  reason: invalid class name */
public final class AnonymousClass22R {
    private static C05540Zi A02;
    private static final AnonymousClass1Y7 A03 = ((AnonymousClass1Y7) C04350Ue.A03.A09("platform_badging/"));
    public final FbSharedPreferences A00;
    private final String A01;

    private boolean A03(AnonymousClass2MG r5, ThreadKey threadKey, Integer num) {
        boolean z = false;
        if (num != null) {
            int A05 = A05(r5, threadKey);
            int intValue = num.intValue();
            if (A05 != intValue) {
                z = true;
            }
            C30281hn edit = this.A00.edit();
            edit.Bz6(A01(this, r5, AnonymousClass07B.A00, threadKey), intValue);
            edit.commit();
        }
        return z;
    }

    public boolean A08(AnonymousClass2MG r5, ThreadKey threadKey, Boolean bool) {
        if (bool == null) {
            return false;
        }
        boolean A07 = A07(r5, threadKey);
        boolean booleanValue = bool.booleanValue();
        boolean z = false;
        if (A07 != booleanValue) {
            z = true;
        }
        boolean z2 = false | z;
        C30281hn edit = this.A00.edit();
        edit.putBoolean(A01(this, r5, AnonymousClass07B.A01, threadKey), booleanValue);
        edit.commit();
        return z2;
    }

    public static final AnonymousClass22R A00(AnonymousClass1XY r4) {
        AnonymousClass22R r0;
        synchronized (AnonymousClass22R.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass22R((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass22R) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass1Y7 A02(AnonymousClass2MG r4, Integer num) {
        String str;
        AnonymousClass1Y7 r0 = (AnonymousClass1Y7) ((AnonymousClass1Y7) A03.A09(AnonymousClass08S.A0J(r4.name(), "/"))).A09(AnonymousClass08S.A0J(this.A01, "/"));
        if (num == null) {
            return r0;
        }
        if (1 - num.intValue() != 0) {
            str = "BADGE_COUNT";
        } else {
            str = "CACHE_INVALIDATED";
        }
        return (AnonymousClass1Y7) r0.A09(AnonymousClass08S.A0J(str, "/"));
    }

    public int A04(AnonymousClass2MG r7) {
        AnonymousClass1Y7 A022 = A02(r7, AnonymousClass07B.A00);
        SortedMap AlW = this.A00.AlW(A022);
        int i = 0;
        for (AnonymousClass1Y7 r1 : AlW.keySet()) {
            if (!r1.equals(A022)) {
                i += Integer.parseInt(AlW.get(r1).toString());
            }
        }
        return i;
    }

    public int A05(AnonymousClass2MG r4, ThreadKey threadKey) {
        return this.A00.AqN(A01(this, r4, AnonymousClass07B.A00, threadKey), 0);
    }

    public void A06(AnonymousClass2MG r5) {
        for (AnonymousClass1Y7 putBoolean : this.A00.Arq(A01(this, r5, AnonymousClass07B.A01, null))) {
            C30281hn edit = this.A00.edit();
            edit.putBoolean(putBoolean, true);
            edit.commit();
        }
    }

    public boolean A07(AnonymousClass2MG r4, ThreadKey threadKey) {
        return this.A00.Aep(A01(this, r4, AnonymousClass07B.A01, threadKey), false);
    }

    public boolean A09(C202069fh r5) {
        boolean z;
        Boolean bool = r5.A03;
        if (bool == null || !bool.booleanValue()) {
            z = false;
        } else {
            AnonymousClass2MG r2 = r5.A00;
            FbSharedPreferences fbSharedPreferences = this.A00;
            fbSharedPreferences.ASu(fbSharedPreferences.Arq(A01(this, r2, null, null)));
            z = true;
        }
        ThreadKey threadKey = r5.A01;
        if (threadKey != null) {
            z |= A03(r5.A00, threadKey, r5.A04);
        }
        return A08(r5.A00, null, r5.A02) | z | A03(r5.A00, null, r5.A05);
    }

    private AnonymousClass22R(AnonymousClass1XY r2) {
        this.A00 = FbSharedPreferencesModule.A00(r2);
        this.A01 = AnonymousClass0XJ.A0F(r2);
    }

    public static AnonymousClass1Y7 A01(AnonymousClass22R r0, AnonymousClass2MG r1, Integer num, ThreadKey threadKey) {
        AnonymousClass1Y7 A022 = r0.A02(r1, num);
        if (threadKey != null) {
            return (AnonymousClass1Y7) A022.A09(threadKey.A0J());
        }
        return A022;
    }
}
