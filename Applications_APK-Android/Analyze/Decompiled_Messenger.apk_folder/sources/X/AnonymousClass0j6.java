package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.ExecutionException;

/* renamed from: X.0j6  reason: invalid class name */
public final class AnonymousClass0j6 extends AnonymousClass0j7 implements AnonymousClass0j8 {
    public AnonymousClass0UN A00;

    public void Bv6(C16330ws r4) {
        ListenableFuture listenableFuture;
        if (r4.A03(false) && (listenableFuture = (ListenableFuture) this.A04.get()) != null && listenableFuture.isDone()) {
            try {
                C22371Lb r0 = (C22371Lb) listenableFuture.get();
                if ((r0 == null || r0.A01) && this.A04.compareAndSet(listenableFuture, null)) {
                    A00();
                }
            } catch (InterruptedException | ExecutionException unused) {
            }
        }
    }

    public AnonymousClass0j6(AnonymousClass1XY r3, C09820ik r4, C09780ig r5) {
        super(r4, new C09790ih(r5, r4));
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
