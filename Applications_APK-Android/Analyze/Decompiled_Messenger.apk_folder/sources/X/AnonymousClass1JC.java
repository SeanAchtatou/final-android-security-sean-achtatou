package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* renamed from: X.1JC  reason: invalid class name */
public final class AnonymousClass1JC {
    public HashSet A00;
    public List A01;
    public Map A02;
    public Map A03;
    public Map A04;
    public Map A05;
    public Map A06;
    public Map A07;
    public Map A08;

    private synchronized void A01() {
        if (this.A05 == null) {
            this.A05 = new HashMap(4);
        }
    }

    private synchronized void A02() {
        if (this.A07 == null) {
            this.A07 = new HashMap(4);
        }
        if (this.A02 == null) {
            this.A02 = new HashMap(4);
        }
    }

    public static synchronized void A03(AnonymousClass1JC r1) {
        synchronized (r1) {
            if (r1.A06 == null) {
                r1.A06 = new HashMap();
            }
        }
    }

    public static synchronized void A04(AnonymousClass1JC r2) {
        synchronized (r2) {
            if (r2.A08 == null) {
                r2.A08 = new HashMap(4);
            }
        }
    }

    public void A08(AnonymousClass1JC r9) {
        Map map;
        Map map2;
        Map map3;
        List list;
        Map map4;
        List list2;
        List list3;
        synchronized (r9) {
            map = r9.A02;
        }
        synchronized (this) {
            if (map != null) {
                try {
                    Map map5 = this.A07;
                    if (map5 != null && !map5.isEmpty()) {
                        for (Map.Entry entry : map.entrySet()) {
                            String str = (String) entry.getKey();
                            synchronized (this) {
                                try {
                                    list2 = (List) this.A07.get(str);
                                    Map map6 = this.A05;
                                    if (map6 == null) {
                                        list3 = null;
                                    } else {
                                        list3 = (List) map6.get(str);
                                    }
                                } catch (Throwable th) {
                                    while (true) {
                                        th = th;
                                        break;
                                    }
                                }
                            }
                            if (list2 != null) {
                                List list4 = (List) entry.getValue();
                                if (list2.size() == list4.size()) {
                                    synchronized (this) {
                                        try {
                                            this.A07.remove(str);
                                            Map map7 = this.A05;
                                            if (map7 != null) {
                                                map7.remove(str);
                                            }
                                        } catch (Throwable th2) {
                                            while (true) {
                                                th = th2;
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    list2.removeAll(list4);
                                    if (list3 != null) {
                                        list3.removeAll(list4);
                                    }
                                }
                            }
                        }
                    }
                } catch (Throwable th3) {
                    while (true) {
                        th = th3;
                    }
                }
            }
        }
        HashSet hashSet = r9.A00;
        ArrayList<String> arrayList = new ArrayList<>();
        if (!(hashSet == null || (map4 = r9.A08) == null)) {
            arrayList.addAll(map4.keySet());
            for (String str2 : arrayList) {
                if (!hashSet.contains(str2)) {
                    r9.A08.remove(str2);
                }
            }
        }
        synchronized (r9) {
            map2 = r9.A08;
        }
        A06(map2);
        synchronized (r9) {
            map3 = r9.A06;
        }
        A07(map3);
        Map map8 = this.A04;
        if (map8 != null) {
            map8.clear();
        }
        Map map9 = r9.A04;
        if (map9 != null && !map9.isEmpty()) {
            Map map10 = this.A04;
            if (map10 == null) {
                this.A04 = new HashMap(r9.A04);
            } else {
                map10.putAll(r9.A04);
            }
        }
        List list5 = null;
        if (list5 != null && (list = r9.A01) != null) {
            list5.removeAll(list);
            return;
        }
        return;
        throw th;
    }

    public synchronized void A09(String str, C61322yh r4, boolean z) {
        A02();
        Map map = this.A07;
        List list = (List) map.get(str);
        if (list == null) {
            list = A00(null);
            map.put(str, list);
        }
        list.add(r4);
        if (z) {
            A01();
            Map map2 = this.A05;
            List list2 = (List) map2.get(str);
            if (list2 == null) {
                list2 = A00(null);
                map2.put(str, list2);
            }
            list2.add(r4);
        }
    }

    public synchronized boolean A0A() {
        boolean z;
        List list;
        Map map = this.A07;
        if ((map == null || map.isEmpty()) && ((list = null) == null || list.isEmpty())) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    private static List A00(List list) {
        int size;
        if (list == null) {
            size = 4;
        } else {
            size = list.size();
        }
        ArrayList arrayList = new ArrayList(size);
        if (list != null) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    public static void A05(AnonymousClass1JC r4, Map map) {
        if (map != null && !map.isEmpty()) {
            r4.A01();
            for (Map.Entry entry : map.entrySet()) {
                r4.A05.put(entry.getKey(), A00((List) entry.getValue()));
            }
        }
    }

    private void A06(Map map) {
        if (map != null && !map.isEmpty()) {
            synchronized (this) {
                A04(this);
                this.A08.clear();
                this.A08.putAll(map);
            }
        }
    }

    private void A07(Map map) {
        if (map != null && !map.isEmpty()) {
            synchronized (this) {
                A03(this);
                this.A06.putAll(map);
            }
        }
    }

    public AnonymousClass1JC() {
        this(null);
    }

    public AnonymousClass1JC(AnonymousClass1JC r9) {
        Map map;
        Map map2;
        Map map3;
        Map map4;
        Map map5;
        if (r9 != null) {
            synchronized (this) {
                synchronized (r9) {
                    try {
                        map = r9.A07;
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
                synchronized (r9) {
                    map2 = r9.A05;
                }
                synchronized (r9) {
                    map3 = r9.A02;
                }
                if ((map != null && !map.isEmpty()) || (map3 != null && !map3.isEmpty())) {
                    A02();
                    synchronized (this) {
                        if (map != null) {
                            try {
                                for (String str : map.keySet()) {
                                    this.A07.put(str, A00((List) map.get(str)));
                                }
                            } catch (Throwable th2) {
                                while (true) {
                                    th = th2;
                                    break;
                                }
                            }
                        }
                        A05(this, map2);
                        if (map3 != null) {
                            for (Map.Entry entry : map3.entrySet()) {
                                this.A02.put(entry.getKey(), A00((List) entry.getValue()));
                            }
                        }
                    }
                }
                synchronized (r9) {
                    map4 = r9.A08;
                }
                A06(map4);
                synchronized (r9) {
                    map5 = r9.A06;
                }
                A07(map5);
                Map map6 = r9.A04;
                if (map6 != null) {
                    this.A04 = new HashMap(map6);
                }
                if (0 != 0) {
                    ArrayList<C112185Vs> arrayList = new ArrayList<>((Collection) null);
                    for (C112185Vs apply : arrayList) {
                        apply.apply(this);
                    }
                    this.A01 = arrayList;
                }
            }
        }
    }
}
