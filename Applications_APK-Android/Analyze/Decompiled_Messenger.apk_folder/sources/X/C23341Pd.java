package X;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

/* renamed from: X.1Pd  reason: invalid class name and case insensitive filesystem */
public class C23341Pd {
    public ContentResolver A00;
    public AssetManager A01;
    public Resources A02;
    public boolean A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final C22831Mz A07;
    public final C30911iq A08;
    public final C30911iq A09;
    public final C22601Mc A0A;
    public final C23311Pa A0B;
    public final C23311Pa A0C;
    public final C22971No A0D;
    public final C22641Mg A0E;
    public final C22761Ms A0F;
    public final C23301Oz A0G;
    public final C22861Nc A0H;
    public final C30661iR A0I;
    public final boolean A0J;
    public final boolean A0K;
    public final boolean A0L;
    public final boolean A0M;

    public AnonymousClass1QB A00(AnonymousClass1Q3 r8, boolean z, C23351Pe r10) {
        return new AnonymousClass1QB(this.A0E.AaN(), this.A0I, r8, z, r10);
    }

    public AnonymousClass0mS A01(AnonymousClass1Q3 r2, AnonymousClass1PR r3) {
        if (!(this instanceof C11240mR)) {
            return new AnonymousClass0mS(r2, r3);
        }
        return new C71213c1(r2, r3);
    }

    public C23341Pd(Context context, C22861Nc r3, C22761Ms r4, C23301Oz r5, boolean z, boolean z2, boolean z3, C22641Mg r9, C30661iR r10, C23311Pa r11, C23311Pa r12, C30911iq r13, C30911iq r14, C22601Mc r15, C22831Mz r16, int i, int i2, boolean z4, int i3, C22971No r21, boolean z5) {
        this.A00 = context.getApplicationContext().getContentResolver();
        this.A02 = context.getApplicationContext().getResources();
        this.A01 = context.getApplicationContext().getAssets();
        this.A0H = r3;
        this.A0F = r4;
        this.A0G = r5;
        this.A0K = z;
        this.A0M = z2;
        this.A0J = z3;
        this.A0E = r9;
        this.A0I = r10;
        this.A0B = r11;
        this.A0C = r12;
        this.A08 = r13;
        this.A09 = r14;
        this.A0A = r15;
        this.A07 = r16;
        this.A05 = i;
        this.A04 = i2;
        this.A03 = z4;
        this.A06 = i3;
        this.A0D = r21;
        this.A0L = z5;
    }
}
