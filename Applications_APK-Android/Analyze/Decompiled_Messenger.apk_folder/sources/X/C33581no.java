package X;

import com.google.common.collect.MapMakerInternalMap;

/* renamed from: X.1no  reason: invalid class name and case insensitive filesystem */
public final class C33581no implements C25571a3 {
    public static final C33581no A00 = new C33581no();

    public C07740e5 AUU(MapMakerInternalMap.Segment segment, C07740e5 r6, C07740e5 r7) {
        MapMakerInternalMap.WeakKeyDummyValueSegment weakKeyDummyValueSegment = (MapMakerInternalMap.WeakKeyDummyValueSegment) segment;
        C33601nq r62 = (C33601nq) r6;
        C33601nq r72 = (C33601nq) r7;
        if (r62.getKey() == null) {
            return null;
        }
        return new C33601nq(weakKeyDummyValueSegment.queueForKeys, r62.getKey(), r62.A01, r72);
    }

    public MapMakerInternalMap.Strength BHg() {
        return MapMakerInternalMap.Strength.WEAK;
    }

    public C07740e5 BLe(MapMakerInternalMap.Segment segment, Object obj, int i, C07740e5 r6) {
        return new C33601nq(((MapMakerInternalMap.WeakKeyDummyValueSegment) segment).queueForKeys, obj, i, (C33601nq) r6);
    }

    public MapMakerInternalMap.Segment BLh(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
        return new MapMakerInternalMap.WeakKeyDummyValueSegment(mapMakerInternalMap, i, i2);
    }

    public MapMakerInternalMap.Strength CLa() {
        return MapMakerInternalMap.Strength.STRONG;
    }

    public void CCs(MapMakerInternalMap.Segment segment, C07740e5 r2, Object obj) {
    }
}
