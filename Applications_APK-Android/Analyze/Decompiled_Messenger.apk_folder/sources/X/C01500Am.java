package X;

import android.os.Handler;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Am  reason: invalid class name and case insensitive filesystem */
public final class C01500Am extends AbstractExecutorService implements C01520Ao {
    public final Handler A00;

    /* renamed from: C4X */
    public C01940Cf schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        AnonymousClass0KC r4 = new AnonymousClass0KC(this.A00, runnable, null);
        AnonymousClass00S.A05(this.A00, r4, timeUnit.toMillis(j), 2069592491);
        return r4;
    }

    public boolean isShutdown() {
        return false;
    }

    public boolean isTerminated() {
        return false;
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void execute(Runnable runnable) {
        AnonymousClass00S.A04(this.A00, runnable, 969313590);
    }

    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void shutdown() {
        throw new UnsupportedOperationException();
    }

    public List shutdownNow() {
        throw new UnsupportedOperationException();
    }

    public C01500Am(Handler handler) {
        this.A00 = handler;
    }

    public RunnableFuture newTaskFor(Runnable runnable, Object obj) {
        return new AnonymousClass0Jl(this.A00, runnable, obj);
    }

    public RunnableFuture newTaskFor(Callable callable) {
        return new AnonymousClass0Jl(this.A00, callable);
    }

    public ScheduledFuture schedule(Callable callable, long j, TimeUnit timeUnit) {
        AnonymousClass0KC r4 = new AnonymousClass0KC(this.A00, callable);
        AnonymousClass00S.A05(this.A00, r4, timeUnit.toMillis(j), 1679472124);
        return r4;
    }

    public Future submit(Runnable runnable) {
        if (runnable != null) {
            AnonymousClass0KC r1 = new AnonymousClass0KC(this.A00, runnable, null);
            AnonymousClass07A.A04(this, r1, 1786217413);
            return r1;
        }
        throw new NullPointerException();
    }

    public /* bridge */ /* synthetic */ Future submit(Runnable runnable, Object obj) {
        if (runnable != null) {
            AnonymousClass0KC r1 = new AnonymousClass0KC(this.A00, runnable, obj);
            AnonymousClass07A.A04(this, r1, 1786217413);
            return r1;
        }
        throw new NullPointerException();
    }

    public Future submit(Callable callable) {
        if (callable != null) {
            AnonymousClass0KC r1 = new AnonymousClass0KC(this.A00, callable);
            AnonymousClass07A.A04(this, r1, 1991479779);
            return r1;
        }
        throw new NullPointerException();
    }
}
