package X;

import java.util.concurrent.Callable;

/* renamed from: X.0XW  reason: invalid class name */
public final class AnonymousClass0XW extends C25131Yl {
    public final /* synthetic */ AnonymousClass0XV A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0XW(Runnable runnable, Callable callable, String str, AnonymousClass0XV r4, String str2) {
        super(runnable, callable);
        this.A02 = str;
        this.A00 = r4;
        this.A01 = str2;
    }

    public Object call() {
        return A00(this.A02, this.A00, this.A01);
    }
}
