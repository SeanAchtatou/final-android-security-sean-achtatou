package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.18g  reason: invalid class name and case insensitive filesystem */
public final class C194618g implements AnonymousClass1AS {
    public final /* synthetic */ ThreadListFragment A00;
    public final /* synthetic */ C197919r A01;

    public C194618g(ThreadListFragment threadListFragment, C197919r r2) {
        this.A00 = threadListFragment;
        this.A01 = r2;
    }

    public void BNq(RecyclerView recyclerView) {
        this.A01.BNq(recyclerView);
    }

    public void BPg(RecyclerView recyclerView) {
        this.A01.BPg(recyclerView);
    }

    public void BeZ() {
        int size = View.MeasureSpec.getSize(AnonymousClass1JI.A00(this.A00.A12(), this.A00.A11().getPackageManager(), View.MeasureSpec.makeMeasureSpec(this.A00.A1F.getMeasuredWidth(), 1073741824)));
        int measuredHeight = this.A00.A1F.getMeasuredHeight();
        if (size > 0 && measuredHeight > 0) {
            ThreadListFragment threadListFragment = this.A00;
            if (size != threadListFragment.A01 || measuredHeight != threadListFragment.A00) {
                threadListFragment.A0S.CBs(size, measuredHeight);
                ThreadListFragment threadListFragment2 = this.A00;
                threadListFragment2.A01 = size;
                threadListFragment2.A00 = measuredHeight;
                ThreadListFragment.A0E(threadListFragment2);
            }
        }
    }
}
