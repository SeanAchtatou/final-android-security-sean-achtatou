package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.common.classmarkers.ClassMarkerLoader;
import com.facebook.messenger.app.MessengerApplicationImpl;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0gD  reason: invalid class name and case insensitive filesystem */
public final class C08930gD implements AnonymousClass06U {
    public final /* synthetic */ MessengerApplicationImpl A00;

    public C08930gD(MessengerApplicationImpl messengerApplicationImpl) {
        this.A00 = messengerApplicationImpl;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(-272405634);
        C93584cy r4 = (C93584cy) AnonymousClass1XX.A02(7, AnonymousClass1Y3.B3x, this.A00.A04);
        if (r4.A01.Aem(282673273046845L) && r4.A01.Aem(282673272915771L)) {
            AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BKH, r4.A00), new C93594cz(r4), 1597941175);
        }
        ClassMarkerLoader.loadIsBackgroundRestartFinishMarker();
        AnonymousClass09Y.A01(-1301903258, A002);
    }
}
