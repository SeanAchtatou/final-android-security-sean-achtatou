package X;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1W1  reason: invalid class name */
public final class AnonymousClass1W1 {
    private static volatile AnonymousClass1W1 A0A;
    public long A00 = Long.MIN_VALUE;
    public C48552ac A01;
    public List A02 = C04300To.A00();
    public AtomicReference A03 = new AtomicReference(AnonymousClass1Vy.UNKNOWN);
    private int A04;
    private AtomicReference A05;
    public final AnonymousClass069 A06;
    public final C25051Yd A07;
    public final double[] A08 = new double[8];
    private volatile boolean A09 = false;

    private synchronized AnonymousClass1Vy A05() {
        AnonymousClass1Vy r0;
        C48552ac r02 = this.A01;
        if (r02 == null) {
            r0 = AnonymousClass1Vy.UNKNOWN;
        } else {
            double Adp = r02.Adp();
            if (Adp < 0.0d) {
                r0 = AnonymousClass1Vy.UNKNOWN;
            } else if (Adp < A00()) {
                r0 = AnonymousClass1Vy.A01;
            } else if (Adp < A03()) {
                r0 = AnonymousClass1Vy.POOR;
            } else if (Adp < A02()) {
                r0 = AnonymousClass1Vy.A04;
            } else if (Adp < A01()) {
                r0 = AnonymousClass1Vy.GOOD;
            } else {
                r0 = AnonymousClass1Vy.A02;
            }
        }
        return r0;
    }

    public synchronized double A07() {
        double d;
        C48552ac r0 = this.A01;
        if (r0 == null) {
            d = -1.0d;
        } else {
            d = r0.Adp();
        }
        return d;
    }

    private double A00() {
        double[] dArr = this.A08;
        if (dArr[0] == 0.0d) {
            dArr[0] = (double) this.A07.At1(563276371067208L, 2);
        }
        return this.A08[0];
    }

    private double A01() {
        double[] dArr = this.A08;
        if (dArr[3] == 0.0d) {
            dArr[3] = (double) this.A07.At1(563276370936134L, 2000);
        }
        return this.A08[3];
    }

    private double A02() {
        double[] dArr = this.A08;
        if (dArr[2] == 0.0d) {
            dArr[2] = (double) this.A07.At1(563276371001671L, 550);
        }
        return this.A08[2];
    }

    private double A03() {
        double[] dArr = this.A08;
        if (dArr[1] == 0.0d) {
            dArr[1] = (double) this.A07.At1(563276371132745L, 150);
        }
        return this.A08[1];
    }

    private long A04() {
        long At1 = this.A07.At1(563276371329356L, 20);
        if (At1 < 0) {
            return 0;
        }
        if (At1 > 99) {
            return 99;
        }
        return At1;
    }

    public static final AnonymousClass1W1 A06(AnonymousClass1XY r5) {
        if (A0A == null) {
            synchronized (AnonymousClass1W1.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0A = new AnonymousClass1W1(AnonymousClass0WT.A00(applicationInjector), AnonymousClass067.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public AnonymousClass1Vy A08(C30181hd r3) {
        synchronized (this.A02) {
            if (r3 != null) {
                this.A02.add(r3);
            }
        }
        return (AnonymousClass1Vy) this.A03.get();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0138, code lost:
        if (r11 < (r15 * r3.A08[7])) goto L_0x013a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x013f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A09(long r18, long r20) {
        /*
            r17 = this;
            r3 = r17
            monitor-enter(r3)
            r1 = 0
            r6 = r20
            int r0 = (r20 > r1 ? 1 : (r20 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0193
            r0 = r18
            double r4 = (double) r0
            r0 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            java.lang.Double.isNaN(r4)
            double r4 = r4 * r0
            double r0 = (double) r6
            java.lang.Double.isNaN(r0)
            double r4 = r4 / r0
            r0 = 4620693217682128896(0x4020000000000000, double:8.0)
            double r4 = r4 * r0
            X.2ac r0 = r3.A01     // Catch:{ all -> 0x019b }
            if (r0 != 0) goto L_0x0050
            X.3gs r8 = new X.3gs     // Catch:{ all -> 0x019b }
            r0 = r3
            double[] r11 = r3.A08     // Catch:{ all -> 0x019b }
            r12 = 4
            r6 = r11[r12]     // Catch:{ all -> 0x019b }
            r1 = 0
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0047
            r9 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
            X.1Yd r2 = r3.A07     // Catch:{ all -> 0x019b }
            r6 = 563276371198282(0x2004c0004014a, double:2.78295504123193E-309)
            r0 = 0
            long r6 = r2.At1(r6, r0)     // Catch:{ all -> 0x019b }
            double r0 = (double) r6     // Catch:{ all -> 0x019b }
            java.lang.Double.isNaN(r0)     // Catch:{ all -> 0x019b }
            double r0 = r0 * r9
            r11[r12] = r0     // Catch:{ all -> 0x019b }
        L_0x0047:
            double[] r0 = r3.A08     // Catch:{ all -> 0x019b }
            r0 = r0[r12]     // Catch:{ all -> 0x019b }
            r8.<init>(r0)     // Catch:{ all -> 0x019b }
            r3.A01 = r8     // Catch:{ all -> 0x019b }
        L_0x0050:
            X.2ac r0 = r3.A01     // Catch:{ all -> 0x019b }
            r0.AN9(r4)     // Catch:{ all -> 0x019b }
            X.069 r0 = r3.A06     // Catch:{ all -> 0x019b }
            long r0 = r0.now()     // Catch:{ all -> 0x019b }
            r3.A00 = r0     // Catch:{ all -> 0x019b }
            boolean r0 = r3.A09     // Catch:{ all -> 0x019b }
            r2 = 1
            if (r0 == 0) goto L_0x0178
            int r0 = r3.A04     // Catch:{ all -> 0x019b }
            int r0 = r0 + r2
            r3.A04 = r0     // Catch:{ all -> 0x019b }
            X.1Vy r1 = r3.A05()     // Catch:{ all -> 0x019b }
            java.util.concurrent.atomic.AtomicReference r0 = r3.A05     // Catch:{ all -> 0x019b }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x019b }
            r9 = 0
            if (r1 == r0) goto L_0x0078
            r3.A09 = r9     // Catch:{ all -> 0x019b }
            r3.A04 = r2     // Catch:{ all -> 0x019b }
        L_0x0078:
            int r6 = r3.A04     // Catch:{ all -> 0x019b }
            r0 = r3
            double[] r8 = r3.A08     // Catch:{ all -> 0x019b }
            r12 = 5
            r10 = r8[r12]     // Catch:{ all -> 0x019b }
            r4 = 0
            int r0 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x0096
            X.1Yd r7 = r3.A07     // Catch:{ all -> 0x019b }
            r4 = 563276371263819(0x2004c0005014b, double:2.782955041555725E-309)
            r0 = 5
            long r4 = r7.At1(r4, r0)     // Catch:{ all -> 0x019b }
            double r0 = (double) r4     // Catch:{ all -> 0x019b }
            r8[r12] = r0     // Catch:{ all -> 0x019b }
        L_0x0096:
            double[] r0 = r3.A08     // Catch:{ all -> 0x019b }
            r4 = r0[r12]     // Catch:{ all -> 0x019b }
            int r0 = (int) r4     // Catch:{ all -> 0x019b }
            if (r6 < r0) goto L_0x0191
            X.2ac r1 = r3.A01     // Catch:{ all -> 0x019b }
            r0 = 0
            if (r1 == 0) goto L_0x013c
            java.util.concurrent.atomic.AtomicReference r0 = r3.A03     // Catch:{ all -> 0x019b }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x019b }
            X.1Vy r1 = (X.AnonymousClass1Vy) r1     // Catch:{ all -> 0x019b }
            int[] r0 = X.C73893gw.A00     // Catch:{ all -> 0x019b }
            int r1 = r1.ordinal()     // Catch:{ all -> 0x019b }
            r0 = r0[r1]     // Catch:{ all -> 0x019b }
            r0 = 1
            switch(r1) {
                case 0: goto L_0x00c2;
                case 1: goto L_0x00c9;
                case 2: goto L_0x00d2;
                case 3: goto L_0x00db;
                case 4: goto L_0x00b8;
                default: goto L_0x00b6;
            }     // Catch:{ all -> 0x019b }
        L_0x00b6:
            goto L_0x013a
        L_0x00b8:
            double r15 = r3.A01()     // Catch:{ all -> 0x019b }
            r13 = 5183643170566569984(0x47efffffe0000000, double:3.4028234663852886E38)
            goto L_0x00e3
        L_0x00c2:
            r15 = 0
            double r13 = r3.A00()     // Catch:{ all -> 0x019b }
            goto L_0x00e3
        L_0x00c9:
            double r15 = r3.A00()     // Catch:{ all -> 0x019b }
            double r13 = r3.A03()     // Catch:{ all -> 0x019b }
            goto L_0x00e3
        L_0x00d2:
            double r15 = r3.A03()     // Catch:{ all -> 0x019b }
            double r13 = r3.A02()     // Catch:{ all -> 0x019b }
            goto L_0x00e3
        L_0x00db:
            double r15 = r3.A02()     // Catch:{ all -> 0x019b }
            double r13 = r3.A01()     // Catch:{ all -> 0x019b }
        L_0x00e3:
            X.2ac r0 = r3.A01     // Catch:{ all -> 0x019b }
            double r11 = r0.Adp()     // Catch:{ all -> 0x019b }
            int r0 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r0 <= 0) goto L_0x0114
            r1 = r3
            double[] r0 = r3.A08     // Catch:{ all -> 0x019b }
            r10 = 6
            r6 = r0[r10]     // Catch:{ all -> 0x019b }
            r4 = 0
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x010a
            long r0 = r3.A04()     // Catch:{ all -> 0x019b }
            double[] r8 = r3.A08     // Catch:{ all -> 0x019b }
            double r6 = (double) r0     // Catch:{ all -> 0x019b }
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            java.lang.Double.isNaN(r6)     // Catch:{ all -> 0x019b }
            double r0 = r4 - r6
            double r4 = r4 / r0
            r8[r10] = r4     // Catch:{ all -> 0x019b }
        L_0x010a:
            double[] r0 = r3.A08     // Catch:{ all -> 0x019b }
            r0 = r0[r10]     // Catch:{ all -> 0x019b }
            double r13 = r13 * r0
            int r0 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r0 <= 0) goto L_0x013c
            goto L_0x013a
        L_0x0114:
            r1 = r3
            double[] r0 = r3.A08     // Catch:{ all -> 0x019b }
            r10 = 7
            r6 = r0[r10]     // Catch:{ all -> 0x019b }
            r4 = 0
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x0131
            long r0 = r3.A04()     // Catch:{ all -> 0x019b }
            double[] r8 = r3.A08     // Catch:{ all -> 0x019b }
            double r6 = (double) r0     // Catch:{ all -> 0x019b }
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            java.lang.Double.isNaN(r6)     // Catch:{ all -> 0x019b }
            double r0 = r4 - r6
            double r0 = r0 / r4
            r8[r10] = r0     // Catch:{ all -> 0x019b }
        L_0x0131:
            double[] r0 = r3.A08     // Catch:{ all -> 0x019b }
            r0 = r0[r10]     // Catch:{ all -> 0x019b }
            double r15 = r15 * r0
            int r0 = (r11 > r15 ? 1 : (r11 == r15 ? 0 : -1))
            if (r0 >= 0) goto L_0x013c
        L_0x013a:
            r0 = 1
            goto L_0x013d
        L_0x013c:
            r0 = 0
        L_0x013d:
            if (r0 == 0) goto L_0x0191
            r3.A09 = r9     // Catch:{ all -> 0x019b }
            r3.A04 = r2     // Catch:{ all -> 0x019b }
            java.util.concurrent.atomic.AtomicReference r1 = r3.A03     // Catch:{ all -> 0x019b }
            java.util.concurrent.atomic.AtomicReference r0 = r3.A05     // Catch:{ all -> 0x019b }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x019b }
            r1.set(r0)     // Catch:{ all -> 0x019b }
            r0 = r3
            java.util.List r1 = r3.A02     // Catch:{ all -> 0x019b }
            monitor-enter(r1)     // Catch:{ all -> 0x019b }
            java.util.List r0 = r3.A02     // Catch:{ all -> 0x0175 }
            java.util.ArrayList r0 = X.C04300To.A03(r0)     // Catch:{ all -> 0x0175 }
            monitor-exit(r1)     // Catch:{ all -> 0x0175 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x019b }
        L_0x015d:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x019b }
            if (r0 == 0) goto L_0x0191
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x019b }
            X.1hd r1 = (X.C30181hd) r1     // Catch:{ all -> 0x019b }
            java.util.concurrent.atomic.AtomicReference r0 = r3.A03     // Catch:{ all -> 0x019b }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x019b }
            X.1Vy r0 = (X.AnonymousClass1Vy) r0     // Catch:{ all -> 0x019b }
            r1.BPP(r0)     // Catch:{ all -> 0x019b }
            goto L_0x015d
        L_0x0175:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0175 }
            throw r0     // Catch:{ all -> 0x019b }
        L_0x0178:
            java.util.concurrent.atomic.AtomicReference r0 = r3.A03     // Catch:{ all -> 0x019b }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x019b }
            X.1Vy r0 = r3.A05()     // Catch:{ all -> 0x019b }
            if (r1 == r0) goto L_0x0191
            r3.A09 = r2     // Catch:{ all -> 0x019b }
            java.util.concurrent.atomic.AtomicReference r1 = new java.util.concurrent.atomic.AtomicReference     // Catch:{ all -> 0x019b }
            X.1Vy r0 = r3.A05()     // Catch:{ all -> 0x019b }
            r1.<init>(r0)     // Catch:{ all -> 0x019b }
            r3.A05 = r1     // Catch:{ all -> 0x019b }
        L_0x0191:
            monitor-exit(r3)
            return
        L_0x0193:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x019b }
            java.lang.String r0 = "timeInMs must be positive"
            r1.<init>(r0)     // Catch:{ all -> 0x019b }
            throw r1     // Catch:{ all -> 0x019b }
        L_0x019b:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1W1.A09(long, long):void");
    }

    private AnonymousClass1W1(C25051Yd r3, AnonymousClass069 r4) {
        this.A07 = r3;
        this.A06 = r4;
    }
}
