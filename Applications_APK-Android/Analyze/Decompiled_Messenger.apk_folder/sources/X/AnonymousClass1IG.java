package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1IG  reason: invalid class name */
public final class AnonymousClass1IG {
    public static final AnonymousClass1Y7 A06 = ((AnonymousClass1Y7) A08.A09("conn_tracking/"));
    public static final AnonymousClass1Y7 A07;
    private static final AnonymousClass1Y7 A08;
    private static volatile AnonymousClass1IG A09;
    public AnonymousClass0UN A00;
    public final C31131jC A01;
    public final ConcurrentMap A02 = AnonymousClass0TG.A07();
    public final AtomicBoolean A03 = new AtomicBoolean();
    public volatile double A04;
    public volatile double A05;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A06.A09("connection_manager/");
        A08 = r1;
        A07 = (AnonymousClass1Y7) r1.A09("history/");
    }

    public static final AnonymousClass1IG A00(AnonymousClass1XY r5) {
        if (A09 == null) {
            synchronized (AnonymousClass1IG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        new C23881Rh(applicationInjector);
                        A09 = new AnonymousClass1IG(applicationInjector);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static void A01(AnonymousClass1IG r12) {
        double d;
        double d2;
        ArrayList arrayList = new ArrayList();
        for (long j = 0; j < 48; j++) {
            AnonymousClass1Y7 r7 = (AnonymousClass1Y7) A06.A09(AnonymousClass08S.A06(Long.toString(j), '/'));
            int i = 0;
            while (true) {
                if (!((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r12.A00)).BBh((AnonymousClass1Y7) r7.A09(Integer.toString(i)))) {
                    break;
                }
                i++;
            }
            if (i == 0) {
                break;
            }
            long[] jArr = new long[i];
            int i2 = 0;
            while (true) {
                AnonymousClass1Y7 r8 = (AnonymousClass1Y7) r7.A09(Integer.toString(i2));
                int i3 = AnonymousClass1Y3.B6q;
                if (!((FbSharedPreferences) AnonymousClass1XX.A02(0, i3, r12.A00)).BBh(r8)) {
                    break;
                }
                jArr[i2] = ((FbSharedPreferences) AnonymousClass1XX.A02(0, i3, r12.A00)).At2(r8, 0);
                i2++;
            }
            arrayList.add(jArr);
        }
        long[] A002 = C31131jC.A00(arrayList);
        long j2 = A002[0];
        long j3 = A002[2];
        if (j2 == 0 && j3 == 0) {
            d = 0.0d;
        } else {
            d = ((double) j2) / ((double) (j3 + j2));
        }
        r12.A04 = d;
        long[] A003 = C31131jC.A00(arrayList);
        long j4 = A003[1];
        long j5 = A003[3];
        if (j4 == 0 && j5 == 0) {
            d2 = 0.0d;
        } else {
            d2 = ((double) j4) / ((double) (j4 + j5));
        }
        r12.A05 = d2;
    }

    public static void A02(AnonymousClass1IG r9, long[] jArr) {
        if (((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r9.A00)).BFQ()) {
            AnonymousClass1Y7 r7 = (AnonymousClass1Y7) A06.A09("index");
            long At2 = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r9.A00)).At2(r7, 0);
            long j = (1 + At2) % 48;
            AnonymousClass1Y7 r6 = (AnonymousClass1Y7) A06.A09(AnonymousClass08S.A06(Long.toString(At2), '/'));
            C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r9.A00)).edit();
            edit.C24(r6);
            for (int i = 0; i < jArr.length; i++) {
                edit.BzA((AnonymousClass1Y7) r6.A09(Integer.toString(i)), jArr[i]);
            }
            edit.BzA(r7, j);
            edit.commit();
            r9.A04 = -1.0d;
            r9.A05 = -1.0d;
        }
    }

    private AnonymousClass1IG(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(2, r4);
        this.A01 = new C31131jC(3600000);
        this.A04 = -1.0d;
        this.A05 = -1.0d;
    }
}
