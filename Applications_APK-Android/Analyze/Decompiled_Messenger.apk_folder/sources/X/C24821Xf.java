package X;

import android.app.Activity;
import android.app.Service;
import android.content.Context;

/* renamed from: X.1Xf  reason: invalid class name and case insensitive filesystem */
public final class C24821Xf extends C24831Xg implements C24851Xi, AnonymousClass062 {
    public AnonymousClass0UN A00;
    public boolean A01 = false;
    private final Context A02;

    public C05920aY B9T() {
        int i;
        AnonymousClass0UN r0;
        synchronized (this) {
            if (!this.A01) {
                this.A00 = new AnonymousClass0UN(0, AnonymousClass1XX.get(Aq3()));
            }
            this.A01 = true;
        }
        Activity activity = (Activity) AnonymousClass065.A00(Aq3(), Activity.class);
        Service service = (Service) AnonymousClass065.A00(Aq3(), Service.class);
        if (activity == null && service == null) {
            i = AnonymousClass1Y3.BJM;
            r0 = this.A00;
        } else {
            i = AnonymousClass1Y3.A6F;
            r0 = this.A00;
        }
        return (C05920aY) AnonymousClass1XX.A03(i, r0);
    }

    public void AZH(Object obj) {
        C24811Xe r1 = (C24811Xe) obj;
        r1.A03();
        r1.A02();
    }

    public Context Aq3() {
        return this.A02;
    }

    public C24821Xf(AnonymousClass1XX r2, Context context) {
        super(r2);
        this.A02 = context;
    }

    public /* bridge */ /* synthetic */ Object AYf() {
        C24811Xe injectorThreadStack = getInjectorThreadStack();
        injectorThreadStack.A01.add(Aq3());
        injectorThreadStack.A02.add(this);
        return injectorThreadStack;
    }
}
