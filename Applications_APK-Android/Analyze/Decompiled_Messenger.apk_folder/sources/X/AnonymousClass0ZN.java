package X;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import java.util.ArrayList;

/* renamed from: X.0ZN  reason: invalid class name */
public final class AnonymousClass0ZN {
    public Bitmap A00;
    public Bundle A01;
    public C06570bi A02;
    public CharSequence A03;
    public CharSequence A04;
    public ArrayList A05 = new ArrayList();
    public boolean A06;
    public int A07 = 0;
    public int A08;
    public int A09;
    public long A0A;
    public Notification A0B;
    public PendingIntent A0C;
    public PendingIntent A0D;
    public Context A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public ArrayList A0I = new ArrayList();
    public ArrayList A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M = false;
    public boolean A0N = true;
    public boolean A0O;

    public static CharSequence A00(CharSequence charSequence) {
        if (charSequence == null || charSequence.length() <= 5120) {
            return charSequence;
        }
        return charSequence.subSequence(0, AnonymousClass1Y3.Age);
    }

    public static void A01(AnonymousClass0ZN r2, int i, boolean z) {
        if (z) {
            Notification notification = r2.A0B;
            notification.flags = i | notification.flags;
            return;
        }
        Notification notification2 = r2.A0B;
        notification2.flags = (i ^ -1) & notification2.flags;
    }

    public Notification A02() {
        return new C71293c9(this).A01();
    }

    public Bundle A03() {
        if (this.A01 == null) {
            this.A01 = new Bundle();
        }
        return this.A01;
    }

    public void A04(int i) {
        Notification notification = this.A0B;
        notification.defaults = i;
        if ((i & 4) != 0) {
            notification.flags |= 1;
        }
    }

    public void A05(int i) {
        this.A0B.icon = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (r6 == 0) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(int r4, int r5, int r6) {
        /*
            r3 = this;
            android.app.Notification r2 = r3.A0B
            r2.ledARGB = r4
            r2.ledOnMS = r5
            r2.ledOffMS = r6
            if (r5 == 0) goto L_0x000d
            r1 = 1
            if (r6 != 0) goto L_0x000e
        L_0x000d:
            r1 = 0
        L_0x000e:
            int r0 = r2.flags
            r0 = r0 & -2
            r1 = r1 | r0
            r2.flags = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0ZN.A06(int, int, int):void");
    }

    public void A07(int i, CharSequence charSequence, PendingIntent pendingIntent) {
        this.A0I.add(new C06010ah(i, charSequence, pendingIntent));
    }

    public void A08(Bitmap bitmap) {
        if (bitmap != null && Build.VERSION.SDK_INT < 27) {
            Resources resources = this.A0E.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(2132148242);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(2132148242);
            if (bitmap.getWidth() > dimensionPixelSize || bitmap.getHeight() > dimensionPixelSize2) {
                double min = Math.min(((double) dimensionPixelSize) / ((double) Math.max(1, bitmap.getWidth())), ((double) dimensionPixelSize2) / ((double) Math.max(1, bitmap.getHeight())));
                bitmap = Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * min), (int) Math.ceil(((double) bitmap.getHeight()) * min), true);
            }
        }
        this.A00 = bitmap;
    }

    public void A09(Uri uri) {
        Notification notification = this.A0B;
        notification.sound = uri;
        notification.audioStreamType = -1;
        if (Build.VERSION.SDK_INT >= 21) {
            notification.audioAttributes = new AudioAttributes.Builder().setContentType(4).setUsage(5).build();
        }
    }

    public void A0A(C06570bi r2) {
        if (this.A02 != r2) {
            this.A02 = r2;
            if (r2 != null && r2.A00 != this) {
                r2.A00 = this;
                if (this != null) {
                    A0A(r2);
                }
            }
        }
    }

    public void A0D(CharSequence charSequence) {
        this.A0B.tickerText = A00(charSequence);
    }

    public void A0E(boolean z) {
        A01(this, 16, z);
    }

    public AnonymousClass0ZN(Context context, String str) {
        Notification notification = new Notification();
        this.A0B = notification;
        this.A0E = context;
        this.A0G = str;
        notification.when = System.currentTimeMillis();
        notification.audioStreamType = -1;
        this.A09 = 0;
        this.A0J = new ArrayList();
        this.A06 = true;
    }

    public void A0B(CharSequence charSequence) {
        this.A03 = A00(charSequence);
    }

    public void A0C(CharSequence charSequence) {
        this.A04 = A00(charSequence);
    }
}
