package X;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.BackStackState;

/* renamed from: X.0Eh  reason: invalid class name */
public final class AnonymousClass0Eh implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new BackStackState(parcel);
    }

    public Object[] newArray(int i) {
        return new BackStackState[i];
    }
}
