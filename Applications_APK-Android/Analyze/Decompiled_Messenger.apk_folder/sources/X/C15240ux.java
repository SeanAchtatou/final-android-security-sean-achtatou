package X;

import android.app.AppOpsManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0ux  reason: invalid class name and case insensitive filesystem */
public final class C15240ux {
    public static String A02;
    public static Set A03 = new HashSet();
    private static C15250uy A04;
    public static final Object A05 = new Object();
    private static final Object A06 = new Object();
    private final NotificationManager A00;
    private final Context A01;

    public void A01(int i, Notification notification) {
        A03(null, i, notification);
    }

    private void A00(C32461ln r4) {
        synchronized (A06) {
            if (A04 == null) {
                A04 = new C15250uy(this.A01.getApplicationContext());
            }
            A04.A02.obtainMessage(0, r4).sendToTarget();
        }
    }

    public void A02(String str, int i) {
        this.A00.cancel(str, i);
        if (Build.VERSION.SDK_INT <= 19) {
            A00(new C15260uz(this.A01.getPackageName(), i, str));
        }
    }

    public boolean A04() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            return this.A00.areNotificationsEnabled();
        }
        if (i < 19) {
            return true;
        }
        AppOpsManager appOpsManager = (AppOpsManager) this.A01.getSystemService("appops");
        ApplicationInfo applicationInfo = this.A01.getApplicationInfo();
        String packageName = this.A01.getApplicationContext().getPackageName();
        int i2 = applicationInfo.uid;
        try {
            Class<?> cls = Class.forName(AppOpsManager.class.getName());
            Class cls2 = Integer.TYPE;
            if (((Integer) cls.getMethod("checkOpNoThrow", cls2, cls2, String.class).invoke(appOpsManager, Integer.valueOf(((Integer) cls.getDeclaredField("OP_POST_NOTIFICATION").get(Integer.class)).intValue()), Integer.valueOf(i2), packageName)).intValue() != 0) {
                return false;
            }
            return true;
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | RuntimeException | InvocationTargetException unused) {
            return true;
        }
    }

    public C15240ux(Context context) {
        this.A01 = context;
        this.A00 = (NotificationManager) context.getSystemService("notification");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        if (r1.getBoolean("android.support.useSideChannel") == false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(java.lang.String r3, int r4, android.app.Notification r5) {
        /*
            r2 = this;
            android.os.Bundle r1 = X.C73503gE.A00(r5)
            if (r1 == 0) goto L_0x000f
            java.lang.String r0 = "android.support.useSideChannel"
            boolean r1 = r1.getBoolean(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 == 0) goto L_0x0026
            X.1FK r1 = new X.1FK
            android.content.Context r0 = r2.A01
            java.lang.String r0 = r0.getPackageName()
            r1.<init>(r0, r4, r3, r5)
            r2.A00(r1)
            android.app.NotificationManager r0 = r2.A00
            r0.cancel(r3, r4)
            return
        L_0x0026:
            android.app.NotificationManager r0 = r2.A00
            r0.notify(r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15240ux.A03(java.lang.String, int, android.app.Notification):void");
    }
}
