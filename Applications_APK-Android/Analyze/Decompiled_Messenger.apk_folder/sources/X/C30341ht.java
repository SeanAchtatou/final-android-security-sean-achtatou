package X;

import android.content.Context;

/* renamed from: X.1ht  reason: invalid class name and case insensitive filesystem */
public final class C30341ht {
    public final C30371hw A00;

    public static C30341ht A00(String str, Context context) {
        C30371hw r1 = null;
        if (0 == 0) {
            r1 = (C30371hw) C27271cv.A00("com_facebook_messenger_msys_plugins_interfaces_messengermsys_MessengerSecureMessageInterfaceSpec", str, context, new Object[0]);
        }
        return new C30341ht(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x007d, code lost:
        if (((X.C36731td) X.AnonymousClass1XX.A02(4, X.AnonymousClass1Y3.BJH, r4.$ul_mInjectionContext)).A00.Aem(283326108207434L) == false) goto L_0x007f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(java.lang.String r7) {
        /*
            r6 = this;
            X.1hw r0 = r6.A00
            X.1hz r3 = r0.A00
            java.util.concurrent.atomic.AtomicInteger r0 = X.C27271cv.A02
            r0.getAndIncrement()
            X.1d0 r1 = r3.A03
            java.lang.String r5 = "isEnabled"
            r0 = 31
            java.lang.String r4 = X.C22298Ase.$const$string(r0)
            r1.A05(r4, r5)
            X.C30401hz.A00(r3)     // Catch:{ all -> 0x009b }
            r0 = 5
            java.lang.String r0 = X.C22298Ase.$const$string(r0)     // Catch:{ all -> 0x009b }
            boolean r0 = r7.equals(r0)     // Catch:{ all -> 0x009b }
            r2 = 0
            if (r0 == 0) goto L_0x0094
            boolean r0 = X.C30401hz.A01(r3)     // Catch:{ all -> 0x009b }
            if (r0 == 0) goto L_0x0094
            java.util.concurrent.atomic.AtomicInteger r0 = X.C27271cv.A02     // Catch:{ all -> 0x009b }
            r0.getAndIncrement()     // Catch:{ all -> 0x009b }
            X.1d0 r1 = r3.A03     // Catch:{ all -> 0x009b }
            r0 = 30
            java.lang.String r0 = X.C22298Ase.$const$string(r0)     // Catch:{ all -> 0x009b }
            r1.A07(r0, r4, r5)     // Catch:{ all -> 0x009b }
            int r1 = X.AnonymousClass1Y3.BRc     // Catch:{ Exception -> 0x008b }
            X.0UN r0 = r3.A00     // Catch:{ Exception -> 0x008b }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ Exception -> 0x008b }
            com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage r4 = (com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage) r4     // Catch:{ Exception -> 0x008b }
            int r2 = X.AnonymousClass1Y3.AYQ     // Catch:{ Exception -> 0x008b }
            X.0UN r1 = r4.$ul_mInjectionContext     // Catch:{ Exception -> 0x008b }
            r0 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ Exception -> 0x008b }
            X.1d4 r0 = (X.C27361d4) r0     // Catch:{ Exception -> 0x008b }
            int r2 = X.AnonymousClass1Y3.AOJ     // Catch:{ Exception -> 0x008b }
            X.0UN r1 = r0.A00     // Catch:{ Exception -> 0x008b }
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ Exception -> 0x008b }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ Exception -> 0x008b }
            r0 = 282853661214636(0x10141000007ac, double:1.397482768065703E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ Exception -> 0x008b }
            if (r0 == 0) goto L_0x007f
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BJH     // Catch:{ Exception -> 0x008b }
            X.0UN r0 = r4.$ul_mInjectionContext     // Catch:{ Exception -> 0x008b }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ Exception -> 0x008b }
            X.1td r0 = (X.C36731td) r0     // Catch:{ Exception -> 0x008b }
            X.1Yd r2 = r0.A00     // Catch:{ Exception -> 0x008b }
            r0 = 283326108207434(0x101af0009094a, double:1.39981696635193E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ Exception -> 0x008b }
            r1 = 1
            if (r0 != 0) goto L_0x0080
        L_0x007f:
            r1 = 0
        L_0x0080:
            X.1d0 r0 = r3.A03     // Catch:{ all -> 0x009b }
            r0.A00()     // Catch:{ all -> 0x009b }
            X.1d0 r0 = r3.A03
            r0.A01()
            return r1
        L_0x008b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x008d }
        L_0x008d:
            r1 = move-exception
            X.1d0 r0 = r3.A03     // Catch:{ all -> 0x009b }
            r0.A00()     // Catch:{ all -> 0x009b }
            throw r1     // Catch:{ all -> 0x009b }
        L_0x0094:
            X.1d0 r0 = r3.A03
            r0.A01()
            r1 = 0
            return r1
        L_0x009b:
            r1 = move-exception
            X.1d0 r0 = r3.A03
            r0.A01()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30341ht.A01(java.lang.String):boolean");
    }

    private C30341ht(C30371hw r1) {
        this.A00 = r1;
    }
}
