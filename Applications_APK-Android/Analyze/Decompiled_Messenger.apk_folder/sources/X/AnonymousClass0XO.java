package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0XO  reason: invalid class name */
public final class AnonymousClass0XO {
    private static volatile AnonymousClass0XO A04;
    public C05770aI A00;
    public final AnonymousClass0XQ A01;
    public final C05770aI A02;
    public final AnonymousClass0US A03;

    public static final AnonymousClass0XO A00(AnonymousClass1XY r6) {
        if (A04 == null) {
            synchronized (AnonymousClass0XO.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A04 = new AnonymousClass0XO(AnonymousClass0XP.A00(applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.B6q, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static void A01(AnonymousClass0XO r1) {
        C30281hn edit = ((FbSharedPreferences) r1.A03.get()).edit();
        edit.C1B(C10730kl.A0C);
        edit.C1B(C10730kl.A0B);
        edit.C1B(C10730kl.A08);
        edit.C1B(C10730kl.A0A);
        edit.C1B(C10730kl.A09);
        edit.C1B(C10730kl.A0D);
        edit.commit();
    }

    private AnonymousClass0XO(AnonymousClass0XQ r2, AnonymousClass0US r3) {
        this.A01 = r2;
        this.A02 = r2.A00("authentication");
        this.A03 = r3;
    }
}
