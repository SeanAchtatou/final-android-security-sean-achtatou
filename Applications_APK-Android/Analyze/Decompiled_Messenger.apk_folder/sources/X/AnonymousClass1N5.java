package X;

import com.facebook.common.util.TriState;

/* renamed from: X.1N5  reason: invalid class name */
public final class AnonymousClass1N5 implements AnonymousClass1N3 {
    private final AnonymousClass1N3 A00;
    private final AnonymousClass1N1 A01;

    public void AMh(C11670nb r3) {
        if (this.A01.A00() == TriState.YES) {
            this.A00.AMh(r3);
        }
    }

    public void BO0(int i) {
        if (this.A01.A00() == TriState.YES) {
            this.A00.BO0(i);
        }
    }

    public void BZs(int i) {
        if (this.A01.A00() == TriState.YES) {
            this.A00.BZs(i);
        }
    }

    public void BaY() {
        if (this.A01.A00() == TriState.YES) {
            this.A00.BaY();
        }
    }

    public void Bp0() {
        if (this.A01.A00() == TriState.YES) {
            this.A00.Bp0();
        }
    }

    public void BuA(int i) {
        if (this.A01.A00() == TriState.YES) {
            this.A00.BuA(i);
        }
    }

    public void BuD(int i) {
        if (this.A01.A00() == TriState.YES) {
            this.A00.BuA(i);
        }
    }

    public void C6L(C22871Nd r2) {
        this.A00.C6L(r2);
    }

    public AnonymousClass1N5(AnonymousClass1N1 r1, AnonymousClass1N3 r2) {
        this.A01 = r1;
        this.A00 = r2;
    }
}
