package X;

import android.graphics.Typeface;
import java.io.File;

/* renamed from: X.1bp  reason: invalid class name and case insensitive filesystem */
public final class C26671bp {
    public final Typeface A00;
    public final File A01;

    public C26671bp(File file, Typeface typeface) {
        this.A01 = file;
        this.A00 = typeface;
    }
}
