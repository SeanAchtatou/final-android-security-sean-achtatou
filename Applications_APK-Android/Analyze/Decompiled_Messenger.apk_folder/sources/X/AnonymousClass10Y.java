package X;

import android.util.Pair;
import java.util.Comparator;

/* renamed from: X.10Y  reason: invalid class name */
public final class AnonymousClass10Y implements Comparator {
    public int compare(Object obj, Object obj2) {
        int BwJ = ((C34181or) ((Pair) obj).second).BwJ();
        int BwJ2 = ((C34181or) ((Pair) obj2).second).BwJ();
        if (BwJ != BwJ2) {
            return BwJ - BwJ2;
        }
        throw new IllegalStateException("Two plugins with the same ordering provided");
    }
}
