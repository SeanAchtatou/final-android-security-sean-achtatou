package X;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;

/* renamed from: X.0Xg  reason: invalid class name and case insensitive filesystem */
public final class C05000Xg {
    public final C05010Xh A00 = new C05010Xh();
    public final Executor A01;

    public void A00(AnonymousClass0YI r3, int i) {
        this.A00.A01(Integer.valueOf(i), r3);
    }

    public void A01(AnonymousClass0YI r6, int... iArr) {
        C05010Xh r4 = this.A00;
        HashSet hashSet = new HashSet(r2);
        for (int valueOf : iArr) {
            hashSet.add(Integer.valueOf(valueOf));
        }
        synchronized (r4) {
            boolean z = false;
            if (r6 != null) {
                z = true;
            }
            AnonymousClass064.A03(z);
            Object obj = (Set) r4.A00.get(r6);
            if (obj == null) {
                obj = new HashSet(hashSet);
            } else {
                obj.addAll(hashSet);
            }
            r4.A00.put(r6, obj);
        }
    }

    public C05000Xg(Executor executor) {
        this.A01 = executor;
    }
}
