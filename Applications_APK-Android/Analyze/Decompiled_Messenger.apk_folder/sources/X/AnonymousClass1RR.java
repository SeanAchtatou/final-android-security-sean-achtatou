package X;

import java.io.InputStream;

/* renamed from: X.1RR  reason: invalid class name */
public final class AnonymousClass1RR extends AnonymousClass1RQ {
    private int A00 = 0;
    public final C23301Oz A01;
    public final AnonymousClass1RP A02;

    public AnonymousClass1RR(AnonymousClass1QC r7, C23581Qb r8, AnonymousClass1QK r9, AnonymousClass1RP r10, C23301Oz r11, boolean z, int i) {
        super(r7, r8, r9, z, i);
        C05520Zg.A02(r10);
        this.A02 = r10;
        C05520Zg.A02(r11);
        this.A01 = r11;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:117:0x013d, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        if (r0 != false) goto L_0x0019;
     */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0132 A[Catch:{ IOException -> 0x0111, all -> 0x0119 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0A(X.AnonymousClass1NY r15, int r16) {
        /*
            r14 = this;
            r3 = r16
            monitor-enter(r14)
            boolean r13 = super.A0A(r15, r3)     // Catch:{ all -> 0x0140 }
            boolean r0 = X.AnonymousClass1QU.A03(r3)     // Catch:{ all -> 0x0140 }
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x0019
            r2 = 8
            r1 = r16 & r2
            r0 = 0
            if (r1 != r2) goto L_0x0017
            r0 = 1
        L_0x0017:
            if (r0 == 0) goto L_0x013e
        L_0x0019:
            r9 = 4
            r3 = r16 & r9
            r0 = 0
            if (r3 != r9) goto L_0x0020
            r0 = 1
        L_0x0020:
            if (r0 != 0) goto L_0x013e
            boolean r0 = X.AnonymousClass1NY.A07(r15)     // Catch:{ all -> 0x0140 }
            if (r0 == 0) goto L_0x013e
            X.AnonymousClass1NY.A05(r15)     // Catch:{ all -> 0x0140 }
            X.1O3 r1 = r15.A07     // Catch:{ all -> 0x0140 }
            X.1O3 r0 = X.AnonymousClass1SI.A06     // Catch:{ all -> 0x0140 }
            if (r1 != r0) goto L_0x013e
            X.1RP r7 = r14.A02     // Catch:{ all -> 0x0140 }
            int r1 = r7.A05     // Catch:{ all -> 0x0140 }
            r0 = 6
            if (r1 == r0) goto L_0x011e
            int r1 = r15.A08()     // Catch:{ all -> 0x0140 }
            int r0 = r7.A02     // Catch:{ all -> 0x0140 }
            if (r1 <= r0) goto L_0x011e
            X.8ci r4 = new X.8ci     // Catch:{ all -> 0x0140 }
            java.io.InputStream r2 = r15.A09()     // Catch:{ all -> 0x0140 }
            X.1Nc r1 = r7.A07     // Catch:{ all -> 0x0140 }
            r0 = 16384(0x4000, float:2.2959E-41)
            java.lang.Object r1 = r1.get(r0)     // Catch:{ all -> 0x0140 }
            byte[] r1 = (byte[]) r1     // Catch:{ all -> 0x0140 }
            X.1Nc r0 = r7.A07     // Catch:{ all -> 0x0140 }
            r4.<init>(r2, r1, r0)     // Catch:{ all -> 0x0140 }
            int r0 = r7.A02     // Catch:{ IOException -> 0x0111 }
            long r0 = (long) r0     // Catch:{ IOException -> 0x0111 }
            A02(r4, r0)     // Catch:{ IOException -> 0x0111 }
            int r6 = r7.A01     // Catch:{ IOException -> 0x0111 }
        L_0x005d:
            r5 = 0
            r12 = 1
            r8 = 6
            int r0 = r7.A05     // Catch:{ IOException -> 0x0100 }
            if (r0 == r8) goto L_0x0104
            int r10 = r4.read()     // Catch:{ IOException -> 0x0100 }
            r0 = -1
            if (r10 == r0) goto L_0x0104
            int r2 = r7.A02     // Catch:{ IOException -> 0x0100 }
            int r2 = r2 + r12
            r7.A02 = r2     // Catch:{ IOException -> 0x0100 }
            boolean r0 = r7.A06     // Catch:{ IOException -> 0x0100 }
            if (r0 == 0) goto L_0x007a
            r7.A05 = r8     // Catch:{ IOException -> 0x0100 }
            r7.A06 = r5     // Catch:{ IOException -> 0x0100 }
            goto L_0x00fe
        L_0x007a:
            int r11 = r7.A05     // Catch:{ IOException -> 0x0100 }
            r1 = 255(0xff, float:3.57E-43)
            if (r11 == 0) goto L_0x00ef
            r3 = 2
            if (r11 == r12) goto L_0x00d8
            r0 = 3
            if (r11 == r3) goto L_0x00d3
            if (r11 == r0) goto L_0x009e
            r0 = 5
            if (r11 == r9) goto L_0x00d5
            if (r11 == r0) goto L_0x008e
            goto L_0x00f7
        L_0x008e:
            int r0 = r7.A03     // Catch:{ IOException -> 0x0100 }
            int r2 = r0 << 8
            int r2 = r2 + r10
            int r2 = r2 - r3
            long r0 = (long) r2     // Catch:{ IOException -> 0x0100 }
            A02(r4, r0)     // Catch:{ IOException -> 0x0100 }
            int r0 = r7.A02     // Catch:{ IOException -> 0x0100 }
            int r0 = r0 + r2
            r7.A02 = r0     // Catch:{ IOException -> 0x0100 }
            goto L_0x00ec
        L_0x009e:
            if (r10 == r1) goto L_0x00d5
            if (r10 == 0) goto L_0x00ec
            r0 = 217(0xd9, float:3.04E-43)
            if (r10 != r0) goto L_0x00a7
            goto L_0x00dd
        L_0x00a7:
            r0 = 218(0xda, float:3.05E-43)
            if (r10 != r0) goto L_0x00b8
            int r2 = r2 - r3
            int r1 = r7.A04     // Catch:{ IOException -> 0x0100 }
            if (r1 <= 0) goto L_0x00b2
            r7.A00 = r2     // Catch:{ IOException -> 0x0100 }
        L_0x00b2:
            int r0 = r1 + 1
            r7.A04 = r0     // Catch:{ IOException -> 0x0100 }
            r7.A01 = r1     // Catch:{ IOException -> 0x0100 }
        L_0x00b8:
            r1 = 1
            if (r10 == r1) goto L_0x00c3
            r0 = 208(0xd0, float:2.91E-43)
            if (r10 < r0) goto L_0x00c7
            r0 = 215(0xd7, float:3.01E-43)
            if (r10 > r0) goto L_0x00c7
        L_0x00c3:
            r1 = 0
        L_0x00c4:
            if (r1 == 0) goto L_0x00ec
            goto L_0x00d0
        L_0x00c7:
            r0 = 217(0xd9, float:3.04E-43)
            if (r10 == r0) goto L_0x00c3
            r0 = 216(0xd8, float:3.03E-43)
            if (r10 == r0) goto L_0x00c3
            goto L_0x00c4
        L_0x00d0:
            r7.A05 = r9     // Catch:{ IOException -> 0x0100 }
            goto L_0x00fa
        L_0x00d3:
            if (r10 != r1) goto L_0x00fa
        L_0x00d5:
            r7.A05 = r0     // Catch:{ IOException -> 0x0100 }
            goto L_0x00fa
        L_0x00d8:
            r0 = 216(0xd8, float:3.03E-43)
            if (r10 != r0) goto L_0x00f4
            goto L_0x00ec
        L_0x00dd:
            r7.A06 = r12     // Catch:{ IOException -> 0x0100 }
            int r2 = r2 - r3
            int r1 = r7.A04     // Catch:{ IOException -> 0x0100 }
            if (r1 <= 0) goto L_0x00e6
            r7.A00 = r2     // Catch:{ IOException -> 0x0100 }
        L_0x00e6:
            int r0 = r1 + 1
            r7.A04 = r0     // Catch:{ IOException -> 0x0100 }
            r7.A01 = r1     // Catch:{ IOException -> 0x0100 }
        L_0x00ec:
            r7.A05 = r3     // Catch:{ IOException -> 0x0100 }
            goto L_0x00fa
        L_0x00ef:
            if (r10 != r1) goto L_0x00f4
            r7.A05 = r12     // Catch:{ IOException -> 0x0100 }
            goto L_0x00fa
        L_0x00f4:
            r7.A05 = r8     // Catch:{ IOException -> 0x0100 }
            goto L_0x00fa
        L_0x00f7:
            X.C05520Zg.A05(r5)     // Catch:{ IOException -> 0x0100 }
        L_0x00fa:
            r7.A03 = r10     // Catch:{ IOException -> 0x0100 }
            goto L_0x005d
        L_0x00fe:
            r5 = 0
            goto L_0x010d
        L_0x0100:
            r0 = move-exception
            X.AnonymousClass95E.A00(r0)     // Catch:{ IOException -> 0x0111 }
        L_0x0104:
            int r0 = r7.A05     // Catch:{ IOException -> 0x0111 }
            if (r0 == r8) goto L_0x010d
            int r0 = r7.A01     // Catch:{ IOException -> 0x0111 }
            if (r0 == r6) goto L_0x010d
            r5 = 1
        L_0x010d:
            X.AnonymousClass2R2.A01(r4)     // Catch:{ all -> 0x0140 }
            goto L_0x011f
        L_0x0111:
            r0 = move-exception
            X.AnonymousClass95E.A00(r0)     // Catch:{ all -> 0x0119 }
            X.AnonymousClass2R2.A01(r4)     // Catch:{ all -> 0x0140 }
            goto L_0x011e
        L_0x0119:
            r0 = move-exception
            X.AnonymousClass2R2.A01(r4)     // Catch:{ all -> 0x0140 }
            throw r0     // Catch:{ all -> 0x0140 }
        L_0x011e:
            r5 = 0
        L_0x011f:
            r3 = 0
            if (r5 == 0) goto L_0x013c
            X.1RP r0 = r14.A02     // Catch:{ all -> 0x0140 }
            int r2 = r0.A01     // Catch:{ all -> 0x0140 }
            int r1 = r14.A00     // Catch:{ all -> 0x0140 }
            if (r2 <= r1) goto L_0x013c
            X.1Oz r0 = r14.A01     // Catch:{ all -> 0x0140 }
            int r0 = r0.Avn(r1)     // Catch:{ all -> 0x0140 }
            if (r2 >= r0) goto L_0x0139
            X.1RP r0 = r14.A02     // Catch:{ all -> 0x0140 }
            boolean r0 = r0.A06     // Catch:{ all -> 0x0140 }
            if (r0 != 0) goto L_0x0139
            goto L_0x013c
        L_0x0139:
            r14.A00 = r2     // Catch:{ all -> 0x0140 }
            goto L_0x013e
        L_0x013c:
            monitor-exit(r14)
            return r3
        L_0x013e:
            monitor-exit(r14)
            return r13
        L_0x0140:
            r0 = move-exception
            monitor-exit(r14)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1RR.A0A(X.1NY, int):boolean");
    }

    public static void A02(InputStream inputStream, long j) {
        C05520Zg.A02(inputStream);
        boolean z = false;
        if (j >= 0) {
            z = true;
        }
        C05520Zg.A04(z);
        while (j > 0) {
            long skip = inputStream.skip(j);
            if (skip <= 0) {
                if (inputStream.read() != -1) {
                    skip = 1;
                } else {
                    return;
                }
            }
            j -= skip;
        }
    }
}
