package X;

/* renamed from: X.1gV  reason: invalid class name and case insensitive filesystem */
public final class C29491gV {
    public static final String MAX_LONG_STR = "9223372036854775807";
    public static final String MIN_LONG_STR_NO_SIGN = "-9223372036854775808".substring(1);

    public static int parseAsInt(String str, int i) {
        String trim;
        int length;
        if (str == null || (length = (trim = str.trim()).length()) == 0) {
            return i;
        }
        int i2 = 0;
        if (length > 0) {
            char charAt = trim.charAt(0);
            if (charAt == '+') {
                trim = trim.substring(1);
                length = trim.length();
            } else if (charAt == '-') {
                i2 = 1;
            }
        }
        while (i2 < length) {
            char charAt2 = trim.charAt(i2);
            if (charAt2 > '9' || charAt2 < '0') {
                try {
                    return (int) parseDouble(trim);
                } catch (NumberFormatException unused) {
                    return i;
                }
            } else {
                i2++;
            }
        }
        return Integer.parseInt(trim);
    }

    public static long parseAsLong(String str, long j) {
        String trim;
        int length;
        if (str == null || (length = (trim = str.trim()).length()) == 0) {
            return j;
        }
        int i = 0;
        if (length > 0) {
            char charAt = trim.charAt(0);
            if (charAt == '+') {
                trim = trim.substring(1);
                length = trim.length();
            } else if (charAt == '-') {
                i = 1;
            }
        }
        while (i < length) {
            char charAt2 = trim.charAt(i);
            if (charAt2 > '9' || charAt2 < '0') {
                try {
                    return (long) parseDouble(trim);
                } catch (NumberFormatException unused) {
                    return j;
                }
            } else {
                i++;
            }
        }
        return Long.parseLong(trim);
    }

    public static double parseDouble(String str) {
        if ("2.2250738585072012e-308".equals(str)) {
            return Double.MIN_VALUE;
        }
        return Double.parseDouble(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005c, code lost:
        if (r6 > 9) goto L_0x005e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int parseInt(java.lang.String r8) {
        /*
            r7 = 0
            char r5 = r8.charAt(r7)
            int r6 = r8.length()
            r2 = 1
            r0 = 45
            if (r5 != r0) goto L_0x000f
            r7 = 1
        L_0x000f:
            r0 = 10
            if (r7 == 0) goto L_0x005a
            if (r6 == r2) goto L_0x005e
            if (r6 > r0) goto L_0x005e
            char r5 = r8.charAt(r2)
            r2 = 2
        L_0x001c:
            r4 = 57
            if (r5 > r4) goto L_0x005e
            r3 = 48
            if (r5 < r3) goto L_0x005e
            int r5 = r5 - r3
            if (r2 >= r6) goto L_0x0063
            int r1 = r2 + 1
            char r0 = r8.charAt(r2)
            if (r0 > r4) goto L_0x005e
            if (r0 < r3) goto L_0x005e
            int r5 = r5 * 10
            int r0 = r0 - r3
            int r5 = r5 + r0
            if (r1 >= r6) goto L_0x0063
            int r2 = r1 + 1
            char r0 = r8.charAt(r1)
            if (r0 > r4) goto L_0x005e
            if (r0 < r3) goto L_0x005e
            int r5 = r5 * 10
            int r0 = r0 - r3
            int r5 = r5 + r0
            if (r2 >= r6) goto L_0x0063
        L_0x0047:
            int r1 = r2 + 1
            char r0 = r8.charAt(r2)
            if (r0 > r4) goto L_0x005e
            if (r0 < r3) goto L_0x005e
            int r5 = r5 * 10
            int r0 = r0 + -48
            int r5 = r5 + r0
            if (r1 >= r6) goto L_0x0063
            r2 = r1
            goto L_0x0047
        L_0x005a:
            r0 = 9
            if (r6 <= r0) goto L_0x001c
        L_0x005e:
            int r0 = java.lang.Integer.parseInt(r8)
            return r0
        L_0x0063:
            if (r7 == 0) goto L_0x0066
            int r5 = -r5
        L_0x0066:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29491gV.parseInt(java.lang.String):int");
    }

    public static int parseInt(char[] cArr, int i, int i2) {
        int i3 = cArr[i] - '0';
        int i4 = i2 + i;
        int i5 = i + 1;
        if (i5 >= i4) {
            return i3;
        }
        int i6 = (i3 * 10) + (cArr[i5] - '0');
        int i7 = i5 + 1;
        if (i7 >= i4) {
            return i6;
        }
        int i8 = (i6 * 10) + (cArr[i7] - '0');
        int i9 = i7 + 1;
        if (i9 >= i4) {
            return i8;
        }
        int i10 = (i8 * 10) + (cArr[i9] - '0');
        int i11 = i9 + 1;
        if (i11 >= i4) {
            return i10;
        }
        int i12 = (i10 * 10) + (cArr[i11] - '0');
        int i13 = i11 + 1;
        if (i13 >= i4) {
            return i12;
        }
        int i14 = (i12 * 10) + (cArr[i13] - '0');
        int i15 = i13 + 1;
        if (i15 >= i4) {
            return i14;
        }
        int i16 = (i14 * 10) + (cArr[i15] - '0');
        int i17 = i15 + 1;
        if (i17 >= i4) {
            return i16;
        }
        int i18 = (i16 * 10) + (cArr[i17] - '0');
        int i19 = i17 + 1;
        return i19 < i4 ? (i18 * 10) + (cArr[i19] - '0') : i18;
    }
}
