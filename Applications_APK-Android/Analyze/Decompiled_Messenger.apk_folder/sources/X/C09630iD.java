package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.0iD  reason: invalid class name and case insensitive filesystem */
public final class C09630iD implements AnonymousClass06U {
    public final /* synthetic */ QuickPerformanceLogger A00;

    public C09630iD(QuickPerformanceLogger quickPerformanceLogger) {
        this.A00 = quickPerformanceLogger;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(795192410);
        this.A00.endAllMarkers(630, true);
        this.A00.markEvent(46333953, "afterEndAllMarkers", 7);
        AnonymousClass09Y.A01(-1736346466, A002);
    }
}
