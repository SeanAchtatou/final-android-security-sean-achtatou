package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Ty  reason: invalid class name and case insensitive filesystem */
public final class C24521Ty {
    private static volatile C24521Ty A02;
    public AnonymousClass0UN A00;
    public final C04310Tq A01;

    public static final C24521Ty A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C24521Ty.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C24521Ty(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C24521Ty(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(7, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.AOJ, r3);
    }
}
