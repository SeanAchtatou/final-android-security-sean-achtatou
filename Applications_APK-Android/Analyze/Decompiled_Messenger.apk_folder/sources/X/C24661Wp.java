package X;

/* renamed from: X.1Wp  reason: invalid class name and case insensitive filesystem */
public final /* synthetic */ class C24661Wp implements AnonymousClass1WW {
    private final AnonymousClass1WX A00;
    private final AnonymousClass1WT A01;

    public C24661Wp(AnonymousClass1WT r1, AnonymousClass1WX r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public Object get() {
        AnonymousClass1WT r3 = this.A01;
        AnonymousClass1WX r2 = this.A00;
        return r2.A02.AUm(new C24681Wr(r2, r3));
    }
}
