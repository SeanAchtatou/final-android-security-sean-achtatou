package X;

import java.util.Set;

/* renamed from: X.1WU  reason: invalid class name */
public abstract class AnonymousClass1WU {
    public AnonymousClass1WW A00(Class cls) {
        if (!(this instanceof C24681Wr)) {
            AnonymousClass09E.A02(cls, "Null interface requested.");
            return (AnonymousClass1WW) ((AnonymousClass1WT) this).A02.get(cls);
        }
        C24681Wr r1 = (C24681Wr) this;
        if (r1.A02.contains(cls)) {
            return r1.A00.A00(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<%s>.", cls));
    }

    public AnonymousClass1WW A01(Class cls) {
        if (!(this instanceof C24681Wr)) {
            C24651Wo r0 = (C24651Wo) ((AnonymousClass1WT) this).A03.get(cls);
            return r0 == null ? AnonymousClass1WT.A04 : r0;
        }
        C24681Wr r1 = (C24681Wr) this;
        if (r1.A05.contains(cls)) {
            return r1.A00.A01(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", cls));
    }

    public Object A02(Class cls) {
        if (!(this instanceof C24681Wr)) {
            AnonymousClass1WW A00 = A00(cls);
            if (A00 == null) {
                return null;
            }
            return A00.get();
        }
        C24681Wr r1 = (C24681Wr) this;
        if (r1.A01.contains(cls)) {
            Object A02 = r1.A00.A02(cls);
            if (cls.equals(AnonymousClass1Wi.class)) {
                return new C30331EuF();
            }
            return A02;
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency %s.", cls));
    }

    public Set A03(Class cls) {
        if (!(this instanceof C24681Wr)) {
            return (Set) A01(cls).get();
        }
        C24681Wr r1 = (C24681Wr) this;
        if (r1.A04.contains(cls)) {
            return r1.A00.A03(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Set<%s>.", cls));
    }
}
