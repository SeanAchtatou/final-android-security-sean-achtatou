package X;

import java.util.BitSet;

/* renamed from: X.1kz  reason: invalid class name and case insensitive filesystem */
public final class C32021kz extends AnonymousClass11F {
    private static final String[] A02 = {"delegate"};
    private C21841Ix A00;
    private final BitSet A01 = new BitSet(1);

    public AnonymousClass11F A2x() {
        return this;
    }

    public void A32(C17770zR r1) {
        this.A00 = (C21841Ix) r1;
    }

    /* renamed from: A33 */
    public C21841Ix A31() {
        AnonymousClass11F.A0C(1, this.A01, A02);
        return this.A00;
    }

    public void A34(C17770zR r3) {
        this.A01.set(0);
        this.A00.A00 = r3;
    }

    public static void A00(C32021kz r0, AnonymousClass0p4 r1, int i, int i2, C21841Ix r4) {
        super.A2L(r1, i, i2, r4);
        r0.A00 = r4;
    }
}
