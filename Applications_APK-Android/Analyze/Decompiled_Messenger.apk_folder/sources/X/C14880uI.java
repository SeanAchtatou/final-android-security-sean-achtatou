package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0uI  reason: invalid class name and case insensitive filesystem */
public final class C14880uI extends Enum {
    private static final /* synthetic */ C14880uI[] A00;
    public static final C14880uI A01;
    public static final C14880uI A02;
    public static final C14880uI A03;
    public static final C14880uI A04;
    public static final C14880uI A05;
    public static final C14880uI A06;
    public static final C14880uI A07;
    public static final C14880uI A08;
    public static final C14880uI A09;
    public static final C14880uI A0A;
    public static final C14880uI A0B;
    public static final C14880uI A0C;
    public static final C14880uI A0D;
    public static final C14880uI A0E;
    public static final C14880uI A0F;
    public static final C14880uI A0G;
    public static final C14880uI A0H;
    public static final C14880uI A0I;
    public static final C14880uI A0J;
    public static final C14880uI A0K;
    public static final C14880uI A0L;
    public final int mAsInt;

    static {
        C14880uI r10 = new C14880uI("NO_ERROR", 0, 100001);
        A0B = r10;
        C14880uI r9 = new C14880uI("API_ERROR", 1, 100002);
        A01 = r9;
        C14880uI r8 = new C14880uI("HTTP_400_AUTHENTICATION", 2, 100003);
        A06 = r8;
        C14880uI r0 = new C14880uI("HTTP_400_OTHER", 3, 100004);
        A07 = r0;
        C14880uI r02 = new C14880uI("HTTP_500_CLASS", 4, 100005);
        A08 = r02;
        C14880uI r03 = new C14880uI("CONNECTION_FAILURE", 5, 100006);
        A04 = r03;
        C14880uI r04 = new C14880uI("ORCA_SERVICE_UNKNOWN_OPERATION", 6, 100007);
        A0D = r04;
        C14880uI r05 = new C14880uI("ORCA_SERVICE_IPC_FAILURE", 7, 100008);
        A0C = r05;
        C14880uI r15 = new C14880uI("OUT_OF_MEMORY", 8, 100009);
        A0F = r15;
        C14880uI r14 = new C14880uI("OTHER", 9, 100010);
        A0E = r14;
        C14880uI r13 = new C14880uI(AnonymousClass24B.$const$string(112), 10, 100011);
        A03 = r13;
        C14880uI r12 = new C14880uI("CACHE_DISK_ERROR", 11, 100012);
        A02 = r12;
        C14880uI r11 = new C14880uI("MQTT_SEND_FAILURE", 12, 100013);
        A0A = r11;
        C14880uI r7 = new C14880uI("WORK_AUTH_FAILED", 13, 100014);
        A0L = r7;
        C14880uI r6 = new C14880uI("WORK_AUTH_COMMUNITY_ID_REQUIRED", 14, 100015);
        A0K = r6;
        C14880uI r5 = new C14880uI("DATE_ERROR", 15, 100016);
        A05 = r5;
        C14880uI r4 = new C14880uI("SEGMENTED_TRANSCODE_ERROR", 16, 100017);
        A0H = r4;
        C14880uI r16 = new C14880uI("STREAMING_UPLOAD_ERROR", 17, 100018);
        C14880uI r3 = new C14880uI("PHASE_ONE_TRANSCODING_ERROR", 18, 100019);
        A0G = r3;
        C14880uI r26 = new C14880uI("UNREACHABLE_STATEMENT_ERROR", 19, 100020);
        A0J = r26;
        C14880uI r262 = new C14880uI("TAGGING_ERROR", 20, 100021);
        A0I = r262;
        C14880uI r263 = new C14880uI("LASSO_LOGIN_UNAVAILABLE_IN_COUNTRY", 21, 2567002);
        A09 = r263;
        C14880uI r162 = r0;
        C14880uI r17 = r02;
        C14880uI r18 = r03;
        C14880uI r19 = r04;
        C14880uI r20 = r05;
        C14880uI r21 = r15;
        C14880uI r22 = r14;
        C14880uI r23 = r13;
        C14880uI r24 = r12;
        C14880uI r25 = r11;
        C14880uI r132 = r10;
        C14880uI r142 = r9;
        C14880uI r152 = r8;
        A00 = new C14880uI[]{r132, r142, r152, r162, r17, r18, r19, r20, r21, r22, r23, r24, r25, r7, r6, r5, r4, r16, r3, r26, r262, r263, new C14880uI("LASSO_SHARE_SHEET__MESSAGE_LIMIT_EXCEEDED", 22, 2613001)};
    }

    public static C14880uI valueOf(String str) {
        return (C14880uI) Enum.valueOf(C14880uI.class, str);
    }

    public static C14880uI[] values() {
        return (C14880uI[]) A00.clone();
    }

    private C14880uI(String str, int i, int i2) {
        this.mAsInt = i2;
    }
}
