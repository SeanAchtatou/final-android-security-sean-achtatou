package X;

import android.content.Context;
import android.widget.LinearLayout;
import com.facebook.graphql.query.GQSQStringShape0S0000000_I0;
import com.facebook.litho.LithoView;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.17a  reason: invalid class name and case insensitive filesystem */
public final class C191617a {
    private static volatile C191617a A08;
    public AnonymousClass0UN A00;
    public C66903Me A01;
    public C133226Lj A02;
    public Object A03;
    public final C11170mD A04;
    public final AnonymousClass1U9 A05;
    public final C05720aD A06;
    public final Map A07 = new HashMap();

    public static void A02(C191617a r4, C17770zR r5, Context context) {
        LithoView A002 = LithoView.A00(context, r5, false);
        A002.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        r4.A02.setContentView(A002);
    }

    public static final C191617a A00(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (C191617a.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new C191617a(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    private void A01(Context context, C66903Me r6, Object obj) {
        C133226Lj r3 = new C133226Lj(context, r6, obj, (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, this.A00));
        this.A02 = r3;
        r3.show();
    }

    public void A03(Context context, String str, C66903Me r8, Object obj) {
        String str2;
        this.A01 = r8;
        this.A03 = obj;
        A01(context, r8, obj);
        if (this.A07.containsKey(str)) {
            A02(this, (C17770zR) this.A07.get(str), context);
            return;
        }
        C204709kw AmK = ((AnonymousClass2X9) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJf, this.A00)).AmK("upsell_dialog_flow");
        GQSQStringShape0S0000000_I0 gQSQStringShape0S0000000_I0 = new GQSQStringShape0S0000000_I0(7);
        gQSQStringShape0S0000000_I0.A04("nt_context", this.A05.A01());
        gQSQStringShape0S0000000_I0.A09("feature_key", String.valueOf(str));
        if (AmK != null) {
            str2 = AmK.A01;
        } else {
            str2 = BuildConfig.FLAVOR;
        }
        gQSQStringShape0S0000000_I0.A09("encrypted_subno", str2);
        C05350Yp.A08(this.A04.A04(C26931cN.A00(gQSQStringShape0S0000000_I0)), new AnonymousClass1ER(this, str, context), (ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADs, this.A00));
    }

    public void A04(Context context, String str, C66903Me r9, Object obj) {
        String str2;
        this.A01 = r9;
        this.A03 = obj;
        if (!this.A07.containsKey(str)) {
            A01(context, r9, obj);
            C204709kw AmK = ((AnonymousClass2X9) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJf, this.A00)).AmK("upsell_dialog_flow");
            GQSQStringShape0S0000000_I0 gQSQStringShape0S0000000_I0 = new GQSQStringShape0S0000000_I0(6);
            gQSQStringShape0S0000000_I0.A04("nt_context", this.A05.A01());
            gQSQStringShape0S0000000_I0.A09("feature_key", str);
            if (AmK != null) {
                str2 = AmK.A01;
            } else {
                str2 = BuildConfig.FLAVOR;
            }
            gQSQStringShape0S0000000_I0.A09("encrypted_subno", str2);
            C05350Yp.A08(this.A04.A04(C26931cN.A00(gQSQStringShape0S0000000_I0)), new AnonymousClass17Z(this, str, context), (ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADs, this.A00));
            return;
        }
        C133226Lj r5 = new C133226Lj(context, this.A01, this.A03, (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, this.A00));
        this.A02 = r5;
        r5.show();
        A02(this, (C17770zR) this.A07.get(str), context);
    }

    private C191617a(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A04 = C11170mD.A00(r3);
        this.A06 = C05720aD.A01(r3);
        C35251qv.A01(r3);
        AnonymousClass1ZD.A00(r3);
        this.A05 = AnonymousClass1U9.A00(r3);
    }
}
