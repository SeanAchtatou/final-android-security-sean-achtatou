package X;

import android.graphics.drawable.Drawable;
import android.view.ViewOutlineProvider;
import com.facebook.common.dextricks.DalvikConstants;
import com.facebook.common.dextricks.DexStore;

/* renamed from: X.11G  reason: invalid class name */
public final class AnonymousClass11G implements C17480yy {
    public byte A00;
    public int A01;
    public int A02;
    public Drawable A03;
    public AnonymousClass1AL A04;
    public C14930uN A05;
    public C31401jd A06;
    public String A07;
    public boolean A08;

    public void A04(float f) {
        this.A08 = true;
        C31401jd A012 = A01(this);
        A012.A00 = f;
        A012.A0A |= 1048576;
    }

    public void A05(float f) {
        this.A08 = true;
        C31401jd A012 = A01(this);
        A012.A01 = f;
        A012.A0A |= 2097152;
    }

    public void A06(float f) {
        this.A08 = true;
        C31401jd A012 = A01(this);
        A012.A04 = f;
        A012.A0A |= DexStore.LOAD_RESULT_WITH_VDEX_ODEX;
    }

    public static C14930uN A00(AnonymousClass11G r1) {
        if (r1.A05 == null) {
            r1.A05 = new C14930uN();
        }
        return r1.A05;
    }

    public static C31401jd A01(AnonymousClass11G r1) {
        if (r1.A06 == null) {
            r1.A06 = new C31401jd();
        }
        return r1.A06;
    }

    private C17480yy A02() {
        if (this.A04 == null) {
            this.A04 = new AnonymousClass1AL();
        }
        return this.A04;
    }

    public void A0B(int i, int i2) {
        this.A01 = i;
        this.A02 = i2;
    }

    public void A0C(Drawable drawable) {
        this.A00 = (byte) (this.A00 | 1);
        this.A03 = drawable;
    }

    public void A0S(String str) {
        this.A00 = (byte) (this.A00 | 2);
        this.A07 = str;
    }

    public Integer A03() {
        return A00(this).A0E;
    }

    public void A07(float f) {
        C31401jd A012 = A01(this);
        A012.A0A |= DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET;
        A012.A05 = f;
    }

    public void A08(float f) {
        C14930uN A002 = A00(this);
        A002.A03 |= 2048;
        A002.A00 = f;
    }

    public void A09(float f) {
        C14930uN A002 = A00(this);
        A002.A03 |= DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED;
        A002.A01 = f;
    }

    public void A0A(int i) {
        C14930uN A002 = A00(this);
        A002.A03 |= 1;
        A002.A02 = i;
    }

    public void A0D(Drawable drawable) {
        C14930uN A002 = A00(this);
        A002.A03 |= 4;
        A002.A05 = drawable;
    }

    public void A0E(ViewOutlineProvider viewOutlineProvider) {
        C31401jd A012 = A01(this);
        A012.A0A |= DexStore.LOAD_RESULT_PGO;
        A012.A0D = viewOutlineProvider;
    }

    public void A0F(AnonymousClass38W r3) {
        C14930uN A002 = A00(this);
        if (r3 != null) {
            A002.A03 |= DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED;
            A002.A06 = r3;
        }
    }

    public void A0G(AnonymousClass10N r3) {
        C31401jd A012 = A01(this);
        A012.A0A |= 8;
        A012.A0E = r3;
    }

    public void A0H(AnonymousClass10N r4) {
        C31401jd A012 = A01(this);
        A012.A0A |= DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP;
        A012.A0G = r4;
    }

    public void A0I(AnonymousClass10N r3) {
        C14930uN A002 = A00(this);
        A002.A03 |= 64;
        A002.A0A = r3;
    }

    public void A0J(AnonymousClass10N r3) {
        C31401jd A012 = A01(this);
        A012.A0A |= 16;
        A012.A0I = r3;
    }

    public void A0K(AnonymousClass10N r3) {
        C31401jd A012 = A01(this);
        A012.A0A |= DexStore.LOAD_RESULT_OATMEAL_QUICKENED;
        A012.A0K = r3;
    }

    public void A0L(AnonymousClass10N r3) {
        C31401jd A012 = A01(this);
        A012.A0A |= 32;
        A012.A0Q = r3;
    }

    public void A0M(AnonymousClass10N r3) {
        C14930uN A002 = A00(this);
        A002.A03 |= 8;
        A002.A0D = r3;
    }

    public void A0N(AnonymousClass10G r3, int i) {
        C14930uN A002 = A00(this);
        A002.A03 |= DexStore.LOAD_RESULT_OATMEAL_QUICKENED;
        if (A002.A07 == null) {
            A002.A07 = new AnonymousClass1IF();
        }
        A002.A07.A03(r3, (float) i);
    }

    public void A0O(CharSequence charSequence) {
        C31401jd A012 = A01(this);
        A012.A0A |= 1;
        A012.A0S = charSequence;
    }

    public void A0P(Integer num) {
        C14930uN A002 = A00(this);
        A002.A03 |= DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP;
        A002.A0E = num;
    }

    public void A0Q(Object obj) {
        C31401jd A012 = A01(this);
        A012.A0A |= 2;
        A012.A0T = obj;
    }

    public void A0R(String str) {
        C31401jd A012 = A01(this);
        A012.A0A |= 4194304;
        A012.A0U = str;
    }

    public void A0T(String str, String str2) {
        C14930uN A002 = A00(this);
        A002.A03 |= 512;
        A002.A0F = str;
        A002.A0G = str2;
    }

    public void A0U(boolean z) {
        C31401jd A012 = A01(this);
        if (z) {
            A012.A07 = 1;
        } else {
            A012.A07 = 2;
        }
    }

    public void A0V(boolean z) {
        C31401jd A012 = A01(this);
        A012.A0A |= DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE;
        A012.A0V = z;
    }

    public void A0W(boolean z) {
        C14930uN A002 = A00(this);
        A002.A03 |= 2;
        A002.A0H = z;
    }

    public void A0X(boolean z) {
        C31401jd A012 = A01(this);
        if (z) {
            A012.A08 = 1;
        } else {
            A012.A08 = 2;
        }
    }

    public void A0Y(boolean z) {
        C31401jd A012 = A01(this);
        if (z) {
            A012.A09 = 1;
        } else {
            A012.A09 = 2;
        }
    }

    public void A0Z(boolean z) {
        C31401jd A012 = A01(this);
        if (z) {
            A012.A0B = 1;
        } else {
            A012.A0B = 2;
        }
    }

    public void ANz(C14940uO r2) {
        A02().ANz(r2);
    }

    public void AOt(float f) {
        A02().AOt(f);
    }

    public void Aa6(float f) {
        A02().Aa6(f);
    }

    public void Aa7(float f) {
        A02().Aa7(f);
    }

    public void Aa8(int i) {
        A02().Aa8(i);
    }

    public void AaB(float f) {
        A02().AaB(f);
    }

    public void AaD(float f) {
        A02().AaD(f);
    }

    public void BC1(float f) {
        A02().BC1(f);
    }

    public void BC2(int i) {
        A02().BC2(i);
    }

    public void BGL(boolean z) {
        A02().BGL(z);
    }

    public void BHv(C17660zG r2) {
        A02().BHv(r2);
    }

    public void BJt(AnonymousClass10G r2) {
        A02().BJt(r2);
    }

    public void BJv(AnonymousClass10G r2, float f) {
        A02().BJv(r2, f);
    }

    public void BJx(AnonymousClass10G r2, int i) {
        A02().BJx(r2, i);
    }

    public void BKG(float f) {
        A02().BKG(f);
    }

    public void BKH(int i) {
        A02().BKH(i);
    }

    public void BKK(float f) {
        A02().BKK(f);
    }

    public void BKL(int i) {
        A02().BKL(i);
    }

    public void BL7(float f) {
        A02().BL7(f);
    }

    public void BL8(int i) {
        A02().BL8(i);
    }

    public void BL9(float f) {
        A02().BL9(f);
    }

    public void BLA(int i) {
        A02().BLA(i);
    }

    public void BwL(AnonymousClass10G r2, float f) {
        A02().BwL(r2, f);
    }

    public void BwM(AnonymousClass10G r2, int i) {
        A02().BwM(r2, i);
    }

    public void BxH(AnonymousClass10G r2, float f) {
        A02().BxH(r2, f);
    }

    public void BxI(AnonymousClass10G r2, int i) {
        A02().BxI(r2, i);
    }

    public void BxJ(AnonymousClass1LC r2) {
        A02().BxJ(r2);
    }

    public void CLJ(boolean z) {
        A02().CLJ(z);
    }

    public void CNJ(float f) {
        A02().CNJ(f);
    }

    public void CNK(int i) {
        A02().CNK(i);
    }
}
