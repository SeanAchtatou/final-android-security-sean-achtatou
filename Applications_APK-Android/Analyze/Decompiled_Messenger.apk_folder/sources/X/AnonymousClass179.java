package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.179  reason: invalid class name */
public final class AnonymousClass179 extends AnonymousClass0W4 {
    private static final C06060am A00;
    private static final ImmutableList A01;

    static {
        AnonymousClass0W6 r2 = AnonymousClass17D.A01;
        A01 = ImmutableList.of(r2, AnonymousClass17D.A02, AnonymousClass17D.A00);
        A00 = new C06050al(ImmutableList.of(r2));
    }

    public AnonymousClass179() {
        super("ephemeral_data", A01, A00);
    }
}
