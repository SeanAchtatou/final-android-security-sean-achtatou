package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.19r  reason: invalid class name and case insensitive filesystem */
public final class C197919r implements AnonymousClass1AS {
    private static volatile C197919r A01;
    private QuickPerformanceLogger A00;

    public void BeZ() {
    }

    public static final C197919r A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C197919r.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C197919r(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void BNq(RecyclerView recyclerView) {
        this.A00.markerEnd(5505073, 2);
        this.A00.markerStart(5505074);
    }

    public void BPg(RecyclerView recyclerView) {
        this.A00.markerStart(5505073);
    }

    private C197919r(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0ZD.A03(r2);
    }
}
