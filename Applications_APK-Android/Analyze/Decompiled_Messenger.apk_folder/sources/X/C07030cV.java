package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0cV  reason: invalid class name and case insensitive filesystem */
public final class C07030cV {
    private static volatile C07030cV A02;
    public C10520kK A00;
    public AnonymousClass0UN A01;

    public static final C07030cV A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C07030cV.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C07030cV(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C07030cV(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(0, r3);
        this.A00 = C10510kJ.A00(r3);
    }
}
