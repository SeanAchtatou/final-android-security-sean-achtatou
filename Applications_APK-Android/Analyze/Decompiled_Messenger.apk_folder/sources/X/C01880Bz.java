package X;

import android.content.Context;
import com.facebook.rti.common.time.RealtimeSinceBootClock;

/* renamed from: X.0Bz  reason: invalid class name and case insensitive filesystem */
public final class C01880Bz extends AnonymousClass0C0 {
    public C01880Bz(Context context, String str, AnonymousClass0AS r4, RealtimeSinceBootClock realtimeSinceBootClock, boolean z) {
        super(context, str, "du", z);
    }
}
