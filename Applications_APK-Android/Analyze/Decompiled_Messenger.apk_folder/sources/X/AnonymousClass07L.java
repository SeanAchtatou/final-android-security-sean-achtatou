package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.07L  reason: invalid class name */
public final class AnonymousClass07L extends Handler implements Application.ActivityLifecycleCallbacks {
    public static final CopyOnWriteArraySet A02 = new CopyOnWriteArraySet();
    public static volatile int A03;
    private boolean A00;
    private boolean A01;

    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (!this.A00) {
            this.A00 = true;
            if (!this.A01) {
                Iterator it = A02.iterator();
                while (it.hasNext()) {
                    ((AnonymousClass0P0) it.next()).onColdStartModeChanged();
                }
            }
        }
    }
}
