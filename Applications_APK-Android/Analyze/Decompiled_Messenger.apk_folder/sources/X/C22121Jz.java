package X;

/* renamed from: X.1Jz  reason: invalid class name and case insensitive filesystem */
public final class C22121Jz {
    public int A00;
    public AnonymousClass10N A01;
    public AnonymousClass10N A02;
    public boolean A03;
    public boolean A04;
    public final AnonymousClass10N A05;

    public void A00(boolean z) {
        if (z) {
            this.A00 |= 32;
        } else {
            this.A00 &= -33;
        }
    }

    public C22121Jz(AnonymousClass10N r1, AnonymousClass10N r2, AnonymousClass10N r3) {
        this.A01 = r1;
        this.A02 = r2;
        this.A05 = r3;
    }
}
