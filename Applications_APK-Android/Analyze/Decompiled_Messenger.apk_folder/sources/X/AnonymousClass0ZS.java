package X;

import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ZS  reason: invalid class name */
public final class AnonymousClass0ZS extends C05930aZ {
    private static volatile AnonymousClass0ZS A02;
    public AnonymousClass0UN A00;
    public final Set A01 = new C05180Xy();

    public static final AnonymousClass0ZS A01(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (AnonymousClass0ZS.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A02 = new AnonymousClass0ZS(applicationInjector, C06820c8.A00(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.BQw, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public boolean A09() {
        C34521pk r3 = (C34521pk) AnonymousClass1XX.A03(AnonymousClass1Y3.B4l, this.A00);
        if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r3.A00)).Aem(286405600090896L) || ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r3.A00)).Aem(286405600025359L)) {
            return true;
        }
        return false;
    }

    private AnonymousClass0ZS(AnonymousClass1XY r3, C06940cL r4, C04310Tq r5) {
        super(r4, r5);
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    public static final AnonymousClass0ZS A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
