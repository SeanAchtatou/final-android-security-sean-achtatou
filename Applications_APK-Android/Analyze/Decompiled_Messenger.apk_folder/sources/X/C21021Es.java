package X;

/* renamed from: X.1Es  reason: invalid class name and case insensitive filesystem */
public final class C21021Es extends C21061Ew implements C31011j0 {
    public int A00;
    public long A01;
    public AnonymousClass0lr A02;
    public String A03;
    public final C08520fU A04;
    public final C09150gc A05;
    public volatile int A06;
    public volatile int A07;
    public volatile C04270Tg A08;

    private AnonymousClass0lr A00() {
        if (this.A02 == null) {
            this.A02 = new AnonymousClass0lr();
        }
        return this.A02;
    }

    public static C04270Tg A01(C21021Es r1) {
        if (r1.A08 != null) {
            return r1.A08;
        }
        throw new IllegalStateException("Do not use MarkerEditor after call to editingCompleted()");
    }

    public C31011j0 ANX(String str, String[] strArr) {
        if (strArr == null) {
            return this;
        }
        A00().A00(str, AnonymousClass36y.A03(strArr), 3);
        return this;
    }

    public C31011j0 Bx9(long j) {
        if (this.A00 != 1 || j == -1) {
            this.A01 = j;
            return this;
        }
        throw new IllegalStateException("You can't collect metadata with custom timestamp, as point appeared in the past but metadata is to be collected in the present");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0072, code lost:
        if (X.C010708t.A0X(3) == false) goto L_0x0074;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C21061Ew BxA() {
        /*
            r25 = this;
            r5 = r25
            java.lang.String r0 = r5.A03
            if (r0 == 0) goto L_0x009a
            X.0lr r1 = r5.A02
            if (r1 == 0) goto L_0x000d
            r0 = 1
            r1.A03 = r0
        L_0x000d:
            X.0fU r4 = r5.A04
            X.0Tg r12 = A01(r5)
            int r13 = r5.A06
            int r14 = r5.A07
            java.lang.String r3 = r5.A03
            X.0lr r2 = r5.A02
            long r0 = r5.A01
            java.util.concurrent.TimeUnit r9 = java.util.concurrent.TimeUnit.MILLISECONDS
            int r6 = r5.A00
            int r22 = android.os.Process.myTid()
            X.1zi r8 = r4.A03
            if (r8 == 0) goto L_0x0045
            int r7 = r12.A02
            X.Ed3 r24 = r8.A01(r7)
        L_0x002f:
            r10 = -1
            int r7 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            r8 = 0
            if (r7 != 0) goto L_0x0037
            r8 = 1
        L_0x0037:
            if (r8 == 0) goto L_0x0048
            boolean r7 = r4.A0C
            if (r7 == 0) goto L_0x0048
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Autotime in lockless is illegal (time won't be valid when method is executed)"
            r1.<init>(r0)
            throw r1
        L_0x0045:
            r24 = 0
            goto L_0x002f
        L_0x0048:
            long r15 = X.C08520fU.A02(r4, r0, r9)
            X.0i5 r11 = r4.A04
            java.util.concurrent.TimeUnit r17 = java.util.concurrent.TimeUnit.NANOSECONDS
            r18 = r8 ^ 1
            X.0gd r0 = r4.A0G
            r20 = r2
            r21 = r6
            r23 = r0
            r19 = r3
            boolean r0 = r11.A0J(r12, r13, r14, r15, r17, r18, r19, r20, r21, r22, r23, r24)
            if (r0 == 0) goto L_0x007d
            int r7 = r12.A02
            java.lang.String r6 = "markerPoint"
            boolean r0 = X.C08520fU.A0C(r4)
            if (r0 == 0) goto L_0x0074
            r0 = 3
            boolean r1 = X.C010708t.A0X(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0075
        L_0x0074:
            r0 = 0
        L_0x0075:
            if (r0 == 0) goto L_0x007d
            if (r2 != 0) goto L_0x0095
            r0 = 0
        L_0x007a:
            X.C08520fU.A08(r4, r6, r7, r3, r0)
        L_0x007d:
            X.1zi r1 = r4.A03
            if (r1 == 0) goto L_0x0088
            r0 = r24
            if (r24 == 0) goto L_0x0088
            r1.A04(r0)
        L_0x0088:
            r0 = 0
            r5.A03 = r0
            r5.A02 = r0
            r0 = -1
            r5.A01 = r0
            r0 = 0
            r5.A00 = r0
            return r25
        L_0x0095:
            java.lang.String r0 = r2.toString()
            goto L_0x007a
        L_0x009a:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "You should not use PointEditor after point was completed"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21021Es.BxA():X.1Ew");
    }

    public C31011j0 BxB(boolean z) {
        if (!z) {
            this.A00 = 0;
            return this;
        } else if (this.A01 == -1) {
            this.A00 = 1;
            return this;
        } else {
            throw new IllegalStateException("You can't collect metadata with custom timestamp, as point appeared in the past but metadata is to be collected in the present");
        }
    }

    public C21021Es(C08520fU r1, C09150gc r2) {
        this.A04 = r1;
        this.A05 = r2;
    }

    public C31011j0 ANQ(String str, double d) {
        A00().A00(str, String.valueOf(d), 5);
        return this;
    }

    public C31011j0 ANR(String str, int i) {
        A00().A00(str, String.valueOf(i), 2);
        return this;
    }

    public C31011j0 ANS(String str, long j) {
        A00().A00(str, String.valueOf(j), 2);
        return this;
    }

    public C31011j0 ANT(String str, String str2) {
        A00().A00(str, str2, 1);
        return this;
    }

    public C31011j0 ANU(String str, boolean z) {
        A00().A00(str, String.valueOf(z), 7);
        return this;
    }

    public C31011j0 ANV(String str, double[] dArr) {
        A00().A00(str, AnonymousClass36y.A00(dArr), 6);
        return this;
    }

    public C31011j0 ANW(String str, int[] iArr) {
        A00().A00(str, AnonymousClass36y.A01(iArr), 4);
        return this;
    }

    public C31011j0 ANY(String str, boolean[] zArr) {
        A00().A00(str, AnonymousClass36y.A04(zArr), 8);
        return this;
    }
}
