package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Zi  reason: invalid class name and case insensitive filesystem */
public final class C25361Zi {
    private static volatile C25361Zi A01;
    public AnonymousClass0UN A00;

    public static final C25361Zi A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C25361Zi.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C25361Zi(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A01(String str, int i, int i2, String str2) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ap7, this.A00)).A01("appjobs_android_job_finished"), 52);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("trigger_name", str2);
            uSLEBaseShape0S0000000.A0D(OptSvcAnalyticsStore.LOGGING_KEY_JOB_NAME, str);
            uSLEBaseShape0S0000000.A0B("wait_time", Integer.valueOf(i));
            uSLEBaseShape0S0000000.A0B(AnonymousClass24B.$const$string(AnonymousClass1Y3.A85), Integer.valueOf(i2));
            uSLEBaseShape0S0000000.A06();
        }
    }

    public void A02(String str, int i, int i2, String str2) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ap7, this.A00)).A01("appjobs_android_job_started"), 54);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("trigger_name", str2);
            uSLEBaseShape0S0000000.A0D(OptSvcAnalyticsStore.LOGGING_KEY_JOB_NAME, str);
            uSLEBaseShape0S0000000.A0B("wait_time", Integer.valueOf(i));
            uSLEBaseShape0S0000000.A0B("is_needed_time", Integer.valueOf(i2));
            uSLEBaseShape0S0000000.A06();
        }
    }

    private C25361Zi(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
