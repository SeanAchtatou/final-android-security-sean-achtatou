package X;

import java.io.DataInputStream;
import java.io.EOFException;
import java.nio.ByteOrder;

/* renamed from: X.0KM  reason: invalid class name */
public final class AnonymousClass0KM {
    public int A00;
    public int A01;
    private final AnonymousClass0KM A02;
    private final DataInputStream A03;

    private void A00() {
        int i;
        int i2 = this.A01;
        if (i2 >= 0 && (i = this.A00) > i2) {
            throw new EOFException(AnonymousClass08S.A0B("Invalid access: limit=", i2, ", consumed=", i));
        }
    }

    public byte A01() {
        this.A00++;
        A00();
        DataInputStream dataInputStream = this.A03;
        if (dataInputStream != null) {
            return dataInputStream.readByte();
        }
        AnonymousClass0KM r0 = this.A02;
        if (r0 != null) {
            return r0.A01();
        }
        throw new NullPointerException("Source input stream was not setup.");
    }

    public int A02() {
        this.A00 += 4;
        A00();
        DataInputStream dataInputStream = this.A03;
        if (dataInputStream != null) {
            int readInt = dataInputStream.readInt();
            if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                return Integer.reverseBytes(readInt);
            }
            return readInt;
        }
        AnonymousClass0KM r0 = this.A02;
        if (r0 != null) {
            return r0.A02();
        }
        throw new NullPointerException("Source input stream was not setup.");
    }

    public short A03() {
        this.A00 += 2;
        A00();
        DataInputStream dataInputStream = this.A03;
        if (dataInputStream != null) {
            short readShort = dataInputStream.readShort();
            if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                return Short.reverseBytes(readShort);
            }
            return readShort;
        }
        AnonymousClass0KM r0 = this.A02;
        if (r0 != null) {
            return r0.A03();
        }
        throw new NullPointerException("Source input stream was not setup.");
    }

    public void A04(int i) {
        this.A00 += i;
        A00();
        DataInputStream dataInputStream = this.A03;
        if (dataInputStream != null) {
            dataInputStream.skipBytes(i);
            return;
        }
        AnonymousClass0KM r0 = this.A02;
        if (r0 != null) {
            r0.A04(i);
            return;
        }
        throw new NullPointerException("Source input stream was not setup.");
    }

    public void A05(byte[] bArr) {
        this.A00 += bArr.length;
        A00();
        DataInputStream dataInputStream = this.A03;
        if (dataInputStream != null) {
            dataInputStream.readFully(bArr);
            return;
        }
        AnonymousClass0KM r0 = this.A02;
        if (r0 != null) {
            r0.A05(bArr);
            return;
        }
        throw new NullPointerException("Source input stream was not setup.");
    }

    public AnonymousClass0KM(AnonymousClass0KM r2) {
        this.A03 = null;
        this.A02 = r2;
    }

    public AnonymousClass0KM(DataInputStream dataInputStream) {
        this.A03 = dataInputStream;
        this.A02 = null;
    }
}
