package X;

import android.os.RemoteException;
import com.facebook.push.mqtt.ipc.MqttPubAckCallback;

/* renamed from: X.0PF  reason: invalid class name */
public final class AnonymousClass0PF {
    private static final Class A01 = AnonymousClass0PF.class;
    private final MqttPubAckCallback A00;

    public void A00(int i) {
        try {
            this.A00.BYX(i);
        } catch (RemoteException e) {
            C010708t.A0F(A01, e, "Failed to deliver onFailure", new Object[0]);
        }
    }

    public void A01(int i) {
        try {
            this.A00.BkS(i);
        } catch (RemoteException e) {
            C010708t.A0F(A01, e, "Failed to deliver onPubAckTimeout", new Object[0]);
        }
    }

    public void A02(int i) {
        try {
            this.A00.BqU(i);
        } catch (RemoteException e) {
            C010708t.A0F(A01, e, "Failed to deliver onSuccess", new Object[0]);
        }
    }

    public AnonymousClass0PF(MqttPubAckCallback mqttPubAckCallback) {
        AnonymousClass0A1.A00(mqttPubAckCallback);
        this.A00 = mqttPubAckCallback;
    }
}
