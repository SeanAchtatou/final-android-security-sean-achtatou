package X;

import com.facebook.http.interfaces.RequestPriority;
import com.facebook.messaging.service.model.FetchThreadListParams;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;

/* renamed from: X.0kg  reason: invalid class name and case insensitive filesystem */
public final class C10680kg {
    public int A00 = 20;
    public int A01;
    public C09510hU A02;
    public RequestPriority A03 = RequestPriority.A00;
    public C10950l8 A04;
    public C10700ki A05 = C10700ki.ALL;
    public ImmutableSet A06 = RegularImmutableSet.A05;
    public Integer A07 = AnonymousClass07B.A00;
    public String A08;

    public void A00(FetchThreadListParams fetchThreadListParams) {
        this.A02 = fetchThreadListParams.A01;
        this.A04 = fetchThreadListParams.A03;
        this.A05 = fetchThreadListParams.A04;
        this.A06 = fetchThreadListParams.A05;
        this.A07 = fetchThreadListParams.A06;
        this.A00 = fetchThreadListParams.A01();
        this.A01 = fetchThreadListParams.A00;
        this.A08 = fetchThreadListParams.A07;
    }
}
