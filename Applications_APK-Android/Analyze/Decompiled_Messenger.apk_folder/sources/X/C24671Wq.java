package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1Wq  reason: invalid class name and case insensitive filesystem */
public final /* synthetic */ class C24671Wq implements AnonymousClass1WW {
    private final Set A00;

    public C24671Wq(Set set) {
        this.A00 = set;
    }

    public Object get() {
        Set<C24651Wo> set = this.A00;
        HashSet hashSet = new HashSet();
        for (C24651Wo r0 : set) {
            hashSet.add(r0.get());
        }
        return Collections.unmodifiableSet(hashSet);
    }
}
