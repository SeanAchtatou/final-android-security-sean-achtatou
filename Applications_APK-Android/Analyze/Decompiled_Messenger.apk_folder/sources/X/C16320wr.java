package X;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: X.0wr  reason: invalid class name and case insensitive filesystem */
public final class C16320wr {
    private static final C16340wt A00;
    private static final C16340wt A01;
    private static final int[] A02 = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10};

    static {
        C37111ui r0;
        C16340wt r02;
        if (Build.VERSION.SDK_INT >= 21) {
            r0 = new C37111ui();
        } else {
            r0 = null;
        }
        A00 = r0;
        try {
            r02 = (C16340wt) Class.forName("androidx.transition.FragmentTransitionSupport").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            r02 = null;
        }
        A01 = r02;
    }

    public static View A00(AnonymousClass04a r3, C29871h7 r4, Object obj, boolean z) {
        ArrayList arrayList;
        ArrayList arrayList2;
        C16280wn r2 = r4.A01;
        if (obj == null || r3 == null || (arrayList = r2.A0D) == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            arrayList2 = r2.A0D;
        } else {
            arrayList2 = r2.A0E;
        }
        return (View) r3.get((String) arrayList2.get(0));
    }

    public static AnonymousClass04a A01(C16340wt r3, AnonymousClass04a r4, Object obj, C29871h7 r6) {
        ArrayList arrayList;
        View view = r6.A03.A0I;
        if (r4.isEmpty() || obj == null || view == null) {
            r4.clear();
            return null;
        }
        AnonymousClass04a r2 = new AnonymousClass04a();
        r3.A0K(r2, view);
        C16280wn r1 = r6.A01;
        if (r6.A05) {
            arrayList = r1.A0D;
        } else {
            arrayList = r1.A0E;
        }
        if (arrayList != null) {
            r2.A0C(arrayList);
            r2.A0C(r4.values());
        }
        for (int size = r4.size() - 1; size >= 0; size--) {
            if (!r2.containsKey((String) r4.A09(size))) {
                r4.A08(size);
            }
        }
        return r2;
    }

    private static C16340wt A03(Fragment fragment, Fragment fragment2) {
        boolean z;
        boolean z2;
        Object obj;
        Object obj2;
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            AnonymousClass2NQ r0 = fragment.A0K;
            if (r0 == null || (obj2 = r0.A08) == Fragment.A0j) {
                obj2 = null;
            }
            if (obj2 != null) {
                arrayList.add(obj2);
            }
            Object A19 = fragment.A19();
            if (A19 != null) {
                arrayList.add(A19);
            }
        }
        if (fragment2 != null) {
            AnonymousClass2NQ r02 = fragment2.A0K;
            if (r02 == null || (obj = r02.A07) == Fragment.A0j) {
                obj = null;
            }
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        C16340wt r3 = A00;
        if (r3 != null) {
            int size = arrayList.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    z2 = true;
                    break;
                } else if (!r3.A0L(arrayList.get(i))) {
                    z2 = false;
                    break;
                } else {
                    i++;
                }
            }
            if (z2) {
                return r3;
            }
        }
        C16340wt r32 = A01;
        if (r32 != null) {
            int size2 = arrayList.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size2) {
                    z = true;
                    break;
                } else if (!r32.A0L(arrayList.get(i2))) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                return r32;
            }
        }
        if (A00 == null && r32 == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    private static Object A04(C16340wt r2, Fragment fragment, boolean z) {
        Object obj;
        AnonymousClass2NQ r0;
        if (fragment == null) {
            return null;
        }
        if (!z || (r0 = fragment.A0K) == null || (obj = r0.A07) == Fragment.A0j) {
            obj = null;
        }
        return r2.A04(obj);
    }

    private static Object A05(C16340wt r2, Fragment fragment, boolean z) {
        Object obj;
        AnonymousClass2NQ r0;
        if (fragment == null) {
            return null;
        }
        if (!z || (r0 = fragment.A0K) == null || (obj = r0.A08) == Fragment.A0j) {
            obj = null;
        }
        return r2.A04(obj);
    }

    public static ArrayList A06(C16340wt r2, Object obj, Fragment fragment, ArrayList arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList arrayList2 = new ArrayList();
        View view2 = fragment.A0I;
        if (view2 != null) {
            r2.A0J(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (!arrayList2.isEmpty()) {
            arrayList2.add(view);
            r2.A0G(obj, arrayList2);
        }
        return arrayList2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0090, code lost:
        if (r3.A0W != false) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00a2, code lost:
        if (r3.A0b != false) goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x00ba, code lost:
        if (r3.A0B >= 0.0f) goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00bc, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x00c7, code lost:
        if (r3.A0b == false) goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x00d9, code lost:
        if (r3.A0b == false) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x00db, code lost:
        r0 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0079 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A08(X.C16280wn r9, X.C29471gT r10, android.util.SparseArray r11, boolean r12, boolean r13) {
        /*
            androidx.fragment.app.Fragment r3 = r10.A05
            if (r3 == 0) goto L_0x0083
            int r2 = r3.A0D
            if (r2 == 0) goto L_0x0083
            if (r12 == 0) goto L_0x00df
            int[] r1 = X.C16320wr.A02
            int r0 = r10.A00
            r5 = r1[r0]
        L_0x0010:
            r0 = 0
            r4 = 1
            if (r5 == r4) goto L_0x00cc
            r1 = 3
            if (r5 == r1) goto L_0x00a5
            r1 = 4
            if (r5 == r1) goto L_0x0096
            r1 = 5
            if (r5 == r1) goto L_0x0084
            r1 = 6
            if (r5 == r1) goto L_0x00a5
            r1 = 7
            if (r5 == r1) goto L_0x00cc
            r5 = 0
        L_0x0024:
            r8 = 0
            r1 = 0
        L_0x0026:
            java.lang.Object r6 = r11.get(r2)
            X.1h7 r6 = (X.C29871h7) r6
            if (r0 == 0) goto L_0x003e
            if (r6 != 0) goto L_0x0038
            X.1h7 r6 = new X.1h7
            r6.<init>()
            r11.put(r2, r6)
        L_0x0038:
            r6.A03 = r3
            r6.A05 = r12
            r6.A01 = r9
        L_0x003e:
            r7 = 0
            if (r13 != 0) goto L_0x005f
            if (r5 == 0) goto L_0x005f
            if (r6 == 0) goto L_0x004b
            androidx.fragment.app.Fragment r0 = r6.A02
            if (r0 != r3) goto L_0x004b
            r6.A02 = r7
        L_0x004b:
            X.0qW r5 = r9.A02
            int r0 = r3.A0F
            if (r0 >= r4) goto L_0x005f
            int r0 = r5.A01
            if (r0 < r4) goto L_0x005f
            boolean r0 = r9.A0F
            if (r0 != 0) goto L_0x005f
            r5.A0m(r3)
            r5.A0s(r3, r4)
        L_0x005f:
            if (r1 == 0) goto L_0x0077
            if (r6 == 0) goto L_0x0067
            androidx.fragment.app.Fragment r0 = r6.A02
            if (r0 != 0) goto L_0x0077
        L_0x0067:
            if (r6 != 0) goto L_0x0071
            X.1h7 r6 = new X.1h7
            r6.<init>()
            r11.put(r2, r6)
        L_0x0071:
            r6.A02 = r3
            r6.A04 = r12
            r6.A00 = r9
        L_0x0077:
            if (r13 != 0) goto L_0x0083
            if (r8 == 0) goto L_0x0083
            if (r6 == 0) goto L_0x0083
            androidx.fragment.app.Fragment r0 = r6.A03
            if (r0 != r3) goto L_0x0083
            r6.A03 = r7
        L_0x0083:
            return
        L_0x0084:
            if (r13 == 0) goto L_0x0093
            boolean r0 = r3.A0c
            if (r0 == 0) goto L_0x00dd
            boolean r0 = r3.A0b
            if (r0 != 0) goto L_0x00dd
            boolean r0 = r3.A0W
            if (r0 == 0) goto L_0x00dd
            goto L_0x00db
        L_0x0093:
            boolean r0 = r3.A0b
            goto L_0x00d0
        L_0x0096:
            if (r13 == 0) goto L_0x00c1
            boolean r1 = r3.A0c
            if (r1 == 0) goto L_0x00ca
            boolean r1 = r3.A0W
            if (r1 == 0) goto L_0x00ca
            boolean r1 = r3.A0b
            if (r1 == 0) goto L_0x00ca
            goto L_0x00bc
        L_0x00a5:
            if (r13 == 0) goto L_0x00c1
            boolean r1 = r3.A0W
            if (r1 != 0) goto L_0x00ca
            android.view.View r1 = r3.A0I
            if (r1 == 0) goto L_0x00ca
            int r1 = r1.getVisibility()
            if (r1 != 0) goto L_0x00ca
            float r5 = r3.A0B
            r1 = 0
            int r1 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r1 < 0) goto L_0x00ca
        L_0x00bc:
            r1 = 1
        L_0x00bd:
            r5 = 0
            r8 = 1
            goto L_0x0026
        L_0x00c1:
            boolean r1 = r3.A0W
            if (r1 == 0) goto L_0x00ca
            boolean r1 = r3.A0b
            if (r1 != 0) goto L_0x00ca
            goto L_0x00bc
        L_0x00ca:
            r1 = 0
            goto L_0x00bd
        L_0x00cc:
            if (r13 == 0) goto L_0x00d3
            boolean r0 = r3.A08
        L_0x00d0:
            r5 = 1
            goto L_0x0024
        L_0x00d3:
            boolean r0 = r3.A0W
            if (r0 != 0) goto L_0x00dd
            boolean r0 = r3.A0b
            if (r0 != 0) goto L_0x00dd
        L_0x00db:
            r0 = 1
            goto L_0x00d0
        L_0x00dd:
            r0 = 0
            goto L_0x00d0
        L_0x00df:
            int r5 = r10.A00
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16320wr.A08(X.0wn, X.1gT, android.util.SparseArray, boolean, boolean):void");
    }

    public static void A09(ArrayList arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                ((View) arrayList.get(size)).setVisibility(i);
            }
        }
    }

    private static AnonymousClass04a A02(C16340wt r3, AnonymousClass04a r4, Object obj, C29871h7 r6) {
        ArrayList arrayList;
        if (r4.isEmpty() || obj == null) {
            r4.clear();
            return null;
        }
        Fragment fragment = r6.A02;
        AnonymousClass04a r2 = new AnonymousClass04a();
        r3.A0K(r2, fragment.A14());
        C16280wn r1 = r6.A00;
        if (r6.A04) {
            arrayList = r1.A0E;
        } else {
            arrayList = r1.A0D;
        }
        if (arrayList != null) {
            r2.A0C(arrayList);
        }
        r4.A0C(r2.keySet());
        return r2;
    }

    private static void A0A(ArrayList arrayList, AnonymousClass04a r4, Collection collection) {
        for (int size = r4.size() - 1; size >= 0; size--) {
            View view = (View) r4.A09(size);
            if (collection.contains(C15320v6.getTransitionName(view))) {
                arrayList.add(view);
            }
        }
    }

    public static void A07(Context context, C27711dd r45, ArrayList arrayList, ArrayList arrayList2, int i, int i2, boolean z, C27771dj r51) {
        ArrayList arrayList3;
        ArrayList arrayList4;
        ViewGroup viewGroup;
        Object obj;
        Object obj2;
        ArrayList arrayList5;
        Object obj3;
        Object obj4;
        Rect rect;
        ArrayList arrayList6;
        ArrayList arrayList7;
        ArrayList arrayList8;
        SparseArray sparseArray = new SparseArray();
        int i3 = i;
        int i4 = i3;
        while (true) {
            boolean z2 = z;
            arrayList3 = arrayList;
            arrayList4 = arrayList2;
            if (i4 >= i2) {
                break;
            }
            C16280wn r7 = (C16280wn) arrayList3.get(i4);
            if (!((Boolean) arrayList4.get(i4)).booleanValue()) {
                int size = r7.A0C.size();
                for (int i5 = 0; i5 < size; i5++) {
                    A08(r7, (C29471gT) r7.A0C.get(i5), sparseArray, false, z2);
                }
            } else if (r7.A02.A05.A01()) {
                for (int size2 = r7.A0C.size() - 1; size2 >= 0; size2--) {
                    A08(r7, (C29471gT) r7.A0C.get(size2), sparseArray, true, z2);
                }
            }
            i4++;
        }
        if (sparseArray.size() != 0) {
            View view = new View(context);
            int size3 = sparseArray.size();
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i7 < size3) {
                    int keyAt = sparseArray.keyAt(i7);
                    AnonymousClass04a r11 = new AnonymousClass04a();
                    for (int i8 = i2 - 1; i8 >= i3; i8--) {
                        C16280wn r2 = (C16280wn) arrayList3.get(i8);
                        if (r2.A0Q(keyAt)) {
                            boolean booleanValue = ((Boolean) arrayList4.get(i8)).booleanValue();
                            ArrayList arrayList9 = r2.A0D;
                            if (arrayList9 != null) {
                                int size4 = arrayList9.size();
                                if (booleanValue) {
                                    arrayList8 = r2.A0D;
                                    arrayList7 = r2.A0E;
                                } else {
                                    arrayList7 = r2.A0D;
                                    arrayList8 = r2.A0E;
                                }
                                for (int i9 = 0; i9 < size4; i9++) {
                                    String str = (String) arrayList7.get(i9);
                                    String str2 = (String) arrayList8.get(i9);
                                    String str3 = (String) r11.remove(str2);
                                    if (str3 != null) {
                                        r11.put(str, str3);
                                    } else {
                                        r11.put(str, str2);
                                    }
                                }
                            }
                        }
                    }
                    C29871h7 r12 = (C29871h7) sparseArray.valueAt(i6);
                    C27711dd r1 = r45;
                    if (r1.A01() && (viewGroup = (ViewGroup) r1.A00(keyAt)) != null) {
                        C27771dj r39 = r51;
                        if (z) {
                            Fragment fragment = r12.A03;
                            Fragment fragment2 = r12.A02;
                            C16340wt A03 = A03(fragment2, fragment);
                            if (A03 != null) {
                                boolean z3 = r12.A05;
                                boolean z4 = r12.A04;
                                ArrayList arrayList10 = new ArrayList();
                                ArrayList arrayList11 = new ArrayList();
                                Object A04 = A04(A03, fragment, z3);
                                Object A05 = A05(A03, fragment2, z4);
                                ArrayList arrayList12 = arrayList11;
                                View view2 = view;
                                Fragment fragment3 = r12.A03;
                                Fragment fragment4 = r12.A02;
                                if (fragment3 != null) {
                                    fragment3.A14().setVisibility(0);
                                }
                                View view3 = null;
                                if (!(fragment3 == null || fragment4 == null)) {
                                    boolean z5 = r12.A05;
                                    if (r11.isEmpty()) {
                                        obj3 = null;
                                    } else if (fragment3 == null || fragment4 == null) {
                                        obj3 = null;
                                    } else {
                                        if (z5) {
                                            obj4 = fragment4.A19();
                                        } else {
                                            obj4 = null;
                                        }
                                        obj3 = A03.A05(A03.A04(obj4));
                                    }
                                    AnonymousClass04a A022 = A02(A03, r11, obj3, r12);
                                    AnonymousClass04a A012 = A01(A03, r11, obj3, r12);
                                    if (r11.isEmpty()) {
                                        if (A022 != null) {
                                            A022.clear();
                                        }
                                        if (A012 != null) {
                                            A012.clear();
                                        }
                                        obj3 = null;
                                    } else {
                                        A0A(arrayList11, A022, r11.keySet());
                                        A0A(arrayList10, A012, r11.values());
                                    }
                                    if (!(A04 == null && A05 == null && obj3 == 0)) {
                                        if (obj3 != null) {
                                            arrayList10.add(view2);
                                            C16340wt r26 = A03;
                                            Object obj5 = obj3;
                                            r26.A0E(obj5, view2, arrayList11);
                                            boolean z6 = r12.A04;
                                            C16280wn r14 = r12.A00;
                                            ArrayList arrayList13 = r14.A0D;
                                            if (arrayList13 != null && !arrayList13.isEmpty()) {
                                                if (z6) {
                                                    arrayList6 = r14.A0E;
                                                } else {
                                                    arrayList6 = r14.A0D;
                                                }
                                                View view4 = (View) A022.get((String) arrayList6.get(0));
                                                r26.A0C(obj5, view4);
                                                if (A05 != null) {
                                                    r26.A0C(A05, view4);
                                                }
                                            }
                                            rect = new Rect();
                                            view3 = A00(A012, r12, A04, z5);
                                            if (view3 != null) {
                                                A03.A09(A04, rect);
                                            }
                                        } else {
                                            rect = null;
                                        }
                                        C206349o5.A01(viewGroup, new AnonymousClass54E(fragment3, fragment4, z5, A012, view3, rect));
                                        view3 = obj3;
                                    }
                                }
                                Object obj6 = A04;
                                if (A04 != null || view3 != null || A05 != null) {
                                    C16340wt r122 = A03;
                                    View view5 = view;
                                    ArrayList A06 = A06(r122, A05, fragment2, arrayList12, view5);
                                    ArrayList A062 = A06(r122, obj6, fragment, arrayList10, view5);
                                    A09(A062, 4);
                                    Object obj7 = A05;
                                    Object obj8 = obj6;
                                    if (A04 == null || A05 == null || fragment == null || z3) {
                                    }
                                    Object A063 = A03.A06(obj7, obj8, view3);
                                    if (!(fragment2 == null || A06 == null || (A06.size() <= 0 && arrayList12.size() <= 0))) {
                                        C29702EgP egP = new C29702EgP();
                                        r39.BpR(fragment2, egP);
                                        A03.A08(fragment2, A063, egP, new C29700EgN(r39, fragment2, egP));
                                    }
                                    if (A063 != null) {
                                        Object obj9 = A05;
                                        if (fragment2 != null && A05 != null && fragment2.A0W && fragment2.A0b && fragment2.A0c) {
                                            Fragment.A01(fragment2).A09 = true;
                                            A03.A0D(obj9, fragment2.A0I, A06);
                                            C206349o5.A01(fragment2.A0J, new AnonymousClass54F(A06));
                                        }
                                        ArrayList arrayList14 = new ArrayList();
                                        int size5 = arrayList10.size();
                                        for (int i10 = 0; i10 < size5; i10++) {
                                            View view6 = (View) arrayList10.get(i10);
                                            arrayList14.add(C15320v6.getTransitionName(view6));
                                            C15320v6.setTransitionName(view6, null);
                                        }
                                        A03.A0F(A063, obj6, A062, A05, A06, view3, arrayList10);
                                        A03.A07(viewGroup, A063);
                                        ArrayList arrayList15 = arrayList12;
                                        int size6 = arrayList10.size();
                                        ArrayList arrayList16 = new ArrayList();
                                        for (int i11 = 0; i11 < size6; i11++) {
                                            View view7 = (View) arrayList15.get(i11);
                                            String transitionName = C15320v6.getTransitionName(view7);
                                            arrayList16.add(transitionName);
                                            if (transitionName != null) {
                                                C15320v6.setTransitionName(view7, null);
                                                String str4 = (String) r11.get(transitionName);
                                                int i12 = 0;
                                                while (true) {
                                                    if (i12 >= size6) {
                                                        break;
                                                    } else if (str4.equals(arrayList14.get(i12))) {
                                                        C15320v6.setTransitionName((View) arrayList10.get(i12), transitionName);
                                                        break;
                                                    } else {
                                                        i12++;
                                                    }
                                                }
                                            }
                                        }
                                        C206349o5.A01(viewGroup, new AnonymousClass2F6(size6, arrayList10, arrayList14, arrayList15, arrayList16));
                                        A09(A062, 0);
                                        A03.A0I(view3, arrayList15, arrayList10);
                                    }
                                }
                            }
                        } else {
                            Fragment fragment5 = r12.A03;
                            Fragment fragment6 = r12.A02;
                            C16340wt A032 = A03(fragment6, fragment5);
                            if (A032 != null) {
                                boolean z7 = r12.A05;
                                boolean z8 = r12.A04;
                                Object A042 = A04(A032, fragment5, z7);
                                Object A052 = A05(A032, fragment6, z8);
                                ArrayList arrayList17 = new ArrayList();
                                ArrayList arrayList18 = new ArrayList();
                                C16340wt r6 = A032;
                                ArrayList arrayList19 = arrayList18;
                                Fragment fragment7 = r12.A03;
                                Fragment fragment8 = r12.A02;
                                Rect rect2 = null;
                                if (!(fragment7 == null || fragment8 == null)) {
                                    boolean z9 = r12.A05;
                                    if (r11.isEmpty()) {
                                        obj = null;
                                    } else if (fragment7 == null || fragment8 == null) {
                                        obj = null;
                                    } else {
                                        if (z9) {
                                            obj2 = fragment8.A19();
                                        } else {
                                            obj2 = null;
                                        }
                                        obj = A032.A05(A032.A04(obj2));
                                    }
                                    AnonymousClass04a A023 = A02(A032, r11, obj, r12);
                                    if (r11.isEmpty()) {
                                        obj = null;
                                    } else {
                                        arrayList17.addAll(A023.values());
                                    }
                                    if (!(A042 == null && A052 == null && obj == 0)) {
                                        if (obj != null) {
                                            rect2 = new Rect();
                                            A032.A0E(obj, view, arrayList17);
                                            boolean z10 = r12.A04;
                                            C16280wn r3 = r12.A00;
                                            ArrayList arrayList20 = r3.A0D;
                                            if (arrayList20 != null && !arrayList20.isEmpty()) {
                                                if (z10) {
                                                    arrayList5 = r3.A0E;
                                                } else {
                                                    arrayList5 = r3.A0D;
                                                }
                                                View view8 = (View) A023.get((String) arrayList5.get(0));
                                                A032.A0C(obj, view8);
                                                if (A052 != null) {
                                                    A032.A0C(A052, view8);
                                                }
                                            }
                                            if (A042 != null) {
                                                A032.A09(A042, rect2);
                                            }
                                        }
                                        AnonymousClass54C r0 = r26;
                                        AnonymousClass54C r262 = new AnonymousClass54C(A032, r11, obj, r12, arrayList19, view, fragment7, fragment8, z9, arrayList17, A042, rect2);
                                        C206349o5.A01(viewGroup, r0);
                                        rect2 = obj;
                                    }
                                }
                                Object obj10 = A042;
                                if (A042 != null || rect2 != null || A052 != null) {
                                    View view9 = view;
                                    ArrayList A064 = A06(r6, A052, fragment6, arrayList17, view9);
                                    if (A064 == null || A064.isEmpty()) {
                                        A052 = null;
                                    }
                                    r6.A0A(obj10, view9);
                                    boolean z11 = r12.A05;
                                    Rect rect3 = rect2;
                                    if (A042 == null || A052 == null || fragment5 == null || z11) {
                                    }
                                    Object A065 = r6.A06(A052, obj10, rect3);
                                    if (!(fragment6 == null || A064 == null || (A064.size() <= 0 && arrayList17.size() <= 0))) {
                                        C29702EgP egP2 = new C29702EgP();
                                        C27771dj r32 = r39;
                                        r32.BpR(fragment6, egP2);
                                        r6.A08(fragment6, A065, egP2, new C29701EgO(r32, fragment6, egP2));
                                    }
                                    if (A065 != null) {
                                        ArrayList arrayList21 = new ArrayList();
                                        r6.A0F(A065, obj10, arrayList21, A052, A064, rect2, arrayList18);
                                        C206349o5.A01(viewGroup, new AnonymousClass54D(obj10, r6, view, fragment5, arrayList18, arrayList21, A064, A052));
                                        C206349o5.A01(viewGroup, new C98664nd(arrayList18, r11));
                                        r6.A07(viewGroup, A065);
                                        C206349o5.A01(viewGroup, new AnonymousClass2F8(arrayList18, r11));
                                    }
                                }
                            }
                        }
                    }
                    i6++;
                } else {
                    return;
                }
            }
        }
    }
}
