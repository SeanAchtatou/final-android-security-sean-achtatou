package X;

import com.facebook.messaging.inbox2.analytics.InboxSourceLoggingData;

/* renamed from: X.1Bg  reason: invalid class name and case insensitive filesystem */
public final class C20191Bg {
    public Integer A00;
    public String A01;
    public String A02;

    public C20191Bg() {
    }

    public C20191Bg(InboxSourceLoggingData inboxSourceLoggingData) {
        this.A02 = inboxSourceLoggingData.A02;
        this.A00 = inboxSourceLoggingData.A00;
        this.A01 = inboxSourceLoggingData.A01;
    }
}
