package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

/* renamed from: X.1Tm  reason: invalid class name and case insensitive filesystem */
public final class C24401Tm implements ServiceConnection {
    public final /* synthetic */ C24391Tl A00;

    public C24401Tm(C24391Tl r1) {
        this.A00 = r1;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.A00.A05.A01(new C34431pZ(this.A00.A04.now(), "ServiceConnected (MqttPushServiceManager)", new Object[0]));
        C24391Tl r3 = this.A00;
        r3.A00 = true;
        AnonymousClass00S.A04(r3.A03, new C34441pa(r3), -1399533619);
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.A00.A05.A01(new C34431pZ(this.A00.A04.now(), "ServiceDisconnected (MqttPushServiceManager)", new Object[0]));
        this.A00.A00 = false;
    }
}
