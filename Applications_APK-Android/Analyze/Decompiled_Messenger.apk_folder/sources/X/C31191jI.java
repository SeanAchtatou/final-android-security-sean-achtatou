package X;

import android.net.Uri;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.proxygen.TraceFieldType;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;

/* renamed from: X.1jI  reason: invalid class name and case insensitive filesystem */
public final class C31191jI {
    public final FbSharedPreferences A00;
    public final AnonymousClass1Y7 A01;
    public final AnonymousClass1Y7 A02;
    public final AnonymousClass1Y7 A03;
    public final AnonymousClass1Y7 A04;
    public final AnonymousClass1Y7 A05;
    public final AnonymousClass1Y7 A06;
    public final AnonymousClass1Y7 A07;
    public final AnonymousClass1Y7 A08;
    public final AnonymousClass1Y7 A09;
    public final AnonymousClass1Y7 A0A;
    public final AnonymousClass1Y7 A0B = ((AnonymousClass1Y7) this.A0D.A09("times_requested"));
    public final AnonymousClass1Y7 A0C;
    public final AnonymousClass1Y7 A0D;

    public synchronized Optional A00() {
        Optional of;
        Preconditions.checkState(this.A00.BFQ());
        String B4F = this.A00.B4F(this.A0C, null);
        if (B4F == null) {
            return Optional.absent();
        }
        long At2 = this.A00.At2(this.A09, -1);
        Uri parse = Uri.parse(B4F);
        int AqN = this.A00.AqN(this.A0B, 0);
        int AqN2 = this.A00.AqN(this.A01, 0);
        long At22 = this.A00.At2(this.A04, 0);
        boolean Aep = this.A00.Aep(this.A0A, false);
        String B4F2 = this.A00.B4F(this.A02, BuildConfig.FLAVOR);
        String B4F3 = this.A00.B4F(this.A03, BuildConfig.FLAVOR);
        if (At2 == -1) {
            of = Optional.absent();
        } else {
            of = Optional.of(Long.valueOf(At2));
        }
        return Optional.of(new C55702oW(parse, AqN, AqN2, At22, Aep, B4F2, B4F3, of, this.A00.B4F(this.A06, BuildConfig.FLAVOR), this.A00.B4F(this.A07, BuildConfig.FLAVOR), this.A00.B4F(this.A08, BuildConfig.FLAVOR), this.A00.B4F(this.A05, BuildConfig.FLAVOR)));
    }

    public C31191jI(FbSharedPreferences fbSharedPreferences, String str) {
        this.A00 = fbSharedPreferences;
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) AnonymousClass1MH.A00.A09(AnonymousClass08S.A0J(str, "EFFICIENCY_QPL"));
        this.A0D = r1;
        this.A0C = (AnonymousClass1Y7) r1.A09("KEY_URI");
        this.A0D.A09("tracking_duration");
        this.A01 = (AnonymousClass1Y7) this.A0D.A09(TraceFieldType.Uri);
        this.A04 = (AnonymousClass1Y7) this.A0D.A09("fetch_time_ms");
        this.A0A = (AnonymousClass1Y7) this.A0D.A09("is_prefetch");
        this.A02 = (AnonymousClass1Y7) this.A0D.A09("fetch_calling_class");
        this.A03 = (AnonymousClass1Y7) this.A0D.A09("fetch_endpoint");
        this.A09 = (AnonymousClass1Y7) this.A0D.A09("first_ui_time");
        this.A06 = (AnonymousClass1Y7) this.A0D.A09("first_ui_calling_class");
        this.A07 = (AnonymousClass1Y7) this.A0D.A09("first_ui_context_chain");
        this.A08 = (AnonymousClass1Y7) this.A0D.A09("first_ui_endpoint");
        this.A05 = (AnonymousClass1Y7) this.A0D.A09("first_ui_callback_source");
    }
}
