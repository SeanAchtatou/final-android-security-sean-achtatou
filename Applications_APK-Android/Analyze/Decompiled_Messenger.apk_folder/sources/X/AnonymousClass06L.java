package X;

import android.content.Context;

/* renamed from: X.06L  reason: invalid class name */
public final class AnonymousClass06L extends AnonymousClass0Wl {
    private AnonymousClass0UN A00;

    public String getSimpleName() {
        return "ProfiloBridgeFactoryImpl";
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.AjG, r1);
    }

    public static final AnonymousClass06L A01(AnonymousClass1XY r1) {
        return new AnonymousClass06L(r1);
    }

    public AnonymousClass0Qc A02() {
        return ((AnonymousClass07E) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AiV, this.A00)).A01();
    }

    public AnonymousClass09H[] A03() {
        return ((AnonymousClass07E) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AiV, this.A00)).A02();
    }

    private AnonymousClass06L(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public void init() {
        int A03 = C000700l.A03(1887535152);
        C004505k A002 = C004505k.A00();
        synchronized (A002) {
            A002.A05 = this;
            AnonymousClass09H[] A032 = A03();
            if (A032 != null) {
                for (AnonymousClass09H add : A032) {
                    A002.A09.A00.add(add);
                }
            }
        }
        int i = AnonymousClass1Y3.BCt;
        AnonymousClass0UN r4 = this.A00;
        AnonymousClass073.A00((Context) AnonymousClass1XX.A02(1, i, r4), true, ((C001300x) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B3a, r4)).A00.A00());
        C000700l.A09(-879784722, A03);
    }
}
