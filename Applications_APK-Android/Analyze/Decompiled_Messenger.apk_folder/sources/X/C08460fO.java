package X;

/* renamed from: X.0fO  reason: invalid class name and case insensitive filesystem */
public final class C08460fO {
    public int A00;
    public long A01;
    public final int A02;

    public void A00(int i) {
        int i2 = this.A02;
        if (i >= i2) {
            this.A01 += (long) i;
            this.A00++;
            C005505z.A05("StallTracker.onDetectedST%d", Integer.valueOf(i2), 548223350);
            C005505z.A00(1871188848);
        }
    }

    public /* bridge */ /* synthetic */ Object clone() {
        return new C08460fO(this.A02, this.A00, this.A01);
    }

    public C08460fO(int i, int i2, long j) {
        this.A02 = i;
        this.A00 = i2;
        this.A01 = j;
    }
}
