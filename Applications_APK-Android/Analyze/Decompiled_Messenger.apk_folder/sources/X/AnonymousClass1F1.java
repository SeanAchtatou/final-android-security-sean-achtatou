package X;

import android.net.Uri;
import android.text.TextUtils;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.graphql.query.GQSQStringShape0S0000000_I0;
import com.facebook.interstitial.api.FQLFetchInterstitialResult;
import com.facebook.interstitial.api.GraphQLInterstitialsResult;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1F1  reason: invalid class name */
public final class AnonymousClass1F1 implements C05460Za, CallerContextable {
    public static final Class A09 = AnonymousClass1F1.class;
    private static volatile AnonymousClass1F1 A0A = null;
    public static final String __redex_internal_original_name = "com.facebook.interstitial.manager.InterstitialManager";
    public int A00 = Integer.MIN_VALUE;
    public int A01 = Integer.MIN_VALUE;
    public AnonymousClass0UN A02;
    public String A03 = BuildConfig.FLAVOR;
    public boolean A04;
    public boolean A05;
    public final AnonymousClass1F2 A06 = new AnonymousClass1F2();
    public final Map A07;
    public final Map A08;

    private int A00() {
        this.A06.A00();
        try {
            if (this.A00 == Integer.MIN_VALUE) {
                this.A00 = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A02)).AqN(C21001Eq.A08, 0);
            }
            return this.A00;
        } finally {
            this.A06.A01();
        }
    }

    private C32821mO A01(InterstitialTrigger interstitialTrigger, Class cls) {
        int i;
        ArrayList A022;
        C005505z.A05("InterstitialManager#getBestInterstitialForTrigger(%s)", interstitialTrigger, 156420533);
        try {
            A0D(interstitialTrigger);
            A0E(interstitialTrigger, cls);
            C37281uz r3 = (C37281uz) this.A08.get(interstitialTrigger);
            C32821mO r7 = null;
            if (r3 != null) {
                Preconditions.checkArgument(r3.A05, StringFormatUtil.formatStrLocaleSafe("Trigger %s is not know to be fully restored!", r3.A00));
                synchronized (r3) {
                    A022 = C04300To.A02(r3.A04.size());
                    for (C70693b8 r0 : r3.A04) {
                        A022.add(r0.A01);
                    }
                }
                Iterator it = A022.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        i = 807422026;
                        break;
                    }
                    C71363cG r5 = (C71363cG) it.next();
                    C32821mO A002 = r5.A00();
                    QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BBd, this.A02);
                    quickPerformanceLogger.markerStart(196627);
                    quickPerformanceLogger.markerTag(196627, interstitialTrigger.toString());
                    quickPerformanceLogger.markerTag(196627, r5.A02);
                    Integer A062 = A06(this, r5, interstitialTrigger);
                    quickPerformanceLogger.markerEnd(196627, 2);
                    if (A062 == AnonymousClass07B.A00) {
                        if (cls.isInstance(A002)) {
                            r7 = A002;
                        }
                        i = 1577546221;
                    }
                }
            } else {
                i = 1727438292;
            }
            C005505z.A00(i);
            return r7;
        } catch (Throwable th) {
            C005505z.A00(-645495834);
            throw th;
        }
    }

    public static final AnonymousClass1F1 A03(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (AnonymousClass1F1.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new AnonymousClass1F1(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public static C71363cG A04(AnonymousClass1F1 r4, String str, InterstitialTrigger interstitialTrigger, Class cls) {
        C71363cG r1;
        r4.A06.A00();
        try {
            r4.A0D(interstitialTrigger);
            if (cls != null) {
                r4.A0E(interstitialTrigger, cls);
            }
            C37281uz r3 = (C37281uz) r4.A08.get(interstitialTrigger);
            if (r3 == null) {
                r1 = null;
            } else {
                Preconditions.checkArgument(r3.A05, StringFormatUtil.formatStrLocaleSafe("Trigger %s is not know to be fully restored!", r3.A00));
                synchronized (r3) {
                    Preconditions.checkArgument(r3.A05, "Before checking contain trigger controllers must be known to be fully restored!");
                    C70693b8 r0 = (C70693b8) r3.A03.get(str);
                    if (r0 == null) {
                        r1 = null;
                    } else {
                        r1 = r0.A01;
                    }
                }
            }
            r4.A06.A01();
            return r1;
        } catch (Throwable th) {
            r4.A06.A01();
            throw th;
        }
    }

    public static C37281uz A05(AnonymousClass1F1 r2, InterstitialTrigger interstitialTrigger, String str) {
        C37281uz r1 = (C37281uz) r2.A08.get(interstitialTrigger);
        if (r1 != null) {
            return r1;
        }
        C37281uz r12 = new C37281uz(interstitialTrigger, str);
        r2.A08.put(interstitialTrigger, r12);
        return r12;
    }

    public static Integer A06(AnonymousClass1F1 r10, C71363cG r11, InterstitialTrigger interstitialTrigger) {
        C70683b7 r3;
        if (r11 != null) {
            C32821mO A002 = r11.A00();
            synchronized (r11) {
                r3 = r11.A00;
            }
            boolean z = true;
            if (A002 == null) {
                C010708t.A0B(A09, "Interstitial with id %s is not initialized!", r11.A02);
                return AnonymousClass07B.A0i;
            } else if (r3 != null) {
                long Auk = A002.Auk();
                if (Auk > 0) {
                    if (((AnonymousClass06B) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AgK, r10.A02)).now() < ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, r10.A02)).At2(C21001Eq.A09.A09(Uri.encode(A002.Aqc())), 0) + Auk) {
                        z = false;
                    }
                }
                if (!z) {
                    return AnonymousClass07B.A0Y;
                }
                int AtQ = r3.AtQ();
                if (AtQ > 0 && ((C57232re) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Asy, r10.A02)).A04(C21001Eq.A00, r11.A02) >= AtQ) {
                    return AnonymousClass07B.A0N;
                }
                if (A002.B3h(interstitialTrigger) != C70753bE.ELIGIBLE) {
                    return AnonymousClass07B.A0C;
                }
                return AnonymousClass07B.A00;
            }
        }
        return AnonymousClass07B.A01;
    }

    private String A07() {
        this.A06.A00();
        try {
            if (BuildConfig.FLAVOR.equals(this.A03)) {
                this.A03 = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A02)).B4F(C21001Eq.A06, BuildConfig.FLAVOR);
            }
            return this.A03;
        } finally {
            this.A06.A01();
        }
    }

    public static Set A08(AnonymousClass1F1 r12, List list) {
        if (list == null || list.isEmpty()) {
            return Collections.EMPTY_SET;
        }
        HashSet A032 = C25011Xz.A03();
        C005505z.A03("InterstitialManager#forceRestoreTriggerStateFromInsterstitialResult", -30583007);
        QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BBd, r12.A02);
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C70683b7 r9 = (C70683b7) it.next();
                String AwC = r9.AwC();
                quickPerformanceLogger.markerStart(196628);
                if (AwC != null) {
                    try {
                        quickPerformanceLogger.markerTag(196628, AwC);
                    } catch (Throwable th) {
                        quickPerformanceLogger.markerEnd(196628, 2);
                        throw th;
                    }
                }
                C32821mO A042 = ((C31511jo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQQ, r12.A02)).A04(AwC);
                if (A042 != null) {
                    r12.A06.A00();
                    String Aqc = A042.Aqc();
                    C71363cG r3 = (C71363cG) r12.A07.get(Aqc);
                    if (r3 == null) {
                        r3 = new C71363cG(A042);
                        r12.A07.put(Aqc, r3);
                    }
                    r12.A06.A01();
                    if (r3.A03(r9, (AnonymousClass09P) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Amr, r12.A02))) {
                        C24971Xv it2 = A042.B7C().iterator();
                        while (it2.hasNext()) {
                            C37281uz A052 = A05(r12, (InterstitialTrigger) it2.next(), AwC);
                            A052.A02(r3, r9.Azz());
                            A032.add(A052);
                        }
                    }
                }
                quickPerformanceLogger.markerEnd(196628, 2);
            }
            return A032;
        } finally {
            C005505z.A00(933062451);
        }
    }

    private void A09() {
        this.A06.A00();
        try {
            if (!this.A04) {
                C005505z.A03("InterstitialManager#maybeClearStaleData", 1358087918);
                if (!A0V()) {
                    this.A04 = true;
                    C005505z.A00(-239521431);
                } else {
                    A00();
                    A07();
                    new GQSQStringShape0S0000000_I0(0).A02();
                    C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A02)).edit();
                    AnonymousClass1F6.A01(edit);
                    A0F(edit);
                    A0H(edit);
                    edit.C1B(C64153Ag.A01.A09(C182438dK.class.getName()));
                    edit.commit();
                    this.A04 = true;
                    C005505z.A00(-175188191);
                }
            }
            this.A06.A01();
        } catch (Throwable th) {
            this.A06.A01();
            throw th;
        }
    }

    public static void A0A(AnonymousClass1F1 r3) {
        r3.A06.A00();
        try {
            C005505z.A03("InterstitialManager#RestoreInterstitialDataFromPreferences", -767440961);
            try {
                r3.A0K(((C31511jo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQQ, r3.A02)).A01());
                r3.A06.A00();
                for (C37281uz r1 : r3.A08.values()) {
                    r1.A05 = true;
                }
                C005505z.A00(456644584);
                r3.A06.A01();
            } catch (Throwable th) {
                C005505z.A00(339124821);
                throw th;
            }
        } finally {
            r3.A06.A01();
        }
    }

    public static void A0B(AnonymousClass1F1 r9, C30281hn r10, List list, Map map) {
        int i;
        String str;
        C005505z.A03("InterstitialManager#cacheInterstitialData", 1747042734);
        try {
            AnonymousClass1F6 r5 = (AnonymousClass1F6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ArB, r9.A02);
            synchronized (r5) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    C70683b7 r3 = (C70683b7) it.next();
                    AnonymousClass1Y7 r6 = (AnonymousClass1Y7) C21001Eq.A03.A09(Uri.encode(r3.AwC()));
                    AnonymousClass1Y7 r4 = (AnonymousClass1Y7) C21001Eq.A05.A09(Uri.encode(r3.AwC()));
                    try {
                        if (r3 instanceof FQLFetchInterstitialResult) {
                            i = 0;
                        } else if (r3 instanceof GraphQLInterstitialsResult) {
                            i = 1;
                        } else {
                            throw new IOException("Unknown Interstitial Result type: " + r3.getClass());
                        }
                        try {
                            str = ((AnonymousClass0jJ) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AmL, r5.A00)).writeValueAsString(r3);
                        } catch (Exception e) {
                            ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, r5.A00)).softReport("InterstitialRepository", "Failed to serialize interstitial data", e);
                            str = null;
                        }
                        if (str != null) {
                            r10.BzC(r6, str);
                            r10.Bz6(r4, i);
                        } else {
                            r10.C1B(r6);
                            r10.C1B(r4);
                        }
                    } catch (IOException e2) {
                        ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, r5.A00)).softReport("InterstitialRepository", "Failed to serialize interstitial data", e2);
                        r10.C1B(r6);
                        r10.C1B(r4);
                    }
                }
            }
            r9.A0I(r10, map);
            r9.A0F(r10);
            r9.A0G(r10);
            r9.A0H(r10);
        } finally {
            C005505z.A00(-1206392805);
        }
    }

    public static void A0C(AnonymousClass1F1 r3, Collection collection) {
        r3.A06.A00();
        if (collection != null) {
            try {
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    ((C37281uz) it.next()).A05 = true;
                }
            } catch (Throwable th) {
                r3.A06.A01();
                throw th;
            }
        }
        r3.A06.A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001a, code lost:
        if (r1 == false) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0D(com.facebook.interstitial.triggers.InterstitialTrigger r11) {
        /*
            r10 = this;
            X.1F2 r0 = r10.A06
            r0.A00()
            java.lang.String r1 = "InterstitialManager#restoreLazyTriggerIds"
            r0 = 459719265(0x1b66c261, float:1.9087961E-22)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x018e }
            java.util.Map r0 = r10.A08     // Catch:{ all -> 0x0186 }
            java.lang.Object r0 = r0.get(r11)     // Catch:{ all -> 0x0186 }
            X.1uz r0 = (X.C37281uz) r0     // Catch:{ all -> 0x0186 }
            if (r0 == 0) goto L_0x001c
            boolean r1 = r0.A05     // Catch:{ all -> 0x0186 }
            r0 = 1
            if (r1 != 0) goto L_0x001d
        L_0x001c:
            r0 = 0
        L_0x001d:
            if (r0 == 0) goto L_0x0024
            r0 = -460338329(0xffffffffe48fcb67, float:-2.1220329E22)
            goto L_0x0160
        L_0x0024:
            r10.A0S()     // Catch:{ all -> 0x0186 }
            r2 = 7
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0186 }
            X.0UN r0 = r10.A02     // Catch:{ all -> 0x0186 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0186 }
            com.facebook.quicklog.QuickPerformanceLogger r5 = (com.facebook.quicklog.QuickPerformanceLogger) r5     // Catch:{ all -> 0x0186 }
            r4 = 196630(0x30016, float:2.75537E-40)
            r5.markerStart(r4)     // Catch:{ all -> 0x0186 }
            r2 = 1
            int r1 = X.AnonymousClass1Y3.ArB     // Catch:{ all -> 0x0186 }
            X.0UN r0 = r10.A02     // Catch:{ all -> 0x0186 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0186 }
            X.1F6 r2 = (X.AnonymousClass1F6) r2     // Catch:{ all -> 0x0186 }
            monitor-enter(r2)     // Catch:{ all -> 0x0186 }
            X.1Y7 r1 = X.C21001Eq.A07     // Catch:{ all -> 0x0180 }
            java.lang.String r0 = r11.toString()     // Catch:{ all -> 0x0180 }
            java.lang.String r0 = android.net.Uri.encode(r0)     // Catch:{ all -> 0x0180 }
            X.063 r6 = r1.A09(r0)     // Catch:{ all -> 0x0180 }
            X.1Y7 r6 = (X.AnonymousClass1Y7) r6     // Catch:{ all -> 0x0180 }
            r3 = 1
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x0180 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0180 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0180 }
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1     // Catch:{ all -> 0x0180 }
            r0 = 0
            java.lang.String r8 = r1.B4F(r6, r0)     // Catch:{ all -> 0x0180 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0180 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0180 }
            r7 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0180 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0180 }
            r6 = 196631(0x30017, float:2.75539E-40)
            r0.markerStart(r6)     // Catch:{ all -> 0x0180 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x0180 }
            r3.<init>()     // Catch:{ all -> 0x0180 }
            if (r8 != 0) goto L_0x0080
            r3 = 0
        L_0x007d:
            if (r8 == 0) goto L_0x0099
            goto L_0x008e
        L_0x0080:
            java.lang.String r0 = "~"
            java.lang.String[] r0 = r8.split(r0)     // Catch:{ all -> 0x0180 }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ all -> 0x0180 }
            r3.addAll(r0)     // Catch:{ all -> 0x0180 }
            goto L_0x007d
        L_0x008e:
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0180 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0180 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x0180 }
            r0.markerTag(r6, r8)     // Catch:{ all -> 0x0180 }
        L_0x0099:
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0180 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0180 }
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1     // Catch:{ all -> 0x0180 }
            if (r3 != 0) goto L_0x00b7
            java.lang.String r0 = "[]"
        L_0x00a5:
            r1.markerTag(r6, r0)     // Catch:{ all -> 0x0180 }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0180 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0180 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0180 }
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1     // Catch:{ all -> 0x0180 }
            r0 = 2
            r1.markerEnd(r6, r0)     // Catch:{ all -> 0x0180 }
            goto L_0x00bc
        L_0x00b7:
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0180 }
            goto L_0x00a5
        L_0x00bc:
            monitor-exit(r2)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r11.toString()     // Catch:{ all -> 0x0186 }
            r5.markerTag(r4, r0)     // Catch:{ all -> 0x0186 }
            if (r3 != 0) goto L_0x00c7
            goto L_0x00cc
        L_0x00c7:
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0186 }
            goto L_0x00ce
        L_0x00cc:
            java.lang.String r0 = "[]"
        L_0x00ce:
            r5.markerTag(r4, r0)     // Catch:{ all -> 0x0186 }
            if (r3 != 0) goto L_0x0164
            java.util.Map r0 = r10.A08     // Catch:{ all -> 0x0186 }
            java.lang.Object r9 = r0.get(r11)     // Catch:{ all -> 0x0186 }
            X.1uz r9 = (X.C37281uz) r9     // Catch:{ all -> 0x0186 }
            if (r9 == 0) goto L_0x0159
            boolean r0 = r9.A05     // Catch:{ all -> 0x0186 }
            if (r0 != 0) goto L_0x0159
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x0186 }
            X.0UN r0 = r10.A02     // Catch:{ all -> 0x0186 }
            r8 = 3
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x0186 }
            X.09P r7 = (X.AnonymousClass09P) r7     // Catch:{ all -> 0x0186 }
            java.lang.String r6 = "InterstitialManagerBadTriggerMapping"
            java.lang.String r2 = "Inconsistent Interstitial Trigger %s state on disk. Debug Info: %s"
            monitor-enter(r9)     // Catch:{ all -> 0x0186 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x0183 }
            r3.<init>()     // Catch:{ all -> 0x0183 }
            java.util.SortedSet r0 = r9.A04     // Catch:{ all -> 0x0183 }
            if (r0 == 0) goto L_0x0112
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0183 }
        L_0x00fe:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0183 }
            if (r0 == 0) goto L_0x0112
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0183 }
            X.3b8 r0 = (X.C70693b8) r0     // Catch:{ all -> 0x0183 }
            X.3cG r0 = r0.A01     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = r0.A02     // Catch:{ all -> 0x0183 }
            r3.add(r0)     // Catch:{ all -> 0x0183 }
            goto L_0x00fe
        L_0x0112:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = "[Debug cause: "
            r1.<init>(r0)     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = r9.A01     // Catch:{ all -> 0x0183 }
            r1.append(r0)     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = ", currentInterstitials: "
            r1.append(r0)     // Catch:{ all -> 0x0183 }
            r1.append(r3)     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = "]"
            r1.append(r0)     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0183 }
            monitor-exit(r9)     // Catch:{ all -> 0x0186 }
            java.lang.String r3 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r2, r11, r0)     // Catch:{ all -> 0x0186 }
            java.lang.Throwable r2 = new java.lang.Throwable     // Catch:{ all -> 0x0186 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0186 }
            r1.<init>()     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = "Could not restore trigger "
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            r1.append(r11)     // Catch:{ all -> 0x0186 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0186 }
            java.lang.Throwable r0 = r9.A02     // Catch:{ all -> 0x0186 }
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0186 }
            r7.softReport(r6, r3, r2)     // Catch:{ all -> 0x0186 }
            A0A(r10)     // Catch:{ all -> 0x0186 }
            r5.markerEnd(r4, r8)     // Catch:{ all -> 0x0186 }
            r0 = -353551037(0xffffffffeaed3d43, float:-1.4340236E26)
            goto L_0x0160
        L_0x0159:
            r0 = 4
            r5.markerEnd(r4, r0)     // Catch:{ all -> 0x0186 }
            r0 = 1835947990(0x6d6e57d6, float:4.6102262E27)
        L_0x0160:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x018e }
            goto L_0x017a
        L_0x0164:
            java.lang.String r0 = "FromTriggerIds"
            X.1uz r1 = A05(r10, r11, r0)     // Catch:{ all -> 0x0186 }
            r10.A0K(r3)     // Catch:{ all -> 0x0186 }
            r0 = 1
            r1.A05 = r0     // Catch:{ all -> 0x0186 }
            r0 = 2
            r5.markerEnd(r4, r0)     // Catch:{ all -> 0x0186 }
            r0 = 801353535(0x2fc3af3f, float:3.5594813E-10)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x018e }
        L_0x017a:
            X.1F2 r0 = r10.A06
            r0.A01()
            return
        L_0x0180:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0186 }
            goto L_0x0185
        L_0x0183:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0186 }
        L_0x0185:
            throw r0     // Catch:{ all -> 0x0186 }
        L_0x0186:
            r1 = move-exception
            r0 = 111908459(0x6ab966b, float:6.454407E-35)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x018e }
            throw r1     // Catch:{ all -> 0x018e }
        L_0x018e:
            r1 = move-exception
            X.1F2 r0 = r10.A06
            r0.A01()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1F1.A0D(com.facebook.interstitial.triggers.InterstitialTrigger):void");
    }

    private void A0E(InterstitialTrigger interstitialTrigger, Class cls) {
        for (String A042 : ((C31511jo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQQ, this.A02)).A03(interstitialTrigger.action)) {
            C32821mO A043 = ((C31511jo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQQ, this.A02)).A04(A042);
            if (!C22281Ks.class.isInstance(A043)) {
                A043 = null;
            }
            C22281Ks r1 = (C22281Ks) A043;
            if (r1 != null && cls.isInstance(r1)) {
                r1.A00.A06(interstitialTrigger);
            }
        }
    }

    private void A0F(C30281hn r3) {
        this.A06.A00();
        try {
            r3.Bz6(C21001Eq.A08, 3);
            this.A00 = Integer.MIN_VALUE;
        } finally {
            this.A06.A01();
        }
    }

    private void A0G(C30281hn r5) {
        this.A06.A00();
        try {
            r5.Bz6(C21001Eq.A02, ((C09400hF) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMV, this.A02)).A01());
            this.A01 = Integer.MIN_VALUE;
        } finally {
            this.A06.A01();
        }
    }

    private void A0H(C30281hn r4) {
        this.A06.A00();
        try {
            r4.BzC(C21001Eq.A06, new GQSQStringShape0S0000000_I0(0).A02());
            this.A03 = BuildConfig.FLAVOR;
        } finally {
            this.A06.A01();
        }
    }

    private void A0J(Collection collection) {
        C70683b7 r1;
        this.A06.A00();
        if (collection != null) {
            try {
                if (!collection.isEmpty()) {
                    A09();
                    Preconditions.checkNotNull(collection);
                    ArrayList A002 = C04300To.A00();
                    QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BBd, this.A02);
                    quickPerformanceLogger.markerStart(196632);
                    Iterator it = collection.iterator();
                    ArrayList arrayList = null;
                    while (it.hasNext()) {
                        String str = (String) it.next();
                        AnonymousClass1F6 r9 = (AnonymousClass1F6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ArB, this.A02);
                        int i = AnonymousClass1Y3.B6q;
                        String B4F = ((FbSharedPreferences) AnonymousClass1XX.A02(1, i, r9.A00)).B4F((AnonymousClass1Y7) C21001Eq.A03.A09(Uri.encode(str)), null);
                        int AqN = ((FbSharedPreferences) AnonymousClass1XX.A02(1, i, r9.A00)).AqN((AnonymousClass1Y7) C21001Eq.A05.A09(Uri.encode(str)), 0);
                        if (TextUtils.isEmpty(B4F) || (r1 = r9.A03(str, B4F, AqN)) == null || !r1.isValid()) {
                            r1 = null;
                        }
                        if (r1 != null) {
                            A002.add(r1);
                        } else {
                            if (arrayList == null) {
                                arrayList = C04300To.A00();
                            }
                            arrayList.add(str);
                        }
                    }
                    quickPerformanceLogger.markerEnd(196632, 2);
                    A08(this, A002);
                }
            } catch (Throwable th) {
                this.A06.A01();
                throw th;
            }
        }
        this.A06.A01();
    }

    private void A0K(Collection collection) {
        this.A06.A00();
        if (collection != null) {
            try {
                if (!collection.isEmpty()) {
                    C005505z.A03("InterstitialManager#restoreControllerIdsIfNeeded", -992006402);
                    ArrayList A002 = C04300To.A00();
                    Iterator it = collection.iterator();
                    while (it.hasNext()) {
                        String str = (String) it.next();
                        if (!A0L(str)) {
                            A002.add(str);
                        }
                    }
                    A0J(A002);
                    C005505z.A00(685573606);
                }
            } catch (Throwable th) {
                this.A06.A01();
                throw th;
            }
        }
        this.A06.A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (r0.A02() == false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A0L(java.lang.String r3) {
        /*
            r2 = this;
            X.1F2 r0 = r2.A06
            r0.A00()
            java.util.Map r0 = r2.A07     // Catch:{ all -> 0x001d }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x001d }
            X.3cG r0 = (X.C71363cG) r0     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x0016
            boolean r0 = r0.A02()     // Catch:{ all -> 0x001d }
            r1 = 1
            if (r0 != 0) goto L_0x0017
        L_0x0016:
            r1 = 0
        L_0x0017:
            X.1F2 r0 = r2.A06
            r0.A01()
            return r1
        L_0x001d:
            r1 = move-exception
            X.1F2 r0 = r2.A06
            r0.A01()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1F1.A0L(java.lang.String):boolean");
    }

    public C32821mO A0M(InterstitialTrigger interstitialTrigger) {
        this.A06.A00();
        try {
            return A0N(interstitialTrigger, C32821mO.class);
        } finally {
            this.A06.A01();
        }
    }

    public C32821mO A0N(InterstitialTrigger interstitialTrigger, Class cls) {
        this.A06.A00();
        try {
            C32821mO A012 = A01(interstitialTrigger, cls);
            if (A012 != null) {
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BBd, this.A02)).markerTag(2293779, AnonymousClass08S.A0J("interstitial=", A012.Aqc()));
                A0T(A012);
            }
            return A012;
        } finally {
            this.A06.A01();
        }
    }

    public C32821mO A0O(InterstitialTrigger interstitialTrigger, Class cls) {
        this.A06.A00();
        try {
            return A01(interstitialTrigger, cls);
        } finally {
            this.A06.A01();
        }
    }

    public C32821mO A0P(String str) {
        this.A06.A00();
        try {
            return A0Q(str, C32821mO.class);
        } finally {
            this.A06.A01();
        }
    }

    /* JADX INFO: finally extract failed */
    public C32821mO A0Q(String str, Class cls) {
        this.A06.A00();
        try {
            C005505z.A03("InterstitialManager#getInterstitialControllerForId", 980462825);
            try {
                C32821mO A042 = ((C31511jo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQQ, this.A02)).A04(str);
                if (!cls.isInstance(A042)) {
                    A042 = null;
                }
                if (A042 != null) {
                    this.A06.A00();
                    try {
                        Preconditions.checkNotNull(A042);
                        C005505z.A03("InterstitialManager#restoreControllersIfNeeded", -420122368);
                        String Aqc = A042.Aqc();
                        if (!A0L(Aqc)) {
                            A0J(Collections.singletonList(Aqc));
                        }
                        C005505z.A00(385183967);
                        this.A06.A01();
                    } catch (Throwable th) {
                        this.A06.A01();
                        throw th;
                    }
                }
                C005505z.A00(-120102493);
                this.A06.A01();
                return A042;
            } catch (Throwable th2) {
                C005505z.A00(957026890);
                throw th2;
            }
        } catch (Throwable th3) {
            this.A06.A01();
            throw th3;
        }
    }

    public C148576v0 A0R() {
        this.A06.A00();
        try {
            return (C148576v0) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ABd, this.A02);
        } finally {
            this.A06.A01();
        }
    }

    /* JADX INFO: finally extract failed */
    public void A0S() {
        this.A06.A00();
        try {
            A09();
            this.A06.A00();
            try {
                if (!this.A05) {
                    this.A06.A00();
                    if (this.A01 == Integer.MIN_VALUE) {
                        this.A01 = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A02)).AqN(C21001Eq.A02, Integer.MIN_VALUE);
                    }
                    int i = this.A01;
                    this.A06.A01();
                    if (i == ((C09400hF) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMV, this.A02)).A01()) {
                        this.A05 = true;
                    } else {
                        C005505z.A03("InterstitialManager#forceOnAppUpgrade", -282353215);
                        try {
                            C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A02)).edit();
                            A0A(this);
                            A0I(edit, this.A08);
                            A0G(edit);
                            edit.commit();
                            this.A05 = true;
                            C005505z.A00(-200783948);
                        } catch (Throwable th) {
                            th = th;
                            C005505z.A00(-1438396719);
                            throw th;
                        }
                    }
                }
                this.A06.A01();
                this.A06.A01();
            } catch (Throwable th2) {
                this.A06.A01();
                throw th2;
            }
        } catch (Throwable th3) {
            this.A06.A01();
            throw th3;
        }
    }

    /* JADX INFO: finally extract failed */
    public void A0U(List list) {
        this.A06.A00();
        if (list != null) {
            try {
                C005505z.A03("InterstitialManager#resetEligibleInterstitialsWithFetchResults", 932777125);
                try {
                    C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A02)).edit();
                    this.A06.A00();
                    AnonymousClass1F6.A01(edit);
                    this.A06.A01();
                    this.A08.clear();
                    A0C(this, A08(this, list));
                    A0B(this, edit, list, this.A08);
                    edit.commit();
                    C005505z.A00(1883244582);
                } catch (Throwable th) {
                    C005505z.A00(-1584339916);
                    throw th;
                }
            } catch (Throwable th2) {
                this.A06.A01();
                throw th2;
            }
        }
        this.A06.A01();
    }

    public boolean A0V() {
        this.A06.A00();
        try {
            boolean z = false;
            if (!this.A04) {
                int A002 = A00();
                String A072 = A07();
                if (A002 != 3 || !new GQSQStringShape0S0000000_I0(0).A02().equals(A072)) {
                    z = true;
                }
            }
            return z;
        } finally {
            this.A06.A01();
        }
    }

    public boolean A0W(C32821mO r4, InterstitialTrigger interstitialTrigger) {
        this.A06.A00();
        try {
            C005505z.A03("InterstitialManager#canShowInterstitialForTrigger", -670039767);
            boolean z = false;
            if (A06(this, A04(this, r4.Aqc(), interstitialTrigger, r4.getClass()), interstitialTrigger) == AnonymousClass07B.A00) {
                z = true;
            }
            C005505z.A00(-1627703527);
            this.A06.A01();
            return z;
        } catch (Throwable th) {
            this.A06.A01();
            throw th;
        }
    }

    public boolean A0X(InterstitialTrigger interstitialTrigger, Class cls) {
        this.A06.A00();
        try {
            C005505z.A03(AnonymousClass08S.A0J("InterstitialManager#hasInterstitialForTrigger ", interstitialTrigger.toString()), -2049918409);
            boolean z = false;
            if (A01(interstitialTrigger, cls) != null) {
                z = true;
            }
            C005505z.A00(1818039765);
            this.A06.A01();
            return z;
        } catch (Throwable th) {
            this.A06.A01();
            throw th;
        }
    }

    public boolean A0Y(String str) {
        this.A06.A00();
        try {
            return this.A07.containsKey(str);
        } finally {
            this.A06.A01();
        }
    }

    public void clearUserData() {
        this.A06.A00();
        try {
            this.A08.clear();
            this.A07.clear();
        } finally {
            this.A06.A01();
        }
    }

    private AnonymousClass1F1(AnonymousClass1XY r3) {
        this.A02 = new AnonymousClass0UN(10, r3);
        this.A08 = AnonymousClass0TG.A04();
        this.A07 = AnonymousClass0TG.A04();
    }

    public static final AnonymousClass1F1 A02(AnonymousClass1XY r0) {
        return A03(r0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:85:0x015d A[Catch:{ all -> 0x0169 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0161 A[Catch:{ all -> 0x0169 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0I(X.C30281hn r13, java.util.Map r14) {
        /*
            r12 = this;
            java.util.Set r4 = r14.keySet()
            int r1 = X.AnonymousClass1Y3.ArB
            X.0UN r0 = r12.A02
            r2 = 1
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1F6 r3 = (X.AnonymousClass1F6) r3
            monitor-enter(r3)
            r11 = r3
            monitor-enter(r11)     // Catch:{ all -> 0x0177 }
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x0174 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x0174 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0174 }
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1     // Catch:{ all -> 0x0174 }
            X.1Y7 r0 = X.C21001Eq.A01     // Catch:{ all -> 0x0174 }
            r5 = 0
            java.lang.String r1 = r1.B4F(r0, r5)     // Catch:{ all -> 0x0174 }
            java.util.ArrayList r6 = X.C04300To.A00()     // Catch:{ all -> 0x0174 }
            if (r1 == 0) goto L_0x006f
            java.lang.String r0 = "~"
            java.lang.String[] r9 = r1.split(r0)     // Catch:{ all -> 0x0174 }
            int r8 = r9.length     // Catch:{ all -> 0x0174 }
            r7 = 0
        L_0x0031:
            if (r7 >= r8) goto L_0x006f
            r10 = r9[r7]     // Catch:{ all -> 0x0174 }
            if (r10 != 0) goto L_0x0039
            r2 = r5
            goto L_0x004a
        L_0x0039:
            java.lang.String r0 = ":"
            int r1 = r10.lastIndexOf(r0)     // Catch:{ all -> 0x0174 }
            if (r1 >= 0) goto L_0x004e
            com.facebook.interstitial.triggers.InterstitialTrigger$Action r0 = com.facebook.interstitial.triggers.InterstitialTrigger.Action.A00(r10)     // Catch:{ all -> 0x0174 }
            com.facebook.interstitial.triggers.InterstitialTrigger r2 = new com.facebook.interstitial.triggers.InterstitialTrigger     // Catch:{ all -> 0x0174 }
            r2.<init>(r0)     // Catch:{ all -> 0x0174 }
        L_0x004a:
            r6.add(r2)     // Catch:{ all -> 0x0174 }
            goto L_0x006c
        L_0x004e:
            r0 = 0
            java.lang.String r2 = r10.substring(r0, r1)     // Catch:{ all -> 0x0174 }
            int r1 = r1 + 1
            int r0 = r10.length()     // Catch:{ all -> 0x0174 }
            if (r0 <= r1) goto L_0x0069
            java.lang.String r1 = r10.substring(r1)     // Catch:{ all -> 0x0174 }
        L_0x005f:
            com.facebook.interstitial.triggers.InterstitialTrigger$Action r0 = com.facebook.interstitial.triggers.InterstitialTrigger.Action.A00(r2)     // Catch:{ all -> 0x0174 }
            com.facebook.interstitial.triggers.InterstitialTrigger r2 = new com.facebook.interstitial.triggers.InterstitialTrigger     // Catch:{ all -> 0x0174 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0174 }
            goto L_0x004a
        L_0x0069:
            java.lang.String r1 = ""
            goto L_0x005f
        L_0x006c:
            int r7 = r7 + 1
            goto L_0x0031
        L_0x006f:
            monitor-exit(r11)     // Catch:{ all -> 0x0177 }
            if (r4 == 0) goto L_0x00a8
            boolean r0 = r4.isEmpty()     // Catch:{ all -> 0x0177 }
            if (r0 != 0) goto L_0x00a8
            int r0 = r4.size()     // Catch:{ all -> 0x0177 }
            java.util.ArrayList r2 = X.C04300To.A02(r0)     // Catch:{ all -> 0x0177 }
            java.util.Iterator r1 = r4.iterator()     // Catch:{ all -> 0x0177 }
        L_0x0084:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0177 }
            if (r0 == 0) goto L_0x009c
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0177 }
            com.facebook.interstitial.triggers.InterstitialTrigger r0 = (com.facebook.interstitial.triggers.InterstitialTrigger) r0     // Catch:{ all -> 0x0177 }
            if (r0 != 0) goto L_0x0094
            r0 = r5
            goto L_0x0098
        L_0x0094:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0177 }
        L_0x0098:
            r2.add(r0)     // Catch:{ all -> 0x0177 }
            goto L_0x0084
        L_0x009c:
            java.lang.Object[] r1 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x0177 }
            java.lang.String r0 = "~"
            java.lang.String r0 = X.C06850cB.A06(r0, r1)     // Catch:{ all -> 0x0177 }
            r1 = r0
            goto L_0x00aa
        L_0x00a8:
            r0 = 0
            r1 = r5
        L_0x00aa:
            if (r0 == 0) goto L_0x00b2
            X.1Y7 r0 = X.C21001Eq.A01     // Catch:{ all -> 0x0177 }
            r13.BzC(r0, r1)     // Catch:{ all -> 0x0177 }
            goto L_0x00b7
        L_0x00b2:
            X.1Y7 r0 = X.C21001Eq.A01     // Catch:{ all -> 0x0177 }
            r13.C1B(r0)     // Catch:{ all -> 0x0177 }
        L_0x00b7:
            r6.removeAll(r4)     // Catch:{ all -> 0x0177 }
            monitor-exit(r3)
            java.lang.Class<X.1F6> r4 = X.AnonymousClass1F6.class
            monitor-enter(r4)
            java.util.Iterator r3 = r6.iterator()     // Catch:{ all -> 0x0171 }
        L_0x00c2:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0171 }
            if (r0 == 0) goto L_0x00e9
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x0171 }
            com.facebook.interstitial.triggers.InterstitialTrigger r0 = (com.facebook.interstitial.triggers.InterstitialTrigger) r0     // Catch:{ all -> 0x0171 }
            java.lang.Class<X.1F6> r2 = X.AnonymousClass1F6.class
            monitor-enter(r2)     // Catch:{ all -> 0x0171 }
            X.1Y7 r1 = X.C21001Eq.A07     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = android.net.Uri.encode(r0)     // Catch:{ all -> 0x00e6 }
            X.063 r0 = r1.A09(r0)     // Catch:{ all -> 0x00e6 }
            X.1Y7 r0 = (X.AnonymousClass1Y7) r0     // Catch:{ all -> 0x00e6 }
            r13.C1B(r0)     // Catch:{ all -> 0x00e6 }
            monitor-exit(r2)     // Catch:{ all -> 0x0171 }
            goto L_0x00c2
        L_0x00e6:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0171 }
            throw r0     // Catch:{ all -> 0x0171 }
        L_0x00e9:
            monitor-exit(r4)
            java.lang.Class<X.1F6> r8 = X.AnonymousClass1F6.class
            monitor-enter(r8)
            java.util.Set r0 = r14.entrySet()     // Catch:{ all -> 0x016e }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x016e }
        L_0x00f5:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x016e }
            if (r0 == 0) goto L_0x016c
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x016e }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x016e }
            java.lang.Object r6 = r0.getKey()     // Catch:{ all -> 0x016e }
            com.facebook.interstitial.triggers.InterstitialTrigger r6 = (com.facebook.interstitial.triggers.InterstitialTrigger) r6     // Catch:{ all -> 0x016e }
            java.lang.Object r3 = r0.getValue()     // Catch:{ all -> 0x016e }
            X.1uz r3 = (X.C37281uz) r3     // Catch:{ all -> 0x016e }
            monitor-enter(r3)     // Catch:{ all -> 0x016e }
            java.util.SortedSet r0 = r3.A04     // Catch:{ all -> 0x0166 }
            int r0 = r0.size()     // Catch:{ all -> 0x0166 }
            java.util.ArrayList r2 = X.C04300To.A02(r0)     // Catch:{ all -> 0x0166 }
            java.util.SortedSet r0 = r3.A04     // Catch:{ all -> 0x0166 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0166 }
        L_0x011e:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0166 }
            if (r0 == 0) goto L_0x0132
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0166 }
            X.3b8 r0 = (X.C70693b8) r0     // Catch:{ all -> 0x0166 }
            X.3cG r0 = r0.A01     // Catch:{ all -> 0x0166 }
            java.lang.String r0 = r0.A02     // Catch:{ all -> 0x0166 }
            r2.add(r0)     // Catch:{ all -> 0x0166 }
            goto L_0x011e
        L_0x0132:
            monitor-exit(r3)     // Catch:{ all -> 0x016e }
            monitor-enter(r4)     // Catch:{ all -> 0x016e }
            if (r2 == 0) goto L_0x0137
            goto L_0x013a
        L_0x0137:
            r3 = 0
            r2 = r5
            goto L_0x014b
        L_0x013a:
            boolean r0 = r2.isEmpty()     // Catch:{ all -> 0x0169 }
            if (r0 != 0) goto L_0x0137
            java.lang.Object[] r1 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x0169 }
            java.lang.String r0 = "~"
            java.lang.String r3 = X.C06850cB.A06(r0, r1)     // Catch:{ all -> 0x0169 }
            r2 = r3
        L_0x014b:
            X.1Y7 r1 = X.C21001Eq.A07     // Catch:{ all -> 0x0169 }
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x0169 }
            java.lang.String r0 = android.net.Uri.encode(r0)     // Catch:{ all -> 0x0169 }
            X.063 r0 = r1.A09(r0)     // Catch:{ all -> 0x0169 }
            X.1Y7 r0 = (X.AnonymousClass1Y7) r0     // Catch:{ all -> 0x0169 }
            if (r3 == 0) goto L_0x0161
            r13.BzC(r0, r2)     // Catch:{ all -> 0x0169 }
            goto L_0x0164
        L_0x0161:
            r13.C1B(r0)     // Catch:{ all -> 0x0169 }
        L_0x0164:
            monitor-exit(r4)     // Catch:{ all -> 0x016e }
            goto L_0x00f5
        L_0x0166:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x016e }
            goto L_0x016b
        L_0x0169:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x016e }
        L_0x016b:
            throw r0     // Catch:{ all -> 0x016e }
        L_0x016c:
            monitor-exit(r8)
            return
        L_0x016e:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x0171:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0174:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0177 }
            throw r0     // Catch:{ all -> 0x0177 }
        L_0x0177:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1F1.A0I(X.1hn, java.util.Map):void");
    }

    public void A0T(C32821mO r7) {
        String Aqc = r7.Aqc();
        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AgK, this.A02)).now();
        this.A06.A00();
        try {
            AnonymousClass1Y8 A0D = C21001Eq.A09.A09(Uri.encode(Aqc));
            C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A02)).edit();
            edit.BzA(A0D, now);
            edit.commit();
        } finally {
            this.A06.A01();
        }
    }
}
