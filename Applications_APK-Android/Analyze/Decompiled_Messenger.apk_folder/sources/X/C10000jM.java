package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.0jM  reason: invalid class name and case insensitive filesystem */
public final class C10000jM implements AnonymousClass0ZM {
    public final /* synthetic */ AnonymousClass0jF A00;

    public C10000jM(AnonymousClass0jF r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r3) {
        this.A00.A01();
    }
}
