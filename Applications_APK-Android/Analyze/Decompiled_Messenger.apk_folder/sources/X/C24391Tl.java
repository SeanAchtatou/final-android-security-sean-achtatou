package X;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.DeadObjectException;
import android.os.Handler;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.1Tl  reason: invalid class name and case insensitive filesystem */
public final class C24391Tl {
    public boolean A00;
    public final Context A01;
    public final ServiceConnection A02 = new C24401Tm(this);
    public final Handler A03;
    public final AnonymousClass069 A04;
    public final C29291gB A05;
    public final C24411Tn A06;

    public static final C24391Tl A00(AnonymousClass1XY r1) {
        return new C24391Tl(r1);
    }

    public void A01() {
        C24411Tn r1 = this.A06;
        Context context = this.A01;
        Intent intent = new Intent();
        C24411Tn.A01(r1, context, intent);
        intent.getComponent().flattenToString();
        C009207y r12 = r1.A01;
        String $const$string = TurboLoader.Locator.$const$string(53);
        try {
            context.stopService(intent);
        } catch (SecurityException e) {
            C010708t.A0R($const$string, e, "Failed to stopService");
            AnonymousClass09P r13 = r12.A00;
            if (r13 != null) {
                r13.softReport($const$string, "stopService SecurityException", e);
            }
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof DeadObjectException) {
                AnonymousClass09P r14 = r12.A00;
                if (r14 != null) {
                    r14.softReport($const$string, "stopService DeadObjectException", e2);
                }
            } else {
                throw e2;
            }
        }
        this.A06.A04(this.A02);
        this.A00 = false;
    }

    private C24391Tl(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass1YA.A00(r2);
        this.A06 = C24411Tn.A00(r2);
        this.A05 = C29291gB.A00(r2);
        this.A04 = AnonymousClass067.A03(r2);
        this.A03 = C29321gE.A00(r2);
    }
}
