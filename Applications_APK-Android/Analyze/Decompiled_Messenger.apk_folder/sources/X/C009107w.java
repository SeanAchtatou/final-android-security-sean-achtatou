package X;

import android.os.PowerManager;
import android.os.SystemClock;

/* renamed from: X.07w  reason: invalid class name and case insensitive filesystem */
public final class C009107w {
    public static final C007807d A00 = new C007807d();

    public static void A00(PowerManager.WakeLock wakeLock) {
        C007807d r6 = A00;
        synchronized (r6) {
            if (r6.A04) {
                C007807d.A00(r6);
                AnonymousClass07f r2 = (AnonymousClass07f) r6.A07.get(wakeLock);
                if (r2 == null) {
                    AnonymousClass0KZ.A00("WakeLockMetricsCollector", "Unknown wakelock modified", null);
                } else if (AnonymousClass07f.A00(r2, SystemClock.uptimeMillis())) {
                    int i = r6.A00 - 1;
                    r6.A00 = i;
                    if (i == 0) {
                        r6.A03 += r2.A03 - r6.A02;
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        if (r1 != 0) goto L_0x0042;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(android.os.PowerManager.WakeLock r7, long r8) {
        /*
            X.07d r5 = X.C009107w.A00
            monitor-enter(r5)
            boolean r0 = r5.A04     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x005a
            X.C007807d.A00(r5)     // Catch:{ all -> 0x005c }
            java.util.WeakHashMap r0 = r5.A07     // Catch:{ all -> 0x005c }
            java.lang.Object r4 = r0.get(r7)     // Catch:{ all -> 0x005c }
            X.07f r4 = (X.AnonymousClass07f) r4     // Catch:{ all -> 0x005c }
            if (r4 != 0) goto L_0x001d
            java.lang.String r2 = "WakeLockMetricsCollector"
            java.lang.String r1 = "Unknown wakelock modified"
            r0 = 0
            X.AnonymousClass0KZ.A00(r2, r1, r0)     // Catch:{ all -> 0x005c }
            goto L_0x005a
        L_0x001d:
            long r2 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x005c }
            r6 = 0
            int r0 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r0 < 0) goto L_0x002a
            long r8 = r8 + r2
            r4.A04 = r8     // Catch:{ all -> 0x005c }
        L_0x002a:
            boolean r0 = r4.A06     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0035
            int r1 = r4.A00     // Catch:{ all -> 0x005c }
            int r0 = r1 + 1
            r4.A00 = r0     // Catch:{ all -> 0x005c }
            goto L_0x0040
        L_0x0035:
            boolean r0 = r4.A05     // Catch:{ all -> 0x005c }
            if (r0 != 0) goto L_0x0042
            r4.A01 = r2     // Catch:{ all -> 0x005c }
            r0 = 1
            r4.A05 = r0     // Catch:{ all -> 0x005c }
            r0 = 1
            goto L_0x0043
        L_0x0040:
            if (r1 == 0) goto L_0x0035
        L_0x0042:
            r0 = 0
        L_0x0043:
            if (r0 == 0) goto L_0x005a
            int r4 = r5.A00     // Catch:{ all -> 0x005c }
            if (r4 != 0) goto L_0x004f
            long r0 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x005c }
            r5.A02 = r0     // Catch:{ all -> 0x005c }
        L_0x004f:
            long r2 = r5.A01     // Catch:{ all -> 0x005c }
            r0 = 1
            long r2 = r2 + r0
            r5.A01 = r2     // Catch:{ all -> 0x005c }
            int r0 = r4 + 1
            r5.A00 = r0     // Catch:{ all -> 0x005c }
        L_0x005a:
            monitor-exit(r5)
            return
        L_0x005c:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C009107w.A01(android.os.PowerManager$WakeLock, long):void");
    }
}
