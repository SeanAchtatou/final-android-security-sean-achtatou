package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.1au  reason: invalid class name and case insensitive filesystem */
public final class C26101au implements C26031an {
    public String getName() {
        return "duration";
    }

    public int B8E(C04270Tg r4) {
        return (int) TimeUnit.NANOSECONDS.toMillis(r4.A09);
    }
}
