package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import io.card.payment.BuildConfig;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1oe  reason: invalid class name and case insensitive filesystem */
public final class C34101oe implements AnonymousClass1LY {
    private static volatile C34101oe A03;
    private final AnonymousClass069 A00;
    private final QuickPerformanceLogger A01;
    private final C34111of A02;

    public static final C34101oe A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C34101oe.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C34101oe(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int, long, boolean):void
     arg types: [int, int, long, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int, long, java.util.concurrent.TimeUnit):void
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int, java.lang.String, java.lang.String):void
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, java.lang.String, java.lang.String, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int, long, boolean):void */
    public void BgD(String str, String str2, Map map) {
        String str3;
        String A0J;
        String A0J2;
        String str4 = str2;
        String str5 = BuildConfig.FLAVOR;
        Map map2 = map;
        if (map != null) {
            if (map2.get("dest_module_class") != null) {
                str3 = (String) map2.get("dest_module_class");
            } else {
                str3 = str5;
            }
            if (map2.get("source_module_class") != null) {
                str5 = (String) map2.get("source_module_class");
            }
        } else {
            str3 = str5;
        }
        String str6 = "missing_info";
        String str7 = str;
        if (str == null) {
            str7 = str6;
        }
        if (str5.isEmpty() || str7.isEmpty()) {
            A0J = AnonymousClass08S.A0J(str5, str7);
        } else {
            A0J = AnonymousClass08S.A0P(str5, ":", str7);
        }
        if (str2 == null) {
            str4 = str6;
        }
        if (str3.isEmpty() || str4.isEmpty()) {
            A0J2 = AnonymousClass08S.A0J(str3, str4);
        } else {
            A0J2 = AnonymousClass08S.A0P(str3, ":", str4);
        }
        if (this.A01.isMarkerOn(25952257, 0)) {
            this.A01.markerAnnotate(25952257, 0, "next_module", str4);
            this.A01.markerAnnotate(25952257, 0, "next_endpoint", A0J2);
            this.A01.markerEnd(25952257, 0, (short) 2);
        }
        this.A01.markerStart(25952257, 0, this.A00.now(), true);
        C34111of r9 = this.A02;
        if (str != null) {
            str6 = str;
        }
        C34111of.A01(r9, str6, false);
        C34111of.A01(this.A02, str4, true);
        this.A01.markerAnnotate(25952257, 0, "previous_module", str7);
        this.A01.markerAnnotate(25952257, 0, "previous_endpoint", A0J);
        this.A01.markerAnnotate(25952257, 0, "current_module", str4);
        this.A01.markerAnnotate(25952257, 0, "current_endpoint", A0J2);
    }

    private C34101oe(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0ZD.A03(r2);
        this.A00 = AnonymousClass067.A03(r2);
        this.A02 = C34111of.A00(r2);
    }
}
