package X;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.List;

/* renamed from: X.0Z5  reason: invalid class name */
public final class AnonymousClass0Z5 extends SQLiteOpenHelper {
    private final AnonymousClass0Z6 A00;

    public AnonymousClass0Z5(Context context, String str, List list, int i, DatabaseErrorHandler databaseErrorHandler) {
        super(context, str, null, AnonymousClass1Y3.A1c, databaseErrorHandler);
        this.A00 = new AnonymousClass0Z6(list, i, context);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(27:3|4|(3:6|7|8)(1:9)|(1:11)(1:12)|13|(2:15|16)|17|18|19|20|(3:22|23|24)(1:25)|26|(1:28)|29|30|31|(3:33|34|35)(1:36)|37|(1:39)|40|(11:43|44|45|(3:47|48|49)(1:50)|51|(3:53|54|(1:56)(3:93|70|73))(2:57|(2:59|(1:61)(3:90|72|73))(1:(4:92|63|71|73)))|(1:66)|67|(2:69|95)(1:94)|89|41)|91|74|75|(2:78|76)|96|79) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x00a6 */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b9 A[Catch:{ all -> 0x01e6, all -> 0x01f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1 A[Catch:{ all -> 0x01e6, all -> 0x01f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ca A[Catch:{ all -> 0x01e6, all -> 0x01f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0106 A[Catch:{ all -> 0x01e6, all -> 0x01f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x010e A[Catch:{ all -> 0x01e6, all -> 0x01f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0117 A[Catch:{ all -> 0x01e6, all -> 0x01f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0148 A[Catch:{ all -> 0x01e6, all -> 0x01f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01db A[LOOP:1: B:76:0x01d5->B:78:0x01db, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onOpen(android.database.sqlite.SQLiteDatabase r18) {
        /*
            r17 = this;
            r0 = r17
            X.0Z6 r0 = r0.A00
            java.lang.String r1 = "page_size"
            r9 = r18
            int r1 = X.AnonymousClass0Z6.A00(r9, r1)
            int r2 = r0.A00
            int r2 = r2 / r1
            r1 = 1
            int r1 = java.lang.Math.max(r1, r2)
            java.lang.String r2 = java.lang.String.valueOf(r1)
            java.lang.String r1 = "wal_autocheckpoint"
            X.AnonymousClass0Z6.A02(r9, r1, r2)
            int r1 = r0.A00
            java.lang.String r2 = java.lang.String.valueOf(r1)
            java.lang.String r1 = "journal_size_limit"
            X.AnonymousClass0Z6.A02(r9, r1, r2)
            java.lang.String r2 = "journal_mode"
            java.lang.String r1 = "PERSIST"
            X.AnonymousClass0Z6.A02(r9, r2, r1)
            r1 = -1954818821(0xffffffff8b7bd4fb, float:-4.850107E-32)
            X.C007406x.A01(r9, r1)
            android.content.Context r1 = r0.A01     // Catch:{ all -> 0x01f0 }
            int r4 = X.AnonymousClass0ZP.A00(r1)     // Catch:{ all -> 0x01f0 }
            java.lang.String[] r11 = X.AnonymousClass0Z6.A03     // Catch:{ all -> 0x01f0 }
            r3 = 0
            java.lang.String r1 = "app_build_number"
            java.lang.String[] r13 = new java.lang.String[]{r1}     // Catch:{ all -> 0x01f0 }
            java.lang.String r10 = "_shared_version"
            java.lang.String r12 = "name=?"
            r14 = 0
            r15 = 0
            r16 = 0
            android.database.Cursor r2 = r9.query(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x01f0 }
            boolean r1 = r2.moveToNext()     // Catch:{ all -> 0x01eb }
            if (r1 == 0) goto L_0x005e
            int r1 = r2.getInt(r3)     // Catch:{ all -> 0x01eb }
            r2.close()     // Catch:{ all -> 0x01f0 }
            goto L_0x0062
        L_0x005e:
            r2.close()     // Catch:{ all -> 0x01f0 }
            r1 = -1
        L_0x0062:
            if (r4 == r1) goto L_0x008a
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ all -> 0x01f0 }
            r3.<init>()     // Catch:{ all -> 0x01f0 }
            java.lang.String r2 = "name"
            java.lang.String r1 = "app_build_number"
            r3.put(r2, r1)     // Catch:{ all -> 0x01f0 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x01f0 }
            java.lang.String r1 = "version"
            r3.put(r1, r2)     // Catch:{ all -> 0x01f0 }
            r1 = -413138429(0xffffffffe7600203, float:-1.0578472E24)
            X.C007406x.A00(r1)     // Catch:{ all -> 0x01f0 }
            r9.replaceOrThrow(r10, r14, r3)     // Catch:{ all -> 0x01f0 }
            r1 = -1344860925(0xffffffffafd70d03, float:-3.9117518E-10)
            X.C007406x.A00(r1)     // Catch:{ all -> 0x01f0 }
            r8 = 1
            goto L_0x008b
        L_0x008a:
            r8 = 0
        L_0x008b:
            android.content.Context r6 = r0.A01     // Catch:{ all -> 0x01f0 }
            long r4 = X.AnonymousClass0ZP.A00     // Catch:{ all -> 0x01f0 }
            r2 = -1
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r1 != 0) goto L_0x00a6
            android.content.pm.PackageManager r3 = r6.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00a6 }
            java.lang.String r2 = r6.getPackageName()     // Catch:{ NameNotFoundException -> 0x00a6 }
            r1 = 0
            android.content.pm.PackageInfo r1 = r3.getPackageInfo(r2, r1)     // Catch:{ NameNotFoundException -> 0x00a6 }
            long r1 = r1.lastUpdateTime     // Catch:{ NameNotFoundException -> 0x00a6 }
            X.AnonymousClass0ZP.A00 = r1     // Catch:{ NameNotFoundException -> 0x00a6 }
        L_0x00a6:
            long r5 = X.AnonymousClass0ZP.A00     // Catch:{ all -> 0x01f0 }
            r2 = 0
            java.lang.String r1 = "app_upgrade_time"
            java.lang.String[] r13 = new java.lang.String[]{r1}     // Catch:{ all -> 0x01f0 }
            android.database.Cursor r4 = r9.query(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x01f0 }
            boolean r1 = r4.moveToNext()     // Catch:{ all -> 0x01e6 }
            if (r1 == 0) goto L_0x00c1
            long r2 = r4.getLong(r2)     // Catch:{ all -> 0x01e6 }
            r4.close()     // Catch:{ all -> 0x01f0 }
            goto L_0x00c6
        L_0x00c1:
            r4.close()     // Catch:{ all -> 0x01f0 }
            r2 = -1
        L_0x00c6:
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x00ef
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ all -> 0x01f0 }
            r3.<init>()     // Catch:{ all -> 0x01f0 }
            java.lang.String r2 = "name"
            java.lang.String r1 = "app_upgrade_time"
            r3.put(r2, r1)     // Catch:{ all -> 0x01f0 }
            java.lang.Long r2 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x01f0 }
            java.lang.String r1 = "version"
            r3.put(r1, r2)     // Catch:{ all -> 0x01f0 }
            r1 = 995225640(0x3b51f028, float:0.0032034013)
            X.C007406x.A00(r1)     // Catch:{ all -> 0x01f0 }
            r9.replaceOrThrow(r10, r14, r3)     // Catch:{ all -> 0x01f0 }
            r1 = -209885610(0xfffffffff37d6656, float:-2.0076397E31)
            X.C007406x.A00(r1)     // Catch:{ all -> 0x01f0 }
            r8 = 1
        L_0x00ef:
            android.content.Context r1 = r0.A01     // Catch:{ all -> 0x01f0 }
            long r5 = com.facebook.common.dextricks.DexLibLoader.getLastCompilationTime(r1)     // Catch:{ all -> 0x01f0 }
            r2 = 0
            java.lang.String r1 = "dex_update_time"
            java.lang.String[] r13 = new java.lang.String[]{r1}     // Catch:{ all -> 0x01f0 }
            android.database.Cursor r4 = r9.query(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x01f0 }
            boolean r1 = r4.moveToNext()     // Catch:{ all -> 0x01e6 }
            if (r1 == 0) goto L_0x010e
            long r2 = r4.getLong(r2)     // Catch:{ all -> 0x01e6 }
            r4.close()     // Catch:{ all -> 0x01f0 }
            goto L_0x0113
        L_0x010e:
            r4.close()     // Catch:{ all -> 0x01f0 }
            r2 = -1
        L_0x0113:
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x013c
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ all -> 0x01f0 }
            r3.<init>()     // Catch:{ all -> 0x01f0 }
            java.lang.String r2 = "name"
            java.lang.String r1 = "dex_update_time"
            r3.put(r2, r1)     // Catch:{ all -> 0x01f0 }
            java.lang.Long r2 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x01f0 }
            java.lang.String r1 = "version"
            r3.put(r1, r2)     // Catch:{ all -> 0x01f0 }
            r1 = -1266403714(0xffffffffb484367e, float:-2.4626564E-7)
            X.C007406x.A00(r1)     // Catch:{ all -> 0x01f0 }
            r9.replaceOrThrow(r10, r14, r3)     // Catch:{ all -> 0x01f0 }
            r1 = -2025760095(0xffffffff87415aa1, float:-1.454634E-34)
            X.C007406x.A00(r1)     // Catch:{ all -> 0x01f0 }
            r8 = 1
        L_0x013c:
            com.google.common.collect.ImmutableList r1 = r0.A02     // Catch:{ all -> 0x01f0 }
            X.1Xv r4 = r1.iterator()     // Catch:{ all -> 0x01f0 }
        L_0x0142:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x01f0 }
            if (r1 == 0) goto L_0x01c6
            java.lang.Object r6 = r4.next()     // Catch:{ all -> 0x01f0 }
            X.0W2 r6 = (X.AnonymousClass0W2) r6     // Catch:{ all -> 0x01f0 }
            java.lang.String r1 = r6.A01     // Catch:{ all -> 0x01f0 }
            r3 = 0
            java.lang.String[] r13 = new java.lang.String[]{r1}     // Catch:{ all -> 0x01f0 }
            android.database.Cursor r2 = r9.query(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x01f0 }
            boolean r1 = r2.moveToNext()     // Catch:{ all -> 0x01eb }
            if (r1 == 0) goto L_0x0167
            int r7 = r2.getInt(r3)     // Catch:{ all -> 0x01eb }
            r2.close()     // Catch:{ all -> 0x01f0 }
            goto L_0x016b
        L_0x0167:
            r2.close()     // Catch:{ all -> 0x01f0 }
            r7 = -1
        L_0x016b:
            java.lang.String r3 = "Can't upgrade readonly database"
            r2 = -1
            if (r7 != r2) goto L_0x017a
            boolean r1 = r9.isReadOnly()     // Catch:{ all -> 0x01f0 }
            if (r1 != 0) goto L_0x01a0
            r6.A04(r9)     // Catch:{ all -> 0x01f0 }
            goto L_0x018d
        L_0x017a:
            int r1 = r6.A00     // Catch:{ all -> 0x01f0 }
            if (r7 >= r1) goto L_0x018a
            boolean r1 = r9.isReadOnly()     // Catch:{ all -> 0x01f0 }
            if (r1 != 0) goto L_0x01c0
            int r1 = r6.A00     // Catch:{ all -> 0x01f0 }
            r6.A08(r9, r7, r1)     // Catch:{ all -> 0x01f0 }
            goto L_0x018d
        L_0x018a:
            if (r7 <= r1) goto L_0x018d
            goto L_0x01a6
        L_0x018d:
            if (r8 == 0) goto L_0x0196
            if (r7 == r2) goto L_0x0196
            android.content.Context r1 = r0.A01     // Catch:{ all -> 0x01f0 }
            r6.A06(r9, r1)     // Catch:{ all -> 0x01f0 }
        L_0x0196:
            int r2 = r6.A00     // Catch:{ all -> 0x01f0 }
            if (r7 == r2) goto L_0x0142
            java.lang.String r1 = r6.A01     // Catch:{ all -> 0x01f0 }
            X.AnonymousClass0Z6.A01(r9, r1, r2)     // Catch:{ all -> 0x01f0 }
            goto L_0x0142
        L_0x01a0:
            android.database.sqlite.SQLiteException r5 = new android.database.sqlite.SQLiteException     // Catch:{ all -> 0x01f0 }
            r5.<init>(r3)     // Catch:{ all -> 0x01f0 }
            goto L_0x01c5
        L_0x01a6:
            X.Am1 r5 = new X.Am1     // Catch:{ all -> 0x01f0 }
            java.lang.String r4 = "Can't downgrade version from %d to %d for %s"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x01f0 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x01f0 }
            java.lang.String r1 = r6.A01     // Catch:{ all -> 0x01f0 }
            java.lang.Object[] r1 = new java.lang.Object[]{r3, r2, r1}     // Catch:{ all -> 0x01f0 }
            java.lang.String r1 = java.lang.String.format(r4, r1)     // Catch:{ all -> 0x01f0 }
            r5.<init>(r0, r1)     // Catch:{ all -> 0x01f0 }
            goto L_0x01c5
        L_0x01c0:
            android.database.sqlite.SQLiteException r5 = new android.database.sqlite.SQLiteException     // Catch:{ all -> 0x01f0 }
            r5.<init>(r3)     // Catch:{ all -> 0x01f0 }
        L_0x01c5:
            throw r5     // Catch:{ all -> 0x01f0 }
        L_0x01c6:
            r9.setTransactionSuccessful()     // Catch:{ all -> 0x01f0 }
            r1 = 2102285035(0x7d4e52eb, float:1.7140719E37)
            X.C007406x.A02(r9, r1)
            com.google.common.collect.ImmutableList r0 = r0.A02
            X.1Xv r1 = r0.iterator()
        L_0x01d5:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x01e5
            java.lang.Object r0 = r1.next()
            X.0W2 r0 = (X.AnonymousClass0W2) r0
            r0.A07(r9)
            goto L_0x01d5
        L_0x01e5:
            return
        L_0x01e6:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x01f0 }
            goto L_0x01ef
        L_0x01eb:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x01f0 }
        L_0x01ef:
            throw r0     // Catch:{ all -> 0x01f0 }
        L_0x01f0:
            r1 = move-exception
            r0 = -1587408170(0xffffffffa16212d6, float:-7.659671E-19)
            X.C007406x.A02(r9, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Z5.onOpen(android.database.sqlite.SQLiteDatabase):void");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        AnonymousClass0Z6 r1 = this.A00;
        if (i < 200) {
            C007406x.A00(2037050678);
            sQLiteDatabase.execSQL("CREATE TABLE _shared_version (name TEXT PRIMARY KEY, version INTEGER)");
            C007406x.A00(1173999346);
            C24971Xv it = r1.A02.iterator();
            while (it.hasNext()) {
                AnonymousClass0Z6.A01(sQLiteDatabase, ((AnonymousClass0W2) it.next()).A01, i);
            }
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        C007406x.A00(1959790794);
        sQLiteDatabase.execSQL("CREATE TABLE _shared_version (name TEXT PRIMARY KEY, version INTEGER)");
        C007406x.A00(-1724107687);
    }
}
