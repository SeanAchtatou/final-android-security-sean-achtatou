package X;

import android.animation.ValueAnimator;
import com.facebook.orca.threadview.ThreadViewMessagesFragment;

/* renamed from: X.1y0  reason: invalid class name and case insensitive filesystem */
public final class C38791y0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ThreadViewMessagesFragment A00;

    public C38791y0(ThreadViewMessagesFragment threadViewMessagesFragment) {
        this.A00 = threadViewMessagesFragment;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.A00.A01 = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        ThreadViewMessagesFragment.A0a(this.A00);
    }
}
