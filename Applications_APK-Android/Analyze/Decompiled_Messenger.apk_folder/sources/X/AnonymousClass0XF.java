package X;

import java.util.HashMap;
import java.util.Map;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0XF  reason: invalid class name */
public final class AnonymousClass0XF extends Enum {
    private static final Map A00 = new HashMap();
    private static final /* synthetic */ AnonymousClass0XF[] A01;
    public static final AnonymousClass0XF A02;
    public static final AnonymousClass0XF A03;
    public static final AnonymousClass0XF A04;
    public static final AnonymousClass0XF A05;
    public static final AnonymousClass0XF A06;
    public static final AnonymousClass0XF A07;
    public static final AnonymousClass0XF A08;
    public int source;

    static {
        AnonymousClass0XF r6 = new AnonymousClass0XF("UNKNOWN", 0, -1);
        A08 = r6;
        AnonymousClass0XF r7 = new AnonymousClass0XF("SERVER", 1, 0);
        A07 = r7;
        AnonymousClass0XF r8 = new AnonymousClass0XF("OVERRIDE", 2, 1);
        A06 = r8;
        AnonymousClass0XF r9 = new AnonymousClass0XF("DEFAULT__SERVER_RETURNED_NULL", 3, 2);
        A05 = r9;
        AnonymousClass0XF r10 = new AnonymousClass0XF("DEFAULT__ACCESSED_BEFORE_MC_INIT", 4, 3);
        A02 = r10;
        AnonymousClass0XF r11 = new AnonymousClass0XF("DEFAULT__NO_DATA_ON_DISK", 5, 4);
        A04 = r11;
        AnonymousClass0XF r12 = new AnonymousClass0XF("DEFAULT__ACCESSED_AFTER_MC_DISPOSE", 6, 5);
        AnonymousClass0XF r13 = new AnonymousClass0XF("DEFAULT__MISSING_SERVER_VALUE", 7, 6);
        A03 = r13;
        A01 = new AnonymousClass0XF[]{r6, r7, r8, r9, r10, r11, r12, r13};
        for (AnonymousClass0XF r2 : values()) {
            A00.put(Integer.valueOf(r2.source), r2);
        }
    }

    public static AnonymousClass0XF valueOf(String str) {
        return (AnonymousClass0XF) Enum.valueOf(AnonymousClass0XF.class, str);
    }

    public static AnonymousClass0XF[] values() {
        return (AnonymousClass0XF[]) A01.clone();
    }

    private AnonymousClass0XF(String str, int i, int i2) {
        this.source = i2;
    }
}
