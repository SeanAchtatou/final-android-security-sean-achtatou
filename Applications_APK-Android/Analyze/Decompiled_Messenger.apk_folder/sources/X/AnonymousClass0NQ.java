package X;

/* renamed from: X.0NQ  reason: invalid class name */
public final class AnonymousClass0NQ {
    private static final long[] A00 = {86400000, 3600000, 60000, 1000};
    private static final String[] A01 = {"d", "h", "m", "s", "ms"};

    public static String A00(long j) {
        StringBuilder sb = new StringBuilder();
        long[] jArr = A00;
        int length = jArr.length;
        int i = 0;
        while (i < length) {
            long j2 = jArr[i];
            long j3 = j / j2;
            if (j3 > 0) {
                if (sb.length() > 0) {
                    sb.append(' ');
                }
                sb.append(j3);
                sb.append(A01[i]);
                j %= j2;
            }
            i++;
        }
        if (sb.length() > 0) {
            if (j > 0) {
                sb.append(' ');
            }
            return sb.toString();
        }
        sb.append(j);
        sb.append(A01[i]);
        return sb.toString();
    }
}
