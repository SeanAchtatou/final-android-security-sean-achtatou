package X;

import com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem;
import com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem;
import com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit;

/* renamed from: X.1BD  reason: invalid class name */
public interface AnonymousClass1BD {
    void BRY(DiscoverTabAttachmentUnit discoverTabAttachmentUnit);

    void BWP(DiscoverTabAttachmentItem discoverTabAttachmentItem);

    void BWQ(DiscoverTabAttachmentItem discoverTabAttachmentItem);

    void Bdl(DiscoverLocationUpsellItem discoverLocationUpsellItem);

    void Bdp(DiscoverLocationUpsellItem discoverLocationUpsellItem);
}
