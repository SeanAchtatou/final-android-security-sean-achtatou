package X;

import android.content.Context;
import com.google.common.base.Objects;
import java.util.Arrays;

/* renamed from: X.0ks  reason: invalid class name and case insensitive filesystem */
public final class C10800ks {
    public long A00;
    public C10560kP A01;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Objects.equal(this.A01, ((C10800ks) obj).A01);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01});
    }

    public C10800ks(C09700iK r3, Context context, C10560kP r5) {
        this.A01 = r5;
        this.A00 = r3.A05(context, r5).lastModified();
    }
}
