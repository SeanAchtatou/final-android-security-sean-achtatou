package X;

import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.common.util.TriState;
import com.facebook.messaging.business.messengerextensions.model.MessengerExtensionProperties;
import com.facebook.messaging.games.model.InstantGameChannel;
import com.facebook.user.model.Name;
import com.facebook.user.model.User;
import com.facebook.user.model.WorkUserInfo;
import com.facebook.user.profilepic.PicSquare;
import com.facebook.user.profilepic.ProfilePicUriWithFilePath;
import com.google.common.base.Platform;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.util.List;

/* renamed from: X.0cv  reason: invalid class name and case insensitive filesystem */
public final class C07220cv {
    public float A00;
    public float A01;
    public int A02 = 0;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public long A09;
    public long A0A;
    public long A0B;
    public long A0C;
    public long A0D;
    public TriState A0E;
    public TriState A0F;
    public C25661aC A0G;
    public MessengerExtensionProperties A0H;
    public InstantGameChannel A0I;
    public Name A0J;
    public Name A0K;
    public C25651aB A0L;
    public User A0M;
    public User A0N;
    public WorkUserInfo A0O;
    public PicSquare A0P;
    public ProfilePicUriWithFilePath A0Q;
    public ImmutableList A0R;
    public ImmutableList A0S;
    public ImmutableList A0T;
    public ImmutableList A0U;
    public ImmutableList A0V;
    public ImmutableList A0W;
    public ImmutableList A0X;
    public ImmutableList A0Y;
    public ImmutableList A0Z;
    public Integer A0a;
    public Integer A0b;
    public Integer A0c;
    public Integer A0d;
    public Integer A0e;
    public String A0f;
    public String A0g;
    public String A0h;
    public String A0i;
    public String A0j;
    public String A0k;
    public String A0l;
    public String A0m;
    public String A0n;
    public String A0o;
    public String A0p;
    public String A0q;
    public String A0r;
    public String A0s;
    public String A0t;
    public String A0u;
    public String A0v;
    public String A0w;
    public String A0x;
    public String A0y;
    public String A0z;
    public String A10;
    public String A11;
    public String A12 = null;
    public String A13;
    public String A14;
    public String A15;
    public List A16 = null;
    public List A17 = null;
    public boolean A18;
    public boolean A19;
    public boolean A1A;
    public boolean A1B;
    public boolean A1C;
    public boolean A1D;
    public boolean A1E;
    public boolean A1F;
    public boolean A1G;
    public boolean A1H;
    public boolean A1I;
    public boolean A1J;
    public boolean A1K;
    public boolean A1L;
    public boolean A1M;
    public boolean A1N;
    public boolean A1O;
    public boolean A1P;
    public boolean A1Q;
    public boolean A1R;
    public boolean A1S;
    public boolean A1T;
    public boolean A1U;
    public boolean A1V;
    public boolean A1W;
    public boolean A1X;
    public boolean A1Y;
    public boolean A1Z;
    public boolean A1a;
    public boolean A1b;
    public boolean A1c;
    public boolean A1d;
    public boolean A1e;
    public boolean A1f;
    public boolean A1g;
    public boolean A1h;
    public boolean A1i;
    public boolean A1j;
    public boolean A1k;
    public boolean A1l;
    public boolean A1m;
    public boolean A1n;
    public boolean A1o;

    public User A02() {
        return new User(this);
    }

    public void A03(C25651aB r1, String str) {
        this.A0L = r1;
        this.A0i = str;
    }

    public void A04(User user) {
        this.A0L = user.A0M;
        this.A0i = user.A0j;
        this.A12 = user.A0w;
        this.A16 = user.A0T;
        this.A0Z = user.A0b;
        this.A17 = user.A05();
        this.A0J = user.A0L;
        this.A0g = user.A08();
        this.A0x = user.A0s;
        this.A02 = user.A08;
        this.A11 = user.A0D();
        this.A10 = user.A0u;
        this.A0f = user.A0h;
        this.A0P = user.A04();
        this.A0Q = user.A1m;
        this.A14 = user.A0z;
        this.A0y = user.A0t;
        this.A01 = user.A04;
        this.A0E = user.A0G;
        this.A1A = user.A14;
        this.A1C = user.A1A;
        this.A15 = user.A10;
        this.A13 = user.A0x;
        this.A1B = user.A18;
        this.A18 = user.A12;
        this.A19 = user.A13;
        this.A0a = user.A0c;
        this.A0R = user.A0S;
        this.A0B = user.A0F;
        this.A09 = user.A0C;
        this.A1W = user.A16;
        this.A1V = user.A15;
        this.A06 = user.A07;
        this.A05 = user.A06;
        this.A04 = user.A05;
        this.A0l = user.A0g;
        this.A0u = user.A0v;
        this.A0w = user.A0y;
        this.A0o = user.A0i;
        this.A1i = user.A1c;
        this.A1n = user.A1g;
        this.A1h = user.A1b;
        this.A0F = user.A0H;
        this.A1L = user.A1J;
        this.A1N = user.A1L;
        this.A1a = user.A1V;
        this.A1f = user.A1Z;
        this.A1F = user.A1D;
        this.A0A = user.A00;
        this.A1T = user.A1Q;
        this.A00 = user.A03;
        this.A0Y = user.A0a;
        this.A1e = user.A1Y;
        this.A0C = user.A0D;
        this.A1E = user.A1C;
        this.A1O = user.A11;
        this.A0X = user.A0Z;
        this.A1G = user.A1E;
        this.A1d = user.A1X;
        this.A1H = user.A1F;
        this.A0c = user.A0d;
        this.A1o = user.A1h;
        this.A1J = user.A1H;
        this.A1I = user.A1G;
        this.A1K = user.A1I;
        this.A0H = user.A0J;
        this.A0N = user.A0O;
        this.A0e = user.A0f;
        this.A0M = user.A0N;
        this.A1Z = user.A1U;
        this.A0s = user.A0q;
        this.A0m = user.A0l;
        this.A1X = user.A1S;
        this.A1D = user.A1B;
        this.A0I = user.A0K;
        this.A07 = user.A0A;
        this.A0r = user.A0p;
        this.A1M = user.A1K;
        this.A0S = user.A0U;
        this.A1g = user.A1a;
        this.A1b = user.A1W;
        this.A0T = user.A0V;
        this.A1m = user.A1f;
        this.A1Q = user.A1N;
        this.A0V = user.A0X;
        this.A1k = user.A1d;
        this.A1R = user.A1O;
        this.A0D = user.A0E;
        this.A0q = user.A0o;
        this.A0O = user.A0R;
        this.A0n = user.A0m;
        this.A0U = user.A0W;
        this.A0W = user.A0Y;
        this.A1l = user.A1e;
        this.A0p = user.A0n;
        this.A0k = user.A0k;
        this.A0t = user.A0r;
        this.A0d = user.A0e;
        this.A1Y = user.A1T;
        this.A08 = user.A0B;
        this.A0G = user.A0I;
        this.A1S = user.A1P;
        this.A1P = user.A1M;
        this.A03 = user.A09;
        this.A1U = user.A1R;
    }

    public void A05(String str, String str2) {
        this.A0L = C25651aB.A05;
        if (str == null) {
            str = BuildConfig.FLAVOR;
        }
        if (str2 == null) {
            str2 = BuildConfig.FLAVOR;
        }
        this.A0i = StringFormatUtil.formatStrLocaleSafe("%s:%s", str, str2);
    }

    public void A06(String str, String str2) {
        this.A0L = C25651aB.A02;
        if (str == null) {
            str = BuildConfig.FLAVOR;
        }
        if (str2 == null) {
            str2 = BuildConfig.FLAVOR;
        }
        this.A0i = StringFormatUtil.formatStrLocaleSafe("%s:%s", str, str2);
    }

    public C07220cv() {
        TriState triState = TriState.UNSET;
        this.A0E = triState;
        this.A0F = triState;
        Integer num = AnonymousClass07B.A00;
        this.A0e = num;
        this.A0b = num;
        this.A1F = true;
        this.A0G = C25661aC.UNSET;
    }

    public static String A00(String str) {
        int indexOf;
        if (Platform.stringIsNullOrEmpty(str) || (indexOf = str.indexOf(58)) < 0) {
            return null;
        }
        String substring = str.substring(0, indexOf);
        if (Platform.stringIsNullOrEmpty(substring)) {
            return null;
        }
        return substring;
    }

    public static String A01(String str) {
        int indexOf;
        int i;
        if (Platform.stringIsNullOrEmpty(str) || (indexOf = str.indexOf(58)) < 0 || (i = indexOf + 1) >= str.length()) {
            return null;
        }
        return str.substring(i);
    }
}
