package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.18R  reason: invalid class name */
public final class AnonymousClass18R implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass18R(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(1507991891);
        C33741o4 r3 = this.A00;
        C193617v r0 = r3.A03;
        if (r0 != null) {
            r0.Btb(true, true, "internal reload");
            r3.A03.BtU("internal reload");
        }
        AnonymousClass09Y.A01(196184324, A002);
    }
}
