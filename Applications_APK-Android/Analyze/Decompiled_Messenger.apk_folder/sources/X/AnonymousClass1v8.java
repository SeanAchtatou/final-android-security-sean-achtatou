package X;

import android.content.Context;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1v8  reason: invalid class name */
public final class AnonymousClass1v8 extends C17770zR {
    public static final MigColorScheme A05 = C17190yT.A00();
    @Comparable(type = 13)
    public C25998CqM A00;
    @Comparable(type = 13)
    public C204059jh A01;
    @Comparable(type = 14)
    public AnonymousClass5CH A02 = new AnonymousClass5CH();
    @Comparable(type = 13)
    public C121605oW A03;
    @Comparable(type = 13)
    public MigColorScheme A04 = A05;

    public static C17770zR A00(AnonymousClass0p4 r11, C121655ob r12, int i) {
        AnonymousClass10N A0E;
        String string;
        String string2;
        String str;
        C17770zR A39;
        if (i == 1) {
            A0E = C17780zS.A0E(AnonymousClass1v8.class, r11, -228443111, new Object[]{r11});
            string = r11.A09.getString(2131821084);
            string2 = null;
            str = "keep_me_logged_in_item";
        } else if (i == 2) {
            A0E = C17780zS.A0E(AnonymousClass1v8.class, r11, -1972293012, new Object[]{r11});
            string = r11.A09.getString(2131821085);
            string2 = r11.A09.getString(2131821086);
            str = "log_me_out_item";
        } else {
            throw new IllegalArgumentException("Unsupported security row type");
        }
        Context context = r11.A09;
        int i2 = 2131821070;
        if (r12.A00 == i) {
            i2 = 2131821069;
        }
        String string3 = context.getString(i2, string);
        int i3 = 2131231351;
        if (r12.A00 == i) {
            i3 = 2131231352;
        }
        C37941wd A002 = C16980y8.A00(r11);
        A002.A20(100.0f);
        A002.A1t(65.0f);
        C37951we A003 = AnonymousClass11D.A00(r11);
        A003.A3E(C14950uP.CENTER);
        A003.A1q(100.0f);
        A003.A2X(AnonymousClass10G.RIGHT, (float) AnonymousClass1JQ.XLARGE.B3A());
        ComponentBuilderCBuilderShape0_0S0100000 A004 = C22291Kt.A00(r11);
        A004.A36(i3);
        A004.A1p(30.0f);
        A004.A1z(30.0f);
        A004.A33(AnonymousClass1JZ.A02.AhV());
        A004.A1o(0.0f);
        A003.A3A((C22291Kt) A004.A00);
        A002.A3A(A003.A00);
        C37951we A005 = AnonymousClass11D.A00(r11);
        C14950uP r0 = C14950uP.CENTER;
        A005.A3E(r0);
        A005.A3E(r0);
        A005.A1q(100.0f);
        ComponentBuilderCBuilderShape0_0S0300000 A006 = C17030yD.A00(r11);
        A006.A3l(string);
        A006.A3f(AnonymousClass10J.A01);
        A005.A3A(A006.A39());
        if (string2 == null) {
            A39 = C16980y8.A00(r11).A00;
        } else {
            ComponentBuilderCBuilderShape0_0S0300000 A007 = C17030yD.A00(r11);
            A007.A3l(string2);
            A007.A3f(AnonymousClass10J.A0B);
            A39 = A007.A39();
        }
        A005.A3A(A39);
        A002.A3A(A005.A00);
        A002.A2y(A0E);
        A002.A2z(string3);
        A002.A2p(str);
        A002.A2H(AnonymousClass1M2.A01(0.0f, 0, AnonymousClass1KA.A01(r12.A02.AfS())));
        return A002.A00;
    }

    public AnonymousClass1v8() {
        super("AccountLoginRecSecurityRootComponent");
    }

    public static void A01(AnonymousClass0p4 r3, int i) {
        if (r3.A04 != null) {
            r3.A0F(new C61322yh(0, Integer.valueOf(i)), "updateState:AccountLoginRecSecurityRootComponent.setSelectedSecurityType");
        }
    }

    public C17770zR A16() {
        AnonymousClass1v8 r1 = (AnonymousClass1v8) super.A16();
        r1.A02 = new AnonymousClass5CH();
        return r1;
    }
}
