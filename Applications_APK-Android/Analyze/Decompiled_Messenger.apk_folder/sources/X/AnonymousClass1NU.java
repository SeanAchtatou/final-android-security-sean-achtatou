package X;

import android.os.Build;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.1NU  reason: invalid class name */
public final class AnonymousClass1NU {
    public static C30591iK A00(AnonymousClass1N0 r4, boolean z) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            int i2 = r4.A08.A04.A01;
            return new C37371vY(r4.A02(), i2, new AnonymousClass0ZG(i2));
        } else if (i >= 21 || !C30601iL.A00) {
            int i3 = r4.A08.A04.A01;
            return new C636237v(r4.A02(), i3, new AnonymousClass0ZG(i3));
        } else if (!z || i >= 19) {
            return (C30591iK) Class.forName("com.facebook.imagepipeline.platform.KitKatPurgeableDecoder").getConstructor(C22841Na.class).newInstance(r4.A03());
        } else {
            try {
                return (C30591iK) Class.forName("com.facebook.imagepipeline.platform.GingerbreadPurgeableDecoder").getConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                throw new RuntimeException("Wrong Native code setup, reflection failed.", e);
            }
        }
    }
}
