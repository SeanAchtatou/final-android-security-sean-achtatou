package X;

/* renamed from: X.195  reason: invalid class name */
public enum AnonymousClass195 {
    CHANNEL_CONNECTING(0),
    CHANNEL_CONNECTED(1),
    CHANNEL_DISCONNECTED(2),
    UNKNOWN(3);
    
    public boolean mClockSkewDetected;
    public final int value;

    private AnonymousClass195(int i) {
        this.value = i;
    }

    public static AnonymousClass195 A00(int i) {
        for (AnonymousClass195 r1 : values()) {
            if (r1.value == i) {
                return r1;
            }
        }
        return UNKNOWN;
    }
}
