package X;

/* renamed from: X.1Kf  reason: invalid class name and case insensitive filesystem */
public final class C22181Kf {
    public static final C22311Kv A00 = new C22191Kg(C32001kx.A01, false);
    public static final C22311Kv A01;
    public static final C22311Kv A02;
    public static final C22311Kv A03 = C17090yJ.A00;
    public static final C22311Kv A04 = new C22191Kg(null, false);
    public static final C22311Kv A05 = new C22191Kg(null, true);

    static {
        C22211Ki r1 = C22211Ki.A00;
        A01 = new C22191Kg(r1, false);
        A02 = new C22191Kg(r1, true);
    }
}
