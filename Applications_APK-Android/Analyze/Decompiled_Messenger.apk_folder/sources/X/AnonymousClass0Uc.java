package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0Uc  reason: invalid class name */
public final class AnonymousClass0Uc extends AnonymousClass0UV {
    public static final Handler A00() {
        return new Handler(Looper.getMainLooper());
    }
}
