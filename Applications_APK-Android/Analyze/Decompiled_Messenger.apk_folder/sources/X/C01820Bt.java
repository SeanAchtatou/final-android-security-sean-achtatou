package X;

import android.content.BroadcastReceiver;

/* renamed from: X.0Bt  reason: invalid class name and case insensitive filesystem */
public final class C01820Bt extends BroadcastReceiver {
    public final /* synthetic */ C01620Ay A00;

    public C01820Bt(C01620Ay r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00bd, code lost:
        if (r0 == false) goto L_0x00bf;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r11, android.content.Intent r12) {
        /*
            r10 = this;
            r0 = 1521646807(0x5ab27cd7, float:2.51199044E16)
            int r2 = X.C000700l.A01(r0)
            X.06j r0 = X.C006506l.A01()
            boolean r0 = r0.A02(r11, r10, r12)
            if (r0 != 0) goto L_0x0018
            r0 = -1525021651(0xffffffffa51a042d, float:-1.3358786E-16)
            X.C000700l.A0D(r12, r0, r2)
            return
        L_0x0018:
            if (r12 == 0) goto L_0x00cb
            X.0Ay r0 = r10.A00
            java.lang.String r1 = r0.A01()
            java.lang.String r0 = r12.getAction()
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00cb
            java.lang.String r0 = "extra_mqtt_endpoint"
            java.lang.String r5 = r12.getStringExtra(r0)
            java.lang.String r0 = "extra_analytics_endpoint"
            java.lang.String r4 = r12.getStringExtra(r0)
            java.lang.String r0 = "extra_fbns_endpoint"
            java.lang.String r8 = r12.getStringExtra(r0)
            java.lang.String r0 = "extra_fbns_analytics_endpoint"
            java.lang.String r7 = r12.getStringExtra(r0)
            X.0Ay r0 = r10.A00
            r0.A00()
            X.09k r1 = new X.09k
            X.09P r0 = r0.A04
            r1.<init>(r11, r0)
            boolean r0 = r1.A04(r12)
            if (r0 != 0) goto L_0x0082
            X.0Ay r0 = r10.A00
            java.lang.String r3 = r0.A00()
            java.lang.Object[] r1 = new java.lang.Object[]{r5, r4, r8, r7}
            java.lang.String r0 = "ignore unauthorized sender %s, %s, %s, %s"
            X.C010708t.A0P(r3, r0, r1)
            X.0Ay r0 = r10.A00
            X.09P r9 = r0.A04
            if (r9 == 0) goto L_0x007b
            java.lang.String r6 = r0.A00()
            java.lang.Object[] r3 = new java.lang.Object[]{r5, r4, r8, r7}
            java.lang.String r1 = "unauthorized endpoint request to %s, %s, %s, %s"
            r0 = 0
            java.lang.String r0 = java.lang.String.format(r0, r1, r3)
            r9.CGY(r6, r0)
        L_0x007b:
            r0 = -182740420(0xfffffffff51b9a3c, float:-1.9724957E32)
            X.C000700l.A0D(r12, r0, r2)
            return
        L_0x0082:
            X.0Ay r3 = r10.A00
            boolean r0 = r3.A05(r5)
            if (r0 == 0) goto L_0x00dc
            boolean r0 = r3.A05(r8)
            if (r0 == 0) goto L_0x00dc
            r3.A00()
            java.lang.Integer r1 = r3.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            if (r1 == r0) goto L_0x009d
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            if (r1 != r0) goto L_0x00ab
        L_0x009d:
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            if (r0 != 0) goto L_0x00a4
            r5 = r8
        L_0x00a4:
            boolean r0 = android.text.TextUtils.isEmpty(r7)
            if (r0 != 0) goto L_0x00ab
            r4 = r7
        L_0x00ab:
            java.lang.String r0 = r3.A07
            if (r0 != 0) goto L_0x00d7
            r0 = 0
            if (r5 != 0) goto L_0x00b3
            r0 = 1
        L_0x00b3:
            if (r0 == 0) goto L_0x00bf
            java.lang.String r0 = r3.A06
            if (r0 != 0) goto L_0x00d2
            r0 = 0
            if (r4 != 0) goto L_0x00bd
            r0 = 1
        L_0x00bd:
            if (r0 != 0) goto L_0x00cb
        L_0x00bf:
            r3.A04(r5, r4)
            r3.A07 = r5
            r3.A06 = r4
            X.07h r0 = r3.A01
            r0.A05()
        L_0x00cb:
            r0 = -1118987921(0xffffffffbd4d996f, float:-0.050195154)
            X.C000700l.A0D(r12, r0, r2)
            return
        L_0x00d2:
            boolean r0 = r0.equals(r4)
            goto L_0x00bd
        L_0x00d7:
            boolean r0 = r0.equals(r5)
            goto L_0x00b3
        L_0x00dc:
            java.lang.String r1 = r3.A00()
            java.lang.Object[] r0 = new java.lang.Object[]{r5, r4, r8, r7}
            java.lang.String r9 = "ignore illegal target endpoint switch %s, %s, %s, %s"
            X.C010708t.A0P(r1, r9, r0)
            X.0Ay r0 = r10.A00
            X.09P r6 = r0.A04
            if (r6 == 0) goto L_0x00ff
            java.lang.String r3 = r0.A00()
            java.lang.Object[] r1 = new java.lang.Object[]{r5, r4, r8, r7}
            r0 = 0
            java.lang.String r0 = java.lang.String.format(r0, r9, r1)
            r6.CGY(r3, r0)
        L_0x00ff:
            r0 = 2129518433(0x7eeddf61, float:1.5809344E38)
            X.C000700l.A0D(r12, r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C01820Bt.onReceive(android.content.Context, android.content.Intent):void");
    }
}
