package X;

import java.util.HashMap;

/* renamed from: X.0gA  reason: invalid class name and case insensitive filesystem */
public final class C08900gA {
    private static final HashMap A0F = new HashMap();
    public int A00 = 1;
    public int A01 = 100;
    public int A02 = -1;
    public int A03 = -1;
    public int A04 = -1;
    public boolean A05 = false;
    public boolean A06 = false;
    public boolean A07 = false;
    public boolean A08 = false;
    public boolean A09 = false;
    public boolean A0A = false;
    public boolean A0B = true;
    public boolean A0C = false;
    public final String A0D;
    public final short A0E;

    public static C08900gA A00(String str) {
        C08900gA r1 = new C08900gA(str);
        A0F.put(str, r1);
        return r1;
    }

    public static C08900gA A01(String str) {
        return (C08900gA) A0F.get(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        if (r3.A04 != -1) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A02() {
        /*
            r3 = this;
            int r1 = r3.A03
            r2 = -1
            if (r1 != r2) goto L_0x0015
            if (r1 != r2) goto L_0x000c
            int r1 = r3.A04
            r0 = 1
            if (r1 == r2) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            r1 = 2147483647(0x7fffffff, float:NaN)
            if (r0 == 0) goto L_0x0015
            r1 = 43200(0xa8c0, float:6.0536E-41)
        L_0x0015:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08900gA.A02():int");
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C08900gA)) {
            return false;
        }
        return ((C08900gA) obj).A0D.equals(this.A0D);
    }

    public int hashCode() {
        return this.A0D.hashCode();
    }

    private C08900gA(String str) {
        this.A0D = str;
        this.A0E = (short) str.hashCode();
    }
}
