package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.zero.common.ZeroToken;

/* renamed from: X.1m8  reason: invalid class name and case insensitive filesystem */
public final class C32671m8 implements Parcelable.Creator {
    public Object[] newArray(int i) {
        return new ZeroToken[0];
    }

    public Object createFromParcel(Parcel parcel) {
        return new ZeroToken(parcel);
    }
}
