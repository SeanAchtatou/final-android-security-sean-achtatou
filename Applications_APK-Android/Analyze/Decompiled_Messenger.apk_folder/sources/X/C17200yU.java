package X;

import com.facebook.secure.intentswitchoff.ActivityIntentSwitchOffDI;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0yU  reason: invalid class name and case insensitive filesystem */
public final class C17200yU {
    private static volatile C17200yU A02;
    public final C28111eH A00;
    public final ActivityIntentSwitchOffDI A01;

    public static final C17200yU A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C17200yU.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C17200yU(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C17200yU(AnonymousClass1XY r2) {
        this.A00 = C28111eH.A00(r2);
        this.A01 = ActivityIntentSwitchOffDI.A00(r2);
    }
}
