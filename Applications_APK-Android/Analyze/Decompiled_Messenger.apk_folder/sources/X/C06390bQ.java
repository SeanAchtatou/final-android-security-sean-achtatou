package X;

/* renamed from: X.0bQ  reason: invalid class name and case insensitive filesystem */
public final class C06390bQ {
    public final int A00;
    public final AnonymousClass0ZG A01;
    public final AnonymousClass0ZG A02;
    private final int A03;

    public static C06390bQ A00() {
        return new C06390bQ(8, 2, 16, 32);
    }

    public C16910xz A01() {
        C16910xz r1 = (C16910xz) this.A01.ALa();
        if (r1 == null) {
            r1 = new C16910xz(this.A03);
        }
        r1.A0A(this);
        return r1;
    }

    public C12240os A02() {
        C12240os r1 = (C12240os) this.A02.ALa();
        if (r1 == null) {
            r1 = new C12240os(this.A03);
        }
        r1.A0A(this);
        return r1;
    }

    private C06390bQ(int i, int i2, int i3, int i4) {
        this.A02 = new AnonymousClass0ZG(i);
        this.A01 = new AnonymousClass0ZG(i2);
        this.A03 = i3;
        this.A00 = i4;
    }
}
