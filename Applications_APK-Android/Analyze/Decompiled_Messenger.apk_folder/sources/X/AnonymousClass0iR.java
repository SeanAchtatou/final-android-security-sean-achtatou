package X;

import android.graphics.Typeface;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0iR  reason: invalid class name */
public final class AnonymousClass0iR extends AnonymousClass0Wl {
    private static volatile AnonymousClass0iR A02;
    private AnonymousClass0UN A00;
    public final AnonymousClass0j7 A01;

    public String getSimpleName() {
        return "FacebookEmojiTypefaceProviderImpl";
    }

    public static final AnonymousClass0iR A00(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (AnonymousClass0iR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A02 = new AnonymousClass0iR(applicationInjector, new AnonymousClass0iT(applicationInjector), AnonymousClass0iU.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public Typeface A01() {
        C26671bp r0 = (C26671bp) this.A01.A00();
        if (r0 == null) {
            return null;
        }
        return r0.A00;
    }

    private AnonymousClass0iR(AnonymousClass1XY r3, AnonymousClass0iT r4, AnonymousClass0iU r5) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = new AnonymousClass0j6(r4, r5, new C09780ig(r4));
    }

    public void init() {
        int A03 = C000700l.A03(41742711);
        if (!((C09960iy) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BLt, this.A00)).A04()) {
            C000700l.A09(-691410170, A03);
            return;
        }
        C09920iu.A00();
        this.A01.A03();
        C000700l.A09(970932210, A03);
    }
}
