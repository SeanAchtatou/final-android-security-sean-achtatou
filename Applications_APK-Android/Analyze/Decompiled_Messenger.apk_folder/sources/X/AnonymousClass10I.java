package X;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.widget.RelativeLayout;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.10I  reason: invalid class name */
public final class AnonymousClass10I extends C17770zR {
    public static final MigColorScheme A0A = C17190yT.A00();
    @Comparable(type = 3)
    public int A00;
    public AnonymousClass0UN A01;
    public AnonymousClass10N A02;
    public AnonymousClass10N A03;
    @Comparable(type = 13)
    public C202449gP A04;
    @Comparable(type = 13)
    public MigColorScheme A05 = A0A;
    @Comparable(type = 13)
    public CharSequence A06;
    @Comparable(type = 13)
    public String A07;
    @Comparable(type = 3)
    public boolean A08;
    @Comparable(type = 3)
    public boolean A09;

    public int A0M() {
        return 3;
    }

    public AnonymousClass10I(Context context) {
        super("EditFieldComponent");
        this.A01 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }

    public static void A00(AnonymousClass0p4 r6, MigColorScheme migColorScheme, C29818EjB ejB, C29822EjF ejF, boolean z, boolean z2) {
        int A002;
        int i;
        int A003 = C007106r.A00(r6.A09, 4.0f);
        C16930y3 B5d = AnonymousClass10J.A02.B5d(migColorScheme);
        if (z) {
            A002 = C007106r.A00(r6.A09, 32.0f);
        } else {
            A002 = C007106r.A00(r6.A09, (float) AnonymousClass1JQ.XSMALL.B3A());
        }
        int A004 = C007106r.A00(r6.A09, (float) AnonymousClass1JQ.SMALL.B3A());
        if (z2) {
            i = C007106r.A00(r6.A09, 60.0f);
        } else {
            i = A002;
        }
        ejB.setPadding(i, A004, A002, A004);
        ejB.setHintTextColor(AnonymousClass10J.A03.B5d(migColorScheme).AhV());
        ejB.setTextColor(AnonymousClass10J.A01.B5d(migColorScheme).AhV());
        ejB.setTextSize(2, (float) AnonymousClass10J.A01.mTextSize.B5k());
        ejF.setPadding(A003, A003, A003, A003);
        ejF.A02(B5d.AhV());
    }

    public void A0j(AnonymousClass0p4 r13, Object obj) {
        RelativeLayout relativeLayout = (RelativeLayout) obj;
        C202449gP r3 = this.A04;
        MigColorScheme migColorScheme = this.A05;
        int i = this.A00;
        CharSequence charSequence = this.A06;
        boolean z = this.A09;
        String str = this.A07;
        C29818EjB ejB = (C29818EjB) relativeLayout.getChildAt(0);
        C29822EjF ejF = (C29822EjF) relativeLayout.getChildAt(1);
        AnonymousClass0p4 r6 = r13;
        Typeface A002 = AnonymousClass10M.ROBOTO_REGULAR.A00(r13.A09);
        ejB.A00 = A002;
        if (A002 != null) {
            ejB.setTypeface(A002);
        }
        ejB.setHint(charSequence);
        ejB.setContentDescription(charSequence);
        ejB.A04 = r3;
        ejB.setImeOptions(i);
        ejB.A03 = ejF;
        if (Build.VERSION.SDK_INT > 26) {
            ejB.setImportantForAutofill(1);
            if (z) {
                ejB.setAutofillHints(new String[]{"phone"});
            } else {
                ejB.setAutofillHints(new String[]{"emailAddress", "username", "phone"});
            }
        }
        if (str != null) {
            ejB.setTag(str);
        }
        if (!ejB.getText().toString().equals(r3.A00) && (!TextUtils.isEmpty(ejB.getText().toString()) || !TextUtils.isEmpty(r3.A00))) {
            ejB.setText(r3.A00);
            ejB.setSelection(r3.A00.length());
        }
        ejF.A02 = true;
        ejF.A00 = ejB;
        ejF.setOnClickListener(ejF.A03);
        ejF.A01 = r3;
        ejF.A03();
        ejB.setBackgroundResource(2132214783);
        A00(r6, migColorScheme, ejB, ejF, false, z);
        if (z) {
            ejB.setInputType(AnonymousClass1Y3.AXs);
        }
    }
}
