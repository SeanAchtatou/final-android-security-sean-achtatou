package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1RW  reason: invalid class name */
public final class AnonymousClass1RW extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C26814DCo A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 13)
    public Boolean A03;
    @Comparable(type = 13)
    public Boolean A04;
    @Comparable(type = 13)
    public Boolean A05;
    @Comparable(type = 3)
    public boolean A06;
    @Comparable(type = 3)
    public boolean A07;
    @Comparable(type = 3)
    public boolean A08;

    public AnonymousClass1RW(Context context) {
        super("M4OmniMPreferencesLayout");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
