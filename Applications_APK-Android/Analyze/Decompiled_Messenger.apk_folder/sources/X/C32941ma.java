package X;

import com.facebook.secure.intentswitchoff.FbReceiverSwitchOffDI;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ma  reason: invalid class name and case insensitive filesystem */
public final class C32941ma extends AnonymousClass1ZS {
    private static volatile C32941ma A02;
    public AnonymousClass0UN A00;
    public boolean A01 = false;

    public static final C32941ma A00(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (C32941ma.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A02 = new C32941ma(applicationInjector, FbReceiverSwitchOffDI.A00(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.B4u, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static void A01(C32941ma r3, C35251qv r4) {
        if (!((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, r3.A00)).A0G()) {
            r4.A05(false, AnonymousClass07B.A01);
            return;
        }
        synchronized (r3) {
            r3.A01 = true;
        }
    }

    private C32941ma(AnonymousClass1XY r3, FbReceiverSwitchOffDI fbReceiverSwitchOffDI, AnonymousClass0US r5) {
        super(fbReceiverSwitchOffDI, r5);
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
