package X;

import com.fasterxml.jackson.databind.JsonSerializer;

/* renamed from: X.20F  reason: invalid class name */
public final class AnonymousClass20F {
    public final CYW map;
    public final JsonSerializer serializer;

    public AnonymousClass20F(JsonSerializer jsonSerializer, CYW cyw) {
        this.serializer = jsonSerializer;
        this.map = cyw;
    }
}
