package X;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import androidx.fragment.app.Fragment;

/* renamed from: X.14U  reason: invalid class name */
public abstract class AnonymousClass14U extends C13020qR implements LayoutInflater.Factory {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass14U(Context context) {
        super(context instanceof Activity ? (Activity) context : null, context, new Handler(context.getMainLooper()));
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        Fragment fragment = null;
        if (!"fragment".equals(str)) {
            return null;
        }
        AttributeSet attributeSet2 = attributeSet;
        String attributeValue = attributeSet2.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet2, AnonymousClass45O.A00);
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(0);
        }
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (resourceId == -1 && string == null) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S(attributeSet2.getPositionDescription(), ": Must specify unique android:id, android:tag, or have a parent with ", "an id for ", attributeValue));
        }
        C13060qW r5 = this.A03;
        if (resourceId != -1) {
            fragment = r5.A0O(resourceId);
        }
        if (fragment == null && string != null) {
            fragment = r5.A0Q(string);
        }
        String hexString = Integer.toHexString(resourceId);
        if (fragment == null) {
            fragment = Fragment.A02(this.A01, attributeValue, null);
            fragment.A0Z = true;
            int i = -1;
            if (resourceId != 0) {
                i = resourceId;
            }
            fragment.A0E = i;
            fragment.A0D = -1;
            fragment.A0T = string;
            fragment.A0d = true;
            fragment.A0P = r5;
            fragment.A1K(this.A01, attributeSet2, fragment.A01);
            r5.A0h(fragment);
        } else if (!fragment.A0d) {
            fragment.A0d = true;
            if (!fragment.A0h) {
                fragment.A1K(this.A01, attributeSet2, fragment.A01);
            }
            r5.A0s(fragment, r5.A01);
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0W(attributeSet2.getPositionDescription(), ": Duplicate id 0x", hexString, ", tag ", string, ", or parent id 0x", Integer.toHexString(-1), " with another fragment for ", attributeValue));
        }
        View view = fragment.A0I;
        if (view != null) {
            if (resourceId != 0) {
                view.setId(resourceId);
            }
            if (fragment.A0I.getTag() == null) {
                fragment.A0I.setTag(string);
            }
            return fragment.A0I;
        }
        throw new IllegalStateException(AnonymousClass08S.A0P("Fragment ", attributeValue, " did not create a view."));
    }
}
