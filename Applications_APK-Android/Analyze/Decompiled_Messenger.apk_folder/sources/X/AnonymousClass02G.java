package X;

import android.os.Handler;
import android.os.HandlerThread;
import java.io.File;
import java.util.Properties;

/* renamed from: X.02G  reason: invalid class name */
public final class AnonymousClass02G {
    public static final Properties A03 = new Properties();
    public char A00 = '!';
    public final Handler A01;
    public final File A02;

    public AnonymousClass02G(File file) {
        this.A02 = file;
        HandlerThread handlerThread = new HandlerThread("ForegroundEntityMapper");
        handlerThread.start();
        this.A01 = new Handler(handlerThread.getLooper());
    }
}
