package X;

import android.os.Process;
import com.facebook.systrace.TraceDirect;
import io.card.payment.BuildConfig;

/* renamed from: X.1d5  reason: invalid class name and case insensitive filesystem */
public final class C27371d5 implements AnonymousClass1XT, AnonymousClass03a, AnonymousClass1XU {
    private final long A00;
    private volatile boolean A01;

    public void Bsf() {
        this.A01 = false;
    }

    public void Bse() {
        this.A01 = AnonymousClass08Z.A05(this.A00);
    }

    public boolean isEnabled() {
        return this.A01;
    }

    public C27371d5(long j) {
        this.A00 = j;
    }

    public void BN6(AnonymousClass1XV r7) {
        AnonymousClass09R r1;
        long j;
        int i;
        int i2;
        if (isEnabled()) {
            C02320Ea A002 = AnonymousClass0EY.A00(this.A00, AnonymousClass0EY.A00, r7.A06);
            A002.A02("runnable_parent", AnonymousClass8t0.A00(r7));
            A002.A02("runnable_identifier", String.valueOf(r7.A04.A00));
            if (!r7.A00(1)) {
                A002.A02("indirect_edge", true);
            }
            if (r7.A00(2)) {
                A002.A02("manual_point", true);
            }
            A002.A00("app_custom_type", r7.A03);
            A002.A03();
            if (r7.A00(2)) {
                C04260Tf r0 = r7.A04;
                long j2 = r0.A01;
                int i3 = (((int) (j2 ^ (j2 >>> 32))) * 31) + r0.A00;
                long j3 = this.A00;
                String str = r7.A06;
                if (!AnonymousClass08Z.A05(j3)) {
                    return;
                }
                if (TraceDirect.checkNative()) {
                    TraceDirect.nativeStartAsyncFlow(str, i3);
                    return;
                }
                r1 = new AnonymousClass09R('s');
                r1.A01(Process.myPid());
                r1.A03(str);
                r1.A01(i3);
            } else {
                C04260Tf r12 = r7.A04.A02;
                boolean z = false;
                if (r12 != null) {
                    z = true;
                }
                if (!z) {
                    i2 = -1;
                } else {
                    if (r12 != null) {
                        j = r12.A01;
                    } else {
                        j = -1;
                    }
                    if (r12 != null) {
                        i = r12.A00;
                    } else {
                        i = -1;
                    }
                    i2 = (((int) (j ^ (j >>> 32))) * 31) + i;
                }
                if (i2 != -1) {
                    long j4 = this.A00;
                    String str2 = r7.A06;
                    if (!AnonymousClass08Z.A05(j4)) {
                        return;
                    }
                    if (TraceDirect.checkNative()) {
                        TraceDirect.nativeEndAsyncFlow(str2, i2);
                        return;
                    }
                    r1 = new AnonymousClass09R('f');
                    r1.A01(Process.myPid());
                    r1.A03(str2);
                    r1.A01(i2);
                } else {
                    return;
                }
            }
            AnonymousClass0HL.A00(r1.toString());
        }
    }

    public void BVi(AnonymousClass1XV r5) {
        if (isEnabled()) {
            AnonymousClass0EY.A00(this.A00, AnonymousClass0EY.A01, BuildConfig.FLAVOR).A03();
        }
    }
}
