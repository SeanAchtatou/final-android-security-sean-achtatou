package X;

import android.content.Context;

/* renamed from: X.0QF  reason: invalid class name */
public final class AnonymousClass0QF implements AnonymousClass0AW {
    private Context A00;

    public AnonymousClass0B0 AbP(Integer num) {
        return new AnonymousClass0QG(AnonymousClass0B4.A00.A01(this.A00, AnonymousClass08S.A0J("rti.mqtt.", C01450Ah.A00(num)), C01450Ah.A01(num)));
    }

    public AnonymousClass0QF(Context context) {
        this.A00 = context;
    }
}
