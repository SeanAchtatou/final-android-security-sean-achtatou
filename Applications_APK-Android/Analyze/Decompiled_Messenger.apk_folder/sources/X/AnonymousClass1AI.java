package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1AI  reason: invalid class name */
public final class AnonymousClass1AI {
    public Map A00 = new HashMap();
    public Map A01 = new HashMap();

    public static AnonymousClass1AI A00(AnonymousClass1AI r6) {
        AnonymousClass1AI r5 = (AnonymousClass1AI) AnonymousClass1AH.A00.A00();
        if (r5 == null) {
            r5 = new AnonymousClass1AI();
        }
        if (!r6.A00.isEmpty()) {
            for (String str : r6.A00.keySet()) {
                r5.A00.put(str, new ArrayList((Collection) r6.A00.get(str)));
            }
            for (String str2 : r6.A01.keySet()) {
                r5.A01.put(str2, new ArrayList((Collection) r6.A01.get(str2)));
            }
        }
        return r5;
    }

    public static void A01(Map map, Map map2, String str) {
        List list = (List) map2.get(str);
        List list2 = (List) map.remove(str);
        if (!(list == null || list2 == null)) {
            list2.removeAll(list);
        }
        if (list2 != null && !list2.isEmpty()) {
            map.put(str, list2);
        }
    }
}
