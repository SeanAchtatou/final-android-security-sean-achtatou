package X;

import android.os.Bundle;
import com.facebook.browserextensions.ipc.messengerplatform.GetEnvironmentJSBridgeCall;
import com.facebook.businessextension.jscalls.BusinessExtensionJSBridgeCall;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1yM  reason: invalid class name and case insensitive filesystem */
public final class C39011yM implements C53672lB {
    private AnonymousClass0UN A00;

    public String Arh() {
        return "getEnvironment";
    }

    public static final C39011yM A00(AnonymousClass1XY r1) {
        return new C39011yM(r1);
    }

    public void BAh(BusinessExtensionJSBridgeCall businessExtensionJSBridgeCall, AnonymousClass9t9 r7) {
        GetEnvironmentJSBridgeCall getEnvironmentJSBridgeCall = (GetEnvironmentJSBridgeCall) businessExtensionJSBridgeCall;
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A3k, false)) {
            getEnvironmentJSBridgeCall.A06(C208829t7.A00(AnonymousClass07B.A0K));
            return;
        }
        String Afq = getEnvironmentJSBridgeCall.Afq();
        Bundle bundle = new Bundle();
        bundle.putString("callbackID", Afq);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("host_app", "messenger");
            jSONObject.put("host_platform", "android");
            bundle.putString(AnonymousClass80H.$const$string(51), jSONObject.toString());
        } catch (JSONException e) {
            BYY.A02("getEnvironment", e, AnonymousClass80H.$const$string(31), e);
        }
        getEnvironmentJSBridgeCall.ATa(bundle);
    }

    private C39011yM(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
