package X;

import android.app.Activity;
import android.content.Intent;
import android.preference.Preference;
import com.facebook.selfupdate2.UpgradeInfoActivity;

/* renamed from: X.1yA  reason: invalid class name and case insensitive filesystem */
public final class C38891yA implements Preference.OnPreferenceClickListener {
    public final /* synthetic */ Activity A00;

    public C38891yA(Activity activity) {
        this.A00 = activity;
    }

    public boolean onPreferenceClick(Preference preference) {
        C417626w.A06(new Intent(this.A00, UpgradeInfoActivity.class), this.A00);
        return true;
    }
}
