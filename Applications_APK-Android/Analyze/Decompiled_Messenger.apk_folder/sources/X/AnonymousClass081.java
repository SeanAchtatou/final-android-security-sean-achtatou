package X;

import java.util.List;

/* renamed from: X.081  reason: invalid class name */
public final class AnonymousClass081 implements AnonymousClass0CZ {
    public final /* synthetic */ AnonymousClass085 A00;
    public final /* synthetic */ List A01;
    public final /* synthetic */ List A02;

    public AnonymousClass081(AnonymousClass085 r1, List list, List list2) {
        this.A00 = r1;
        this.A01 = list;
        this.A02 = list2;
    }

    public void BYW() {
        AnonymousClass085 r0 = this.A00;
        r0.A01.A0Y(r0.A02);
    }

    public void BqV(long j) {
        AnonymousClass085 r0 = this.A00;
        r0.A01.A0c(this.A01, this.A02, r0.A02);
    }
}
