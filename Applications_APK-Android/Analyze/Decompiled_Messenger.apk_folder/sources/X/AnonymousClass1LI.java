package X;

import java.util.Arrays;

/* renamed from: X.1LI  reason: invalid class name */
public final class AnonymousClass1LI {
    public final String A00;
    public final String A01;
    public final String A02;

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass1LI)) {
            return super.equals(obj);
        }
        AnonymousClass1LI r3 = (AnonymousClass1LI) obj;
        if (!C14620th.A01(this.A01, r3.A01) || !C14620th.A01(this.A02, r3.A02) || !C14620th.A01(this.A00, r3.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, this.A02, this.A00});
    }

    public AnonymousClass1LI(String str, String str2, String str3) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = str3;
    }
}
