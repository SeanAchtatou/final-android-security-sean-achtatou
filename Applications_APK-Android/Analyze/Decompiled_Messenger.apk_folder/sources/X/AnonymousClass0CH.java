package X;

import android.os.SystemClock;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0CH  reason: invalid class name */
public final class AnonymousClass0CH implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.protocol.MqttClient$6";
    public final /* synthetic */ int A00;
    public final /* synthetic */ long A01;
    public final /* synthetic */ AnonymousClass0CU A02;
    public final /* synthetic */ AnonymousClass0C8 A03;
    public final /* synthetic */ AnonymousClass0CZ A04;
    public final /* synthetic */ Integer A05;
    public final /* synthetic */ String A06;
    public final /* synthetic */ String A07;
    public final /* synthetic */ byte[] A08;

    public AnonymousClass0CH(AnonymousClass0C8 r1, String str, byte[] bArr, Integer num, int i, AnonymousClass0CZ r6, long j, String str2, AnonymousClass0CU r10) {
        this.A03 = r1;
        this.A07 = str;
        this.A08 = bArr;
        this.A05 = num;
        this.A00 = i;
        this.A04 = r6;
        this.A01 = j;
        this.A06 = str2;
        this.A02 = r10;
    }

    public void run() {
        AtomicReference atomicReference;
        String str;
        AnonymousClass0CV r12;
        long j;
        int i;
        AnonymousClass0C8 r6 = this.A03;
        String str2 = this.A07;
        byte[] bArr = this.A08;
        int A002 = AnonymousClass08G.A00(this.A05);
        int i2 = this.A00;
        AnonymousClass0CZ r5 = this.A04;
        long j2 = this.A01;
        String str3 = this.A06;
        Object BJL = r6.A02.BJL(str2, bArr);
        try {
            r6.A05((long) (r6.A0E.A03 * AnonymousClass1Y3.A87));
            if (!r6.A09()) {
                if (r5 != null) {
                    r5.BYW();
                }
                r6.A02.BJQ(BJL, false, "not_connected");
            } else {
                if ("/t_sm".equals(str2) && (atomicReference = r6.A0I) != null) {
                    Integer num = (Integer) atomicReference.get();
                    if (num != null) {
                        str = String.valueOf(num);
                    } else {
                        str = null;
                    }
                    if ((str == null && str3 != null) || (str != null && !str.equals(str3))) {
                        AnonymousClass0CC r1 = r6.A0Y;
                        if (r1 != null) {
                            AnonymousClass0CI r11 = new AnonymousClass0CI(AnonymousClass07B.A01);
                            AnonymousClass0BN r13 = r1.A02.A0L;
                            synchronized (r13.A04) {
                                r12 = (AnonymousClass0CV) r13.A04.remove(Integer.valueOf(i2));
                            }
                            if (r12 != null) {
                                r12.A03(r11);
                                AnonymousClass0C8 r0 = r12.A03;
                                if (r0 == null) {
                                    j = 0;
                                } else {
                                    j = r0.A0W;
                                }
                                int AwM = r12.AwM();
                                boolean z = r12 instanceof C02070Cs;
                                if (z) {
                                    AwM = ((C02070Cs) r12).A00;
                                }
                                if (z) {
                                    i = ((C02070Cs) r12).A01;
                                } else {
                                    i = 0;
                                }
                                r13.A00.A05("op_failed", r12.A05, AnonymousClass08G.A00(AnonymousClass07B.A01), i2, AwM, r11, i, j);
                            }
                        }
                        if (r5 != null) {
                            r5.BYW();
                        }
                        r6.A02.BJQ(BJL, false, "ref_code_expired");
                    }
                }
                r6.A0C.C5Q(str2, bArr, A002, i2);
                if (j2 > 0) {
                    ((C02050Cq) r6.A08.A07(C02050Cq.class)).A03(C02060Cr.StackSendingLatencyMs, SystemClock.elapsedRealtime() - j2);
                }
                AnonymousClass0CC r14 = r6.A0Y;
                if (r14 != null) {
                    r14.A01(AnonymousClass0CL.A08.name(), i2);
                }
                if (r5 != null) {
                    r5.BqV(SystemClock.elapsedRealtime());
                }
                r6.A02.BJQ(BJL, true, null);
            }
        } catch (Throwable th) {
            AnonymousClass0C8.A03(r6, AnonymousClass0CE.A01(th), AnonymousClass0CG.A06, th);
            if (r5 != null) {
                r5.BYW();
            }
            r6.A02.BJQ(BJL, false, AnonymousClass08S.A0J("publish_exception:", th.getMessage()));
        }
        this.A02.A00();
    }
}
