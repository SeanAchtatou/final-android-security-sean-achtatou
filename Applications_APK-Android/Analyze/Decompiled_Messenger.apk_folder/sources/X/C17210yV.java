package X;

import android.content.Intent;
import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.0yV  reason: invalid class name and case insensitive filesystem */
public final class C17210yV implements AnonymousClass0ZM {
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C17210yV(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r7) {
        AnonymousClass0r6 r4 = this.A00;
        ((C04460Ut) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AKb, r4.A02)).C4x(new Intent("com.facebook.presence.PRESENCE_MANAGER_SETTINGS_CHANGED"));
        AnonymousClass0r6.A0C(r4, true);
    }
}
