package X;

/* renamed from: X.1cr  reason: invalid class name and case insensitive filesystem */
public abstract class C27231cr implements C05280Yi {
    private C05180Xy A00 = null;
    private boolean A01 = false;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0014, code lost:
        r2 = 0;
        r1 = r0.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
        if (r2 >= r1) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
        ((X.C20241Bq) r3.A00.A00.A07(r2)).BMF(r3);
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        r3.A00 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        r0 = r3.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r0 == null) goto L_?;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.BEO()     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r3)     // Catch:{ all -> 0x002f }
            return
        L_0x0009:
            r3.A05()     // Catch:{ all -> 0x002f }
            r0 = 1
            r3.A01 = r0     // Catch:{ all -> 0x002f }
            monitor-exit(r3)     // Catch:{ all -> 0x002f }
            X.0Xy r0 = r3.A00
            if (r0 == 0) goto L_0x002e
            r2 = 0
            int r1 = r0.size()
        L_0x0019:
            if (r2 >= r1) goto L_0x002b
            X.0Xy r0 = r3.A00
            X.04b r0 = r0.A00
            java.lang.Object r0 = r0.A07(r2)
            X.1Bq r0 = (X.C20241Bq) r0
            r0.BMF(r3)
            int r2 = r2 + 1
            goto L_0x0019
        L_0x002b:
            r0 = 0
            r3.A00 = r0
        L_0x002e:
            return
        L_0x002f:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27231cr.A03():void");
    }

    public final void A04(C20241Bq r2) {
        boolean z;
        synchronized (this) {
            if (BEO()) {
                z = true;
            } else {
                if (this.A00 == null) {
                    this.A00 = new C05180Xy();
                }
                this.A00.add(r2);
                z = false;
            }
        }
        if (z) {
            r2.BMF(this);
        }
    }

    public void A05() {
        C27171cl r3 = ((C27221cq) this).A00;
        synchronized (r3) {
            if (r3.A04 == C27181cm.INIT) {
                C010708t.A0L("DefaultBlueServiceOperation", "onDisposeInternal() was called with INIT state", new IllegalStateException());
            }
            r3.A04 = C27181cm.COMPLETED;
            r3.A09 = null;
            C27171cl.A02(r3);
            r3.A07 = null;
            C156107Jr r0 = r3.A05;
            if (r0 != null) {
                r0.CI0();
            }
            r3.A0L.cancel(false);
        }
    }

    public final synchronized boolean BEO() {
        return this.A01;
    }
}
