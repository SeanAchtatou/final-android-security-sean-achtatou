package X;

import java.util.concurrent.Executor;

/* renamed from: X.1Yo  reason: invalid class name and case insensitive filesystem */
public final class C25161Yo {
    public C25161Yo A00;
    public final Runnable A01;
    public final Executor A02;

    public C25161Yo(Runnable runnable, Executor executor, C25161Yo r3) {
        this.A01 = runnable;
        this.A02 = executor;
        this.A00 = r3;
    }
}
