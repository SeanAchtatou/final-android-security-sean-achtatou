package X;

import com.google.common.collect.ImmutableSet;
import java.util.AbstractMap;
import java.util.Set;

/* renamed from: X.080  reason: invalid class name */
public final class AnonymousClass080 extends AbstractMap<String, Object> {
    private final ImmutableSet A00;

    public AnonymousClass080(Object obj) {
        AnonymousClass1CJ A04 = AnonymousClass1CJ.A00(C04300To.A04(obj.getClass().getDeclaredFields())).A04(new C03350Nj(obj));
        this.A00 = ImmutableSet.A03((Iterable) A04.A00.or(A04));
    }

    public Set entrySet() {
        return this.A00;
    }
}
