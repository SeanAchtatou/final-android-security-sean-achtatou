package X;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.NullNode;
import io.card.payment.BuildConfig;

/* renamed from: X.13u  reason: invalid class name and case insensitive filesystem */
public abstract class C185013u extends C185113v implements C10540kM {
    public final JsonNodeFactory _nodeFactory;

    public String asText() {
        return BuildConfig.FLAVOR;
    }

    public abstract C182811d asToken();

    public abstract JsonNode get(int i);

    public abstract JsonNode get(String str);

    public abstract int size();

    public final BooleanNode booleanNode(boolean z) {
        if (z) {
            return BooleanNode.TRUE;
        }
        return BooleanNode.FALSE;
    }

    public final NullNode nullNode() {
        return NullNode.instance;
    }

    public final C14680to numberNode(int i) {
        if (i > 10 || i < -1) {
            return new AnonymousClass14O(i);
        }
        return AnonymousClass14O.CANONICALS[i - -1];
    }

    public C185013u(JsonNodeFactory jsonNodeFactory) {
        this._nodeFactory = jsonNodeFactory;
    }
}
