package X;

import android.text.Spannable;
import android.text.SpannableString;
import com.facebook.ui.emoji.model.BasicEmoji;
import com.facebook.ui.emoji.model.Emoji;

/* renamed from: X.1rN  reason: invalid class name and case insensitive filesystem */
public abstract class C35531rN {
    public static Spannable A01(C35531rN r7, CharSequence charSequence, int i, int i2, int i3, boolean z) {
        int i4;
        Object A05;
        Object obj;
        int charCount;
        Spannable spannable = null;
        while (true) {
            boolean z2 = true;
            while (r9 < i2) {
                int codePointAt = Character.codePointAt(charSequence, r9);
                if (Character.isWhitespace(codePointAt)) {
                    i = r9 + Character.charCount(codePointAt);
                } else if (!z2) {
                    r9 += Character.charCount(codePointAt);
                } else {
                    int A01 = AnonymousClass1LN.A00.A01(charSequence, r9, i2);
                    if (A01 <= r9) {
                        r9 += Character.charCount(codePointAt);
                    } else {
                        if (A01 < i2) {
                            i4 = Character.codePointAt(charSequence, A01);
                        } else {
                            i4 = 32;
                        }
                        if (!Character.isWhitespace(i4)) {
                            r9 = Character.charCount(i4) + A01;
                        } else {
                            String A07 = r7.A07(Emoji.A02(charSequence, r9, A01));
                            if (A07 == null) {
                                A05 = null;
                                obj = null;
                            } else if (r7.A0B()) {
                                A05 = r7.A06(A07, 0, A07.length(), i3);
                                obj = A05;
                            } else {
                                A05 = r7.A05(new BasicEmoji(A07), i3);
                                obj = A05;
                            }
                            if (A05 == null) {
                                charCount = Character.charCount(i4);
                            } else {
                                if (spannable == null) {
                                    if (z || !(charSequence instanceof Spannable)) {
                                        spannable = new SpannableString(charSequence);
                                        if (spannable.length() < i2) {
                                            r7.A09("Background modification: %d -> %d (%d)", Integer.valueOf(i2), Integer.valueOf(spannable.length()), Integer.valueOf(charSequence.length()));
                                            return null;
                                        }
                                    } else {
                                        spannable = (Spannable) charSequence;
                                    }
                                }
                                spannable.setSpan(obj, r9, A01, 33);
                                charCount = Character.charCount(i4);
                            }
                            i = charCount + A01;
                        }
                    }
                    z2 = false;
                }
            }
            return spannable;
        }
    }

    public Object A04(int i) {
        return new C35751rl("FacebookEmoji", ((C182010s) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCw, ((AnonymousClass1LL) this).A00)).A00.A01(), i);
    }

    public Object A05(Emoji emoji, int i) {
        return new C60362x1(((C182010s) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCw, ((AnonymousClass1LL) this).A00)).A00.A01(), emoji, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0100, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01af, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:102:0x01b3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x0104 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A06(java.lang.CharSequence r21, int r22, int r23, int r24) {
        /*
            r20 = this;
            r8 = r22
            r0 = r20
            X.1LL r0 = (X.AnonymousClass1LL) r0
            int r1 = X.AnonymousClass1Y3.Ayo
            X.0UN r2 = r0.A00
            r0 = 3
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r1, r2)
            X.6Q7 r7 = (X.AnonymousClass6Q7) r7
            int r1 = X.AnonymousClass1Y3.AEg
            r0 = 0
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r0, r1, r2)
            android.content.res.Resources r6 = (android.content.res.Resources) r6
            java.io.File r1 = r7.A01()
            r11 = 0
            if (r1 == 0) goto L_0x0159
            java.util.concurrent.atomic.AtomicReference r0 = r7.A01
            java.lang.Object r5 = r0.get()
            X.8uU r5 = (X.C190898uU) r5
            boolean r0 = r7.A02
            if (r0 == 0) goto L_0x0033
            if (r5 == 0) goto L_0x0117
            java.io.File r0 = r5.A00
            if (r0 == r1) goto L_0x0117
        L_0x0033:
            r13 = r1
            r12 = 0
            if (r1 == 0) goto L_0x010f
            java.io.RandomAccessFile r4 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0105 }
            java.lang.String r0 = "r"
            r4.<init>(r1, r0)     // Catch:{ IOException -> 0x0105 }
            X.8uT r10 = new X.8uT     // Catch:{ all -> 0x00fe }
            r10.<init>(r4)     // Catch:{ all -> 0x00fe }
            r14 = 4
            r10.A02(r14)     // Catch:{ all -> 0x00fe }
            java.nio.ByteBuffer r1 = r10.A01     // Catch:{ all -> 0x00fe }
            r0 = 0
            r1.position(r0)     // Catch:{ all -> 0x00fe }
            r0 = 2
            X.C190888uT.A00(r10, r0)     // Catch:{ all -> 0x00fe }
            java.nio.ByteBuffer r0 = r10.A01     // Catch:{ all -> 0x00fe }
            short r3 = r0.getShort()     // Catch:{ all -> 0x00fe }
            r0 = 65535(0xffff, float:9.1834E-41)
            r3 = r3 & r0
            r0 = 100
            if (r3 > r0) goto L_0x00e8
            r0 = 6
            r10.A02(r0)     // Catch:{ all -> 0x00fe }
            r9 = 0
            r2 = 0
        L_0x0065:
            r15 = -1
            if (r2 >= r3) goto L_0x0089
            java.nio.ByteBuffer r1 = r10.A01     // Catch:{ all -> 0x00fe }
            r1.position(r9)     // Catch:{ all -> 0x00fe }
            X.C190888uT.A00(r10, r14)     // Catch:{ all -> 0x00fe }
            java.nio.ByteBuffer r0 = r10.A01     // Catch:{ all -> 0x00fe }
            int r1 = r0.getInt()     // Catch:{ all -> 0x00fe }
            r10.A02(r14)     // Catch:{ all -> 0x00fe }
            long r18 = r10.A01()     // Catch:{ all -> 0x00fe }
            r10.A02(r14)     // Catch:{ all -> 0x00fe }
            r0 = 1835365473(0x6d657461, float:4.4382975E27)
            if (r0 == r1) goto L_0x008b
            int r2 = r2 + 1
            goto L_0x0065
        L_0x0089:
            r18 = -1
        L_0x008b:
            int r0 = (r18 > r15 ? 1 : (r18 == r15 ? 0 : -1))
            if (r0 == 0) goto L_0x00f6
            long r2 = r10.A00     // Catch:{ all -> 0x00fe }
            long r0 = r18 - r2
            int r2 = (int) r0     // Catch:{ all -> 0x00fe }
            r10.A02(r2)     // Catch:{ all -> 0x00fe }
            r0 = 12
            r10.A02(r0)     // Catch:{ all -> 0x00fe }
            long r16 = r10.A01()     // Catch:{ all -> 0x00fe }
        L_0x00a0:
            long r0 = (long) r9     // Catch:{ all -> 0x00fe }
            int r2 = (r0 > r16 ? 1 : (r0 == r16 ? 0 : -1))
            if (r2 >= 0) goto L_0x00e0
            java.nio.ByteBuffer r1 = r10.A01     // Catch:{ all -> 0x00fe }
            r0 = 0
            r1.position(r0)     // Catch:{ all -> 0x00fe }
            r0 = 4
            X.C190888uT.A00(r10, r0)     // Catch:{ all -> 0x00fe }
            java.nio.ByteBuffer r0 = r10.A01     // Catch:{ all -> 0x00fe }
            int r1 = r0.getInt()     // Catch:{ all -> 0x00fe }
            long r2 = r10.A01()     // Catch:{ all -> 0x00fe }
            long r14 = r10.A01()     // Catch:{ all -> 0x00fe }
            r0 = 1114599275(0x426f6f6b, float:59.858807)
            if (r0 != r1) goto L_0x00d9
            long r2 = r2 + r18
            r0 = 32
            long r2 = r2 << r0
            long r2 = r2 | r14
            long r0 = r2 >> r0
            int r9 = (int) r2     // Catch:{ all -> 0x00fe }
            byte[] r2 = new byte[r9]     // Catch:{ all -> 0x00fe }
            r4.seek(r0)     // Catch:{ all -> 0x00fe }
            r4.read(r2)     // Catch:{ all -> 0x00fe }
            X.8uU r0 = new X.8uU     // Catch:{ all -> 0x00fe }
            r0.<init>(r13, r2)     // Catch:{ all -> 0x00fe }
            goto L_0x00dc
        L_0x00d9:
            int r9 = r9 + 1
            goto L_0x00a0
        L_0x00dc:
            r4.close()     // Catch:{ IOException -> 0x0105 }
            goto L_0x010e
        L_0x00e0:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x00fe }
            java.lang.String r0 = "Cannot find meta subtable."
            r2.<init>(r0)     // Catch:{ all -> 0x00fe }
            goto L_0x00fd
        L_0x00e8:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x00fe }
            java.lang.String r1 = "Font tables are probably malformed! ("
            java.lang.String r0 = ")"
            java.lang.String r0 = X.AnonymousClass08S.A0A(r1, r3, r0)     // Catch:{ all -> 0x00fe }
            r2.<init>(r0)     // Catch:{ all -> 0x00fe }
            goto L_0x00fd
        L_0x00f6:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x00fe }
            java.lang.String r0 = "Cannot find meta table."
            r2.<init>(r0)     // Catch:{ all -> 0x00fe }
        L_0x00fd:
            throw r2     // Catch:{ all -> 0x00fe }
        L_0x00fe:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0100 }
        L_0x0100:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x0104 }
        L_0x0104:
            throw r0     // Catch:{ IOException -> 0x0105 }
        L_0x0105:
            r2 = move-exception
            java.lang.Class<X.EuL> r1 = X.C30337EuL.class
            java.lang.String r0 = "Unable to read tables"
            X.C010708t.A08(r1, r0, r2)
            goto L_0x010f
        L_0x010e:
            r12 = r0
        L_0x010f:
            java.util.concurrent.atomic.AtomicReference r0 = r7.A01
            r0.compareAndSet(r5, r12)
            r0 = 1
            r7.A02 = r0
        L_0x0117:
            java.util.concurrent.atomic.AtomicReference r0 = r7.A01
            java.lang.Object r9 = r0.get()
            X.8uU r9 = (X.C190898uU) r9
            if (r9 == 0) goto L_0x0159
            r4 = -2128831035(0xffffffff811c9dc5, float:-2.876587E-38)
        L_0x0124:
            r0 = r23
            if (r8 >= r0) goto L_0x013e
            r0 = r21
            int r1 = java.lang.Character.codePointAt(r0, r8)
            int r0 = java.lang.Character.charCount(r1)
            int r8 = r8 + r0
            r0 = 65039(0xfe0f, float:9.1139E-41)
            if (r1 == r0) goto L_0x0124
            r4 = r4 ^ r1
            r0 = 16777619(0x1000193, float:2.3511016E-38)
            int r4 = r4 * r0
            goto L_0x0124
        L_0x013e:
            android.util.SparseArray r1 = r7.A00
            monitor-enter(r1)
            android.util.SparseArray r0 = r7.A00     // Catch:{ all -> 0x014b }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x014b }
            java.lang.ref.SoftReference r0 = (java.lang.ref.SoftReference) r0     // Catch:{ all -> 0x014b }
            monitor-exit(r1)     // Catch:{ all -> 0x014b }
            goto L_0x014e
        L_0x014b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x014b }
            throw r0
        L_0x014e:
            if (r0 == 0) goto L_0x015d
            java.lang.Object r5 = r0.get()
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5
            if (r5 == 0) goto L_0x015d
        L_0x0158:
            r11 = r5
        L_0x0159:
            if (r11 != 0) goto L_0x01bd
            r1 = 0
            return r1
        L_0x015d:
            int[] r0 = r9.A01
            int r0 = java.util.Arrays.binarySearch(r0, r4)
            if (r0 < 0) goto L_0x0159
            int[] r1 = r9.A02
            int r0 = r0 << 1
            r5 = r1[r0]
            int r0 = r0 + 1
            r2 = r1[r0]
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x01b4 }
            java.io.File r1 = r9.A00     // Catch:{ IOException -> 0x01b4 }
            java.lang.String r0 = "r"
            r3.<init>(r1, r0)     // Catch:{ IOException -> 0x01b4 }
            long r0 = (long) r5     // Catch:{ IOException -> 0x01b4 }
            r3.seek(r0)     // Catch:{ all -> 0x01ad }
            X.0ZG r0 = X.AnonymousClass6Q7.A03     // Catch:{ all -> 0x01ad }
            java.lang.Object r1 = r0.ALa()     // Catch:{ all -> 0x01ad }
            byte[] r1 = (byte[]) r1     // Catch:{ all -> 0x01ad }
            if (r1 == 0) goto L_0x0189
            int r0 = r1.length     // Catch:{ all -> 0x01ad }
            if (r0 >= r2) goto L_0x018b
        L_0x0189:
            byte[] r1 = new byte[r2]     // Catch:{ all -> 0x01ad }
        L_0x018b:
            r0 = 0
            r3.read(r1, r0, r2)     // Catch:{ all -> 0x01ad }
            android.graphics.Bitmap r5 = android.graphics.BitmapFactory.decodeByteArray(r1, r0, r2)     // Catch:{ all -> 0x01ad }
            X.0ZG r0 = X.AnonymousClass6Q7.A03     // Catch:{ all -> 0x01ad }
            r0.C0w(r1)     // Catch:{ all -> 0x01ad }
            java.lang.ref.SoftReference r2 = new java.lang.ref.SoftReference     // Catch:{ all -> 0x01ad }
            r2.<init>(r5)     // Catch:{ all -> 0x01ad }
            android.util.SparseArray r1 = r7.A00     // Catch:{ all -> 0x01ad }
            monitor-enter(r1)     // Catch:{ all -> 0x01ad }
            android.util.SparseArray r0 = r7.A00     // Catch:{ all -> 0x01aa }
            r0.put(r4, r2)     // Catch:{ all -> 0x01aa }
            monitor-exit(r1)     // Catch:{ all -> 0x01aa }
            r3.close()     // Catch:{ IOException -> 0x01b4 }
            goto L_0x0158
        L_0x01aa:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01aa }
            throw r0     // Catch:{ all -> 0x01ad }
        L_0x01ad:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x01af }
        L_0x01af:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x01b3 }
        L_0x01b3:
            throw r0     // Catch:{ IOException -> 0x01b4 }
        L_0x01b4:
            r2 = move-exception
            java.lang.Class<X.6Q7> r1 = X.AnonymousClass6Q7.class
            java.lang.String r0 = "Unable to create bitmap"
            X.C010708t.A08(r1, r0, r2)
            goto L_0x0159
        L_0x01bd:
            android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable
            r4.<init>(r6, r11)
            r0 = r24
            float r3 = (float) r0
            r0 = 1066758308(0x3f9570a4, float:1.1675)
            float r3 = r3 * r0
            r1 = 1099431936(0x41880000, float:17.0)
            float r1 = r1 * r3
            r0 = 1098907648(0x41800000, float:16.0)
            float r1 = r1 / r0
            r0 = 1056964608(0x3f000000, float:0.5)
            float r1 = r1 + r0
            int r2 = (int) r1
            float r3 = r3 + r0
            int r1 = (int) r3
            r0 = 0
            r4.setBounds(r0, r0, r2, r1)
            X.2jh r1 = new X.2jh
            r1.<init>(r4, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35531rN.A06(java.lang.CharSequence, int, int, int):java.lang.Object");
    }

    public String A07(String str) {
        return ((AnonymousClass1L1) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AjT, ((AnonymousClass1LL) this).A00)).A01(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r1 == false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(android.text.Spannable r8, int r9, int r10) {
        /*
            r7 = this;
            r2 = r7
            X.1LL r2 = (X.AnonymousClass1LL) r2
            java.lang.Boolean r0 = r2.A02
            if (r0 != 0) goto L_0x001f
            X.0Tq r0 = r2.A04
            if (r0 == 0) goto L_0x0018
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r1 = r0.booleanValue()
            r0 = 1
            if (r1 != 0) goto L_0x0019
        L_0x0018:
            r0 = 0
        L_0x0019:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r2.A02 = r0
        L_0x001f:
            java.lang.Boolean r0 = r2.A02
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0053
            int r1 = r8.length()
            java.lang.Class<X.5py> r0 = X.C122475py.class
            r6 = 0
            java.lang.Object[] r5 = r8.getSpans(r6, r1, r0)
            X.5py[] r5 = (X.C122475py[]) r5
            if (r5 == 0) goto L_0x0053
            int r4 = r5.length
            if (r4 <= 0) goto L_0x0053
        L_0x0039:
            if (r6 >= r4) goto L_0x005d
            r3 = r5[r6]
        L_0x003d:
            java.util.concurrent.atomic.AtomicInteger r0 = r3.A00
            int r2 = r0.get()
            r0 = 2
            if (r2 >= r0) goto L_0x0050
            java.util.concurrent.atomic.AtomicInteger r1 = r3.A00
            int r0 = r2 + 1
            boolean r0 = r1.compareAndSet(r2, r0)
            if (r0 == 0) goto L_0x003d
        L_0x0050:
            int r6 = r6 + 1
            goto L_0x0039
        L_0x0053:
            X.5py r1 = new X.5py
            r1.<init>()
            r0 = 33
            r8.setSpan(r1, r9, r10, r0)
        L_0x005d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35531rN.A08(android.text.Spannable, int, int):void");
    }

    public void A09(String str, Object... objArr) {
        C010708t.A0D(AnonymousClass1LL.class, str, objArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r1 == false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0A() {
        /*
            r3 = this;
            r2 = r3
            X.1LL r2 = (X.AnonymousClass1LL) r2
            java.lang.Boolean r0 = r2.A01
            if (r0 != 0) goto L_0x001f
            X.0Tq r0 = r2.A03
            if (r0 == 0) goto L_0x0018
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r1 = r0.booleanValue()
            r0 = 1
            if (r1 != 0) goto L_0x0019
        L_0x0018:
            r0 = 0
        L_0x0019:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r2.A01 = r0
        L_0x001f:
            java.lang.Boolean r0 = r2.A01
            boolean r0 = r0.booleanValue()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35531rN.A0A():boolean");
    }

    public boolean A0B() {
        return false;
    }

    public static boolean A03(CharSequence charSequence, int i, int i2) {
        while (i < i2) {
            int codePointAt = Character.codePointAt(charSequence, i);
            boolean z = false;
            if (codePointAt >= 169 && (codePointAt >= 8252 ? !(codePointAt >= 126980 ? codePointAt >= 983040 ? codePointAt > 983042 : codePointAt > 129791 : codePointAt > 12953) : codePointAt == 169 || codePointAt == 174)) {
                z = true;
            }
            if (z) {
                return true;
            }
            i += Character.charCount(codePointAt);
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002a, code lost:
        if (A02(r4, r5, r6, r7, r8, r9) == false) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0C(android.text.Spannable r17, int r18, int r19, int r20, boolean r21) {
        /*
            r16 = this;
            r5 = r17
            int r6 = r5.length()
            r0 = -1
            r1 = r20
            r7 = r19
            int r8 = r20 + r19
            if (r1 != r0) goto L_0x0010
            r8 = r6
        L_0x0010:
            r1 = r16
            boolean r0 = r1.A0A()
            if (r0 == 0) goto L_0x001b
            r1.A08(r5, r7, r8)
        L_0x001b:
            boolean r0 = A03(r5, r7, r8)
            r3 = 1
            r9 = r18
            if (r0 == 0) goto L_0x002c
            r4 = r1
            boolean r0 = A02(r4, r5, r6, r7, r8, r9)
            r2 = 1
            if (r0 != 0) goto L_0x002d
        L_0x002c:
            r2 = 0
        L_0x002d:
            if (r21 == 0) goto L_0x003f
            r15 = 0
            r10 = r1
            r12 = 0
            r11 = r5
            r13 = r8
            r14 = r9
            android.text.Spannable r1 = A01(r10, r11, r12, r13, r14, r15)
            r0 = 0
            if (r1 == 0) goto L_0x003d
            r0 = 1
        L_0x003d:
            if (r0 != 0) goto L_0x0042
        L_0x003f:
            if (r2 != 0) goto L_0x0042
            r3 = 0
        L_0x0042:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35531rN.A0C(android.text.Spannable, int, int, int, boolean):boolean");
    }

    public static boolean A02(C35531rN r5, Spannable spannable, int i, int i2, int i3, int i4) {
        int A01;
        Object A04;
        boolean A0B = r5.A0B();
        boolean z = false;
        while (i2 < i3) {
            if (!A0B) {
                A01 = C21951Ji.A00.A01(spannable, i2, i3);
                if (A01 >= 0) {
                    while (A01 < i3) {
                        int A012 = C21951Ji.A00.A01(spannable, A01, i3);
                        if (A012 < 0) {
                            break;
                        }
                        A01 = A012;
                    }
                } else {
                    A01 = -1;
                }
            } else {
                A01 = C21951Ji.A00.A01(spannable, i2, i3);
            }
            if (A01 == -1) {
                i2++;
            } else {
                if (A0B) {
                    A04 = r5.A06(spannable, i2, i3, i4);
                } else {
                    A04 = r5.A04(i4);
                }
                if (A04 != null && A01 <= i) {
                    spannable.setSpan(A04, i2, A01, 33);
                    z = true;
                }
                i2 = A01;
            }
        }
        return z;
    }
}
