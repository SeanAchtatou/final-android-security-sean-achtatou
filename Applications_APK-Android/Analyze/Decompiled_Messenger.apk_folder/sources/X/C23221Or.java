package X;

/* renamed from: X.1Or  reason: invalid class name and case insensitive filesystem */
public final class C23221Or implements C30781id {
    public final String A00;
    private final C23121Oh A01;
    private final C23051Oa A02;
    private final C23211Oq A03;
    private final C23131Oi A04;
    private volatile int A05 = 1;

    public C30152EqW[] AiU() {
        return null;
    }

    public C30152EqW[] Auv() {
        return null;
    }

    public int Ax8(String str) {
        return -1;
    }

    public int Acu() {
        int i = this.A05;
        this.A05 = Math.min(i * 10, 1000000);
        return i;
    }

    public String Ayb() {
        String str;
        C23131Oi r0 = this.A04;
        if (r0 == null || (str = r0.A05) == null) {
            return "error";
        }
        return str;
    }

    public C23121Oh B00() {
        return this.A01;
    }

    public long B3V() {
        C23131Oi r0 = this.A04;
        if (r0 != null) {
            return r0.A00;
        }
        return -1;
    }

    public String B8L() {
        String str;
        C23131Oi r1 = this.A04;
        if (r1 == null || (str = r1.A03) == null) {
            return "error";
        }
        Integer num = r1.A02;
        if (num == null) {
            return str;
        }
        return str + ":" + num;
    }

    public void C2s(String str, String str2) {
        this.A02.C08(this, AnonymousClass08S.A0S("PARAM ACCESS ERROR - ", str2, ": ", str), Acu());
    }

    public C23211Oq C3k(AnonymousClass1P6 r4) {
        this.A02.C08(this, this.A00, Acu());
        return this.A03;
    }

    public String getName() {
        String str;
        C23131Oi r0 = this.A04;
        if (r0 == null || (str = r0.A04) == null) {
            return "error";
        }
        return str;
    }

    public C23221Or(C23121Oh r3, C23131Oi r4, String str, C23051Oa r6) {
        this.A01 = r3;
        this.A04 = r4;
        this.A00 = str;
        this.A02 = r6;
        this.A03 = new C23211Oq(this, null);
    }
}
