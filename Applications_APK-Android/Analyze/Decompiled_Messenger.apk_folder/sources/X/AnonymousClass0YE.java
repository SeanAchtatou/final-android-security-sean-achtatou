package X;

import android.app.Application;

/* renamed from: X.0YE  reason: invalid class name */
public final class AnonymousClass0YE implements C04310Tq {
    public final /* synthetic */ C28671fB A00;

    public AnonymousClass0YE(C28671fB r1) {
        this.A00 = r1;
    }

    public Object get() {
        return new AHu((Application) C28671fB.A00().A09.get(), (AI0) this.A00.A0C.get());
    }
}
