package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.user.model.UserKey;

/* renamed from: X.115  reason: invalid class name */
public final class AnonymousClass115 {
    public final long A00;
    public final ThreadKey A01;
    public final ThreadParticipant A02;
    public final UserKey A03;
    public final UserKey A04;
    public final Integer A05;
    public final Integer A06;

    public AnonymousClass115(ThreadKey threadKey, ThreadParticipant threadParticipant, Integer num) {
        this.A01 = threadKey;
        this.A02 = threadParticipant;
        this.A06 = num;
        this.A04 = null;
        this.A00 = 0;
        this.A03 = null;
        this.A05 = null;
    }

    public AnonymousClass115(ThreadKey threadKey, UserKey userKey, long j, UserKey userKey2, Integer num) {
        this.A01 = threadKey;
        this.A04 = userKey;
        this.A06 = AnonymousClass07B.A0C;
        this.A02 = null;
        this.A00 = j;
        this.A03 = userKey2;
        this.A05 = num;
    }
}
