package X;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

/* renamed from: X.0eF  reason: invalid class name and case insensitive filesystem */
public final class C07840eF {
    public static final Looper A00;
    public static final Looper A01;
    public static final Looper A02;
    public static volatile C27471dF A03;

    static {
        HandlerThread handlerThread = new HandlerThread("SynchronizedData_ReceiveThread", -4);
        handlerThread.start();
        A02 = handlerThread.getLooper();
        HandlerThread handlerThread2 = new HandlerThread("DataNavigation_CleanupThread", 10);
        handlerThread2.start();
        A00 = handlerThread2.getLooper();
        HandlerThread handlerThread3 = new HandlerThread("DataFetch_LoggingThread", 10);
        handlerThread3.start();
        A01 = handlerThread3.getLooper();
    }

    public static C31571ju A00() {
        return A01(new C71843dA(A02));
    }

    public static C31571ju A01(C31571ju r3) {
        if (A03 == null || (r3 instanceof C27441dC)) {
            return r3;
        }
        if (r3 instanceof Handler) {
            return new C31561jt((Handler) r3, "DataFetch_", 2);
        }
        return new C29041EIe(r3, "DataFetch_");
    }
}
