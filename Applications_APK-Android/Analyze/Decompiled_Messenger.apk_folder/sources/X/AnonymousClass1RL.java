package X;

import android.util.Pair;
import java.util.Iterator;

/* renamed from: X.1RL  reason: invalid class name */
public final class AnonymousClass1RL extends AnonymousClass1QU {
    public final /* synthetic */ AnonymousClass1R4 A00;

    public AnonymousClass1RL(AnonymousClass1R4 r1) {
        this.A00 = r1;
    }

    public void A05(float f) {
        try {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("MultiplexProducer#onProgressUpdate");
            }
            AnonymousClass1R4 r1 = this.A00;
            synchronized (r1) {
                try {
                    if (r1.A03 == this) {
                        r1.A00 = f;
                        Iterator it = r1.A06.iterator();
                        while (it.hasNext()) {
                            Pair pair = (Pair) it.next();
                            synchronized (pair) {
                                try {
                                    ((C23581Qb) pair.first).BkJ(f);
                                } catch (Throwable th) {
                                    th = th;
                                    throw th;
                                }
                            }
                        }
                    }
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                    }
                    throw th;
                }
            }
        } finally {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
        }
    }
}
