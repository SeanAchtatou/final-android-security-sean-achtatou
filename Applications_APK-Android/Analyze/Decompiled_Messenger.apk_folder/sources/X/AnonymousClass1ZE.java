package X;

/* renamed from: X.1ZE  reason: invalid class name */
public final class AnonymousClass1ZE {
    private static final C06440bV A01 = new C06440bV();
    private final C06230bA A00;

    /* access modifiers changed from: private */
    /* renamed from: A00 */
    public C06450bW A02(String str, C04970Xc r6) {
        boolean z = false;
        if (r6.A00 == AnonymousClass07B.A01) {
            z = true;
        }
        AnonymousClass0ZF A05 = this.A00.A05(str, r6.A01, AnonymousClass07B.A00, z);
        if (A05.A0G()) {
            return new C06450bW(A05);
        }
        return A01;
    }

    public C06450bW A01(String str) {
        return A02(str, C04970Xc.A03);
    }

    public AnonymousClass1ZE(C06230bA r1) {
        this.A00 = r1;
    }
}
