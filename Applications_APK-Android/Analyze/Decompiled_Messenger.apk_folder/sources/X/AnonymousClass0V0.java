package X;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0V0  reason: invalid class name */
public interface AnonymousClass0V0 {
    Map AOp();

    boolean AUB(Object obj, Object obj2);

    Collection AYj();

    Collection AbK(Object obj);

    AnonymousClass0UG BHh();

    boolean Byx(Object obj, Object obj2);

    boolean Byz(Object obj, Iterable iterable);

    Collection C1N(Object obj);

    Collection C2O(Object obj, Iterable iterable);

    void clear();

    boolean containsKey(Object obj);

    boolean containsValue(Object obj);

    boolean equals(Object obj);

    boolean isEmpty();

    Set keySet();

    boolean remove(Object obj, Object obj2);

    int size();

    Collection values();
}
