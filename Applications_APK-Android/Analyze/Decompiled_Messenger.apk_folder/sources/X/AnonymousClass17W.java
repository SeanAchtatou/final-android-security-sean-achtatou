package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.17W  reason: invalid class name */
public final class AnonymousClass17W implements C20021Ap {
    public final /* synthetic */ AnonymousClass16R A00;

    public AnonymousClass17W(AnonymousClass16R r1) {
        this.A00 = r1;
    }

    public void Bd1(Object obj, Object obj2) {
        AnonymousClass16R.A00(this.A00);
    }

    public void BdJ(Object obj, Object obj2) {
        this.A00.A0N = true;
    }

    public void Bgh(Object obj, Object obj2) {
        Integer num;
        AnonymousClass16R r2 = this.A00;
        r2.A0B = (AnonymousClass101) obj2;
        if (((AnonymousClass1G5) obj).A00 == AnonymousClass07B.A0C) {
            num = AnonymousClass07B.A0C;
        } else {
            num = AnonymousClass07B.A0n;
        }
        AnonymousClass16R.A04(r2, num, "Inbox2");
        if (!this.A00.A0a) {
            ((C08770fv) AnonymousClass1XX.A02(18, AnonymousClass1Y3.B9x, this.A00.A08)).A0F("inbox_data_loaded");
            AnonymousClass16R.A00(this.A00);
        }
    }

    public void BdU(Object obj, ListenableFuture listenableFuture) {
    }
}
