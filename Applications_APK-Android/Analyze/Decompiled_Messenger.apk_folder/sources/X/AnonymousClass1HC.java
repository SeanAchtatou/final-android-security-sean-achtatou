package X;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.1HC  reason: invalid class name */
public final class AnonymousClass1HC {
    public final ReentrantReadWriteLock A00 = new ReentrantReadWriteLock();
    private final AnonymousClass1HD A01 = new AnonymousClass1HD(this);
    private final AnonymousClass1HS A02 = new AnonymousClass1HS(this);

    public static final AnonymousClass1HC A00() {
        return new AnonymousClass1HC();
    }

    public AnonymousClass1HE A01() {
        this.A00.readLock().lock();
        return this.A01;
    }

    public AnonymousClass1HE A02() {
        this.A00.writeLock().lock();
        return this.A02;
    }
}
