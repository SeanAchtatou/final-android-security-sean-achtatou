package X;

import android.app.Application;
import android.content.Context;
import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ir  reason: invalid class name and case insensitive filesystem */
public final class C09890ir extends AnonymousClass0Wl implements AnonymousClass0ZM {
    private static volatile C09890ir A02;
    private AnonymousClass0UN A00;
    private boolean A01;

    public String getSimpleName() {
        return "WhitehatOverlayInitializer";
    }

    public static final C09890ir A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C09890ir.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C09890ir(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private void A01() {
        boolean z;
        if (!this.A01 && ((C09940iw) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BUl, this.A00)).A01()) {
            AnonymousClass8Yf r2 = (AnonymousClass8Yf) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ara, this.A00);
            Context context = r2.A00;
            if (!(context instanceof Application)) {
                z = false;
            } else {
                ((Application) context).registerActivityLifecycleCallbacks(r2);
                z = true;
            }
            this.A01 = z;
        }
    }

    private C09890ir(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }

    public void init() {
        int A03 = C000700l.A03(-1579877918);
        A01();
        ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A00)).C0f(C25951af.A0A, this);
        C000700l.A09(1212570670, A03);
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r6) {
        A01();
        if (this.A01) {
            int i = AnonymousClass1Y3.Ara;
            AnonymousClass0UN r3 = this.A00;
            ((AnonymousClass8Yf) AnonymousClass1XX.A02(3, i, r3)).A01(((AnonymousClass0Ud) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B5j, r3)).A0B());
        }
    }
}
