package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1GS  reason: invalid class name */
public final class AnonymousClass1GS {
    private static C05540Zi A01;
    public volatile C13890sF A00;

    public static final AnonymousClass1GS A00(AnonymousClass1XY r3) {
        AnonymousClass1GS r0;
        synchronized (AnonymousClass1GS.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r3)) {
                    A01.A01();
                    A01.A00 = new AnonymousClass1GS();
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass1GS) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }
}
