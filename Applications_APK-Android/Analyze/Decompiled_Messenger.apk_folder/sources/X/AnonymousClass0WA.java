package X;

import android.content.Context;
import com.facebook.gk.store.GatekeeperWriter;
import com.facebook.inject.InjectorModule;
import java.io.File;

@InjectorModule
/* renamed from: X.0WA  reason: invalid class name */
public final class AnonymousClass0WA extends AnonymousClass0UV {
    private static volatile AnonymousClass0WC A00;
    private static volatile AnonymousClass0WB A01;
    private static volatile AnonymousClass0WG A02;

    public static final AnonymousClass0WC A02(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass0WC.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass0WF();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final AnonymousClass0WB A03(AnonymousClass1XY r10) {
        if (A01 == null) {
            synchronized (AnonymousClass0WB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r10);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r10.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        AnonymousClass0WC A022 = A02(applicationInjector);
                        AnonymousClass0WG A05 = A05(applicationInjector);
                        boolean z = false;
                        if (A022 != null) {
                            z = true;
                        }
                        AnonymousClass064.A04(z);
                        File dir = A003.getDir(AnonymousClass24B.$const$string(22), 0);
                        A01 = new AnonymousClass0WB(A022, new AnonymousClass0WH(A022, dir), null, A05, new AnonymousClass0X3(A022, dir));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final AnonymousClass0WG A05(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass0WG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        r4.getApplicationInjector();
                        A02 = new AnonymousClass0WG("GatekeeperStore");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final AnonymousClass1YI A00(AnonymousClass1XY r0) {
        return A03(r0);
    }

    public static final AnonymousClass1YI A01(AnonymousClass1XY r0) {
        return A03(r0);
    }

    public static final GatekeeperWriter A04(AnonymousClass1XY r0) {
        return A03(r0);
    }
}
