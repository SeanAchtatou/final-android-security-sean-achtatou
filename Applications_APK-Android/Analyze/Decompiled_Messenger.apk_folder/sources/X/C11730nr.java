package X;

import com.fasterxml.jackson.databind.JsonSerializer;

/* renamed from: X.0nr  reason: invalid class name and case insensitive filesystem */
public interface C11730nr {
    JsonSerializer findArraySerializer(C10450k8 r1, BMA bma, C10120ja r3, CY1 cy1, JsonSerializer jsonSerializer);

    JsonSerializer findCollectionLikeSerializer(C10450k8 r1, AnonymousClass1CA r2, C10120ja r3, CY1 cy1, JsonSerializer jsonSerializer);

    JsonSerializer findCollectionSerializer(C10450k8 r1, C28331ed r2, C10120ja r3, CY1 cy1, JsonSerializer jsonSerializer);

    JsonSerializer findMapLikeSerializer(C10450k8 r1, C21991Jm r2, C10120ja r3, JsonSerializer jsonSerializer, CY1 cy1, JsonSerializer jsonSerializer2);

    JsonSerializer findMapSerializer(C10450k8 r1, C180610a r2, C10120ja r3, JsonSerializer jsonSerializer, CY1 cy1, JsonSerializer jsonSerializer2);

    JsonSerializer findSerializer(C10450k8 r1, C10030jR r2, C10120ja r3);
}
