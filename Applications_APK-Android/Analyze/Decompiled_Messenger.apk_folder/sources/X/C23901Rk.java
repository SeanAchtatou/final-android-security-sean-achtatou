package X;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

/* renamed from: X.1Rk  reason: invalid class name and case insensitive filesystem */
public final class C23901Rk {
    public static C23901Rk A08 = new C23901Rk((Object) false);
    public static C23901Rk A09 = new C23901Rk((Object) null);
    public static C23901Rk A0A = new C23901Rk((Object) true);
    private static final Executor A0B = C23911Rl.A03.A00;
    public AnonymousClass9N2 A00;
    public Exception A01;
    public Object A02;
    public List A03 = new ArrayList();
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public final Object A07 = new Object();

    public static C23901Rk A00(Callable callable, Executor executor) {
        C23951Rp r2 = new C23951Rp();
        try {
            AnonymousClass07A.A04(executor, new C23991Rt(null, r2, callable), -1925434631);
        } catch (Exception e) {
            r2.A01(new C99634pO(e));
        }
        return r2.A00;
    }

    static {
        new C23901Rk(true);
    }

    public static void A01(C23901Rk r3) {
        synchronized (r3.A07) {
            for (C23981Rs CIx : r3.A03) {
                try {
                    CIx.CIx(r3);
                } catch (RuntimeException e) {
                    throw e;
                } catch (Exception e2) {
                    throw new RuntimeException(e2);
                }
            }
            r3.A03 = null;
        }
    }

    public Exception A02() {
        Exception exc;
        synchronized (this.A07) {
            exc = this.A01;
            if (exc != null) {
                this.A06 = true;
                AnonymousClass9N2 r1 = this.A00;
                if (r1 != null) {
                    r1.A00 = null;
                    this.A00 = null;
                }
            }
        }
        return exc;
    }

    public void A03(C23981Rs r8) {
        boolean z;
        Executor executor = A0B;
        C23951Rp r4 = new C23951Rp();
        synchronized (this.A07) {
            synchronized (this.A07) {
                z = this.A05;
            }
            if (!z) {
                this.A03.add(new C33201nC(r4, r8, executor, null));
            }
        }
        if (z) {
            try {
                AnonymousClass07A.A04(executor, new C33221nE(null, r4, r8, this), 1766719051);
            } catch (Exception e) {
                r4.A01(new C99634pO(e));
            }
        }
    }

    public boolean A04() {
        boolean z;
        synchronized (this.A07) {
            z = false;
            if (A02() != null) {
                z = true;
            }
        }
        return z;
    }

    public boolean A05() {
        synchronized (this.A07) {
            if (this.A05) {
                return false;
            }
            this.A05 = true;
            this.A04 = true;
            this.A07.notifyAll();
            A01(this);
            return true;
        }
    }

    public boolean A06(Object obj) {
        synchronized (this.A07) {
            if (this.A05) {
                return false;
            }
            this.A05 = true;
            this.A02 = obj;
            this.A07.notifyAll();
            A01(this);
            return true;
        }
    }

    public C23901Rk() {
    }

    private C23901Rk(Object obj) {
        A06(obj);
    }

    private C23901Rk(boolean z) {
        if (z) {
            A05();
        } else {
            A06(null);
        }
    }
}
