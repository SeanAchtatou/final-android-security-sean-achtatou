package X;

import android.content.Context;
import com.facebook.acra.ACRA;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1IZ  reason: invalid class name */
public final class AnonymousClass1IZ extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public InboxUnitThreadItem A01;
    @Comparable(type = 13)
    public AnonymousClass1BN A02;
    @Comparable(type = 13)
    public AnonymousClass1BO A03;
    @Comparable(type = 13)
    public AnonymousClass1BG A04;
    @Comparable(type = 13)
    public AnonymousClass1BE A05;
    @Comparable(type = 13)
    public AnonymousClass1BK A06;
    @Comparable(type = 13)
    public AnonymousClass1BY A07;
    @Comparable(type = 13)
    public MigColorScheme A08;

    public AnonymousClass1IZ(Context context) {
        super("M4ThreadItemComponent");
        this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }

    public static ImmutableList A00(AnonymousClass0p4 r6, MigColorScheme migColorScheme, ImmutableList immutableList) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            AnonymousClass1NF r0 = (AnonymousClass1NF) it.next();
            builder.add((Object) AnonymousClass1NJ.A00(r6, migColorScheme, r0.A00, C17780zS.A0E(AnonymousClass1IZ.class, r6, -1351902487, new Object[]{r6, r0})));
        }
        return builder.build();
    }

    public static ImmutableList A01(InboxUnitThreadItem inboxUnitThreadItem, AnonymousClass1BE r8, C21361Gm[] r9, AnonymousClass1BY r10) {
        AnonymousClass1MP r2;
        ImmutableList.Builder builder = new ImmutableList.Builder();
        if (r9 != null) {
            for (C21361Gm r6 : r9) {
                switch (r6.ordinal()) {
                    case 0:
                        r2 = new AnonymousClass1MP(new AnonymousClass1NG(r8), inboxUnitThreadItem, C21361Gm.MORE, r10);
                        break;
                    case 1:
                        r2 = new AnonymousClass1MP(new C22471Lm(r8), inboxUnitThreadItem, C21361Gm.NOTIFICATION_ON, r10);
                        break;
                    case 2:
                        r2 = new AnonymousClass1MP(new C37621vy(r8), inboxUnitThreadItem, C21361Gm.NOTIFICATION_OFF, r10);
                        break;
                    case 3:
                        r2 = new AnonymousClass1MP(new AnonymousClass1NI(r8), inboxUnitThreadItem, C21361Gm.DELETE, r10);
                        break;
                    case 4:
                        r2 = new AnonymousClass1MP(new AnonymousClass1Lx(r8), inboxUnitThreadItem, C21361Gm.CAMERA, r10);
                        break;
                    case 5:
                        r2 = new AnonymousClass1MP(new AnonymousClass1Kl(r8), inboxUnitThreadItem, C21361Gm.AUDIO_CALL, r10);
                        break;
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                        r2 = new AnonymousClass1MP(new C33481ne(r8), inboxUnitThreadItem, C21361Gm.VIDEO_CALL, r10);
                        break;
                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                        r2 = new AnonymousClass1MP(new C111525Sv(r8), inboxUnitThreadItem, C21361Gm.PIN_THREAD, r10);
                        break;
                    case 8:
                        r2 = new AnonymousClass1MP(new C111515Su(r8), inboxUnitThreadItem, C21361Gm.UNPIN_THREAD, r10);
                        break;
                    default:
                        throw new RuntimeException("Unknown swipe action: " + r6);
                }
                builder.add((Object) r2);
            }
        }
        return builder.build();
    }
}
