package X;

import java.util.ArrayList;

/* renamed from: X.0Wk  reason: invalid class name and case insensitive filesystem */
public final class C04850Wk implements AnonymousClass0WC {
    public ArrayList Anu() {
        ArrayList arrayList = new ArrayList((int) AnonymousClass1Y3.A15);
        arrayList.add("android_analytics_force_new_config_on_user_switch");
        arrayList.add("android_bootstrap_tier_kill_switch");
        arrayList.add("android_dbl_local_auth");
        arrayList.add("android_first_party_provider");
        arrayList.add("android_logout_push_session_async_sessionless");
        arrayList.add("android_messenger_downloadable_languages_killswitc");
        arrayList.add("android_messenger_sms_retriever");
        arrayList.add("android_messenger_unified_login_kill_switch");
        arrayList.add("android_new_2fac_authentication_2018_h2_killswitch");
        arrayList.add("android_password_encryption_common_lib_disabled");
        arrayList.add("android_sso_provider_use_trustedapp_api");
        arrayList.add("attack_discovery_fb4a_sprinkle_login_killswitch");
        arrayList.add("fb4a_account_removal_confirmation_dialog_gk");
        arrayList.add("fb4a_account_removal_dialog_for_new_user_gk");
        arrayList.add("fb4a_account_search_numeric_keyboard_killswitch");
        arrayList.add("fb4a_account_search_numeric_keyboard_launchswitch");
        arrayList.add("fb4a_ar_account_search_clear_icon_bug_fix");
        arrayList.add("fb4a_ar_bg_sms_keep_notification_for_testing");
        arrayList.add("fb4a_ar_color_migration_killswitch");
        arrayList.add("fb4a_ar_color_migration_launchswitch");
        arrayList.add("fb4a_ar_cuid_expired_for_testing");
        arrayList.add("fb4a_ar_done_ime_on_password_reset");
        arrayList.add("fb4a_ar_permission_toast_killswitch");
        arrayList.add("fb4a_ar_permission_toast_launchswitch");
        arrayList.add("fb4a_ar_show_placeholder_profile_pic");
        arrayList.add("fb4a_ar_sms_retriever_v1");
        arrayList.add("fb4a_ar_sms_retriever_v9_killswitch");
        arrayList.add("fb4a_as_allow_one_tap_login");
        arrayList.add("fb4a_as_allow_password_accounts");
        arrayList.add("fb4a_as_badging_holdout");
        arrayList.add("fb4a_as_menu_option_fix");
        arrayList.add("fb4a_bd_locale_defaulting_killswitch");
        arrayList.add("fb4a_before_logout_parallel_and_combine");
        arrayList.add("fb4a_boucing_cliff_optimization");
        arrayList.add("fb4a_bypass_confirmation_oe_killswitch");
        arrayList.add("fb4a_cp_reminders_killswitch");
        arrayList.add("fb4a_dbl_based_sso");
        arrayList.add("fb4a_dbl_nux_fetch_fix_launch_gk");
        arrayList.add("fb4a_dbl_nux_password_account_fix");
        arrayList.add("fb4a_dbl_popup_menu_do_not_disturb_lc");
        arrayList.add("fb4a_dbl_preload_killswitch");
        arrayList.add("fb4a_dbl_white_as_v7_killswitch");
        arrayList.add("fb4a_dbl_white_as_v7_launchswitch");
        arrayList.add("fb4a_device_owner_data_fetcher_fix");
        arrayList.add("fb4a_fetch_locales_early_gk");
        arrayList.add("fb4a_flex_show_profile_pic_on_logout_killswitch");
        arrayList.add("fb4a_initial_app_launch_killswitch");
        arrayList.add("fb4a_instagram_sso_killswitch");
        arrayList.add("fb4a_locale_defaulting_killswitch");
        arrayList.add("fb4a_locale_switcher_bottom_ial_v2_killswitch");
        arrayList.add("fb4a_locale_variants_killswitch");
        arrayList.add("fb4a_login_first_party_sso_ial_killswitch");
        arrayList.add("fb4a_login_openid_identify_and_auth_holdout");
        arrayList.add("fb4a_login_save_password_dialog_killswitch");
        arrayList.add("fb4a_login_send_device_emails_to_server_killswitch");
        arrayList.add("fb4a_login_send_sim_phone_info");
        arrayList.add("fb4a_login_send_sso_userids_to_server_killswitch");
        arrayList.add("fb4a_login_sms_retriever_on_cpl_killswitch");
        arrayList.add("fb4a_login_string_es_la_killswitch");
        arrayList.add("fb4a_login_switch_oauth_smart_lock_killswitch");
        arrayList.add("fb4a_login_switch_oauth_smart_lock_launchswitch");
        arrayList.add("fb4a_login_userid_error_to_ar_killswitch");
        arrayList.add("fb4a_login_userid_error_to_rec_killswitch");
        arrayList.add("fb4a_login_userid_error_to_reg_killswitch");
        arrayList.add("fb4a_logout_parallel_user_data_cleaners");
        arrayList.add("fb4a_logout_shard_prefs_sessionless_v170");
        arrayList.add("fb4a_logout_white_killswitch");
        arrayList.add("fb4a_logout_white_launchswitch");
        arrayList.add("fb4a_mobile_config_fetch_timeout_killswitch");
        arrayList.add("fb4a_mobile_config_fetch_timeout_launch");
        arrayList.add("fb4a_mx_locale_defaulting_killswitch");
        arrayList.add("fb4a_oe_example_launchswitch");
        arrayList.add("fb4a_openid_token_future_fix_v2");
        arrayList.add("fb4a_openid_token_future_fix_v3");
        arrayList.add("fb4a_post_dbl_login_perf_test_killswitch");
        arrayList.add("fb4a_post_dbl_login_perf_test_launch");
        arrayList.add("fb4a_post_login_perf_test_killswitch");
        arrayList.add("fb4a_post_login_perf_test_launch");
        arrayList.add("fb4a_preserve_reg_form_data_killswitch");
        arrayList.add("fb4a_pymb_client_rate_limit");
        arrayList.add("fb4a_pymb_login_source_logging");
        arrayList.add("fb4a_read_first_party_sso_credentials_killswitch");
        arrayList.add("fb4a_reg_birthday_step_killswitch");
        arrayList.add("fb4a_reg_header_prefill_killswitch");
        arrayList.add("fb4a_reg_proactive_audio_killswitch");
        arrayList.add("fb4a_reg_question_titles_gk");
        arrayList.add("fb4a_reg_text_contexts_killswitch");
        arrayList.add("fb4a_save_login_info_on_logout_local_auth_sync");
        arrayList.add("fb4a_save_login_info_on_logout_set_nonce_async");
        arrayList.add("fb4a_save_login_info_on_logout_set_nonce_sync");
        arrayList.add("fb4a_skip_auth_header_v2_killswitch");
        arrayList.add("fb4a_sms_retriever_v9_launch");
        arrayList.add("fb4a_sms_retriever_v9_launch_keep_screen_on");
        arrayList.add("fb4a_sms_retriever_v9_launch_on_demand");
        arrayList.add("fb4a_two_step_login_lid_v7_killswitch");
        arrayList.add("fb4a_two_step_login_universe_v2_killswitch");
        arrayList.add("foreground_service_tracker");
        arrayList.add("gk_cpuboost_killswitch_2019h2");
        arrayList.add("gk_mobileboost_killswitch_2019h2");
        arrayList.add("gk_threadaffinity_killswitch_2019h2");
        arrayList.add("jio_login_prefill_oe_kill_switch");
        arrayList.add("killswitch_header_config_ping_messenger");
        arrayList.add("m4a_account_switcher_native_sso_killswitch");
        arrayList.add("m4a_account_switcher_native_sso_launchswitch");
        arrayList.add("m4a_hash_sessionless_language_support");
        arrayList.add("m4a_romanian_sessionless_language_support");
        arrayList.add("me_to_fb4a_sso_save_extra_info_killswitch");
        arrayList.add("messenger_android_native_sso_launch");
        arrayList.add("messenger_msite_reg_killswitch");
        arrayList.add("messenger_rtc_snapshots");
        arrayList.add("mk_android_auth_device_content_test_killswitch");
        arrayList.add("mk_android_login_password_fallback_killswitch");
        arrayList.add("mk_android_parent_login_fallback_killswitch");
        arrayList.add("mk_fallback_on_account_not_found_killswitch");
        arrayList.add("mk_landing_page_test_killswitch");
        arrayList.add("mk_parent_login_password_coin_flip_killswitch");
        arrayList.add("mkandroid_prelogin_onboarding_flow_test_killswitch");
        arrayList.add("mobile_config_emergency_push_omnistore_rollout");
        arrayList.add("mobile_config_omnistore_log_delta_size");
        arrayList.add("mobileconfig_canary_client");
        arrayList.add("mobileconfig_enable_virtual_gk_client");
        arrayList.add("mobileconfig_java_ep_handler");
        arrayList.add("mobileconfig_omnistore_sync_on_queue_request");
        arrayList.add("mobileconfig_omnistore_v2");
        arrayList.add("mobileconfig_partial_fetch_killswitch");
        arrayList.add("mobileconfig_use_ota_resource_android");
        arrayList.add("msgr_as_extended_sso_login");
        arrayList.add("msgr_login_blue_on_disabled_gk");
        arrayList.add("msgr_login_first_party_sso_ial_killswitch");
        arrayList.add("msgr_login_notif_killswitch");
        arrayList.add("msgr_login_pwd_error_ar_redirect_killswitch");
        arrayList.add("msgr_login_save_pwd_dialog");
        arrayList.add("pre_reg_push_token_registration");
        arrayList.add("sessionless_android_mclassic_two_fac_content_updat");
        arrayList.add("sessionless_android_messenger_recovery_killswitch");
        arrayList.add("work_mobile_number_signup_killswitch");
        arrayList.add("workplace_split_start_screen_text_killswitch");
        arrayList.add("zero_block_hping_on_wifi");
        arrayList.add("zero_header_transparency_fb4a_killswitch");
        arrayList.add("zero_header_transparency_fb4a_strict_policy");
        arrayList.add("zero_sessionless_backup_rewrite_rules");
        return arrayList;
    }

    public String Anv() {
        return "80F4D9059F06ACB2A64CAF575C391B5EE16D5DBC";
    }

    public int AwA() {
        return AnonymousClass1Y3.A15;
    }
}
