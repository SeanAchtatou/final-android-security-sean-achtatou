package X;

import android.app.Activity;
import com.facebook.common.activitycleaner.ActivityStackManager;
import com.facebook.common.stringformat.StringFormatUtil;
import java.util.Iterator;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Yu  reason: invalid class name and case insensitive filesystem */
public final class C05400Yu implements AnonymousClass0Z1 {
    private static volatile C05400Yu A01;
    private AnonymousClass0US A00;

    public String Amj() {
        return "activity_stack";
    }

    public static final C05400Yu A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (C05400Yu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        A01 = new C05400Yu(AnonymousClass0VB.A00(AnonymousClass1Y3.A1C, r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public String getCustomData(Throwable th) {
        C13460rT activeContentFragment;
        ActivityStackManager activityStackManager = (ActivityStackManager) this.A00.get();
        StringBuilder sb = new StringBuilder();
        synchronized (activityStackManager.A04) {
            Iterator it = activityStackManager.A04.iterator();
            while (it.hasNext()) {
                C14690tp r1 = (C14690tp) it.next();
                Activity A002 = r1.A00();
                if (A002 != null) {
                    sb.append(A002.toString());
                    try {
                        if ((r1.A00() instanceof AnonymousClass14G) && (activeContentFragment = ((AnonymousClass14G) r1.A00()).getActiveContentFragment()) != null) {
                            sb.append(StringFormatUtil.formatStrLocaleSafe("[%s]", activeContentFragment.toString()));
                        }
                    } catch (Exception unused) {
                    }
                    sb.append(StringFormatUtil.formatStrLocaleSafe("%n"));
                }
            }
        }
        return sb.toString();
    }

    private C05400Yu(AnonymousClass0US r1) {
        this.A00 = r1;
    }
}
