package X;

/* renamed from: X.10a  reason: invalid class name and case insensitive filesystem */
public final class C180610a extends C21991Jm {
    private static final long serialVersionUID = -811146779148281500L;

    public static C180610a construct(Class cls, C10030jR r8, C10030jR r9) {
        return new C180610a(cls, r8, r9, null, null, false);
    }

    public C10030jR _narrow(Class cls) {
        return new C180610a(cls, this._keyType, this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    public C10030jR narrowContentsBy(Class cls) {
        C10030jR r1 = this._valueType;
        if (cls == r1._class) {
            return this;
        }
        return new C180610a(this._class, this._keyType, r1.narrowBy(cls), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public C10030jR narrowKey(Class cls) {
        C10030jR r1 = this._keyType;
        if (cls == r1._class) {
            return this;
        }
        return new C180610a(this._class, r1.narrowBy(cls), this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    public String toString() {
        return "[map type; class " + this._class.getName() + ", " + this._keyType + " -> " + this._valueType + "]";
    }

    public C10030jR widenContentsBy(Class cls) {
        C10030jR r1 = this._valueType;
        if (cls == r1._class) {
            return this;
        }
        return new C180610a(this._class, this._keyType, r1.widenBy(cls), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public /* bridge */ /* synthetic */ C21991Jm withKeyValueHandler(Object obj) {
        return new C180610a(this._class, this._keyType.withValueHandler(obj), this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    public C180610a(Class cls, C10030jR r2, C10030jR r3, Object obj, Object obj2, boolean z) {
        super(cls, r2, r3, obj, obj2, z);
    }

    /* access modifiers changed from: private */
    public C180610a withContentTypeHandler(Object obj) {
        return new C180610a(this._class, this._keyType, this._valueType.withTypeHandler(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* access modifiers changed from: private */
    public C180610a withContentValueHandler(Object obj) {
        return new C180610a(this._class, this._keyType, this._valueType.withValueHandler(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* access modifiers changed from: private */
    public C180610a withStaticTyping() {
        if (this._asStatic) {
            return this;
        }
        return new C180610a(this._class, this._keyType.withStaticTyping(), this._valueType.withStaticTyping(), this._valueHandler, this._typeHandler, true);
    }
}
