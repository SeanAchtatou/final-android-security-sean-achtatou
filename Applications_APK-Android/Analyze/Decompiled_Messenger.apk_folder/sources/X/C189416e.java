package X;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.16e  reason: invalid class name and case insensitive filesystem */
public final class C189416e {
    public static final Class A05 = C189416e.class;
    public static final String[] A06 = {"_id", "has_phone_number"};
    public static final String[] A07 = {"_id", "version"};
    public static final String[] A08 = {"_id"};
    private static final String[] A09 = {"_id", "contact_id", TurboLoader.Locator.$const$string(151), "data_version", "mimetype", "is_primary", AnonymousClass80H.$const$string(159), "data_version", "data1", "data2", "data3", "data4", "data5", "data6", "data7", "data8", "data9"};
    private static volatile C189416e A0A;
    public final ContentResolver A00;
    public final C189616g A01;
    public final C189516f A02;
    public final C189716h A03;
    public final C12460pP A04;

    public static Cursor A00(C189416e r5, Uri uri) {
        try {
            return r5.A00.query(uri, C207859qz.A01, null, null, "contact_id");
        } catch (Exception e) {
            C010708t.A0F(A05, e, "Got Exception in getCursorByEndpoint, closing open cursor.", new Object[0]);
            return null;
        }
    }

    public static Cursor A01(C189416e r5, List list) {
        C06140av A042 = C06160ax.A04("contact_id", list);
        return r5.A00.query(ContactsContract.RawContactsEntity.CONTENT_URI, A09, A042.A02(), A042.A04(), null);
    }

    public static final C189416e A02(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (C189416e.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new C189416e(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    private C189416e(AnonymousClass1XY r2) {
        this.A00 = C04490Ux.A0E(r2);
        this.A02 = new C189516f(r2);
        this.A01 = new C189616g(r2);
        this.A03 = new C189716h(r2);
        this.A04 = C12460pP.A01(r2);
    }

    /* JADX INFO: finally extract failed */
    public AnonymousClass7US A03(String str, int i) {
        ArrayList A002 = C04300To.A00();
        Uri build = ContactsContract.Contacts.CONTENT_FILTER_URI.buildUpon().appendPath(str).build();
        ContentResolver contentResolver = this.A00;
        String[] strArr = A08;
        Cursor query = contentResolver.query(build, strArr, null, null, null);
        while (query.moveToNext() && A002.size() < i) {
            try {
                A002.add(Integer.valueOf(query.getInt(0)));
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        query = this.A00.query(ContactsContract.PhoneLookup.CONTENT_FILTER_URI.buildUpon().appendPath(str).build(), strArr, null, null, null);
        while (query.moveToNext() && A002.size() < i) {
            A002.add(Integer.valueOf(query.getInt(0)));
        }
        query.close();
        Cursor A012 = A01(this, A002);
        C189516f r3 = this.A02;
        new C50642eM(r3);
        return new AnonymousClass7US(r3, A012);
    }
}
