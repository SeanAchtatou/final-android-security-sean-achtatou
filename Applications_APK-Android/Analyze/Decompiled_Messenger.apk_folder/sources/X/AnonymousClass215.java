package X;

import android.graphics.Rect;
import android.net.Uri;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import java.io.File;

/* renamed from: X.215  reason: invalid class name */
public final class AnonymousClass215 implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.media.editing.trimmer.VideoStripController$BitmapData";
    public long A00;
    public Rect A01;
    public Uri A02;
    public AnonymousClass306 A03;
    public AnonymousClass306 A04;
    public String A05;

    public AnonymousClass215(long j, int i, int i2, File file, C61832zZ r8) {
        this.A00 = j;
        this.A01 = new Rect(0, 0, i, i2);
        String A0O = AnonymousClass08S.A0O(file.getPath(), File.separator, j);
        this.A05 = A0O;
        this.A02 = Uri.fromFile(new File(A0O));
        r8.A0Q(CallerContext.A04(AnonymousClass215.class));
        r8.A0P(this.A02);
        AnonymousClass303 A0N = r8.A0F();
        this.A04 = A0N;
        this.A03 = A0N;
    }
}
