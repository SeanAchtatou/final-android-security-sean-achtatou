package X;

import android.content.res.Resources;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import java.util.Comparator;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1H6  reason: invalid class name */
public final class AnonymousClass1H6 implements C05460Za {
    private static volatile AnonymousClass1H6 A03;
    public AnonymousClass0UN A00;
    public UserKey A01 = null;
    private final Comparator A02 = new AnonymousClass1H7();

    public void clearUserData() {
        this.A01 = null;
    }

    public static final AnonymousClass1H6 A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass1H6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass1H6(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public ImmutableList A01(ThreadSummary threadSummary, boolean z) {
        return A02(((C17270yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A3x, this.A00)).A08(threadSummary), z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b9, code lost:
        if (r7 > 0) goto L_0x00bb;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A02(com.google.common.collect.ImmutableList r15, boolean r16) {
        /*
            r14 = this;
            int r1 = X.AnonymousClass1Y3.AgK
            X.0UN r0 = r14.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A03(r1, r0)
            X.06B r6 = (X.AnonymousClass06B) r6
            int r1 = X.AnonymousClass1Y3.Auh
            X.0UN r0 = r14.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r1, r0)
            X.0r6 r5 = (X.AnonymousClass0r6) r5
            boolean r0 = r5.A0Y()
            if (r0 == 0) goto L_0x0063
            com.facebook.user.model.UserKey r0 = r14.A01
            if (r0 != 0) goto L_0x002a
            int r1 = X.AnonymousClass1Y3.B7T
            X.0UN r0 = r14.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            r14.A01 = r0
        L_0x002a:
            com.facebook.user.model.UserKey r4 = r14.A01
            if (r4 == 0) goto L_0x0063
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            X.1Xv r13 = r15.iterator()
        L_0x0037:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x00e2
            java.lang.Object r2 = r13.next()
            com.facebook.user.model.UserKey r2 = (com.facebook.user.model.UserKey) r2
            boolean r0 = r4.equals(r2)
            if (r0 != 0) goto L_0x0037
            com.facebook.user.model.LastActive r0 = r5.A0M(r2)
            if (r0 == 0) goto L_0x0037
            long r11 = r6.now()
            long r0 = r0.A00
            long r11 = r11 - r0
            r0 = -1
            X.18m r0 = r5.A0L(r2, r0)
            java.lang.Integer r1 = r0.A07
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x0066
            if (r16 == 0) goto L_0x0037
        L_0x0063:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            return r0
        L_0x0066:
            r0 = 1000(0x3e8, double:4.94E-321)
            long r9 = r11 / r0
            r7 = 60
            int r0 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r0 < 0) goto L_0x00bb
            r7 = 2
            int r1 = X.AnonymousClass1Y3.BTP
            X.0UN r0 = r14.A00
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.1FZ r9 = (X.AnonymousClass1FZ) r9
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r9.A00
            r8 = 0
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.1Yd r7 = (X.C25051Yd) r7
            r0 = 282437049451920(0x100e000010590, double:1.39542443246959E-309)
            boolean r0 = r7.Aem(r0)
            if (r0 == 0) goto L_0x00cc
            r7 = 2
            int r1 = X.AnonymousClass1Y3.ABB
            X.0UN r0 = r9.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.1rG r0 = (X.C35461rG) r0
            int r7 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r8, r7, r1)
            X.1Yd r8 = (X.C25051Yd) r8
            r0 = 563912026096236(0x200e00000026c, double:2.786095593708827E-309)
            r7 = 60
            int r0 = r8.AqL(r0, r7)
        L_0x00b1:
            long r0 = (long) r0
            r7 = 60000(0xea60, double:2.9644E-319)
            long r0 = r0 * r7
            int r7 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            r0 = 1
            if (r7 <= 0) goto L_0x00bc
        L_0x00bb:
            r0 = 0
        L_0x00bc:
            if (r0 == 0) goto L_0x0037
            android.util.Pair r1 = new android.util.Pair
            java.lang.Long r0 = java.lang.Long.valueOf(r11)
            r1.<init>(r2, r0)
            r3.add(r1)
            goto L_0x0037
        L_0x00cc:
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r9.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.1Yd r8 = (X.C25051Yd) r8
            r0 = 564195494265584(0x20122000502f0, double:2.78749611255047E-309)
            r7 = 60
            int r0 = r8.AqO(r0, r7)
            goto L_0x00b1
        L_0x00e2:
            java.util.Comparator r0 = r14.A02
            java.util.Collections.sort(r3, r0)
            com.google.common.collect.ImmutableList$Builder r2 = new com.google.common.collect.ImmutableList$Builder
            r2.<init>()
            java.util.Iterator r1 = r3.iterator()
        L_0x00f0:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0102
            java.lang.Object r0 = r1.next()
            android.util.Pair r0 = (android.util.Pair) r0
            java.lang.Object r0 = r0.first
            r2.add(r0)
            goto L_0x00f0
        L_0x0102:
            com.google.common.collect.ImmutableList r0 = r2.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1H6.A02(com.google.common.collect.ImmutableList, boolean):com.google.common.collect.ImmutableList");
    }

    public String A03(ThreadSummary threadSummary) {
        String str = null;
        if (ThreadKey.A09(threadSummary.A0S)) {
            ImmutableList A012 = ((AnonymousClass1HB) AnonymousClass1XX.A03(AnonymousClass1Y3.AGy, this.A00)).A01(threadSummary);
            if (!A012.isEmpty()) {
                ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass1FZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BTP, this.A00)).A00)).BJC(282720517752670L);
                if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass1FZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BTP, this.A00)).A00)).Aer(282720517752670L, AnonymousClass0XE.A07)) {
                    str = ((Resources) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AEg, this.A00)).getQuantityString(2131689476, A012.size(), Integer.valueOf(A012.size()));
                }
            }
        }
        if (str != null) {
            return str;
        }
        ImmutableList A013 = A01(threadSummary, true);
        String str2 = null;
        if (!A013.isEmpty()) {
            ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass1FZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BTP, this.A00)).A00)).BJC(282720518014818L);
            if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass1FZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BTP, this.A00)).A00)).Aer(282720518014818L, AnonymousClass0XE.A07)) {
                str2 = ((Resources) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AEg, this.A00)).getQuantityString(2131689672, A013.size(), Integer.valueOf(A013.size()));
            }
        }
        if (str2 != null) {
            return str2;
        }
        return null;
    }

    private AnonymousClass1H6(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
