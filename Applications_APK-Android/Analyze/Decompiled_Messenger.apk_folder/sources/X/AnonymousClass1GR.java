package X;

import android.os.AsyncTask;

/* renamed from: X.1GR  reason: invalid class name */
public final class AnonymousClass1GR extends AsyncTask {
    public final /* synthetic */ AnonymousClass1GT A00;

    public AnonymousClass1GR(AnonymousClass1GT r1) {
        this.A00 = r1;
    }

    public Object doInBackground(Object... objArr) {
        return this.A00.A02(objArr);
    }

    public void onPostExecute(Object obj) {
        this.A00.A04(obj);
    }

    public void onPreExecute() {
        this.A00.A03();
    }

    public void onCancelled(Object obj) {
    }

    public void onProgressUpdate(Object... objArr) {
    }
}
