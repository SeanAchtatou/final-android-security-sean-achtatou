package X;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1WM  reason: invalid class name */
public final class AnonymousClass1WM implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    public static final AnonymousClass1WM A04 = new AnonymousClass1WM();
    private boolean A00 = false;
    public final AtomicBoolean A01 = new AtomicBoolean();
    public final AtomicBoolean A02 = new AtomicBoolean();
    private final ArrayList A03 = new ArrayList();

    public final void onActivityDestroyed(Activity activity) {
    }

    public final void onActivityPaused(Activity activity) {
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    public final void onConfigurationChanged(Configuration configuration) {
    }

    public final void onLowMemory() {
    }

    public static void A00(Application application) {
        AnonymousClass1WM r1 = A04;
        synchronized (r1) {
            if (!r1.A00) {
                application.registerActivityLifecycleCallbacks(r1);
                application.registerComponentCallbacks(r1);
                r1.A00 = true;
            }
        }
    }

    private final void A01(boolean z) {
        synchronized (A04) {
            ArrayList arrayList = this.A03;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                ((AnonymousClass1WL) obj).BPH(z);
            }
        }
    }

    public final void A02(AnonymousClass1WL r3) {
        synchronized (A04) {
            this.A03.add(r3);
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.A01.compareAndSet(true, false);
        this.A02.set(true);
        if (compareAndSet) {
            A01(false);
        }
    }

    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.A01.compareAndSet(true, false);
        this.A02.set(true);
        if (compareAndSet) {
            A01(false);
        }
    }

    public final void onTrimMemory(int i) {
        if (i == 20 && this.A01.compareAndSet(false, true)) {
            this.A02.set(true);
            A01(true);
        }
    }

    private AnonymousClass1WM() {
    }
}
