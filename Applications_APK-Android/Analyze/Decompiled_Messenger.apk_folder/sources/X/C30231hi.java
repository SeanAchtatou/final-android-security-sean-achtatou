package X;

import com.facebook.acra.ACRA;
import com.facebook.messaging.model.messages.Message;
import com.facebook.ui.media.attachments.model.MediaResource;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.1hi  reason: invalid class name and case insensitive filesystem */
public final class C30231hi {
    private static volatile C30231hi A03;
    public final AnonymousClass18P A00;
    public final Random A01 = C30241hj.A00();
    private final AnonymousClass09P A02;

    public static String A02(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "video";
            case 2:
                return "audio";
            default:
                return "photo";
        }
    }

    public static final C30231hi A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C30231hi.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C30231hi(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static Integer A01(int i) {
        if (i == 0) {
            return AnonymousClass07B.A00;
        }
        if (i == 1 || i == 2) {
            return AnonymousClass07B.A01;
        }
        throw new IllegalArgumentException("Unknown tincan attachment type");
    }

    private static Map A03(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("media_type", str);
        hashMap.put("epsilon", "0.050000");
        hashMap.put("num_types", String.valueOf(AnonymousClass07B.A00(3).length));
        return hashMap;
    }

    public Map A04(Message message) {
        String A022;
        if (((double) this.A01.nextInt(Integer.MAX_VALUE)) < 1.0737418235000001E8d) {
            String A032 = this.A00.A03(message);
            char c = 65535;
            int hashCode = A032.hashCode();
            if (hashCode != 97) {
                if (hashCode != 105) {
                    if (hashCode == 118 && A032.equals("v")) {
                        c = 1;
                    }
                } else if (A032.equals("i")) {
                    c = 0;
                }
            } else if (A032.equals("a")) {
                c = 2;
            }
            if (c == 0) {
                A022 = "photo";
            } else if (c == 1) {
                A022 = "video";
            } else if (c != 2) {
                this.A02.CGS("TincanMessageTypeLoggingHelper_IllegalArgument", "Unknown tincan message type");
                A022 = BuildConfig.FLAVOR;
            } else {
                A022 = "audio";
            }
        } else {
            A022 = A02(A01(this.A01.nextInt(AnonymousClass07B.A00(3).length)));
        }
        return A03(A022);
    }

    public Map A05(MediaResource mediaResource) {
        String A022;
        if (((double) this.A01.nextInt(Integer.MAX_VALUE)) < 1.0737418235000001E8d) {
            switch (mediaResource.A0L.ordinal()) {
                case 5:
                    A022 = "photo";
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    A022 = "audio";
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    A022 = "video";
                    break;
                default:
                    this.A02.CGS("TincanMessageTypeLoggingHelper_IllegalArgument", "Unknown mediaResource type");
                    A022 = BuildConfig.FLAVOR;
                    break;
            }
        } else {
            A022 = A02(A01(this.A01.nextInt(AnonymousClass07B.A00(3).length)));
        }
        return A03(A022);
    }

    private C30231hi(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass18P.A00(r2);
        this.A02 = C04750Wa.A01(r2);
    }
}
