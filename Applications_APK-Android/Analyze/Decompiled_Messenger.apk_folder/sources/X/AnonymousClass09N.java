package X;

import android.os.ConditionVariable;

/* renamed from: X.09N  reason: invalid class name */
public final class AnonymousClass09N {
    public final long A00;
    public final ConditionVariable A01 = new ConditionVariable();
    public final String A02;

    public AnonymousClass09N(long j, String str) {
        this.A00 = j;
        this.A02 = str;
    }
}
