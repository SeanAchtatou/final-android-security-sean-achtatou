package X;

import android.content.SharedPreferences;
import android.os.Bundle;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0HA  reason: invalid class name */
public final class AnonymousClass0HA extends Enum {
    private static final /* synthetic */ AnonymousClass0HA[] A00;
    public static final AnonymousClass0HA A01;
    public static final AnonymousClass0HA A02;
    public static final AnonymousClass0HA A03;
    public static final AnonymousClass0HA A04;
    public static final AnonymousClass0HA A05;
    public static final AnonymousClass0HA A06;
    public static final AnonymousClass0HA A07;
    public final String mPrefKey;
    public final AnonymousClass0HB mWrapper;

    static {
        AnonymousClass0HB r4 = AnonymousClass0HB.A03;
        AnonymousClass0HA r0 = new AnonymousClass0HA("BLOCKED_COUNTRIES_HOSTNAME", 0, "host_name_ipv6", r4);
        AnonymousClass0HA r15 = new AnonymousClass0HA("BLOCKED_COUNTRIES_ANALYTICS_ENDPOINT", 1, "analytics_endpoint", r4);
        AnonymousClass0HA r14 = new AnonymousClass0HA("BLOCKED_COUNTRIES_HN_TIMESTAMP", 2, "bc_host_name_timestamp", AnonymousClass0HB.A02);
        AnonymousClass0HA r13 = new AnonymousClass0HA("ANALYTIC_FB_UID", 3, "fb_uid", r4);
        A01 = r13;
        AnonymousClass0HA r12 = new AnonymousClass0HA("ANALYTIC_UID", 4, "user_id", r4);
        A03 = r12;
        AnonymousClass0HA r11 = new AnonymousClass0HA("ANALYTIC_IS_EMPLOYEE", 5, "is_employee", AnonymousClass0HB.A00);
        A02 = r11;
        AnonymousClass0HB r3 = AnonymousClass0HB.A01;
        AnonymousClass0HA r10 = new AnonymousClass0HA("ANALYTIC_YEAR_CLASS", 6, "year_class", r3);
        A04 = r10;
        AnonymousClass0HA r9 = new AnonymousClass0HA("LOGGING_HEALTH_STATS_SAMPLE_RATE", 7, "logging_health_stats_sample_rate", r3);
        A06 = r9;
        AnonymousClass0HA r8 = new AnonymousClass0HA("LOG_ANALYTICS_EVENTS", 8, "log_analytic_events", AnonymousClass0HB.A00);
        A07 = r8;
        AnonymousClass0HA r7 = new AnonymousClass0HA("LOGGING_ANALYTICS_EVENTS_SAMPLE_RATE", 9, "logging_analytic_events_sample_rate", AnonymousClass0HB.A01);
        A05 = r7;
        AnonymousClass0HA r6 = new AnonymousClass0HA("MQTT_DEVICE_ID", 10, "/settings/mqtt/id/mqtt_device_id", r4);
        AnonymousClass0HA r26 = r6;
        AnonymousClass0HA r25 = r7;
        AnonymousClass0HA r24 = r8;
        AnonymousClass0HA r23 = r9;
        AnonymousClass0HA r22 = r10;
        AnonymousClass0HA r21 = r11;
        AnonymousClass0HA r20 = r12;
        AnonymousClass0HA r19 = r13;
        AnonymousClass0HA r18 = r14;
        AnonymousClass0HA r17 = r15;
        A00 = new AnonymousClass0HA[]{r0, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, new AnonymousClass0HA("MQTT_DEVICE_SECRET", 11, "/settings/mqtt/id/mqtt_device_secret", r4), new AnonymousClass0HA("MQTT_DEVICE_CREDENTIALS_TIMESTAMP", 12, "/settings/mqtt/id/timestamp", AnonymousClass0HB.A02)};
    }

    public static AnonymousClass0HA valueOf(String str) {
        return (AnonymousClass0HA) Enum.valueOf(AnonymousClass0HA.class, str);
    }

    public static AnonymousClass0HA[] values() {
        return (AnonymousClass0HA[]) A00.clone();
    }

    public Object A00(SharedPreferences sharedPreferences, Object obj) {
        if (obj == null || this.mWrapper.A00().isInstance(obj)) {
            return this.mWrapper.A01(sharedPreferences, this.mPrefKey, obj);
        }
        throw new ClassCastException("Cannot cast" + obj.getClass());
    }

    public void A01(SharedPreferences.Editor editor, Object obj) {
        if (obj == null || this.mWrapper.A00().isInstance(obj)) {
            this.mWrapper.A03(editor, name(), obj);
            return;
        }
        throw new ClassCastException("Cannot cast" + obj.getClass());
    }

    public void A02(Bundle bundle, Object obj) {
        if (obj == null || this.mWrapper.A00().isInstance(obj)) {
            this.mWrapper.A04(bundle, name(), obj);
            return;
        }
        throw new ClassCastException("Cannot cast" + obj.getClass());
    }

    private AnonymousClass0HA(String str, int i, String str2, AnonymousClass0HB r4) {
        this.mPrefKey = str2;
        this.mWrapper = r4;
    }
}
