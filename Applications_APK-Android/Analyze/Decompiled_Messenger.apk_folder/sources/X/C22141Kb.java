package X;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* renamed from: X.1Kb  reason: invalid class name and case insensitive filesystem */
public final class C22141Kb<K, V> implements Map<K, V>, Cloneable, Serializable {
    private static final Object A07 = new AnonymousClass1J9();
    private static final long serialVersionUID = 5491530837953731070L;
    public transient int A00;
    public transient int A01;
    public transient int A02;
    public transient Object[] A03;
    private transient Collection A04;
    private transient Set A05;
    private transient Set A06;

    public void clear() {
        this.A01 = 0;
        Arrays.fill(this.A03, (Object) null);
        this.A00++;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof Map) {
                Map map = (Map) obj;
                if (size() == map.size()) {
                    int A062 = A06(-1);
                    while (A062 >= 0) {
                        Object A072 = A07(A062);
                        Object A08 = A08(A062);
                        if (map.containsKey(A072) && A05(map.get(A072), A08)) {
                            A062 = A06(A062);
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode;
        int hashCode2;
        int A062 = A06(-1);
        int i = 0;
        while (A062 >= 0) {
            Object A072 = A07(A062);
            Object A08 = A08(A062);
            if (A072 == null) {
                hashCode = 0;
            } else {
                hashCode = A072.hashCode();
            }
            if (A08 == null) {
                hashCode2 = 0;
            } else {
                hashCode2 = A08.hashCode();
            }
            i += hashCode ^ hashCode2;
            A062 = A06(A062);
        }
        return i;
    }

    public static int A00(C22141Kb r4, Object obj) {
        if (r4.A01 != 0) {
            if (obj == null) {
                obj = A07;
            }
            int A012 = r4.A01(obj);
            Object[] objArr = r4.A03;
            while (true) {
                Object obj2 = objArr[A012 << 1];
                if (obj2 == null) {
                    break;
                } else if (obj2 == obj || obj2.equals(obj)) {
                    return A012;
                } else {
                    objArr = r4.A03;
                    A012 = (A012 + 1) & ((objArr.length >>> 1) - 1);
                }
            }
            return A012;
        }
        return -1;
    }

    private void A02(int i) {
        if (((i - 1) & i) == 0) {
            Object[] objArr = this.A03;
            int length = objArr.length >>> 1;
            if (i <= length) {
                throw new IllegalArgumentException("New capacity must be greater than current capacity.");
            } else if (i <= 268435456) {
                int i2 = this.A01;
                A03(i);
                if (i2 > 0) {
                    int i3 = 0;
                    for (int i4 = 0; i4 < length && i3 < i2; i4++) {
                        int i5 = i4 << 1;
                        Object obj = objArr[i5];
                        if (obj != null) {
                            put(obj, objArr[i5 + 1]);
                            i3++;
                        }
                    }
                }
            } else {
                throw new IllegalArgumentException("New capacity is greater than maximum capacity.");
            }
        } else {
            throw new IllegalArgumentException("New capacity must be a power of two.");
        }
    }

    private void A03(int i) {
        if (((i - 1) & i) == 0) {
            this.A02 = (i >> 1) + (i >> 2);
            this.A01 = 0;
            this.A03 = new Object[(i << 1)];
            return;
        }
        throw new IllegalArgumentException("Capacity must be a power of two.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r0 > r6) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r0 > r1) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
        if (r0 <= r6) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A04(X.C22141Kb r5, int r6) {
        /*
            java.lang.Object[] r2 = r5.A03
            int r0 = r6 << 1
            r0 = r2[r0]
            if (r0 == 0) goto L_0x005a
            int r1 = r6 + 1
            int r0 = r2.length
        L_0x000b:
            int r0 = r0 >>> 1
            int r0 = r0 + -1
            r1 = r1 & r0
            java.lang.Object[] r2 = r5.A03
            int r4 = r1 << 1
            r0 = r2[r4]
            if (r0 == 0) goto L_0x0044
            r2 = 0
            if (r6 == r1) goto L_0x0028
            if (r0 == 0) goto L_0x005a
            int r0 = r5.A01(r0)
            if (r6 >= r1) goto L_0x0041
            if (r0 <= r6) goto L_0x0027
        L_0x0025:
            if (r0 <= r1) goto L_0x0028
        L_0x0027:
            r2 = 1
        L_0x0028:
            if (r2 == 0) goto L_0x003b
            java.lang.Object[] r3 = r5.A03
            int r2 = r6 << 1
            r0 = r3[r4]
            r3[r2] = r0
            int r2 = r2 + 1
            int r0 = r4 + 1
            r0 = r3[r0]
            r3[r2] = r0
            r6 = r1
        L_0x003b:
            int r1 = r1 + 1
            java.lang.Object[] r0 = r5.A03
            int r0 = r0.length
            goto L_0x000b
        L_0x0041:
            if (r0 > r6) goto L_0x0028
            goto L_0x0025
        L_0x0044:
            int r0 = r6 << 1
            r1 = 0
            r2[r0] = r1
            int r0 = r0 + 1
            r2[r0] = r1
            int r0 = r5.A01
            int r0 = r0 + -1
            r5.A01 = r0
            int r0 = r5.A00
            int r0 = r0 + 1
            r5.A00 = r0
            return
        L_0x005a:
            java.util.NoSuchElementException r0 = new java.util.NoSuchElementException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22141Kb.A04(X.1Kb, int):void");
    }

    public static boolean A05(Object obj, Object obj2) {
        if (obj != null) {
            return obj.equals(obj2);
        }
        if (obj2 == null) {
            return true;
        }
        return false;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeFloat(0.75f);
        objectOutputStream.writeInt(this.A03.length >>> 1);
        objectOutputStream.writeInt(this.A01);
        int A062 = A06(-1);
        while (A062 >= 0) {
            objectOutputStream.writeObject(A07(A062));
            objectOutputStream.writeObject(A08(A062));
            A062 = A06(A062);
        }
    }

    public int A06(int i) {
        if (this.A01 <= 0) {
            return -1;
        }
        Object[] objArr = this.A03;
        int length = objArr.length >>> 1;
        do {
            i++;
            if (i >= length) {
                return -1;
            }
        } while (objArr[i << 1] == null);
        return i;
    }

    public Object A07(int i) {
        Object obj = this.A03[i << 1];
        if (obj == null) {
            throw new NoSuchElementException();
        } else if (obj == A07) {
            return null;
        } else {
            return obj;
        }
    }

    public Object A08(int i) {
        Object[] objArr = this.A03;
        int i2 = i << 1;
        if (objArr[i2] != null) {
            return objArr[i2 + 1];
        }
        throw new NoSuchElementException();
    }

    public boolean containsValue(Object obj) {
        if (obj == null) {
            Object[] objArr = this.A03;
            int length = objArr.length >>> 1;
            for (int i = 0; i < length; i++) {
                int i2 = i << 1;
                if (objArr[i2 + 1] == null && objArr[i2] != null) {
                    return true;
                }
            }
            return false;
        }
        int length2 = this.A03.length >>> 1;
        for (int i3 = 0; i3 < length2; i3++) {
            Object obj2 = this.A03[(i3 << 1) + 1];
            if (obj2 != null && (obj2 == obj || obj2.equals(obj))) {
                return true;
            }
        }
        return false;
    }

    public Set entrySet() {
        if (this.A05 == null) {
            this.A05 = new AnonymousClass2TA(this);
        }
        return this.A05;
    }

    public boolean isEmpty() {
        if (this.A01 == 0) {
            return true;
        }
        return false;
    }

    public Set keySet() {
        if (this.A06 == null) {
            this.A06 = new C22315Asx(this);
        }
        return this.A06;
    }

    public Object put(Object obj, Object obj2) {
        int i;
        if (obj == null) {
            obj = A07;
        }
        int A012 = A01(obj);
        while (true) {
            Object[] objArr = this.A03;
            i = A012 << 1;
            Object obj3 = objArr[i];
            if (obj3 == null) {
                int i2 = this.A01;
                if (i2 != 268435456) {
                    if (i2 >= this.A02) {
                        A02((objArr.length >>> 1) << 1);
                        A012 = A01(obj);
                    }
                    while (true) {
                        Object[] objArr2 = this.A03;
                        int i3 = A012 << 1;
                        if (objArr2[i3] != null) {
                            A012 = (A012 + 1) & ((objArr2.length >>> 1) - 1);
                        } else {
                            objArr2[i3] = obj;
                            objArr2[i3 + 1] = obj2;
                            this.A01++;
                            this.A00++;
                            return null;
                        }
                    }
                } else {
                    throw new IllegalStateException("Maximum capacity reached, cannot add new item.");
                }
            } else if (obj3 == obj || obj3.equals(obj)) {
                int i4 = i + 1;
                Object[] objArr3 = this.A03;
                Object obj4 = objArr3[i4];
                objArr3[i4] = obj2;
            } else {
                A012 = (A012 + 1) & ((this.A03.length >>> 1) - 1);
            }
        }
        int i42 = i + 1;
        Object[] objArr32 = this.A03;
        Object obj42 = objArr32[i42];
        objArr32[i42] = obj2;
        return obj42;
    }

    public int size() {
        return this.A01;
    }

    public Collection values() {
        if (this.A04 == null) {
            this.A04 = new C22061Jt(this);
        }
        return this.A04;
    }

    private int A01(Object obj) {
        int hashCode = obj.hashCode();
        int i = hashCode + ((hashCode << 15) ^ -12931);
        int i2 = i ^ (i >>> 10);
        int i3 = i2 + (i2 << 3);
        int i4 = i3 ^ (i3 >>> 6);
        int i5 = i4 + (i4 << 2) + (i4 << 14);
        return (i5 ^ (i5 >>> 16)) & ((this.A03.length >>> 1) - 1);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        objectInputStream.readFloat();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            int i = 268435456;
            if (readInt < 4) {
                i = 4;
            } else if (readInt <= 268435456) {
                int i2 = readInt - 1;
                int i3 = i2 | (i2 >>> 1);
                int i4 = i3 | (i3 >>> 2);
                int i5 = i4 | (i4 >>> 4);
                int i6 = i5 | (i5 >>> 8);
                i = (i6 | (i6 >>> 16)) + 1;
            }
            A03(i);
            int readInt2 = objectInputStream.readInt();
            if (readInt2 >= 0) {
                for (int i7 = 0; i7 < readInt2; i7++) {
                    put(objectInputStream.readObject(), objectInputStream.readObject());
                }
                return;
            }
            throw new InvalidObjectException(AnonymousClass08S.A09("Invalid size: ", readInt2));
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Invalid capacity: ", readInt));
    }

    public Object clone() {
        try {
            C22141Kb r4 = (C22141Kb) super.clone();
            Object[] objArr = new Object[this.A03.length];
            r4.A03 = objArr;
            Object[] objArr2 = this.A03;
            System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
            return r4;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public boolean containsKey(Object obj) {
        if (A00(this, obj) >= 0) {
            return true;
        }
        return false;
    }

    public Object get(Object obj) {
        int A002 = A00(this, obj);
        if (A002 >= 0) {
            return this.A03[(A002 << 1) + 1];
        }
        return null;
    }

    public void putAll(Map map) {
        if (map.size() != 0) {
            if (map.size() > size() && map.size() > (this.A03.length >>> 1)) {
                int size = map.size() - 1;
                int i = size | (size >>> 1);
                int i2 = i | (i >>> 2);
                int i3 = i2 | (i2 >>> 4);
                int i4 = i3 | (i3 >>> 8);
                A02((i4 | (i4 >>> 16)) + 1);
            }
            for (Map.Entry entry : map.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    public Object remove(Object obj) {
        int A002 = A00(this, obj);
        if (A002 < 0) {
            return null;
        }
        Object A08 = A08(A002);
        A04(this, A002);
        return A08;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder("{");
        boolean z = true;
        int A062 = A06(-1);
        while (A062 >= 0) {
            if (!z) {
                sb.append(", ");
            }
            Object A072 = A07(A062);
            if (A072 == null) {
                A072 = "null";
            }
            sb.append(A072);
            sb.append("=");
            sb.append(A08(A062));
            z = false;
            A062 = A06(A062);
        }
        sb.append("}");
        return sb.toString();
    }

    public C22141Kb() {
        this(4);
    }

    public C22141Kb(int i) {
        if (i >= 0) {
            int i2 = 268435456;
            if (i < 4) {
                i2 = 4;
            } else if (i <= 268435456) {
                int i3 = i - 1;
                int i4 = i3 | (i3 >>> 1);
                int i5 = i4 | (i4 >>> 2);
                int i6 = i5 | (i5 >>> 4);
                int i7 = i6 | (i6 >>> 8);
                i2 = (i7 | (i7 >>> 16)) + 1;
            }
            A03(i2);
            return;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0A("Invalid capacity: ", i, " (must not be less than zero)."));
    }
}
