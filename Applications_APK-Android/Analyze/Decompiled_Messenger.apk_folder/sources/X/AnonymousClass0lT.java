package X;

import android.content.Context;
import com.facebook.fbservice.service.AuthQueue;
import com.facebook.fbservice.service.messenger.PhoneConfirmationQueue;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0lT  reason: invalid class name */
public final class AnonymousClass0lT {
    private static volatile AnonymousClass0lT A03;
    public boolean A00;
    public final Context A01;
    public final Set A02 = new C05180Xy();

    public synchronized void A02() {
        this.A00 = false;
    }

    public static final AnonymousClass0lT A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass0lT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass0lT(AnonymousClass1YA.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static boolean A01(C11000la r2) {
        Class<PhoneConfirmationQueue> cls = r2.A0D;
        if (cls == AuthQueue.class || cls == PhoneConfirmationQueue.class) {
            return true;
        }
        return false;
    }

    private AnonymousClass0lT(Context context) {
        this.A01 = context;
    }
}
