package X;

import com.facebook.common.util.TriState;

/* renamed from: X.0Wx  reason: invalid class name and case insensitive filesystem */
public final class C04930Wx implements AnonymousClass09Q {
    private final AnonymousClass1YI A00;

    public TriState BEu() {
        return this.A00.Ab9(AnonymousClass1Y3.A5J);
    }

    public TriState BGs() {
        return this.A00.Ab9(AnonymousClass1Y3.A2w);
    }

    public TriState CEb() {
        return this.A00.Ab9(729);
    }

    public C04930Wx(AnonymousClass1YI r1) {
        this.A00 = r1;
    }
}
