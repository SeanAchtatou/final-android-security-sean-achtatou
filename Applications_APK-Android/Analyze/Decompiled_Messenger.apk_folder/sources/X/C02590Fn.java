package X;

import org.json.JSONObject;

/* renamed from: X.0Fn  reason: invalid class name and case insensitive filesystem */
public final class C02590Fn extends AnonymousClass0FM {
    public long acquiredCount;
    public long heldTimeMs;
    public boolean isAttributionEnabled;
    public final AnonymousClass04b tagTimeMs;

    private void A00(C02590Fn r3) {
        this.heldTimeMs = r3.heldTimeMs;
        this.acquiredCount = r3.acquiredCount;
        if (r3.isAttributionEnabled && this.isAttributionEnabled) {
            this.tagTimeMs.clear();
            this.tagTimeMs.A0B(r3.tagTimeMs);
        }
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A00((C02590Fn) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r10, AnonymousClass0FM r11) {
        long longValue;
        C02590Fn r102 = (C02590Fn) r10;
        C02590Fn r112 = (C02590Fn) r11;
        if (r112 == null) {
            r112 = new C02590Fn(this.isAttributionEnabled);
        }
        if (r102 == null) {
            r112.A00(this);
        } else {
            r112.heldTimeMs = this.heldTimeMs - r102.heldTimeMs;
            r112.acquiredCount = this.acquiredCount - r102.acquiredCount;
            if (r112.isAttributionEnabled) {
                r112.tagTimeMs.clear();
                int size = this.tagTimeMs.size();
                for (int i = 0; i < size; i++) {
                    String str = (String) this.tagTimeMs.A07(i);
                    Long l = (Long) r102.tagTimeMs.get(str);
                    long longValue2 = ((Long) this.tagTimeMs.A09(i)).longValue();
                    if (l == null) {
                        longValue = 0;
                    } else {
                        longValue = l.longValue();
                    }
                    long j = longValue2 - longValue;
                    if (j != 0) {
                        r112.tagTimeMs.put(str, Long.valueOf(j));
                    }
                }
            }
        }
        return r112;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r10, AnonymousClass0FM r11) {
        long longValue;
        C02590Fn r102 = (C02590Fn) r10;
        C02590Fn r112 = (C02590Fn) r11;
        if (r112 == null) {
            r112 = new C02590Fn(this.isAttributionEnabled);
        }
        if (r102 == null) {
            r112.A00(this);
        } else {
            r112.heldTimeMs = this.heldTimeMs + r102.heldTimeMs;
            r112.acquiredCount = this.acquiredCount + r102.acquiredCount;
            if (r112.isAttributionEnabled) {
                r112.tagTimeMs.clear();
                int size = this.tagTimeMs.size();
                for (int i = 0; i < size; i++) {
                    String str = (String) this.tagTimeMs.A07(i);
                    Long l = (Long) r102.tagTimeMs.get(str);
                    AnonymousClass04b r5 = r112.tagTimeMs;
                    long longValue2 = ((Long) this.tagTimeMs.A09(i)).longValue();
                    if (l == null) {
                        longValue = 0;
                    } else {
                        longValue = l.longValue();
                    }
                    r5.put(str, Long.valueOf(longValue2 + longValue));
                }
                int size2 = r102.tagTimeMs.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str2 = (String) r102.tagTimeMs.A07(i2);
                    if (this.tagTimeMs.get(str2) == null) {
                        r112.tagTimeMs.put(str2, r102.tagTimeMs.A09(i2));
                    }
                }
            }
        }
        return r112;
    }

    public JSONObject A09() {
        if (!this.isAttributionEnabled) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        int size = this.tagTimeMs.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass04b r5 = this.tagTimeMs;
            long longValue = ((Long) r5.A09(i)).longValue();
            if (longValue > 0) {
                jSONObject.put((String) r5.A07(i), longValue);
            }
        }
        return jSONObject;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C02590Fn r7 = (C02590Fn) obj;
            if (this.isAttributionEnabled == r7.isAttributionEnabled && this.heldTimeMs == r7.heldTimeMs && this.acquiredCount == r7.acquiredCount) {
                return C02740Gd.A02(this.tagTimeMs, r7.tagTimeMs);
            }
        }
        return false;
    }

    public int hashCode() {
        long j = this.heldTimeMs;
        long j2 = this.acquiredCount;
        return ((((((this.isAttributionEnabled ? 1 : 0) * true) + this.tagTimeMs.hashCode()) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "WakeLockMetrics{isAttributionEnabled=" + this.isAttributionEnabled + ", tagTimeMs=" + this.tagTimeMs + ", heldTimeMs=" + this.heldTimeMs + ", acquiredCount=" + this.acquiredCount + '}';
    }

    public C02590Fn() {
        this(false);
    }

    public C02590Fn(boolean z) {
        this.tagTimeMs = new AnonymousClass04b();
        this.isAttributionEnabled = z;
    }
}
