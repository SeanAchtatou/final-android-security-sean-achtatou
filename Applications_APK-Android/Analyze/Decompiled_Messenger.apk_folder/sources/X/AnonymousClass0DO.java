package X;

/* renamed from: X.0DO  reason: invalid class name */
public final class AnonymousClass0DO implements AnonymousClass33X {
    public static final String __redex_internal_original_name = "com.facebook.redex.dynamicanalysis.support.DynamicAnalysisUploadConditionalWorkerInfo";
    private final C25051Yd A00;
    private final AnonymousClass0DL A01;
    private final C04310Tq A02;

    public String Ann() {
        return "DynamicAnalysisUploadConditionalWorkerInfo";
    }

    public static final AnonymousClass0DO A00(AnonymousClass1XY r1) {
        return new AnonymousClass0DO(r1);
    }

    public long Aqd() {
        return this.A00.At4(563319320609120L, AnonymousClass0XE.A07) * 60000;
    }

    public C627733g B13() {
        C627933j r0;
        C627333c r4 = new C627333c();
        C627333c.A00(r4, C627533e.BACKGROUND);
        if (this.A00.Aer(281844344226558L, AnonymousClass0XE.A07)) {
            r0 = C627933j.CONNECTED;
        } else {
            r0 = C627933j.CONNECTED_UNMETERED;
        }
        C627333c.A00(r4, r0);
        C627333c.A00(r4, C628033k.LOGGED_IN);
        return r4.A01();
    }

    private AnonymousClass0DO(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass0DM.A01(r2);
        this.A01 = AnonymousClass0DL.A01(r2);
        this.A00 = AnonymousClass0WT.A00(r2);
    }

    public C04310Tq Ai0() {
        return this.A02;
    }

    public C629734d B7B() {
        return C629734d.STATE_CHANGE;
    }

    public boolean CE4() {
        boolean z;
        if (AnonymousClass00M.A00().A05()) {
            AnonymousClass0DL r2 = this.A01;
            synchronized (r2) {
                z = r2.A00;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }
}
