package X;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

/* renamed from: X.0r5  reason: invalid class name and case insensitive filesystem */
public final class C13260r5 {
    public AnonymousClass0UN A00;
    public final Map A01 = new HashMap();
    public final Map A02 = new HashMap();

    public static final C13260r5 A00(AnonymousClass1XY r1) {
        return new C13260r5(r1);
    }

    public void A02(AnonymousClass13G r8) {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, this.A00)).AOz();
        String B53 = r8.B53();
        if (!this.A01.containsKey(B53)) {
            this.A01.put(B53, ((AnonymousClass0WP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8a, this.A00)).CIG(AnonymousClass08S.A0J(r8.B53(), "_preload"), new AnonymousClass13H(this, r8), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE, AnonymousClass07B.A00));
        }
    }

    private C13260r5(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public static void A01(Map map) {
        for (Future future : map.values()) {
            if (future != null) {
                future.cancel(false);
            }
        }
        map.clear();
    }
}
