package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.callercontext.CallerContextable;
import java.util.Set;

@UserScoped
/* renamed from: X.0tQ  reason: invalid class name and case insensitive filesystem */
public final class C14490tQ implements CallerContextable {
    private static C05540Zi A01 = null;
    public static final String __redex_internal_original_name = "com.facebook.messaging.cache.SpamThreadManager";
    public final Set A00 = new C05180Xy();

    public static final C14490tQ A00(AnonymousClass1XY r4) {
        C14490tQ r0;
        synchronized (C14490tQ.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C14490tQ((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C14490tQ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C14490tQ(AnonymousClass1XY r2) {
        C189216c.A02(r2);
        C30111hV.A00(r2);
    }
}
