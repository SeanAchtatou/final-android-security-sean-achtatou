package X;

/* renamed from: X.1No  reason: invalid class name and case insensitive filesystem */
public final class C22971No {
    public final C22991Nq A00;

    public AnonymousClass1PS A00(Object obj, C22891Nf r5) {
        C22991Nq r2 = this.A00;
        Throwable th = null;
        if (obj == null) {
            return null;
        }
        if (r2.C3V()) {
            th = new Throwable();
        }
        return AnonymousClass1PS.A03(obj, r5, r2, th);
    }

    public C22971No(C30641iP r2) {
        this.A00 = new C22981Np(r2);
    }
}
