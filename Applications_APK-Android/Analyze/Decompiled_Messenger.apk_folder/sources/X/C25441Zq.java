package X;

import com.google.common.base.Preconditions;
import java.io.Serializable;

/* renamed from: X.1Zq  reason: invalid class name and case insensitive filesystem */
public final class C25441Zq extends C07190cq implements Serializable {
    private static final long serialVersionUID = 0;
    public final byte[] bytes;

    public byte[] A05() {
        return (byte[]) this.bytes.clone();
    }

    public C25441Zq(byte[] bArr) {
        Preconditions.checkNotNull(bArr);
        this.bytes = bArr;
    }
}
