package X;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

/* renamed from: X.0EC  reason: invalid class name */
public abstract class AnonymousClass0EC implements Iterator, Map.Entry {
    private Object A00;
    private Object A01;
    private boolean A02;
    public Object A03;
    public Object A04;

    public abstract boolean A00();

    public final boolean hasNext() {
        if (this.A02) {
            return true;
        }
        this.A03 = null;
        this.A04 = null;
        this.A02 = false;
        boolean A002 = A00();
        this.A02 = A002;
        return A002;
    }

    public Object next() {
        if (this.A02 || hasNext()) {
            this.A00 = this.A03;
            this.A01 = this.A04;
            this.A02 = false;
            this.A03 = null;
            this.A04 = null;
            return this;
        }
        throw new NoSuchElementException();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public final Object setValue(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final Object getKey() {
        return this.A00;
    }

    public final Object getValue() {
        return this.A01;
    }
}
