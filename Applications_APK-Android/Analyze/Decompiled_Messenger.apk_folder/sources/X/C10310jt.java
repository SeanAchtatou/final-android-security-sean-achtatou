package X;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0jt  reason: invalid class name and case insensitive filesystem */
public final class C10310jt<K, V> extends LinkedHashMap<K, V> implements Serializable {
    private static final long serialVersionUID = 1;
    public transient int _jdkSerializeMaxEntries;
    public final int _maxEntries;

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(this._jdkSerializeMaxEntries);
    }

    public Object readResolve() {
        int i = this._jdkSerializeMaxEntries;
        return new C10310jt(i, i);
    }

    public C10310jt(int i, int i2) {
        super(i, 0.8f, true);
        this._maxEntries = i2;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this._jdkSerializeMaxEntries = objectInputStream.readInt();
    }

    public boolean removeEldestEntry(Map.Entry entry) {
        if (size() > this._maxEntries) {
            return true;
        }
        return false;
    }
}
