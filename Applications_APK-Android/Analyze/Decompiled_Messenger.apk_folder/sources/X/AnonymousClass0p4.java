package X;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import com.facebook.litho.ComponentTree;
import io.card.payment.BuildConfig;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* renamed from: X.0p4  reason: invalid class name */
public class AnonymousClass0p4 {
    public static final C17470yx A0F = new C17460yw();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public C17770zR A04;
    public ComponentTree A05;
    public C31941kq A06;
    public AnonymousClass1KE A07;
    public String A08;
    public final Context A09;
    public final C17510z1 A0A;
    public final C17540z4 A0B;
    public final AnonymousClass1JC A0C;
    private final C637038i A0D;
    private final String A0E;

    public static AnonymousClass0p4 A00(AnonymousClass0p4 r2, C17770zR r3) {
        AnonymousClass0p4 r1 = new AnonymousClass0p4(r2);
        r1.A04 = r3;
        r1.A05 = r2.A05;
        return r1;
    }

    public int A01(int i) {
        return this.A09.getResources().getColor(i);
    }

    public final Context A02() {
        return this.A09.getApplicationContext();
    }

    public Resources A03() {
        return this.A09.getResources();
    }

    public TypedArray A04(int[] iArr, int i) {
        Context context = this.A09;
        if (i == 0) {
            i = this.A00;
        }
        return context.obtainStyledAttributes(null, iArr, i, this.A01);
    }

    public C637038i A05() {
        C637038i r0;
        ComponentTree componentTree = this.A05;
        if (componentTree == null || (r0 = componentTree.A0T) == null) {
            return this.A0D;
        }
        return r0;
    }

    public AnonymousClass10N A06(int i, Object[] objArr) {
        if (!(this instanceof AnonymousClass1GA)) {
            C17770zR r1 = this.A04;
            if (r1 != null) {
                return new AnonymousClass10N(r1, i, objArr);
            }
            C09070gU.A01(AnonymousClass07B.A0C, "ComponentContext:NoScopeEventHandler", "Creating event handler without scope.");
            return C151456zm.A00;
        }
        C16070wR r12 = (C16070wR) ((AnonymousClass1GA) this).A03.get();
        if (r12 != null) {
            return new AnonymousClass10N(r12, i, objArr);
        }
        throw new IllegalStateException("Called newEventHandler on a released Section");
    }

    public C61302yf A07(String str, int i, C128015zX r5) {
        String str2;
        C16070wR r0;
        if (!(this instanceof AnonymousClass1GA)) {
            C17770zR r02 = this.A04;
            if (r02 != null) {
                str2 = r02.A06;
            }
            str2 = BuildConfig.FLAVOR;
        } else {
            WeakReference weakReference = ((AnonymousClass1GA) this).A03;
            if (weakReference == null) {
                r0 = null;
            } else {
                r0 = (C16070wR) weakReference.get();
            }
            if (r0 != null) {
                str2 = r0.A04;
            }
            str2 = BuildConfig.FLAVOR;
        }
        return new C61302yf(str2, i, str, r5);
    }

    public AnonymousClass1KE A08() {
        AnonymousClass1KE r0 = this.A07;
        if (r0 == null) {
            return null;
        }
        return AnonymousClass1KE.A00(r0);
    }

    public CharSequence A09(int i) {
        return this.A09.getResources().getText(i);
    }

    public Object A0A(Object obj) {
        Object obj2;
        ComponentTree componentTree = this.A05;
        if (componentTree == null) {
            return null;
        }
        synchronized (componentTree) {
            if (componentTree.A0M) {
                return null;
            }
            AnonymousClass1JC r1 = componentTree.A0E;
            synchronized (r1) {
                if (r1.A03 == null) {
                    r1.A03 = new HashMap();
                }
                obj2 = r1.A03.get(obj);
            }
            return obj2;
        }
    }

    public String A0B() {
        String str;
        ComponentTree componentTree = this.A05;
        if (componentTree == null || (str = componentTree.A0a) == null) {
            return this.A0E;
        }
        return str;
    }

    public String A0C(int i) {
        return this.A09.getResources().getString(i);
    }

    public String A0D(int i, Object... objArr) {
        return this.A09.getResources().getString(i, objArr);
    }

    public void A0E(C61322yh r5) {
        if (this instanceof C12310p3) {
            C12320p7 r0 = ((C12310p3) this).A00;
            if (r0 != null) {
                synchronized (r0.A0B) {
                    try {
                        r0.A0D.add(r5);
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
            }
        } else if (!(this instanceof AnonymousClass1GA)) {
            ComponentTree componentTree = this.A05;
            if (componentTree != null) {
                String str = this.A04.A06;
                synchronized (componentTree) {
                    try {
                        if (componentTree.A05 != null) {
                            componentTree.A0E.A09(str, r5, true);
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        throw th;
                    }
                }
            }
        } else {
            AnonymousClass1GA r02 = (AnonymousClass1GA) this;
            AnonymousClass1AC r2 = r02.A02;
            String str2 = ((C16070wR) r02.A03.get()).A04;
            synchronized (r2) {
                AnonymousClass1AC.A0J(r2, str2, r5, true);
            }
        }
    }

    public void A0F(C61322yh r6, String str) {
        boolean z;
        C188915v r0;
        if (this instanceof C12310p3) {
            C12320p7 r4 = ((C12310p3) this).A00;
            if (r4 != null) {
                synchronized (r4.A0B) {
                    try {
                        r4.A0D.add(r6);
                        r4.A0G.set(true);
                    } catch (Throwable th) {
                        while (true) {
                            th = th;
                        }
                    }
                }
                if (r4.A0E.compareAndSet(false, true)) {
                    r4.A07.BxN(r4.A0C, "SurfaceManager_updateState");
                    return;
                }
                return;
            }
            return;
        } else if (!(this instanceof AnonymousClass1GA)) {
            String str2 = this.A08;
            if (str2 == null) {
                ComponentTree componentTree = this.A05;
                if (componentTree != null) {
                    String str3 = this.A04.A06;
                    C31941kq r02 = this.A06;
                    if (r02 == null || (r0 = r02.A01) == null) {
                        z = false;
                    } else {
                        z = r0.A0S;
                    }
                    if (componentTree.A0f) {
                        synchronized (componentTree) {
                            try {
                                if (componentTree.A05 != null) {
                                    componentTree.A0E.A09(str3, r6, false);
                                    C32121lB.A04.addAndGet(1);
                                    componentTree.A0W(true, str, z);
                                    return;
                                }
                                return;
                            } catch (Throwable th2) {
                                while (true) {
                                    th = th2;
                                    break;
                                }
                            }
                        }
                    } else {
                        throw new RuntimeException("Triggering async state updates on this component tree is disabled, use sync state updates.");
                    }
                } else {
                    return;
                }
            } else {
                throw new IllegalStateException(AnonymousClass08S.A0P("Updating the state of a component during ", str2, " leads to unexpected behaviour, consider using lazy state updates."));
            }
        } else {
            AnonymousClass1GA r1 = (AnonymousClass1GA) this;
            C16070wR r2 = (C16070wR) r1.A03.get();
            AnonymousClass1AC r12 = r1.A02;
            if (r12 != null && r2 != null) {
                if (AnonymousClass1AJ.A00) {
                    r12.hashCode();
                }
                r12.A0Q(r2.A04, r6, str);
                return;
            }
            return;
        }
        throw th;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002f, code lost:
        X.C32121lB.A05.addAndGet(1);
        r1 = android.os.Looper.myLooper();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003a, code lost:
        if (r1 != null) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003c, code lost:
        android.util.Log.w("ComponentTree", "You cannot update state synchronously from a thread without a looper, using the default background layout thread instead");
        r3 = r4.A0Y;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0045, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r1 = r4.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0048, code lost:
        if (r1 == null) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
        r4.A0A.C1H(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004f, code lost:
        r4.A07 = new X.AnonymousClass16M(r4, r8, r5);
        r2 = io.card.payment.BuildConfig.FLAVOR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005e, code lost:
        if (r4.A0A.BHG() == false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0060, code lost:
        r2 = X.AnonymousClass08S.A0J("updateStateSyncNoLooper ", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0066, code lost:
        r4.A0A.BxL(r4.A07, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006d, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0072, code lost:
        r0 = (java.lang.ref.WeakReference) com.facebook.litho.ComponentTree.A0r.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x007a, code lost:
        if (r0 == null) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x007c, code lost:
        r3 = (X.C31551js) r0.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0082, code lost:
        if (r3 != null) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0084, code lost:
        r3 = new X.AnonymousClass11N(r1);
        com.facebook.litho.ComponentTree.A0r.set(new java.lang.ref.WeakReference(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0093, code lost:
        r2 = r4.A0Y;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0095, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0097, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r0 = r4.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x009b, code lost:
        if (r0 == null) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x009d, code lost:
        r3.C1H(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a0, code lost:
        r4.A07 = new X.AnonymousClass16M(r4, r8, r5);
        r1 = io.card.payment.BuildConfig.FLAVOR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ad, code lost:
        if (r3.BHG() == false) goto L_0x00b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00af, code lost:
        r1 = X.AnonymousClass08S.A0J("updateStateSync ", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00b5, code lost:
        r3.BxL(r4.A07, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00ba, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00bb, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00bc, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c1, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0G(X.C61322yh r7, java.lang.String r8) {
        /*
            r6 = this;
            boolean r0 = r6 instanceof X.C12310p3
            if (r0 != 0) goto L_0x00ee
            boolean r0 = r6 instanceof X.AnonymousClass1GA
            if (r0 != 0) goto L_0x00d0
            java.lang.String r3 = r6.A08
            if (r3 != 0) goto L_0x00c2
            com.facebook.litho.ComponentTree r4 = r6.A05
            if (r4 == 0) goto L_0x00ed
            X.0zR r0 = r6.A04
            java.lang.String r2 = r0.A06
            X.1kq r0 = r6.A06
            if (r0 == 0) goto L_0x0020
            X.15v r0 = r0.A01
            if (r0 == 0) goto L_0x0020
            boolean r5 = r0.A0S
        L_0x001e:
            monitor-enter(r4)
            goto L_0x0022
        L_0x0020:
            r5 = 0
            goto L_0x001e
        L_0x0022:
            X.0zR r0 = r4.A05     // Catch:{ all -> 0x00bf }
            if (r0 != 0) goto L_0x0028
            monitor-exit(r4)     // Catch:{ all -> 0x00bf }
            return
        L_0x0028:
            X.1JC r1 = r4.A0E     // Catch:{ all -> 0x00bf }
            r0 = 0
            r1.A09(r2, r7, r0)     // Catch:{ all -> 0x00bf }
            monitor-exit(r4)     // Catch:{ all -> 0x00bf }
            java.util.concurrent.atomic.AtomicLong r2 = X.C32121lB.A05
            r0 = 1
            r2.addAndGet(r0)
            android.os.Looper r1 = android.os.Looper.myLooper()
            if (r1 != 0) goto L_0x0072
            java.lang.String r1 = "ComponentTree"
            java.lang.String r0 = "You cannot update state synchronously from a thread without a looper, using the default background layout thread instead"
            android.util.Log.w(r1, r0)
            java.lang.Object r3 = r4.A0Y
            monitor-enter(r3)
            X.16M r1 = r4.A07     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x004f
            X.1js r0 = r4.A0A     // Catch:{ all -> 0x006f }
            r0.C1H(r1)     // Catch:{ all -> 0x006f }
        L_0x004f:
            X.16M r0 = new X.16M     // Catch:{ all -> 0x006f }
            r0.<init>(r4, r8, r5)     // Catch:{ all -> 0x006f }
            r4.A07 = r0     // Catch:{ all -> 0x006f }
            java.lang.String r2 = ""
            X.1js r0 = r4.A0A     // Catch:{ all -> 0x006f }
            boolean r0 = r0.BHG()     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x0066
            java.lang.String r0 = "updateStateSyncNoLooper "
            java.lang.String r2 = X.AnonymousClass08S.A0J(r0, r8)     // Catch:{ all -> 0x006f }
        L_0x0066:
            X.1js r1 = r4.A0A     // Catch:{ all -> 0x006f }
            X.16M r0 = r4.A07     // Catch:{ all -> 0x006f }
            r1.BxL(r0, r2)     // Catch:{ all -> 0x006f }
            monitor-exit(r3)     // Catch:{ all -> 0x006f }
            return
        L_0x006f:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x006f }
            goto L_0x00c1
        L_0x0072:
            java.lang.ThreadLocal r0 = com.facebook.litho.ComponentTree.A0r
            java.lang.Object r0 = r0.get()
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0
            if (r0 == 0) goto L_0x0097
            java.lang.Object r3 = r0.get()
            X.1js r3 = (X.C31551js) r3
        L_0x0082:
            if (r3 != 0) goto L_0x0093
            X.11N r3 = new X.11N
            r3.<init>(r1)
            java.lang.ThreadLocal r1 = com.facebook.litho.ComponentTree.A0r
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r3)
            r1.set(r0)
        L_0x0093:
            java.lang.Object r2 = r4.A0Y
            monitor-enter(r2)
            goto L_0x0099
        L_0x0097:
            r3 = 0
            goto L_0x0082
        L_0x0099:
            X.16M r0 = r4.A07     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x00a0
            r3.C1H(r0)     // Catch:{ all -> 0x00bc }
        L_0x00a0:
            X.16M r0 = new X.16M     // Catch:{ all -> 0x00bc }
            r0.<init>(r4, r8, r5)     // Catch:{ all -> 0x00bc }
            r4.A07 = r0     // Catch:{ all -> 0x00bc }
            java.lang.String r1 = ""
            boolean r0 = r3.BHG()     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x00b5
            java.lang.String r0 = "updateStateSync "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r8)     // Catch:{ all -> 0x00bc }
        L_0x00b5:
            X.16M r0 = r4.A07     // Catch:{ all -> 0x00bc }
            r3.BxL(r0, r1)     // Catch:{ all -> 0x00bc }
            monitor-exit(r2)     // Catch:{ all -> 0x00bc }
            return
        L_0x00bc:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00bc }
            goto L_0x00c1
        L_0x00bf:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00bf }
        L_0x00c1:
            throw r0
        L_0x00c2:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = "Updating the state of a component during "
            java.lang.String r0 = " leads to unexpected behaviour, consider using lazy state updates."
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r3, r0)
            r2.<init>(r0)
            throw r2
        L_0x00d0:
            r1 = r6
            X.1GA r1 = (X.AnonymousClass1GA) r1
            java.lang.ref.WeakReference r0 = r1.A03
            java.lang.Object r2 = r0.get()
            X.0wR r2 = (X.C16070wR) r2
            X.1AC r1 = r1.A02
            if (r1 == 0) goto L_0x00ed
            if (r2 == 0) goto L_0x00ed
            boolean r0 = X.AnonymousClass1AJ.A00
            if (r0 == 0) goto L_0x00e8
            r1.hashCode()
        L_0x00e8:
            java.lang.String r0 = r2.A04
            r1.A0P(r0, r7, r8)
        L_0x00ed:
            return
        L_0x00ee:
            r0 = r6
            X.0p3 r0 = (X.C12310p3) r0
            r0.A0F(r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0p4.A0G(X.2yh, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001f, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r0 = r1.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0022, code lost:
        if (r0 != null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0024, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0026, code lost:
        r0 = (java.util.List) r0.get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002c, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002d, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002f, code lost:
        r1 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0037, code lost:
        if (r1.hasNext() == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0039, code lost:
        r6.applyStateUpdate((X.C61322yh) r1.next());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0043, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0H(X.AnonymousClass11I r6) {
        /*
            r5 = this;
            com.facebook.litho.ComponentTree r4 = r5.A05
            if (r4 == 0) goto L_0x004c
            X.0zR r0 = r5.A04
            java.lang.String r3 = r0.A06
            monitor-enter(r4)
            X.0zR r0 = r4.A05     // Catch:{ all -> 0x0049 }
            if (r0 != 0) goto L_0x000f
            monitor-exit(r4)     // Catch:{ all -> 0x0049 }
            return
        L_0x000f:
            X.1JC r2 = r4.A0E     // Catch:{ all -> 0x0049 }
            X.1JC r1 = new X.1JC     // Catch:{ all -> 0x0049 }
            r0 = 0
            r1.<init>(r0)     // Catch:{ all -> 0x0049 }
            monitor-enter(r2)     // Catch:{ all -> 0x0049 }
            java.util.Map r0 = r2.A05     // Catch:{ all -> 0x0046 }
            X.AnonymousClass1JC.A05(r1, r0)     // Catch:{ all -> 0x0046 }
            monitor-exit(r2)     // Catch:{ all -> 0x0046 }
            monitor-exit(r4)     // Catch:{ all -> 0x0049 }
            monitor-enter(r1)
            java.util.Map r0 = r1.A05     // Catch:{ all -> 0x0043 }
            if (r0 != 0) goto L_0x0026
            r0 = 0
            goto L_0x002c
        L_0x0026:
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x0043 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x0043 }
        L_0x002c:
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            if (r0 == 0) goto L_0x004c
            java.util.Iterator r1 = r0.iterator()
        L_0x0033:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x004c
            java.lang.Object r0 = r1.next()
            X.2yh r0 = (X.C61322yh) r0
            r6.applyStateUpdate(r0)
            goto L_0x0033
        L_0x0043:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            goto L_0x004b
        L_0x0046:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0046 }
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0049 }
        L_0x004b:
            throw r0
        L_0x004c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0p4.A0H(X.11I):void");
    }

    public void A0I(Object obj, Object obj2) {
        ComponentTree componentTree = this.A05;
        if (componentTree != null) {
            synchronized (componentTree) {
                if (!componentTree.A0M) {
                    AnonymousClass1JC r1 = componentTree.A0E;
                    synchronized (r1) {
                        if (r1.A03 == null) {
                            r1.A03 = new HashMap();
                        }
                        r1.A03.put(obj, obj2);
                    }
                }
            }
        }
    }

    public boolean A0J() {
        AnonymousClass1KH r0;
        C31941kq r02 = this.A06;
        if (r02 == null || (r0 = r02.A00) == null) {
            return false;
        }
        return r0.A0H;
    }

    public AnonymousClass0p4(AnonymousClass0p4 r4) {
        this(r4, r4.A0C, r4.A07, r4.A06);
    }

    public AnonymousClass0p4(AnonymousClass0p4 r3, AnonymousClass1JC r4, AnonymousClass1KE r5, C31941kq r6) {
        this.A01 = 0;
        this.A00 = 0;
        this.A09 = r3.A09;
        this.A0A = r3.A0A;
        this.A0B = r3.A0B;
        this.A03 = r3.A03;
        this.A02 = r3.A02;
        this.A04 = r3.A04;
        ComponentTree componentTree = r3.A05;
        this.A05 = componentTree;
        this.A06 = r6;
        this.A0D = r3.A0D;
        String str = r3.A0E;
        if (str == null && componentTree != null) {
            str = componentTree.A0I();
        }
        this.A0E = str;
        this.A0C = r4 == null ? r3.A0C : r4;
        this.A07 = r5 == null ? r3.A07 : r5;
    }

    public AnonymousClass0p4(Context context) {
        this(context, null, null, null, null);
    }

    public AnonymousClass0p4(Context context, String str, C637038i r7, AnonymousClass1JC r8, AnonymousClass1KE r9) {
        C17510z1 r0;
        this.A01 = 0;
        this.A00 = 0;
        if (r7 == null || str != null) {
            this.A09 = context;
            Configuration configuration = context.getResources().getConfiguration();
            synchronized (C17510z1.class) {
                C17510z1 r02 = C17510z1.latest;
                if (r02 == null || !r02.A00.equals(configuration)) {
                    C17510z1.latest = new C17520z2(new Configuration(configuration));
                }
                r0 = C17510z1.latest;
            }
            this.A0A = r0;
            this.A0B = new C17540z4(this);
            this.A07 = r9;
            this.A0D = r7;
            this.A0E = str;
            this.A0C = r8;
            return;
        }
        throw new IllegalStateException("When a ComponentsLogger is set, a LogTag must be set");
    }
}
