package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1Y8  reason: invalid class name */
public final class AnonymousClass1Y8 extends AnonymousClass1Y7 {
    public boolean A00;

    public static AnonymousClass1Y8 A02(AnonymousClass1Y8 r1, String str) {
        if (r1.A00 || str == null || BuildConfig.FLAVOR.equals(str)) {
            return r1;
        }
        return A03(r1, str);
    }

    private static AnonymousClass1Y8 A03(AnonymousClass1Y8 r2, String str) {
        AnonymousClass1Y8 r0 = (AnonymousClass1Y8) r2.A02;
        if (r0 == null) {
            return new AnonymousClass1Y8(AnonymousClass08S.A0P("/", str, "/"), true);
        }
        return A03(r0, str).A09(r2.A0C());
    }

    /* renamed from: A0D */
    public AnonymousClass1Y8 A09(String str) {
        return new AnonymousClass1Y8(this, str, this.A00);
    }

    public static AnonymousClass1Y8 A00(AnonymousClass1Y7 r3, String str) {
        if (r3.A0A() != null) {
            return A00(r3.A0A(), str).A09(r3.A0C());
        }
        if (!r3.A0C().startsWith(AnonymousClass08S.A0J("/", str))) {
            return C04350Ue.A0A;
        }
        return new AnonymousClass1Y8(r3.A0C().substring(AnonymousClass08S.A0J("/", str).length()), false);
    }

    public static AnonymousClass1Y8 A01(AnonymousClass1Y7 r2, boolean z) {
        if (r2.A0A() == null) {
            return new AnonymousClass1Y8(r2.A0C(), z);
        }
        return A01(r2.A0A(), z).A09(r2.A0C());
    }

    public AnonymousClass1Y8(AnonymousClass063 r1, String str, boolean z) {
        super(r1, str);
        this.A00 = z;
    }

    public AnonymousClass1Y8(String str, boolean z) {
        super(str);
        this.A00 = z;
    }
}
