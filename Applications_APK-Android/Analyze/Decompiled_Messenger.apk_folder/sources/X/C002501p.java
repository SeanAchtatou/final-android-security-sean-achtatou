package X;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/* renamed from: X.01p  reason: invalid class name and case insensitive filesystem */
public final class C002501p extends OutputStream {
    private final List A00;

    public void close() {
        AnonymousClass0Jk r2 = null;
        for (OutputStream close : this.A00) {
            try {
                close.close();
            } catch (IOException e) {
                if (r2 == null) {
                    r2 = new AnonymousClass0Jk("Exception closing the stream");
                }
                r2.A00(e);
            }
        }
        if (r2 != null) {
            throw r2;
        }
    }

    public void flush() {
        AnonymousClass0Jk r2 = null;
        for (OutputStream flush : this.A00) {
            try {
                flush.flush();
            } catch (IOException e) {
                if (r2 == null) {
                    r2 = new AnonymousClass0Jk("Exception flushing the stream");
                }
                r2.A00(e);
            }
        }
        if (r2 != null) {
            throw r2;
        }
    }

    public C002501p(List list) {
        this.A00 = list;
    }

    public void write(int i) {
        AnonymousClass0Jk r2 = null;
        for (OutputStream write : this.A00) {
            try {
                write.write(i);
            } catch (IOException e) {
                if (r2 == null) {
                    r2 = new AnonymousClass0Jk("Exception writing one byte to the stream");
                }
                r2.A00(e);
            }
        }
        if (r2 != null) {
            throw r2;
        }
    }

    public void write(byte[] bArr) {
        AnonymousClass0Jk r2 = null;
        for (OutputStream write : this.A00) {
            try {
                write.write(bArr);
            } catch (IOException e) {
                if (r2 == null) {
                    r2 = new AnonymousClass0Jk("Exception writing to the stream");
                }
                r2.A00(e);
            }
        }
        if (r2 != null) {
            throw r2;
        }
    }

    public void write(byte[] bArr, int i, int i2) {
        AnonymousClass0Jk r2 = null;
        for (OutputStream write : this.A00) {
            try {
                write.write(bArr, i, i2);
            } catch (IOException e) {
                if (r2 == null) {
                    r2 = new AnonymousClass0Jk("Exception writing to the stream");
                }
                r2.A00(e);
            }
        }
        if (r2 != null) {
            throw r2;
        }
    }
}
