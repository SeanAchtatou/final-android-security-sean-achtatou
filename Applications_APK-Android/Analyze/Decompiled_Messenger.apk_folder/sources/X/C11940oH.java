package X;

import android.net.Uri;
import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.model.attachment.Attachment;
import com.facebook.messaging.model.attachment.AttachmentImageMap;
import com.facebook.messaging.model.attachment.AudioData;
import com.facebook.messaging.model.attachment.ImageData;
import com.facebook.messaging.model.attachment.ImageUrl;
import com.facebook.messaging.model.attachment.VideoData;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0oH  reason: invalid class name and case insensitive filesystem */
public final class C11940oH {
    public final AnonymousClass0jD A00;

    private AttachmentImageMap A01(JsonNode jsonNode, String str) {
        C47422Uv r3;
        if (jsonNode != null && jsonNode.isTextual()) {
            String asText = jsonNode.asText();
            AnonymousClass1TT r2 = new AnonymousClass1TT();
            if (!C06850cB.A0B(asText)) {
                Iterator fields = this.A00.A02(asText).fields();
                while (fields.hasNext()) {
                    Map.Entry entry = (Map.Entry) fields.next();
                    String str2 = (String) entry.getKey();
                    C47422Uv[] values = C47422Uv.values();
                    int length = values.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            r3 = null;
                            break;
                        }
                        r3 = values[i];
                        if (Objects.equal(r3.serializedName, str2)) {
                            break;
                        }
                        i++;
                    }
                    if (r3 != null) {
                        String asText2 = ((JsonNode) entry.getValue()).asText();
                        C47432Uw r4 = new C47432Uw();
                        if (!C06850cB.A0B(asText2)) {
                            JsonNode A02 = this.A00.A02(asText2);
                            if (A02.has("width")) {
                                r4.A01 = JSONUtil.A04(A02.get("width"));
                            }
                            if (A02.has("height")) {
                                r4.A00 = JSONUtil.A04(A02.get("height"));
                            }
                            if (A02.has("src")) {
                                r4.A02 = JSONUtil.A0N(A02.get("src"));
                            }
                        }
                        r2.A01.put(r3, new ImageUrl(r4));
                    }
                }
                r2.A00 = C61202yV.A00(str);
                return new AttachmentImageMap(r2);
            }
        }
        return null;
    }

    public static final C11940oH A00(AnonymousClass1XY r1) {
        return new C11940oH(r1);
    }

    private static String A02(AttachmentImageMap attachmentImageMap) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        if (attachmentImageMap != null) {
            C24971Xv it = attachmentImageMap.A01.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                String str = ((C47422Uv) entry.getKey()).serializedName;
                ImageUrl imageUrl = (ImageUrl) entry.getValue();
                ObjectNode objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                if (imageUrl != null) {
                    objectNode2.put("width", imageUrl.A01);
                    objectNode2.put("height", imageUrl.A00);
                    objectNode2.put("src", imageUrl.A02);
                }
                objectNode.put(str, objectNode2.toString());
            }
        }
        return objectNode.toString();
    }

    public ImmutableList A03(String str, String str2) {
        AudioData audioData;
        String str3 = str;
        if (C06850cB.A0B(str3) || str3.equals("[]")) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = this.A00.A02(str3).iterator();
        while (it.hasNext()) {
            JsonNode jsonNode = (JsonNode) it.next();
            C15860w6 r1 = new C15860w6(JSONUtil.A0N(jsonNode.get("id")), str2);
            r1.A06 = JSONUtil.A0N(jsonNode.get("fbid"));
            r1.A09 = JSONUtil.A0N(jsonNode.get("mime_type"));
            r1.A07 = JSONUtil.A0N(jsonNode.get("filename"));
            r1.A00 = JSONUtil.A04(jsonNode.get("file_size"));
            if (jsonNode.has("image_data_width") && jsonNode.has("image_data_height")) {
                int A04 = JSONUtil.A04(jsonNode.get("image_data_source"));
                C47392Ur r14 = C47392Ur.QUICKCAM;
                if (A04 != r14.intValue) {
                    r14 = C47392Ur.NONQUICKCAM;
                }
                r1.A03 = new ImageData(JSONUtil.A04(jsonNode.get("image_data_width")), JSONUtil.A04(jsonNode.get("image_data_height")), A01(jsonNode.get("urls"), JSONUtil.A0N(jsonNode.get("image_format"))), A01(jsonNode.get("image_animated_urls"), JSONUtil.A0N(jsonNode.get("animated_image_format"))), r14, JSONUtil.A0S(jsonNode.get("render_as_sticker")), JSONUtil.A0N(jsonNode.get("mini_preview")), JSONUtil.A0N(jsonNode.get("blurred_image_uri")));
            }
            if (jsonNode.has("video_data_width") && jsonNode.has("video_data_height")) {
                AnonymousClass2VB A002 = AnonymousClass2VB.A00(JSONUtil.A04(jsonNode.get("video_data_source")));
                Uri uri = null;
                if (jsonNode.hasNonNull("video_data_thumbnail_url")) {
                    uri = Uri.parse(JSONUtil.A0N(jsonNode.get("video_data_thumbnail_url")));
                }
                r1.A04 = new VideoData(JSONUtil.A04(jsonNode.get("video_data_width")), JSONUtil.A04(jsonNode.get("video_data_height")), JSONUtil.A04(jsonNode.get("video_data_rotation")), JSONUtil.A04(jsonNode.get("video_data_length")), JSONUtil.A04(jsonNode.get("video_loop_count")), A002, Uri.parse(JSONUtil.A0N(jsonNode.get("video_data_url"))), uri, null);
            }
            if (jsonNode.has("is_voicemail")) {
                if (!jsonNode.has("call_id") || !jsonNode.hasNonNull("audio_uri")) {
                    audioData = new AudioData(JSONUtil.A0S(jsonNode.get("is_voicemail")), BuildConfig.FLAVOR, null, 0, 0);
                } else {
                    audioData = new AudioData(JSONUtil.A0S(jsonNode.get("is_voicemail")), JSONUtil.A0N(jsonNode.get("call_id")), Uri.parse(JSONUtil.A0N(jsonNode.get("audio_uri"))), JSONUtil.A04(jsonNode.get("durationS")), JSONUtil.A04(jsonNode.get("durationMs")));
                }
                r1.A02 = audioData;
            }
            ImmutableMap.Builder builder2 = ImmutableMap.builder();
            if (jsonNode.has("is_preview")) {
                builder2.put("is_preview", JSONUtil.A0N(jsonNode.get("is_preview")));
            }
            if (jsonNode.has("is_thumbnail")) {
                builder2.put("is_thumbnail", JSONUtil.A0N(jsonNode.get("is_thumbnail")));
            }
            if (jsonNode.has("dash_manifest")) {
                builder2.put("dash_manifest", JSONUtil.A0N(jsonNode.get("dash_manifest")));
            }
            String $const$string = AnonymousClass24B.$const$string(7);
            if (jsonNode.has($const$string)) {
                builder2.put($const$string, JSONUtil.A0N(jsonNode.get($const$string)));
            }
            if (jsonNode.has("blurred_image_url")) {
                builder2.put("blurred_image_url", JSONUtil.A0N(jsonNode.get("blurred_image_url")));
            }
            r1.A0A = builder2.build();
            if (jsonNode.has("setReceivedTimestampMs")) {
                r1.A01 = JSONUtil.A06(jsonNode.get("setReceivedTimestampMs"));
            }
            builder.add((Object) new Attachment(r1));
        }
        return builder.build();
    }

    public String A04(List list) {
        String uri;
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Attachment attachment = (Attachment) it.next();
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("id", attachment.A09);
            objectNode.put("fbid", attachment.A07);
            objectNode.put("mime_type", attachment.A0C);
            objectNode.put("filename", attachment.A08);
            objectNode.put("file_size", attachment.A00);
            ImageData imageData = attachment.A03;
            if (imageData != null) {
                objectNode.put("image_data_width", imageData.A01);
                objectNode.put("image_data_height", attachment.A03.A00);
                AttachmentImageMap attachmentImageMap = attachment.A03.A03;
                if (attachmentImageMap != null) {
                    objectNode.put("urls", A02(attachmentImageMap));
                    C61202yV r0 = attachment.A03.A03.A00;
                    if (r0 != null) {
                        objectNode.put("image_format", r0.stringValue);
                    }
                }
                AttachmentImageMap attachmentImageMap2 = attachment.A03.A02;
                if (attachmentImageMap2 != null) {
                    objectNode.put("image_animated_urls", A02(attachmentImageMap2));
                    C61202yV r02 = attachment.A03.A02.A00;
                    if (r02 != null) {
                        objectNode.put("animated_image_format", r02.stringValue);
                    }
                }
                objectNode.put("image_data_source", attachment.A03.A04.intValue);
                objectNode.put("render_as_sticker", attachment.A03.A07);
                objectNode.put("mini_preview", attachment.A03.A06);
                objectNode.put("blurred_image_uri", attachment.A03.A05);
            }
            VideoData videoData = attachment.A04;
            if (videoData != null) {
                objectNode.put("video_data_width", videoData.A04);
                objectNode.put("video_data_height", attachment.A04.A01);
                objectNode.put("video_data_rotation", attachment.A04.A03);
                objectNode.put("video_data_length", attachment.A04.A00);
                objectNode.put("video_loop_count", attachment.A04.A02);
                objectNode.put("video_data_source", attachment.A04.A08.intValue);
                objectNode.put("video_data_url", attachment.A04.A07.toString());
                Uri uri2 = attachment.A04.A06;
                if (uri2 != null) {
                    objectNode.put("video_data_thumbnail_url", uri2.toString());
                }
            }
            AudioData audioData = attachment.A02;
            if (audioData != null) {
                Uri uri3 = audioData.A02;
                objectNode.put("is_voicemail", audioData.A04);
                objectNode.put("call_id", attachment.A02.A03);
                if (uri3 == null) {
                    uri = BuildConfig.FLAVOR;
                } else {
                    uri = uri3.toString();
                }
                objectNode.put("audio_uri", uri);
                objectNode.put("durationS", attachment.A02.A01);
                objectNode.put("durationMs", attachment.A02.A00);
            }
            ImmutableMap immutableMap = attachment.A05;
            if (immutableMap != null) {
                if (immutableMap.get("is_preview") != null) {
                    objectNode.put("is_preview", (String) attachment.A05.get("is_preview"));
                }
                if (attachment.A05.get("is_thumbnail") != null) {
                    objectNode.put("is_thumbnail", (String) attachment.A05.get("is_thumbnail"));
                }
                if (attachment.A05.get("dash_manifest") != null) {
                    objectNode.put("dash_manifest", (String) attachment.A05.get("dash_manifest"));
                }
                ImmutableMap immutableMap2 = attachment.A05;
                String $const$string = AnonymousClass24B.$const$string(7);
                if (immutableMap2.get($const$string) != null) {
                    objectNode.put($const$string, (String) attachment.A05.get($const$string));
                }
                if (attachment.A05.get("blurred_image_url") != null) {
                    objectNode.put("blurred_image_url", (String) attachment.A05.get("blurred_image_url"));
                }
            }
            long j = attachment.A01;
            if (j > 0) {
                objectNode.put("setReceivedTimestampMs", j);
            }
            arrayNode.add(objectNode);
        }
        return arrayNode.toString();
    }

    public C11940oH(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0jD.A00(r2);
    }
}
