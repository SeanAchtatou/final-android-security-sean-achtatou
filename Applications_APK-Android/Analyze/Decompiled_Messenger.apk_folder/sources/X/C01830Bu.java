package X;

import android.text.TextUtils;
import android.util.Pair;
import io.card.payment.BuildConfig;

/* renamed from: X.0Bu  reason: invalid class name and case insensitive filesystem */
public final class C01830Bu extends Pair {
    public static final C01830Bu A00 = new C01830Bu(BuildConfig.FLAVOR, BuildConfig.FLAVOR);

    public static C01830Bu A00(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return A00;
        }
        return new C01830Bu(str, str2);
    }

    private C01830Bu(String str, String str2) {
        super(str, str2);
    }
}
