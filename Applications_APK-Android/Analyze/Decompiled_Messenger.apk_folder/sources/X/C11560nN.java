package X;

import com.google.common.base.Preconditions;
import java.util.IdentityHashMap;

/* renamed from: X.0nN  reason: invalid class name and case insensitive filesystem */
public final class C11560nN {
    private IdentityHashMap A00;

    public synchronized Object A00(Object obj) {
        Object obj2;
        Preconditions.checkNotNull(obj);
        IdentityHashMap identityHashMap = this.A00;
        if (identityHashMap != null) {
            obj2 = identityHashMap.get(obj);
        } else {
            obj2 = null;
        }
        return obj2;
    }

    public synchronized void A01(Object obj, Object obj2) {
        Preconditions.checkNotNull(obj);
        Preconditions.checkNotNull(obj2);
        if (this.A00 == null) {
            this.A00 = new IdentityHashMap();
        }
        this.A00.put(obj, obj2);
    }
}
