package X;

import java.util.Map;

/* renamed from: X.1Fv  reason: invalid class name and case insensitive filesystem */
public abstract class C21251Fv extends C08890g9 implements Map.Entry {
    public Map.Entry A02() {
        return ((C21241Fu) this).A01;
    }

    public boolean equals(Object obj) {
        return A02().equals(obj);
    }

    public Object getKey() {
        return A02().getKey();
    }

    public Object getValue() {
        return A02().getValue();
    }

    public int hashCode() {
        return A02().hashCode();
    }

    public Object setValue(Object obj) {
        return A02().setValue(obj);
    }
}
