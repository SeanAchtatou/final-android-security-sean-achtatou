package X;

import android.content.Context;

/* renamed from: X.0Bi  reason: invalid class name and case insensitive filesystem */
public final class C01710Bi implements C01630Az {
    public final Context A00;

    public String Acq() {
        return AnonymousClass0B2.A00(this.A00, AnonymousClass07B.A06).getString("analytics_endpoint", null);
    }

    public String Av5() {
        return AnonymousClass0B2.A00(this.A00, AnonymousClass07B.A06).getString("host_name_ipv6", null);
    }

    public C01710Bi(Context context) {
        this.A00 = context;
    }
}
