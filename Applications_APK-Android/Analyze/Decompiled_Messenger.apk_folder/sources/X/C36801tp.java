package X;

import com.facebook.gk.sessionless.GkSessionlessModule;
import com.facebook.mobileconfigonomnistore.MobileConfigOmnistoreUpdaterHolder;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tp  reason: invalid class name and case insensitive filesystem */
public final class C36801tp implements AnonymousClass1V0 {
    private static final Class A06 = C36801tp.class;
    private static volatile C36801tp A07;
    private MobileConfigOmnistoreUpdaterHolder A00 = null;
    public final AnonymousClass1YI A01;
    private final C09400hF A02;
    private final C04310Tq A03;
    private final C04310Tq A04;
    private final C04310Tq A05;

    public static final C36801tp A00(AnonymousClass1XY r9) {
        if (A07 == null) {
            synchronized (C36801tp.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r9);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r9.getApplicationInjector();
                        AnonymousClass0VG A003 = AnonymousClass0VG.A00(AnonymousClass1Y3.BHG, applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.BC5, applicationInjector);
                        C04310Tq A032 = C10580kT.A03(applicationInjector);
                        C04750Wa.A01(applicationInjector);
                        A07 = new C36801tp(A003, A004, A032, C09450hO.A00(applicationInjector), GkSessionlessModule.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0030, code lost:
        if (r14.A01.AbO(X.AnonymousClass1Y3.A0r, false) != false) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Bgx(X.C29271g9 r15) {
        /*
            r14 = this;
            X.0Tq r0 = r14.A03
            java.lang.Object r1 = r0.get()
            com.facebook.mobileconfig.init.MobileConfigInit r1 = (com.facebook.mobileconfig.init.MobileConfigInit) r1
            X.0Tq r0 = r14.A05
            java.lang.Object r0 = r0.get()
            com.facebook.auth.viewercontext.ViewerContext r0 = (com.facebook.auth.viewercontext.ViewerContext) r0
            r1.A08(r0)
            X.0Tq r0 = r14.A04
            java.lang.Object r2 = r0.get()
            X.1Yb r2 = (X.C25031Yb) r2
            X.1YI r3 = r14.A01
            r1 = 123(0x7b, float:1.72E-43)
            r0 = 0
            boolean r0 = r3.AbO(r1, r0)
            if (r0 != 0) goto L_0x0032
            X.1YI r3 = r14.A01
            r1 = 117(0x75, float:1.64E-43)
            r0 = 0
            boolean r1 = r3.AbO(r1, r0)
            r0 = 0
            if (r1 == 0) goto L_0x0033
        L_0x0032:
            r0 = 1
        L_0x0033:
            if (r0 == 0) goto L_0x00a3
            if (r2 == 0) goto L_0x00a3
            boolean r0 = r2.isValid()
            if (r0 == 0) goto L_0x00a3
            X.0WV r0 = r2.A00()
            boolean r0 = r0 instanceof com.facebook.mobileconfig.MobileConfigManagerHolderImpl
            if (r0 != 0) goto L_0x004d
            java.lang.Class r1 = X.C36801tp.A06
            java.lang.String r0 = "mobileconfig_omnistore cannot provide collection params without a C++ manager"
            X.C010708t.A06(r1, r0)
            return
        L_0x004d:
            X.0Tq r0 = r14.A05
            java.lang.Object r0 = r0.get()
            com.facebook.auth.viewercontext.ViewerContext r0 = (com.facebook.auth.viewercontext.ViewerContext) r0
            java.lang.String r5 = r0.mUserId
            com.facebook.mobileconfigonomnistore.MobileConfigOmnistoreUpdaterHolder r4 = new com.facebook.mobileconfigonomnistore.MobileConfigOmnistoreUpdaterHolder
            X.1YI r3 = r14.A01
            r1 = 118(0x76, float:1.65E-43)
            r0 = 0
            boolean r6 = r3.AbO(r1, r0)
            X.1YI r3 = r14.A01
            r1 = 123(0x7b, float:1.72E-43)
            boolean r7 = r3.AbO(r1, r0)
            X.1YI r3 = r14.A01
            r1 = 117(0x75, float:1.64E-43)
            boolean r8 = r3.AbO(r1, r0)
            X.1YI r3 = r14.A01
            r1 = 119(0x77, float:1.67E-43)
            boolean r9 = r3.AbO(r1, r0)
            X.1YI r3 = r14.A01
            r1 = 122(0x7a, float:1.71E-43)
            r0 = 1
            boolean r10 = r3.AbO(r1, r0)
            X.0hF r0 = r14.A02
            java.lang.String r11 = r0.A02()
            com.facebook.omnistore.Omnistore r12 = X.C29271g9.A00(r15)
            X.C29271g9.A00(r15)
            com.facebook.omnistore.OmnistoreCollections r13 = r15.A01
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13)
            r14.A00 = r4
            X.0WV r1 = r2.A00()
            com.facebook.mobileconfig.MobileConfigManagerHolderImpl r1 = (com.facebook.mobileconfig.MobileConfigManagerHolderImpl) r1
            com.facebook.mobileconfigonomnistore.MobileConfigOmnistoreUpdaterHolder r0 = r14.A00
            r0.initWithManager(r1)
            return
        L_0x00a3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36801tp.Bgx(X.1g9):void");
    }

    public void Bgy() {
        ((C25031Yb) this.A04.get()).clearAlternativeUpdater();
    }

    private C36801tp(C04310Tq r2, C04310Tq r3, C04310Tq r4, C09400hF r5, AnonymousClass1YI r6) {
        this.A04 = r2;
        this.A03 = r3;
        this.A05 = r4;
        this.A01 = r6;
        this.A02 = r5;
    }
}
