package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.acra.LogCatCollector;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.18h  reason: invalid class name and case insensitive filesystem */
public final class C194718h {
    public static final String[] A06 = {C194818i.A01.A00, C194818i.A05.A00};
    public static final String[] A07 = {C194818i.A00.A00};
    public static final String[] A08 = {C194818i.A03.A00};
    public static final String[] A09 = {C194818i.A0A.A00};
    private static final String[] A0A = {C194818i.A08.A00, C194818i.A02.A00, C194818i.A09.A00};
    private static volatile C194718h A0B;
    public final AnonymousClass06B A00;
    public final C194918j A01;
    public final C04310Tq A02;
    public final C04310Tq A03;
    private final C195018k A04;
    private final C12420pJ A05;

    public static C06140av A00(ThreadKey threadKey) {
        return C06160ax.A03(C194818i.A0A.A00, threadKey.A0J());
    }

    public static C06140av A01(ThreadKey threadKey, String str, String str2) {
        return C06160ax.A01(C06160ax.A03(C194818i.A0A.A00, threadKey.A0J()), C06160ax.A03(C194818i.A01.A00, str), C06160ax.A03(C194818i.A05.A00, str2));
    }

    public static C06140av A02(Long l, String str) {
        return C06160ax.A01(C06160ax.A03(C194818i.A01.A00, Long.toString(l.longValue())), C06160ax.A03(C194818i.A05.A00, str));
    }

    private C06140av A03(C22079Anz anz) {
        C30477ExH A052 = A05(anz.A01.A01);
        return C06160ax.A01(C06160ax.A03(C194818i.A0A.A00, A04(anz).A0J()), C06160ax.A03(C194818i.A01.A00, A052.A01), C06160ax.A03(C194818i.A05.A00, A052.A00));
    }

    private ThreadKey A04(C22079Anz anz) {
        return ThreadKey.A05(Long.parseLong(anz.A00), Long.parseLong((String) this.A02.get()));
    }

    private static C30477ExH A05(String str) {
        int indexOf = str.indexOf(95);
        if (indexOf == -1) {
            return new C30477ExH(str, null);
        }
        return new C30477ExH(str.substring(0, indexOf), str.substring(indexOf + 1));
    }

    public static final C194718h A06(AnonymousClass1XY r11) {
        if (A0B == null) {
            synchronized (C194718h.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0B, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        A0B = new C194718h(AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, applicationInjector), AnonymousClass0WY.A02(applicationInjector), C12420pJ.A00(applicationInjector), AnonymousClass187.A01(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.B6n, applicationInjector), AnonymousClass067.A02(), C194918j.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0B;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void A08(C194718h r6, C06140av r7, boolean z) {
        ContentValues contentValues = new ContentValues();
        contentValues.putNull(C194818i.A02.A00);
        contentValues.put(C194818i.A06.A00, (Integer) 1);
        int update = ((AnonymousClass183) r6.A03.get()).A01().update("thread_devices", contentValues, r7.A02(), r7.A04());
        if (z && update != 1) {
            C010708t.A06(C194718h.class, "No SenderKey was found to delete.");
        }
    }

    public static void A09(C194718h r14, ThreadKey threadKey, List list, int i) {
        SQLiteDatabase A012 = ((AnonymousClass183) r14.A03.get()).A01();
        C007406x.A01(A012, 1232239516);
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C22338Aw9 aw9 = (C22338Aw9) it.next();
                String l = Long.toString(aw9.user_id.longValue());
                ThreadKey threadKey2 = threadKey;
                C06140av A013 = A01(threadKey, l, aw9.instance_id);
                ContentValues contentValues = new ContentValues();
                int i2 = i;
                contentValues.put(C194818i.A06.A00, Integer.valueOf(i2));
                if (A012.update("thread_devices", contentValues, A013.A02(), A013.A04()) != 1) {
                    A07(A012, threadKey2, l, aw9.instance_id, i2, r14.A00.now());
                }
            }
            A012.setTransactionSuccessful();
        } finally {
            C007406x.A02(A012, -35163603);
        }
    }

    public C22107Aog A0C(C22079Anz anz) {
        Class<C194718h> cls = C194718h.class;
        C06140av A032 = A03(anz);
        byte[] bArr = null;
        Cursor query = ((AnonymousClass183) this.A03.get()).A01().query("thread_devices", A0A, A032.A02(), A032.A04(), null, null, null);
        try {
            if (query.moveToNext() && !query.isNull(1)) {
                try {
                    if (query.isNull(0)) {
                        return new C22107Aog(query.getBlob(1));
                    }
                    ThreadKey A042 = A04(anz);
                    if (query.getInt(0) != 1) {
                        C010708t.A05(cls, "Unknown storage format for SenderKey. Erasing.");
                        A08(this, A03(anz), true);
                    } else {
                        int i = -1;
                        if (!C194818i.A09.A06(query)) {
                            i = C194818i.A09.A00(query);
                        }
                        C195018k r0 = this.A04;
                        byte[] blob = query.getBlob(1);
                        if (blob != null) {
                            bArr = r0.A00.A05(C195018k.A00(r0, A042, i), blob);
                        }
                        C22107Aog aog = new C22107Aog(bArr);
                        query.close();
                        return aog;
                    }
                } catch (C37911wY | C37921wZ | IOException e) {
                    C010708t.A08(C195018k.A04, "Failed to decrypt thread information for local storage", e);
                    throw new RuntimeException(e);
                } catch (IOException e2) {
                    C010708t.A08(cls, "Error decoding sender key record", e2);
                }
            }
            query.close();
            return new C22107Aog();
        } finally {
            query.close();
        }
    }

    public void A0E(ThreadKey threadKey, String str) {
        A08(this, A01(threadKey, (String) this.A02.get(), str), true);
    }

    public void A0G(ThreadKey threadKey, List list) {
        SQLiteDatabase A012 = ((AnonymousClass183) this.A03.get()).A01();
        C007406x.A01(A012, 1013611783);
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C22338Aw9 aw9 = (C22338Aw9) it.next();
                A07(A012, threadKey, Long.toString(aw9.user_id.longValue()), aw9.instance_id, 1, this.A00.now());
            }
            A012.setTransactionSuccessful();
        } finally {
            C007406x.A02(A012, -1907959516);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void A0H(C22079Anz anz, C22107Aog aog) {
        int A012;
        C22079Anz anz2 = anz;
        C06140av A032 = A03(anz2);
        SQLiteDatabase A013 = ((AnonymousClass183) this.A03.get()).A01();
        C007406x.A01(A013, -1257303409);
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(C194818i.A08.A00, (Integer) 1);
            ThreadKey A042 = A04(anz2);
            C25601a6 A014 = C06160ax.A01(A03(anz2), C06160ax.A02(new AnonymousClass1CG(C194818i.A07.A00), new AnonymousClass1CG(C194818i.A02.A00)));
            AnonymousClass0W6 r1 = C194818i.A09;
            Cursor query = A013.query("thread_devices", new String[]{r1.A00}, A014.A02(), A014.A04(), null, null, null);
            if (query == null || !query.moveToNext()) {
                A012 = this.A01.A01();
            } else {
                if (!r1.A06(query)) {
                    A012 = r1.A00(query);
                } else {
                    A012 = -1;
                }
                query.close();
            }
            String str = C194818i.A02.A00;
            C195018k r10 = this.A04;
            C22215Aqr aqr = new C22215Aqr();
            Iterator it = aog.A00.iterator();
            while (it.hasNext()) {
                C22218Aqu aqu = ((AnonymousClass20P) it.next()).A00;
                C22218Aqu aqu2 = aqu;
                C22200Aqc aqc = aqr.A01;
                if (aqc != null) {
                    aqc.A08(aqu);
                } else if (aqu != null) {
                    if ((aqr.A00 & 1) != 1) {
                        aqr.A02 = new ArrayList(aqr.A02);
                        aqr.A00 |= 1;
                    }
                    aqr.A02.add(aqu2);
                    aqr.A0W();
                } else {
                    throw new NullPointerException();
                }
            }
            C22216Aqs A002 = C22215Aqr.A00(aqr);
            if (A002.BFQ()) {
                contentValues.put(str, r10.A02(A042, A002.CJ3(), A012));
                contentValues.put(r1.A00, Integer.valueOf(A012));
                int update = A013.update("thread_devices", contentValues, A032.A02(), A032.A04());
                if (update == 0) {
                    C30477ExH A052 = A05(anz2.A01.A01);
                    contentValues.put(C194818i.A0A.A00, A042.A0J());
                    contentValues.put(C194818i.A01.A00, A052.A01);
                    contentValues.put(C194818i.A05.A00, A052.A00);
                    C007406x.A00(-1756915164);
                    A013.insert("thread_devices", null, contentValues);
                    C007406x.A00(-1024472868);
                } else if (update != 1) {
                    C010708t.A0C(C194718h.class, "[TC] New sender key did not cause a unique row update (%d rows updated)", Integer.valueOf(update));
                }
                A013.setTransactionSuccessful();
                return;
            }
            throw As3.A03(A002);
        } finally {
            C007406x.A02(A013, 1694248184);
        }
    }

    public boolean A0J(ThreadKey threadKey) {
        if (!this.A05.A02() || A0A(threadKey).size() == 2) {
            return true;
        }
        return false;
    }

    public byte[] A0K(ThreadKey threadKey) {
        if (!this.A05.A02()) {
            return null;
        }
        ImmutableList A0A2 = A0A(threadKey);
        if (A0A2.isEmpty()) {
            ImmutableList.Builder builder = new ImmutableList.Builder();
            builder.add((Object) new C22338Aw9(Long.valueOf(threadKey.A01), "tincan_dummy_device"));
            A0A2 = builder.build();
        }
        TreeMap treeMap = new TreeMap();
        C24971Xv it = A0A2.iterator();
        while (it.hasNext()) {
            C22338Aw9 aw9 = (C22338Aw9) it.next();
            if (aw9.instance_id == null) {
                C010708t.A0P("ParticipantChecksumCalculator", "Encountered address without device details for %s", aw9.user_id);
            } else {
                Object obj = (List) treeMap.get(aw9.user_id);
                if (obj == null) {
                    obj = new ArrayList();
                    treeMap.put(aw9.user_id, obj);
                }
                obj.add(aw9.instance_id);
            }
        }
        for (List sort : treeMap.values()) {
            Collections.sort(sort);
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : treeMap.entrySet()) {
            sb.append(entry.getKey());
            sb.append(':');
            for (String append : (List) entry.getValue()) {
                sb.append(append);
                sb.append(',');
            }
            int length = sb.length() - 1;
            if (length >= 0 && sb.charAt(length) == ',') {
                sb.deleteCharAt(length);
            }
            sb.append(';');
        }
        int length2 = sb.length() - 1;
        if (length2 >= 0 && sb.charAt(length2) == ';') {
            sb.deleteCharAt(length2);
        }
        String sb2 = sb.toString();
        MessageDigest instance = MessageDigest.getInstance("SHA-256");
        instance.reset();
        return instance.digest(sb2.getBytes(LogCatCollector.UTF_8_ENCODING));
    }

    private C194718h(C04310Tq r2, C04310Tq r3, C12420pJ r4, AnonymousClass187 r5, C04310Tq r6, AnonymousClass06B r7, C194918j r8) {
        this.A03 = r2;
        this.A02 = r3;
        this.A05 = r4;
        this.A01 = r8;
        this.A04 = new C195018k(r5, r6, r2, r8);
        this.A00 = r7;
    }

    public static void A07(SQLiteDatabase sQLiteDatabase, ThreadKey threadKey, String str, String str2, int i, long j) {
        C06140av A012 = A01(threadKey, str, str2);
        sQLiteDatabase.delete("thread_devices", A012.A02(), A012.A04());
        ContentValues contentValues = new ContentValues();
        contentValues.put(C194818i.A0A.A00, threadKey.A0J());
        contentValues.put(C194818i.A01.A00, str);
        contentValues.put(C194818i.A05.A00, str2);
        contentValues.put(C194818i.A06.A00, Integer.valueOf(i));
        contentValues.put(C194818i.A00.A00, Long.valueOf(j));
        C007406x.A00(659709117);
        sQLiteDatabase.insert("thread_devices", null, contentValues);
        C007406x.A00(-1428209703);
    }

    public ImmutableList A0A(ThreadKey threadKey) {
        C06140av A002 = A00(threadKey);
        Cursor query = ((AnonymousClass183) this.A03.get()).A01().query("thread_devices", A06, A002.A02(), A002.A04(), null, null, null);
        try {
            ImmutableList.Builder builder = new ImmutableList.Builder();
            while (query.moveToNext()) {
                builder.add((Object) new C22338Aw9(Long.valueOf(query.getLong(0)), query.getString(1)));
            }
            return builder.build();
        } finally {
            query.close();
        }
    }

    public ImmutableList A0B(ThreadKey threadKey) {
        C25601a6 A012 = C06160ax.A01(A00(threadKey), C06160ax.A03(C194818i.A06.A00, Integer.toString(1)));
        Cursor query = ((AnonymousClass183) this.A03.get()).A01().query("thread_devices", A06, A012.A02(), A012.A04(), null, null, null);
        try {
            ImmutableList.Builder builder = new ImmutableList.Builder();
            while (query.moveToNext()) {
                builder.add((Object) new C22338Aw9(Long.valueOf(query.getLong(0)), query.getString(1)));
            }
            return builder.build();
        } finally {
            query.close();
        }
    }

    public void A0D(ThreadKey threadKey) {
        C06140av A002 = A00(threadKey);
        ((AnonymousClass183) this.A03.get()).A01().delete("thread_devices", A002.A02(), A002.A04());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void A0F(ThreadKey threadKey, String str) {
        C06140av A002 = A00(threadKey);
        ContentValues contentValues = new ContentValues();
        AnonymousClass0W6 r6 = C194818i.A06;
        contentValues.put(r6.A00, (Integer) 1);
        SQLiteDatabase A012 = ((AnonymousClass183) this.A03.get()).A01();
        A012.update("thread_devices", contentValues, A002.A02(), A002.A04());
        C06140av A013 = A01(threadKey, (String) this.A02.get(), str);
        ContentValues contentValues2 = new ContentValues();
        contentValues2.put(r6.A00, (Integer) 4);
        A012.update("thread_devices", contentValues2, A013.A02(), A013.A04());
        new C22034AnA(this).A00(C22047AnO.A00(threadKey, Long.parseLong((String) this.A02.get()), str));
    }

    public boolean A0I(ThreadKey threadKey) {
        C25601a6 A012 = C06160ax.A01(A00(threadKey), C06160ax.A03(C194818i.A06.A00, Long.toString(1)));
        Cursor query = ((AnonymousClass183) this.A03.get()).A01().query("thread_devices", A09, A012.A02(), A012.A04(), null, null, null, "1");
        try {
            return query.moveToNext();
        } finally {
            query.close();
        }
    }
}
