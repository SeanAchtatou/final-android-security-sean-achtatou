package X;

/* renamed from: X.21O  reason: invalid class name */
public final class AnonymousClass21O implements AnonymousClass04e {
    public final /* synthetic */ AnonymousClass09P A00;

    public AnonymousClass21O(AnonymousClass09P r1) {
        this.A00 = r1;
    }

    public void C2S(String str) {
        this.A00.CGS("SecureContextHelperDI", str);
    }

    public void C2T(String str, String str2, Throwable th) {
        this.A00.softReport("SecureContextHelperDI", str2, th);
    }
}
