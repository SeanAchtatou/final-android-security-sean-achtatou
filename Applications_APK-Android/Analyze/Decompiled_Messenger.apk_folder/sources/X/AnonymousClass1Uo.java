package X;

import android.content.Context;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.facebook.rtc.datasource.RtcMessengerCallStatusManager;
import com.google.common.collect.ImmutableList;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Uo  reason: invalid class name */
public final class AnonymousClass1Uo implements OmnistoreComponent {
    private static final Class A07 = AnonymousClass1Uo.class;
    private static volatile AnonymousClass1Uo A08;
    public HashMap A00 = new HashMap();
    public Set A01 = new HashSet();
    public TreeMap A02 = new TreeMap(new AnonymousClass1Up());
    private boolean A03;
    public final C001300x A04;
    private final Context A05;
    private final C04310Tq A06;

    public String getCollectionLabel() {
        return "messenger_calls";
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
        this.A03 = true;
    }

    public static final AnonymousClass1Uo A00(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass1Uo.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new AnonymousClass1Uo(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:19|20|21|22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0037, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x003b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0044 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String A01(java.lang.String r7) {
        /*
            r6 = this;
            android.content.Context r0 = r6.A05     // Catch:{ IOException -> 0x0045 }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ IOException -> 0x0045 }
            java.io.InputStream r5 = r0.open(r7)     // Catch:{ IOException -> 0x0045 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x003c }
            java.nio.charset.Charset r0 = java.nio.charset.Charset.defaultCharset()     // Catch:{ all -> 0x003c }
            r4.<init>(r5, r0)     // Catch:{ all -> 0x003c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
            r3.<init>()     // Catch:{ all -> 0x0035 }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r2 = new char[r0]     // Catch:{ all -> 0x0035 }
        L_0x001c:
            int r1 = r4.read(r2)     // Catch:{ all -> 0x0035 }
            r0 = -1
            if (r1 == r0) goto L_0x0028
            r0 = 0
            r3.append(r2, r0, r1)     // Catch:{ all -> 0x0035 }
            goto L_0x001c
        L_0x0028:
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0035 }
            r4.close()     // Catch:{ all -> 0x003c }
            if (r5 == 0) goto L_0x0034
            r5.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0034:
            return r0
        L_0x0035:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x003b }
        L_0x003b:
            throw r0     // Catch:{ all -> 0x003c }
        L_0x003c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003e }
        L_0x003e:
            r0 = move-exception
            if (r5 == 0) goto L_0x0044
            r5.close()     // Catch:{ all -> 0x0044 }
        L_0x0044:
            throw r0     // Catch:{ IOException -> 0x0045 }
        L_0x0045:
            java.lang.String r0 = ""
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Uo.A01(java.lang.String):java.lang.String");
    }

    private void A02() {
        C163907iB r0;
        for (C163307h0 r4 : this.A01) {
            ImmutableList copyOf = ImmutableList.copyOf(this.A00.values());
            if (!r4.A00.A01.equals(copyOf)) {
                RtcMessengerCallStatusManager rtcMessengerCallStatusManager = r4.A00;
                rtcMessengerCallStatusManager.A01 = copyOf;
                AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A6Z, rtcMessengerCallStatusManager.A00), new C163277gw(rtcMessengerCallStatusManager), 1287044827);
                RtcMessengerCallStatusManager rtcMessengerCallStatusManager2 = r4.A00;
                int i = AnonymousClass1Y3.BTr;
                C14260sv r1 = (C14260sv) AnonymousClass1XX.A02(1, i, rtcMessengerCallStatusManager2.A00);
                if (!r1.A0u() && (r0 = r1.A0C) != null) {
                    String ATl = r0.ATl();
                    String str = null;
                    ThreadKey threadKey = ((C14260sv) AnonymousClass1XX.A02(1, i, rtcMessengerCallStatusManager2.A00)).A0A;
                    if (threadKey != null) {
                        str = String.valueOf(threadKey.A0G());
                    }
                    C24971Xv it = rtcMessengerCallStatusManager2.A01.iterator();
                    while (it.hasNext()) {
                        AnonymousClass32q r12 = (AnonymousClass32q) it.next();
                        if (ATl.equals(r12.A07()) && !C06850cB.A0B(r12.A08()) && !r12.A08().equals(str)) {
                            ((C14260sv) AnonymousClass1XX.A02(1, i, rtcMessengerCallStatusManager2.A00)).A0H(ThreadKey.A00(Long.parseLong(r12.A08())));
                        }
                    }
                }
            }
        }
        for (C163307h0 r3 : this.A01) {
            ImmutableList copyOf2 = ImmutableList.copyOf(this.A02.values());
            r3.A00.A02 = ImmutableList.copyOf((Collection) copyOf2);
            ImmutableList A012 = RtcMessengerCallStatusManager.A01(copyOf2);
            if (!r3.A00.A03.equals(A012)) {
                RtcMessengerCallStatusManager rtcMessengerCallStatusManager3 = r3.A00;
                rtcMessengerCallStatusManager3.A03 = A012;
                AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A6Z, rtcMessengerCallStatusManager3.A00), new C163297gz(rtcMessengerCallStatusManager3), -1452802641);
            }
        }
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        return new IndexedFields();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        if (r1 != null) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0026 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCollectionAvailable(com.facebook.omnistore.Collection r5) {
        /*
            r4 = this;
            java.lang.String r2 = "9999999999999999"
            r1 = -1
            r0 = 2
            com.facebook.omnistore.Cursor r1 = r5.query(r2, r1, r0)     // Catch:{ OmnistoreIOException -> 0x0027 }
        L_0x0008:
            boolean r0 = r1.step()     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x001a
            java.nio.ByteBuffer r0 = r1.getBlob()     // Catch:{ all -> 0x001e }
            X.32q r0 = X.AnonymousClass32q.A00(r0)     // Catch:{ all -> 0x001e }
            r4.A03(r0)     // Catch:{ all -> 0x001e }
            goto L_0x0008
        L_0x001a:
            r1.close()     // Catch:{ OmnistoreIOException -> 0x0027 }
            goto L_0x0032
        L_0x001e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0020 }
        L_0x0020:
            r0 = move-exception
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ all -> 0x0026 }
        L_0x0026:
            throw r0     // Catch:{ OmnistoreIOException -> 0x0027 }
        L_0x0027:
            r3 = move-exception
            java.lang.Class r2 = X.AnonymousClass1Uo.A07
            r0 = 0
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r0 = "IO Error while updating call list"
            X.C010708t.A0E(r2, r3, r0, r1)
        L_0x0032:
            r4.A02()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Uo.onCollectionAvailable(com.facebook.omnistore.Collection):void");
    }

    public void onCollectionInvalidated() {
        if (!this.A00.isEmpty() || !this.A02.isEmpty()) {
            this.A00.clear();
            this.A02.clear();
            A02();
        }
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
        this.A00.size();
        this.A03 = false;
    }

    public C32171lK provideSubscriptionInfo(Omnistore omnistore) {
        boolean z = false;
        if (this.A04.A02 == C001500z.A07) {
            z = true;
        }
        if (!z) {
            return C32171lK.A03;
        }
        CollectionName.Builder createCollectionNameWithDomainBuilder = omnistore.createCollectionNameWithDomainBuilder(getCollectionLabel(), "messenger_user_sq");
        createCollectionNameWithDomainBuilder.addSegment((String) this.A06.get());
        CollectionName build = createCollectionNameWithDomainBuilder.build();
        C32151lI r1 = new C32151lI();
        r1.A03 = A01("MessengerCallSchema.fbs");
        r1.A04 = A01("MessengerCallSchema.idna");
        return C32171lK.A00(build, new C32161lJ(r1));
    }

    private AnonymousClass1Uo(AnonymousClass1XY r3) {
        this.A06 = AnonymousClass0XJ.A0L(r3);
        this.A05 = AnonymousClass1YA.A00(r3);
        this.A04 = AnonymousClass0UU.A02(r3);
    }

    private void A03(AnonymousClass32q r6) {
        byte b;
        byte b2;
        Long valueOf = Long.valueOf(r6.A06());
        int A022 = r6.A02(12);
        if (A022 != 0) {
            b = r6.A01.get(A022 + r6.A00);
        } else {
            b = 0;
        }
        if (b == 1) {
            this.A00.put(valueOf, r6);
            this.A02.remove(valueOf);
            return;
        }
        int A023 = r6.A02(12);
        if (A023 != 0) {
            b2 = r6.A01.get(A023 + r6.A00);
        } else {
            b2 = 0;
        }
        if (b2 != 1) {
            this.A00.remove(valueOf);
            this.A02.put(valueOf, r6);
        }
    }

    public void BVs(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass32q A002 = AnonymousClass32q.A00(((Delta) it.next()).getBlob());
            A03(A002);
            for (C163307h0 r0 : this.A01) {
                A002.A07();
                RtcMessengerCallStatusManager rtcMessengerCallStatusManager = r0.A00;
                AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A6Z, rtcMessengerCallStatusManager.A00), new C172397xD(rtcMessengerCallStatusManager, A002), 722513259);
            }
        }
        A02();
        if (!this.A03) {
            this.A00.size();
        }
    }

    public void Boz(int i) {
    }
}
