package X;

import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

@InjectorModule
/* renamed from: X.0zw  reason: invalid class name and case insensitive filesystem */
public final class C18020zw extends AnonymousClass0UV {
    public static final Boolean A02(AnonymousClass1XY r2) {
        return Boolean.valueOf(new C18040zy(r2, AnonymousClass0WA.A00(r2)).A01());
    }

    public static final Boolean A00(AnonymousClass1XY r3) {
        FbSharedPreferences A00 = FbSharedPreferencesModule.A00(r3);
        boolean z = true;
        if (!((Boolean) AnonymousClass0VG.A00(AnonymousClass1Y3.ATA, r3).get()).booleanValue() || !A00.Aep(C13730ry.A08, true)) {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    public static final Boolean A01(AnonymousClass1XY r2) {
        return Boolean.valueOf(FbSharedPreferencesModule.A00(r2).Aep(C13730ry.A04, false));
    }

    public static final Boolean A03(AnonymousClass1XY r3) {
        return Boolean.valueOf(FbSharedPreferencesModule.A00(r3).Aep(C13730ry.A0A, ((Boolean) AnonymousClass0VG.A00(AnonymousClass1Y3.AUX, r3).get()).booleanValue()));
    }
}
