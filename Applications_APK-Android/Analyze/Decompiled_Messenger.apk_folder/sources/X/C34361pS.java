package X;

/* renamed from: X.1pS  reason: invalid class name and case insensitive filesystem */
public final class C34361pS implements AnonymousClass0AU {
    private final C001300x A00;
    private final C25921ac A01;
    private final String A02 = "Orca-Android";

    public String AkN() {
        return null;
    }

    public void BIb() {
    }

    public boolean CKB(AnonymousClass0C5 r2) {
        return false;
    }

    public static final C34361pS A00(AnonymousClass1XY r1) {
        return new C34361pS(r1);
    }

    public String AdF() {
        return this.A00.A04;
    }

    public String AdI() {
        return this.A02;
    }

    public String getDeviceId() {
        return this.A01.B7Z();
    }

    public C34361pS(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0UU.A02(r2);
        this.A01 = C25901aa.A00(r2);
    }
}
