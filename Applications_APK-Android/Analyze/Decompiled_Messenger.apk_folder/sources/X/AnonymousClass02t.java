package X;

import android.util.Log;

/* renamed from: X.02t  reason: invalid class name */
public final class AnonymousClass02t extends AnonymousClass02u {
    public static final AnonymousClass02t A00 = new AnonymousClass02t();

    public void CNm(String str, String str2) {
        Log.e(str, str2);
    }

    public void CNn(String str, String str2, Throwable th) {
        Log.e(str, str2, th);
    }
}
