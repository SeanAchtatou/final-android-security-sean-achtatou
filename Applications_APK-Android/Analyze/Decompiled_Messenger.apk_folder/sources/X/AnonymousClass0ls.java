package X;

import android.os.Parcelable;
import com.google.common.base.Preconditions;

/* renamed from: X.0ls  reason: invalid class name */
public final class AnonymousClass0ls {
    public static Parcelable A00(C11060ln r0, String str) {
        Parcelable parcelable = r0.A00.getParcelable(str);
        Preconditions.checkNotNull(parcelable);
        return parcelable;
    }
}
