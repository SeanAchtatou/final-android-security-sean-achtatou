package X;

/* renamed from: X.0G0  reason: invalid class name */
public final class AnonymousClass0G0 extends AnonymousClass0FM {
    public long javaHeapAllocatedKb;
    public long javaHeapMaxSizeKb;
    public long nativeHeapAllocatedKb;
    public long nativeHeapSizeKb;
    public long sequenceNumber;
    public long vmRssKb;
    public long vmSizeKb;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass0G0 r7 = (AnonymousClass0G0) obj;
            if (!(this.javaHeapMaxSizeKb == r7.javaHeapMaxSizeKb && this.javaHeapAllocatedKb == r7.javaHeapAllocatedKb && this.nativeHeapSizeKb == r7.nativeHeapSizeKb && this.nativeHeapAllocatedKb == r7.nativeHeapAllocatedKb && this.vmSizeKb == r7.vmSizeKb && this.vmRssKb == r7.vmRssKb)) {
                return false;
            }
        }
        return true;
    }

    private void A00(AnonymousClass0G0 r3) {
        this.javaHeapMaxSizeKb = r3.javaHeapMaxSizeKb;
        this.javaHeapAllocatedKb = r3.javaHeapAllocatedKb;
        this.nativeHeapSizeKb = r3.nativeHeapSizeKb;
        this.nativeHeapAllocatedKb = r3.nativeHeapAllocatedKb;
        this.vmSizeKb = r3.vmSizeKb;
        this.vmRssKb = r3.vmRssKb;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A00((AnonymousClass0G0) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r6, AnonymousClass0FM r7) {
        AnonymousClass0G0 r62 = (AnonymousClass0G0) r6;
        AnonymousClass0G0 r72 = (AnonymousClass0G0) r7;
        if (r72 == null) {
            r72 = new AnonymousClass0G0();
        }
        if (r62 == null) {
            r72.A00(this);
            return r72;
        }
        if (this.sequenceNumber >= r62.sequenceNumber) {
            r62 = this;
        }
        r72.sequenceNumber = r62.sequenceNumber;
        r72.javaHeapMaxSizeKb = r62.javaHeapMaxSizeKb;
        r72.javaHeapAllocatedKb = r62.javaHeapAllocatedKb;
        r72.nativeHeapSizeKb = r62.nativeHeapSizeKb;
        r72.nativeHeapAllocatedKb = r62.nativeHeapAllocatedKb;
        r72.vmSizeKb = r62.vmSizeKb;
        r72.vmRssKb = r62.vmRssKb;
        return r72;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r6, AnonymousClass0FM r7) {
        AnonymousClass0G0 r62 = (AnonymousClass0G0) r6;
        AnonymousClass0G0 r72 = (AnonymousClass0G0) r7;
        if (r72 == null) {
            r72 = new AnonymousClass0G0();
        }
        if (r62 == null) {
            r72.A00(this);
            return r72;
        }
        if (this.sequenceNumber > r62.sequenceNumber) {
            r62 = this;
        }
        r72.sequenceNumber = r62.sequenceNumber;
        r72.javaHeapMaxSizeKb = r62.javaHeapMaxSizeKb;
        r72.javaHeapAllocatedKb = r62.javaHeapAllocatedKb;
        r72.nativeHeapSizeKb = r62.nativeHeapSizeKb;
        r72.nativeHeapAllocatedKb = r62.nativeHeapAllocatedKb;
        r72.vmSizeKb = r62.vmSizeKb;
        r72.vmRssKb = r62.vmRssKb;
        return r72;
    }

    public int hashCode() {
        long j = this.javaHeapMaxSizeKb;
        long j2 = this.javaHeapAllocatedKb;
        long j3 = this.nativeHeapSizeKb;
        long j4 = this.nativeHeapAllocatedKb;
        long j5 = this.vmSizeKb;
        long j6 = this.vmRssKb;
        return (((((((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)));
    }

    public String toString() {
        return "MemoryMetrics{javaHeapMaxSizeKb=" + this.javaHeapMaxSizeKb + ", javaHeapAllocatedKb=" + this.javaHeapAllocatedKb + ", nativeHeapSizeKb=" + this.nativeHeapSizeKb + ", nativeHeapAllocatedKb=" + this.nativeHeapAllocatedKb + ", vmSizeKb=" + this.vmSizeKb + ", vmRssKb=" + this.vmRssKb + "}";
    }
}
