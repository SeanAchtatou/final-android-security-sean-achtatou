package X;

/* renamed from: X.0Gl  reason: invalid class name and case insensitive filesystem */
public final class C02800Gl implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        AnonymousClass0FV r62 = (AnonymousClass0FV) r6;
        double d = r62.userTimeS;
        if (d != 0.0d) {
            r7.AMT("cpu_user_time_s", d);
        }
        double d2 = r62.systemTimeS;
        if (d2 != 0.0d) {
            r7.AMT("cpu_system_time_s", d2);
        }
        double d3 = r62.childUserTimeS;
        if (d3 != 0.0d) {
            r7.AMT("child_cpu_user_time_s", d3);
        }
        double d4 = r62.childSystemTimeS;
        if (d4 != 0.0d) {
            r7.AMT("child_cpu_system_time_s", d4);
        }
    }
}
