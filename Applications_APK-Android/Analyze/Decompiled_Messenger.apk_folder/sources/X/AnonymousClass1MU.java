package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1MU  reason: invalid class name */
public final class AnonymousClass1MU extends AnonymousClass0UV {
    private static volatile C22001Jn A00;
    private static volatile AnonymousClass1MV A01;

    public static final C22001Jn A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C22001Jn.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C22011Jo();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final AnonymousClass1MV A01(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass1MV.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new AnonymousClass1MW();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
