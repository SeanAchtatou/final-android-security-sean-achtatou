package X;

import android.os.Bundle;
import android.os.Messenger;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

/* renamed from: X.0au  reason: invalid class name and case insensitive filesystem */
public final class C06130au {
    public Messenger A00;
    public final int A01;
    public final AnonymousClass00M A02;

    public static C06130au A00(Bundle bundle) {
        Messenger messenger = (Messenger) bundle.getParcelable("key_messenger");
        Preconditions.checkNotNull(messenger, "The messenger is not in the bundle passed in");
        int i = bundle.getInt("key_pid", -1);
        if (i != -1) {
            String string = bundle.getString("key_process_name");
            Preconditions.checkNotNull(string, "The process name is not in the bundle passed in");
            return new C06130au(messenger, i, AnonymousClass00M.A01(string));
        }
        throw new IllegalArgumentException("The pid is not in the bundle passed in");
    }

    public Bundle A01() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("key_messenger", this.A00);
        bundle.putInt("key_pid", this.A01);
        bundle.putString("key_process_name", this.A02.A01);
        return bundle;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C06130au) || this.A01 != ((C06130au) obj).A01) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A01;
    }

    public C06130au(Messenger messenger, int i, AnonymousClass00M r3) {
        Preconditions.checkNotNull(r3);
        this.A00 = messenger;
        this.A01 = i;
        this.A02 = r3;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("messenger: ", this.A00);
        stringHelper.add("pid: ", this.A01);
        stringHelper.add("process name:", this.A02);
        return stringHelper.toString();
    }
}
