package X;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1lR  reason: invalid class name and case insensitive filesystem */
public final class C32241lR implements AnonymousClass1LY {
    private static volatile C32241lR A01;
    public final List A00 = new ArrayList(20);

    public synchronized void BgD(String str, String str2, Map map) {
        this.A00.add(str2);
        while (this.A00.size() > 10) {
            this.A00.remove(0);
        }
    }

    public static final C32241lR A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C32241lR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C32241lR();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
