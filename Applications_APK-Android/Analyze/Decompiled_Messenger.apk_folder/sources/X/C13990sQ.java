package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0sQ  reason: invalid class name and case insensitive filesystem */
public final class C13990sQ {
    public boolean A00;
    public boolean A01;
    public boolean A02 = true;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public final C13460rT A06;
    private final C13970sO A07;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r3.A02 == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.C13990sQ r3) {
        /*
            boolean r0 = r3.A01
            if (r0 != 0) goto L_0x0015
            boolean r0 = r3.A04
            if (r0 == 0) goto L_0x0015
            boolean r0 = r3.A03
            if (r0 == 0) goto L_0x0015
            boolean r0 = r3.A00
            if (r0 == 0) goto L_0x0015
            boolean r0 = r3.A02
            r1 = 1
            if (r0 != 0) goto L_0x0016
        L_0x0015:
            r1 = 0
        L_0x0016:
            boolean r0 = r3.A05
            if (r1 == r0) goto L_0x004f
            r3.A05 = r1
            X.0sO r0 = r3.A07
            if (r0 == 0) goto L_0x0025
            if (r1 == 0) goto L_0x004b
            r0.BZi()
        L_0x0025:
            X.0rT r0 = r3.A06
            X.0qW r0 = r0.A17()
            java.util.List r0 = r0.A0U()
            java.util.Iterator r2 = r0.iterator()
        L_0x0033:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x004f
            java.lang.Object r1 = r2.next()
            androidx.fragment.app.Fragment r1 = (androidx.fragment.app.Fragment) r1
            boolean r0 = r1 instanceof X.C13250qz
            if (r0 == 0) goto L_0x0033
            X.0qz r1 = (X.C13250qz) r1
            boolean r0 = r3.A05
            r1.CAE(r0)
            goto L_0x0033
        L_0x004b:
            r0.BZh()
            goto L_0x0025
        L_0x004f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13990sQ.A00(X.0sQ):void");
    }

    public C13990sQ(C13460rT r3, C13970sO r4) {
        this.A07 = r4;
        Preconditions.checkNotNull(r3);
        this.A06 = r3;
        this.A01 = r3.A0b;
        this.A04 = r3.A0i;
        this.A00 = r3.A0I != null;
        r3.A2N(new AnonymousClass1JD(this));
    }
}
