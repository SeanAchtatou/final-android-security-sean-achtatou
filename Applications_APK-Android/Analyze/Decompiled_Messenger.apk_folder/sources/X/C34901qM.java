package X;

/* renamed from: X.1qM  reason: invalid class name and case insensitive filesystem */
public final class C34901qM extends C33541nk {
    public AnonymousClass0UN A00;
    private final C34951qR A01 = new C34931qP(this);
    private final C16960y6 A02 = new C34911qN(this);
    private final C17040yE A03 = new C34921qO(this);

    private C34901qM(AnonymousClass1XY r3) {
        super("LegacyCallStatusBarPresenter");
        this.A00 = new AnonymousClass0UN(6, r3);
    }

    public static final C34901qM A00(AnonymousClass1XY r1) {
        return new C34901qM(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x003b, code lost:
        if (((X.C34991qV) X.AnonymousClass1XX.A02(5, X.AnonymousClass1Y3.BEY, r2)).A02() == false) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C34901qM r5) {
        /*
            int r0 = X.AnonymousClass1Y3.AME
            X.0UN r2 = r5.A00
            r1 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.0sv r0 = (X.C14260sv) r0
            int r0 = r0.A04()
            r4 = 1
            if (r0 == 0) goto L_0x0080
            int r0 = X.AnonymousClass1Y3.A0a
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r0, r2)
            X.7az r0 = (X.C159867az) r0
            boolean r0 = r0.A1U()
            if (r0 != 0) goto L_0x0080
            int r0 = X.AnonymousClass1Y3.AME
            X.0UN r2 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.0sv r0 = (X.C14260sv) r0
            boolean r0 = r0.A0Q
            if (r0 != 0) goto L_0x0080
            r1 = 5
            int r0 = X.AnonymousClass1Y3.BEY
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.1qV r0 = (X.C34991qV) r0
            boolean r0 = r0.A02()
            if (r0 != 0) goto L_0x0080
        L_0x003d:
            com.google.common.base.Optional r0 = r5.A0G()
            boolean r0 = r0.isPresent()
            if (r0 == 0) goto L_0x007a
            com.google.common.base.Optional r0 = r5.A0G()
            java.lang.Object r0 = r0.get()
            X.1qa r0 = (X.C35041qa) r0
            X.1qZ r3 = new X.1qZ
            r3.<init>(r0)
        L_0x0056:
            r3.A01 = r4
            if (r4 == 0) goto L_0x0071
            r2 = 2
            int r1 = X.AnonymousClass1Y3.Aq3
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.7fh r1 = (X.C162637fh) r1
            r0 = 1
            java.lang.String r1 = r1.A03(r0)
            r3.A00 = r1
            java.lang.String r0 = "text"
            X.C28931fb.A06(r1, r0)
        L_0x0071:
            X.1qa r0 = new X.1qa
            r0.<init>(r3)
            r5.A0K(r0)
            return
        L_0x007a:
            X.1qZ r3 = new X.1qZ
            r3.<init>()
            goto L_0x0056
        L_0x0080:
            r4 = 0
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34901qM.A01(X.1qM):void");
    }

    public void A0L() {
        ((AnonymousClass12S) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ApR, this.A00)).A02(this.A02);
        ((C32621m3) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ayw, this.A00)).A0L(this.A03);
        ((C34991qV) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BEY, this.A00)).A01.remove(this.A01);
    }

    public void A0M() {
        ((AnonymousClass12S) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ApR, this.A00)).A01(this.A02);
        ((C32621m3) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ayw, this.A00)).A0K(this.A03);
        ((C34991qV) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BEY, this.A00)).A01.add(this.A01);
        A01(this);
    }
}
