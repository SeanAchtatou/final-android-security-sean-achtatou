package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.22Q  reason: invalid class name */
public final class AnonymousClass22Q implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass22O A00;

    public AnonymousClass22Q(AnonymousClass22O r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(-75132248);
        AnonymousClass22O r1 = this.A00;
        if (r1.A02.A02) {
            r1.BUD();
        } else {
            AnonymousClass22O.A01(r1);
            AnonymousClass22O.A02(this.A00, C05360Yq.$const$string(111));
        }
        AnonymousClass09Y.A01(1574764983, A002);
    }
}
