package X;

/* renamed from: X.0VS  reason: invalid class name */
public enum AnonymousClass0VS {
    BACKGROUND('B', AnonymousClass0V7.BACKGROUND),
    NORMAL('N', AnonymousClass0V7.NORMAL),
    FOREGROUND('F', AnonymousClass0V7.FOREGROUND),
    IMPORTANT('O', AnonymousClass0V7.IMPORTANT),
    URGENT('U', AnonymousClass0V7.URGENT),
    SUPER_HIGH('S', 0),
    BLOCKING_UI('X', AnonymousClass0V7.BLOCKING_UI);
    
    public final int mAndroidThreadPriority;
    public final AnonymousClass0V7 mThreadPriority;
    public final char mToken;

    public static AnonymousClass0VS A00(int i) {
        for (AnonymousClass0VS r2 : values()) {
            int i2 = r2.mAndroidThreadPriority;
            if (i2 != Integer.MIN_VALUE && i2 == i) {
                return r2;
            }
        }
        AnonymousClass0V7 A002 = AnonymousClass0V7.A00(i);
        if (A002.mAndroidThreadPriority != i) {
            C010708t.A0O("Priority", "Unknown androidThreadPriority:%d.  Mapped to %s", Integer.valueOf(i), A002);
        }
        return A01(A002);
    }

    public static AnonymousClass0VS A01(AnonymousClass0V7 r5) {
        for (AnonymousClass0VS r1 : values()) {
            AnonymousClass0V7 r0 = r1.mThreadPriority;
            if (r0 != null && r0 == r5) {
                return r1;
            }
        }
        switch (r5.ordinal()) {
            case 0:
                return URGENT;
            case 1:
                return BLOCKING_UI;
            default:
                C010708t.A0O("Priority", "Unknown threadPriority %s", r5);
                return BACKGROUND;
        }
    }

    private AnonymousClass0VS(char c, int i) {
        this.mToken = c;
        this.mThreadPriority = null;
        this.mAndroidThreadPriority = i;
    }

    private AnonymousClass0VS(char c, AnonymousClass0V7 r5) {
        this.mToken = c;
        this.mThreadPriority = r5;
        this.mAndroidThreadPriority = Integer.MIN_VALUE;
    }
}
