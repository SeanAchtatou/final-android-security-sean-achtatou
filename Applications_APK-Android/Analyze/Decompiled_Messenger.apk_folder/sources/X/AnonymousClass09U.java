package X;

import android.content.ContentResolver;
import android.content.IntentFilter;
import io.card.payment.BuildConfig;

/* renamed from: X.09U  reason: invalid class name */
public final class AnonymousClass09U {
    public ContentResolver A00;
    public final IntentFilter A01;
    public final String A02;
    public final boolean A03;

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0099, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x009a, code lost:
        android.util.Log.e("IntentCriteria", "Error parsing switch-off criteria.", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a4, code lost:
        return new X.AnonymousClass09U[0];
     */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005c A[Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099, IOException | IllegalArgumentException -> 0x0099 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005e A[Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099, IOException | IllegalArgumentException -> 0x0099 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0099 A[ExcHandler: IOException | IllegalArgumentException (r2v0 'e' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass09U[] A00(java.lang.String r11, android.content.Context r12) {
        /*
            android.content.ContentResolver r9 = r12.getContentResolver()     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            boolean r0 = android.text.TextUtils.isEmpty(r11)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            r8 = 0
            if (r0 == 0) goto L_0x000e
            X.09U[] r10 = new X.AnonymousClass09U[r8]     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            return r10
        L_0x000e:
            java.lang.String r0 = "\\^\\^\\^"
            java.lang.String[] r7 = r11.split(r0)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            int r6 = r7.length     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            X.09U[] r10 = new X.AnonymousClass09U[r6]     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
        L_0x0017:
            if (r8 >= r6) goto L_0x0098
            r12 = r7[r8]     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            boolean r0 = android.text.TextUtils.isEmpty(r12)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            if (r0 == 0) goto L_0x0029
            X.09U r0 = new X.09U     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            r0.<init>()     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
        L_0x0026:
            r10[r8] = r0     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            goto L_0x0080
        L_0x0029:
            r5 = 0
            int r11 = r12.codePointAt(r5)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            java.lang.String r3 = "Criteria specification is not valid"
            r2 = 33
            r1 = 1
            if (r11 == r2) goto L_0x003e
            r0 = 42
            if (r11 == r0) goto L_0x0051
            r0 = 58
            if (r11 == r0) goto L_0x003e
            goto L_0x0083
        L_0x003e:
            int r0 = r12.indexOf(r11, r1)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            if (r0 < 0) goto L_0x0092
            java.lang.String r4 = r12.substring(r1, r0)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            int r0 = r0 + r1
            java.lang.String r3 = r12.substring(r0)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            if (r11 != r2) goto L_0x0056
            r5 = 1
            goto L_0x0056
        L_0x0051:
            r4 = 0
            java.lang.String r3 = r12.substring(r1)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
        L_0x0056:
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            if (r0 == 0) goto L_0x005e
            r2 = 0
            goto L_0x007a
        L_0x005e:
            android.content.IntentFilter r2 = new android.content.IntentFilter     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            r2.<init>()     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            org.xmlpull.v1.XmlPullParserFactory r1 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099 }
            r0 = 1
            r1.setNamespaceAware(r0)     // Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099 }
            org.xmlpull.v1.XmlPullParser r1 = r1.newPullParser()     // Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099 }
            java.io.StringReader r0 = new java.io.StringReader     // Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099 }
            r0.<init>(r3)     // Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099 }
            r1.setInput(r0)     // Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099 }
            r2.readFromXml(r1)     // Catch:{ XmlPullParserException -> 0x0089, IOException | IllegalArgumentException -> 0x0099 }
        L_0x007a:
            X.09U r0 = new X.09U     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            r0.<init>(r9, r4, r5, r2)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            goto L_0x0026
        L_0x0080:
            int r8 = r8 + 1
            goto L_0x0017
        L_0x0083:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            r0.<init>(r3)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            goto L_0x0097
        L_0x0089:
            r2 = move-exception
            java.io.IOException r1 = new java.io.IOException     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            java.lang.String r0 = "Something went wrong with the parser"
            r1.<init>(r0, r2)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            throw r1     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
        L_0x0092:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
            r0.<init>(r3)     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
        L_0x0097:
            throw r0     // Catch:{ IOException | IllegalArgumentException -> 0x0099 }
        L_0x0098:
            return r10
        L_0x0099:
            r2 = move-exception
            java.lang.String r1 = "IntentCriteria"
            java.lang.String r0 = "Error parsing switch-off criteria."
            android.util.Log.e(r1, r0, r2)
            r0 = 0
            X.09U[] r0 = new X.AnonymousClass09U[r0]
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass09U.A00(java.lang.String, android.content.Context):X.09U[]");
    }

    public AnonymousClass09U() {
        this.A00 = null;
        this.A02 = BuildConfig.FLAVOR;
        this.A03 = false;
        this.A01 = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000a, code lost:
        if (r4 == false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private AnonymousClass09U(android.content.ContentResolver r2, java.lang.String r3, boolean r4, android.content.IntentFilter r5) {
        /*
            r1 = this;
            r1.<init>()
            r1.A00 = r2
            r1.A02 = r3
            if (r3 == 0) goto L_0x000c
            r0 = 1
            if (r4 != 0) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            r1.A03 = r0
            r1.A01 = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass09U.<init>(android.content.ContentResolver, java.lang.String, boolean, android.content.IntentFilter):void");
    }
}
