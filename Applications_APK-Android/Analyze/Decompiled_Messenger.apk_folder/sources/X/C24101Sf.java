package X;

import com.facebook.common.dextricks.DexStore;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: X.1Sf  reason: invalid class name and case insensitive filesystem */
public final class C24101Sf extends AnonymousClass1RO {
    public final C23601Qd A00;
    public final C30911iq A01;
    public final AnonymousClass1NY A02;
    public final C30661iR A03;
    private final C22861Nc A04;

    public static void A00(C24101Sf r5, InputStream inputStream, OutputStream outputStream, int i) {
        byte[] bArr = (byte[]) r5.A04.get(DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET);
        int i2 = i;
        while (i2 > 0) {
            try {
                int read = inputStream.read(bArr, 0, Math.min((int) DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET, i2));
                if (read < 0) {
                    break;
                } else if (read > 0) {
                    outputStream.write(bArr, 0, read);
                    i2 -= read;
                }
            } catch (Throwable th) {
                r5.A04.C0t(bArr);
                throw th;
            }
        }
        r5.A04.C0t(bArr);
        if (i2 > 0) {
            throw new IOException(String.format(null, "Failed to read %d bytes - finished %d short", Integer.valueOf(i), Integer.valueOf(i2)));
        }
    }

    public C24101Sf(C23581Qb r1, C30911iq r2, C23601Qd r3, C30661iR r4, C22861Nc r5, AnonymousClass1NY r6) {
        super(r1);
        this.A01 = r2;
        this.A00 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r6;
    }
}
