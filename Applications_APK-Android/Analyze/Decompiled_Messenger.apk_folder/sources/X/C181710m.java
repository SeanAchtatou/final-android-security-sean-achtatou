package X;

import com.google.common.base.Objects;

/* renamed from: X.10m  reason: invalid class name and case insensitive filesystem */
public final class C181710m {
    public static Integer A00(String str) {
        for (Integer num : AnonymousClass07B.A00(3)) {
            if (Objects.equal(A01(num), str)) {
                return num;
            }
        }
        return null;
    }

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "BOT";
            case 2:
                return "REQUEST";
            default:
                return "PARTICIPANT";
        }
    }
}
