package X;

import com.facebook.systrace.TraceDirect;

/* renamed from: X.00r  reason: invalid class name and case insensitive filesystem */
public final class C001000r {
    public static final C001100s A00 = new C001100s();
    public static volatile long A01;

    static {
        A01(false);
        C001200t r2 = new C001200t();
        if (AnonymousClass00I.A03) {
            AnonymousClass00I.A01(AnonymousClass00I.A00, r2);
        }
    }

    public static void A00(AnonymousClass03a r3) {
        C001100s r2 = A00;
        synchronized (r2.A01) {
            r2.A02.add(r3);
            if (r2.A00) {
                r3.Bse();
            }
        }
    }

    public static boolean A02(long j) {
        if ((j & A01) != 0) {
            return true;
        }
        return false;
    }

    public static void A01(boolean z) {
        long j;
        boolean z2;
        boolean A012 = AnonymousClass00H.A01();
        long A002 = AnonymousClass00I.A00("debug.fbsystrace.tags", 0);
        if (!A012 || A002 == 0) {
            j = 0;
        } else {
            j = A002 | 1;
        }
        if ((A01 != 0 || j == 0) && (j != 0 || A01 == 0)) {
            z2 = false;
        } else {
            z2 = true;
        }
        A01 = j;
        if (z2) {
            if (TraceDirect.checkNative()) {
                TraceDirect.nativeSetEnabledTags(j);
            }
            boolean z3 = false;
            if (j > 0) {
                z3 = true;
            }
            if (z3) {
                C001100s r6 = A00;
                if (!z) {
                    r6.A01();
                    return;
                }
                synchronized (r6.A01) {
                    try {
                        Thread thread = new Thread(new AnonymousClass0QU(r6, C001100s.A03.lastModified()), "fbsystrace notification thread");
                        thread.setPriority(10);
                        thread.start();
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
                return;
            }
            C001100s r2 = A00;
            synchronized (r2.A01) {
                try {
                    C001100s.A00(r2, false);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }
}
