package X;

import java.util.Map;
import java.util.Set;

/* renamed from: X.1Y2  reason: invalid class name */
public final class AnonymousClass1Y2 {
    public final Map A00;
    public final Map A01;
    public final Map A02;
    public final Map A03;
    public final Set A04;

    public AnonymousClass1Y2(Map map, Map map2, Map map3, Set set, Map map4) {
        this.A00 = map;
        this.A03 = map2;
        this.A01 = map3;
        this.A04 = set;
        this.A02 = map4;
    }
}
