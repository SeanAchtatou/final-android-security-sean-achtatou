package X;

import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1vy  reason: invalid class name and case insensitive filesystem */
public final class C37621vy implements AnonymousClass1NH {
    public final /* synthetic */ AnonymousClass1BE A00;

    public C37621vy(AnonymousClass1BE r1) {
        this.A00 = r1;
    }

    public void Bri(InboxUnitThreadItem inboxUnitThreadItem, C21361Gm r3) {
        AnonymousClass1BE r0 = this.A00;
        if (r0 != null) {
            r0.BtG(inboxUnitThreadItem);
        }
    }
}
