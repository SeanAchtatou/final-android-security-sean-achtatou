package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

/* renamed from: X.0E4  reason: invalid class name */
public final class AnonymousClass0E4 extends AnonymousClass0E3 {
    public boolean A00;
    public boolean A01;
    private final Context A02;
    private final PowerManager.WakeLock A03;
    private final PowerManager.WakeLock A04;

    public void A00() {
        synchronized (this) {
            if (this.A01) {
                if (this.A00) {
                    C009007v.A02(this.A03, 60000);
                }
                this.A01 = false;
                C009007v.A01(this.A04);
            }
        }
    }

    public void A01() {
        synchronized (this) {
            if (!this.A01) {
                this.A01 = true;
                C009007v.A02(this.A04, 600000);
                C009007v.A01(this.A03);
            }
        }
    }

    public void A02() {
        synchronized (this) {
            this.A00 = false;
        }
    }

    public void A04(Intent intent) {
        Intent intent2 = new Intent(intent);
        intent2.setComponent(this.A02);
        if (this.A02.startService(intent2) != null) {
            synchronized (this) {
                if (!this.A00) {
                    this.A00 = true;
                    if (!this.A01) {
                        C009007v.A02(this.A03, 60000);
                    }
                }
            }
        }
    }

    public AnonymousClass0E4(Context context, ComponentName componentName) {
        super(componentName);
        this.A02 = context.getApplicationContext();
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        PowerManager.WakeLock A002 = C009007v.A00(powerManager, 1, AnonymousClass08S.A0J(componentName.getClassName(), ":launch"));
        this.A03 = A002;
        C009007v.A03(A002, false);
        PowerManager.WakeLock A003 = C009007v.A00(powerManager, 1, AnonymousClass08S.A0J(componentName.getClassName(), ":run"));
        this.A04 = A003;
        C009007v.A03(A003, false);
    }
}
