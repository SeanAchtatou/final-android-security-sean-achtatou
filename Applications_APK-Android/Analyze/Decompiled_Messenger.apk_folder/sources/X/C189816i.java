package X;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;

/* renamed from: X.16i  reason: invalid class name and case insensitive filesystem */
public final class C189816i {
    public static final C189816i A0H = new C189816i(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false);
    public final C34801qC A00;
    public final C34801qC A01;
    public final ImmutableList A02;
    public final ImmutableList A03;
    public final ImmutableList A04;
    public final ImmutableList A05;
    public final ImmutableList A06;
    public final ImmutableList A07;
    public final ImmutableList A08;
    public final ImmutableList A09;
    public final ImmutableList A0A;
    public final ImmutableList A0B;
    public final ImmutableList A0C;
    public final ImmutableList A0D;
    public final boolean A0E;
    private final ImmutableList A0F;
    private final ImmutableList A0G;

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(C189816i.class);
        stringHelper.add("topFriends", this.A0B);
        stringHelper.add("onlineFriends", this.A0F);
        stringHelper.add("topOnlineFriends", this.A0G);
        stringHelper.add("onMessengerFriends", this.A06);
        stringHelper.add("topOnMessengerFriends", this.A0C);
        stringHelper.add("notOnMessengerFriends", this.A05);
        stringHelper.add("PHATContacts", this.A02);
        stringHelper.add("topContacts", this.A0A);
        stringHelper.add("topRtcContacts", this.A01);
        stringHelper.add("smsInviteContacts", this.A07);
        stringHelper.add("allContacts", this.A03);
        stringHelper.add("hasPendingUpdates", this.A0E);
        stringHelper.add("specificUsers", this.A08);
        stringHelper.add("favoriteMessengerContacts", this.A04);
        return stringHelper.toString();
    }

    public C189816i(ImmutableList immutableList, ImmutableList immutableList2, C34801qC r4, ImmutableList immutableList3, ImmutableList immutableList4, ImmutableList immutableList5, ImmutableList immutableList6, ImmutableList immutableList7, ImmutableList immutableList8, C34801qC r11, ImmutableList immutableList9, ImmutableList immutableList10, ImmutableList immutableList11, ImmutableList immutableList12, ImmutableList immutableList13, ImmutableList immutableList14, boolean z) {
        this.A0B = immutableList;
        this.A0F = immutableList2;
        this.A00 = r4;
        this.A0G = immutableList3;
        this.A06 = immutableList4;
        this.A0C = immutableList5;
        this.A05 = immutableList6;
        this.A02 = immutableList7;
        this.A0A = immutableList8;
        this.A01 = r11;
        this.A07 = immutableList9;
        this.A0D = immutableList10;
        this.A0E = z;
        this.A03 = immutableList11;
        this.A08 = immutableList12;
        this.A04 = immutableList13;
        this.A09 = immutableList14;
    }
}
