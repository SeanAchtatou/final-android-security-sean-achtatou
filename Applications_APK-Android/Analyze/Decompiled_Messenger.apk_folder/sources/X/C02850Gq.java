package X;

/* renamed from: X.0Gq  reason: invalid class name and case insensitive filesystem */
public final class C02850Gq implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        C02520Fg r62 = (C02520Fg) r6;
        long j = r62.mobileBytesTx;
        if (j != 0) {
            r7.AMV("mobile_bytes_tx", j);
        }
        long j2 = r62.mobileBytesRx;
        if (j2 != 0) {
            r7.AMV("mobile_bytes_rx", j2);
        }
        long j3 = r62.wifiBytesTx;
        if (j3 != 0) {
            r7.AMV("wifi_bytes_tx", j3);
        }
        long j4 = r62.wifiBytesRx;
        if (j4 != 0) {
            r7.AMV("wifi_bytes_rx", j4);
        }
    }
}
