package X;

import com.facebook.messaging.model.messages.Message;
import com.google.common.base.Preconditions;

/* renamed from: X.1w9  reason: invalid class name and case insensitive filesystem */
public final class C37721w9 extends Exception {
    public final Message failedMessage;

    public C37721w9(String str, Message message) {
        super(str);
        Preconditions.checkNotNull(message);
        Preconditions.checkArgument(message.A0D == AnonymousClass1V7.A0A);
        this.failedMessage = message;
    }

    public C37721w9(String str, Throwable th, Message message) {
        super(str, th);
        Preconditions.checkNotNull(message);
        Preconditions.checkArgument(message.A0D == AnonymousClass1V7.A0A);
        this.failedMessage = message;
    }

    public C37721w9(Throwable th, Message message) {
        super(th);
        Preconditions.checkNotNull(message);
        Preconditions.checkArgument(message.A0D == AnonymousClass1V7.A0A);
        this.failedMessage = message;
    }
}
