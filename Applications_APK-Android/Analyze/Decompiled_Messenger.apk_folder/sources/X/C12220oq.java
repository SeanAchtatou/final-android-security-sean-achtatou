package X;

import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0oq  reason: invalid class name and case insensitive filesystem */
public final class C12220oq implements C12210op {
    private boolean A00 = ((C25051Yd) this.A09.get()).Aem(287359081913762L);
    private boolean A01;
    private boolean A02 = ((C25051Yd) this.A09.get()).Aem(286959650938174L);
    private boolean A03 = ((C25051Yd) this.A09.get()).Aem(286959651003711L);
    private final int A04 = ((C25051Yd) this.A09.get()).AqL(568434626726195L, 100000);
    private final int A05 = ((int) ((C25051Yd) this.A09.get()).At0(568434627250485L));
    private final int A06 = ((int) ((C25051Yd) this.A09.get()).At0(568434627184948L));
    private final AnonymousClass0US A07;
    private final AnonymousClass0US A08;
    private final AnonymousClass0US A09;
    private final String A0A = ((C25051Yd) this.A09.get()).B4E(849909603631809L, BuildConfig.FLAVOR);
    private final boolean A0B = ((C25051Yd) this.A09.get()).Aem(286959649955124L);
    private final boolean A0C;
    private final boolean A0D = ((C25051Yd) this.A09.get()).Aem(2306129968863845686L);
    private final boolean A0E = ((C25051Yd) this.A09.get()).Aem(286959650676026L);
    private final boolean A0F = ((C25051Yd) this.A09.get()).Aem(286959650872637L);
    private final boolean A0G = ((C25051Yd) this.A09.get()).Aem(2306130385475476902L);
    private final boolean A0H = ((C25051Yd) this.A09.get()).Aem(2306130385475542439L);
    private final boolean A0I = ((C25051Yd) this.A09.get()).Aem(286959650282807L);
    private final boolean A0J = ((C25051Yd) this.A09.get()).Aem(286959650348344L);
    private final boolean A0K = ((C25051Yd) this.A09.get()).Aem(286959650413881L);
    private volatile C08360fE A0L;

    public int Aog() {
        return this.A04;
    }

    public int Ask() {
        return this.A05;
    }

    public C08360fE AuU() {
        if (this.A0L == null) {
            synchronized (this) {
                if (this.A0L == null) {
                    this.A0L = new C08360fE((AnonymousClass1YI) this.A08.get(), (C25051Yd) this.A09.get());
                }
            }
        }
        return this.A0L;
    }

    public int B3f() {
        return this.A06;
    }

    public boolean BCO() {
        return this.A01;
    }

    public boolean BFG() {
        return this.A00;
    }

    public boolean BFh() {
        return this.A0C;
    }

    public boolean BIl() {
        return this.A02;
    }

    public boolean Bwk() {
        return this.A0D;
    }

    public String Bwl() {
        return this.A0A;
    }

    public boolean Bwm() {
        return this.A0B;
    }

    public boolean Bwn() {
        return this.A0I;
    }

    public boolean Bwo() {
        return this.A0J;
    }

    public boolean Bwp() {
        return this.A0E;
    }

    public boolean Bwq() {
        return this.A03;
    }

    public boolean C5V() {
        return this.A0F;
    }

    public boolean CDd() {
        return this.A0G;
    }

    public boolean CDn() {
        return this.A0H;
    }

    public boolean CDo() {
        return ((C25051Yd) this.A09.get()).Aem(286959650807100L);
    }

    public boolean CEU() {
        return ((AnonymousClass1YI) this.A08.get()).AbO(AnonymousClass1Y3.A0i, false);
    }

    public boolean CEd() {
        C09400hF r0 = (C09400hF) this.A07.get();
        if (r0 != null) {
            return r0.A03();
        }
        return false;
    }

    public boolean CFD() {
        return this.A0K;
    }

    public boolean CFF() {
        return ((AnonymousClass1YI) this.A08.get()).AbO(796, false);
    }

    public C12220oq(AnonymousClass0US r5, AnonymousClass0US r6, AnonymousClass0US r7, AnonymousClass0US r8) {
        this.A08 = r5;
        this.A09 = r6;
        this.A07 = r8;
        this.A01 = ((C25051Yd) r6.get()).Aem(286959650086197L);
        this.A0C = ((C25051Yd) r7.get()).Aem(2324144775395024291L);
    }
}
