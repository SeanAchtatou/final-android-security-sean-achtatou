package X;

import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1BF  reason: invalid class name */
public interface AnonymousClass1BF {
    void BfU(InboxMontageItem inboxMontageItem);

    void BfZ(InboxMontageItem inboxMontageItem);

    void Bfb(InboxMontageItem inboxMontageItem);

    void BhR(UserKey userKey, long j, InboxMontageItem inboxMontageItem);

    void Bpd(ImmutableList immutableList, long j, InboxUnitItem inboxUnitItem, C114765cl r5);
}
