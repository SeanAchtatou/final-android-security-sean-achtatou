package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@UserScoped
/* renamed from: X.1HM  reason: invalid class name */
public final class AnonymousClass1HM {
    private static C05540Zi A04;
    public AnonymousClass0UN A00;
    public final AnonymousClass10R A01;
    public final List A02 = new ArrayList();
    public final ReadWriteLock A03 = new ReentrantReadWriteLock();

    public static final AnonymousClass1HM A00(AnonymousClass1XY r4) {
        AnonymousClass1HM r0;
        synchronized (AnonymousClass1HM.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new AnonymousClass1HM((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (AnonymousClass1HM) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    public List A01() {
        Lock readLock = this.A03.readLock();
        readLock.lock();
        try {
            return this.A02;
        } finally {
            readLock.unlock();
        }
    }

    private AnonymousClass1HM(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A01 = AnonymousClass10R.A00(r3);
    }
}
