package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.0cZ  reason: invalid class name and case insensitive filesystem */
public final class C07060cZ extends Handler {
    public final /* synthetic */ C07930eP A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C07060cZ(C07930eP r1, Looper looper) {
        super(looper);
        this.A00 = r1;
    }

    public void handleMessage(Message message) {
        C06150aw r1;
        C06130au A002;
        int i = message.what;
        if (i == 0) {
            C06130au A003 = C06130au.A00(message.getData());
            if (!this.A00.A0B.containsKey(Integer.valueOf(A003.A01))) {
                C07930eP.A04(this.A00, A003, AnonymousClass07B.A01);
            }
        } else if (i != 1) {
            C07930eP r3 = this.A00;
            synchronized (r3.A0A) {
                r1 = (C06150aw) r3.A0A.get(Integer.valueOf(message.what));
            }
            if (r1 != null && (A002 = C07930eP.A00(r3, message)) != null) {
                r1.Bex(A002, message);
            }
        } else {
            C06130au A004 = C07930eP.A00(this.A00, message);
            if (A004 != null) {
                C07930eP.A03(this.A00, A004);
            }
        }
    }
}
