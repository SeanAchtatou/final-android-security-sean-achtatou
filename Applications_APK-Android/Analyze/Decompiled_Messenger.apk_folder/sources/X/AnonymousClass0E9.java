package X;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: X.0E9  reason: invalid class name */
public final class AnonymousClass0E9 extends AnonymousClass0B4 {
    public SharedPreferences A01(Context context, String str, boolean z) {
        return context.getSharedPreferences(str, 0);
    }
}
