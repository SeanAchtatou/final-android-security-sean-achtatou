package X;

import java.util.concurrent.ExecutorService;

/* renamed from: X.0XY  reason: invalid class name */
public final class AnonymousClass0XY {
    public final int A00;
    public final int A01;
    public final AnonymousClass0XV A02;
    public final AnonymousClass0XX A03;
    public final String A04;
    public final ExecutorService A05;

    public String toString() {
        return "[Task priority: " + this.A02 + " executor: " + this.A05 + " order: " + this.A00 + " description: " + this.A04 + " idleDelay: " + this.A01 + " ]";
    }

    public AnonymousClass0XY(AnonymousClass0XX r1, AnonymousClass0XV r2, ExecutorService executorService, String str, int i, int i2) {
        this.A03 = r1;
        this.A02 = r2;
        this.A05 = executorService;
        this.A04 = str;
        this.A00 = i;
        this.A01 = i2;
    }
}
