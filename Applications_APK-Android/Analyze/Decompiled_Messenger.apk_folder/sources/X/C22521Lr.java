package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Preconditions;

/* renamed from: X.1Lr  reason: invalid class name and case insensitive filesystem */
public final class C22521Lr {
    public int A00;
    public int A01;
    public C21061Ew A02;
    public QuickPerformanceLogger A03;
    public boolean A04 = false;
    private String A05;

    public void A00() {
        C21061Ew withMarker = this.A03.withMarker(this.A00, this.A01);
        this.A02 = withMarker;
        withMarker.A08("module", this.A05);
        this.A04 = true;
    }

    public void A01(String str, double d) {
        Preconditions.checkNotNull(this.A02);
        Preconditions.checkState(this.A04);
        this.A02.A04(str, d);
    }

    public void A02(String str, int i) {
        Preconditions.checkNotNull(this.A02);
        Preconditions.checkState(this.A04);
        this.A02.A05(str, i);
    }

    public void A03(String str, long j) {
        Preconditions.checkNotNull(this.A02);
        Preconditions.checkState(this.A04);
        this.A02.A06(str, j);
    }

    public void A04(String str, String str2) {
        Preconditions.checkNotNull(this.A02);
        Preconditions.checkState(this.A04);
        if (str2 != null) {
            this.A02.A08(str, str2);
        }
    }

    public C22521Lr(QuickPerformanceLogger quickPerformanceLogger, int i, String str, int i2) {
        this.A03 = quickPerformanceLogger;
        this.A00 = i;
        this.A05 = str;
        this.A01 = i2;
    }
}
