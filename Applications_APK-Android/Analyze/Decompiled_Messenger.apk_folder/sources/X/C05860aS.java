package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0aS  reason: invalid class name and case insensitive filesystem */
public final class C05860aS {
    public Map A00 = new HashMap();

    public static synchronized void A00(C05860aS r2, Object obj) {
        synchronized (r2) {
            Object obj2 = r2.A00.get(obj);
            C000300h.A01(obj2);
            C05870aT r1 = (C05870aT) obj2;
            int i = r1.A00 - 1;
            r1.A00 = i;
            if (i == 0) {
                r2.A00.remove(obj);
            }
        }
    }

    public synchronized void A01(Object obj) {
        Object obj2 = this.A00.get(obj);
        C000300h.A01(obj2);
        C05870aT r2 = (C05870aT) obj2;
        boolean z = false;
        if (r2.A01.availablePermits() == 0) {
            z = true;
        }
        C000300h.A03(z);
        r2.A01.release();
        A00(this, obj);
    }
}
