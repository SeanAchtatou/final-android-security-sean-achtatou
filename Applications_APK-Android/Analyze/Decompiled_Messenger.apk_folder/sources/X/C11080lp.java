package X;

import com.facebook.fbservice.service.OperationResult;
import com.google.common.collect.ImmutableSet;
import java.util.Map;

/* renamed from: X.0lp  reason: invalid class name and case insensitive filesystem */
public final class C11080lp implements C11090lq {
    public OperationResult BL3(Map map, ImmutableSet immutableSet) {
        if (immutableSet.isEmpty()) {
            return OperationResult.A00;
        }
        throw new C97214kN(immutableSet);
    }
}
