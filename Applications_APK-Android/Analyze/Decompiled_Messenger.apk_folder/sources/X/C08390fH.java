package X;

import com.facebook.common.dextricks.DexStore;
import com.facebook.quicklog.PerformanceLoggingEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fH  reason: invalid class name and case insensitive filesystem */
public final class C08390fH extends AnonymousClass0f8 {
    private static volatile C08390fH A01;
    public AnonymousClass0UN A00;

    public String Azg() {
        return "nt_stats";
    }

    public static final C08390fH A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C08390fH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C08390fH(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A01(String str, int i) {
        ((BJ0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BFQ, this.A00)).A02(AnonymousClass08S.A0J("nt_", str), i, AnonymousClass07B.A01);
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        BJ0 bj0 = (BJ0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BFQ, this.A00);
        synchronized (bj0) {
            HashMap hashMap = new HashMap();
            long At0 = bj0.A01.At0(566626445428444L);
            PerformanceLoggingEvent performanceLoggingEvent2 = performanceLoggingEvent;
            long j = performanceLoggingEvent2.A0C;
            long millis = j + ((long) ((int) TimeUnit.NANOSECONDS.toMillis(performanceLoggingEvent2.A08)));
            int i = bj0.A00;
            int i2 = 0;
            while (true) {
                BJ2 bj2 = bj0.A02[i];
                if (bj2 == null) {
                    break;
                }
                long j2 = bj2.A01;
                if (j2 < j) {
                    break;
                }
                if (j2 <= millis && bj2.A02 == AnonymousClass07B.A01) {
                    for (int i3 = ((i - 1) + DexStore.LOAD_RESULT_OATMEAL_QUICKENED) % DexStore.LOAD_RESULT_OATMEAL_QUICKENED; i3 != i; i3 = ((i3 - 1) + DexStore.LOAD_RESULT_OATMEAL_QUICKENED) % DexStore.LOAD_RESULT_OATMEAL_QUICKENED) {
                        BJ2 bj22 = bj0.A02[i3];
                        if (bj22 == null || bj22.A01 < j) {
                            break;
                        }
                        if (bj22.A02 == AnonymousClass07B.A00 && bj22.A00 == bj2.A00) {
                            String str = bj22.A03;
                            String str2 = bj2.A03;
                            if (str.equals(str2)) {
                                Object obj3 = (List) hashMap.get(str2);
                                if (obj3 == null) {
                                    obj3 = new ArrayList();
                                    hashMap.put(bj2.A03, obj3);
                                }
                                obj3.add(new BJ1(bj22, bj2));
                                if (bj22.A03.equals("nt_parse")) {
                                    i2 = (int) (((long) i2) + (bj2.A01 - bj22.A01));
                                }
                            }
                        }
                    }
                }
                i = ((i - 1) + DexStore.LOAD_RESULT_OATMEAL_QUICKENED) % DexStore.LOAD_RESULT_OATMEAL_QUICKENED;
                if (i == bj0.A00) {
                    BJ0.A01(hashMap, performanceLoggingEvent2, j, At0);
                    performanceLoggingEvent2.A0A("data_provider_error", "ring buffer overflow");
                    performanceLoggingEvent2.A05("total_parse_time", (long) i2);
                    break;
                }
            }
            BJ0.A01(hashMap, performanceLoggingEvent2, j, At0);
            performanceLoggingEvent2.A05("total_parse_time", (long) i2);
        }
    }

    public long Azh() {
        return C08350fD.A0D;
    }

    public Class B3O() {
        return C30403Eve.class;
    }

    public Object CGO() {
        return new C30403Eve();
    }

    private C08390fH(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A08;
    }
}
