package X;

/* renamed from: X.1yI  reason: invalid class name and case insensitive filesystem */
public final class C38971yI {
    public static final C08900gA A02 = C08870g7.A1U;
    public final AnonymousClass0XN A00;
    public final C185414b A01;

    public static final C38971yI A00(AnonymousClass1XY r1) {
        return new C38971yI(r1);
    }

    public static void A01(C38971yI r2, String str, AnonymousClass26K r4) {
        if (r4 == null) {
            r4 = AnonymousClass26K.A00();
        }
        r2.A01.AOO(A02, str, null, r4);
    }

    private C38971yI(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass14R.A00(r2);
        this.A00 = C25481Zu.A00(r2);
    }
}
