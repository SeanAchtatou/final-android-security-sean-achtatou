package X;

import android.graphics.Bitmap;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1PS  reason: invalid class name */
public abstract class AnonymousClass1PS implements Closeable, Cloneable {
    public static int A04;
    public static Class A05 = AnonymousClass1PS.class;
    public static final C22891Nf A06 = new AnonymousClass1PT();
    private static final C22991Nq A07 = new AnonymousClass1PV();
    public boolean A00 = false;
    public final C22991Nq A01;
    public final AnonymousClass1SA A02;
    public final Throwable A03;

    /* renamed from: A08 */
    public AnonymousClass1PS clone() {
        if (this instanceof AnonymousClass1ST) {
            AnonymousClass1ST r4 = (AnonymousClass1ST) this;
            C05520Zg.A05(r4.A0B());
            return new AnonymousClass1ST(r4.A02, r4.A01, r4.A03);
        } else if (this instanceof AnonymousClass1SU) {
            return (AnonymousClass1SU) this;
        } else {
            if (this instanceof AnonymousClass1SW) {
                return (AnonymousClass1SW) this;
            }
            C24251Sw r42 = (C24251Sw) this;
            C05520Zg.A05(r42.A0B());
            return new C24251Sw(r42.A02, r42.A01, r42.A03);
        }
    }

    public synchronized AnonymousClass1PS A09() {
        if (!A0B()) {
            return null;
        }
        return clone();
    }

    public synchronized Object A0A() {
        boolean z = false;
        if (!this.A00) {
            z = true;
        }
        C05520Zg.A05(z);
        return this.A02.A01();
    }

    public synchronized boolean A0B() {
        return !this.A00;
    }

    public static AnonymousClass1PS A00(AnonymousClass1PS r0) {
        if (r0 != null) {
            return r0.A09();
        }
        return null;
    }

    public static AnonymousClass1PS A01(Closeable closeable) {
        return A02(closeable, A06);
    }

    public static AnonymousClass1PS A02(Object obj, C22891Nf r4) {
        C22991Nq r2 = A07;
        Throwable th = null;
        if (obj == null) {
            return null;
        }
        if (r2.C3V()) {
            th = new Throwable();
        }
        return A03(obj, r4, r2, th);
    }

    public static AnonymousClass1PS A03(Object obj, C22891Nf r3, C22991Nq r4, Throwable th) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Bitmap) || (obj instanceof AnonymousClass1S5)) {
            int i = A04;
            if (i == 1) {
                return new AnonymousClass1SW(obj, r3, r4, th);
            }
            if (i == 2) {
                return new AnonymousClass1ST(obj, r3, r4, th);
            }
            if (i == 3) {
                return new AnonymousClass1SU(obj, r3, r4, th);
            }
        }
        return new C24251Sw(obj, r3, r4, th);
    }

    public static List A04(Collection collection) {
        if (collection == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            arrayList.add(A00((AnonymousClass1PS) it.next()));
        }
        return arrayList;
    }

    public static void A05(AnonymousClass1PS r0) {
        if (r0 != null) {
            r0.close();
        }
    }

    public static void A06(Iterable iterable) {
        if (iterable != null) {
            Iterator it = iterable.iterator();
            while (it.hasNext()) {
                A05((AnonymousClass1PS) it.next());
            }
        }
    }

    public static boolean A07(AnonymousClass1PS r1) {
        if (r1 == null || !r1.A0B()) {
            return false;
        }
        return true;
    }

    public void close() {
        if (!(this instanceof AnonymousClass1SU) && !(this instanceof AnonymousClass1SW)) {
            synchronized (this) {
                if (!this.A00) {
                    this.A00 = true;
                    this.A02.A02();
                }
            }
        }
    }

    public void finalize() {
        boolean z;
        int A032 = C000700l.A03(105516448);
        try {
            synchronized (this) {
                z = this.A00;
            }
            if (z) {
                super.finalize();
                C000700l.A09(-289656946, A032);
                return;
            }
            this.A01.C2m(this.A02, this.A03);
            close();
            super.finalize();
            C000700l.A09(-872040364, A032);
        } catch (Throwable th) {
            super.finalize();
            C000700l.A09(-827482466, A032);
            throw th;
        }
    }

    public AnonymousClass1PS(AnonymousClass1SA r2, C22991Nq r3, Throwable th) {
        C05520Zg.A02(r2);
        this.A02 = r2;
        synchronized (r2) {
            AnonymousClass1SA.A00(r2);
            r2.A00++;
        }
        this.A01 = r3;
        this.A03 = th;
    }

    public AnonymousClass1PS(Object obj, C22891Nf r3, C22991Nq r4, Throwable th) {
        this.A02 = new AnonymousClass1SA(obj, r3);
        this.A01 = r4;
        this.A03 = th;
    }
}
