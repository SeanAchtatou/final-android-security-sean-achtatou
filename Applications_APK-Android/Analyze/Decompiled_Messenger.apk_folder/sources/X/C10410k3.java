package X;

/* renamed from: X.0k3  reason: invalid class name and case insensitive filesystem */
public final class C10410k3 {
    public static final C10410k3 sBootstrapSymbolTable = new C10410k3();
    public C10420k4[] _buckets;
    public final boolean _canonicalize;
    public boolean _dirty;
    public final int _hashSeed;
    public int _indexMask;
    public final boolean _intern;
    public int _longestCollisionList;
    public C10410k3 _parent;
    public int _size;
    public int _sizeThreshold;
    public String[] _symbols;

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003a, code lost:
        if (r3 == r9) goto L_0x003c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String findSymbol(char[] r18, int r19, int r20, int r21) {
        /*
            r17 = this;
            r8 = 1
            r9 = r20
            if (r9 >= r8) goto L_0x0008
            java.lang.String r0 = ""
            return r0
        L_0x0008:
            r5 = r17
            boolean r0 = r5._canonicalize
            r10 = r19
            r11 = r18
            if (r0 != 0) goto L_0x0018
            java.lang.String r0 = new java.lang.String
            r0.<init>(r11, r10, r9)
            return r0
        L_0x0018:
            int r0 = r21 >>> 15
            int r1 = r21 + r0
            int r0 = r5._indexMask
            r1 = r1 & r0
            java.lang.String[] r0 = r5._symbols
            r7 = r0[r1]
            if (r7 == 0) goto L_0x006c
            int r0 = r7.length()
            if (r0 != r9) goto L_0x003d
            r3 = 0
        L_0x002c:
            char r2 = r7.charAt(r3)
            int r0 = r19 + r3
            char r0 = r18[r0]
            if (r2 != r0) goto L_0x003a
            int r3 = r3 + 1
            if (r3 < r9) goto L_0x002c
        L_0x003a:
            if (r3 != r9) goto L_0x003d
        L_0x003c:
            return r7
        L_0x003d:
            X.0k4[] r2 = r5._buckets
            int r0 = r1 >> 1
            r0 = r2[r0]
            if (r0 == 0) goto L_0x006c
            java.lang.String r7 = r0._symbol
            X.0k4 r4 = r0._next
        L_0x0049:
            int r0 = r7.length()
            if (r0 != r9) goto L_0x0063
            r3 = 0
        L_0x0050:
            char r2 = r7.charAt(r3)
            int r0 = r19 + r3
            char r0 = r18[r0]
            if (r2 != r0) goto L_0x005e
            int r3 = r3 + 1
            if (r3 < r9) goto L_0x0050
        L_0x005e:
            if (r3 != r9) goto L_0x0063
        L_0x0060:
            if (r7 == 0) goto L_0x006c
            return r7
        L_0x0063:
            if (r4 != 0) goto L_0x0067
            r7 = 0
            goto L_0x0060
        L_0x0067:
            java.lang.String r7 = r4._symbol
            X.0k4 r4 = r4._next
            goto L_0x0049
        L_0x006c:
            boolean r0 = r5._dirty
            if (r0 != 0) goto L_0x00a4
            java.lang.String[] r3 = r5._symbols
            int r2 = r3.length
            java.lang.String[] r0 = new java.lang.String[r2]
            r5._symbols = r0
            r4 = 0
            java.lang.System.arraycopy(r3, r4, r0, r4, r2)
            X.0k4[] r3 = r5._buckets
            int r2 = r3.length
            X.0k4[] r0 = new X.C10420k4[r2]
            r5._buckets = r0
            java.lang.System.arraycopy(r3, r4, r0, r4, r2)
            r5._dirty = r8
        L_0x0087:
            java.lang.String r7 = new java.lang.String
            r7.<init>(r11, r10, r9)
            boolean r0 = r5._intern
            if (r0 == 0) goto L_0x0096
            X.0xn r0 = X.C16800xn.instance
            java.lang.String r7 = r0.intern(r7)
        L_0x0096:
            int r6 = r5._size
            int r6 = r6 + r8
            r5._size = r6
            java.lang.String[] r2 = r5._symbols
            r0 = r2[r1]
            if (r0 != 0) goto L_0x0195
            r2[r1] = r7
            return r7
        L_0x00a4:
            int r2 = r5._size
            int r0 = r5._sizeThreshold
            if (r2 < r0) goto L_0x0087
            java.lang.String[] r6 = r5._symbols
            int r4 = r6.length
            int r1 = r4 + r4
            r14 = 0
            r0 = 65536(0x10000, float:9.18355E-41)
            if (r1 <= r0) goto L_0x00ce
            r5._size = r14
            r1 = 0
            java.util.Arrays.fill(r6, r1)
            X.0k4[] r0 = r5._buckets
            java.util.Arrays.fill(r0, r1)
            r5._dirty = r8
        L_0x00c1:
            int r1 = r5._hashSeed
            r2 = 0
        L_0x00c4:
            if (r2 >= r9) goto L_0x018a
            int r1 = r1 * 33
            char r0 = r18[r2]
            int r1 = r1 + r0
            int r2 = r2 + 1
            goto L_0x00c4
        L_0x00ce:
            X.0k4[] r13 = r5._buckets
            java.lang.String[] r0 = new java.lang.String[r1]
            r5._symbols = r0
            int r0 = r1 >> 1
            X.0k4[] r0 = new X.C10420k4[r0]
            r5._buckets = r0
            int r0 = r1 + -1
            r5._indexMask = r0
            int r0 = r1 >> 2
            int r1 = r1 - r0
            r5._sizeThreshold = r1
            r3 = 0
            r7 = 0
            r12 = 0
        L_0x00e6:
            if (r3 >= r4) goto L_0x012a
            r15 = r6[r3]
            if (r15 == 0) goto L_0x0113
            int r7 = r7 + 1
            int r1 = r15.length()
            int r2 = r5._hashSeed
            r0 = 0
        L_0x00f5:
            if (r0 >= r1) goto L_0x0102
            int r2 = r2 * 33
            char r16 = r15.charAt(r0)
            int r2 = r2 + r16
            int r0 = r0 + 1
            goto L_0x00f5
        L_0x0102:
            if (r2 != 0) goto L_0x0105
            r2 = 1
        L_0x0105:
            int r0 = r2 >>> 15
            int r2 = r2 + r0
            int r0 = r5._indexMask
            r2 = r2 & r0
            java.lang.String[] r1 = r5._symbols
            r0 = r1[r2]
            if (r0 != 0) goto L_0x0116
            r1[r2] = r15
        L_0x0113:
            int r3 = r3 + 1
            goto L_0x00e6
        L_0x0116:
            int r16 = r2 >> 1
            X.0k4 r2 = new X.0k4
            X.0k4[] r1 = r5._buckets
            r0 = r1[r16]
            r2.<init>(r15, r0)
            r1[r16] = r2
            int r0 = r2._length
            int r12 = java.lang.Math.max(r12, r0)
            goto L_0x0113
        L_0x012a:
            int r6 = r4 >> 1
        L_0x012c:
            if (r14 >= r6) goto L_0x0174
            r4 = r13[r14]
        L_0x0130:
            if (r4 == 0) goto L_0x0171
            int r7 = r7 + 1
            java.lang.String r15 = r4._symbol
            int r3 = r15.length()
            int r2 = r5._hashSeed
            r1 = 0
        L_0x013d:
            if (r1 >= r3) goto L_0x0149
            int r2 = r2 * 33
            char r0 = r15.charAt(r1)
            int r2 = r2 + r0
            int r1 = r1 + 1
            goto L_0x013d
        L_0x0149:
            if (r2 != 0) goto L_0x014c
            r2 = 1
        L_0x014c:
            int r0 = r2 >>> 15
            int r2 = r2 + r0
            int r0 = r5._indexMask
            r2 = r2 & r0
            java.lang.String[] r1 = r5._symbols
            r0 = r1[r2]
            if (r0 != 0) goto L_0x015d
            r1[r2] = r15
        L_0x015a:
            X.0k4 r4 = r4._next
            goto L_0x0130
        L_0x015d:
            int r3 = r2 >> 1
            X.0k4 r2 = new X.0k4
            X.0k4[] r1 = r5._buckets
            r0 = r1[r3]
            r2.<init>(r15, r0)
            r1[r3] = r2
            int r0 = r2._length
            int r12 = java.lang.Math.max(r12, r0)
            goto L_0x015a
        L_0x0171:
            int r14 = r14 + 1
            goto L_0x012c
        L_0x0174:
            r5._longestCollisionList = r12
            int r4 = r5._size
            if (r7 == r4) goto L_0x00c1
            java.lang.Error r3 = new java.lang.Error
            java.lang.String r2 = "Internal error on SymbolTable.rehash(): had "
            java.lang.String r1 = " entries; now have "
            java.lang.String r0 = "."
            java.lang.String r0 = X.AnonymousClass08S.A0C(r2, r4, r1, r7, r0)
            r3.<init>(r0)
            throw r3
        L_0x018a:
            if (r1 != 0) goto L_0x018d
            r1 = 1
        L_0x018d:
            int r0 = r1 >>> 15
            int r1 = r1 + r0
            int r0 = r5._indexMask
            r1 = r1 & r0
            goto L_0x0087
        L_0x0195:
            int r3 = r1 >> 1
            X.0k4 r2 = new X.0k4
            X.0k4[] r1 = r5._buckets
            r0 = r1[r3]
            r2.<init>(r7, r0)
            r1[r3] = r2
            int r1 = r2._length
            int r0 = r5._longestCollisionList
            int r0 = java.lang.Math.max(r1, r0)
            r5._longestCollisionList = r0
            r4 = 255(0xff, float:3.57E-43)
            if (r0 <= r4) goto L_0x003c
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r2 = "Longest collision chain in symbol table (of size "
            java.lang.String r1 = ") now exceeds maximum, "
            java.lang.String r0 = " -- suspect a DoS attack based on hash collisions"
            java.lang.String r0 = X.AnonymousClass08S.A0C(r2, r6, r1, r4, r0)
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10410k3.findSymbol(char[], int, int, int):java.lang.String");
    }

    public C10410k3 makeChild(boolean z, boolean z2) {
        String[] strArr;
        C10420k4[] r5;
        int i;
        int i2;
        int i3;
        synchronized (this) {
            strArr = this._symbols;
            r5 = this._buckets;
            i = this._size;
            i2 = this._hashSeed;
            i3 = this._longestCollisionList;
        }
        return new C10410k3(this, z, z2, strArr, r5, i, i2, i3);
    }

    public static void initTables(C10410k3 r1, int i) {
        r1._symbols = new String[i];
        r1._buckets = new C10420k4[(i >> 1)];
        r1._indexMask = i - 1;
        r1._size = 0;
        r1._longestCollisionList = 0;
        r1._sizeThreshold = i - (i >> 2);
    }

    public static C10410k3 createRoot() {
        long currentTimeMillis = System.currentTimeMillis();
        C10410k3 r0 = sBootstrapSymbolTable;
        return new C10410k3(null, true, true, r0._symbols, r0._buckets, r0._size, (((int) currentTimeMillis) + ((int) (currentTimeMillis >>> 32))) | 1, r0._longestCollisionList);
    }

    private C10410k3() {
        this._canonicalize = true;
        this._intern = true;
        this._dirty = true;
        this._hashSeed = 0;
        this._longestCollisionList = 0;
        initTables(this, 64);
    }

    private C10410k3(C10410k3 r3, boolean z, boolean z2, String[] strArr, C10420k4[] r7, int i, int i2, int i3) {
        this._parent = r3;
        this._canonicalize = z;
        this._intern = z2;
        this._symbols = strArr;
        this._buckets = r7;
        this._size = i;
        this._hashSeed = i2;
        int length = strArr.length;
        this._sizeThreshold = length - (length >> 2);
        this._indexMask = length - 1;
        this._longestCollisionList = i3;
        this._dirty = false;
    }
}
