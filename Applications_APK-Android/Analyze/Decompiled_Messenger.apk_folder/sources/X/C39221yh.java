package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.payments.p2p.service.model.request.DeclinePaymentRequestParams;

/* renamed from: X.1yh  reason: invalid class name and case insensitive filesystem */
public final class C39221yh implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new DeclinePaymentRequestParams(parcel);
    }

    public Object[] newArray(int i) {
        return new DeclinePaymentRequestParams[i];
    }
}
