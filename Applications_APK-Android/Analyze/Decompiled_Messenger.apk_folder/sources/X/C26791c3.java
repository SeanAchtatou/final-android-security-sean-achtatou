package X;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.impl.TypeWrappedDeserializer;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/* renamed from: X.1c3  reason: invalid class name and case insensitive filesystem */
public abstract class C26791c3 extends C11270mV implements Serializable {
    private static final long serialVersionUID = -7727373309391091315L;
    public transient C11830o1 _arrayBuilders;
    public final C26841cE _cache;
    public final C10490kF _config;
    public transient DateFormat _dateFormat;
    public final AnonymousClass1c6 _factory;
    public final int _featureFlags;
    public final CYj _injectableValues;
    public transient CZ3 _objectBuffer;
    public transient C28271eX _parser;
    public final Class _view;

    public abstract JsonDeserializer deserializerInstance(C10080jW r1, Object obj);

    public abstract C87454Ev findObjectId(Object obj, C25128CaN caN);

    public abstract C422628y keyDeserializerInstance(C10080jW r1, Object obj);

    public static C37701w6 wrongTokenException(C28271eX r2, C182811d r3, String str) {
        return C37701w6.from(r2, "Unexpected token (" + r2.getCurrentToken() + "), expected " + r3 + ": " + str);
    }

    public final JsonDeserializer findContextualValueDeserializer(C10030jR r3, CX9 cx9) {
        JsonDeserializer findValueDeserializer = this._cache.findValueDeserializer(this, this._factory, r3);
        if (findValueDeserializer == null || !(findValueDeserializer instanceof AnonymousClass13L)) {
            return findValueDeserializer;
        }
        return ((AnonymousClass13L) findValueDeserializer).createContextual(this, cx9);
    }

    public final Object findInjectableValue(Object obj, CX9 cx9, Object obj2) {
        CYj cYj = this._injectableValues;
        if (cYj != null) {
            return cYj.findInjectableValue(obj, this, cx9, obj2);
        }
        throw new IllegalStateException("No 'injectableValues' configured, can not inject value with id [" + obj + "]");
    }

    public final C422628y findKeyDeserializer(C10030jR r4, CX9 cx9) {
        C422628y createKeyDeserializer = this._factory.createKeyDeserializer(this, r4);
        if (createKeyDeserializer == null) {
            throw new C37701w6("Can not find a (Map) Key deserializer for type " + r4);
        }
        if (createKeyDeserializer instanceof C29181g0) {
            ((C29181g0) createKeyDeserializer).resolve(this);
        }
        if (createKeyDeserializer instanceof CZV) {
            return ((CZV) createKeyDeserializer).createContextual(this, cx9);
        }
        return createKeyDeserializer;
    }

    public final JsonDeserializer findRootValueDeserializer(C10030jR r5) {
        JsonDeserializer findValueDeserializer = this._cache.findValueDeserializer(this, this._factory, r5);
        if (findValueDeserializer == null) {
            return null;
        }
        if (findValueDeserializer instanceof AnonymousClass13L) {
            findValueDeserializer = ((AnonymousClass13L) findValueDeserializer).createContextual(this, null);
        }
        C64433Bp findTypeDeserializer = this._factory.findTypeDeserializer(this._config, r5);
        if (findTypeDeserializer != null) {
            return new TypeWrappedDeserializer(findTypeDeserializer.forProperty(null), findValueDeserializer);
        }
        return findValueDeserializer;
    }

    public final C10140jc getAnnotationIntrospector() {
        return this._config.getAnnotationIntrospector();
    }

    public final C11830o1 getArrayBuilders() {
        if (this._arrayBuilders == null) {
            this._arrayBuilders = new C11830o1();
        }
        return this._arrayBuilders;
    }

    public /* bridge */ /* synthetic */ C10470kA getConfig() {
        return this._config;
    }

    public final C10300js getTypeFactory() {
        return this._config._base._typeFactory;
    }

    public final boolean isEnabled(AnonymousClass1c1 r3) {
        if ((r3.getMask() & this._featureFlags) != 0) {
            return true;
        }
        return false;
    }

    public final CZ3 leaseObjectBuffer() {
        CZ3 cz3 = this._objectBuffer;
        if (cz3 == null) {
            return new CZ3();
        }
        this._objectBuffer = null;
        return cz3;
    }

    public Date parseDate(String str) {
        try {
            DateFormat dateFormat = this._dateFormat;
            if (dateFormat == null) {
                dateFormat = (DateFormat) this._config._base._dateFormat.clone();
                this._dateFormat = dateFormat;
            }
            return dateFormat.parse(str);
        } catch (ParseException e) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Failed to parse Date value '", str, "': ", e.getMessage()));
        }
    }

    public final void returnObjectBuffer(CZ3 cz3) {
        int length;
        int length2;
        CZ3 cz32 = this._objectBuffer;
        if (cz32 != null) {
            Object[] objArr = cz3._freeBuffer;
            if (objArr == null) {
                length = 0;
            } else {
                length = objArr.length;
            }
            Object[] objArr2 = cz32._freeBuffer;
            if (objArr2 == null) {
                length2 = 0;
            } else {
                length2 = objArr2.length;
            }
            if (length < length2) {
                return;
            }
        }
        this._objectBuffer = cz3;
    }

    public C37701w6 weirdKeyException(Class cls, String str, String str2) {
        return new C21472AFn(AnonymousClass08S.A0U("Can not construct Map key of type ", cls.getName(), " from String \"", _desc(str), "\": ", str2), this._parser.getTokenLocation(), str, cls);
    }

    public C37701w6 weirdStringException(String str, Class cls, String str2) {
        String str3;
        C28271eX r3 = this._parser;
        String name = cls.getName();
        try {
            str3 = _desc(r3.getText());
        } catch (Exception unused) {
            str3 = "[N/A]";
        }
        return new C21472AFn(AnonymousClass08S.A0U("Can not construct instance of ", name, " from String value '", str3, "': ", str2), r3.getTokenLocation(), str, cls);
    }

    private String _calcName(Class cls) {
        if (cls.isArray()) {
            return AnonymousClass08S.A0J(_calcName(cls.getComponentType()), "[]");
        }
        return cls.getName();
    }

    public static String _desc(String str) {
        int length = str.length();
        if (length > 500) {
            return AnonymousClass08S.A0P(str.substring(0, 500), "]...[", str.substring(length - 500));
        }
        return str;
    }

    public C26791c3(C26791c3 r2, C10490kF r3, C28271eX r4, CYj cYj) {
        this._cache = r2._cache;
        this._factory = r2._factory;
        this._config = r3;
        this._featureFlags = r3._deserFeatures;
        this._view = r3._view;
        this._parser = r4;
        this._injectableValues = cYj;
    }

    public C26791c3(C26791c3 r2, AnonymousClass1c6 r3) {
        this._cache = r2._cache;
        this._factory = r3;
        this._config = r2._config;
        this._featureFlags = r2._featureFlags;
        this._view = r2._view;
        this._parser = r2._parser;
        this._injectableValues = r2._injectableValues;
    }

    public C26791c3(AnonymousClass1c6 r3, C26841cE r4) {
        if (r3 != null) {
            this._factory = r3;
            this._cache = r4 == null ? new C26841cE() : r4;
            this._featureFlags = 0;
            this._config = null;
            this._injectableValues = null;
            this._view = null;
            return;
        }
        throw new IllegalArgumentException("Can not pass null DeserializerFactory");
    }

    public C37701w6 instantiationException(Class cls, String str) {
        return C37701w6.from(this._parser, AnonymousClass08S.A0S("Can not construct instance of ", cls.getName(), ", problem: ", str));
    }

    public C37701w6 instantiationException(Class cls, Throwable th) {
        C64443Bu tokenLocation;
        C28271eX r4 = this._parser;
        String A0S = AnonymousClass08S.A0S("Can not construct instance of ", cls.getName(), ", problem: ", th.getMessage());
        if (r4 == null) {
            tokenLocation = null;
        } else {
            tokenLocation = r4.getTokenLocation();
        }
        return new C37701w6(A0S, tokenLocation, th);
    }

    public C37701w6 mappingException(Class cls) {
        return mappingException(cls, this._parser.getCurrentToken());
    }

    public C37701w6 mappingException(Class cls, C182811d r6) {
        String name;
        if (cls.isArray()) {
            name = AnonymousClass08S.A0J(_calcName(cls.getComponentType()), "[]");
        } else {
            name = cls.getName();
        }
        C28271eX r2 = this._parser;
        return C37701w6.from(r2, "Can not deserialize instance of " + name + " out of " + r6 + " token");
    }

    public C37701w6 mappingException(String str) {
        return C37701w6.from(this._parser, str);
    }
}
