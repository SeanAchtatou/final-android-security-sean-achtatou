package X;

import com.facebook.orca.threadlist.ThreadListFragment;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;

/* renamed from: X.17p  reason: invalid class name and case insensitive filesystem */
public final class C193017p implements C193617v {
    public final /* synthetic */ ThreadListFragment A00;

    public C193017p(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public void BRu() {
        ThreadListFragment threadListFragment = this.A00;
        if (threadListFragment.A1V) {
            threadListFragment.A14.A0C(AnonymousClass1FD.AUTOMATIC_REFRESH, "channelStateChangedReceiver", "ThreadListFragment");
        } else {
            threadListFragment.A1Y = true;
        }
    }

    public void Bow() {
        ThreadListFragment threadListFragment = this.A00;
        if (!threadListFragment.A1V || !threadListFragment.A0i) {
            threadListFragment.A1X = true;
        } else {
            ThreadListFragment.A0G(threadListFragment);
        }
    }

    public void BtR(String str) {
        AnonymousClass1G3 r1;
        AnonymousClass16S r4 = this.A00.A14.A0A;
        ImmutableList.Builder builder = new ImmutableList.Builder();
        ImmutableList.Builder builder2 = new ImmutableList.Builder();
        for (int i = 0; i < r4.A03.A00.size(); i++) {
            if (!((User) r4.A03.A00.get(i)).A0j.equals(str)) {
                builder.add(r4.A03.A00.get(i));
                builder2.add(r4.A03.A01.get(i));
            }
        }
        r4.A03 = new C34801qC(r4.A03.A02, builder.build(), builder2.build());
        if (r4.A08 && (r1 = r4.A04) != null) {
            r1.A00(true);
        }
    }

    public void BtS(boolean z, String str) {
        this.A00.A14.A0D(z);
    }

    public void BtU(String str) {
        AnonymousClass16R r3 = this.A00.A14;
        AnonymousClass1TC r1 = (AnonymousClass1TC) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AEP, r3.A08);
        r1.C6Z(new AnonymousClass1TJ(r3));
        r1.CHB(null);
    }

    public void BtV(AnonymousClass1FD r2, String str, boolean z) {
        this.A00.A14.A0B(r2, str);
    }

    public void Btb(boolean z, boolean z2, String str) {
        if (!z2) {
            C15410vF r2 = this.A00.A16;
            boolean z3 = true;
            if (!r2.A00) {
                r2.A01 = true;
                z3 = false;
            }
            if (!z3) {
                return;
            }
        }
        this.A00.A14.A0E(z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        if (X.C06680bu.A0c.equals(r9) != false) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BtY(java.lang.String r9, X.AnonymousClass1FD r10, java.util.Collection r11, java.lang.String r12) {
        /*
            r8 = this;
            boolean r0 = r11.isEmpty()
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = X.C06680bu.A0e
            boolean r0 = r0.equals(r9)
            if (r0 == 0) goto L_0x0015
            com.facebook.orca.threadlist.ThreadListFragment r0 = r8.A00
            java.util.Set r0 = r0.A1M
            r0.addAll(r11)
        L_0x0015:
            com.facebook.orca.threadlist.ThreadListFragment r3 = r8.A00
            X.0vF r2 = r3.A16
            boolean r1 = r2.A00
            r0 = 1
            if (r1 != 0) goto L_0x0021
            r2.A01 = r0
            r0 = 0
        L_0x0021:
            if (r0 == 0) goto L_0x007d
            X.16R r3 = r3.A14
            X.0Xy r2 = new X.0Xy
            r2.<init>(r11)
            java.lang.String r4 = "threadsUpdatedReceiver"
            java.lang.String r0 = X.C06680bu.A0e
            boolean r0 = r0.equals(r9)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = X.C06680bu.A0c
            boolean r1 = r0.equals(r9)
            r0 = 0
            if (r1 == 0) goto L_0x003e
        L_0x003d:
            r0 = 1
        L_0x003e:
            if (r0 == 0) goto L_0x0093
            java.util.Iterator r7 = r2.iterator()
            r5 = 0
        L_0x0045:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0064
            java.lang.Object r6 = r7.next()
            com.facebook.messaging.model.threadkey.ThreadKey r6 = (com.facebook.messaging.model.threadkey.ThreadKey) r6
            int r2 = X.AnonymousClass1Y3.AuB
            X.0UN r1 = r3.A08
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1bq r0 = (X.C26681bq) r0
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A08(r6)
            r2 = 7
            if (r0 != 0) goto L_0x007e
            r5 = 7
        L_0x0064:
            r0 = r5 & 1
            if (r0 == 0) goto L_0x0076
            X.AnonymousClass16R.A03(r3, r10, r12)
            X.1FD r0 = X.AnonymousClass1FD.EXPLICIT_USER_REFRESH
            if (r10 != r0) goto L_0x0073
            r0 = 1
            r3.A0E(r0)
        L_0x0073:
            X.AnonymousClass16R.A02(r3, r10, r12)
        L_0x0076:
            r0 = r5 & 2
            if (r0 == 0) goto L_0x007d
        L_0x007a:
            r3.A0B(r10, r4)
        L_0x007d:
            return
        L_0x007e:
            X.0l8 r1 = r0.A0O
            X.0l8 r0 = X.C10950l8.A06
            if (r1 != r0) goto L_0x0089
            r5 = r5 | 2
        L_0x0086:
            if (r5 != r2) goto L_0x0045
            goto L_0x0064
        L_0x0089:
            X.0l8 r0 = X.C10950l8.A0A
            if (r1 != r0) goto L_0x0090
            r5 = r5 | 4
            goto L_0x0086
        L_0x0090:
            r5 = r5 | 1
            goto L_0x0086
        L_0x0093:
            java.lang.String r0 = X.C06680bu.A0Z
            boolean r0 = r0.equals(r9)
            if (r0 == 0) goto L_0x00b3
            r2 = 2
            r0 = r2 & 1
            if (r0 == 0) goto L_0x00ae
            X.AnonymousClass16R.A03(r3, r10, r12)
            X.1FD r0 = X.AnonymousClass1FD.EXPLICIT_USER_REFRESH
            if (r10 != r0) goto L_0x00ab
            r0 = 1
            r3.A0E(r0)
        L_0x00ab:
            X.AnonymousClass16R.A02(r3, r10, r12)
        L_0x00ae:
            r0 = r2 & 2
            if (r0 == 0) goto L_0x007d
            goto L_0x007a
        L_0x00b3:
            r3.A0C(r10, r4, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C193017p.BtY(java.lang.String, X.1FD, java.util.Collection, java.lang.String):void");
    }
}
