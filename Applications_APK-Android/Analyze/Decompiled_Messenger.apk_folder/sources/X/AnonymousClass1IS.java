package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1IS  reason: invalid class name */
public final class AnonymousClass1IS implements AnonymousClass1IT {
    public final int A00;
    public final C21541Hs A01;
    public final C21551Ht A02;
    public final List A03;
    public final List A04;
    public final List A05;
    public final List A06;
    public final List A07;

    public void BRm(int i, int i2, Object obj) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList(i2);
        int i3 = 0;
        while (true) {
            int i4 = i;
            if (i3 < i2) {
                int i5 = i + i3;
                AnonymousClass1IV r1 = (AnonymousClass1IV) this.A06.get(i5);
                r1.A01 = true;
                arrayList.add(r1);
                arrayList2.add(this.A03.get(i5));
                i3++;
            } else {
                this.A05.add(new AnonymousClass1IW(1, i4, -1, arrayList, arrayList2));
                return;
            }
        }
    }

    public void BbO(int i, int i2) {
        ArrayList arrayList = new ArrayList(i2);
        ArrayList arrayList2 = new ArrayList(i2);
        int i3 = 0;
        while (true) {
            int i4 = i;
            if (i3 < i2) {
                int i5 = i + i3;
                AnonymousClass1IV r1 = new AnonymousClass1IV(null, true);
                this.A06.add(i5, r1);
                arrayList.add(r1);
                C33111mx r12 = new C33111mx(null, null);
                this.A03.add(i5, r12);
                arrayList2.add(r12);
                i3++;
            } else {
                this.A05.add(new AnonymousClass1IW(0, i4, -1, arrayList, arrayList2));
                return;
            }
        }
    }

    public void Bfk(int i, int i2) {
        ArrayList arrayList = new ArrayList(1);
        this.A06.add(i2, (AnonymousClass1IV) this.A06.remove(i));
        C33111mx r1 = (C33111mx) this.A03.remove(i);
        arrayList.add(r1);
        this.A03.add(i2, r1);
        this.A05.add(new AnonymousClass1IW(3, i, i2, null, arrayList));
    }

    public void Bld(int i, int i2) {
        int i3 = i2;
        ArrayList arrayList = new ArrayList(i2);
        int i4 = 0;
        while (true) {
            int i5 = i;
            if (i4 < i2) {
                this.A06.remove(i);
                arrayList.add((C33111mx) this.A03.remove(i));
                i4++;
            } else {
                this.A05.add(new AnonymousClass1IW(2, i5, i3, null, arrayList));
                return;
            }
        }
    }

    public AnonymousClass1IS(List list, List list2, C21541Hs r9, C21551Ht r10) {
        int i;
        this.A07 = list;
        if (list != null) {
            i = list.size();
        } else {
            i = 0;
        }
        this.A00 = i;
        this.A04 = list2;
        this.A01 = r9;
        this.A02 = r10;
        this.A05 = new ArrayList();
        this.A06 = new ArrayList();
        this.A03 = new ArrayList();
        for (int i2 = 0; i2 < this.A00; i2++) {
            this.A06.add(new AnonymousClass1IV(null, false));
            this.A03.add(new C33111mx(this.A07.get(i2), null));
        }
    }
}
