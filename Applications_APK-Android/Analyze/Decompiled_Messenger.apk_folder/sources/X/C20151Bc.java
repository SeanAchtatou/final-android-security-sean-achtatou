package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.1Bc  reason: invalid class name and case insensitive filesystem */
public final class C20151Bc {
    private static C04470Uu A01;
    public final C20161Bd A00;

    public static final C20151Bc A00(AnonymousClass1XY r4) {
        C20151Bc r0;
        synchronized (C20151Bc.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C20151Bc((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C20151Bc) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C20151Bc(AnonymousClass1XY r2) {
        this.A00 = C20161Bd.A01(r2);
    }
}
