package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1v3  reason: invalid class name and case insensitive filesystem */
public final class C37301v3 extends C17770zR {
    public static final int A0B = (AnonymousClass1JQ.LARGE.B3A() << 1);
    public static final int A0C = (AnonymousClass1JQ.MEDIUM.B3A() << 1);
    @Comparable(type = 3)
    public int A00;
    @Comparable(type = 3)
    public int A01;
    @Comparable(type = 13)
    public Drawable A02;
    @Comparable(type = 13)
    public View.OnClickListener A03;
    @Comparable(type = 13)
    public View.OnClickListener A04;
    @Comparable(type = 13)
    public C43842Fx A05;
    @Comparable(type = 13)
    public C202449gP A06;
    @Comparable(type = 14)
    public AnonymousClass5CM A07 = new AnonymousClass5CM();
    @Comparable(type = 13)
    public MigColorScheme A08;
    @Comparable(type = 13)
    public CharSequence A09;
    @Comparable(type = 13)
    public String A0A;

    public C37301v3() {
        super("RequestCodeComponent");
    }

    public C17770zR A16() {
        C37301v3 r1 = (C37301v3) super.A16();
        r1.A07 = new AnonymousClass5CM();
        return r1;
    }
}
