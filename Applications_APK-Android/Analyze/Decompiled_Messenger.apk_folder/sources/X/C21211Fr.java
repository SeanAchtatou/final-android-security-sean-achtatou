package X;

import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1Fr  reason: invalid class name and case insensitive filesystem */
public final class C21211Fr extends AnonymousClass1H5<Map.Entry<K, V>> {
    public final /* synthetic */ C21111Fg A00;

    public C21211Fr(C21111Fg r1) {
        this.A00 = r1;
    }

    public Iterator iterator() {
        return new AnonymousClass0r1(this, this.A00.A00.iterator());
    }
}
