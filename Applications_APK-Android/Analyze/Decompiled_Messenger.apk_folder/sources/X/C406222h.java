package X;

import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.graphql.query.GQSQStringShape1S0000000_I1;

/* renamed from: X.22h  reason: invalid class name and case insensitive filesystem */
public final class C406222h implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.facecast.livingroom.videostate.fetcher.impl.delegate.LivingRoomVideoFetcherImplDelegate$6";
    public final /* synthetic */ ViewerContext A00;
    public final /* synthetic */ AnonymousClass8FQ A01;
    public final /* synthetic */ String A02;

    public C406222h(AnonymousClass8FQ r1, String str, ViewerContext viewerContext) {
        this.A01 = r1;
        this.A02 = str;
        this.A00 = viewerContext;
    }

    public void run() {
        synchronized (this.A01) {
            GQSQStringShape1S0000000_I1 gQSQStringShape1S0000000_I1 = new GQSQStringShape1S0000000_I1(4);
            gQSQStringShape1S0000000_I1.A09("living_room_id", this.A02);
            C26931cN A002 = C26931cN.A00(gQSQStringShape1S0000000_I1);
            A002.A0B(C09290gx.NETWORK_ONLY);
            ViewerContext viewerContext = this.A00;
            if (viewerContext != null) {
                A002.A0A(viewerContext);
            }
            AnonymousClass8FQ r3 = this.A01;
            r3.A07 = ((C11170mD) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ASp, r3.A00)).A04(A002);
            AnonymousClass8FQ r1 = this.A01;
            r1.A04 = new C176758Ef(r1);
            C05350Yp.A08(this.A01.A07, this.A01.A04, C25141Ym.INSTANCE);
        }
    }
}
