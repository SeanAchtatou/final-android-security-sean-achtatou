package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.1GP  reason: invalid class name */
public final class AnonymousClass1GP implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.loader.AbstractListenableFutureFbLoader$3";
    public final /* synthetic */ AnonymousClass1EP A00;
    public final /* synthetic */ ListenableFuture A01;
    public final /* synthetic */ Object A02;

    public AnonymousClass1GP(AnonymousClass1EP r1, Object obj, ListenableFuture listenableFuture) {
        this.A00 = r1;
        this.A02 = obj;
        this.A01 = listenableFuture;
    }

    public void run() {
        this.A00.A01.BdU(this.A02, this.A01);
    }
}
