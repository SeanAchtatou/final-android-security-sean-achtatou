package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.content.SecureContextHelper;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Bz  reason: invalid class name and case insensitive filesystem */
public final class C20331Bz {
    private static volatile C20331Bz A01;
    public AnonymousClass0UN A00;

    public boolean A02(Context context, InterstitialTrigger interstitialTrigger) {
        return A01(this, context, interstitialTrigger, null, null, null);
    }

    public boolean A03(Context context, InterstitialTrigger interstitialTrigger, Class cls, Object obj) {
        return A01(this, context, interstitialTrigger, cls, obj, null);
    }

    public static final C20331Bz A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C20331Bz.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C20331Bz(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static boolean A01(C20331Bz r5, Context context, InterstitialTrigger interstitialTrigger, Class cls, Object obj, C35731rh r10) {
        boolean z;
        int i = AnonymousClass1Y3.BE8;
        AnonymousClass1F1 r0 = (AnonymousClass1F1) AnonymousClass1XX.A02(0, i, r5.A00);
        if (cls == null) {
            cls = C32821mO.class;
        }
        C32821mO A0N = r0.A0N(interstitialTrigger, cls);
        if (A0N == null || ((A0N instanceof C132156Gm) && !((C132156Gm) A0N).A01())) {
            z = false;
        } else {
            z = true;
        }
        if (!z) {
            return false;
        }
        ((AnonymousClass1F1) AnonymousClass1XX.A02(0, i, r5.A00)).A0R().A02(A0N.Aqc());
        if (r10 != null) {
            r10.Bbj(A0N.Aqc());
        }
        if (A0N instanceof C21791Is) {
            ((C21791Is) A0N).Bwu(context, interstitialTrigger, obj);
            return true;
        } else if (A0N instanceof C22281Ks) {
            Intent A03 = ((C22281Ks) A0N).A03(context);
            if (A03 == null) {
                return false;
            }
            A03.setFlags(268435456 | A03.getFlags());
            ((SecureContextHelper) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A7S, r5.A00)).startFacebookActivity(A03, context);
            return true;
        } else {
            throw new RuntimeException("Unknown InterstitialController: " + A0N);
        }
    }

    private C20331Bz(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
