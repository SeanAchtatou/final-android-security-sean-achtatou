package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0rP  reason: invalid class name and case insensitive filesystem */
public final class C13420rP {
    private static volatile C13420rP A01;
    private AnonymousClass0UN A00;

    public static final C13420rP A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C13420rP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C13420rP(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C13420rP(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    public AnonymousClass13r A01(C16040wO r4) {
        int i;
        switch (r4.ordinal()) {
            case 0:
                return (C184813q) AnonymousClass1XX.A03(AnonymousClass1Y3.BIB, this.A00);
            case 1:
                i = AnonymousClass1Y3.A45;
                break;
            case 2:
                i = AnonymousClass1Y3.Aah;
                break;
            case 3:
                i = AnonymousClass1Y3.A5l;
                break;
            default:
                throw new IllegalArgumentException("No binding for tab type " + r4);
        }
        return (AnonymousClass13r) AnonymousClass1XX.A03(i, this.A00);
    }
}
