package X;

import android.net.Uri;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/* renamed from: X.06i  reason: invalid class name and case insensitive filesystem */
public final class C006306i {
    public static final C29761gw A00 = new C29761gw(100);
    public static final ThreadLocal A01 = new ThreadLocal();
    public static final Set A02;
    private static final Pattern A03 = Pattern.compile("^(z-.*)?(origincache.*|cdn).fbsbx.com$");

    public static boolean A02(Uri uri) {
        String queryParameter;
        if (uri != null) {
            return (A04(uri) || A05(uri)) && (queryParameter = uri.getQueryParameter("oe")) != null && Long.parseLong(queryParameter, 16) * 1000 < System.currentTimeMillis();
        }
        return false;
    }

    public static boolean A03(Uri uri) {
        String host;
        if (uri == null || (host = uri.getHost()) == null || !A06(uri)) {
            return false;
        }
        return (host.endsWith(".facebook.com") || host.equals("facebook.com") || host.equals("fb.me")) && !host.startsWith("our.intern.");
    }

    static {
        HashSet hashSet = new HashSet(6);
        Collections.addAll(hashSet, "__gda__", "oh", "oe", "hdnea", "logcdn", "efg");
        A02 = Collections.unmodifiableSet(hashSet);
    }

    public static boolean A06(Uri uri) {
        if (uri == null) {
            return false;
        }
        if ("http".equals(uri.getScheme()) || "https".equals(uri.getScheme())) {
            return true;
        }
        return false;
    }

    public static Uri A01(Uri uri) {
        while (A07(uri)) {
            String queryParameter = uri.getQueryParameter("u");
            if (TextUtils.isEmpty(queryParameter)) {
                break;
            }
            uri = Uri.parse(queryParameter);
        }
        return uri;
    }

    public static boolean A04(Uri uri) {
        String host;
        if (!A06(uri) || (host = uri.getHost()) == null || (!host.endsWith(".fbcdn.net") && (!host.endsWith(".akamaihd.net") || (!host.startsWith("fbcdn-") && !host.startsWith("fbstatic-") && !host.startsWith("fbexternal-") && !host.startsWith("fb-"))))) {
            return false;
        }
        return true;
    }

    public static boolean A05(Uri uri) {
        String host;
        if (!A06(uri) || (host = uri.getHost()) == null || !A03.matcher(host).matches()) {
            return false;
        }
        return true;
    }

    public static boolean A07(Uri uri) {
        if (!A03(uri) || !uri.getPath().equals("/l.php")) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public static Uri A00(Uri uri) {
        String sb;
        AnonymousClass00C.A01(68719476736L, "FacebookUriUtil#getNormalizedUriForCache", -11678734);
        Uri uri2 = (Uri) A00.A03(uri);
        if (uri2 != null) {
            AnonymousClass00C.A00(68719476736L, -1705242903);
            return uri2;
        }
        Uri uri3 = uri;
        AnonymousClass00C.A01(68719476736L, "FacebookUriUtil#normalizeUriForCache", 1441745691);
        boolean z = false;
        if (A06(uri)) {
            String host = uri.getHost();
            if (host.endsWith(".net")) {
                if (host.contains(".fbcdn.")) {
                    z = !host.contains("sonar.");
                } else if (host.endsWith(".akamaihd.net") && (host.startsWith("fbcdn-") || host.startsWith("fb-"))) {
                    z = true;
                }
            }
        }
        if (!z) {
            AnonymousClass00C.A00(68719476736L, 1570166487);
        } else {
            AnonymousClass00C.A00(68719476736L, 374264106);
            AnonymousClass00C.A01(68719476736L, "FacebookUriUtil#doNormalize", -472146355);
            String encodedQuery = uri.getEncodedQuery();
            String[] strArr = null;
            if (encodedQuery != null) {
                ArrayList arrayList = new ArrayList();
                int length = encodedQuery.length();
                int i = 0;
                do {
                    int indexOf = encodedQuery.indexOf(38, i);
                    if (indexOf == -1) {
                        indexOf = length;
                    }
                    int indexOf2 = encodedQuery.indexOf(61, i);
                    if (indexOf2 > indexOf || indexOf2 == -1) {
                        indexOf2 = indexOf;
                    }
                    String substring = encodedQuery.substring(i, indexOf2);
                    if (!substring.startsWith("_nc_") && !A02.contains(substring)) {
                        arrayList.add(Uri.decode(substring));
                    }
                    i = indexOf + 1;
                } while (i < length);
                if (!arrayList.isEmpty()) {
                    strArr = (String[]) arrayList.toArray(new String[0]);
                    Arrays.sort(strArr);
                }
            }
            String encodedUserInfo = uri.getEncodedUserInfo();
            int port = uri.getPort();
            String str = "fbcdn.net";
            if (!(encodedUserInfo == null && port == -1)) {
                StringBuilder sb2 = new StringBuilder();
                if (encodedUserInfo != null) {
                    sb2.append(encodedUserInfo);
                    sb2.append('@');
                }
                sb2.append(str);
                if (port != -1) {
                    sb2.append(':');
                    sb2.append(port);
                }
                str = sb2.toString();
            }
            String encodedPath = uri.getEncodedPath();
            int indexOf3 = encodedPath.indexOf("/h");
            int i2 = 1;
            if (indexOf3 == -1) {
                sb = encodedPath.substring(1);
            } else {
                if (indexOf3 == 0) {
                    i2 = 1 + encodedPath.indexOf(47, 2);
                }
                ThreadLocal threadLocal = A01;
                StringBuilder sb3 = (StringBuilder) threadLocal.get();
                if (sb3 == null) {
                    sb3 = new StringBuilder();
                    threadLocal.set(sb3);
                }
                sb3.setLength(0);
                while (i2 >= 0) {
                    int indexOf4 = encodedPath.indexOf("/h", i2);
                    if (indexOf4 == -1) {
                        break;
                    }
                    sb3.append((CharSequence) encodedPath, i2, indexOf4);
                    i2 = encodedPath.indexOf(47, indexOf4 + 1);
                }
                if (i2 >= 0) {
                    sb3.append((CharSequence) encodedPath, i2, encodedPath.length());
                }
                sb = sb3.toString();
            }
            Uri.Builder appendEncodedPath = new Uri.Builder().scheme(uri.getScheme()).encodedAuthority(str).appendEncodedPath(sb);
            if (strArr != null) {
                for (String str2 : strArr) {
                    appendEncodedPath.appendQueryParameter(str2, uri.getQueryParameter(str2));
                }
            }
            appendEncodedPath.fragment(uri.getFragment());
            AnonymousClass00C.A00(68719476736L, -303687933);
            uri3 = appendEncodedPath.build();
        }
        A00.A05(uri, uri3);
        AnonymousClass00C.A00(68719476736L, -163899663);
        return uri3;
    }
}
