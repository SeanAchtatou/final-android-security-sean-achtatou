package X;

import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.17h  reason: invalid class name and case insensitive filesystem */
public final class C192217h implements C192317i {
    private int A00;
    private QuickPerformanceLogger A01;
    private String A02;

    public void BWM() {
        this.A01.markerAnnotate(this.A00, "module", this.A02);
        this.A01.markerEnd(this.A00, 2);
    }

    public void BXb() {
        this.A01.markerStart(this.A00);
    }

    public void Bln(C73873gu r5) {
        C21061Ew withMarker = this.A01.withMarker(this.A00);
        withMarker.A04("sfd", r5.A01);
        withMarker.A04("lfd", r5.A00);
        withMarker.A06("ts", r5.A02);
        withMarker.BK9();
    }

    public C192217h(QuickPerformanceLogger quickPerformanceLogger, int i, String str) {
        this.A01 = quickPerformanceLogger;
        this.A00 = i;
        this.A02 = str;
    }
}
