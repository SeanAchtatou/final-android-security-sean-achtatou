package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.litho.ComponentTree;
import com.facebook.litho.LithoView;
import java.util.List;

/* renamed from: X.1Ec  reason: invalid class name and case insensitive filesystem */
public final class C20861Ec extends C20871Ed implements C20881Ee {
    public C20221Bj A00;
    public boolean A01 = false;
    public final RecyclerView A02;
    public final LithoView A03;

    public C20861Ec(Context context, RecyclerView recyclerView) {
        super(context, null);
        this.A02 = recyclerView;
        recyclerView.A0w(new C61502yz());
        this.A02.A0k(0);
        addView(this.A02);
        LithoView lithoView = new LithoView(new AnonymousClass0p4(getContext()), (AttributeSet) null);
        this.A03 = lithoView;
        lithoView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        addView(this.A03);
    }

    public void BMi(List list) {
        int childCount = this.A02.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.A02.getChildAt(i);
            if (childAt instanceof LithoView) {
                list.add((LithoView) childAt);
            }
        }
    }

    public void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.A02.setOnTouchListener(onTouchListener);
    }

    public void A0E(ComponentTree componentTree) {
        if (componentTree.getLithoView() != null) {
            LithoView lithoView = componentTree.getLithoView();
            lithoView.A04 = lithoView.A03;
        }
        this.A03.A0b(componentTree);
        measureChild(this.A03, View.MeasureSpec.makeMeasureSpec(getWidth(), 1073741824), 0);
    }

    public boolean isLayoutRequested() {
        if (getParent() == null) {
            return super.isLayoutRequested();
        }
        if (getParent().isLayoutRequested() || super.isLayoutRequested()) {
            return true;
        }
        return false;
    }

    public void onDetachedFromWindow() {
        int A06 = C000700l.A06(613349657);
        super.onDetachedFromWindow();
        this.A01 = true;
        C000700l.A0C(1449575207, A06);
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.A03.getVisibility() != 8) {
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            LithoView lithoView = this.A03;
            lithoView.layout(paddingLeft, paddingTop, lithoView.getMeasuredWidth() + paddingLeft, this.A03.getMeasuredHeight() + paddingTop);
        }
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        measureChild(this.A03, View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i), 1073741824), 0);
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (getParent() != null && !isNestedScrollingEnabled()) {
            getParent().requestDisallowInterceptTouchEvent(z);
        }
    }
}
