package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;

/* renamed from: X.227  reason: invalid class name */
public final class AnonymousClass227 extends C403921k {
    private final C28131eJ A00;

    public static final AnonymousClass227 A00(AnonymousClass1XY r4) {
        return new AnonymousClass227(AnonymousClass0WT.A00(r4), C43882Gb.A00(r4), C04920Ww.A00(r4));
    }

    public boolean A09(Intent intent, int i, Activity activity) {
        return this.A00.A09(intent, i, activity);
    }

    public boolean A0A(Intent intent, int i, Fragment fragment) {
        return this.A00.A0A(intent, i, fragment);
    }

    public boolean A0B(Intent intent, Context context) {
        return this.A00.A0B(intent, context);
    }

    private AnonymousClass227(C25051Yd r5, C43882Gb r6, AnonymousClass09P r7) {
        this.A00 = new C44062Gt(r6, new C14130sf(C008807t.A00(r5.At0(566587790788306L)), new AnonymousClass21E(r7, "DefaultExternalIntentHandler")));
    }
}
