package X;

import com.facebook.common.callercontext.CallerContextable;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1hm  reason: invalid class name and case insensitive filesystem */
public final class C30271hm extends C10740km implements CallerContextable {
    private static volatile C30271hm A07 = null;
    public static final String __redex_internal_original_name = "com.facebook.http.protocol.SingleMethodRunnerImpl";
    public AnonymousClass0UN A00;
    public final C04310Tq A01;
    public final C04310Tq A02;
    public final C04310Tq A03;
    private final C04310Tq A04;
    private final C04310Tq A05;
    private final C04310Tq A06;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r6 == null) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r6 == null) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        if ((r6 instanceof X.C47712Xn) == false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        r7 = (X.C47712Xn) r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0033, code lost:
        r0 = r3.A09(r4, r5, r6, r7, r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
        r7 = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object A01(X.C30271hm r8, X.AnonymousClass2W8 r9, X.C47702Xm r10, java.lang.Object r11, X.C47692Xl r12, com.facebook.common.callercontext.CallerContext r13) {
        /*
            r6 = r9
            r3 = r8
            r8 = r11
            r9 = r13
            r5 = r12
            if (r6 == 0) goto L_0x0008
            goto L_0x000a
        L_0x0008:
            r7 = 0
            goto L_0x0011
        L_0x000a:
            boolean r0 = r6 instanceof X.C47712Xn     // Catch:{ 3cE -> 0x001e, 3cF -> 0x0017 }
            if (r0 == 0) goto L_0x0008
            r7 = r6
            X.2Xn r7 = (X.C47712Xn) r7     // Catch:{ 3cE -> 0x001e, 3cF -> 0x0017 }
        L_0x0011:
            r4 = r10
            X.2aY r0 = r3.A09(r4, r5, r6, r7, r8, r9)     // Catch:{ 3cE -> 0x001e, 3cF -> 0x0017 }
            goto L_0x0037
        L_0x0017:
            X.2Xm r4 = r6.B0t(r11)
            if (r6 == 0) goto L_0x003a
            goto L_0x002c
        L_0x001e:
            r2 = move-exception
            java.lang.String r1 = "SingleMethodRunnerImpl"
            java.lang.String r0 = "Invalid persisted graphql query id"
            X.C010708t.A0T(r1, r2, r0)
            X.2Xm r4 = r6.B0t(r11)
            if (r6 == 0) goto L_0x003a
        L_0x002c:
            boolean r0 = r6 instanceof X.C47712Xn
            if (r0 == 0) goto L_0x003a
            r7 = r6
            X.2Xn r7 = (X.C47712Xn) r7
        L_0x0033:
            X.2aY r0 = r3.A09(r4, r5, r6, r7, r8, r9)
        L_0x0037:
            java.lang.Object r0 = r0.A00
            return r0
        L_0x003a:
            r7 = 0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30271hm.A01(X.1hm, X.2W8, X.2Xm, java.lang.Object, X.2Xl, com.facebook.common.callercontext.CallerContext):java.lang.Object");
    }

    public static final C30271hm A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C30271hm.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C30271hm(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    private boolean A02(C47702Xm r23) {
        C47702Xm r9 = r23;
        String str = r9.A0E;
        String str2 = str;
        if (!"accountRecoveryFrrNonce".equals(str)) {
            String str3 = r9.A0I;
            if (!"aldrin_logged_out_status".equalsIgnoreCase(str3)) {
                String str4 = str2;
                if (!"getLanguagePackInfo".equals(str4) && !"getQTLanguagePack".equals(str4) && !"method/logging.clientevent".equalsIgnoreCase(str3) && !"logging_client_events".equalsIgnoreCase(str3)) {
                    String str5 = str2;
                    if (!"secured_action_asset_uri_fetch".equals(str5) && !"dbl_remove_nonce".equals(str5) && !"dbl_remove_identity".equals(str5) && !"dbl_change_nonce".equals(str5) && !"dbl_check_nonce".equals(str5) && !"dbl_check_password".equals(str5) && !"pwdKeyFetch".equals(str5) && !"determine_user_type".equals(str5) && !"ig_authenticate".equals(str5) && !"dbl_password_set_nonce".equals(str5) && !"pymb_get_accounts".equals(str5) && !"pymb_blacklist_suggestion".equals(str5) && !"messenger_invites".equals(str5) && !"recover_accounts".equalsIgnoreCase(str3)) {
                        String str6 = str2;
                        if (!"accountRecoverySendConfirmationCode".equals(str6) && !"accountRecoveryValidateCode".equals(str6) && !"openidConnectAccountRecovery".equals(str6) && !"accountRecoveryShortUrlHandler".equals(str6) && !"accountRecoveryloginHelpNotif".equals(str6) && !"accountRecoveryNewEmails".equals(str6) && !"authenticityUploads".equals(str6) && !"at_work_self_invite".equals(str6) && !"at_work_accounts_send_notification".equals(str6) && !"at_work_accounts_info".equals(str6) && !"at_work_accounts_claim_info".equals(str6) && !"at_work_contact_point_verification".equals(str6) && !"checkApprovedMachine".equals(str6) && !"login_approval_resend_code".equals(str6) && !"talk_first_party_sso_context_method".equals(str6) && !"at_work_pre_login_info".equals(str6) && !"at_work_invite_check".equals(str6) && !"at_work_invite_company_info".equals(str6) && !"at_work_claim_account".equals(str6) && !"at_work_company_creation_with_invite".equals(str6) && !"at_work_company_creation".equals(str6) && !"at_work_company_creation_info".equals(str6) && !"at_work_accounts_self_invite".equals(str6) && !"at_work_accounts_claim".equals(str6) && !"at_work_access_code_info".equals(str6) && !"at_work_username_check".equals(str6) && !"install_notifier".equals(str6) && !"moments_folder".equalsIgnoreCase(str3) && !"moments_phone_confirmation_code".equalsIgnoreCase(str3) && !"check_moments_phone_confirmation_code".equalsIgnoreCase(str3) && !"register_moments_only_user".equalsIgnoreCase(str3) && !"check_phone_has_moments_account".equalsIgnoreCase(str3) && !"moments_aldrin_wechat_login".equalsIgnoreCase(str3) && !"moments_account_recovery_verify_confirmation_code".equalsIgnoreCase(str3) && !"moments_account_recovery_confirmation_code".equalsIgnoreCase(str3) && !"moments_account_recovery_reset_password".equalsIgnoreCase(str3) && !"moments_account_recovery_determine_eligibility".equalsIgnoreCase(str3) && !"moments_login_device".equalsIgnoreCase(str3) && !"parties/phone_registration".equalsIgnoreCase(str3) && !"parties/check_phone_has_accounts".equalsIgnoreCase(str3) && !"parties/check_confirmation_code".equalsIgnoreCase(str3) && !"parties/confirm_phone".equalsIgnoreCase(str3) && !"parties/account_recovery_send_confirmation_code".equalsIgnoreCase(str3) && !"parties/account_recovery_verify_confirmation_code".equalsIgnoreCase(str3) && !"parties/linked_accounts".equalsIgnoreCase(str3) && !"parties/create_account".equalsIgnoreCase(str3) && !"parties/phone_reset_password".equalsIgnoreCase(str3) && !"parties/social_context".equalsIgnoreCase(str3) && !"parties/check_username".equalsIgnoreCase(str3)) {
                            String str7 = str2;
                            if (!"logged_out_push".equals(str7) && !"logged_out_badge".equals(str7) && !"logged_out_value_props".equals(str7) && (!"UserSemClickTrackingMutation".equals(str2) || ((AnonymousClass0XN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ArW, this.A00)).A0I())) {
                                String str8 = r9.A0E;
                                if (!"get_linked_fb_user_from_ig_session".equals(str8) && !"RainbowFeedQuery".equals(str8) && !"install_notifier".equals(str8)) {
                                    String str9 = r9.A0I;
                                    if (!"moments_folder".equalsIgnoreCase(str9) && !"moments_phone_confirmation_code".equalsIgnoreCase(str9) && !"check_moments_phone_confirmation_code".equalsIgnoreCase(str9) && !"register_moments_only_user".equalsIgnoreCase(str9) && !"check_phone_has_moments_account".equalsIgnoreCase(str9) && !"moments_aldrin_wechat_login".equalsIgnoreCase(str9) && !"moments_account_recovery_verify_confirmation_code".equalsIgnoreCase(str9) && !"moments_account_recovery_confirmation_code".equalsIgnoreCase(str9) && !"moments_account_recovery_reset_password".equalsIgnoreCase(str9) && !"moments_account_recovery_determine_eligibility".equalsIgnoreCase(str9) && !"moments_login_device".equalsIgnoreCase(str9) && !"register_messenger_only_user".equals(str9) && !"login_softmatched_messenger_only_user".equals(str9) && !"logged_out_push".equals(str8) && !"logged_out_badge".equals(str8) && (!"UserSemClickTrackingMutation".equals(str8) || ((AnonymousClass0XN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ArW, this.A00)).A0I())) {
                                        String str10 = r9.A0E;
                                        if ("get_linked_fb_user_from_ig_session".equals(str10) || "RainbowFeedQuery".equals(str10) || "sale_group_threads".equals(str10) || "fetchZeroHeaderRequest".equals(str10) || "talk_validate_parent_request".equals(str10) || "FB4ANonceLoginInterestialContentsQuery".equals(str10) || "headersConfigurationParams".equals(str10) || "headersConfigurationParamsV2".equals(str10) || "browser_to_native_sso_token_fetch".equals(str10)) {
                                            return true;
                                        }
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean A03(C47702Xm r3) {
        String str = r3.A0E;
        if ("at_work_claim_account".equals(str) || "at_work_company_creation_with_invite".equals(str) || "at_work_company_creation".equals(str) || "at_work_company_creation_info".equals(str) || "at_work_accounts_self_invite".equals(str) || "at_work_accounts_claim".equals(str) || "at_work_fetch_account_details".equals(str) || "FetchEmailQuery".equals(str) || "logout".equals(str) || "FetchFirstPartyUserData".equals(str)) {
            return true;
        }
        if (!AnonymousClass24B.$const$string(848).equals(str) || !((Boolean) this.A05.get()).booleanValue()) {
            return false;
        }
        return true;
    }

    private static boolean A04(C47702Xm r3) {
        if (r3.A0R || !r3.A0L) {
            return false;
        }
        if (r3.A00) {
            return true;
        }
        String str = r3.A0I;
        if (!AnonymousClass80H.$const$string(69).equalsIgnoreCase(str) && !"method/user.confirmContactpointPreconfirmation".equalsIgnoreCase(str) && !"method/user.initiatePreconfirmation".equalsIgnoreCase(str) && !"method/user.register".equalsIgnoreCase(str) && !"method/user.prefillorautocompletecontactpoint".equalsIgnoreCase(str) && !"method/user.resetPasswordPreconfirmation".equalsIgnoreCase(str) && !"method/user.validateregistrationdata".equalsIgnoreCase(str) && !"method/user.sendMessengerOnlyPhoneConfirmationCode".equalsIgnoreCase(str) && !"method/user.confirmMessengerOnlyPhone".equalsIgnoreCase(str) && !"method/user.createMessengerOnlyAccount".equalsIgnoreCase(str) && !"method/user.bypassLoginWithConfirmedMessengerCredentials".equalsIgnoreCase(str) && !"register_messenger_only_user".equalsIgnoreCase(str) && !"login_softmatched_messenger_only_user".equalsIgnoreCase(str) && !"method/intl.getLocaleSuggestions".equalsIgnoreCase(str)) {
            String str2 = r3.A0E;
            if ("accountRecoverySendConfirmationCode".equals(str2) || "accountRecoveryValidateCode".equals(str2) || "accountRecoveryNewEmails".equals(str2) || "authenticityUploads".equalsIgnoreCase(str2)) {
                return true;
            }
            return false;
        }
        return true;
    }

    private C30271hm(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(16, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.B9n, r3);
        this.A06 = C10580kT.A03(r3);
        this.A04 = AnonymousClass0VG.A00(AnonymousClass1Y3.ANs, r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.AQr, r3);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.AJY, r3);
        AnonymousClass0VG.A00(AnonymousClass1Y3.AcD, r3);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.AUm, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:184:0x0336, code lost:
        if (((X.BPA) r5).mIsTailing == false) goto L_0x0338;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:0x03f7, code lost:
        if (r1.equals(r2) != false) goto L_0x03f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:319:0x0611, code lost:
        if ("fetchZeroHeaderRequest".equals(r2) != false) goto L_0x0613;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:321:0x0614, code lost:
        if (r0 == false) goto L_0x0616;
     */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x050e  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:325:0x0623  */
    /* JADX WARNING: Removed duplicated region for block: B:327:0x0629  */
    /* JADX WARNING: Removed duplicated region for block: B:330:0x0632  */
    /* JADX WARNING: Removed duplicated region for block: B:336:0x064a  */
    /* JADX WARNING: Removed duplicated region for block: B:342:0x0664  */
    /* JADX WARNING: Removed duplicated region for block: B:345:0x067f  */
    /* JADX WARNING: Removed duplicated region for block: B:348:0x06a0  */
    /* JADX WARNING: Removed duplicated region for block: B:351:0x06b6  */
    /* JADX WARNING: Removed duplicated region for block: B:359:0x06e2  */
    /* JADX WARNING: Removed duplicated region for block: B:362:0x06ed  */
    /* JADX WARNING: Removed duplicated region for block: B:369:0x0764  */
    /* JADX WARNING: Removed duplicated region for block: B:378:0x0784 A[SYNTHETIC, Splitter:B:378:0x0784] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C48512aY A09(X.C47702Xm r28, X.C47692Xl r29, X.C10810kt r30, X.C47712Xn r31, java.lang.Object r32, com.facebook.common.callercontext.CallerContext r33) {
        /*
            r27 = this;
            r6 = r27
            r7 = r29
            r1 = r33
            r8 = r28
            r19 = r30
            if (r33 == 0) goto L_0x057d
            java.lang.String r0 = r8.A0E
            com.facebook.common.callercontext.CallerContext r15 = com.facebook.common.callercontext.CallerContext.A02(r1, r0)
        L_0x0012:
            if (r29 != 0) goto L_0x0019
            X.2Xl r7 = new X.2Xl
            r7.<init>()
        L_0x0019:
            java.lang.String r1 = r8.A0E
            com.google.common.collect.ImmutableSet r0 = com.facebook.http.common.BootstrapRequestName.A00
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x004b
            X.0Tq r0 = r6.A03
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x004b
            r2 = 7
            int r1 = X.AnonymousClass1Y3.Afh
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2pS r0 = (X.C56172pS) r0
            X.1YI r2 = r0.A00
            r1 = 1
            r0 = 0
            boolean r0 = r2.AbO(r1, r0)
            if (r0 != 0) goto L_0x004b
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            r7.A01(r0)
        L_0x004b:
            if (r7 == 0) goto L_0x0056
            java.lang.Integer r0 = r7.A05
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x055e;
                case 2: goto L_0x0563;
                default: goto L_0x0056;
            }
        L_0x0056:
            X.0Tq r0 = r6.A01
            java.lang.Object r10 = r0.get()
        L_0x005c:
            X.13Q r10 = (X.AnonymousClass13Q) r10
            java.lang.String r1 = r8.A0I
            boolean r0 = r8.A0Q
            if (r0 == 0) goto L_0x050e
            android.net.Uri$Builder r2 = r10.AoC()
        L_0x0068:
            r2.appendEncodedPath(r1)
            boolean r0 = A04(r8)
            if (r0 != 0) goto L_0x008a
            X.0Tq r0 = r6.A04
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x008a
            r0 = 841(0x349, float:1.178E-42)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            java.lang.String r0 = "1"
            r2.appendQueryParameter(r1, r0)
        L_0x008a:
            java.lang.Integer r13 = r8.A0C
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r13 != r0) goto L_0x00a2
            java.util.List r0 = r8.A02()
            if (r0 == 0) goto L_0x050a
            java.util.List r0 = r8.A02()
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x050a
            java.lang.Integer r13 = X.AnonymousClass07B.A01
        L_0x00a2:
            boolean r0 = r8.A0R
            java.lang.String r1 = "GET"
            if (r0 != 0) goto L_0x00ac
            boolean r0 = r8.A0M
            if (r0 == 0) goto L_0x02c1
        L_0x00ac:
            java.lang.String r0 = r8.A0H
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x02c1
            r3 = 5
            int r1 = X.AnonymousClass1Y3.BQ0
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.2Xr r0 = (X.C47742Xr) r0
            X.0os r0 = r0.A01(r8)
            java.lang.String r0 = X.C47752Xs.A00(r0)
            r2.encodedQuery(r0)
            org.apache.http.client.methods.HttpGet r9 = new org.apache.http.client.methods.HttpGet
            android.net.Uri r0 = r2.build()
            java.lang.String r0 = r0.toString()
            r9.<init>(r0)
        L_0x00d7:
            java.lang.String r0 = r8.A0E
            java.lang.String r2 = "upload-video-chunk-user-auth"
            boolean r3 = r0.startsWith(r2)
            r0 = 29
            java.lang.String r1 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            if (r3 == 0) goto L_0x0112
            com.google.common.collect.ImmutableList r0 = r8.A08
            if (r0 != 0) goto L_0x00ed
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
        L_0x00ed:
            X.1Xv r3 = r0.iterator()
        L_0x00f1:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0112
            java.lang.Object r4 = r3.next()
            org.apache.http.NameValuePair r4 = (org.apache.http.NameValuePair) r4
            java.lang.String r0 = r4.getName()
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00f1
            java.lang.String r3 = r4.getName()
            java.lang.String r0 = r4.getValue()
            r9.addHeader(r3, r0)
        L_0x0112:
            boolean r0 = r8.A0R
            r5 = 0
            if (r0 != 0) goto L_0x0280
            boolean r0 = r8.A00
            if (r0 != 0) goto L_0x0280
            java.lang.String r4 = r8.A0I
            r0 = 69
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            r0 = 1209(0x4b9, float:1.694E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.confirmContactpointPreconfirmation"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.initiatePreconfirmation"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.register"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.prefillorautocompletecontactpoint"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.resetPasswordPreconfirmation"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.validateregistrationdata"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r3 = r8.A0E
            java.lang.String r0 = "getSsoUserMethod"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/auth.extendSSOAccessToken"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.sendMessengerOnlyPhoneConfirmationCode"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.confirmMessengerOnlyPhone"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.createMessengerOnlyAccount"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/user.bypassLoginWithConfirmedMessengerCredentials"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "register_messenger_only_user"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "login_softmatched_messenger_only_user"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "method/intl.getLocaleSuggestions"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "350685531728/nonuserpushtokens"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "256002347743983/nonuserpushtokens"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "181425161904154/nonuserpushtokens"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "moments_folder"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "moments_phone_confirmation_code"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "check_moments_phone_confirmation_code"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "register_moments_only_user"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "check_phone_has_moments_account"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "moments_aldrin_wechat_login"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "moments_account_recovery_verify_confirmation_code"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "moments_account_recovery_confirmation_code"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "moments_account_recovery_reset_password"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "moments_account_recovery_determine_eligibility"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "moments_login_device"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/check_phone_has_accounts"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/phone_registration"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/check_confirmation_code"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/account_recovery_send_confirmation_code"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/account_recovery_verify_confirmation_code"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/confirm_phone"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/linked_accounts"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/create_account"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/phone_reset_password"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/social_context"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "parties/check_username"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0280
            java.lang.String r0 = "orca_first_party_sso_context"
            boolean r0 = r0.equalsIgnoreCase(r3)
            if (r0 != 0) goto L_0x0280
            r5 = 1
        L_0x0280:
            r4 = 0
            if (r5 == 0) goto L_0x061f
            boolean r0 = r6.A03(r8)
            if (r0 == 0) goto L_0x02bf
            java.lang.String r3 = r7.A06
        L_0x028b:
            if (r3 != 0) goto L_0x0299
            X.0Tq r0 = r6.A06
            java.lang.Object r0 = r0.get()
            com.facebook.auth.viewercontext.ViewerContext r0 = (com.facebook.auth.viewercontext.ViewerContext) r0
            if (r0 == 0) goto L_0x02bd
            java.lang.String r3 = r0.mAuthToken
        L_0x0299:
            if (r3 != 0) goto L_0x02a7
            java.lang.String r5 = r8.A0E
            java.lang.String r0 = "ota_resource"
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x02a7
            java.lang.String r3 = r7.A06
        L_0x02a7:
            if (r3 != 0) goto L_0x0589
            boolean r0 = r6.A02(r8)
            if (r0 != 0) goto L_0x0589
            boolean r0 = r6.A03(r8)
            if (r0 != 0) goto L_0x0589
            X.9Ir r1 = new X.9Ir
            java.lang.String r0 = "auth token is null, user logged out?"
            r1.<init>(r0)
            throw r1
        L_0x02bd:
            r3 = r4
            goto L_0x0299
        L_0x02bf:
            r3 = r4
            goto L_0x028b
        L_0x02c1:
            boolean r0 = r8.A0T
            if (r0 == 0) goto L_0x034e
            android.net.Uri r3 = r2.build()
            X.B9s r11 = r7.A02
            java.lang.String r2 = r8.A0H
            java.lang.String r0 = "GET"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0300
            org.apache.http.client.methods.HttpGet r9 = new org.apache.http.client.methods.HttpGet
            java.lang.String r0 = r3.toString()
            r9.<init>(r0)
        L_0x02de:
            com.google.common.collect.ImmutableList r0 = r8.A08
            if (r0 != 0) goto L_0x02e4
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
        L_0x02e4:
            X.1Xv r2 = r0.iterator()
        L_0x02e8:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00d7
            java.lang.Object r0 = r2.next()
            org.apache.http.NameValuePair r0 = (org.apache.http.NameValuePair) r0
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = r0.getValue()
            r9.addHeader(r1, r0)
            goto L_0x02e8
        L_0x0300:
            java.lang.String r0 = "POST"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x07de
            org.apache.http.client.methods.HttpPost r9 = new org.apache.http.client.methods.HttpPost
            java.lang.String r0 = r3.toString()
            r9.<init>(r0)
            java.lang.Integer r1 = r8.A0C
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            if (r1 != r0) goto L_0x02de
            X.8jx r5 = r8.A06
            int r4 = r5.A00
            int r3 = r5.A01
            java.io.File r0 = r5.A02
            long r0 = r0.length()
            if (r11 == 0) goto L_0x034c
            X.BAq r2 = new X.BAq
            r2.<init>(r11, r3, r0)
        L_0x032a:
            java.io.File r5 = r5.A02
            boolean r0 = r5 instanceof X.BPA
            if (r0 == 0) goto L_0x0338
            r0 = r5
            X.BPA r0 = (X.BPA) r0
            boolean r1 = r0.mIsTailing
            r0 = 1
            if (r1 != 0) goto L_0x0339
        L_0x0338:
            r0 = 0
        L_0x0339:
            if (r0 == 0) goto L_0x0346
            X.2Yw r0 = new X.2Yw
            X.BPA r5 = (X.BPA) r5
            r0.<init>(r5, r3)
        L_0x0342:
            r9.setEntity(r0)
            goto L_0x02de
        L_0x0346:
            X.2Yv r0 = new X.2Yv
            r0.<init>(r5, r3, r4, r2)
            goto L_0x0342
        L_0x034c:
            r2 = 0
            goto L_0x032a
        L_0x034e:
            java.lang.String r3 = r8.A0H
            boolean r0 = r1.equals(r3)
            if (r0 != 0) goto L_0x0372
            java.lang.String r0 = "POST"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x0372
            java.lang.String r0 = "DELETE"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x0372
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            java.lang.String r0 = "Unsupported method: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r1.<init>(r0)
            throw r1
        L_0x0372:
            org.apache.http.client.methods.HttpPost r9 = new org.apache.http.client.methods.HttpPost
            android.net.Uri r0 = r2.build()
            java.lang.String r0 = r0.toString()
            r9.<init>(r0)
            int r2 = X.AnonymousClass1Y3.BQ0
            X.0UN r1 = r6.A00
            r0 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2Xr r0 = (X.C47742Xr) r0
            X.0os r11 = r0.A01(r8)
            java.lang.String r1 = r8.A0I
            java.lang.String r0 = "method/"
            boolean r0 = r1.startsWith(r0)
            java.lang.String r3 = "method"
            if (r0 == 0) goto L_0x03e7
            r0 = 7
            java.lang.String r0 = r1.substring(r0)
            r11.A0K(r3, r0)
        L_0x03a2:
            java.lang.String r1 = r8.A0E
            r0 = 1030(0x406, float:1.443E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r11.A0K(r0, r1)
            java.lang.String r1 = r15.A02
            java.lang.String r0 = "fb_api_caller_class"
            r11.A0K(r0, r1)
            boolean r0 = A04(r8)
            if (r0 == 0) goto L_0x046f
            int r1 = X.AnonymousClass1Y3.Abx
            X.0UN r0 = r6.A00
            r12 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r12, r1, r0)
            X.00x r0 = (X.C001300x) r0
            java.lang.String r1 = r0.A03
            java.lang.String r0 = "api_key"
            r11.A0K(r0, r1)
            int r5 = r11.A00
            int[] r4 = new int[r5]
            android.util.Pair[] r3 = new android.util.Pair[r5]
            r2 = 0
            r1 = 0
        L_0x03d4:
            if (r1 >= r5) goto L_0x03fd
            java.lang.String r14 = r11.A0H(r1)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            android.util.Pair r0 = android.util.Pair.create(r14, r0)
            r3[r1] = r0
            int r1 = r1 + 1
            goto L_0x03d4
        L_0x03e7:
            java.lang.String r2 = r8.A0H
            java.lang.String r1 = "DELETE"
            boolean r0 = r1.equals(r2)
            if (r0 != 0) goto L_0x03f9
            java.lang.String r1 = "GET"
            boolean r0 = r1.equals(r2)
            if (r0 == 0) goto L_0x03a2
        L_0x03f9:
            r11.A0K(r3, r1)
            goto L_0x03a2
        L_0x03fd:
            X.4Hz r0 = new X.4Hz
            r0.<init>()
            java.util.Arrays.sort(r3, r0)
        L_0x0405:
            if (r2 >= r5) goto L_0x0416
            r0 = r3[r2]
            java.lang.Object r0 = r0.second
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            r4[r2] = r0
            int r2 = r2 + 1
            goto L_0x0405
        L_0x0416:
            java.io.StringWriter r3 = new java.io.StringWriter
            r3.<init>()
            r2 = 0
        L_0x041c:
            int r0 = r4.length     // Catch:{ IOException -> 0x0805 }
            if (r2 >= r0) goto L_0x0453
            r1 = r4[r2]     // Catch:{ IOException -> 0x0805 }
            java.lang.String r0 = r11.A0H(r1)     // Catch:{ IOException -> 0x0805 }
            r3.write(r0)     // Catch:{ IOException -> 0x0805 }
            r0 = 61
            r3.write(r0)     // Catch:{ IOException -> 0x0805 }
            java.lang.Object r1 = r11.A0G(r1)     // Catch:{ IOException -> 0x0805 }
            if (r1 == 0) goto L_0x0450
            boolean r0 = r1 instanceof java.lang.String     // Catch:{ IOException -> 0x0805 }
            if (r0 != 0) goto L_0x0449
            boolean r0 = r1 instanceof java.lang.Number     // Catch:{ IOException -> 0x0805 }
            if (r0 != 0) goto L_0x0449
            boolean r0 = r1 instanceof java.lang.Boolean     // Catch:{ IOException -> 0x0805 }
            if (r0 != 0) goto L_0x0449
            boolean r0 = r1 instanceof X.C12250ot     // Catch:{ IOException -> 0x0805 }
            if (r0 == 0) goto L_0x07ea
            X.0ot r1 = (X.C12250ot) r1     // Catch:{ IOException -> 0x0805 }
            r1.A0C(r3)     // Catch:{ IOException -> 0x0805 }
            goto L_0x0450
        L_0x0449:
            java.lang.String r0 = r1.toString()     // Catch:{ IOException -> 0x0805 }
            r3.write(r0)     // Catch:{ IOException -> 0x0805 }
        L_0x0450:
            int r2 = r2 + 1
            goto L_0x041c
        L_0x0453:
            int r1 = X.AnonymousClass1Y3.Abx     // Catch:{ IOException -> 0x0805 }
            X.0UN r0 = r6.A00     // Catch:{ IOException -> 0x0805 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r12, r1, r0)     // Catch:{ IOException -> 0x0805 }
            X.00x r0 = (X.C001300x) r0     // Catch:{ IOException -> 0x0805 }
            java.lang.String r0 = r0.A05     // Catch:{ IOException -> 0x0805 }
            r3.append(r0)     // Catch:{ IOException -> 0x0805 }
            java.lang.String r0 = r3.toString()     // Catch:{ IOException -> 0x0805 }
            java.lang.String r1 = X.C03380Nn.A00(r0)     // Catch:{ IOException -> 0x0805 }
            java.lang.String r0 = "sig"
            r11.A0K(r0, r1)     // Catch:{ IOException -> 0x0805 }
        L_0x046f:
            r1 = 0
            boolean r0 = r6.A03(r8)
            if (r0 == 0) goto L_0x04e9
            if (r7 == 0) goto L_0x04e9
            java.lang.String r0 = r7.A06
            if (r0 == 0) goto L_0x04e9
            r1 = r0
        L_0x047d:
            if (r1 == 0) goto L_0x0484
            java.lang.String r0 = "access_token"
            r11.A0K(r0, r1)
        L_0x0484:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            if (r13 != r0) goto L_0x04a7
            X.B9s r1 = r7.A02
            java.util.List r0 = r8.A02()
            if (r0 != 0) goto L_0x080d
            X.2Xs r5 = new X.2Xs
            r5.<init>(r11)
            if (r1 == 0) goto L_0x049e
            X.BAr r0 = new X.BAr
            r0.<init>(r1)
            r5.A00 = r0
        L_0x049e:
            org.apache.http.HttpEntity r0 = X.C47772Xu.A01(r5)
            r9.setEntity(r0)
            goto L_0x00d7
        L_0x04a7:
            X.B9s r12 = r7.A02
            X.8eZ r5 = new X.8eZ
            r5.<init>()
            r5.A01(r11)
            java.util.List r1 = r8.A02()
            if (r1 == 0) goto L_0x04d5
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x04d5
            java.util.Iterator r2 = r1.iterator()
        L_0x04c1:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x04d5
            java.lang.Object r0 = r2.next()
            X.2U5 r0 = (X.AnonymousClass2U5) r0
            java.lang.String r1 = r0.A02
            X.2U4 r0 = r0.A01
            r5.A00(r1, r0)
            goto L_0x04c1
        L_0x04d5:
            if (r12 == 0) goto L_0x049e
            long r0 = r5.getContentLength()
            r3 = 0
            int r2 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x049e
            X.BAp r2 = new X.BAp
            r2.<init>(r12, r0)
            r5.A00 = r2
            goto L_0x049e
        L_0x04e9:
            boolean r0 = r6.A02(r8)
            if (r0 == 0) goto L_0x047d
            int r2 = X.AnonymousClass1Y3.Abx
            X.0UN r1 = r6.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.00x r0 = (X.C001300x) r0
            java.lang.String r1 = r0.A04
            java.lang.String r0 = r0.A05
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "|"
            java.lang.String r1 = X.C06850cB.A06(r0, r1)
            goto L_0x047d
        L_0x050a:
            java.lang.Integer r13 = X.AnonymousClass07B.A0C
            goto L_0x00a2
        L_0x050e:
            boolean r0 = r8.A0T
            if (r0 != 0) goto L_0x0552
            boolean r0 = r8.A0S
            if (r0 != 0) goto L_0x0552
            boolean r0 = r8.A0N
            if (r0 == 0) goto L_0x0520
            android.net.Uri$Builder r2 = r10.B2H()
            goto L_0x0068
        L_0x0520:
            boolean r0 = r8.A0O
            if (r0 == 0) goto L_0x052a
            android.net.Uri$Builder r2 = r10.B2G()
            goto L_0x0068
        L_0x052a:
            boolean r0 = r8.A0R
            if (r0 == 0) goto L_0x0534
            android.net.Uri$Builder r2 = r10.AoJ()
            goto L_0x0068
        L_0x0534:
            boolean r0 = r8.A02
            if (r0 == 0) goto L_0x053e
            android.net.Uri$Builder r2 = r10.Auq()
            goto L_0x0068
        L_0x053e:
            java.lang.String r0 = "method"
            boolean r0 = r1.startsWith(r0)
            if (r0 == 0) goto L_0x054c
            android.net.Uri$Builder r2 = r10.AdB()
            goto L_0x0068
        L_0x054c:
            android.net.Uri$Builder r2 = r10.AoB()
            goto L_0x0068
        L_0x0552:
            java.lang.String r0 = r8.A0G
            android.net.Uri r0 = android.net.Uri.parse(r0)
            android.net.Uri$Builder r2 = r0.buildUpon()
            goto L_0x0068
        L_0x055e:
            r2 = 11
            int r1 = X.AnonymousClass1Y3.Afu
            goto L_0x0575
        L_0x0563:
            X.0Tq r0 = r6.A02
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0056
            r2 = 12
            int r1 = X.AnonymousClass1Y3.Aqd
        L_0x0575:
            X.0UN r0 = r6.A00
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r2, r1, r0)
            goto L_0x005c
        L_0x057d:
            java.lang.Class r1 = r19.getClass()
            java.lang.String r0 = r8.A0E
            com.facebook.common.callercontext.CallerContext r15 = com.facebook.common.callercontext.CallerContext.A07(r1, r0)
            goto L_0x0012
        L_0x0589:
            java.lang.String r0 = r8.A0E
            boolean r0 = r0.startsWith(r2)
            if (r0 != 0) goto L_0x061f
            int r2 = X.AnonymousClass1Y3.B50
            X.0UN r0 = r6.A00
            r5 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r2, r0)
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r0 = 90
            boolean r0 = r2.AbO(r0, r5)
            if (r0 != 0) goto L_0x0616
            r5 = 10
            int r2 = X.AnonymousClass1Y3.AXA
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r2, r0)
            X.0XN r0 = (X.AnonymousClass0XN) r0
            boolean r0 = r0.A0J()
            if (r0 == 0) goto L_0x0616
            java.lang.String r2 = r8.A0E
            java.lang.String r0 = "pymb_get_accounts"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "pymb_blacklist_suggestion"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r5 = r8.A0I
            java.lang.String r0 = "recover_accounts"
            boolean r0 = r0.equalsIgnoreCase(r5)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "accountRecoverySendConfirmationCode"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "accountRecoveryValidateCode"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "openidConnectAccountRecovery"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "accountRecoveryNewEmails"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "authenticityUploads"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "logged_out_badge"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "get_linked_fb_user_from_ig_session"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0613
            java.lang.String r0 = "fetchZeroHeaderRequest"
            boolean r2 = r0.equals(r2)
            r0 = 0
            if (r2 == 0) goto L_0x0614
        L_0x0613:
            r0 = 1
        L_0x0614:
            if (r0 != 0) goto L_0x061f
        L_0x0616:
            java.lang.String r0 = "OAuth "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r9.addHeader(r1, r0)
        L_0x061f:
            java.lang.String r1 = r8.A0J
            if (r1 != 0) goto L_0x0627
            java.lang.String r1 = r10.AdC()
        L_0x0627:
            if (r1 == 0) goto L_0x062e
            java.lang.String r0 = "User-Agent"
            r9.addHeader(r0, r1)
        L_0x062e:
            com.google.common.collect.ImmutableList r0 = r8.A07
            if (r0 == 0) goto L_0x0646
            X.1Xv r1 = r0.iterator()
        L_0x0636:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0646
            java.lang.Object r0 = r1.next()
            org.apache.http.Header r0 = (org.apache.http.Header) r0
            r9.addHeader(r0)
            goto L_0x0636
        L_0x0646:
            com.google.common.collect.ImmutableList r0 = r7.A04
            if (r0 == 0) goto L_0x065e
            X.1Xv r1 = r0.iterator()
        L_0x064e:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x065e
            java.lang.Object r0 = r1.next()
            org.apache.http.Header r0 = (org.apache.http.Header) r0
            r9.addHeader(r0)
            goto L_0x064e
        L_0x065e:
            java.lang.String r1 = r10.Ad9()
            if (r1 == 0) goto L_0x066d
            r0 = 173(0xad, float:2.42E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r9.addHeader(r0, r1)
        L_0x066d:
            int r1 = X.AnonymousClass1Y3.B3e
            X.0UN r0 = r6.A00
            r2 = 8
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.common.connectionstatus.FbDataConnectionManager r0 = (com.facebook.common.connectionstatus.FbDataConnectionManager) r0
            X.1Vy r0 = r0.A08()
            if (r0 == 0) goto L_0x068c
            java.lang.String r1 = r0.name()
            r0 = 124(0x7c, float:1.74E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r9.addHeader(r0, r1)
        L_0x068c:
            int r1 = X.AnonymousClass1Y3.B3e
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.common.connectionstatus.FbDataConnectionManager r0 = (com.facebook.common.connectionstatus.FbDataConnectionManager) r0
            double r2 = r0.A03()
            r10 = 0
            int r0 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x06b0
            r0 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r2 = r2 * r0
            long r0 = (long) r2
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = "X-FB-Connection-Bandwidth"
            r9.addHeader(r0, r1)
        L_0x06b0:
            com.facebook.fbtrace.FbTraceNode r1 = r7.A00
            com.facebook.fbtrace.FbTraceNode r0 = com.facebook.fbtrace.FbTraceNode.A03
            if (r1 == r0) goto L_0x06c8
            java.lang.String r1 = "X-FBTrace-Sampled"
            java.lang.String r0 = "true"
            r9.addHeader(r1, r0)
            com.facebook.fbtrace.FbTraceNode r0 = r7.A00
            java.lang.String r1 = r0.A02()
            java.lang.String r0 = "X-FBTrace-Meta"
            r9.addHeader(r0, r1)
        L_0x06c8:
            boolean r0 = r8.A0Q
            if (r0 == 0) goto L_0x06de
            boolean r0 = r8.A0P
            if (r0 == 0) goto L_0x06de
            java.lang.String r1 = "X-FB-Video-Upload-Method"
            java.lang.String r0 = "chunked"
            r9.addHeader(r1, r0)
            java.lang.String r1 = r8.A0K
            java.lang.String r0 = "X_FB_VIDEO_WATERFALL_ID"
            r9.addHeader(r0, r1)
        L_0x06de:
            java.lang.String r1 = r8.A0E
            if (r1 == 0) goto L_0x06e7
            java.lang.String r0 = "X-FB-Friendly-Name"
            r9.addHeader(r0, r1)
        L_0x06e7:
            r12 = r31
            X.9HU r2 = r7.A03
            if (r2 == 0) goto L_0x06f0
            r2.A01(r9)
        L_0x06f0:
            X.2Xy r16 = new X.2Xy
            r1 = 4
            int r0 = X.AnonymousClass1Y3.AtA
            X.0UN r10 = r6.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r0, r10)
            X.200 r5 = (X.AnonymousClass200) r5
            r1 = 2
            int r0 = X.AnonymousClass1Y3.ABw
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r0, r10)
            X.0jz r3 = (X.C10370jz) r3
            r1 = 3
            int r0 = X.AnonymousClass1Y3.AmL
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r10)
            X.0jJ r0 = (X.AnonymousClass0jJ) r0
            r17 = r8
            r18 = r32
            r20 = r5
            r21 = r3
            r22 = r0
            r16.<init>(r17, r18, r19, r20, r21, r22)
            X.0xw r1 = r8.A01()
            com.facebook.http.interfaces.RequestPriority r0 = r7.A01
            if (r0 == 0) goto L_0x0728
            if (r0 == 0) goto L_0x0728
            r1.A04 = r0
        L_0x0728:
            com.facebook.http.interfaces.RequestPriority r18 = r1.A01()
            java.lang.Integer r0 = r8.A0A
            java.lang.String r3 = X.AnonymousClass2Y0.A00(r0)
            com.google.common.base.Preconditions.checkNotNull(r9)
            org.apache.http.params.HttpParams r1 = r9.getParams()
            r0 = 243(0xf3, float:3.4E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r1.setParameter(r0, r3)
            java.lang.String r11 = r8.A0E
            java.lang.Integer r10 = r8.A0B
            int r5 = r8.A03
            long r0 = r8.A04
            java.lang.String r3 = r8.A0F
            r17 = r9
            r19 = r10
            r20 = r16
            r21 = r7
            r22 = r15
            r23 = r5
            r24 = r0
            r26 = r3
            r16 = r11
            X.2Y3 r7 = X.C47772Xu.A00(r16, r17, r18, r19, r20, r21, r22, r23, r24, r26)
            if (r2 == 0) goto L_0x076c
            monitor-enter(r2)
            r2.A00 = r7     // Catch:{ all -> 0x0768 }
            goto L_0x076b
        L_0x0768:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x076b:
            monitor-exit(r2)
        L_0x076c:
            X.2Y4 r10 = new X.2Y4
            int r1 = X.AnonymousClass1Y3.AAv
            X.0UN r0 = r6.A00
            r5 = 13
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0Z4 r0 = (X.AnonymousClass0Z4) r0
            r10.<init>(r0, r7)
            X.0xw r3 = r8.A01()
            java.lang.Object r1 = r3.A02
            monitor-enter(r1)
            com.google.common.base.Preconditions.checkNotNull(r10)     // Catch:{ all -> 0x07d1 }
            r3.A03 = r10     // Catch:{ all -> 0x07d1 }
            com.facebook.http.interfaces.RequestPriority r0 = r3.A00     // Catch:{ all -> 0x07d1 }
            if (r0 == 0) goto L_0x0793
            r3.A01()     // Catch:{ all -> 0x07d1 }
            X.C16890xw.A00(r3, r0)     // Catch:{ all -> 0x07d1 }
        L_0x0793:
            monitor-exit(r1)     // Catch:{ all -> 0x07d1 }
            int r1 = X.AnonymousClass1Y3.AAv     // Catch:{ Exception -> 0x07ad }
            X.0UN r0 = r6.A00     // Catch:{ Exception -> 0x07ad }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ Exception -> 0x07ad }
            X.0Z4 r0 = (X.AnonymousClass0Z4) r0     // Catch:{ Exception -> 0x07ad }
            java.lang.Object r0 = r0.A05(r7)     // Catch:{ Exception -> 0x07ad }
            X.2aY r0 = (X.C48512aY) r0     // Catch:{ Exception -> 0x07ad }
            X.C47772Xu.A02(r9)
            if (r2 == 0) goto L_0x07ac
            r2.A01(r4)
        L_0x07ac:
            return r0
        L_0x07ad:
            r3 = move-exception
            boolean r0 = r3 instanceof X.C190118sk     // Catch:{ all -> 0x07d4 }
            if (r0 == 0) goto L_0x07ba
            X.8sk r3 = (X.C190118sk) r3     // Catch:{ all -> 0x07d4 }
            java.lang.Throwable r3 = r3.getCause()     // Catch:{ all -> 0x07d4 }
            java.lang.Exception r3 = (java.lang.Exception) r3     // Catch:{ all -> 0x07d4 }
        L_0x07ba:
            if (r31 == 0) goto L_0x07ce
            boolean r0 = r3 instanceof X.C37741wB     // Catch:{ all -> 0x07d4 }
            if (r0 == 0) goto L_0x07cf
            X.A0F r1 = new X.A0F     // Catch:{ all -> 0x07d4 }
            r0 = r3
            X.1wB r0 = (X.C37741wB) r0     // Catch:{ all -> 0x07d4 }
            r1.<init>(r12, r0)     // Catch:{ all -> 0x07d4 }
        L_0x07c8:
            java.lang.Object r3 = com.google.common.base.MoreObjects.firstNonNull(r1, r3)     // Catch:{ all -> 0x07d4 }
            java.lang.Exception r3 = (java.lang.Exception) r3     // Catch:{ all -> 0x07d4 }
        L_0x07ce:
            throw r3     // Catch:{ all -> 0x07d4 }
        L_0x07cf:
            r1 = 0
            goto L_0x07c8
        L_0x07d1:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x07d1 }
            goto L_0x07dd
        L_0x07d4:
            r0 = move-exception
            X.C47772Xu.A02(r9)
            if (r2 == 0) goto L_0x07dd
            r2.A01(r4)
        L_0x07dd:
            throw r0
        L_0x07de:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unsupported method: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r2)
            r1.<init>(r0)
            throw r1
        L_0x07ea:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x0805 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0805 }
            r2.<init>()     // Catch:{ IOException -> 0x0805 }
            java.lang.String r0 = "Unsupported value type "
            r2.append(r0)     // Catch:{ IOException -> 0x0805 }
            java.lang.Class r0 = r1.getClass()     // Catch:{ IOException -> 0x0805 }
            r2.append(r0)     // Catch:{ IOException -> 0x0805 }
            java.lang.String r0 = r2.toString()     // Catch:{ IOException -> 0x0805 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x0805 }
            throw r3     // Catch:{ IOException -> 0x0805 }
        L_0x0805:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "StringWriter cannot throw IOException"
            r1.<init>(r0)
            throw r1
        L_0x080d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Cannot add attachment to string entities"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30271hm.A09(X.2Xm, X.2Xl, X.0kt, X.2Xn, java.lang.Object, com.facebook.common.callercontext.CallerContext):X.2aY");
    }
}
