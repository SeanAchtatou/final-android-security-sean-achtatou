package X;

import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

@InjectorModule
/* renamed from: X.0uH  reason: invalid class name and case insensitive filesystem */
public final class C14870uH extends AnonymousClass0UV {
    public static final Boolean A00(AnonymousClass1XY r4) {
        AnonymousClass1Y7 r0;
        FbSharedPreferences A00 = FbSharedPreferencesModule.A00(r4);
        if (!AnonymousClass0WT.A00(r4).Aem(287333312109977L) || !A00.Aep(C34341pQ.A02, false)) {
            r0 = C34341pQ.A00;
        } else {
            r0 = C34341pQ.A01;
        }
        return Boolean.valueOf(A00.Aep(r0, true));
    }
}
