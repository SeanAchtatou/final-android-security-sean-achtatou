package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1HF  reason: invalid class name */
public final class AnonymousClass1HF implements C05460Za {
    private static volatile AnonymousClass1HF A03;
    public AnonymousClass3HB A00;
    public C57092rP A01;
    public C30215Erj A02;

    public void clearUserData() {
        this.A00 = null;
        this.A02 = null;
        this.A01 = null;
    }

    public static final AnonymousClass1HF A00(AnonymousClass1XY r3) {
        if (A03 == null) {
            synchronized (AnonymousClass1HF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A03 = new AnonymousClass1HF();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public void A01(String str, boolean z) {
        C57092rP r0;
        C30215Erj erj = this.A02;
        if (erj != null) {
            C54612mh r2 = erj.A01;
            if (C06850cB.A0C(r2.A09, str)) {
                if (!z) {
                    AnonymousClass3HB r1 = this.A00;
                    if (r1 != null) {
                        r1.A0P = false;
                        AnonymousClass3HB.A07(r1);
                    }
                } else if (!(erj == null || (r0 = this.A01) == null)) {
                    r0.A05("omni_m_suggestion_completed", r2, erj.A00, erj.A03, erj.A02);
                    AnonymousClass3HB r4 = this.A00;
                    if (r4 != null) {
                        C54612mh r5 = this.A02.A01;
                        if (!r5.A00.equals(r4.A0K)) {
                            C010708t.A0I(AnonymousClass24B.$const$string(391), "Completed action belongs to different thread.");
                        } else {
                            COB cob = r5.A06;
                            if (cob == COB.A0E) {
                                boolean z2 = false;
                                if (cob != COB.A0E) {
                                    z2 = true;
                                }
                                AnonymousClass3HB.A0A(r4, r5, z2);
                                AnonymousClass3HB.A08(r4);
                            } else {
                                if (!AnonymousClass3HB.A0N(r4) && !((C67123Nc) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AGG, r4.A0I)).A0B()) {
                                    AnonymousClass3HB.A0H(r4, true);
                                }
                                r4.A0P = false;
                            }
                        }
                    }
                }
                this.A02 = null;
                this.A01 = null;
            }
        }
    }
}
