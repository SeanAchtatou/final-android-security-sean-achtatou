package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.profilo.logger.Logger;
import com.facebook.profilo.logger.api.ProfiloLogger;

/* renamed from: X.00j  reason: invalid class name and case insensitive filesystem */
public final class C000500j extends Handler {
    public final /* synthetic */ AnonymousClass009 A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C000500j(AnonymousClass009 r1, Looper looper) {
        super(looper);
        this.A00 = r1;
    }

    public void handleMessage(Message message) {
        int i;
        int i2 = message.arg1;
        if (ProfiloLogger.sHasProfilo) {
            i = Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 14, 0, 0, -1346527100, i2, 0);
        } else {
            i = -1;
        }
        try {
            int i3 = message.what;
            if (i3 == 0) {
                this.A00.A00 = true;
            } else if (i3 == 1) {
                this.A00.A05();
            } else {
                throw new AssertionError("unknown message " + message);
            }
        } finally {
            ProfiloLogger.asyncCallEnd(i, -1645477904);
        }
    }
}
