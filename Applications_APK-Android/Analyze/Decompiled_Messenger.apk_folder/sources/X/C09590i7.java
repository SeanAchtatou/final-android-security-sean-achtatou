package X;

import com.facebook.i18n.TranslationsFetcher;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.Callable;

/* renamed from: X.0i7  reason: invalid class name and case insensitive filesystem */
public final class C09590i7 implements C04810Wg, Callable {
    public AnonymousClass0UN A00;
    private final C09660iG A01;
    private final C09770if A02;

    public void BYh(Throwable th) {
        this.A01.BYb(this.A02);
    }

    public void Bqf(Object obj) {
        this.A01.Bhq();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public Object call() {
        InputStream AqA = this.A01.AqA(this.A02);
        try {
            this.A01.Bhs();
            C09640iE r3 = this.A02.A04;
            switch (r3.ordinal()) {
                case 0:
                    C08640fh r2 = new C08640fh(new C08660fj(C08630fg.A00(AqA)), true);
                    AqA.close();
                    return r2;
                case 1:
                    C88624Ld r32 = (C88624Ld) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BQs, this.A00);
                    ByteBuffer A002 = C08630fg.A00(AqA);
                    new TranslationsFetcher(A002, ((AnonymousClass0ZS) AnonymousClass1XX.A03(AnonymousClass1Y3.A9d, r32.A00)).A03(), ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AWK, ((C09620iC) AnonymousClass1XX.A03(AnonymousClass1Y3.B6k, r32.A00)).A00)).Aem(18302380361719333L));
                    C08640fh r6 = new C08640fh(new C08660fj(A002), true);
                    AqA.close();
                    return r6;
                default:
                    throw new RuntimeException("Unrecognized language pack type: " + r3);
            }
        } catch (Throwable th) {
            AqA.close();
            throw th;
        }
        this.A01.Bhr();
        throw th;
    }

    public C09590i7(AnonymousClass1XY r3, C09770if r4, C09660iG r5) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A02 = r4;
        this.A01 = r5;
    }
}
