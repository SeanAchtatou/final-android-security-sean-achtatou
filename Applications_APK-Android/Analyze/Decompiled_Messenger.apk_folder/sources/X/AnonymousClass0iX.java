package X;

import android.os.Looper;

/* renamed from: X.0iX  reason: invalid class name */
public final class AnonymousClass0iX implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.debug.looperlog.LooperLogMessagesDispatcher$2";
    public final /* synthetic */ C09730iO A00;

    public AnonymousClass0iX(C09730iO r1) {
        this.A00 = r1;
    }

    public void run() {
        boolean isEmpty;
        synchronized (this.A00.A02) {
            isEmpty = this.A00.A02.isEmpty();
        }
        Looper looper = this.A00.A00.getLooper();
        if (isEmpty) {
            looper.setMessageLogging(null);
        } else {
            looper.setMessageLogging(this.A00.A01);
        }
    }
}
