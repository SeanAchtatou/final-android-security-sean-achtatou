package X;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

/* renamed from: X.22L  reason: invalid class name */
public final class AnonymousClass22L implements C22602B2z {
    public Boolean A00;
    public Long A01;
    public String A02;
    public String A03;

    public boolean BEQ(C22602B2z b2z) {
        if (!(b2z instanceof AnonymousClass22L)) {
            return false;
        }
        AnonymousClass22L r4 = (AnonymousClass22L) b2z;
        if (!C06850cB.A0C(this.A02, r4.A02) || this.A01 != r4.A01 || this.A00 != r4.A00 || !C06850cB.A0C(this.A03, r4.A03)) {
            return false;
        }
        return true;
    }

    public String C5Y() {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        objectNode.put("query", this.A02);
        objectNode.put("1234567890", this.A01);
        objectNode.put("true", this.A00);
        objectNode.put("http://marketplace", this.A03);
        return objectNode.toString();
    }

    public AnonymousClass22L(String str, Long l, Boolean bool, String str2) {
        this.A02 = str;
        this.A01 = l;
        this.A00 = bool;
        this.A03 = str2;
    }
}
