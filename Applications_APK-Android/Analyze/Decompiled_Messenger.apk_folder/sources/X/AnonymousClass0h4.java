package X;

import com.facebook.common.dextricks.DexStore;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

/* renamed from: X.0h4  reason: invalid class name */
public final class AnonymousClass0h4 {
    public static byte[] A01(InputStream inputStream, int i) {
        byte[] bArr = new byte[i];
        int i2 = i;
        while (i2 > 0) {
            int i3 = i - i2;
            int read = inputStream.read(bArr, i3, i2);
            if (read == -1) {
                return Arrays.copyOf(bArr, i3);
            }
            i2 -= read;
        }
        int read2 = inputStream.read();
        if (read2 == -1) {
            return bArr;
        }
        AnonymousClass8XH r2 = new AnonymousClass8XH();
        r2.write(read2);
        A00(inputStream, r2);
        byte[] bArr2 = new byte[(r2.size() + i)];
        System.arraycopy(bArr, 0, bArr2, 0, i);
        r2.A00(bArr2, i);
        return bArr2;
    }

    public static void A00(InputStream inputStream, OutputStream outputStream) {
        C05520Zg.A02(inputStream);
        C05520Zg.A02(outputStream);
        byte[] bArr = new byte[DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }
}
