package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import java.util.List;

/* renamed from: X.1Sx  reason: invalid class name and case insensitive filesystem */
public final class C24261Sx extends C17770zR {
    @Comparable(type = 3)
    public int A00 = 3;
    @Comparable(type = 3)
    public int A01 = -1;
    @Comparable(type = 3)
    public int A02 = -1;
    @Comparable(type = 3)
    public int A03 = -1;
    @Comparable(type = 13)
    public MigColorScheme A04;
    @Comparable(type = 5)
    public List A05;
    @Comparable(type = 3)
    public boolean A06 = false;

    public C24261Sx() {
        super("MigFacepile");
    }
}
