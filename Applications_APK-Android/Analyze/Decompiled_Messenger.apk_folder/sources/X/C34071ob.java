package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1ob  reason: invalid class name and case insensitive filesystem */
public abstract class C34071ob implements C26401bO {
    public static final int[] A01 = new int[0];
    public final C26311bF A00;

    public void A00(int i, List list) {
        int i2;
        int max;
        int i3;
        if (!(this instanceof C34081oc)) {
            if (this instanceof C34091od) {
                C34091od r2 = (C34091od) this;
                if (i >= 0 && i <= 100) {
                    double d = ((double) i) / 100.0d;
                    int i4 = AnonymousClass1Y3.A6U;
                    if (i >= 50) {
                        i4 = AnonymousClass1Y3.A87;
                    }
                    boolean z = false;
                    if (i >= 50) {
                        z = true;
                    }
                    boolean z2 = false;
                    if (i >= 50) {
                        z2 = true;
                    }
                    C26311bF r6 = r2.A00;
                    int[] iArr = r6.A0B;
                    int i5 = iArr[1];
                    int i6 = iArr[0];
                    int max2 = Math.max((i6 + ((int) (((double) (i5 - i6)) * d))) / AnonymousClass1Y3.A87, i4);
                    int min = Math.min(r6.A02, AnonymousClass2RI.A00.length);
                    int i7 = (max2 - 1000) / 100;
                    if (i7 < 0) {
                        i7 = 0;
                    }
                    for (int i8 = 0; i8 < min; i8++) {
                        list.add(Integer.valueOf(AnonymousClass2RI.A00[i8] + i7));
                    }
                    list.add(Integer.valueOf((int) AnonymousClass1Y3.B2h));
                    if (z) {
                        list.add(15873);
                    }
                    if (z2) {
                        i2 = AnonymousClass1Y3.AEg;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } else if (this instanceof C37611vx) {
                C37611vx r22 = (C37611vx) this;
                if (i >= 0 && i <= 100) {
                    double d2 = ((double) i) / 100.0d;
                    int i9 = AnonymousClass1Y3.A6U;
                    if (i >= 50) {
                        i9 = AnonymousClass1Y3.A87;
                    }
                    C26311bF r62 = r22.A00;
                    if (r62.A07) {
                        int[] iArr2 = r62.A0A;
                        int i10 = iArr2[1];
                        int i11 = iArr2[0];
                        int max3 = Math.max((i11 + ((int) (((double) (i10 - i11)) * d2))) / AnonymousClass1Y3.A87, i9);
                        int[] iArr3 = r62.A0C;
                        int i12 = iArr3[1];
                        int i13 = iArr3[0];
                        max = Math.max((i13 + ((int) (((double) (i12 - i13)) * d2))) / AnonymousClass1Y3.A87, i9);
                        list.add(Integer.valueOf(r62.A00));
                        list.add(Integer.valueOf(max3 * AnonymousClass1Y3.A87));
                        i3 = r22.A00.A03;
                    } else {
                        int[] iArr4 = r62.A0B;
                        int i14 = iArr4[1];
                        int i15 = iArr4[0];
                        max = Math.max((i15 + ((int) (((double) (i14 - i15)) * d2))) / AnonymousClass1Y3.A87, i9);
                        i3 = r62.A02;
                    }
                    list.add(Integer.valueOf(i3));
                    i2 = max * AnonymousClass1Y3.A87;
                } else {
                    return;
                }
            } else if (this instanceof C37601vw) {
                list.add(Integer.valueOf(i));
                return;
            } else {
                return;
            }
            list.add(Integer.valueOf(i2));
            return;
        }
        C34081oc r7 = (C34081oc) this;
        if (i >= 0 && i <= 100) {
            double d3 = ((double) i) / 100.0d;
            int i16 = AnonymousClass1Y3.A6U;
            if (i >= 50) {
                i16 = AnonymousClass1Y3.A87;
            }
            boolean z3 = false;
            if (i >= 50) {
                z3 = true;
            }
            boolean z4 = false;
            if (i >= 50) {
                z4 = true;
            }
            C26311bF r63 = r7.A00;
            int[] iArr5 = r63.A0A;
            int i17 = iArr5[1];
            int i18 = iArr5[0];
            int max4 = Math.max((i18 + ((int) (((double) (i17 - i18)) * d3))) / AnonymousClass1Y3.A87, i16);
            int[] iArr6 = r63.A0C;
            int i19 = iArr6[1];
            int i20 = iArr6[0];
            int max5 = Math.max((i20 + ((int) (((double) (i19 - i20)) * d3))) / AnonymousClass1Y3.A87, i16);
            if (r7.A00 && max4 > 1100) {
                max4 = 1100;
            }
            list.add(1086324736);
            list.add(1);
            if (z3) {
                list.add(1090519040);
                list.add(Integer.valueOf(r7.A00.A00));
            }
            if (z4) {
                list.add(1077936128);
                list.add(1);
            }
            list.add(1082130432);
            list.add(Integer.valueOf(max4));
            list.add(1082130688);
            list.add(Integer.valueOf(max5));
            C26311bF r1 = r7.A00;
            if (r1.A08) {
                int[] iArr7 = r1.A0D;
                int i21 = iArr7[1];
                int i22 = iArr7[0];
                int max6 = Math.max((i22 + ((int) (((double) (i21 - i22)) * d3))) / AnonymousClass1Y3.A87, i16);
                if (r7.A00 && max6 > 1100) {
                    max6 = 1100;
                }
                list.add(1082130944);
                list.add(Integer.valueOf(max6));
            }
        }
    }

    public void A01(int i, List list) {
        if (this instanceof C34081oc) {
            list.add(1115701248);
            list.add(1);
        }
    }

    public int[] Aet(C26501bY r6) {
        if (r6 != null) {
            ArrayList<Integer> arrayList = new ArrayList<>();
            if (r6.A02 == 2) {
                A01(r6.A03, arrayList);
            } else {
                A00(r6.A03, arrayList);
            }
            if (!arrayList.isEmpty()) {
                int[] iArr = new int[arrayList.size()];
                int i = 0;
                for (Integer intValue : arrayList) {
                    iArr[i] = intValue.intValue();
                    i++;
                }
                return iArr;
            }
        }
        return A01;
    }

    public C34071ob(C26311bF r1) {
        this.A00 = r1;
    }
}
