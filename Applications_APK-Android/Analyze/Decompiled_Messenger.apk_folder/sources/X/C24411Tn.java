package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import com.facebook.mqttlite.MqttService;
import com.google.common.base.Preconditions;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Tn  reason: invalid class name and case insensitive filesystem */
public final class C24411Tn {
    private static volatile C24411Tn A04;
    public final PackageManager A00;
    public final C009207y A01;
    public final Set A02 = new HashSet();
    private final C24421To A03;

    public static final C24411Tn A00(AnonymousClass1XY r5) {
        if (A04 == null) {
            synchronized (C24411Tn.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A04 = new C24411Tn(applicationInjector, C04750Wa.A01(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static void A01(C24411Tn r3, Context context, Intent intent) {
        ComponentName componentName = new ComponentName(context, MqttService.class);
        boolean z = false;
        if (intent.getComponent() == null) {
            z = true;
        }
        Preconditions.checkArgument(z, "Explicit component selection is not allowed");
        synchronized (r3.A02) {
            if (!r3.A02.contains(componentName)) {
                r3.A02.add(componentName);
                r3.A00.setComponentEnabledSetting(componentName, 1, 1);
            }
        }
        intent.setComponent(componentName);
    }

    public void A04(ServiceConnection serviceConnection) {
        C24421To r4 = this.A03;
        synchronized (r4) {
            Iterator it = r4.A00.values().iterator();
            while (it.hasNext()) {
                C33401nW r1 = (C33401nW) it.next();
                if (r1.A04.remove(serviceConnection) && r1.A04.isEmpty()) {
                    it.remove();
                    AnonymousClass0lQ r0 = r4.A01;
                    C006406k.A01(r0.A00, r1.A03, -810719460);
                }
            }
        }
    }

    private C24411Tn(AnonymousClass1XY r2, AnonymousClass09P r3) {
        C009207y r0;
        this.A03 = C24421To.A00(r2);
        this.A00 = C04490Ux.A0J(r2);
        if (r3 == null) {
            r0 = C009207y.A01;
        } else {
            r0 = new C009207y(r3);
        }
        this.A01 = r0;
    }

    public C34271pF A02(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
        C34271pF r2;
        A01(this, context, intent);
        ComponentName component = intent.getComponent();
        component.flattenToString();
        C24421To r6 = this.A03;
        synchronized (r6) {
            Preconditions.checkNotNull(intent);
            Preconditions.checkNotNull(serviceConnection);
            ComponentName component2 = intent.getComponent();
            boolean z = false;
            boolean z2 = false;
            if (component2 != null) {
                z2 = true;
            }
            Preconditions.checkArgument(z2, "Bindings are cached by specific service components but none was specified");
            C33401nW r7 = (C33401nW) r6.A00.get(component2);
            if (r7 == null) {
                r7 = new C33401nW(component2, new C36091sJ(r6), i);
                r6.A00.put(component2, r7);
            } else {
                int i2 = r7.A02;
                if (i2 == i) {
                    z = true;
                }
                Preconditions.checkArgument(z, "Inconsistent binding flags provided: got %d, expected %d", i, i2);
            }
            r7.A04.add(serviceConnection);
            if (!r7.A01) {
                boolean A012 = r6.A01.A01(intent, r7.A03, r7.A02);
                r7.A01 = true;
                if (!A012) {
                    r6.A00.remove(component2);
                }
                r2 = new C34271pF(A012, null);
            } else {
                r2 = new C34271pF(true, r7.A00);
            }
        }
        if (!r2.A01) {
            C010708t.A0O("PushServiceTargetingHelper", "Unable to bind to %s", component.flattenToString());
            this.A01.A06(context, component);
        }
        return r2;
    }

    public void A03(Context context, Intent intent) {
        A01(this, context, intent);
        if (this.A01.A00(context, intent) == null) {
            intent.getComponent().flattenToShortString();
            this.A01.A06(context, intent.getComponent());
        }
    }
}
