package X;

import android.app.ActivityManager;
import android.os.Debug;
import java.util.concurrent.Callable;

/* renamed from: X.0py  reason: invalid class name and case insensitive filesystem */
public final class C12790py implements Callable {
    public final /* synthetic */ C08380fG A00;

    public C12790py(C08380fG r1) {
        this.A00 = r1;
    }

    public Object call() {
        C08380fG r5 = this.A00;
        C12800pz r4 = new C12800pz();
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        r4.A02 = memoryInfo;
        ((ActivityManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOn, r5.A00)).getMemoryInfo(memoryInfo);
        r4.A00 = r5.A01.totalMemory() - r5.A01.freeMemory();
        r4.A01 = Debug.getNativeHeapAllocatedSize() - Debug.getNativeHeapFreeSize();
        return r4;
    }
}
