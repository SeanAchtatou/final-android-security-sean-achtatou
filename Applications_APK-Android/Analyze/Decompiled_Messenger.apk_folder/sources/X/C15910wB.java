package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0wB  reason: invalid class name and case insensitive filesystem */
public final class C15910wB implements Runnable {
    public static final String __redex_internal_original_name = "androidx.recyclerview.widget.RecyclerView$1";
    public final /* synthetic */ RecyclerView A00;

    public C15910wB(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    public void run() {
        RecyclerView recyclerView = this.A00;
        if (recyclerView.A03 && !recyclerView.isLayoutRequested()) {
            RecyclerView recyclerView2 = this.A00;
            if (!recyclerView2.A04) {
                recyclerView2.requestLayout();
            } else if (recyclerView2.A0W) {
                recyclerView2.A07 = true;
            } else {
                recyclerView2.A0e();
            }
        }
    }
}
