package X;

import android.content.Context;
import android.content.res.Resources;
import android.database.SQLException;
import android.view.WindowManager;
import android.widget.ImageView;
import com.facebook.acra.ActionId;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.drawee.fbpipeline.FbDraweeView;
import com.facebook.fbservice.service.ServiceException;
import com.facebook.graphql.error.GraphQLError;
import com.facebook.messaging.livelocation.feature.LiveLocationForegroundService;
import com.facebook.messaging.media.loader.LocalMediaLoaderParams;
import com.facebook.messaging.media.picker.MediaPickerPopupVideoView;
import com.facebook.messaging.neo.threadviewbannerentrypoint.LikelyParentDownloadPromptNotificationsManager;
import com.facebook.messaging.sharing.ShareLauncherActivity;
import com.facebook.messaging.sharing.SingleRecipientShareLauncherActivity;
import com.facebook.messaging.sharing.mediapreview.MediaSharePreviewPlayableView;
import com.facebook.orca.threadview.DownloadAttachmentDialogFragment;
import com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity;
import com.facebook.payments.checkout.errors.dialog.PaymentsErrorActionDialog;
import com.facebook.payments.checkout.model.SimpleCheckoutData;
import com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams;
import com.facebook.payments.logging.PaymentsFlowStep;
import com.facebook.payments.logging.PaymentsLoggingSessionData;
import com.facebook.payments.model.PaymentItemType;
import com.facebook.payments.paymentmethods.cardform.CardFormParams;
import com.facebook.payments.paymentmethods.picker.PaymentMethodsPickerScreenConfig;
import com.facebook.payments.settings.PaymentSettingsPickerRunTimeData;
import com.facebook.payments.ui.PaymentsErrorView;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.stickers.keyboard.StickerKeyboardView;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.facebook.video.player.FbVideoView;
import com.facebook.video.player.RichVideoPlayer;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/* renamed from: X.0ai  reason: invalid class name and case insensitive filesystem */
public abstract class C06020ai implements C05270Yh {
    private volatile boolean A00;

    /* JADX WARNING: Code restructure failed: missing block: B:1010:0x1b13, code lost:
        if (r1.getBooleanValue(2065348735) == false) goto L_0x1b15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1108:0x1e78, code lost:
        if (r3.A03.A00.Aem(285769944012669L) == false) goto L_0x1e7a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1137:0x1f0c, code lost:
        if (((X.C51042fH.A07 || X.C51042fH.A01(r4, X.C51042fH.A0A)) ? true : r4.A04.A07("mk_promotion_fm_thread_tap_timestamp").isPresent()) != false) goto L_0x1f0e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1148:0x1f3a, code lost:
        if (r2.equals(r0) != false) goto L_0x1f3c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1151:0x1f3e, code lost:
        if (r1 != false) goto L_0x1f41;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1387:0x26c1, code lost:
        if (X.C06850cB.A0B(r2) != false) goto L_0x26c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1543:0x2cee, code lost:
        if (r3.equals("MOR_FAN_FUNDING") == false) goto L_0x2cf0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1547:0x2cf4, code lost:
        if (r1 == 1) goto L_0x2cf6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1560:0x2d56, code lost:
        if (r3.equals("MOR_GROUP_SUBSCRIPTION") == false) goto L_0x2cf0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:488:0x08af, code lost:
        if (r2 != false) goto L_0x0910;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:501:0x090e, code lost:
        if (r6 != false) goto L_0x0910;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:502:0x0910, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:503:0x0911, code lost:
        r1.Bqf(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:504:0x0914, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:544:0x0a13, code lost:
        if (r3 == X.C421828p.A02) goto L_0x0a15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:692:0x0fdc, code lost:
        if (((com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0.A0J(986016248, r3, -1203717159)) != null) goto L_0x0fde;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:701:0x1045, code lost:
        if (((com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0.A0J(-2061685216, r3, 1324889998)) != null) goto L_0x1047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:873:0x16e7, code lost:
        if (r4 == X.AnonymousClass07B.A00) goto L_0x16e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:899:0x1783, code lost:
        if (r12 == X.AnonymousClass07B.A00) goto L_0x1785;
     */
    /* JADX WARNING: Removed duplicated region for block: B:1286:0x23eb  */
    /* JADX WARNING: Removed duplicated region for block: B:887:0x1721 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(java.lang.Object r19) {
        /*
            r18 = this;
            r2 = r18
            r0 = r19
            boolean r1 = r2 instanceof X.AnonymousClass1ER
            if (r1 != 0) goto L_0x15ae
            boolean r1 = r2 instanceof X.AnonymousClass17Z
            if (r1 != 0) goto L_0x153e
            boolean r1 = r2 instanceof X.C31281jR
            if (r1 != 0) goto L_0x305f
            boolean r1 = r2 instanceof X.AnonymousClass1GO
            if (r1 != 0) goto L_0x146a
            boolean r1 = r2 instanceof X.C20341Ca
            if (r1 != 0) goto L_0x1458
            boolean r1 = r2 instanceof X.C20351Cc
            if (r1 != 0) goto L_0x3041
            boolean r1 = r2 instanceof X.C31291jS
            if (r1 != 0) goto L_0x3038
            boolean r1 = r2 instanceof X.AnonymousClass1BR
            if (r1 != 0) goto L_0x143a
            boolean r1 = r2 instanceof X.C20381Cf
            if (r1 != 0) goto L_0x302c
            boolean r1 = r2 instanceof X.C20391Cg
            if (r1 != 0) goto L_0x2fdb
            boolean r1 = r2 instanceof X.C20411Ci
            if (r1 != 0) goto L_0x13d9
            boolean r1 = r2 instanceof X.AnonymousClass1Cb
            if (r1 != 0) goto L_0x1121
            boolean r1 = r2 instanceof X.C20421Cj
            if (r1 != 0) goto L_0x2f58
            boolean r1 = r2 instanceof X.C31301jT
            if (r1 != 0) goto L_0x2f32
            boolean r1 = r2 instanceof X.C20441Cl
            if (r1 != 0) goto L_0x2eaa
            boolean r1 = r2 instanceof X.C20451Cm
            if (r1 != 0) goto L_0x2e2d
            boolean r1 = r2 instanceof X.C20461Cn
            if (r1 != 0) goto L_0x2e1c
            boolean r1 = r2 instanceof X.C20471Co
            if (r1 != 0) goto L_0x2dee
            boolean r1 = r2 instanceof X.C20361Cd
            if (r1 != 0) goto L_0x10c0
            boolean r1 = r2 instanceof X.C20481Cp
            if (r1 != 0) goto L_0x2d96
            boolean r1 = r2 instanceof X.C20491Cq
            if (r1 != 0) goto L_0x2d86
            boolean r1 = r2 instanceof X.AnonymousClass1Cs
            if (r1 != 0) goto L_0x2d7c
            boolean r1 = r2 instanceof X.C20511Ct
            if (r1 != 0) goto L_0x2d72
            boolean r1 = r2 instanceof X.C20521Cu
            if (r1 != 0) goto L_0x2d68
            boolean r1 = r2 instanceof X.C20531Cv
            if (r1 != 0) goto L_0x2cd1
            boolean r1 = r2 instanceof X.C20371Ce
            if (r1 != 0) goto L_0x109e
            boolean r1 = r2 instanceof X.C20541Cw
            if (r1 != 0) goto L_0x2cc1
            boolean r1 = r2 instanceof X.C20551Cx
            if (r1 != 0) goto L_0x2cb2
            boolean r1 = r2 instanceof X.C20571Cz
            if (r1 != 0) goto L_0x2c87
            boolean r1 = r2 instanceof X.C20401Ch
            if (r1 != 0) goto L_0x1030
            boolean r1 = r2 instanceof X.C20431Ck
            if (r1 != 0) goto L_0x0fc7
            boolean r1 = r2 instanceof X.AnonymousClass1D0
            if (r1 != 0) goto L_0x2c12
            boolean r1 = r2 instanceof X.C20561Cy
            if (r1 != 0) goto L_0x0fb0
            boolean r1 = r2 instanceof X.C20501Cr
            if (r1 != 0) goto L_0x0fb6
            boolean r1 = r2 instanceof X.AnonymousClass1D1
            if (r1 != 0) goto L_0x2bf5
            boolean r1 = r2 instanceof X.AnonymousClass1D3
            if (r1 != 0) goto L_0x2b9c
            boolean r1 = r2 instanceof X.AnonymousClass1D6
            if (r1 != 0) goto L_0x2b61
            boolean r1 = r2 instanceof X.AnonymousClass1D7
            if (r1 != 0) goto L_0x2aa5
            boolean r1 = r2 instanceof X.AnonymousClass1D8
            if (r1 != 0) goto L_0x2f3a
            boolean r1 = r2 instanceof X.AnonymousClass1D9
            if (r1 != 0) goto L_0x2a41
            boolean r1 = r2 instanceof X.AnonymousClass1D2
            if (r1 != 0) goto L_0x0f0d
            boolean r1 = r2 instanceof X.AnonymousClass1DA
            if (r1 != 0) goto L_0x2a2d
            boolean r1 = r2 instanceof X.AnonymousClass1D5
            if (r1 != 0) goto L_0x0eee
            boolean r1 = r2 instanceof X.AnonymousClass1DB
            if (r1 != 0) goto L_0x2a20
            boolean r1 = r2 instanceof X.AnonymousClass1DD
            if (r1 != 0) goto L_0x29d4
            boolean r1 = r2 instanceof X.AnonymousClass1D4
            if (r1 != 0) goto L_0x0ea4
            boolean r1 = r2 instanceof X.AnonymousClass1DE
            if (r1 != 0) goto L_0x29b5
            boolean r1 = r2 instanceof X.AnonymousClass1DF
            if (r1 != 0) goto L_0x29c1
            boolean r1 = r2 instanceof X.C31311jU
            if (r1 != 0) goto L_0x0da9
            boolean r1 = r2 instanceof X.AnonymousClass1DH
            if (r1 != 0) goto L_0x2985
            boolean r1 = r2 instanceof X.AnonymousClass1DC
            if (r1 != 0) goto L_0x0d6d
            boolean r1 = r2 instanceof X.AnonymousClass1DI
            if (r1 != 0) goto L_0x298f
            boolean r1 = r2 instanceof X.AnonymousClass1DK
            if (r1 != 0) goto L_0x2970
            boolean r1 = r2 instanceof X.AnonymousClass1DL
            if (r1 != 0) goto L_0x2953
            boolean r1 = r2 instanceof X.AnonymousClass1DG
            if (r1 != 0) goto L_0x0d3a
            boolean r1 = r2 instanceof X.AnonymousClass1DJ
            if (r1 != 0) goto L_0x0cda
            boolean r1 = r2 instanceof X.AnonymousClass1DM
            if (r1 != 0) goto L_0x283d
            boolean r1 = r2 instanceof X.AnonymousClass1DN
            if (r1 != 0) goto L_0x278b
            boolean r1 = r2 instanceof X.AnonymousClass1DQ
            if (r1 != 0) goto L_0x26cc
            boolean r1 = r2 instanceof X.AnonymousClass1DR
            if (r1 != 0) goto L_0x26fd
            boolean r1 = r2 instanceof X.AnonymousClass1DS
            if (r1 != 0) goto L_0x268a
            boolean r1 = r2 instanceof X.AnonymousClass1DT
            if (r1 != 0) goto L_0x2661
            boolean r1 = r2 instanceof X.AnonymousClass1DU
            if (r1 != 0) goto L_0x2651
            boolean r1 = r2 instanceof X.AnonymousClass1DV
            if (r1 != 0) goto L_0x2677
            boolean r1 = r2 instanceof X.AnonymousClass1DX
            if (r1 != 0) goto L_0x2533
            boolean r1 = r2 instanceof X.C20981Eo
            if (r1 != 0) goto L_0x24a0
            boolean r1 = r2 instanceof X.AnonymousClass1DY
            if (r1 != 0) goto L_0x242a
            boolean r1 = r2 instanceof X.AnonymousClass1DZ
            if (r1 != 0) goto L_0x227a
            boolean r1 = r2 instanceof X.C20581Da
            if (r1 != 0) goto L_0x224f
            boolean r1 = r2 instanceof X.C20591Db
            if (r1 != 0) goto L_0x21b0
            boolean r1 = r2 instanceof X.AnonymousClass1GQ
            if (r1 != 0) goto L_0x21a2
            boolean r1 = r2 instanceof X.C31321jV
            if (r1 != 0) goto L_0x0b9a
            boolean r1 = r2 instanceof X.C31331jW
            if (r1 != 0) goto L_0x211d
            boolean r1 = r2 instanceof X.AnonymousClass1DO
            if (r1 != 0) goto L_0x0b5a
            boolean r1 = r2 instanceof X.C20601Dc
            if (r1 != 0) goto L_0x209a
            boolean r1 = r2 instanceof X.AnonymousClass1DP
            if (r1 != 0) goto L_0x0b24
            boolean r1 = r2 instanceof X.C31341jX
            if (r1 != 0) goto L_0x0ae2
            boolean r1 = r2 instanceof X.C20611Dd
            if (r1 != 0) goto L_0x2063
            boolean r1 = r2 instanceof X.C20631Df
            if (r1 != 0) goto L_0x0a92
            boolean r1 = r2 instanceof X.C20651Dh
            if (r1 != 0) goto L_0x09b1
            boolean r1 = r2 instanceof X.C20661Di
            if (r1 != 0) goto L_0x093a
            boolean r1 = r2 instanceof X.C31351jY
            if (r1 != 0) goto L_0x0239
            boolean r1 = r2 instanceof X.C20671Dj
            if (r1 != 0) goto L_0x0927
            boolean r1 = r2 instanceof X.C20621De
            if (r1 != 0) goto L_0x2033
            boolean r1 = r2 instanceof X.C20691Dl
            if (r1 != 0) goto L_0x085d
            boolean r1 = r2 instanceof X.C20701Dm
            if (r1 != 0) goto L_0x088f
            boolean r1 = r2 instanceof X.C20681Dk
            if (r1 != 0) goto L_0x08c3
            boolean r1 = r2 instanceof X.C20721Do
            if (r1 != 0) goto L_0x081c
            boolean r1 = r2 instanceof X.C20641Dg
            if (r1 != 0) goto L_0x2004
            boolean r1 = r2 instanceof X.C20731Dp
            if (r1 != 0) goto L_0x1fef
            boolean r1 = r2 instanceof X.C20781Du
            if (r1 != 0) goto L_0x1ff7
            boolean r1 = r2 instanceof X.C20761Ds
            if (r1 != 0) goto L_0x0739
            boolean r1 = r2 instanceof X.C20791Dv
            if (r1 != 0) goto L_0x1fc8
            boolean r1 = r2 instanceof X.C20741Dq
            if (r1 != 0) goto L_0x070b
            boolean r1 = r2 instanceof X.C20801Dw
            if (r1 != 0) goto L_0x1eb2
            boolean r1 = r2 instanceof X.C35941s4
            if (r1 != 0) goto L_0x0239
            boolean r1 = r2 instanceof X.C20771Dt
            if (r1 != 0) goto L_0x06ca
            boolean r1 = r2 instanceof X.C20811Dx
            if (r1 != 0) goto L_0x06b3
            boolean r1 = r2 instanceof X.C23771Qu
            if (r1 != 0) goto L_0x0683
            boolean r1 = r2 instanceof X.AnonymousClass1E1
            if (r1 != 0) goto L_0x0650
            boolean r1 = r2 instanceof X.C20821Dy
            if (r1 != 0) goto L_0x1e21
            boolean r1 = r2 instanceof X.AnonymousClass1E0
            if (r1 != 0) goto L_0x1daf
            boolean r1 = r2 instanceof X.AnonymousClass1E5
            if (r1 != 0) goto L_0x1d98
            boolean r1 = r2 instanceof X.AnonymousClass1E6
            if (r1 != 0) goto L_0x1cdd
            boolean r1 = r2 instanceof X.C37211us
            if (r1 != 0) goto L_0x1cb9
            boolean r1 = r2 instanceof X.AnonymousClass1E2
            if (r1 != 0) goto L_0x0629
            boolean r1 = r2 instanceof X.AnonymousClass1A4
            if (r1 != 0) goto L_0x061c
            boolean r1 = r2 instanceof X.AnonymousClass1E7
            if (r1 != 0) goto L_0x1ca2
            boolean r1 = r2 instanceof X.AnonymousClass1FB
            if (r1 != 0) goto L_0x05ff
            boolean r1 = r2 instanceof X.AnonymousClass1E8
            if (r1 != 0) goto L_0x1c93
            boolean r1 = r2 instanceof X.AnonymousClass1E9
            if (r1 != 0) goto L_0x1c99
            boolean r1 = r2 instanceof X.C37221ut
            if (r1 != 0) goto L_0x1c7d
            boolean r1 = r2 instanceof X.AnonymousClass1EC
            if (r1 != 0) goto L_0x1bdf
            boolean r1 = r2 instanceof X.AnonymousClass1EE
            if (r1 != 0) goto L_0x1bd0
            boolean r1 = r2 instanceof X.AnonymousClass1E3
            if (r1 != 0) goto L_0x0422
            boolean r1 = r2 instanceof X.AnonymousClass1EF
            if (r1 != 0) goto L_0x1b23
            boolean r1 = r2 instanceof X.AnonymousClass1EG
            if (r1 != 0) goto L_0x1aec
            boolean r1 = r2 instanceof X.C37231uu
            if (r1 != 0) goto L_0x0239
            boolean r1 = r2 instanceof X.C37241uv
            if (r1 != 0) goto L_0x1aae
            boolean r1 = r2 instanceof X.C37251uw
            if (r1 != 0) goto L_0x0239
            boolean r1 = r2 instanceof X.AnonymousClass1E4
            if (r1 != 0) goto L_0x0417
            boolean r1 = r2 instanceof X.AnonymousClass1EH
            if (r1 != 0) goto L_0x1a13
            boolean r1 = r2 instanceof X.AnonymousClass1EJ
            if (r1 != 0) goto L_0x1938
            boolean r1 = r2 instanceof X.AnonymousClass1EA
            if (r1 != 0) goto L_0x0392
            boolean r1 = r2 instanceof X.AnonymousClass1EB
            if (r1 != 0) goto L_0x02a0
            boolean r1 = r2 instanceof X.AnonymousClass1EI
            if (r1 != 0) goto L_0x027e
            boolean r1 = r2 instanceof X.AnonymousClass1EK
            if (r1 != 0) goto L_0x1855
            boolean r1 = r2 instanceof X.AnonymousClass1EL
            if (r1 != 0) goto L_0x168c
            boolean r1 = r2 instanceof X.AnonymousClass1EN
            if (r1 != 0) goto L_0x163e
            boolean r1 = r2 instanceof X.C20711Dn
            if (r1 != 0) goto L_0x0239
            boolean r1 = r2 instanceof X.AnonymousClass0lj
            if (r1 != 0) goto L_0x023a
            boolean r1 = r2 instanceof X.AnonymousClass1EQ
            if (r1 == 0) goto L_0x0239
            r1 = r2
            X.1EQ r1 = (X.AnonymousClass1EQ) r1
            X.1EO r0 = (X.AnonymousClass1EO) r0
            X.1EP r5 = r1.A01
            java.lang.Object r4 = r1.A02
            int r3 = r1.A00
            r1 = 0
            r5.A00 = r1
            X.1Ap r2 = r5.A01
            java.lang.Object r1 = r0.A01
            r2.Bgh(r4, r1)
            java.lang.Integer r2 = r0.A00
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            if (r2 != r1) goto L_0x1625
            r5.A0C()
            X.1Ap r1 = r5.A01
            java.lang.Object r0 = r0.A01
            r1.BdJ(r4, r0)
        L_0x0239:
            return
        L_0x023a:
            X.0lj r2 = (X.AnonymousClass0lj) r2
            boolean r1 = r2 instanceof X.AnonymousClass1EV
            if (r1 != 0) goto L_0x027b
            boolean r1 = r2 instanceof X.AnonymousClass1EW
            if (r1 != 0) goto L_0x0278
            boolean r1 = r2 instanceof X.AnonymousClass148
            if (r1 != 0) goto L_0x0275
            boolean r1 = r2 instanceof X.AnonymousClass149
            if (r1 != 0) goto L_0x0272
            boolean r1 = r2 instanceof X.AnonymousClass14A
            if (r1 != 0) goto L_0x026f
            boolean r1 = r2 instanceof X.AnonymousClass14C
            if (r1 != 0) goto L_0x026c
            boolean r1 = r2 instanceof X.C186014j
            if (r1 != 0) goto L_0x0269
            boolean r1 = r2 instanceof X.C186214l
            if (r1 != 0) goto L_0x0266
            boolean r1 = r2 instanceof X.AnonymousClass0li
            if (r1 != 0) goto L_0x0239
        L_0x0260:
            com.facebook.fbservice.service.OperationResult r0 = (com.facebook.fbservice.service.OperationResult) r0
            r2.A04(r0)
            return
        L_0x0266:
            X.14l r2 = (X.C186214l) r2
            goto L_0x0260
        L_0x0269:
            X.14j r2 = (X.C186014j) r2
            goto L_0x0260
        L_0x026c:
            X.14C r2 = (X.AnonymousClass14C) r2
            goto L_0x0260
        L_0x026f:
            X.14A r2 = (X.AnonymousClass14A) r2
            goto L_0x0260
        L_0x0272:
            X.149 r2 = (X.AnonymousClass149) r2
            goto L_0x0260
        L_0x0275:
            X.148 r2 = (X.AnonymousClass148) r2
            goto L_0x0260
        L_0x0278:
            X.1EW r2 = (X.AnonymousClass1EW) r2
            goto L_0x0260
        L_0x027b:
            X.1EV r2 = (X.AnonymousClass1EV) r2
            goto L_0x0260
        L_0x027e:
            r1 = r2
            X.1EI r1 = (X.AnonymousClass1EI) r1
            X.2kz r0 = (X.C53562kz) r0
            X.2ky r5 = r1.A00
            boolean r1 = r5.A03
            if (r1 != 0) goto L_0x0239
            java.lang.Integer r1 = r5.A02
            if (r1 != 0) goto L_0x0239
            com.google.common.base.Preconditions.checkNotNull(r0)
            java.lang.Integer r7 = r0.A00
            int r1 = r7.intValue()
            r6 = 1
            switch(r1) {
                case 0: goto L_0x185e;
                case 1: goto L_0x029a;
                case 2: goto L_0x1864;
                default: goto L_0x029a;
            }
        L_0x029a:
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            X.C53552ky.A01(r5, r0)
            return
        L_0x02a0:
            r3 = r2
            X.1EB r3 = (X.AnonymousClass1EB) r3
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            java.lang.Object r4 = r0.A03
            r5 = 0
            if (r4 == 0) goto L_0x02d6
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 98043867(0x5d807db, float:2.0315428E-35)
            r0 = -1582865975(0xffffffffa1a761c9, float:-1.1342244E-18)
            com.facebook.graphservice.tree.TreeJNI r4 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            if (r4 == 0) goto L_0x02d6
            r1 = -816631278(0xffffffffcf533212, float:-3.54327398E9)
            r0 = 240345991(0xe536387, float:2.6055679E-30)
            com.facebook.graphservice.tree.TreeJNI r4 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            if (r4 == 0) goto L_0x02d6
            r1 = 245609042(0xea3b252, float:4.035432E-30)
            r0 = 1006763763(0x3c01fef3, float:0.00793432)
            com.facebook.graphservice.tree.TreeJNI r5 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
        L_0x02d6:
            X.2nh r0 = r3.A01
            r0.A00()
            X.2f9 r0 = r3.A00
            X.2l0 r0 = r0.A06
            X.2l1 r6 = r3.A02
            com.facebook.prefs.shared.FbSharedPreferences r0 = r0.A00
            X.1hn r4 = r0.edit()
            com.facebook.common.util.TriState r2 = r6.A02
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r2 == r0) goto L_0x02f6
            X.1Y8 r1 = X.C53572l0.A03
            boolean r0 = r2.asBoolean()
            r4.putBoolean(r1, r0)
        L_0x02f6:
            com.facebook.common.util.TriState r2 = r6.A00
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r2 == r0) goto L_0x0305
            X.1Y8 r1 = X.C53572l0.A01
            boolean r0 = r2.asBoolean()
            r4.putBoolean(r1, r0)
        L_0x0305:
            com.facebook.common.util.TriState r2 = r6.A01
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r2 == r0) goto L_0x0314
            X.1Y8 r1 = X.C53572l0.A02
            boolean r0 = r2.asBoolean()
            r4.putBoolean(r1, r0)
        L_0x0314:
            r4.commit()
            X.2f9 r4 = r3.A00
            X.2l1 r3 = r3.A02
            java.util.concurrent.ExecutorService r2 = r4.A0B
            X.2fJ r1 = new X.2fJ
            r1.<init>(r4, r3, r5)
            r0 = 181226896(0xacd4d90, float:1.9769958E-32)
            X.AnonymousClass07A.A04(r2, r1, r0)
            X.2l2 r2 = r4.A07
            com.facebook.common.util.TriState r9 = r3.A00
            java.lang.Integer r6 = X.AnonymousClass07B.A01
            boolean r0 = r2.A06
            if (r0 == 0) goto L_0x0239
            X.1YI r3 = r2.A02
            r1 = 638(0x27e, float:8.94E-43)
            r0 = 0
            boolean r0 = r3.AbO(r1, r0)
            if (r0 == 0) goto L_0x0239
            X.0Tq r0 = r2.A05
            java.lang.Object r4 = r0.get()
            java.lang.String r4 = (java.lang.String) r4
            X.0bB r0 = r2.A01
            java.lang.String r3 = r0.A05()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r4)
            if (r0 != 0) goto L_0x190a
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)
            if (r0 != 0) goto L_0x190a
            android.content.Context r1 = r2.A00
            X.6Sz r2 = r2.A04
            r0 = 16
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            android.accounts.Account r8 = X.C17430yt.A00(r1, r0)
            if (r8 != 0) goto L_0x0382
            X.2l3 r1 = new X.2l3
            r1.<init>()
            r1.A01 = r6
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.A02 = r0
        L_0x0372:
            r1.A00 = r9
            java.lang.String r0 = X.C53592l2.A02(r4, r3)
            r1.A03 = r0
            X.6Sy r0 = r1.A00()
            r2.A03(r0)
            return
        L_0x0382:
            android.accounts.AccountManager r7 = X.C53592l2.A00(r1, r6, r2, r4, r3)
            if (r7 != 0) goto L_0x1894
            X.2l3 r1 = new X.2l3
            r1.<init>()
            r1.A01 = r6
            r1.A02 = r6
            goto L_0x0372
        L_0x0392:
            X.1EA r2 = (X.AnonymousClass1EA) r2
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            java.lang.Object r1 = r0.A03
            if (r1 == 0) goto L_0x1927
            X.2l4 r6 = r2.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r0 = -1780147386(0xffffffff95e51b46, float:-9.2535393E-26)
            java.lang.String r5 = r1.A0P(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 == 0) goto L_0x03c9
            r2 = 0
            int r1 = X.AnonymousClass1Y3.AHJ
            X.2l5 r0 = r6.A00
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.BMB r0 = (X.BMB) r0
            r0.A01()
            X.2l5 r0 = r6.A00
            X.2l8 r3 = r0.A02
            java.lang.String r2 = r6.A02
            java.lang.String r1 = r6.A01
            java.lang.String r0 = "account linking url is null or empty."
            r3.A01(r2, r1, r0)
            return
        L_0x03c9:
            r3 = 1
            int r2 = X.AnonymousClass1Y3.BRx
            X.2l5 r1 = r6.A00
            X.0UN r0 = r1.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.3Am r3 = (X.C64213Am) r3
            android.content.Context r2 = r1.A01
            android.net.Uri r1 = android.net.Uri.parse(r5)
            com.facebook.messaging.browser.model.MessengerInAppBrowserLaunchParam r0 = com.facebook.messaging.browser.model.MessengerInAppBrowserLaunchParam.A0A
            r3.A04(r2, r1, r0)
            X.2l5 r0 = r6.A00
            X.2l8 r0 = r0.A02
            java.lang.String r4 = r6.A02
            java.lang.String r3 = r6.A01
            X.1ZE r1 = r0.A00
            java.lang.String r0 = "account_linking_success"
            X.0bW r2 = r1.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r1 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 1
            r1.<init>(r2, r0)
            boolean r0 = r1.A0G()
            if (r0 == 0) goto L_0x0239
            r0 = 1199(0x4af, float:1.68E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r1.A0O(r0)
            r1.A0N(r4)
            java.lang.String r0 = "cta_id"
            r1.A0D(r0, r3)
            java.lang.String r0 = "account_link_url"
            r1.A0D(r0, r5)
            r1.A06()
            return
        L_0x0417:
            r0 = r2
            X.1E4 r0 = (X.AnonymousClass1E4) r0
            X.8sH r0 = r0.A01
            if (r0 == 0) goto L_0x0239
            r0.BQM()
            return
        L_0x0422:
            r1 = r2
            X.1E3 r1 = (X.AnonymousClass1E3) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.2l9 r5 = r1.A00
            java.lang.Object r7 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r7 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r7
            int r2 = X.AnonymousClass1Y3.A2t
            X.2lA r0 = r5.A01
            X.0UN r1 = r0.A02
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2lC r1 = (X.C53682lC) r1
            com.facebook.browserextensions.ipc.messengerplatform.permission.AskPermissionJSBridgeCall r0 = r5.A00
            r1.A00 = r0
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 625409519(0x2546fdef, float:1.7259798E-16)
            r0 = -1586007609(0xffffffffa17771c7, float:-8.383744E-19)
            com.facebook.graphservice.tree.TreeJNI r3 = r7.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x1bc4
            r1 = 704191545(0x29f91c39, float:1.1062717E-13)
            r0 = -1784841380(0xffffffff959d7b5c, float:-6.360642E-26)
            com.google.common.collect.ImmutableList r1 = r3.A0M(r1, r2, r0)
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x1bc4
            r0 = 0
            java.lang.Object r1 = r1.get(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r0 = 1780605094(0x6a21e0a6, float:4.892448E25)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x04a9
            r6 = 0
            if (r7 == 0) goto L_0x1bae
            r1 = 625409519(0x2546fdef, float:1.7259798E-16)
            r0 = -1586007609(0xffffffffa17771c7, float:-8.383744E-19)
            com.facebook.graphservice.tree.TreeJNI r3 = r7.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x1bae
            r1 = -303113191(0xffffffffedeedc19, float:-9.240439E27)
            r0 = 2014742715(0x781688bb, float:1.2212777E34)
            com.google.common.collect.ImmutableList r4 = r3.A0M(r1, r2, r0)
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x1bae
            int r2 = r4.size()
            java.lang.String[] r3 = new java.lang.String[r2]
        L_0x0495:
            if (r6 >= r2) goto L_0x1bb0
            java.lang.Object r1 = r4.get(r6)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r0 = -1621505110(0xffffffff9f59cbaa, float:-4.6120004E-20)
            java.lang.String r0 = r1.A0P(r0)
            r3[r6] = r0
            int r6 = r6 + 1
            goto L_0x0495
        L_0x04a9:
            X.2lA r6 = r5.A01
            com.facebook.browserextensions.ipc.messengerplatform.permission.AskPermissionJSBridgeCall r4 = r5.A00
            java.lang.String r3 = r5.A02
            if (r7 == 0) goto L_0x1bba
            r1 = 625409519(0x2546fdef, float:1.7259798E-16)
            r0 = -1586007609(0xffffffffa17771c7, float:-8.383744E-19)
            com.facebook.graphservice.tree.TreeJNI r5 = r7.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            if (r5 == 0) goto L_0x1bba
            r1 = 704191545(0x29f91c39, float:1.1062717E-13)
            r0 = -1784841380(0xffffffff959d7b5c, float:-6.360642E-26)
            com.google.common.collect.ImmutableList r1 = r5.A0M(r1, r2, r0)
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x1bba
            r0 = 0
            java.lang.Object r5 = r1.get(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            r1 = 23407941(0x1652d45, float:4.2093137E-38)
            r0 = 578811059(0x227ff4b3, float:3.4688487E-18)
            com.facebook.graphservice.tree.TreeJNI r8 = r5.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r8 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r8
            if (r8 == 0) goto L_0x1bba
            android.content.Intent r5 = new android.content.Intent
            android.content.Context r1 = r6.A00
            java.lang.Class<com.facebook.messaging.business.messengerextensions.permission.PlatformAskPermissionActivity> r0 = com.facebook.messaging.business.messengerextensions.permission.PlatformAskPermissionActivity.class
            r5.<init>(r1, r0)
            X.2lF r7 = new X.2lF
            r7.<init>()
            r1 = -724044987(0xffffffffd4d7f345, float:-7.4199948E12)
            r0 = -1611443706(0xffffffff9ff35206, float:-1.030502E-19)
            com.facebook.graphservice.tree.TreeJNI r0 = r8.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x05fc
            java.lang.String r0 = r0.A41()
        L_0x0504:
            r7.A05 = r0
            java.lang.String r0 = r8.A40()
            r7.A08 = r0
            java.lang.String r0 = r8.A3h()
            r7.A03 = r0
            r0 = -1126520037(0xffffffffbcdaab1b, float:-0.026692918)
            java.lang.String r0 = r8.A0P(r0)
            r7.A01 = r0
            r0 = 337895802(0x1423e17a, float:8.273864E-27)
            java.lang.String r0 = r8.A0P(r0)
            r7.A02 = r0
            r7.A06 = r3
            r0 = 97
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            java.lang.Object r0 = r4.A04(r0)
            java.lang.String r0 = (java.lang.String) r0
            r7.A07 = r0
            java.lang.String r0 = r4.A09()
            r7.A04 = r0
            r7.A00 = r4
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.lang.String r1 = r7.A05
            java.lang.String r0 = "profile_image_uri_string"
            r2.putString(r0, r1)
            java.lang.String r1 = r7.A08
            java.lang.String r0 = "title"
            r2.putString(r0, r1)
            java.lang.String r1 = r7.A03
            java.lang.String r0 = "description"
            r2.putString(r0, r1)
            java.lang.String r1 = r7.A01
            java.lang.String r0 = "accept_button"
            r2.putString(r0, r1)
            java.lang.String r1 = r7.A02
            java.lang.String r0 = "decline_button"
            r2.putString(r0, r1)
            java.lang.String r1 = r7.A06
            r0 = 319(0x13f, float:4.47E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r2.putString(r0, r1)
            java.lang.String r1 = r7.A07
            r0 = 30
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r2.putString(r0, r1)
            java.lang.String r1 = r7.A04
            java.lang.String r0 = "page_id"
            r2.putString(r0, r1)
            android.os.Parcelable r1 = r7.A00
            java.lang.String r0 = "js_bridge_call"
            r2.putParcelable(r0, r1)
            r5.putExtras(r2)
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r5.addFlags(r0)
            int r2 = X.AnonymousClass1Y3.A7S
            X.0UN r1 = r6.A02
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            com.facebook.content.SecureContextHelper r1 = (com.facebook.content.SecureContextHelper) r1
            android.content.Context r0 = r6.A00
            r1.startFacebookActivity(r5, r0)
            X.9ti r2 = r6.A01
            android.os.Bundle r0 = r4.Af2()
            X.2lG r1 = new X.2lG
            r1.<init>(r2, r0)
            java.lang.String[] r0 = new java.lang.String[]{r3}
            java.util.List r3 = java.util.Arrays.asList(r0)
            java.util.Set r0 = r1.A07
            java.util.Iterator r5 = r0.iterator()
        L_0x05b9:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0239
            java.lang.Object r2 = r5.next()
            X.9tX r2 = (X.C209009tX) r2
            android.os.Bundle r0 = r4.Af2()
            boolean r0 = r2.BBt(r0)
            if (r0 == 0) goto L_0x05b9
            android.os.Bundle r1 = r4.Af2()
            java.lang.String r0 = "browser_extensions_permission_dialog_shown"
            X.1La r2 = X.C209009tX.A00(r2, r0, r1)
            if (r2 == 0) goto L_0x05b9
            java.lang.String r1 = r4.A05
            java.lang.String r0 = "website_url"
            r2.A06(r0, r1)
            java.lang.String r1 = r4.A04
            r0 = 379(0x17b, float:5.31E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r2.A06(r0, r1)
            java.lang.String r0 = ","
            java.lang.String r1 = android.text.TextUtils.join(r0, r3)
            java.lang.String r0 = "permission_requested"
            r2.A06(r0, r1)
            r2.A0A()
            goto L_0x05b9
        L_0x05fc:
            r0 = 0
            goto L_0x0504
        L_0x05ff:
            r3 = r2
            X.1FB r3 = (X.AnonymousClass1FB) r3
            X.16i r0 = (X.C189816i) r0
            X.16a r1 = r3.A00
            r1.A04 = r0
            r2 = 0
            r1.A00 = r2
            X.1Ap r1 = r1.A01
            if (r1 == 0) goto L_0x0239
            r1.Bgh(r2, r0)
            X.16a r0 = r3.A00
            X.1Ap r1 = r0.A01
            X.16i r0 = r0.A04
            r1.BdJ(r2, r0)
            return
        L_0x061c:
            r0 = r2
            X.1A4 r0 = (X.AnonymousClass1A4) r0
            X.70e r1 = r0.A00
            if (r1 == 0) goto L_0x0239
            java.lang.String r0 = r0.A02
            r1.Bqh(r0)
            return
        L_0x0629:
            r4 = r2
            X.1E2 r4 = (X.AnonymousClass1E2) r4
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.70e r1 = r4.A00
            if (r1 == 0) goto L_0x0239
            java.lang.Object r3 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -1476146057(0xffffffffa803cc77, float:-7.316297E-15)
            r0 = -804619969(0xffffffffd00a793f, float:-9.2928072E9)
            com.facebook.graphservice.tree.TreeJNI r0 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x0239
            X.70e r1 = r4.A00
            java.lang.String r0 = r0.A3m()
            r1.Bqh(r0)
            return
        L_0x0650:
            r3 = r2
            X.1E1 r3 = (X.AnonymousClass1E1) r3
            X.2lH r1 = r3.A00
            X.2gv r2 = r1.A00
            if (r2 == 0) goto L_0x0239
            java.lang.String r5 = r3.A01
            X.8za r1 = r1.A02
            com.google.common.collect.ImmutableList r4 = r1.CJL(r0)
            X.2fS r3 = r2.A00
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x1e64
            com.facebook.messaging.location.model.NearbyPlace r0 = r3.A02
            if (r0 != 0) goto L_0x1e5a
            com.facebook.messaging.location.picker.NearbyPlacesView r2 = r3.A07
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            r0 = 2131823361(0x7f110b01, float:1.927952E38)
            if (r1 == 0) goto L_0x067b
            r0 = 2131823362(0x7f110b02, float:1.9279522E38)
        L_0x067b:
            java.lang.String r0 = r3.A1A(r0)
            r2.A0M(r0)
            return
        L_0x0683:
            r1 = r2
            X.1Qu r1 = (X.C23771Qu) r1
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            X.2fT r7 = r1.A01
            com.facebook.messaging.media.loader.LocalMediaLoaderParams r6 = r1.A00
            X.2lI r5 = r7.A04
            int r1 = r0.size()
            com.facebook.quicklog.QuickPerformanceLogger r4 = r5.A00
            java.lang.String r2 = java.lang.String.valueOf(r1)
            r3 = 5505085(0x54003d, float:7.714267E-39)
            java.lang.String r1 = "final_count"
            r4.markerAnnotate(r3, r1, r2)
            com.facebook.quicklog.QuickPerformanceLogger r2 = r5.A00
            r1 = 2
            r2.markerEnd(r3, r1)
            X.1Ap r1 = r7.A00
            if (r1 == 0) goto L_0x0239
            r1.Bgh(r6, r0)
            X.1Ap r1 = r7.A00
            r1.BdJ(r6, r0)
            return
        L_0x06b3:
            X.1Dx r2 = (X.C20811Dx) r2
            com.facebook.ui.media.attachments.model.MediaResource r0 = (com.facebook.ui.media.attachments.model.MediaResource) r0
            com.facebook.messaging.media.picker.MediaPickerPopupVideoView r1 = r2.A00
            r1.A04 = r0
            boolean r0 = r1.A09
            if (r0 == 0) goto L_0x0239
            X.2lJ r0 = X.C53752lJ.A05
            r1.A0M(r0)
            com.facebook.messaging.media.picker.MediaPickerPopupVideoView r1 = r2.A00
            r0 = 0
            r1.A09 = r0
            return
        L_0x06ca:
            r3 = r2
            X.1Dt r3 = (X.C20771Dt) r3
            com.facebook.spherical.photo.metadata.SphericalPhotoMetadata r0 = (com.facebook.spherical.photo.metadata.SphericalPhotoMetadata) r0
            if (r0 == 0) goto L_0x0239
            X.1Dr r1 = r3.A01
            X.3Cp r1 = r1.A0A
            boolean r1 = r1.A04()
            if (r1 == 0) goto L_0x0239
            X.2Qw r2 = com.facebook.ui.media.attachments.model.MediaResource.A00()
            com.facebook.ui.media.attachments.model.MediaResource r1 = r3.A02
            r2.A01(r1)
            r1 = 1
            r2.A0k = r1
            r2.A0H = r0
            com.facebook.ui.media.attachments.model.MediaResource r1 = r2.A00()
            java.util.ArrayList r2 = new java.util.ArrayList
            X.1Dr r0 = r3.A01
            com.google.common.collect.ImmutableList r0 = r0.A02
            r2.<init>(r0)
            int r0 = r3.A00
            r2.set(r0, r1)
            X.1Dr r1 = r3.A01
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r2)
            r1.A02 = r0
            X.1Dr r1 = r3.A01
            int r0 = r3.A00
            r1.A05(r0)
            return
        L_0x070b:
            r5 = r2
            X.1Dq r5 = (X.C20741Dq) r5
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            X.2lK r1 = r5.A00
            if (r1 == 0) goto L_0x0239
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape3S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape3S0000000.class
            r2 = 1616625511(0x605bbf67, float:6.3337952E19)
            r1 = 423528630(0x193e88b6, float:9.850378E-24)
            com.facebook.graphservice.tree.TreeJNI r0 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape3S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape3S0000000) r0
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r4 = X.C35971s7.A00(r0)
            X.2lK r3 = r5.A00
            X.2lL r0 = r3.A01
            X.9MN r0 = r0.A01
            if (r0 == 0) goto L_0x0731
            r0.dismiss()
        L_0x0731:
            if (r4 != 0) goto L_0x1fa8
            X.2lL r0 = r3.A01
            X.C53772lL.A00(r0)
            return
        L_0x0739:
            r1 = r2
            X.1Ds r1 = (X.C20761Ds) r1
            java.util.List r0 = (java.util.List) r0
            X.2lM r4 = r1.A01
            X.2lO r1 = r4.A01
            boolean r1 = r1.A1X()
            if (r1 == 0) goto L_0x0239
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.Iterator r9 = r0.iterator()
        L_0x0751:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x1fd3
            java.lang.Object r5 = r9.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            java.lang.String r8 = r5.A3m()
            if (r8 == 0) goto L_0x0751
            com.facebook.graphql.enums.GraphQLLightweightEventType r7 = r5.A0j()
            if (r7 == 0) goto L_0x0751
            r0 = 450436211(0x1ad91c73, float:8.979506E-23)
            java.lang.String r2 = r5.A0P(r0)
            if (r2 != 0) goto L_0x077f
            X.2lO r0 = r4.A01
            android.content.res.Resources r1 = r0.A12()
            r0 = 2131829305(0x7f112239, float:1.9291575E38)
            java.lang.String r2 = r1.getString(r0)
        L_0x077f:
            X.2lN r6 = new X.2lN
            r6.<init>()
            r6.A06 = r8
            r0 = 3560141(0x3652cd, float:4.98882E-39)
            long r0 = r5.getTimeValue(r0)
            r6.A01 = r0
            r6.A02 = r7
            r0 = 1725551537(0x66d9d3b1, float:5.1432927E23)
            long r0 = r5.getTimeValue(r0)
            r6.A00 = r0
            r0 = 1909973392(0x71d7e190, float:2.1379829E30)
            boolean r0 = r5.getBooleanValue(r0)
            r6.A0A = r0
            r6.A09 = r2
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -1149857770(0xffffffffbb769016, float:-0.0037622503)
            r0 = 1605838637(0x5fb7272d, float:2.6395133E19)
            com.facebook.graphservice.tree.TreeJNI r0 = r5.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x07c1
            java.lang.String r0 = r0.A3m()
            if (r0 == 0) goto L_0x07c1
            com.facebook.user.model.UserKey r0 = com.facebook.user.model.UserKey.A01(r0)
            r6.A04 = r0
        L_0x07c1:
            r0 = -1796793131(0xffffffff94e71cd5, float:-2.3336411E-26)
            java.lang.String r0 = r5.A0P(r0)
            if (r0 == 0) goto L_0x07cc
            r6.A07 = r0
        L_0x07cc:
            r1 = 1026442562(0x3d2e4542, float:0.04254652)
            r0 = 1340369980(0x4fe46c3c, float:7.664597E9)
            com.facebook.graphservice.tree.TreeJNI r5 = r5.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            if (r5 == 0) goto L_0x0805
            r1 = -1184643414(0xffffffffb963c6aa, float:-2.1722415E-4)
            r0 = 21062027(0x141618b, float:3.5518485E-38)
            com.facebook.graphservice.tree.TreeJNI r0 = r5.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x0805
            java.lang.String r5 = r0.A3s()
            if (r5 == 0) goto L_0x080f
            r2 = 0
            int r1 = X.AnonymousClass1Y3.BGy
            X.2lO r0 = r4.A01
            X.0UN r0 = r0.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0uY r2 = (X.C15000uY) r2
            long r0 = java.lang.Long.parseLong(r5)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r2.A01(r0)
        L_0x0803:
            r6.A03 = r0
        L_0x0805:
            com.facebook.messaging.model.threads.ThreadEventReminder r0 = new com.facebook.messaging.model.threads.ThreadEventReminder
            r0.<init>(r6)
            r3.add(r0)
            goto L_0x0751
        L_0x080f:
            java.lang.String r0 = r0.A3z()
            long r0 = java.lang.Long.parseLong(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A00(r0)
            goto L_0x0803
        L_0x081c:
            r1 = r2
            X.1Do r1 = (X.C20721Do) r1
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            X.2lP r5 = r1.A00
            if (r5 == 0) goto L_0x0239
            X.2lQ r3 = r5.A00
            android.widget.ProgressBar r2 = r3.A03
            r1 = 8
            r2.setVisibility(r1)
            android.view.View r2 = r3.A02
            r1 = 0
            r2.setVisibility(r1)
            if (r0 == 0) goto L_0x0843
            X.2lQ r1 = r5.A00
            X.DE1 r1 = r1.A0D
            r1.A0F(r0)
        L_0x083d:
            X.2lQ r0 = r5.A00
            X.C53822lQ.A00(r0)
            return
        L_0x0843:
            X.2lQ r4 = r5.A00
            android.content.Context r1 = r4.A01
            r0 = 2131830597(0x7f112745, float:1.9294196E38)
            java.lang.String r3 = r1.getString(r0)
            X.4pn r2 = r4.A07
            android.content.Context r1 = r4.A01
            r0 = 2131830596(0x7f112744, float:1.9294194E38)
            java.lang.String r0 = r1.getString(r0)
            r2.A00(r0, r3)
            goto L_0x083d
        L_0x085d:
            r4 = r2
            X.1Dl r4 = (X.C20691Dl) r4
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x087e
            java.lang.String r1 = r0.A3j()
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r1 == 0) goto L_0x087e
            java.lang.String r1 = r0.A3k()
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r1 == 0) goto L_0x087e
            r2 = 0
        L_0x0879:
            X.0Wg r1 = r4.A01
            if (r1 == 0) goto L_0x0239
            goto L_0x08af
        L_0x087e:
            X.2lR r1 = r4.A00
            X.4pn r3 = r1.A00
            java.lang.String r2 = r0.A3k()
            java.lang.String r1 = r0.A3j()
            r3.A00(r2, r1)
            r2 = 1
            goto L_0x0879
        L_0x088f:
            r4 = r2
            X.1Dm r4 = (X.C20701Dm) r4
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x08b2
            java.lang.String r1 = r0.A3j()
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r1 == 0) goto L_0x08b2
            java.lang.String r1 = r0.A3k()
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r1 == 0) goto L_0x08b2
            r2 = 0
        L_0x08ab:
            X.0Wg r1 = r4.A01
            if (r1 == 0) goto L_0x0239
        L_0x08af:
            if (r2 == 0) goto L_0x0911
            goto L_0x0910
        L_0x08b2:
            X.2lR r1 = r4.A00
            X.4pn r3 = r1.A00
            java.lang.String r2 = r0.A3k()
            java.lang.String r1 = r0.A3j()
            r3.A00(r2, r1)
            r2 = 1
            goto L_0x08ab
        L_0x08c3:
            r5 = r2
            X.1Dk r5 = (X.C20681Dk) r5
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            r6 = 1
            if (r0 == 0) goto L_0x0917
            java.lang.String r1 = r0.A3j()
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r1 == 0) goto L_0x0917
            java.lang.String r1 = r0.A3k()
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r1 == 0) goto L_0x0917
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1165870106(0xffffffffba823be6, float:-9.936064E-4)
            r1 = -1863968103(0xffffffff90e61a99, float:-9.0759984E-29)
            com.facebook.graphservice.tree.TreeJNI r1 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 != 0) goto L_0x0915
            X.2lR r1 = r5.A00
            X.4pn r4 = r1.A00
            android.content.res.Resources r2 = r1.A02
            r1 = 2131830596(0x7f112744, float:1.9294194E38)
            java.lang.String r3 = r2.getString(r1)
            X.2lR r1 = r5.A00
            android.content.res.Resources r2 = r1.A02
            r1 = 2131830594(0x7f112742, float:1.929419E38)
            java.lang.String r1 = r2.getString(r1)
            r4.A00(r3, r1)
        L_0x090a:
            X.0Wg r1 = r5.A01
            if (r1 == 0) goto L_0x0239
            if (r6 == 0) goto L_0x0911
        L_0x0910:
            r0 = 0
        L_0x0911:
            r1.Bqf(r0)
            return
        L_0x0915:
            r6 = 0
            goto L_0x090a
        L_0x0917:
            X.2lR r1 = r5.A00
            X.4pn r3 = r1.A00
            java.lang.String r2 = r0.A3k()
            java.lang.String r1 = r0.A3j()
            r3.A00(r2, r1)
            goto L_0x090a
        L_0x0927:
            r0 = r2
            X.1Dj r0 = (X.C20671Dj) r0
            X.2lS r0 = r0.A00
            X.2lT r0 = r0.A00
            if (r0 == 0) goto L_0x0239
            X.2lU r0 = r0.A00
            X.2lV r0 = r0.A01
            X.6PI r0 = r0.A06
            r0.BV8()
            return
        L_0x093a:
            r5 = r2
            X.1Di r5 = (X.C20661Di) r5
            com.facebook.ui.media.attachments.model.MediaResource r0 = (com.facebook.ui.media.attachments.model.MediaResource) r0
            X.2lW r1 = r5.A00
            X.0pP r2 = r1.A0S
            r1 = 65
            java.lang.String r1 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r1)
            boolean r1 = r2.A08(r1)
            if (r1 == 0) goto L_0x0239
            X.2lW r2 = r5.A00
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r2.A0J
            boolean r1 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r1)
            if (r1 != 0) goto L_0x0239
            com.facebook.prefs.shared.FbSharedPreferences r2 = r2.A0R
            X.1Y7 r1 = X.C53902lY.A00
            com.facebook.common.util.TriState r2 = r2.Aeq(r1)
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.UNSET
            if (r2 != r1) goto L_0x09a9
            X.2lW r2 = r5.A00
            com.facebook.prefs.shared.FbSharedPreferences r1 = r2.A0R
            X.1hn r4 = r1.edit()
            X.0rX r3 = new X.0rX
            android.content.Context r1 = r2.getContext()
            r3.<init>(r1)
            r1 = 2131831076(0x7f112924, float:1.9295167E38)
            r3.A09(r1)
            X.2lW r1 = r5.A00
            java.lang.Boolean r1 = r1.A0Y
            boolean r2 = r1.booleanValue()
            r1 = 2131831072(0x7f112920, float:1.929516E38)
            if (r2 == 0) goto L_0x098c
            r1 = 2131831073(0x7f112921, float:1.9295161E38)
        L_0x098c:
            r3.A08(r1)
            r2 = 2131831075(0x7f112923, float:1.9295165E38)
            X.2lZ r1 = new X.2lZ
            r1.<init>(r5, r4, r0)
            r3.A02(r2, r1)
            r1 = 2131831074(0x7f112922, float:1.9295163E38)
            X.2la r0 = new X.2la
            r0.<init>(r4)
            r3.A00(r1, r0)
            r3.A07()
            return
        L_0x09a9:
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.YES
            if (r2 != r1) goto L_0x0239
            X.C20661Di.A00(r5, r0)
            return
        L_0x09b1:
            r8 = r2
            X.1Dh r8 = (X.C20651Dh) r8
            com.facebook.ui.media.attachments.model.MediaResource r0 = (com.facebook.ui.media.attachments.model.MediaResource) r0
            com.google.common.base.Preconditions.checkNotNull(r0)
            X.2lW r1 = r8.A01
            X.1Y6 r1 = r1.A08
            r1.AOz()
            X.2lW r1 = r8.A01
            X.2lb r4 = r1.A0I
            if (r4 == 0) goto L_0x0239
            r3 = 0
            if (r3 == 0) goto L_0x09d6
            X.28p r2 = r3.A0L
            X.28p r1 = r0.A0L
            if (r2 != r1) goto L_0x09d6
            X.2Qx r2 = r3.A0J
            X.2Qx r1 = r0.A0J
            if (r2 != r1) goto L_0x09d6
            r0 = r3
        L_0x09d6:
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r8.A00
            X.2lc r1 = r4.A00
            com.facebook.messaging.composer.ComposerKeyboardManager r1 = r1.A00
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r1.A07
            boolean r1 = com.google.common.base.Objects.equal(r1, r2)
            if (r1 == 0) goto L_0x0a1c
            X.2lc r1 = r4.A00
            com.facebook.messaging.composer.ComposerKeyboardManager r1 = r1.A00
            X.2le r4 = r1.A05
            if (r4 == 0) goto L_0x0a1c
            int r3 = X.AnonymousClass1Y3.A6G
            com.facebook.messaging.composer.ComposeFragment r1 = r4.A00
            X.0UN r2 = r1.A09
            r1 = 4
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0so r3 = (X.C14220so) r3
            java.lang.Integer r2 = X.AnonymousClass07B.A02
            java.lang.String r1 = "Send QuickCam"
            r3.A01(r1, r2)
            com.facebook.messaging.composer.ComposeFragment r7 = r4.A00
            X.2lf r6 = X.C53972lf.A0C
            com.google.common.collect.ImmutableMap r5 = com.google.common.collect.RegularImmutableMap.A03
            X.28p r3 = r0.A0L
            X.28p r1 = X.C421828p.PHOTO
            if (r3 == r1) goto L_0x0a15
            X.28p r1 = X.C421828p.VIDEO
            if (r3 == r1) goto L_0x0a15
            X.28p r2 = X.C421828p.ANIMATED_PHOTO
            r1 = 0
            if (r3 != r2) goto L_0x0a16
        L_0x0a15:
            r1 = 1
        L_0x0a16:
            com.google.common.base.Preconditions.checkState(r1)
            com.facebook.messaging.composer.ComposeFragment.A0W(r7, r0, r6, r5, r5)
        L_0x0a1c:
            X.28p r1 = r0.A0L
            X.28p r0 = X.C421828p.PHOTO
            if (r1 != r0) goto L_0x0a6f
            X.2lW r0 = r8.A01
            com.facebook.prefs.shared.FbSharedPreferences r2 = r0.A0R
            X.1Y7 r1 = X.C05690aA.A1U
            r0 = 0
            int r3 = r2.AqN(r1, r0)
            r0 = 3
            if (r3 >= r0) goto L_0x0a40
            X.2lW r0 = r8.A01
            com.facebook.prefs.shared.FbSharedPreferences r0 = r0.A0R
            X.1hn r2 = r0.edit()
            int r0 = r3 + 1
            r2.Bz6(r1, r0)
            r2.commit()
        L_0x0a40:
            X.2lW r0 = r8.A01
            X.2lg r2 = r0.A0O
            X.DHx r1 = r0.A09
            boolean r0 = X.C53882lW.A0K(r0)
            X.146 r4 = r2.A00
            java.util.Map r3 = X.C53982lg.A01(r2, r1, r0)
            r0 = 39
            java.lang.String r2 = X.C22298Ase.$const$string(r0)
            java.lang.String r1 = "button"
            java.lang.String r0 = "send_from_insta"
            r4.A0E(r2, r1, r0, r3)
        L_0x0a5d:
            X.2lW r0 = r8.A01
            X.C53882lW.A0B(r0)
            X.2lW r1 = r8.A01
            r0 = 0
            r1.A0e = r0
            boolean r0 = r1.A0d
            if (r0 == 0) goto L_0x0239
            X.C53882lW.A07(r1)
            return
        L_0x0a6f:
            X.28p r0 = X.C421828p.VIDEO
            if (r1 != r0) goto L_0x0a5d
            X.2lW r0 = r8.A01
            com.facebook.prefs.shared.FbSharedPreferences r2 = r0.A0R
            X.1Y7 r1 = X.C05690aA.A1V
            r0 = 0
            int r3 = r2.AqN(r1, r0)
            r0 = 3
            if (r3 >= r0) goto L_0x0a5d
            X.2lW r0 = r8.A01
            com.facebook.prefs.shared.FbSharedPreferences r0 = r0.A0R
            X.1hn r2 = r0.edit()
            int r0 = r3 + 1
            r2.Bz6(r1, r0)
            r2.commit()
            goto L_0x0a5d
        L_0x0a92:
            r4 = r2
            X.1Df r4 = (X.C20631Df) r4
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            java.lang.Object r3 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 1471135030(0x57afbd36, float:3.86454379E14)
            r0 = 234553283(0xdfaffc3, float:1.5469012E-30)
            com.facebook.graphservice.tree.TreeJNI r0 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x0239
            java.lang.String r1 = r0.A3m()
            if (r1 == 0) goto L_0x0239
            X.2lh r6 = r4.A00
            java.lang.String r0 = "feedback:"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r1)
            byte[] r1 = r0.getBytes()
            r0 = 0
            java.lang.String r5 = android.util.Base64.encodeToString(r1, r0)
            com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000 r4 = new com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000
            r0 = 124(0x7c, float:1.74E-43)
            r4.<init>(r0)
            java.lang.String r1 = r6.A0A
            java.lang.String r0 = "text"
            r4.A09(r0, r1)
            X.3DS r3 = r6.A08
            X.2lj r2 = new X.2lj
            r2.<init>(r6, r5, r4)
            X.1Dd r1 = new X.1Dd
            r1.<init>(r6)
            java.lang.String r0 = "save_to_faq_comment"
            r3.A0C(r0, r2, r1)
            return
        L_0x0ae2:
            r5 = r2
            X.1jX r5 = (X.C31341jX) r5
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.2lk r1 = r5.A00
            X.4xO r1 = r1.A00
            if (r1 == 0) goto L_0x0239
            if (r0 == 0) goto L_0x2091
            java.lang.Object r4 = r0.A03
            if (r4 == 0) goto L_0x2091
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -379606751(0xffffffffe95fa921, float:-1.6899322E25)
            r0 = 1198424155(0x476e805b, float:61056.355)
            com.facebook.graphservice.tree.TreeJNI r3 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x2091
            r1 = 1913864075(0x72133f8b, float:2.9165514E30)
            r0 = 392532003(0x17659023, float:7.4175817E-25)
            com.facebook.graphservice.tree.TreeJNI r0 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x2091
            X.2lk r0 = r5.A00
            X.4xO r2 = r0.A00
            X.2lm r1 = new X.2lm
            X.5uT r0 = r3.A0o()
            r1.<init>(r4, r0)
            r2.AZb(r1)
            return
        L_0x0b24:
            r5 = r2
            X.1DP r5 = (X.AnonymousClass1DP) r5
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass0j4.A07(r0, r1)
            com.facebook.messaging.sharedimage.SharedMedia r1 = (com.facebook.messaging.sharedimage.SharedMedia) r1
            if (r1 == 0) goto L_0x0b57
            com.facebook.ui.media.attachments.model.MediaResource r1 = r1.Atk()
            long r3 = r1.A05
            r1 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r1
            java.lang.String r8 = java.lang.Long.toString(r3)
        L_0x0b3f:
            boolean r1 = r0.isEmpty()
            r10 = r1 ^ 1
            X.2lm r6 = new X.2lm
            r7 = 0
            r9 = 1
            r11 = r0
            r6.<init>(r7, r8, r9, r10, r11)
            X.2ln r0 = r5.A00
            X.4xO r0 = r0.A00
            if (r0 == 0) goto L_0x0239
            r0.AZb(r6)
            return
        L_0x0b57:
            java.lang.String r8 = ""
            goto L_0x0b3f
        L_0x0b5a:
            r4 = r2
            X.1DO r4 = (X.AnonymousClass1DO) r4
            X.2lo r0 = (X.C54062lo) r0
            com.facebook.messaging.sharing.SingleRecipientShareLauncherActivity r3 = r4.A00
            r2 = 0
            r3.A0B = r2
            boolean r1 = r0.A02
            if (r1 == 0) goto L_0x0b72
            r0 = 0
            r3.setResult(r0)
            com.facebook.messaging.sharing.SingleRecipientShareLauncherActivity r0 = r4.A00
            r0.finish()
            return
        L_0x0b72:
            X.7Ey r1 = r0.A00
            r3.A08 = r1
            X.72i r0 = r0.A01
            r3.A0A = r0
            X.9MN r0 = r3.A01
            if (r0 == 0) goto L_0x0b8a
            r0.dismiss()
            com.facebook.messaging.sharing.SingleRecipientShareLauncherActivity r1 = r4.A00
            X.7Ab r0 = com.facebook.messaging.sharing.SingleRecipientShareLauncherActivity.A00(r1)
            com.facebook.messaging.sharing.SingleRecipientShareLauncherActivity.A02(r1, r0)
        L_0x0b8a:
            com.facebook.messaging.sharing.SingleRecipientShareLauncherActivity r0 = r4.A00
            com.facebook.messaging.sharing.SingleRecipientShareComposerFragment r1 = r0.A09
            X.72i r0 = r0.A0A
            r1.A03 = r0
            boolean r0 = r1.A06
            if (r0 == 0) goto L_0x0239
            com.facebook.messaging.sharing.SingleRecipientShareComposerFragment.A00(r1)
            return
        L_0x0b9a:
            r5 = r2
            X.1jV r5 = (X.C31321jV) r5
            com.facebook.ui.media.attachments.model.MediaResource r0 = (com.facebook.ui.media.attachments.model.MediaResource) r0
            com.facebook.messaging.sharing.mediapreview.MediaSharePreviewPlayableView r1 = r5.A00
            X.2lp r1 = r1.A07
            if (r1 == 0) goto L_0x0bae
            com.facebook.messaging.sharing.mediapreview.MediaSharePreviewThumbnailView r1 = r1.A00
            com.facebook.widget.listview.EmptyListViewItem r2 = r1.A03
            r1 = 8
            r2.setVisibility(r1)
        L_0x0bae:
            X.28p r2 = r0.A0L
            X.28p r1 = X.C421828p.VIDEO
            if (r2 != r1) goto L_0x0c9d
            com.facebook.messaging.sharing.mediapreview.MediaSharePreviewPlayableView r3 = r5.A00
            r1 = 2131299589(0x7f090d05, float:1.8217184E38)
            android.view.View r2 = X.C012809p.A01(r3, r1)
            com.facebook.drawee.fbpipeline.FbDraweeView r2 = (com.facebook.drawee.fbpipeline.FbDraweeView) r2
            r3.A05 = r2
            android.net.Uri r1 = r0.A0B
            if (r1 != 0) goto L_0x0c70
            r1 = 8
            r2.setVisibility(r1)
            com.facebook.drawee.fbpipeline.FbDraweeView r2 = r3.A05
            r1 = 0
            r2.A08(r1)
        L_0x0bd0:
            com.facebook.messaging.sharing.mediapreview.MediaSharePreviewPlayableView r4 = r5.A00
            r1 = 2131298448(0x7f090890, float:1.821487E38)
            android.view.View r2 = X.C012809p.A01(r4, r1)
            com.facebook.video.player.FbVideoView r2 = (com.facebook.video.player.FbVideoView) r2
            r4.A0B = r2
            X.2lq r1 = X.C54082lq.CENTER
            r2.A0W(r1)
            com.facebook.video.player.FbVideoView r1 = r4.A0B
            r5 = 1
            r1.A0Z(r5)
            X.2lr r2 = new X.2lr
            r2.<init>()
            android.net.Uri r1 = r0.A0D
            r2.A03 = r1
            X.2ls r1 = X.C54102ls.FROM_LOCAL_STORAGE
            r2.A04 = r1
            boolean r1 = r0.A0e
            if (r1 == 0) goto L_0x0c6d
            X.2lt r1 = X.C54112lt.MIRROR_HORIZONTALLY
        L_0x0bfb:
            r2.A05 = r1
            com.facebook.video.engine.api.VideoDataSource r1 = new com.facebook.video.engine.api.VideoDataSource
            r1.<init>(r2)
            X.2lv r6 = new X.2lv
            r6.<init>()
            r6.A0H = r1
            java.lang.String r1 = r0.A03()
            r6.A0O = r1
            long r2 = r0.A07
            int r1 = (int) r2
            r6.A0B = r1
            r6.A0n = r5
            com.facebook.video.engine.api.VideoPlayerParams r3 = new com.facebook.video.engine.api.VideoPlayerParams
            r3.<init>(r6)
            com.facebook.video.player.FbVideoView r2 = r4.A0B
            X.2lw r1 = new X.2lw
            r1.<init>()
            r1.A02 = r3
            X.8Aj r1 = r1.A00()
            r2.A0T(r1)
            com.facebook.video.player.FbVideoView r2 = r4.A0B
            X.2lJ r1 = X.C53752lJ.A05
            r2.C9f(r5, r1)
            com.facebook.video.player.FbVideoView r2 = r4.A0B
            X.2lx r1 = X.C54152lx.A0E
            r2.A0S(r1)
            r4.A0E = r5
            X.2lJ r3 = X.C53752lJ.A05
            com.facebook.video.player.FbVideoView r1 = r4.A0B
            if (r1 == 0) goto L_0x0c56
            boolean r1 = r1.A0d()
            if (r1 != 0) goto L_0x0c56
            boolean r1 = r4.A0E
            if (r1 == 0) goto L_0x0c56
            com.facebook.video.player.FbVideoView r2 = r4.A0B
            r1 = 0
            r2.setVisibility(r1)
            com.facebook.video.player.FbVideoView r1 = r4.A0B
            r1.Bx4(r3)
        L_0x0c56:
            X.3tN r2 = r4.A0A
            int r0 = r0.A01()
            long r0 = (long) r0
            java.lang.String r1 = r2.A01(r0)
            android.widget.TextView r0 = r4.A02
            r0.setText(r1)
            android.widget.TextView r1 = r4.A02
            r0 = 0
            r1.setVisibility(r0)
            return
        L_0x0c6d:
            X.2lt r1 = X.C54112lt.NONE
            goto L_0x0bfb
        L_0x0c70:
            r1 = 0
            r2.setVisibility(r1)
            com.facebook.drawee.fbpipeline.FbDraweeView r4 = r3.A05
            X.2zZ r3 = r3.A04
            android.net.Uri r1 = r0.A0B
            X.1Pv r2 = X.C23521Pv.A00(r1)
            X.9QO r1 = X.AnonymousClass9QO.A00(r0)
            r2.A09 = r1
            X.1Q0 r1 = r2.A02()
            r3.A0A(r1)
            java.lang.Class<com.facebook.messaging.sharing.mediapreview.MediaSharePreviewPlayableView> r1 = com.facebook.messaging.sharing.mediapreview.MediaSharePreviewPlayableView.class
            com.facebook.common.callercontext.CallerContext r1 = com.facebook.common.callercontext.CallerContext.A04(r1)
            r3.A0Q(r1)
            X.303 r1 = r3.A0F()
            r4.A08(r1)
            goto L_0x0bd0
        L_0x0c9d:
            X.28p r1 = X.C421828p.AUDIO
            if (r2 != r1) goto L_0x0239
            com.facebook.messaging.sharing.mediapreview.MediaSharePreviewPlayableView r5 = r5.A00
            r1 = 2131296643(0x7f090183, float:1.8211208E38)
            android.view.View r1 = X.C012809p.A01(r5, r1)
            android.widget.ImageView r1 = (android.widget.ImageView) r1
            r5.A01 = r1
            android.widget.TextView r4 = r5.A02
            long r2 = r0.A07
            float r1 = (float) r2
            r0 = 1148846080(0x447a0000, float:1000.0)
            float r1 = r1 / r0
            int r0 = java.lang.Math.round(r1)
            int r1 = r0 / 60
            int r0 = r0 % 60
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            r0 = 28
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r2, r1)
            r4.setText(r0)
            android.widget.TextView r1 = r5.A02
            r0 = 0
            r1.setVisibility(r0)
            return
        L_0x0cda:
            r4 = r2
            X.1DJ r4 = (X.AnonymousClass1DJ) r4
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x0239
            java.lang.Object r3 = r0.A03
            if (r3 == 0) goto L_0x0239
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 845998327(0x326ce8f7, float:1.3789978E-8)
            r0 = 2132645373(0x7f1d95fd, float:2.0946758E38)
            com.facebook.graphservice.tree.TreeJNI r0 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x0239
            X.2ly r5 = r4.A00
            java.lang.String r1 = r0.A3m()
            X.2ly r3 = r4.A00
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r3.A05
            com.facebook.payments.checkout.model.CheckoutCommonParams r0 = r0.A02()
            com.facebook.payments.model.PaymentItemType r0 = r0.AxY()
            java.lang.String r0 = r0.mValue
            X.3zu r0 = com.facebook.payments.checkout.model.PaymentsSessionData.A00(r1, r0)
            com.facebook.payments.checkout.model.PaymentsSessionData r2 = new com.facebook.payments.checkout.model.PaymentsSessionData
            r2.<init>(r0)
            X.2m0 r1 = new X.2m0
            r1.<init>()
            r1.A01 = r2
            r0 = 1288(0x508, float:1.805E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            X.C28931fb.A06(r2, r0)
            com.facebook.payments.checkout.model.PaymentsSessionStatusData r0 = new com.facebook.payments.checkout.model.PaymentsSessionStatusData
            r0.<init>(r1)
            r5.A04 = r0
            X.CB8 r1 = r3.A02
            if (r1 == 0) goto L_0x0d34
            com.facebook.payments.checkout.model.PaymentsSessionStatusData r0 = r3.A04
            r1.A00(r0)
        L_0x0d34:
            X.2ly r0 = r4.A00
            X.C54162ly.A02(r0)
            return
        L_0x0d3a:
            r5 = r2
            X.1DG r5 = (X.AnonymousClass1DG) r5
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x0239
            X.2ly r2 = r5.A00
            X.C54162ly.A04(r2, r0)
            X.2m1 r0 = r2.A03
            long r0 = r0.A01
            X.C54162ly.A03(r2, r0)
            android.os.Handler r4 = new android.os.Handler
            r4.<init>()
            r2.A00 = r4
            X.2m2 r3 = new X.2m2
            r3.<init>(r2)
            X.2m1 r0 = r2.A03
            long r1 = r0.A02
            r0 = 1531469564(0x5b485efc, float:5.6399432E16)
            X.AnonymousClass00S.A05(r4, r3, r1, r0)
            X.2ly r2 = r5.A00
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A1M
            java.lang.String r0 = "payflows_success"
            X.C54162ly.A05(r2, r1, r0)
            return
        L_0x0d6d:
            r3 = r2
            X.1DC r3 = (X.AnonymousClass1DC) r3
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x0e1a
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r4 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1421741669(0xffffffffab41f19b, float:-6.890267E-13)
            r1 = 459933768(0x1b6a0848, float:1.9358702E-22)
            com.facebook.graphservice.tree.TreeJNI r2 = r0.A0J(r2, r4, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            if (r2 == 0) goto L_0x0e1a
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r2.A28()
            if (r1 != 0) goto L_0x0e1a
            r0 = 1210412029(0x48256bfd, float:169391.95)
            java.lang.String r0 = r2.A0P(r0)
            com.google.common.base.Preconditions.checkNotNull(r0)
            X.2m3 r1 = r3.A01
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r4 = r3.A00
            com.facebook.payments.contactinfo.model.ContactInfoFormInput r5 = r3.A02
            com.facebook.payments.contactinfo.model.ContactInfo r0 = r4.A01
            java.lang.String r6 = r0.getId()
            boolean r7 = r3.A03
            X.2m6 r3 = r1.A03
            r8 = 0
            r3.A01(r4, r5, r6, r7, r8)
            return
        L_0x0da9:
            r3 = r2
            X.1jU r3 = (X.C31311jU) r3
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x0dc6
            java.lang.Object r5 = r0.A03
            if (r5 == 0) goto L_0x0dc6
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r4 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1411718291(0xffffffffabdae36d, float:-1.5552955E-12)
            r1 = -1866642409(0xffffffff90bd4c17, float:-7.466459E-29)
            com.facebook.graphservice.tree.TreeJNI r1 = r5.A0J(r2, r4, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 == 0) goto L_0x29a0
        L_0x0dc6:
            java.lang.Object r6 = r0.A03
            r4 = r6
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -1411718291(0xffffffffabdae36d, float:-1.5552955E-12)
            r0 = -1866642409(0xffffffff90bd4c17, float:-7.466459E-29)
            com.facebook.graphservice.tree.TreeJNI r0 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = r0.A28()
            if (r5 == 0) goto L_0x29a0
            if (r6 == 0) goto L_0x0239
            if (r0 == 0) goto L_0x0239
            if (r5 == 0) goto L_0x0239
            X.2m5 r1 = r3.A01
            X.2m6 r0 = r1.A03
            X.Bzl r4 = r0.A00
            X.CBK r2 = r1.A02
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r0 = r3.A00
            com.facebook.payments.model.PaymentItemType r1 = r0.A06
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = r0.A05
            X.0pc r0 = r2.A02(r5, r1, r0)
            r4.BvY(r0)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.lang.Throwable r1 = new java.lang.Throwable
            java.lang.String r0 = r5.A3i()
            r1.<init>(r0)
            java.lang.String r0 = "extra_failure"
            r2.putSerializable(r0, r1)
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            r1.<init>(r0, r2)
            X.2m5 r0 = r3.A01
            X.2m6 r0 = r0.A03
            goto L_0x0e9e
        L_0x0e1a:
            if (r0 == 0) goto L_0x0239
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r4 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1421741669(0xffffffffab41f19b, float:-6.890267E-13)
            r1 = 459933768(0x1b6a0848, float:1.9358702E-22)
            com.facebook.graphservice.tree.TreeJNI r0 = r0.A0J(r2, r4, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x0239
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r6 = r0.A28()
            if (r6 == 0) goto L_0x0239
            X.2m3 r0 = r3.A01
            X.BpH r4 = r0.A04
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r0 = r3.A00
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A05
            r0 = 1635686852(0x617e99c4, float:2.9353456E20)
            int r0 = r6.getIntValue(r0)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "error_code"
            r4.A09(r2, r0, r1)
            X.2m3 r0 = r3.A01
            X.BpH r2 = r0.A04
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r0 = r3.A00
            com.facebook.payments.logging.PaymentsLoggingSessionData r1 = r0.A05
            java.lang.String r5 = r6.A3i()
            com.google.common.base.Preconditions.checkNotNull(r5)
            java.lang.String r0 = "error_message"
            r2.A09(r1, r0, r5)
            X.2m3 r0 = r3.A01
            X.BpH r4 = r0.A04
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r0 = r3.A00
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A05
            com.facebook.payments.logging.PaymentsFlowStep r1 = X.C23888Box.A00(r0)
            java.lang.String r0 = "payflows_fail"
            r4.A04(r2, r1, r0)
            X.2m3 r1 = r3.A01
            X.2m6 r0 = r1.A03
            X.Bzl r4 = r0.A00
            X.CBK r2 = r1.A02
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r0 = r3.A00
            com.facebook.payments.model.PaymentItemType r1 = r0.A06
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = r0.A05
            X.0pc r0 = r2.A02(r6, r1, r0)
            r4.BvY(r0)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.lang.Throwable r1 = new java.lang.Throwable
            r1.<init>(r5)
            java.lang.String r0 = "extra_failure"
            r2.putSerializable(r0, r1)
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            r1.<init>(r0, r2)
            X.2m3 r0 = r3.A01
            X.2m6 r0 = r0.A03
        L_0x0e9e:
            X.Bzl r0 = r0.A00
            r0.Bwx(r1)
            return
        L_0x0ea4:
            r5 = r2
            X.1D4 r5 = (X.AnonymousClass1D4) r5
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            X.2m7 r2 = r5.A01
            com.facebook.payments.contactinfo.picker.ContactInfoPickerRunTimeData r1 = r5.A00
            com.facebook.payments.picker.model.PickerScreenConfig r1 = r1.A01
            X.BpH r4 = r2.A01
            com.facebook.payments.picker.model.PickerScreenCommonConfig r1 = r1.Ay1()
            com.facebook.payments.picker.model.PickerScreenAnalyticsParams r1 = r1.analyticsParams
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r1.paymentsLoggingSessionData
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A0o
            java.lang.String r1 = "payflows_success"
            r4.A04(r3, r2, r1)
            com.google.common.collect.ImmutableList$Builder r1 = r5.A03
            r1.addAll(r0)
            X.2m7 r0 = r5.A01
            X.3DS r0 = r0.A02
            boolean r0 = r0.A0A()
            if (r0 != 0) goto L_0x0239
            X.2m7 r0 = r5.A01
            X.Bq9 r0 = r0.A00
            r0.A00()
            X.Bka r2 = r5.A02
            X.2m9 r1 = new X.2m9
            r1.<init>()
            com.google.common.collect.ImmutableList$Builder r0 = r5.A03
            com.google.common.collect.ImmutableList r0 = r0.build()
            r1.A00 = r0
            com.facebook.payments.contactinfo.picker.ContactInfoCoreClientData r0 = new com.facebook.payments.contactinfo.picker.ContactInfoCoreClientData
            r0.<init>(r1)
            r2.BVU(r0)
            return
        L_0x0eee:
            r1 = r2
            X.1D5 r1 = (X.AnonymousClass1D5) r1
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            X.2eS r1 = r1.A00
            X.2eU r1 = r1.A01
            java.util.List r1 = r1.A00
            java.util.Iterator r2 = r1.iterator()
        L_0x0efd:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x0239
            java.lang.Object r1 = r2.next()
            X.CFA r1 = (X.CFA) r1
            r1.BnS(r0)
            goto L_0x0efd
        L_0x0f0d:
            r5 = r2
            X.1D2 r5 = (X.AnonymousClass1D2) r5
            com.facebook.payments.p2p.service.model.cards.AddPaymentCardResult r0 = (com.facebook.payments.p2p.service.model.cards.AddPaymentCardResult) r0
            X.2eV r1 = r5.A00
            X.2mC r4 = r1.A03
            java.lang.String r3 = r0.credentialId
            X.2eW r1 = r5.A01
            java.lang.String r2 = r1.A09
            java.util.Map r1 = r4.A00
            r1.put(r3, r2)
            X.2eV r3 = r5.A00
            com.facebook.payments.paymentmethods.cardform.CardFormParams r1 = r5.A02
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r1 = r1.Agg()
            com.facebook.payments.paymentmethods.cardform.CardFormAnalyticsParams r2 = r1.cardFormAnalyticsParams
            X.2eW r5 = r5.A01
            X.2eX r1 = r3.A04
            r1.A04(r2)
            X.Bzl r1 = r3.A00
            if (r1 == 0) goto L_0x0239
            java.lang.String r1 = r5.A08
            com.google.common.base.Preconditions.checkNotNull(r1)
            com.facebook.payments.p2p.model.PartialPaymentCard r6 = new com.facebook.payments.p2p.model.PartialPaymentCard
            java.lang.String r7 = r0.credentialId
            java.lang.String r4 = r5.A08
            int r2 = r4.length()
            int r1 = r2 + -4
            java.lang.String r8 = r4.substring(r1, r2)
            int r9 = r5.A00
            int r1 = r5.A01
            int r10 = r1 + 2000
            com.facebook.payments.p2p.model.Address r2 = new com.facebook.payments.p2p.model.Address
            java.lang.String r1 = r5.A07
            r2.<init>(r1)
            com.facebook.payments.paymentmethods.model.FbPaymentCardType r1 = r5.A03
            if (r1 != 0) goto L_0x0fad
            r1 = 0
        L_0x0f5d:
            r13 = 1
            r11 = r2
            r12 = r1
            r6.<init>(r7, r8, r9, r10, r11, r12, r13)
            X.2eY r4 = new X.2eY
            r4.<init>()
            java.lang.String r1 = r0.followUpActionType
            r4.A02 = r1
            java.lang.String r1 = r0.followUpActionText
            r4.A01 = r1
            java.lang.String r1 = r0.followUpActionUrl
            r4.A03 = r1
            java.lang.String r1 = r0.followUpActionButtonText
            r4.A00 = r1
            com.facebook.payments.p2p.model.VerificationFollowUpAction r2 = new com.facebook.payments.p2p.model.VerificationFollowUpAction
            r2.<init>(r4)
            android.content.Intent r4 = new android.content.Intent
            r4.<init>()
            java.lang.String r1 = r0.encodedCredentialId
            r0 = 4
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r4.putExtra(r0, r1)
            java.lang.String r0 = "partial_payment_card"
            r4.putExtra(r0, r6)
            java.lang.String r0 = "verification_follow_up_action"
            r4.putExtra(r0, r2)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.lang.String r0 = "extra_activity_result_data"
            r2.putParcelable(r0, r4)
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.<init>(r0, r2)
            X.Bzl r0 = r3.A00
            r0.Bwx(r1)
            return
        L_0x0fad:
            java.lang.String r1 = r1.mHumanReadableName
            goto L_0x0f5d
        L_0x0fb0:
            r0 = r2
            X.1Cy r0 = (X.C20561Cy) r0
            X.2mD r1 = r0.A00
            goto L_0x0fbb
        L_0x0fb6:
            r0 = r2
            X.1Cr r0 = (X.C20501Cr) r0
            X.2mD r1 = r0.A00
        L_0x0fbb:
            boolean r0 = X.C54312mD.A04(r1)
            if (r0 != 0) goto L_0x0239
            X.2o0 r0 = r1.A06
            r0.A2W()
            return
        L_0x0fc7:
            r4 = r2
            X.1Ck r4 = (X.C20431Ck) r4
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x0fde
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 986016248(0x3ac569f8, float:0.0015061488)
            r1 = -1203717159(0xffffffffb840bbd9, float:-4.5951314E-5)
            com.facebook.graphservice.tree.TreeJNI r1 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 == 0) goto L_0x2c1b
        L_0x0fde:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 986016248(0x3ac569f8, float:0.0015061488)
            r1 = -1203717159(0xffffffffb840bbd9, float:-4.5951314E-5)
            com.facebook.graphservice.tree.TreeJNI r1 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r6 = r1.A28()
            if (r6 == 0) goto L_0x2c1b
            if (r1 == 0) goto L_0x0239
            if (r6 == 0) goto L_0x0239
            X.2eX r0 = r4.A02
            X.Bzl r5 = r0.A00
            X.CBK r3 = r0.A03
            com.facebook.payments.paymentmethods.cardform.CardFormParams r2 = r4.A01
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r0 = r2.Agg()
            com.facebook.payments.model.PaymentItemType r1 = r0.paymentItemType
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r0 = r2.Agg()
            com.facebook.payments.paymentmethods.cardform.CardFormAnalyticsParams r0 = r0.cardFormAnalyticsParams
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = r0.paymentsLoggingSessionData
            X.0pc r0 = r3.A02(r6, r1, r0)
            r5.BvY(r0)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.lang.Throwable r1 = new java.lang.Throwable
            java.lang.String r0 = r6.A3i()
            r1.<init>(r0)
            java.lang.String r0 = "extra_failure"
            r2.putSerializable(r0, r1)
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            r1.<init>(r0, r2)
            X.2eX r0 = r4.A02
            goto L_0x1098
        L_0x1030:
            r4 = r2
            X.1Ch r4 = (X.C20401Ch) r4
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x1047
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -2061685216(0xffffffff851d2e20, float:-7.390576E-36)
            r1 = 1324889998(0x4ef8378e, float:2.0821952E9)
            com.facebook.graphservice.tree.TreeJNI r1 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 == 0) goto L_0x2c51
        L_0x1047:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -2061685216(0xffffffff851d2e20, float:-7.390576E-36)
            r1 = 1324889998(0x4ef8378e, float:2.0821952E9)
            com.facebook.graphservice.tree.TreeJNI r1 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r6 = r1.A28()
            if (r6 == 0) goto L_0x2c51
            if (r1 == 0) goto L_0x0239
            if (r6 == 0) goto L_0x0239
            X.2eX r0 = r4.A02
            X.Bzl r5 = r0.A00
            X.CBK r3 = r0.A03
            com.facebook.payments.paymentmethods.cardform.CardFormParams r2 = r4.A01
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r0 = r2.Agg()
            com.facebook.payments.model.PaymentItemType r1 = r0.paymentItemType
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r0 = r2.Agg()
            com.facebook.payments.paymentmethods.cardform.CardFormAnalyticsParams r0 = r0.cardFormAnalyticsParams
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = r0.paymentsLoggingSessionData
            X.0pc r0 = r3.A02(r6, r1, r0)
            r5.BvY(r0)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.lang.Throwable r1 = new java.lang.Throwable
            java.lang.String r0 = r6.A3i()
            r1.<init>(r0)
            java.lang.String r0 = "extra_failure"
            r2.putSerializable(r0, r1)
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            r1.<init>(r0, r2)
            X.2eX r0 = r4.A02
        L_0x1098:
            X.Bzl r0 = r0.A00
            r0.Bwx(r1)
            return
        L_0x109e:
            X.1Ce r2 = (X.C20371Ce) r2
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x10b0
            java.lang.Object r1 = r0.A03
            if (r1 == 0) goto L_0x10b0
            X.2eZ r0 = r2.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            X.C50762eZ.A04(r0, r1)
            return
        L_0x10b0:
            X.2eZ r2 = r2.A00
            r1 = 0
            X.Bx0 r0 = r2.A01
            if (r0 == 0) goto L_0x0239
            r0.BCF()
            X.Bx0 r0 = r2.A01
            r0.ATX(r1)
            return
        L_0x10c0:
            r7 = r2
            X.1Cd r7 = (X.C20361Cd) r7
            java.util.List r0 = (java.util.List) r0
            if (r0 == 0) goto L_0x0239
            int r2 = r0.size()
            r1 = 2
            if (r2 != r1) goto L_0x0239
            r6 = 0
            java.lang.Object r1 = r0.get(r6)
            if (r1 == 0) goto L_0x0239
            r5 = 1
            java.lang.Object r1 = r0.get(r5)
            if (r1 == 0) goto L_0x0239
            java.lang.Object r1 = r0.get(r6)
            boolean r1 = r1 instanceof com.google.common.collect.ImmutableList
            if (r1 == 0) goto L_0x0239
            java.lang.Object r1 = r0.get(r5)
            boolean r1 = r1 instanceof com.facebook.payments.shipping.model.AddressFormConfig
            if (r1 == 0) goto L_0x0239
            X.2ea r2 = r7.A02
            com.facebook.payments.shipping.addresspicker.ShippingAddressPickerRunTimeData r1 = r7.A01
            com.facebook.payments.picker.model.PickerScreenConfig r1 = r1.A01
            X.BpH r4 = r2.A02
            com.facebook.payments.picker.model.PickerScreenCommonConfig r1 = r1.Ay1()
            com.facebook.payments.picker.model.PickerScreenAnalyticsParams r1 = r1.analyticsParams
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r1.paymentsLoggingSessionData
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A1q
            java.lang.String r1 = "payflows_success"
            r4.A04(r3, r2, r1)
            X.2ea r1 = r7.A02
            X.Bq9 r1 = r1.A00
            r1.A00()
            X.Bka r3 = r7.A00
            com.facebook.payments.shipping.addresspicker.ShippingCoreClientData r2 = new com.facebook.payments.shipping.addresspicker.ShippingCoreClientData
            java.lang.Object r1 = r0.get(r6)
            com.google.common.collect.ImmutableList r1 = (com.google.common.collect.ImmutableList) r1
            java.lang.Object r0 = r0.get(r5)
            com.facebook.payments.shipping.model.AddressFormConfig r0 = (com.facebook.payments.shipping.model.AddressFormConfig) r0
            r2.<init>(r1, r0)
            r3.BVU(r2)
            return
        L_0x1121:
            r1 = r2
            X.1Cb r1 = (X.AnonymousClass1Cb) r1
            X.2mH r0 = (X.C54352mH) r0
            X.2mE r7 = r1.A00
            X.2mF r3 = r7.A01
            com.facebook.payments.ui.PaymentsSecureSpinnerWithMessageView r2 = r3.A0D
            r1 = 8
            r2.setVisibility(r1)
            androidx.core.widget.NestedScrollView r2 = r3.A02
            r1 = 0
            r2.setVisibility(r1)
            X.2mF r5 = r7.A01
            int r3 = X.AnonymousClass1Y3.AP2
            X.0UN r2 = r5.A03
            r1 = 2
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.BpH r4 = (X.C23904BpH) r4
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r5.A04
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A10
            java.lang.String r1 = "payflows_success"
            r4.A04(r3, r2, r1)
            X.2mF r6 = r7.A01
            android.os.Bundle r5 = r7.A00
            int r3 = X.AnonymousClass1Y3.AP2
            X.0UN r2 = r6.A03
            r1 = 2
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.BpH r4 = (X.C23904BpH) r4
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r6.A04
            com.facebook.payments.model.PaymentItemType r2 = com.facebook.payments.model.PaymentItemType.A01
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A0z
            r4.A06(r3, r2, r1, r5)
            X.2mF r6 = r7.A01
            int r3 = X.AnonymousClass1Y3.AYe
            X.0UN r2 = r6.A03
            r1 = 4
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Bz r5 = (X.C20331Bz) r5
            android.content.Context r4 = r6.A1j()
            com.facebook.interstitial.triggers.InterstitialTrigger r3 = new com.facebook.interstitial.triggers.InterstitialTrigger
            com.facebook.interstitial.triggers.InterstitialTrigger$Action r1 = com.facebook.interstitial.triggers.InterstitialTrigger.Action.A5k
            r3.<init>(r1)
            java.lang.Class<X.C2J> r2 = X.C2J.class
            X.2mG r1 = new X.2mG
            r1.<init>(r6, r0)
            boolean r1 = r5.A03(r4, r3, r2, r1)
            r6.A0F = r1
            X.2mF r3 = r7.A01
            com.facebook.resources.ui.FbTextView r2 = r3.A0E
            r1 = 2131834165(0x7f113535, float:1.9301433E38)
            r2.setText(r1)
            com.google.common.collect.ImmutableList r5 = r0.A04
            r2 = 0
            if (r5 == 0) goto L_0x11b7
            boolean r1 = r5.isEmpty()
            if (r1 != 0) goto L_0x11b7
            boolean r1 = r3.A0G
            if (r1 == 0) goto L_0x13a9
            r1 = 2131300436(0x7f091054, float:1.8218902E38)
            android.view.View r1 = r3.A2K(r1)
            r1.setVisibility(r2)
        L_0x11ad:
            r1 = 2131297663(0x7f09057f, float:1.8213277E38)
            android.view.View r1 = r3.A2K(r1)
            r1.setVisibility(r2)
        L_0x11b7:
            r1 = 2131300437(0x7f091055, float:1.8218904E38)
            android.view.View r4 = r3.A2K(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r4 = (com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView) r4
            r3.A0A = r4
            r1 = 2131834258(0x7f113592, float:1.9301621E38)
            java.lang.String r1 = r3.A1A(r1)
            r4.A0D(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r4 = r3.A0A
            X.Bo1 r1 = r3.A0I
            r4.A0C(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = r3.A0A
            r1.setVisibility(r2)
            r1 = 2131297664(0x7f090580, float:1.821328E38)
            android.view.View r1 = r3.A2K(r1)
            r1.setVisibility(r2)
            com.google.common.collect.ImmutableList r5 = r0.A03
            if (r5 == 0) goto L_0x13a0
            boolean r1 = r5.isEmpty()
            if (r1 != 0) goto L_0x13a0
            boolean r1 = r0.A06
            if (r1 == 0) goto L_0x127d
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r7 = r3.A0A
            r1 = 2131834261(0x7f113595, float:1.9301627E38)
            java.lang.String r6 = r3.A1A(r1)
            com.facebook.resources.ui.FbTextView r4 = r7.A03
            r4.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r1 = r7.A03
            r1.setText(r6)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r10 = r3.A0A
            android.content.Context r14 = r3.A1j()
            android.content.Context r6 = r3.A1j()
            com.facebook.payments.logging.PaymentsLoggingSessionData r7 = r3.A04
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A1N
            X.2mI r4 = new X.2mI
            r4.<init>(r1, r7)
            java.lang.String r1 = "payment_history"
            r4.A00 = r1
            com.facebook.payments.picker.model.PickerScreenAnalyticsParams r8 = new com.facebook.payments.picker.model.PickerScreenAnalyticsParams
            r8.<init>(r4)
            X.2mJ r7 = new X.2mJ
            r7.<init>()
            X.2mK r4 = new X.2mK
            r4.<init>()
            X.2mL r9 = new X.2mL
            r9.<init>()
            com.facebook.payments.decorator.PaymentsDecoratorAnimation r1 = com.facebook.payments.decorator.PaymentsDecoratorAnimation.A03
            r9.A00 = r1
            com.facebook.payments.ui.titlebar.model.PaymentsTitleBarStyle r1 = com.facebook.payments.ui.titlebar.model.PaymentsTitleBarStyle.PAYMENTS_WHITE
            r9.A01 = r1
            com.facebook.payments.ui.titlebar.model.PaymentsTitleBarTitleStyle r1 = com.facebook.payments.ui.titlebar.model.PaymentsTitleBarTitleStyle.CENTER_ALIGNED
            r9.A02 = r1
            r1 = 1
            r9.A06 = r1
            com.facebook.payments.decorator.PaymentsDecoratorParams r1 = new com.facebook.payments.decorator.PaymentsDecoratorParams
            r1.<init>(r9)
            r4.A00 = r1
            com.facebook.payments.picker.model.PickerScreenStyleParams r1 = new com.facebook.payments.picker.model.PickerScreenStyleParams
            r1.<init>(r4)
            r7.A04 = r1
            r7.A01 = r8
            com.facebook.payments.picker.model.PickerScreenStyle r1 = com.facebook.payments.picker.model.PickerScreenStyle.A08
            r7.A03 = r1
            com.facebook.payments.model.PaymentItemType r1 = com.facebook.payments.model.PaymentItemType.A01
            r7.A00 = r1
            r1 = 2131834171(0x7f11353b, float:1.9301445E38)
            java.lang.String r1 = r6.getString(r1)
            r7.A06 = r1
            r1 = 1
            r7.A07 = r1
            com.facebook.payments.picker.model.PickerScreenCommonConfig r4 = new com.facebook.payments.picker.model.PickerScreenCommonConfig
            r4.<init>(r7)
            com.facebook.payments.history.picker.PaymentHistoryPickerScreenConfig r1 = new com.facebook.payments.history.picker.PaymentHistoryPickerScreenConfig
            r1.<init>(r4)
            android.content.Intent r13 = com.facebook.payments.picker.PickerScreenActivity.A00(r6, r1)
            com.facebook.payments.logging.PaymentsLoggingSessionData r11 = r3.A04
            com.facebook.payments.logging.PaymentsFlowStep r12 = com.facebook.payments.logging.PaymentsFlowStep.A19
            com.facebook.resources.ui.FbTextView r1 = r10.A03
            X.2mM r9 = new X.2mM
            r9.<init>(r10, r11, r12, r13, r14)
            r1.setOnClickListener(r9)
        L_0x127d:
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = r3.A0A
            X.1vg r1 = r1.A01
            r1.A03 = r5
            r1.A04()
        L_0x1286:
            com.google.common.collect.ImmutableList r5 = r0.A02
            if (r5 == 0) goto L_0x12ca
            boolean r1 = r5.isEmpty()
            if (r1 != 0) goto L_0x12ca
            r1 = 2131300438(0x7f091056, float:1.8218906E38)
            android.view.View r1 = r3.A2K(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = (com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView) r1
            r3.A0B = r1
            r1 = 2131297665(0x7f090581, float:1.8213281E38)
            android.view.View r1 = r3.A2K(r1)
            r3.A00 = r1
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r4 = r3.A0B
            X.Bo1 r1 = r3.A0I
            r4.A0C(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r4 = r3.A0B
            r1 = 2131834257(0x7f113591, float:1.930162E38)
            java.lang.String r1 = r3.A1A(r1)
            r4.A0D(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = r3.A0B
            X.1vg r1 = r1.A01
            r1.A03 = r5
            r1.A04()
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = r3.A0B
            r1.setVisibility(r2)
            android.view.View r1 = r3.A00
            r1.setVisibility(r2)
        L_0x12ca:
            com.google.common.collect.ImmutableList r4 = r0.A05
            if (r4 == 0) goto L_0x0239
            boolean r1 = r4.isEmpty()
            if (r1 != 0) goto L_0x0239
            r1 = 2131300439(0x7f091057, float:1.8218908E38)
            android.view.View r1 = r3.A2K(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = (com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView) r1
            r3.A0C = r1
            r1 = 2131297666(0x7f090582, float:1.8213283E38)
            android.view.View r1 = r3.A2K(r1)
            r3.A01 = r1
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r5 = r3.A0C
            r1 = 2131834304(0x7f1135c0, float:1.9301715E38)
            java.lang.String r1 = r3.A1A(r1)
            r5.A0D(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r5 = r3.A0C
            X.Bo1 r1 = r3.A0I
            r5.A0C(r1)
            boolean r0 = r0.A07
            if (r0 == 0) goto L_0x138c
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r6 = r3.A0C
            r0 = 2131834261(0x7f113595, float:1.9301627E38)
            java.lang.String r5 = r3.A1A(r0)
            com.facebook.resources.ui.FbTextView r1 = r6.A03
            r1.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r6.A03
            r0.setText(r5)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r9 = r3.A0C
            android.content.Context r13 = r3.A1j()
            android.content.Context r5 = r3.A1j()
            com.facebook.payments.logging.PaymentsLoggingSessionData r6 = r3.A04
            com.facebook.payments.logging.PaymentsFlowStep r0 = com.facebook.payments.logging.PaymentsFlowStep.A1v
            X.2mI r1 = new X.2mI
            r1.<init>(r0, r6)
            java.lang.String r0 = "fbpay_subscriptions_history"
            r1.A00 = r0
            com.facebook.payments.picker.model.PickerScreenAnalyticsParams r8 = new com.facebook.payments.picker.model.PickerScreenAnalyticsParams
            r8.<init>(r1)
            X.2mJ r6 = new X.2mJ
            r6.<init>()
            X.2mK r7 = new X.2mK
            r7.<init>()
            X.2mL r1 = new X.2mL
            r1.<init>()
            com.facebook.payments.decorator.PaymentsDecoratorAnimation r0 = com.facebook.payments.decorator.PaymentsDecoratorAnimation.A03
            r1.A00 = r0
            com.facebook.payments.ui.titlebar.model.PaymentsTitleBarStyle r0 = com.facebook.payments.ui.titlebar.model.PaymentsTitleBarStyle.PAYMENTS_WHITE
            r1.A01 = r0
            com.facebook.payments.ui.titlebar.model.PaymentsTitleBarTitleStyle r0 = com.facebook.payments.ui.titlebar.model.PaymentsTitleBarTitleStyle.CENTER_ALIGNED
            r1.A02 = r0
            r0 = 1
            r1.A06 = r0
            com.facebook.payments.decorator.PaymentsDecoratorParams r0 = new com.facebook.payments.decorator.PaymentsDecoratorParams
            r0.<init>(r1)
            r7.A00 = r0
            com.facebook.payments.picker.model.PickerScreenStyleParams r0 = new com.facebook.payments.picker.model.PickerScreenStyleParams
            r0.<init>(r7)
            r6.A04 = r0
            r6.A01 = r8
            com.facebook.payments.picker.model.PickerScreenStyle r0 = com.facebook.payments.picker.model.PickerScreenStyle.FBPAY_SUBSCRIPTIONS_HISTORY
            r6.A03 = r0
            com.facebook.payments.model.PaymentItemType r0 = com.facebook.payments.model.PaymentItemType.A01
            r6.A00 = r0
            r0 = 2131824828(0x7f1110bc, float:1.9282495E38)
            java.lang.String r0 = r5.getString(r0)
            r6.A06 = r0
            r0 = 1
            r6.A07 = r0
            com.facebook.payments.picker.model.PickerScreenCommonConfig r1 = new com.facebook.payments.picker.model.PickerScreenCommonConfig
            r1.<init>(r6)
            com.facebook.payments.transactionhub.subscriptionshistory.picker.FbPaySubscriptionsHistoryPickerScreenConfig r0 = new com.facebook.payments.transactionhub.subscriptionshistory.picker.FbPaySubscriptionsHistoryPickerScreenConfig
            r0.<init>(r1)
            android.content.Intent r12 = com.facebook.payments.picker.PickerScreenActivity.A00(r5, r0)
            com.facebook.payments.logging.PaymentsLoggingSessionData r10 = r3.A04
            com.facebook.payments.logging.PaymentsFlowStep r11 = com.facebook.payments.logging.PaymentsFlowStep.A14
            com.facebook.resources.ui.FbTextView r0 = r9.A03
            X.2mM r8 = new X.2mM
            r8.<init>(r9, r10, r11, r12, r13)
            r0.setOnClickListener(r8)
        L_0x138c:
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r0 = r3.A0C
            X.1vg r0 = r0.A01
            r0.A03 = r4
            r0.A04()
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r0 = r3.A0C
            r0.setVisibility(r2)
            android.view.View r0 = r3.A01
            r0.setVisibility(r2)
            return
        L_0x13a0:
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = r3.A0A
            com.facebook.payments.transactionhub.views.TxnHistoryPlaceHolderView r4 = r1.A02
            r4.setVisibility(r2)
            goto L_0x1286
        L_0x13a9:
            r1 = 2131300436(0x7f091054, float:1.8218902E38)
            android.view.View r4 = r3.A2K(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r4 = (com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView) r4
            r3.A09 = r4
            r1 = 2131830143(0x7f11257f, float:1.9293275E38)
            java.lang.String r1 = r3.A1A(r1)
            r4.A0D(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r4 = r3.A09
            X.Bo1 r1 = r3.A0I
            r4.A0C(r1)
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = r3.A09
            X.1vg r1 = r1.A01
            r1.A03 = r5
            r1.A04()
            com.facebook.payments.paymentmethods.model.NewCreditCardOption r1 = r0.A01
            r3.A05 = r1
            com.facebook.payments.transactionhub.views.hubsections.HubLandingSectionView r1 = r3.A09
            r1.setVisibility(r2)
            goto L_0x11ad
        L_0x13d9:
            r5 = r2
            X.1Ci r5 = (X.C20411Ci) r5
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x0239
            java.lang.Object r3 = r0.A03     // Catch:{ JSONException -> 0x2fce }
            if (r3 == 0) goto L_0x0239
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3     // Catch:{ JSONException -> 0x2fce }
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 264589905(0xfc55251, float:1.9457407E-29)
            r0 = -1715510431(0xffffffff99bf6361, float:-1.9789076E-23)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)     // Catch:{ JSONException -> 0x2fce }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3     // Catch:{ JSONException -> 0x2fce }
            if (r3 == 0) goto L_0x0239
            r1 = -702003549(0xffffffffd62846a3, float:-4.6255334E13)
            r0 = -442431467(0xffffffffe5a10815, float:-9.505626E22)
            com.google.common.collect.ImmutableList r1 = r3.A0M(r1, r2, r0)     // Catch:{ JSONException -> 0x2fce }
            if (r1 == 0) goto L_0x0239
            r0 = 0
            java.lang.Object r3 = r1.get(r0)     // Catch:{ JSONException -> 0x2fce }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3     // Catch:{ JSONException -> 0x2fce }
            r1 = -23317757(0xfffffffffe9c3303, float:-1.0381222E38)
            r0 = 824659389(0x31274dbd, float:2.4345888E-9)
            com.google.common.collect.ImmutableList r0 = r3.A0M(r1, r2, r0)     // Catch:{ JSONException -> 0x2fce }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x2fce }
            r4.<init>()     // Catch:{ JSONException -> 0x2fce }
            X.1Xv r3 = r0.iterator()     // Catch:{ JSONException -> 0x2fce }
        L_0x141c:
            boolean r0 = r3.hasNext()     // Catch:{ JSONException -> 0x2fce }
            if (r0 == 0) goto L_0x2fbc
            java.lang.Object r2 = r3.next()     // Catch:{ JSONException -> 0x2fce }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2     // Catch:{ JSONException -> 0x2fce }
            r0 = 1265525146(0x4b6e619a, float:1.5622554E7)
            java.lang.String r1 = r2.A0P(r0)     // Catch:{ JSONException -> 0x2fce }
            r0 = 703951340(0x29f571ec, float:1.0899948E-13)
            java.lang.String r0 = r2.A0P(r0)     // Catch:{ JSONException -> 0x2fce }
            r4.put(r1, r0)     // Catch:{ JSONException -> 0x2fce }
            goto L_0x141c
        L_0x143a:
            r1 = r2
            X.1BR r1 = (X.AnonymousClass1BR) r1
            com.facebook.fbservice.service.OperationResult r0 = (com.facebook.fbservice.service.OperationResult) r0
            java.lang.Object r4 = r0.A07()
            com.facebook.stickers.service.FetchStickersResult r4 = (com.facebook.stickers.service.FetchStickersResult) r4
            X.2ec r0 = r1.A01
            X.1Ap r3 = r0.A00
            if (r3 == 0) goto L_0x0239
            X.D9t r2 = r1.A00
            X.2ee r1 = new X.2ee
            com.google.common.collect.ImmutableList r0 = r4.A00
            r1.<init>(r0)
            r3.BdJ(r2, r1)
            return
        L_0x1458:
            r3 = r2
            X.1Ca r3 = (X.C20341Ca) r3
            X.2MP r0 = (X.AnonymousClass2MP) r0
            X.2ed r2 = r3.A01
            r1 = 0
            r2.A00 = r1
            X.D69 r1 = r3.A00
            if (r1 == 0) goto L_0x0239
            r1.Bqd(r0)
            return
        L_0x146a:
            r3 = r2
            X.1GO r3 = (X.AnonymousClass1GO) r3
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x1508
            java.lang.Object r4 = r0.A03
            if (r4 == 0) goto L_0x1508
            X.2MT r5 = r3.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -816631278(0xffffffffcf533212, float:-3.54327398E9)
            r0 = -2076922023(0xffffffff8434af59, float:-2.1239414E-36)
            com.facebook.graphservice.tree.TreeJNI r4 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            r1 = -1384270571(0xffffffffad7db515, float:-1.4421593E-11)
            r0 = 1951647341(0x7453c66d, float:6.711421E31)
            com.facebook.graphservice.tree.TreeJNI r4 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            if (r4 == 0) goto L_0x14f8
            r1 = 660173473(0x275972a1, float:3.017694E-15)
            r0 = -1524043475(0xffffffffa528f12d, float:-1.4653391E-16)
            com.google.common.collect.ImmutableList r10 = r4.A0M(r1, r2, r0)
            if (r10 == 0) goto L_0x14f8
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            int r8 = r10.size()
            r7 = 0
        L_0x14ab:
            if (r7 >= r8) goto L_0x14d1
            java.lang.Object r1 = r10.get(r7)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            com.facebook.zero.carriersignal.CarrierSignalPingURL r6 = new com.facebook.zero.carriersignal.CarrierSignalPingURL
            r0 = 106079(0x19e5f, float:1.48648E-40)
            java.lang.String r4 = r1.A0P(r0)
            java.lang.String r2 = r1.A42()
            r0 = -546109589(0xffffffffdf73076b, float:-1.7512083E19)
            int r0 = r1.getIntValue(r0)
            long r0 = (long) r0
            r6.<init>(r4, r2, r0)
            r9.add(r6)
            int r7 = r7 + 1
            goto L_0x14ab
        L_0x14d1:
            X.0jE r0 = X.AnonymousClass0jE.getInstance()     // Catch:{ 1vt -> 0x14da }
            java.lang.String r0 = r0.writeValueAsString(r9)     // Catch:{ 1vt -> 0x14da }
            goto L_0x14e7
        L_0x14da:
            r2 = move-exception
            r0 = 450(0x1c2, float:6.3E-43)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            java.lang.String r0 = "Failed to serialize carrier signal config"
            X.C010708t.A0L(r1, r0, r2)
            r0 = 0
        L_0x14e7:
            r2 = r0
            if (r0 == 0) goto L_0x14f8
            com.facebook.prefs.shared.FbSharedPreferences r0 = r5.A01
            X.1hn r1 = r0.edit()
            X.1Y8 r0 = X.C05730aE.A0G
            r1.BzC(r0, r2)
            r1.commit()
        L_0x14f8:
            X.2MT r0 = r3.A00
            com.facebook.zero.carriersignal.CarrierSignalController r1 = r0.A02
            boolean r0 = r1.A05()
            if (r0 == 0) goto L_0x0239
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            r1.A03(r0)
            return
        L_0x1508:
            X.2MT r0 = r3.A00
            com.facebook.zero.carriersignal.CarrierSignalController r0 = r0.A02
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r0.A00
            r0 = 9
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1ZE r1 = (X.AnonymousClass1ZE) r1
            java.lang.String r0 = "fb4a_carrier_signal_v2_run"
            X.0bW r1 = r1.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 152(0x98, float:2.13E-43)
            r2.<init>(r1, r0)
            boolean r0 = r2.A0G()
            if (r0 == 0) goto L_0x0239
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            java.lang.String r0 = X.C137936cG.A00(r0)
            java.lang.String r1 = r0.toLowerCase()
            java.lang.String r0 = "state"
            r2.A0D(r0, r1)
            r2.A06()
            return
        L_0x153e:
            r1 = r2
            X.17Z r1 = (X.AnonymousClass17Z) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x0239
            java.lang.Object r3 = r0.A03
            if (r3 == 0) goto L_0x0239
            X.17a r7 = r1.A01
            java.lang.String r6 = r1.A02
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            android.content.Context r5 = r1.A00
            if (r3 == 0) goto L_0x0239
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -816631278(0xffffffffcf533212, float:-3.54327398E9)
            r0 = -253539593(0xfffffffff0e34af7, float:-5.627498E29)
            com.facebook.graphservice.tree.TreeJNI r9 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r9
            if (r9 == 0) goto L_0x0239
            X.0aD r1 = r7.A06
            java.lang.String r0 = "native_template_enable_cache"
            boolean r10 = r1.A07(r0)
            X.0p4 r2 = new X.0p4
            r2.<init>(r5)
            r1 = 1
            r0 = 73
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            java.lang.String[] r8 = new java.lang.String[]{r0}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r1)
            X.2eg r3 = new X.2eg
            android.content.Context r0 = r2.A09
            r3.<init>(r0)
            X.0zR r0 = r2.A04
            if (r0 == 0) goto L_0x158f
            java.lang.String r1 = r0.A06
            r3.A07 = r1
        L_0x158f:
            r4.clear()
            java.lang.Class<X.4DL> r2 = X.AnonymousClass4DL.class
            r1 = 1129313434(0x434ff49a, float:207.95547)
            r0 = 369377121(0x16043f61, float:1.0682874E-25)
            com.facebook.graphservice.tree.TreeJNI r0 = r9.A0J(r1, r2, r0)
            X.4DL r0 = (X.AnonymousClass4DL) r0
            r3.A04 = r0
            r0 = 0
            r4.set(r0)
            X.11G r0 = r3.A14()
            r0.A0S(r6)
            goto L_0x1616
        L_0x15ae:
            r1 = r2
            X.1ER r1 = (X.AnonymousClass1ER) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x0239
            java.lang.Object r3 = r0.A03
            if (r3 == 0) goto L_0x0239
            X.17a r7 = r1.A01
            java.lang.String r6 = r1.A02
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            android.content.Context r5 = r1.A00
            if (r3 == 0) goto L_0x0239
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -816631278(0xffffffffcf533212, float:-3.54327398E9)
            r0 = 1311528665(0x4e2c56d9, float:7.228432E8)
            com.facebook.graphservice.tree.TreeJNI r9 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r9
            if (r9 == 0) goto L_0x0239
            X.0aD r1 = r7.A06
            java.lang.String r0 = "native_template_enable_cache"
            boolean r10 = r1.A07(r0)
            X.0p4 r2 = new X.0p4
            r2.<init>(r5)
            r1 = 1
            r0 = 73
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            java.lang.String[] r8 = new java.lang.String[]{r0}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r1)
            X.2eg r3 = new X.2eg
            android.content.Context r0 = r2.A09
            r3.<init>(r0)
            X.0zR r0 = r2.A04
            if (r0 == 0) goto L_0x15ff
            java.lang.String r0 = r0.A06
            r3.A07 = r0
        L_0x15ff:
            r4.clear()
            java.lang.Class<X.4DL> r2 = X.AnonymousClass4DL.class
            r1 = -422593238(0xffffffffe6cfbd2a, float:-4.9050966E23)
            r0 = 369377121(0x16043f61, float:1.0682874E-25)
            com.facebook.graphservice.tree.TreeJNI r0 = r9.A0J(r1, r2, r0)
            X.4DL r0 = (X.AnonymousClass4DL) r0
            r3.A04 = r0
            r0 = 0
            r4.set(r0)
        L_0x1616:
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r4, r8)
            if (r10 == 0) goto L_0x1621
            java.util.Map r0 = r7.A07
            r0.put(r6, r3)
        L_0x1621:
            X.C191617a.A02(r7, r3, r5)
            return
        L_0x1625:
            r1 = 2
            if (r3 < r1) goto L_0x1638
            r5.A0C()
            X.1Ap r2 = r5.A01
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r0 = "Too many attempts"
            r1.<init>(r0)
            r2.Bd1(r4, r1)
            return
        L_0x1638:
            int r1 = r3 + 1
            X.AnonymousClass1EP.A02(r5, r4, r0, r1)
            return
        L_0x163e:
            r8 = r2
            X.1EN r8 = (X.AnonymousClass1EN) r8
            boolean r0 = r8.A01
            if (r0 == 0) goto L_0x1686
            X.2ev r0 = r8.A00
            X.2ew r7 = r0.A02
            java.lang.String r2 = "fgl_write_success"
            r1 = 1
            X.C50882ew.A05(r7, r1)     // Catch:{ IllegalStateException -> 0x1686 }
            X.C50882ew.A03(r7, r1)     // Catch:{ IllegalStateException -> 0x1686 }
            r0 = 0
            X.C50882ew.A04(r7, r0)     // Catch:{ IllegalStateException -> 0x1686 }
            X.C50882ew.A06(r7, r1)     // Catch:{ IllegalStateException -> 0x1686 }
            int r0 = r7.A06     // Catch:{ IllegalStateException -> 0x1686 }
            int r0 = r0 + r1
            r7.A06 = r0     // Catch:{ IllegalStateException -> 0x1686 }
            X.1La r6 = X.C50882ew.A00(r7, r2)     // Catch:{ IllegalStateException -> 0x1686 }
            if (r6 == 0) goto L_0x167e
            X.069 r0 = r7.A0C     // Catch:{ IllegalStateException -> 0x1686 }
            long r4 = r0.now()     // Catch:{ IllegalStateException -> 0x1686 }
            long r0 = r7.A0A     // Catch:{ IllegalStateException -> 0x1686 }
            long r2 = r4 - r0
            long r0 = r7.A07     // Catch:{ IllegalStateException -> 0x1686 }
            long r4 = r4 - r0
            java.lang.String r0 = "write_duration_ms"
            r6.A03(r0, r2)     // Catch:{ IllegalStateException -> 0x1686 }
            java.lang.String r0 = "request_duration_ms"
            r6.A03(r0, r4)     // Catch:{ IllegalStateException -> 0x1686 }
            r6.A0A()     // Catch:{ IllegalStateException -> 0x1686 }
        L_0x167e:
            r0 = -9223372036854775808
            r7.A0A = r0     // Catch:{ IllegalStateException -> 0x1686 }
            r7.A08 = r0     // Catch:{ IllegalStateException -> 0x1686 }
            r7.A07 = r0     // Catch:{ IllegalStateException -> 0x1686 }
        L_0x1686:
            X.2ev r1 = r8.A00
            r0 = 0
            r1.A00 = r0
            return
        L_0x168c:
            r3 = r2
            X.1EL r3 = (X.AnonymousClass1EL) r3
            java.util.List r0 = (java.util.List) r0
            r1 = 0
            java.lang.Object r14 = r0.get(r1)
            boolean r1 = r14 instanceof X.C188898pw
            r2 = 0
            if (r1 == 0) goto L_0x16fb
            X.8pw r14 = (X.C188898pw) r14
            r10 = r2
        L_0x169e:
            r12 = 1
            java.lang.Object r13 = r0.get(r12)
            boolean r1 = r13 instanceof java.util.List
            if (r1 == 0) goto L_0x16f6
            java.util.List r13 = (java.util.List) r13
            r9 = r2
        L_0x16aa:
            r1 = 2
            java.lang.Object r11 = r0.get(r1)
            boolean r1 = r11 instanceof java.util.List
            if (r1 == 0) goto L_0x16f1
            java.util.List r11 = (java.util.List) r11
            r15 = r2
        L_0x16b6:
            r1 = 3
            java.lang.Object r1 = r0.get(r1)
            boolean r0 = r1 instanceof X.C30441EwW
            if (r0 == 0) goto L_0x16ef
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>(r12)
            X.EwW r1 = (X.C30441EwW) r1
            r5.add(r1)
        L_0x16c9:
            int r1 = X.AnonymousClass1Y3.BGZ
            X.2mN r0 = r3.A00
            X.0UN r0 = r0.A02
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r12, r1, r0)
            X.4VN r6 = (X.AnonymousClass4VN) r6
            r0 = 844918852354130(0x30073000f0052, double:4.17445378471788E-309)
            java.lang.Integer r4 = X.AnonymousClass07B.A01
            java.lang.Integer r4 = X.AnonymousClass4VN.A01(r6, r0, r4)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            if (r4 == r0) goto L_0x16e9
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 0
            if (r4 != r1) goto L_0x16ea
        L_0x16e9:
            r0 = 1
        L_0x16ea:
            r8 = 14
            if (r0 == 0) goto L_0x1711
            goto L_0x1700
        L_0x16ef:
            r5 = r2
            goto L_0x16c9
        L_0x16f1:
            java.lang.Throwable r11 = (java.lang.Throwable) r11
            r15 = r11
            r11 = r2
            goto L_0x16b6
        L_0x16f6:
            java.lang.Throwable r13 = (java.lang.Throwable) r13
            r9 = r13
            r13 = r2
            goto L_0x16aa
        L_0x16fb:
            java.lang.Throwable r14 = (java.lang.Throwable) r14
            r10 = r14
            r14 = r2
            goto L_0x169e
        L_0x1700:
            int r1 = X.AnonymousClass1Y3.AEd     // Catch:{ Exception -> 0x1711 }
            X.2mN r0 = r3.A00     // Catch:{ Exception -> 0x1711 }
            X.0UN r0 = r0.A02     // Catch:{ Exception -> 0x1711 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ Exception -> 0x1711 }
            X.A5C r0 = (X.A5C) r0     // Catch:{ Exception -> 0x1711 }
            java.util.List r7 = r0.A04()     // Catch:{ Exception -> 0x1711 }
            goto L_0x1712
        L_0x1711:
            r7 = r2
        L_0x1712:
            r4 = 8
            int r1 = X.AnonymousClass1Y3.AHw
            X.2mN r0 = r3.A00
            X.0UN r0 = r0.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.2ex r1 = (X.C50892ex) r1
            monitor-enter(r1)
            java.util.List r0 = r1.A0C     // Catch:{ all -> 0x1852 }
            com.google.common.collect.ImmutableList r6 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x1852 }
            java.util.List r0 = r1.A0C     // Catch:{ all -> 0x1852 }
            r0.clear()     // Catch:{ all -> 0x1852 }
            monitor-exit(r1)
            int r1 = r6.size()
            java.lang.String r0 = "ForegroundLocationFrameworkSignalReader"
            if (r1 <= 0) goto L_0x1738
            r6.size()
        L_0x1738:
            X.2ey r4 = new X.2ey
            r4.<init>()
            r4.A01 = r14
            r4.A08 = r0
            r14 = 10
            int r1 = X.AnonymousClass1Y3.B5j
            X.2mN r0 = r3.A00
            X.0UN r0 = r0.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r14, r1, r0)
            X.0Ud r0 = (X.AnonymousClass0Ud) r0
            boolean r0 = r0.A0H()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r4.A04 = r0
            r4.A0D = r13
            r0 = 0
            if (r9 != 0) goto L_0x175f
            r0 = 1
        L_0x175f:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r4.A05 = r0
            int r1 = X.AnonymousClass1Y3.BGZ
            X.2mN r0 = r3.A00
            X.0UN r0 = r0.A02
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r12, r1, r0)
            X.4VN r13 = (X.AnonymousClass4VN) r13
            r0 = 844918852288593(0x30073000e0051, double:4.174453784394085E-309)
            java.lang.Integer r12 = X.AnonymousClass07B.A01
            java.lang.Integer r12 = X.AnonymousClass4VN.A01(r13, r0, r12)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            if (r12 == r0) goto L_0x1785
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1 = 0
            if (r12 != r0) goto L_0x1786
        L_0x1785:
            r1 = 1
        L_0x1786:
            r0 = r2
            if (r1 == 0) goto L_0x179b
            r12 = 12
            int r1 = X.AnonymousClass1Y3.Abl
            X.2mN r0 = r3.A00
            X.0UN r0 = r0.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r12, r1, r0)
            X.2pA r0 = (X.C56042pA) r0
            X.A4I r0 = r0.A02()
        L_0x179b:
            r4.A02 = r0
            r4.A0A = r11
            r0 = 0
            if (r15 != 0) goto L_0x17a3
            r0 = 1
        L_0x17a3:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r4.A03 = r0
            int r1 = X.AnonymousClass1Y3.AEd
            X.2mN r0 = r3.A00
            X.0UN r0 = r0.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.A5C r0 = (X.A5C) r0
            X.A5G r0 = r0.A03()
            r4.A00 = r0
            r4.A0B = r7
            r4.A09 = r5
            r4.A0C = r6
            X.2mO r7 = new X.2mO
            r7.<init>(r4)
            X.2mN r6 = r3.A00
            java.lang.String r5 = r3.A01
            X.8pw r0 = r7.A01
            r4 = 5
            r8 = 3
            if (r0 != 0) goto L_0x17f1
            int r1 = X.AnonymousClass1Y3.BM3
            X.0UN r0 = r6.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.2ew r0 = (X.C50882ew) r0
            r0.A07(r7, r10, r9)
            int r1 = X.AnonymousClass1Y3.ANp
            X.0UN r0 = r6.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.4Uz r1 = (X.C90774Uz) r1
            java.lang.String r0 = "FOREGROUND_LOCATION_CHECK_FAILED"
            X.C90774Uz.A01(r1, r0)
        L_0x17ec:
            X.2mN r0 = r3.A00
            r0.A01 = r2
            return
        L_0x17f1:
            int r1 = X.AnonymousClass1Y3.BM3
            X.0UN r0 = r6.A02
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.2ew r9 = (X.C50882ew) r9
            java.lang.String r8 = "fgl_scan_success"
            r1 = 1
            X.C50882ew.A05(r9, r1)     // Catch:{ IllegalStateException -> 0x1831 }
            X.C50882ew.A03(r9, r1)     // Catch:{ IllegalStateException -> 0x1831 }
            X.C50882ew.A04(r9, r1)     // Catch:{ IllegalStateException -> 0x1831 }
            r0 = 0
            X.C50882ew.A06(r9, r0)     // Catch:{ IllegalStateException -> 0x1831 }
            int r0 = r9.A03     // Catch:{ IllegalStateException -> 0x1831 }
            int r0 = r0 + r1
            r9.A03 = r0     // Catch:{ IllegalStateException -> 0x1831 }
            X.1La r10 = X.C50882ew.A00(r9, r8)     // Catch:{ IllegalStateException -> 0x1831 }
            if (r10 == 0) goto L_0x182d
            X.069 r0 = r9.A0C     // Catch:{ IllegalStateException -> 0x1831 }
            long r12 = r0.now()     // Catch:{ IllegalStateException -> 0x1831 }
            X.06B r0 = r9.A0B     // Catch:{ IllegalStateException -> 0x1831 }
            long r14 = r0.now()     // Catch:{ IllegalStateException -> 0x1831 }
            r16 = 0
            r17 = 0
            r11 = r7
            X.C50882ew.A02(r9, r10, r11, r12, r14, r16, r17)     // Catch:{ IllegalStateException -> 0x1831 }
            r10.A0A()     // Catch:{ IllegalStateException -> 0x1831 }
        L_0x182d:
            r0 = -9223372036854775808
            r9.A08 = r0     // Catch:{ IllegalStateException -> 0x1831 }
        L_0x1831:
            X.0UN r8 = r6.A02
            r1 = 2
            int r0 = X.AnonymousClass1Y3.B3D
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r0, r8)
            X.2ev r8 = (X.C50872ev) r8
            boolean r1 = r6.A03
            r0 = 1
            r8.A01(r7, r5, r1, r0)
            int r1 = X.AnonymousClass1Y3.ANp
            X.0UN r0 = r6.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.4Uz r1 = (X.C90774Uz) r1
            java.lang.String r0 = "FOREGROUND_LOCATION_AVAILABLE"
            X.C90774Uz.A01(r1, r0)
            goto L_0x17ec
        L_0x1852:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x1855:
            r0 = r2
            X.1EK r0 = (X.AnonymousClass1EK) r0
            X.2f1 r1 = r0.A00
            r0 = 0
            r1.A00 = r0
            return
        L_0x185e:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            X.C53552ky.A01(r5, r0)
            return
        L_0x1864:
            com.facebook.base.activity.FbFragmentActivity r3 = r5.A00
            r2 = 4975(0x136f, float:6.971E-42)
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r4 = 0
            if (r7 == r1) goto L_0x1873
            r0 = 0
        L_0x186e:
            if (r0 == 0) goto L_0x188e
            r5.A03 = r6
            return
        L_0x1873:
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            r0.A00 = r1
            com.google.android.gms.location.LocationSettingsResult r0 = r0.A01     // Catch:{ SendIntentException -> 0x1882 }
            com.google.android.gms.common.api.Status r0 = r0.B3p()     // Catch:{ SendIntentException -> 0x1882 }
            r0.A00(r3, r2)     // Catch:{ SendIntentException -> 0x1882 }
            r0 = 1
            goto L_0x186e
        L_0x1882:
            r3 = move-exception
            java.lang.Class r2 = X.C50922f0.A02
            java.lang.Object[] r1 = new java.lang.Object[r4]
            java.lang.String r0 = "Error starting google play services location dialog"
            X.C010708t.A0E(r2, r3, r0, r1)
            r0 = 0
            goto L_0x186e
        L_0x188e:
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            X.C53552ky.A01(r5, r0)
            return
        L_0x1894:
            java.lang.String r1 = X.C53592l2.A02(r4, r3)     // Catch:{ SecurityException -> 0x18ef }
            int r0 = r9.getDbValue()     // Catch:{ SecurityException -> 0x18ef }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ SecurityException -> 0x18ef }
            r7.setUserData(r8, r1, r0)     // Catch:{ SecurityException -> 0x18ef }
            X.2l3 r1 = new X.2l3     // Catch:{ SecurityException -> 0x18ef }
            r1.<init>()     // Catch:{ SecurityException -> 0x18ef }
            r1.A01 = r6     // Catch:{ SecurityException -> 0x18ef }
            java.lang.Integer r0 = X.AnonymousClass07B.A07     // Catch:{ SecurityException -> 0x18ef }
            r1.A02 = r0     // Catch:{ SecurityException -> 0x18ef }
            r1.A00 = r9     // Catch:{ SecurityException -> 0x18ef }
            java.lang.String r0 = X.C53592l2.A02(r4, r3)     // Catch:{ SecurityException -> 0x18ef }
            r1.A03 = r0     // Catch:{ SecurityException -> 0x18ef }
            X.6Sy r0 = r1.A00()     // Catch:{ SecurityException -> 0x18ef }
            r2.A03(r0)     // Catch:{ SecurityException -> 0x18ef }
            java.lang.String r1 = "background_location_settings"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r4)     // Catch:{ SecurityException -> 0x18ef }
            if (r9 != 0) goto L_0x18c7
            r0 = 0
            goto L_0x18cf
        L_0x18c7:
            int r0 = r9.getDbValue()     // Catch:{ SecurityException -> 0x18ef }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ SecurityException -> 0x18ef }
        L_0x18cf:
            r7.setUserData(r8, r1, r0)     // Catch:{ SecurityException -> 0x18ef }
            X.2l3 r1 = new X.2l3     // Catch:{ SecurityException -> 0x18ef }
            r1.<init>()     // Catch:{ SecurityException -> 0x18ef }
            r1.A01 = r6     // Catch:{ SecurityException -> 0x18ef }
            java.lang.Integer r0 = X.AnonymousClass07B.A09     // Catch:{ SecurityException -> 0x18ef }
            r1.A02 = r0     // Catch:{ SecurityException -> 0x18ef }
            r1.A00 = r9     // Catch:{ SecurityException -> 0x18ef }
            java.lang.String r0 = "background_location_settings"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r4)     // Catch:{ SecurityException -> 0x18ef }
            r1.A03 = r0     // Catch:{ SecurityException -> 0x18ef }
            X.6Sy r0 = r1.A00()     // Catch:{ SecurityException -> 0x18ef }
            r2.A03(r0)     // Catch:{ SecurityException -> 0x18ef }
            return
        L_0x18ef:
            X.2l3 r1 = new X.2l3
            r1.<init>()
            r1.A01 = r6
            java.lang.Integer r0 = X.AnonymousClass07B.A08
            r1.A02 = r0
            r1.A00 = r9
            java.lang.String r0 = X.C53592l2.A02(r4, r3)
            r1.A03 = r0
            X.6Sy r0 = r1.A00()
            r2.A03(r0)
            return
        L_0x190a:
            X.2l3 r1 = new X.2l3
            r1.<init>()
            r1.A01 = r6
            java.lang.Integer r0 = X.AnonymousClass07B.A05
            r1.A02 = r0
            r1.A00 = r9
            java.lang.String r0 = X.C53592l2.A02(r4, r3)
            r1.A03 = r0
            X.6Sy r1 = r1.A00()
            X.6Sz r0 = r2.A04
            r0.A03(r1)
            return
        L_0x1927:
            X.2l4 r0 = r2.A00
            r0.A00()
            X.2ez r0 = r2.A01
            X.09P r2 = r0.A00
            java.lang.String r1 = "AccountLinkTaskManager"
            java.lang.String r0 = "Messenger platform account link return null data"
            r2.CGS(r1, r0)
            return
        L_0x1938:
            r1 = r2
            X.1EJ r1 = (X.AnonymousClass1EJ) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x194f
            X.2f2 r5 = r1.A00
            java.lang.Object r3 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 != 0) goto L_0x1954
            com.facebook.messaging.business.ads.extension.MessengerAdContextAdItemView r1 = r5.A00
        L_0x1949:
            r0 = 8
            r1.setVisibility(r0)
            return
        L_0x194f:
            X.2f2 r0 = r1.A00
            com.facebook.messaging.business.ads.extension.MessengerAdContextAdItemView r1 = r0.A00
            goto L_0x1949
        L_0x1954:
            X.2lr r2 = new X.2lr
            r2.<init>()
            X.2ls r0 = X.C54102ls.FROM_STREAM
            r2.A04 = r0
            java.lang.String r1 = r3.A3t()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x196d
            android.net.Uri r0 = android.net.Uri.parse(r1)
            r2.A03 = r0
        L_0x196d:
            r0 = 2093822798(0x7ccd334e, float:8.5236915E36)
            java.lang.String r1 = r3.A0P(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x1980
            android.net.Uri r0 = android.net.Uri.parse(r1)
            r2.A02 = r0
        L_0x1980:
            r0 = 1879474642(0x700681d2, float:1.6651174E29)
            java.lang.String r1 = r3.A0P(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x198f
            r2.A07 = r1
        L_0x198f:
            X.2lv r1 = new X.2lv
            r1.<init>()
            java.lang.String r0 = r5.A01
            r1.A0O = r0
            com.facebook.video.engine.api.VideoDataSource r0 = new com.facebook.video.engine.api.VideoDataSource
            r0.<init>(r2)
            r1.A0H = r0
            com.facebook.video.engine.api.VideoPlayerParams r0 = new com.facebook.video.engine.api.VideoPlayerParams
            r0.<init>(r1)
            X.2lw r6 = new X.2lw
            r6.<init>()
            r6.A02 = r0
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 1508901707(0x59f0034b, float:8.4447019E15)
            r0 = -1378953145(0xffffffffadced847, float:-2.3515535E-11)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            r4 = 0
            if (r3 == 0) goto L_0x1a02
            r1 = 104993457(0x64212b1, float:3.6501077E-35)
            r0 = 537603271(0x200b2cc7, float:1.1788574E-19)
            com.google.common.collect.ImmutableList r7 = r3.A0M(r1, r2, r0)
            if (r7 == 0) goto L_0x1a02
            java.lang.Object r3 = r7.get(r4)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            r1 = 100313435(0x5faa95b, float:2.3572098E-35)
            r0 = 1232448243(0x4975aaf3, float:1006255.2)
            com.facebook.graphservice.tree.TreeJNI r0 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x1a02
            java.lang.Object r3 = r7.get(r4)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            r0 = 1232448243(0x4975aaf3, float:1006255.2)
            com.facebook.graphservice.tree.TreeJNI r0 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            java.lang.String r1 = r0.A41()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x1a02
            X.1Q0 r1 = X.AnonymousClass1Q0.A01(r1)
            r0 = 107(0x6b, float:1.5E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r6.A03(r0, r1)
        L_0x1a02:
            com.facebook.messaging.business.ads.extension.MessengerAdContextAdItemView r0 = r5.A00
            com.facebook.video.player.RichVideoPlayer r1 = r0.A04
            X.8Aj r0 = r6.A00()
            r1.A0T(r0)
            com.facebook.messaging.business.ads.extension.MessengerAdContextAdItemView r0 = r5.A00
            r0.setVisibility(r4)
            return
        L_0x1a13:
            r1 = r2
            X.1EH r1 = (X.AnonymousClass1EH) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.2mP r6 = r1.A01
            java.lang.Object r3 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            X.2mQ r2 = r6.A00
            android.widget.ProgressBar r1 = r2.A00
            r0 = 8
            r1.setVisibility(r0)
            androidx.recyclerview.widget.RecyclerView r1 = r2.A01
            r0 = 0
            r1.setVisibility(r0)
            X.2mQ r0 = r6.A00
            X.2mS r0 = r0.A04
            if (r0 == 0) goto L_0x1a3c
            com.facebook.messaging.business.common.activity.BusinessActivity r0 = r0.A00
            X.0xM r1 = r0.A01
            X.7xw r0 = r0.A05
            r1.A05(r0)
        L_0x1a3c:
            X.2mQ r0 = r6.A00
            X.2mT r7 = r0.A03
            r7.A01 = r3
            X.8jJ r1 = r7.A04
            r0 = -1003455201(0xffffffffc4307d1f, float:-705.955)
            java.lang.String r0 = r3.A0P(r0)
            int r0 = r1.A01(r0)
            r7.A00 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r7.A02 = r0
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -5540135(0xffffffffffab76d9, float:NaN)
            r0 = 806640248(0x30145a78, float:5.397065E-10)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x1aa6
            r1 = 104993457(0x64212b1, float:3.6501077E-35)
            r0 = -48186094(0xfffffffffd20bd12, float:-1.3353637E37)
            com.google.common.collect.ImmutableList r1 = r3.A0M(r1, r2, r0)
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x1aa6
            X.1Xv r5 = r1.iterator()
        L_0x1a7c:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x1aa6
            java.lang.Object r3 = r5.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            r1 = 1055868832(0x3eef47a0, float:0.46734333)
            r0 = -1074226180(0xffffffffbff89bfc, float:-1.9422603)
            com.facebook.graphservice.tree.TreeJNI r4 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            if (r4 == 0) goto L_0x1a7c
            java.util.List r3 = r7.A02
            r1 = 104993457(0x64212b1, float:3.6501077E-35)
            r0 = 1041381285(0x3e1237a5, float:0.14279039)
            com.google.common.collect.ImmutableList r0 = r4.A0M(r1, r2, r0)
            r3.addAll(r0)
            goto L_0x1a7c
        L_0x1aa6:
            X.2mQ r0 = r6.A00
            X.2mT r0 = r0.A03
            r0.A04()
            return
        L_0x1aae:
            r4 = r2
            X.1uv r4 = (X.C37241uv) r4
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x1acf
            java.lang.Object r1 = r0.A03
            if (r1 == 0) goto L_0x1acf
            r3 = 1
            int r2 = X.AnonymousClass1Y3.APr
            X.2E1 r1 = r4.A02
            X.0UN r1 = r1.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1Y6 r2 = (X.AnonymousClass1Y6) r2
            X.EVC r1 = new X.EVC
            r1.<init>(r4, r0)
            r2.BxR(r1)
            return
        L_0x1acf:
            r2 = 0
            int r1 = X.AnonymousClass1Y3.Amr
            X.2E1 r3 = r4.A02
            X.0UN r0 = r3.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "GraphQL return invalid results for: "
            X.7HV r0 = r3.A03
            java.lang.String r0 = r0.tag
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.String r0 = "ComposerShortcutsDataProvider"
            r2.CGS(r0, r1)
            return
        L_0x1aec:
            r1 = r2
            X.1EG r1 = (X.AnonymousClass1EG) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            java.lang.Object r4 = r0.A03
            if (r4 == 0) goto L_0x1b1a
            X.2f3 r0 = r1.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            com.facebook.browserextensions.ipc.payments.CanMakePaymentJSBridgeCall r3 = r0.A00
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 3433103(0x34628f, float:4.810802E-39)
            r0 = 1295238197(0x4d33c435, float:1.88498768E8)
            com.facebook.graphservice.tree.TreeJNI r1 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 == 0) goto L_0x1b15
            r0 = 2065348735(0x7b1ab87f, float:8.0335574E35)
            boolean r1 = r1.getBooleanValue(r0)
            r0 = 1
            if (r1 != 0) goto L_0x1b16
        L_0x1b15:
            r0 = 0
        L_0x1b16:
            X.C209059tc.A01(r3, r0)
            return
        L_0x1b1a:
            X.2f3 r0 = r1.A00
            com.facebook.browserextensions.ipc.payments.CanMakePaymentJSBridgeCall r1 = r0.A00
            r0 = 0
            X.C209059tc.A01(r1, r0)
            return
        L_0x1b23:
            r1 = r2
            X.1EF r1 = (X.AnonymousClass1EF) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.2mU r4 = r1.A00
            java.lang.Object r3 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            r6 = 0
            if (r3 == 0) goto L_0x1b6b
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 625409519(0x2546fdef, float:1.7259798E-16)
            r0 = -1935221110(0xffffffff8ca6de8a, float:-2.5710285E-31)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x1b6b
            r1 = -303113191(0xffffffffedeedc19, float:-9.240439E27)
            r0 = 874920303(0x3426396f, float:1.5480849E-7)
            com.google.common.collect.ImmutableList r3 = r3.A0M(r1, r2, r0)
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x1b6b
            int r2 = r3.size()
            java.lang.String[] r5 = new java.lang.String[r2]
        L_0x1b57:
            if (r6 >= r2) goto L_0x1b6d
            java.lang.Object r1 = r3.get(r6)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r0 = -1621505110(0xffffffff9f59cbaa, float:-4.6120004E-20)
            java.lang.String r0 = r1.A0P(r0)
            r5[r6] = r0
            int r6 = r6 + 1
            goto L_0x1b57
        L_0x1b6b:
            java.lang.String[] r5 = new java.lang.String[r6]
        L_0x1b6d:
            com.facebook.browserextensions.ipc.messengerplatform.permission.GetGrantedPermissionsJSBridgeCall r4 = r4.A00
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            java.lang.String r1 = r4.Afq()
            java.lang.String r0 = "callbackID"
            r3.putString(r0, r1)
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ JSONException -> 0x1b9a }
            r1.<init>(r5)     // Catch:{ JSONException -> 0x1b9a }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x1b9a }
            r2.<init>()     // Catch:{ JSONException -> 0x1b9a }
            java.lang.String r0 = "permissions"
            r2.put(r0, r1)     // Catch:{ JSONException -> 0x1b9a }
            r0 = 51
            java.lang.String r1 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ JSONException -> 0x1b9a }
            java.lang.String r0 = r2.toString()     // Catch:{ JSONException -> 0x1b9a }
            r3.putString(r1, r0)     // Catch:{ JSONException -> 0x1b9a }
            goto L_0x1baa
        L_0x1b9a:
            r2 = move-exception
            r0 = 488(0x1e8, float:6.84E-43)
            java.lang.String r1 = X.AnonymousClass80H.$const$string(r0)
            r0 = 31
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            X.C010708t.A0R(r1, r2, r0)
        L_0x1baa:
            r4.ATa(r3)
            return
        L_0x1bae:
            java.lang.String[] r3 = new java.lang.String[r6]
        L_0x1bb0:
            com.facebook.browserextensions.ipc.messengerplatform.permission.AskPermissionJSBridgeCall r1 = r5.A00
            android.os.Bundle r0 = r1.A0A(r3)
            r1.ATa(r0)
            return
        L_0x1bba:
            java.lang.Integer r0 = X.AnonymousClass07B.A03
            int r0 = X.C208829t7.A00(r0)
            r4.A06(r0)
            return
        L_0x1bc4:
            com.facebook.browserextensions.ipc.messengerplatform.permission.AskPermissionJSBridgeCall r1 = r5.A00
            java.lang.Integer r0 = X.AnonymousClass07B.A0J
            int r0 = X.C208829t7.A00(r0)
            r1.A06(r0)
            return
        L_0x1bd0:
            r1 = r2
            X.1EE r1 = (X.AnonymousClass1EE) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.CWg r1 = r1.A00
            java.lang.Object r0 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            r1.Bgd(r0)
            return
        L_0x1bdf:
            r1 = r2
            X.1EC r1 = (X.AnonymousClass1EC) r1
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.2mV r1 = r1.A00
            java.lang.Object r3 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            com.facebook.browserextensions.ipc.messengerplatform.GetContextJSBridgeCall r4 = r1.A00
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 2102165926(0x7d4c81a6, float:1.698973E37)
            r0 = -1787001444(0xffffffff957c859c, float:-5.0996394E-26)
            com.facebook.graphservice.tree.TreeJNI r1 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r0 = 1991322732(0x76b12c6c, float:1.796753E33)
            java.lang.String r10 = r1.A0P(r0)
            r0 = 114831(0x1c08f, float:1.60913E-40)
            java.lang.String r9 = r1.A0P(r0)
            r0 = 3450462(0x34a65e, float:4.835127E-39)
            java.lang.String r8 = r1.A0P(r0)
            r0 = -1679569615(0xffffffff9be3cd31, float:-3.7686602E-22)
            java.lang.String r7 = r1.A0P(r0)
            r0 = 1106441079(0x41f2f377, float:30.36888)
            com.google.common.collect.ImmutableList r6 = r1.A0L(r0)
            r0 = 1931046991(0x7319704f, float:1.2156667E31)
            java.lang.String r5 = r1.A0P(r0)
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            java.lang.String r1 = r4.Afq()
            java.lang.String r0 = "callbackID"
            r3.putString(r0, r1)
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x1c6d }
            r2.<init>()     // Catch:{ JSONException -> 0x1c6d }
            java.lang.String r0 = "signed_request"
            r2.put(r0, r10)     // Catch:{ JSONException -> 0x1c6d }
            java.lang.String r0 = "tid"
            r2.put(r0, r9)     // Catch:{ JSONException -> 0x1c6d }
            java.lang.String r0 = "psid"
            r2.put(r0, r8)     // Catch:{ JSONException -> 0x1c6d }
            r0 = 140(0x8c, float:1.96E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ JSONException -> 0x1c6d }
            r2.put(r0, r7)     // Catch:{ JSONException -> 0x1c6d }
            boolean r0 = r6.isEmpty()     // Catch:{ JSONException -> 0x1c6d }
            if (r0 != 0) goto L_0x1c5a
            java.lang.String r0 = "thread_participant_ids"
            r2.put(r0, r6)     // Catch:{ JSONException -> 0x1c6d }
        L_0x1c5a:
            java.lang.String r0 = "thread_type"
            r2.put(r0, r5)     // Catch:{ JSONException -> 0x1c6d }
            r0 = 51
            java.lang.String r1 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ JSONException -> 0x1c6d }
            java.lang.String r0 = r2.toString()     // Catch:{ JSONException -> 0x1c6d }
            r3.putString(r1, r0)     // Catch:{ JSONException -> 0x1c6d }
            goto L_0x1c79
        L_0x1c6d:
            r2 = move-exception
            java.lang.String r1 = "getContext"
            r0 = 31
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            X.C010708t.A0R(r1, r2, r0)
        L_0x1c79:
            r4.ATa(r3)
            return
        L_0x1c7d:
            r0 = r2
            X.1ut r0 = (X.C37221ut) r0
            X.DUG r1 = r0.A00
            X.DTt r0 = r1.A00
            X.5t1 r0 = r0.A01
            if (r0 == 0) goto L_0x1c8b
            r0.CI0()
        L_0x1c8b:
            X.DTt r0 = r1.A00
            android.app.Activity r0 = r0.A05
            r0.onBackPressed()
            return
        L_0x1c93:
            r0 = r2
            X.1E8 r0 = (X.AnonymousClass1E8) r0
            X.9xk r0 = r0.A00
            goto L_0x1c9e
        L_0x1c99:
            r0 = r2
            X.1E9 r0 = (X.AnonymousClass1E9) r0
            X.9xk r0 = r0.A00
        L_0x1c9e:
            r0.onSuccess()
            return
        L_0x1ca2:
            X.1E7 r2 = (X.AnonymousClass1E7) r2
            com.facebook.fbservice.service.OperationResult r0 = (com.facebook.fbservice.service.OperationResult) r0
            java.lang.Object r0 = r0.A07()
            com.facebook.messaging.emoji.service.FetchRecentEmojiResult r0 = (com.facebook.messaging.emoji.service.FetchRecentEmojiResult) r0
            X.2mW r1 = r2.A00
            com.google.common.collect.ImmutableList r0 = r0.A00
            X.C54502mW.A00(r1, r0)
            X.2mW r1 = r2.A00
            r0 = 0
            r1.A01 = r0
            return
        L_0x1cb9:
            r4 = r2
            X.1us r4 = (X.C37211us) r4
            X.8Z3 r1 = r4.A00
            X.3VL r1 = r1.A01
            if (r1 == 0) goto L_0x1cc5
            r1.Bqf(r0)
        L_0x1cc5:
            r3 = 5
            int r2 = X.AnonymousClass1Y3.AZ8
            X.8Z3 r1 = r4.A00
            X.3Aq r0 = r1.A03
            X.0UN r0 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.4Wi r3 = (X.C91084Wi) r3
            X.4Wj r2 = r1.A02
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            r0 = 0
            r3.A01(r2, r1, r0)
            return
        L_0x1cdd:
            r3 = r2
            X.1E6 r3 = (X.AnonymousClass1E6) r3
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            r5 = 0
            java.lang.Object r4 = r0.A03     // Catch:{ Exception -> 0x1d22 }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4     // Catch:{ Exception -> 0x1d22 }
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -956262000(0xffffffffc7009990, float:-32921.562)
            r0 = -371912820(0xffffffffe9d50f8c, float:-3.2196827E25)
            com.facebook.graphservice.tree.TreeJNI r4 = r4.A0J(r1, r2, r0)     // Catch:{ Exception -> 0x1d22 }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4     // Catch:{ Exception -> 0x1d22 }
            com.google.common.base.Preconditions.checkNotNull(r4)     // Catch:{ Exception -> 0x1d22 }
            r1 = 96356950(0x5be4a56, float:1.7894821E-35)
            r0 = -286836405(0xffffffffeee7394b, float:-3.578015E28)
            com.google.common.collect.ImmutableList r0 = r4.A0M(r1, r2, r0)     // Catch:{ Exception -> 0x1d22 }
            java.lang.Object r4 = r0.get(r5)     // Catch:{ Exception -> 0x1d22 }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4     // Catch:{ Exception -> 0x1d22 }
            r1 = 3386882(0x33ae02, float:4.746033E-39)
            r0 = -1707917148(0xffffffff9a3340a4, float:-3.706855E-23)
            com.facebook.graphservice.tree.TreeJNI r1 = r4.A0J(r1, r2, r0)     // Catch:{ Exception -> 0x1d22 }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1     // Catch:{ Exception -> 0x1d22 }
            if (r1 == 0) goto L_0x1d47
            r0 = -362700720(0xffffffffea61a050, float:-6.819134E25)
            java.lang.String r0 = r1.A0P(r0)     // Catch:{ Exception -> 0x1d22 }
            int r5 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x1d22 }
            goto L_0x1d48
        L_0x1d22:
            r4 = move-exception
            X.2f4 r0 = r3.A02
            X.09P r2 = r0.A00
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Error parsing result. start="
            r1.<init>(r0)
            android.location.Location r0 = r3.A01
            r1.append(r0)
            java.lang.String r0 = " destination="
            r1.append(r0)
            android.location.Location r0 = r3.A00
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "live_location_eta_calculator_graphql"
            r2.CGZ(r0, r1, r4)
            goto L_0x1d48
        L_0x1d47:
            r5 = -1
        L_0x1d48:
            android.location.Location r1 = r3.A01
            android.location.Location r0 = r3.A00
            float r0 = r1.distanceTo(r0)
            int r2 = (int) r0
            X.2f5 r3 = r3.A03
            X.2hd r4 = new X.2hd
            X.2f6 r0 = r3.A00
            X.06B r0 = r0.A01
            long r0 = r0.now()
            r4.<init>(r5, r2, r0)
            X.2f6 r0 = r3.A00
            java.util.Map r1 = r0.A02
            java.lang.String r0 = r3.A01
            r1.put(r0, r4)
            X.2f6 r0 = r3.A00
            X.0bK r0 = r0.A00
            java.util.List r0 = r0.A00()
            java.util.Iterator r2 = r0.iterator()
        L_0x1d75:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x1d87
            java.lang.Object r1 = r2.next()
            X.2mZ r1 = (X.C54532mZ) r1
            java.lang.String r0 = r3.A01
            r1.BXz(r0, r4)
            goto L_0x1d75
        L_0x1d87:
            X.2f6 r0 = r3.A00
            X.0bK r0 = r0.A00
            r0.A01()
            X.2f6 r0 = r3.A00
            java.util.Set r1 = r0.A03
            java.lang.String r0 = r3.A01
            r1.remove(r0)
            return
        L_0x1d98:
            r1 = r2
            X.1E5 r1 = (X.AnonymousClass1E5) r1
            X.8pw r0 = (X.C188898pw) r0
            if (r0 != 0) goto L_0x1da5
            X.A5B r0 = r1.A00
            r0.BYW()
            return
        L_0x1da5:
            X.A5B r1 = r1.A00
            android.location.Location r0 = r0.A01()
            r1.BVF(r0)
            return
        L_0x1daf:
            r3 = r2
            X.1E0 r3 = (X.AnonymousClass1E0) r3
            X.8pw r0 = (X.C188898pw) r0
            if (r0 != 0) goto L_0x1dce
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AvX
            com.facebook.messaging.livelocation.feature.LiveLocationForegroundService r0 = r3.A00
            X.0UN r0 = r0.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3KD r1 = (X.AnonymousClass3KD) r1
            java.lang.String r0 = "messenger_live_location_did_receive_null_location"
            X.AnonymousClass3KD.A05(r1, r0)
            com.facebook.messaging.livelocation.feature.LiveLocationForegroundService r0 = r3.A00
            com.facebook.messaging.livelocation.feature.LiveLocationForegroundService.A00(r0)
            return
        L_0x1dce:
            int r2 = X.AnonymousClass1Y3.B54
            com.facebook.messaging.livelocation.feature.LiveLocationForegroundService r4 = r3.A00
            X.0UN r1 = r4.A01
            r6 = 3
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.8Vj r2 = (X.AnonymousClass8Vj) r2
            X.0Tq r1 = r4.A04
            java.lang.Object r1 = r1.get()
            com.facebook.user.model.UserKey r1 = (com.facebook.user.model.UserKey) r1
            com.google.common.collect.ImmutableSet r4 = r2.A05(r1)
            android.location.Location r5 = r0.A01()
            int r1 = X.AnonymousClass1Y3.B54
            com.facebook.messaging.livelocation.feature.LiveLocationForegroundService r2 = r3.A00
            X.0UN r0 = r2.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.8Vj r1 = (X.AnonymousClass8Vj) r1
            X.0Tq r0 = r2.A04
            java.lang.Object r0 = r0.get()
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            X.8Vn r0 = r1.A03(r0)
            r0.A00(r5)
            r1 = 7
            int r0 = X.AnonymousClass1Y3.AT2
            com.facebook.messaging.livelocation.feature.LiveLocationForegroundService r3 = r3.A00
            X.0UN r2 = r3.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.8ph r1 = (X.C188778ph) r1
            int r0 = X.AnonymousClass1Y3.B54
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r0, r2)
            X.8Vj r0 = (X.AnonymousClass8Vj) r0
            int r0 = r0.A00
            r1.A01(r5, r4, r0, r3)
            return
        L_0x1e21:
            r1 = r2
            X.1Dy r1 = (X.C20821Dy) r1
            X.8pw r0 = (X.C188898pw) r0
            X.2lH r8 = r1.A00
            java.lang.String r7 = r1.A03
            java.lang.Integer r6 = r1.A01
            java.lang.Long r5 = r1.A02
            X.3DS r4 = r8.A03
            X.2mc r3 = X.C54562mc.GET_PLACES
            X.8pk r2 = r8.A01
            android.location.Location r1 = r0.A01()
            if (r6 == 0) goto L_0x1e58
            int r0 = r6.intValue()
            int r0 = 1 - r0
            if (r0 == 0) goto L_0x1e51
            java.lang.String r0 = "MARKETPLACE_MEETING_LOCATIONS"
        L_0x1e44:
            com.google.common.util.concurrent.ListenableFuture r1 = r2.AZW(r1, r7, r0, r5)
            X.1E1 r0 = new X.1E1
            r0.<init>(r8, r7)
            r4.A08(r3, r1, r0)
            return
        L_0x1e51:
            r0 = 41
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            goto L_0x1e44
        L_0x1e58:
            r0 = 0
            goto L_0x1e44
        L_0x1e5a:
            com.facebook.messaging.location.picker.NearbyPlacesView r0 = r3.A07
            android.view.View r1 = r0.A00
            r0 = 8
            r1.setVisibility(r0)
            return
        L_0x1e64:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 == 0) goto L_0x1e7a
            X.2fD r0 = r3.A03
            X.1Yd r2 = r0.A00
            r0 = 285769944012669(0x103e80000177d, double:1.41189111950636E-309)
            boolean r0 = r2.Aem(r0)
            r2 = 1
            if (r0 != 0) goto L_0x1e7b
        L_0x1e7a:
            r2 = 0
        L_0x1e7b:
            com.google.common.collect.ImmutableList$Builder r1 = com.google.common.collect.ImmutableList.builder()
            com.facebook.messaging.location.model.NearbyPlace r0 = r3.A02
            if (r0 == 0) goto L_0x1e86
            r1.add(r0)
        L_0x1e86:
            if (r2 == 0) goto L_0x1e8f
            com.google.common.collect.ImmutableList r0 = X.C51102fS.A00(r3)
            r1.addAll(r0)
        L_0x1e8f:
            r1.addAll(r4)
            com.google.common.collect.ImmutableList r1 = r1.build()
            com.facebook.messaging.location.picker.NearbyPlacesView r2 = r3.A07
            X.2md r0 = r2.A03
            r0.A00 = r1
            r0.A04()
            android.view.View r1 = r2.A00
            r0 = 8
            r1.setVisibility(r0)
            androidx.recyclerview.widget.RecyclerView r1 = r2.A01
            r0 = 0
            r1.setVisibility(r0)
            X.0vt r0 = r2.A04
            r0.A03()
            return
        L_0x1eb2:
            r3 = r2
            X.1Dw r3 = (X.C20801Dw) r3
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            X.2me r4 = r3.A00
            com.facebook.messaging.neo.threadviewbannerentrypoint.LikelyParentDownloadPromptNotificationsManager r2 = r4.A01
            com.facebook.messaging.model.threads.ThreadSummary r1 = r2.A00
            if (r1 == 0) goto L_0x1f9d
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x1f9d
            com.google.common.base.Optional r1 = r2.A02
            if (r1 == 0) goto L_0x1f9d
            com.google.common.base.Optional r0 = r4.A02
            if (r1 != r0) goto L_0x1f9d
            X.2fH r4 = r2.A0A
            java.lang.Object r0 = r1.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r5 = r0.booleanValue()
            boolean r0 = X.C51042fH.A06
            if (r0 != 0) goto L_0x1f9a
            X.1Y7 r0 = X.C51042fH.A09
            boolean r0 = X.C51042fH.A01(r4, r0)
            if (r0 != 0) goto L_0x1f9a
            X.31u r1 = r4.A04
            java.lang.String r0 = "mk_promotion_fm_thread_dismiss_timestamp"
            com.google.common.base.Optional r0 = r1.A07(r0)
            boolean r0 = r0.isPresent()
        L_0x1ef1:
            if (r0 != 0) goto L_0x1f0e
            boolean r0 = X.C51042fH.A07
            if (r0 != 0) goto L_0x1f97
            X.1Y7 r0 = X.C51042fH.A0A
            boolean r0 = X.C51042fH.A01(r4, r0)
            if (r0 != 0) goto L_0x1f97
            X.31u r1 = r4.A04
            java.lang.String r0 = "mk_promotion_fm_thread_tap_timestamp"
            com.google.common.base.Optional r0 = r1.A07(r0)
            boolean r1 = r0.isPresent()
        L_0x1f0b:
            r0 = 0
            if (r1 == 0) goto L_0x1f0f
        L_0x1f0e:
            r0 = 1
        L_0x1f0f:
            if (r0 != 0) goto L_0x1f40
            X.1Yd r2 = r4.A02
            r0 = 285061274539374(0x103430002156e, double:1.408389827096247E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x1f40
            X.1Yd r4 = r4.A02
            r1 = 848011227824672(0x3034300000220, double:4.18973214955825E-309)
            java.lang.String r0 = "ALWAYS_SHOW"
            java.lang.String r2 = r4.B4E(r1, r0)
            r1 = 0
            if (r5 == 0) goto L_0x1f8e
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x1f3c
            java.lang.String r0 = "ONLY_SHOW_FOR_THREADS_WITH_UNREAD_MESSAGES"
        L_0x1f36:
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x1f3d
        L_0x1f3c:
            r1 = 1
        L_0x1f3d:
            r0 = 1
            if (r1 != 0) goto L_0x1f41
        L_0x1f40:
            r0 = 0
        L_0x1f41:
            if (r0 == 0) goto L_0x1f9d
            X.2me r0 = r3.A00
            com.facebook.messaging.neo.threadviewbannerentrypoint.LikelyParentDownloadPromptNotificationsManager r4 = r0.A01
            X.2fI r3 = new X.2fI
            r3.<init>()
            java.util.Date r2 = new java.util.Date
            X.06B r0 = r4.A08
            long r0 = r0.now()
            r2.<init>(r0)
            r3.A05 = r2
            X.2fH r0 = r4.A0A
            X.1Yd r2 = r0.A02
            r0 = 848011227890209(0x3034300010221, double:4.189732149882046E-309)
            java.lang.String r0 = r2.B4C(r0)
            r3.A03 = r0
            X.2fH r0 = r4.A0A
            android.content.res.Resources r1 = r0.A00
            r0 = 2131827754(0x7f111c2a, float:1.928843E38)
            java.lang.String r0 = r1.getString(r0)
            r3.A04 = r0
            r0 = 1
            r3.A06 = r0
            android.net.Uri r0 = r4.A07
            r3.A00 = r0
            X.2mf r0 = new X.2mf
            r0.<init>(r3)
            r4.A01 = r0
            r4.A0A()
            X.3Lc r1 = r4.A09
            java.lang.String r0 = "saw_banner"
            X.C66653Lc.A01(r1, r0)
            return
        L_0x1f8e:
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x1f3c
            java.lang.String r0 = "ONLY_SHOW_FOR_THREADS_WITH_READ_MESSAGES"
            goto L_0x1f36
        L_0x1f97:
            r1 = 1
            goto L_0x1f0b
        L_0x1f9a:
            r0 = 1
            goto L_0x1ef1
        L_0x1f9d:
            X.2me r0 = r3.A00
            com.facebook.messaging.neo.threadviewbannerentrypoint.LikelyParentDownloadPromptNotificationsManager r1 = r0.A01
            r0 = 0
            r1.A01 = r0
            r1.A0A()
            return
        L_0x1fa8:
            X.2mg r2 = new X.2mg
            r2.<init>()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A00
            r2.A06 = r0
            X.2mh r0 = r3.A02
            java.lang.String r0 = r0.A08
            r2.A0C = r0
            X.2fL r0 = X.C51072fL.A0F
            r2.A03 = r0
            com.facebook.messaging.business.common.calltoaction.CallToActionContextParams r1 = new com.facebook.messaging.business.common.calltoaction.CallToActionContextParams
            r1.<init>(r2)
            X.2lL r0 = r3.A01
            X.3LU r0 = r0.A02
            r0.A02(r4, r1)
            return
        L_0x1fc8:
            r1 = r2
            X.1Dv r1 = (X.C20791Dv) r1
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            X.CU6 r1 = r1.A00
            r1.BqX(r0)
            return
        L_0x1fd3:
            boolean r0 = r3.isEmpty()
            if (r0 == 0) goto L_0x1fe5
            X.2lO r0 = r4.A01
            X.CSW r1 = r0.A04
            if (r1 == 0) goto L_0x1fe5
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            r1.A01(r0)
            return
        L_0x1fe5:
            X.2lO r0 = r4.A01
            X.CSo r1 = r0.A06
            X.2lO r0 = r4.A02
            r1.A0F(r3, r0)
            return
        L_0x1fef:
            r0 = r2
            X.1Dp r0 = (X.C20731Dp) r0
            X.CSi r1 = r0.A00
            com.facebook.messaging.omnim.reminder.model.OmniMReminderParams r0 = r0.A01
            goto L_0x1ffe
        L_0x1ff7:
            r0 = r2
            X.1Du r0 = (X.C20781Du) r0
            X.CSi r1 = r0.A00
            com.facebook.messaging.omnim.reminder.model.OmniMReminderParams r0 = r0.A01
        L_0x1ffe:
            java.lang.String r0 = r0.A0D
            r1.Bqh(r0)
            return
        L_0x2004:
            r4 = r2
            X.1Dg r4 = (X.C20641Dg) r4
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            java.lang.Object r3 = r0.A03
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -1476146057(0xffffffffa803cc77, float:-7.316297E-15)
            r0 = -804619969(0xffffffffd00a793f, float:-9.2928072E9)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 != 0) goto L_0x2029
            X.CSi r2 = r4.A00
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "event reminder is empty"
            r1.<init>(r0)
            r2.BYh(r1)
        L_0x2029:
            X.CSi r1 = r4.A00
            java.lang.String r0 = r3.A3m()
            r1.Bqh(r0)
            return
        L_0x2033:
            r5 = r2
            X.1De r5 = (X.C20621De) r5
            X.2fQ r0 = r5.A00
            X.4fg r4 = r0.A07
            java.lang.String r3 = r0.A0F
            java.lang.String r2 = r0.A0G
            java.lang.String r1 = "services_request_appointment_create_success"
            java.lang.String r0 = "message"
            r4.A01(r3, r1, r2, r0)
            X.2fQ r0 = r5.A00
            android.widget.ProgressBar r1 = r0.A01
            r0 = 8
            r1.setVisibility(r0)
            X.2fQ r0 = r5.A00
            X.1HF r2 = r0.A04
            java.lang.String r1 = r0.A0H
            r0 = 1
            r2.A01(r1, r0)
            X.2fQ r0 = r5.A00
            X.3Lk r2 = r0.A05
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 0
            r2.AXR(r1, r0)
            return
        L_0x2063:
            r0 = r2
            X.1Dd r0 = (X.C20611Dd) r0
            X.2lh r3 = r0.A00
            X.1HF r2 = r3.A04
            java.lang.String r1 = r3.A0C
            r0 = 1
            r2.A01(r1, r0)
            android.widget.ProgressBar r1 = r3.A01
            r0 = 8
            r1.setVisibility(r0)
            X.3Lk r2 = r3.A05
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 0
            r2.AXR(r1, r0)
            X.3Ph r2 = r3.A09
            X.2fR r1 = new X.2fR
            r0 = 2131832392(0x7f112e48, float:1.9297837E38)
            java.lang.String r0 = r3.A1A(r0)
            r1.<init>(r0)
            r2.A04(r1)
            return
        L_0x2091:
            X.2lk r0 = r5.A00
            X.4xO r1 = r0.A00
            r0 = 0
            r1.AZb(r0)
            return
        L_0x209a:
            r4 = r2
            X.1Dc r4 = (X.C20601Dc) r4
            X.2lo r0 = (X.C54062lo) r0
            com.facebook.messaging.sharing.ShareLauncherActivity r3 = r4.A00
            r2 = 0
            r3.A0N = r2
            boolean r1 = r0.A02
            if (r1 == 0) goto L_0x20b2
            r0 = 0
            r3.setResult(r0)
            com.facebook.messaging.sharing.ShareLauncherActivity r0 = r4.A00
            r0.finish()
            return
        L_0x20b2:
            X.7Ey r1 = r0.A00
            r3.A0G = r1
            X.72i r0 = r0.A01
            r3.A0I = r0
            X.9MN r0 = r3.A01
            if (r0 == 0) goto L_0x2105
            r0.dismiss()
            com.facebook.messaging.sharing.ShareLauncherActivity r1 = r4.A00
            X.7Ey r0 = r1.A0G
            X.2mj r0 = r0.Ahj()
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x210f
            com.facebook.messaging.sharing.ShareComposerFragment r0 = r1.A0B
            java.util.Set r0 = r0.A0P
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.copyOf(r0)
            com.facebook.messaging.sharing.ShareLauncherActivity r0 = r4.A00
            com.facebook.messaging.sharing.ShareLauncherActivity.A04(r0, r1)
            com.facebook.messaging.sharing.ShareLauncherActivity r0 = r4.A00
            com.facebook.messaging.sharing.ShareComposerFragment r0 = r0.A0B
            r0.A2U(r1)
        L_0x20e1:
            com.facebook.messaging.sharing.ShareLauncherActivity r3 = r4.A00
            X.7Ey r2 = r3.A0G
            X.2mj r0 = r2.Ahj()
            com.facebook.messaging.model.attribution.ContentAppAttribution r0 = r0.A00
            if (r0 == 0) goto L_0x2105
            X.26P r1 = r3.A04
            X.2mj r0 = r2.Ahj()
            com.facebook.messaging.model.attribution.ContentAppAttribution r0 = r0.A00
            com.google.common.util.concurrent.ListenableFuture r2 = r1.A01(r0)
            r3.A0M = r2
            X.2fV r1 = new X.2fV
            r1.<init>(r3)
            java.util.concurrent.Executor r0 = r3.A0O
            X.C05350Yp.A08(r2, r1, r0)
        L_0x2105:
            com.facebook.messaging.sharing.ShareLauncherActivity r0 = r4.A00
            com.facebook.messaging.sharing.ShareComposerFragment r1 = r0.A0B
            X.72i r0 = r0.A0I
            r1.A2T(r0)
            return
        L_0x210f:
            com.facebook.messaging.sharing.ShareComposerFragment r0 = r1.A0B
            X.7Dc r0 = r0.A0B
            java.util.List r0 = r0.A01
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
            com.facebook.messaging.sharing.ShareLauncherActivity.A04(r1, r0)
            goto L_0x20e1
        L_0x211d:
            r4 = r2
            X.1jW r4 = (X.C31331jW) r4
            com.facebook.share.model.LinksPreview r0 = (com.facebook.share.model.LinksPreview) r0
            if (r0 != 0) goto L_0x2164
            X.2ml r0 = r4.A01
            com.facebook.messaging.sharing.previewmodel.MessengerSharePreviewLayout r2 = r0.A01
            com.facebook.messaging.business.share.model.MessengerPlatformExtensibleShareContentFields r0 = r4.A00
            java.lang.String r1 = r0.A0I
            android.widget.TextView r0 = r2.A05
            r0.setText(r1)
            com.facebook.messaging.sharing.previewmodel.MessengerSharePreviewLayout.A00(r2)
            com.facebook.messaging.business.share.model.MessengerPlatformExtensibleShareContentFields r0 = r4.A00
            java.lang.String r1 = r0.A0H
            android.widget.TextView r0 = r2.A04
            r0.setText(r1)
            com.facebook.messaging.sharing.previewmodel.MessengerSharePreviewLayout.A00(r2)
            com.facebook.messaging.business.share.model.MessengerPlatformExtensibleShareContentFields r0 = r4.A00
            java.lang.String r1 = r0.A0G
            android.widget.TextView r0 = r2.A03
            r0.setText(r1)
            com.facebook.messaging.sharing.previewmodel.MessengerSharePreviewLayout.A00(r2)
            com.facebook.messaging.business.share.model.MessengerPlatformExtensibleShareContentFields r0 = r4.A00
            java.lang.String r1 = r0.A08
            X.28p r0 = X.C421828p.PHOTO
            r2.A0G(r1, r0)
            X.2ml r0 = r4.A01
            X.2mo r0 = r0.A00
            com.facebook.messaging.sharing.directshare.DirectShareFragment r0 = r0.A00
            X.2mp r0 = r0.A05
            com.facebook.resources.ui.FbTextView r1 = r0.A04
            r0 = 1
            r1.setEnabled(r0)
            return
        L_0x2164:
            X.2ml r1 = r4.A01
            com.facebook.messaging.sharing.previewmodel.MessengerSharePreviewLayout r3 = r1.A01
            java.lang.String r2 = r0.name
            android.widget.TextView r1 = r3.A05
            r1.setText(r2)
            com.facebook.messaging.sharing.previewmodel.MessengerSharePreviewLayout.A00(r3)
            java.lang.String r2 = r0.A00()
            android.widget.TextView r1 = r3.A03
            r1.setText(r2)
            com.facebook.messaging.sharing.previewmodel.MessengerSharePreviewLayout.A00(r3)
            com.facebook.messaging.business.share.model.MessengerPlatformExtensibleShareContentFields r1 = r4.A00
            java.lang.String r2 = r1.A0H
            android.widget.TextView r1 = r3.A04
            r1.setText(r2)
            com.facebook.messaging.sharing.previewmodel.MessengerSharePreviewLayout.A00(r3)
            java.lang.String r1 = r0.A01()
            X.28p r0 = X.C421828p.PHOTO
            r3.A0G(r1, r0)
            X.2ml r0 = r4.A01
            X.2mo r0 = r0.A00
            com.facebook.messaging.sharing.directshare.DirectShareFragment r0 = r0.A00
            X.2mp r0 = r0.A05
            com.facebook.resources.ui.FbTextView r1 = r0.A04
            r0 = 1
            r1.setEnabled(r0)
            return
        L_0x21a2:
            r0 = r2
            X.1GQ r0 = (X.AnonymousClass1GQ) r0
            X.16R r0 = r0.A00
            r1 = 0
            r0.A04 = r1
            X.15b r0 = r0.A0G
            r0.A00()
            return
        L_0x21b0:
            X.1Db r2 = (X.C20591Db) r2
            X.2mq r1 = r2.A00
            r0 = 0
            r1.A2T(r0)
            X.2mq r0 = r2.A00
            androidx.fragment.app.FragmentActivity r0 = r0.A15()
            X.AnonymousClass8VS.A00(r0)
            X.2mq r3 = r2.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r3.A07
            if (r0 != 0) goto L_0x21cb
            X.C54692mq.A07(r3)
            return
        L_0x21cb:
            X.14b r2 = r3.A03
            X.0gA r1 = X.C08870g7.A2V
            boolean r0 = X.C54692mq.A09(r3)
            if (r0 == 0) goto L_0x224c
            java.lang.String r0 = "show_need_admin_approval_alert"
        L_0x21d7:
            r2.AOI(r1, r0)
            X.2ms r1 = r3.A0A
            java.lang.String r0 = "confirmation_screen_impression"
            X.C54712ms.A02(r1, r0)
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            X.0Tq r0 = r3.A0P
            java.lang.Object r0 = r0.get()
            android.content.ComponentName r0 = (android.content.ComponentName) r0
            android.content.Intent r2 = r1.setComponent(r0)
            r1 = 432(0x1b0, float:6.05E-43)
            r0 = 7
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r2.putExtra(r0, r1)
            java.lang.String r1 = r3.A0D
            r0 = 189(0xbd, float:2.65E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r2.putExtra(r0, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r3.A05
            r0 = 135(0x87, float:1.89E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            X.AnonymousClass324.A08(r2, r0, r1)
            boolean r1 = X.C54692mq.A09(r3)
            r0 = 136(0x88, float:1.9E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r2.putExtra(r0, r1)
            int r1 = r3.A01
            r0 = 137(0x89, float:1.92E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r2.putExtra(r0, r1)
            int r1 = r3.A00
            r0 = 134(0x86, float:1.88E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r2.putExtra(r0, r1)
            androidx.fragment.app.FragmentActivity r0 = r3.A15()
            r0.finish()
            X.0Dj r0 = X.C02200Dj.A00()
            X.15w r1 = r0.A0G()
            android.content.Context r0 = r3.A1j()
            r1.A0B(r2, r0)
            return
        L_0x224c:
            java.lang.String r0 = "show_confirmation_alert"
            goto L_0x21d7
        L_0x224f:
            r4 = r2
            X.1Da r4 = (X.C20581Da) r4
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x2274
            java.lang.Object r3 = r0.A03
            if (r3 == 0) goto L_0x2274
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -1492801091(0xffffffffa705a9bd, float:-1.8549473E-15)
            r0 = 1167625110(0x45988b96, float:4881.448)
            com.facebook.graphservice.tree.TreeJNI r0 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x2274
            X.2mq r1 = r4.A00
            java.lang.String r0 = r0.A3m()
            r1.A0D = r0
        L_0x2274:
            X.2mq r0 = r4.A00
            X.C54692mq.A06(r0)
            return
        L_0x227a:
            r4 = r2
            X.1DZ r4 = (X.AnonymousClass1DZ) r4
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.2mq r2 = r4.A00
            r5 = 0
            r2.A2T(r5)
            if (r0 == 0) goto L_0x2424
            java.lang.Object r2 = r0.A03
            if (r2 == 0) goto L_0x2424
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r2.A24()
            if (r0 == 0) goto L_0x2424
            X.2mq r1 = r4.A00
            r1.A05 = r2
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r2.A24()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A2T()
            r1.A06 = r0
            X.2mq r6 = r4.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r6.A05
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r0.A24()
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 1971973977(0x7589ef59, float:3.4970665E32)
            r0 = 431158690(0x19b2f5a2, float:1.8503979E-23)
            com.facebook.graphservice.tree.TreeJNI r0 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            r6.A07 = r0
            X.2mq r0 = r4.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = r0.A07
            if (r2 != 0) goto L_0x22c3
            X.C54692mq.A07(r0)
            return
        L_0x22c3:
            com.facebook.graphql.enums.GraphQLServicesConsumerFlowType r1 = com.facebook.graphql.enums.GraphQLServicesConsumerFlowType.A02
            r0 = -1499933781(0xffffffffa698d3ab, float:-1.0604491E-15)
            java.lang.Enum r1 = r2.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLServicesConsumerFlowType r1 = (com.facebook.graphql.enums.GraphQLServicesConsumerFlowType) r1
            com.facebook.graphql.enums.GraphQLServicesConsumerFlowType r0 = com.facebook.graphql.enums.GraphQLServicesConsumerFlowType.A01
            r2 = 1
            if (r1 != r0) goto L_0x22ff
            X.2mq r0 = r4.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r0.A07
            r0 = -1782949230(0xffffffff95ba5a92, float:-7.5267696E-26)
            java.lang.String r0 = r1.A0P(r0)
            android.net.Uri r1 = android.net.Uri.parse(r0)
            android.content.Intent r5 = new android.content.Intent
            java.lang.String r0 = "android.intent.action.VIEW"
            r5.<init>(r0, r1)
            int r1 = X.AnonymousClass1Y3.AT5
            X.2mq r3 = r4.A00
            X.0UN r0 = r3.A08
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pR r0 = (X.C12480pR) r0
            X.1eJ r1 = r0.A05
            android.content.Context r0 = r3.A1j()
            r1.A0B(r5, r0)
            return
        L_0x22ff:
            X.2mq r0 = r4.A00
            X.14b r1 = r0.A03
            X.0gA r0 = X.C08870g7.A2V
            r1.CH1(r0)
            X.2mq r7 = r4.A00
            X.2ms r6 = r7.A0A
            java.lang.String r1 = r7.A0K
            r6.A03 = r1
            java.lang.String r1 = r7.A0D
            r6.A00 = r1
            java.lang.String r1 = r7.A0I
            r6.A01 = r1
            java.lang.String r3 = r7.A0J
            r6.A02 = r3
            r6.A02 = r3
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r7.A06
            if (r1 == 0) goto L_0x232b
            r0 = -1854815236(0xffffffff9171c3fc, float:-1.907194E-28)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x232c
        L_0x232b:
            r5 = 1
        L_0x232c:
            r6.A06 = r5
            X.2mq r0 = r4.A00
            X.2ms r1 = r0.A0A
            java.lang.String r0 = "enter_flow"
            X.C54712ms.A02(r1, r0)
            X.2mq r0 = r4.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A05
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A24()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A2U()
            if (r0 == 0) goto L_0x23fb
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A2W()
            if (r0 == 0) goto L_0x23fb
            com.google.common.collect.ImmutableList r0 = r0.A3X()
            int r3 = r0.size()
        L_0x2353:
            X.2mq r1 = r4.A00
            java.lang.String r0 = r1.A0F
            if (r0 == 0) goto L_0x23c4
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r1.A05
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A24()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r0.A2U()
            X.2mq r0 = r4.A00
            java.lang.String r6 = r0.A0F
            r5 = 0
            if (r1 == 0) goto L_0x2391
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r1.A2W()
            if (r0 == 0) goto L_0x2391
            com.google.common.collect.ImmutableList r0 = r0.A3X()
            X.1Xv r2 = r0.iterator()
        L_0x2378:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x2391
            java.lang.Object r1 = r2.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 == 0) goto L_0x2378
            java.lang.String r0 = r1.A3m()
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x2378
            r5 = r1
        L_0x2391:
            if (r5 == 0) goto L_0x23a0
            X.2mq r0 = r4.A00
            java.util.ArrayList r1 = r0.A0O
            java.lang.String r0 = r0.A0D
            X.5vc r0 = X.C125745vc.A00(r0, r5)
            r1.add(r0)
        L_0x23a0:
            X.2mq r0 = r4.A00
            java.util.ArrayList r0 = r0.A0O
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x23fe
            if (r3 == 0) goto L_0x23fe
            X.2mq r4 = r4.A00
            r0 = 1
            r4.A2T(r0)
            X.3DS r3 = r4.A0B
            X.2mt r2 = new X.2mt
            r2.<init>(r4)
            X.2mu r1 = new X.2mu
            r1.<init>(r4)
            java.lang.String r0 = "fetch_other_option_service_menu_content"
            r3.A0C(r0, r2, r1)
            return
        L_0x23c4:
            if (r3 != r2) goto L_0x23a0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r1.A05
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A24()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A2U()
            if (r0 == 0) goto L_0x23f9
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A2W()
            if (r0 == 0) goto L_0x23f9
            com.google.common.collect.ImmutableList r1 = r0.A3X()
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x23f9
            r0 = 0
            java.lang.Object r0 = r1.get(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
        L_0x23e9:
            if (r0 == 0) goto L_0x23a0
            X.2mq r1 = r4.A00
            java.util.ArrayList r2 = r1.A0O
            java.lang.String r1 = r1.A0D
            X.5vc r0 = X.C125745vc.A00(r1, r0)
            r2.add(r0)
            goto L_0x23a0
        L_0x23f9:
            r0 = 0
            goto L_0x23e9
        L_0x23fb:
            r3 = 0
            goto L_0x2353
        L_0x23fe:
            X.2mq r3 = r4.A00
            int r2 = r3.A02
            if (r2 > 0) goto L_0x2410
            java.util.ArrayList r0 = r3.A0O
            X.2mv r1 = X.C54692mq.A05(r3, r0)
        L_0x240a:
            X.2mq r0 = r4.A00
            r0.A2S(r1)
            return
        L_0x2410:
            r3.A01 = r2
            java.util.ArrayList r0 = r3.A0O
            int r0 = X.C54692mq.A00(r3, r0)
            int r2 = r2 + r0
            r3.A00 = r2
            X.2mq r1 = r4.A00
            java.util.ArrayList r0 = r1.A0O
            X.5fP r1 = X.C54692mq.A04(r1, r0)
            goto L_0x240a
        L_0x2424:
            X.2mq r0 = r4.A00
            X.C54692mq.A07(r0)
            return
        L_0x242a:
            r7 = r2
            X.1DY r7 = (X.AnonymousClass1DY) r7
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.2mq r2 = r7.A00
            r4 = 0
            r2.A2T(r4)
            if (r0 == 0) goto L_0x249a
            java.lang.Object r3 = r0.A03
            if (r3 == 0) goto L_0x249a
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 3433103(0x34628f, float:4.810802E-39)
            r0 = -1589688591(0xffffffffa13f46f1, float:-6.4807207E-19)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x249a
            r1 = -455127526(0xffffffffe4df4e1a, float:-3.2954014E22)
            r0 = -532291967(0xffffffffe045de81, float:-5.70319E19)
            com.facebook.graphservice.tree.TreeJNI r1 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 == 0) goto L_0x249a
            r0 = -318161122(0xffffffffed093f1e, float:-2.6547344E27)
            com.google.common.collect.ImmutableList r6 = r1.A0K(r0)
            X.2mq r0 = r7.A00
            X.2ms r3 = r0.A0A
            java.util.Date r0 = r7.A02
            long r1 = r0.getTime()
            r0 = 0
            r3.A03(r1, r6, r0)
            boolean r0 = r6.isEmpty()
            if (r0 != 0) goto L_0x2494
            X.2mv r5 = r7.A01
            java.lang.Object r0 = r6.get(r4)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            long r3 = (long) r0
            java.util.Date r2 = new java.util.Date
            r0 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r0
            r2.<init>(r3)
            r5.A04 = r2
            com.facebook.pages.common.requesttime.widget.SingleWeekCalendarView r0 = r5.A01
            if (r0 == 0) goto L_0x2494
            r0.A0T(r2)
        L_0x2494:
            X.2mv r0 = r7.A01
            r0.A2S(r6)
            return
        L_0x249a:
            X.2mq r0 = r7.A00
            X.C54692mq.A07(r0)
            return
        L_0x24a0:
            r8 = r2
            X.1Eo r8 = (X.C20981Eo) r8
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            X.2mq r2 = r8.A00
            r1 = 0
            r2.A2T(r1)
            if (r0 == 0) goto L_0x252d
            java.lang.Object r3 = r0.A03
            if (r3 == 0) goto L_0x252d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 3433103(0x34628f, float:4.810802E-39)
            r0 = -736219249(0xffffffffd41e2f8f, float:-2.71761093E12)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x252d
            r1 = -455127526(0xffffffffe4df4e1a, float:-3.2954014E22)
            r0 = 1048005403(0x3e774b1b, float:0.24149744)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x252d
            r1 = -520707170(0xffffffffe0f6a39e, float:-1.4217778E20)
            r0 = 1689363630(0x64b1a4ae, float:2.621552E22)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            if (r3 == 0) goto L_0x252d
            r1 = 96784904(0x5c4d208, float:1.8508905E-35)
            r0 = -1654377677(0xffffffff9d643333, float:-3.0202018E-21)
            com.facebook.graphservice.tree.TreeJNI r7 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r7 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r7
            r0 = -934426595(0xffffffffc84dc81d, float:-210720.45)
            com.google.common.collect.ImmutableList r6 = r3.A0K(r0)
            X.2mq r0 = r8.A00
            X.2ms r5 = r0.A0A
            java.util.Date r0 = r8.A02
            long r2 = r0.getTime()
            r4 = 0
            if (r7 != 0) goto L_0x2516
            r0 = r4
        L_0x2500:
            r5.A03(r2, r6, r0)
            if (r7 == 0) goto L_0x2522
            X.2mv r2 = r8.A01
            r0 = -1857640538(0xffffffff9146a7a6, float:-1.5671107E-28)
            java.lang.String r1 = r7.A0P(r0)
            java.lang.String r0 = r7.A3h()
            r2.A2T(r1, r0)
            return
        L_0x2516:
            r0 = 3059181(0x2eaded, float:4.286826E-39)
            int r0 = r7.getIntValue(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x2500
        L_0x2522:
            X.2mv r0 = r8.A01
            r0.A2T(r4, r4)
            X.2mv r0 = r8.A01
            r0.A2S(r6)
            return
        L_0x252d:
            X.2mq r0 = r8.A00
            X.C54692mq.A07(r0)
            return
        L_0x2533:
            X.1DX r2 = (X.AnonymousClass1DX) r2
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            if (r0 == 0) goto L_0x2641
            java.lang.Object r3 = r0.A03
            if (r3 == 0) goto L_0x2641
            X.2mw r0 = r2.A00
            android.content.Context r0 = r0.A1j()
            if (r0 == 0) goto L_0x2641
            X.2mw r1 = r2.A00
            r0 = 0
            X.C54752mw.A04(r1, r0)
            X.2mw r0 = r2.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            r0.A01 = r3
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r3.A1q()
            if (r0 == 0) goto L_0x258f
            X.2mw r0 = r2.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A1q()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A23()
            if (r0 == 0) goto L_0x258f
            X.2mw r0 = r2.A00
            X.3NF r3 = r0.A04
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A1q()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A23()
            java.lang.String r5 = r0.A3m()
            X.2mw r0 = r2.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A1q()
            java.lang.String r7 = r0.A3m()
            X.2mw r0 = r2.A00
            java.lang.String r6 = r0.A08
            java.lang.String r4 = "booking_consumer_enter_view_detail"
            r8 = 0
            r9 = 0
            r10 = 0
            X.AnonymousClass3NF.A02(r3, r4, r5, r6, r7, r8, r9, r10)
        L_0x258f:
            X.2mw r0 = r2.A00
            X.2fe r0 = r0.A03
            if (r0 == 0) goto L_0x25a6
            com.facebook.messaging.professionalservices.booking.activities.AppointmentActivity r3 = r0.A00
            r0 = 2131828660(0x7f111fb4, float:1.9290267E38)
            java.lang.String r1 = r3.getString(r0)
            X.C1P r0 = r3.A05
            com.google.common.base.Preconditions.checkNotNull(r1)
            r0.CCX(r1)
        L_0x25a6:
            X.0p4 r8 = new X.0p4
            X.2mw r0 = r2.A00
            android.content.Context r0 = r0.A1j()
            r8.<init>(r0)
            r4 = 3
            java.lang.String r3 = "appointmentDetail"
            java.lang.String r1 = "onCancelButtonClickListener"
            java.lang.String r0 = "onRecurringCancelEntireSeriesButtonClickListener"
            java.lang.String[] r7 = new java.lang.String[]{r3, r1, r0}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r4)
            X.2fj r5 = new X.2fj
            android.content.Context r0 = r8.A09
            r5.<init>(r0)
            X.0zR r0 = r8.A04
            if (r0 == 0) goto L_0x25d0
            java.lang.String r0 = r0.A06
            r5.A07 = r0
        L_0x25d0:
            r6.clear()
            X.2mw r4 = r2.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r4.A01
            r5.A02 = r1
            r0 = 0
            r6.set(r0)
            if (r1 == 0) goto L_0x263f
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r1.A1q()
            if (r0 == 0) goto L_0x263f
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r4.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r0.A1q()
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r1 = com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            r0 = -1833804136(0xffffffff92b25e98, float:-1.1256698E-27)
            java.lang.Enum r1 = r3.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r1 = (com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus) r1
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r0 = com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus.CONFIRMED
            if (r1 != r0) goto L_0x263f
            X.2mx r0 = new X.2mx
            r0.<init>(r4)
        L_0x25ff:
            r5.A00 = r0
            r0 = 1
            r6.set(r0)
            X.2mw r4 = r2.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r4.A01
            if (r0 == 0) goto L_0x263d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A1q()
            if (r0 == 0) goto L_0x263d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r4.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r0.A1q()
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r1 = com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            r0 = -1833804136(0xffffffff92b25e98, float:-1.1256698E-27)
            java.lang.Enum r1 = r3.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r1 = (com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus) r1
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r0 = com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus.CONFIRMED
            if (r1 != r0) goto L_0x263d
            X.2my r0 = new X.2my
            r0.<init>(r4)
        L_0x262b:
            r5.A01 = r0
            r0 = 2
            r6.set(r0)
            r0 = 3
            X.AnonymousClass11F.A0C(r0, r6, r7)
            X.2mw r0 = r2.A00
            com.facebook.litho.LithoView r0 = r0.A02
            r0.A0a(r5)
            return
        L_0x263d:
            r0 = 0
            goto L_0x262b
        L_0x263f:
            r0 = 0
            goto L_0x25ff
        L_0x2641:
            X.2mw r0 = r2.A00
            X.3Ph r2 = r0.A06
            X.2fR r1 = new X.2fR
            r0 = 2131825404(0x7f1112fc, float:1.9283663E38)
            r1.<init>(r0)
            r2.A04(r1)
            return
        L_0x2651:
            X.1DU r2 = (X.AnonymousClass1DU) r2
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r1 = r2.A00
            r0 = 1
            r1.setResult(r0)
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r0 = r2.A00
            X.AnonymousClass8VS.A00(r0)
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r0 = r2.A00
            goto L_0x2686
        L_0x2661:
            X.1DT r2 = (X.AnonymousClass1DT) r2
            X.2fw r0 = r2.A00
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r1 = r0.A00
            r0 = 1
            r1.setResult(r0)
            X.2fw r0 = r2.A00
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r0 = r0.A00
            X.AnonymousClass8VS.A00(r0)
            X.2fw r0 = r2.A00
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r0 = r0.A00
            goto L_0x2686
        L_0x2677:
            X.1DV r2 = (X.AnonymousClass1DV) r2
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r0 = r2.A00
            X.AnonymousClass8VS.A00(r0)
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r1 = r2.A00
            r0 = 1
            r1.setResult(r0)
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r0 = r2.A00
        L_0x2686:
            r0.finish()
            return
        L_0x268a:
            r4 = r2
            X.1DS r4 = (X.AnonymousClass1DS) r4
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            java.lang.Object r3 = r0.A03
            if (r3 == 0) goto L_0x26c3
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 3386882(0x33ae02, float:4.746033E-39)
            r0 = 1925409559(0x72c36b17, float:7.741317E30)
            com.facebook.graphservice.tree.TreeJNI r2 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            if (r2 == 0) goto L_0x26c3
            com.facebook.graphql.enums.GraphQLServicesAppointmentMessagingOptionType r1 = com.facebook.graphql.enums.GraphQLServicesAppointmentMessagingOptionType.A02
            r0 = 81273360(0x4d82210, float:5.0812637E-36)
            java.lang.Enum r1 = r2.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLServicesAppointmentMessagingOptionType r1 = (com.facebook.graphql.enums.GraphQLServicesAppointmentMessagingOptionType) r1
            if (r1 == 0) goto L_0x26c3
            com.facebook.graphql.enums.GraphQLServicesAppointmentMessagingOptionType r0 = com.facebook.graphql.enums.GraphQLServicesAppointmentMessagingOptionType.A01
            if (r1 != r0) goto L_0x26c3
            r0 = -1711529327(0xffffffff99fc2291, float:-2.607015E-23)
            java.lang.String r2 = r2.A0P(r0)
            boolean r0 = X.C06850cB.A0B(r2)
            if (r0 == 0) goto L_0x26c4
        L_0x26c3:
            r2 = 0
        L_0x26c4:
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity r1 = r4.A00
            java.lang.String r0 = r4.A01
            com.facebook.pages.common.requesttime.shared.cancelappointment.RejectAppointmentActivity.A01(r1, r0, r2)
            return
        L_0x26cc:
            r4 = r2
            X.1DQ r4 = (X.AnonymousClass1DQ) r4
            X.2mz r0 = r4.A00
            X.BpH r3 = r0.A05
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r4.A01
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r0 = r0.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A00
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A0F
            java.lang.String r0 = "payflows_success"
            r3.A04(r2, r1, r0)
            com.facebook.payments.checkout.model.SimpleCheckoutData r2 = r4.A01
            com.facebook.payments.checkout.model.CheckoutCommonParams r0 = r2.A02()
            java.lang.String r1 = r0.AwR()
            X.2es r0 = new X.2es
            r0.<init>(r1)
            com.facebook.payments.checkout.model.SimpleSendPaymentCheckoutResult r1 = new com.facebook.payments.checkout.model.SimpleSendPaymentCheckoutResult
            r1.<init>(r0)
            X.2mz r0 = r4.A00
            r2.A02()
            goto L_0x2783
        L_0x26fd:
            r4 = r2
            X.1DR r4 = (X.AnonymousClass1DR) r4
            com.facebook.payments.checkout.protocol.model.CheckoutChargeResult r0 = (com.facebook.payments.checkout.protocol.model.CheckoutChargeResult) r0
            X.2mz r1 = r4.A00
            X.BpH r5 = r1.A05
            com.facebook.payments.checkout.model.SimpleCheckoutData r1 = r4.A02
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r1 = r1.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r1.A00
            r1 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r1)
            java.lang.String r1 = "async"
            r5.A09(r3, r1, r2)
            X.2mz r1 = r4.A00
            X.BpH r5 = r1.A05
            com.facebook.payments.checkout.model.SimpleCheckoutData r1 = r4.A02
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r1 = r1.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r1.A00
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A1M
            java.lang.String r1 = "payflows_success"
            r5.A04(r3, r2, r1)
            java.lang.String r1 = r0.A01
            X.2es r3 = new X.2es
            r3.<init>(r1)
            com.fasterxml.jackson.databind.JsonNode r5 = r0.A00
            r3.A01 = r5
            if (r5 == 0) goto L_0x2789
            java.lang.String r1 = "order_id"
            boolean r0 = r5.hasNonNull(r1)
            if (r0 == 0) goto L_0x2789
            X.2n0 r2 = new X.2n0
            r2.<init>()
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r1)
            java.lang.String r0 = r0.asText()
            r2.A05 = r0
            java.lang.String r1 = "message_with_email"
            boolean r0 = r5.hasNonNull(r1)
            if (r0 == 0) goto L_0x2761
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r1)
            java.lang.String r0 = r0.asText()
            r2.A03 = r0
        L_0x2761:
            java.lang.String r1 = "receipt_url"
            boolean r0 = r5.hasNonNull(r1)
            if (r0 == 0) goto L_0x2773
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r1)
            java.lang.String r0 = r0.asText()
            r2.A07 = r0
        L_0x2773:
            com.facebook.payments.checkout.model.PaymentsOrderDetails r0 = new com.facebook.payments.checkout.model.PaymentsOrderDetails
            r0.<init>(r2)
        L_0x2778:
            if (r0 == 0) goto L_0x277c
            r3.A00 = r0
        L_0x277c:
            X.2mz r0 = r4.A00
            com.facebook.payments.checkout.model.SimpleSendPaymentCheckoutResult r1 = new com.facebook.payments.checkout.model.SimpleSendPaymentCheckoutResult
            r1.<init>(r3)
        L_0x2783:
            X.C8I r0 = r0.A00
            r0.A01(r1)
            return
        L_0x2789:
            r0 = 0
            goto L_0x2778
        L_0x278b:
            r4 = r2
            X.1DN r4 = (X.AnonymousClass1DN) r4
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -847737601(0xffffffffcd788cff, float:-2.60624368E8)
            r1 = -106641904(0xfffffffff9a4c610, float:-1.0694423E35)
            com.facebook.graphservice.tree.TreeJNI r5 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            if (r5 == 0) goto L_0x27c2
            com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum r1 = com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum.A03
            r0 = 737873220(0x2bfb0d44, float:1.7838305E-12)
            java.lang.Enum r1 = r5.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum r1 = (com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum) r1
            if (r1 == 0) goto L_0x27c2
            X.2mz r0 = r4.A00
            X.BpH r3 = r0.A05
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r4.A01
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r0 = r0.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A00
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "app_switch_invoice_status"
            r3.A09(r2, r0, r1)
        L_0x27c2:
            com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum r1 = com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum.A03
            r0 = 737873220(0x2bfb0d44, float:1.7838305E-12)
            java.lang.Enum r1 = r5.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum r1 = (com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum) r1
            com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum r0 = com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum.A01
            if (r1 == r0) goto L_0x280b
            X.2mz r0 = r4.A00
            X.BpH r3 = r0.A05
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r4.A01
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r0 = r0.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A00
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A0A
            java.lang.String r0 = "payflows_fail"
            r3.A04(r2, r1, r0)
            X.2mz r2 = r4.A00
            android.content.Context r0 = r2.A03
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131823779(0x7f110ca3, float:1.9280367E38)
            r1.getString(r0)
            X.2mz r0 = r4.A00
            android.content.Context r0 = r0.A03
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131821488(0x7f1103b0, float:1.927572E38)
            r1.getString(r0)
            r1 = 0
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r4.A01
            com.facebook.payments.checkout.model.CheckoutCommonParams r0 = r0.A02()
            X.C54782mz.A03(r2, r1, r0)
            return
        L_0x280b:
            X.2mz r0 = r4.A00
            X.BpH r3 = r0.A05
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r4.A01
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r0 = r0.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A00
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A0A
            java.lang.String r0 = "payflows_success"
            r3.A04(r2, r1, r0)
            com.facebook.payments.checkout.model.SimpleCheckoutData r2 = r4.A01
            com.facebook.payments.checkout.model.CheckoutCommonParams r0 = r2.A02()
            java.lang.String r1 = r0.AwR()
            X.2es r0 = new X.2es
            r0.<init>(r1)
            com.facebook.payments.checkout.model.SimpleSendPaymentCheckoutResult r1 = new com.facebook.payments.checkout.model.SimpleSendPaymentCheckoutResult
            r1.<init>(r0)
            X.2mz r0 = r4.A00
            r2.A02()
            X.C8I r0 = r0.A00
            r0.A01(r1)
            return
        L_0x283d:
            r1 = r2
            X.1DM r1 = (X.AnonymousClass1DM) r1
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            X.2n1 r5 = r1.A00
            com.google.common.base.Preconditions.checkNotNull(r0)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 631269566(0x25a068be, float:2.7826552E-16)
            r1 = 480232028(0x1c9fc25c, float:1.0571978E-21)
            com.facebook.graphservice.tree.TreeJNI r4 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            com.facebook.graphql.enums.GraphQLPayTransactionStatus r2 = com.facebook.graphql.enums.GraphQLPayTransactionStatus.A03
            r1 = -844339125(0xffffffffcdac684b, float:-3.61564512E8)
            java.lang.Enum r2 = r4.A0O(r1, r2)
            com.facebook.graphql.enums.GraphQLPayTransactionStatus r2 = (com.facebook.graphql.enums.GraphQLPayTransactionStatus) r2
            com.facebook.graphql.enums.GraphQLPayTransactionStatus r1 = com.facebook.graphql.enums.GraphQLPayTransactionStatus.A01
            if (r2 != r1) goto L_0x2892
            X.2n2 r0 = r5.A00
            X.BpH r3 = r0.A0G
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r0.A04
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r0 = r0.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A00
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A1L
            java.lang.String r0 = "payflows_success"
            r3.A04(r2, r1, r0)
            if (r4 == 0) goto L_0x288c
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r4.A2k()
            if (r3 == 0) goto L_0x288c
            X.2n2 r0 = r5.A00
            X.C9C r2 = r0.A02
            com.facebook.payments.checkout.model.SimpleCheckoutData r1 = r0.A04
            com.facebook.payments.checkout.model.SendPaymentCheckoutResult r0 = X.AnonymousClass2n2.A01(r0, r3)
            r2.A07(r1, r0)
        L_0x288c:
            X.2n2 r0 = r5.A00
            r0.A0A()
            return
        L_0x2892:
            com.facebook.graphql.enums.GraphQLPayTransactionStatus r1 = com.facebook.graphql.enums.GraphQLPayTransactionStatus.A02
            if (r2 != r1) goto L_0x293a
            X.2n2 r1 = r5.A00
            X.BpH r4 = r1.A0G
            com.facebook.payments.checkout.model.SimpleCheckoutData r1 = r1.A04
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r1 = r1.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r1.A00
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A1L
            java.lang.String r1 = "payflows_custom"
            r4.A04(r3, r2, r1)
            X.2n2 r3 = r5.A00
            com.facebook.payments.checkout.model.SimpleCheckoutData r1 = r3.A04
            com.facebook.payments.checkout.model.CheckoutCommonParams r1 = r1.A02()
            boolean r1 = r1.CEn()
            if (r1 == 0) goto L_0x2921
            X.2n3 r1 = r3.A0E
            X.1YI r4 = r1.A01
            r2 = 331(0x14b, float:4.64E-43)
            r1 = 0
            boolean r1 = r4.AbO(r2, r1)
            if (r1 == 0) goto L_0x2921
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r4 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 631269566(0x25a068be, float:2.7826552E-16)
            r1 = 480232028(0x1c9fc25c, float:1.0571978E-21)
            com.facebook.graphservice.tree.TreeJNI r0 = r0.A0J(r2, r4, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = r0.A2k()
            com.google.common.base.Preconditions.checkNotNull(r4)
            X.C9j r1 = r3.A09
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r3.A04
            com.facebook.payments.checkout.model.CheckoutParams r0 = r0.A09
            com.facebook.payments.checkout.model.CheckoutCommonParams r0 = r0.Agq()
            X.C3W r0 = r0.Agz()
            X.C3e r2 = r1.A05(r0)
            com.facebook.payments.checkout.model.SimpleCheckoutData r1 = r3.A04
            com.facebook.payments.checkout.model.SendPaymentCheckoutResult r0 = X.AnonymousClass2n2.A01(r3, r4)
            com.facebook.payments.confirmation.ConfirmationCommonParams r4 = r2.Aao(r1, r0)
            android.content.Context r2 = r3.A06
            com.google.common.base.Preconditions.checkNotNull(r2)
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.facebook.payments.confirmation.ConfirmationActivity> r0 = com.facebook.payments.confirmation.ConfirmationActivity.class
            r1.<init>(r2, r0)
            r0 = 68
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r1.putExtra(r0, r4)
            r0 = 33554432(0x2000000, float:9.403955E-38)
            r1.addFlags(r0)
            X.Bzl r0 = r3.A05
            r0.CGq(r1)
        L_0x2914:
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.<init>(r0)
            X.Bzl r0 = r3.A05
            r0.Bwx(r1)
            return
        L_0x2921:
            com.facebook.payments.checkout.model.SimpleCheckoutData r1 = r3.A04
            com.facebook.payments.checkout.model.CheckoutCommonParams r0 = r1.A02()
            android.content.Intent r2 = r0.B4c()
            if (r2 == 0) goto L_0x2914
            com.facebook.payments.checkout.model.SendPaymentCheckoutResult r1 = r1.A0C
            java.lang.String r0 = "com.facebook.payments.checkout.simpleCheckoutSenderResultExtra"
            r2.putExtra(r0, r1)
            android.content.Context r0 = r3.A06
            r0.sendBroadcast(r2)
            goto L_0x2914
        L_0x293a:
            X.2n2 r0 = r5.A00
            X.BpH r3 = r0.A0G
            com.facebook.payments.checkout.model.SimpleCheckoutData r0 = r0.A04
            com.facebook.payments.checkout.model.CheckoutAnalyticsParams r0 = r0.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A00
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A1L
            java.lang.String r0 = "payflows_fail"
            r3.A04(r2, r1, r0)
            X.2n2 r0 = r5.A00
            r0.A09()
            return
        L_0x2953:
            r3 = r2
            X.1DL r3 = (X.AnonymousClass1DL) r3
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            X.2ly r1 = r3.A00
            X.C54162ly.A04(r1, r0)
            X.2ly r2 = r3.A00
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A1O
            java.lang.String r0 = "payflows_success"
            X.C54162ly.A05(r2, r1, r0)
            X.2ly r2 = r3.A00
            X.2m1 r0 = r2.A03
            long r0 = r0.A00
            X.C54162ly.A03(r2, r0)
            return
        L_0x2970:
            r3 = r2
            X.1DK r3 = (X.AnonymousClass1DK) r3
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            X.2ly r1 = r3.A00
            java.util.concurrent.Executor r2 = r1.A0F
            X.2n4 r1 = new X.2n4
            r1.<init>(r3, r0)
            r0 = -73351000(0xfffffffffba0c0a8, float:-1.66935E36)
            X.AnonymousClass07A.A04(r2, r1, r0)
            return
        L_0x2985:
            r1 = r2
            X.1DH r1 = (X.AnonymousClass1DH) r1
            X.2m5 r0 = r1.A01
            X.2m6 r0 = r0.A03
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r1 = r1.A00
            goto L_0x2998
        L_0x298f:
            r1 = r2
            X.1DI r1 = (X.AnonymousClass1DI) r1
            X.2m3 r0 = r1.A01
            X.2m6 r0 = r0.A03
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r1 = r1.A00
        L_0x2998:
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 1
            r0.A01(r1, r2, r3, r4, r5)
            return
        L_0x29a0:
            X.2m5 r0 = r3.A01
            X.2m6 r4 = r0.A03
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r5 = r3.A00
            com.facebook.payments.contactinfo.model.ContactInfoFormInput r6 = r3.A02
            com.facebook.payments.contactinfo.model.ContactInfo r0 = r5.A01
            java.lang.String r7 = r0.getId()
            boolean r8 = r3.A03
            r9 = 0
            r4.A01(r5, r6, r7, r8, r9)
            return
        L_0x29b5:
            r1 = r2
            X.1DE r1 = (X.AnonymousClass1DE) r1
            com.facebook.payments.contactinfo.protocol.model.ContactInfoProtocolResult r0 = (com.facebook.payments.contactinfo.protocol.model.ContactInfoProtocolResult) r0
            X.2m6 r2 = r1.A01
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r3 = r1.A00
            com.facebook.payments.contactinfo.model.ContactInfoFormInput r4 = r1.A02
            goto L_0x29cc
        L_0x29c1:
            r1 = r2
            X.1DF r1 = (X.AnonymousClass1DF) r1
            com.facebook.payments.contactinfo.protocol.model.ContactInfoProtocolResult r0 = (com.facebook.payments.contactinfo.protocol.model.ContactInfoProtocolResult) r0
            X.2m6 r2 = r1.A01
            com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams r3 = r1.A00
            com.facebook.payments.contactinfo.model.ContactInfoFormInput r4 = r1.A02
        L_0x29cc:
            java.lang.String r5 = r0.mContactInfoId
            r6 = 0
            r7 = 0
            r2.A01(r3, r4, r5, r6, r7)
            return
        L_0x29d4:
            X.1DD r2 = (X.AnonymousClass1DD) r2
            com.facebook.payments.history.model.SimplePaymentTransactions r0 = (com.facebook.payments.history.model.SimplePaymentTransactions) r0
            X.2n5 r1 = r2.A02
            X.Bq9 r1 = r1.A00
            r1.A00()
            X.Bka r5 = r2.A03
            X.2n6 r4 = new X.2n6
            r4.<init>()
            com.facebook.payments.history.model.SimplePaymentTransactions r1 = r2.A01
            if (r1 == 0) goto L_0x2a0f
            com.google.common.collect.ImmutableList$Builder r3 = new com.google.common.collect.ImmutableList$Builder
            r3.<init>()
            com.google.common.collect.ImmutableList r1 = r1.A01
            r3.addAll(r1)
            com.google.common.collect.ImmutableList r1 = r0.A01
            r3.addAll(r1)
            com.facebook.payments.history.model.PaymentHistoryPageInfo r2 = new com.facebook.payments.history.model.PaymentHistoryPageInfo
            com.facebook.payments.history.model.PaymentHistoryPageInfo r0 = r0.A00
            if (r0 != 0) goto L_0x2a1d
            r1 = 0
        L_0x2a00:
            if (r0 != 0) goto L_0x2a1a
            r0 = 0
        L_0x2a03:
            r2.<init>(r1, r0)
            com.facebook.payments.history.model.SimplePaymentTransactions r0 = new com.facebook.payments.history.model.SimplePaymentTransactions
            com.google.common.collect.ImmutableList r1 = r3.build()
            r0.<init>(r1, r2)
        L_0x2a0f:
            r4.A00 = r0
            com.facebook.payments.history.picker.PaymentHistoryCoreClientData r0 = new com.facebook.payments.history.picker.PaymentHistoryCoreClientData
            r0.<init>(r4)
            r5.BVU(r0)
            return
        L_0x2a1a:
            java.lang.Integer r0 = r0.A00
            goto L_0x2a03
        L_0x2a1d:
            boolean r1 = r0.A01
            goto L_0x2a00
        L_0x2a20:
            r1 = r2
            X.1DB r1 = (X.AnonymousClass1DB) r1
            com.facebook.payments.invoice.protocol.InvoiceConfigResult r0 = (com.facebook.payments.invoice.protocol.InvoiceConfigResult) r0
            X.2eS r1 = r1.A00
            r1.A00 = r0
            X.C50692eS.A01(r1)
            return
        L_0x2a2d:
            r1 = r2
            X.1DA r1 = (X.AnonymousClass1DA) r1
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            if (r0 != 0) goto L_0x2a3c
            java.lang.Class r1 = X.C54842n7.A06
            java.lang.String r0 = "Failed to fetch P2P post actions"
            X.C010708t.A05(r1, r0)
            return
        L_0x2a3c:
            X.2n7 r1 = r1.A00
            r1.A01 = r0
            return
        L_0x2a41:
            r3 = r2
            X.1D9 r3 = (X.AnonymousClass1D9) r3
            X.2n9 r4 = r3.A00
            com.facebook.payments.paymentmethods.cardform.CardFormParams r1 = r3.A03
            X.2eW r2 = r3.A02
            X.Bzl r0 = r4.A00
            if (r0 == 0) goto L_0x2a91
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r0 = r1.Agg()
            com.facebook.payments.paymentmethods.model.FbPaymentCard r1 = r0.fbPaymentCard
            com.facebook.payments.p2p.model.PaymentCard r1 = (com.facebook.payments.p2p.model.PaymentCard) r1
            com.facebook.payments.p2p.model.PartialPaymentCard r5 = new com.facebook.payments.p2p.model.PartialPaymentCard
            java.lang.String r6 = r1.A04
            java.lang.String r7 = r1.Arx()
            int r8 = r2.A00
            int r0 = r2.A01
            int r9 = r0 + 2000
            com.facebook.payments.p2p.model.Address r10 = new com.facebook.payments.p2p.model.Address
            java.lang.String r0 = r2.A07
            r10.<init>(r0)
            java.lang.String r11 = r1.A03
            r12 = 1
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.String r0 = "partial_payment_card"
            r1.putExtra(r0, r5)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.lang.String r0 = "extra_activity_result_data"
            r2.putParcelable(r0, r1)
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.<init>(r0, r2)
            X.Bzl r0 = r4.A00
            r0.Bwx(r1)
        L_0x2a91:
            X.2n9 r0 = r3.A00
            X.2mC r4 = r0.A05
            com.facebook.payments.p2p.model.PaymentCard r0 = r3.A01
            java.lang.String r2 = r0.getId()
            X.2eW r0 = r3.A02
            java.lang.String r1 = r0.A09
            java.util.Map r0 = r4.A00
            r0.put(r2, r1)
            return
        L_0x2aa5:
            X.1D7 r2 = (X.AnonymousClass1D7) r2
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            X.2nA r1 = r2.A00
            X.2nD r1 = r1.A00
            X.AnonymousClass064.A00(r1)
            r1 = -2131617846(0xffffffff80f217ca, float:-2.2232724E-38)
            java.lang.String r3 = r0.A0P(r1)
            if (r3 == 0) goto L_0x2b25
            r1 = -198041123(0xfffffffff43221dd, float:-5.6452372E31)
            java.lang.String r1 = r0.A0P(r1)
            if (r1 == 0) goto L_0x2b25
            X.2nA r0 = r2.A00
            r0.A02 = r1
            X.2nD r4 = r0.A00
            java.lang.String r2 = "fb-messenger://payments/paypal_close/"
            X.2nB r1 = new X.2nB
            r1.<init>()
            r1.A03 = r3
            r0 = 41
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            X.C28931fb.A06(r3, r0)
            r1.A05 = r2
            r0 = 45
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            X.C28931fb.A06(r2, r0)
            r0 = 0
            r1.A06 = r0
            com.facebook.payments.webview.model.PaymentsWebViewOnlinePaymentParams r0 = new com.facebook.payments.webview.model.PaymentsWebViewOnlinePaymentParams
            r0.<init>(r1)
            X.2nC r2 = new X.2nC
            r2.<init>()
            r2.A02(r0)
            com.facebook.payments.logging.PaymentsFlowName r0 = com.facebook.payments.logging.PaymentsFlowName.A09
            X.BsN r1 = com.facebook.payments.logging.PaymentsLoggingSessionData.A00(r0)
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = new com.facebook.payments.logging.PaymentsLoggingSessionData
            r0.<init>(r1)
            r2.A00(r0)
            com.facebook.payments.model.PaymentItemType r0 = com.facebook.payments.model.PaymentItemType.A0C
            r2.A01(r0)
            com.facebook.payments.webview.model.PaymentsWebViewParams r1 = new com.facebook.payments.webview.model.PaymentsWebViewParams
            r1.<init>(r2)
            X.2nE r0 = r4.A00
            android.content.Context r0 = r0.A00
            android.content.Intent r3 = com.facebook.payments.webview.PaymentsWebViewActivity.A00(r0, r1)
            X.0Dj r0 = X.C02200Dj.A00()
            X.15w r2 = r0.A0G()
            X.2nE r1 = r4.A00
            r0 = 8512(0x2140, float:1.1928E-41)
            r2.A0A(r3, r0, r1)
            return
        L_0x2b25:
            X.2nA r1 = r2.A00
            X.2nD r1 = r1.A00
            X.2nE r1 = r1.A00
            X.Bq9 r1 = r1.A05
            r1.A00()
            X.2nA r1 = r2.A00
            X.2nD r4 = r1.A00
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 2020975452(0x7875a35c, float:1.9928532E34)
            r1 = 882067735(0x34934917, float:2.7434064E-7)
            com.google.common.collect.ImmutableList r2 = r0.A0M(r2, r3, r1)
            X.2nE r0 = r4.A00
            X.Bm0 r1 = r0.A01
            r0 = 0
            r1.setNotifyOnChange(r0)
            X.2nE r0 = r4.A00
            X.Bm0 r0 = r0.A01
            r0.clear()
            X.2nE r0 = r4.A00
            X.Bm0 r0 = r0.A01
            r0.addAll(r2)
            X.2nE r0 = r4.A00
            X.Bm0 r1 = r0.A01
            r0 = -1296155000(0xffffffffb2be3e88, float:-2.2147347E-8)
            X.AnonymousClass0QL.A00(r1, r0)
            return
        L_0x2b61:
            X.1D6 r2 = (X.AnonymousClass1D6) r2
            com.facebook.payments.p2p.phases.PaymentPhaseWrapper r0 = (com.facebook.payments.p2p.phases.PaymentPhaseWrapper) r0
            if (r0 != 0) goto L_0x2b70
            X.2nI r0 = r2.A00
            X.Byp r1 = r0.A01
            r0 = 0
            r1.BdW(r0)
            return
        L_0x2b70:
            X.2nI r1 = r2.A00
            r1.A02 = r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A00
            com.google.common.collect.ImmutableList r0 = r0.A3c()
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x2b94
            X.2nI r0 = r2.A00
            X.2nL r3 = r0.A05
            X.2nJ r2 = new X.2nJ
            r2.<init>(r0)
            X.1D3 r1 = new X.1D3
            r1.<init>(r0)
            java.lang.String r0 = "phase_end_task_key"
            r3.A03(r0, r2, r1)
            return
        L_0x2b94:
            X.2nI r2 = r2.A00
            r1 = 0
            r0 = 1
            X.C54952nI.A02(r2, r1, r0)
            return
        L_0x2b9c:
            r5 = r2
            X.1D3 r5 = (X.AnonymousClass1D3) r5
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            boolean r1 = r0.isEmpty()
            if (r1 == 0) goto L_0x2bb0
            X.2nI r0 = r5.A00
            X.Byp r1 = r0.A01
            r0 = 0
            r1.BiS(r0)
            return
        L_0x2bb0:
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>()
            r1 = 0
            java.lang.Object r1 = r0.get(r1)
            com.facebook.payments.p2p.phases.PaymentPhaseWrapper r1 = (com.facebook.payments.p2p.phases.PaymentPhaseWrapper) r1
            if (r1 == 0) goto L_0x2bf2
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r1.A00
            if (r3 == 0) goto L_0x2bf2
            com.facebook.graphql.enums.GraphQLPaymentPhaseType r2 = com.facebook.graphql.enums.GraphQLPaymentPhaseType.A04
            r1 = -310905986(0xffffffffed77f37e, float:-4.7960726E27)
            java.lang.Enum r1 = r3.A0O(r1, r2)
            com.facebook.graphql.enums.GraphQLPaymentPhaseType r1 = (com.facebook.graphql.enums.GraphQLPaymentPhaseType) r1
            if (r1 == 0) goto L_0x2bf2
            int r2 = r1.ordinal()
            r1 = 3
            if (r2 != r1) goto L_0x2bf2
            X.2nK r2 = X.C54972nK.A02
        L_0x2bd8:
            r1 = 306(0x132, float:4.29E-43)
            java.lang.String r1 = X.C22298Ase.$const$string(r1)
            r4.putSerializable(r1, r2)
            r1 = 308(0x134, float:4.32E-43)
            java.lang.String r1 = X.C22298Ase.$const$string(r1)
            X.AnonymousClass324.A0A(r4, r1, r0)
            X.2nI r0 = r5.A00
            X.Byp r0 = r0.A01
            r0.BiS(r4)
            return
        L_0x2bf2:
            X.2nK r2 = X.C54972nK.A01
            goto L_0x2bd8
        L_0x2bf5:
            r3 = r2
            X.1D1 r3 = (X.AnonymousClass1D1) r3
            X.2nL r2 = r3.A00
            boolean r1 = r2.A01
            if (r1 == 0) goto L_0x2c0c
            java.util.LinkedList r4 = r2.A00
            X.0Wg r3 = r3.A01
            X.2gM r2 = new X.2gM
            r1 = 0
            r2.<init>(r3, r0, r1)
            r4.add(r2)
            return
        L_0x2c0c:
            X.0Wg r1 = r3.A01
            r1.Bqf(r0)
            return
        L_0x2c12:
            r0 = r2
            X.1D0 r0 = (X.AnonymousClass1D0) r0
            X.2eX r1 = r0.A01
            com.facebook.payments.paymentmethods.cardform.CardFormParams r0 = r0.A00
            goto L_0x2f42
        L_0x2c1b:
            r1 = -1203717159(0xffffffffb840bbd9, float:-4.5951314E-5)
            com.facebook.graphservice.tree.TreeJNI r3 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -303793002(0xffffffffede47c96, float:-8.8391497E27)
            r0 = 744240956(0x2c5c373c, float:3.1294542E-12)
            com.facebook.graphservice.tree.TreeJNI r1 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            com.google.common.base.Preconditions.checkNotNull(r1)
            r0 = -1485040125(0xffffffffa77c1603, float:-3.4983958E-15)
            java.lang.String r3 = r1.A0P(r0)
            com.google.common.base.Preconditions.checkNotNull(r3)
            X.2eX r2 = r4.A02
            com.facebook.payments.paymentmethods.cardform.CardFormParams r0 = r4.A01
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r0 = r0.Agg()
            com.facebook.payments.paymentmethods.cardform.CardFormAnalyticsParams r1 = r0.cardFormAnalyticsParams
            X.2eW r0 = r4.A00
            java.lang.String r0 = r0.A09
            X.C50742eX.A03(r2, r1, r3, r0)
            return
        L_0x2c51:
            r1 = 1324889998(0x4ef8378e, float:2.0821952E9)
            com.facebook.graphservice.tree.TreeJNI r3 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -303793002(0xffffffffede47c96, float:-8.8391497E27)
            r0 = 1784199743(0x6a58ba3f, float:6.5501874E25)
            com.facebook.graphservice.tree.TreeJNI r1 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            com.google.common.base.Preconditions.checkNotNull(r1)
            r0 = -1485040125(0xffffffffa77c1603, float:-3.4983958E-15)
            java.lang.String r3 = r1.A0P(r0)
            com.google.common.base.Preconditions.checkNotNull(r3)
            X.2eX r2 = r4.A02
            com.facebook.payments.paymentmethods.cardform.CardFormParams r0 = r4.A01
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r0 = r0.Agg()
            com.facebook.payments.paymentmethods.cardform.CardFormAnalyticsParams r1 = r0.cardFormAnalyticsParams
            X.2eW r0 = r4.A00
            java.lang.String r0 = r0.A09
            X.C50742eX.A03(r2, r1, r3, r0)
            return
        L_0x2c87:
            r4 = r2
            X.1Cz r4 = (X.C20571Cz) r4
            com.facebook.payments.paymentmethods.model.PaymentMethodsInfo r0 = (com.facebook.payments.paymentmethods.model.PaymentMethodsInfo) r0
            X.2nM r1 = r4.A00
            X.Bq9 r1 = r1.A00
            r1.A00()
            X.Bka r3 = r4.A02
            X.2nN r2 = new X.2nN
            r2.<init>()
            r2.A00 = r0
            com.facebook.payments.paymentmethods.picker.model.PaymentMethodsPickerRunTimeData r0 = r4.A01
            com.facebook.payments.picker.model.CoreClientData r1 = r0.A00
            r0 = 0
            if (r1 == 0) goto L_0x2ca7
            com.facebook.payments.paymentmethods.picker.model.PaymentMethodsCoreClientData r1 = (com.facebook.payments.paymentmethods.picker.model.PaymentMethodsCoreClientData) r1
            com.facebook.payments.picker.model.ProductCoreClientData r0 = r1.A01
        L_0x2ca7:
            r2.A01 = r0
            com.facebook.payments.paymentmethods.picker.model.PaymentMethodsCoreClientData r0 = new com.facebook.payments.paymentmethods.picker.model.PaymentMethodsCoreClientData
            r0.<init>(r2)
            r3.BVU(r0)
            return
        L_0x2cb2:
            r1 = r2
            X.1Cx r1 = (X.C20551Cx) r1
            java.lang.String r0 = (java.lang.String) r0
            X.2g9 r3 = r1.A01
            com.facebook.payments.paymentmethods.picker.model.PaymentMethodsPickerRunTimeData r2 = r1.A02
            int r1 = r1.A00
            X.C51252g9.A01(r3, r2, r1, r0)
            return
        L_0x2cc1:
            r0 = r2
            X.1Cw r0 = (X.C20541Cw) r0
            X.2g9 r2 = r0.A00
            com.facebook.payments.paymentmethods.model.PayPalJwtToken r1 = new com.facebook.payments.paymentmethods.model.PayPalJwtToken
            java.lang.String r0 = r0.A01
            r1.<init>(r0)
            r2.A02(r1)
            return
        L_0x2cd1:
            r5 = r2
            X.1Cv r5 = (X.C20531Cv) r5
            java.lang.String r3 = r5.A01
            if (r3 == 0) goto L_0x2d59
            int r1 = r3.hashCode()
            r0 = 1620972716(0x609e14ac, float:9.112735E19)
            r2 = 1
            if (r1 == r0) goto L_0x2d4f
            r0 = 2119697090(0x7e5802c2, float:7.178189E37)
            if (r1 != r0) goto L_0x2cf0
            java.lang.String r0 = "MOR_FAN_FUNDING"
            boolean r0 = r3.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x2cf1
        L_0x2cf0:
            r1 = -1
        L_0x2cf1:
            if (r1 == 0) goto L_0x2cf6
            r0 = 0
            if (r1 != r2) goto L_0x2cf7
        L_0x2cf6:
            r0 = 1
        L_0x2cf7:
            if (r0 == 0) goto L_0x2d59
            X.2nO r0 = r5.A00
            X.2nT r4 = r0.A04
            android.content.Context r9 = r0.A02
            java.lang.String r3 = r3.toUpperCase()
            X.2nP r1 = new X.2nP
            r1.<init>(r5)
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r0 = "product"
            r2.put(r0, r3)
            X.2nQ r0 = new X.2nQ
            r0.<init>(r1)
            X.2nS r1 = new X.2nS
            r1.<init>()
            r1.A02 = r0
            X.2nU r6 = r4.A01
            r6.A00 = r1
            X.10T r4 = r4.A00
            java.lang.Integer r0 = X.AnonymousClass07B.A0b
            java.lang.String r0 = com.facebook.graphql.enums.GraphQLAllPaymentTypeFields.A00(r0)
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x2d3e
            java.lang.String r5 = "818234021883730"
        L_0x2d32:
            X.2nW r7 = new X.2nW
            r7.<init>(r2)
            java.lang.String r8 = ""
            r10 = 0
            X.AnonymousClass10T.A01(r4, r5, r6, r7, r8, r9, r10)
            return
        L_0x2d3e:
            java.lang.Integer r0 = X.AnonymousClass07B.A0X
            java.lang.String r0 = com.facebook.graphql.enums.GraphQLAllPaymentTypeFields.A00(r0)
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x2d4d
            java.lang.String r5 = "2378175749077391"
            goto L_0x2d32
        L_0x2d4d:
            r5 = 0
            goto L_0x2d32
        L_0x2d4f:
            java.lang.String r0 = "MOR_GROUP_SUBSCRIPTION"
            boolean r0 = r3.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x2cf1
            goto L_0x2cf0
        L_0x2d59:
            X.2nO r0 = r5.A00
            X.Bzl r2 = r0.A00
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.<init>(r0)
            r2.Bwx(r1)
            return
        L_0x2d68:
            r1 = r2
            X.1Cu r1 = (X.C20521Cu) r1
            com.facebook.payments.paymentmethods.model.PaymentMethodsInfo r0 = (com.facebook.payments.paymentmethods.model.PaymentMethodsInfo) r0
            X.2nX r1 = r1.A03
            r1.A04 = r0
            return
        L_0x2d72:
            r1 = r2
            X.1Ct r1 = (X.C20511Ct) r1
            com.facebook.payments.history.model.SimplePaymentTransactions r0 = (com.facebook.payments.history.model.SimplePaymentTransactions) r0
            X.2nX r1 = r1.A03
            r1.A03 = r0
            return
        L_0x2d7c:
            r1 = r2
            X.1Cs r1 = (X.AnonymousClass1Cs) r1
            com.facebook.payments.auth.pin.model.PaymentPin r0 = (com.facebook.payments.auth.pin.model.PaymentPin) r0
            X.2nX r1 = r1.A03
            r1.A01 = r0
            return
        L_0x2d86:
            r1 = r2
            X.1Cq r1 = (X.C20491Cq) r1
            com.facebook.payments.settings.protocol.GetPayAccountResult r0 = (com.facebook.payments.settings.protocol.GetPayAccountResult) r0
            X.2nX r1 = r1.A03
            com.facebook.payments.currency.CurrencyAmount r2 = r0.A01
            r1.A02 = r2
            int r0 = r0.A00
            r1.A00 = r0
            return
        L_0x2d96:
            r5 = r2
            X.1Cp r5 = (X.C20481Cp) r5
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            X.2nY r2 = r5.A02
            com.facebook.payments.settings.PaymentSettingsPickerRunTimeData r1 = r5.A01
            com.facebook.payments.picker.model.PickerScreenAnalyticsParams r1 = r1.A01()
            com.facebook.payments.logging.PaymentsLoggingSessionData r4 = r1.paymentsLoggingSessionData
            X.BpH r3 = r2.A04
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A1q
            java.lang.String r1 = "payflows_success"
            r3.A04(r4, r2, r1)
            X.2nX r1 = r5.A03
            com.google.common.base.Optional r0 = X.C22159Apl.A00(r0)
            r1.A05 = r0
            com.google.common.collect.ImmutableList r0 = r5.A04
            X.1Xv r1 = r0.iterator()
        L_0x2dbc:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x2dcf
            java.lang.Object r0 = r1.next()
            X.0Yh r0 = (X.C05270Yh) r0
            boolean r0 = r0.BEO()
            if (r0 == 0) goto L_0x2dbc
            return
        L_0x2dcf:
            X.2nY r3 = r5.A02
            X.Bka r2 = r5.A00
            X.2nX r1 = r5.A03
            X.3DS r0 = r3.A07
            boolean r0 = r0.A0A()
            r0 = r0 ^ 1
            com.google.common.base.Preconditions.checkArgument(r0)
            X.Bq9 r0 = r3.A00
            r0.A00()
            com.facebook.payments.settings.model.PaymentSettingsCoreClientData r0 = new com.facebook.payments.settings.model.PaymentSettingsCoreClientData
            r0.<init>(r1)
            r2.BVU(r0)
            return
        L_0x2dee:
            r5 = r2
            X.1Co r5 = (X.C20471Co) r5
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            X.2ea r2 = r5.A02
            com.facebook.payments.shipping.addresspicker.ShippingAddressPickerRunTimeData r1 = r5.A01
            com.facebook.payments.picker.model.PickerScreenConfig r1 = r1.A01
            X.BpH r4 = r2.A02
            com.facebook.payments.picker.model.PickerScreenCommonConfig r1 = r1.Ay1()
            com.facebook.payments.picker.model.PickerScreenAnalyticsParams r1 = r1.analyticsParams
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r1.paymentsLoggingSessionData
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A1q
            java.lang.String r1 = "payflows_success"
            r4.A04(r3, r2, r1)
            X.2ea r1 = r5.A02
            X.Bq9 r1 = r1.A00
            r1.A00()
            X.Bka r2 = r5.A00
            com.facebook.payments.shipping.addresspicker.ShippingCoreClientData r1 = new com.facebook.payments.shipping.addresspicker.ShippingCoreClientData
            r1.<init>(r0)
            r2.BVU(r1)
            return
        L_0x2e1c:
            r0 = r2
            X.1Cn r0 = (X.C20461Cn) r0
            X.2nZ r1 = r0.A02
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r0.A00
            com.facebook.payments.logging.PaymentsFlowStep r7 = com.facebook.payments.logging.PaymentsFlowStep.A0e
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 1
            X.C55122nZ.A02(r1, r2, r3, r4, r5, r6, r7)
            return
        L_0x2e2d:
            r5 = r2
            X.1Cm r5 = (X.C20451Cm) r5
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            r3 = 0
            if (r0 == 0) goto L_0x2e6c
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r0.A28()
            if (r1 != 0) goto L_0x2e6c
            int r2 = X.AnonymousClass1Y3.B46
            X.2nZ r1 = r5.A03
            X.0UN r1 = r1.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.BzI r2 = (X.C24411BzI) r2
            java.lang.String r1 = "save_address_success"
            r2.A03(r1)
            X.2nZ r6 = r5.A03
            com.facebook.payments.logging.PaymentsLoggingSessionData r7 = r5.A01
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -483333504(0xffffffffe330ea80, float:-3.2635245E21)
            r1 = -976461374(0xffffffffc5cc61c2, float:-6540.2197)
            com.facebook.graphservice.tree.TreeJNI r0 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            java.lang.String r8 = r0.A3m()
            com.facebook.payments.shipping.model.ShippingAddressFormInput r9 = r5.A04
            r10 = 0
            r11 = 0
            com.facebook.payments.logging.PaymentsFlowStep r12 = r5.A00
            X.C55122nZ.A02(r6, r7, r8, r9, r10, r11, r12)
            return
        L_0x2e6c:
            if (r0 == 0) goto L_0x2e97
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = r0.A28()
            if (r4 == 0) goto L_0x2e97
            int r1 = X.AnonymousClass1Y3.B46
            X.2nZ r0 = r5.A03
            X.0UN r0 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.BzI r1 = (X.C24411BzI) r1
            java.lang.String r0 = "save_address_error"
            r1.A03(r0)
            X.2nZ r0 = r5.A03
            X.Bzl r3 = r0.A01
            X.CBK r2 = r0.A04
            com.facebook.payments.model.PaymentItemType r1 = r5.A02
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = r5.A01
            X.0pc r0 = r2.A02(r4, r1, r0)
            r3.BvY(r0)
            return
        L_0x2e97:
            X.2nZ r0 = r5.A03
            X.BpH r4 = r0.A05
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r5.A01
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A08
            java.lang.Throwable r1 = new java.lang.Throwable
            java.lang.String r0 = "Received Null result with no PaymentError Node"
            r1.<init>(r0)
            r4.A05(r3, r2, r1)
            return
        L_0x2eaa:
            r5 = r2
            X.1Cl r5 = (X.C20441Cl) r5
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            r6 = 0
            if (r0 == 0) goto L_0x2ec2
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -465451254(0xffffffffe441c70a, float:-1.4298256E22)
            r1 = 374749762(0x16563a42, float:1.7305165E-25)
            com.facebook.graphservice.tree.TreeJNI r1 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 == 0) goto L_0x2f10
        L_0x2ec2:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -465451254(0xffffffffe441c70a, float:-1.4298256E22)
            r1 = 374749762(0x16563a42, float:1.7305165E-25)
            com.facebook.graphservice.tree.TreeJNI r0 = r0.A0J(r2, r3, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = r0.A28()
            if (r4 == 0) goto L_0x2f10
            if (r0 == 0) goto L_0x2efd
            if (r4 == 0) goto L_0x2efd
            int r1 = X.AnonymousClass1Y3.B46
            X.2nZ r0 = r5.A02
            X.0UN r0 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.BzI r1 = (X.C24411BzI) r1
            java.lang.String r0 = "save_address_error"
            r1.A03(r0)
            X.2nZ r0 = r5.A02
            X.Bzl r3 = r0.A01
            X.CBK r2 = r0.A04
            com.facebook.payments.model.PaymentItemType r1 = r5.A01
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = r5.A00
            X.0pc r0 = r2.A02(r4, r1, r0)
            r3.BvY(r0)
            return
        L_0x2efd:
            X.2nZ r0 = r5.A02
            X.BpH r4 = r0.A05
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r5.A00
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A0m
            java.lang.Throwable r1 = new java.lang.Throwable
            java.lang.String r0 = "Received Null result with no PaymentError Node"
            r1.<init>(r0)
            r4.A05(r3, r2, r1)
            return
        L_0x2f10:
            int r1 = X.AnonymousClass1Y3.B46
            X.2nZ r0 = r5.A02
            X.0UN r0 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.BzI r1 = (X.C24411BzI) r1
            java.lang.String r0 = "save_address_success"
            r1.A03(r0)
            X.2nZ r0 = r5.A02
            com.facebook.payments.logging.PaymentsLoggingSessionData r1 = r5.A00
            java.lang.String r2 = r5.A04
            com.facebook.payments.shipping.model.ShippingAddressFormInput r3 = r5.A03
            boolean r4 = r5.A05
            r5 = 0
            com.facebook.payments.logging.PaymentsFlowStep r6 = com.facebook.payments.logging.PaymentsFlowStep.A0m
            X.C55122nZ.A02(r0, r1, r2, r3, r4, r5, r6)
            return
        L_0x2f32:
            r0 = r2
            X.1jT r0 = (X.C31301jT) r0
            X.2na r0 = r0.A01
            X.Bzl r2 = r0.A02
            goto L_0x2f4d
        L_0x2f3a:
            X.1D8 r2 = (X.AnonymousClass1D8) r2
            X.2n9 r0 = r2.A00
            X.2eX r1 = r0.A06
            com.facebook.payments.paymentmethods.cardform.CardFormParams r0 = r2.A01
        L_0x2f42:
            com.facebook.payments.paymentmethods.cardform.CardFormCommonParams r0 = r0.Agg()
            com.facebook.payments.paymentmethods.cardform.CardFormAnalyticsParams r0 = r0.cardFormAnalyticsParams
            r1.A04(r0)
            X.Bzl r2 = r1.A00
        L_0x2f4d:
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.<init>(r0)
            r2.Bwx(r1)
            return
        L_0x2f58:
            r4 = r2
            X.1Cj r4 = (X.C20421Cj) r4
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x2f83
            com.facebook.payments.ui.ctabutton.PrimaryCtaButtonView r1 = r4.A04
            r1.A0O()
            X.2nc r1 = r4.A03
            X.BpH r5 = r1.A03
            com.facebook.payments.logging.PaymentsLoggingSessionData r3 = r4.A00
            com.facebook.payments.logging.PaymentsFlowStep r2 = com.facebook.payments.logging.PaymentsFlowStep.A1S
            java.lang.String r1 = "payflows_fail"
            r5.A04(r3, r2, r1)
            X.2nc r1 = r4.A03
            X.Bzl r5 = r1.A00
            X.CBK r3 = r1.A02
            com.facebook.payments.model.PaymentItemType r2 = r4.A01
            com.facebook.payments.logging.PaymentsLoggingSessionData r1 = r4.A00
            X.0pc r0 = r3.A02(r0, r2, r1)
            r5.BvY(r0)
            return
        L_0x2f83:
            X.2nc r0 = r4.A03
            X.BpH r3 = r0.A03
            com.facebook.payments.logging.PaymentsLoggingSessionData r2 = r4.A00
            com.facebook.payments.logging.PaymentsFlowStep r1 = com.facebook.payments.logging.PaymentsFlowStep.A1S
            java.lang.String r0 = "payflows_success"
            r3.A04(r2, r1, r0)
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            com.facebook.payments.paymentmethods.model.PayPalBillingAgreement r0 = r4.A02
            java.lang.String r1 = r0.getId()
            r0 = 4
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r2.putExtra(r0, r1)
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            java.lang.String r0 = "extra_activity_result_data"
            r3.putParcelable(r0, r2)
            X.2nc r0 = r4.A03
            X.Bzl r2 = r0.A00
            X.2hx r1 = new X.2hx
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.<init>(r0, r3)
            r2.Bwx(r1)
            return
        L_0x2fbc:
            X.CgP r3 = r5.A00     // Catch:{ JSONException -> 0x2fce }
            java.lang.String r2 = r4.toString()     // Catch:{ JSONException -> 0x2fce }
            X.Cf1 r0 = r3.A00     // Catch:{ JSONException -> 0x2fce }
            X.CeO r0 = r0.A00     // Catch:{ JSONException -> 0x2fce }
            X.CgG r1 = r0.A0H     // Catch:{ JSONException -> 0x2fce }
            java.lang.String r0 = r3.A01     // Catch:{ JSONException -> 0x2fce }
            r1.C3m(r0, r2)     // Catch:{ JSONException -> 0x2fce }
            return
        L_0x2fce:
            X.CgP r2 = r5.A00
            java.lang.Throwable r1 = new java.lang.Throwable
            java.lang.String r0 = "Could not decode match data."
            r1.<init>(r0)
            r2.A00(r1)
            return
        L_0x2fdb:
            r3 = r2
            X.1Cg r3 = (X.C20391Cg) r3
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r0 = "com.facebook.orca.stickers.ADD_SUCCESS"
            r2.<init>(r0)
            com.facebook.stickers.model.StickerPack r0 = r3.A01
            java.lang.String r1 = "stickerPack"
            r2.putExtra(r1, r0)
            X.2nd r0 = r3.A00
            X.0Ut r0 = r0.A00
            r0.C4x(r2)
            X.2nd r6 = r3.A00
            com.facebook.stickers.model.StickerPack r5 = r3.A01
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>()
            r4.putParcelable(r1, r5)
            com.facebook.fbservice.ops.BlueServiceOperationFactory r3 = r6.A01
            com.facebook.common.callercontext.CallerContext r2 = X.C55162nd.A06
            java.lang.String r1 = "download_sticker_pack_assets"
            r0 = 1
            X.0lL r1 = r3.newInstance(r1, r4, r0, r2)
            X.2ne r0 = new X.2ne
            r0.<init>(r6, r5)
            r1.CA2(r0)
            X.1cp r4 = r1.CGe()
            X.1Cf r3 = new X.1Cf
            r3.<init>(r6, r5)
            java.util.concurrent.ExecutorService r0 = r6.A04
            X.C05350Yp.A08(r4, r3, r0)
            java.util.HashMap r2 = r6.A02
            java.lang.String r1 = r5.A0B
            X.1G0 r0 = X.AnonymousClass1G0.A00(r4, r3)
            r2.put(r1, r0)
            return
        L_0x302c:
            r0 = r2
            X.1Cf r0 = (X.C20381Cf) r0
            X.2nd r2 = r0.A00
            com.facebook.stickers.model.StickerPack r1 = r0.A01
            r0 = 1
            X.C55162nd.A01(r2, r0, r1)
            return
        L_0x3038:
            r0 = r2
            X.1jS r0 = (X.C31291jS) r0
            com.facebook.stickers.keyboard.StickerKeyboardView r1 = r0.A00
            r0 = 0
            r1.A03 = r0
            return
        L_0x3041:
            r3 = r2
            X.1Cc r3 = (X.C20351Cc) r3
            com.facebook.stickers.model.Sticker r2 = r3.A01
            com.facebook.stickers.model.StickerCapabilities r0 = r2.A09
            boolean r0 = r0.A00()
            if (r0 != 0) goto L_0x3059
            com.facebook.stickers.keyboard.StickerKeyboardView r1 = r3.A00
            java.lang.String r0 = r2.A0B
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r0)
            com.facebook.stickers.keyboard.StickerKeyboardView.A08(r1, r0)
        L_0x3059:
            com.facebook.stickers.keyboard.StickerKeyboardView r1 = r3.A00
            r0 = 0
            r1.A05 = r0
            return
        L_0x305f:
            r6 = r2
            X.1jR r6 = (X.C31281jR) r6
            com.facebook.graphql.executor.GraphQLResult r0 = (com.facebook.graphql.executor.GraphQLResult) r0
            com.facebook.zero.cms.ZeroCmsUtil r1 = r6.A00
            java.lang.Object r4 = r1.A08
            monitor-enter(r4)
            com.facebook.zero.cms.ZeroCmsUtil r5 = r6.A00     // Catch:{ all -> 0x30ae }
            r1 = 0
            r5.A01 = r1     // Catch:{ all -> 0x30ae }
            if (r0 == 0) goto L_0x309a
            java.lang.Object r3 = r0.A03     // Catch:{ all -> 0x30ae }
            if (r3 == 0) goto L_0x309a
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3     // Catch:{ all -> 0x30ae }
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -816631278(0xffffffffcf533212, float:-3.54327398E9)
            r0 = 806612172(0x3013eccc, float:5.3814797E-10)
            com.facebook.graphservice.tree.TreeJNI r3 = r3.A0J(r1, r2, r0)     // Catch:{ all -> 0x30ae }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3     // Catch:{ all -> 0x30ae }
            if (r3 == 0) goto L_0x30ac
            r1 = 1655870916(0x62b295c4, float:1.6471561E21)
            r0 = 1749541067(0x6847e0cb, float:3.7755905E24)
            com.facebook.graphservice.tree.TreeJNI r1 = r3.A0J(r1, r2, r0)     // Catch:{ all -> 0x30ae }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1     // Catch:{ all -> 0x30ae }
            if (r1 == 0) goto L_0x30ac
            com.facebook.zero.cms.ZeroCmsUtil r0 = r6.A00     // Catch:{ all -> 0x30ae }
            com.facebook.zero.cms.ZeroCmsUtil.A03(r0, r1)     // Catch:{ all -> 0x30ae }
            goto L_0x30ac
        L_0x309a:
            r2 = 2
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x30ae }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x30ae }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x30ae }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ all -> 0x30ae }
            java.lang.String r1 = "ZeroCmsUtil"
            java.lang.String r0 = "zero cms result is null"
            r2.CGS(r1, r0)     // Catch:{ all -> 0x30ae }
        L_0x30ac:
            monitor-exit(r4)     // Catch:{ all -> 0x30ae }
            return
        L_0x30ae:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x30ae }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06020ai.A01(java.lang.Object):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void A02(Throwable th) {
        AnonymousClass09P r2;
        String str;
        String str2;
        String str3;
        C50772ea r0;
        C55112nY r22;
        C23694Bka bka;
        PaymentSettingsPickerRunTimeData paymentSettingsPickerRunTimeData;
        C50742eX r1;
        CardFormParams cardFormParams;
        ImmutableList of;
        boolean z;
        Context context;
        int i;
        C54242m6 r23;
        ContactInfoCommonFormParams contactInfoCommonFormParams;
        C54242m6 r24;
        ContactInfoCommonFormParams contactInfoCommonFormParams2;
        C54782mz r12;
        SimpleCheckoutData simpleCheckoutData;
        RejectAppointmentActivity rejectAppointmentActivity;
        C54692mq r02;
        C103574xO r03;
        C53992lh r13;
        int i2;
        C99834pn r3;
        String str4;
        Resources resources;
        int i3;
        C24997CSi cSi;
        C1516270e r04;
        AnonymousClass09P r25;
        String str5;
        String str6;
        AnonymousClass09P r26;
        String str7;
        String str8;
        if (!(this instanceof AnonymousClass1ER) && !(this instanceof AnonymousClass17Z)) {
            if (this instanceof C31281jR) {
                C31281jR r14 = (C31281jR) this;
                synchronized (r14.A00.A08) {
                    try {
                        r14.A00.A01 = null;
                    } catch (Throwable th2) {
                        th = th2;
                        throw th;
                    }
                }
            } else if (this instanceof AnonymousClass1GO) {
                USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Ap7, ((AnonymousClass1GO) this).A00.A02.A00)).A01("fb4a_carrier_signal_v2_run"), 152);
                if (uSLEBaseShape0S0000000.A0G()) {
                    uSLEBaseShape0S0000000.A0D("state", C137936cG.A00(AnonymousClass07B.A00).toLowerCase());
                    uSLEBaseShape0S0000000.A06();
                }
            } else if (!(this instanceof C20341Ca)) {
                if (this instanceof C20351Cc) {
                    StickerKeyboardView stickerKeyboardView = ((C20351Cc) this).A00;
                    stickerKeyboardView.A05 = null;
                    r2 = (AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, stickerKeyboardView.A06);
                    str = StickerKeyboardView.A0W.getName();
                    str2 = "Updating recent stickers failed";
                } else if (this instanceof C31291jS) {
                    C31291jS r32 = (C31291jS) this;
                    C010708t.A0E(StickerKeyboardView.A0W, th, "Unable to close sticker pack %s", r32.A01.A0B);
                    StickerKeyboardView stickerKeyboardView2 = r32.A00;
                    stickerKeyboardView2.A03 = null;
                    r2 = (AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, stickerKeyboardView2.A06);
                    str = StickerKeyboardView.A0W.getName();
                    str2 = "Marking sticker pack as closed failed";
                } else if (this instanceof AnonymousClass1BR) {
                    AnonymousClass1BR r27 = (AnonymousClass1BR) this;
                    C20021Ap r15 = r27.A01.A00;
                    if (r15 != null) {
                        r15.Bd1(r27.A00, th);
                        return;
                    }
                    return;
                } else if (this instanceof C20381Cf) {
                    C20381Cf r4 = (C20381Cf) this;
                    C010708t.A0E(C55162nd.A07, th, "Unable to download sticker pack %s", r4.A01.A0B);
                    C55162nd.A01(r4.A00, true, r4.A01);
                    return;
                } else if (this instanceof C20391Cg) {
                    C20391Cg r42 = (C20391Cg) this;
                    C010708t.A0E(C55162nd.A07, th, "Unable to add sticker pack %s", r42.A01.A0B);
                    C55162nd.A01(r42.A00, false, r42.A01);
                    return;
                } else if (this instanceof C20411Ci) {
                    ((C20411Ci) this).A00.A00(th);
                    return;
                } else if (this instanceof AnonymousClass1Cb) {
                    C54322mE r43 = ((AnonymousClass1Cb) this).A00;
                    C54332mF r33 = r43.A01;
                    ((C23904BpH) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AP2, r33.A03)).A05(r33.A04, PaymentsFlowStep.A10, th);
                    C54332mF r28 = r43.A01;
                    if (!r28.A0F) {
                        r28.A0D.setVisibility(8);
                        r28.A02.setVisibility(0);
                        r28.A06.setVisibility(0);
                        return;
                    }
                    return;
                } else if (this instanceof C20421Cj) {
                    C20421Cj r16 = (C20421Cj) this;
                    r16.A04.A0O();
                    C55152nc r5 = r16.A03;
                    PaymentsLoggingSessionData paymentsLoggingSessionData = r16.A00;
                    r16.A04.getContext();
                    PaymentItemType paymentItemType = r16.A01;
                    r5.A03.A04(paymentsLoggingSessionData, PaymentsFlowStep.A1S, "payflows_fail");
                    r5.A00.BvY(r5.A02.A03(th, paymentItemType, paymentsLoggingSessionData));
                    return;
                } else if (this instanceof C31301jT) {
                    C31301jT r44 = (C31301jT) this;
                    r44.A01.A01.setVisibility(8);
                    r44.A01.A00.setAlpha(1.0f);
                    C55132na r34 = r44.A01;
                    r34.A03.A01.setOnClickListener(new C51272gC(r34, r44.A00));
                    return;
                } else if (this instanceof C20441Cl) {
                    C20441Cl r6 = (C20441Cl) this;
                    C55122nZ r52 = r6.A02;
                    PaymentsLoggingSessionData paymentsLoggingSessionData2 = r6.A00;
                    r52.A02.getString(2131833026);
                    PaymentItemType paymentItemType2 = r6.A01;
                    r52.A05.A05(paymentsLoggingSessionData2, PaymentsFlowStep.A0m, th);
                    r52.A01.BvY(r52.A04.A03(th, paymentItemType2, paymentsLoggingSessionData2));
                    ((C24411BzI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B46, r6.A02.A00)).A03("save_address_error");
                    return;
                } else if (this instanceof C20451Cm) {
                    C20451Cm r62 = (C20451Cm) this;
                    C55122nZ r53 = r62.A03;
                    PaymentsLoggingSessionData paymentsLoggingSessionData3 = r62.A01;
                    r53.A02.getString(2131832995);
                    PaymentItemType paymentItemType3 = r62.A02;
                    r53.A05.A05(paymentsLoggingSessionData3, r62.A00, th);
                    r53.A01.BvY(r53.A04.A03(th, paymentItemType3, paymentsLoggingSessionData3));
                    ((C24411BzI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B46, r62.A03.A00)).A03("save_address_error");
                    return;
                } else if (!(this instanceof C20461Cn)) {
                    if (this instanceof C20471Co) {
                        C20471Co r45 = (C20471Co) this;
                        r45.A02.A02.A05(r45.A01.A01.Ay1().analyticsParams.paymentsLoggingSessionData, PaymentsFlowStep.A1q, th);
                        r45.A02.A00.A02(new C49932d6(r45));
                        C37741wB r05 = (C37741wB) AnonymousClass0D4.A02(th, C37741wB.class);
                        str3 = r05 != null ? r05.getMessage() : th.getMessage();
                        r0 = r45.A02;
                    } else if (!(this instanceof C20361Cd)) {
                        if (this instanceof C20481Cp) {
                            C20481Cp r46 = (C20481Cp) this;
                            r46.A02.A04.A05(r46.A01.A01().paymentsLoggingSessionData, PaymentsFlowStep.A1q, th);
                            C55112nY r29 = r46.A02;
                            C37741wB r06 = (C37741wB) AnonymousClass0D4.A02(th, C37741wB.class);
                            String message = r06 != null ? r06.getMessage() : th.getMessage();
                            AnonymousClass09P r210 = r29.A01;
                            AnonymousClass06G A02 = AnonymousClass06F.A02(AnonymousClass08S.A0J("PaymentSettingsPickerScreenDataFetcher", "startMailingAddressesFetch_failure"), message);
                            A02.A00 = 1;
                            A02.A03 = th;
                            A02.A04 = true;
                            r210.CGQ(A02.A00());
                            r22 = r46.A02;
                            bka = r46.A00;
                            paymentSettingsPickerRunTimeData = r46.A01;
                        } else if (this instanceof C20491Cq) {
                            C20491Cq r07 = (C20491Cq) this;
                            r22 = r07.A02;
                            bka = r07.A00;
                            paymentSettingsPickerRunTimeData = r07.A01;
                        } else if (this instanceof AnonymousClass1Cs) {
                            AnonymousClass1Cs r08 = (AnonymousClass1Cs) this;
                            r22 = r08.A02;
                            bka = r08.A00;
                            paymentSettingsPickerRunTimeData = r08.A01;
                        } else if (this instanceof C20511Ct) {
                            C20511Ct r09 = (C20511Ct) this;
                            r22 = r09.A02;
                            bka = r09.A00;
                            paymentSettingsPickerRunTimeData = r09.A01;
                        } else if (this instanceof C20521Cu) {
                            C20521Cu r010 = (C20521Cu) this;
                            r22 = r010.A02;
                            bka = r010.A00;
                            paymentSettingsPickerRunTimeData = r010.A01;
                        } else if (this instanceof C20531Cv) {
                            return;
                        } else {
                            if (this instanceof C20371Ce) {
                                C50762eZ r17 = ((C20371Ce) this).A00;
                                C24291Bx0 bx0 = r17.A01;
                                if (bx0 != null) {
                                    bx0.BCF();
                                    r17.A01.ATX(th);
                                    return;
                                }
                                return;
                            } else if (this instanceof C20541Cw) {
                                return;
                            } else {
                                if (this instanceof C20551Cx) {
                                    C20551Cx r18 = (C20551Cx) this;
                                    r18.A01.A01.A00();
                                    r18.A01.A03.A04(((PaymentMethodsPickerScreenConfig) r18.A02.A01).A00.analyticsParams.paymentsLoggingSessionData, PaymentsFlowStep.A06, "payflows_fail");
                                    return;
                                } else if (!(this instanceof C20571Cz)) {
                                    if (this instanceof C20401Ch) {
                                        C20401Ch r47 = (C20401Ch) this;
                                        C50742eX r35 = r47.A02;
                                        CardFormParams cardFormParams2 = r47.A01;
                                        r35.A09(th, null, cardFormParams2.Agg().paymentItemType, cardFormParams2.Agg().cardFormAnalyticsParams.paymentsLoggingSessionData);
                                        r1 = r47.A02;
                                        cardFormParams = r47.A01;
                                    } else if (this instanceof C20431Ck) {
                                        C20431Ck r48 = (C20431Ck) this;
                                        C50742eX r36 = r48.A02;
                                        CardFormParams cardFormParams3 = r48.A01;
                                        r36.A09(th, null, cardFormParams3.Agg().paymentItemType, cardFormParams3.Agg().cardFormAnalyticsParams.paymentsLoggingSessionData);
                                        r1 = r48.A02;
                                        cardFormParams = r48.A01;
                                    } else if (this instanceof AnonymousClass1D0) {
                                        AnonymousClass1D0 r211 = (AnonymousClass1D0) this;
                                        r211.A01.A08(th, null);
                                        r211.A01.A07(th, r211.A00);
                                        return;
                                    } else if (this instanceof C20561Cy) {
                                        C54312mD r49 = ((C20561Cy) this).A00;
                                        CardFormParams cardFormParams4 = r49.A05;
                                        if (!C54312mD.A04(r49)) {
                                            r49.A06.A2W();
                                        }
                                        C010708t.A0L(C54312mD.A0F, "Card failed to update card", th);
                                        r49.A00.softReport(cardFormParams4.Agg().cardFormAnalyticsParams.A00, "Attempted to submit card form, but received a response with an error", th);
                                        C37741wB r011 = (C37741wB) AnonymousClass0D4.A02(th, C37741wB.class);
                                        if (r011 != null) {
                                            C55392o0 r212 = r49.A06;
                                            switch (r011.AmF().mErrorSubCode) {
                                                case 2078020:
                                                case 2078021:
                                                    of = ImmutableList.of(AnonymousClass2K5.EXPIRATION_DATE_FIELD, AnonymousClass2K5.SECURITY_CODE_FIELD);
                                                    break;
                                                case 2078022:
                                                case 2078023:
                                                default:
                                                    of = RegularImmutableList.A02;
                                                    break;
                                                case 2078024:
                                                    of = ImmutableList.of(AnonymousClass2K5.BILLING_ZIP_FIELD);
                                                    break;
                                            }
                                            r212.A2b();
                                            C24971Xv it = of.iterator();
                                            String str9 = null;
                                            String str10 = null;
                                            while (true) {
                                                z = true;
                                                if (it.hasNext()) {
                                                    AnonymousClass2K5 r012 = (AnonymousClass2K5) it.next();
                                                    if (str9 == null && str10 == null) {
                                                        switch (r012.ordinal()) {
                                                            case 0:
                                                                if (r212.A0K.getVisibility() != 0) {
                                                                    break;
                                                                } else {
                                                                    r212.A0K.A0Q();
                                                                    str9 = r212.A12().getString(2131831020);
                                                                    break;
                                                                }
                                                            case 1:
                                                                if (r212.A0L.getVisibility() != 0) {
                                                                    break;
                                                                } else {
                                                                    r212.A0L.A0Q();
                                                                    str9 = r212.A12().getString(2131831021, Integer.valueOf(CoM.A00(r212.A0L.A0O())));
                                                                    break;
                                                                }
                                                            case 2:
                                                                if (r212.A0H.getVisibility() != 0) {
                                                                    break;
                                                                } else {
                                                                    r212.A0L.A0Q();
                                                                    r212.A0H.A0Q();
                                                                    str10 = r212.A12().getString(2131831019, Integer.valueOf(CoM.A00(r212.A0H.A0O())));
                                                                    break;
                                                                }
                                                        }
                                                    }
                                                } else {
                                                    z = false;
                                                }
                                            }
                                            if (z) {
                                                C55392o0.A04(r212);
                                                return;
                                            } else if (str9 != null) {
                                                C55392o0.A06(r212, r212.A0O, 0);
                                                PaymentsErrorView paymentsErrorView = r212.A0O;
                                                paymentsErrorView.A01.setVisibility(0);
                                                paymentsErrorView.A01.setText(str9);
                                                return;
                                            } else if (str10 != null) {
                                                C55392o0.A06(r212, r212.A0P, 0);
                                                PaymentsErrorView paymentsErrorView2 = r212.A0P;
                                                paymentsErrorView2.A01.setVisibility(0);
                                                paymentsErrorView2.A01.setText(str10);
                                                return;
                                            } else {
                                                return;
                                            }
                                        } else {
                                            return;
                                        }
                                    } else if (this instanceof C20501Cr) {
                                        C54312mD r19 = ((C20501Cr) this).A00;
                                        if (!C54312mD.A04(r19)) {
                                            r19.A06.A2W();
                                            return;
                                        }
                                        return;
                                    } else if (this instanceof AnonymousClass1D1) {
                                        AnonymousClass1D1 r213 = (AnonymousClass1D1) this;
                                        C54982nL r110 = r213.A00;
                                        if (r110.A01) {
                                            r110.A00.add(new C51322gM(r213.A01, null, th));
                                            return;
                                        } else {
                                            r213.A01.BYh(th);
                                            return;
                                        }
                                    } else if (this instanceof AnonymousClass1D3) {
                                        AnonymousClass1D3 r214 = (AnonymousClass1D3) this;
                                        if (!(th instanceof C55192ng) || ((C55192ng) th).error.apiErrorCode != 10121) {
                                            r214.A00.A01.BdW(th);
                                            return;
                                        } else {
                                            r214.A00.A01.BlH(th);
                                            return;
                                        }
                                    } else if (this instanceof AnonymousClass1D6) {
                                        ((AnonymousClass1D6) this).A00.A01.BdW(th);
                                        return;
                                    } else if (this instanceof AnonymousClass1D7) {
                                        AnonymousClass1D7 r111 = (AnonymousClass1D7) this;
                                        AnonymousClass064.A00(r111.A00.A00);
                                        r111.A00.A00.A00.A05.A00();
                                        C54902nD r215 = r111.A00.A00;
                                        C24126Btj.A05(r215.A00.A00, th, new C51242g7(r215));
                                        return;
                                    } else if (this instanceof AnonymousClass1D8) {
                                        AnonymousClass1D8 r013 = (AnonymousClass1D8) this;
                                        C54862n9 r410 = r013.A00;
                                        CardFormParams cardFormParams5 = r013.A01;
                                        if (r410.A03.Aem(282879431018428L)) {
                                            C24126Btj.A05(r410.A01, th, new C51232g6(th));
                                        } else {
                                            String str11 = null;
                                            C37741wB r014 = (C37741wB) AnonymousClass0D4.A02(th, C37741wB.class);
                                            if (r014 != null && r014.AmF().A02() == 10058) {
                                                str11 = r410.A01.getString(2131823800);
                                            }
                                            r410.A06.A08(th, str11);
                                        }
                                        r410.A06.A07(th, cardFormParams5);
                                        return;
                                    } else if (this instanceof AnonymousClass1D9) {
                                        AnonymousClass1D9 r54 = (AnonymousClass1D9) this;
                                        if (r54.A00.A03.Aem(282879431018428L)) {
                                            C24126Btj.A05(r54.A00.A01, th, new C51232g6(th));
                                        } else {
                                            C54862n9 r015 = r54.A00;
                                            C50742eX r411 = r015.A06;
                                            String string = r015.A01.getString(2131824155);
                                            CardFormParams cardFormParams6 = r54.A03;
                                            r411.A09(th, string, cardFormParams6.Agg().paymentItemType, cardFormParams6.Agg().cardFormAnalyticsParams.paymentsLoggingSessionData);
                                        }
                                        r1 = r54.A00.A06;
                                        cardFormParams = r54.A03;
                                    } else if (this instanceof AnonymousClass1D2) {
                                        AnonymousClass1D2 r55 = (AnonymousClass1D2) this;
                                        if (r55.A00.A02.Aem(282879431018428L)) {
                                            C24126Btj.A05(r55.A00.A01, th, new C51202g3(th));
                                        } else {
                                            C50722eV r216 = r55.A00;
                                            C50742eX r412 = r216.A04;
                                            C37741wB r016 = (C37741wB) AnonymousClass0D4.A02(th, C37741wB.class);
                                            String str12 = null;
                                            if (r016 != null) {
                                                switch (r016.AmF().A02()) {
                                                    case AnonymousClass1Y3.BNe /*10052*/:
                                                    case AnonymousClass1Y3.BNi /*10057*/:
                                                        context = r216.A01;
                                                        i = 2131821198;
                                                        str12 = context.getString(i);
                                                        break;
                                                    case AnonymousClass1Y3.BNf /*10053*/:
                                                        context = r216.A01;
                                                        i = 2131821197;
                                                        str12 = context.getString(i);
                                                        break;
                                                    case AnonymousClass1Y3.BNg /*10054*/:
                                                        context = r216.A01;
                                                        i = 2131821199;
                                                        str12 = context.getString(i);
                                                        break;
                                                    case AnonymousClass1Y3.BNh /*10055*/:
                                                        context = r216.A01;
                                                        i = 2131821200;
                                                        str12 = context.getString(i);
                                                        break;
                                                    case 10056:
                                                        context = r216.A01;
                                                        i = 2131821201;
                                                        str12 = context.getString(i);
                                                        break;
                                                }
                                            }
                                            CardFormParams cardFormParams7 = r55.A02;
                                            r412.A09(th, str12, cardFormParams7.Agg().paymentItemType, cardFormParams7.Agg().cardFormAnalyticsParams.paymentsLoggingSessionData);
                                        }
                                        r1 = r55.A00.A04;
                                        cardFormParams = r55.A02;
                                    } else if (this instanceof AnonymousClass1DA) {
                                        C010708t.A08(C54842n7.A06, "Failed to fetch P2P post actions", th);
                                        return;
                                    } else if (this instanceof AnonymousClass1D5) {
                                        for (CFA BnR : ((AnonymousClass1D5) this).A00.A01.A00) {
                                            BnR.BnR(th);
                                        }
                                        return;
                                    } else if (this instanceof AnonymousClass1DB) {
                                        for (CFA BTG : ((AnonymousClass1DB) this).A00.A01.A00) {
                                            BTG.BTG(th);
                                        }
                                        return;
                                    } else if (this instanceof AnonymousClass1DD) {
                                        AnonymousClass1DD r217 = (AnonymousClass1DD) this;
                                        r217.A02.A00.A02(new C51192g2(r217));
                                        return;
                                    } else if (!(this instanceof AnonymousClass1D4)) {
                                        if (this instanceof AnonymousClass1DE) {
                                            AnonymousClass1DE r017 = (AnonymousClass1DE) this;
                                            r23 = r017.A01;
                                            contactInfoCommonFormParams = r017.A00;
                                        } else if (this instanceof AnonymousClass1DF) {
                                            AnonymousClass1DF r018 = (AnonymousClass1DF) this;
                                            r23 = r018.A01;
                                            contactInfoCommonFormParams = r018.A00;
                                        } else if (!(this instanceof C31311jU)) {
                                            if (this instanceof AnonymousClass1DH) {
                                                AnonymousClass1DH r37 = (AnonymousClass1DH) this;
                                                C54232m5 r019 = r37.A01;
                                                r24 = r019.A03;
                                                r019.A00.getString(2131823192);
                                                contactInfoCommonFormParams2 = r37.A00;
                                            } else if (this instanceof AnonymousClass1DC) {
                                                AnonymousClass1DC r218 = (AnonymousClass1DC) this;
                                                C54212m3 r38 = r218.A01;
                                                r38.A00.getString(2131823192);
                                                r38.A03.A02(th, r218.A00, false);
                                                return;
                                            } else if (this instanceof AnonymousClass1DI) {
                                                AnonymousClass1DI r39 = (AnonymousClass1DI) this;
                                                C54212m3 r020 = r39.A01;
                                                r24 = r020.A03;
                                                r020.A00.getString(2131823192);
                                                contactInfoCommonFormParams2 = r39.A00;
                                            } else if (this instanceof AnonymousClass1DK) {
                                                AnonymousClass1DK r310 = (AnonymousClass1DK) this;
                                                AnonymousClass07A.A04(r310.A00.A0F, new C55372ny(r310), 1456608317);
                                                return;
                                            } else if (this instanceof AnonymousClass1DL) {
                                                AnonymousClass1DL r311 = (AnonymousClass1DL) this;
                                                C54162ly.A05(r311.A00, PaymentsFlowStep.A1O, "payflows_fail");
                                                C54162ly r219 = r311.A00;
                                                C54162ly.A03(r219, r219.A03.A00);
                                                return;
                                            } else if (this instanceof AnonymousClass1DG) {
                                                AnonymousClass1DG r112 = (AnonymousClass1DG) this;
                                                C54162ly.A01(r112.A00);
                                                C54162ly.A05(r112.A00, PaymentsFlowStep.A1M, "payflows_fail");
                                                return;
                                            } else if (this instanceof AnonymousClass1DJ) {
                                                C54162ly.A01(((AnonymousClass1DJ) this).A00);
                                                return;
                                            } else if (!(this instanceof AnonymousClass1DM)) {
                                                if (this instanceof AnonymousClass1DN) {
                                                    AnonymousClass1DN r413 = (AnonymousClass1DN) this;
                                                    r413.A00.A05.A04(r413.A01.A01().A00, PaymentsFlowStep.A0A, "payflows_fail");
                                                    r12 = r413.A00;
                                                    simpleCheckoutData = r413.A01;
                                                } else if (this instanceof AnonymousClass1DQ) {
                                                    AnonymousClass1DQ r414 = (AnonymousClass1DQ) this;
                                                    r414.A00.A05.A04(r414.A01.A01().A00, PaymentsFlowStep.A0F, "payflows_fail");
                                                    r12 = r414.A00;
                                                    simpleCheckoutData = r414.A01;
                                                } else if (this instanceof AnonymousClass1DR) {
                                                    AnonymousClass1DR r312 = (AnonymousClass1DR) this;
                                                    r312.A00.A05.A09(r312.A02.A01().A00, "async", false);
                                                    r312.A00.A05.A05(r312.A02.A01().A00, PaymentsFlowStep.A1M, th);
                                                    C54782mz.A03(r312.A00, th, r312.A01);
                                                    return;
                                                } else if (!(this instanceof AnonymousClass1DS)) {
                                                    if (this instanceof AnonymousClass1DT) {
                                                        rejectAppointmentActivity = ((AnonymousClass1DT) this).A00.A00;
                                                    } else if (this instanceof AnonymousClass1DU) {
                                                        rejectAppointmentActivity = ((AnonymousClass1DU) this).A00;
                                                    } else if (this instanceof AnonymousClass1DV) {
                                                        rejectAppointmentActivity = ((AnonymousClass1DV) this).A00;
                                                    } else if (!(this instanceof AnonymousClass1DX)) {
                                                        if (this instanceof C20981Eo) {
                                                            C20981Eo r220 = (C20981Eo) this;
                                                            r220.A00.A2T(false);
                                                            r02 = r220.A00;
                                                        } else if (this instanceof AnonymousClass1DY) {
                                                            AnonymousClass1DY r221 = (AnonymousClass1DY) this;
                                                            r221.A00.A2T(false);
                                                            r02 = r221.A00;
                                                        } else if (this instanceof AnonymousClass1DZ) {
                                                            AnonymousClass1DZ r222 = (AnonymousClass1DZ) this;
                                                            r222.A00.A2T(false);
                                                            r02 = r222.A00;
                                                        } else if (this instanceof C20581Da) {
                                                            C20581Da r223 = (C20581Da) this;
                                                            r223.A00.A2T(false);
                                                            r02 = r223.A00;
                                                        } else if (this instanceof C20591Db) {
                                                            C20591Db r313 = (C20591Db) this;
                                                            r313.A00.A2T(false);
                                                            if (!(th instanceof C55192ng)) {
                                                                r313.A00.A03.AOI(C08870g7.A2V, "create_appt_failed");
                                                                return;
                                                            }
                                                            C55192ng r415 = (C55192ng) th;
                                                            if (r415.error.code == 2114011) {
                                                                r313.A00.A03.AOI(C08870g7.A2V, "create_appt_failed_with_conflict");
                                                                C54712ms.A02(r313.A00.A0A, "appointment_confirmation_failure_with_conflict_impression");
                                                                C13500rX r224 = new C13500rX(r313.A00.A1j());
                                                                r224.A09(2131823159);
                                                                r224.A08(2131823158);
                                                                r224.A02(2131823866, new C55332nu(r313));
                                                                r224.A06().show();
                                                                return;
                                                            }
                                                            r313.A00.A03.AOI(C08870g7.A2V, "create_appt_failed");
                                                            C54712ms r225 = r313.A00.A0A;
                                                            int i4 = r415.error.code;
                                                            USLEBaseShape0S0000000 A002 = C54712ms.A00(r225, "appointment_confirmation_failure_impression");
                                                            if (A002 != null) {
                                                                A002.A0B(TraceFieldType.ErrorCode, Integer.valueOf(i4));
                                                                A002.A06();
                                                            }
                                                            C54692mq r021 = r313.A00;
                                                            th.getMessage();
                                                            C54692mq.A07(r021);
                                                            return;
                                                        } else if (this instanceof AnonymousClass1GQ) {
                                                            AnonymousClass16R r113 = ((AnonymousClass1GQ) this).A00;
                                                            r113.A04 = null;
                                                            r113.A0G.A00();
                                                            return;
                                                        } else if (this instanceof C31321jV) {
                                                            C31321jV r314 = (C31321jV) this;
                                                            ImageView imageView = r314.A00.A01;
                                                            if (imageView != null) {
                                                                imageView.setVisibility(4);
                                                            }
                                                            FbVideoView fbVideoView = r314.A00.A0B;
                                                            if (fbVideoView != null) {
                                                                fbVideoView.setVisibility(4);
                                                            }
                                                            FbDraweeView fbDraweeView = r314.A00.A05;
                                                            if (fbDraweeView != null) {
                                                                fbDraweeView.setVisibility(8);
                                                            }
                                                            r314.A00.A02.setVisibility(4);
                                                            C55322nt r022 = r314.A00.A06;
                                                            if (r022 != null) {
                                                                r022.A00.A01.setVisibility(0);
                                                            }
                                                            r2 = r314.A00.A03;
                                                            str = MediaSharePreviewPlayableView.A0F.getName();
                                                            str2 = "Failed to fetch media resource for playable";
                                                        } else if (this instanceof C31331jW) {
                                                            C54672mo r023 = ((C31331jW) this).A01.A00;
                                                            if (r023 != null) {
                                                                r023.A00.A06.A02();
                                                                return;
                                                            }
                                                            return;
                                                        } else if (this instanceof AnonymousClass1DO) {
                                                            AnonymousClass1DO r315 = (AnonymousClass1DO) this;
                                                            SingleRecipientShareLauncherActivity singleRecipientShareLauncherActivity = r315.A00;
                                                            singleRecipientShareLauncherActivity.A0B = null;
                                                            singleRecipientShareLauncherActivity.setResult(0);
                                                            r315.A00.finish();
                                                            return;
                                                        } else if (!(this instanceof C20601Dc)) {
                                                            if (this instanceof AnonymousClass1DP) {
                                                                r03 = ((AnonymousClass1DP) this).A00.A00;
                                                                if (r03 == null) {
                                                                    return;
                                                                }
                                                            } else if (!(this instanceof C31341jX)) {
                                                                if (this instanceof C20611Dd) {
                                                                    r13 = ((C20611Dd) this).A00;
                                                                    i2 = 2131832391;
                                                                } else if (this instanceof C20631Df) {
                                                                    r13 = ((C20631Df) this).A00;
                                                                    i2 = 2131832396;
                                                                } else if (this instanceof C20651Dh) {
                                                                    C20651Dh r316 = (C20651Dh) this;
                                                                    AnonymousClass09P r226 = r316.A01.A07;
                                                                    AnonymousClass06G A022 = AnonymousClass06F.A02("quick cam popup", th.getMessage());
                                                                    A022.A03 = th;
                                                                    r226.CGQ(A022.A00());
                                                                    r316.A01.A0V.A04(new C51092fR(2131825368));
                                                                    C53882lW r227 = r316.A01;
                                                                    r227.A0e = false;
                                                                    C53882lW.A07(r227);
                                                                    return;
                                                                } else if (this instanceof C20661Di) {
                                                                    C20661Di r317 = (C20661Di) this;
                                                                    AnonymousClass09P r228 = r317.A00.A07;
                                                                    AnonymousClass06G A023 = AnonymousClass06F.A02("quick cam popup", th.getMessage());
                                                                    A023.A03 = th;
                                                                    r228.CGQ(A023.A00());
                                                                    C53882lW.A07(r317.A00);
                                                                    return;
                                                                } else if (this instanceof C31351jY) {
                                                                    C96304iY.A00(((C31351jY) this).A00);
                                                                    return;
                                                                } else if (this instanceof C20671Dj) {
                                                                    C20671Dj r63 = (C20671Dj) this;
                                                                    r63.A00.A01.CGS(C53842lS.class.getName(), AnonymousClass08S.A0S("admin_accept_appointment: ", r63.A01, " ", th.getMessage()));
                                                                    r63.A00.A03.A04(new C51092fR(2131825404));
                                                                    C53852lT r024 = r63.A00.A00;
                                                                    if (r024 != null) {
                                                                        r024.A00.A01.A06.BV8();
                                                                        return;
                                                                    }
                                                                    return;
                                                                } else if (!(this instanceof C20621De)) {
                                                                    if (this instanceof C20691Dl) {
                                                                        C20691Dl r416 = (C20691Dl) this;
                                                                        C04810Wg r025 = r416.A01;
                                                                        if (r025 != null) {
                                                                            r025.BYh(th);
                                                                        }
                                                                        r416.A00.A03.softReport("PollMutator", th);
                                                                        C53832lR r026 = r416.A00;
                                                                        r3 = r026.A00;
                                                                        str4 = r026.A02.getString(2131830596);
                                                                        resources = r416.A00.A02;
                                                                        i3 = 2131830598;
                                                                    } else if (this instanceof C20701Dm) {
                                                                        C20701Dm r417 = (C20701Dm) this;
                                                                        C04810Wg r027 = r417.A01;
                                                                        if (r027 != null) {
                                                                            r027.BYh(th);
                                                                        }
                                                                        r417.A00.A03.softReport("PollMutator", th);
                                                                        C53832lR r028 = r417.A00;
                                                                        r3 = r028.A00;
                                                                        str4 = r028.A02.getString(2131830596);
                                                                        resources = r417.A00.A02;
                                                                        i3 = 2131830595;
                                                                    } else if (this instanceof C20681Dk) {
                                                                        C20681Dk r418 = (C20681Dk) this;
                                                                        C04810Wg r029 = r418.A01;
                                                                        if (r029 != null) {
                                                                            r029.BYh(th);
                                                                        }
                                                                        r418.A00.A03.softReport("PollMutator", th);
                                                                        C53832lR r030 = r418.A00;
                                                                        r3 = r030.A00;
                                                                        str4 = r030.A02.getString(2131830596);
                                                                        resources = r418.A00.A02;
                                                                        i3 = 2131830594;
                                                                    } else if (!(this instanceof C20721Do)) {
                                                                        if (this instanceof C20641Dg) {
                                                                            cSi = ((C20641Dg) this).A00;
                                                                        } else if (this instanceof C20731Dp) {
                                                                            cSi = ((C20731Dp) this).A00;
                                                                        } else if (this instanceof C20781Du) {
                                                                            cSi = ((C20781Du) this).A00;
                                                                        } else if (this instanceof C20761Ds) {
                                                                            C20761Ds r229 = (C20761Ds) this;
                                                                            r229.A00.A00.softReport("OmniMReminderLoader", th);
                                                                            C53782lM r419 = r229.A01;
                                                                            C53802lO r318 = r419.A01;
                                                                            if (r318.A1X()) {
                                                                                r419.A01.A06.A0F(C78353on.A02(((C26681bq) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AuB, r318.A02)).A08(r419.A00)), r419.A02);
                                                                                return;
                                                                            }
                                                                            return;
                                                                        } else if (this instanceof C20791Dv) {
                                                                            C20791Dv r230 = (C20791Dv) this;
                                                                            r230.A01.A00.softReport("OmniMReminderLoader", th);
                                                                            r230.A00.BYh(th);
                                                                            return;
                                                                        } else if (this instanceof C20741Dq) {
                                                                            C20741Dq r231 = (C20741Dq) this;
                                                                            if (r231.A00 != null) {
                                                                                r231.A01.A00.softReport("OmniMChatExtensionMutator", th);
                                                                                C53762lK r114 = r231.A00;
                                                                                AnonymousClass9MN r031 = r114.A01.A01;
                                                                                if (r031 != null) {
                                                                                    r031.dismiss();
                                                                                }
                                                                                C53772lL.A00(r114.A01);
                                                                                return;
                                                                            }
                                                                            return;
                                                                        } else if (this instanceof C20801Dw) {
                                                                            LikelyParentDownloadPromptNotificationsManager likelyParentDownloadPromptNotificationsManager = ((C20801Dw) this).A00.A01;
                                                                            likelyParentDownloadPromptNotificationsManager.A01 = null;
                                                                            likelyParentDownloadPromptNotificationsManager.A0A();
                                                                            th.toString();
                                                                            return;
                                                                        } else if (this instanceof C35941s4) {
                                                                            C35941s4 r032 = (C35941s4) this;
                                                                            synchronized (r032.A00) {
                                                                                try {
                                                                                    r032.A00.A00 = null;
                                                                                } catch (Throwable th3) {
                                                                                    th = th3;
                                                                                    throw th;
                                                                                }
                                                                            }
                                                                            return;
                                                                        } else if (this instanceof C20771Dt) {
                                                                            C20771Dt r319 = (C20771Dt) this;
                                                                            C46642Qw A003 = MediaResource.A00();
                                                                            A003.A01(r319.A02);
                                                                            A003.A0k = true;
                                                                            MediaResource A004 = A003.A00();
                                                                            ArrayList arrayList = new ArrayList(r319.A01.A02);
                                                                            arrayList.set(r319.A00, A004);
                                                                            r319.A01.A02 = ImmutableList.copyOf((Collection) arrayList);
                                                                            return;
                                                                        } else if (this instanceof C20811Dx) {
                                                                            C20811Dx r232 = (C20811Dx) this;
                                                                            RichVideoPlayer richVideoPlayer = r232.A00.A05;
                                                                            if (richVideoPlayer != null) {
                                                                                richVideoPlayer.setVisibility(4);
                                                                            }
                                                                            C55432o4 r033 = r232.A00.A02;
                                                                            if (r033 != null) {
                                                                                r033.A00.A04.A04();
                                                                            }
                                                                            r2 = r232.A00.A00;
                                                                            str = MediaPickerPopupVideoView.A0B.getName();
                                                                            str2 = "Failed to fetch media resource for video";
                                                                        } else if (this instanceof C23771Qu) {
                                                                            C23771Qu r034 = (C23771Qu) this;
                                                                            C51112fT r320 = r034.A01;
                                                                            LocalMediaLoaderParams localMediaLoaderParams = r034.A00;
                                                                            r320.A04.A00.markerCancel(5505085);
                                                                            C20021Ap r035 = r320.A00;
                                                                            if (r035 != null) {
                                                                                r035.Bd1(localMediaLoaderParams, th);
                                                                                return;
                                                                            }
                                                                            return;
                                                                        } else if (this instanceof AnonymousClass1E1) {
                                                                            AnonymousClass1E1 r233 = (AnonymousClass1E1) this;
                                                                            C51462gv r115 = r233.A00.A00;
                                                                            if (r115 != null) {
                                                                                r115.A00(r233.A01);
                                                                                return;
                                                                            }
                                                                            return;
                                                                        } else if (this instanceof C20821Dy) {
                                                                            C20821Dy r234 = (C20821Dy) this;
                                                                            C51462gv r116 = r234.A00.A00;
                                                                            if (r116 != null) {
                                                                                r116.A00(r234.A03);
                                                                                return;
                                                                            }
                                                                            return;
                                                                        } else if (this instanceof AnonymousClass1E0) {
                                                                            AnonymousClass1E0 r321 = (AnonymousClass1E0) this;
                                                                            AnonymousClass3KD.A05((AnonymousClass3KD) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AvX, r321.A00.A01), "messenger_live_location_location_request_did_fail");
                                                                            LiveLocationForegroundService.A00(r321.A00);
                                                                            return;
                                                                        } else if (this instanceof AnonymousClass1E5) {
                                                                            ((AnonymousClass1E5) this).A00.BYW();
                                                                            return;
                                                                        } else if (this instanceof AnonymousClass1E6) {
                                                                            AnonymousClass1E6 r322 = (AnonymousClass1E6) this;
                                                                            r322.A02.A00.softReport("live_location_eta_calculator_graphql", "onNonCancellationFailure start=" + r322.A01 + " destination=" + r322.A00, th);
                                                                            C50972f5 r235 = r322.A03;
                                                                            r235.A00.A03.remove(r235.A01);
                                                                            return;
                                                                        } else if (!(this instanceof C37211us)) {
                                                                            if (this instanceof AnonymousClass1E2) {
                                                                                AnonymousClass1E2 r323 = (AnonymousClass1E2) this;
                                                                                r323.A01.A00.softReport("EventReminderMutator", "Failed to create an event reminder.", th);
                                                                                r04 = r323.A00;
                                                                                if (r04 == null) {
                                                                                    return;
                                                                                }
                                                                            } else if (this instanceof AnonymousClass1A4) {
                                                                                AnonymousClass1A4 r324 = (AnonymousClass1A4) this;
                                                                                r324.A01.A00.softReport("EventReminderMutator", "Failed to send RSVP for an event reminder.", th);
                                                                                r04 = r324.A00;
                                                                                if (r04 == null) {
                                                                                    return;
                                                                                }
                                                                            } else if (this instanceof AnonymousClass1E7) {
                                                                                C010708t.A09(C54502mW.A0B, "Failed to load recent emoji", th);
                                                                                ((AnonymousClass1E7) this).A00.A01 = null;
                                                                                return;
                                                                            } else if (this instanceof AnonymousClass1FB) {
                                                                                AnonymousClass1FB r325 = (AnonymousClass1FB) this;
                                                                                C010708t.A09(C189016a.A0C, "ContactsLoader.onNonCancellationFailure", th);
                                                                                r325.A00.A06.softReport("ContactsLoader", "onNonCancellationFailure", th);
                                                                                C189016a r036 = r325.A00;
                                                                                r036.A00 = null;
                                                                                C20021Ap r037 = r036.A01;
                                                                                if (r037 != null) {
                                                                                    r037.Bd1(null, th);
                                                                                    return;
                                                                                }
                                                                                return;
                                                                            } else if (this instanceof AnonymousClass1E8) {
                                                                                AnonymousClass1E8 r326 = (AnonymousClass1E8) this;
                                                                                r326.A01.A01.softReport("PlatformWebviewShareMutator", "Can't get request mutation result", th);
                                                                                r326.A00.BYh(th);
                                                                                return;
                                                                            } else if (!(this instanceof AnonymousClass1E9)) {
                                                                                if (!(this instanceof C37221ut)) {
                                                                                    if (this instanceof AnonymousClass1EC) {
                                                                                        AnonymousClass1EC r327 = (AnonymousClass1EC) this;
                                                                                        C54492mV r117 = r327.A00;
                                                                                        if (th instanceof C55192ng) {
                                                                                            GraphQLError graphQLError = ((C55192ng) th).error;
                                                                                            r117.A00.A07(graphQLError.code, graphQLError.A05());
                                                                                        } else {
                                                                                            r117.A00.A06(C208829t7.A00(AnonymousClass07B.A0J));
                                                                                        }
                                                                                        r25 = (AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r327.A01.A01);
                                                                                        str5 = "PlatformGetContextGQLController";
                                                                                    } else if (this instanceof AnonymousClass1EE) {
                                                                                        AnonymousClass1EE r118 = (AnonymousClass1EE) this;
                                                                                        r118.A00.BYP(th);
                                                                                        r25 = r118.A01.A00;
                                                                                        str5 = "PlatformPermissionGQLController";
                                                                                        str6 = "Ask permission mutation GQL query fails";
                                                                                    } else if (this instanceof AnonymousClass1E3) {
                                                                                        AnonymousClass1E3 r328 = (AnonymousClass1E3) this;
                                                                                        C53652l9 r119 = r328.A00;
                                                                                        if (th instanceof C55192ng) {
                                                                                            GraphQLError graphQLError2 = ((C55192ng) th).error;
                                                                                            r119.A00.A07(graphQLError2.code, graphQLError2.A05());
                                                                                        } else {
                                                                                            r119.A00.A06(C208829t7.A00(AnonymousClass07B.A0J));
                                                                                        }
                                                                                        r25 = r328.A01.A00;
                                                                                        str5 = "PlatformPermissionGQLController";
                                                                                        str6 = "Get ask permission display info GQL query fails";
                                                                                    } else if (this instanceof AnonymousClass1EF) {
                                                                                        AnonymousClass1EF r329 = (AnonymousClass1EF) this;
                                                                                        C54482mU r120 = r329.A00;
                                                                                        if (th instanceof C55192ng) {
                                                                                            GraphQLError graphQLError3 = ((C55192ng) th).error;
                                                                                            r120.A00.A07(graphQLError3.code, graphQLError3.A05());
                                                                                        } else {
                                                                                            r120.A00.A06(C208829t7.A00(AnonymousClass07B.A0J));
                                                                                        }
                                                                                        r25 = r329.A01.A00;
                                                                                        str5 = "PlatformPermissionGQLController";
                                                                                    } else if (!(this instanceof AnonymousClass1EG)) {
                                                                                        if (this instanceof C37231uu) {
                                                                                            C37231uu r330 = (C37231uu) this;
                                                                                            r26 = (AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r330.A01.A00);
                                                                                            str7 = AnonymousClass08S.A0J("Can't get request mutation result for: ", r330.A00.tag);
                                                                                            str8 = "ComposerShortcutsInteractionGQLController";
                                                                                        } else if (this instanceof C37241uv) {
                                                                                            C37241uv r121 = (C37241uv) this;
                                                                                            if (!(th instanceof IOException)) {
                                                                                                int i5 = AnonymousClass1Y3.Amr;
                                                                                                AnonymousClass2E1 r122 = r121.A02;
                                                                                                ((AnonymousClass09P) AnonymousClass1XX.A02(0, i5, r122.A00)).softReport("ComposerShortcutsDataProvider", r122.A03.tag, th);
                                                                                                return;
                                                                                            }
                                                                                            return;
                                                                                        } else if (this instanceof C37251uw) {
                                                                                            C37251uw r331 = (C37251uw) this;
                                                                                            r26 = (AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r331.A00.A00);
                                                                                            str7 = AnonymousClass08S.A0J("Can't get request mutation result for: ", r331.A01.tag);
                                                                                            str8 = "ClearBadgeGQLController";
                                                                                        } else if (this instanceof AnonymousClass1E4) {
                                                                                            AnonymousClass1E4 r332 = (AnonymousClass1E4) this;
                                                                                            try {
                                                                                                C13500rX r236 = new C13500rX(r332.A00);
                                                                                                r236.A09(2131823483);
                                                                                                r236.A08(2131823482);
                                                                                                r236.A02(2131823481, new C55222nj());
                                                                                                r236.A07();
                                                                                            } catch (WindowManager.BadTokenException unused) {
                                                                                            }
                                                                                            C189918sH r038 = r332.A01;
                                                                                            if (r038 != null) {
                                                                                                r038.BQJ();
                                                                                                return;
                                                                                            }
                                                                                            return;
                                                                                        } else if (this instanceof AnonymousClass1EH) {
                                                                                            AnonymousClass1EH r420 = (AnonymousClass1EH) this;
                                                                                            C54432mP r333 = r420.A01;
                                                                                            C54442mQ r237 = r333.A00;
                                                                                            r237.A00.setVisibility(8);
                                                                                            r237.A01.setVisibility(0);
                                                                                            C54462mS r039 = r333.A00.A04;
                                                                                            if (r039 != null) {
                                                                                                r039.A00();
                                                                                            }
                                                                                            r25 = r420.A00.A00;
                                                                                            str5 = "AirlineItineraryLoader";
                                                                                            str6 = "Airline itinerary graphQL query fails";
                                                                                        } else if (this instanceof AnonymousClass1EJ) {
                                                                                            ((AnonymousClass1EJ) this).A00.A00.setVisibility(8);
                                                                                            return;
                                                                                        } else if (this instanceof AnonymousClass1EA) {
                                                                                            AnonymousClass1EA r123 = (AnonymousClass1EA) this;
                                                                                            r123.A00.A00();
                                                                                            r25 = r123.A01.A00;
                                                                                            str5 = "AccountLinkTaskManager";
                                                                                            str6 = "Messenger platform account link mutation fails";
                                                                                        } else if (this instanceof AnonymousClass1EB) {
                                                                                            C55202nh r334 = ((AnonymousClass1EB) this).A01;
                                                                                            C53602l3 r238 = new C53602l3();
                                                                                            r238.A01 = r334.A01;
                                                                                            r238.A02 = AnonymousClass07B.A0Y;
                                                                                            r238.A00 = null;
                                                                                            r238.A03 = C53592l2.A02(r334.A03, r334.A02);
                                                                                            r334.A00.A04.A02(r238.A00());
                                                                                            return;
                                                                                        } else if (this instanceof AnonymousClass1EI) {
                                                                                            C53552ky.A01(((AnonymousClass1EI) this).A00, AnonymousClass07B.A0Y);
                                                                                            return;
                                                                                        } else if (this instanceof AnonymousClass1EK) {
                                                                                            ((AnonymousClass1EK) this).A00.A00 = null;
                                                                                            return;
                                                                                        } else if (this instanceof AnonymousClass1EL) {
                                                                                            AnonymousClass1EL r335 = (AnonymousClass1EL) this;
                                                                                            ((AnonymousClass09P) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Amr, r335.A00.A02)).softReport("foreground_location_framework", "Request future failed", th);
                                                                                            ((C50882ew) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BM3, r335.A00.A02)).A07(null, null, null);
                                                                                            C54412mN r124 = r335.A00;
                                                                                            r124.A01 = null;
                                                                                            C90774Uz.A01((C90774Uz) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ANp, r124.A02), "FOREGROUND_LOCATION_CHECK_FAILED");
                                                                                            return;
                                                                                        } else if (this instanceof AnonymousClass1EN) {
                                                                                            AnonymousClass1EN r8 = (AnonymousClass1EN) this;
                                                                                            if (r8.A01) {
                                                                                                C50882ew r7 = r8.A00.A02;
                                                                                                try {
                                                                                                    C50882ew.A05(r7, true);
                                                                                                    C50882ew.A03(r7, true);
                                                                                                    C50882ew.A04(r7, false);
                                                                                                    C50882ew.A06(r7, true);
                                                                                                    r7.A05++;
                                                                                                    C22361La A005 = C50882ew.A00(r7, "fgl_write_fail");
                                                                                                    if (A005 != null) {
                                                                                                        long now = r7.A0C.now();
                                                                                                        A005.A03("write_duration_ms", now - r7.A0A);
                                                                                                        A005.A03("request_duration_ms", now - r7.A07);
                                                                                                        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
                                                                                                        objectNode.put("class", th.getClass().getSimpleName());
                                                                                                        if (th instanceof C55192ng) {
                                                                                                            GraphQLError graphQLError4 = ((C55192ng) th).error;
                                                                                                            if (graphQLError4 != null) {
                                                                                                                objectNode.put("graphql_error_code", graphQLError4.code);
                                                                                                            }
                                                                                                        } else if (th instanceof ExecutionException) {
                                                                                                            objectNode.put(AnonymousClass24B.$const$string(992), ((ExecutionException) th).getCause().getClass().getSimpleName());
                                                                                                        }
                                                                                                        A005.A04("write_throwable", objectNode);
                                                                                                        A005.A0A();
                                                                                                    }
                                                                                                    r7.A0A = Long.MIN_VALUE;
                                                                                                    r7.A08 = Long.MIN_VALUE;
                                                                                                    r7.A07 = Long.MIN_VALUE;
                                                                                                } catch (IllegalStateException unused2) {
                                                                                                }
                                                                                            }
                                                                                            r8.A00.A00 = null;
                                                                                            return;
                                                                                        } else if (this instanceof C20711Dn) {
                                                                                            C20711Dn r239 = (C20711Dn) this;
                                                                                            r239.A00.A07.setText(r239.A01);
                                                                                            return;
                                                                                        } else if (this instanceof AnonymousClass0lj) {
                                                                                            AnonymousClass0lj r421 = (AnonymousClass0lj) this;
                                                                                            if (r421 instanceof AnonymousClass1EV) {
                                                                                                return;
                                                                                            }
                                                                                            if (r421 instanceof AnonymousClass1EW) {
                                                                                                AnonymousClass1EW r422 = (AnonymousClass1EW) r421;
                                                                                                DownloadAttachmentDialogFragment downloadAttachmentDialogFragment = r422.A00;
                                                                                                C55962p2 r336 = downloadAttachmentDialogFragment.A09;
                                                                                                C55512oC A006 = C104114yK.A00(downloadAttachmentDialogFragment.A00);
                                                                                                A006.A05 = A006.A07.getString(C121665oc.A01());
                                                                                                A006.A01(2131821678);
                                                                                                r336.A02(A006.A00());
                                                                                                r422.A00.A22();
                                                                                                return;
                                                                                            } else if (r421 instanceof AnonymousClass148) {
                                                                                                AnonymousClass148 r423 = (AnonymousClass148) r421;
                                                                                                if (!(th instanceof ServiceException)) {
                                                                                                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, r423.A01.A00)).markerEnd(5505173, r423.A00, (short) ActionId.ABORTED);
                                                                                                    return;
                                                                                                } else {
                                                                                                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, r423.A01.A00)).markerEnd(5505173, r423.A00, (short) 433);
                                                                                                    return;
                                                                                                }
                                                                                            } else if (r421 instanceof AnonymousClass149) {
                                                                                                ((C20011Ao) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BSe, ((AnonymousClass149) r421).A01.A04)).A01("prefetchMoreFailed", "ThreadViewLoader", null, th);
                                                                                                return;
                                                                                            } else if (r421 instanceof AnonymousClass14A) {
                                                                                                AnonymousClass14A r424 = (AnonymousClass14A) r421;
                                                                                                C55462o7 r64 = r424.A01;
                                                                                                r64.A00 = null;
                                                                                                AnonymousClass3SU r425 = r424.A02;
                                                                                                ServiceException A007 = ServiceException.A00(th);
                                                                                                C55472o8 r125 = new C55472o8();
                                                                                                r125.A00 = A007;
                                                                                                r125.A01 = r64.A09;
                                                                                                C55482o9 r337 = new C55482o9(r125);
                                                                                                C20021Ap r040 = r64.A02;
                                                                                                if (r040 != null) {
                                                                                                    r040.Bd1(r425, r337);
                                                                                                    ((C20011Ao) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BSe, r64.A04)).A01("notifyLoadFailed", "ThreadViewLoader", r425, r337);
                                                                                                }
                                                                                                C55462o7.A09(r64, r425, r337);
                                                                                                r64.A09 = false;
                                                                                                C55492oA r426 = (C55492oA) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BU3, r64.A04);
                                                                                                boolean A01 = A007.A01(SQLException.class);
                                                                                                ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, r426.A00)).A03("android_messenger_thread_view_load_more_messages_failure");
                                                                                                if (A01) {
                                                                                                    ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, r426.A00)).A03("android_messenger_thread_view_load_more_messages_db_failure");
                                                                                                    return;
                                                                                                }
                                                                                                return;
                                                                                            } else if (r421 instanceof AnonymousClass14C) {
                                                                                                AnonymousClass14C r427 = (AnonymousClass14C) r421;
                                                                                                C55462o7 r338 = r427.A00;
                                                                                                r338.A01 = null;
                                                                                                AnonymousClass3SU r126 = r338.A06;
                                                                                                r338.A06 = null;
                                                                                                C55462o7.A0A(r338, r427.A01, th, r126);
                                                                                                return;
                                                                                            } else if (r421 instanceof C186014j) {
                                                                                                C186014j r428 = (C186014j) r421;
                                                                                                ServiceException A008 = ServiceException.A00(th);
                                                                                                C31371ja r65 = r428.A01;
                                                                                                r65.A01 = null;
                                                                                                C31371ja.A0E(r65, r428.A00, new C55452o6(A008, true));
                                                                                                ((QuickPerformanceLogger) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BBd, r65.A03)).markerEnd(5505136, r65.A00, (short) 3);
                                                                                                C15500vO r429 = (C15500vO) AnonymousClass1XX.A02(8, AnonymousClass1Y3.ATj, r428.A01.A03);
                                                                                                boolean A012 = A008.A01(SQLException.class);
                                                                                                ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, r429.A00)).A03("android_messenger_thread_list_load_more_threads_failure");
                                                                                                if (A012) {
                                                                                                    ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, r429.A00)).A03("android_messenger_thread_list_load_more_threads_db_failure");
                                                                                                    return;
                                                                                                }
                                                                                                return;
                                                                                            } else if (r421 instanceof C186214l) {
                                                                                                C186214l r430 = (C186214l) r421;
                                                                                                ServiceException A009 = ServiceException.A00(th);
                                                                                                C31371ja r72 = r430.A01;
                                                                                                r72.A02 = null;
                                                                                                C32971md r56 = r72.A07;
                                                                                                r72.A07 = null;
                                                                                                C32971md r339 = r430.A00;
                                                                                                C31371ja.A0E(r72, r339, new C55452o6(A009, r339.A04));
                                                                                                if (r56 != null) {
                                                                                                    C31371ja.A0F(r72, r56, "onFetchThreadsError");
                                                                                                }
                                                                                                C15500vO r431 = (C15500vO) AnonymousClass1XX.A02(8, AnonymousClass1Y3.ATj, r430.A01.A03);
                                                                                                boolean A013 = A009.A01(SQLException.class);
                                                                                                ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, r431.A00)).A03("android_messenger_thread_list_load_threads_failure");
                                                                                                if (A013) {
                                                                                                    ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, r431.A00)).A03("android_messenger_thread_list_load_threads_db_failure");
                                                                                                    return;
                                                                                                }
                                                                                                return;
                                                                                            } else if (r421 instanceof AnonymousClass0li) {
                                                                                                AnonymousClass0li r432 = (AnonymousClass0li) r421;
                                                                                                ((C149056vp) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ANo, r432.A02.A00)).A01("ThreadsPrefetcher", r432.A01, th, ImmutableMap.of(C99084oO.$const$string(3), r432.A00.A0G()));
                                                                                                return;
                                                                                            } else {
                                                                                                return;
                                                                                            }
                                                                                        } else if (!(this instanceof AnonymousClass1EQ)) {
                                                                                            C010708t.A0G(AnonymousClass0Y6.A0T, th, "Task failed.", new Object[0]);
                                                                                            return;
                                                                                        } else {
                                                                                            AnonymousClass1EQ r240 = (AnonymousClass1EQ) this;
                                                                                            AnonymousClass1EP r127 = r240.A01;
                                                                                            r127.A00 = null;
                                                                                            r127.A0C();
                                                                                            r240.A01.A01.Bd1(r240.A02, th);
                                                                                            return;
                                                                                        }
                                                                                        r26.softReport(str8, str7, th);
                                                                                        return;
                                                                                    } else {
                                                                                        AnonymousClass1EG r241 = (AnonymousClass1EG) this;
                                                                                        C209059tc.A01(r241.A00.A00, false);
                                                                                        r25 = r241.A01.A00;
                                                                                        str5 = "MessengerExtensionPaymentFetcher";
                                                                                        str6 = "Get payment info GQL query fails";
                                                                                    }
                                                                                    str6 = "Get granted permissions GQL query fails";
                                                                                } else {
                                                                                    C37221ut r242 = (C37221ut) this;
                                                                                    DUG dug = r242.A00;
                                                                                    AnonymousClass5t1 r041 = dug.A00.A01;
                                                                                    if (r041 != null) {
                                                                                        r041.CI0();
                                                                                    }
                                                                                    dug.A00.A03.A01();
                                                                                    r25 = r242.A01.A00;
                                                                                    str5 = "ReportTaskManager";
                                                                                    str6 = "Messenger platform report mutation fails.";
                                                                                }
                                                                                r25.CGS(str5, str6);
                                                                                return;
                                                                            } else {
                                                                                AnonymousClass1E9 r340 = (AnonymousClass1E9) this;
                                                                                r340.A01.A00.softReport("PlatformShareMutator", "Can't get request mutation result", th);
                                                                                C210769xk r042 = r340.A00;
                                                                                if (r042 != null) {
                                                                                    r042.BYh(th);
                                                                                    return;
                                                                                }
                                                                                return;
                                                                            }
                                                                            r04.BYh(th);
                                                                            return;
                                                                        } else {
                                                                            AnonymousClass8Z3 r341 = ((C37211us) this).A00;
                                                                            AnonymousClass3VL r043 = r341.A01;
                                                                            if (r043 != null) {
                                                                                r043.BXw(th);
                                                                            }
                                                                            ((C91084Wi) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AZ8, r341.A03.A00)).A01(r341.A02, AnonymousClass07B.A0C, th);
                                                                            return;
                                                                        }
                                                                        cSi.BYh(th);
                                                                        return;
                                                                    } else {
                                                                        C20721Do r243 = (C20721Do) this;
                                                                        if (r243.A00 != null) {
                                                                            r243.A01.A01.softReport("PollLoader", th);
                                                                            C53812lP r342 = r243.A00;
                                                                            C53822lQ r244 = r342.A00;
                                                                            r244.A03.setVisibility(8);
                                                                            r244.A02.setVisibility(0);
                                                                            C53822lQ r433 = r342.A00;
                                                                            r433.A07.A00(r433.A01.getString(2131830596), r433.A01.getString(2131830597));
                                                                            return;
                                                                        }
                                                                        return;
                                                                    }
                                                                    r3.A00(str4, resources.getString(i3));
                                                                    return;
                                                                } else {
                                                                    C20621De r57 = (C20621De) this;
                                                                    C51082fQ r044 = r57.A00;
                                                                    r044.A07.A01(r044.A0F, "services_request_appointment_create_error", r044.A0G, "message");
                                                                    r57.A00.A01.setVisibility(8);
                                                                    C51082fQ r128 = r57.A00;
                                                                    r128.A0A.A04(new C51092fR(r128.A1A(2131825404)));
                                                                    return;
                                                                }
                                                                C53992lh.A00(r13, r13.A1A(i2));
                                                                return;
                                                            } else {
                                                                r03 = ((C31341jX) this).A00.A00;
                                                                if (r03 == null) {
                                                                    return;
                                                                }
                                                            }
                                                            r03.AZc(th);
                                                            return;
                                                        } else {
                                                            C20601Dc r343 = (C20601Dc) this;
                                                            ShareLauncherActivity shareLauncherActivity = r343.A00;
                                                            shareLauncherActivity.A0N = null;
                                                            shareLauncherActivity.setResult(0);
                                                            r343.A00.finish();
                                                            return;
                                                        }
                                                        th.getMessage();
                                                        C54692mq.A07(r02);
                                                        return;
                                                    } else {
                                                        AnonymousClass1DX r245 = (AnonymousClass1DX) this;
                                                        C54752mw.A04(r245.A00, false);
                                                        r245.A00.A06.A04(new C51092fR(2131825404));
                                                        return;
                                                    }
                                                    rejectAppointmentActivity.A06.A03(new C51092fR(rejectAppointmentActivity.getResources().getString(2131825404)));
                                                    return;
                                                } else {
                                                    return;
                                                }
                                                C54782mz.A03(r12, th, simpleCheckoutData.A02());
                                                return;
                                            } else {
                                                AnonymousClass2n1 r344 = ((AnonymousClass1DM) this).A00;
                                                if ((th instanceof C55192ng) && ((C55192ng) th).error.code == 2078111) {
                                                    AnonymousClass2n2 r045 = r344.A00;
                                                    CBK cbk = r045.A0B;
                                                    SimpleCheckoutData simpleCheckoutData2 = r045.A04;
                                                    C12570pc A03 = cbk.A03(th, simpleCheckoutData2.A02().AxY(), simpleCheckoutData2.A01().A00);
                                                    ((PaymentsErrorActionDialog) A03).A04 = new C51482h5(r344);
                                                    if (r344.A00.A0E.A08()) {
                                                        AnonymousClass2n2 r046 = r344.A00;
                                                        r046.A0F.A04("checkout_payment_error_screen_displayed", r046.A04.A02().AxY());
                                                    }
                                                    r344.A00.A05.BvY(A03);
                                                }
                                                r344.A00.A09();
                                                return;
                                            }
                                            r24.A02(th, contactInfoCommonFormParams2, true);
                                            return;
                                        } else {
                                            C31311jU r345 = (C31311jU) this;
                                            C54232m5 r047 = r345.A01;
                                            r23 = r047.A03;
                                            r047.A00.getString(2131823192);
                                            contactInfoCommonFormParams = r345.A00;
                                        }
                                        r23.A02(th, contactInfoCommonFormParams, false);
                                        return;
                                    } else {
                                        AnonymousClass1D4 r346 = (AnonymousClass1D4) this;
                                        r346.A01.A01.A05(r346.A00.A01.Ay1().analyticsParams.paymentsLoggingSessionData, PaymentsFlowStep.A0o, th);
                                        r346.A01.A02.A04();
                                        r346.A01.A00.A02(new C51172g0(r346));
                                        return;
                                    }
                                    r1.A05(cardFormParams.Agg().cardFormAnalyticsParams, th);
                                    return;
                                } else {
                                    C20571Cz r246 = (C20571Cz) this;
                                    r246.A00.A00.A02(new C55422o3(r246));
                                    C37741wB r048 = (C37741wB) AnonymousClass0D4.A02(th, C37741wB.class);
                                    r246.A00.A02.CGZ("PaymentMethodsPickerScreenDataFetcher", AnonymousClass08S.A0J("Get Payment Methods Info for the logged-in user failed. ", r048 != null ? r048.getMessage() : th.getMessage()), th);
                                    return;
                                }
                            }
                        }
                        C55112nY.A01(r22, bka, paymentSettingsPickerRunTimeData);
                        return;
                    } else {
                        C20361Cd r434 = (C20361Cd) this;
                        r434.A02.A02.A05(r434.A01.A01.Ay1().analyticsParams.paymentsLoggingSessionData, PaymentsFlowStep.A1q, th);
                        r434.A02.A00.A02(new C49922d5(r434));
                        C37741wB r049 = (C37741wB) AnonymousClass0D4.A02(th, C37741wB.class);
                        str3 = r049 != null ? r049.getMessage() : th.getMessage();
                        r0 = r434.A02;
                    }
                    r0.A01.CGZ("ShippingPickerScreenDataFetcher", AnonymousClass08S.A0J("Get mailing addresses for the logged-in user failed. ", str3), th);
                    return;
                } else {
                    C20461Cn r247 = (C20461Cn) this;
                    C55122nZ r58 = r247.A02;
                    PaymentsLoggingSessionData paymentsLoggingSessionData4 = r247.A00;
                    r58.A02.getString(2131833005);
                    PaymentItemType paymentItemType4 = r247.A01;
                    r58.A05.A05(paymentsLoggingSessionData4, PaymentsFlowStep.A0e, th);
                    r58.A01.BvY(r58.A04.A03(th, paymentItemType4, paymentsLoggingSessionData4));
                    return;
                }
                r2.softReport(str, str2, th);
            } else {
                C20341Ca r248 = (C20341Ca) this;
                r248.A01.A00 = null;
                D69 d69 = r248.A00;
                if (d69 != null) {
                    d69.BYh(th);
                }
            }
        }
    }

    public void A03(CancellationException cancellationException) {
    }

    public void dispose() {
        this.A00 = true;
    }

    public boolean BEO() {
        return this.A00;
    }

    public final void BYh(Throwable th) {
        if (this.A00) {
            return;
        }
        if (th instanceof CancellationException) {
            A03((CancellationException) th);
        } else {
            A02(th);
        }
    }

    public final void Bqf(Object obj) {
        if (!this.A00) {
            A01(obj);
        }
    }
}
