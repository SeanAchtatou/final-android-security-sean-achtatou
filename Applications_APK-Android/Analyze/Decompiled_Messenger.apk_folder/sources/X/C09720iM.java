package X;

import com.google.common.util.concurrent.SettableFuture;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0iM  reason: invalid class name and case insensitive filesystem */
public final class C09720iM extends AnonymousClass0Wl {
    public static final Class A03 = C09720iM.class;
    private static volatile C09720iM A04;
    public AnonymousClass0UN A00;
    public String[] A01 = null;
    public final SettableFuture A02 = SettableFuture.create();

    public static C71813d4 A01(SettableFuture settableFuture, int i) {
        try {
            return (C71813d4) settableFuture.get((long) i, TimeUnit.MILLISECONDS);
        } catch (TimeoutException unused) {
            C010708t.A06(A03, "Timed out when trying to get OpenH264 codec paths");
            return null;
        } catch (InterruptedException e) {
            C010708t.A08(A03, "Interrupted when trying to get OpenH264 codec paths", e);
            return null;
        } catch (ExecutionException e2) {
            C010708t.A08(A03, "ExecutionException when trying to get OpenH264 codec paths", e2);
            return null;
        }
    }

    private static String A04(File file, String str) {
        if (file != null) {
            try {
                File file2 = new File(file, str);
                if (file2.exists()) {
                    return file2.getCanonicalPath();
                }
            } catch (IOException e) {
                C010708t.A08(A03, "IO Error trying to get the canonical path of a module", e);
                return null;
            }
        }
        return null;
    }

    public String getSimpleName() {
        return "RtcVoltronModulesLoader";
    }

    public static C71813d4 A00(C09720iM r3) {
        File A012 = ((C02380El) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B0R, r3.A00)).A01("openh264");
        String A032 = r3.A03(A012, "libopenh264libencoderAndroid.so");
        String A033 = r3.A03(A012, "libopenh264libdecoderAndroid.so");
        if (A032 == null || A033 == null) {
            return null;
        }
        return new C71813d4(A032, A033);
    }

    public static final C09720iM A02(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C09720iM.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C09720iM(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private C09720iM(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }

    private String A03(File file, String str) {
        String A042 = A04(file, str);
        if (A042 != null) {
            return A042;
        }
        Class cls = A03;
        C010708t.A0C(cls, "Error loading %s from Voltron. Trying fall-back", str);
        if (this.A01 == null) {
            this.A01 = AnonymousClass01q.A03().split(":");
        }
        for (String file2 : this.A01) {
            String A043 = A04(new File(file2), str);
            if (A043 != null) {
                return A043;
            }
        }
        C010708t.A0C(cls, "Failed to find fallback for %s", str);
        return null;
    }

    public void init() {
        int A032 = C000700l.A03(-2052966253);
        ((AnonymousClass0WP) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A8a, this.A00)).CIG("Trigger Voltron loading of OpenH264", new AnonymousClass0iQ(this), AnonymousClass0XV.APPLICATION_LOADED_HIGH_PRIORITY, AnonymousClass07B.A01);
        C000700l.A09(1747432606, A032);
    }
}
