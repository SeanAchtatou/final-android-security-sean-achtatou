package X;

import android.graphics.Color;

/* renamed from: X.09n  reason: invalid class name and case insensitive filesystem */
public final class C012609n {
    public static int A00(int i, float f) {
        return Color.rgb(Math.round(((float) Color.red(i)) * f), Math.round(((float) Color.green(i)) * f), Math.round(f * ((float) Color.blue(i))));
    }

    public static int A01(int i, float f) {
        int red = Color.red(i);
        int green = Color.green(i);
        int blue = Color.blue(i);
        float f2 = 1.0f - f;
        return Color.rgb(Math.round(((float) red) + (((float) (255 - red)) * f2)), Math.round(((float) green) + (((float) (255 - green)) * f2)), Math.round(((float) blue) + (((float) (255 - blue)) * f2)));
    }

    public static boolean A02(int i) {
        if (C15970wH.A00(i) < 0.7d) {
            return true;
        }
        return false;
    }

    public static boolean A03(int i) {
        if (C15970wH.A00(i) < 0.1d) {
            return true;
        }
        return false;
    }
}
