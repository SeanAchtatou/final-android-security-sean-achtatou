package X;

import com.facebook.inject.InjectorModule;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

@InjectorModule
/* renamed from: X.0sd  reason: invalid class name and case insensitive filesystem */
public final class C14110sd extends AnonymousClass0UV {
    public static final MigColorScheme A00(AnonymousClass1XY r0) {
        AnonymousClass40L.A00(r0);
        return C17190yT.A00();
    }

    public static final MigColorScheme A01(AnonymousClass1XY r0) {
        return C14120se.A00(r0).A01();
    }
}
