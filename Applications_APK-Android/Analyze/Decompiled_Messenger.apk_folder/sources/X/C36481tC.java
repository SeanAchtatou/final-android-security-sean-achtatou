package X;

import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;

/* renamed from: X.1tC  reason: invalid class name and case insensitive filesystem */
public final class C36481tC implements AnonymousClass1V0 {
    public final OmnistoreStoredProcedureComponent A00;

    public void Bgx(C29271g9 r4) {
        int provideStoredProcedureId = this.A00.provideStoredProcedureId();
        synchronized (r4) {
            C29271g9.A00(r4).addStoredProcedureResultCallback(new AnonymousClass2WR(this, provideStoredProcedureId));
        }
        this.A00.onSenderAvailable(new AnonymousClass2WS(r4, provideStoredProcedureId));
    }

    public void Bgy() {
        this.A00.onSenderInvalidated();
    }

    public C36481tC(OmnistoreStoredProcedureComponent omnistoreStoredProcedureComponent) {
        this.A00 = omnistoreStoredProcedureComponent;
    }
}
