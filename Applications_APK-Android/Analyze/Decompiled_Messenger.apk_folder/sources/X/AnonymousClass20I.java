package X;

import android.util.Log;

/* renamed from: X.20I  reason: invalid class name */
public final class AnonymousClass20I {
    public volatile Throwable A00;

    public void finalize() {
        int A03 = C000700l.A03(-1679928878);
        if (this != null) {
            try {
                if (this.A00 != null) {
                    Throwable th = this.A00;
                    int i = C29060EIy.A00.A00;
                    if (!((1 & i) == 0 || (i & 65536) == 0)) {
                        Log.e("AppStrictMode", "A resource was acquired and never released.", th);
                    }
                }
            } catch (Throwable th2) {
                super.finalize();
                C000700l.A09(1351534551, A03);
                throw th2;
            }
        }
        super.finalize();
        C000700l.A09(579410278, A03);
    }
}
