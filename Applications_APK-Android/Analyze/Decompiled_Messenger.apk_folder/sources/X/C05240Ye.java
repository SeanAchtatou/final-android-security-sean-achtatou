package X;

import android.os.Environment;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Ye  reason: invalid class name and case insensitive filesystem */
public final class C05240Ye implements AnonymousClass1YQ, AnonymousClass015 {
    private static volatile C05240Ye A02;
    private AnonymousClass0UN A00;
    private final C05250Yf A01;

    public String getSimpleName() {
        return "MemoryDumpHandler";
    }

    public static final C05240Ye A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C05240Ye.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C05240Ye(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public void handleUncaughtException(Thread thread, Throwable th, C02120Da r8) {
        if (this.A01.A00.AbO(93, false) || (th instanceof OutOfMemoryError)) {
            AnonymousClass24P r4 = (AnonymousClass24P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B4K, this.A00);
            while (true) {
                try {
                    Throwable cause = th.getCause();
                    if (cause == null) {
                        break;
                    }
                    th = cause;
                } catch (Throwable th2) {
                    C010708t.A0M(AnonymousClass80H.$const$string(35), "Error writing Hprof dump", th2);
                    r4.A01.Bz2("hprof", AnonymousClass08S.A0J("Failed - ", th2.getMessage()));
                    return;
                }
            }
            String name = th.getClass().getName();
            if (r4.A02.A00.AbO(94, false)) {
                AnonymousClass24P.A01(r4, name);
            } else if (r4.A02.A00.AbO(92, false)) {
                AnonymousClass24P.A02(r4, name, Environment.getExternalStorageDirectory().getPath());
            }
        }
    }

    private C05240Ye(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = C05250Yf.A00(r3);
    }

    public void init() {
        int A03 = C000700l.A03(-772879037);
        AnonymousClass016.A02(this, -100);
        C000700l.A09(-82461464, A03);
    }
}
