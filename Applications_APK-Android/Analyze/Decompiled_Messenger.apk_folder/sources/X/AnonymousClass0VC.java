package X;

/* renamed from: X.0VC  reason: invalid class name */
public abstract class AnonymousClass0VC implements C04310Tq {
    private final AnonymousClass1XY A00;
    private final C24851Xi A01;

    public Object A01(AnonymousClass1XY r5) {
        if (!(this instanceof AnonymousClass0VB)) {
            AnonymousClass0VG r1 = (AnonymousClass0VG) this;
            try {
                return AnonymousClass1Y4.A00(r1.A00, r5);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException(String.format("Invalid binding id %d", Integer.valueOf(r1.A00)), e);
            }
        } else {
            AnonymousClass0VB r12 = (AnonymousClass0VB) this;
            try {
                return AnonymousClass1Y4.A00(r12.A00, r5);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException(String.format("Invalid binding id %d", Integer.valueOf(r12.A00)), e2);
            }
        }
    }

    public final Object get() {
        C24781Xb scopeUnawareInjector = this.A00.getScopeUnawareInjector();
        Object AYf = this.A01.AYf();
        try {
            return A01(scopeUnawareInjector);
        } finally {
            this.A01.AZH(AYf);
        }
    }

    public AnonymousClass0VC(AnonymousClass1XY r2) {
        this.A00 = r2;
        this.A01 = r2.getScopeAwareInjector();
    }
}
