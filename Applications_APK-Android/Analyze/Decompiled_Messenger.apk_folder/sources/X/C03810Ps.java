package X;

import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.facebook.tigon.iface.TigonRequest;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.inject.Singleton;
import org.apache.http.message.BasicNameValuePair;

@Singleton
/* renamed from: X.0Ps  reason: invalid class name and case insensitive filesystem */
public final class C03810Ps implements C10810kt {
    private static volatile C03810Ps A01 = null;
    public static final String __redex_internal_original_name = "com.facebook.profilo.upload.ProfiloUploadMethod";
    private final C25921ac A00;

    public static final C03810Ps A02(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C03810Ps.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C03810Ps(C25901aa.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C03810Ps(C25921ac r1) {
        this.A00 = r1;
    }

    /* access modifiers changed from: private */
    /* renamed from: A00 */
    public C47702Xm B0t(File file) {
        String str;
        if (file.getName().contains(".zip")) {
            str = "application/zip";
        } else {
            str = "application/x-gzip";
        }
        C183328eo r5 = new C183328eo(file, str, file.getName(), new C03800Pr());
        List asList = Arrays.asList(new BasicNameValuePair("type", "loom"), new BasicNameValuePair("device_id", this.A00.B7Z()));
        AnonymousClass2U5 r3 = new AnonymousClass2U5("file", r5);
        AnonymousClass2U7 r2 = new AnonymousClass2U7();
        r2.A0B = "profiloUpload";
        r2.A0D = "me/traces";
        r2.A0C = TigonRequest.POST;
        Integer num = AnonymousClass07B.A01;
        r2.A05 = num;
        r2.A0G = Arrays.asList(r3);
        r2.A0H = asList;
        r2.A07 = num;
        return r2.A01();
    }

    public static final C03810Ps A01(AnonymousClass1XY r0) {
        return A02(r0);
    }

    public Object B1B(Object obj, AnonymousClass2U8 r6) {
        r6.A04();
        JsonNode A012 = r6.A01();
        boolean z = false;
        if (r6.A00 == 200 && A012.hasNonNull(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS) && A012.get(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS).asBoolean(false)) {
            z = true;
        }
        return Boolean.valueOf(z);
    }
}
