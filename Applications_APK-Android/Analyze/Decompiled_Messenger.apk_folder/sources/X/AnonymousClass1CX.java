package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1CX  reason: invalid class name */
public final class AnonymousClass1CX {
    public final List A00;

    public List A00(int i, int i2, int i3) {
        if (i < 0) {
            i = 0;
        }
        int i4 = i + i3;
        if (i2 < 0) {
            i2 = 0;
        }
        int i5 = i3 + i2;
        ArrayList arrayList = new ArrayList();
        int size = this.A00.size();
        for (int i6 = 0; i6 < size; i6++) {
            AnonymousClass1IH r3 = (AnonymousClass1IH) this.A00.get(i6);
            int i7 = r3.A01;
            boolean z = false;
            if (Math.max(i4, i7) <= Math.min(i5, (r3.A00 + i7) - 1)) {
                z = true;
            }
            if (z) {
                arrayList.add(r3);
            }
        }
        return arrayList;
    }

    public AnonymousClass1CX(List list) {
        this.A00 = Collections.unmodifiableList(list);
    }
}
