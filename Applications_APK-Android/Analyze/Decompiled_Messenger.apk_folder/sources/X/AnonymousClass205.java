package X;

import com.facebook.litho.LithoView;
import com.facebook.messenger.neue.MessengerMePreferenceActivity;

/* renamed from: X.205  reason: invalid class name */
public final class AnonymousClass205 implements C199319Zu {
    public final /* synthetic */ MessengerMePreferenceActivity A00;

    public AnonymousClass205(MessengerMePreferenceActivity messengerMePreferenceActivity) {
        this.A00 = messengerMePreferenceActivity;
    }

    public C199329Zv AhZ() {
        return new C199279Zq();
    }

    public void BtO(int i) {
        LithoView lithoView = this.A00.A02;
        if (lithoView != null) {
            C15980wI.A00(lithoView, i);
        }
    }
}
