package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0od  reason: invalid class name and case insensitive filesystem */
public final class C12150od {
    private static volatile C12150od A00;

    public static final C12150od A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C12150od.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C12150od();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static ThreadKey A01(long j) {
        if (j == 0) {
            return null;
        }
        return ThreadKey.A01(j);
    }

    public static boolean A02(ThreadKey threadKey) {
        if (threadKey == null) {
            return false;
        }
        return threadKey.A0M();
    }
}
