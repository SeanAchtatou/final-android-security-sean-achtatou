package X;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1rS  reason: invalid class name and case insensitive filesystem */
public class C35581rS extends C27441dC {
    public final Map A00 = new ConcurrentHashMap();

    public final Runnable A00(Runnable runnable, String str) {
        Runnable A002 = C25231Yv.A00(AnonymousClass08S.A0J(this.A01, str), runnable, this.A00);
        if (A002 == runnable) {
            return A002;
        }
        AnonymousClass44Z r1 = new AnonymousClass44Z(this.A00, runnable, A002);
        this.A00.put(runnable, r1);
        return r1;
    }

    public C35581rS(String str, int i) {
        super(str, i);
    }
}
