package X;

import com.facebook.mobileconfig.MobileConfigCxxChangeListener;
import com.facebook.mobileconfig.MobileConfigEmergencyPushChangeListener;
import com.facebook.tigon.iface.TigonServiceHolder;
import io.card.payment.BuildConfig;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0WX  reason: invalid class name */
public final class AnonymousClass0WX implements AnonymousClass0WV {
    public AtomicReference A00 = new AtomicReference(BuildConfig.FLAVOR);

    public void clearAlternativeUpdater() {
    }

    public void clearOverrides() {
    }

    public void deleteOldUserData(int i) {
    }

    public String getConsistencyLoggingFlagsJSON() {
        return "MobileConfigManagerHolderNoop: MobileConfig manager not yet initialized";
    }

    public String getFrameworkStatus() {
        return "UNINITIALIZED";
    }

    public C06190b2 getLatestHandle() {
        return null;
    }

    public C25851aV getNewOverridesTable() {
        return null;
    }

    public C25851aV getNewOverridesTableIfExists() {
        return null;
    }

    public boolean isConsistencyLoggingNeeded(AnonymousClass8ZO r2) {
        return false;
    }

    public boolean isFetchNeeded() {
        return false;
    }

    public boolean isTigonServiceSet() {
        return false;
    }

    public boolean isValid() {
        return false;
    }

    public void logConfigs(String str, AnonymousClass8ZO r2, Map map) {
    }

    public void logExposure(String str, String str2, String str3) {
    }

    public void logShadowResult(String str, String str2, String str3, String str4, String str5, String str6) {
    }

    public void logStorageConsistency() {
    }

    public boolean registerConfigChangeListener(MobileConfigCxxChangeListener mobileConfigCxxChangeListener) {
        return false;
    }

    public boolean setEpHandler(MobileConfigEmergencyPushChangeListener mobileConfigEmergencyPushChangeListener) {
        return false;
    }

    public boolean setSandboxURL(String str) {
        return false;
    }

    public void setTigonService(TigonServiceHolder tigonServiceHolder, boolean z) {
    }

    public boolean tryUpdateConfigsSynchronously(int i) {
        return false;
    }

    public boolean updateConfigsSynchronouslyWithDefaultUpdater(int i) {
        return false;
    }

    public boolean updateEmergencyPushConfigs() {
        return false;
    }

    public boolean updateEmergencyPushConfigsSynchronously(int i) {
        return false;
    }

    public String syncFetchReason() {
        return AnonymousClass08S.A0J("MobileConfigManagerHolderNoop: ", (String) this.A00.get());
    }

    public boolean updateConfigs() {
        return false;
    }
}
