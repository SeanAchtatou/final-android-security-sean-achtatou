package X;

import com.facebook.litho.annotations.Comparable;
import java.util.List;

/* renamed from: X.1AM  reason: invalid class name */
public final class AnonymousClass1AM extends C16070wR {
    public AnonymousClass10N A00;
    public AnonymousClass10N A01;
    public AnonymousClass10N A02;
    @Comparable(type = 13)
    public Boolean A03;
    @Comparable(type = 5)
    public List A04;

    public AnonymousClass1AM() {
        super("DataDiffSection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0071, code lost:
        if (r1 != false) goto L_0x0073;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(X.AnonymousClass1GA r28, X.AnonymousClass1CW r29, X.C16070wR r30, X.C16070wR r31) {
        /*
            r27 = this;
            r5 = r30
            r4 = r31
            X.1AM r5 = (X.AnonymousClass1AM) r5
            X.1AM r4 = (X.AnonymousClass1AM) r4
            X.1mx r2 = new X.1mx
            r3 = 0
            if (r5 != 0) goto L_0x0123
            r1 = r3
        L_0x000e:
            if (r4 != 0) goto L_0x011f
            r0 = r3
        L_0x0011:
            r2.<init>(r1, r0)
            X.1mx r11 = new X.1mx
            if (r5 != 0) goto L_0x011b
            r0 = r3
        L_0x0019:
            if (r4 == 0) goto L_0x001d
            java.lang.Boolean r3 = r4.A03
        L_0x001d:
            r11.<init>(r0, r3)
            java.lang.Object r6 = r2.A01
            java.util.List r6 = (java.util.List) r6
            java.lang.Object r5 = r2.A00
            java.util.List r5 = (java.util.List) r5
            X.1Hs r4 = new X.1Hs
            r3 = r28
            X.0wR r0 = r3.A0K()
            if (r0 != 0) goto L_0x0111
            r0 = 0
        L_0x0033:
            r10 = 0
            r4.<init>(r0, r3)
            X.1Ht r7 = new X.1Ht
            r0 = r29
            r7.<init>(r0)
            boolean r9 = X.C27041cY.A02()
            X.1RK r8 = new X.1RK
            java.lang.Object r1 = r2.A01
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r0 = r2.A00
            java.util.List r0 = (java.util.List) r0
            r8.<init>(r3, r1, r0)
            X.38i r2 = r3.A05()
            if (r2 == 0) goto L_0x005f
            r0 = 12
            X.3eb r0 = r2.BLg(r3, r0)
            X.3eb r10 = X.C637138j.A00(r3, r2, r0)
        L_0x005f:
            if (r9 == 0) goto L_0x0066
            java.lang.String r0 = "DiffUtil.calculateDiff"
            X.C27041cY.A01(r0)
        L_0x0066:
            java.lang.Object r0 = r11.A00
            if (r0 == 0) goto L_0x0073
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r1 = r0.booleanValue()
            r0 = 0
            if (r1 == 0) goto L_0x0074
        L_0x0073:
            r0 = 1
        L_0x0074:
            X.1IQ r0 = X.AnonymousClass1IE.A00(r8, r0)
            if (r9 == 0) goto L_0x007d
            X.C27041cY.A00()
        L_0x007d:
            if (r10 == 0) goto L_0x0082
            r2.BJI(r10)
        L_0x0082:
            X.1IS r2 = new X.1IS
            r2.<init>(r6, r5, r4, r7)
            r0.A02(r2)
            boolean r16 = X.C27041cY.A02()
            java.util.List r0 = r2.A04
            java.lang.String r4 = "renderInfo:"
            r1 = 0
            if (r0 == 0) goto L_0x01ed
            int r5 = r0.size()
            java.util.List r0 = r2.A06
            int r0 = r0.size()
            if (r5 == r0) goto L_0x01ed
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r0 = "Inconsistent size between mPlaceholders("
            r7.<init>(r0)
            java.util.List r0 = r2.A06
            int r0 = r0.size()
            r7.append(r0)
            java.lang.String r0 = ") and mNextData("
            r7.append(r0)
            java.util.List r0 = r2.A04
            int r0 = r0.size()
            r7.append(r0)
            java.lang.String r0 = "); "
            r7.append(r0)
            java.lang.String r0 = "mOperations: ["
            r7.append(r0)
            java.util.List r0 = r2.A05
            int r10 = r0.size()
            r8 = 0
            r9 = 0
        L_0x00d1:
            java.lang.String r6 = "], "
            if (r9 >= r10) goto L_0x0127
            java.util.List r0 = r2.A05
            java.lang.Object r5 = r0.get(r9)
            X.1IW r5 = (X.AnonymousClass1IW) r5
            java.lang.String r0 = "[type="
            r7.append(r0)
            int r0 = r5.A02
            r7.append(r0)
            java.lang.String r0 = ", index="
            r7.append(r0)
            int r0 = r5.A00
            r7.append(r0)
            java.lang.String r0 = ", toIndex="
            r7.append(r0)
            int r0 = r5.A01
            r7.append(r0)
            java.util.List r5 = r5.A03
            if (r5 == 0) goto L_0x010b
            java.lang.String r0 = ", count="
            r7.append(r0)
            int r0 = r5.size()
            r7.append(r0)
        L_0x010b:
            r7.append(r6)
            int r9 = r9 + 1
            goto L_0x00d1
        L_0x0111:
            X.0wR r0 = r3.A0K()
            X.1AM r0 = (X.AnonymousClass1AM) r0
            X.10N r0 = r0.A02
            goto L_0x0033
        L_0x011b:
            java.lang.Boolean r0 = r5.A03
            goto L_0x0019
        L_0x011f:
            java.util.List r0 = r4.A04
            goto L_0x0011
        L_0x0123:
            java.util.List r1 = r5.A04
            goto L_0x000e
        L_0x0127:
            java.lang.String r0 = "]; "
            r7.append(r0)
            java.lang.String r0 = "mNextData: ["
            r7.append(r0)
            java.util.List r0 = r2.A04
            int r5 = r0.size()
        L_0x0137:
            if (r8 >= r5) goto L_0x014d
            java.lang.String r0 = "["
            r7.append(r0)
            java.util.List r0 = r2.A04
            java.lang.Object r0 = r0.get(r8)
            r7.append(r0)
            r7.append(r6)
            int r8 = r8 + 1
            goto L_0x0137
        L_0x014d:
            java.lang.String r0 = "]"
            r7.append(r0)
            java.lang.Integer r6 = X.AnonymousClass07B.A01
            java.lang.String r5 = r7.toString()
            java.lang.String r0 = "RecyclerBinderUpdateCallback:InconsistentSize"
            X.C09070gU.A01(r6, r0, r5)
            java.util.List r0 = r2.A05
            r0.clear()
            java.util.List r0 = r2.A03
            r0.clear()
            java.util.List r0 = r2.A06
            r0.clear()
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            r7 = 0
        L_0x0172:
            int r0 = r2.A00
            r5 = 0
            if (r7 >= r0) goto L_0x0188
            X.1mx r6 = new X.1mx
            java.util.List r0 = r2.A07
            java.lang.Object r0 = r0.get(r7)
            r6.<init>(r0, r5)
            r11.add(r6)
            int r7 = r7 + 1
            goto L_0x0172
        L_0x0188:
            java.util.List r0 = r2.A03
            r0.addAll(r11)
            java.util.List r0 = r2.A05
            X.1IW r6 = new X.1IW
            r7 = 2
            r8 = 0
            int r9 = r2.A00
            r10 = 0
            r6.<init>(r7, r8, r9, r10, r11)
            r0.add(r6)
            java.util.List r0 = r2.A04
            int r9 = r0.size()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>(r9)
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>(r9)
        L_0x01ac:
            if (r8 >= r9) goto L_0x0244
            java.util.List r0 = r2.A04
            java.lang.Object r7 = r0.get(r8)
            if (r16 == 0) goto L_0x01c6
            boolean r0 = r7 instanceof X.AnonymousClass3X4
            if (r0 == 0) goto L_0x01e4
            r0 = r7
            X.3X4 r0 = (X.AnonymousClass3X4) r0
            java.lang.String r0 = r0.A03
        L_0x01bf:
            java.lang.String r0 = X.AnonymousClass08S.A0J(r4, r0)
            X.C27041cY.A01(r0)
        L_0x01c6:
            X.1Hs r0 = r2.A01
            X.1Ih r6 = r0.A00(r7, r8)
            if (r16 == 0) goto L_0x01d1
            X.C27041cY.A00()
        L_0x01d1:
            X.1IV r0 = new X.1IV
            r0.<init>(r6, r1)
            r10.add(r8, r0)
            X.1mx r0 = new X.1mx
            r0.<init>(r5, r7)
            r11.add(r0)
            int r8 = r8 + 1
            goto L_0x01ac
        L_0x01e4:
            java.lang.Class r0 = r7.getClass()
            java.lang.String r0 = r0.getSimpleName()
            goto L_0x01bf
        L_0x01ed:
            java.util.List r0 = r2.A06
            int r7 = r0.size()
        L_0x01f3:
            if (r1 >= r7) goto L_0x025b
            java.util.List r0 = r2.A06
            java.lang.Object r0 = r0.get(r1)
            X.1IV r0 = (X.AnonymousClass1IV) r0
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x0238
            java.util.List r0 = r2.A04
            java.lang.Object r6 = r0.get(r1)
            if (r16 == 0) goto L_0x0219
            boolean r0 = r6 instanceof X.AnonymousClass3X4
            if (r0 == 0) goto L_0x023b
            r0 = r6
            X.3X4 r0 = (X.AnonymousClass3X4) r0
            java.lang.String r0 = r0.A03
        L_0x0212:
            java.lang.String r0 = X.AnonymousClass08S.A0J(r4, r0)
            X.C27041cY.A01(r0)
        L_0x0219:
            java.util.List r0 = r2.A06
            java.lang.Object r5 = r0.get(r1)
            X.1IV r5 = (X.AnonymousClass1IV) r5
            X.1Hs r0 = r2.A01
            X.1Ih r0 = r0.A00(r6, r1)
            r5.A00 = r0
            if (r16 == 0) goto L_0x022e
            X.C27041cY.A00()
        L_0x022e:
            java.util.List r0 = r2.A03
            java.lang.Object r0 = r0.get(r1)
            X.1mx r0 = (X.C33111mx) r0
            r0.A00 = r6
        L_0x0238:
            int r1 = r1 + 1
            goto L_0x01f3
        L_0x023b:
            java.lang.Class r0 = r6.getClass()
            java.lang.String r0 = r0.getSimpleName()
            goto L_0x0212
        L_0x0244:
            java.util.List r0 = r2.A06
            r0.addAll(r10)
            java.util.List r0 = r2.A03
            r0.addAll(r11)
            java.util.List r0 = r2.A05
            X.1IW r6 = new X.1IW
            r7 = 0
            r8 = 0
            r9 = -1
            r6.<init>(r7, r8, r9, r10, r11)
            r0.add(r6)
        L_0x025b:
            if (r16 == 0) goto L_0x0262
            java.lang.String r0 = "executeOperations"
            X.C27041cY.A01(r0)
        L_0x0262:
            X.1Ht r1 = r2.A02
            java.util.List r0 = r2.A05
            r26 = r0
            int r4 = r26.size()
            r0 = 0
            r2 = 0
        L_0x026e:
            if (r2 >= r4) goto L_0x044a
            r5 = r26
            java.lang.Object r8 = r5.get(r2)
            X.1IW r8 = (X.AnonymousClass1IW) r8
            java.util.List r11 = r8.A03
            java.util.List r12 = r8.A04
            r10 = 1
            if (r11 != 0) goto L_0x0444
            r6 = 1
        L_0x0280:
            int r7 = r8.A02
            if (r7 == 0) goto L_0x039c
            if (r7 == r10) goto L_0x02fb
            r5 = 2
            if (r7 == r5) goto L_0x02a4
            r5 = 3
            if (r7 != r5) goto L_0x02a1
            X.1CW r9 = r1.A00
            int r7 = r8.A00
            int r6 = r8.A01
            java.lang.Object r5 = r12.get(r0)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r5 = r5.A00
            X.1IH r5 = X.AnonymousClass1IH.A01(r7, r6, r5)
            r9.A04(r5)
        L_0x02a1:
            int r2 = r2 + 1
            goto L_0x026e
        L_0x02a4:
            int r9 = r8.A01
            if (r9 != r10) goto L_0x02c2
            X.1CW r10 = r1.A00
            int r9 = r8.A00
            java.lang.Object r5 = r12.get(r0)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r8 = r5.A01
            X.1Ih r7 = X.C21621Ib.A01()
            r6 = 3
            r5 = 0
            X.1IH r5 = X.AnonymousClass1IH.A00(r6, r9, r7, r8, r5)
            r10.A04(r5)
            goto L_0x02a1
        L_0x02c2:
            X.1CW r7 = r1.A00
            int r8 = r8.A00
            int r11 = r12.size()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>(r11)
            r10 = 0
        L_0x02d0:
            if (r10 >= r11) goto L_0x02e0
            java.lang.Object r5 = r12.get(r10)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r5 = r5.A01
            r6.add(r5)
            int r10 = r10 + 1
            goto L_0x02d0
        L_0x02e0:
            java.util.List r23 = X.AnonymousClass1IH.A08
            r25 = 0
            r22 = 0
            X.1IH r5 = new X.1IH
            r18 = -3
            r20 = -1
            r19 = r8
            r21 = r9
            r24 = r6
            r17 = r5
            r17.<init>(r18, r19, r20, r21, r22, r23, r24, r25)
            r7.A04(r5)
            goto L_0x02a1
        L_0x02fb:
            if (r6 != r10) goto L_0x032c
            X.1CW r9 = r1.A00
            int r13 = r8.A00
            java.lang.Object r5 = r11.get(r0)
            X.1IV r5 = (X.AnonymousClass1IV) r5
            X.1Ih r11 = r5.A00
            X.1KE r10 = r3.A08()
            java.lang.Object r5 = r12.get(r0)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r8 = r5.A01
            java.lang.Object r5 = r12.get(r0)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r7 = r5.A00
            X.1In r6 = new X.1In
            r6.<init>(r11, r10)
            r5 = 2
            X.1IH r5 = X.AnonymousClass1IH.A00(r5, r13, r6, r8, r7)
            r9.A04(r5)
            goto L_0x02a1
        L_0x032c:
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>(r6)
            r7 = 0
        L_0x0332:
            if (r7 >= r6) goto L_0x0342
            java.lang.Object r5 = r11.get(r7)
            X.1IV r5 = (X.AnonymousClass1IV) r5
            X.1Ih r5 = r5.A00
            r15.add(r5)
            int r7 = r7 + 1
            goto L_0x0332
        L_0x0342:
            X.1CW r10 = r1.A00
            int r9 = r8.A00
            X.1KE r14 = r3.A08()
            int r11 = r12.size()
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>(r11)
            r7 = 0
        L_0x0354:
            if (r7 >= r11) goto L_0x0364
            java.lang.Object r5 = r12.get(r7)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r5 = r5.A01
            r8.add(r5)
            int r7 = r7 + 1
            goto L_0x0354
        L_0x0364:
            int r13 = r12.size()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>(r13)
            r11 = 0
        L_0x036e:
            if (r11 >= r13) goto L_0x037e
            java.lang.Object r5 = r12.get(r11)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r5 = r5.A00
            r7.add(r5)
            int r11 = r11 + 1
            goto L_0x036e
        L_0x037e:
            java.util.List r23 = X.AnonymousClass1CW.A02(r15, r14)
            r22 = 0
            X.1IH r5 = new X.1IH
            r18 = -2
            r20 = -1
            r19 = r9
            r21 = r6
            r24 = r8
            r25 = r7
            r17 = r5
            r17.<init>(r18, r19, r20, r21, r22, r23, r24, r25)
            r10.A04(r5)
            goto L_0x02a1
        L_0x039c:
            if (r6 != r10) goto L_0x03d1
            X.1CW r9 = r1.A00
            int r10 = r8.A00
            java.lang.Object r5 = r11.get(r0)
            X.1IV r5 = (X.AnonymousClass1IV) r5
            X.1Ih r13 = r5.A00
            X.1KE r11 = r3.A08()
            java.lang.Object r5 = r12.get(r0)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r8 = r5.A00
            X.0wR r5 = r9.A02
            if (r5 == 0) goto L_0x03c1
            java.lang.String r6 = r5.A04
            java.lang.String r5 = "section_global_key"
            r13.AMk(r5, r6)
        L_0x03c1:
            X.1In r7 = new X.1In
            r7.<init>(r13, r11)
            r6 = 1
            r5 = 0
            X.1IH r5 = X.AnonymousClass1IH.A00(r6, r10, r7, r5, r8)
            r9.A04(r5)
            goto L_0x02a1
        L_0x03d1:
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>(r6)
            r7 = 0
        L_0x03d7:
            if (r7 >= r6) goto L_0x03e7
            java.lang.Object r5 = r11.get(r7)
            X.1IV r5 = (X.AnonymousClass1IV) r5
            X.1Ih r5 = r5.A00
            r15.add(r5)
            int r7 = r7 + 1
            goto L_0x03d7
        L_0x03e7:
            X.1CW r9 = r1.A00
            int r8 = r8.A00
            X.1KE r14 = r3.A08()
            int r11 = r12.size()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>(r11)
            r10 = 0
        L_0x03f9:
            if (r10 >= r11) goto L_0x0409
            java.lang.Object r5 = r12.get(r10)
            X.1mx r5 = (X.C33111mx) r5
            java.lang.Object r5 = r5.A00
            r7.add(r5)
            int r10 = r10 + 1
            goto L_0x03f9
        L_0x0409:
            X.0wR r5 = r9.A02
            if (r5 == 0) goto L_0x0426
            r13 = 0
            int r12 = r15.size()
        L_0x0412:
            if (r13 >= r12) goto L_0x0426
            java.lang.Object r11 = r15.get(r13)
            X.1Ih r11 = (X.C21681Ih) r11
            X.0wR r5 = r9.A02
            java.lang.String r10 = r5.A04
            java.lang.String r5 = "section_global_key"
            r11.AMk(r5, r10)
            int r13 = r13 + 1
            goto L_0x0412
        L_0x0426:
            java.util.List r23 = X.AnonymousClass1CW.A02(r15, r14)
            r24 = 0
            r22 = 0
            X.1IH r5 = new X.1IH
            r18 = -1
            r20 = -1
            r19 = r8
            r21 = r6
            r25 = r7
            r17 = r5
            r17.<init>(r18, r19, r20, r21, r22, r23, r24, r25)
            r9.A04(r5)
            goto L_0x02a1
        L_0x0444:
            int r6 = r11.size()
            goto L_0x0280
        L_0x044a:
            if (r16 == 0) goto L_0x044f
            X.C27041cY.A00()
        L_0x044f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AM.A0F(X.1GA, X.1CW, X.0wR, X.0wR):void");
    }
}
