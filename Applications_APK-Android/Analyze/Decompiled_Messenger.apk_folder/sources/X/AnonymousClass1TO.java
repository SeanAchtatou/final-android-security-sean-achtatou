package X;

import com.google.common.base.MoreObjects;

/* renamed from: X.1TO  reason: invalid class name */
public final class AnonymousClass1TO {
    public final int A00;
    public final boolean A01;
    public final boolean A02;
    public final boolean A03;

    public AnonymousClass1TO(boolean z, boolean z2, int i, boolean z3) {
        this.A02 = z;
        this.A03 = z2;
        this.A00 = i;
        this.A01 = z3;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("loadWasFromUserAction", this.A02);
        stringHelper.add("queryStaleData", this.A03);
        stringHelper.add("maxItemsToFetch", this.A00);
        return stringHelper.toString();
    }
}
