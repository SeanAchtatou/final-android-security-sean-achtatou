package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0GY  reason: invalid class name */
public final class AnonymousClass0GY implements AnonymousClass1YQ {
    private static volatile AnonymousClass0GY A01;
    private final AnonymousClass09P A00;

    public String getSimpleName() {
        return "LogSmartGcErrorInit";
    }

    public static final AnonymousClass0GY A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0GY.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0GY(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final AnonymousClass0US A01(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.A1B, r1);
    }

    private AnonymousClass0GY(AnonymousClass1XY r2) {
        this.A00 = C04750Wa.A01(r2);
    }

    public void init() {
        String errorMessage;
        int A03 = C000700l.A03(1131168030);
        if (!C011108x.A00) {
            C000700l.A09(1323343553, A03);
            return;
        }
        AnonymousClass0A0 r2 = AnonymousClass08D.A00;
        boolean z = false;
        if (r2 != C02920Gz.A00) {
            z = true;
        }
        if (!z) {
            errorMessage = null;
        } else {
            errorMessage = r2.getErrorMessage();
        }
        if (errorMessage == null || errorMessage.isEmpty()) {
            C000700l.A09(-300186339, A03);
            return;
        }
        this.A00.CGT("SmartGcInitError", errorMessage, 1000000);
        C000700l.A09(-916360338, A03);
    }
}
