package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1JW  reason: invalid class name */
public final class AnonymousClass1JW extends C17770zR {
    public static final MigColorScheme A03 = C17190yT.A00();
    public AnonymousClass10N A00;
    @Comparable(type = 13)
    public C43842Fx A01;
    @Comparable(type = 13)
    public MigColorScheme A02 = A03;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1JW() {
        super("CountrySpinnerComponent");
    }
}
