package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1KF  reason: invalid class name */
public final class AnonymousClass1KF extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public MigColorScheme A01;
    @Comparable(type = 5)
    public ImmutableList A02;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1KF(Context context) {
        super("MontageInboxIdentityItemBlinker");
        this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }
}
