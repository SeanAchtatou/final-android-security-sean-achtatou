package X;

import android.os.Process;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.quicklog.PerformanceLoggingEvent;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.systrace.TraceDirect;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0ep  reason: invalid class name and case insensitive filesystem */
public abstract class C08190ep implements AnonymousClass0Td {
    public void BeF(C04270Tg r38) {
        if (this instanceof C25841aU) {
            C25841aU r9 = (C25841aU) this;
            synchronized (r9) {
                C04270Tg r2 = r38;
                if (r2.A02 == 46333953) {
                    C72993fK r8 = (C72993fK) r9.A02.get();
                    C25841aU r0 = r8.A02;
                    ArrayList<C73003fL> arrayList = new ArrayList<>();
                    for (C26111av r4 : r0.A01) {
                        String str = r4.A00.A00;
                        ArrayList arrayList2 = new ArrayList(r4.A01.size());
                        for (C72933fE r1 : r4.A01.values()) {
                            int[] iArr = r1.A02;
                            int[] iArr2 = iArr;
                            String[] strArr = new String[iArr.length];
                            String[] strArr2 = r1.A04;
                            String[] strArr3 = strArr2;
                            String[] strArr4 = new String[strArr2.length];
                            int[] iArr3 = r1.A03;
                            int[] iArr4 = iArr3;
                            String[] strArr5 = new String[iArr3.length];
                            double[] dArr = r1.A01;
                            double[] dArr2 = dArr;
                            String[] strArr6 = new String[dArr.length];
                            String[] strArr7 = r1.A05;
                            String[] strArr8 = new String[strArr7.length];
                            C25971ah r14 = r4.A00;
                            int i = 0;
                            int i2 = 0;
                            for (C25981ai r02 : r14.A03) {
                                if (r02 instanceof C26031an) {
                                    strArr[i] = r02.getName();
                                    i++;
                                } else if (r02 instanceof C26051ap) {
                                    strArr4[i2] = r02.getName();
                                    i2++;
                                } else {
                                    throw new UnsupportedOperationException(AnonymousClass08S.A0J("Unsupported Dimension: ", r02.getClass().getName()));
                                }
                            }
                            int i3 = 0;
                            int i4 = 0;
                            int i5 = 0;
                            for (C26071ar r142 : r14.A02) {
                                switch (r142.A00().intValue()) {
                                    case 0:
                                        strArr5[i3] = r142.A01();
                                        i3++;
                                        break;
                                    case 1:
                                        strArr6[i4] = r142.A01();
                                        i4++;
                                        break;
                                    case 2:
                                        strArr8[i5] = r142.A01();
                                        i5++;
                                        break;
                                }
                            }
                            arrayList2.add(new C73013fM(strArr, iArr2, strArr4, strArr3, strArr5, iArr4, strArr6, dArr2, strArr8, strArr7, r1.A00));
                        }
                        r4.A01.clear();
                        arrayList.add(new C73003fL(str, arrayList2));
                    }
                    for (C73003fL r7 : arrayList) {
                        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r8.A00.A01("qpl_aggregations"), AnonymousClass1Y3.A4H);
                        if (uSLEBaseShape0S0000000.A0G()) {
                            ArrayList arrayList3 = new ArrayList();
                            for (C73013fM r13 : r7.A01) {
                                C73023fN r42 = new C73023fN();
                                ArrayList arrayList4 = new ArrayList();
                                String[] strArr9 = r13.A09;
                                String[] strArr10 = r13.A08;
                                for (int i6 = 0; i6 < strArr9.length; i6++) {
                                    C73043fP r12 = new C73043fP();
                                    r12.A02("name", strArr9[i6]);
                                    String str2 = strArr10[i6];
                                    C73053fQ r15 = new C73053fQ();
                                    r15.A02("string_value", str2);
                                    r12.A00.put("value", r15.A00);
                                    arrayList4.add(r12);
                                }
                                String[] strArr11 = r13.A06;
                                int[] iArr5 = r13.A02;
                                for (int i7 = 0; i7 < strArr11.length; i7++) {
                                    C73043fP r3 = new C73043fP();
                                    r3.A02("name", strArr11[i7]);
                                    int i8 = iArr5[i7];
                                    C73053fQ r22 = new C73053fQ();
                                    r22.A00("int_value", Long.valueOf((long) i8).longValue());
                                    r3.A00.put("value", r22.A00);
                                    arrayList4.add(r3);
                                }
                                r42.A00.put("dimensions", arrayList4);
                                ArrayList arrayList5 = new ArrayList();
                                String[] strArr12 = r13.A07;
                                String[] strArr13 = r13.A0A;
                                for (int i9 = 0; i9 < strArr12.length; i9++) {
                                    C73063fR r122 = new C73063fR();
                                    r122.A02("metric", strArr12[i9]);
                                    String str3 = strArr13[i9];
                                    C73073fS r16 = new C73073fS();
                                    r16.A02("string_value", str3);
                                    arrayList5.add(r122.A03(r16));
                                }
                                String[] strArr14 = r13.A05;
                                int[] iArr6 = r13.A03;
                                for (int i10 = 0; i10 < strArr14.length; i10++) {
                                    C73063fR r32 = new C73063fR();
                                    r32.A02("metric", strArr14[i10]);
                                    int i11 = iArr6[i10];
                                    C73073fS r23 = new C73073fS();
                                    r23.A00("int_value", Long.valueOf((long) i11).longValue());
                                    arrayList5.add(r32.A03(r23));
                                }
                                String[] strArr15 = r13.A04;
                                double[] dArr3 = r13.A01;
                                for (int i12 = 0; i12 < strArr15.length; i12++) {
                                    C73063fR r123 = new C73063fR();
                                    r123.A02("metric", strArr15[i12]);
                                    double d = dArr3[i12];
                                    C73073fS r152 = new C73073fS();
                                    r152.A00.put("double_value", Double.valueOf(Double.valueOf(d).doubleValue()));
                                    arrayList5.add(r123.A03(r152));
                                }
                                r42.A00.put("aggregations", arrayList5);
                                r42.A00("count", Long.valueOf((long) r13.A00).longValue());
                                arrayList3.add(r42);
                            }
                            uSLEBaseShape0S0000000.A0D("scenario", r7.A00);
                            uSLEBaseShape0S0000000.A0E("summaries", arrayList3);
                            uSLEBaseShape0S0000000.A06();
                        } else {
                            r8.A01.CGS("qpl", "QplAggregations event is not sampled");
                        }
                    }
                } else {
                    C25841aU.A00(r9, r2);
                }
            }
        }
    }

    public void BeL(C04270Tg r8, String str, String str2) {
        if (!(this instanceof C08180eo)) {
            if (this instanceof C08050eb) {
                C08050eb r2 = (C08050eb) this;
                boolean z = false;
                if (r2.A01 != null) {
                    z = true;
                }
                if (z && str.equals("persist_id")) {
                    C08050eb.A01(r2, r8.A01).mPersistId = str2;
                }
            }
        } else if (AnonymousClass08Z.A05(4)) {
            long millis = TimeUnit.NANOSECONDS.toMillis(r8.A0B);
            int i = r8.A02;
            int i2 = r8.A08;
            String A00 = C03020Ho.A00(i);
            int A002 = C09580i5.A00(i, i2);
            long nanos = TimeUnit.MILLISECONDS.toNanos(millis);
            String A0S = AnonymousClass08S.A0S("<ANNOTATION>=", str, "->", str2);
            if (AnonymousClass08Z.A05(274877906944L)) {
                TraceDirect.asyncTraceStageBegin(A00, A002, AnonymousClass0I0.A00(nanos), A0S);
            }
        }
    }

    public void BeM(C04270Tg r5) {
        if (this instanceof C26531bb) {
            C26531bb.A01((C26531bb) this, r5);
        } else if (!(this instanceof C08180eo)) {
            if (this instanceof C07880eK) {
                ((C07880eK) this).A07(r5);
            }
        } else if (AnonymousClass08Z.A05(4)) {
            int i = r5.A02;
            int i2 = r5.A08;
            String A00 = C03020Ho.A00(i);
            int A002 = C09580i5.A00(i, i2);
            if (!AnonymousClass08Z.A05(4)) {
                return;
            }
            if (TraceDirect.checkNative()) {
                TraceDirect.nativeAsyncTraceCancel(A00, A002);
                return;
            }
            AnonymousClass09R r1 = new AnonymousClass09R('F');
            r1.A01(Process.myPid());
            r1.A03(A00);
            r1.A02("<X>");
            r1.A01(A002);
            AnonymousClass0HL.A00(r1.toString());
        }
    }

    public void BeQ(C04270Tg r10, String str, AnonymousClass0lr r12, long j, boolean z, int i) {
        if (!(this instanceof C08180eo)) {
            if (this instanceof C08050eb) {
                C08050eb r2 = (C08050eb) this;
                boolean z2 = false;
                if (r2.A01 != null) {
                    z2 = true;
                }
                if (z2) {
                    C25871aX A01 = C08050eb.A01(r2, r10.A01);
                    Map<Long, String> map = A01.mMarkerPoints;
                    Long valueOf = Long.valueOf(j);
                    String str2 = map.get(valueOf);
                    Map<Long, String> map2 = A01.mMarkerPoints;
                    if (str2 == null) {
                        map2.put(valueOf, str);
                    } else {
                        map2.put(valueOf, AnonymousClass08S.A0P(str2, ", ", str));
                    }
                }
            }
        } else if (AnonymousClass08Z.A05(4)) {
            int i2 = r10.A02;
            int i3 = r10.A08;
            String A00 = C03020Ho.A00(i2);
            int A002 = C09580i5.A00(i2, i3);
            long nanos = TimeUnit.MILLISECONDS.toNanos(j);
            if (AnonymousClass08Z.A05(4)) {
                TraceDirect.asyncTraceStageBegin(A00, A002, AnonymousClass0I0.A00(nanos), str);
            }
            if (r12 != null) {
                String A003 = C03020Ho.A00(i2);
                int A004 = C09580i5.A00(i2, i3);
                long nanos2 = TimeUnit.MILLISECONDS.toNanos(j);
                String A0J = AnonymousClass08S.A0J("<PDATA>=", r12.toString());
                if (AnonymousClass08Z.A05(274877906944L)) {
                    TraceDirect.asyncTraceStageBegin(A003, A004, AnonymousClass0I0.A00(nanos2), A0J);
                }
            }
        }
    }

    public void BeR(C04270Tg r2) {
        if (this instanceof C08180eo) {
            ((C08180eo) this).BeT(r2);
        }
    }

    public void BeT(C04270Tg r11) {
        boolean z;
        if (this instanceof C07800eB) {
            C07800eB r3 = (C07800eB) this;
            C25881aY r2 = (C25881aY) r3.A05.get();
            if (r2 != null && r2.A01 == r11.A02) {
                r2.A02.A00(r3.A04);
            }
        } else if (this instanceof C26531bb) {
            C26531bb r4 = (C26531bb) this;
            int i = r11.A02;
            if (r4.A0T) {
                int[] iArr = r4.A0Q;
                int length = iArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z = false;
                        break;
                    } else if (iArr[i2] == i) {
                        z = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z) {
                    r4.A02 = TimeUnit.NANOSECONDS.toMillis(r11.A0C);
                }
            }
            if (r11.A02 == 24444929 && r4.A0P) {
                r4.A01 = Math.max(TimeUnit.NANOSECONDS.toMillis(r11.A0C), r4.A01);
                r4.A0P = false;
            }
        } else if (!(this instanceof C08180eo)) {
            if (this instanceof C07880eK) {
                ((C07880eK) this).A08(r11);
            }
        } else if (AnonymousClass08Z.A05(4)) {
            int i3 = r11.A02;
            int i4 = r11.A08;
            long millis = TimeUnit.NANOSECONDS.toMillis(r11.A0C);
            String A00 = C03020Ho.A00(i3);
            int A002 = C09580i5.A00(i3, i4);
            long nanos = TimeUnit.MILLISECONDS.toNanos(millis);
            if (AnonymousClass08Z.A05(4)) {
                TraceDirect.asyncTraceBegin(A00, A002, AnonymousClass0I0.A00(nanos));
            }
            ArrayList arrayList = r11.A0a;
            String join = (arrayList == null || arrayList.isEmpty()) ? null : String.join(", ", arrayList);
            if (join != null) {
                String A003 = C03020Ho.A00(i3);
                int A004 = C09580i5.A00(i3, i4);
                long nanos2 = TimeUnit.MILLISECONDS.toNanos(millis);
                String A0J = AnonymousClass08S.A0J("<TAG>=", join);
                if (AnonymousClass08Z.A05(274877906944L)) {
                    TraceDirect.asyncTraceStageBegin(A003, A004, AnonymousClass0I0.A00(nanos2), A0J);
                }
            }
        }
    }

    public void BeU(C04270Tg r11) {
        if (this instanceof C07800eB) {
            C07800eB r3 = (C07800eB) this;
            C25881aY r2 = (C25881aY) r3.A05.get();
            if (r2 != null && r2.A01 == r11.A02) {
                short s = r11.A0M;
                r2.A02.A00(s != 3 ? s != 4 ? r3.A01 : r3.A00 : r3.A02);
            }
        } else if (this instanceof C26531bb) {
            C26531bb.A01((C26531bb) this, r11);
        } else if (!(this instanceof C08180eo)) {
            if (this instanceof C25841aU) {
                C25841aU r1 = (C25841aU) this;
                synchronized (r1) {
                    C25841aU.A00(r1, r11);
                }
            } else if (this instanceof AnonymousClass0eG) {
                AnonymousClass1LT r5 = (AnonymousClass1LT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BGS, ((AnonymousClass0eG) this).A00);
                int i = r11.A02;
                long millis = (long) ((int) TimeUnit.NANOSECONDS.toMillis(r11.A09));
                r5.A02 = r5.A04.now();
                r5.A01 = i;
                r5.A00 = (int) millis;
                AnonymousClass1LT.A01(r5);
            } else if (this instanceof C07880eK) {
                ((C07880eK) this).A07(r11);
            }
        } else if (AnonymousClass08Z.A05(4)) {
            short s2 = r11.A0M;
            long millis2 = TimeUnit.NANOSECONDS.toMillis(r11.A0B);
            int i2 = r11.A02;
            int i3 = r11.A08;
            String A00 = C03020Ho.A00(i2);
            int A002 = C09580i5.A00(i2, i3);
            long nanos = TimeUnit.MILLISECONDS.toNanos(millis2);
            if (AnonymousClass08Z.A05(4)) {
                TraceDirect.asyncTraceEnd(A00, A002, AnonymousClass0I0.A00(nanos));
            }
            String A0P = AnonymousClass08S.A0P(C03020Ho.A00(i2), "-", AnonymousClass0I1.A00(s2));
            int A003 = C09580i5.A00(i2, i3);
            if (!AnonymousClass08Z.A05(4)) {
                return;
            }
            if (TraceDirect.checkNative()) {
                TraceDirect.nativeAsyncTraceRename(A00, A0P, A003);
                return;
            }
            AnonymousClass09R r12 = new AnonymousClass09R('F');
            r12.A01(Process.myPid());
            r12.A03(A00);
            r12.A02("<M>");
            r12.A01(A003);
            r12.A03(A0P);
            AnonymousClass0HL.A00(r12.toString());
        }
    }

    public void BeV(int i, int i2, C04270Tg r4) {
        boolean z = this instanceof C08180eo;
    }

    public void BfD(PerformanceLoggingEvent performanceLoggingEvent) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x008b, code lost:
        if (r1.contains(java.lang.Integer.valueOf(r11)) == false) goto L_0x008d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Bkk(int r10, int r11) {
        /*
            r9 = this;
            boolean r0 = r9 instanceof X.C26531bb
            if (r0 != 0) goto L_0x00ba
            boolean r0 = r9 instanceof X.C26131ax
            if (r0 != 0) goto L_0x0059
            boolean r0 = r9 instanceof X.C08000eW
            if (r0 != 0) goto L_0x003e
            boolean r0 = r9 instanceof X.C08200eq
            if (r0 != 0) goto L_0x001c
            boolean r0 = r9 instanceof X.C07880eK
            if (r0 == 0) goto L_0x0138
            r1 = r9
            X.0eK r1 = (X.C07880eK) r1
            r0 = 0
            r1.A07(r0)
            return
        L_0x001c:
            r3 = r9
            X.0eq r3 = (X.C08200eq) r3
            java.util.HashSet r0 = r3.A01
            if (r0 == 0) goto L_0x0138
            java.util.HashSet r1 = r3.A01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0138
            r2 = 0
            int r1 = X.AnonymousClass1Y3.AsC
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0fx r0 = (X.C08790fx) r0
            r0.A08(r10)
            return
        L_0x003e:
            boolean r0 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sEnabled
            if (r0 == 0) goto L_0x0138
            r0 = 1215735889(0x4876a851, float:252577.27)
            long r3 = (long) r0
            r0 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r3 = r3 & r0
            long r1 = (long) r10
            r0 = 32
            long r1 = r1 << r0
            long r1 = r1 | r3
            r0 = 0
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerNativeHolder.classLoadStarted(r0)
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerNativeHolder.classLoaded(r1)
            return
        L_0x0059:
            r0 = r9
            X.1ax r0 = (X.C26131ax) r0
            X.1bT r0 = r0.A00
            X.0eX r1 = r0.A01
            android.util.SparseArray r0 = r1.A01
            java.lang.Object r3 = r0.get(r10)
            X.0ea r3 = (X.C08040ea) r3
            if (r3 != 0) goto L_0x006c
            X.0ea r3 = r1.A06
        L_0x006c:
            java.util.List r0 = r3.A04
            java.util.Iterator r5 = r0.iterator()
        L_0x0072:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0138
            java.lang.Object r4 = r5.next()
            X.0ct r4 = (X.C07210ct) r4
            java.util.Set r1 = r4.A02
            if (r1 == 0) goto L_0x008d
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
            boolean r1 = r1.contains(r0)
            r0 = 1
            if (r1 != 0) goto L_0x008e
        L_0x008d:
            r0 = 0
        L_0x008e:
            if (r0 != 0) goto L_0x0072
            X.0f7 r2 = X.C08040ea.A00(r3, r4)
            r0 = 0
            if (r0 == 0) goto L_0x00b5
            r0 = 0
            r2.A05(r0)     // Catch:{ Exception -> 0x009c }
            goto L_0x00a2
        L_0x009c:
            r1 = move-exception
            X.1b3 r0 = r3.A00
            r0.A01(r2, r1)
        L_0x00a2:
            X.1b3 r0 = r3.A00
            boolean r0 = r0.A02(r2)
            if (r0 == 0) goto L_0x0072
            X.0eu r1 = X.C08240eu.A00
            r4.A00 = r1
            r0 = 0
            if (r0 == 0) goto L_0x0072
            r1.A04(r0)
            goto L_0x0072
        L_0x00b5:
            r0 = 0
            r2.A05(r0)
            goto L_0x0072
        L_0x00ba:
            r4 = r9
            X.1bb r4 = (X.C26531bb) r4
            r0 = 3997722(0x3d001a, float:5.602002E-39)
            r1 = 0
            r3 = 0
            if (r10 == r0) goto L_0x0124
            r0 = 44826634(0x2ac000a, float:2.527315E-37)
            if (r10 == r0) goto L_0x011e
            switch(r10) {
                case 44826625: goto L_0x0118;
                case 44826626: goto L_0x0112;
                case 44826627: goto L_0x00ce;
                default: goto L_0x00cd;
            }
        L_0x00cd:
            return
        L_0x00ce:
            X.3fV r5 = r4.A04
            X.2O2 r4 = new X.2O2
            r4.<init>()
        L_0x00d5:
            X.3fY r6 = X.C73103fV.A00(r5)
            int[] r8 = r6.A01
            r7 = 0
        L_0x00dc:
            int r0 = r8.length
            if (r7 >= r0) goto L_0x00e6
            r0 = r8[r7]
            if (r0 == r11) goto L_0x00e7
            int r7 = r7 + 1
            goto L_0x00dc
        L_0x00e6:
            r7 = -1
        L_0x00e7:
            if (r7 < 0) goto L_0x0138
            X.3fY r3 = new X.3fY
            r3.<init>()
            r4.A00 = r3
            int r0 = r8.length
            int r2 = r0 + -1
            int[] r1 = new int[r2]
            r0 = 0
            java.lang.System.arraycopy(r8, r0, r1, r0, r2)
            if (r7 >= r2) goto L_0x00ff
            r0 = r8[r2]
            r1[r7] = r0
        L_0x00ff:
            r3.A01 = r1
            X.3fY r1 = r4.A00
            int r0 = r6.A00
            int r0 = r0 + 1
            r1.A00 = r0
            java.util.concurrent.atomic.AtomicReference r0 = r5.A00
            boolean r0 = r0.compareAndSet(r6, r1)
            if (r0 == 0) goto L_0x00d5
            return
        L_0x0112:
            X.3fU r0 = r4.A0J
            r0.A04(r3)
            return
        L_0x0118:
            X.3fU r0 = r4.A0K
            r0.A04(r3)
            return
        L_0x011e:
            java.util.concurrent.atomic.AtomicLong r0 = r4.A0N
            r0.set(r1)
            return
        L_0x0124:
            X.3fU r0 = r4.A0J
            r0.A04(r3)
            X.3fU r0 = r4.A0L
            r0.A04(r3)
            X.3fU r0 = r4.A0K
            r0.A04(r3)
            java.util.concurrent.atomic.AtomicLong r0 = r4.A0N
            r0.set(r1)
        L_0x0138:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08190ep.Bkk(int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0074, code lost:
        if (r1.contains(java.lang.Integer.valueOf(r10)) == false) goto L_0x0076;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean Bkl(int r9, int r10) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.C26531bb
            if (r0 != 0) goto L_0x00ed
            boolean r0 = r8 instanceof X.C26131ax
            if (r0 != 0) goto L_0x0040
            boolean r0 = r8 instanceof X.C08000eW
            if (r0 != 0) goto L_0x0184
            boolean r0 = r8 instanceof X.C08200eq
            if (r0 != 0) goto L_0x001d
            boolean r0 = r8 instanceof X.C07880eK
            if (r0 == 0) goto L_0x019e
            r1 = r8
            X.0eK r1 = (X.C07880eK) r1
            r0 = 0
            r1.A08(r0)
            r0 = 1
            return r0
        L_0x001d:
            r3 = r8
            X.0eq r3 = (X.C08200eq) r3
            java.util.HashSet r0 = r3.A01
            r2 = 0
            if (r0 == 0) goto L_0x003f
            java.util.HashSet r1 = r3.A01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x003f
            int r1 = X.AnonymousClass1Y3.AsC
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0fx r0 = (X.C08790fx) r0
            X.C08790fx.A04(r0, r9)
            r2 = 1
        L_0x003f:
            return r2
        L_0x0040:
            r0 = r8
            X.1ax r0 = (X.C26131ax) r0
            X.1bT r0 = r0.A00
            X.0eX r1 = r0.A01
            android.util.SparseArray r0 = r1.A01
            java.lang.Object r3 = r0.get(r9)
            X.0ea r3 = (X.C08040ea) r3
            if (r3 != 0) goto L_0x0053
            X.0ea r3 = r1.A06
        L_0x0053:
            java.util.List r0 = r3.A04
            java.util.Iterator r7 = r0.iterator()
        L_0x0059:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x00eb
            java.lang.Object r5 = r7.next()
            X.0ct r5 = (X.C07210ct) r5
            if (r10 == 0) goto L_0x007a
            java.util.Set r1 = r5.A02
            if (r1 == 0) goto L_0x0076
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)
            boolean r1 = r1.contains(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0077
        L_0x0076:
            r0 = 0
        L_0x0077:
            if (r0 == 0) goto L_0x007a
            goto L_0x0059
        L_0x007a:
            java.util.HashSet r1 = X.C07210ct.A06
            int r0 = r5.A04
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x008d
            boolean r0 = X.C08040ea.A06
            if (r0 != 0) goto L_0x008d
            goto L_0x0059
        L_0x008d:
            X.0f7 r4 = X.C08040ea.A00(r3, r5)
            r0 = 0
            if (r0 == 0) goto L_0x00e0
            int r1 = r3.A02     // Catch:{ Exception -> 0x009f }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x009f }
            boolean r0 = r4.A07(r1, r0)     // Catch:{ Exception -> 0x009f }
            goto L_0x00ba
        L_0x009f:
            r6 = move-exception
            X.1b3 r2 = r3.A00
            if (r4 == 0) goto L_0x00b0
            int r1 = r4.A02()
            r0 = -1
            if (r1 == r0) goto L_0x00b0
            java.util.Map r0 = r2.A01
            X.C26191b3.A00(r0, r4)
        L_0x00b0:
            X.09P r2 = r2.A00
            java.lang.String r1 = "MobileBoost"
            java.lang.String r0 = "BoosterFailsRequestWithException"
            r2.CGZ(r1, r0, r6)
            r0 = 1
        L_0x00ba:
            if (r0 != 0) goto L_0x00cc
            X.1b3 r2 = r3.A00
            if (r4 == 0) goto L_0x00cc
            int r1 = r4.A02()
            r0 = -1
            if (r1 == r0) goto L_0x00cc
            java.util.Map r0 = r2.A02
            X.C26191b3.A00(r0, r4)
        L_0x00cc:
            X.1b3 r0 = r3.A00
            boolean r0 = r0.A02(r4)
            if (r0 == 0) goto L_0x0059
            X.0eu r1 = X.C08240eu.A00
            r5.A00 = r1
            r0 = 0
            if (r0 == 0) goto L_0x0059
            r1.A04(r0)
            goto L_0x0059
        L_0x00e0:
            int r1 = r3.A02
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)
            r4.A07(r1, r0)
            goto L_0x0059
        L_0x00eb:
            r0 = 1
            return r0
        L_0x00ed:
            r5 = r8
            X.1bb r5 = (X.C26531bb) r5
            r0 = 44826629(0x2ac0005, float:2.527314E-37)
            r3 = 1
            if (r9 == r0) goto L_0x0162
            r0 = 44826634(0x2ac000a, float:2.527315E-37)
            if (r9 == r0) goto L_0x014e
            switch(r9) {
                case 44826625: goto L_0x0100;
                case 44826626: goto L_0x0107;
                case 44826627: goto L_0x010e;
                default: goto L_0x00fe;
            }
        L_0x00fe:
            goto L_0x019e
        L_0x0100:
            X.3fU r0 = r5.A0K
            r0.A03(r3)
            goto L_0x019e
        L_0x0107:
            X.3fU r0 = r5.A0J
            r0.A03(r3)
            goto L_0x019e
        L_0x010e:
            X.3fV r5 = r5.A04
            X.2O2 r4 = new X.2O2
            r4.<init>()
        L_0x0115:
            X.3fY r6 = X.C73103fV.A00(r5)
            int[] r7 = r6.A01
            r1 = 0
        L_0x011c:
            int r0 = r7.length
            if (r1 >= r0) goto L_0x0126
            r0 = r7[r1]
            if (r0 == r10) goto L_0x0127
            int r1 = r1 + 1
            goto L_0x011c
        L_0x0126:
            r1 = -1
        L_0x0127:
            if (r1 >= 0) goto L_0x019e
            X.3fY r3 = new X.3fY
            r3.<init>()
            r4.A00 = r3
            int r2 = r7.length
            int r0 = r2 + 1
            int[] r1 = new int[r0]
            r0 = 0
            java.lang.System.arraycopy(r7, r0, r1, r0, r2)
            r1[r2] = r10
            r3.A01 = r1
            X.3fY r1 = r4.A00
            int r0 = r6.A00
            int r0 = r0 + 1
            r1.A00 = r0
            java.util.concurrent.atomic.AtomicReference r0 = r5.A00
            boolean r0 = r0.compareAndSet(r6, r1)
            if (r0 == 0) goto L_0x0115
            goto L_0x019e
        L_0x014e:
            java.util.concurrent.atomic.AtomicLong r2 = r5.A0N
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r5.A03
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.069 r0 = (X.AnonymousClass069) r0
            long r0 = r0.now()
            r2.set(r0)
            goto L_0x019e
        L_0x0162:
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r5.A03
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.069 r0 = (X.AnonymousClass069) r0
            long r3 = r0.now()
            java.util.concurrent.atomic.AtomicLong r0 = r5.A0O
            long r0 = r0.get()
            long r3 = r3 - r0
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x019e
            java.util.concurrent.atomic.AtomicInteger r1 = r5.A0M
            int r0 = (int) r3
            r1.set(r0)
            goto L_0x019e
        L_0x0184:
            boolean r0 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sEnabled
            if (r0 == 0) goto L_0x019e
            r0 = 1505373456(0x59ba2d10, float:6.5504866E15)
            long r3 = (long) r0
            r0 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r3 = r3 & r0
            long r1 = (long) r9
            r0 = 32
            long r1 = r1 << r0
            long r1 = r1 | r3
            r0 = 0
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerNativeHolder.classLoadStarted(r0)
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerNativeHolder.classLoaded(r1)
        L_0x019e:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08190ep.Bkl(int, int):boolean");
    }

    public void CB4(QuickPerformanceLogger quickPerformanceLogger) {
        if (this instanceof C26131ax) {
            C26611bj r2 = new C26611bj(quickPerformanceLogger, new C08510fT());
            synchronized (C26621bk.class) {
                C26621bk.A00.add(r2);
            }
        }
    }
}
