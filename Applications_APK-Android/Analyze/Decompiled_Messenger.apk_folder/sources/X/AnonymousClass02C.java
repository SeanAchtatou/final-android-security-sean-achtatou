package X;

import android.app.Service;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.02C  reason: invalid class name */
public final class AnonymousClass02C {
    public static int A00(Service service, int i) {
        C011409b r0 = C013309u.A00;
        if (r0 != null) {
            r0.BUe(service);
        }
        return Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 28, 0, 0, i, 0, 0);
    }

    public static int A01(Service service, int i) {
        C011409b r0 = C013309u.A00;
        if (r0 != null) {
            r0.BpW(service);
        }
        return Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 28, 0, 0, i, 0, 0);
    }

    public static void A02(int i, int i2) {
        Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 29, 0, 0, i, i2, 0);
    }
}
