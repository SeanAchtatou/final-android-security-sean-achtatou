package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import io.card.payment.BuildConfig;

/* renamed from: X.06l  reason: invalid class name and case insensitive filesystem */
public final class C006506l {
    public static C007306v A00;
    private static final C007306v A01 = new C006706n();
    private static final AnonymousClass06j A02 = new AnonymousClass08N(new C007206t());

    public static synchronized C007306v A00() {
        C007306v r0;
        synchronized (C006506l.class) {
            r0 = A00;
            if (r0 == null) {
                throw new IllegalStateException();
            }
        }
        return r0;
    }

    public static synchronized AnonymousClass06j A01() {
        AnonymousClass06j r0;
        synchronized (C006506l.class) {
            r0 = A02;
        }
        return r0;
    }

    private static void A02(Context context) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.facebook.secure.switchoff", 0);
            A00 = new AnonymousClass08O(AnonymousClass09U.A00(sharedPreferences.getString("last_criteria", BuildConfig.FLAVOR), context), AnonymousClass1ZT.A00(sharedPreferences.getString("last_custom_config", BuildConfig.FLAVOR)), C24461Ts.A00(sharedPreferences.getString("last_deeplink_config", BuildConfig.FLAVOR)));
        } catch (Throwable th) {
            Log.w("DefaultSwitchOffs", "Error loading last config", th);
        }
    }

    public static synchronized void A03(Context context) {
        synchronized (C006506l.class) {
            if (A00 == null) {
                A02(context);
                if (A00 == null) {
                    A00 = A01;
                }
            }
        }
    }

    private C006506l() {
    }
}
