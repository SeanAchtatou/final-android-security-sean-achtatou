package X;

import android.app.Activity;
import android.app.Application;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import com.facebook.base.app.SplashScreenApplication$RedirectHackActivity;

/* renamed from: X.00Q  reason: invalid class name */
public final class AnonymousClass00Q extends Instrumentation {
    private Instrumentation A00;
    private final AnonymousClass001 A01;

    public void callActivityOnCreate(Activity activity, Bundle bundle) {
        if (activity instanceof SplashScreenApplication$RedirectHackActivity) {
            ((SplashScreenApplication$RedirectHackActivity) activity).A00 = bundle;
            super.callActivityOnCreate(activity, null);
            return;
        }
        if (bundle != null && bundle.getBoolean("com.facebook.bundleHack")) {
            bundle = null;
        }
        int i = this.A01.A0E;
        if (i > 0 && (activity instanceof AnonymousClass00G)) {
            ((AnonymousClass00G) activity).BPe(i);
        }
        super.callActivityOnCreate(activity, bundle);
    }

    public void callActivityOnPostCreate(Activity activity, Bundle bundle) {
        Bundle bundle2 = null;
        if (!(activity instanceof SplashScreenApplication$RedirectHackActivity) && (bundle == null || !bundle.getBoolean("com.facebook.bundleHack"))) {
            bundle2 = bundle;
        }
        super.callActivityOnPostCreate(activity, bundle2);
    }

    public void callActivityOnRestoreInstanceState(Activity activity, Bundle bundle) {
        Bundle bundle2 = null;
        if (!(activity instanceof SplashScreenApplication$RedirectHackActivity) && (bundle == null || !bundle.getBoolean("com.facebook.bundleHack"))) {
            bundle2 = bundle;
        }
        if (bundle2 != null) {
            super.callActivityOnRestoreInstanceState(activity, bundle2);
        }
    }

    public void callActivityOnSaveInstanceState(Activity activity, Bundle bundle) {
        if (activity instanceof SplashScreenApplication$RedirectHackActivity) {
            Bundle bundle2 = ((SplashScreenApplication$RedirectHackActivity) activity).A00;
            if (bundle2 == null) {
                bundle.putBoolean("com.facebook.bundleHack", true);
                return;
            }
            Parcel obtain = Parcel.obtain();
            try {
                obtain.setDataPosition(0);
                bundle2.writeToParcel(obtain, 0);
                obtain.setDataPosition(0);
                bundle.readFromParcel(obtain);
            } finally {
                obtain.recycle();
            }
        } else {
            super.callActivityOnSaveInstanceState(activity, bundle);
            bundle.remove("com.facebook.bundleHack");
        }
    }

    public void onCreate(Bundle bundle) {
        Instrumentation instrumentation = this.A00;
        if (instrumentation != null) {
            instrumentation.onCreate(bundle);
        }
    }

    public AnonymousClass00Q(AnonymousClass001 r1, Instrumentation instrumentation) {
        this.A01 = r1;
        this.A00 = instrumentation;
    }

    public void callActivityOnDestroy(Activity activity) {
        super.callActivityOnDestroy(activity);
        if (!(activity instanceof SplashScreenApplication$RedirectHackActivity)) {
            this.A01.A0g = true;
        }
    }

    public Activity newActivity(Class cls, Context context, IBinder iBinder, Application application, Intent intent, ActivityInfo activityInfo, CharSequence charSequence, Activity activity, String str, Object obj) {
        throw new AssertionError("should be unused by framework");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001e, code lost:
        if ("com.facebook.showSplashScreen".equals(r5.getAction()) != false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Activity newActivity(java.lang.ClassLoader r3, java.lang.String r4, android.content.Intent r5) {
        /*
            r2 = this;
            X.001 r0 = r2.A01
            r0.A0I(r5)
            X.001 r1 = r2.A01
            boolean r0 = r1.A0Y
            if (r0 == 0) goto L_0x0020
            boolean r0 = X.AnonymousClass001.A0l
            if (r0 != 0) goto L_0x0020
            boolean r0 = r1.A0X
            if (r0 != 0) goto L_0x0020
            java.lang.String r1 = r5.getAction()
            java.lang.String r0 = "com.facebook.showSplashScreen"
            boolean r1 = r0.equals(r1)
            r0 = 1
            if (r1 == 0) goto L_0x0021
        L_0x0020:
            r0 = 0
        L_0x0021:
            if (r0 == 0) goto L_0x0030
            X.001 r1 = r2.A01
            r0 = 1
            r1.A0e = r0
            X.001 r1 = r2.A01
            com.facebook.base.app.SplashScreenApplication$RedirectHackActivity r0 = new com.facebook.base.app.SplashScreenApplication$RedirectHackActivity
            r0.<init>(r1)
            return r0
        L_0x0030:
            android.app.Activity r0 = super.newActivity(r3, r4, r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00Q.newActivity(java.lang.ClassLoader, java.lang.String, android.content.Intent):android.app.Activity");
    }
}
