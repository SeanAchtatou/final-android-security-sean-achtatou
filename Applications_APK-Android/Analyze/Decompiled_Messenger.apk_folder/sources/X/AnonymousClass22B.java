package X;

import android.content.Context;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.drawee.fbpipeline.FbDraweeView;
import com.facebook.messaging.model.messages.GenericAdminMessageInfo;
import com.facebook.messaging.model.messages.OmniMUpdateFlowStateProperties;
import com.facebook.ui.compat.fbrelativelayout.FbRelativeLayout;
import com.facebook.widget.text.BetterTextView;
import java.util.Map;
import java.util.concurrent.Executor;

/* renamed from: X.22B  reason: invalid class name */
public final class AnonymousClass22B extends AnonymousClass3DI implements C117405hL, CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.threadview.item.adminmessage.AdminMessageOmniMUpdateFlowState";
    public FbDraweeView A00 = ((FbDraweeView) C012809p.A01(this, 2131299458));
    public FbDraweeView A01 = ((FbDraweeView) C012809p.A01(this, 2131299462));
    public CME A02;
    public B3F A03;
    public C69283Wl A04;
    public C25051Yd A05;
    public C65103Ew A06;
    public FbRelativeLayout A07 = ((FbRelativeLayout) C012809p.A01(this, 2131299460));
    public BetterTextView A08 = ((BetterTextView) C012809p.A01(this, 2131299459));
    public BetterTextView A09 = ((BetterTextView) C012809p.A01(this, 2131299461));
    public BetterTextView A0A = ((BetterTextView) C012809p.A01(this, 2131299465));
    public BetterTextView A0B = ((BetterTextView) C012809p.A01(this, 2131299464));
    public BetterTextView A0C = ((BetterTextView) C012809p.A01(this, 2131299467));
    public BetterTextView A0D = ((BetterTextView) C012809p.A01(this, 2131299466));

    public void CCQ(AnonymousClass2FB r1) {
    }

    public static void A00(AnonymousClass22B r2) {
        r2.A0D.setText(r2.A04.A04.A10);
        r2.A0D.setVisibility(0);
        r2.A09.setOnClickListener(null);
        r2.A07.setVisibility(8);
    }

    public void AQJ(C69283Wl r7) {
        String str;
        if (!r7.equals(this.A04) || this.A02 != null) {
            this.A04 = r7;
            OmniMUpdateFlowStateProperties omniMUpdateFlowStateProperties = null;
            this.A02 = null;
            if (r7.A05.A05() && this.A05.Aem(284936722781429L)) {
                GenericAdminMessageInfo genericAdminMessageInfo = r7.A04.A09;
                if (genericAdminMessageInfo != null) {
                    omniMUpdateFlowStateProperties = (OmniMUpdateFlowStateProperties) genericAdminMessageInfo.A01();
                }
                if (!(omniMUpdateFlowStateProperties == null || (str = omniMUpdateFlowStateProperties.A00) == null)) {
                    B3F b3f = this.A03;
                    CMB cmb = new CMB(this);
                    synchronized (b3f) {
                        Map map = (Map) b3f.A03.get(str);
                        if (map == null) {
                            C05350Yp.A08(((AnonymousClass0VL) b3f.A00.get()).CIF(new C22160Apn(b3f, str)), new B3G(b3f, str, cmb), (Executor) b3f.A01.get());
                        } else {
                            cmb.A00(B3F.A00(b3f, map), B3F.A01(b3f, map));
                        }
                    }
                    return;
                }
            }
            A00(this);
        }
    }

    public void C9A(C65103Ew r1) {
        this.A06 = r1;
    }

    public AnonymousClass22B(Context context) {
        super(context);
        AnonymousClass1XX r1 = AnonymousClass1XX.get(getContext());
        this.A05 = AnonymousClass0WT.A00(r1);
        this.A03 = B3F.A02(r1);
        setContentView(2132411105);
    }
}
