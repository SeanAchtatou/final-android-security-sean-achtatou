package X;

/* renamed from: X.1hn  reason: invalid class name and case insensitive filesystem */
public interface C30281hn {
    C30281hn Bz4(AnonymousClass1Y7 r1, double d);

    C30281hn Bz5(AnonymousClass1Y7 r1, float f);

    C30281hn Bz6(AnonymousClass1Y7 r1, int i);

    C30281hn BzA(AnonymousClass1Y7 r1, long j);

    C30281hn BzC(AnonymousClass1Y7 r1, String str);

    C30281hn C1B(AnonymousClass1Y7 r1);

    C30281hn C24(AnonymousClass1Y7 r1);

    void commit();

    void commitImmediately();

    C30281hn putBoolean(AnonymousClass1Y7 r1, boolean z);
}
