package X;

/* renamed from: X.0qJ  reason: invalid class name and case insensitive filesystem */
public final class C12970qJ extends RuntimeException {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C12970qJ(android.content.Context r4, int r5, java.lang.Throwable r6) {
        /*
            r3 = this;
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "\nFailed inflating resource: "
            r2.<init>(r0)
            r2.append(r5)
            java.lang.String r0 = "\nWith Resource Name: "
            r2.append(r0)
            android.content.res.Resources r0 = r4.getResources()
            java.lang.String r0 = r0.getResourceName(r5)
            r2.append(r0)
            java.lang.String r1 = android.util.Log.getStackTraceString(r6)
            java.lang.String r0 = "line #-1 (sorry, not yet implemented)"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0042
            java.lang.String r0 = "\n  TESTING NOTE:  If you receive this error in a test, you might not have a theme set on the activity.  This can be set in the testing manifest or by adding a call to setTheme(R.style.Theme_FBUi) when creating the activity"
        L_0x0028:
            r2.append(r0)
            java.lang.String r0 = "\n  Original Throwable: "
            r2.append(r0)
            java.lang.String r0 = android.util.Log.getStackTraceString(r6)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r3.<init>(r0)
            r3.initCause(r6)
            return
        L_0x0042:
            java.lang.String r0 = ""
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12970qJ.<init>(android.content.Context, int, java.lang.Throwable):void");
    }
}
