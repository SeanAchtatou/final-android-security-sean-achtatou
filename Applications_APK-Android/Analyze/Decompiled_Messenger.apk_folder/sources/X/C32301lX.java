package X;

import androidx.fragment.app.Fragment;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.threadview.params.ThreadViewParams;
import com.facebook.orca.threadview.ThreadViewFragment;
import com.google.common.base.Preconditions;

/* renamed from: X.1lX  reason: invalid class name and case insensitive filesystem */
public final class C32301lX {
    public AnonymousClass0UN A00;
    public final C13060qW A01;
    public final C08860g6 A02;
    public final C08770fv A03;
    public final C32571ly A04 = new C32571ly();
    public final C15830w3 A05;
    public final C33041mk A06;
    public final C32331la A07;
    public final C34741q6 A08;
    private final C08790fx A09;

    public static C13150qn A00(C32301lX r4) {
        return new C13150qn(r4.A01, ((C33661nw) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AJS, r4.A00)).A01());
    }

    public C32301lX(AnonymousClass1XY r3, C13060qW r4, C34741q6 r5, C32291lW r6) {
        this.A00 = new AnonymousClass0UN(6, r3);
        this.A06 = new C33041mk(r3);
        this.A05 = new C15830w3();
        this.A03 = C08770fv.A00(r3);
        this.A02 = C08860g6.A00(r3);
        this.A09 = C08790fx.A02(r3);
        this.A01 = r4;
        this.A08 = r5;
        this.A07 = new C15810w1(this, r6);
    }

    public static void A01(C32301lX r3) {
        if (A02(r3) || A03(r3)) {
            C13400rN r1 = (C13400rN) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A7C, r3.A00);
            C13400rN.A01(r1, r1.A02);
        }
    }

    public static boolean A02(C32301lX r2) {
        Fragment A0O = r2.A01.A0O(2131300410);
        if (A0O == null || A0O.A0b) {
            return false;
        }
        return true;
    }

    public static boolean A03(C32301lX r2) {
        Fragment A0O = r2.A01.A0O(2131301002);
        if (A0O == null || A0O.A0b) {
            return false;
        }
        return true;
    }

    public ThreadKey A04() {
        if (!A03(this)) {
            return null;
        }
        Fragment A0O = this.A01.A0O(2131301002);
        Preconditions.checkNotNull(A0O);
        return ((ThreadViewFragment) A0O).A0H;
    }

    public void A05() {
        A01(this);
        C13150qn A002 = A00(this);
        Fragment fragment = A002.A01;
        if (fragment == null) {
            A002.A00();
        } else {
            C16290wo r2 = A002.A05;
            r2.A07(2130771978, 0);
            r2.A0J(fragment);
        }
        A002.A03();
        Fragment fragment2 = A002.A03;
        if (fragment2 != null) {
            C15830w3.A01(fragment2);
            C16290wo r22 = A002.A05;
            r22.A07(0, 2130771979);
            r22.A0I(A002.A03);
        }
        Fragment fragment3 = A002.A00;
        if (fragment3 != null) {
            C16290wo r23 = A002.A05;
            r23.A07(0, 2130771979);
            r23.A0I(fragment3);
            A002.A00 = null;
        }
        A002.A01();
    }

    public void A06() {
        A01(this);
        C13150qn A002 = A00(this);
        A002.A04();
        A002.A03();
        Fragment fragment = A002.A03;
        if (fragment != null) {
            C15830w3.A01(fragment);
            C16290wo r2 = A002.A05;
            r2.A07(0, 2130771979);
            r2.A0I(A002.A03);
        }
        A002.A01();
    }

    public void A07(ThreadViewParams threadViewParams) {
        Preconditions.checkNotNull(threadViewParams);
        C08790fx.A04(this.A09, 5505032);
        C08860g6 r3 = this.A02;
        r3.A01 = AnonymousClass07B.A01;
        ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r3.A00)).AOI(r3.A05, "threadview_transition_start");
        C13150qn A002 = A00(this);
        C16290wo r2 = A002.A05;
        r2.A07(2130771980, 0);
        Fragment fragment = A002.A04;
        if (fragment != null) {
            r2.A0J(fragment);
            ((ThreadViewFragment) A002.A04).A2V(threadViewParams);
        } else {
            ThreadViewFragment A052 = ThreadViewFragment.A05(null);
            A002.A05.A08(2131301002, A052);
            A052.A2V(threadViewParams);
            A002.A04 = A052;
        }
        A002.A02();
        Fragment fragment2 = A002.A03;
        if (fragment2 != null) {
            C16290wo r22 = A002.A05;
            r22.A07(0, 2130771981);
            r22.A0H(fragment2);
        }
        Fragment fragment3 = A002.A00;
        if (fragment3 != null && !fragment3.A0b) {
            C16290wo r23 = A002.A05;
            r23.A07(0, 2130771979);
            r23.A0H(fragment3);
        }
        A002.A01();
        Fragment A0O = this.A01.A0O(2131301002);
        if (A0O instanceof ThreadViewFragment) {
            ((ThreadViewFragment) A0O).A2T();
        }
        C13400rN r1 = (C13400rN) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A7C, this.A00);
        r1.A00 = threadViewParams.A02;
        C13400rN.A01(r1, "thread");
    }
}
