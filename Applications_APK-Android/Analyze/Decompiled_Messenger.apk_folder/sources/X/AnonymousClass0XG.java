package X;

import com.facebook.common.stringformat.StringFormatUtil;

/* renamed from: X.0XG  reason: invalid class name */
public final class AnonymousClass0XG {
    public static int A01(long j) {
        return (int) (j & 65535);
    }

    public static boolean A03(long j) {
        return ((j >> 61) & 1) == 1;
    }

    public static int A00(long j) {
        return AnonymousClass0XH.A00((int) ((j >>> 48) & 63));
    }

    public static boolean A02(long j) {
        char c;
        int i = (int) ((j >>> 54) & 63);
        if (i == 0) {
            c = 0;
        } else if (i == 1) {
            c = 1;
        } else if (i == 2) {
            c = 2;
        } else {
            throw new IllegalArgumentException(StringFormatUtil.formatStrLocaleSafe("%d is not a MobileConfigUnitType", Integer.valueOf(i)));
        }
        if (c != 1) {
            return false;
        }
        return true;
    }
}
