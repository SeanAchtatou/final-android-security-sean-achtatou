package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.0u0  reason: invalid class name */
public final class AnonymousClass0u0 {
    private static C05540Zi A01;
    public final C25051Yd A00;

    public static final AnonymousClass0u0 A00(AnonymousClass1XY r4) {
        AnonymousClass0u0 r0;
        synchronized (AnonymousClass0u0.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new AnonymousClass0u0((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass0u0) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass0u0(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
