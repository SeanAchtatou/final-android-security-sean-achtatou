package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.0V2  reason: invalid class name */
public final class AnonymousClass0V2 {
    private static AnonymousClass0V2 A05;
    private static final Object A06 = new Object();
    public final Context A00;
    public final Handler A01;
    public final ArrayList A02 = new ArrayList();
    public final HashMap A03 = new HashMap();
    public final HashMap A04 = new HashMap();

    public static AnonymousClass0V2 A00(Context context) {
        AnonymousClass0V2 r0;
        synchronized (A06) {
            if (A05 == null) {
                A05 = new AnonymousClass0V2(context.getApplicationContext());
            }
            r0 = A05;
        }
        return r0;
    }

    public void A01(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        synchronized (this.A04) {
            C06580bj r5 = new C06580bj(intentFilter, broadcastReceiver);
            ArrayList arrayList = (ArrayList) this.A04.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList(1);
                this.A04.put(broadcastReceiver, arrayList);
            }
            arrayList.add(r5);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                String action = intentFilter.getAction(i);
                ArrayList arrayList2 = (ArrayList) this.A03.get(action);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(1);
                    this.A03.put(action, arrayList2);
                }
                arrayList2.add(r5);
            }
        }
    }

    private AnonymousClass0V2(Context context) {
        this.A00 = context;
        this.A01 = new AnonymousClass0V3(this, context.getMainLooper());
    }
}
