package X;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/* renamed from: X.1b0  reason: invalid class name and case insensitive filesystem */
public final class C26161b0 implements C26171b1 {
    public static C26161b0 A03;
    public boolean A00 = false;
    public final C26181b2 A01;
    public final Queue A02 = new LinkedList();

    public static C26161b0 A00() {
        C26161b0 r0;
        synchronized (C26161b0.class) {
            if (A03 == null) {
                A03 = new C26161b0(null);
            }
            r0 = A03;
        }
        return r0;
    }

    public static void A02(C26161b0 r3) {
        synchronized (r3.A02) {
            while (!r3.A02.isEmpty()) {
                A01((C26461bU) r3.A02.remove(), C26621bk.A00);
            }
        }
    }

    public void BJ9(C26461bU r6) {
        if (this.A00) {
            Set set = C26621bk.A00;
            boolean z = !set.isEmpty();
            boolean z2 = false;
            if (this.A02.size() < 50) {
                z2 = true;
            }
            if (!z || z2) {
                synchronized (this.A02) {
                    this.A02.add(r6);
                }
            } else {
                A01(r6, set);
                A02(this);
            }
            if (r6.A04) {
                int i = r6.A00;
                if (i == 2 || i == 4) {
                    A01(r6, C26621bk.A01);
                }
            }
        }
    }

    private C26161b0(C26181b2 r2) {
        this.A01 = r2 == null ? C26181b2.A01 : r2;
    }

    public static void A01(C26461bU r2, Set set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            ((C26171b1) it.next()).BJ9(r2);
        }
    }
}
