package X;

import android.app.job.JobParameters;
import android.app.job.JobWorkItem;
import android.content.Intent;

/* renamed from: X.0Hn  reason: invalid class name and case insensitive filesystem */
public final class C03010Hn implements AnonymousClass0E7 {
    public final JobWorkItem A00;
    public final /* synthetic */ C02990Hl A01;

    public C03010Hn(C02990Hl r1, JobWorkItem jobWorkItem) {
        this.A01 = r1;
        this.A00 = jobWorkItem;
    }

    public void ATZ() {
        synchronized (this.A01.A02) {
            JobParameters jobParameters = this.A01.A00;
            if (jobParameters != null) {
                jobParameters.completeWork(this.A00);
            }
        }
    }

    public Intent getIntent() {
        return this.A00.getIntent();
    }
}
