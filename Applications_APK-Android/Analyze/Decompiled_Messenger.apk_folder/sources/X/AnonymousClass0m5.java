package X;

import com.facebook.litho.ComponentTree;

/* renamed from: X.0m5  reason: invalid class name */
public final class AnonymousClass0m5 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.litho.ComponentTree$1";
    public final /* synthetic */ ComponentTree A00;

    public AnonymousClass0m5(ComponentTree componentTree) {
        this.A00 = componentTree;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0010, code lost:
        r7 = r1.A0T;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0012, code lost:
        if (r7 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0014, code lost:
        r7 = r1.A0S.A05();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001a, code lost:
        if (r7 == null) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001c, code lost:
        r1 = r1.A0S;
        r6 = X.C637138j.A00(r1, r7, r7.BLg(r1, 8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0028, code lost:
        r10 = X.C27041cY.A02();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        if (r10 == false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0030, code lost:
        X.C27041cY.A01(X.AnonymousClass08S.A0J("preAllocateMountContent:", r8.A0B.A1A()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003d, code lost:
        r0 = r8.A0e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003f, code lost:
        if (r0 == null) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0045, code lost:
        if (r0.isEmpty() != false) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0047, code lost:
        r4 = 0;
        r3 = r8.A0e.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004e, code lost:
        if (r4 >= r3) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
        r2 = ((X.C17730zN) r8.A0e.get(r4)).A09;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005a, code lost:
        if (r9 == false) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0060, code lost:
        if (r2.A0t() != false) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0062, code lost:
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0069, code lost:
        if (X.C17770zR.A09(r2) == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006b, code lost:
        if (r10 == false) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006d, code lost:
        X.C27041cY.A01(X.AnonymousClass08S.A0J("preAllocateMountContent:", r2.A1A()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0078, code lost:
        r1 = r8.A0a.A09;
        r0 = X.C15040ud.A00(r1, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0080, code lost:
        if (r0 == null) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0082, code lost:
        r0.BKc(r1, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0085, code lost:
        if (r10 == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0087, code lost:
        X.C27041cY.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x008b, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x008d, code lost:
        if (r10 == false) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x008f, code lost:
        X.C27041cY.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0092, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0094, code lost:
        r7.BJI(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r11 = this;
            com.facebook.litho.ComponentTree r1 = r11.A00
            boolean r9 = r1.A0h
            monitor-enter(r1)
            X.15v r8 = r1.A09     // Catch:{ all -> 0x0098 }
            if (r8 != 0) goto L_0x000f
            X.15v r8 = r1.A08     // Catch:{ all -> 0x0098 }
            if (r8 != 0) goto L_0x000f
            monitor-exit(r1)     // Catch:{ all -> 0x0098 }
            return
        L_0x000f:
            monitor-exit(r1)     // Catch:{ all -> 0x0098 }
            X.38i r7 = r1.A0T
            if (r7 != 0) goto L_0x001a
            X.0p4 r0 = r1.A0S
            X.38i r7 = r0.A05()
        L_0x001a:
            if (r7 == 0) goto L_0x008b
            X.0p4 r1 = r1.A0S
            r0 = 8
            X.3eb r0 = r7.BLg(r1, r0)
            X.3eb r6 = X.C637138j.A00(r1, r7, r0)
        L_0x0028:
            boolean r10 = X.C27041cY.A02()
            java.lang.String r5 = "preAllocateMountContent:"
            if (r10 == 0) goto L_0x003d
            X.0zR r0 = r8.A0B
            java.lang.String r0 = r0.A1A()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r5, r0)
            X.C27041cY.A01(r0)
        L_0x003d:
            java.util.List r0 = r8.A0e
            if (r0 == 0) goto L_0x008d
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x008d
            r4 = 0
            java.util.List r0 = r8.A0e
            int r3 = r0.size()
        L_0x004e:
            if (r4 >= r3) goto L_0x008d
            java.util.List r0 = r8.A0e
            java.lang.Object r0 = r0.get(r4)
            X.0zN r0 = (X.C17730zN) r0
            X.0zR r2 = r0.A09
            if (r9 == 0) goto L_0x0065
            boolean r0 = r2.A0t()
            if (r0 != 0) goto L_0x0065
        L_0x0062:
            int r4 = r4 + 1
            goto L_0x004e
        L_0x0065:
            boolean r0 = X.C17770zR.A09(r2)
            if (r0 == 0) goto L_0x0062
            if (r10 == 0) goto L_0x0078
            java.lang.String r0 = r2.A1A()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r5, r0)
            X.C27041cY.A01(r0)
        L_0x0078:
            X.0p4 r0 = r8.A0a
            android.content.Context r1 = r0.A09
            X.0vh r0 = X.C15040ud.A00(r1, r2)
            if (r0 == 0) goto L_0x0085
            r0.BKc(r1, r2)
        L_0x0085:
            if (r10 == 0) goto L_0x0062
            X.C27041cY.A00()
            goto L_0x0062
        L_0x008b:
            r6 = 0
            goto L_0x0028
        L_0x008d:
            if (r10 == 0) goto L_0x0092
            X.C27041cY.A00()
        L_0x0092:
            if (r6 == 0) goto L_0x0097
            r7.BJI(r6)
        L_0x0097:
            return
        L_0x0098:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0098 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m5.run():void");
    }
}
