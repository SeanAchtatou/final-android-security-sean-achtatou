package X;

import android.media.AudioManager;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.util.TriState;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.rtc.interfaces.RtcCallStartParams;
import com.facebook.user.model.UserKey;
import com.facebook.webrtc.ConferenceCall;
import com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo;
import com.facebook.webrtc.videopause.VideoPauseParameters;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

@UserScoped
/* renamed from: X.0sv  reason: invalid class name and case insensitive filesystem */
public final class C14260sv implements C32651m6 {
    private static C05540Zi A0z;
    public int A00 = 0;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07 = 0;
    public long A08;
    public AnonymousClass0UN A09;
    public ThreadKey A0A;
    public ThreadSummary A0B;
    public C163907iB A0C;
    public RtcCallStartParams A0D;
    public FbWebrtcConferenceParticipantInfo A0E;
    public VideoPauseParameters A0F;
    public Integer A0G;
    public Integer A0H = AnonymousClass07B.A00;
    public Integer A0I = AnonymousClass07B.A01;
    public String A0J = BuildConfig.FLAVOR;
    public String A0K = BuildConfig.FLAVOR;
    public String A0L = BuildConfig.FLAVOR;
    public String A0M = BuildConfig.FLAVOR;
    public ArrayList A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;
    public boolean A0k;
    public boolean A0l;
    public boolean A0m;
    public boolean A0n;
    public boolean A0o;
    public boolean A0p;
    public boolean A0q;
    public boolean A0r;
    public boolean A0s;
    public boolean A0t;
    public boolean A0u;
    public boolean A0v;
    private TriState A0w = TriState.UNSET;
    private final C35001qW A0x;
    private final Set A0y = Collections.newSetFromMap(new WeakHashMap());

    public static final C14260sv A00(AnonymousClass1XY r4) {
        C14260sv r0;
        synchronized (C14260sv.class) {
            C05540Zi A002 = C05540Zi.A00(A0z);
            A0z = A002;
            try {
                if (A002.A03(r4)) {
                    A0z.A00 = new C14260sv((AnonymousClass1XY) A0z.A01());
                }
                C05540Zi r1 = A0z;
                r0 = (C14260sv) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A0z.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A01(C14260sv r6) {
        r6.A05 = 0;
        r6.A04 = 0;
        r6.A08 = 0;
        r6.A02 = 0;
        r6.A0H = AnonymousClass07B.A00;
        r6.A0U = false;
        r6.A0D = null;
        r6.A0E(0);
        r6.A0M(null);
        r6.A0G(0);
        r6.A0J = BuildConfig.FLAVOR;
        r6.A0O(BuildConfig.FLAVOR);
        r6.A0Y(false);
        r6.A0Z = false;
        r6.A0h = false;
        r6.A0T(false);
        if (r6.A0m) {
            r6.A0m = false;
            A02(r6, new C160777cU());
        }
        if (false != r6.A0O) {
            r6.A0O = false;
            A02(r6, new C160787cV());
        }
        r6.A0W = false;
        r6.A06 = 0;
        A02(r6, new C160807cX());
        r6.A0X = false;
        r6.A0u = false;
        r6.A0q = false;
        r6.A0l = false;
        r6.A0R(false);
        r6.A0U(false);
        r6.A0Z(false);
        if (!C06850cB.A0C(BuildConfig.FLAVOR, r6.A0L)) {
            r6.A0L = BuildConfig.FLAVOR;
        }
        if (!C06850cB.A0C(BuildConfig.FLAVOR, r6.A0K)) {
            r6.A0K = BuildConfig.FLAVOR;
        }
        r6.A07 = 0;
        r6.A0P(false);
        r6.A01 = 0;
        r6.A0R = false;
        r6.A0S = false;
        r6.A0H(null);
        r6.A0I(null);
        r6.A0c = false;
        r6.A0T = false;
        A02(r6, new C160837ca());
        if (r6.A0g) {
            r6.A0g = false;
            A02(r6, new C160827cZ());
        }
        r6.A0E = null;
        r6.A0a = false;
        r6.A0t = false;
        r6.A0w = TriState.UNSET;
        r6.A0N(AnonymousClass07B.A01);
        r6.A0W(false);
        r6.A0V(false);
        if (r6.A0P) {
            r6.A0P = false;
            A02(r6, new C165947lg());
        }
        r6.A0X(false);
        if (r6.A0b) {
            r6.A0b = false;
            A02(r6, new C161057d2());
        }
        r6.A0F(0);
        r6.A0F = null;
        r6.A0Y = false;
        r6.A0N = null;
        if (r6.A0i) {
            r6.A0i = false;
            A02(r6, new C160797cW());
        }
    }

    public static void A02(C14260sv r2, C161167dD r3) {
        for (C47042Sz C48 : new HashSet(r2.A0y)) {
            r3.C48(C48);
        }
    }

    public final int A04() {
        return ((C14270sw) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AeR, this.A09)).A00;
    }

    public C163917iC A06(C162567fa r3) {
        C163907iB r1 = this.A0C;
        if (r1 == null) {
            return null;
        }
        return new C163917iC(r1, r3);
    }

    public UserKey A07() {
        long j = this.A07;
        if (j == 0) {
            return null;
        }
        return UserKey.A00(Long.valueOf(j));
    }

    public String A09() {
        ThreadKey threadKey = this.A0A;
        if (ThreadKey.A09(threadKey) && threadKey != null) {
            return String.valueOf(threadKey.A0G());
        }
        C163907iB r0 = this.A0C;
        if (r0 == null) {
            return null;
        }
        String ATl = r0.ATl();
        if (!C06850cB.A0B(ATl) && ATl.startsWith("GROUP:")) {
            String replace = ATl.replace("GROUP:", BuildConfig.FLAVOR);
            if (C06850cB.A0B(replace)) {
                return null;
            }
            return replace;
        }
        return null;
    }

    public String A0A() {
        C163907iB r0 = this.A0C;
        if (r0 != null) {
            return r0.ATl();
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r4.A0B == null) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A0B() {
        /*
            r4 = this;
            X.7iB r0 = r4.A0C
            if (r0 == 0) goto L_0x0009
            com.facebook.messaging.model.threads.ThreadSummary r1 = r4.A0B
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 != 0) goto L_0x000e
            r0 = 0
            return r0
        L_0x000e:
            r2 = 4
            int r1 = X.AnonymousClass1Y3.A8j
            X.0UN r0 = r4.A09
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.7OU r3 = (X.AnonymousClass7OU) r3
            com.facebook.messaging.model.threads.ThreadSummary r1 = r4.A0B
            if (r1 != 0) goto L_0x003f
            r2 = 0
        L_0x001e:
            r1 = 0
            if (r2 == 0) goto L_0x0027
            boolean r0 = r2.A02
            if (r0 == 0) goto L_0x0028
            java.lang.String r1 = r2.A01
        L_0x0027:
            return r1
        L_0x0028:
            com.google.common.collect.ImmutableList r0 = r2.A00
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0027
            X.0Tq r0 = r3.A06
            java.lang.Object r1 = r0.get()
            X.1Sb r1 = (X.C24061Sb) r1
            com.google.common.collect.ImmutableList r0 = r2.A00
            java.lang.String r1 = r1.A04(r0)
            return r1
        L_0x003f:
            X.0Tq r0 = r3.A03
            java.lang.Object r0 = r0.get()
            X.1Ke r0 = (X.C22171Ke) r0
            com.facebook.messaging.ui.name.MessengerThreadNameViewData r2 = r0.A03(r1)
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14260sv.A0B():java.lang.String");
    }

    public String A0C() {
        if (C06850cB.A0B(this.A0K)) {
            return this.A0L;
        }
        return this.A0K;
    }

    public String A0D() {
        if (C06850cB.A0B(this.A0L)) {
            return this.A0K;
        }
        return this.A0L;
    }

    public void A0E(int i) {
        int i2 = AnonymousClass1Y3.AeR;
        int i3 = ((C14270sw) AnonymousClass1XX.A02(3, i2, this.A09)).A00;
        if (i3 != i) {
            C157217Oq.A02("RtcCallStateImpl", "Changing call state from: %d, to: %d", Integer.valueOf(i3), Integer.valueOf(i));
            ((C14270sw) AnonymousClass1XX.A02(3, i2, this.A09)).A00 = i;
            A02(this, new C165047k9(i3, i));
        }
    }

    public void A0F(int i) {
        if (this.A00 != i) {
            this.A00 = i;
            A02(this, new C161047d1());
        }
    }

    public void A0G(long j) {
        if (this.A03 != j) {
            this.A03 = j;
            A02(this, new C161027cz());
        }
    }

    public void A0H(ThreadKey threadKey) {
        if (!C14620th.A01(this.A0A, threadKey)) {
            this.A0A = threadKey;
            A02(this, new C161097d6());
        }
    }

    public void A0I(ThreadSummary threadSummary) {
        if (!C14620th.A01(this.A0B, threadSummary)) {
            if (threadSummary != null) {
                Preconditions.checkArgument(C14620th.A01(threadSummary.A0S, this.A0A));
            }
            this.A0B = threadSummary;
            A02(this, new C167707od());
        }
    }

    public void A0J(C162567fa r5) {
        C163907iB r3 = this.A0C;
        if (r3 != null) {
            Preconditions.checkNotNull(r5);
            C157217Oq.A02(AnonymousClass80H.$const$string(AnonymousClass1Y3.A2l), "Updating Subscription source from %s to %s", r3.A01, r5);
            r3.A01 = r5;
        }
    }

    public void A0K(C47042Sz r2) {
        this.A0y.add(r2);
    }

    public void A0L(C47042Sz r2) {
        this.A0y.remove(r2);
    }

    public void A0M(ConferenceCall conferenceCall) {
        C163907iB r2 = this.A0C;
        if (r2 != null) {
            ((C34961qS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Axj, this.A09)).A07(r2);
        }
        if (conferenceCall == null) {
            this.A0C = null;
        } else {
            C163907iB r22 = new C163907iB(this.A0x, conferenceCall);
            this.A0C = r22;
            ((C34961qS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Axj, this.A09)).A06(r22);
        }
        A02(this, new C160927cl());
    }

    public void A0N(Integer num) {
        Integer num2 = this.A0I;
        if (num2 != num) {
            this.A0I = num;
            boolean z = this.A0U;
            boolean z2 = false;
            if (num == AnonymousClass07B.A00) {
                z2 = true;
            }
            this.A0U = z | z2;
            A02(this, new C165677lD(num2, num));
        }
    }

    public void A0O(String str) {
        if (!str.equals(this.A0M)) {
            this.A0M = str;
            A02(this, new C161087d5());
        }
    }

    public void A0P(boolean z) {
        if (this.A0Q != z) {
            this.A0Q = z;
            A02(this, new C161107d7());
        }
    }

    public void A0Q(boolean z) {
        if (this.A0w != TriState.valueOf(z)) {
            this.A0w = TriState.valueOf(z);
            A02(this, new C161077d4());
        }
    }

    public void A0R(boolean z) {
        if (z != this.A0V) {
            this.A0V = z;
            A02(this, new C160997cw());
        }
    }

    public void A0S(boolean z) {
        if (this.A0d != z) {
            this.A0d = z;
            A02(this, new C161037d0());
        }
    }

    public void A0T(boolean z) {
        if (this.A0n != z) {
            this.A0n = z;
            this.A0m = false;
            A02(this, new C161017cy());
        }
    }

    public void A0U(boolean z) {
        if (z != this.A0o) {
            this.A0o = z;
            A02(this, new C161127d9());
        }
    }

    public void A0V(boolean z) {
        if (this.A0r != z) {
            this.A0r = z;
            A02(this, new C167617oU());
        }
    }

    public void A0W(boolean z) {
        if (this.A0s != z) {
            this.A0s = z;
            A02(this, new C167627oV());
        }
    }

    public void A0X(boolean z) {
        if (this.A0v != z) {
            this.A0v = z;
            A02(this, new C161067d3());
        }
    }

    public void A0Y(boolean z) {
        if (z != this.A0j) {
            this.A0j = z;
            A02(this, new C161007cx());
        }
    }

    public void A0Z(boolean z) {
        if (z != this.A0k) {
            this.A0k = z;
            A02(this, new C161117d8());
        }
    }

    public boolean A0e() {
        if (((C32621m3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayw, this.A09)).A0C().size() > 2) {
            return true;
        }
        return false;
    }

    public boolean A0h() {
        if (this.A0l) {
            return false;
        }
        boolean z = false;
        if (this.A0I == AnonymousClass07B.A0C) {
            z = true;
        }
        if (z || A0i()) {
            return true;
        }
        return false;
    }

    public boolean A0i() {
        if (this.A0I == AnonymousClass07B.A00) {
            return true;
        }
        return false;
    }

    public boolean A0j() {
        if (this.A0f || ((AudioManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AAT, this.A09)).isMicrophoneMute()) {
            return true;
        }
        return false;
    }

    public boolean A0k() {
        boolean z;
        C163907iB r3 = this.A0C;
        if (r3 == null) {
            return false;
        }
        if (r3.A05) {
            z = false;
        } else {
            z = false;
            if (r3.A02.mConferenceType == C163247gm.A02) {
                z = true;
            }
        }
        if (z || this.A0T || r3.A02()) {
            return true;
        }
        boolean z2 = false;
        if (r3.A02.mConferenceType == C163247gm.A03) {
            z2 = true;
        }
        if (z2) {
            return true;
        }
        return false;
    }

    public boolean A0o() {
        boolean z;
        if (!this.A0u || (!A0k() && !this.A0q)) {
            z = false;
        } else {
            z = true;
        }
        if (!z || !this.A0s) {
            return false;
        }
        return true;
    }

    public boolean A0p() {
        if (this.A0C != null) {
            return true;
        }
        return false;
    }

    public boolean A0q() {
        if (!this.A0j || !A0d()) {
            return false;
        }
        return true;
    }

    public final boolean A0r() {
        if (this.A06 > 0) {
            return true;
        }
        return false;
    }

    public final boolean A0s() {
        return ((C14270sw) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AeR, this.A09)).A01();
    }

    public final boolean A0t() {
        boolean z = false;
        if (((C14270sw) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AeR, this.A09)).A00 == 4) {
            z = true;
        }
        if (!z || !A0r()) {
            return false;
        }
        return true;
    }

    public final boolean A0u() {
        if (((C14270sw) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AeR, this.A09)).A00 == 0) {
            return true;
        }
        return false;
    }

    public final boolean A0v() {
        if (((C14270sw) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AeR, this.A09)).A00 == 4) {
            return true;
        }
        return false;
    }

    public Map Bxv() {
        AnonymousClass04a r2 = new AnonymousClass04a();
        r2.put("callId", Long.toString(this.A03));
        r2.put("isVideoCall", Boolean.toString(this.A0j));
        r2.put("incomingVideoEscalating", Boolean.toString(this.A0V));
        r2.put("outgoingVideoEscalating", Boolean.toString(this.A0o));
        r2.put("hasMultiwayVideoEscalationSucceeded", this.A0w.toString());
        r2.put("isVideoResponding", Boolean.toString(this.A0k));
        r2.put("isDirectVideoCall", Boolean.toString(this.A0Z));
        r2.put("selfSupportVideo", Boolean.toString(this.A0u));
        r2.put("peerSupportVideo", Boolean.toString(this.A0q));
        r2.put("localVideoMuted", Boolean.toString(this.A0l));
        r2.put("localVideoState", C163357h7.A00(this.A0I));
        r2.put("remoteVideoOn", Boolean.toString(this.A0s));
        r2.put("hasSharedLocalVideo", Boolean.toString(this.A0U));
        r2.put("disableSendMediaStatusOnVideoStarted", Boolean.toString(this.A0P));
        r2.put("shouldEscalateToVideoOnConnection", Boolean.toString(this.A0v));
        return r2;
    }

    private C14260sv(AnonymousClass1XY r4) {
        this.A09 = new AnonymousClass0UN(6, r4);
        AnonymousClass0WA.A00(r4);
        this.A0x = new C35001qW(r4);
        new C35011qX(r4);
    }

    private boolean A03(boolean z, boolean z2) {
        if (!A0k()) {
            return false;
        }
        return C32621m3.A08((C32621m3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayw, this.A09), new C161807eL(z2), z);
    }

    public long A05() {
        if (!A0t()) {
            return 0;
        }
        return ((AnonymousClass069) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Asv, this.A09)).now() - this.A06;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r2.A0B == null) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A08() {
        /*
            r2 = this;
            boolean r0 = r2.A0k()
            if (r0 == 0) goto L_0x0017
            X.7iB r0 = r2.A0C
            if (r0 == 0) goto L_0x000f
            com.facebook.messaging.model.threads.ThreadSummary r1 = r2.A0B
            r0 = 1
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 == 0) goto L_0x0017
            java.lang.String r0 = r2.A0B()
            return r0
        L_0x0017:
            java.lang.String r0 = r2.A0D()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14260sv.A08():java.lang.String");
    }

    public boolean A0a() {
        if (A0k()) {
            return C32621m3.A08((C32621m3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayw, this.A09), new C161837eO(), false);
        }
        return A0r();
    }

    public boolean A0b() {
        if (A04() != 0) {
            return true;
        }
        return false;
    }

    public boolean A0c() {
        boolean z = true;
        if (A04() != 1) {
            z = false;
        }
        if (!z || !this.A0h || this.A0n) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (A0g() != false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0d() {
        /*
            r2 = this;
            boolean r0 = r2.A0c()
            if (r0 != 0) goto L_0x0023
            int r1 = r2.A04()
            r0 = 1
            if (r1 == r0) goto L_0x000e
            r0 = 0
        L_0x000e:
            if (r0 == 0) goto L_0x0018
            boolean r0 = r2.A0h
            if (r0 == 0) goto L_0x0018
            boolean r0 = r2.A0n
            if (r0 != 0) goto L_0x001f
        L_0x0018:
            boolean r0 = r2.A0g()
            r1 = 0
            if (r0 == 0) goto L_0x0020
        L_0x001f:
            r1 = 1
        L_0x0020:
            r0 = 0
            if (r1 == 0) goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14260sv.A0d():boolean");
    }

    public boolean A0f() {
        if (!A0k() || !this.A0j) {
            return false;
        }
        return true;
    }

    public boolean A0g() {
        if (A04() == 2) {
            return true;
        }
        return false;
    }

    public boolean A0l() {
        if (!A0k() || !this.A0s || this.A0U || !A0v() || this.A0w.isSet()) {
            return false;
        }
        return true;
    }

    public boolean A0m() {
        if (A0u() || A0k()) {
            return false;
        }
        return true;
    }

    public boolean A0n() {
        if (A0o() || A03(false, true)) {
            return true;
        }
        return false;
    }

    public boolean A0w(boolean z) {
        if (A0k()) {
            return A03(true, z);
        }
        if (A0i() || this.A0s) {
            return true;
        }
        return false;
    }
}
