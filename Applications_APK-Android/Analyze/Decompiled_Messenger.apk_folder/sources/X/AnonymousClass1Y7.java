package X;

/* renamed from: X.1Y7  reason: invalid class name */
public class AnonymousClass1Y7 extends AnonymousClass063 {
    public /* bridge */ /* synthetic */ AnonymousClass063 A04(AnonymousClass063 r3, String str) {
        if (!(this instanceof AnonymousClass1Y8)) {
            return A0B(r3, str);
        }
        return new AnonymousClass1Y8(r3, str, false);
    }

    public AnonymousClass1Y7 A0A() {
        if (!(this instanceof AnonymousClass1Y8)) {
            return (AnonymousClass1Y7) this.A02;
        }
        return (AnonymousClass1Y8) ((AnonymousClass1Y8) this).A02;
    }

    public AnonymousClass1Y7 A0B(AnonymousClass063 r3, String str) {
        if (!(this instanceof AnonymousClass1Y8)) {
            return new AnonymousClass1Y7(r3, str);
        }
        return new AnonymousClass1Y8(r3, str, false);
    }

    public String A0C() {
        if (!(this instanceof AnonymousClass1Y8)) {
            return this.A03;
        }
        return ((AnonymousClass1Y8) this).A03;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass1Y7)) {
            return false;
        }
        return A08((AnonymousClass1Y7) obj);
    }

    public AnonymousClass1Y7(AnonymousClass063 r1, String str) {
        super(r1, str);
    }

    public AnonymousClass1Y7(String str) {
        super(str);
    }
}
