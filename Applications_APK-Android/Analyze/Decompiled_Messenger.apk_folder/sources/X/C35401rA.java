package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.collect.ImmutableList;

@UserScoped
/* renamed from: X.1rA  reason: invalid class name and case insensitive filesystem */
public final class C35401rA implements C35411rB {
    private static C05540Zi A04;
    private long A00;
    private AnonymousClass0UN A01;
    private boolean A02;
    private final AnonymousClass04a A03 = new AnonymousClass04a();

    public synchronized double Amf(String str, String... strArr) {
        double d;
        Long l = (Long) this.A03.get(str);
        if (l != null) {
            double now = (double) (((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A01)).now() - l.longValue());
            Double.isNaN(now);
            d = now / 60000.0d;
        } else {
            d = 2.147483647E9d;
        }
        return d;
    }

    public static final C35401rA A00(AnonymousClass1XY r4) {
        C35401rA r0;
        synchronized (C35401rA.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new C35401rA((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (C35401rA) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    public void A01(ImmutableList immutableList) {
        C17930zi r1;
        if (this.A02) {
            synchronized (this) {
                C24971Xv it = immutableList.iterator();
                while (it.hasNext()) {
                    ThreadSummary threadSummary = (ThreadSummary) it.next();
                    if (threadSummary.A0B()) {
                        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A01)).now();
                        long j = threadSummary.A06;
                        if (now - j < this.A00 && ((r1 = threadSummary.A0i) == C17930zi.VIDEO_CALL || r1 == C17930zi.VOICE_CALL)) {
                            this.A03.put(threadSummary.A0S.A0I(), Long.valueOf(j));
                        }
                    }
                    this.A03.remove(threadSummary.A0S.A0I());
                }
            }
        }
    }

    public C74973it Ame() {
        return C74973it.A0I;
    }

    private C35401rA(AnonymousClass1XY r9) {
        AnonymousClass0UN r1 = new AnonymousClass0UN(3, r9);
        this.A01 = r1;
        boolean z = true;
        this.A00 = Math.max(((C14970uR) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AEk, this.A01)).A00.At4(563800360878681L, AnonymousClass0XE.A07), (long) ((int) ((C35391r9) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ACN, r1)).A00.At4(563800360026708L, AnonymousClass0XE.A07))) * 3600000;
        boolean A012 = ((C35391r9) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ACN, this.A01)).A01();
        if (!((C14970uR) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AEk, this.A01)).A00.Aem(282325384103247L) && !A012) {
            z = false;
        }
        this.A02 = z;
    }
}
