package X;

/* renamed from: X.1OR  reason: invalid class name */
public final class AnonymousClass1OR implements AnonymousClass1OL {
    private final AnonymousClass1OS A00;

    public void BSA() {
    }

    public void BvE(C33211nD r1) {
    }

    public void BY8(C33211nD r6) {
        this.A00.A03(r6.A01, 1, r6.A00);
    }

    public void Baj(C33211nD r10) {
        AnonymousClass1OS r7 = this.A00;
        Integer num = AnonymousClass07B.A0E;
        long now = r7.A00.now();
        Long l = (Long) r7.A03.get();
        if (l != null) {
            long longValue = l.longValue();
            if (longValue > 0) {
                AnonymousClass1OS.A00(r7, num, now - longValue);
                r7.A03.remove();
            }
        }
        AnonymousClass1OS.A00(r7, AnonymousClass07B.A00, 1);
    }

    public void BfG(C33211nD r5) {
        AnonymousClass1OS r3 = this.A00;
        AnonymousClass1OS.A00(r3, AnonymousClass07B.A01, 1);
        r3.A03.remove();
    }

    public void Bkx(C33211nD r5) {
        AnonymousClass1OS.A00(this.A00, AnonymousClass07B.A0B, 1);
    }

    public void BvC(C33211nD r5) {
        AnonymousClass1OS.A00(this.A00, AnonymousClass07B.A0D, 1);
    }

    public void BvD(C33211nD r5) {
        AnonymousClass1OS.A00(this.A00, AnonymousClass07B.A0A, 1);
    }

    public AnonymousClass1OR(AnonymousClass1OS r1) {
        this.A00 = r1;
    }
}
