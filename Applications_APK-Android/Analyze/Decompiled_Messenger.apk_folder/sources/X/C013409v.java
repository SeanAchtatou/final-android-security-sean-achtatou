package X;

import android.os.Build;
import android.telephony.TelephonyManager;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.09v  reason: invalid class name and case insensitive filesystem */
public final class C013409v {
    static {
        C29981hI.A02 = true;
    }

    public static List A00(TelephonyManager telephonyManager) {
        List list;
        if (C29981hI.A00()) {
            try {
                C29981hI.A01.readLock().lock();
                C29991hJ r2 = C29981hI.A00;
                if (r2.A02 && C25891aZ.A02(r2.A00)) {
                    C25891aZ.A01(r2.A00, C25891aZ.A05);
                }
                if (r2.A01 && C25891aZ.A02(r2.A00)) {
                    list = new ArrayList();
                } else if (Build.VERSION.SDK_INT >= 17) {
                    list = telephonyManager.getAllCellInfo();
                } else {
                    list = new ArrayList();
                }
                return list;
            } finally {
                C29981hI.A01.readLock().unlock();
            }
        } else if (Build.VERSION.SDK_INT >= 17) {
            return telephonyManager.getAllCellInfo();
        } else {
            return new ArrayList();
        }
    }
}
