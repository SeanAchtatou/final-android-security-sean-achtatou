package X;

import java.util.concurrent.CountDownLatch;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1aH  reason: invalid class name and case insensitive filesystem */
public final class C25711aH implements C07250d5 {
    private static volatile C25711aH A02;
    private AnonymousClass0UN A00;
    public final CountDownLatch A01 = new CountDownLatch(1);

    public synchronized void AQQ(Integer num) {
        if (this.A01.getCount() > 0) {
            ((AnonymousClass0WP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8a, this.A00)).CIG("ColdStartCompletedLock-waitForInitialized", new C14240st(this), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY, AnonymousClass07B.A01);
            boolean z = false;
            while (true) {
                try {
                    this.A01.await();
                    break;
                } catch (InterruptedException unused) {
                    z = true;
                } catch (Throwable th) {
                    if (z) {
                        Thread.currentThread().interrupt();
                    }
                    throw th;
                }
            }
            if (z) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public static final C25711aH A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C25711aH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C25711aH(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C25711aH(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
