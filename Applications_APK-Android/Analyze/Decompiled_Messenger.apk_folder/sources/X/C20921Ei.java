package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.quicklog.QuickPerformanceLogger;

@UserScoped
/* renamed from: X.1Ei  reason: invalid class name and case insensitive filesystem */
public final class C20921Ei {
    private static C05540Zi A07;
    public static final int[] A08 = {5505198, 5505218};
    public AnonymousClass0UN A00;
    public boolean A01;
    public boolean A02;
    private int A03 = 0;
    private int A04 = 0;
    private boolean A05 = false;
    public final Object A06 = new Object();

    public static final C20921Ei A00(AnonymousClass1XY r4) {
        C20921Ei r0;
        synchronized (C20921Ei.class) {
            C05540Zi A002 = C05540Zi.A00(A07);
            A07 = A002;
            try {
                if (A002.A03(r4)) {
                    A07.A00 = new C20921Ei((AnonymousClass1XY) A07.A01());
                }
                C05540Zi r1 = A07;
                r0 = (C20921Ei) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A07.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A01(C20921Ei r2) {
        synchronized (r2.A06) {
            r2.A02 = false;
            r2.A01 = false;
            r2.A05 = false;
            r2.A04 = 0;
            r2.A03 = 0;
        }
    }

    public static boolean A02(C20921Ei r3) {
        return ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r3.A00)).isMarkerOn(5505218);
    }

    public static boolean A03(C20921Ei r2) {
        boolean z;
        synchronized (r2.A06) {
            z = r2.A05;
        }
        return z;
    }

    public final void A07(int i, boolean z) {
        int i2;
        for (int i3 : A08) {
            int i4 = AnonymousClass1Y3.BBd;
            if (((QuickPerformanceLogger) AnonymousClass1XX.A02(0, i4, this.A00)).isMarkerOn(i3)) {
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, i4, this.A00)).markerAnnotate(i3, "failure_reason", "overlapped");
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerEnd(i3, 4);
            }
        }
        if (i == 1) {
            if (((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, this.A00)).A0A(5505027)) {
                i2 = 1;
            } else if (((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, this.A00)).A0A(5505026)) {
                i2 = 2;
            } else {
                i2 = 0;
                if (((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, this.A00)).A0A(5505028)) {
                    i2 = 3;
                }
            }
            if (i2 == 0) {
                return;
            }
        } else {
            i2 = 0;
        }
        A01(this);
        int[] iArr = A08;
        for (int markerStart : iArr) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerStart(markerStart);
        }
        synchronized (this.A06) {
            try {
                this.A05 = true;
                this.A04 = i2;
                this.A03 = i;
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (i == 2) {
            synchronized (this.A06) {
                try {
                    this.A01 = true;
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
        }
        int[] iArr2 = A08;
        int length = iArr.length;
        for (int i5 = 0; i5 < length; i5++) {
            C21061Ew withMarker = ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).withMarker(iArr2[i5]);
            if (i == 1) {
                withMarker.A05("start_type", i2);
            }
            withMarker.A05("refresh_type", i);
            withMarker.A0B("m4_enabled", z);
            withMarker.BK9();
        }
        return;
        throw th;
    }

    private C20921Ei(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public final void A04() {
        if (A03(this)) {
            synchronized (this.A06) {
                this.A02 = true;
            }
            for (int markerPoint : A08) {
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerPoint(markerPoint, "stories_loaded_from_memory");
            }
        }
    }

    public final void A05() {
        if (A03(this)) {
            synchronized (this.A06) {
                this.A01 = true;
            }
        }
    }

    public final void A06(int i, String str) {
        boolean z;
        if (i == 5505198) {
            z = A03(this);
        } else if (i == 5505218) {
            z = A02(this);
        } else {
            z = false;
        }
        if (z) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerPoint(i, str);
        }
    }

    public final void A08(String str) {
        boolean z;
        if (A03(this)) {
            for (int i : A08) {
                if (i == 5505198) {
                    z = A03(this);
                } else if (i == 5505218) {
                    z = A02(this);
                } else {
                    z = false;
                }
                if (z) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerPoint(i, str);
                }
            }
        }
    }

    public final void A09(String str) {
        if (A03(this)) {
            for (int i : A08) {
                if (str != null) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(i, "cancel_reason", str);
                }
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerEnd(i, 4);
            }
            A01(this);
        }
    }

    public final void A0A(boolean z) {
        boolean z2;
        boolean z3;
        if (!A03(this)) {
            return;
        }
        if (z || this.A03 != 1 || this.A04 == 2) {
            synchronized (this.A06) {
                z2 = this.A01;
                z3 = this.A02;
            }
            int i = AnonymousClass1Y3.BBd;
            C21061Ew withMarker = ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, i, this.A00)).withMarker(5505198);
            if (z3) {
                withMarker.A03("stories_rendered");
                if (A02(this)) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, i, this.A00)).markerEnd(5505218, 2);
                }
            }
            if (!z2 || !z3) {
                if (!z3) {
                    withMarker.A03("tray_items_rendered_stories_not_loaded");
                }
                if (!z2) {
                    withMarker.A03("tray_items_rendered_active_now_not_loaded");
                }
                withMarker.BK9();
                return;
            }
            withMarker.A0B("bottom_section", z);
            withMarker.BK9();
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerEnd(5505198, 2);
            A01(this);
        }
    }
}
