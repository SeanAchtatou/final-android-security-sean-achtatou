package X;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableMap;
import com.google.common.collect.RegularImmutableSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Xl  reason: invalid class name and case insensitive filesystem */
public final class C24881Xl implements AnonymousClass1Xm {
    public List A00;
    public Set A01;
    private List A02;
    private List A03;
    private Map A04;
    private Map A05;
    private Set A06;
    private Set A07;
    public final Class A08;
    private final AnonymousClass1XX A09;

    private B3Y A00(C22916BKm bKm) {
        if (this.A02 == null) {
            this.A02 = C04300To.A00();
        }
        B3Y b3y = new B3Y();
        b3y.A02 = this.A08;
        b3y.A01 = bKm;
        this.A02.add(b3y);
        return b3y;
    }

    private C38551xc A01(C22916BKm bKm) {
        if (this.A04 == null) {
            this.A04 = AnonymousClass0TG.A04();
        }
        C38551xc r1 = (C38551xc) this.A04.get(bKm);
        if (r1 != null) {
            return r1;
        }
        C38551xc r12 = new C38551xc();
        this.A04.put(bKm, r12);
        return r12;
    }

    public void AOu(C22916BKm bKm) {
        if (this.A06 == null) {
            this.A06 = C25011Xz.A03();
        }
        this.A06.add(bKm);
    }

    public void AOv(Class cls) {
        if (this.A06 == null) {
            this.A06 = C25011Xz.A03();
        }
        this.A06.add(new C22916BKm(cls, C22919BKp.INSTANCE));
    }

    public C30187ErE APs(Class cls) {
        return new C30187ErE(A00(new C22916BKm(cls, C22919BKp.INSTANCE)));
    }

    public C30360Eul AQ4(Class cls) {
        C22916BKm bKm = new C22916BKm(cls, C22919BKp.INSTANCE);
        if (this.A00 == null) {
            this.A00 = C04300To.A00();
        }
        C38521xZ r1 = new C38521xZ();
        r1.A01 = this.A08;
        r1.A00 = bKm;
        this.A00.add(r1);
        return new C30360Eul();
    }

    public C30187ErE AQ6(Class cls) {
        B3Y A002 = A00(new C22916BKm(cls, C22919BKp.INSTANCE));
        A002.A01(true);
        return new C30187ErE(A002);
    }

    public C38551xc AQB(Class cls) {
        return A01(new C22916BKm(cls, C22919BKp.INSTANCE));
    }

    public C38551xc AQC(Class cls, Class cls2) {
        return A01(new C22916BKm(cls, C22916BKm.A00(cls2)));
    }

    public void AQE(Class cls, C24891Xn r3) {
        if (this.A05 == null) {
            this.A05 = AnonymousClass0TG.A04();
        }
        this.A05.put(cls, r3);
    }

    public void AWd(C22916BKm bKm) {
        if (this.A07 == null) {
            this.A07 = C25011Xz.A03();
        }
        this.A07.add(bKm);
    }

    public void AWe(Class cls) {
        AWd(new C22916BKm(cls, C22919BKp.INSTANCE));
    }

    public void AWf(Class cls, Class cls2) {
        AWd(new C22916BKm(cls, C22916BKm.A00(cls2)));
    }

    public List Aho() {
        List list = this.A00;
        if (list == null) {
            return RegularImmutableList.A02;
        }
        return list;
    }

    public AnonymousClass0UH Aq2() {
        return this.A09;
    }

    public Set Av9() {
        Set set = this.A07;
        if (set == null) {
            return RegularImmutableSet.A05;
        }
        return set;
    }

    public Map AvA() {
        Map map = this.A04;
        if (map == null) {
            return RegularImmutableMap.A03;
        }
        return map;
    }

    public List B12() {
        List list = this.A03;
        if (list == null) {
            return RegularImmutableList.A02;
        }
        return list;
    }

    public Map B1y() {
        Map map = this.A05;
        if (map == null) {
            return RegularImmutableMap.A03;
        }
        return map;
    }

    public void C3N(Class cls) {
        if (this.A03 == null) {
            this.A03 = new ArrayList();
        }
        this.A03.add(cls);
        if (this.A01 == null) {
            this.A01 = C25011Xz.A03();
        }
        this.A01.add(cls);
    }

    public C24881Xl(AnonymousClass1XX r1, Class cls) {
        this.A09 = r1;
        this.A08 = cls;
    }

    public C30183ErA APt(C22916BKm bKm) {
        return new C30187ErE(A00(bKm));
    }

    public void AQ3(Class cls) {
        APs(cls).CJ8(new C196229Ko(cls));
    }

    public C30183ErA AQ7(C22916BKm bKm) {
        B3Y A002 = A00(bKm);
        A002.A01(true);
        return new C30187ErE(A002);
    }

    public C38551xc AQA(C22916BKm bKm) {
        return A01(bKm);
    }

    public List AeT() {
        ImmutableList.Builder builder = ImmutableList.builder();
        List list = this.A02;
        if (list != null) {
            HashSet hashSet = new HashSet(list.size());
            for (B3Y b3y : this.A02) {
                Integer valueOf = Integer.valueOf(AnonymousClass1Y3.A00(b3y.A01));
                if (!hashSet.contains(valueOf)) {
                    hashSet.add(valueOf);
                    builder.add((Object) b3y);
                }
            }
        }
        return builder.build();
    }
}
