package X;

/* renamed from: X.1m4  reason: invalid class name and case insensitive filesystem */
public enum C32631m4 {
    UNKNOWN,
    DISCONNECTED,
    NO_ANSWER,
    REJECTED,
    UNREACHABLE,
    CONNECTION_DROPPED,
    CONTACTING,
    RINGING,
    CONNECTING,
    CONNECTED,
    PARTICIPANT_LIMIT_REACHED,
    IN_ANOTHER_CALL,
    RING_TYPE_UNSUPPORTED;
    
    public static final C32631m4[] A00 = values();
}
