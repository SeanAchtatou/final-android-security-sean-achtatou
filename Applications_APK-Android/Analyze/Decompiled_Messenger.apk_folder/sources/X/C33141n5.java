package X;

import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1n5  reason: invalid class name and case insensitive filesystem */
public final class C33141n5 {
    private static volatile C33141n5 A01;
    public final Set A00;

    public static final C33141n5 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C33141n5.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C33141n5(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C33141n5(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0X5(r3, AnonymousClass0X6.A1Y);
    }
}
