package X;

import com.facebook.gk.sessionless.GkSessionlessModule;
import java.util.concurrent.Executor;

/* renamed from: X.0WO  reason: invalid class name */
public final class AnonymousClass0WO implements C04830Wi {
    private final AnonymousClass1Z9 A00;
    private final AnonymousClass0US[] A01;

    public static final AnonymousClass0WO A00(AnonymousClass1XY r23) {
        AnonymousClass1XY r0 = r23;
        C001500z A05 = AnonymousClass0UU.A05(r0);
        AnonymousClass1YI A002 = AnonymousClass0WA.A00(r0);
        C04840Wj A012 = C04840Wj.A01(r0);
        GkSessionlessModule.A00(r0);
        return new AnonymousClass0WO(A05, A002, A012, AnonymousClass0VB.A00(AnonymousClass1Y3.BA6, r0), AnonymousClass0UQ.A00(AnonymousClass1Y3.ASy, r0), AnonymousClass06M.A01(r0), AnonymousClass0VB.A00(AnonymousClass1Y3.BUD, r0), AnonymousClass0UQ.A00(AnonymousClass1Y3.B8p, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.Aj4, r0), AnonymousClass0UQ.A00(AnonymousClass1Y3.AGb, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.B9q, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.AWx, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.A70, r0), AnonymousClass0UQ.A00(AnonymousClass1Y3.Av7, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.BC5, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.B6u, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.BM9, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.A7k, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.AEB, r0), AnonymousClass0VB.A00(AnonymousClass1Y3.Arv, r0), AnonymousClass09T.A00(r0));
    }

    public int BKI() {
        return this.A00.A02.length;
    }

    public AnonymousClass1Z5 BLq() {
        AnonymousClass0US A02 = this.A00.A02();
        if (A02 != null) {
            return (AnonymousClass1Z5) A02.get();
        }
        return null;
    }

    public void C0T(AnonymousClass0VL r2, C04800Wf r3, Executor executor) {
        this.A00.A03(r2, r3, executor);
    }

    private AnonymousClass0WO(C001500z r36, AnonymousClass1YI r37, C04840Wj r38, AnonymousClass0US r39, AnonymousClass0US r40, AnonymousClass0US r41, AnonymousClass0US r42, AnonymousClass0US r43, AnonymousClass0US r44, AnonymousClass0US r45, AnonymousClass0US r46, AnonymousClass0US r47, AnonymousClass0US r48, AnonymousClass0US r49, AnonymousClass0US r50, AnonymousClass0US r51, AnonymousClass0US r52, AnonymousClass0US r53, AnonymousClass0US r54, AnonymousClass0US r55, AnonymousClass0US r56) {
        AnonymousClass0US r14 = r49;
        AnonymousClass0US r13 = r48;
        AnonymousClass0US r12 = r47;
        AnonymousClass0US r4 = r39;
        AnonymousClass0US r6 = r41;
        AnonymousClass0US r5 = r40;
        AnonymousClass0US r7 = r42;
        AnonymousClass0US r8 = r43;
        AnonymousClass0US r9 = r44;
        AnonymousClass0US r10 = r45;
        AnonymousClass0US r11 = r46;
        this.A01 = new AnonymousClass0US[]{r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r50, r51, r52, r53, r54, r55, r56};
        AnonymousClass0WW r82 = AnonymousClass0WW.A06;
        C04860Wo r102 = new C04860Wo(new AnonymousClass0WW[]{r82});
        AnonymousClass0WW r72 = AnonymousClass0WW.A01;
        C04860Wo r62 = new C04860Wo(new AnonymousClass0WW[]{r82});
        C04860Wo r25 = new C04860Wo(new AnonymousClass0WW[]{r82, r72});
        C001500z r28 = r36;
        AnonymousClass0US[] r26 = this.A01;
        boolean[] zArr = {true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false};
        boolean[] zArr2 = {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false};
        this.A00 = new AnonymousClass1Z9(r26, new C001500z[]{null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}, r28, new Integer[]{null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 447, Integer.valueOf((int) AnonymousClass1Y3.A5N)}, zArr, zArr2, new C04860Wo[]{new C04860Wo(new AnonymousClass0WW[]{r72}), r102, r102, r102, r102, r102, new C04860Wo(new AnonymousClass0WW[]{r72, AnonymousClass0WW.A07, r82, AnonymousClass0WW.A03, AnonymousClass0WW.A04}), r102, r102, r102, r102, r102, r62, new C04860Wo(new AnonymousClass0WW[]{r82}), r102, r102, r25, r102}, r37, r38);
    }
}
