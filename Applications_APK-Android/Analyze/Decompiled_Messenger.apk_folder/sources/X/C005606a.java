package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.06a  reason: invalid class name and case insensitive filesystem */
public final class C005606a {
    private static final List A02 = Arrays.asList("async", "atrace", "qpl", "other", "liger", "fbsystrace", "liger_http2", "system_counters", "stack_trace", "high_freq_main_thread_counters", "transient_network_data", "frames", "main_thread_messages", "libc_io", "memory_allocation", "class_load");
    public final List A00;
    public final boolean A01;

    public C005606a(boolean z, List list) {
        this.A01 = z;
        this.A00 = list == null ? A02 : list;
    }
}
