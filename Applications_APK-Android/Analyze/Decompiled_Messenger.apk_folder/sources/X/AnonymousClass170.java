package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;

/* renamed from: X.170  reason: invalid class name */
public final class AnonymousClass170 {
    public static final Comparator A00 = new C12610pg();
    public static final Comparator A01 = new AnonymousClass178();

    public static ThreadsCollection A00(Collection collection) {
        ArrayList<ImmutableList> arrayList = new ArrayList<>();
        C05180Xy r7 = new C05180Xy(collection.size());
        Iterator it = collection.iterator();
        boolean z = false;
        int i = 0;
        while (it.hasNext()) {
            ThreadsCollection threadsCollection = (ThreadsCollection) it.next();
            ImmutableList immutableList = threadsCollection.A00;
            arrayList.add(immutableList);
            i += immutableList.size();
            if (!threadsCollection.A01 && !immutableList.isEmpty()) {
                r7.add(((ThreadSummary) immutableList.get(immutableList.size() - 1)).A0S);
            }
        }
        PriorityQueue priorityQueue = new PriorityQueue(arrayList.size(), A01);
        ThreadKey threadKey = null;
        ImmutableList immutableList2 = null;
        for (ImmutableList immutableList3 : arrayList) {
            if (!immutableList3.isEmpty()) {
                priorityQueue.offer(C24931Xr.A03(immutableList3.iterator()));
                immutableList2 = immutableList3;
            }
        }
        if (priorityQueue.size() != 1 || immutableList2 == null) {
            ImmutableList.Builder builder = ImmutableList.builder();
            while (!priorityQueue.isEmpty() && !r7.contains(threadKey)) {
                AnonymousClass191 r2 = (AnonymousClass191) priorityQueue.poll();
                ThreadSummary threadSummary = (ThreadSummary) r2.next();
                builder.add((Object) threadSummary);
                threadKey = threadSummary.A0S;
                if (r2.hasNext()) {
                    priorityQueue.offer(r2);
                }
            }
            immutableList2 = builder.build();
        }
        if (immutableList2.size() == i) {
            Iterator it2 = collection.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (!((ThreadsCollection) it2.next()).A01) {
                        break;
                    }
                } else {
                    z = true;
                    break;
                }
            }
        }
        return new ThreadsCollection(immutableList2, z);
    }

    public static ImmutableList A01(ImmutableList immutableList, ImmutableList immutableList2) {
        if (immutableList2 == null || immutableList2.isEmpty()) {
            return immutableList;
        }
        HashSet hashSet = new HashSet();
        ImmutableList.Builder builder = ImmutableList.builder();
        ArrayList arrayList = new ArrayList(immutableList2);
        Collections.sort(arrayList, new C104784za());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ThreadSummary threadSummary = (ThreadSummary) it.next();
            Preconditions.checkNotNull(threadSummary.A0S);
            builder.add((Object) threadSummary);
            hashSet.add(threadSummary.A0S);
        }
        C24971Xv it2 = immutableList.iterator();
        while (it2.hasNext()) {
            ThreadSummary threadSummary2 = (ThreadSummary) it2.next();
            if (!hashSet.contains(threadSummary2.A0S)) {
                builder.add((Object) threadSummary2);
            }
        }
        return builder.build();
    }
}
