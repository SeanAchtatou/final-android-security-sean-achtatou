package X;

import java.util.ArrayList;

/* renamed from: X.1R0  reason: invalid class name */
public final class AnonymousClass1R0 extends AnonymousClass1QN {
    public int A00 = 0;
    public final AnonymousClass1QO[] A01;

    public Object B1I() {
        synchronized (this) {
            if (!BBn()) {
                return null;
            }
            ArrayList arrayList = new ArrayList(r2);
            for (AnonymousClass1QO B1I : this.A01) {
                arrayList.add(B1I.B1I());
            }
            return arrayList;
        }
    }

    public AnonymousClass1R0(AnonymousClass1QO[] r2) {
        this.A01 = r2;
    }

    public boolean AT6() {
        if (!super.AT6()) {
            return false;
        }
        for (AnonymousClass1QO AT6 : this.A01) {
            AT6.AT6();
        }
        return true;
    }
}
