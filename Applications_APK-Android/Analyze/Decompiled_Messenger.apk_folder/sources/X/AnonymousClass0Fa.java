package X;

import android.content.Context;

/* renamed from: X.0Fa  reason: invalid class name */
public final class AnonymousClass0Fa extends C007907e {
    public long A00;
    public long A01;
    public long A02;
    public boolean A03;
    public final Context A04;

    public AnonymousClass0FM A03() {
        return new AnonymousClass0FZ();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r3v1, types: [android.content.Intent] */
    /* JADX WARN: Type inference failed for: r3v3, types: [android.content.Intent] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041 A[SYNTHETIC] */
    public boolean A04(X.AnonymousClass0FM r7) {
        /*
            r6 = this;
            X.0FZ r7 = (X.AnonymousClass0FZ) r7
            java.lang.String r0 = "Null value passed to getSnapshot!"
            X.C02740Gd.A00(r7, r0)
            r3 = 0
            android.content.Context r2 = r6.A04     // Catch:{ Exception -> 0x0016 }
            android.content.IntentFilter r1 = new android.content.IntentFilter     // Catch:{ Exception -> 0x0016 }
            java.lang.String r0 = "android.intent.action.BATTERY_CHANGED"
            r1.<init>(r0)     // Catch:{ Exception -> 0x0016 }
            android.content.Intent r3 = r2.registerReceiver(r3, r1)     // Catch:{ Exception -> 0x0016 }
            goto L_0x001d
        L_0x0016:
            java.lang.String r2 = "DeviceBatteryMetricsCollector"
            java.lang.String r1 = "Exception registering receiver for ACTION_BATTERY_CHANGED"
            X.AnonymousClass0KZ.A00(r2, r1, r3)
        L_0x001d:
            if (r3 == 0) goto L_0x003e
            r2 = -1
            java.lang.String r0 = "level"
            int r1 = r3.getIntExtra(r0, r2)
            java.lang.String r0 = "scale"
            int r0 = r3.getIntExtra(r0, r2)
            if (r1 < 0) goto L_0x003e
            if (r0 <= 0) goto L_0x003e
            float r1 = (float) r1
            float r0 = (float) r0
            float r1 = r1 / r0
            r0 = 1120403456(0x42c80000, float:100.0)
            float r1 = r1 * r0
        L_0x0036:
            r7.batteryLevelPct = r1
            long r4 = android.os.SystemClock.elapsedRealtime()
            monitor-enter(r6)
            goto L_0x0041
        L_0x003e:
            r0 = -1
            float r1 = (float) r0
            goto L_0x0036
        L_0x0041:
            boolean r0 = r6.A03     // Catch:{ all -> 0x0062 }
            if (r0 == 0) goto L_0x0054
            long r2 = r6.A01     // Catch:{ all -> 0x0062 }
            long r0 = r6.A02     // Catch:{ all -> 0x0062 }
            long r4 = r4 - r0
            long r2 = r2 + r4
            r7.chargingRealtimeMs = r2     // Catch:{ all -> 0x0062 }
            long r0 = r6.A00     // Catch:{ all -> 0x0062 }
            r7.batteryRealtimeMs = r0     // Catch:{ all -> 0x0062 }
        L_0x0051:
            r0 = 1
            monitor-exit(r6)     // Catch:{ all -> 0x0062 }
            goto L_0x0061
        L_0x0054:
            long r0 = r6.A01     // Catch:{ all -> 0x0062 }
            r7.chargingRealtimeMs = r0     // Catch:{ all -> 0x0062 }
            long r2 = r6.A00     // Catch:{ all -> 0x0062 }
            long r0 = r6.A02     // Catch:{ all -> 0x0062 }
            long r4 = r4 - r0
            long r2 = r2 + r4
            r7.batteryRealtimeMs = r2     // Catch:{ all -> 0x0062 }
            goto L_0x0051
        L_0x0061:
            return r0
        L_0x0062:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0062 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Fa.A04(X.0FM):boolean");
    }

    public void A05(String str, long j) {
        AnonymousClass0KZ.A00("DeviceBatteryMetricsCollector", "Consecutive " + str + "broadcasts: (" + this.A02 + ", " + j + ")", null);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r3v1, types: [android.content.Intent] */
    /* JADX WARN: Type inference failed for: r3v3, types: [android.content.Intent] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    public AnonymousClass0Fa(android.content.Context r5) {
        /*
            r4 = this;
            r4.<init>()
            r4.A04 = r5
            r3 = 0
            android.content.IntentFilter r1 = new android.content.IntentFilter     // Catch:{ Exception -> 0x0012 }
            java.lang.String r0 = "android.intent.action.BATTERY_CHANGED"
            r1.<init>(r0)     // Catch:{ Exception -> 0x0012 }
            android.content.Intent r3 = r5.registerReceiver(r3, r1)     // Catch:{ Exception -> 0x0012 }
            goto L_0x0019
        L_0x0012:
            java.lang.String r1 = "DeviceBatteryMetricsCollector"
            java.lang.String r0 = "Exception registering receiver for ACTION_BATTERY_CHANGED"
            X.AnonymousClass0KZ.A00(r1, r0, r3)
        L_0x0019:
            r2 = -1
            if (r3 == 0) goto L_0x0022
            java.lang.String r0 = "status"
            int r2 = r3.getIntExtra(r0, r2)
        L_0x0022:
            r0 = 2
            if (r2 == r0) goto L_0x0029
            r1 = 5
            r0 = 0
            if (r2 != r1) goto L_0x002a
        L_0x0029:
            r0 = 1
        L_0x002a:
            r4.A03 = r0
            long r0 = android.os.SystemClock.elapsedRealtime()
            r4.A02 = r0
            android.content.IntentFilter r1 = new android.content.IntentFilter
            r1.<init>()
            java.lang.String r0 = "android.intent.action.ACTION_POWER_CONNECTED"
            r1.addAction(r0)
            java.lang.String r0 = "android.intent.action.ACTION_POWER_DISCONNECTED"
            r1.addAction(r0)
            X.0Fb r0 = new X.0Fb
            r0.<init>(r4)
            r5.registerReceiver(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Fa.<init>(android.content.Context):void");
    }
}
