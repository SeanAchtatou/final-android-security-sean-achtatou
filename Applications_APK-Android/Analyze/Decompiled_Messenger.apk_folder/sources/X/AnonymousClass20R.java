package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CancellationException;

/* renamed from: X.20R  reason: invalid class name */
public final class AnonymousClass20R implements CallerContextable {
    private static final CallerContext A01 = CallerContext.A04(AnonymousClass20R.class);
    public static final String __redex_internal_original_name = "com.facebook.messaging.contacts.loader.fetch.ContactPickerUserFetcher";
    private AnonymousClass0UN A00;

    public static final AnonymousClass20R A00(AnonymousClass1XY r1) {
        return new AnonymousClass20R(r1);
    }

    public ImmutableList A02(List list) {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, this.A00)).AOx();
        if (C013509w.A02(list)) {
            return RegularImmutableList.A02;
        }
        try {
            Collection<AnonymousClass7S5> A002 = ((GraphQLResult) ((C11170mD) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASp, this.A00)).A04(((C157437Pu) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Am3, this.A00)).A01(list)).get()).A00();
            if (list.size() != A002.size()) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A00)).CGY("ContactPickerUserFetcher_fetchUserFailure", "The GraphQL fetch returned a different number of users.");
            }
            ImmutableList.Builder builder = new ImmutableList.Builder();
            for (AnonymousClass7S5 A08 : A002) {
                User A082 = ((AnonymousClass7PT) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BSJ, this.A00)).A08(A08);
                if (A082 == null) {
                    ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A00)).CGY("ContactPickerUserFetcher_fetchUserFailure", "Failed to convert user.");
                }
                builder.add((Object) A082);
            }
            return builder.build();
        } catch (CancellationException unused) {
            return RegularImmutableList.A02;
        }
    }

    public AnonymousClass20R(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }

    public User A01(String str) {
        ImmutableList A02 = A02(Collections.singletonList(str));
        if (A02.isEmpty()) {
            return null;
        }
        return (User) A02.get(0);
    }
}
