package X;

/* renamed from: X.0wD  reason: invalid class name and case insensitive filesystem */
public final class C15930wD {
    public int A00 = 0;
    public int A01;
    public int A02;
    public int A03 = 0;
    public int A04 = 1;
    public int A05 = 0;
    public int A06 = -1;
    public long A07;
    public boolean A08 = false;
    public boolean A09 = false;
    public boolean A0A = false;
    public boolean A0B = false;
    public boolean A0C = false;
    public boolean A0D = false;

    public int A00() {
        if (this.A08) {
            return this.A05 - this.A00;
        }
        return this.A03;
    }

    public void A01(int i) {
        int i2 = this.A04;
        if ((i2 & i) == 0) {
            throw new IllegalStateException(AnonymousClass08S.A0S("Layout state should be one of ", Integer.toBinaryString(i), " but it is ", Integer.toBinaryString(i2)));
        }
    }

    public String toString() {
        return "State{mTargetPosition=" + this.A06 + ", mData=" + ((Object) null) + ", mItemCount=" + this.A03 + ", mIsMeasuring=" + this.A09 + ", mPreviousLayoutItemCount=" + this.A05 + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.A00 + ", mStructureChanged=" + this.A0C + ", mInPreLayout=" + this.A08 + ", mRunSimpleAnimations=" + this.A0B + ", mRunPredictiveAnimations=" + this.A0A + '}';
    }
}
