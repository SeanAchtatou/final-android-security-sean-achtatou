package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1A7  reason: invalid class name */
public final class AnonymousClass1A7 {
    public int A00;
    public int A01;
    public boolean A02;
    private int A03;
    private int A04;
    private int A05;
    public final AnonymousClass1A8 A06 = new AnonymousClass1A8(this);
    public final List A07;
    private final AnonymousClass19N A08;

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005b, code lost:
        r1 = r2.size();
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0060, code lost:
        if (r0 >= r1) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0062, code lost:
        ((X.C198619y) r2.get(r0)).CLg(r4, r5, r6, r7, r8);
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(int r10) {
        /*
            r9 = this;
            X.19N r0 = r9.A08
            int r4 = r0.AZq()
            X.19N r0 = r9.A08
            int r5 = r0.AZs()
            X.19N r0 = r9.A08
            int r6 = r0.AZo()
            X.19N r0 = r9.A08
            int r7 = r0.AZr()
            X.19N r0 = r9.A08
            int r1 = r0.ArU()
            if (r4 < 0) goto L_0x003a
            if (r5 < 0) goto L_0x003a
            int r0 = r9.A00
            r8 = r10
            if (r4 != r0) goto L_0x003b
            int r0 = r9.A01
            if (r5 != r0) goto L_0x003b
            int r0 = r9.A03
            if (r6 != r0) goto L_0x003b
            int r0 = r9.A04
            if (r7 != r0) goto L_0x003b
            int r0 = r9.A05
            if (r1 != r0) goto L_0x003b
            r0 = 1
            if (r10 == r0) goto L_0x003b
        L_0x003a:
            return
        L_0x003b:
            r9.A00 = r4
            r9.A01 = r5
            r9.A03 = r6
            r9.A04 = r7
            r9.A05 = r1
            r0 = 0
            r9.A02 = r0
            monitor-enter(r9)
            java.util.List r0 = r9.A07     // Catch:{ all -> 0x006f }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x0053
            monitor-exit(r9)     // Catch:{ all -> 0x006f }
            goto L_0x006e
        L_0x0053:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x006f }
            java.util.List r0 = r9.A07     // Catch:{ all -> 0x006f }
            r2.<init>(r0)     // Catch:{ all -> 0x006f }
            monitor-exit(r9)     // Catch:{ all -> 0x006f }
            int r1 = r2.size()
            r0 = 0
        L_0x0060:
            if (r0 >= r1) goto L_0x003a
            java.lang.Object r3 = r2.get(r0)
            X.19y r3 = (X.C198619y) r3
            r3.CLg(r4, r5, r6, r7, r8)
            int r0 = r0 + 1
            goto L_0x0060
        L_0x006e:
            return
        L_0x006f:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x006f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1A7.A00(int):void");
    }

    public void A01(C198619y r2) {
        if (r2 != null) {
            synchronized (this) {
                this.A07.add(r2);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0005, code lost:
        if (r2 != false) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(boolean r2) {
        /*
            r1 = this;
            boolean r0 = r1.A02
            if (r0 != 0) goto L_0x0007
            r0 = 0
            if (r2 == 0) goto L_0x0008
        L_0x0007:
            r0 = 1
        L_0x0008:
            r1.A02 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1A7.A02(boolean):void");
    }

    public boolean A03() {
        if (this.A00 < 0 || this.A01 < 0 || this.A02) {
            return true;
        }
        return false;
    }

    public AnonymousClass1A7(int i, int i2, AnonymousClass19N r5) {
        this.A00 = i;
        this.A01 = i2;
        this.A03 = r5.AZo();
        this.A04 = r5.AZr();
        this.A05 = r5.ArU();
        this.A08 = r5;
        this.A07 = new ArrayList(2);
    }

    public boolean A04(int i, int i2) {
        if (!A03()) {
            int i3 = i;
            while (i3 < i + i2) {
                if (this.A00 > i3 || i3 > this.A01) {
                    i3++;
                }
            }
            return false;
        }
        return true;
    }

    public boolean A05(int i, int i2) {
        if (A03() || i2 == -1 || i <= Math.max((this.A00 + i2) - 1, this.A01)) {
            return true;
        }
        return false;
    }
}
