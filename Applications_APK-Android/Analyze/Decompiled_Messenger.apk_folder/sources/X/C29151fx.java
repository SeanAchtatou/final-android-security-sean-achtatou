package X;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Type;

/* renamed from: X.1fx  reason: invalid class name and case insensitive filesystem */
public final class C29151fx extends C184112y {
    private static final long serialVersionUID = 1;
    public final Constructor _constructor;
    public C195669Ht _serialization;

    public final Object call1(Object obj) {
        return this._constructor.newInstance(obj);
    }

    public /* bridge */ /* synthetic */ AnnotatedElement getAnnotated() {
        return this._constructor;
    }

    public Class getDeclaringClass() {
        return this._constructor.getDeclaringClass();
    }

    public Type getGenericParameterType(int i) {
        Type[] genericParameterTypes = this._constructor.getGenericParameterTypes();
        if (i >= genericParameterTypes.length) {
            return null;
        }
        return genericParameterTypes[i];
    }

    public Member getMember() {
        return this._constructor;
    }

    public String getName() {
        return this._constructor.getName();
    }

    public Class getRawType() {
        return this._constructor.getDeclaringClass();
    }

    public C10030jR getType(C28311eb r2) {
        return getType(r2, this._constructor.getTypeParameters());
    }

    public Object getValue(Object obj) {
        throw new UnsupportedOperationException(AnonymousClass08S.A0J("Cannot call getValue() on constructor of ", getDeclaringClass().getName()));
    }

    public Object readResolve() {
        C195669Ht r0 = this._serialization;
        Class cls = r0.clazz;
        try {
            Constructor declaredConstructor = cls.getDeclaredConstructor(r0.args);
            if (!declaredConstructor.isAccessible()) {
                C29081fq.checkAndFixAccess(declaredConstructor);
            }
            return new C29151fx(declaredConstructor, null, null);
        } catch (Exception unused) {
            throw new IllegalArgumentException(AnonymousClass08S.A0F("Could not find constructor with ", this._serialization.args.length, " args from Class '", cls.getName()));
        }
    }

    public void setValue(Object obj, Object obj2) {
        throw new UnsupportedOperationException(AnonymousClass08S.A0J("Cannot call setValue() on constructor of ", getDeclaringClass().getName()));
    }

    public String toString() {
        return "[constructor for " + getName() + ", annotations: " + this._annotations + "]";
    }

    public Object writeReplace() {
        return new C29151fx(new C195669Ht(this._constructor));
    }

    public Type getGenericType() {
        return getRawType();
    }

    private C29151fx(C195669Ht r2) {
        super(null, null);
        this._constructor = null;
        this._serialization = r2;
    }

    public C29151fx(Constructor constructor, C10090jX r4, C10090jX[] r5) {
        super(r4, r5);
        if (constructor != null) {
            this._constructor = constructor;
            return;
        }
        throw new IllegalArgumentException("Null constructor not allowed");
    }

    public final Object call() {
        return this._constructor.newInstance(new Object[0]);
    }

    public final Object call(Object[] objArr) {
        return this._constructor.newInstance(objArr);
    }
}
