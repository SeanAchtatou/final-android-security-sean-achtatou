package X;

import com.google.common.collect.ImmutableSet;
import java.util.Iterator;

/* renamed from: X.0dQ  reason: invalid class name and case insensitive filesystem */
public class C07410dQ extends C24981Xw {
    /* renamed from: A04 */
    public ImmutableSet build() {
        if (this instanceof C06770c3) {
            return ((C06770c3) this).build();
        }
        ImmutableSet A02 = ImmutableSet.A02(this.A00, this.A02);
        this.A00 = A02.size();
        this.A01 = true;
        return A02;
    }

    public C07410dQ A00(Iterable iterable) {
        super.addAll(iterable);
        return this;
    }

    public C07410dQ A01(Object obj) {
        super.add(obj);
        return this;
    }

    public C07410dQ A02(Iterator it) {
        super.addAll(it);
        return this;
    }

    public C07410dQ A03(Object... objArr) {
        super.add(objArr);
        return this;
    }

    public C07410dQ() {
        this(4);
    }

    private C07410dQ(int i) {
        super(i);
    }

    public /* bridge */ /* synthetic */ C24991Xx add(Object[] objArr) {
        A03(objArr);
        return this;
    }

    public /* bridge */ /* synthetic */ C24991Xx addAll(Iterable iterable) {
        A00(iterable);
        return this;
    }

    public /* bridge */ /* synthetic */ C24991Xx addAll(Iterator it) {
        A02(it);
        return this;
    }
}
