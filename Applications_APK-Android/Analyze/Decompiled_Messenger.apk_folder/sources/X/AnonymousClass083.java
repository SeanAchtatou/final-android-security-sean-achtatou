package X;

/* renamed from: X.083  reason: invalid class name */
public final class AnonymousClass083 implements C004405h {
    private int A00;
    private long A01;
    private long A02;
    private long A03;
    private boolean A04;

    public AnonymousClass083(long j, long j2, long j3, int i, boolean z) {
        this.A02 = j;
        this.A01 = j2;
        this.A03 = j3;
        this.A00 = i;
        this.A04 = z;
    }

    public int AfE() {
        return this.A00;
    }

    public long B7l() {
        return this.A01;
    }

    public long B7m() {
        return this.A02;
    }

    public long B7n() {
        return this.A03;
    }

    public boolean BFq() {
        return this.A04;
    }
}
