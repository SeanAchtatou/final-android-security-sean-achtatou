package X;

import android.content.res.AssetManager;
import android.content.res.Resources;
import com.facebook.mobileconfig.MobileConfigCxxChangeListener;
import com.facebook.mobileconfig.MobileConfigEmergencyPushChangeListener;
import com.facebook.mobileconfig.init.MobileConfigApi2LoggerImpl;
import io.card.payment.BuildConfig;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.RandomAccess;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReferenceArray;

/* renamed from: X.1Yc  reason: invalid class name and case insensitive filesystem */
public final class C25041Yc implements C25051Yd, MobileConfigCxxChangeListener, MobileConfigEmergencyPushChangeListener {
    public AssetManager A00;
    public C25071Yf A01;
    public C04780Wd A02;
    public File A03;
    public C04310Tq A04;
    public C04310Tq A05;
    public boolean A06 = false;
    public boolean A07;
    private C25851aV A08;
    public final AnonymousClass0WV A09;
    public final AtomicBoolean A0A = new AtomicBoolean(false);
    public final AtomicBoolean A0B = new AtomicBoolean(false);
    private final C04760Wb A0C;
    private final Object A0D = new Object();
    private final Random A0E = new Random();
    private final Set A0F = new HashSet();
    private final AtomicBoolean A0G = new AtomicBoolean(false);
    private final AtomicBoolean A0H = new AtomicBoolean(false);
    private final boolean A0I;
    public volatile C04770Wc A0J;
    public volatile C25061Ye A0K;
    public volatile AtomicReferenceArray A0L;
    public volatile C04310Tq A0M;
    public volatile C04310Tq A0N;

    public synchronized void A0A() {
        this.A08 = this.A09.getNewOverridesTableIfExists();
        for (C04770Wc r1 : this.A0F) {
            if (r1 != null && (r1 instanceof AnonymousClass1ZM)) {
                AnonymousClass1ZM r12 = (AnonymousClass1ZM) r1;
                C25851aV r0 = this.A08;
                synchronized (r12) {
                    r12.A00 = r0;
                }
            }
        }
        if (this.A0J != null && (this.A0J instanceof AnonymousClass1ZM)) {
            AnonymousClass1ZM r13 = (AnonymousClass1ZM) this.A0J;
            C25851aV r02 = this.A08;
            synchronized (r13) {
                r13.A00 = r02;
            }
        }
    }

    public boolean Aeo(long j, boolean z) {
        A02(j, 1);
        return A00(j).Aeo(j, z);
    }

    public double Akj(long j, double d) {
        A02(j, 4);
        return A00(j).Akj(j, d);
    }

    public Map Al7() {
        return A07(-1).Al7();
    }

    public int AqL(long j, int i) {
        A02(j, 2);
        return A00(j).AqL(j, i);
    }

    public int AqO(long j, int i) {
        A02(j, 2);
        return A00(j).AqO(j, i);
    }

    public long At1(long j, long j2) {
        A02(j, 2);
        return A00(j).At1(j, j2);
    }

    public String B4D(long j, int i, Resources resources) {
        A02(j, 3);
        return A00(j).B4D(j, i, resources);
    }

    public String B4E(long j, String str) {
        A02(j, 3);
        return A00(j).B4E(j, str);
    }

    public void BJC(long j) {
        A02(j, 0);
        A00(j).BJC(j);
    }

    private C04770Wc A00(long j) {
        return A06((int) ((j >>> 32) & 65535));
    }

    public static String A01(C25041Yc r1) {
        String str;
        C04780Wd r0 = r1.A02;
        if (r0 == null || (str = (String) r0.A00.get()) == null) {
            return BuildConfig.FLAVOR;
        }
        return str;
    }

    private void A02(long j, int i) {
        String str;
        boolean z = this.A07;
        if (z != AnonymousClass0XG.A02(j)) {
            if (z) {
                str = "Sessionless factory used for session-based param";
            } else {
                str = "Session-based factory used for sessionless param";
            }
            if (this.A0N != null) {
                ((AnonymousClass09P) this.A0N.get()).CGY("MobileConfigFactoryImpl: invalid specifier", str);
            }
            C010708t.A0J("MobileConfigFactoryImpl", str);
        }
        if (i != 0 && i != AnonymousClass0XG.A00(j)) {
            String A0B2 = AnonymousClass08S.A0B("Invalid param type used for config: ", (int) ((j >>> 32) & 65535), ", param: ", (int) ((j >>> 16) & 65535));
            if (this.A0N != null) {
                ((AnonymousClass09P) this.A0N.get()).CGS("MobileConfigFactoryImpl: invalid specifier", A0B2);
            }
            C010708t.A0J("MobileConfigFactoryImpl", A0B2);
        }
    }

    private void A03(long j, String str) {
        boolean z = false;
        if (((j >>> 60) & 1) == 1) {
            z = true;
        }
        if (z) {
            if (this.A0N != null) {
                ((AnonymousClass09P) this.A0N.get()).CGS("MobileConfigFactoryImpl: invalid specifier", AnonymousClass08S.A0S("QE param does not have a universe default. Please call `", str, "(specifier, defaultValue)` instead and provide a call-site default value. ", "https://fburl.com/callsitedefault"));
            }
            C010708t.A0J("MobileConfigFactoryImpl", "QE param does not have a universe default. Please call `" + str + "(specifier, defaultValue)` instead and provide a call-site default value. " + "https://fburl.com/callsitedefault");
        }
    }

    private boolean A05() {
        if (this.A07 || !BuildConfig.FLAVOR.equals(A01(this))) {
            return false;
        }
        return true;
    }

    public C04770Wc A06(int i) {
        Object[] objArr;
        String str;
        String A012;
        AtomicReferenceArray atomicReferenceArray = this.A0L;
        if (i < 0 || i >= atomicReferenceArray.length()) {
            objArr = new Object[]{Integer.valueOf(i), Boolean.valueOf(this.A07)};
            str = "Attempt to read invalid config index(%d) from config caches, isSessionless: %s";
        } else {
            C04770Wc r4 = (C04770Wc) atomicReferenceArray.get(i);
            boolean z = true;
            if (r4 == null) {
                z = false;
            }
            if (!z) {
                if (!this.A0A.get() || !A05()) {
                    C04770Wc A072 = A07(i);
                    if (!atomicReferenceArray.compareAndSet(i, r4, A072)) {
                        r4 = (C04770Wc) atomicReferenceArray.get(i);
                    } else {
                        r4 = A072;
                    }
                    AnonymousClass1ZM r3 = (AnonymousClass1ZM) r4;
                    C04310Tq r2 = this.A0M;
                    if (r2 != null && this.A06 && (this.A07 || !((A012 = A01(this)) == null || A012 == BuildConfig.FLAVOR))) {
                        ((MobileConfigApi2LoggerImpl) r2.get()).A04(i, r3);
                    }
                } else {
                    objArr = new Object[]{Integer.valueOf(i)};
                    str = "Attempt to read config (index:%d) after logout, see https://fburl.com/bicj8iz0";
                }
            }
            return r4;
        }
        C010708t.A0P("MobileConfigFactoryImpl", str, objArr);
        return AnonymousClass37T.A00();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a2, code lost:
        r6.A09();
        r6 = r6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C04770Wc A07(int r13) {
        /*
            r12 = this;
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.A0A
            boolean r0 = r0.get()
            r5 = 0
            if (r0 == 0) goto L_0x0023
            boolean r0 = r12.A05()
            if (r0 == 0) goto L_0x0023
            java.lang.Integer r0 = java.lang.Integer.valueOf(r13)
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            java.lang.String r1 = "MobileConfigFactoryImpl"
            java.lang.String r0 = "Attempt to read config (index:%d) after logout, see https://fburl.com/bicj8iz0"
            X.C010708t.A0P(r1, r0, r2)
            X.37T r0 = X.AnonymousClass37T.A00()
            return r0
        L_0x0023:
            r12.A09()
            X.0Wc r6 = r12.A0J
            if (r6 != 0) goto L_0x00a5
            monitor-enter(r12)
            X.0Wc r6 = r12.A0J     // Catch:{ all -> 0x009f }
            if (r6 == 0) goto L_0x0031
            monitor-exit(r12)     // Catch:{ all -> 0x009f }
            return r6
        L_0x0031:
            X.0WV r0 = r12.A09     // Catch:{ all -> 0x009f }
            r11 = 0
            if (r0 == 0) goto L_0x003b
            X.0b2 r4 = r0.getLatestHandle()     // Catch:{ all -> 0x009f }
            goto L_0x003c
        L_0x003b:
            r4 = r11
        L_0x003c:
            if (r4 == 0) goto L_0x0043
            java.nio.ByteBuffer r7 = r4.getJavaByteBuffer()     // Catch:{ all -> 0x009f }
            goto L_0x0044
        L_0x0043:
            r7 = r11
        L_0x0044:
            if (r7 != 0) goto L_0x005e
            java.lang.String r3 = "MobileConfigFactoryImpl"
            java.lang.String r2 = "Unable to enable contextV2 due to null buffer - sessionless: %s, handleHolder is null: %b"
            boolean r0 = r12.A07     // Catch:{ all -> 0x009f }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x009f }
            if (r4 != 0) goto L_0x0053
            r5 = 1
        L_0x0053:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)     // Catch:{ all -> 0x009f }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0}     // Catch:{ all -> 0x009f }
            X.C010708t.A0O(r3, r2, r0)     // Catch:{ all -> 0x009f }
        L_0x005e:
            long[][] r11 = (long[][]) r11     // Catch:{ all -> 0x009f }
            X.0WV r1 = r12.A09     // Catch:{ all -> 0x009f }
            boolean r0 = r1 instanceof X.C25031Yb     // Catch:{ all -> 0x009f }
            if (r0 == 0) goto L_0x007c
            X.1Yb r1 = (X.C25031Yb) r1     // Catch:{ all -> 0x009f }
            X.0WV r0 = r1.A00()     // Catch:{ all -> 0x009f }
            boolean r0 = r0 instanceof X.AnonymousClass1Z7     // Catch:{ all -> 0x009f }
            if (r0 == 0) goto L_0x007c
            X.0WV r0 = r12.A09     // Catch:{ all -> 0x009f }
            X.1Yb r0 = (X.C25031Yb) r0     // Catch:{ all -> 0x009f }
            X.0WV r0 = r0.A00()     // Catch:{ all -> 0x009f }
            X.1Z7 r0 = (X.AnonymousClass1Z7) r0     // Catch:{ all -> 0x009f }
            long[][] r11 = r0.A01     // Catch:{ all -> 0x009f }
        L_0x007c:
            if (r11 == 0) goto L_0x007f
            goto L_0x008b
        L_0x007f:
            X.1ZL r6 = new X.1ZL     // Catch:{ all -> 0x009f }
            X.0WV r2 = r12.A09     // Catch:{ all -> 0x009f }
            X.1aV r1 = r12.A08     // Catch:{ all -> 0x009f }
            X.0Tq r0 = r12.A0N     // Catch:{ all -> 0x009f }
            r6.<init>(r7, r2, r1, r0)     // Catch:{ all -> 0x009f }
            goto L_0x0096
        L_0x008b:
            X.95V r6 = new X.95V     // Catch:{ all -> 0x009f }
            X.0WV r8 = r12.A09     // Catch:{ all -> 0x009f }
            X.1aV r9 = r12.A08     // Catch:{ all -> 0x009f }
            X.0Tq r10 = r12.A0N     // Catch:{ all -> 0x009f }
            r6.<init>(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x009f }
        L_0x0096:
            r12.A0J = r6     // Catch:{ all -> 0x009f }
            java.util.Set r0 = r12.A0F     // Catch:{ all -> 0x009f }
            r0.add(r6)     // Catch:{ all -> 0x009f }
            monitor-exit(r12)     // Catch:{ all -> 0x009f }
            goto L_0x00a2
        L_0x009f:
            r0 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x009f }
            throw r0
        L_0x00a2:
            r6.A09()
        L_0x00a5:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25041Yc.A07(int):X.0Wc");
    }

    public void A08() {
        if (!this.A0G.get()) {
            String A012 = A01(this);
            synchronized (this.A0D) {
                if (this.A0G.compareAndSet(false, true)) {
                    if (this.A0K == null) {
                        C010708t.A0P("MobileConfigFactoryImpl", "Calling initCppManagerIfNeeded with null mCppManagerCreator, userId:%s", A012);
                    } else if (this.A09 instanceof C25031Yb) {
                        if (!this.A07) {
                            if (!A012.isEmpty()) {
                                if (A012.equals("0")) {
                                }
                            }
                        }
                        AnonymousClass0WV AUk = this.A0K.AUk(this.A03, A012);
                        if (AUk != null) {
                            int[] A0B2 = A0B();
                            synchronized (this) {
                                this.A0B.set(true);
                                ((C25031Yb) this.A09).A01(AUk, this);
                                this.A09.setEpHandler(this);
                                A04(this, false);
                            }
                            for (int A062 : A0B2) {
                                A06(A062);
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01b7, code lost:
        if (r14 != null) goto L_0x01b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01ce, code lost:
        if (r1 != false) goto L_0x01d0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x01b2 A[Catch:{ IOException -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x01cd A[Catch:{ IOException -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x01d4 A[SYNTHETIC, Splitter:B:121:0x01d4] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x01ad A[SYNTHETIC, Splitter:B:98:0x01ad] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09() {
        /*
            r27 = this;
            r2 = r27
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.A0H
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0221
            java.lang.String r11 = A01(r2)
            monitor-enter(r2)
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.A0H     // Catch:{ all -> 0x021e }
            r6 = 0
            r4 = 1
            boolean r0 = r0.compareAndSet(r6, r4)     // Catch:{ all -> 0x021e }
            if (r0 == 0) goto L_0x0048
            java.io.File r3 = r2.A03     // Catch:{ all -> 0x021e }
            if (r3 == 0) goto L_0x0048
            X.0WV r1 = r2.A09     // Catch:{ all -> 0x021e }
            boolean r0 = r1 instanceof X.C25031Yb     // Catch:{ all -> 0x021e }
            if (r0 == 0) goto L_0x0048
            boolean r0 = r2.A07     // Catch:{ all -> 0x021e }
            if (r0 != 0) goto L_0x004b
            boolean r0 = r11.isEmpty()     // Catch:{ all -> 0x021e }
            if (r0 != 0) goto L_0x0035
            java.lang.String r0 = "0"
            boolean r0 = r11.equals(r0)     // Catch:{ all -> 0x021e }
            if (r0 == 0) goto L_0x004b
        L_0x0035:
            X.1Yb r1 = (X.C25031Yb) r1     // Catch:{ all -> 0x021e }
            X.0WV r3 = r1.A00()     // Catch:{ all -> 0x021e }
            boolean r0 = r3 instanceof X.AnonymousClass0WX     // Catch:{ all -> 0x021e }
            if (r0 == 0) goto L_0x0048
            X.0WX r3 = (X.AnonymousClass0WX) r3     // Catch:{ all -> 0x021e }
            java.lang.String r1 = "Logout"
            java.util.concurrent.atomic.AtomicReference r0 = r3.A00     // Catch:{ all -> 0x021e }
            r0.set(r1)     // Catch:{ all -> 0x021e }
        L_0x0048:
            monitor-exit(r2)     // Catch:{ all -> 0x021e }
            goto L_0x021b
        L_0x004b:
            X.1Z6 r5 = new X.1Z6     // Catch:{ all -> 0x021e }
            r5.<init>()     // Catch:{ all -> 0x021e }
            android.content.res.AssetManager r9 = r2.A00     // Catch:{ all -> 0x021e }
            boolean r8 = r2.A0I     // Catch:{ all -> 0x021e }
            X.0Tq r0 = r2.A05     // Catch:{ all -> 0x021c }
            X.1Z7 r7 = new X.1Z7     // Catch:{ all -> 0x021c }
            r7.<init>(r3, r11)     // Catch:{ all -> 0x021c }
            r7.A00 = r0     // Catch:{ all -> 0x021c }
            X.0b2 r0 = r7.getLatestHandle()     // Catch:{ all -> 0x021c }
            java.lang.String r10 = X.AnonymousClass1Z7.A00(r5, r0)     // Catch:{ all -> 0x021c }
            if (r10 == 0) goto L_0x0088
            boolean r0 = r10.isEmpty()     // Catch:{ all -> 0x021c }
            if (r0 != 0) goto L_0x0088
            r0 = 58
            int r1 = r10.indexOf(r0)     // Catch:{ all -> 0x021c }
            r0 = -1
            if (r1 == r0) goto L_0x007a
            java.lang.String r10 = r10.substring(r6, r1)     // Catch:{ all -> 0x021c }
        L_0x007a:
            java.lang.String r0 = "57fc7f53b1ffe1c163b77e35360bc1d3"
            boolean r1 = r10.equals(r0)     // Catch:{ all -> 0x021c }
            r1 = r1 ^ r4
            if (r1 == 0) goto L_0x0089
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y     // Catch:{ all -> 0x021c }
            r5.A00 = r0     // Catch:{ all -> 0x021c }
            goto L_0x0089
        L_0x0088:
            r1 = 1
        L_0x0089:
            r26 = 0
            if (r1 == 0) goto L_0x01ce
            if (r9 == 0) goto L_0x01ce
            if (r8 == 0) goto L_0x01ce
            X.0b2 r0 = r7.getLatestHandle()     // Catch:{ all -> 0x021c }
            java.lang.String r10 = X.AnonymousClass1Z7.A00(r5, r0)     // Catch:{ all -> 0x021c }
            java.lang.String r25 = "MobileConfigJavaManager"
            if (r10 == 0) goto L_0x01ca
            r13 = 0
            java.io.RandomAccessFile r8 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x01b6, all -> 0x01a9 }
            java.lang.String r3 = X.C36811tq.A00(r3, r11)     // Catch:{ IOException -> 0x01b6, all -> 0x01a9 }
            java.lang.String r1 = "/"
            java.lang.String r0 = "spec_to_param.txt"
            java.lang.String r1 = X.AnonymousClass08S.A0P(r3, r1, r0)     // Catch:{ IOException -> 0x01b6, all -> 0x01a9 }
            java.lang.String r0 = "r"
            r8.<init>(r1, r0)     // Catch:{ IOException -> 0x01b6, all -> 0x01a9 }
            java.nio.channels.FileChannel r14 = r8.getChannel()     // Catch:{ IOException -> 0x01b6, all -> 0x01a9 }
            java.lang.String r0 = X.AnonymousClass1Z7.A01(r14)     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            boolean r0 = X.AnonymousClass1Z7.A02(r0, r10)     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            if (r0 != 0) goto L_0x00c6
            if (r14 == 0) goto L_0x01ca
            r14.close()     // Catch:{ IOException -> 0x01c2 }
            goto L_0x01ca
        L_0x00c6:
            java.lang.String r0 = "spec_to_param.txt"
            java.io.InputStream r0 = r9.open(r0)     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            java.nio.channels.ReadableByteChannel r13 = java.nio.channels.Channels.newChannel(r0)     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            java.lang.String r1 = X.AnonymousClass1Z7.A01(r13)     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            java.lang.String r0 = "57fc7f53b1ffe1c163b77e35360bc1d3"
            boolean r0 = X.AnonymousClass1Z7.A02(r1, r0)     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            if (r0 != 0) goto L_0x00e0
            if (r14 == 0) goto L_0x01bc
            goto L_0x01b9
        L_0x00e0:
            r24 = 4
            r0 = 7844(0x1ea4, float:1.0992E-41)
            long[] r8 = new long[r0]     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            r0 = 2988(0xbac, float:4.187E-42)
            long[] r3 = new long[r0]     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            r0 = 821(0x335, float:1.15E-42)
            long[] r1 = new long[r0]     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            r0 = 398(0x18e, float:5.58E-43)
            long[] r0 = new long[r0]     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            long[][] r8 = new long[][]{r8, r3, r1, r0}     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
            java.lang.String r9 = "MobileConfigJavaManager"
            r12 = 16
            java.nio.ByteBuffer r11 = java.nio.ByteBuffer.allocate(r12)     // Catch:{ IOException -> 0x018f }
            java.nio.ByteOrder r0 = java.nio.ByteOrder.BIG_ENDIAN     // Catch:{ IOException -> 0x018f }
            r11.order(r0)     // Catch:{ IOException -> 0x018f }
            java.nio.ByteBuffer r10 = java.nio.ByteBuffer.allocate(r12)     // Catch:{ IOException -> 0x018f }
            r10.order(r0)     // Catch:{ IOException -> 0x018f }
            int r1 = r14.read(r11)     // Catch:{ IOException -> 0x018f }
            int r3 = r13.read(r10)     // Catch:{ IOException -> 0x018f }
        L_0x0112:
            if (r3 != r12) goto L_0x0197
            if (r1 != r12) goto L_0x0197
            r11.flip()     // Catch:{ IOException -> 0x018f }
            r10.flip()     // Catch:{ IOException -> 0x018f }
            long r22 = r10.getLong()     // Catch:{ IOException -> 0x018f }
            long r20 = r10.getLong()     // Catch:{ IOException -> 0x018f }
            long r18 = r11.getLong()     // Catch:{ IOException -> 0x018f }
            long r16 = r11.getLong()     // Catch:{ IOException -> 0x018f }
            int r0 = (r18 > r22 ? 1 : (r18 == r22 ? 0 : -1))
            if (r0 != 0) goto L_0x0169
            int r1 = X.AnonymousClass0XG.A00(r20)     // Catch:{ IOException -> 0x018f }
            int r0 = X.AnonymousClass0XG.A00(r16)     // Catch:{ IOException -> 0x018f }
            if (r1 == r0) goto L_0x0140
            java.lang.String r0 = "populateTranslationTableInternal: invalid specifiers"
            X.C010708t.A0K(r9, r0)     // Catch:{ IOException -> 0x018f }
            goto L_0x0195
        L_0x0140:
            int r1 = X.AnonymousClass0XG.A00(r20)     // Catch:{ IOException -> 0x018f }
            int r1 = r1 - r4
            int r15 = X.AnonymousClass0XG.A01(r20)     // Catch:{ IOException -> 0x018f }
            if (r1 < 0) goto L_0x017d
            r0 = r24
            if (r1 >= r0) goto L_0x017d
            if (r15 < 0) goto L_0x017d
            r3 = r8[r1]     // Catch:{ IOException -> 0x018f }
            if (r3 == 0) goto L_0x017d
            int r0 = r3.length     // Catch:{ IOException -> 0x018f }
            if (r15 >= r0) goto L_0x017d
            r3[r15] = r16     // Catch:{ IOException -> 0x018f }
            r10.clear()     // Catch:{ IOException -> 0x018f }
            int r3 = r13.read(r10)     // Catch:{ IOException -> 0x018f }
            r11.clear()     // Catch:{ IOException -> 0x018f }
            int r1 = r14.read(r11)     // Catch:{ IOException -> 0x018f }
            goto L_0x0112
        L_0x0169:
            int r0 = (r18 > r22 ? 1 : (r18 == r22 ? 0 : -1))
            if (r0 <= 0) goto L_0x0175
            r10.clear()     // Catch:{ IOException -> 0x018f }
            int r3 = r13.read(r10)     // Catch:{ IOException -> 0x018f }
            goto L_0x0112
        L_0x0175:
            r11.clear()     // Catch:{ IOException -> 0x018f }
            int r1 = r14.read(r11)     // Catch:{ IOException -> 0x018f }
            goto L_0x0112
        L_0x017d:
            java.lang.String r3 = "populateTranslationTableInternal: Type/slot ids out of bound type_id: %d slot_id: %d"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ IOException -> 0x018f }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r15)     // Catch:{ IOException -> 0x018f }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0}     // Catch:{ IOException -> 0x018f }
            X.C010708t.A0Q(r9, r3, r0)     // Catch:{ IOException -> 0x018f }
            goto L_0x0195
        L_0x018f:
            r1 = move-exception
            java.lang.String r0 = "populateTranslationTableInternal: IOException"
            X.C010708t.A0T(r9, r1, r0)     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
        L_0x0195:
            r0 = 0
            goto L_0x0198
        L_0x0197:
            r0 = 1
        L_0x0198:
            if (r0 == 0) goto L_0x019c
            r7.A01 = r8     // Catch:{ IOException -> 0x01b7, all -> 0x01a7 }
        L_0x019c:
            if (r14 == 0) goto L_0x01a1
            r14.close()     // Catch:{ IOException -> 0x01c2 }
        L_0x01a1:
            if (r13 == 0) goto L_0x01cb
            r13.close()     // Catch:{ IOException -> 0x01c2 }
            goto L_0x01cb
        L_0x01a7:
            r0 = move-exception
            goto L_0x01ab
        L_0x01a9:
            r0 = move-exception
            r14 = r13
        L_0x01ab:
            if (r14 == 0) goto L_0x01b0
            r14.close()     // Catch:{ IOException -> 0x01c2 }
        L_0x01b0:
            if (r13 == 0) goto L_0x01b5
            r13.close()     // Catch:{ IOException -> 0x01c2 }
        L_0x01b5:
            throw r0     // Catch:{ IOException -> 0x01c2 }
        L_0x01b6:
            r14 = r13
        L_0x01b7:
            if (r14 == 0) goto L_0x01bc
        L_0x01b9:
            r14.close()     // Catch:{ IOException -> 0x01c2 }
        L_0x01bc:
            if (r13 == 0) goto L_0x01ca
            r13.close()     // Catch:{ IOException -> 0x01c2 }
            goto L_0x01ca
        L_0x01c2:
            r3 = move-exception
            java.lang.String r1 = "Failed to populate translation table due to exception"
            r0 = r25
            X.C010708t.A0L(r0, r1, r3)     // Catch:{ all -> 0x021c }
        L_0x01ca:
            r0 = 0
        L_0x01cb:
            if (r0 == 0) goto L_0x01d0
            goto L_0x01d2
        L_0x01ce:
            if (r1 == 0) goto L_0x01d2
        L_0x01d0:
            r7 = r26
        L_0x01d2:
            if (r7 == 0) goto L_0x01e5
            X.0WV r0 = r2.A09     // Catch:{ all -> 0x021e }
            X.1Yb r0 = (X.C25031Yb) r0     // Catch:{ all -> 0x021e }
            r0.A01(r7, r2)     // Catch:{ all -> 0x021e }
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.A0B     // Catch:{ all -> 0x021e }
            r0.set(r4)     // Catch:{ all -> 0x021e }
            A04(r2, r6)     // Catch:{ all -> 0x021e }
            goto L_0x0048
        L_0x01e5:
            X.0WV r0 = r2.A09     // Catch:{ all -> 0x021e }
            X.1Yb r0 = (X.C25031Yb) r0     // Catch:{ all -> 0x021e }
            X.0WV r3 = r0.A00()     // Catch:{ all -> 0x021e }
            boolean r0 = r3 instanceof X.AnonymousClass0WX     // Catch:{ all -> 0x021e }
            if (r0 == 0) goto L_0x0048
            X.0WX r3 = (X.AnonymousClass0WX) r3     // Catch:{ all -> 0x021e }
            java.lang.Integer r0 = r5.A00     // Catch:{ all -> 0x021e }
            if (r0 == 0) goto L_0x0048
            int[] r1 = X.AnonymousClass45J.A00     // Catch:{ all -> 0x021e }
            int r0 = r0.intValue()     // Catch:{ all -> 0x021e }
            r1 = r1[r0]     // Catch:{ all -> 0x021e }
            if (r1 == r4) goto L_0x0212
            r0 = 2
            if (r1 == r0) goto L_0x0209
            r0 = 3
            if (r1 == r0) goto L_0x0209
            goto L_0x0048
        L_0x0209:
            java.lang.String r1 = "AppUpgrade"
            java.util.concurrent.atomic.AtomicReference r0 = r3.A00     // Catch:{ all -> 0x021e }
            r0.set(r1)     // Catch:{ all -> 0x021e }
            goto L_0x0048
        L_0x0212:
            java.lang.String r1 = "FreshInstall"
            java.util.concurrent.atomic.AtomicReference r0 = r3.A00     // Catch:{ all -> 0x021e }
            r0.set(r1)     // Catch:{ all -> 0x021e }
            goto L_0x0048
        L_0x021b:
            return
        L_0x021c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x021e }
        L_0x021e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x021e }
            throw r0
        L_0x0221:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25041Yc.A09():void");
    }

    public int[] A0B() {
        int[] iArr;
        AtomicReferenceArray atomicReferenceArray = this.A0L;
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < atomicReferenceArray.length(); i++) {
            if (atomicReferenceArray.get(i) != null) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        if (arrayList instanceof RandomAccess) {
            int size = arrayList.size();
            iArr = new int[size];
            for (int i2 = 0; i2 < size; i2++) {
                iArr[i2] = ((Integer) arrayList.get(i2)).intValue();
            }
        } else {
            iArr = new int[arrayList.size()];
            int i3 = 0;
            for (Integer intValue : arrayList) {
                iArr[i3] = intValue.intValue();
                i3++;
            }
        }
        return iArr;
    }

    public boolean Aem(long j) {
        return Aer(j, AnonymousClass0XE.A06);
    }

    public boolean Aer(long j, AnonymousClass0XE r4) {
        A03(j, "getBooleanWithOptions");
        return Aes(j, AnonymousClass0XG.A03(j), r4);
    }

    public boolean Aes(long j, boolean z, AnonymousClass0XE r21) {
        C04770Wc A002;
        String str;
        AnonymousClass0XE r5 = r21;
        boolean z2 = z;
        long j2 = j;
        A02(j2, 1);
        boolean z3 = false;
        boolean z4 = false;
        if (this.A0E.nextInt(200000) == 0) {
            z4 = true;
        }
        if (z4) {
            r5 = r5.A03();
        }
        if (r5.A01) {
            A002 = A07((int) ((j >>> 32) & 65535));
        } else {
            A002 = A00(j2);
        }
        AnonymousClass0WV r1 = this.A09;
        if (!(r1 instanceof C25031Yb) || !(((C25031Yb) r1).A00() instanceof AnonymousClass0WX)) {
            z2 = A002.Aes(j2, z2, r5);
        } else if (r5.A02) {
            AnonymousClass0XE.A01(r5).A00 = AnonymousClass0XF.A02;
        }
        if (z4) {
            AnonymousClass0XF r12 = r5.A00;
            String syncFetchReason = this.A09.syncFetchReason();
            if (z2) {
                str = "1";
            } else {
                str = "0";
            }
            C25071Yf r8 = this.A01;
            if (r12.source > 1) {
                z3 = true;
            }
            r8.BfI(j2, 200000, z3, r12.name(), syncFetchReason, str, this.A0I);
        }
        return z2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001d, code lost:
        if (r2.A09.getLatestHandle().getJavaByteBuffer() == null) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer Aj4(boolean r3) {
        /*
            r2 = this;
            boolean r0 = r2.A07
            if (r0 == r3) goto L_0x0007
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            return r0
        L_0x0007:
            r2.A09()
            X.0WV r0 = r2.A09
            X.0b2 r0 = r0.getLatestHandle()
            if (r0 == 0) goto L_0x001f
            X.0WV r0 = r2.A09
            X.0b2 r0 = r0.getLatestHandle()
            java.nio.ByteBuffer r1 = r0.getJavaByteBuffer()
            r0 = 1
            if (r1 != 0) goto L_0x0020
        L_0x001f:
            r0 = 0
        L_0x0020:
            if (r0 == 0) goto L_0x0025
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            return r0
        L_0x0025:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25041Yc.Aj4(boolean):java.lang.Integer");
    }

    public double Aki(long j) {
        return Akm(j, AnonymousClass0XE.A06);
    }

    public double Akl(long j, double d, AnonymousClass0XE r24) {
        C04770Wc A002;
        AnonymousClass0XE r4 = r24;
        double d2 = d;
        long j2 = j;
        A02(j2, 4);
        boolean z = false;
        if (this.A0E.nextInt(200000) == 0) {
            z = true;
        }
        if (z) {
            r4 = r4.A03();
        }
        if (r4.A01) {
            A002 = A07((int) ((j >>> 32) & 65535));
        } else {
            A002 = A00(j2);
        }
        AnonymousClass0WV r1 = this.A09;
        if (!(r1 instanceof C25031Yb) || !(((C25031Yb) r1).A00() instanceof AnonymousClass0WX)) {
            d2 = A002.Akl(j2, d2, r4);
        } else if (r4.A02) {
            AnonymousClass0XE.A01(r4).A00 = AnonymousClass0XF.A02;
        }
        if (z) {
            AnonymousClass0XF r12 = r4.A00;
            String syncFetchReason = this.A09.syncFetchReason();
            C25071Yf r7 = this.A01;
            boolean z2 = false;
            if (r12.source > 1) {
                z2 = true;
            }
            r7.BfI(j2, 200000, z2, r12.name(), syncFetchReason, Double.toString(d2), this.A0I);
        }
        return d2;
    }

    public double Akm(long j, AnonymousClass0XE r9) {
        A03(j, "getDoubleWithOptions");
        return Akl(j, C05480Zc.A00(j), r9);
    }

    public long At0(long j) {
        return At4(j, AnonymousClass0XE.A06);
    }

    public long At3(long j, long j2, AnonymousClass0XE r24) {
        C04770Wc A002;
        AnonymousClass0XE r4 = r24;
        long j3 = j2;
        long j4 = j;
        A02(j4, 2);
        boolean z = false;
        if (this.A0E.nextInt(200000) == 0) {
            z = true;
        }
        if (z) {
            r4 = r4.A03();
        }
        if (r4.A01) {
            A002 = A07((int) ((j >>> 32) & 65535));
        } else {
            A002 = A00(j4);
        }
        AnonymousClass0WV r1 = this.A09;
        if (!(r1 instanceof C25031Yb) || !(((C25031Yb) r1).A00() instanceof AnonymousClass0WX)) {
            j3 = A002.At3(j4, j3, r4);
        } else if (r4.A02) {
            AnonymousClass0XE.A01(r4).A00 = AnonymousClass0XF.A02;
        }
        if (z) {
            AnonymousClass0XF r12 = r4.A00;
            String syncFetchReason = this.A09.syncFetchReason();
            C25071Yf r7 = this.A01;
            boolean z2 = false;
            if (r12.source > 1) {
                z2 = true;
            }
            r7.BfI(j4, 200000, z2, r12.name(), syncFetchReason, Long.toString(j3), this.A0I);
        }
        return j3;
    }

    public long At4(long j, AnonymousClass0XE r9) {
        A03(j, "getLongWithOptions");
        return At3(j, C05480Zc.A01(j), r9);
    }

    public String B4C(long j) {
        return B4J(j, AnonymousClass0XE.A06);
    }

    public String B4J(long j, AnonymousClass0XE r4) {
        A03(j, "getStringWithOptions");
        return B4K(j, C05480Zc.A02(j), r4);
    }

    public String B4K(long j, String str, AnonymousClass0XE r20) {
        C04770Wc A002;
        String A0P;
        AnonymousClass0XE r2 = r20;
        String str2 = str;
        long j2 = j;
        A02(j2, 3);
        boolean z = true;
        boolean z2 = false;
        if (this.A0E.nextInt(200000) == 0) {
            z2 = true;
        }
        if (z2) {
            r2 = r2.A03();
        }
        if (r2.A01) {
            A002 = A07((int) ((j >>> 32) & 65535));
        } else {
            A002 = A00(j2);
        }
        AnonymousClass0WV r1 = this.A09;
        if (!(r1 instanceof C25031Yb) || !(((C25031Yb) r1).A00() instanceof AnonymousClass0WX)) {
            str2 = A002.B4K(j2, str2, r2);
        } else if (r2.A02) {
            AnonymousClass0XE.A01(r2).A00 = AnonymousClass0XF.A02;
        }
        if (z2) {
            AnonymousClass0XF r12 = r2.A00;
            String syncFetchReason = this.A09.syncFetchReason();
            C25071Yf r7 = this.A01;
            if (r12.source <= 1) {
                z = false;
            }
            String name = r12.name();
            if (str2 == null) {
                A0P = "<null>";
            } else {
                int length = str2.length();
                if (length <= 12) {
                    A0P = str2;
                } else {
                    A0P = AnonymousClass08S.A0P(str2.substring(0, 5), "..", str2.substring(length - 5, length));
                }
            }
            r7.BfI(j2, 200000, z, name, syncFetchReason, A0P, this.A0I);
        }
        return str2;
    }

    public boolean BFd(boolean z) {
        if (this.A07 != z || this.A0J == null) {
            return false;
        }
        return true;
    }

    public void onConfigChanged(String[] strArr) {
        if (strArr != null && strArr.length != 0) {
            synchronized (this) {
                this.A0J = null;
                C04310Tq r0 = this.A04;
                if (!(r0 == null || r0.get() == null)) {
                    ((AnonymousClass1YI) this.A04.get()).C0L(true);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:166:0x033f, code lost:
        if (r10 == false) goto L_0x0356;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0341, code lost:
        ((X.AnonymousClass0VK) X.AnonymousClass1XX.A02(0, X.AnonymousClass1Y3.Anj, r9.A01)).C4Y(new X.AnonymousClass48t(r9), (long) r4, java.util.concurrent.TimeUnit.SECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005f, code lost:
        if (java.lang.Integer.parseInt(r7) == 0) goto L_0x0061;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onEpConfigChanged(java.lang.String[] r42, java.lang.String[] r43) {
        /*
            r41 = this;
            r40 = r41
            r17 = 0
            r12 = r42
            if (r42 == 0) goto L_0x0357
            int r11 = r12.length
            if (r11 == 0) goto L_0x0357
            r6 = r43
            if (r43 == 0) goto L_0x0357
            int r4 = r6.length
            if (r4 == 0) goto L_0x0357
            X.43u r5 = new X.43u
            r5.<init>()
            android.util.SparseArray r0 = new android.util.SparseArray
            r0.<init>()
            r5.A00 = r0
            r10 = 0
        L_0x001f:
            java.lang.String r9 = ","
            r8 = 1
            java.lang.String r3 = "\\d+"
            r2 = -1
            if (r10 >= r11) goto L_0x009d
            r0 = r42[r10]
            java.lang.String[] r1 = r0.split(r9)
            int r7 = r1.length
            r0 = 6
            if (r7 != r0) goto L_0x0094
            r7 = r1[r17]
            boolean r0 = r7.matches(r3)
            if (r0 == 0) goto L_0x009b
            int r9 = java.lang.Integer.parseInt(r7)
        L_0x003d:
            if (r9 == r2) goto L_0x0094
            r19 = r1[r8]
            r0 = 2
            r7 = r1[r0]
            boolean r0 = r7.matches(r3)
            if (r0 == 0) goto L_0x0099
            int r13 = java.lang.Integer.parseInt(r7)
        L_0x004e:
            if (r13 == r2) goto L_0x0094
            r0 = 3
            r7 = r1[r0]
            boolean r0 = r7.matches(r3)
            if (r0 == 0) goto L_0x0061
            int r0 = java.lang.Integer.parseInt(r7)
            r21 = 1
            if (r0 != 0) goto L_0x0063
        L_0x0061:
            r21 = 0
        L_0x0063:
            r0 = 4
            r7 = r1[r0]
            boolean r0 = r7.matches(r3)
            if (r0 == 0) goto L_0x0097
            int r7 = java.lang.Integer.parseInt(r7)
        L_0x0070:
            if (r7 == r2) goto L_0x0094
            r0 = 5
            r1 = r1[r0]
            boolean r0 = r1.matches(r3)
            if (r0 == 0) goto L_0x0081
            int r0 = java.lang.Integer.parseInt(r1)
            if (r0 != 0) goto L_0x0082
        L_0x0081:
            r8 = 0
        L_0x0082:
            X.3yL r1 = new X.3yL
            r18 = r1
            r20 = r13
            r22 = r7
            r23 = r8
            r18.<init>(r19, r20, r21, r22, r23)
            android.util.SparseArray r0 = r5.A00
            r0.put(r9, r1)
        L_0x0094:
            int r10 = r10 + 1
            goto L_0x001f
        L_0x0097:
            r7 = -1
            goto L_0x0070
        L_0x0099:
            r13 = -1
            goto L_0x004e
        L_0x009b:
            r9 = -1
            goto L_0x003d
        L_0x009d:
            r7 = 0
        L_0x009e:
            if (r7 >= r4) goto L_0x00f9
            r0 = r43[r7]
            java.lang.String[] r14 = r0.split(r9)
            r1 = r14[r17]
            boolean r0 = r1.matches(r3)
            if (r0 == 0) goto L_0x00f7
            int r10 = java.lang.Integer.parseInt(r1)
        L_0x00b2:
            if (r10 == r2) goto L_0x00ee
            java.util.HashSet r1 = new java.util.HashSet
            r1.<init>()
            r13 = 1
        L_0x00ba:
            int r0 = r14.length
            if (r13 >= r0) goto L_0x00dc
            r11 = r14[r13]
            boolean r0 = r11.matches(r3)
            r15 = -1
            if (r0 == 0) goto L_0x00d9
            long r11 = java.lang.Long.parseLong(r11)
        L_0x00cb:
            int r0 = (r11 > r15 ? 1 : (r11 == r15 ? 0 : -1))
            if (r0 == 0) goto L_0x00d6
            java.lang.Long r0 = java.lang.Long.valueOf(r11)
            r1.add(r0)
        L_0x00d6:
            int r13 = r13 + 1
            goto L_0x00ba
        L_0x00d9:
            r11 = -1
            goto L_0x00cb
        L_0x00dc:
            android.util.SparseArray r0 = r5.A00
            int r0 = r0.indexOfKey(r10)
            if (r0 < 0) goto L_0x00f1
            android.util.SparseArray r0 = r5.A00
            java.lang.Object r0 = r0.get(r10)
            X.3yL r0 = (X.C83863yL) r0
            r0.A00 = r1
        L_0x00ee:
            int r7 = r7 + 1
            goto L_0x009e
        L_0x00f1:
            android.util.SparseArray r0 = r5.A00
            r0.remove(r10)
            goto L_0x00ee
        L_0x00f7:
            r10 = -1
            goto L_0x00b2
        L_0x00f9:
            r0 = r40
            X.0Wb r13 = r0.A0C
            if (r13 == 0) goto L_0x0357
            r10 = r0
            int[] r4 = r40.A0B()
            java.util.ArrayList r25 = new java.util.ArrayList
            r25.<init>()
            int r3 = r4.length
            r2 = 0
        L_0x010b:
            if (r2 >= r3) goto L_0x011b
            r0 = r4[r2]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            r0 = r25
            r0.add(r1)
            int r2 = r2 + 1
            goto L_0x010b
        L_0x011b:
            java.util.HashSet r24 = new java.util.HashSet
            r24.<init>()
            java.util.HashSet r23 = new java.util.HashSet
            r23.<init>()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r7 = 0
            r9 = 2147483647(0x7fffffff, float:NaN)
            r4 = 2147483647(0x7fffffff, float:NaN)
            r38 = 0
            r39 = 1
        L_0x0135:
            android.util.SparseArray r0 = r5.A00
            int r0 = r0.size()
            if (r7 >= r0) goto L_0x02b1
            android.util.SparseArray r0 = r5.A00
            int r3 = r0.keyAt(r7)
            android.util.SparseArray r0 = r5.A00
            java.lang.Object r11 = r0.get(r3)
            X.3yL r11 = (X.C83863yL) r11
            if (r11 == 0) goto L_0x0191
            java.lang.String r0 = r11.A03
            boolean r0 = X.C04760Wb.A01(r0)
            if (r0 != 0) goto L_0x0157
            r39 = 0
        L_0x0157:
            java.lang.Integer r22 = java.lang.Integer.valueOf(r3)
            r1 = r22
            r2 = r25
            boolean r29 = r2.contains(r1)
            int r0 = r11.A02
            r33 = r0
            X.0Wc r2 = r10.A07(r3)
            if (r29 != 0) goto L_0x0196
            X.95Z r0 = new X.95Z
            java.lang.String r14 = r11.A03
            boolean r12 = r11.A05
            int r10 = r11.A01
            boolean r1 = r11.A04
            r32 = 0
            r34 = 0
            r36 = 0
            r30 = r10
            r31 = r1
            r35 = r3
            r37 = r2
            r26 = r0
            r27 = r14
            r28 = r12
            r26.<init>(r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37)
            r6.add(r0)
        L_0x0191:
            int r7 = r7 + 1
            r10 = r40
            goto L_0x0135
        L_0x0196:
            X.0Wc r10 = r10.A06(r3)
            java.util.Map r0 = r10.Al7()
            boolean r12 = r0.containsKey(r1)
            if (r12 == 0) goto L_0x01d9
            java.lang.Object r0 = r0.get(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r15 = r0.intValue()
        L_0x01ae:
            r0 = r33
            if (r15 < r0) goto L_0x01db
            X.95Z r0 = new X.95Z
            java.lang.String r1 = r11.A03
            r16 = r1
            boolean r14 = r11.A05
            int r12 = r11.A01
            boolean r1 = r11.A04
            r34 = 0
            r30 = r12
            r31 = r1
            r32 = r15
            r35 = r3
            r36 = r10
            r37 = r2
            r26 = r0
            r27 = r16
            r28 = r14
            r26.<init>(r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37)
            r6.add(r0)
            goto L_0x0191
        L_0x01d9:
            r15 = 0
            goto L_0x01ae
        L_0x01db:
            java.util.Set r0 = r11.A00
            java.util.Iterator r21 = r0.iterator()
            r20 = 1
        L_0x01e3:
            boolean r0 = r21.hasNext()
            if (r0 == 0) goto L_0x023b
            java.lang.Object r0 = r21.next()
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
            if (r20 == 0) goto L_0x023b
            int r12 = X.AnonymousClass0XG.A00(r0)
            r14 = 1
            if (r12 == r14) goto L_0x0230
            r14 = 2
            if (r12 == r14) goto L_0x0223
            r14 = 3
            if (r12 == r14) goto L_0x0214
            r14 = 4
            if (r12 != r14) goto L_0x01e3
            double r18 = r10.Aki(r0)
            double r16 = r2.Aki(r0)
            int r0 = (r18 > r16 ? 1 : (r18 == r16 ? 0 : -1))
            if (r0 == 0) goto L_0x01e3
        L_0x0211:
            r20 = 0
            goto L_0x01e3
        L_0x0214:
            java.lang.String r12 = r10.B4C(r0)
            java.lang.String r0 = r2.B4C(r0)
            boolean r0 = r12.equals(r0)
            if (r0 != 0) goto L_0x01e3
            goto L_0x0211
        L_0x0223:
            long r18 = r10.At0(r0)
            long r16 = r2.At0(r0)
            int r0 = (r18 > r16 ? 1 : (r18 == r16 ? 0 : -1))
            if (r0 == 0) goto L_0x01e3
            goto L_0x0211
        L_0x0230:
            boolean r12 = r10.Aem(r0)
            boolean r0 = r2.Aem(r0)
            if (r12 == r0) goto L_0x01e3
            goto L_0x0211
        L_0x023b:
            X.95Z r0 = new X.95Z
            java.lang.String r1 = r11.A03
            r16 = r1
            boolean r14 = r11.A05
            int r12 = r11.A01
            boolean r1 = r11.A04
            r34 = r20 ^ 1
            r26 = r0
            r27 = r16
            r28 = r14
            r30 = r12
            r31 = r1
            r32 = r15
            r35 = r3
            r36 = r10
            r37 = r2
            r26.<init>(r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37)
            r6.add(r0)
            if (r20 != 0) goto L_0x0191
            boolean r0 = r11.A05
            if (r0 == 0) goto L_0x0284
            java.lang.String r1 = r11.A03
            r0 = r24
            r0.add(r1)
            int r0 = r11.A01
            if (r0 >= r9) goto L_0x0273
            r9 = r0
        L_0x0273:
            java.lang.String r0 = r11.A03
            boolean r0 = X.C04760Wb.A01(r0)
            if (r0 != 0) goto L_0x0280
            int r0 = r11.A01
            if (r0 >= r4) goto L_0x0280
            r4 = r0
        L_0x0280:
            r38 = 1
            goto L_0x0191
        L_0x0284:
            boolean r0 = r11.A04
            if (r0 == 0) goto L_0x0191
            if (r3 < 0) goto L_0x02a5
            r0 = r40
            java.util.concurrent.atomic.AtomicReferenceArray r0 = r0.A0L
            int r0 = r0.length()
            if (r3 >= r0) goto L_0x02a5
            r0 = r40
            java.util.concurrent.atomic.AtomicReferenceArray r1 = r0.A0L
            r0 = 0
            r1.set(r3, r0)
        L_0x029c:
            java.lang.String r1 = r11.A03
            r0 = r23
            r0.add(r1)
            goto L_0x0191
        L_0x02a5:
            java.lang.Object[] r2 = new java.lang.Object[]{r22}
            java.lang.String r1 = "MobileConfigFactoryImpl"
            java.lang.String r0 = "Cannot refresh config index(%d) from config caches"
            X.C010708t.A0P(r1, r0, r2)
            goto L_0x029c
        L_0x02b1:
            int r0 = r24.size()
            if (r0 <= 0) goto L_0x02b9
            r38 = 1
        L_0x02b9:
            java.util.Iterator r1 = r24.iterator()
            r37 = 0
        L_0x02bf:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x02d4
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = X.C04760Wb.A01(r0)
            if (r0 != 0) goto L_0x02bf
            r37 = 1
            goto L_0x02bf
        L_0x02d4:
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r4 != r0) goto L_0x02da
            r4 = 0
        L_0x02da:
            if (r9 != r0) goto L_0x02dd
            r9 = 0
        L_0x02dd:
            X.95X r3 = new X.95X
            r2 = 0
            r31 = r3
            r32 = r2
            r33 = r4
            r34 = r9
            r35 = r24
            r36 = r23
            r31.<init>(r32, r33, r34, r35, r36, r37, r38, r39)
            int r1 = X.AnonymousClass1Y3.APj
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.95S r0 = (X.AnonymousClass95S) r0
            r0.A01(r3, r6)
            if (r37 == 0) goto L_0x0356
            int r2 = X.AnonymousClass1Y3.BNv
            X.0UN r1 = r13.A00
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r8, r2, r1)
            X.0yv r9 = (X.C17450yv) r9
            java.lang.String r0 = r3.A02
            java.lang.Object r3 = r9.A04
            monitor-enter(r3)
            r10 = 1
            r9.A03 = r8     // Catch:{ all -> 0x033c }
            r9.A02 = r0     // Catch:{ all -> 0x033c }
            if (r4 == 0) goto L_0x032f
            boolean r0 = X.AnonymousClass01P.A0D()     // Catch:{ all -> 0x033c }
            if (r0 == 0) goto L_0x032f
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x033c }
            int r0 = r4 * 1000
            long r0 = (long) r0     // Catch:{ all -> 0x033c }
            long r7 = r7 + r0
            long r5 = r9.A00     // Catch:{ all -> 0x033c }
            r2 = 0
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x032d
            r9.A00 = r7     // Catch:{ all -> 0x033c }
        L_0x032b:
            monitor-exit(r3)     // Catch:{ all -> 0x033c }
            goto L_0x033f
        L_0x032d:
            r10 = 0
            goto L_0x032b
        L_0x032f:
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x033c }
            r9.A00 = r0     // Catch:{ all -> 0x033c }
            java.lang.String r0 = "Emergency Push is resetting app immediately"
            X.C17450yv.A01(r9, r0)     // Catch:{ all -> 0x033c }
            monitor-exit(r3)     // Catch:{ all -> 0x033c }
            return r38
        L_0x033c:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x033c }
            throw r0
        L_0x033f:
            if (r10 == 0) goto L_0x0356
            int r1 = X.AnonymousClass1Y3.Anj
            X.0UN r0 = r9.A01
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0VK r5 = (X.AnonymousClass0VK) r5
            X.48t r3 = new X.48t
            r3.<init>(r9)
            long r1 = (long) r4
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS
            r5.C4Y(r3, r1, r0)
        L_0x0356:
            return r38
        L_0x0357:
            return r17
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25041Yc.onEpConfigChanged(java.lang.String[], java.lang.String[]):boolean");
    }

    public C25041Yc(AnonymousClass0WV r3, C04760Wb r4, boolean z) {
        this.A09 = r3;
        this.A08 = r3.getNewOverridesTableIfExists();
        this.A0J = null;
        this.A0N = null;
        this.A0C = r4;
        this.A0L = new AtomicReferenceArray(10000);
        this.A0I = z;
    }

    public static void A04(C25041Yc r4, boolean z) {
        C04310Tq r0;
        boolean A052 = r4.A05();
        synchronized (r4) {
            r4.A08 = r4.A09.getNewOverridesTableIfExists();
            r4.A0F.clear();
            r4.A0L = new AtomicReferenceArray(10000);
            r4.A0J = null;
            if (r4.A0A.get() && A052) {
                r4.A0B.set(false);
                r4.A0H.set(false);
                r4.A0G.set(false);
                r4.A0M = null;
            }
        }
        if (z && (r0 = r4.A04) != null && r0.get() != null) {
            ((AnonymousClass1YI) r4.A04.get()).C0L(false);
        }
    }
}
