package X;

import android.os.Looper;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0vC  reason: invalid class name and case insensitive filesystem */
public abstract class C15380vC implements C15390vD {
    private C15400vE A00 = C15400vE.A02;
    public final Set A01 = new C05180Xy();

    public void A04() {
        if (this instanceof C182410w) {
            return;
        }
        if (this instanceof AnonymousClass12M) {
            AnonymousClass12M r3 = (AnonymousClass12M) this;
            C24971Xv it = r3.A01.iterator();
            while (it.hasNext()) {
                ((C15520vQ) it.next()).Ae6().CIL(r3.A00);
            }
            r3.A06();
        } else if (this instanceof C15370vB) {
            C15370vB r4 = (C15370vB) this;
            C199819ap r5 = (C199819ap) AnonymousClass1XX.A03(AnonymousClass1Y3.BDS, r4.A00);
            r4.A01 = r5;
            C46932Sl r2 = new C46932Sl(r5);
            r5.A04 = r2;
            r5.A01 = new AnonymousClass6QM(r5);
            r5.A03 = new C199889aw(r5);
            r5.A05 = new C199849as(r5);
            r5.A0D.C0f(C10980lB.A06, r2);
            AnonymousClass6QG r0 = r5.A08;
            r0.A00.add(r5.A01);
            C197479Ql r22 = r5.A0C;
            C197489Qm r1 = r5.A03;
            synchronized (r22) {
                r22.A00.add(r1);
            }
            C199829aq r32 = r5.A0A;
            String str = C06680bu.A08;
            AnonymousClass06U r12 = r5.A05;
            C06600bl BMm = r32.A01.BMm();
            BMm.A02(str, r12);
            C06790c5 A002 = BMm.A00();
            r32.A00 = A002;
            A002.A00();
            ((C100384ql) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AZ0, r5.A00)).CIL(r5.A09);
            r4.A01.A02 = new C199899ax(r4);
            C15370vB.A01(r4);
        } else if (this instanceof C32521lt) {
        } else {
            if (!(this instanceof AnonymousClass12P)) {
                AnonymousClass11W r23 = (AnonymousClass11W) this;
                if (r23.A02.A0E() || r23.A02.A0N() || r23.A02.A04()) {
                    r23.A03.A05(r23.A01);
                    return;
                }
                r23.A04.A0W(r23);
                r23.A04.A0S(r23.A05);
                return;
            }
            AnonymousClass12P r24 = (AnonymousClass12P) this;
            r24.A00.CIL(r24.A01);
            AnonymousClass12P.A00(r24);
        }
    }

    public void A05() {
        if (this instanceof C182410w) {
            return;
        }
        if (this instanceof AnonymousClass12M) {
            AnonymousClass12M r3 = (AnonymousClass12M) this;
            C24971Xv it = r3.A01.iterator();
            while (it.hasNext()) {
                ((C15520vQ) it.next()).Ae6().CK1(r3.A00);
            }
        } else if (this instanceof C15370vB) {
            C15370vB r4 = (C15370vB) this;
            C199819ap r5 = r4.A01;
            if (r5 != null) {
                r5.A02 = null;
                AnonymousClass0ZM r2 = r5.A04;
                if (r2 != null) {
                    r5.A0D.CJr(C10980lB.A06, r2);
                }
                AnonymousClass6QM r1 = r5.A01;
                if (r1 != null) {
                    r5.A08.A00.remove(r1);
                }
                C197489Qm r22 = r5.A03;
                if (r22 != null) {
                    C197479Ql r12 = r5.A0C;
                    synchronized (r12) {
                        r12.A00.remove(r22);
                    }
                }
                C199829aq r13 = r5.A0A;
                C06790c5 r0 = r13.A00;
                if (r0 != null && r0.A02()) {
                    r13.A00.A01();
                }
                r13.A00 = null;
                ((C100384ql) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AZ0, r5.A00)).CK1(r5.A09);
                r4.A01 = null;
            }
        } else if (this instanceof C32521lt) {
        } else {
            if (!(this instanceof AnonymousClass12P)) {
                AnonymousClass11W r23 = (AnonymousClass11W) this;
                if (r23.A02.A0E() || r23.A02.A0N() || r23.A02.A04()) {
                    r23.A03.A05(null);
                    return;
                }
                r23.A04.A0X(r23);
                r23.A04.A0T(r23.A05);
                return;
            }
            AnonymousClass12P r02 = (AnonymousClass12P) this;
            r02.A00.CK1(r02.A01);
        }
    }

    public void BUD() {
    }

    public final C15400vE Ae7() {
        return this.A00;
    }

    public final void CIL(C15960wG r3) {
        boolean isEmpty = this.A01.isEmpty();
        this.A01.add(r3);
        if (isEmpty) {
            A04();
        }
    }

    public void CK1(C15960wG r2) {
        if (this.A01.remove(r2) && this.A01.isEmpty()) {
            A05();
        }
    }

    public final void A03(C15400vE r3) {
        if (Looper.myLooper() == Looper.getMainLooper() && !this.A00.equals(r3)) {
            this.A00 = r3;
            Iterator it = new C05180Xy(this.A01).iterator();
            while (it.hasNext()) {
                ((C15960wG) it.next()).BPI();
            }
        }
    }
}
