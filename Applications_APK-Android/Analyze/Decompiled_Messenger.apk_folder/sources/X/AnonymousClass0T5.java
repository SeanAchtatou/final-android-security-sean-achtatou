package X;

import java.util.concurrent.Executor;

/* renamed from: X.0T5  reason: invalid class name */
public final /* synthetic */ class AnonymousClass0T5 implements Executor {
    public static final Executor A00 = new AnonymousClass0T5();

    private AnonymousClass0T5() {
    }

    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
