package X;

import android.content.Context;
import java.util.Map;

/* renamed from: X.0bs  reason: invalid class name and case insensitive filesystem */
public abstract class C06660bs implements C007306v {
    public Map A00;
    public AnonymousClass09U[] A01;
    public AnonymousClass1ZT[] A02;
    public final Context A03;

    public String A02() {
        return ((C06650br) this).A00.B4C(846177276854513L);
    }

    public String A03() {
        return ((C06650br) this).A00.B4C(846177276788976L);
    }

    public String A04() {
        return ((C06650br) this).A00.B4C(846164391952619L);
    }

    public synchronized AnonymousClass1ZT[] AjO() {
        if (this.A02 == null) {
            this.A02 = AnonymousClass1ZT.A00(A03());
        }
        return this.A02;
    }

    public synchronized Map Ajo() {
        if (this.A00 == null) {
            this.A00 = C24461Ts.A00(A04());
        }
        return this.A00;
    }

    public synchronized AnonymousClass09U[] AqR() {
        if (this.A01 == null) {
            this.A01 = AnonymousClass09U.A00(A02(), this.A03);
        }
        return this.A01;
    }

    public C06660bs(Context context) {
        this.A03 = context;
    }
}
