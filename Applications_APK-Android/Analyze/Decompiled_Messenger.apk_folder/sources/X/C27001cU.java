package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableEntry;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1cU  reason: invalid class name and case insensitive filesystem */
public abstract class C27001cU implements Iterator {
    public final Iterator A00;

    public Object A00(Object obj) {
        if (this instanceof C31251jO) {
            return new B7F((C31251jO) this, obj);
        }
        if (this instanceof C31261jP) {
            return ((C74603iA) obj).A0E();
        }
        if (this instanceof C14330t8) {
            return new C74593i9((Map.Entry) obj);
        }
        if (this instanceof AnonymousClass0r1) {
            return new C21241Fu((AnonymousClass0r1) this, (Map.Entry) obj);
        }
        if (this instanceof C13040qU) {
            return new ImmutableEntry(obj, ((C13040qU) this).A00.apply(obj));
        }
        return (!(this instanceof C31271jQ) ? !(this instanceof AnonymousClass0t2) ? ((C26991cT) this).A00 : ((AnonymousClass0t2) this).A00.function : ((C31271jQ) this).A00.function).apply(obj);
    }

    public final boolean hasNext() {
        return this.A00.hasNext();
    }

    public final Object next() {
        return A00(this.A00.next());
    }

    public final void remove() {
        this.A00.remove();
    }

    public C27001cU(Iterator it) {
        Preconditions.checkNotNull(it);
        this.A00 = it;
    }
}
