package X;

import com.mapbox.mapboxsdk.log.Logger;
import java.lang.ref.WeakReference;

/* renamed from: X.1zd  reason: invalid class name and case insensitive filesystem */
public final class C39801zd implements C28865E7w {
    private final WeakReference A00;

    public void BYd(Exception exc) {
        Logger.e(C22298Ase.$const$string(143), "Failed to obtain location update", exc);
    }

    public void Bqf(Object obj) {
        C28862E7q e7q = (C28862E7q) obj;
        C28394DuB duB = (C28394DuB) this.A00.get();
        if (duB != null) {
            C28394DuB.A06(duB, e7q.A01(), false);
        }
    }

    public C39801zd(C28394DuB duB) {
        this.A00 = new WeakReference(duB);
    }
}
