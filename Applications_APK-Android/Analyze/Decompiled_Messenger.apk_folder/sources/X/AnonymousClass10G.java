package X;

import com.facebook.acra.ACRA;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.10G  reason: invalid class name */
public enum AnonymousClass10G {
    LEFT(0),
    TOP(1),
    RIGHT(2),
    BOTTOM(3),
    START(4),
    END(5),
    A04(6),
    A09(7),
    ALL(8);
    
    public final int mIntValue;

    private AnonymousClass10G(int i) {
        this.mIntValue = i;
    }

    public static AnonymousClass10G A00(int i) {
        switch (i) {
            case 0:
                return LEFT;
            case 1:
                return TOP;
            case 2:
                return RIGHT;
            case 3:
                return BOTTOM;
            case 4:
                return START;
            case 5:
                return END;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return A04;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return A09;
            case 8:
                return ALL;
            default:
                throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown enum value: ", i));
        }
    }
}
