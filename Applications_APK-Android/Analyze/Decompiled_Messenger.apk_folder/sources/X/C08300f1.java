package X;

/* renamed from: X.0f1  reason: invalid class name and case insensitive filesystem */
public final class C08300f1 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.mobileboost.apps.common.BaseOptimizationProvider$1";
    public final /* synthetic */ C08010eX A00;

    public C08300f1(C08010eX r1) {
        this.A00 = r1;
    }

    public void run() {
        for (int i : this.A00.A01) {
            C08040ea r5 = (C08040ea) this.A00.A01.get(i);
            for (C07210ct r1 : r5.A04) {
                if (0 != 0) {
                    try {
                        C08040ea.A00(r5, r1);
                    } catch (Exception e) {
                        r5.A00.A00.CGZ("MobileBoost", "BoosterInitializationWithException", e);
                    }
                } else {
                    C08040ea.A00(r5, r1);
                }
            }
        }
    }
}
