package X;

import android.os.Build;
import java.lang.reflect.Method;

/* renamed from: X.00i  reason: invalid class name and case insensitive filesystem */
public final class C000400i {
    public final Method A00;
    public final Method A01;
    public final Method A02;
    public final Method A03;

    public static C000400i A00() {
        Method method;
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            Class<String> cls2 = String.class;
            Method method2 = cls.getMethod("get", cls2);
            Method method3 = cls.getMethod("getLong", cls2, Long.TYPE);
            Method method4 = cls.getMethod("set", cls2, cls2);
            if (Build.VERSION.SDK_INT >= 16) {
                method = cls.getMethod("addChangeCallback", Runnable.class);
            } else {
                method = null;
            }
            return new C000400i(method, method2, method3, method4);
        } catch (ClassNotFoundException | NoSuchMethodException unused) {
            return null;
        }
    }

    private C000400i(Method method, Method method2, Method method3, Method method4) {
        this.A00 = method;
        this.A01 = method2;
        this.A02 = method3;
        this.A03 = method4;
    }
}
