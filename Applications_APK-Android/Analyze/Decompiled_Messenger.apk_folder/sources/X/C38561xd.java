package X;

import java.util.List;
import java.util.Set;

/* renamed from: X.1xd  reason: invalid class name and case insensitive filesystem */
public abstract class C38561xd implements AnonymousClass1XY {
    public AnonymousClass1XY mInjector;

    public AnonymousClass1XY getApplicationInjector() {
        return this.mInjector.getApplicationInjector();
    }

    public C24811Xe getInjectorThreadStack() {
        return this.mInjector.getInjectorThreadStack();
    }

    public AnonymousClass0US getLazy(C22916BKm bKm) {
        return this.mInjector.getLazy(bKm);
    }

    public AnonymousClass0US getLazyList(C22916BKm bKm) {
        return this.mInjector.getLazyList(bKm);
    }

    public AnonymousClass0US getLazySet(C22916BKm bKm) {
        return this.mInjector.getLazySet(bKm);
    }

    public List getList(C22916BKm bKm) {
        return this.mInjector.getList(bKm);
    }

    public C04310Tq getListProvider(C22916BKm bKm) {
        return this.mInjector.getScopeAwareInjector().getListProvider(bKm);
    }

    public C04310Tq getProvider(C22916BKm bKm) {
        return this.mInjector.getScopeAwareInjector().getProvider(bKm);
    }

    public C24891Xn getScope(Class cls) {
        return this.mInjector.getScope(cls);
    }

    public C24851Xi getScopeAwareInjector() {
        return this.mInjector.getScopeAwareInjector();
    }

    public AnonymousClass0UH getScopeAwareInjectorInternal() {
        return this.mInjector.getScopeAwareInjector();
    }

    public C24781Xb getScopeUnawareInjector() {
        return this.mInjector.getScopeUnawareInjector();
    }

    public Set getSet(C22916BKm bKm) {
        return this.mInjector.getSet(bKm);
    }

    public C04310Tq getSetProvider(C22916BKm bKm) {
        return this.mInjector.getScopeAwareInjector().getSetProvider(bKm);
    }

    public void setInjector(AnonymousClass1XY r1) {
        this.mInjector = r1;
    }

    public Object getInstance(int i) {
        return this.mInjector.getInstance(i);
    }

    public Object getInstance(C22916BKm bKm) {
        return this.mInjector.getInstance(bKm);
    }

    public Object getInstance(Class cls) {
        return this.mInjector.getInstance(cls);
    }

    public Object getInstance(Class cls, Class cls2) {
        return this.mInjector.getInstance(cls, cls2);
    }
}
