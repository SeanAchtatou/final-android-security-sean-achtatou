package X;

/* renamed from: X.1p9  reason: invalid class name and case insensitive filesystem */
public final class C34251p9 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.scroll.executor.FeedScrollIdleRunnable";
    private final long A00;
    private final C07980eU A01;
    private final Runnable A02;

    public void run() {
        try {
            this.A01.A03(this.A00);
        } catch (InterruptedException e) {
            C010708t.A0L("FeedScrollIdleRunnable", "Exception while waiting until user is idle", e);
        }
        this.A02.run();
    }

    public C34251p9(Runnable runnable, C07980eU r2, long j) {
        this.A02 = runnable;
        this.A01 = r2;
        this.A00 = j;
    }
}
