package X;

import com.facebook.messaging.notify.JoinRequestNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1hy  reason: invalid class name and case insensitive filesystem */
public final class C30391hy extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$21";
    public final /* synthetic */ JoinRequestNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C30391hy(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, JoinRequestNotification joinRequestNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = joinRequestNotification;
    }
}
