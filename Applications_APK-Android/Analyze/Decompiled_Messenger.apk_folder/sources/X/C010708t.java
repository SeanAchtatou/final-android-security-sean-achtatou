package X;

import com.facebook.common.stringformat.StringFormatUtil;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.08t  reason: invalid class name and case insensitive filesystem */
public final class C010708t {
    private static final List A00 = new ArrayList();
    public static volatile AnonymousClass02v A01 = AnonymousClass02t.A00;

    static {
        A01.C9Z(5);
        AnonymousClass02v r0 = A01;
        if (r0 != null) {
            AnonymousClass02w.A00 = r0;
            return;
        }
        throw new IllegalArgumentException();
    }

    public static void A00() {
        A01.BFj(3);
    }

    public static synchronized void A01(int i) {
        synchronized (C010708t.class) {
            A01.C9Z(i);
            for (AnonymousClass090 Bdr : A00) {
                Bdr.Bdr(i);
            }
        }
    }

    public static void A02(int i, String str, String str2) {
        if (A01.BFj(i)) {
            A01.BIm(i, str, str2);
        }
    }

    public static void A03(AnonymousClass02v r1) {
        if (r1 == null) {
            r1 = AnonymousClass02t.A00;
        }
        int Aum = A01.Aum();
        A01 = r1;
        r1.C9Z(Aum);
        AnonymousClass02v r0 = A01;
        if (r0 != null) {
            AnonymousClass02w.A00 = r0;
            return;
        }
        throw new IllegalArgumentException();
    }

    public static synchronized void A04(AnonymousClass090 r2) {
        synchronized (C010708t.class) {
            A00.add(r2);
        }
    }

    public static void A05(Class cls, String str) {
        if (A01.BFj(6)) {
            A01.AY3(C02190Di.A00(cls), str);
        }
    }

    public static void A06(Class cls, String str) {
        if (A01.BFj(5)) {
            A01.CMp(C02190Di.A00(cls), str);
        }
    }

    public static void A07(Class cls, String str) {
        if (A01.BFj(6)) {
            A01.CNm(C02190Di.A00(cls), str);
        }
    }

    public static void A08(Class cls, String str, Throwable th) {
        if (A01.BFj(6)) {
            A01.AY4(C02190Di.A00(cls), str, th);
        }
    }

    public static void A09(Class cls, String str, Throwable th) {
        if (A01.BFj(5)) {
            A01.CMq(C02190Di.A00(cls), str, th);
        }
    }

    public static void A0A(Class cls, String str, Throwable th) {
        if (A01.BFj(6)) {
            A01.CNn(C02190Di.A00(cls), str, th);
        }
    }

    public static void A0B(Class cls, String str, Object... objArr) {
        if (A01.BFj(6)) {
            A0I(C02190Di.A00(cls), StringFormatUtil.formatStrLocaleSafe(str, objArr));
        }
    }

    public static void A0C(Class cls, String str, Object... objArr) {
        if (A01.BFj(5)) {
            A0J(C02190Di.A00(cls), StringFormatUtil.formatStrLocaleSafe(str, objArr));
        }
    }

    public static void A0D(Class cls, String str, Object... objArr) {
        if (A01.BFj(6)) {
            A0K(C02190Di.A00(cls), StringFormatUtil.formatStrLocaleSafe(str, objArr));
        }
    }

    public static void A0E(Class cls, Throwable th, String str, Object... objArr) {
        if (A01.BFj(6)) {
            A0L(C02190Di.A00(cls), StringFormatUtil.formatStrLocaleSafe(str, objArr), th);
        }
    }

    public static void A0F(Class cls, Throwable th, String str, Object... objArr) {
        if (A01.BFj(5)) {
            A0M(C02190Di.A00(cls), StringFormatUtil.formatStrLocaleSafe(str, objArr), th);
        }
    }

    public static void A0G(Class cls, Throwable th, String str, Object... objArr) {
        if (A01.BFj(6)) {
            A0N(C02190Di.A00(cls), StringFormatUtil.formatStrLocaleSafe(str, objArr), th);
        }
    }

    public static void A0H(String str, String str2) {
        if (A01.BFj(3)) {
            A01.AWW(str, str2);
        }
    }

    public static void A0I(String str, String str2) {
        if (A01.BFj(6)) {
            A01.AY3(str, str2);
        }
    }

    public static void A0J(String str, String str2) {
        if (A01.BFj(5)) {
            A01.CMp(str, str2);
        }
    }

    public static void A0K(String str, String str2) {
        if (A01.BFj(6)) {
            A01.CNm(str, str2);
        }
    }

    public static void A0L(String str, String str2, Throwable th) {
        if (A01.BFj(6)) {
            A01.AY4(str, str2, th);
        }
    }

    public static void A0M(String str, String str2, Throwable th) {
        if (A01.BFj(5)) {
            A01.CMq(str, str2, th);
        }
    }

    public static void A0N(String str, String str2, Throwable th) {
        if (A01.BFj(6)) {
            A01.CNn(str, str2, th);
        }
    }

    public static void A0O(String str, String str2, Object... objArr) {
        if (A01.BFj(6)) {
            A0I(str, StringFormatUtil.formatStrLocaleSafe(str2, objArr));
        }
    }

    public static void A0P(String str, String str2, Object... objArr) {
        if (A01.BFj(5)) {
            A0J(str, StringFormatUtil.formatStrLocaleSafe(str2, objArr));
        }
    }

    public static void A0Q(String str, String str2, Object... objArr) {
        if (A01.BFj(6)) {
            A0K(str, StringFormatUtil.formatStrLocaleSafe(str2, objArr));
        }
    }

    public static void A0R(String str, Throwable th, String str2) {
        if (A01.BFj(6)) {
            A01.AY4(str, str2, th);
        }
    }

    public static void A0S(String str, Throwable th, String str2) {
        if (A01.BFj(5)) {
            A01.CMq(str, str2, th);
        }
    }

    public static void A0T(String str, Throwable th, String str2) {
        if (A01.BFj(6)) {
            A01.CNn(str, str2, th);
        }
    }

    public static void A0U(String str, Throwable th, String str2, Object... objArr) {
        if (A01.BFj(6)) {
            A0L(str, StringFormatUtil.formatStrLocaleSafe(str2, objArr), th);
        }
    }

    public static void A0V(String str, Throwable th, String str2, Object... objArr) {
        if (A01.BFj(5)) {
            A0M(str, StringFormatUtil.formatStrLocaleSafe(str2, objArr), th);
        }
    }

    public static void A0W(String str, Throwable th, String str2, Object... objArr) {
        if (A01.BFj(6)) {
            A0N(str, StringFormatUtil.formatStrLocaleSafe(str2, objArr), th);
        }
    }

    public static boolean A0X(int i) {
        return A01.BFj(i);
    }
}
