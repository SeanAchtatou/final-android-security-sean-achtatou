package X;

import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

@InjectorModule
/* renamed from: X.14B  reason: invalid class name */
public final class AnonymousClass14B extends AnonymousClass0UV {
    public static final Boolean A00(AnonymousClass1XY r2) {
        return Boolean.valueOf(FbSharedPreferencesModule.A00(r2).Aep(C31831kS.A00, false));
    }
}
