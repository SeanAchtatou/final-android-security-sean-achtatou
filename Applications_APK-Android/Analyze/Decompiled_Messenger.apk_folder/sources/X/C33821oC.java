package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.proxygen.TraceFieldType;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1oC  reason: invalid class name and case insensitive filesystem */
public final class C33821oC {
    private static volatile C33821oC A06;
    public final AnonymousClass1ZE A00;
    public final C21041Eu A01;
    public final C25051Yd A02;
    public final FbSharedPreferences A03;
    private final DeprecatedAnalyticsLogger A04;
    private final C13850sB A05;

    public static final C33821oC A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (C33821oC.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new C33821oC(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static Map A01(long j) {
        HashMap hashMap = new HashMap();
        hashMap.put(TraceFieldType.RequestID, Long.toString(j));
        return hashMap;
    }

    public static void A02(C33821oC r3, String str, Map map) {
        if (r3.A05.A00.AbO(AnonymousClass1Y3.A6h, false)) {
            C11670nb r1 = new C11670nb(str);
            if (map != null) {
                C11670nb.A02(r1, map, false);
            }
            r3.A04.A09(r1);
        }
    }

    public void A03(boolean z) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000;
        if (z) {
            boolean z2 = true;
            Map A012 = A01(this.A01.A02(true));
            if (this.A03.Aeq(C34341pQ.A00) == TriState.UNSET) {
                z2 = false;
            }
            A012.put("current_availability", Boolean.toString(this.A03.Aep(C34341pQ.A00, true)));
            A012.put("availabilty_is_client_override", Boolean.toString(z2));
            A02(this, "client_presence_app_foregrounded", A012);
            if (this.A02.Aem(285855843489690L)) {
                uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(this.A00.A01("unified_presence_client_app_foregrounded"), AnonymousClass1Y3.A4p);
                if (!uSLEBaseShape0S0000000.A0G()) {
                    return;
                }
            } else {
                return;
            }
        } else {
            A02(this, "client_presence_app_backgrounded", A01(this.A01.A01(true)));
            if (this.A02.Aem(285855843424153L)) {
                uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(this.A00.A01("unified_presence_client_app_backgrounded"), 579);
                if (!uSLEBaseShape0S0000000.A0G()) {
                    return;
                }
            } else {
                return;
            }
        }
        uSLEBaseShape0S0000000.A08("toggle_state", Boolean.valueOf(this.A03.Aep(C34341pQ.A00, true)));
        uSLEBaseShape0S0000000.A06();
    }

    private C33821oC(AnonymousClass1XY r2) {
        this.A04 = C06920cI.A00(r2);
        this.A05 = new C13850sB(r2);
        this.A03 = FbSharedPreferencesModule.A00(r2);
        this.A01 = C21041Eu.A00(r2);
        this.A00 = AnonymousClass1ZD.A00(r2);
        this.A02 = AnonymousClass0WT.A00(r2);
    }
}
