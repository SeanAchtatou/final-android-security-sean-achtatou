package X;

import android.net.Uri;
import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.browser.model.MessengerWebViewParams;
import com.facebook.messaging.business.common.calltoaction.model.CTAInformationIdentify;
import com.facebook.messaging.business.common.calltoaction.model.CTAPaymentInfo;
import com.facebook.messaging.business.common.calltoaction.model.CTAUserConfirmation;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.business.common.calltoaction.model.CallToActionSimpleTarget;
import com.facebook.messaging.business.common.calltoaction.model.CallToActionTarget;
import com.facebook.messaging.business.informationidentify.model.PIIPrivacy;
import com.facebook.messaging.business.informationidentify.model.PIIQuestion;
import com.facebook.messaging.business.informationidentify.model.PIISinglePage;
import com.facebook.messaging.business.mdotme.model.PlatformRefParams;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1hE  reason: invalid class name and case insensitive filesystem */
public final class C29941hE {
    public static CallToAction A00(JsonNode jsonNode) {
        CallToActionTarget callToActionTarget;
        CTAUserConfirmation cTAUserConfirmation;
        CTAInformationIdentify cTAInformationIdentify;
        MessengerWebViewParams A00;
        CTAPaymentInfo cTAPaymentInfo;
        PlatformRefParams platformRefParams;
        C16550xJ A002;
        JsonNode jsonNode2 = jsonNode;
        String A0N = JSONUtil.A0N(jsonNode2.get("action_open_type"));
        String A0N2 = JSONUtil.A0N(jsonNode2.get("id"));
        String A0N3 = JSONUtil.A0N(jsonNode2.get("action_title"));
        String A0N4 = JSONUtil.A0N(jsonNode2.get("action_url"));
        String A0N5 = JSONUtil.A0N(jsonNode2.get("native_url"));
        JsonNode jsonNode3 = jsonNode2.get("action_object");
        C16550xJ A003 = C16550xJ.A00(A0N);
        if (jsonNode3 == null || jsonNode3.isNull()) {
            callToActionTarget = null;
        } else {
            C210809xp r0 = (C210809xp) C210779xl.A00.get(A003);
            if (r0 == null) {
                r0 = CallToActionSimpleTarget.CREATOR;
            }
            callToActionTarget = r0.AX8(jsonNode3);
        }
        boolean A0U = JSONUtil.A0U(jsonNode2.get("is_mutable_by_server"), false);
        JsonNode jsonNode4 = jsonNode2.get("user_confirmation");
        if (jsonNode4 == null) {
            cTAUserConfirmation = null;
        } else {
            C30011hL r1 = new C30011hL();
            r1.A02 = JSONUtil.A0N(jsonNode4.get("confirmation_title"));
            r1.A01 = JSONUtil.A0N(jsonNode4.get("confirmation_message"));
            r1.A03 = JSONUtil.A0N(jsonNode4.get("continue_button_label"));
            r1.A00 = JSONUtil.A0N(jsonNode4.get("cancel_button_label"));
            cTAUserConfirmation = new CTAUserConfirmation(r1);
        }
        JsonNode jsonNode5 = jsonNode2.get("cta_data");
        if (jsonNode5 == null) {
            cTAInformationIdentify = null;
        } else {
            C04160So r2 = new C04160So();
            r2.A05 = JSONUtil.A0N(jsonNode5.get("form_url"));
            r2.A03 = JSONUtil.A0N(jsonNode5.get("form_id"));
            r2.A01 = JSONUtil.A0N(jsonNode5.get("form_color_theme"));
            r2.A04 = JSONUtil.A0N(jsonNode5.get("form_num_screens"));
            r2.A02 = JSONUtil.A0N(jsonNode5.get("form_current_screen_index"));
            if (jsonNode5.get("form_first_screen") != null) {
                JsonNode jsonNode6 = jsonNode5.get("form_first_screen");
                C187588nS r3 = new C187588nS();
                r3.A02 = JSONUtil.A0N(jsonNode6.get("screen_title"));
                if (jsonNode6.get("questions") != null) {
                    ArrayNode A0E = JSONUtil.A0E(jsonNode6, "questions");
                    ImmutableList.Builder builder = new ImmutableList.Builder();
                    Iterator it = A0E.iterator();
                    while (it.hasNext()) {
                        JsonNode jsonNode7 = (JsonNode) it.next();
                        C187518nL r5 = new C187518nL();
                        r5.A02 = JSONUtil.A0N(jsonNode7.get("id"));
                        r5.A01 = C187818nr.A00(JSONUtil.A0N(jsonNode7.get("type")));
                        r5.A07 = JSONUtil.A0N(jsonNode7.get("title"));
                        r5.A05 = JSONUtil.A0N(jsonNode7.get("placeholder"));
                        r5.A06 = JSONUtil.A0N(jsonNode7.get("subtitle"));
                        r5.A03 = JSONUtil.A0N(jsonNode7.get("length"));
                        r5.A00 = C187598nT.A00(JSONUtil.A0N(jsonNode7.get("format")));
                        r5.A04 = JSONUtil.A0N(jsonNode7.get("mask"));
                        builder.add((Object) new PIIQuestion(r5));
                    }
                    r3.A01 = builder.build();
                }
                if (jsonNode6.get("business_privacy") != null) {
                    JsonNode jsonNode8 = jsonNode5.get("business_privacy");
                    C187738nj r52 = new C187738nj();
                    r52.A00 = JSONUtil.A0N(jsonNode8.get("text"));
                    r52.A01 = JSONUtil.A0N(jsonNode8.get("url"));
                    r3.A00 = new PIIPrivacy(r52);
                }
                r2.A00 = new PIISinglePage(r3);
            }
            cTAInformationIdentify = new CTAInformationIdentify(r2);
        }
        JsonNode jsonNode9 = jsonNode2.get("platform_webview_param");
        if (jsonNode9 == null) {
            A00 = null;
        } else {
            C30041hO r53 = new C30041hO();
            r53.A00 = JSONUtil.A00(jsonNode9.get("height_ratio"));
            r53.A07 = JSONUtil.A0S(jsonNode9.get("hide_share_button"));
            r53.A01 = C30051hP.A00(JSONUtil.A0N(jsonNode9.get("browser_chrome_style")));
            A00 = r53.A00();
        }
        JsonNode jsonNode10 = jsonNode2.get("payment_metadata");
        if (jsonNode10 == null) {
            cTAPaymentInfo = null;
        } else {
            C30071hR r32 = new C30071hR();
            r32.A01 = JSONUtil.A0N(jsonNode10.get("total_price"));
            r32.A00 = JSONUtil.A0N(jsonNode10.get("payment_module_config"));
            cTAPaymentInfo = new CTAPaymentInfo(r32);
        }
        JsonNode jsonNode11 = jsonNode2.get("postback_ref");
        if (jsonNode11 == null) {
            platformRefParams = null;
        } else {
            C30091hT r54 = new C30091hT();
            r54.A00 = JSONUtil.A0N(jsonNode11.get("postback_ref_code"));
            r54.A01 = JSONUtil.A0N(jsonNode11.get("postback_ref_code_source"));
            platformRefParams = new PlatformRefParams(r54);
        }
        String A0N6 = JSONUtil.A0N(jsonNode2.get(AnonymousClass24B.$const$string(73)));
        boolean A0S = JSONUtil.A0S(jsonNode2.get("is_disabled"));
        boolean A0S2 = JSONUtil.A0S(jsonNode2.get("is_post_handling_enabled"));
        String A0N7 = JSONUtil.A0N(jsonNode2.get("cta_render_style"));
        if (A0N2 == null || A0N3 == null || (A002 = C16550xJ.A00(A0N)) == null) {
            return null;
        }
        C30101hU A004 = C30101hU.A00(A0N7);
        C188515m r4 = new C188515m();
        r4.A0B = A0N2;
        r4.A0E = A0N3;
        r4.A01(A0N4);
        r4.A02(A0N5);
        r4.A08 = A002;
        r4.A09 = callToActionTarget;
        r4.A0G = A0U;
        r4.A06 = cTAUserConfirmation;
        r4.A02 = A00;
        r4.A05 = cTAPaymentInfo;
        r4.A04 = cTAInformationIdentify;
        r4.A0A = platformRefParams;
        r4.A0F = A0S;
        r4.A0H = A0S2;
        r4.A0C = A0N6;
        r4.A07 = A004;
        return r4.A00();
    }

    public static ObjectNode A01(CallToAction callToAction) {
        String uri;
        String uri2;
        String name;
        JsonNode jsonNode;
        ObjectNode objectNode;
        ObjectNode objectNode2;
        ObjectNode objectNode3;
        ObjectNode objectNode4;
        ObjectNode objectNode5;
        ObjectNode objectNode6 = new ObjectNode(JsonNodeFactory.instance);
        objectNode6.put("id", callToAction.A0B);
        objectNode6.put("action_title", callToAction.A0E);
        Uri uri3 = callToAction.A00;
        String str = null;
        if (uri3 == null) {
            uri = null;
        } else {
            uri = uri3.toString();
        }
        objectNode6.put("action_url", uri);
        Uri uri4 = callToAction.A01;
        if (uri4 == null) {
            uri2 = null;
        } else {
            uri2 = uri4.toString();
        }
        objectNode6.put("native_url", uri2);
        C16550xJ r0 = callToAction.A08;
        if (r0 == null) {
            name = null;
        } else {
            name = r0.name();
        }
        objectNode6.put("action_open_type", name);
        CallToActionTarget callToActionTarget = callToAction.A09;
        if (callToActionTarget != null) {
            jsonNode = callToActionTarget.C5a();
        } else {
            jsonNode = null;
        }
        objectNode6.put("action_object", jsonNode);
        objectNode6.put("is_mutable_by_server", callToAction.A0G);
        objectNode6.put("is_disabled", callToAction.A0F);
        objectNode6.put("is_post_handling_enabled", callToAction.A0H);
        CTAUserConfirmation cTAUserConfirmation = callToAction.A06;
        if (cTAUserConfirmation == null) {
            objectNode = null;
        } else {
            objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("confirmation_title", cTAUserConfirmation.A02);
            objectNode.put("confirmation_message", cTAUserConfirmation.A01);
            objectNode.put("continue_button_label", cTAUserConfirmation.A03);
            objectNode.put("cancel_button_label", cTAUserConfirmation.A00);
        }
        objectNode6.put("user_confirmation", objectNode);
        CTAInformationIdentify cTAInformationIdentify = callToAction.A04;
        if (cTAInformationIdentify == null) {
            objectNode2 = null;
        } else {
            objectNode2 = new ObjectNode(JsonNodeFactory.instance);
            objectNode2.put("form_url", cTAInformationIdentify.A05);
            objectNode2.put("form_id", cTAInformationIdentify.A03);
            objectNode2.put("form_color_theme", cTAInformationIdentify.A01);
            objectNode2.put("form_num_screens", cTAInformationIdentify.A04);
            objectNode2.put("form_current_screen_index", cTAInformationIdentify.A02);
            PIISinglePage pIISinglePage = cTAInformationIdentify.A00;
            if (pIISinglePage != null) {
                ObjectNode objectNode7 = new ObjectNode(JsonNodeFactory.instance);
                objectNode7.put("screen_title", pIISinglePage.A02);
                PIIPrivacy pIIPrivacy = pIISinglePage.A00;
                if (pIIPrivacy != null) {
                    ObjectNode objectNode8 = new ObjectNode(JsonNodeFactory.instance);
                    String str2 = pIIPrivacy.A00;
                    if (str2 != null) {
                        objectNode8.put("text", str2);
                    }
                    String str3 = pIISinglePage.A00.A01;
                    if (str3 != null) {
                        objectNode8.put("url", str3);
                    }
                    objectNode7.put("business_privacy", objectNode8);
                }
                ImmutableList immutableList = pIISinglePage.A01;
                if (immutableList != null) {
                    ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
                    C24971Xv it = immutableList.iterator();
                    while (it.hasNext()) {
                        PIIQuestion pIIQuestion = (PIIQuestion) it.next();
                        ObjectNode objectNode9 = new ObjectNode(JsonNodeFactory.instance);
                        objectNode9.put("id", pIIQuestion.A02);
                        Integer num = pIIQuestion.A01;
                        if (num != null) {
                            objectNode9.put("type", C187818nr.A01(num));
                        }
                        objectNode9.put("title", pIIQuestion.A07);
                        objectNode9.put("placeholder", pIIQuestion.A05);
                        objectNode9.put("subtitle", pIIQuestion.A06);
                        objectNode9.put("length", pIIQuestion.A03);
                        Integer num2 = pIIQuestion.A00;
                        if (num2 != null) {
                            objectNode9.put("format", C187598nT.A01(num2));
                        }
                        objectNode9.put("mask", pIIQuestion.A04);
                        arrayNode.add(objectNode9);
                    }
                    objectNode7.put("questions", arrayNode);
                }
            }
        }
        objectNode6.put("cta_data", objectNode2);
        MessengerWebViewParams messengerWebViewParams = callToAction.A02;
        if (messengerWebViewParams == null) {
            objectNode3 = null;
        } else {
            objectNode3 = new ObjectNode(JsonNodeFactory.instance);
            objectNode3.put("height_ratio", messengerWebViewParams.A00);
            objectNode3.put("hide_share_button", messengerWebViewParams.A08);
            objectNode3.put("browser_chrome_style", messengerWebViewParams.A01.dbValue);
        }
        objectNode6.put("platform_webview_param", objectNode3);
        CTAPaymentInfo cTAPaymentInfo = callToAction.A05;
        if (cTAPaymentInfo == null) {
            objectNode4 = null;
        } else {
            objectNode4 = new ObjectNode(JsonNodeFactory.instance);
            objectNode4.put("total_price", cTAPaymentInfo.A01);
            objectNode4.put("payment_module_config", cTAPaymentInfo.A00);
        }
        objectNode6.put("payment_metadata", objectNode4);
        PlatformRefParams platformRefParams = callToAction.A0A;
        if (platformRefParams == null) {
            objectNode5 = null;
        } else {
            objectNode5 = new ObjectNode(JsonNodeFactory.instance);
            objectNode5.put("postback_ref_code", platformRefParams.A00);
            objectNode5.put("postback_ref_code_source", platformRefParams.A01);
        }
        objectNode6.put("postback_ref", objectNode5);
        objectNode6.put(AnonymousClass24B.$const$string(73), callToAction.A0C);
        C30101hU r02 = callToAction.A07;
        if (r02 != null) {
            str = r02.name();
        }
        objectNode6.put("cta_render_style", str);
        return objectNode6;
    }

    public static String A03(List list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayNode.add(A01((CallToAction) it.next()));
        }
        return arrayNode.toString();
    }

    public static ImmutableList A02(String str) {
        if (C06850cB.A0B(str)) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        try {
            Iterator it = AnonymousClass0jE.getInstance().readTree(str).iterator();
            while (it.hasNext()) {
                CallToAction A00 = A00((JsonNode) it.next());
                if (A00 != null) {
                    builder.add((Object) A00);
                }
            }
            return builder.build();
        } catch (Exception unused) {
            return null;
        }
    }
}
