package X;

import com.facebook.common.jniexecutors.PooledNativeRunnable;

/* renamed from: X.1iD  reason: invalid class name and case insensitive filesystem */
public final class C30531iD extends C005806d {
    public void BO1(Object obj) {
        ((PooledNativeRunnable) obj).mNativeExecutor = null;
    }

    public void BlQ(Object obj) {
        ((PooledNativeRunnable) obj).mHybridData = null;
    }

    public Object create() {
        return new PooledNativeRunnable();
    }

    public C30531iD(Class cls) {
        super(cls);
    }
}
