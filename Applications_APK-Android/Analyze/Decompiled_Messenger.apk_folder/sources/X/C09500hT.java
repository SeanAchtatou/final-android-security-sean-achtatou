package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0hT  reason: invalid class name and case insensitive filesystem */
public final class C09500hT {
    public final C09510hU A00;
    public final C25611a7 A01;
    public final String A02;

    public C09500hT(C09510hU r1, C25611a7 r2, String str) {
        Preconditions.checkNotNull(r1);
        this.A00 = r1;
        Preconditions.checkNotNull(r2);
        this.A01 = r2;
        this.A02 = str;
    }
}
