package X;

import android.net.TrafficStats;
import android.os.DeadObjectException;
import android.os.StrictMode;
import com.facebook.common.util.TriState;
import com.facebook.device.resourcemonitor.DataUsageBytes;
import java.io.File;
import java.lang.reflect.Method;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0f9  reason: invalid class name */
public final class AnonymousClass0f9 {
    private static volatile AnonymousClass0f9 A02;
    private TriState A00 = TriState.UNSET;
    public final boolean A01;

    private static Method A01(AnonymousClass09P r3, Class cls, String str) {
        Method method;
        try {
            method = cls.getDeclaredMethod(str, new Class[0]);
        } catch (NoSuchMethodException unused) {
            method = null;
        }
        if (method == null) {
            r3.CGS(AnonymousClass08S.A0J("FbTrafficStats_missingMethod_", str), "Method not found.");
            return null;
        }
        method.setAccessible(true);
        return method;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0055, code lost:
        if (java.util.Arrays.asList(r0).contains(null) != false) goto L_0x006e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x009e A[Catch:{ RuntimeException -> 0x00a8, all -> 0x00b3 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.AnonymousClass0f9 A00(X.AnonymousClass1XY r9) {
        /*
            X.0f9 r0 = X.AnonymousClass0f9.A02
            if (r0 != 0) goto L_0x00c0
            java.lang.Class<X.0f9> r8 = X.AnonymousClass0f9.class
            monitor-enter(r8)
            X.0f9 r0 = X.AnonymousClass0f9.A02     // Catch:{ all -> 0x00bd }
            X.0WD r7 = X.AnonymousClass0WD.A00(r0, r9)     // Catch:{ all -> 0x00bd }
            if (r7 == 0) goto L_0x00bb
            X.1XY r0 = r9.getApplicationInjector()     // Catch:{ all -> 0x00b3 }
            X.0f9 r4 = new X.0f9     // Catch:{ all -> 0x00b3 }
            X.09P r6 = X.C04750Wa.A01(r0)     // Catch:{ all -> 0x00b3 }
            r5 = 0
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0059 }
            r0 = 16
            if (r1 < r0) goto L_0x0057
            r0 = 26
            if (r1 > r0) goto L_0x0057
            java.lang.Class<android.net.TrafficStats> r1 = android.net.TrafficStats.class
            java.lang.String r0 = "getStatsService"
            java.lang.reflect.Method r1 = A01(r6, r1, r0)     // Catch:{ all -> 0x0059 }
            if (r1 == 0) goto L_0x0057
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x0059 }
            r3 = 0
            java.lang.Object r2 = r1.invoke(r3, r0)     // Catch:{ all -> 0x0059 }
            if (r2 == 0) goto L_0x006d
            java.lang.Class r1 = r2.getClass()     // Catch:{ all -> 0x0059 }
            java.lang.String r0 = "getMobileIfaces"
            java.lang.reflect.Method r1 = A01(r6, r1, r0)     // Catch:{ all -> 0x0059 }
            if (r1 == 0) goto L_0x0057
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x0059 }
            java.lang.Object r0 = r1.invoke(r2, r0)     // Catch:{ all -> 0x0059 }
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x006e
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ all -> 0x0059 }
            boolean r0 = r0.contains(r3)     // Catch:{ all -> 0x0059 }
            if (r0 != 0) goto L_0x006e
        L_0x0057:
            r5 = 1
            goto L_0x006e
        L_0x0059:
            r0 = move-exception
            java.lang.String r1 = "FbTrafficStats_exception_"
            java.lang.Class r0 = r0.getClass()     // Catch:{ RuntimeException -> 0x00a8 }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ RuntimeException -> 0x00a8 }
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ RuntimeException -> 0x00a8 }
            java.lang.String r0 = "Exception in trustTrafficStatsToNotCrash."
            r6.CGS(r1, r0)     // Catch:{ RuntimeException -> 0x00a8 }
        L_0x006d:
            r5 = 0
        L_0x006e:
            if (r5 == 0) goto L_0x00ac
            long r1 = android.net.TrafficStats.getTotalRxBytes()     // Catch:{ RuntimeException -> 0x00a8 }
            r5 = -1
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00ac
            long r1 = android.net.TrafficStats.getTotalTxBytes()     // Catch:{ RuntimeException -> 0x00a8 }
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00ac
            long r1 = android.net.TrafficStats.getMobileRxBytes()     // Catch:{ RuntimeException -> 0x00a8 }
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00ac
            long r1 = android.net.TrafficStats.getMobileTxBytes()     // Catch:{ RuntimeException -> 0x00a8 }
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00ac
            int r3 = android.os.Process.myUid()     // Catch:{ RuntimeException -> 0x00a8 }
            long r1 = android.net.TrafficStats.getUidRxBytes(r3)     // Catch:{ RuntimeException -> 0x00a8 }
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00ac
            long r1 = android.net.TrafficStats.getUidTxBytes(r3)     // Catch:{ RuntimeException -> 0x00a8 }
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            r1 = 1
            if (r0 != 0) goto L_0x00ad
            goto L_0x00ac
        L_0x00a8:
            r0 = move-exception
            A02(r0)     // Catch:{ all -> 0x00b3 }
        L_0x00ac:
            r1 = 0
        L_0x00ad:
            r4.<init>(r1)     // Catch:{ all -> 0x00b3 }
            X.AnonymousClass0f9.A02 = r4     // Catch:{ all -> 0x00b3 }
            goto L_0x00b8
        L_0x00b3:
            r0 = move-exception
            r7.A01()     // Catch:{ all -> 0x00bd }
            throw r0     // Catch:{ all -> 0x00bd }
        L_0x00b8:
            r7.A01()     // Catch:{ all -> 0x00bd }
        L_0x00bb:
            monitor-exit(r8)     // Catch:{ all -> 0x00bd }
            goto L_0x00c0
        L_0x00bd:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00bd }
            throw r0
        L_0x00c0:
            X.0f9 r0 = X.AnonymousClass0f9.A02
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0f9.A00(X.1XY):X.0f9");
    }

    public static boolean A03(AnonymousClass0f9 r3) {
        TriState triState;
        if (r3.A00 == TriState.UNSET) {
            StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            try {
                if (new File("/proc/net/xt_qtaguid/stats").exists()) {
                    triState = TriState.YES;
                } else {
                    triState = TriState.NO;
                }
                r3.A00 = triState;
            } finally {
                StrictMode.setThreadPolicy(allowThreadDiskReads);
            }
        }
        if (r3.A00 == TriState.YES) {
            return true;
        }
        return false;
    }

    private AnonymousClass0f9(boolean z) {
        this.A01 = z;
    }

    public static void A02(RuntimeException runtimeException) {
        if (runtimeException.getCause() instanceof DeadObjectException) {
            C010708t.A0S("FbTrafficStats", runtimeException, "netstats connection lost");
            return;
        }
        throw runtimeException;
    }

    public DataUsageBytes A04(int i, int i2) {
        if (A03(this)) {
            try {
                DataUsageBytes A002 = C31921kn.A00(i, i2, 0);
                DataUsageBytes A003 = C31921kn.A00(i, i2, 1);
                return new DataUsageBytes(A002.A00 + A003.A00, A002.A01 + A003.A01);
            } catch (C37581vu unused) {
                return new DataUsageBytes(0, 0);
            }
        } else {
            try {
                return new DataUsageBytes(TrafficStats.getUidRxBytes(i), TrafficStats.getUidTxBytes(i));
            } catch (RuntimeException e) {
                A02(e);
                return new DataUsageBytes(0, 0);
            }
        }
    }
}
