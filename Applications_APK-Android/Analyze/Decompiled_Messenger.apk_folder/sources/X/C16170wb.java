package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0wb  reason: invalid class name and case insensitive filesystem */
public final class C16170wb implements C16190wd {
    public final /* synthetic */ RecyclerView A00;

    public C16170wb(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    private void A00(C21731Im r6) {
        int i = r6.A00;
        if (i == 1) {
            RecyclerView recyclerView = this.A00;
            recyclerView.A0L.A1M(recyclerView, r6.A02, r6.A01);
        } else if (i == 2) {
            RecyclerView recyclerView2 = this.A00;
            recyclerView2.A0L.A1N(recyclerView2, r6.A02, r6.A01);
        } else if (i == 4) {
            RecyclerView recyclerView3 = this.A00;
            recyclerView3.A0L.A1P(recyclerView3, r6.A02, r6.A01, r6.A03);
        } else if (i == 8) {
            RecyclerView recyclerView4 = this.A00;
            recyclerView4.A0L.A1O(recyclerView4, r6.A02, r6.A01, 1);
        }
    }

    public C33781o8 AZt(int i) {
        RecyclerView recyclerView = this.A00;
        int Ah4 = recyclerView.A0H.A01.Ah4();
        C33781o8 r4 = null;
        int i2 = 0;
        while (true) {
            if (i2 >= Ah4) {
                break;
            }
            C33781o8 A05 = RecyclerView.A05(recyclerView.A0H.A01.Ah1(i2));
            if (A05 != null && !A05.A0E() && A05.A04 == i) {
                C16220wh r0 = recyclerView.A0H;
                if (!r0.A02.contains(A05.A0H)) {
                    r4 = A05;
                    break;
                }
                r4 = A05;
            }
            i2++;
        }
        if (r4 != null) {
            C16220wh r02 = this.A00.A0H;
            if (!r02.A02.contains(r4.A0H)) {
                return r4;
            }
        }
        return null;
    }

    public void BK3(int i, int i2, Object obj) {
        int i3;
        int i4;
        RecyclerView recyclerView = this.A00;
        int Ah4 = recyclerView.A0H.A01.Ah4();
        int i5 = i + i2;
        for (int i6 = 0; i6 < Ah4; i6++) {
            View Ah1 = recyclerView.A0H.A01.Ah1(i6);
            C33781o8 A05 = RecyclerView.A05(Ah1);
            if (A05 != null && !A05.A0F() && (i4 = A05.A04) >= i && i4 < i5) {
                A05.A00 = 2 | A05.A00;
                A05.A09(obj);
                ((AnonymousClass1R7) Ah1.getLayoutParams()).A01 = true;
            }
        }
        C15740vp r4 = recyclerView.A0w;
        for (int size = r4.A05.size() - 1; size >= 0; size--) {
            C33781o8 r2 = (C33781o8) r4.A05.get(size);
            if (r2 != null && (i3 = r2.A04) >= i && i3 < i5) {
                r2.A00 = 2 | r2.A00;
                C15740vp.A02(r4, size);
            }
        }
        this.A00.A06 = true;
    }

    public void BMo(int i, int i2) {
        RecyclerView recyclerView = this.A00;
        int Ah4 = recyclerView.A0H.A01.Ah4();
        for (int i3 = 0; i3 < Ah4; i3++) {
            C33781o8 A05 = RecyclerView.A05(recyclerView.A0H.A01.Ah1(i3));
            if (A05 != null && !A05.A0F() && A05.A04 >= i) {
                A05.A08(i2, false);
                recyclerView.A0y.A0C = true;
            }
        }
        C15740vp r4 = recyclerView.A0w;
        int size = r4.A05.size();
        for (int i4 = 0; i4 < size; i4++) {
            C33781o8 r1 = (C33781o8) r4.A05.get(i4);
            if (r1 != null && r1.A04 >= i) {
                r1.A08(i2, true);
            }
        }
        recyclerView.requestLayout();
        this.A00.A05 = true;
    }

    public void BMp(int i, int i2) {
        int i3;
        int i4;
        RecyclerView recyclerView = this.A00;
        int Ah4 = recyclerView.A0H.A01.Ah4();
        int i5 = i;
        int i6 = i2;
        int i7 = 1;
        if (i < i2) {
            i6 = i;
            i5 = i2;
            i7 = -1;
        }
        for (int i8 = 0; i8 < Ah4; i8++) {
            C33781o8 A05 = RecyclerView.A05(recyclerView.A0H.A01.Ah1(i8));
            if (A05 != null && (i4 = A05.A04) >= i6 && i4 <= i5) {
                if (i4 == i) {
                    A05.A08(i2 - i, false);
                } else {
                    A05.A08(i7, false);
                }
                recyclerView.A0y.A0C = true;
            }
        }
        C15740vp r8 = recyclerView.A0w;
        int i9 = i;
        int i10 = i2;
        int i11 = 1;
        if (i < i2) {
            i10 = i;
            i9 = i2;
            i11 = -1;
        }
        int size = r8.A05.size();
        for (int i12 = 0; i12 < size; i12++) {
            C33781o8 r1 = (C33781o8) r8.A05.get(i12);
            if (r1 != null && (i3 = r1.A04) >= i10 && i3 <= i9) {
                if (i3 == i) {
                    r1.A08(i2 - i, false);
                } else {
                    r1.A08(i11, false);
                }
            }
        }
        recyclerView.requestLayout();
        this.A00.A05 = true;
    }

    public void BMq(int i, int i2) {
        this.A00.A0s(i, i2, true);
        RecyclerView recyclerView = this.A00;
        recyclerView.A05 = true;
        recyclerView.A0y.A00 += i2;
    }

    public void BMr(int i, int i2) {
        this.A00.A0s(i, i2, false);
        this.A00.A05 = true;
    }

    public void BWe(C21731Im r1) {
        A00(r1);
    }

    public void BWf(C21731Im r1) {
        A00(r1);
    }
}
