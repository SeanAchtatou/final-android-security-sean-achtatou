package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1CM  reason: invalid class name */
public final class AnonymousClass1CM implements AnonymousClass1CR {
    private static volatile AnonymousClass1CM A01;
    public final C25051Yd A00;

    public static final AnonymousClass1CM A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1CM.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1CM(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public boolean Aen(long j, String str) {
        return this.A00.Aem(j);
    }

    public Integer Aj3() {
        return this.A00.Aj4(false);
    }

    public int AqM(long j, String str) {
        return (int) this.A00.At0(j);
    }

    public String B4E(long j, String str) {
        return this.A00.B4C(j);
    }

    private AnonymousClass1CM(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
