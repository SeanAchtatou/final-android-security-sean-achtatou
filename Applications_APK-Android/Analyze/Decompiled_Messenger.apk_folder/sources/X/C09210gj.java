package X;

/* renamed from: X.0gj  reason: invalid class name and case insensitive filesystem */
public abstract class C09210gj {
    public long A00(int i, long j) {
        if (!(this instanceof C37081ue)) {
            return ((C09200gi) this).A00.get(i);
        }
        C37081ue r2 = (C37081ue) this;
        int indexOfKey = r2.A01.indexOfKey(i);
        if (indexOfKey < 0) {
            return j;
        }
        return (((long) r2.A01.valueAt(indexOfKey)) & 4294967295L) | (((long) r2.A00.valueAt(indexOfKey)) << 32);
    }

    public void A01(int i, long j) {
        if (!(this instanceof C37081ue)) {
            ((C09200gi) this).A00.put(i, j);
            return;
        }
        C37081ue r2 = (C37081ue) this;
        r2.A01.put(i, (int) j);
        r2.A00.put(i, (int) (j >>> 32));
    }
}
