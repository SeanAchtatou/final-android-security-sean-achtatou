package X;

/* renamed from: X.182  reason: invalid class name */
public final class AnonymousClass182 {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("first_name", "TEXT");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6("last_fetch_time_ms", "INTEGER");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("last_name", "TEXT");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("name", "TEXT");
    public static final AnonymousClass0W6 A04 = new AnonymousClass0W6("profile_pic_square", "TEXT");
    public static final AnonymousClass0W6 A05 = new AnonymousClass0W6("user_fbid", "INTEGER");
}
