package X;

import com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import io.card.payment.BuildConfig;

/* renamed from: X.1jb  reason: invalid class name and case insensitive filesystem */
public abstract class C31381jb {
    public Object A00(Object obj, RankingLoggingItem rankingLoggingItem) {
        if (!(this instanceof C32381lf)) {
            return null;
        }
        C17920zh A00 = ThreadSummary.A00();
        A00.A02((ThreadSummary) obj);
        String str = ((C32381lf) this).A00.A01;
        if (str == null) {
            str = BuildConfig.FLAVOR;
        }
        A00.A0r = str;
        return A00.A00();
    }

    public String A01(Object obj) {
        return !(this instanceof C32381lf) ? ((BasicMontageThreadInfo) obj).A03.id : ((ThreadSummary) obj).A0S.A0I();
    }
}
