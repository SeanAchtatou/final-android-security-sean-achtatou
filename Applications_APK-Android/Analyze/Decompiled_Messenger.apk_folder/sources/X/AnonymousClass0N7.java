package X;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/* renamed from: X.0N7  reason: invalid class name */
public final class AnonymousClass0N7 implements AnonymousClass01J {
    private static final String[] A02 = {"VmHWM:", "VmRSS:"};
    private long A00;
    private AnonymousClass0S0 A01 = AnonymousClass0N0.A00;

    public boolean ANg(Writer writer, AnonymousClass0HM r22) {
        List<AnonymousClass0N1> emptyList;
        long j;
        AnonymousClass0HM r1 = r22;
        if (r22 != null) {
            r1.onCodePoint$REDEX$yYf5PajrCI7(AnonymousClass07B.A0C);
        }
        StringBuilder sb = new StringBuilder("\"");
        StringBuilder sb2 = new StringBuilder("\"");
        StringBuilder sb3 = new StringBuilder("\"");
        long[] jArr = new long[A02.length];
        synchronized (this.A01) {
            emptyList = Collections.emptyList();
        }
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        for (AnonymousClass0N1 r13 : emptyList) {
            AnonymousClass00V.A02(AnonymousClass08S.A0A("/proc/", r13.A00, "/status"), A02, jArr);
            j2 += jArr[0];
            j3 += jArr[1];
            try {
                Scanner scanner = new Scanner(new File(AnonymousClass08S.A0A("/proc/", r13.A00, "/statm")));
                scanner.nextLong();
                j = (scanner.nextLong() * 4) - (scanner.nextLong() * 4);
            } catch (IOException unused) {
                j = 0;
            }
            j4 += j;
            String str = r13.A01;
            sb.append(str);
            sb.append(':');
            sb.append(jArr[0]);
            sb.append(' ');
            sb2.append(str);
            sb2.append(':');
            sb2.append(jArr[1]);
            sb2.append(' ');
            sb3.append(str);
            sb3.append(':');
            sb3.append(j);
            sb3.append(' ');
        }
        sb2.append('\"');
        sb.append('\"');
        sb3.append('\"');
        Writer writer2 = writer;
        writer2.append((CharSequence) "\"totalPeakRssAllAppProcessesKB\":");
        writer2.append((CharSequence) Long.toString(j2));
        writer2.append((CharSequence) ",\"totalCurrentRssAllAppProcessesKB\":");
        writer2.append((CharSequence) Long.toString(j3));
        writer2.append((CharSequence) ",\"totalCurrentResidentAnonymousAllAppProcessesKB\":");
        writer2.append((CharSequence) Long.toString(j4));
        if (j4 > this.A00) {
            this.A00 = j4;
        }
        writer2.append((CharSequence) ",\"totalPeakResidentAnonymousAllAppProcessesKB\":");
        writer2.append((CharSequence) Long.toString(this.A00));
        writer2.append((CharSequence) ",\"perAppProcessPeakRssKB\":");
        writer2.append((CharSequence) sb);
        writer2.append((CharSequence) ",\"perAppProcessCurrentRssKB\":");
        writer2.append((CharSequence) sb2);
        writer2.append((CharSequence) ",\"perAppProcessResidentAnonymousKB\":");
        writer2.append((CharSequence) sb3);
        return true;
    }
}
