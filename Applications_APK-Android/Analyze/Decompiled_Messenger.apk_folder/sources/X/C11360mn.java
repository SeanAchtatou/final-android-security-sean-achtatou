package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0mn  reason: invalid class name and case insensitive filesystem */
public final class C11360mn {
    private static volatile C11360mn A00;

    public static final C11360mn A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C11360mn.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C11360mn();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
