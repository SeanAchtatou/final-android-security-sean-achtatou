package X;

/* renamed from: X.0Ym  reason: invalid class name and case insensitive filesystem */
public final class C05320Ym {
    public final int A00;
    public final String A01;

    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null || !(obj instanceof C05320Ym)) {
            return false;
        }
        C05320Ym r4 = (C05320Ym) obj;
        if (this.A00 != r4.A00) {
            return false;
        }
        String str = this.A01;
        String str2 = r4.A01;
        if (str == null) {
            equals = false;
            if (str2 == null) {
                equals = true;
            }
        } else {
            equals = str.equals(str2);
        }
        return equals;
    }

    public int hashCode() {
        int i;
        int i2 = (this.A00 + 31) * 31;
        String str = this.A01;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        return i2 + i;
    }

    public String toString() {
        return "[" + getClass().getSimpleName() + " id: " + this.A00 + " type: " + this.A01 + " ]";
    }

    public C05320Ym(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }
}
