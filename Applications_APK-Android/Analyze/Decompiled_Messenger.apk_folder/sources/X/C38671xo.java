package X;

import com.google.common.collect.ImmutableMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1xo  reason: invalid class name and case insensitive filesystem */
public final class C38671xo implements C04240Tb {
    private static volatile C38671xo A01;
    private final AnonymousClass4LX A00;

    public ImmutableMap AmJ() {
        return null;
    }

    public String getName() {
        return "InboxBadgeCountUpdate";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    public static final C38671xo A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C38671xo.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C38671xo(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public ImmutableMap AmI() {
        C861348y r5;
        ImmutableMap copyOf;
        ImmutableMap copyOf2;
        AnonymousClass4LX r1 = this.A00;
        synchronized (r1) {
            r5 = r1.A00;
        }
        AnonymousClass4LX r12 = this.A00;
        synchronized (r12) {
            copyOf = ImmutableMap.copyOf(r12.A03);
        }
        AnonymousClass4LX r13 = this.A00;
        synchronized (r13) {
            copyOf2 = ImmutableMap.copyOf(r13.A02);
        }
        StringBuilder sb = new StringBuilder("\n");
        if (r5 != null) {
            sb.append("  sent: ");
            sb.append(r5);
            sb.append("\n");
        }
        if (!copyOf.isEmpty()) {
            sb.append("  received:\n");
            A01(sb, copyOf);
        }
        if (!copyOf2.isEmpty()) {
            sb.append("  queried:\n");
            A01(sb, copyOf2);
        }
        return ImmutableMap.of("InboxBadgeCountUpdate", sb.toString());
    }

    private C38671xo(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass4LX.A00(r2);
    }

    private static void A01(StringBuilder sb, ImmutableMap immutableMap) {
        C24971Xv it = immutableMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            sb.append("    ");
            sb.append((String) entry.getKey());
            sb.append(" : ");
            sb.append(entry.getValue());
            sb.append("\n");
        }
    }
}
