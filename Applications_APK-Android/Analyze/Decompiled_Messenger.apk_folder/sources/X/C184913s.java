package X;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import com.facebook.resources.compat.RedexResourcesCompat;

/* renamed from: X.13s  reason: invalid class name and case insensitive filesystem */
public final class C184913s {
    public static int A00 = -1;

    public static int A00(Resources resources) {
        int dimensionPixelSize;
        int i = A00;
        boolean z = false;
        if (i > 0) {
            z = true;
        }
        if (z) {
            return i;
        }
        int identifier = RedexResourcesCompat.getIdentifier(resources, AnonymousClass80H.$const$string(3), AnonymousClass24B.$const$string(18), "android");
        if (identifier <= 0) {
            dimensionPixelSize = 0;
        } else {
            dimensionPixelSize = resources.getDimensionPixelSize(identifier);
        }
        if (Build.VERSION.SDK_INT < 20) {
            A00 = dimensionPixelSize;
        }
        return dimensionPixelSize;
    }

    public static int A01(Resources resources, Window window) {
        int i = A00;
        boolean z = false;
        if (i > 0) {
            z = true;
        }
        if (!z) {
            int A002 = A00(resources);
            if (A002 > 0) {
                return A002;
            }
            i = A00;
            boolean z2 = false;
            if (i > 0) {
                z2 = true;
            }
            if (!z2) {
                Rect rect = new Rect();
                window.getDecorView().getWindowVisibleDisplayFrame(rect);
                i = rect.top;
            }
            if (i <= 0) {
                return (int) TypedValue.applyDimension(1, 25.0f, resources.getDisplayMetrics());
            }
        }
        return i;
    }

    public static void A02(Window window) {
        if (C012709o.A00(16)) {
            A03(window);
            return;
        }
        window.clearFlags(2048);
        window.addFlags(1024);
    }

    public static void A03(Window window) {
        boolean A002 = C012709o.A00(19);
        int i = AnonymousClass1Y3.AAX;
        if (A002) {
            i = AnonymousClass1Y3.Ain;
        }
        window.getDecorView().setSystemUiVisibility(i);
    }

    public static void A04(Window window, int i) {
        if (C012709o.A00(21)) {
            C37141ul.A00(window, i);
        }
    }

    public static void A05(Window window, int i) {
        if (C012709o.A00(16)) {
            View decorView = window.getDecorView();
            if (C012709o.A00(16)) {
                decorView.setSystemUiVisibility(i);
                return;
            }
            return;
        }
        window.getAttributes().flags = i;
    }

    public static void A06(Window window, int i) {
        if (C012709o.A00(21)) {
            A07(window, i);
        }
    }

    public static void A07(Window window, int i) {
        window.addFlags(Integer.MIN_VALUE);
        window.setStatusBarColor(i);
    }

    public static void A08(Window window, boolean z) {
        if (!C012709o.A00(19)) {
            return;
        }
        if (z) {
            window.addFlags(67108864);
        } else {
            window.clearFlags(67108864);
        }
    }

    public static boolean A09(Window window) {
        if (C012709o.A00(16)) {
            return A0A(window);
        }
        int i = window.getAttributes().flags & 2048;
        int i2 = window.getAttributes().flags & 1024;
        if (i != 0 || i2 == 0) {
            return true;
        }
        return false;
    }

    public static boolean A0A(Window window) {
        if ((window.getDecorView().getSystemUiVisibility() & 4) == 0) {
            return true;
        }
        return false;
    }
}
