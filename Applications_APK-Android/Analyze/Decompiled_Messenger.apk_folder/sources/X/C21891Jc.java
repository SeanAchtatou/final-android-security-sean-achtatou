package X;

/* renamed from: X.1Jc  reason: invalid class name and case insensitive filesystem */
public final class C21891Jc {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r11 == X.C22111Jy.A05) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C17770zR A00(X.AnonymousClass0p4 r9, X.AnonymousClass1J0 r10, X.C22111Jy r11, int r12) {
        /*
            if (r12 != 0) goto L_0x0004
            r0 = 0
            return r0
        L_0x0004:
            X.1Jy r0 = X.C22111Jy.A04
            r2 = 0
            if (r11 == r0) goto L_0x000e
            X.1Jy r1 = X.C22111Jy.A05
            r0 = 1
            if (r11 != r1) goto L_0x000f
        L_0x000e:
            r0 = 0
        L_0x000f:
            if (r0 == 0) goto L_0x008c
            int r0 = r10.A05
            int r0 = r0 >> 1
            float r2 = (float) r0
            int r0 = r10.A04
            int r0 = r0 >> 1
            float r1 = (float) r0
            float r0 = r10.A01
            float r1 = r1 + r0
            double r0 = (double) r1
            double r2 = (double) r2
            r4 = r2
            float r3 = X.C24271Sy.A00(r0, r2, r4)
            r0 = 1110704128(0x42340000, float:45.0)
            float r0 = r0 + r3
            int r2 = (int) r0
            r1 = 1135869952(0x43b40000, float:360.0)
            r0 = 1073741824(0x40000000, float:2.0)
            float r3 = r3 * r0
            float r1 = r1 - r3
            int r7 = (int) r1
        L_0x0030:
            r1 = 1
            java.lang.String r0 = "ringColor"
            java.lang.String[] r5 = new java.lang.String[]{r0}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r1)
            X.36a r3 = new X.36a
            android.content.Context r0 = r9.A09
            r3.<init>(r0)
            X.0z4 r6 = r9.A0B
            X.0zR r0 = r9.A04
            if (r0 == 0) goto L_0x004d
            java.lang.String r1 = r0.A06
            r3.A07 = r1
        L_0x004d:
            r4.clear()
            r3.A00 = r12
            r0 = 0
            r4.set(r0)
            float r0 = r10.A03
            int r0 = r6.A00(r0)
            r3.A01 = r0
            r3.A02 = r2
            r3.A03 = r7
            X.1LC r1 = X.AnonymousClass1LC.A00
            X.11G r0 = r3.A14()
            r0.BxJ(r1)
            X.10G r2 = X.AnonymousClass10G.LEFT
            r0 = 0
            int r1 = r6.A00(r0)
            X.11G r0 = r3.A14()
            r0.BxI(r2, r1)
            X.10G r2 = X.AnonymousClass10G.TOP
            r0 = 0
            int r1 = r6.A00(r0)
            X.11G r0 = r3.A14()
            r0.BxI(r2, r1)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x008c:
            r7 = 360(0x168, float:5.04E-43)
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21891Jc.A00(X.0p4, X.1J0, X.1Jy, int):X.0zR");
    }
}
