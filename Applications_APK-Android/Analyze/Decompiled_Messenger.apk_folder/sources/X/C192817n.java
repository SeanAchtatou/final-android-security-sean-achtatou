package X;

import android.view.Choreographer;

/* renamed from: X.17n  reason: invalid class name and case insensitive filesystem */
public final class C192817n implements Choreographer.FrameCallback {
    public final /* synthetic */ AnonymousClass088 A00;

    public C192817n(AnonymousClass088 r1) {
        this.A00 = r1;
    }

    public void doFrame(long j) {
        Choreographer choreographer;
        AnonymousClass088 r5 = this.A00;
        if (!r5.A03) {
            r5.A04.removeFrameCallback(this);
            return;
        }
        if (r5.A00 == -1) {
            r5.A00 = j;
            r5.A01 = j;
            choreographer = r5.A04;
        } else {
            long j2 = j - r5.A01;
            r5.A01 = j;
            AnonymousClass05U r0 = r5.A02;
            if (r0 != null) {
                r0.BZp(j2);
            }
            choreographer = this.A00.A04;
        }
        choreographer.postFrameCallback(this);
    }
}
