package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0L9  reason: invalid class name */
public final class AnonymousClass0L9<A, B> extends LinkedHashMap<A, B> {
    private final int maxEntries;

    public AnonymousClass0L9(int i) {
        super(i + 1, 1.0f, true);
        this.maxEntries = i;
    }

    public boolean removeEldestEntry(Map.Entry entry) {
        if (size() > this.maxEntries) {
            return true;
        }
        return false;
    }
}
