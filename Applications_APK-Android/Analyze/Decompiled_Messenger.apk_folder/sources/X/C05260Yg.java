package X;

/* renamed from: X.0Yg  reason: invalid class name and case insensitive filesystem */
public final class C05260Yg {
    public static String A00(Class cls) {
        String name = cls.getName();
        StringBuilder sb = new StringBuilder(11 + name.length());
        sb.append("<cls>");
        sb.append(name);
        sb.append("</cls>");
        return sb.toString();
    }
}
