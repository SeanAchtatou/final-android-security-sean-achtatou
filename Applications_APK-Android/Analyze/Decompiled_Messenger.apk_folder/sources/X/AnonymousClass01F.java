package X;

import android.content.Context;
import android.os.Build;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.01F  reason: invalid class name */
public final class AnonymousClass01F extends AnonymousClass01G {
    private static volatile AnonymousClass01F A03;
    private AnonymousClass01H A00 = new AnonymousClass01H();
    private AnonymousClass02G A01;
    private List A02 = new ArrayList();

    public C001701e A07(Context context) {
        int A002 = AnonymousClass08X.A00(context, "app_state_log_write_policy", -1);
        if (A002 == -1) {
            return new AnonymousClass0NC();
        }
        return new AnonymousClass01Z(A002);
    }

    public boolean A0Q(Context context) {
        if (AnonymousClass08X.A00(context, "android_foreground_app_death_logging", 0) != 1) {
            return false;
        }
        return true;
    }

    public static AnonymousClass01F A00() {
        if (A03 == null) {
            synchronized (AnonymousClass01F.class) {
                if (A03 == null) {
                    A03 = new AnonymousClass01F();
                }
            }
        }
        return A03;
    }

    public int A01(Context context) {
        return AnonymousClass08X.A00(context, "foreground_state_initialization_policy", 0);
    }

    public int A02(Context context) {
        return AnonymousClass08X.A00(context, "time_between_importance_queries", 0);
    }

    public int A03(Context context) {
        return AnonymousClass08X.A00(context, "app_state_file_writing_maximum_time_between_writes_background_ms", 0);
    }

    public int A04(Context context) {
        return AnonymousClass08X.A00(context, "app_state_file_writing_maximum_time_between_writes_foreground_ms", 30000);
    }

    public int A05(Context context) {
        return AnonymousClass08X.A00(context, "app_state_file_writing_minimum_time_between_writes_ms", AnonymousClass1Y3.A87);
    }

    public int A06(Context context) {
        return AnonymousClass08X.A00(context, "app_state_report_healthy_app_state_rate", 0);
    }

    public AnonymousClass02G A08(File file) {
        if (this.A01 == null) {
            this.A01 = new AnonymousClass02G(file);
        }
        return this.A01;
    }

    public boolean A0B(Context context) {
        return AnonymousClass08X.A08(context, "app_state_join_logger_thread_on_device_shutdown", false);
    }

    public boolean A0C(Context context) {
        return AnonymousClass08X.A07(context, "monitor_home_task_switcher_event");
    }

    public boolean A0D(Context context) {
        return AnonymousClass08X.A07(context, "monitor_process_importance");
    }

    public boolean A0E(Context context) {
        return AnonymousClass08X.A07(context, "write_process_importance_field");
    }

    public boolean A0F(Context context) {
        return AnonymousClass08X.A07(context, "app_state_log_self_sigkill");
    }

    public boolean A0G(Context context) {
        return AnonymousClass08X.A07(context, "app_state_log_uncaught_exceptions");
    }

    public boolean A0H(Context context) {
        return AnonymousClass08X.A07(context, "app_state_log_vm_oom");
    }

    public boolean A0I(Context context) {
        return AnonymousClass08X.A07(context, "app_state_file_writing_non_critical_writes_lower_priority");
    }

    public boolean A0J(Context context) {
        return AnonymousClass08X.A07(context, "app_state_native_late_init");
    }

    public boolean A0K(Context context) {
        return AnonymousClass08X.A07(context, "log_finalizer_latency");
    }

    public boolean A0L(Context context) {
        return AnonymousClass08X.A08(context, "monitor_native_library", false);
    }

    public boolean A0M(Context context) {
        return AnonymousClass08X.A07(context, "monitor_pending_launches");
    }

    public boolean A0N(Context context) {
        return AnonymousClass08X.A07(context, "monitor_pending_stops");
    }

    public boolean A0O(Context context) {
        return AnonymousClass08X.A07(context, "report_all_process_memory_usage");
    }

    public boolean A0P(Context context) {
        return AnonymousClass08X.A07(context, "android_background_app_death_logging");
    }

    public boolean A0R(Context context) {
        return AnonymousClass08X.A07(context, "app_state_report_healthy_app_state");
    }

    public boolean A0S(Context context) {
        return AnonymousClass08X.A07(context, "app_state_log_private_dirty_mem_usage");
    }

    public boolean A0T(Context context) {
        if (Build.VERSION.SDK_INT >= 21) {
            return true;
        }
        return false;
    }

    public boolean A0U(Context context) {
        return AnonymousClass08X.A07(context, "app_state_log_serialize_fg_anr_info");
    }

    private AnonymousClass01F() {
    }

    public List A09() {
        return this.A02;
    }

    public void A0A(Runnable runnable) {
        AnonymousClass016.A05 = runnable;
    }
}
