package X;

import com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet;
import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.1TJ  reason: invalid class name */
public final class AnonymousClass1TJ implements C20021Ap {
    public final /* synthetic */ AnonymousClass16R A00;

    public AnonymousClass1TJ(AnonymousClass16R r1) {
        this.A00 = r1;
    }

    public void BdJ(Object obj, Object obj2) {
        MessageRequestsSnippet messageRequestsSnippet = (MessageRequestsSnippet) obj2;
        AnonymousClass16R r2 = this.A00;
        if (messageRequestsSnippet == null || messageRequestsSnippet.A02 <= 0) {
            messageRequestsSnippet = null;
        }
        r2.A0D = messageRequestsSnippet;
        AnonymousClass16R.A04(r2, AnonymousClass07B.A0n, "MessageRequests");
    }

    public void Bd1(Object obj, Object obj2) {
    }

    public void BdU(Object obj, ListenableFuture listenableFuture) {
    }

    public void Bgh(Object obj, Object obj2) {
    }
}
