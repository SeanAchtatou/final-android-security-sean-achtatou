package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0Ez  reason: invalid class name and case insensitive filesystem */
public final class C02480Ez extends AnonymousClass0UV {
    private static volatile AnonymousClass0F1 A00;

    public static final AnonymousClass0F1 A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass0F1.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        AnonymousClass0F2.A00(r3.getApplicationInjector());
                        A00 = new AnonymousClass0F0();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
