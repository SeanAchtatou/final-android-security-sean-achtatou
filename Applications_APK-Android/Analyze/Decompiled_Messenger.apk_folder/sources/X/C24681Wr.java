package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1Wr  reason: invalid class name and case insensitive filesystem */
public final class C24681Wr extends AnonymousClass1WU {
    public final AnonymousClass1WU A00;
    public final Set A01;
    public final Set A02;
    public final Set A03;
    public final Set A04;
    public final Set A05;

    public C24681Wr(AnonymousClass1WX r10, AnonymousClass1WU r11) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        HashSet hashSet3 = new HashSet();
        HashSet hashSet4 = new HashSet();
        for (C24601Wf r7 : r10.A03) {
            if (r7.A00 == 0) {
                if (r7.A01 == 2) {
                    hashSet3.add(r7.A02);
                } else {
                    hashSet.add(r7.A02);
                }
            } else {
                if (r7.A01 == 2) {
                    hashSet4.add(r7.A02);
                } else {
                    hashSet2.add(r7.A02);
                }
            }
        }
        if (!r10.A05.isEmpty()) {
            hashSet.add(AnonymousClass1Wi.class);
        }
        this.A01 = Collections.unmodifiableSet(hashSet);
        this.A02 = Collections.unmodifiableSet(hashSet2);
        this.A04 = Collections.unmodifiableSet(hashSet3);
        this.A05 = Collections.unmodifiableSet(hashSet4);
        this.A03 = r10.A05;
        this.A00 = r11;
    }
}
