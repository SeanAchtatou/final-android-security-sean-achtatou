package X;

import io.card.payment.BuildConfig;

/* renamed from: X.10N  reason: invalid class name */
public class AnonymousClass10N {
    public C17810zV A00;
    public final int A01;
    public final Object[] A02;

    public boolean A01(AnonymousClass10N r6) {
        if (this != r6) {
            if (r6 != null && r6.getClass() == getClass() && this.A01 == r6.A01) {
                Object[] objArr = this.A02;
                Object[] objArr2 = r6.A02;
                if (objArr != objArr2) {
                    if (objArr != null && objArr2 != null && objArr.length == objArr2.length) {
                        int i = 1;
                        while (true) {
                            Object[] objArr3 = this.A02;
                            if (i >= objArr3.length) {
                                break;
                            }
                            Object obj = objArr3[i];
                            Object obj2 = r6.A02[i];
                            if (obj == null) {
                                if (obj2 != null) {
                                    break;
                                }
                            } else if (!obj.equals(obj2)) {
                                break;
                            }
                            i++;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public void A00(Object obj) {
        String str;
        String str2;
        if (!(this instanceof AnonymousClass1TB)) {
            this.A00.Alq().AXa(this, obj);
            return;
        }
        C1293464k r7 = (C1293464k) obj;
        AnonymousClass1AC r5 = (AnonymousClass1AC) ((AnonymousClass1TB) this).A00.get();
        if (r5 != null) {
            Integer num = r7.A00;
            C58142tE r2 = r5.A01;
            if (r2 != null) {
                boolean z = r7.A02;
                switch (num.intValue()) {
                    case 0:
                        r2.A01();
                        break;
                    case 1:
                        r2.A03(z);
                        break;
                    case 2:
                        r2.A04(z);
                        break;
                    case 3:
                        r2.A02(z);
                        break;
                }
            }
            if (C191216w.A00()) {
                AnonymousClass1AC.A0I(r5, num);
                return;
            }
            if (r5.A0B.BHG()) {
                switch (num.intValue()) {
                    case 1:
                        str2 = "LOADING";
                        break;
                    case 2:
                        str2 = C99084oO.$const$string(38);
                        break;
                    case 3:
                        str2 = "FAILED";
                        break;
                    default:
                        str2 = "INITIAL_LOAD";
                        break;
                }
                str = AnonymousClass08S.A0S("SectionTree.postLoadingStateToFocusDispatch - ", str2, " - ", r5.A0G);
            } else {
                str = BuildConfig.FLAVOR;
            }
            r5.A0B.BxL(new C30424Evz(r5, num), str);
        }
    }

    public AnonymousClass10N(C17810zV r1, int i, Object[] objArr) {
        this.A00 = r1;
        this.A01 = i;
        this.A02 = objArr;
    }
}
