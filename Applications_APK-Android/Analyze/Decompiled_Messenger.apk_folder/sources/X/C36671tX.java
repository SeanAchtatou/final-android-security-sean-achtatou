package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/* renamed from: X.1tX  reason: invalid class name and case insensitive filesystem */
public class C36671tX {
    public C36691tZ A00;
    public AnonymousClass1Y7 A01;
    public Class A02 = null;
    public Map A03 = null;
    public final DeprecatedAnalyticsLogger A04;
    public final AnonymousClass06B A05;
    public final FbSharedPreferences A06;
    public final AnonymousClass0jJ A07;

    private synchronized void A01() {
        long j;
        if (A02(this)) {
            try {
                int i = this.A00.A00;
                Map map = this.A03;
                if (map == null || map.size() > i) {
                    int i2 = this.A00.A01;
                    AnonymousClass06B r0 = this.A05;
                    if (r0 == null) {
                        j = Long.MAX_VALUE;
                    } else {
                        j = r0.now();
                    }
                    long j2 = j - (((long) i2) * 3600000);
                    PriorityQueue priorityQueue = new PriorityQueue(i, new C159177Yy());
                    for (Map.Entry entry : this.A03.entrySet()) {
                        if (((C36711tb) entry.getValue()).A07 >= j2) {
                            priorityQueue.offer(entry);
                            if (priorityQueue.size() > i) {
                                priorityQueue.poll();
                            }
                        }
                    }
                    this.A03.clear();
                    Iterator it = priorityQueue.iterator();
                    while (it.hasNext()) {
                        Map.Entry entry2 = (Map.Entry) it.next();
                        this.A03.put(entry2.getKey(), entry2.getValue());
                    }
                }
                HashMap hashMap = new HashMap();
                for (Map.Entry entry3 : this.A03.entrySet()) {
                    hashMap.put(entry3.getKey(), this.A07.writeValueAsString(entry3.getValue()));
                }
                String writeValueAsString = this.A07.writeValueAsString(hashMap);
                C30281hn edit = this.A06.edit();
                edit.BzC(this.A01, writeValueAsString);
                edit.commit();
            } catch (Throwable th) {
                C11670nb r1 = new C11670nb(this.A00.A02);
                r1.A0C("exception", th);
                DeprecatedAnalyticsLogger deprecatedAnalyticsLogger = this.A04;
                if (deprecatedAnalyticsLogger != null) {
                    deprecatedAnalyticsLogger.A09(r1);
                }
                C30281hn edit2 = this.A06.edit();
                edit2.C1B(this.A01);
                edit2.commit();
            }
        }
    }

    public boolean A07() {
        String str;
        return (this instanceof C36661tW) && (str = C36651tV.A04) != null && str.equals(new TimeoutException().toString());
    }

    public static boolean A02(C36671tX r7) {
        HashMap hashMap;
        if (!r7.A00.A05 || r7.A06 == null) {
            return false;
        }
        if (r7.A03 == null) {
            synchronized (r7) {
                FbSharedPreferences fbSharedPreferences = r7.A06;
                if (fbSharedPreferences == null || !fbSharedPreferences.BFQ()) {
                    hashMap = new HashMap();
                } else {
                    String B4F = r7.A06.B4F(r7.A01, null);
                    if (B4F == null || B4F.equals("{}")) {
                        hashMap = new HashMap();
                    } else {
                        try {
                            hashMap = new HashMap();
                            for (Map.Entry entry : ((Map) r7.A07.readValue(B4F, new AnonymousClass57G(r7))).entrySet()) {
                                hashMap.put(entry.getKey(), (C36711tb) r7.A07.readValue((String) entry.getValue(), r7.A02));
                            }
                        } catch (IOException | ClassCastException e) {
                            C11670nb r1 = new C11670nb(r7.A00.A02);
                            r1.A0C("exception", e);
                            DeprecatedAnalyticsLogger deprecatedAnalyticsLogger = r7.A04;
                            if (deprecatedAnalyticsLogger != null) {
                                deprecatedAnalyticsLogger.A09(r1);
                            }
                            C30281hn edit = r7.A06.edit();
                            edit.C1B(r7.A01);
                            edit.commit();
                            hashMap = new HashMap();
                        }
                    }
                }
            }
            r7.A03 = hashMap;
        }
        if (r7.A03 != null) {
            return true;
        }
        return false;
    }

    public void A05(C36711tb r4, C11670nb r5) {
        C36691tZ r1 = this.A00;
        if (r1.A05) {
            if (r5 == null) {
                r5 = new C11670nb(r1.A04);
            }
            if (r4 != null) {
                r5.A0D("otd", r4.A0A);
                r5.A0A("start_ts", r4.A07);
                r5.A09("is_failed", 0);
                r5.A09("last_error_code", r4.A00);
                r5.A0D("last_error_message", r4.A08);
                r5.A0A("last_attempt_id", r4.A06);
                r5.A09("network_error_count", r4.A04);
                r5.A09("non_network_error_count", r4.A05);
                r5.A09("mqtt_attempts", r4.A03);
                r5.A09("graph_attempts", r4.A01);
            }
            r5.A05();
            DeprecatedAnalyticsLogger deprecatedAnalyticsLogger = this.A04;
            if (deprecatedAnalyticsLogger != null) {
                deprecatedAnalyticsLogger.A09(r5);
            }
        }
    }

    public C36671tX(AnonymousClass06B r2, FbSharedPreferences fbSharedPreferences, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        this.A05 = r2;
        this.A06 = fbSharedPreferences;
        this.A04 = deprecatedAnalyticsLogger;
        this.A07 = new AnonymousClass0jJ();
    }

    public C36711tb A03(String str) {
        if (!A02(this)) {
            return null;
        }
        C36711tb r0 = (C36711tb) this.A03.remove(str);
        A01();
        return r0;
    }

    public C36711tb A04(String str, long j, int i, String str2, boolean z) {
        C36711tb r1;
        if (!A02(this) || (r1 = (C36711tb) this.A03.get(str)) == null) {
            return null;
        }
        r1.A06 = j;
        r1.A00 = i;
        r1.A08 = str2;
        if (z) {
            r1.A03++;
        } else {
            r1.A01++;
        }
        if (A07()) {
            r1.A04++;
        } else {
            r1.A05++;
        }
        A01();
        return r1;
    }

    public void A06(String str, C36711tb r4) {
        if (A02(this) && !this.A03.containsKey(str)) {
            r4.A0A = str;
            AnonymousClass06B r0 = this.A05;
            if (r0 != null) {
                r4.A07 = r0.now();
            }
            this.A03.put(str, r4);
            A01();
        }
    }
}
