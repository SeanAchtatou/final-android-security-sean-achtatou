package X;

import com.facebook.common.callercontext.CallerContext;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.util.Arrays;

/* renamed from: X.1md  reason: invalid class name and case insensitive filesystem */
public final class C32971md {
    public final CallerContext A00;
    public final C10700ki A01;
    public final C35381r8 A02;
    public final boolean A03;
    public final boolean A04;
    public final boolean A05;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C32971md r5 = (C32971md) obj;
            if (!(this.A05 == r5.A05 && this.A04 == r5.A04 && this.A03 == r5.A03 && this.A02 == r5.A02 && this.A01 == r5.A01 && Objects.equal(this.A00, r5.A00))) {
                return false;
            }
        }
        return true;
    }

    public static C32971md A00(boolean z, boolean z2, boolean z3, C10700ki r7, CallerContext callerContext) {
        return new C32971md(z, z2, C35381r8.A02, r7, z3, callerContext);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Boolean.valueOf(this.A05), Boolean.valueOf(this.A04), this.A02, this.A01, Boolean.valueOf(this.A03), this.A00});
    }

    public C32971md(boolean z, boolean z2, C35381r8 r3, C10700ki r4, boolean z3, CallerContext callerContext) {
        this.A05 = z;
        this.A04 = z2;
        this.A02 = r3;
        this.A01 = r4;
        this.A03 = z3;
        this.A00 = callerContext;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("needToHitServer", this.A05);
        stringHelper.add("loadWasFromUserAction", this.A04);
        stringHelper.add("loadType", this.A02.toString());
        stringHelper.add("inboxFilter", this.A01);
        stringHelper.add("isForceUpdate", this.A03);
        stringHelper.add(C99084oO.$const$string(3), this.A00);
        return stringHelper.toString();
    }
}
