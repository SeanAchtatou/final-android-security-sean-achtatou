package X;

/* renamed from: X.0sl  reason: invalid class name and case insensitive filesystem */
public final class C14190sl {
    public static final Integer[] A00 = AnonymousClass07B.A00(2);

    public static String A00(Integer num) {
        return 1 - num.intValue() != 0 ? "light" : "dark";
    }

    public static String A01(Integer num) {
        return 1 - num.intValue() != 0 ? "light_scheme" : "dark_scheme";
    }
}
