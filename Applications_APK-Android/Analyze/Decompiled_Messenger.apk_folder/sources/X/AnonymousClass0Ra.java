package X;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.facebook.profilo.multiprocess.ProfiloIPCParcelable;

/* renamed from: X.0Ra  reason: invalid class name */
public final class AnonymousClass0Ra extends AnonymousClass07D {
    private final IBinder A00;
    private final String A01;

    public String A01() {
        return "com.facebook.profilo.MAIN_PROCESS_STARTED_V3";
    }

    public String A02() {
        return "com.facebook.profilo.NON_MAIN_PROCESS_STARTED_V3";
    }

    public ProfiloIPCParcelable A00() {
        return new ProfiloIPCParcelable(this.A00);
    }

    public AnonymousClass0Ra(String str, IBinder iBinder) {
        this.A01 = str;
        this.A00 = iBinder;
    }

    public void onReceive(Context context, Intent intent) {
        int A012 = C000700l.A01(-232486732);
        if (!intent.getAction().equals(A01())) {
            C000700l.A0D(intent, -617955204, A012);
            return;
        }
        A03(context, this.A01);
        C000700l.A0D(intent, -976131565, A012);
    }
}
