package X;

import android.net.Uri;
import java.util.Locale;

/* renamed from: X.0R5  reason: invalid class name */
public final class AnonymousClass0R5 {
    public static Uri A01(String str, AnonymousClass04e r5, boolean z) {
        if (r5 != null) {
            try {
                return A00(str);
            } catch (Exception e) {
                r5.C2T("UriParser", String.format(Locale.US, "Parse uri %s failed. Fail open: %b", str, Boolean.valueOf(z)), e);
                if (z) {
                    return Uri.parse(str);
                }
                return null;
            }
        } else {
            throw new IllegalArgumentException("reporter is null");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0084, code lost:
        if (r1 == false) goto L_0x0086;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.net.Uri A00(java.lang.String r4) {
        /*
            java.net.URI r3 = java.net.URI.create(r4)
            android.net.Uri$Builder r1 = new android.net.Uri$Builder
            r1.<init>()
            java.lang.String r0 = r3.getScheme()
            android.net.Uri$Builder r1 = r1.scheme(r0)
            java.lang.String r0 = r3.getRawAuthority()
            android.net.Uri$Builder r1 = r1.encodedAuthority(r0)
            java.lang.String r0 = r3.getRawPath()
            android.net.Uri$Builder r1 = r1.encodedPath(r0)
            java.lang.String r0 = r3.getRawQuery()
            android.net.Uri$Builder r1 = r1.encodedQuery(r0)
            java.lang.String r0 = r3.getRawFragment()
            android.net.Uri$Builder r0 = r1.encodedFragment(r0)
            android.net.Uri r2 = r0.build()
            java.lang.String r0 = r3.getScheme()
            java.lang.String r1 = r2.getScheme()
            if (r0 != 0) goto L_0x009e
            r0 = 0
            if (r1 != 0) goto L_0x0043
            r0 = 1
        L_0x0043:
            if (r0 == 0) goto L_0x0086
            java.lang.String r0 = r3.getAuthority()
            java.lang.String r1 = r2.getAuthority()
            if (r0 != 0) goto L_0x0099
            r0 = 0
            if (r1 != 0) goto L_0x0053
            r0 = 1
        L_0x0053:
            if (r0 == 0) goto L_0x0086
            java.lang.String r0 = r3.getPath()
            java.lang.String r1 = r2.getPath()
            if (r0 != 0) goto L_0x0094
            r0 = 0
            if (r1 != 0) goto L_0x0063
            r0 = 1
        L_0x0063:
            if (r0 == 0) goto L_0x0086
            java.lang.String r0 = r3.getQuery()
            java.lang.String r1 = r2.getQuery()
            if (r0 != 0) goto L_0x008f
            r0 = 0
            if (r1 != 0) goto L_0x0073
            r0 = 1
        L_0x0073:
            if (r0 == 0) goto L_0x0086
            java.lang.String r1 = r3.getFragment()
            java.lang.String r0 = r2.getFragment()
            if (r1 != 0) goto L_0x008a
            r1 = 0
            if (r0 != 0) goto L_0x0083
            r1 = 1
        L_0x0083:
            r0 = 1
            if (r1 != 0) goto L_0x0087
        L_0x0086:
            r0 = 0
        L_0x0087:
            if (r0 == 0) goto L_0x00a3
            return r2
        L_0x008a:
            boolean r1 = r1.equals(r0)
            goto L_0x0083
        L_0x008f:
            boolean r0 = r0.equals(r1)
            goto L_0x0073
        L_0x0094:
            boolean r0 = r0.equals(r1)
            goto L_0x0063
        L_0x0099:
            boolean r0 = r0.equals(r1)
            goto L_0x0053
        L_0x009e:
            boolean r0 = r0.equals(r1)
            goto L_0x0043
        L_0x00a3:
            java.lang.SecurityException r3 = new java.lang.SecurityException
            java.util.Locale r2 = java.util.Locale.US
            java.lang.Object[] r1 = new java.lang.Object[]{r4}
            java.lang.String r0 = "java uri not equal to android uri for uri string %s"
            java.lang.String r0 = java.lang.String.format(r2, r0, r1)
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0R5.A00(java.lang.String):android.net.Uri");
    }
}
