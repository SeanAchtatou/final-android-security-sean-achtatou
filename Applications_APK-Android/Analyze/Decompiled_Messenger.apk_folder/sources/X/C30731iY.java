package X;

import com.facebook.acra.ActionId;
import com.facebook.acra.util.StatFsUtil;
import com.facebook.common.dextricks.OdexSchemeArtXdex;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1iY  reason: invalid class name and case insensitive filesystem */
public class C30731iY implements AnonymousClass1OI, C30741iZ {
    public AtomicLong A00 = new AtomicLong(0);
    public AtomicLong A01 = new AtomicLong(0);
    public final AnonymousClass09P A02;
    public final QuickPerformanceLogger A03;
    public final WeakHashMap A04 = new WeakHashMap();
    public final C012109i A05;

    /* JADX WARNING: Removed duplicated region for block: B:48:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x00e4  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:49:0x0091=Splitter:B:49:0x0091, B:39:0x0072=Splitter:B:39:0x0072, B:77:0x00e5=Splitter:B:77:0x00e5, B:67:0x00cc=Splitter:B:67:0x00cc} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int A01(long r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            long r1 = r11.A03()     // Catch:{ all -> 0x00f8 }
            int r0 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x000d
            r0 = 101(0x65, float:1.42E-43)
            monitor-exit(r11)
            return r0
        L_0x000d:
            java.util.WeakHashMap r1 = r11.A04     // Catch:{ all -> 0x00f8 }
            monitor-enter(r1)     // Catch:{ all -> 0x00f8 }
            java.util.HashSet r3 = new java.util.HashSet     // Catch:{ all -> 0x00f5 }
            java.util.WeakHashMap r0 = r11.A04     // Catch:{ all -> 0x00f5 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x00f5 }
            r3.<init>(r0)     // Catch:{ all -> 0x00f5 }
            monitor-exit(r1)     // Catch:{ all -> 0x00f5 }
            boolean r0 = r3.isEmpty()     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0026
            r0 = 102(0x66, float:1.43E-43)
            monitor-exit(r11)
            return r0
        L_0x0026:
            long r9 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00f8 }
            r7 = 1000000(0xf4240, double:4.940656E-318)
            long r9 = r9 / r7
            long r1 = r11.A05()     // Catch:{ all -> 0x00f8 }
            int r0 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0091
            java.util.concurrent.atomic.AtomicLong r0 = r11.A01     // Catch:{ all -> 0x00f8 }
            long r0 = r0.get()     // Catch:{ all -> 0x00f8 }
            long r4 = r9 - r0
            long r1 = r11.A04()     // Catch:{ all -> 0x00f8 }
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0091
            r6 = 3
            r5 = 43253766(0x2940006, float:2.174666E-37)
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03     // Catch:{ all -> 0x008b }
            r0.markerStart(r5)     // Catch:{ all -> 0x008b }
            java.util.Iterator r4 = r3.iterator()     // Catch:{ all -> 0x008b }
            r3 = 1
        L_0x0054:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x006f
            java.lang.Object r0 = r4.next()     // Catch:{ all -> 0x0089 }
            X.1OP r0 = (X.AnonymousClass1OP) r0     // Catch:{ all -> 0x0089 }
            r0.trimToNothing()     // Catch:{ Exception -> 0x0064 }
            goto L_0x006d
        L_0x0064:
            r2 = move-exception
            X.09P r1 = r11.A02     // Catch:{ all -> 0x0089 }
            java.lang.String r0 = "DiskTrimmableManager"
            r1.softReport(r0, r2)     // Catch:{ all -> 0x0089 }
            goto L_0x0054
        L_0x006d:
            r3 = 0
            goto L_0x0054
        L_0x006f:
            if (r3 != 0) goto L_0x0072
            r6 = 2
        L_0x0072:
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03     // Catch:{ all -> 0x00f8 }
            r0.markerEnd(r5, r6)     // Catch:{ all -> 0x00f8 }
            long r1 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00f8 }
            long r1 = r1 / r7
            java.util.concurrent.atomic.AtomicLong r0 = r11.A00     // Catch:{ all -> 0x00f8 }
            r0.set(r1)     // Catch:{ all -> 0x00f8 }
            java.util.concurrent.atomic.AtomicLong r0 = r11.A01     // Catch:{ all -> 0x00f8 }
            r0.set(r1)     // Catch:{ all -> 0x00f8 }
            r0 = 1
            monitor-exit(r11)
            return r0
        L_0x0089:
            r1 = move-exception
            goto L_0x008d
        L_0x008b:
            r1 = move-exception
            r3 = 1
        L_0x008d:
            if (r3 != 0) goto L_0x00eb
            r6 = 2
            goto L_0x00eb
        L_0x0091:
            java.util.concurrent.atomic.AtomicLong r0 = r11.A00     // Catch:{ all -> 0x00f8 }
            long r0 = r0.get()     // Catch:{ all -> 0x00f8 }
            long r9 = r9 - r0
            long r1 = r11.A02()     // Catch:{ all -> 0x00f8 }
            int r0 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00f1
            r6 = 3
            r5 = 43253765(0x2940005, float:2.1746657E-37)
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03     // Catch:{ all -> 0x00e0 }
            r0.markerStart(r5)     // Catch:{ all -> 0x00e0 }
            java.util.Iterator r4 = r3.iterator()     // Catch:{ all -> 0x00e0 }
            r3 = 1
        L_0x00ae:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x00de }
            if (r0 == 0) goto L_0x00c9
            java.lang.Object r0 = r4.next()     // Catch:{ all -> 0x00de }
            X.1OP r0 = (X.AnonymousClass1OP) r0     // Catch:{ all -> 0x00de }
            r0.trimToMinimum()     // Catch:{ Exception -> 0x00be }
            goto L_0x00c7
        L_0x00be:
            r2 = move-exception
            X.09P r1 = r11.A02     // Catch:{ all -> 0x00de }
            java.lang.String r0 = "DiskTrimmableManager"
            r1.softReport(r0, r2)     // Catch:{ all -> 0x00de }
            goto L_0x00ae
        L_0x00c7:
            r3 = 0
            goto L_0x00ae
        L_0x00c9:
            if (r3 != 0) goto L_0x00cc
            r6 = 2
        L_0x00cc:
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03     // Catch:{ all -> 0x00f8 }
            r0.markerEnd(r5, r6)     // Catch:{ all -> 0x00f8 }
            java.util.concurrent.atomic.AtomicLong r2 = r11.A00     // Catch:{ all -> 0x00f8 }
            long r0 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00f8 }
            long r0 = r0 / r7
            r2.set(r0)     // Catch:{ all -> 0x00f8 }
            r0 = 2
            monitor-exit(r11)
            return r0
        L_0x00de:
            r1 = move-exception
            goto L_0x00e2
        L_0x00e0:
            r1 = move-exception
            r3 = 1
        L_0x00e2:
            if (r3 != 0) goto L_0x00e5
            r6 = 2
        L_0x00e5:
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03     // Catch:{ all -> 0x00f8 }
            r0.markerEnd(r5, r6)     // Catch:{ all -> 0x00f8 }
            goto L_0x00f0
        L_0x00eb:
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03     // Catch:{ all -> 0x00f8 }
            r0.markerEnd(r5, r6)     // Catch:{ all -> 0x00f8 }
        L_0x00f0:
            throw r1     // Catch:{ all -> 0x00f8 }
        L_0x00f1:
            r0 = 103(0x67, float:1.44E-43)
            monitor-exit(r11)
            return r0
        L_0x00f5:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00f5 }
            throw r0     // Catch:{ all -> 0x00f8 }
        L_0x00f8:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30731iY.A01(long):int");
    }

    public long A02() {
        boolean z = this instanceof C30721iX;
        return 86400000;
    }

    public long A03() {
        return !(this instanceof C30721iX) ? OdexSchemeArtXdex.MIN_DISK_FREE_FOR_MIXED_MODE : ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass24O) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AdL, ((C30721iX) this).A00)).A00)).At0(567790382483577L) * StatFsUtil.IN_KILO_BYTE;
    }

    public long A04() {
        if (!(this instanceof C30721iX)) {
            return 300000;
        }
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass24O) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AdL, ((C30721iX) this).A00)).A00)).At0(567790382155895L);
    }

    public long A05() {
        if (!(this instanceof C30721iX)) {
            return 5242880;
        }
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass24O) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AdL, ((C30721iX) this).A00)).A00)).At0(567790382549114L) * StatFsUtil.IN_KILO_BYTE;
    }

    public void C0U(AnonymousClass1OP r6) {
        try {
            this.A03.markerStart(43253763);
            synchronized (this.A04) {
                this.A04.put(r6, true);
            }
            this.A03.markerEnd(43253763, 2);
        } catch (Throwable th) {
            this.A03.markerEnd(43253763, 2);
            throw th;
        }
    }

    public C30731iY(C012109i r4, AnonymousClass09P r5, QuickPerformanceLogger quickPerformanceLogger) {
        this.A05 = r4;
        this.A02 = r5;
        this.A03 = quickPerformanceLogger;
    }

    public void BtP(long j) {
        long j2;
        try {
            this.A03.markerStart(43253764);
            this.A03.markerAnnotate(43253764, "available_space", j);
            synchronized (this.A04) {
                this.A03.markerAnnotate(43253764, "listener_count", this.A04.size());
            }
            int A012 = A01(j);
            this.A03.markerAnnotate(43253764, "trim_action", A012);
            boolean z = false;
            if (A012 <= 100) {
                z = true;
            }
            short s = ActionId.ABORTED;
            if (z) {
                s = 2;
            }
            if (z) {
                this.A05.A08();
                j2 = this.A05.A05(AnonymousClass07B.A00);
            } else {
                j2 = j;
            }
            this.A03.markerAnnotate(43253764, "available_space_change", j2 - j);
            this.A03.markerEnd(43253764, s);
        } catch (Throwable th) {
            this.A03.markerEnd(43253764, 3);
            throw th;
        }
    }
}
