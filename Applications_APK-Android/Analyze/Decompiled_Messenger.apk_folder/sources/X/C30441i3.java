package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.send.SendError;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.base.Platform;
import com.google.common.collect.ImmutableSet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1i3  reason: invalid class name and case insensitive filesystem */
public final class C30441i3 extends C30451i4 {
    public static final String[] A07;
    public static final String[] A08;
    private static volatile C30441i3 A09;
    public final AnonymousClass06A A00;
    public final C11920oF A01;
    public final AnonymousClass186 A02;
    public final C04310Tq A03;
    private final AnonymousClass187 A04;
    @LoggedInUser
    private final C04310Tq A05;
    private final C04310Tq A06;

    private byte[] A09(byte[] bArr, byte[] bArr2) {
        if (!(bArr == null || bArr2 == null)) {
            try {
                return this.A04.A05(bArr, bArr2);
            } catch (C37911wY | C37921wZ | IOException e) {
                C010708t.A0L("TincanDbMessagesFetcher", "Failed to decrypt message content", e);
            }
        }
        return null;
    }

    static {
        String str = C195818v.A05.A00;
        String str2 = C195818v.A0E.A00;
        String str3 = C195818v.A06.A00;
        String str4 = C195818v.A02.A00;
        String str5 = C195818v.A09.A00;
        String str6 = C195818v.A0F.A00;
        String str7 = C195818v.A0G.A00;
        String str8 = C195818v.A07.A00;
        String str9 = C195818v.A0A.A00;
        String str10 = C195818v.A0B.A00;
        String str11 = C195818v.A0C.A00;
        String str12 = C195818v.A08.A00;
        String str13 = C195818v.A01.A00;
        String str14 = C195818v.A03.A00;
        String str15 = C195818v.A0D.A00;
        A08 = new String[]{str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14, str15, AnonymousClass182.A03.A00};
        A07 = new String[]{str2, str12, str15};
    }

    public static Cursor A00(SQLiteDatabase sQLiteDatabase, C06140av r9) {
        C25601a6 A012 = C06160ax.A01(C06160ax.A03(C195818v.A06.A00, String.valueOf(AnonymousClass1V7.A0J.dbKeyValue)), r9);
        return sQLiteDatabase.query("messages", A07, A012.A02(), A012.A04(), null, null, null);
    }

    public static C06140av A01() {
        return new C12930qE(AnonymousClass08S.A0J(C195818v.A03.A00, " IS NOT 1"));
    }

    public static Message A02(C30441i3 r21, ContentValues contentValues) {
        int i;
        long j;
        long j2;
        long j3;
        List A042;
        long j4;
        ContentValues contentValues2 = contentValues;
        String asString = contentValues2.getAsString(C195818v.A05.A00);
        String asString2 = contentValues2.getAsString(C195818v.A0E.A00);
        Integer asInteger = contentValues2.getAsInteger(C195818v.A06.A00);
        if (asInteger != null) {
            i = asInteger.intValue();
        } else {
            i = 0;
        }
        byte[] asByteArray = contentValues2.getAsByteArray(C195818v.A02.A00);
        Long asLong = contentValues2.getAsLong(C195818v.A0F.A00);
        if (asLong != null) {
            j = asLong.longValue();
        } else {
            j = 0;
        }
        Long asLong2 = contentValues2.getAsLong(C195818v.A0G.A00);
        if (asLong2 != null) {
            j2 = asLong2.longValue();
        } else {
            j2 = 0;
        }
        String asString3 = contentValues2.getAsString(C195818v.A07.A00);
        String asString4 = contentValues2.getAsString(C195818v.A0A.A00);
        Long asLong3 = contentValues2.getAsLong(C195818v.A01.A00);
        if (asLong3 != null) {
            j3 = asLong3.longValue();
        } else {
            j3 = 0;
        }
        String valueOf = String.valueOf(contentValues2.getAsLong(C195818v.A09.A00));
        String asString5 = contentValues2.getAsString(AnonymousClass182.A03.A00);
        C30441i3 r10 = r21;
        if (C06850cB.A0A(asString5)) {
            User user = (User) r10.A05.get();
            if (user.A0j.equals(valueOf)) {
                asString5 = user.A09();
            }
        }
        ThreadKey A062 = ThreadKey.A06(asString2);
        AnonymousClass1V7 A002 = AnonymousClass1V7.A00(i);
        ParticipantInfo participantInfo = new ParticipantInfo(UserKey.A01(valueOf), asString5);
        if (A002 == AnonymousClass1V7.A0K) {
            C22748BAs.A01.A00(j);
        }
        Integer asInteger2 = contentValues2.getAsInteger(C195818v.A0D.A00);
        int i2 = -1;
        if (asInteger2 != null) {
            i2 = asInteger2.intValue();
        }
        byte[] A022 = ((AnonymousClass23J) r10.A06.get()).A02(A062, i2);
        byte[] A092 = r10.A09(A022, asByteArray);
        SendError sendError = SendError.A08;
        if (!C06850cB.A0A(asString4)) {
            Long asLong4 = contentValues2.getAsLong(C195818v.A0C.A00);
            if (asLong4 != null) {
                j4 = asLong4.longValue();
            } else {
                j4 = 0;
            }
            String asString6 = contentValues2.getAsString(C195818v.A0B.A00);
            C196899Nw r15 = new C196899Nw();
            r15.A02 = C36891u4.A00(asString4);
            r15.A01(Long.valueOf(j4));
            r15.A06 = asString6;
            sendError = new SendError(r15);
        }
        String A023 = r10.A04.A02(A022, contentValues2.getAsByteArray(C195818v.A08.A00));
        if (Platform.stringIsNullOrEmpty(A023)) {
            A042 = new ArrayList();
        } else {
            A042 = r10.A01.A04(A023);
        }
        AnonymousClass1TG A003 = Message.A00();
        A003.A06(asString);
        A003.A0T = A062;
        A003.A0C = A002;
        A003.A0J = participantInfo;
        A003.A04 = j;
        A003.A03 = j2;
        A003.A0u = asString3;
        A003.A0R = sendError;
        A003.A09(A042);
        A003.A0j = Long.valueOf(j3);
        if (!(A092 == null || A092 == null || A092.length == 0)) {
            try {
                r10.A02.A03(A003, asString, C22030An2.A00(A092));
            } catch (IllegalStateException e) {
                C010708t.A0L("TincanDbMessagesFetcher", "Retrieved Salamander decoded with invalid body", e);
            }
        }
        return A003.A00();
    }

    public static final C30441i3 A04(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (C30441i3.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new C30441i3(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0065, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0066, code lost:
        if (r1 != null) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006b, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.common.collect.ImmutableSet A05(X.C30441i3 r12, X.C06140av r13) {
        /*
            X.0dQ r3 = new X.0dQ
            r3.<init>()
            java.util.LinkedList r2 = new java.util.LinkedList
            r2.<init>()
            X.0Tq r0 = r12.A03
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r4 = r0.A01()
            if (r4 == 0) goto L_0x006c
            java.lang.String r5 = A07()
            java.lang.String[] r6 = X.C30441i3.A08
            java.lang.String r7 = r13.A02()
            java.lang.String[] r8 = r13.A04()
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r1 = r4.query(r5, r6, r7, r8, r9, r10, r11)
        L_0x002d:
            boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x0063 }
            if (r0 == 0) goto L_0x003f
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x0063 }
            r0.<init>()     // Catch:{ all -> 0x0063 }
            android.database.DatabaseUtils.cursorRowToContentValues(r1, r0)     // Catch:{ all -> 0x0063 }
            r2.add(r0)     // Catch:{ all -> 0x0063 }
            goto L_0x002d
        L_0x003f:
            r1.close()
            java.util.Iterator r4 = r2.iterator()
        L_0x0046:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x006c
            java.lang.Object r0 = r4.next()
            android.content.ContentValues r0 = (android.content.ContentValues) r0
            com.facebook.messaging.model.messages.Message r0 = A02(r12, r0)     // Catch:{ all -> 0x005a }
            r3.A01(r0)     // Catch:{ all -> 0x005a }
            goto L_0x0046
        L_0x005a:
            r2 = move-exception
            java.lang.String r1 = "TincanDbMessagesFetcher"
            java.lang.String r0 = "Unable to decrypt message. Skipping"
            X.C010708t.A0S(r1, r2, r0)
            goto L_0x0046
        L_0x0063:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0065 }
        L_0x0065:
            r0 = move-exception
            if (r1 == 0) goto L_0x006b
            r1.close()     // Catch:{ all -> 0x006b }
        L_0x006b:
            throw r0
        L_0x006c:
            com.google.common.collect.ImmutableSet r0 = r3.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30441i3.A05(X.1i3, X.0av):com.google.common.collect.ImmutableSet");
    }

    public static String A07() {
        return StringFormatUtil.formatStrLocaleSafe("%s LEFT OUTER JOIN %s ON (%s.%s = %s.%s)", "messages", "thread_participants", "messages", C195818v.A09.A00, "thread_participants", AnonymousClass182.A05.A00);
    }

    public static String A08(C30441i3 r4, Cursor cursor) {
        int i;
        ThreadKey A062 = ThreadKey.A06(C195818v.A0E.A05(cursor));
        byte[] A072 = C195818v.A08.A07(cursor);
        AnonymousClass23J r1 = (AnonymousClass23J) r4.A06.get();
        if (!C195818v.A0D.A06(cursor)) {
            i = C195818v.A0D.A00(cursor);
        } else {
            i = -1;
        }
        return r4.A04.A02(r1.A02(A062, i), A072);
    }

    private C30441i3(AnonymousClass1XY r2) {
        this.A01 = C11920oF.A00(r2);
        this.A02 = AnonymousClass186.A00(r2);
        this.A00 = AnonymousClass067.A0A(r2);
        this.A03 = AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, r2);
        this.A05 = AnonymousClass0WY.A01(r2);
        this.A04 = AnonymousClass187.A01(r2);
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.B6n, r2);
    }

    public static Message A03(Set set) {
        if (set.isEmpty()) {
            return null;
        }
        if (set.size() == 1) {
            return (Message) set.iterator().next();
        }
        throw new RuntimeException("Should never return more than 1 message from DB.");
    }

    public static ImmutableSet A06(C30441i3 r2, C06140av r3) {
        C25601a6 A002 = C06160ax.A00();
        A002.A05(r3);
        A002.A05(A01());
        return A05(r2, A002);
    }

    public Message A0C(String str) {
        return A03(A06(this, C06160ax.A04(C195818v.A05.A00, ImmutableSet.A04(str))));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b2, code lost:
        if (r1 != null) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00b7, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.MessagesCollection A0D(com.facebook.messaging.model.threadkey.ThreadKey r13) {
        /*
            r12 = this;
            X.1a6 r1 = X.C06160ax.A00()
            X.0W6 r2 = X.C195818v.A0E
            java.lang.String r0 = r13.toString()
            X.0av r0 = r2.A03(r0)
            r1.A05(r0)
            X.0W6 r2 = X.C195818v.A06
            X.1V7 r0 = X.AnonymousClass1V7.A0J
            int r0 = r0.dbKeyValue
            java.lang.String r0 = java.lang.Integer.toString(r0)
            X.0av r0 = r2.A03(r0)
            r1.A05(r0)
            X.0av r0 = A01()
            r1.A05(r0)
            java.lang.String r5 = A07()
            java.util.LinkedList r2 = new java.util.LinkedList
            r2.<init>()
            com.google.common.collect.ImmutableList$Builder r3 = new com.google.common.collect.ImmutableList$Builder
            r3.<init>()
            X.0Tq r0 = r12.A03
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r4 = r0.A01()
            if (r4 != 0) goto L_0x004a
            com.facebook.messaging.model.messages.MessagesCollection r0 = com.facebook.messaging.model.messages.MessagesCollection.A02(r13)
            return r0
        L_0x004a:
            java.lang.String[] r6 = X.C30441i3.A08
            java.lang.String r7 = r1.A02()
            java.lang.String[] r8 = r1.A04()
            r9 = 0
            r10 = 0
            X.0W6 r0 = X.C195818v.A0F
            java.lang.String r11 = r0.A04()
            android.database.Cursor r1 = r4.query(r5, r6, r7, r8, r9, r10, r11)
        L_0x0060:
            boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x0072
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x00af }
            r0.<init>()     // Catch:{ all -> 0x00af }
            android.database.DatabaseUtils.cursorRowToContentValues(r1, r0)     // Catch:{ all -> 0x00af }
            r2.add(r0)     // Catch:{ all -> 0x00af }
            goto L_0x0060
        L_0x0072:
            r1.close()
            java.util.Iterator r4 = r2.iterator()
        L_0x0079:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0096
            java.lang.Object r0 = r4.next()
            android.content.ContentValues r0 = (android.content.ContentValues) r0
            com.facebook.messaging.model.messages.Message r0 = A02(r12, r0)     // Catch:{ all -> 0x008d }
            r3.add(r0)     // Catch:{ all -> 0x008d }
            goto L_0x0079
        L_0x008d:
            r2 = move-exception
            java.lang.String r1 = "TincanDbMessagesFetcher"
            java.lang.String r0 = "Unable to decrypt message. Skipping"
            X.C010708t.A0S(r1, r2, r0)
            goto L_0x0079
        L_0x0096:
            com.google.common.collect.ImmutableList r0 = r3.build()
            X.1oI r1 = new X.1oI
            r1.<init>()
            r1.A00 = r13
            r1.A01(r0)
            r0 = 0
            r1.A03 = r0
            r0 = 1
            r1.A02 = r0
            com.facebook.messaging.model.messages.MessagesCollection r0 = r1.A00()
            return r0
        L_0x00af:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00b1 }
        L_0x00b1:
            r0 = move-exception
            if (r1 == 0) goto L_0x00b7
            r1.close()     // Catch:{ all -> 0x00b7 }
        L_0x00b7:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30441i3.A0D(com.facebook.messaging.model.threadkey.ThreadKey):com.facebook.messaging.model.messages.MessagesCollection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00a1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00a2, code lost:
        if (r4 != null) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a7, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A0E(com.facebook.messaging.model.threadkey.ThreadKey r18, int r19) {
        /*
            r17 = this;
            com.google.common.collect.ImmutableList$Builder r1 = com.google.common.collect.ImmutableList.builder()
            X.0W6 r3 = X.C195818v.A0E
            r2 = r18
            java.lang.String r0 = r2.toString()
            X.0av r8 = r3.A03(r0)
            X.0W6 r0 = X.C195818v.A0F
            java.lang.String r3 = r0.A04()
            java.lang.String r0 = " LIMIT "
            r4 = r19
            java.lang.String r16 = X.AnonymousClass08S.A0L(r3, r0, r4)
            r0 = r17
            X.0Tq r3 = r0.A03
            java.lang.Object r3 = r3.get()
            X.183 r3 = (X.AnonymousClass183) r3
            android.database.sqlite.SQLiteDatabase r9 = r3.A01()
            if (r9 == 0) goto L_0x00ab
            X.0W6 r3 = X.C195818v.A09
            java.lang.String r7 = r3.A00
            X.0W6 r3 = X.C195818v.A0F
            java.lang.String r6 = r3.A00
            X.0W6 r3 = X.C195818v.A02
            java.lang.String r5 = r3.A00
            X.0W6 r3 = X.C195818v.A04
            java.lang.String r4 = r3.A00
            X.0W6 r3 = X.C195818v.A0D
            java.lang.String r3 = r3.A00
            java.lang.String[] r11 = new java.lang.String[]{r7, r6, r5, r4, r3}
            java.lang.String r12 = r8.A02()
            java.lang.String[] r13 = r8.A04()
            r14 = 0
            r15 = 0
            java.lang.String r10 = "messages"
            android.database.Cursor r4 = r9.query(r10, r11, r12, r13, r14, r15, r16)
        L_0x0056:
            boolean r3 = r4.moveToNext()     // Catch:{ all -> 0x009f }
            if (r3 == 0) goto L_0x00a8
            X.0W6 r3 = X.C195818v.A09     // Catch:{ all -> 0x009f }
            long r7 = r3.A02(r4)     // Catch:{ all -> 0x009f }
            X.0W6 r3 = X.C195818v.A0F     // Catch:{ all -> 0x009f }
            long r9 = r3.A02(r4)     // Catch:{ all -> 0x009f }
            X.0W6 r3 = X.C195818v.A02     // Catch:{ all -> 0x009f }
            byte[] r6 = r3.A07(r4)     // Catch:{ all -> 0x009f }
            X.0Tq r3 = r0.A06     // Catch:{ all -> 0x009f }
            java.lang.Object r5 = r3.get()     // Catch:{ all -> 0x009f }
            X.23J r5 = (X.AnonymousClass23J) r5     // Catch:{ all -> 0x009f }
            X.0W6 r3 = X.C195818v.A0D     // Catch:{ all -> 0x009f }
            boolean r3 = r3.A06(r4)     // Catch:{ all -> 0x009f }
            if (r3 != 0) goto L_0x009d
            X.0W6 r3 = X.C195818v.A0D     // Catch:{ all -> 0x009f }
            int r3 = r3.A00(r4)     // Catch:{ all -> 0x009f }
        L_0x0084:
            byte[] r3 = r5.A02(r2, r3)     // Catch:{ all -> 0x009f }
            byte[] r11 = r0.A09(r3, r6)     // Catch:{ all -> 0x009f }
            if (r11 == 0) goto L_0x0056
            X.0W6 r3 = X.C195818v.A04     // Catch:{ all -> 0x009f }
            byte[] r12 = r3.A07(r4)     // Catch:{ all -> 0x009f }
            com.facebook.messaging.tincan.database.RawTincanMessageContent r6 = new com.facebook.messaging.tincan.database.RawTincanMessageContent     // Catch:{ all -> 0x009f }
            r6.<init>(r7, r9, r11, r12)     // Catch:{ all -> 0x009f }
            r1.add(r6)     // Catch:{ all -> 0x009f }
            goto L_0x0056
        L_0x009d:
            r3 = -1
            goto L_0x0084
        L_0x009f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a1 }
        L_0x00a1:
            r0 = move-exception
            if (r4 == 0) goto L_0x00a7
            r4.close()     // Catch:{ all -> 0x00a7 }
        L_0x00a7:
            throw r0
        L_0x00a8:
            r4.close()
        L_0x00ab:
            com.google.common.collect.ImmutableList r0 = r1.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30441i3.A0E(com.facebook.messaging.model.threadkey.ThreadKey, int):com.google.common.collect.ImmutableList");
    }

    public ImmutableSet A0F(ThreadKey threadKey, long j) {
        C25601a6 A002 = C06160ax.A00();
        A002.A05(C195818v.A0E.A03(threadKey.toString()));
        AnonymousClass0W6 r0 = C195818v.A0F;
        A002.A05(new C46962Sr(r0.A00, String.valueOf(j)));
        A002.A05(new C29661gm(C195818v.A01.A00, "0"));
        return A06(this, A002);
    }

    public ImmutableSet A0G(ThreadKey threadKey, long j, String str) {
        C25601a6 A002 = C06160ax.A00();
        A002.A05(C195818v.A0E.A03(threadKey.toString()));
        AnonymousClass0W6 r3 = C195818v.A01;
        A002.A05(new C29661gm(r3.A00, "0"));
        A002.A05(new C46962Sr(r3.A00, String.valueOf(j)));
        if (str != null) {
            A002.A05(C06160ax.A05(C195818v.A09.A00, str));
        }
        return A06(this, A002);
    }
}
