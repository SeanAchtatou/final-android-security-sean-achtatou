package X;

import android.view.View;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1v9  reason: invalid class name */
public final class AnonymousClass1v9 extends C17770zR {
    @Comparable(type = 3)
    public int A00;
    @Comparable(type = 13)
    public View.OnClickListener A01;
    @Comparable(type = 13)
    public C17840zZ A02;
    @Comparable(type = 13)
    public String A03;
    public C17840zZ[] A04;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1v9() {
        super("PaymentFormEditTextViewComponent");
    }
}
