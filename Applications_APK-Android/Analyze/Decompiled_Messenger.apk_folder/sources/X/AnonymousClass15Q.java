package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.15Q  reason: invalid class name */
public final class AnonymousClass15Q {
    private static volatile AnonymousClass15Q A08;
    public int A00 = 1;
    public int A01 = 4;
    public int A02;
    public AnonymousClass15R A03 = AnonymousClass15R.UNKNOWN;
    public boolean A04 = false;
    private boolean A05;
    private boolean A06 = false;
    private final Context A07;

    public static int A00(AnonymousClass15Q r1, int i, boolean z) {
        int i2;
        if (!r1.A05 || !z) {
            return i;
        }
        if (z) {
            i2 = r1.A00;
        } else {
            i2 = 1;
        }
        return i + (i2 - 1);
    }

    public static final AnonymousClass15Q A01(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass15Q.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new AnonymousClass15Q(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public static void A02(AnonymousClass15Q r9) {
        if (!r9.A06) {
            AnonymousClass00K A012 = AnonymousClass00J.A01(r9.A07);
            int max = Math.max(1, A012.A0M);
            r9.A00 = max;
            if (A012.A01 > 0.0d) {
            }
            r9.A04 = A012.A1E;
            r9.A02 = A012.A0H;
            r9.A05 = A012.A1C;
            r9.A01 = Math.max(1, 4 / max);
            r9.A06 = true;
        }
    }

    private AnonymousClass15Q(AnonymousClass1XY r3) {
        this.A07 = AnonymousClass1YA.A02(r3);
        C008307n.A00(r3);
        C06460bX.A00(r3);
    }

    public double A03(int i, boolean z) {
        A02(this);
        int A002 = A00(this, i, z);
        boolean z2 = false;
        if (A002 >= 4) {
            z2 = true;
        }
        if (z2) {
            return ((double) Math.round((((double) A002) * 100.0d) / ((double) 4))) / 100.0d;
        }
        return 0.0d;
    }
}
