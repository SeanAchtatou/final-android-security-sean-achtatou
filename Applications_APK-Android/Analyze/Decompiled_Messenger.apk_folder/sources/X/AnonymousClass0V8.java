package X;

import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0V8  reason: invalid class name */
public final class AnonymousClass0V8 {
    private static volatile AnonymousClass0V8 A02;
    public final ArrayList A00;
    public final HashMap A01;

    public static final AnonymousClass0V8 A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass0V8.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        String str = AnonymousClass00J.A01(AnonymousClass1YA.A02(r4.getApplicationInjector())).A0l;
                        AnonymousClass00K.A00(str);
                        Preconditions.checkNotNull(str);
                        A02 = new AnonymousClass0V8(str);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0069, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.os.HandlerThread A01(java.lang.String r6, int r7, X.AnonymousClass0V6 r8) {
        /*
            r5 = this;
            java.util.HashMap r0 = r5.A01
            if (r0 != 0) goto L_0x000e
            java.util.ArrayList r0 = r5.A00
            if (r0 != 0) goto L_0x000e
            r0 = 0
            android.os.HandlerThread r0 = r8.AUc(r6, r7, r0)
            return r0
        L_0x000e:
            r4 = r5
            monitor-enter(r4)
            r3 = 0
            java.util.HashMap r0 = r5.A01     // Catch:{ all -> 0x006a }
            if (r0 == 0) goto L_0x001b
            java.lang.Object r3 = r0.get(r6)     // Catch:{ all -> 0x006a }
            X.43t r3 = (X.C855243t) r3     // Catch:{ all -> 0x006a }
        L_0x001b:
            if (r3 != 0) goto L_0x0040
            java.util.ArrayList r0 = r5.A00     // Catch:{ all -> 0x006a }
            r3 = 0
            if (r0 == 0) goto L_0x0040
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x006a }
        L_0x0026:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x006a }
            if (r0 == 0) goto L_0x0040
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x006a }
            android.util.Pair r1 = (android.util.Pair) r1     // Catch:{ all -> 0x006a }
            java.lang.Object r0 = r1.first     // Catch:{ all -> 0x006a }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x006a }
            boolean r0 = r6.startsWith(r0)     // Catch:{ all -> 0x006a }
            if (r0 == 0) goto L_0x0026
            java.lang.Object r3 = r1.second     // Catch:{ all -> 0x006a }
            X.43t r3 = (X.C855243t) r3     // Catch:{ all -> 0x006a }
        L_0x0040:
            if (r3 != 0) goto L_0x0049
            r0 = 0
            android.os.HandlerThread r1 = r8.AUc(r6, r7, r0)     // Catch:{ all -> 0x006a }
            monitor-exit(r4)
            return r1
        L_0x0049:
            java.lang.ref.WeakReference r0 = r3.A00     // Catch:{ all -> 0x006a }
            if (r0 != 0) goto L_0x004e
            goto L_0x0055
        L_0x004e:
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x006a }
            android.os.HandlerThread r1 = (android.os.HandlerThread) r1     // Catch:{ all -> 0x006a }
            goto L_0x0056
        L_0x0055:
            r1 = 0
        L_0x0056:
            if (r1 != 0) goto L_0x0068
            java.lang.String r2 = r3.A02     // Catch:{ all -> 0x006a }
            int r1 = r3.A01     // Catch:{ all -> 0x006a }
            r0 = 1
            android.os.HandlerThread r1 = r8.AUc(r2, r1, r0)     // Catch:{ all -> 0x006a }
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x006a }
            r0.<init>(r1)     // Catch:{ all -> 0x006a }
            r3.A00 = r0     // Catch:{ all -> 0x006a }
        L_0x0068:
            monitor-exit(r4)
            return r1
        L_0x006a:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0V8.A01(java.lang.String, int, X.0V6):android.os.HandlerThread");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(11:10|11|12|13|14|15|16|(4:19|(2:21|33)(2:22|32)|23|17)|31|24|8) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0049 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private AnonymousClass0V8(java.lang.String r12) {
        /*
            r11 = this;
            r11.<init>()
            com.google.common.base.Preconditions.checkNotNull(r12)
            boolean r0 = r12.isEmpty()
            r3 = 0
            if (r0 == 0) goto L_0x001d
            r0 = r3
        L_0x000e:
            if (r0 == 0) goto L_0x00a3
            java.lang.Object r1 = r0.first
            java.util.HashMap r1 = (java.util.HashMap) r1
            r11.A01 = r1
            java.lang.Object r0 = r0.second
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            r11.A00 = r0
            return
        L_0x001d:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x009a }
            r1.<init>(r12)     // Catch:{ Exception -> 0x009a }
            r0 = 59
            java.lang.String r0 = X.C99084oO.$const$string(r0)     // Catch:{ Exception -> 0x009a }
            org.json.JSONArray r9 = r1.getJSONArray(r0)     // Catch:{ Exception -> 0x009a }
            java.util.HashMap r8 = new java.util.HashMap     // Catch:{ Exception -> 0x009a }
            r8.<init>()     // Catch:{ Exception -> 0x009a }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ Exception -> 0x009a }
            r7.<init>()     // Catch:{ Exception -> 0x009a }
            r6 = 0
            r5 = 0
        L_0x0038:
            int r0 = r9.length()     // Catch:{ Exception -> 0x009a }
            if (r5 >= r0) goto L_0x008c
            org.json.JSONObject r2 = r9.getJSONObject(r5)     // Catch:{ Exception -> 0x009a }
            java.lang.String r0 = "priority"
            int r1 = r2.getInt(r0)     // Catch:{ JSONException -> 0x0049 }
            goto L_0x004d
        L_0x0049:
            X.0V7 r0 = X.AnonymousClass0V7.BACKGROUND     // Catch:{ Exception -> 0x009a }
            int r1 = r0.mAndroidThreadPriority     // Catch:{ Exception -> 0x009a }
        L_0x004d:
            X.43t r4 = new X.43t     // Catch:{ Exception -> 0x009a }
            java.lang.String r0 = "name"
            java.lang.String r0 = r2.getString(r0)     // Catch:{ Exception -> 0x009a }
            r4.<init>(r0, r1)     // Catch:{ Exception -> 0x009a }
            java.lang.String r0 = "handlers"
            org.json.JSONArray r2 = r2.getJSONArray(r0)     // Catch:{ Exception -> 0x009a }
            r1 = 0
        L_0x005f:
            int r0 = r2.length()     // Catch:{ Exception -> 0x009a }
            if (r1 >= r0) goto L_0x0089
            java.lang.String r10 = r2.getString(r1)     // Catch:{ Exception -> 0x009a }
            java.lang.String r0 = "*"
            boolean r0 = r10.endsWith(r0)     // Catch:{ Exception -> 0x009a }
            if (r0 == 0) goto L_0x0083
            int r0 = r10.length()     // Catch:{ Exception -> 0x009a }
            int r0 = r0 + -1
            java.lang.String r0 = r10.substring(r6, r0)     // Catch:{ Exception -> 0x009a }
            android.util.Pair r0 = android.util.Pair.create(r0, r4)     // Catch:{ Exception -> 0x009a }
            r7.add(r0)     // Catch:{ Exception -> 0x009a }
            goto L_0x0086
        L_0x0083:
            r8.put(r10, r4)     // Catch:{ Exception -> 0x009a }
        L_0x0086:
            int r1 = r1 + 1
            goto L_0x005f
        L_0x0089:
            int r5 = r5 + 1
            goto L_0x0038
        L_0x008c:
            X.4dP r0 = new X.4dP     // Catch:{ Exception -> 0x009a }
            r0.<init>()     // Catch:{ Exception -> 0x009a }
            java.util.Collections.sort(r7, r0)     // Catch:{ Exception -> 0x009a }
            android.util.Pair r0 = android.util.Pair.create(r8, r7)     // Catch:{ Exception -> 0x009a }
            goto L_0x000e
        L_0x009a:
            r2 = move-exception
            java.lang.String r1 = "FbHandlerThreadCache"
            r0 = 0
            X.C179868Tg.A00(r1, r3, r2)
            goto L_0x000e
        L_0x00a3:
            r11.A01 = r3
            r11.A00 = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0V8.<init>(java.lang.String):void");
    }
}
