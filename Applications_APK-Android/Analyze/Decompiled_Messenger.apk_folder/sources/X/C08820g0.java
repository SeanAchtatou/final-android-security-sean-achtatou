package X;

import java.util.HashMap;

/* renamed from: X.0g0  reason: invalid class name and case insensitive filesystem */
public final class C08820g0 extends HashMap<String, String> {
    public final /* synthetic */ C08710fp this$0;
    public final /* synthetic */ String val$locale;
    public final /* synthetic */ String val$userId;

    public C08820g0(C08710fp r3, String str, String str2) {
        this.this$0 = r3;
        this.val$locale = str;
        this.val$userId = str2;
        put("qt_locale", str);
        put("qt_user_id", this.val$userId);
    }
}
