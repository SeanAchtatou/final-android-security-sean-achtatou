package X;

import android.util.Log;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.common.dextricks.DexStore;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1o8  reason: invalid class name and case insensitive filesystem */
public abstract class C33781o8 {
    private static final List A0I = Collections.emptyList();
    public int A00;
    public int A01 = -1;
    public int A02 = -1;
    public int A03 = -1;
    public int A04 = -1;
    public int A05 = -1;
    public int A06 = 0;
    public long A07 = -1;
    public C15740vp A08 = null;
    public C33781o8 A09 = null;
    public C33781o8 A0A = null;
    public RecyclerView A0B;
    public WeakReference A0C;
    public List A0D = null;
    public List A0E = null;
    public boolean A0F = false;
    private int A0G = 0;
    public final View A0H;

    public void A07() {
        this.A00 = 0;
        this.A04 = -1;
        this.A02 = -1;
        this.A07 = -1;
        this.A05 = -1;
        this.A0G = 0;
        this.A09 = null;
        this.A0A = null;
        List list = this.A0D;
        if (list != null) {
            list.clear();
        }
        this.A00 &= -1025;
        this.A06 = 0;
        this.A03 = -1;
        RecyclerView.A0H(this);
    }

    public final void A0A(boolean z) {
        int i;
        int i2 = this.A0G;
        if (z) {
            i = i2 - 1;
        } else {
            i = i2 + 1;
        }
        this.A0G = i;
        if (i < 0) {
            this.A0G = 0;
            Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
        } else if (!z && i == 1) {
            this.A00 |= 16;
        } else if (z && i == 0) {
            this.A00 &= -17;
        }
    }

    public final int A04() {
        RecyclerView recyclerView = this.A0B;
        if (recyclerView == null) {
            return -1;
        }
        return recyclerView.A0X(this);
    }

    public final int A05() {
        int i = this.A05;
        if (i == -1) {
            return this.A04;
        }
        return i;
    }

    public List A06() {
        List list;
        if ((this.A00 & 1024) != 0 || (list = this.A0D) == null || list.size() == 0) {
            return A0I;
        }
        return this.A0E;
    }

    public void A08(int i, boolean z) {
        if (this.A02 == -1) {
            this.A02 = this.A04;
        }
        if (this.A05 == -1) {
            this.A05 = this.A04;
        }
        if (z) {
            this.A05 += i;
        }
        this.A04 += i;
        if (this.A0H.getLayoutParams() != null) {
            ((AnonymousClass1R7) this.A0H.getLayoutParams()).A01 = true;
        }
    }

    public void A09(Object obj) {
        if (obj == null) {
            this.A00 = 1024 | this.A00;
        } else if ((1024 & this.A00) == 0) {
            if (this.A0D == null) {
                ArrayList arrayList = new ArrayList();
                this.A0D = arrayList;
                this.A0E = Collections.unmodifiableList(arrayList);
            }
            this.A0D.add(obj);
        }
    }

    public boolean A0B() {
        if ((this.A00 & DexStore.LOAD_RESULT_OATMEAL_QUICKENED) != 0) {
            return true;
        }
        return false;
    }

    public boolean A0C() {
        if (this.A0H.getParent() == null || this.A0H.getParent() == this.A0B) {
            return false;
        }
        return true;
    }

    public boolean A0D() {
        if ((this.A00 & 4) != 0) {
            return true;
        }
        return false;
    }

    public boolean A0E() {
        if ((this.A00 & 8) != 0) {
            return true;
        }
        return false;
    }

    public boolean A0F() {
        if ((this.A00 & 128) != 0) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ac, code lost:
        if (X.C15320v6.hasTransientState(r4.A0H) != false) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00c9, code lost:
        if (A0D() != false) goto L_0x00cb;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r4 = this;
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "ViewHolder{"
            r2.<init>(r0)
            int r0 = r4.hashCode()
            java.lang.String r0 = java.lang.Integer.toHexString(r0)
            r2.append(r0)
            java.lang.String r0 = " position="
            r2.append(r0)
            int r0 = r4.A04
            r2.append(r0)
            java.lang.String r0 = " id="
            r2.append(r0)
            long r0 = r4.A07
            r2.append(r0)
            java.lang.String r0 = ", oldPos="
            r2.append(r0)
            int r0 = r4.A02
            r2.append(r0)
            java.lang.String r0 = ", pLpos:"
            r2.append(r0)
            int r0 = r4.A05
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r3.<init>(r0)
            X.0vp r1 = r4.A08
            r0 = 0
            if (r1 == 0) goto L_0x0049
            r0 = 1
        L_0x0049:
            if (r0 == 0) goto L_0x0059
            java.lang.String r0 = " scrap "
            r3.append(r0)
            boolean r0 = r4.A0F
            if (r0 == 0) goto L_0x00ea
            java.lang.String r0 = "[changeScrap]"
        L_0x0056:
            r3.append(r0)
        L_0x0059:
            boolean r0 = r4.A0D()
            if (r0 == 0) goto L_0x0064
            java.lang.String r0 = " invalid"
            r3.append(r0)
        L_0x0064:
            int r2 = r4.A00
            r1 = 1
            r0 = r2 & r1
            if (r0 != 0) goto L_0x006c
            r1 = 0
        L_0x006c:
            if (r1 != 0) goto L_0x0073
            java.lang.String r0 = " unbound"
            r3.append(r0)
        L_0x0073:
            r1 = r2 & 2
            r0 = 0
            if (r1 == 0) goto L_0x0079
            r0 = 1
        L_0x0079:
            if (r0 == 0) goto L_0x0080
            java.lang.String r0 = " update"
            r3.append(r0)
        L_0x0080:
            boolean r0 = r4.A0E()
            if (r0 == 0) goto L_0x008b
            java.lang.String r0 = " removed"
            r3.append(r0)
        L_0x008b:
            boolean r0 = r4.A0F()
            if (r0 == 0) goto L_0x0096
            java.lang.String r0 = " ignored"
            r3.append(r0)
        L_0x0096:
            boolean r0 = r4.A0B()
            if (r0 == 0) goto L_0x00a1
            java.lang.String r0 = " tmpDetached"
            r3.append(r0)
        L_0x00a1:
            r0 = r2 & 16
            if (r0 != 0) goto L_0x00ae
            android.view.View r0 = r4.A0H
            boolean r1 = X.C15320v6.hasTransientState(r0)
            r0 = 1
            if (r1 == 0) goto L_0x00af
        L_0x00ae:
            r0 = 0
        L_0x00af:
            if (r0 != 0) goto L_0x00be
            java.lang.String r2 = " not recyclable("
            int r1 = r4.A0G
            java.lang.String r0 = ")"
            java.lang.String r0 = X.AnonymousClass08S.A0A(r2, r1, r0)
            r3.append(r0)
        L_0x00be:
            int r0 = r4.A00
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 != 0) goto L_0x00cb
            boolean r1 = r4.A0D()
            r0 = 0
            if (r1 == 0) goto L_0x00cc
        L_0x00cb:
            r0 = 1
        L_0x00cc:
            if (r0 == 0) goto L_0x00d3
            java.lang.String r0 = " undefined adapter position"
            r3.append(r0)
        L_0x00d3:
            android.view.View r0 = r4.A0H
            android.view.ViewParent r0 = r0.getParent()
            if (r0 != 0) goto L_0x00e0
            java.lang.String r0 = " no parent"
            r3.append(r0)
        L_0x00e0:
            java.lang.String r0 = "}"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            return r0
        L_0x00ea:
            java.lang.String r0 = "[attachedScrap]"
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33781o8.toString():java.lang.String");
    }

    public C33781o8(View view) {
        if (view != null) {
            this.A0H = view;
            return;
        }
        throw new IllegalArgumentException("itemView may not be null");
    }
}
