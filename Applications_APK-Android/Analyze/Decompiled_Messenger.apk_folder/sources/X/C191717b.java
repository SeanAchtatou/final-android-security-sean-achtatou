package X;

import android.os.RemoteException;
import com.fasterxml.jackson.databind.JsonNode;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.17b  reason: invalid class name and case insensitive filesystem */
public final class C191717b {
    private static volatile C191717b A01;
    private final C29281gA A00;

    public static final C191717b A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C191717b.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C191717b(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public int A01(String str, JsonNode jsonNode, Integer num, C57332ro r6) {
        C36991uR BvK;
        try {
            BvK = this.A00.BvK();
            int A03 = BvK.A03(str, C06850cB.A0G(jsonNode.toString()), num, r6);
            BvK.A06();
            return A03;
        } catch (RemoteException unused) {
            return -1;
        } catch (Throwable th) {
            BvK.A06();
            throw th;
        }
    }

    public int A02(String str, byte[] bArr, Integer num, C57332ro r6) {
        C36991uR BvK;
        try {
            BvK = this.A00.BvK();
            int A03 = BvK.A03(str, bArr, num, r6);
            BvK.A06();
            return A03;
        } catch (RemoteException unused) {
            return -1;
        } catch (Throwable th) {
            BvK.A06();
            throw th;
        }
    }

    private C191717b(AnonymousClass1XY r2) {
        this.A00 = C24381Tk.A00(r2);
    }
}
