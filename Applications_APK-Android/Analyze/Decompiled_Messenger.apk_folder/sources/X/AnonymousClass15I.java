package X;

import android.content.Context;

/* renamed from: X.15I  reason: invalid class name */
public final class AnonymousClass15I {
    public final int A00;
    public final Context A01;
    public final C06410bS A02;
    public final AnonymousClass15A A03;
    public final C06320bJ A04;
    public final C07300dC A05;
    public final C07330dF A06;
    public final C06390bQ A07;
    public final Class A08;
    public final String A09;

    public AnonymousClass15I(Context context, int i, String str, C06320bJ r4, C06410bS r5, C06390bQ r6, AnonymousClass15A r7, C07330dF r8, Class cls, C07300dC r10) {
        this.A01 = context;
        this.A04 = r4;
        this.A02 = r5;
        this.A07 = r6;
        this.A06 = r8;
        this.A08 = cls;
        this.A03 = r7;
        this.A00 = i;
        this.A09 = str;
        this.A05 = r10;
    }
}
