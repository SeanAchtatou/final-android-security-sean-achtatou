package X;

import android.os.PowerManager;

/* renamed from: X.0Al  reason: invalid class name and case insensitive filesystem */
public final class C01490Al {
    public static final C01490Al A01 = new C01490Al();
    private final PowerManager A00;

    public AnonymousClass0CU A00() {
        PowerManager powerManager = this.A00;
        if (powerManager == null) {
            return AnonymousClass0CU.A01;
        }
        AnonymousClass0CU r3 = new AnonymousClass0CU(powerManager);
        try {
            PowerManager.WakeLock wakeLock = r3.A00;
            if (wakeLock == null) {
                return r3;
            }
            C009007v.A02(wakeLock, 60000);
            return r3;
        } catch (Throwable unused) {
            return r3;
        }
    }

    private C01490Al() {
        this.A00 = null;
    }

    public C01490Al(C01420Ad r3) {
        C01540Aq A002 = r3.A00("power", PowerManager.class);
        if (A002.A02()) {
            this.A00 = (PowerManager) A002.A01();
            return;
        }
        throw new IllegalArgumentException("Cannot acquire power service");
    }
}
