package X;

/* renamed from: X.0zM  reason: invalid class name and case insensitive filesystem */
public final class C17720zM {
    public short A00;
    public final Object[] A01;

    private static String A00(int i) {
        if (i == 0) {
            return AnonymousClass80H.$const$string(85);
        }
        if (i == 1) {
            return "BACKGROUND";
        }
        if (i == 2) {
            return "FOREGROUND";
        }
        if (i == 3) {
            return "HOST";
        }
        if (i != 4) {
            return null;
        }
        return "BORDER";
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                C17720zM r6 = (C17720zM) obj;
                if (this.A00 == r6.A00) {
                    int i = 0;
                    while (true) {
                        Object[] objArr = this.A01;
                        if (i < objArr.length) {
                            if (objArr[i] != r6.A01[i]) {
                                break;
                            }
                            i++;
                        } else {
                            break;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int A01(int i) {
        if (i < 0 || i >= this.A00) {
            throw new IndexOutOfBoundsException(AnonymousClass08S.A0B("index=", i, ", size=", this.A00));
        }
        int i2 = 0;
        int i3 = 0;
        while (i2 <= i) {
            if (this.A01[i3] != null) {
                i2++;
            }
            i3++;
        }
        return i3 - 1;
    }

    public Object A02() {
        Object[] objArr = this.A01;
        Object obj = objArr[3];
        if (obj == null && (obj = objArr[0]) == null && (obj = objArr[1]) == null && (obj = objArr[2]) == null) {
            return objArr[4];
        }
        return obj;
    }

    public void A04(int i, Object obj) {
        if (obj != null) {
            Object[] objArr = this.A01;
            if (objArr[i] != null) {
                throw new RuntimeException(AnonymousClass08S.A0J("Already contains unit for type ", A00(i)));
            } else if (objArr[3] != null || (i == 3 && this.A00 > 0)) {
                throw new RuntimeException("OutputUnitType.HOST unit should be the only member of an OutputUnitsAffinityGroup");
            } else {
                objArr[i] = obj;
                this.A00 = (short) (this.A00 + 1);
            }
        } else {
            throw new IllegalArgumentException("value should not be null");
        }
    }

    public void A05(int i, Object obj) {
        if (obj != null) {
            Object[] objArr = this.A01;
            if (objArr[i] != null) {
                objArr[i] = obj;
                return;
            }
        }
        if (obj != null && this.A01[i] == null) {
            A04(i, obj);
        } else if (obj == null) {
            Object[] objArr2 = this.A01;
            if (objArr2[i] != null) {
                objArr2[i] = null;
                this.A00 = (short) (this.A00 - 1);
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());
        for (int i = 0; i < this.A00; i++) {
            int A012 = A01(i);
            Object A03 = A03(i);
            sb.append("\n\t");
            sb.append(A00(A012));
            sb.append(": ");
            sb.append(A03.toString());
        }
        return sb.toString();
    }

    public Object A03(int i) {
        return this.A01[A01(i)];
    }

    public C17720zM() {
        this.A01 = new Object[5];
        this.A00 = 0;
    }

    public C17720zM(C17720zM r5) {
        Object[] objArr = new Object[5];
        this.A01 = objArr;
        this.A00 = 0;
        for (int i = 0; i < 5; i++) {
            objArr[i] = r5.A01[i];
        }
        this.A00 = r5.A00;
    }
}
