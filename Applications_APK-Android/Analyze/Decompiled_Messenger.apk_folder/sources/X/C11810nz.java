package X;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.GuavaOptionalDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.HashMultisetDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.ImmutableBiMapDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.ImmutableListDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.ImmutableMapDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.ImmutableMultisetDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.ImmutableSetDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.ImmutableSortedMapDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.ImmutableSortedSetDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.LinkedHashMultisetDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.MultimapDeserializer;
import com.fasterxml.jackson.datatype.guava.deser.TreeMultisetDeserializer;
import com.google.common.base.Optional;
import com.google.common.collect.EnumBiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.EnumMultiset;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.LinkedHashMultiset;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.TreeMultiset;
import java.lang.reflect.Method;
import java.util.Iterator;

/* renamed from: X.0nz  reason: invalid class name and case insensitive filesystem */
public final class C11810nz extends C11820o0 {
    public JsonDeserializer findMapLikeDeserializer(C21991Jm r12, C10490kF r13, C10120ja r14, C422628y r15, C64433Bp r16, JsonDeserializer jsonDeserializer) {
        Method method;
        C21991Jm r6 = r12;
        Class cls = r12._class;
        if (ImmutableMultimap.class.isAssignableFrom(cls)) {
            ImmutableListMultimap.class.isAssignableFrom(cls);
            ImmutableSetMultimap.class.isAssignableFrom(cls);
        }
        Class<AnonymousClass0V0> cls2 = AnonymousClass0V0.class;
        if (cls2.isAssignableFrom(cls)) {
            Class<LinkedListMultimap> cls3 = r12._class;
            Method method2 = null;
            if (cls3 != LinkedListMultimap.class && cls3 != AnonymousClass0V1.class && cls3 != cls2) {
                Iterator it = MultimapDeserializer.METHOD_NAMES.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        Iterator it2 = MultimapDeserializer.METHOD_NAMES.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            try {
                                method = cls3.getMethod((String) it2.next(), cls2);
                                if (method != null) {
                                    break;
                                }
                            } catch (NoSuchMethodException unused) {
                            }
                        }
                    } else {
                        try {
                            method = cls3.getMethod((String) it.next(), cls2);
                            if (method != null) {
                                break;
                            }
                        } catch (NoSuchMethodException unused2) {
                        }
                    }
                }
                method2 = method;
            }
            return new MultimapDeserializer(r6, r15, r16, jsonDeserializer, method2);
        }
        AnonymousClass3K9.class.isAssignableFrom(cls);
        return null;
    }

    public JsonDeserializer findBeanDeserializer(C10030jR r3, C10490kF r4, C10120ja r5) {
        if (Optional.class.isAssignableFrom(r3._class)) {
            return new GuavaOptionalDeserializer(r3);
        }
        return super.findBeanDeserializer(r3, r4, r5);
    }

    public JsonDeserializer findCollectionDeserializer(C28331ed r5, C10490kF r6, C10120ja r7, C64433Bp r8, JsonDeserializer jsonDeserializer) {
        Class cls = r5._class;
        if (ImmutableCollection.class.isAssignableFrom(cls)) {
            if (!ImmutableList.class.isAssignableFrom(cls)) {
                if (ImmutableMultiset.class.isAssignableFrom(cls)) {
                    return new ImmutableMultisetDeserializer(r5, r8, jsonDeserializer);
                }
                if (ImmutableSet.class.isAssignableFrom(cls)) {
                    if (!ImmutableSortedSet.class.isAssignableFrom(cls)) {
                        return new ImmutableSetDeserializer(r5, r8, jsonDeserializer);
                    }
                    if (Comparable.class.isAssignableFrom(r5.getContentType()._class)) {
                        return new ImmutableSortedSetDeserializer(r5, r8, jsonDeserializer);
                    }
                    throw new IllegalArgumentException(AnonymousClass08S.A0P("Can not handle ImmutableSortedSet with elements that are not Comparable<?> (", cls.getName(), ")"));
                }
            }
            return new ImmutableListDeserializer(r5, r8, jsonDeserializer);
        } else if (!AnonymousClass0UG.class.isAssignableFrom(cls)) {
            return null;
        } else {
            if (LinkedHashMultiset.class.isAssignableFrom(cls)) {
                return new LinkedHashMultisetDeserializer(r5, r8, jsonDeserializer);
            }
            if (!HashMultiset.class.isAssignableFrom(cls)) {
                EnumMultiset.class.isAssignableFrom(cls);
                if (TreeMultiset.class.isAssignableFrom(cls)) {
                    return new TreeMultisetDeserializer(r5, r8, jsonDeserializer);
                }
            }
            return new HashMultisetDeserializer(r5, r8, jsonDeserializer);
        }
    }

    public JsonDeserializer findMapDeserializer(C180610a r3, C10490kF r4, C10120ja r5, C422628y r6, C64433Bp r7, JsonDeserializer jsonDeserializer) {
        Class cls = r3._class;
        if (ImmutableMap.class.isAssignableFrom(cls)) {
            if (ImmutableSortedMap.class.isAssignableFrom(cls)) {
                return new ImmutableSortedMapDeserializer(r3, r6, r7, jsonDeserializer);
            }
            if (ImmutableBiMap.class.isAssignableFrom(cls)) {
                return new ImmutableBiMapDeserializer(r3, r6, r7, jsonDeserializer);
            }
            return new ImmutableMapDeserializer(r3, r6, r7, jsonDeserializer);
        } else if (!C38501xX.class.isAssignableFrom(cls)) {
            return null;
        } else {
            EnumBiMap.class.isAssignableFrom(cls);
            EnumHashBiMap.class.isAssignableFrom(cls);
            HashBiMap.class.isAssignableFrom(cls);
            return null;
        }
    }
}
