package X;

/* renamed from: X.1fb  reason: invalid class name and case insensitive filesystem */
public final class C28931fb {
    public static int A02(int i, long j) {
        return (i * 31) + ((int) (j ^ (j >>> 32)));
    }

    public static int A04(int i, boolean z) {
        int i2 = i * 31;
        int i3 = 1237;
        if (z) {
            i3 = AnonymousClass1Y3.AA5;
        }
        return i2 + i3;
    }

    public static int A01(int i, float f) {
        return (i * 31) + Float.floatToIntBits(f);
    }

    public static int A03(int i, Object obj) {
        int hashCode;
        int i2 = i * 31;
        if (obj == null) {
            hashCode = 0;
        } else {
            hashCode = obj.hashCode();
        }
        return i2 + hashCode;
    }

    public static void A05(Object obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
    }

    public static void A06(Object obj, String str) {
        if (obj == null) {
            throw new NullPointerException(AnonymousClass08S.A0J(str, " is null"));
        }
    }

    public static boolean A07(Object obj, Object obj2) {
        if (obj == obj2) {
            return true;
        }
        if (obj == null || !obj.equals(obj2)) {
            return false;
        }
        return true;
    }

    public static int A00(int i, double d) {
        long doubleToLongBits = Double.doubleToLongBits(d);
        return (i * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)));
    }
}
