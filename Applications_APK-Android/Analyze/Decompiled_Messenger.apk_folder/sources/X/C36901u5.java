package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.send.SendError;

/* renamed from: X.1u5  reason: invalid class name and case insensitive filesystem */
public final class C36901u5 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new SendError(parcel);
    }

    public Object[] newArray(int i) {
        return new SendError[i];
    }
}
