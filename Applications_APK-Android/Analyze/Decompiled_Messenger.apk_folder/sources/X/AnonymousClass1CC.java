package X;

import com.google.common.base.Function;

/* renamed from: X.1CC  reason: invalid class name */
public final class AnonymousClass1CC implements Function {
    public Object apply(Object obj) {
        return ((Iterable) obj).iterator();
    }
}
