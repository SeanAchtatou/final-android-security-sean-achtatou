package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0wP  reason: invalid class name and case insensitive filesystem */
public final class C16050wP implements C16090wT {
    public final /* synthetic */ RecyclerView A00;

    public void BOE(C33781o8 r7) {
        boolean z;
        r7.A0A(true);
        if (r7.A09 != null && r7.A0A == null) {
            r7.A09 = null;
        }
        r7.A0A = null;
        boolean z2 = false;
        if ((r7.A00 & 16) != 0) {
            z2 = true;
        }
        if (!z2) {
            RecyclerView recyclerView = this.A00;
            View view = r7.A0H;
            RecyclerView.A0M(recyclerView);
            C16220wh r3 = recyclerView.A0H;
            int BCT = r3.A01.BCT(view);
            if (BCT == -1) {
                C16220wh.A01(r3, view);
                z = true;
            } else {
                C16250wk r1 = r3.A00;
                if (r1.A06(BCT)) {
                    r1.A07(BCT);
                    C16220wh.A01(r3, view);
                    r3.A01.C27(BCT);
                    z = true;
                } else {
                    z = false;
                }
            }
            if (z) {
                C33781o8 A05 = RecyclerView.A05(view);
                recyclerView.A0w.A0A(A05);
                recyclerView.A0w.A09(A05);
            }
            RecyclerView.A0S(recyclerView, !z);
            if (!z && r7.A0B()) {
                this.A00.removeDetachedView(r7.A0H, false);
            }
        }
    }

    public C16050wP(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }
}
