package X;

import android.database.sqlite.SQLiteDatabase;
import com.facebook.omnistore.OmnistoreIOException;

/* renamed from: X.1lC  reason: invalid class name and case insensitive filesystem */
public final class C32131lC {
    public final SQLiteDatabase A00;

    public SQLiteDatabase A00() {
        if (this.A00.isOpen()) {
            return this.A00;
        }
        throw new OmnistoreIOException("SQLite database is closed");
    }

    public C32131lC(SQLiteDatabase sQLiteDatabase) {
        this.A00 = sQLiteDatabase;
    }
}
