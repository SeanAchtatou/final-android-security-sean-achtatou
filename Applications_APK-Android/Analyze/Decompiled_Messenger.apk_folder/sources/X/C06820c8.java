package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0c8  reason: invalid class name and case insensitive filesystem */
public final class C06820c8 extends C06940cL {
    private static volatile C06820c8 A02;
    public AnonymousClass0UN A00;
    public boolean A01 = false;

    public static final C06820c8 A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C06820c8.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C06820c8(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C06820c8(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(8, r3);
    }
}
