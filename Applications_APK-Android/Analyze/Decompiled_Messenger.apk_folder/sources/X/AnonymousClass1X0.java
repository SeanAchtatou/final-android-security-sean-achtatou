package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.1X0  reason: invalid class name */
public class AnonymousClass1X0 extends Handler {
    public AnonymousClass1X0() {
    }

    public AnonymousClass1X0(Looper looper) {
        super(looper);
    }

    public AnonymousClass1X0(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
