package X;

import android.text.Layout;

/* renamed from: X.1S1  reason: invalid class name */
public interface AnonymousClass1S1 {
    Layout getLayout();

    CharSequence getText();

    float getTextSize();

    int getTotalPaddingLeft();

    int getTotalPaddingTop();
}
