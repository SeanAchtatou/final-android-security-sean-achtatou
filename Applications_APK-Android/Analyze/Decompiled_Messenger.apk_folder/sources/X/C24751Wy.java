package X;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.os.Messenger;
import android.util.Log;
import com.google.firebase.iid.zzm;

/* renamed from: X.1Wy  reason: invalid class name and case insensitive filesystem */
public final class C24751Wy {
    public static int A06;
    public static PendingIntent A07;
    public Messenger A00;
    public zzm A01;
    private Messenger A02;
    public final Context A03;
    public final AnonymousClass04b A04 = new AnonymousClass04b();
    public final C24701Wt A05;

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00e7 A[SYNTHETIC, Splitter:B:48:0x00e7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final android.os.Bundle A00(android.os.Bundle r9) {
        /*
            r8 = this;
            java.lang.Class<X.1Wy> r2 = X.C24751Wy.class
            monitor-enter(r2)
            int r1 = X.C24751Wy.A06     // Catch:{ all -> 0x0121 }
            int r0 = r1 + 1
            X.C24751Wy.A06 = r0     // Catch:{ all -> 0x0121 }
            java.lang.String r3 = java.lang.Integer.toString(r1)     // Catch:{ all -> 0x0121 }
            monitor-exit(r2)
            X.2kE r6 = new X.2kE
            r6.<init>()
            X.04b r1 = r8.A04
            monitor-enter(r1)
            X.04b r0 = r8.A04     // Catch:{ all -> 0x011e }
            r0.put(r3, r6)     // Catch:{ all -> 0x011e }
            monitor-exit(r1)     // Catch:{ all -> 0x011e }
            X.1Wt r0 = r8.A05
            int r0 = r0.A03()
            if (r0 == 0) goto L_0x0116
            android.content.Intent r4 = new android.content.Intent
            r4.<init>()
            java.lang.String r0 = "com.google.android.gms"
            r4.setPackage(r0)
            X.1Wt r0 = r8.A05
            int r0 = r0.A03()
            r7 = 2
            if (r0 != r7) goto L_0x0043
            java.lang.String r0 = "com.google.iid.TOKEN_REQUEST"
            r4.setAction(r0)
        L_0x003c:
            r4.putExtras(r9)
            android.content.Context r5 = r8.A03
            monitor-enter(r2)
            goto L_0x0049
        L_0x0043:
            java.lang.String r0 = "com.google.android.c2dm.intent.REGISTER"
            r4.setAction(r0)
            goto L_0x003c
        L_0x0049:
            android.app.PendingIntent r0 = X.C24751Wy.A07     // Catch:{ all -> 0x0121 }
            if (r0 != 0) goto L_0x005e
            android.content.Intent r1 = new android.content.Intent     // Catch:{ all -> 0x0121 }
            r1.<init>()     // Catch:{ all -> 0x0121 }
            java.lang.String r0 = "com.google.example.invalidpackage"
            r1.setPackage(r0)     // Catch:{ all -> 0x0121 }
            r0 = 0
            android.app.PendingIntent r0 = android.app.PendingIntent.getBroadcast(r5, r0, r1, r0)     // Catch:{ all -> 0x0121 }
            X.C24751Wy.A07 = r0     // Catch:{ all -> 0x0121 }
        L_0x005e:
            java.lang.String r1 = "app"
            android.app.PendingIntent r0 = X.C24751Wy.A07     // Catch:{ all -> 0x0121 }
            r4.putExtra(r1, r0)     // Catch:{ all -> 0x0121 }
            monitor-exit(r2)
            java.lang.String r0 = java.lang.String.valueOf(r3)
            int r0 = r0.length()
            int r0 = r0 + 5
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "|ID|"
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = "|"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "kid"
            r4.putExtra(r0, r1)
            r0 = 3
            java.lang.String r5 = "FirebaseInstanceId"
            boolean r0 = android.util.Log.isLoggable(r5, r0)
            if (r0 == 0) goto L_0x009b
            android.os.Bundle r0 = r4.getExtras()
            java.lang.String.valueOf(r0)
        L_0x009b:
            android.os.Messenger r1 = r8.A02
            java.lang.String r0 = "google.messenger"
            r4.putExtra(r0, r1)
            android.os.Messenger r0 = r8.A00
            if (r0 != 0) goto L_0x00aa
            com.google.firebase.iid.zzm r0 = r8.A01
            if (r0 == 0) goto L_0x00c4
        L_0x00aa:
            android.os.Message r2 = android.os.Message.obtain()
            r2.obj = r4
            android.os.Messenger r0 = r8.A00     // Catch:{ RemoteException -> 0x00c4 }
            if (r0 != 0) goto L_0x00c0
            com.google.firebase.iid.zzm r1 = r8.A01     // Catch:{ RemoteException -> 0x00c4 }
            android.os.Messenger r0 = r1.A00     // Catch:{ RemoteException -> 0x00c4 }
            if (r0 != 0) goto L_0x00c0
            com.google.firebase.iid.zzu r0 = r1.A01     // Catch:{ RemoteException -> 0x00c4 }
            r0.C4v(r2)     // Catch:{ RemoteException -> 0x00c4 }
            goto L_0x00d1
        L_0x00c0:
            r0.send(r2)     // Catch:{ RemoteException -> 0x00c4 }
            goto L_0x00d1
        L_0x00c4:
            X.1Wt r0 = r8.A05
            int r0 = r0.A03()
            if (r0 != r7) goto L_0x00d2
            android.content.Context r0 = r8.A03
            r0.sendBroadcast(r4)
        L_0x00d1:
            goto L_0x00d8
        L_0x00d2:
            android.content.Context r0 = r8.A03
            r0.startService(r4)
            goto L_0x00d1
        L_0x00d8:
            X.2kC r4 = r6.A00     // Catch:{ InterruptedException | TimeoutException -> 0x00f8, ExecutionException -> 0x00f1 }
            r1 = 30000(0x7530, double:1.4822E-319)
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException | TimeoutException -> 0x00f8, ExecutionException -> 0x00f1 }
            java.lang.Object r2 = X.C53232kQ.A01(r4, r1, r0)     // Catch:{ InterruptedException | TimeoutException -> 0x00f8, ExecutionException -> 0x00f1 }
            android.os.Bundle r2 = (android.os.Bundle) r2     // Catch:{ InterruptedException | TimeoutException -> 0x00f8, ExecutionException -> 0x00f1 }
            X.04b r1 = r8.A04
            monitor-enter(r1)
            X.04b r0 = r8.A04     // Catch:{ all -> 0x00ee }
            r0.remove(r3)     // Catch:{ all -> 0x00ee }
            monitor-exit(r1)     // Catch:{ all -> 0x00ee }
            return r2
        L_0x00ee:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ee }
            goto L_0x0120
        L_0x00f1:
            r1 = move-exception
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0109 }
            r0.<init>(r1)     // Catch:{ all -> 0x0109 }
            throw r0     // Catch:{ all -> 0x0109 }
        L_0x00f8:
            java.lang.String r0 = "No response"
            android.util.Log.w(r5, r0)     // Catch:{ all -> 0x0109 }
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0109 }
            r0 = 127(0x7f, float:1.78E-43)
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ all -> 0x0109 }
            r1.<init>(r0)     // Catch:{ all -> 0x0109 }
            throw r1     // Catch:{ all -> 0x0109 }
        L_0x0109:
            r2 = move-exception
            X.04b r1 = r8.A04
            monitor-enter(r1)
            X.04b r0 = r8.A04     // Catch:{ all -> 0x0114 }
            r0.remove(r3)     // Catch:{ all -> 0x0114 }
        L_0x0112:
            monitor-exit(r1)     // Catch:{ all -> 0x0114 }
            goto L_0x0120
        L_0x0114:
            r2 = move-exception
            goto L_0x0112
        L_0x0116:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r0 = "MISSING_INSTANCEID_SERVICE"
            r1.<init>(r0)
            throw r1
        L_0x011e:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x011e }
        L_0x0120:
            throw r2
        L_0x0121:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24751Wy.A00(android.os.Bundle):android.os.Bundle");
    }

    public static final void A02(C24751Wy r5, String str, Bundle bundle) {
        String str2;
        synchronized (r5.A04) {
            C53122kE r0 = (C53122kE) r5.A04.remove(str);
            if (r0 == null) {
                String valueOf = String.valueOf(str);
                if (valueOf.length() != 0) {
                    str2 = "Missing callback for ".concat(valueOf);
                } else {
                    str2 = new String("Missing callback for ");
                }
                Log.w("FirebaseInstanceId", str2);
            } else {
                r0.A00.A0K(bundle);
            }
        }
    }

    public C24751Wy(Context context, C24701Wt r5) {
        this.A03 = context;
        this.A05 = r5;
        this.A02 = new Messenger(new C24761Wz(this, Looper.getMainLooper()));
    }

    public static final Bundle A01(C24751Wy r3, Bundle bundle) {
        Bundle A002 = r3.A00(bundle);
        if (A002 == null || !A002.containsKey("google.messenger")) {
            return A002;
        }
        Bundle A003 = r3.A00(bundle);
        if (A003 == null || !A003.containsKey("google.messenger")) {
            return A003;
        }
        return null;
    }
}
