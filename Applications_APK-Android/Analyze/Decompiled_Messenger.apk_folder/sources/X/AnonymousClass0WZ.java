package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0WZ  reason: invalid class name */
public final class AnonymousClass0WZ {
    private static volatile AnonymousClass0WZ A05;
    public AnonymousClass0UN A00;
    public final C04310Tq A01;
    public final C04310Tq A02;
    private final AnonymousClass0US A03;
    private final C04310Tq A04;

    public synchronized void A03(String str) {
        int i;
        int i2;
        String str2;
        String str3;
        if (A01(str)) {
            i = 12;
            i2 = AnonymousClass1Y3.AWK;
        } else {
            i = 13;
            i2 = AnonymousClass1Y3.AOJ;
        }
        C25041Yc r4 = (C25041Yc) ((C25051Yd) AnonymousClass1XX.A02(i, i2, this.A00));
        AnonymousClass0US r0 = this.A03;
        if (r0 != null) {
            r4.A0N = r0;
        }
        int i3 = 13631491;
        if (A01(str)) {
            i3 = 13631492;
        }
        QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBd, this.A00);
        boolean z = false;
        if (r4.A0B.get()) {
            z = true;
        }
        quickPerformanceLogger.markerAnnotate(i3, "JavaManagerAlreadyInited", z);
        r4.A09();
        QuickPerformanceLogger quickPerformanceLogger2 = (QuickPerformanceLogger) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBd, this.A00);
        if (r4.A0B.get()) {
            str2 = "true";
        } else {
            str2 = "false";
        }
        quickPerformanceLogger2.markerPoint(i3, AnonymousClass08S.A0J("initJavaManagerSuccessful: ", str2));
        if (r4.A0B.get()) {
            AnonymousClass07A.A04((C06480bZ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ACz, this.A00), new C09690iJ(r4), 551392312);
        } else {
            r4.A08();
        }
        QuickPerformanceLogger quickPerformanceLogger3 = (QuickPerformanceLogger) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBd, this.A00);
        if (r4.A0B.get()) {
            str3 = "scheduled";
        } else {
            str3 = "done";
        }
        quickPerformanceLogger3.markerPoint(i3, AnonymousClass08S.A0J("C++ manager process: ", str3));
    }

    public static final AnonymousClass0WZ A00(AnonymousClass1XY r5) {
        if (A05 == null) {
            synchronized (AnonymousClass0WZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A05 = new AnonymousClass0WZ(applicationInjector, C04750Wa.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static boolean A01(String str) {
        if (str == null || str.isEmpty()) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:40|(2:42|43)|44|45) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:32|33|34|35|36) */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a0, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00a4 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x00ad */
    /* JADX WARNING: Missing exception handler attribute for start block: B:46:0x00ae */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0214  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.AnonymousClass0WV A02(java.lang.String r22) {
        /*
            r21 = this;
            r2 = r21
            monitor-enter(r21)
            X.00M r0 = X.AnonymousClass00M.A00()     // Catch:{ all -> 0x0219 }
            boolean r0 = r0.A05()     // Catch:{ all -> 0x0219 }
            r4 = 1
            r18 = r0 ^ 1
            r3 = 2
            r7 = 13631489(0xd00001, float:1.9101785E-38)
            r8 = 8
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x01fc }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x01fc }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x01fc }
            com.facebook.quicklog.QuickPerformanceLogger r6 = (com.facebook.quicklog.QuickPerformanceLogger) r6     // Catch:{ all -> 0x01fc }
            boolean r0 = A01(r22)     // Catch:{ all -> 0x01fc }
            r5 = 2
            if (r0 == 0) goto L_0x0026
            r5 = 1
        L_0x0026:
            java.lang.String r1 = "MODE"
            boolean r0 = A01(r22)     // Catch:{ all -> 0x01fc }
            if (r0 == 0) goto L_0x0079
            java.lang.String r0 = "SESSIONLESS"
        L_0x0030:
            r6.markerStart(r7, r5, r1, r0)     // Catch:{ all -> 0x01fc }
            X.0Tq r0 = r2.A04     // Catch:{ all -> 0x01fc }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x01fc }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x01fc }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ all -> 0x01fc }
            java.io.File r6 = r0.getFilesDir()     // Catch:{ all -> 0x01fc }
            X.0Tq r0 = r2.A04     // Catch:{ all -> 0x01fc }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x01fc }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x01fc }
            android.content.res.AssetManager r9 = r0.getAssets()     // Catch:{ all -> 0x01fc }
            if (r9 == 0) goto L_0x00ae
            r0 = r22
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x01fc }
            java.lang.String r0 = X.C36811tq.A00(r6, r0)     // Catch:{ all -> 0x01fc }
            r1.<init>(r0)     // Catch:{ all -> 0x01fc }
            r1.mkdir()     // Catch:{ all -> 0x01fc }
            java.io.File r5 = new java.io.File     // Catch:{ all -> 0x01fc }
            r0 = r22
            java.lang.String r6 = X.C36811tq.A00(r6, r0)     // Catch:{ all -> 0x01fc }
            java.lang.String r1 = "/"
            java.lang.String r0 = "spec_to_param.txt"
            java.lang.String r1 = X.AnonymousClass08S.A0P(r6, r1, r0)     // Catch:{ all -> 0x01fc }
            java.lang.String r0 = ".tmp"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x01fc }
            r5.<init>(r0)     // Catch:{ all -> 0x01fc }
            goto L_0x007c
        L_0x0079:
            java.lang.String r0 = "SESSION_BASED"
            goto L_0x0030
        L_0x007c:
            java.lang.String r0 = "spec_to_param.txt"
            java.io.InputStream r6 = r9.open(r0)     // Catch:{ IOException -> 0x00ae }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ all -> 0x00a5 }
            r9.<init>(r5)     // Catch:{ all -> 0x00a5 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r5 = new byte[r0]     // Catch:{ all -> 0x009e }
        L_0x008b:
            int r1 = r6.read(r5)     // Catch:{ all -> 0x009e }
            r0 = -1
            if (r1 == r0) goto L_0x0097
            r0 = 0
            r9.write(r5, r0, r1)     // Catch:{ all -> 0x009e }
            goto L_0x008b
        L_0x0097:
            r9.close()     // Catch:{ all -> 0x00a5 }
            r6.close()     // Catch:{ IOException -> 0x00ae }
            goto L_0x00ae
        L_0x009e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a0 }
        L_0x00a0:
            r0 = move-exception
            r9.close()     // Catch:{ all -> 0x00a4 }
        L_0x00a4:
            throw r0     // Catch:{ all -> 0x00a5 }
        L_0x00a5:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a7 }
        L_0x00a7:
            r0 = move-exception
            if (r6 == 0) goto L_0x00ad
            r6.close()     // Catch:{ all -> 0x00ad }
        L_0x00ad:
            throw r0     // Catch:{ IOException -> 0x00ae }
        L_0x00ae:
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x01fc }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x01fc }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r8, r1, r0)     // Catch:{ all -> 0x01fc }
            com.facebook.quicklog.QuickPerformanceLogger r5 = (com.facebook.quicklog.QuickPerformanceLogger) r5     // Catch:{ all -> 0x01fc }
            boolean r0 = A01(r22)     // Catch:{ all -> 0x01fc }
            r1 = 2
            if (r0 == 0) goto L_0x00c0
            r1 = 1
        L_0x00c0:
            java.lang.String r0 = "copiedSpecToHash"
            r5.markerPoint(r7, r1, r0)     // Catch:{ all -> 0x01fc }
            com.facebook.mobileconfig.MobileConfigManagerHolderImpl r7 = new com.facebook.mobileconfig.MobileConfigManagerHolderImpl     // Catch:{ all -> 0x01fc }
            X.0Tq r0 = r2.A04     // Catch:{ all -> 0x01fc }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x01fc }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x01fc }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ all -> 0x01fc }
            java.io.File r8 = r0.getFilesDir()     // Catch:{ all -> 0x01fc }
            r9 = 0
            int r0 = X.AnonymousClass1Y3.BMV     // Catch:{ all -> 0x01fc }
            X.0UN r1 = r2.A00     // Catch:{ all -> 0x01fc }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r0, r1)     // Catch:{ all -> 0x01fc }
            X.0hF r0 = (X.C09400hF) r0     // Catch:{ all -> 0x01fc }
            java.lang.String r10 = r0.A02()     // Catch:{ all -> 0x01fc }
            int r0 = X.AnonymousClass1Y3.BEf     // Catch:{ all -> 0x01fc }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r0, r1)     // Catch:{ all -> 0x01fc }
            X.1ac r0 = (X.C25921ac) r0     // Catch:{ all -> 0x01fc }
            java.lang.String r11 = r0.B7Z()     // Catch:{ all -> 0x01fc }
            boolean r0 = A01(r22)     // Catch:{ all -> 0x01fc }
            r12 = 0
            if (r0 == 0) goto L_0x0105
            r3 = 11
            int r1 = X.AnonymousClass1Y3.BH7     // Catch:{ all -> 0x01fc }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x01fc }
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x01fc }
            com.facebook.tigon.nativeservice.NativeTigonServiceHolder r12 = (com.facebook.tigon.nativeservice.NativeTigonServiceHolder) r12     // Catch:{ all -> 0x01fc }
        L_0x0105:
            r13 = 0
            r3 = 3
            int r1 = X.AnonymousClass1Y3.BUN     // Catch:{ all -> 0x01fc }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x01fc }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x01fc }
            X.1aQ r0 = (X.C25801aQ) r0     // Catch:{ all -> 0x01fc }
            com.facebook.xanalytics.XAnalyticsHolder r14 = r0.BA9()     // Catch:{ all -> 0x01fc }
            java.lang.String r16 = ""
            X.0Tq r0 = r2.A04     // Catch:{ all -> 0x01fc }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x01fc }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x01fc }
            android.content.res.AssetManager r17 = r0.getAssets()     // Catch:{ all -> 0x01fc }
            r0 = r22
            boolean r0 = A01(r0)     // Catch:{ all -> 0x01fc }
            if (r0 == 0) goto L_0x0167
            X.0Tq r0 = r2.A02     // Catch:{ all -> 0x01fc }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x01fc }
            com.facebook.mobileconfig.MobileConfigManagerParamsHolder r1 = (com.facebook.mobileconfig.MobileConfigManagerParamsHolder) r1     // Catch:{ all -> 0x01fc }
        L_0x0133:
            r6 = r22
            int r3 = X.AnonymousClass1Y3.A9d     // Catch:{ all -> 0x01fc }
            X.0UN r4 = r2.A00     // Catch:{ all -> 0x01fc }
            r0 = 4
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r3, r4)     // Catch:{ all -> 0x01fc }
            X.0ZS r5 = (X.AnonymousClass0ZS) r5     // Catch:{ all -> 0x01fc }
            int r3 = X.AnonymousClass1Y3.ADx     // Catch:{ all -> 0x01fc }
            r0 = 5
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r3, r4)     // Catch:{ all -> 0x01fc }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x01fc }
            boolean r6 = A01(r6)     // Catch:{ all -> 0x01fc }
            r0 = 0
            if (r6 == 0) goto L_0x0159
            r6 = 6
            int r0 = X.AnonymousClass1Y3.Abx     // Catch:{ all -> 0x01fc }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r0, r4)     // Catch:{ all -> 0x01fc }
            X.00x r0 = (X.C001300x) r0     // Catch:{ all -> 0x01fc }
        L_0x0159:
            com.google.common.collect.ImmutableMap$Builder r4 = com.google.common.collect.ImmutableMap.builder()     // Catch:{ all -> 0x01fc }
            java.lang.String r6 = r5.A03()     // Catch:{ all -> 0x01fc }
            java.lang.String r5 = "locale"
            r4.put(r5, r6)     // Catch:{ all -> 0x01fc }
            goto L_0x0170
        L_0x0167:
            X.0Tq r0 = r2.A01     // Catch:{ all -> 0x01fc }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x01fc }
            com.facebook.mobileconfig.MobileConfigManagerParamsHolder r1 = (com.facebook.mobileconfig.MobileConfigManagerParamsHolder) r1     // Catch:{ all -> 0x01fc }
            goto L_0x0133
        L_0x0170:
            if (r3 == 0) goto L_0x017b
            r5 = 886(0x376, float:1.242E-42)
            java.lang.String r5 = X.AnonymousClass24B.$const$string(r5)     // Catch:{ all -> 0x01fc }
            r4.put(r5, r3)     // Catch:{ all -> 0x01fc }
        L_0x017b:
            if (r0 == 0) goto L_0x018c
            java.lang.String r5 = r0.A04     // Catch:{ all -> 0x01fc }
            r3 = 124(0x7c, float:1.74E-43)
            java.lang.String r0 = r0.A05     // Catch:{ all -> 0x01fc }
            java.lang.String r3 = X.AnonymousClass08S.A07(r5, r3, r0)     // Catch:{ all -> 0x01fc }
            java.lang.String r0 = "access_token"
            r4.put(r0, r3)     // Catch:{ all -> 0x01fc }
        L_0x018c:
            com.google.common.collect.ImmutableMap r20 = r4.build()     // Catch:{ all -> 0x01fc }
            r15 = r22
            r19 = r1
            r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)     // Catch:{ all -> 0x01fc }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x01fc }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x01fc }
            r6 = 8
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x01fa }
            com.facebook.quicklog.QuickPerformanceLogger r3 = (com.facebook.quicklog.QuickPerformanceLogger) r3     // Catch:{ all -> 0x01fa }
            boolean r0 = A01(r22)     // Catch:{ all -> 0x01fa }
            r1 = 2
            if (r0 == 0) goto L_0x01ab
            r1 = 1
        L_0x01ab:
            java.lang.String r0 = "createdManager"
            r5 = 13631489(0xd00001, float:1.9101785E-38)
            r3.markerPoint(r5, r1, r0)     // Catch:{ all -> 0x01f8 }
            boolean r0 = A01(r22)     // Catch:{ all -> 0x01f8 }
            if (r0 != 0) goto L_0x01e0
            boolean r0 = r7.isFetchNeeded()     // Catch:{ all -> 0x01f8 }
            if (r0 == 0) goto L_0x01e0
            java.lang.String r1 = "AppUpgrade"
            java.lang.String r0 = r7.syncFetchReason()     // Catch:{ all -> 0x01f8 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x01f8 }
            if (r0 == 0) goto L_0x01e0
            int r1 = X.AnonymousClass1Y3.ACz     // Catch:{ all -> 0x01f8 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x01f8 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r13, r1, r0)     // Catch:{ all -> 0x01f8 }
            X.0bZ r3 = (X.C06480bZ) r3     // Catch:{ all -> 0x01f8 }
            X.95b r1 = new X.95b     // Catch:{ all -> 0x01f8 }
            r1.<init>(r2, r7)     // Catch:{ all -> 0x01f8 }
            r0 = -1510312039(0xffffffffa5fa7799, float:-4.344913E-16)
            X.AnonymousClass07A.A04(r3, r1, r0)     // Catch:{ all -> 0x01f8 }
        L_0x01e0:
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0219 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0219 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x0219 }
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2     // Catch:{ all -> 0x0219 }
            boolean r0 = A01(r22)     // Catch:{ all -> 0x0219 }
            r1 = 2
            if (r0 == 0) goto L_0x01f2
            r1 = 1
        L_0x01f2:
            r0 = 2
            r2.markerEnd(r5, r1, r0)     // Catch:{ all -> 0x0219 }
            monitor-exit(r21)
            return r7
        L_0x01f8:
            r4 = move-exception
            goto L_0x0202
        L_0x01fa:
            r4 = move-exception
            goto L_0x01ff
        L_0x01fc:
            r4 = move-exception
            r6 = 8
        L_0x01ff:
            r5 = 13631489(0xd00001, float:1.9101785E-38)
        L_0x0202:
            r3 = 2
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x0219 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0219 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x0219 }
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2     // Catch:{ all -> 0x0219 }
            boolean r1 = A01(r22)     // Catch:{ all -> 0x0219 }
            r0 = 2
            if (r1 == 0) goto L_0x0215
            r0 = 1
        L_0x0215:
            r2.markerEnd(r5, r0, r3)     // Catch:{ all -> 0x0219 }
            throw r4     // Catch:{ all -> 0x0219 }
        L_0x0219:
            r0 = move-exception
            monitor-exit(r21)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0WZ.A02(java.lang.String):X.0WV");
    }

    private AnonymousClass0WZ(AnonymousClass1XY r3, AnonymousClass0US r4) {
        this.A00 = new AnonymousClass0UN(14, r3);
        this.A04 = AnonymousClass0VG.A00(AnonymousClass1Y3.BCt, r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.AWj, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.AOh, r3);
        this.A03 = r4;
    }
}
