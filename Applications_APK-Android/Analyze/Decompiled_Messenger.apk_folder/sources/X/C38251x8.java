package X;

import java.util.concurrent.Callable;

/* renamed from: X.1x8  reason: invalid class name and case insensitive filesystem */
public final /* synthetic */ class C38251x8 implements Callable {
    private final C196269Ks A00;

    public C38251x8(C196269Ks r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0061, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        X.C196269Ks.A00(r1, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0065, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0068, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0069, code lost:
        if (r3 != null) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        X.C196269Ks.A00(r1, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006e, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object call() {
        /*
            r7 = this;
            X.9Ks r5 = r7.A00
            java.net.URL r0 = r5.A01
            java.lang.String.valueOf(r0)
            java.lang.String r4 = "FirebaseMessaging"
            java.net.URL r0 = r5.A01     // Catch:{ IOException -> 0x006f }
            java.net.URLConnection r1 = r0.openConnection()     // Catch:{ IOException -> 0x006f }
            r0 = -1305589951(0xffffffffb22e4741, float:-1.0144334E-8)
            java.io.InputStream r3 = X.AnonymousClass0EN.A00(r1, r0)     // Catch:{ IOException -> 0x006f }
            X.94r r6 = new X.94r     // Catch:{ all -> 0x0066 }
            r6.<init>(r3)     // Catch:{ all -> 0x0066 }
            r5.A02 = r3     // Catch:{ all -> 0x005f }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r6)     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0039
            r0 = 3
            boolean r0 = android.util.Log.isLoggable(r4, r0)     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x002f
            java.net.URL r0 = r5.A01     // Catch:{ all -> 0x005f }
            java.lang.String.valueOf(r0)     // Catch:{ all -> 0x005f }
        L_0x002f:
            r0 = 0
            X.C196269Ks.A00(r0, r6)     // Catch:{ all -> 0x0066 }
            if (r3 == 0) goto L_0x0038
            X.C196269Ks.A00(r0, r3)     // Catch:{ IOException -> 0x006f }
        L_0x0038:
            return r1
        L_0x0039:
            java.net.URL r0 = r5.A01     // Catch:{ all -> 0x005f }
            java.lang.String r2 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x005f }
            int r0 = r2.length()     // Catch:{ all -> 0x005f }
            int r0 = r0 + 24
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x005f }
            r1.<init>(r0)     // Catch:{ all -> 0x005f }
            java.lang.String r0 = "Failed to decode image: "
            r1.append(r0)     // Catch:{ all -> 0x005f }
            r1.append(r2)     // Catch:{ all -> 0x005f }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x005f }
            android.util.Log.w(r4, r1)     // Catch:{ all -> 0x005f }
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x005f }
            r0.<init>(r1)     // Catch:{ all -> 0x005f }
            throw r0     // Catch:{ all -> 0x005f }
        L_0x005f:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0061 }
        L_0x0061:
            r0 = move-exception
            X.C196269Ks.A00(r1, r6)     // Catch:{ all -> 0x0066 }
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0068 }
        L_0x0068:
            r0 = move-exception
            if (r3 == 0) goto L_0x006e
            X.C196269Ks.A00(r1, r3)     // Catch:{ IOException -> 0x006f }
        L_0x006e:
            throw r0     // Catch:{ IOException -> 0x006f }
        L_0x006f:
            r3 = move-exception
            java.net.URL r0 = r5.A01
            java.lang.String r2 = java.lang.String.valueOf(r0)
            int r0 = r2.length()
            int r0 = r0 + 26
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Failed to download image: "
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            android.util.Log.w(r4, r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38251x8.call():java.lang.Object");
    }
}
