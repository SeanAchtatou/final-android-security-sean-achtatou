package X;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps$EntryFunction;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.0TG  reason: invalid class name */
public final class AnonymousClass0TG {
    public static int A00(int i) {
        if (i < 3) {
            C25001Xy.A00(i, AnonymousClass24B.$const$string(AnonymousClass1Y3.A86));
            return i + 1;
        } else if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        } else {
            return Integer.MAX_VALUE;
        }
    }

    public static Predicate A01(Predicate predicate) {
        return new Predicates.CompositionPredicate(predicate, Maps$EntryFunction.VALUE);
    }

    public static HashMap A04() {
        return new HashMap();
    }

    public static Iterator A05(Iterator it) {
        Maps$EntryFunction maps$EntryFunction = Maps$EntryFunction.VALUE;
        Preconditions.checkNotNull(maps$EntryFunction);
        return new C26991cT(it, maps$EntryFunction);
    }

    public static ConcurrentMap A07() {
        return new C07940eQ().A02();
    }

    public static ImmutableMap A02(Iterable iterable, Function function) {
        Preconditions.checkNotNull(function);
        ImmutableMap.Builder builder = ImmutableMap.builder();
        for (Object next : iterable) {
            builder.put(function.apply(next), next);
        }
        try {
            return builder.build();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(AnonymousClass08S.A0J(e.getMessage(), ". To index multiple values under a key, use Multimaps.index."));
        }
    }

    public static Object A03(Map map, Object obj) {
        Preconditions.checkNotNull(map);
        try {
            return map.get(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return null;
        }
    }

    public static Map A06(Map map, Predicate predicate) {
        Predicate A01 = A01(predicate);
        Preconditions.checkNotNull(A01);
        if (map instanceof C21091Fe) {
            C21091Fe r4 = (C21091Fe) map;
            return new C21111Fg(r4.A01, Predicates.and(r4.A00, A01));
        }
        Preconditions.checkNotNull(map);
        return new C21111Fg(map, A01);
    }
}
