package X;

import com.google.common.base.Platform;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.Callable;

/* renamed from: X.1FA  reason: invalid class name */
public final class AnonymousClass1FA implements Callable {
    public final /* synthetic */ C189016a A00;

    public AnonymousClass1FA(C189016a r1) {
        this.A00 = r1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
    public Object call() {
        ImmutableList immutableList;
        ImmutableList immutableList2;
        ImmutableList immutableList3;
        ImmutableList immutableList4;
        ImmutableList immutableList5;
        C34801qC r11;
        ImmutableList immutableList6;
        ImmutableList immutableList7;
        ImmutableList immutableList8;
        ImmutableList immutableList9;
        ImmutableList immutableList10;
        ImmutableList immutableList11;
        ImmutableList immutableList12;
        ImmutableList immutableList13;
        ImmutableList immutableList14;
        ImmutableList immutableList15;
        ImmutableList immutableList16;
        ImmutableList immutableList17;
        ImmutableList immutableList18;
        ImmutableList immutableList19;
        int i;
        C189016a r0 = this.A00;
        boolean z = false;
        if (r0.A08.A02(AnonymousClass2C6.A03, -1) <= 0) {
            z = true;
        }
        boolean z2 = false;
        if (z) {
            z2 = true;
        }
        C005505z.A03("ANL:ContactsLoader:loadLocalData", 1167033778);
        try {
            HashMap A04 = AnonymousClass0TG.A04();
            C91694Zm r2 = (C91694Zm) AnonymousClass1XX.A03(AnonymousClass1Y3.BSF, r0.A02);
            C34801qC r39 = null;
            if (r0.A03.A01.contains(AnonymousClass16Y.A0E)) {
                C005505z.A03("getTopFriends", -510438941);
                try {
                    int i2 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r5 = r0.A02;
                    C42532Av r7 = (C42532Av) AnonymousClass1XX.A02(7, i2, r5);
                    C42562Ay r6 = (C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r5);
                    ImmutableList immutableList20 = AnonymousClass2B4.A02;
                    int i3 = r0.A03.A00;
                    if (i3 == -1) {
                        i3 = C855143s.A00;
                    }
                    ImmutableList A01 = r7.A01(r6.A08(immutableList20, i3));
                    C005505z.A00(710983332);
                    immutableList = C189016a.A04(A04, A01, false);
                } catch (Throwable th) {
                    th = th;
                    C005505z.A00(918159761);
                    throw th;
                }
            } else {
                immutableList = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A04) || r0.A03.A01.contains(AnonymousClass16Y.A0F)) {
                C005505z.A03("getTopOnMessenger", 1519900146);
                try {
                    int i4 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r62 = r0.A02;
                    C42572Az A03 = ((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r62)).A03();
                    A03.A03 = AnonymousClass2B4.A02;
                    A03.A05 = true;
                    A03.A08 = true;
                    A03.A0A = true;
                    A03.A01 = AnonymousClass2B0.A00;
                    A03.A0D = true;
                    A03.A00 = 15;
                    ImmutableList A012 = ((C42532Av) AnonymousClass1XX.A02(7, i4, r62)).A01(A03);
                    C005505z.A00(-1262336037);
                    immutableList2 = C189016a.A04(A04, A012, false);
                    synchronized (r2) {
                        r2.A04 = immutableList2;
                        r2.A01 = r2.A06.now();
                    }
                } catch (Throwable th2) {
                    th = th2;
                    C005505z.A00(-1361897082);
                    throw th;
                }
            } else {
                immutableList2 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A05)) {
                C005505z.A03("getNotOnMessengerFriends", -975779785);
                ImmutableList.Builder builder = ImmutableList.builder();
                int i5 = AnonymousClass1Y3.BEw;
                AnonymousClass0UN r4 = r0.A02;
                C42572Az A032 = ((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r4)).A03();
                A032.A03 = AnonymousClass2B4.A02;
                A032.A05 = true;
                A032.A06 = true;
                A032.A0A = true;
                A032.A01 = AnonymousClass2B0.A00;
                A032.A0D = true;
                builder.addAll((Iterable) ((C42532Av) AnonymousClass1XX.A02(7, i5, r4)).A01(A032));
                int i6 = AnonymousClass1Y3.BEw;
                AnonymousClass0UN r42 = r0.A02;
                C42572Az A033 = ((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r42)).A03();
                A033.A03 = AnonymousClass2B4.A02;
                A033.A05 = true;
                A033.A06 = true;
                A033.A0A = true;
                A033.A01 = AnonymousClass2B0.A03;
                A033.A09 = true;
                builder.addAll((Iterable) ((C42532Av) AnonymousClass1XX.A02(7, i6, r42)).A01(A033));
                C005505z.A00(1175565419);
                immutableList3 = C189016a.A04(A04, builder.build(), true);
                synchronized (r2) {
                    r2.A02 = immutableList3;
                    r2.A00 = r2.A06.now();
                }
            } else {
                immutableList3 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A08)) {
                int i7 = r0.A03.A00;
                C005505z.A03("getPHATContacts", -307932753);
                try {
                    int i8 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r72 = r0.A02;
                    C42572Az A034 = ((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r72)).A03();
                    A034.A03 = AnonymousClass2B4.A02;
                    A034.A05 = true;
                    A034.A08 = true;
                    A034.A0A = true;
                    A034.A01 = AnonymousClass2B0.A05;
                    A034.A0D = true;
                    A034.A00 = i7;
                    ImmutableList A013 = ((C42532Av) AnonymousClass1XX.A02(7, i8, r72)).A01(A034);
                    C005505z.A00(1543331559);
                    immutableList4 = C189016a.A04(A04, A013, false);
                } catch (Throwable th3) {
                    th = th3;
                    C005505z.A00(-594386720);
                    throw th;
                }
            } else {
                immutableList4 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A0D)) {
                C005505z.A03("getTopContacts", -174324762);
                try {
                    int i9 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r52 = r0.A02;
                    C42532Av r63 = (C42532Av) AnonymousClass1XX.A02(7, i9, r52);
                    C42562Ay r53 = (C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r52);
                    int i10 = r0.A03.A00;
                    if (i10 == -1) {
                        i10 = 15;
                    }
                    ImmutableList A014 = r63.A01(r53.A05(i10));
                    C005505z.A00(-791540113);
                    immutableList5 = C189016a.A04(A04, A014, false);
                } catch (Throwable th4) {
                    th = th4;
                    C005505z.A00(-246144224);
                    throw th;
                }
            } else {
                immutableList5 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A0I)) {
                C005505z.A03("getTopRtcContacts", -1379901593);
                try {
                    AnonymousClass754 A09 = ((AnonymousClass4I0) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A4l, r0.A02)).A00.A09(AnonymousClass230.A0J);
                    if (A09 == null || (immutableList19 = A09.A00) == null) {
                        immutableList19 = RegularImmutableList.A02;
                    }
                    if (!immutableList19.isEmpty()) {
                        Collection A002 = AnonymousClass0TH.A00(immutableList19, new AnonymousClass74V());
                        int i11 = AnonymousClass1Y3.A4R;
                        AnonymousClass0UN r73 = r0.A02;
                        r11 = ((C406822n) AnonymousClass1XX.A02(3, i11, r73)).A03(new C49522cQ(((AnonymousClass79M) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AnX, r73)).A02(A002), AnonymousClass334.A00, AnonymousClass230.A0J, null, true));
                        i = -1258563680;
                    } else {
                        r11 = C34801qC.A03;
                        i = 321180592;
                    }
                    C005505z.A00(i);
                } catch (Throwable th5) {
                    th = th5;
                    C005505z.A00(-799180249);
                    throw th;
                }
            } else {
                r11 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A06)) {
                C005505z.A03("getOnlineFriendsSortedByCoefficient", 509540677);
                try {
                    Collection A02 = AnonymousClass0r6.A02(r0.A09, 450, -1);
                    int i12 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r64 = r0.A02;
                    ImmutableList A015 = ((C42532Av) AnonymousClass1XX.A02(7, i12, r64)).A01(((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r64)).A07(A02, 15));
                    C005505z.A00(439323766);
                    immutableList6 = C189016a.A04(A04, A015, false);
                } catch (Throwable th6) {
                    th = th6;
                    C005505z.A00(888902953);
                    throw th;
                }
            } else {
                immutableList6 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A01)) {
                immutableList7 = C189016a.A02(r0, A04, false);
            } else {
                immutableList7 = null;
            }
            if (immutableList7 == null && r0.A03.A01.contains(AnonymousClass16Y.A02)) {
                immutableList7 = C189016a.A02(r0, A04, true);
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A0A)) {
                immutableList8 = C189016a.A03(r0, A04, true);
                r2.A03(immutableList8);
            } else if (r0.A03.A01.contains(AnonymousClass16Y.A09)) {
                immutableList8 = C189016a.A03(r0, A04, false);
                r2.A03(immutableList8);
            } else {
                immutableList8 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A0G)) {
                immutableList9 = C189016a.A01(r0, AnonymousClass07B.A0C);
            } else {
                immutableList9 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A0H)) {
                immutableList9 = C189016a.A01(r0, AnonymousClass07B.A01);
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A0B)) {
                C005505z.A03("getSpecificUsers", -1015759571);
                ImmutableList.Builder builder2 = ImmutableList.builder();
                try {
                    AnonymousClass79M r43 = (AnonymousClass79M) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AnX, r0.A02);
                    ImmutableList immutableList21 = r43.A01;
                    if (immutableList21 == null) {
                        immutableList18 = RegularImmutableList.A02;
                    } else {
                        immutableList18 = r43.A02(AnonymousClass0TH.A00(immutableList21, new AnonymousClass74V()));
                    }
                    if (immutableList18 != null) {
                        int i13 = r0.A03.A00;
                        if (i13 == -1) {
                            i13 = 20;
                        }
                        if (immutableList18.size() > i13) {
                            immutableList18 = immutableList18.subList(0, i13);
                        }
                        builder2.addAll((Iterable) immutableList18);
                    }
                    C005505z.A00(-844238599);
                    immutableList10 = C189016a.A04(A04, builder2.build(), false);
                } catch (Throwable th7) {
                    th = th7;
                    C005505z.A00(2096661216);
                    throw th;
                }
            } else {
                immutableList10 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A03)) {
                int i14 = r0.A03.A00;
                C005505z.A03("getFavoriteMessengerContacts", -1903106807);
                try {
                    int i15 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r54 = r0.A02;
                    ImmutableList A016 = ((C42532Av) AnonymousClass1XX.A02(7, i15, r54)).A01(((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r54)).A04(i14));
                    C005505z.A00(1637241490);
                    immutableList11 = C189016a.A04(A04, A016, false);
                } catch (Throwable th8) {
                    th = th8;
                    C005505z.A00(471037065);
                    throw th;
                }
            } else {
                immutableList11 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A0C)) {
                C005505z.A03("getTopBlockableContacts", 1570528892);
                try {
                    int i16 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r44 = r0.A02;
                    C42532Av r55 = (C42532Av) AnonymousClass1XX.A02(7, i16, r44);
                    C42562Ay r45 = (C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r44);
                    ImmutableList immutableList22 = AnonymousClass2B4.A00;
                    int i17 = r0.A03.A00;
                    if (i17 == -1) {
                        i17 = C855143s.A00;
                    }
                    C42572Az A08 = r45.A08(immutableList22, i17);
                    A08.A05 = true;
                    ImmutableList A017 = r55.A01(A08);
                    C005505z.A00(1008491202);
                    immutableList12 = C189016a.A04(A04, A017, false);
                } catch (Throwable th9) {
                    th = th9;
                    C005505z.A00(1158560567);
                    throw th;
                }
            } else {
                immutableList12 = null;
            }
            if (C013509w.A01(immutableList) || C013509w.A01(immutableList2) || C013509w.A01(immutableList6) || C013509w.A01(immutableList5) || ((r11 != null && C013509w.A01(r11.A00)) || C013509w.A01(immutableList3) || C013509w.A01(immutableList4) || C013509w.A01(immutableList8) || C013509w.A01(immutableList7) || C013509w.A01(immutableList9) || C013509w.A01(immutableList10) || C013509w.A01(immutableList11) || C013509w.A01(immutableList12))) {
                C189816i r19 = new C189816i(immutableList, null, null, immutableList6, null, immutableList2, immutableList3, immutableList4, immutableList5, r11, immutableList8, immutableList9, immutableList7, immutableList10, immutableList11, immutableList12, true);
                r0.A04 = r19;
                AnonymousClass07A.A04(r0.A0A, new C116895gR(r0, r19), 1567328503);
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A04)) {
                C005505z.A03("getOnMessenger", -1937043329);
                try {
                    int i18 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r46 = r0.A02;
                    C42572Az A035 = ((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r46)).A03();
                    A035.A03 = AnonymousClass2B4.A02;
                    A035.A05 = true;
                    A035.A08 = true;
                    A035.A0A = true;
                    A035.A0C = true;
                    A035.A01 = AnonymousClass2B0.A03;
                    ImmutableList A018 = ((C42532Av) AnonymousClass1XX.A02(7, i18, r46)).A01(A035);
                    C005505z.A00(777564615);
                    immutableList13 = C189016a.A04(A04, A018, false);
                } catch (Throwable th10) {
                    th = th10;
                    C005505z.A00(-1020698782);
                    throw th;
                }
            } else {
                immutableList13 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A06)) {
                C005505z.A03("getOnlineFriends", 1015867577);
                try {
                    Collection A022 = AnonymousClass0r6.A02(r0.A09, -1, -1);
                    int i19 = AnonymousClass1Y3.BEw;
                    AnonymousClass0UN r22 = r0.A02;
                    C42572Az A036 = ((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r22)).A03();
                    A036.A03 = AnonymousClass2B4.A02;
                    A036.A04 = A022;
                    A036.A01 = AnonymousClass2B0.A03;
                    ImmutableList A019 = ((C42532Av) AnonymousClass1XX.A02(7, i19, r22)).A01(A036);
                    C005505z.A00(-1176553367);
                    immutableList14 = C189016a.A04(A04, A019, false);
                } catch (Throwable th11) {
                    th = th11;
                    C005505z.A00(547469870);
                    throw th;
                }
            } else {
                immutableList14 = null;
            }
            if (r0.A03.A01.contains(AnonymousClass16Y.A07)) {
                ImmutableList copyOf = ImmutableList.copyOf(AnonymousClass0r6.A02(r0.A09, -1, -1));
                if (((AnonymousClass10R) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BO4, r0.A02)).A00.Aem(282325382071610L)) {
                    immutableList15 = ImmutableList.copyOf(r0.A09.A0P(0));
                } else {
                    immutableList15 = RegularImmutableList.A02;
                }
                String B4C = ((AnonymousClass10R) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BO4, r0.A02)).A00.B4C(845275337851007L);
                if (!((AnonymousClass10R) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BO4, r0.A02)).A00.Aem(282325384365394L) || Platform.stringIsNullOrEmpty(B4C)) {
                    immutableList16 = ((C406822n) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A4R, r0.A02)).A03(new C49522cQ(ImmutableList.copyOf((Collection) copyOf), AnonymousClass334.A01, AnonymousClass230.A09, null, true)).A00;
                    immutableList17 = ((C406822n) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A4R, r0.A02)).A03(new C49522cQ(ImmutableList.copyOf((Collection) immutableList15), AnonymousClass334.A01, AnonymousClass230.A09, null, true)).A00;
                } else {
                    immutableList16 = ((C76243lC) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B2l, r0.A02)).A01(new C76233lB(ImmutableList.copyOf((Collection) copyOf), AnonymousClass334.A01, B4C, true)).A00;
                    immutableList17 = ((C76243lC) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B2l, r0.A02)).A01(new C76233lB(ImmutableList.copyOf((Collection) immutableList15), AnonymousClass334.A01, B4C, true)).A00;
                }
                ImmutableList.Builder builder3 = new ImmutableList.Builder();
                int At0 = (int) ((AnonymousClass10R) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BO4, r0.A02)).A00.At0(563800358126159L);
                if (At0 <= 0) {
                    immutableList16 = RegularImmutableList.A02;
                } else if (immutableList16.size() > At0) {
                    immutableList16 = immutableList16.subList(0, At0);
                }
                builder3.addAll((Iterable) immutableList16);
                int At02 = (int) ((AnonymousClass10R) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BO4, r0.A02)).A00.At0(563800358191696L);
                if (At02 <= 0) {
                    immutableList17 = RegularImmutableList.A02;
                } else if (immutableList17.size() > At02) {
                    immutableList17 = immutableList17.subList(0, At02);
                }
                builder3.addAll((Iterable) immutableList17);
                ImmutableList build = builder3.build();
                ((C07380dK) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BQ3, r0.A02)).A04("active_now_users_omnistore_loading_counter_prefix:userkeys", (long) build.size(), "core_counters");
                int i20 = AnonymousClass1Y3.BEw;
                AnonymousClass0UN r23 = r0.A02;
                r39 = ((C406822n) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A4R, r0.A02)).A03(new C49522cQ(C189016a.A04(A04, ((C42532Av) AnonymousClass1XX.A02(7, i20, r23)).A01(((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r23)).A07(build, -1)), false), AnonymousClass334.A00, AnonymousClass230.A09, null, true));
            }
            C189816i r36 = new C189816i(immutableList, immutableList14, r39, immutableList6, immutableList13, immutableList2, immutableList3, immutableList4, immutableList5, r11, immutableList8, immutableList9, immutableList7, immutableList10, immutableList11, immutableList12, z2);
            C005505z.A00(-49529423);
            return r36;
        } catch (Throwable th12) {
            C005505z.A00(481424767);
            throw th12;
        }
    }
}
