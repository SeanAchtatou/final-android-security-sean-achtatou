package X;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

/* renamed from: X.0BB  reason: invalid class name */
public final class AnonymousClass0BB implements HostnameVerifier {
    public static final Pattern A00 = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    private static boolean A03(String str, String str2) {
        int length;
        int indexOf;
        int i;
        int length2;
        int length3;
        if (!(str == null || (length = str.length()) == 0 || str2 == null || str2.length() == 0)) {
            String lowerCase = str2.toLowerCase(Locale.US);
            if (!lowerCase.contains("*")) {
                return str.equals(lowerCase);
            }
            return (lowerCase.startsWith("*.") && str.regionMatches(0, lowerCase, 2, lowerCase.length() - 2)) || ((indexOf = lowerCase.indexOf(42)) <= lowerCase.indexOf(46) && str.regionMatches(0, lowerCase, 0, indexOf) && ((str.indexOf(46, indexOf) >= (length3 = length - (length2 = lowerCase.length() - (i = indexOf + 1))) || str.endsWith(".clients.google.com")) && str.regionMatches(length3, lowerCase, i, length2)));
        }
    }

    public boolean verify(String str, SSLSession sSLSession) {
        try {
            return A04(str, (X509Certificate) sSLSession.getPeerCertificates()[0]).A00;
        } catch (SSLException unused) {
            return false;
        }
    }

    public static C03880Qg A00(String str, String str2, List list) {
        C03880Qg r4 = new C03880Qg();
        String lowerCase = str.toLowerCase(Locale.US);
        Iterator it = list.iterator();
        boolean z = false;
        while (true) {
            if (it.hasNext()) {
                if (A03(lowerCase, (String) it.next())) {
                    break;
                }
                z = true;
            } else if (z || str2 == null || !A03(lowerCase, str2)) {
                r4.A00 = false;
                return r4;
            }
        }
        r4.A00 = true;
        return r4;
    }

    public static C03880Qg A01(String str, List list) {
        C03880Qg r2 = new C03880Qg();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (str.equalsIgnoreCase((String) it.next())) {
                r2.A00 = true;
                return r2;
            }
        }
        r2.A00 = false;
        return r2;
    }

    private static List A02(X509Certificate x509Certificate, int i) {
        Integer num;
        String str;
        ArrayList arrayList = new ArrayList();
        Collection<List<?>> subjectAlternativeNames = x509Certificate.getSubjectAlternativeNames();
        if (subjectAlternativeNames == null) {
            return Collections.emptyList();
        }
        for (List next : subjectAlternativeNames) {
            if (!(next == null || next.size() < 2 || (num = (Integer) next.get(0)) == null || num.intValue() != i || (str = (String) next.get(1)) == null)) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0101, code lost:
        r3 = new java.lang.String(r9, r10, r0.A01 - r10);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C03880Qg A04(java.lang.String r22, java.security.cert.X509Certificate r23) {
        /*
            r21 = this;
            java.util.regex.Pattern r0 = X.AnonymousClass0BB.A00
            r14 = r22
            java.util.regex.Matcher r0 = r0.matcher(r14)
            boolean r0 = r0.matches()
            r20 = r23
            if (r0 == 0) goto L_0x0027
            r1 = 7
            r0 = r20
            java.util.List r1 = A02(r0, r1)     // Catch:{ CertificateParsingException -> 0x0018 }
            goto L_0x0022
        L_0x0018:
            r2 = move-exception
            java.util.ArrayList r1 = new java.util.ArrayList
            r0 = 0
            r1.<init>(r0)
            r2.getMessage()
        L_0x0022:
            X.0Qg r0 = A01(r14, r1)
            return r0
        L_0x0027:
            javax.security.auth.x500.X500Principal r1 = r20.getSubjectX500Principal()
            X.0Qi r18 = new X.0Qi
            r0 = r18
            r0.<init>(r1)
            java.lang.String r17 = "cn"
            r1 = 0
            r0.A02 = r1
            r0.A00 = r1
            r0.A01 = r1
            java.lang.String r15 = X.C03900Qi.A02(r18)
            r16 = 0
            if (r15 != 0) goto L_0x0046
        L_0x0043:
            r1 = 2
            goto L_0x021d
        L_0x0046:
            int r10 = r0.A02
            int r0 = r0.A04
            r19 = r0
            if (r10 == r0) goto L_0x0043
            r0 = r18
            char[] r0 = r0.A03
            r9 = r0
            char r1 = r0[r10]
            r0 = 34
            r13 = 59
            r12 = 44
            r11 = 43
            if (r1 == r0) goto L_0x01c2
            r0 = 35
            if (r1 == r0) goto L_0x014c
            if (r1 == r11) goto L_0x0148
            if (r1 == r12) goto L_0x0148
            if (r1 == r13) goto L_0x0148
            r0 = r18
            r0.A00 = r10
            r0.A01 = r10
        L_0x006f:
            r0 = r18
            int r1 = r0.A02
            r0 = r19
            if (r1 < r0) goto L_0x00c8
            java.lang.String r3 = new java.lang.String
            r0 = r18
            int r1 = r0.A01
            int r1 = r1 - r10
            r3.<init>(r9, r10, r1)
        L_0x0081:
            r0 = r17
            boolean r0 = r0.equalsIgnoreCase(r15)
            if (r0 == 0) goto L_0x008c
            r16 = r3
            goto L_0x0043
        L_0x008c:
            r0 = r18
            int r1 = r0.A02
            int r0 = r0.A04
            if (r1 >= r0) goto L_0x0043
            r0 = r18
            char[] r0 = r0.A03
            char r0 = r0[r1]
            java.lang.String r2 = "Malformed DN: "
            if (r0 == r12) goto L_0x00b0
            if (r0 == r13) goto L_0x00b0
            if (r0 == r11) goto L_0x00b0
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            r0 = r18
            java.lang.String r0 = r0.A05
            java.lang.String r0 = X.AnonymousClass08S.A0J(r2, r0)
            r1.<init>(r0)
            throw r1
        L_0x00b0:
            int r1 = r1 + 1
            r0 = r18
            r0.A02 = r1
            java.lang.String r15 = X.C03900Qi.A02(r18)
            if (r15 != 0) goto L_0x0046
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = r0.A05
            java.lang.String r0 = X.AnonymousClass08S.A0J(r2, r0)
            r1.<init>(r0)
            throw r1
        L_0x00c8:
            char r2 = r9[r1]
            r5 = 32
            if (r2 == r5) goto L_0x010d
            if (r2 == r13) goto L_0x0101
            r0 = 92
            if (r2 == r0) goto L_0x00e7
            if (r2 == r11) goto L_0x0101
            if (r2 == r12) goto L_0x0101
            r0 = r18
            int r4 = r0.A01
            int r3 = r4 + 1
            r0.A01 = r3
            r9[r4] = r2
            int r1 = r1 + 1
            r0.A02 = r1
            goto L_0x006f
        L_0x00e7:
            r0 = r18
            int r2 = r0.A01
            int r1 = r2 + 1
            r0.A01 = r1
            char r0 = X.C03900Qi.A00(r18)
            r9[r2] = r0
            r0 = r18
            int r0 = r0.A02
            int r1 = r0 + 1
            r0 = r18
            r0.A02 = r1
            goto L_0x006f
        L_0x0101:
            java.lang.String r3 = new java.lang.String
            r0 = r18
            int r0 = r0.A01
            int r0 = r0 - r10
            r3.<init>(r9, r10, r0)
            goto L_0x0081
        L_0x010d:
            r0 = r18
            int r4 = r0.A01
            int r1 = r1 + 1
            r0.A02 = r1
            int r1 = r4 + 1
            r0.A01 = r1
            r9[r4] = r5
        L_0x011b:
            int r3 = r0.A02
            r0 = r19
            if (r3 >= r0) goto L_0x0134
            char r0 = r9[r3]
            if (r0 != r5) goto L_0x0134
            r0 = r18
            int r2 = r0.A01
            int r1 = r2 + 1
            r0.A01 = r1
            r9[r2] = r5
            int r1 = r3 + 1
            r0.A02 = r1
            goto L_0x011b
        L_0x0134:
            r0 = r19
            if (r3 == r0) goto L_0x0140
            char r0 = r9[r3]
            if (r0 == r12) goto L_0x0140
            if (r0 == r11) goto L_0x0140
            if (r0 != r13) goto L_0x006f
        L_0x0140:
            java.lang.String r3 = new java.lang.String
            int r4 = r4 - r10
            r3.<init>(r9, r10, r4)
            goto L_0x0081
        L_0x0148:
            java.lang.String r3 = ""
            goto L_0x0081
        L_0x014c:
            int r2 = r10 + 4
            java.lang.String r1 = "Unexpected end of DN: "
            r0 = r19
            if (r2 >= r0) goto L_0x0243
            r0 = r18
            r0.A00 = r10
            int r3 = r10 + 1
            r0.A02 = r3
        L_0x015c:
            int r4 = r0.A02
            r0 = r19
            if (r4 == r0) goto L_0x019a
            char r5 = r9[r4]
            if (r5 == r11) goto L_0x019a
            if (r5 == r12) goto L_0x019a
            if (r5 == r13) goto L_0x019a
            r3 = 32
            if (r5 != r3) goto L_0x0187
            r0 = r18
            r0.A01 = r4
            int r4 = r4 + 1
            r0.A02 = r4
        L_0x0176:
            int r4 = r0.A02
            r0 = r19
            if (r4 >= r0) goto L_0x019e
            char r0 = r9[r4]
            if (r0 != r3) goto L_0x019e
            int r4 = r4 + 1
            r0 = r18
            r0.A02 = r4
            goto L_0x0176
        L_0x0187:
            r0 = 65
            if (r5 < r0) goto L_0x0193
            r0 = 70
            if (r5 > r0) goto L_0x0193
            int r5 = r5 + r3
            char r0 = (char) r5
            r9[r4] = r0
        L_0x0193:
            int r3 = r4 + 1
            r0 = r18
            r0.A02 = r3
            goto L_0x015c
        L_0x019a:
            r0 = r18
            r0.A01 = r4
        L_0x019e:
            r0 = r18
            int r5 = r0.A01
            int r5 = r5 - r10
            r0 = 5
            if (r5 < r0) goto L_0x0235
            r0 = r5 & 1
            if (r0 == 0) goto L_0x0235
            int r4 = r5 >> 1
            r3 = 0
            int r1 = r10 + 1
        L_0x01af:
            if (r3 >= r4) goto L_0x01bb
            r0 = r18
            X.C03900Qi.A01(r0, r1)
            int r1 = r1 + 2
            int r3 = r3 + 1
            goto L_0x01af
        L_0x01bb:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r10, r5)
            goto L_0x0081
        L_0x01c2:
            int r1 = r10 + 1
            r0 = r18
            r0.A02 = r1
            r0.A00 = r1
            r0.A01 = r1
        L_0x01cc:
            int r5 = r0.A02
            r3 = r19
            if (r5 == r3) goto L_0x0251
            char r2 = r9[r5]
            r0 = 34
            if (r2 == r0) goto L_0x0200
            r0 = 92
            if (r2 != r0) goto L_0x01f9
            r0 = r18
            int r2 = r0.A01
            char r0 = X.C03900Qi.A00(r18)
            r9[r2] = r0
        L_0x01e6:
            r0 = r18
            int r0 = r0.A02
            int r2 = r0 + 1
            r0 = r18
            r0.A02 = r2
            int r0 = r0.A01
            int r2 = r0 + 1
            r0 = r18
            r0.A01 = r2
            goto L_0x01cc
        L_0x01f9:
            r0 = r18
            int r0 = r0.A01
            r9[r0] = r2
            goto L_0x01e6
        L_0x0200:
            int r2 = r5 + 1
            r0 = r18
            r0.A02 = r2
            int r5 = r0.A02
            if (r5 >= r3) goto L_0x0211
            char r2 = r9[r5]
            r0 = 32
            if (r2 != r0) goto L_0x0211
            goto L_0x0200
        L_0x0211:
            java.lang.String r3 = new java.lang.String
            r0 = r18
            int r0 = r0.A01
            int r0 = r0 - r1
            r3.<init>(r9, r1, r0)
            goto L_0x0081
        L_0x021d:
            r0 = r20
            java.util.List r1 = A02(r0, r1)     // Catch:{ CertificateParsingException -> 0x0224 }
            goto L_0x022e
        L_0x0224:
            r2 = move-exception
            java.util.ArrayList r1 = new java.util.ArrayList
            r0 = 0
            r1.<init>(r0)
            r2.getMessage()
        L_0x022e:
            r0 = r16
            X.0Qg r0 = A00(r14, r0, r1)
            return r0
        L_0x0235:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            r0 = r18
            java.lang.String r0 = r0.A05
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x0243:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            r0 = r18
            java.lang.String r0 = r0.A05
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x0251:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = "Unexpected end of DN: "
            r0 = r18
            java.lang.String r0 = r0.A05
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BB.A04(java.lang.String, java.security.cert.X509Certificate):X.0Qg");
    }
}
