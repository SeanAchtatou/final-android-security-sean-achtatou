package X;

/* renamed from: X.1qw  reason: invalid class name and case insensitive filesystem */
public final class C35261qw {
    public final C35271qx A00;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r4.equals("InboxViewBinder") == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C35261qw(java.lang.String r4, android.content.Context r5, X.AnonymousClass0p4 r6, X.AnonymousClass1BC r7, X.AnonymousClass1BY r8) {
        /*
            r3 = this;
            r3.<init>()
            int r1 = r4.hashCode()
            r0 = 108377013(0x675b3b5, float:4.6211406E-35)
            if (r1 != r0) goto L_0x0015
            java.lang.String r0 = "InboxViewBinder"
            boolean r0 = r4.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0016
        L_0x0015:
            r1 = -1
        L_0x0016:
            if (r1 != 0) goto L_0x0020
            X.1qx r0 = new X.1qx
            r0.<init>(r5, r6, r7, r8)
            r3.A00 = r0
            return
        L_0x0020:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r1 = "Invalid registry name \""
            java.lang.String r0 = "\"!"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r4, r0)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35261qw.<init>(java.lang.String, android.content.Context, X.0p4, X.1BC, X.1BY):void");
    }
}
