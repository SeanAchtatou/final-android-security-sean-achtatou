package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.database.threads.MessageCursorUtil;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.messages.MontageEventsSticker;
import com.facebook.messaging.model.messages.MontageFeedbackOverlay;
import com.facebook.messaging.model.messages.MontageLinkSticker;
import com.facebook.messaging.model.messages.MontageReactionSticker;
import com.facebook.messaging.model.messages.MontageReshareContentSticker;
import com.facebook.messaging.model.messages.MontageStickerOverlayBounds;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadCriteria;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@UserScoped
/* renamed from: X.1br  reason: invalid class name and case insensitive filesystem */
public final class C26691br {
    private static C05540Zi A0C;
    public AnonymousClass0UN A00;
    public boolean A01;
    public final AnonymousClass06B A02 = AnonymousClass067.A02();
    public final C07050cY A03;
    public final C07040cX A04;
    public final C04310Tq A05;
    public final C04310Tq A06;
    public final C04310Tq A07;
    public final C04310Tq A08;
    public final C04310Tq A09;
    private final C12160of A0A;
    private final MessageCursorUtil A0B;

    public Message A09(String str) {
        if (str != null) {
            LinkedHashMap linkedHashMap = A01(this, C06160ax.A03("offline_threading_id", str), null, 1, false).A00;
            if (!linkedHashMap.isEmpty()) {
                return (Message) linkedHashMap.values().iterator().next();
            }
        }
        return null;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:35|36) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0056, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0057, code lost:
        if (r2 != null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0098, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0099, code lost:
        if (r2 != null) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x009e */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a9 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.C60232wo A00(com.facebook.messaging.model.threadkey.ThreadKey r15, java.lang.String r16) {
        /*
            r14 = this;
            java.lang.String r0 = "msg_type"
            java.lang.String r2 = "DbFetchThreadHandler.doThreadQuery"
            r1 = 568431987(0x21e19573, float:1.5286152E-18)
            X.C005505z.A03(r2, r1)
            java.lang.String r2 = "#threads"
            r1 = 1849732677(0x6e40ae45, float:1.490795E28)
            X.C005505z.A03(r2, r1)     // Catch:{ all -> 0x017f }
            r1 = 0
            boolean r2 = X.C12150od.A02(r15)     // Catch:{ all -> 0x0177 }
            r3 = r16
            if (r2 == 0) goto L_0x005d
            X.0Tq r2 = r14.A09     // Catch:{ all -> 0x0177 }
            java.lang.Object r5 = r2.get()     // Catch:{ all -> 0x0177 }
            X.1eD r5 = (X.C28071eD) r5     // Catch:{ all -> 0x0177 }
            X.0Tq r2 = r14.A08     // Catch:{ all -> 0x0177 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0177 }
            X.1aN r2 = (X.C25771aN) r2     // Catch:{ all -> 0x0177 }
            android.database.sqlite.SQLiteDatabase r6 = r2.A06()     // Catch:{ all -> 0x0177 }
            java.lang.String[] r7 = X.C42712Bn.A05     // Catch:{ all -> 0x0177 }
            java.lang.String r2 = "=?"
            java.lang.String r8 = X.AnonymousClass08S.A0J(r3, r2)     // Catch:{ all -> 0x0177 }
            java.lang.String r2 = r15.A0J()     // Catch:{ all -> 0x0177 }
            java.lang.String[] r9 = new java.lang.String[]{r2}     // Catch:{ all -> 0x0177 }
            r10 = 0
            android.database.Cursor r3 = r5.A08(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0177 }
            if (r3 == 0) goto L_0x009f
            X.0cY r2 = r14.A03     // Catch:{ all -> 0x0177 }
            X.2Bn r2 = r2.A00(r3, r1)     // Catch:{ all -> 0x0177 }
            X.1gi r4 = r2.BLn()     // Catch:{ all -> 0x0054 }
            r2.close()     // Catch:{ all -> 0x0177 }
            goto L_0x00a0
        L_0x0054:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0056 }
        L_0x0056:
            r0 = move-exception
            if (r2 == 0) goto L_0x009e
            r2.close()     // Catch:{ all -> 0x009e }
            goto L_0x009e
        L_0x005d:
            X.0Tq r2 = r14.A09     // Catch:{ all -> 0x0177 }
            java.lang.Object r5 = r2.get()     // Catch:{ all -> 0x0177 }
            X.1eD r5 = (X.C28071eD) r5     // Catch:{ all -> 0x0177 }
            X.0Tq r2 = r14.A08     // Catch:{ all -> 0x0177 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0177 }
            X.1aN r2 = (X.C25771aN) r2     // Catch:{ all -> 0x0177 }
            android.database.sqlite.SQLiteDatabase r6 = r2.A06()     // Catch:{ all -> 0x0177 }
            java.lang.String[] r7 = X.C42712Bn.A05     // Catch:{ all -> 0x0177 }
            java.lang.String r2 = "=?"
            java.lang.String r8 = X.AnonymousClass08S.A0J(r3, r2)     // Catch:{ all -> 0x0177 }
            java.lang.String r2 = r15.A0J()     // Catch:{ all -> 0x0177 }
            java.lang.String[] r9 = new java.lang.String[]{r2}     // Catch:{ all -> 0x0177 }
            r10 = 0
            android.database.Cursor r3 = r5.A08(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0177 }
            if (r3 == 0) goto L_0x009f
            X.0cX r2 = r14.A04     // Catch:{ all -> 0x0177 }
            X.0og r2 = r2.A00(r3, r1)     // Catch:{ all -> 0x0177 }
            X.1gi r4 = r2.BLn()     // Catch:{ all -> 0x0096 }
            r2.close()     // Catch:{ all -> 0x0177 }
            goto L_0x00a0
        L_0x0096:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0098 }
        L_0x0098:
            r0 = move-exception
            if (r2 == 0) goto L_0x009e
            r2.close()     // Catch:{ all -> 0x009e }
        L_0x009e:
            throw r0     // Catch:{ all -> 0x0177 }
        L_0x009f:
            r4 = 0
        L_0x00a0:
            r2 = 645923598(0x2680030e, float:8.882612E-16)
            X.C005505z.A00(r2)     // Catch:{ all -> 0x017f }
            r12 = 0
            if (r4 != 0) goto L_0x00b0
            r0 = 661119871(0x2767e37f, float:3.2181016E-15)
            X.C005505z.A00(r0)
            return r10
        L_0x00b0:
            java.lang.String r3 = "#messages"
            r2 = 495416146(0x1d877352, float:3.585344E-21)
            X.C005505z.A03(r3, r2)     // Catch:{ all -> 0x017f }
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x016f }
            java.lang.String r3 = "thread_key"
            java.lang.String r2 = r15.A0J()     // Catch:{ all -> 0x016f }
            X.0av r9 = X.C06160ax.A03(r3, r2)     // Catch:{ all -> 0x016f }
            X.1V7 r2 = X.AnonymousClass1V7.A0A     // Catch:{ all -> 0x016f }
            int r2 = r2.dbKeyValue     // Catch:{ all -> 0x016f }
            java.lang.String r3 = java.lang.Integer.toString(r2)     // Catch:{ all -> 0x016f }
            X.1V7 r2 = X.AnonymousClass1V7.A0J     // Catch:{ all -> 0x016f }
            int r2 = r2.dbKeyValue     // Catch:{ all -> 0x016f }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ all -> 0x016f }
            com.google.common.collect.ImmutableSet r2 = com.google.common.collect.ImmutableSet.A05(r3, r2)     // Catch:{ all -> 0x016f }
            X.0av r8 = X.C06160ax.A04(r0, r2)     // Catch:{ all -> 0x016f }
            r2 = 1
            java.lang.String r7 = "timestamp_ms"
            r5 = 86400000(0x5265c00, double:4.2687272E-316)
            long r10 = r10 - r5
            java.lang.String r5 = java.lang.Long.toString(r10)     // Catch:{ all -> 0x016f }
            X.1gm r3 = new X.1gm     // Catch:{ all -> 0x016f }
            r3.<init>(r7, r5)     // Catch:{ all -> 0x016f }
            X.0av[] r3 = new X.C06140av[]{r9, r8, r3}     // Catch:{ all -> 0x016f }
            X.1a6 r5 = X.C06160ax.A01(r3)     // Catch:{ all -> 0x016f }
            X.0Tq r3 = r14.A08     // Catch:{ all -> 0x016f }
            java.lang.Object r3 = r3.get()     // Catch:{ all -> 0x016f }
            X.1aN r3 = (X.C25771aN) r3     // Catch:{ all -> 0x016f }
            android.database.sqlite.SQLiteDatabase r6 = r3.A06()     // Catch:{ all -> 0x016f }
            java.lang.String[] r7 = new java.lang.String[]{r0}     // Catch:{ all -> 0x016f }
            java.lang.String r8 = r5.A02()     // Catch:{ all -> 0x016f }
            java.lang.String[] r9 = r5.A04()     // Catch:{ all -> 0x016f }
            android.database.sqlite.SQLiteQueryBuilder r5 = new android.database.sqlite.SQLiteQueryBuilder     // Catch:{ all -> 0x016f }
            r5.<init>()     // Catch:{ all -> 0x016f }
            java.lang.String r0 = X.C61162yR.A01(r7, r8, r12)     // Catch:{ all -> 0x016f }
            r5.setTables(r0)     // Catch:{ all -> 0x016f }
            r10 = 0
            r11 = 0
            r13 = r12
            android.database.Cursor r7 = r5.query(r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x016f }
            r6 = 0
            r5 = 0
        L_0x0123:
            boolean r0 = r7.moveToNext()     // Catch:{ all -> 0x016a }
            if (r0 == 0) goto L_0x014d
            if (r6 == 0) goto L_0x012d
            if (r5 != 0) goto L_0x014d
        L_0x012d:
            java.lang.String r0 = r7.getString(r1)     // Catch:{ all -> 0x016a }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x016a }
            X.1V7 r3 = X.AnonymousClass1V7.A00(r0)     // Catch:{ all -> 0x016a }
            X.1V7 r0 = X.AnonymousClass1V7.A0A     // Catch:{ all -> 0x016a }
            if (r3 != r0) goto L_0x0143
            X.0zh r0 = r4.A01     // Catch:{ all -> 0x016a }
            r0.A0z = r2     // Catch:{ all -> 0x016a }
            r6 = 1
            goto L_0x0123
        L_0x0143:
            X.1V7 r0 = X.AnonymousClass1V7.A0J     // Catch:{ all -> 0x016a }
            if (r3 != r0) goto L_0x0123
            X.0zh r0 = r4.A01     // Catch:{ all -> 0x016a }
            r0.A11 = r2     // Catch:{ all -> 0x016a }
            r5 = 1
            goto L_0x0123
        L_0x014d:
            r7.close()     // Catch:{ all -> 0x016f }
            r0 = 1961587675(0x74eb73db, float:1.4923579E32)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x017f }
            X.2wo r3 = new X.2wo     // Catch:{ all -> 0x017f }
            X.0zh r0 = r4.A01     // Catch:{ all -> 0x017f }
            com.facebook.messaging.model.threads.ThreadSummary r2 = r0.A00()     // Catch:{ all -> 0x017f }
            long r0 = r4.A00     // Catch:{ all -> 0x017f }
            r3.<init>(r2, r0)     // Catch:{ all -> 0x017f }
            r0 = -1714174908(0xffffffff99d3c444, float:-2.189616E-23)
            X.C005505z.A00(r0)
            return r3
        L_0x016a:
            r0 = move-exception
            r7.close()     // Catch:{ all -> 0x016f }
            throw r0     // Catch:{ all -> 0x016f }
        L_0x016f:
            r1 = move-exception
            r0 = -1484290430(0xffffffffa7878682, float:-3.761586E-15)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x017f }
            goto L_0x017e
        L_0x0177:
            r1 = move-exception
            r0 = 1660527056(0x62f9a1d0, float:2.3024495E21)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x017f }
        L_0x017e:
            throw r1     // Catch:{ all -> 0x017f }
        L_0x017f:
            r1 = move-exception
            r0 = 1803814173(0x6b84051d, float:3.192047E26)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26691br.A00(com.facebook.messaging.model.threadkey.ThreadKey, java.lang.String):X.2wo");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static C61212yW A01(C26691br r13, C06140av r14, String str, int i, boolean z) {
        C61212yW r1;
        int i2;
        C61182yT r6;
        C005505z.A03("DbFetchThreadHandler.doMessagesQuery", 1396224862);
        String str2 = null;
        int i3 = i;
        if (i == 0) {
            try {
                r1 = new C61212yW(new LinkedHashMap());
                i2 = -1592641849;
            } catch (Throwable th) {
                C005505z.A00(-1705215322);
                throw th;
            }
        } else {
            if (i3 != -1) {
                str2 = Integer.toString(i3);
            }
            SQLiteDatabase A062 = ((C25771aN) r13.A08.get()).A06();
            C007406x.A01(A062, -1572206472);
            try {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                MessageCursorUtil messageCursorUtil = r13.A0B;
                SQLiteDatabase A063 = ((C25771aN) r13.A08.get()).A06();
                String[] strArr = MessageCursorUtil.A0F;
                String A022 = r14.A02();
                String[] A042 = r14.A04();
                SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
                sQLiteQueryBuilder.setTables(C61162yR.A01(strArr, A022, str));
                r6 = new C61182yT(messageCursorUtil, sQLiteQueryBuilder.query(A063, strArr, A022, A042, null, null, str, str2), messageCursorUtil.A0D);
                HashSet hashSet = new HashSet();
                while (true) {
                    Message A002 = r6.A00();
                    if (A002 == null) {
                        break;
                    }
                    linkedHashMap.put(A002.A0q, A002);
                    if (r6.A00) {
                        hashSet.add(A002.A0U.A0J());
                    }
                }
                if (z) {
                    LinkedHashMap linkedHashMap2 = linkedHashMap;
                    AnonymousClass0qX.A00(linkedHashMap);
                    if (!linkedHashMap.isEmpty()) {
                        linkedHashMap = new LinkedHashMap();
                        String[] strArr2 = (String[]) linkedHashMap2.keySet().toArray(new String[0]);
                        for (int length = strArr2.length - 1; length >= 0; length--) {
                            String str3 = strArr2[length];
                            linkedHashMap.put(str3, linkedHashMap2.get(str3));
                        }
                    }
                }
                if (!hashSet.isEmpty()) {
                    C06140av A043 = C06160ax.A04("thread_key", hashSet);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("initial_fetch_complete", (Integer) 0);
                    ((C25771aN) r13.A08.get()).A06().update("threads", contentValues, A043.A02(), A043.A04());
                }
                r6.A1K.close();
                A062.setTransactionSuccessful();
                r1 = new C61212yW(linkedHashMap);
                C007406x.A02(A062, 974160261);
                i2 = -774872874;
            } catch (Throwable th2) {
                C007406x.A02(A062, 484625116);
                throw th2;
            }
        }
        C005505z.A00(i2);
        return r1;
    }

    public static final C26691br A03(AnonymousClass1XY r4) {
        C26691br r0;
        synchronized (C26691br.class) {
            C05540Zi A002 = C05540Zi.A00(A0C);
            A0C = A002;
            try {
                if (A002.A03(r4)) {
                    A0C.A00 = new C26691br((AnonymousClass1XY) A0C.A01());
                }
                C05540Zi r1 = A0C;
                r0 = (C26691br) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A0C.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00e3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00e4, code lost:
        if (r11 != null) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e9, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.messaging.model.messages.MessagesCollection A04(com.facebook.messaging.model.threads.ThreadSummary r21, java.util.LinkedHashMap r22) {
        /*
            r20 = this;
            r1 = r21
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A0S
            java.lang.String r0 = r0.A0J()
            r5 = r22
            boolean r0 = r5.containsKey(r0)
            if (r0 == 0) goto L_0x0076
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A0S
            java.lang.String r0 = r0.A0J()
            r5.remove(r0)
            r3 = 1
        L_0x001a:
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r1.A0S
            java.util.Collection r0 = r5.values()
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.copyOf(r0)
            boolean r0 = X.C12150od.A02(r4)
            r4 = r20
            if (r0 == 0) goto L_0x012c
            X.0Tq r0 = r4.A05
            java.lang.Object r8 = r0.get()
            X.30o r8 = (X.C622330o) r8
            java.util.Set r7 = r5.keySet()
            X.04a r0 = new X.04a
            r0.<init>()
            int r6 = X.C013509w.A00(r7)
            if (r6 <= 0) goto L_0x00ed
            java.lang.String r11 = "msg_id"
            X.0av r10 = X.C06160ax.A04(r11, r7)
            X.0Tq r6 = r8.A00
            java.lang.Object r6 = r6.get()
            X.1aN r6 = (X.C25771aN) r6
            android.database.sqlite.SQLiteDatabase r12 = r6.A06()
            java.lang.String r9 = "user_key"
            java.lang.String r8 = "emoji"
            java.lang.String r7 = "offset"
            java.lang.String r6 = "timestamp"
            java.lang.String[] r14 = new java.lang.String[]{r11, r9, r8, r7, r6}
            java.lang.String r15 = r10.A02()
            java.lang.String[] r16 = r10.A04()
            java.lang.String r13 = "montage_message_reactions"
            r17 = 0
            r18 = 0
            r19 = 0
            android.database.Cursor r11 = r12.query(r13, r14, r15, r16, r17, r18, r19)
            goto L_0x0078
        L_0x0076:
            r3 = 0
            goto L_0x001a
        L_0x0078:
            boolean r6 = r11.moveToNext()     // Catch:{ all -> 0x00e1 }
            if (r6 == 0) goto L_0x00ea
            java.lang.String r6 = "msg_id"
            int r6 = r11.getColumnIndex(r6)     // Catch:{ all -> 0x00e1 }
            java.lang.String r9 = r11.getString(r6)     // Catch:{ all -> 0x00e1 }
            java.lang.String r6 = "user_key"
            int r6 = r11.getColumnIndex(r6)     // Catch:{ all -> 0x00e1 }
            java.lang.String r6 = r11.getString(r6)     // Catch:{ all -> 0x00e1 }
            com.facebook.user.model.UserKey r8 = com.facebook.user.model.UserKey.A02(r6)     // Catch:{ all -> 0x00e1 }
            java.lang.String r6 = "emoji"
            int r6 = r11.getColumnIndex(r6)     // Catch:{ all -> 0x00e1 }
            java.lang.String r13 = r11.getString(r6)     // Catch:{ all -> 0x00e1 }
            java.lang.String r6 = "offset"
            int r6 = r11.getColumnIndex(r6)     // Catch:{ all -> 0x00e1 }
            long r14 = r11.getLong(r6)     // Catch:{ all -> 0x00e1 }
            java.lang.String r6 = "timestamp"
            int r6 = r11.getColumnIndex(r6)     // Catch:{ all -> 0x00e1 }
            long r16 = r11.getLong(r6)     // Catch:{ all -> 0x00e1 }
            java.lang.Object r7 = r0.get(r9)     // Catch:{ all -> 0x00e1 }
            X.0V0 r7 = (X.AnonymousClass0V0) r7     // Catch:{ all -> 0x00e1 }
            if (r7 != 0) goto L_0x00c1
            com.google.common.collect.HashMultimap r7 = new com.google.common.collect.HashMultimap     // Catch:{ all -> 0x00e1 }
            r7.<init>()     // Catch:{ all -> 0x00e1 }
        L_0x00c1:
            java.util.Collection r10 = r7.AbK(r8)     // Catch:{ all -> 0x00e1 }
            if (r10 != 0) goto L_0x00db
            X.0Xy r6 = new X.0Xy     // Catch:{ all -> 0x00e1 }
            r6.<init>()     // Catch:{ all -> 0x00e1 }
        L_0x00cc:
            com.facebook.messaging.model.messages.MontageMessageReaction r12 = new com.facebook.messaging.model.messages.MontageMessageReaction     // Catch:{ all -> 0x00e1 }
            r12.<init>(r13, r14, r16)     // Catch:{ all -> 0x00e1 }
            r6.add(r12)     // Catch:{ all -> 0x00e1 }
            r7.Byz(r8, r6)     // Catch:{ all -> 0x00e1 }
            r0.put(r9, r7)     // Catch:{ all -> 0x00e1 }
            goto L_0x0078
        L_0x00db:
            X.0Xy r6 = new X.0Xy     // Catch:{ all -> 0x00e1 }
            r6.<init>(r10)     // Catch:{ all -> 0x00e1 }
            goto L_0x00cc
        L_0x00e1:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00e3 }
        L_0x00e3:
            r0 = move-exception
            if (r11 == 0) goto L_0x00e9
            r11.close()     // Catch:{ all -> 0x00e9 }
        L_0x00e9:
            throw r0
        L_0x00ea:
            r11.close()
        L_0x00ed:
            boolean r6 = r0.isEmpty()
            if (r6 != 0) goto L_0x012c
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder
            r9.<init>()
            X.1Xv r8 = r2.iterator()
        L_0x00fc:
            boolean r2 = r8.hasNext()
            if (r2 == 0) goto L_0x0128
            java.lang.Object r7 = r8.next()
            com.facebook.messaging.model.messages.Message r7 = (com.facebook.messaging.model.messages.Message) r7
            java.lang.String r2 = r7.A0q
            java.lang.Object r6 = r0.get(r2)
            X.0V0 r6 = (X.AnonymousClass0V0) r6
            if (r6 != 0) goto L_0x0116
            r9.add(r7)
            goto L_0x00fc
        L_0x0116:
            X.1TG r2 = com.facebook.messaging.model.messages.Message.A00()
            r2.A03(r7)
            r2.A05(r6)
            com.facebook.messaging.model.messages.Message r2 = r2.A00()
            r9.add(r2)
            goto L_0x00fc
        L_0x0128:
            com.google.common.collect.ImmutableList r2 = r9.build()
        L_0x012c:
            com.facebook.messaging.model.threadkey.ThreadKey r6 = r1.A0S
            java.util.Set r0 = r5.keySet()
            com.google.common.collect.ImmutableList r6 = r4.A06(r6, r2, r0)
            X.1Xv r2 = r6.iterator()
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x014d
            java.lang.Object r2 = r2.next()
            com.facebook.messaging.model.messages.Message r2 = (com.facebook.messaging.model.messages.Message) r2
            X.0of r0 = r4.A0A
            long r4 = r2.A03
            r0.A01(r4)
        L_0x014d:
            X.1oI r2 = new X.1oI
            r2.<init>()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A0S
            r2.A00 = r0
            r2.A01(r6)
            r2.A03 = r3
            r0 = 1
            r2.A02 = r0
            com.facebook.messaging.model.messages.MessagesCollection r0 = r2.A00()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26691br.A04(com.facebook.messaging.model.threads.ThreadSummary, java.util.LinkedHashMap):com.facebook.messaging.model.messages.MessagesCollection");
    }

    private FetchThreadResult A05(C60232wo r9, int i) {
        DataFetchDisposition dataFetchDisposition;
        if (r9 == null) {
            return FetchThreadResult.A09;
        }
        ThreadSummary threadSummary = r9.A01;
        if (threadSummary.A09 == 0 || !((C07080cb) this.A07.get()).A06(C12740pt.A00(threadSummary.A0O), true)) {
            dataFetchDisposition = DataFetchDisposition.A0F;
        } else {
            dataFetchDisposition = DataFetchDisposition.A0E;
        }
        C05180Xy<UserKey> r2 = new C05180Xy();
        C24971Xv it = threadSummary.A0m.iterator();
        while (it.hasNext()) {
            r2.add(((ThreadParticipant) it.next()).A00());
        }
        C24971Xv it2 = threadSummary.A0k.iterator();
        while (it2.hasNext()) {
            r2.add(((ThreadParticipant) it2.next()).A00());
        }
        C24971Xv it3 = threadSummary.A0n.iterator();
        while (it3.hasNext()) {
            r2.add(((ParticipantInfo) it3.next()).A01);
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        ImmutableList.Builder builder2 = ImmutableList.builder();
        C14300t0 r3 = (C14300t0) AnonymousClass1XX.A03(AnonymousClass1Y3.AxE, this.A00);
        for (UserKey userKey : r2) {
            User A032 = r3.A03(userKey);
            if (A032 != null) {
                builder.add((Object) A032);
            } else {
                builder2.add((Object) userKey);
            }
        }
        builder.addAll((Iterable) ((C28011e7) this.A06.get()).A04(builder2.build()));
        ImmutableList build = builder.build();
        C61172yS r1 = new C61172yS();
        r1.A04 = threadSummary.A0S;
        r1.A00 = i;
        MessagesCollection A042 = A04(threadSummary, A0I(new C58002t0(r1)));
        C61222yX A002 = FetchThreadResult.A00();
        A002.A01 = dataFetchDisposition;
        A002.A04 = threadSummary;
        A002.A02 = A042;
        A002.A06 = build;
        A002.A00 = r9.A00;
        return A002.A00();
    }

    /* JADX INFO: finally extract failed */
    private ImmutableList A06(ThreadKey threadKey, ImmutableList immutableList, Set set) {
        Set set2 = set;
        if (set == null) {
            set2 = new HashSet();
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                String str = ((Message) it.next()).A0q;
                if (str != null) {
                    set2.add(str);
                }
            }
        }
        AnonymousClass0UN r2 = this.A00;
        if (C12150od.A02(threadKey)) {
            ImmutableList.Builder builder = new ImmutableList.Builder();
            C49152bj r25 = (C49152bj) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Al9, r2);
            C06140av A042 = C06160ax.A04(TraceFieldType.MsgId, set2);
            String buildQueryString = SQLiteQueryBuilder.buildQueryString(false, "montage_message_poll", null, null, null, null, null, null);
            SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
            sQLiteQueryBuilder.setTables(AnonymousClass08S.A0W("((", buildQueryString, ") _poll JOIN ", "montage_message_poll_options", " _options ON _poll.", "poll_id", " = _options.", "poll_id", ")"));
            SQLiteDatabase A062 = ((C25771aN) r25.A01.get()).A06();
            Cursor query = sQLiteQueryBuilder.query(A062, null, A042.A02(), A042.A04(), null, null, null);
            if (query.getCount() == 0) {
                query.close();
            }
            int columnIndexOrThrow = query.getColumnIndexOrThrow(TraceFieldType.MsgId);
            int columnIndexOrThrow2 = query.getColumnIndexOrThrow("poll_id");
            int columnIndexOrThrow3 = query.getColumnIndexOrThrow("style");
            int columnIndexOrThrow4 = query.getColumnIndexOrThrow("question_text");
            int columnIndexOrThrow5 = query.getColumnIndexOrThrow("url");
            int columnIndexOrThrow6 = query.getColumnIndexOrThrow("can_viewer_vote");
            int columnIndexOrThrow7 = query.getColumnIndexOrThrow("viewer_vote_index");
            int columnIndexOrThrow8 = query.getColumnIndexOrThrow("bound_x");
            int columnIndexOrThrow9 = query.getColumnIndexOrThrow("bound_y");
            int columnIndexOrThrow10 = query.getColumnIndexOrThrow("bound_width");
            int columnIndexOrThrow11 = query.getColumnIndexOrThrow("bound_height");
            int columnIndexOrThrow12 = query.getColumnIndexOrThrow("bound_rotation");
            int columnIndexOrThrow13 = query.getColumnIndexOrThrow("option_text");
            int columnIndexOrThrow14 = query.getColumnIndexOrThrow("vote_count");
            int columnIndexOrThrow15 = query.getColumnIndexOrThrow("option_index");
            HashMap hashMap = new HashMap();
            while (query.moveToNext()) {
                try {
                    String string = query.getString(columnIndexOrThrow);
                    if (!hashMap.containsKey(string)) {
                        hashMap.put(string, new HashMap());
                    }
                    Map map = (Map) hashMap.get(string);
                    String string2 = query.getString(columnIndexOrThrow2);
                    if (!map.containsKey(string2)) {
                        C72673el r4 = new C72673el();
                        r4.A03 = string2;
                        r4.A05 = query.getString(columnIndexOrThrow3);
                        r4.A04 = query.getString(columnIndexOrThrow4);
                        r4.A06 = query.getString(columnIndexOrThrow5);
                        boolean z = true;
                        if (query.getInt(columnIndexOrThrow6) != 1) {
                            z = false;
                        }
                        r4.A08 = z;
                        r4.A00 = query.getInt(columnIndexOrThrow7);
                        MontageStickerOverlayBounds.MontageStickerOverlayBoundsBuilder montageStickerOverlayBoundsBuilder = new MontageStickerOverlayBounds.MontageStickerOverlayBoundsBuilder();
                        montageStickerOverlayBoundsBuilder.A00 = query.getDouble(columnIndexOrThrow8);
                        montageStickerOverlayBoundsBuilder.A01 = query.getDouble(columnIndexOrThrow9);
                        montageStickerOverlayBoundsBuilder.A04 = query.getDouble(columnIndexOrThrow10);
                        montageStickerOverlayBoundsBuilder.A02 = query.getDouble(columnIndexOrThrow11);
                        montageStickerOverlayBoundsBuilder.A03 = query.getDouble(columnIndexOrThrow12);
                        r4.A02 = montageStickerOverlayBoundsBuilder.A00();
                        map.put(string2, r4);
                    }
                    C72683em r1 = new C72683em();
                    r1.A04 = query.getString(columnIndexOrThrow13);
                    r1.A02 = query.getInt(columnIndexOrThrow14);
                    r1.A00 = query.getInt(columnIndexOrThrow15);
                    ((C72673el) map.get(string2)).A07.add(r1.A00());
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            }
            query.close();
            HashMap hashMap2 = new HashMap();
            HashMap hashMap3 = new HashMap();
            HashMap hashMap4 = new HashMap();
            HashMap hashMap5 = new HashMap();
            SQLiteQueryBuilder sQLiteQueryBuilder2 = new SQLiteQueryBuilder();
            sQLiteQueryBuilder2.setTables("montage_message_interactive_overlays");
            Cursor query2 = sQLiteQueryBuilder2.query(A062, null, A042.A02(), A042.A04(), null, null, null);
            if (query2.getCount() == 0) {
                query2.close();
            }
            int columnIndexOrThrow16 = query2.getColumnIndexOrThrow(TraceFieldType.MsgId);
            int columnIndexOrThrow17 = query2.getColumnIndexOrThrow("overlay_type");
            int columnIndexOrThrow18 = query2.getColumnIndexOrThrow("overlay_data");
            while (query2.moveToNext()) {
                String string3 = query2.getString(columnIndexOrThrow16);
                String string4 = query2.getString(columnIndexOrThrow17);
                String string5 = query2.getString(columnIndexOrThrow18);
                if (!hashMap2.containsKey(string3)) {
                    hashMap2.put(string3, new ArrayList());
                }
                if (C99084oO.$const$string(210).equals(string4)) {
                    ((List) hashMap2.get(string3)).add(string5);
                }
                if ("events_sticker".equals(string4)) {
                    hashMap3.put(string3, string5);
                }
                if ("reshare_post_sticker".equals(string4)) {
                    hashMap4.put(string3, string5);
                }
                if ("link_sticker".equals(string4)) {
                    hashMap5.put(string3, string5);
                }
            }
            query2.close();
            HashMap hashMap6 = new HashMap();
            for (Map.Entry entry : hashMap.entrySet()) {
                ArrayList arrayList = new ArrayList();
                for (C72673el A002 : ((Map) entry.getValue()).values()) {
                    arrayList.add(new MontageFeedbackOverlay(A002.A00()));
                }
                hashMap6.put(entry.getKey(), arrayList);
            }
            try {
                for (Map.Entry entry2 : hashMap2.entrySet()) {
                    String str2 = (String) entry2.getKey();
                    if (!hashMap6.containsKey(str2)) {
                        hashMap6.put(str2, new ArrayList());
                    }
                    for (String readValue : (List) entry2.getValue()) {
                        ((List) hashMap6.get(str2)).add(new MontageFeedbackOverlay((MontageReactionSticker) r25.A00.readValue(readValue, MontageReactionSticker.class)));
                    }
                }
                for (Map.Entry entry3 : hashMap3.entrySet()) {
                    String str3 = (String) entry3.getKey();
                    if (!hashMap6.containsKey(str3)) {
                        hashMap6.put(str3, new ArrayList());
                    }
                    ((List) hashMap6.get(str3)).add(new MontageFeedbackOverlay((MontageEventsSticker) r25.A00.readValue((String) entry3.getValue(), MontageEventsSticker.class)));
                }
                for (Map.Entry entry4 : hashMap4.entrySet()) {
                    String str4 = (String) entry4.getKey();
                    if (!hashMap6.containsKey(str4)) {
                        hashMap6.put(str4, new ArrayList());
                    }
                    ((List) hashMap6.get(str4)).add(new MontageFeedbackOverlay((MontageReshareContentSticker) r25.A00.readValue((String) entry4.getValue(), MontageReshareContentSticker.class)));
                }
                for (Map.Entry entry5 : hashMap5.entrySet()) {
                    String str5 = (String) entry5.getKey();
                    if (!hashMap6.containsKey(str5)) {
                        hashMap6.put(str5, new ArrayList());
                    }
                    ((List) hashMap6.get(str5)).add(new MontageFeedbackOverlay((MontageLinkSticker) r25.A00.readValue((String) entry5.getValue(), MontageLinkSticker.class)));
                }
            } catch (Exception e) {
                C010708t.A0L("DbMontageFeedbackOverlaysHandler", e.getMessage(), e);
            }
            HashMap hashMap7 = new HashMap();
            for (Map.Entry entry6 : hashMap6.entrySet()) {
                hashMap7.put(entry6.getKey(), ImmutableList.copyOf((Collection) entry6.getValue()));
            }
            if (!hashMap7.isEmpty()) {
                C24971Xv it2 = immutableList.iterator();
                while (it2.hasNext()) {
                    Message message = (Message) it2.next();
                    ImmutableList immutableList2 = (ImmutableList) hashMap7.get(message.A0q);
                    if (C013509w.A01(immutableList2)) {
                        AnonymousClass1TG A003 = Message.A00();
                        A003.A03(message);
                        A003.A0b = immutableList2;
                        builder.add((Object) A003.A00());
                    } else {
                        builder.add((Object) message);
                    }
                }
                return builder.build();
            }
        }
        return immutableList;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:43|44) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a8, code lost:
        if (r10 != null) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00f9, code lost:
        if (r9 != null) goto L_0x00fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00fe */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass04b A07(java.util.Set r30, int r31) {
        /*
            r29 = this;
            java.lang.String r1 = "DbFetchThreadHandler.fetchThreadFromDb"
            r0 = 1676799200(0x63f1ece0, float:8.925468E21)
            X.C005505z.A03(r1, r0)
            X.04b r19 = new X.04b
            r19.<init>()
            java.lang.Thread r18 = java.lang.Thread.currentThread()
            int r7 = r18.getPriority()
            r1 = 10
            r0 = r18
            r0.setPriority(r1)     // Catch:{ all -> 0x026b }
            java.lang.String r3 = "thread_key"
            X.04b r2 = new X.04b     // Catch:{ all -> 0x026b }
            r2.<init>()     // Catch:{ all -> 0x026b }
            X.04b r4 = new X.04b     // Catch:{ all -> 0x026b }
            r4.<init>()     // Catch:{ all -> 0x026b }
            java.lang.String r1 = "DbFetchThreadHandler.doThreadQuery"
            r0 = 1150982997(0x449a9b55, float:1236.8541)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x026b }
            java.util.HashSet r9 = new java.util.HashSet     // Catch:{ all -> 0x0263 }
            r9.<init>()     // Catch:{ all -> 0x0263 }
            java.util.HashSet r8 = new java.util.HashSet     // Catch:{ all -> 0x0263 }
            r8.<init>()     // Catch:{ all -> 0x0263 }
            java.util.Iterator r5 = r30.iterator()     // Catch:{ all -> 0x0263 }
        L_0x003e:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x0263 }
            r1 = 0
            if (r0 == 0) goto L_0x0059
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x0263 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = (com.facebook.messaging.model.threadkey.ThreadKey) r1     // Catch:{ all -> 0x0263 }
            boolean r0 = X.C12150od.A02(r1)     // Catch:{ all -> 0x0263 }
            if (r0 == 0) goto L_0x0055
            r9.add(r1)     // Catch:{ all -> 0x0263 }
            goto L_0x003e
        L_0x0055:
            r8.add(r1)     // Catch:{ all -> 0x0263 }
            goto L_0x003e
        L_0x0059:
            java.lang.String r5 = "#threads"
            r0 = -65754369(0xfffffffffc14aaff, float:-3.0877126E36)
            X.C005505z.A03(r5, r0)     // Catch:{ all -> 0x0263 }
            X.04b r6 = new X.04b     // Catch:{ all -> 0x025b }
            r6.<init>()     // Catch:{ all -> 0x025b }
            X.0av r9 = X.C06160ax.A04(r3, r9)     // Catch:{ all -> 0x025b }
            r0 = r29
            X.0Tq r5 = r0.A09     // Catch:{ all -> 0x025b }
            java.lang.Object r10 = r5.get()     // Catch:{ all -> 0x025b }
            X.1eD r10 = (X.C28071eD) r10     // Catch:{ all -> 0x025b }
            X.0Tq r5 = r0.A08     // Catch:{ all -> 0x025b }
            java.lang.Object r5 = r5.get()     // Catch:{ all -> 0x025b }
            X.1aN r5 = (X.C25771aN) r5     // Catch:{ all -> 0x025b }
            android.database.sqlite.SQLiteDatabase r11 = r5.A06()     // Catch:{ all -> 0x025b }
            java.lang.String[] r12 = X.C42712Bn.A05     // Catch:{ all -> 0x025b }
            java.lang.String r13 = r9.A02()     // Catch:{ all -> 0x025b }
            java.lang.String[] r14 = r9.A04()     // Catch:{ all -> 0x025b }
            r15 = 0
            android.database.Cursor r10 = r10.A08(r11, r12, r13, r14, r15)     // Catch:{ all -> 0x025b }
            if (r10 == 0) goto L_0x00b1
            X.0cY r9 = r0.A03     // Catch:{ all -> 0x025b }
            X.2Bn r10 = r9.A00(r10, r1)     // Catch:{ all -> 0x025b }
        L_0x0097:
            X.1gi r9 = r10.BLn()     // Catch:{ all -> 0x00a5 }
            if (r9 == 0) goto L_0x00ae
            X.0zh r5 = r9.A01     // Catch:{ all -> 0x00a5 }
            com.facebook.messaging.model.threadkey.ThreadKey r5 = r5.A0R     // Catch:{ all -> 0x00a5 }
            r6.put(r5, r9)     // Catch:{ all -> 0x00a5 }
            goto L_0x0097
        L_0x00a5:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a7 }
        L_0x00a7:
            r0 = move-exception
            if (r10 == 0) goto L_0x00fe
            r10.close()     // Catch:{ all -> 0x00fe }
            goto L_0x00fe
        L_0x00ae:
            r10.close()     // Catch:{ all -> 0x025b }
        L_0x00b1:
            r2.A0B(r6)     // Catch:{ all -> 0x025b }
            X.04b r6 = new X.04b     // Catch:{ all -> 0x025b }
            r6.<init>()     // Catch:{ all -> 0x025b }
            X.0av r8 = X.C06160ax.A04(r3, r8)     // Catch:{ all -> 0x025b }
            X.0Tq r5 = r0.A09     // Catch:{ all -> 0x025b }
            java.lang.Object r9 = r5.get()     // Catch:{ all -> 0x025b }
            X.1eD r9 = (X.C28071eD) r9     // Catch:{ all -> 0x025b }
            X.0Tq r5 = r0.A08     // Catch:{ all -> 0x025b }
            java.lang.Object r5 = r5.get()     // Catch:{ all -> 0x025b }
            X.1aN r5 = (X.C25771aN) r5     // Catch:{ all -> 0x025b }
            android.database.sqlite.SQLiteDatabase r10 = r5.A06()     // Catch:{ all -> 0x025b }
            java.lang.String[] r11 = X.C42712Bn.A05     // Catch:{ all -> 0x025b }
            java.lang.String r12 = r8.A02()     // Catch:{ all -> 0x025b }
            java.lang.String[] r13 = r8.A04()     // Catch:{ all -> 0x025b }
            r14 = 0
            android.database.Cursor r9 = r9.A08(r10, r11, r12, r13, r14)     // Catch:{ all -> 0x025b }
            if (r9 == 0) goto L_0x0102
            X.0cX r8 = r0.A04     // Catch:{ all -> 0x025b }
            X.0og r9 = r8.A00(r9, r1)     // Catch:{ all -> 0x025b }
        L_0x00e8:
            X.1gi r8 = r9.BLn()     // Catch:{ all -> 0x00f6 }
            if (r8 == 0) goto L_0x00ff
            X.0zh r5 = r8.A01     // Catch:{ all -> 0x00f6 }
            com.facebook.messaging.model.threadkey.ThreadKey r5 = r5.A0R     // Catch:{ all -> 0x00f6 }
            r6.put(r5, r8)     // Catch:{ all -> 0x00f6 }
            goto L_0x00e8
        L_0x00f6:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00f8 }
        L_0x00f8:
            r0 = move-exception
            if (r9 == 0) goto L_0x00fe
            r9.close()     // Catch:{ all -> 0x00fe }
        L_0x00fe:
            throw r0     // Catch:{ all -> 0x025b }
        L_0x00ff:
            r9.close()     // Catch:{ all -> 0x025b }
        L_0x0102:
            r2.A0B(r6)     // Catch:{ all -> 0x025b }
            r5 = -1990052109(0xffffffff896236f3, float:-2.7229597E-33)
            X.C005505z.A00(r5)     // Catch:{ all -> 0x0263 }
            boolean r5 = r2.isEmpty()     // Catch:{ all -> 0x0263 }
            if (r5 == 0) goto L_0x0116
            r1 = 103229515(0x627284b, float:3.1438812E-35)
            goto L_0x021d
        L_0x0116:
            java.lang.String r6 = "#messages"
            r5 = -456314051(0xffffffffe4cd333d, float:-3.0282197E22)
            X.C005505z.A03(r6, r5)     // Catch:{ all -> 0x0263 }
            X.06B r5 = r0.A02     // Catch:{ all -> 0x0253 }
            long r16 = r5.now()     // Catch:{ all -> 0x0253 }
            r5 = 86400000(0x5265c00, double:4.2687272E-316)
            long r16 = r16 - r5
            X.0qA r11 = new X.0qA     // Catch:{ all -> 0x0253 }
            r11.<init>()     // Catch:{ all -> 0x0253 }
            X.04b r8 = new X.04b     // Catch:{ all -> 0x0253 }
            r8.<init>()     // Catch:{ all -> 0x0253 }
            int r10 = r2.size()     // Catch:{ all -> 0x0253 }
            r9 = 0
        L_0x0138:
            java.lang.String r12 = "msg_type"
            r6 = 1
            if (r9 >= r10) goto L_0x0187
            java.lang.Object r13 = r2.A07(r9)     // Catch:{ all -> 0x0253 }
            com.facebook.messaging.model.threadkey.ThreadKey r13 = (com.facebook.messaging.model.threadkey.ThreadKey) r13     // Catch:{ all -> 0x0253 }
            java.lang.String r5 = r13.A0J()     // Catch:{ all -> 0x0253 }
            X.0av r14 = X.C06160ax.A03(r3, r5)     // Catch:{ all -> 0x0253 }
            X.1V7 r5 = X.AnonymousClass1V7.A0A     // Catch:{ all -> 0x0253 }
            int r5 = r5.dbKeyValue     // Catch:{ all -> 0x0253 }
            java.lang.String r6 = java.lang.Integer.toString(r5)     // Catch:{ all -> 0x0253 }
            X.1V7 r5 = X.AnonymousClass1V7.A0J     // Catch:{ all -> 0x0253 }
            int r5 = r5.dbKeyValue     // Catch:{ all -> 0x0253 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ all -> 0x0253 }
            com.google.common.collect.ImmutableSet r5 = com.google.common.collect.ImmutableSet.A05(r6, r5)     // Catch:{ all -> 0x0253 }
            X.0av r15 = X.C06160ax.A04(r12, r5)     // Catch:{ all -> 0x0253 }
            java.lang.String r12 = "timestamp_ms"
            java.lang.String r6 = java.lang.Long.toString(r16)     // Catch:{ all -> 0x0253 }
            X.1gm r5 = new X.1gm     // Catch:{ all -> 0x0253 }
            r5.<init>(r12, r6)     // Catch:{ all -> 0x0253 }
            X.0av[] r5 = new X.C06140av[]{r14, r15, r5}     // Catch:{ all -> 0x0253 }
            X.1a6 r5 = X.C06160ax.A01(r5)     // Catch:{ all -> 0x0253 }
            r11.A05(r5)     // Catch:{ all -> 0x0253 }
            java.lang.String r6 = r13.A0J()     // Catch:{ all -> 0x0253 }
            java.lang.Object r5 = r2.A09(r9)     // Catch:{ all -> 0x0253 }
            r8.put(r6, r5)     // Catch:{ all -> 0x0253 }
            int r9 = r9 + 1
            goto L_0x0138
        L_0x0187:
            X.0Tq r5 = r0.A08     // Catch:{ all -> 0x0253 }
            java.lang.Object r5 = r5.get()     // Catch:{ all -> 0x0253 }
            X.1aN r5 = (X.C25771aN) r5     // Catch:{ all -> 0x0253 }
            android.database.sqlite.SQLiteDatabase r21 = r5.A06()     // Catch:{ all -> 0x0253 }
            java.lang.String[] r10 = new java.lang.String[]{r3, r12}     // Catch:{ all -> 0x0253 }
            java.lang.String r9 = r11.A02()     // Catch:{ all -> 0x0253 }
            java.lang.String[] r24 = r11.A04()     // Catch:{ all -> 0x0253 }
            r5 = 0
            r28 = 0
            android.database.sqlite.SQLiteQueryBuilder r3 = new android.database.sqlite.SQLiteQueryBuilder     // Catch:{ all -> 0x0253 }
            r3.<init>()     // Catch:{ all -> 0x0253 }
            java.lang.String r11 = X.C61162yR.A01(r10, r9, r5)     // Catch:{ all -> 0x0253 }
            r3.setTables(r11)     // Catch:{ all -> 0x0253 }
            r25 = 0
            r26 = 0
            r22 = r10
            r23 = r9
            r27 = r5
            r20 = r3
            android.database.Cursor r9 = r20.query(r21, r22, r23, r24, r25, r26, r27, r28)     // Catch:{ all -> 0x0253 }
        L_0x01be:
            boolean r3 = r9.moveToNext()     // Catch:{ all -> 0x024e }
            if (r3 == 0) goto L_0x01ec
            java.lang.String r3 = r9.getString(r1)     // Catch:{ all -> 0x024e }
            java.lang.Object r10 = r8.get(r3)     // Catch:{ all -> 0x024e }
            X.1gi r10 = (X.C29621gi) r10     // Catch:{ all -> 0x024e }
            java.lang.String r3 = r9.getString(r6)     // Catch:{ all -> 0x024e }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ all -> 0x024e }
            X.1V7 r5 = X.AnonymousClass1V7.A00(r3)     // Catch:{ all -> 0x024e }
            X.1V7 r3 = X.AnonymousClass1V7.A0A     // Catch:{ all -> 0x024e }
            if (r5 != r3) goto L_0x01e3
            X.0zh r5 = r10.A01     // Catch:{ all -> 0x024e }
            r5.A0z = r6     // Catch:{ all -> 0x024e }
            goto L_0x01be
        L_0x01e3:
            X.1V7 r3 = X.AnonymousClass1V7.A0J     // Catch:{ all -> 0x024e }
            if (r5 != r3) goto L_0x01be
            X.0zh r5 = r10.A01     // Catch:{ all -> 0x024e }
            r5.A11 = r6     // Catch:{ all -> 0x024e }
            goto L_0x01be
        L_0x01ec:
            r9.close()     // Catch:{ all -> 0x0253 }
            r3 = -896276921(0xffffffffca93e647, float:-4846371.5)
            X.C005505z.A00(r3)     // Catch:{ all -> 0x0263 }
            int r10 = r2.size()     // Catch:{ all -> 0x0263 }
        L_0x01f9:
            if (r1 >= r10) goto L_0x021a
            java.lang.Object r9 = r2.A07(r1)     // Catch:{ all -> 0x0263 }
            com.facebook.messaging.model.threadkey.ThreadKey r9 = (com.facebook.messaging.model.threadkey.ThreadKey) r9     // Catch:{ all -> 0x0263 }
            java.lang.Object r5 = r2.A09(r1)     // Catch:{ all -> 0x0263 }
            X.1gi r5 = (X.C29621gi) r5     // Catch:{ all -> 0x0263 }
            X.2wo r8 = new X.2wo     // Catch:{ all -> 0x0263 }
            X.0zh r3 = r5.A01     // Catch:{ all -> 0x0263 }
            com.facebook.messaging.model.threads.ThreadSummary r3 = r3.A00()     // Catch:{ all -> 0x0263 }
            long r5 = r5.A00     // Catch:{ all -> 0x0263 }
            r8.<init>(r3, r5)     // Catch:{ all -> 0x0263 }
            r4.put(r9, r8)     // Catch:{ all -> 0x0263 }
            int r1 = r1 + 1
            goto L_0x01f9
        L_0x021a:
            r1 = 470357373(0x1c09157d, float:4.535727E-22)
        L_0x021d:
            X.C005505z.A00(r1)     // Catch:{ all -> 0x026b }
            java.util.Iterator r5 = r30.iterator()     // Catch:{ all -> 0x026b }
        L_0x0224:
            boolean r1 = r5.hasNext()     // Catch:{ all -> 0x026b }
            if (r1 == 0) goto L_0x0242
            java.lang.Object r3 = r5.next()     // Catch:{ all -> 0x026b }
            com.facebook.messaging.model.threadkey.ThreadKey r3 = (com.facebook.messaging.model.threadkey.ThreadKey) r3     // Catch:{ all -> 0x026b }
            java.lang.Object r1 = r4.get(r3)     // Catch:{ all -> 0x026b }
            X.2wo r1 = (X.C60232wo) r1     // Catch:{ all -> 0x026b }
            r2 = r31
            com.facebook.messaging.service.model.FetchThreadResult r2 = r0.A05(r1, r2)     // Catch:{ all -> 0x026b }
            r1 = r19
            r1.put(r3, r2)     // Catch:{ all -> 0x026b }
            goto L_0x0224
        L_0x0242:
            r0 = -467307611(0xffffffffe42573a5, float:-1.2208183E22)
            X.C005505z.A00(r0)
            r0 = r18
            r0.setPriority(r7)
            return r19
        L_0x024e:
            r0 = move-exception
            r9.close()     // Catch:{ all -> 0x0253 }
            throw r0     // Catch:{ all -> 0x0253 }
        L_0x0253:
            r1 = move-exception
            r0 = -2063298020(0xffffffff8504921c, float:-6.233446E-36)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0263 }
            goto L_0x0262
        L_0x025b:
            r1 = move-exception
            r0 = -1660620370(0xffffffff9d04f1ae, float:-1.7595E-21)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0263 }
        L_0x0262:
            throw r1     // Catch:{ all -> 0x0263 }
        L_0x0263:
            r1 = move-exception
            r0 = 129189386(0x7b3460a, float:2.6974092E-34)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x026b }
            throw r1     // Catch:{ all -> 0x026b }
        L_0x026b:
            r1 = move-exception
            r0 = -1463581269(0xffffffffa8c385ab, float:-2.1707318E-14)
            X.C005505z.A00(r0)
            r0 = r18
            r0.setPriority(r7)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26691br.A07(java.util.Set, int):X.04b");
    }

    public Message A08(String str) {
        return (Message) A01(this, C06160ax.A03(TraceFieldType.MsgId, str), null, 1, false).A00.get(str);
    }

    public Message A0A(String str, String str2) {
        C05180Xy r1 = new C05180Xy();
        C05180Xy r0 = new C05180Xy();
        if (str != null) {
            r1.add(str);
        }
        if (str2 != null) {
            r0.add(str2);
        }
        Map A0K = A0K(r1, r0);
        if (A0K.size() > 0) {
            return (Message) A0K.values().iterator().next();
        }
        return null;
    }

    public MessagesCollection A0B(ThreadKey threadKey, MessagesCollection messagesCollection) {
        ImmutableList reverse = ImmutableList.copyOf(A0J(threadKey, -1, Integer.MAX_VALUE, AnonymousClass1V7.A0J, AnonymousClass1V7.A0A).values()).reverse();
        C33881oI r0 = new C33881oI();
        r0.A00 = threadKey;
        r0.A01(reverse);
        r0.A03 = false;
        r0.A02 = true;
        return AnonymousClass0mB.A00((AnonymousClass0mB) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BFT, this.A00), messagesCollection, r0.A00(), false);
    }

    public MessagesCollection A0C(ThreadSummary threadSummary, int i) {
        C005505z.A03("DbFetchThreadHandler.fetchThreadWithMessagesCollectionFromDb", -182265459);
        try {
            C61172yS r1 = new C61172yS();
            r1.A04 = threadSummary.A0S;
            r1.A00 = i;
            return A04(threadSummary, A0I(new C58002t0(r1)));
        } finally {
            C005505z.A00(-438701999);
        }
    }

    public ThreadSummary A0D(ThreadKey threadKey) {
        ThreadSummary threadSummary;
        C005505z.A03("DbFetchThreadHandler.fetchThreadSummaryFromDb", -1846399381);
        try {
            C60232wo A002 = A00(threadKey, "thread_key");
            if (A002 == null) {
                threadSummary = null;
            } else {
                threadSummary = A002.A01;
            }
            return threadSummary;
        } finally {
            C005505z.A00(1402971104);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001c, code lost:
        if (" ASC".equals(r12) != false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.service.model.FetchMoreMessagesResult A0E(com.facebook.messaging.model.threadkey.ThreadKey r6, long r7, long r9, int r11, java.lang.String r12) {
        /*
            r5 = this;
            X.2yS r2 = new X.2yS
            r2.<init>()
            r2.A04 = r6
            r2.A00 = r11
            r2.A02 = r7
            r2.A01 = r9
            java.lang.String r0 = " DESC"
            boolean r0 = r0.equals(r12)
            if (r0 != 0) goto L_0x001e
            java.lang.String r0 = " ASC"
            boolean r0 = r0.equals(r12)
            r1 = 0
            if (r0 == 0) goto L_0x001f
        L_0x001e:
            r1 = 1
        L_0x001f:
            java.lang.String r0 = "order should be equals to DESC or ASC"
            com.google.common.base.Preconditions.checkArgument(r1, r0)
            r2.A05 = r12
            X.2t0 r0 = new X.2t0
            r0.<init>(r2)
            java.util.LinkedHashMap r2 = r5.A0I(r0)
            java.lang.String r0 = r6.A0J()
            boolean r0 = r2.containsKey(r0)
            if (r0 == 0) goto L_0x0071
            java.lang.String r0 = r6.A0J()
            r2.remove(r0)
            r3 = 1
        L_0x0041:
            java.util.Collection r0 = r2.values()
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.copyOf(r0)
            boolean r0 = X.C12150od.A02(r6)
            if (r0 == 0) goto L_0x0054
            r0 = 0
            com.google.common.collect.ImmutableList r2 = r5.A06(r6, r2, r0)
        L_0x0054:
            X.1oI r1 = new X.1oI
            r1.<init>()
            r1.A00 = r6
            r1.A01(r2)
            r1.A03 = r3
            r0 = 1
            r1.A02 = r0
            com.facebook.messaging.model.messages.MessagesCollection r4 = r1.A00()
            com.facebook.messaging.service.model.FetchMoreMessagesResult r3 = new com.facebook.messaging.service.model.FetchMoreMessagesResult
            com.facebook.fbservice.results.DataFetchDisposition r2 = com.facebook.fbservice.results.DataFetchDisposition.A0F
            r0 = -1
            r3.<init>(r2, r4, r0)
            return r3
        L_0x0071:
            r3 = 0
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26691br.A0E(com.facebook.messaging.model.threadkey.ThreadKey, long, long, int, java.lang.String):com.facebook.messaging.service.model.FetchMoreMessagesResult");
    }

    public FetchThreadResult A0F(ThreadKey threadKey, int i) {
        C005505z.A03("DbFetchThreadHandler.fetchThreadFromDb", 1312418721);
        Thread currentThread = Thread.currentThread();
        int priority = currentThread.getPriority();
        try {
            currentThread.setPriority(10);
            return A05(A00(threadKey, "thread_key"), i);
        } finally {
            C005505z.A00(1376143744);
            currentThread.setPriority(priority);
        }
    }

    public FetchThreadResult A0G(ThreadKey threadKey, int i) {
        if (threadKey == null || !C12150od.A02(threadKey)) {
            return FetchThreadResult.A09;
        }
        C005505z.A03("DbFetchThreadHandler.fetchThreadFromDbByMontageThread", 242471866);
        try {
            return A05(A00(threadKey, "montage_thread_key"), i);
        } finally {
            C005505z.A00(-1288087787);
        }
    }

    public LinkedHashMap A0I(C58002t0 r8) {
        C005505z.A03("DbFetchThreadHandler.fetchMessagesFromDb", 1174087239);
        try {
            C25601a6 A002 = C06160ax.A00();
            ThreadKey threadKey = r8.A04;
            if (threadKey != null) {
                A002.A05(C06160ax.A03("thread_key", threadKey.A0J()));
            }
            C10950l8 r0 = r8.A03;
            if (r0 != null) {
                A002.A05(C06160ax.A03("folder", r0.dbName));
            }
            long j = r8.A02;
            long j2 = (long) -1;
            if (j != j2) {
                A002.A05(new AnonymousClass13P("timestamp_ms", Long.toString(j)));
            }
            long j3 = r8.A01;
            if (j3 != j2) {
                A002.A05(new C46962Sr("timestamp_ms", Long.toString(j3)));
            }
            String str = r8.A05;
            return A01(this, A002, AnonymousClass08S.A0J("timestamp_ms", str), r8.A00, str.contains(" ASC")).A00;
        } finally {
            C005505z.A00(-1948812743);
        }
    }

    private C26691br(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A08 = C25771aN.A02(r3);
        this.A09 = AnonymousClass0VG.A00(AnonymousClass1Y3.AnN, r3);
        this.A0B = MessageCursorUtil.A00(r3);
        C12130oa.A00(r3);
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.Amz, r3);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.Arr, r3);
        this.A0A = C12160of.A00(r3);
        this.A04 = new C07040cX(r3);
        this.A03 = new C07050cY(r3);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.Aeq, r3);
    }

    public static final C26691br A02(AnonymousClass1XY r0) {
        return A03(r0);
    }

    public FetchThreadResult A0H(ThreadCriteria threadCriteria, int i) {
        if (threadCriteria.A01() != null) {
            return A0F(threadCriteria.A01(), i);
        }
        throw new IllegalArgumentException("No threadkey specified.");
    }

    public LinkedHashMap A0J(ThreadKey threadKey, long j, int i, AnonymousClass1V7... r11) {
        C25601a6 A002 = C06160ax.A00();
        if (threadKey != null) {
            A002.A05(C06160ax.A03("thread_key", threadKey.toString()));
        }
        if (j != -1) {
            A002.A05(new C46962Sr("timestamp_ms", Long.toString(j)));
        }
        HashSet hashSet = new HashSet();
        for (AnonymousClass1V7 r0 : r11) {
            hashSet.add(Integer.valueOf(r0.dbKeyValue));
        }
        if (!hashSet.isEmpty()) {
            A002.A05(C06160ax.A04(TraceFieldType.MsgType, hashSet));
        }
        return A01(this, A002, "timestamp_ms", i, false).A00;
    }

    public Map A0K(Collection collection, Collection collection2) {
        if (!collection.isEmpty() || !collection2.isEmpty()) {
            return A01(this, C06160ax.A02(C06160ax.A04(TraceFieldType.MsgId, collection), C06160ax.A04("offline_threading_id", collection2)), null, collection.size() + collection2.size(), false).A00;
        }
        return RegularImmutableMap.A03;
    }
}
