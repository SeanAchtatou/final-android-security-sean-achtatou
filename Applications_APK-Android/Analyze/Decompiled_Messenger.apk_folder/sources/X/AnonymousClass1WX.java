package X;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

/* renamed from: X.1WX  reason: invalid class name */
public final class AnonymousClass1WX {
    public final int A00;
    public final int A01;
    public final C24551Wa A02;
    public final Set A03;
    public final Set A04;
    public final Set A05;

    public static AnonymousClass1WX A00(Object obj, Class cls, Class... clsArr) {
        AnonymousClass1WY r2 = new AnonymousClass1WY(cls, clsArr);
        AnonymousClass1WZ r1 = new AnonymousClass1WZ(obj);
        AnonymousClass09E.A02(r1, "Null factory");
        r2.A02 = r1;
        return r2.A00();
    }

    public String toString() {
        return "Component<" + Arrays.toString(this.A04.toArray()) + ">{" + this.A00 + ", type=" + this.A01 + ", deps=" + Arrays.toString(this.A03.toArray()) + "}";
    }

    public AnonymousClass1WX(Set set, Set set2, int i, int i2, C24551Wa r6, Set set3) {
        this.A04 = Collections.unmodifiableSet(set);
        this.A03 = Collections.unmodifiableSet(set2);
        this.A00 = i;
        this.A01 = i2;
        this.A02 = r6;
        this.A05 = Collections.unmodifiableSet(set3);
    }
}
