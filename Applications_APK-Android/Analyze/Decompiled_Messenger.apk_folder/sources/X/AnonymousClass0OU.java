package X;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Choreographer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.0OU  reason: invalid class name */
public final class AnonymousClass0OU {
    public static AnonymousClass0OU A04;
    public static final Runnable A05 = new C84073yo();
    public Choreographer A00;
    public Method A01;
    public boolean A02;
    private Handler A03;

    public void A00(Runnable runnable) {
        if (!this.A02) {
            if (this.A03 == null) {
                this.A03 = new Handler(Looper.getMainLooper());
            }
            Handler handler = this.A03;
            handler.sendMessageAtFrontOfQueue(Message.obtain(handler, runnable));
            return;
        }
        try {
            this.A01.invoke(this.A00, 3, runnable, null);
        } catch (IllegalAccessException | InvocationTargetException unused) {
        }
    }

    public AnonymousClass0OU() {
        boolean z;
        boolean z2;
        boolean z3 = false;
        if (Build.VERSION.SDK_INT >= 23) {
            this.A00 = Choreographer.getInstance();
            try {
                this.A01 = Choreographer.class.getMethod("postCallback", Integer.TYPE, Runnable.class, Object.class);
                z = true;
            } catch (NoSuchMethodException unused) {
                z = false;
            }
            if (z) {
                try {
                    this.A01.invoke(this.A00, 3, A05, null);
                    z2 = true;
                } catch (IllegalAccessException | InvocationTargetException unused2) {
                    z2 = false;
                }
                if (z2) {
                    z3 = true;
                }
            }
        }
        this.A02 = z3;
    }
}
