package X;

import com.facebook.proxygen.HTTPClient;
import io.card.payment.BuildConfig;

/* renamed from: X.10h  reason: invalid class name and case insensitive filesystem */
public final class C181310h {
    public int A00;
    public int A01;
    public C29347EWa A02;
    public String A03;
    public String A04;
    public String A05;

    public void A00(C29347EWa eWa) {
        this.A02 = eWa;
        C29354EWh eWh = eWa.A00;
        if (eWh != null) {
            this.A04 = eWh.A01;
            this.A00 = eWh.A00;
        } else {
            this.A04 = BuildConfig.FLAVOR;
            this.A00 = 0;
        }
        C29354EWh eWh2 = eWa.A01;
        if (eWh2 != null) {
            this.A05 = eWh2.A01;
            this.A01 = eWh2.A00;
        } else {
            this.A05 = BuildConfig.FLAVOR;
            this.A01 = 0;
        }
        this.A03 = C06850cB.A06(",", eWa.A02);
    }

    public void A01(HTTPClient hTTPClient, boolean z) {
        hTTPClient.setProxy(this.A04, this.A00, BuildConfig.FLAVOR, BuildConfig.FLAVOR);
        hTTPClient.setSecureProxy(this.A05, this.A01, BuildConfig.FLAVOR, BuildConfig.FLAVOR);
        hTTPClient.setBypassProxyDomains(this.A03);
        hTTPClient.setIsSandbox(z);
        hTTPClient.reInitializeIfNeeded();
    }
}
