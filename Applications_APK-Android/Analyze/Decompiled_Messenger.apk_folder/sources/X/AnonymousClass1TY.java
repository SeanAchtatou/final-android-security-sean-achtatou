package X;

/* renamed from: X.1TY  reason: invalid class name */
public final class AnonymousClass1TY implements AnonymousClass1TW {
    public AnonymousClass0UN A00;

    public static final AnonymousClass1TY A00(AnonymousClass1XY r1) {
        return new AnonymousClass1TY(r1);
    }

    private C24281Sz A01(String str, String str2, String str3) {
        C24281Sz r1 = new C24281Sz(str, str2, str3);
        r1.A01 = C23691Qm.A00;
        r1.A00 = new C23731Qq(this);
        return r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0038, code lost:
        if (r1.A00() == false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00e9, code lost:
        if (r1 == false) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00ff, code lost:
        if (r0 == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001f, code lost:
        if (((java.lang.Boolean) X.AnonymousClass1XX.A02(6, X.AnonymousClass1Y3.Ayu, r4)).booleanValue() != false) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x017d, code lost:
        if (r2 == X.C17930zi.VIDEO_CALL) goto L_0x017f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C23761Qt Afj(com.facebook.messaging.model.threads.ThreadSummary r7) {
        /*
            r6 = this;
            int r1 = X.AnonymousClass1Y3.B0T
            X.0UN r4 = r6.A00
            r0 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r1, r4)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0021
            r1 = 6
            int r0 = X.AnonymousClass1Y3.Ayu
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r4)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r1 = r0.booleanValue()
            r0 = 0
            if (r1 == 0) goto L_0x0022
        L_0x0021:
            r0 = 1
        L_0x0022:
            r3 = 0
            if (r0 == 0) goto L_0x0226
            if (r7 == 0) goto L_0x003a
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r1 = r7.A0h
            if (r1 == 0) goto L_0x003a
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0S
            boolean r0 = r0.A0L()
            if (r0 == 0) goto L_0x003a
            boolean r1 = r1.A00()
            r0 = 1
            if (r1 != 0) goto L_0x003b
        L_0x003a:
            r0 = 0
        L_0x003b:
            if (r0 == 0) goto L_0x012b
            int r0 = X.AnonymousClass1Y3.BH8
            r2 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r0, r4)
            X.1nP r1 = (X.C33331nP) r1
            r0 = 0
            boolean r0 = X.C33331nP.A03(r1, r7, r0)
            if (r0 != 0) goto L_0x005e
            int r1 = X.AnonymousClass1Y3.BH8
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1nP r1 = (X.C33331nP) r1
            r0 = 1
            boolean r0 = X.C33331nP.A03(r1, r7, r0)
            if (r0 == 0) goto L_0x012b
        L_0x005e:
            r2 = 7
            int r1 = X.AnonymousClass1Y3.Ag9
            X.0UN r0 = r6.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3Sk r3 = (X.C68333Sk) r3
            X.BfF r2 = new X.BfF
            r2.<init>()
            java.lang.String r1 = "GROUP"
            r2.A01 = r1
            r0 = 200(0xc8, float:2.8E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            X.C28931fb.A06(r1, r0)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0S
            long r0 = r0.A0G()
            r2.A00 = r0
            X.BfE r5 = new X.BfE
            r5.<init>(r2)
            X.BfE r0 = r3.A02
            boolean r0 = com.google.common.base.Objects.equal(r5, r0)
            if (r0 != 0) goto L_0x00c2
            r3.A02 = r5
            r2 = 0
            int r1 = X.AnonymousClass1Y3.Aqp
            X.0UN r0 = r3.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2rQ r4 = (X.C57102rQ) r4
            X.3Sm r3 = new X.3Sm
            r3.<init>()
            java.lang.String r0 = "inbox_cta"
            r3.A0C = r0
            java.lang.String r0 = r5.A01
            r3.A0D = r0
            long r0 = r5.A00
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r3.A09 = r0
            int r2 = X.AnonymousClass1Y3.AdR
            X.0UN r1 = r4.A01
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.3Sn r1 = (X.C68363Sn) r1
            java.lang.String r0 = "rtc_callsite_impression"
            r1.A01(r0, r3)
        L_0x00c2:
            int r2 = X.AnonymousClass1Y3.AEg
            X.0UN r1 = r6.A00
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            android.content.res.Resources r1 = (android.content.res.Resources) r1
            r0 = 2131834663(0x7f113727, float:1.9302443E38)
            java.lang.String r2 = r1.getString(r0)
            java.lang.String r1 = "rtc_group_call"
            java.lang.String r0 = "join_call"
            X.1Sz r4 = r6.A01(r2, r1, r0)
            if (r7 == 0) goto L_0x00eb
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r0 = r7.A0h
            java.lang.String r2 = r0.A00
            java.lang.String r0 = "VIDEO_GROUP_CALL"
            r1 = 0
            if (r2 != r0) goto L_0x00e8
            r1 = 1
        L_0x00e8:
            r0 = 1
            if (r1 != 0) goto L_0x00ec
        L_0x00eb:
            r0 = 0
        L_0x00ec:
            if (r0 == 0) goto L_0x0128
            X.1Qm r0 = X.C23691Qm.A03
        L_0x00f0:
            r4.A01 = r0
            if (r7 == 0) goto L_0x0101
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r0 = r7.A0h
            java.lang.String r2 = r0.A00
            java.lang.String r1 = "VIDEO_GROUP_CALL"
            r0 = 0
            if (r2 != r1) goto L_0x00fe
            r0 = 1
        L_0x00fe:
            r3 = 1
            if (r0 != 0) goto L_0x0102
        L_0x0101:
            r3 = 0
        L_0x0102:
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AEg
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            android.content.res.Resources r1 = (android.content.res.Resources) r1
            if (r3 == 0) goto L_0x0120
            r0 = 2131834924(0x7f11382c, float:1.9302972E38)
            java.lang.String r0 = r1.getString(r0)
        L_0x0116:
            r4.A04 = r0
            r4.A07 = r2
            X.1Qt r0 = new X.1Qt
            r0.<init>(r4)
            return r0
        L_0x0120:
            r0 = 2131834920(0x7f113828, float:1.9302964E38)
            java.lang.String r0 = r1.getString(r0)
            goto L_0x0116
        L_0x0128:
            X.1Qm r0 = X.C23691Qm.A02
            goto L_0x00f0
        L_0x012b:
            boolean r0 = X.AnonymousClass1JT.A01(r7)
            if (r0 == 0) goto L_0x0226
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r6.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 283107064350900(0x1017c000108b4, double:1.39873474590747E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x01bf
            int r2 = X.AnonymousClass1Y3.AbH
            X.0UN r1 = r6.A00
            r0 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1JT r0 = (X.AnonymousClass1JT) r0
            boolean r0 = r0.A02(r7)
            java.lang.String r2 = "rtc_canonical"
            r4 = 1
            if (r0 == 0) goto L_0x01a3
            int r1 = X.AnonymousClass1Y3.AEg
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            android.content.res.Resources r1 = (android.content.res.Resources) r1
            r0 = 2131833810(0x7f1133d2, float:1.9300713E38)
            java.lang.String r1 = r1.getString(r0)
            java.lang.String r0 = "call_again"
            X.1Sz r3 = r6.A01(r1, r2, r0)
        L_0x0172:
            X.0UN r4 = r6.A00
            X.0zi r2 = r7.A0i
            X.0zi r0 = X.C17930zi.RECENT_VIDEO_CALL
            if (r2 == r0) goto L_0x017f
            X.0zi r1 = X.C17930zi.VIDEO_CALL
            r0 = 0
            if (r2 != r1) goto L_0x0180
        L_0x017f:
            r0 = 1
        L_0x0180:
            if (r0 == 0) goto L_0x0186
            X.1Qm r0 = X.C23691Qm.A05
            r3.A01 = r0
        L_0x0186:
            r0 = 1
            r3.A07 = r0
            r1 = 0
            int r0 = X.AnonymousClass1Y3.AOJ
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r0, r4)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 283107064416437(0x1017c000208b5, double:1.398734746231263E-309)
            boolean r0 = r2.Aem(r0)
            r3.A06 = r0
            X.1Qt r0 = new X.1Qt
            r0.<init>(r3)
            return r0
        L_0x01a3:
            int r1 = X.AnonymousClass1Y3.AEg
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            android.content.res.Resources r1 = (android.content.res.Resources) r1
            r0 = 2131833810(0x7f1133d2, float:1.9300713E38)
            java.lang.String r1 = r1.getString(r0)
            java.lang.String r0 = "call_back"
            X.1Sz r3 = r6.A01(r1, r2, r0)
            X.1Qm r0 = X.C23691Qm.A04
            r3.A01 = r0
            goto L_0x0172
        L_0x01bf:
            int r2 = X.AnonymousClass1Y3.AbH
            X.0UN r1 = r6.A00
            r0 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1JT r0 = (X.AnonymousClass1JT) r0
            boolean r0 = r0.A02(r7)
            r2 = 0
            java.lang.String r4 = "rtc_canonical"
            r5 = 1
            if (r0 == 0) goto L_0x020a
            int r1 = X.AnonymousClass1Y3.AEg
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r1, r0)
            android.content.res.Resources r1 = (android.content.res.Resources) r1
            r0 = 2131833808(0x7f1133d0, float:1.9300709E38)
            java.lang.String r1 = r1.getString(r0)
            java.lang.String r0 = "call_again"
            X.1Sz r3 = r6.A01(r1, r4, r0)
        L_0x01eb:
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r6.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 283107064285363(0x1017c000008b3, double:1.39873474558367E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0204
            X.1Qm r0 = X.C23691Qm.A05
            r3.A01 = r0
        L_0x0204:
            X.1Qt r0 = new X.1Qt
            r0.<init>(r3)
            return r0
        L_0x020a:
            int r1 = X.AnonymousClass1Y3.AEg
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r1, r0)
            android.content.res.Resources r1 = (android.content.res.Resources) r1
            r0 = 2131833809(0x7f1133d1, float:1.930071E38)
            java.lang.String r1 = r1.getString(r0)
            java.lang.String r0 = "call_back"
            X.1Sz r3 = r6.A01(r1, r4, r0)
            X.1Qm r0 = X.C23691Qm.A04
            r3.A01 = r0
            goto L_0x01eb
        L_0x0226:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TY.Afj(com.facebook.messaging.model.threads.ThreadSummary):X.1Qt");
    }

    private AnonymousClass1TY(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(8, r3);
    }
}
