package X;

import com.facebook.acra.ACRA;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.user.model.User;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableList;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.16R  reason: invalid class name */
public final class AnonymousClass16R implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.orca.threadlist.inbox.InboxLoaderCoordinator";
    public long A00;
    public AnonymousClass0WP A01;
    public AnonymousClass1Y6 A02;
    public AnonymousClass0XX A03;
    public AnonymousClass1G0 A04;
    public C20021Ap A05;
    public AnonymousClass06B A06;
    public BlueServiceOperationFactory A07;
    public AnonymousClass0UN A08;
    public C34801qC A09;
    public AnonymousClass16S A0A;
    public AnonymousClass101 A0B;
    public AnonymousClass17O A0C;
    public MessageRequestsSnippet A0D;
    public C10950l8 A0E;
    public C31371ja A0F;
    public C187815b A0G;
    public C13880sE A0H;
    public C32771mJ A0I;
    public AnonymousClass7KN A0J;
    public C04810Wg A0K;
    public ScheduledExecutorService A0L;
    public ScheduledFuture A0M;
    public boolean A0N;
    public boolean A0O;
    private C190016k A0P;
    private C04310Tq A0Q;
    public final AnonymousClass10R A0R;
    public final C32421lj A0S;
    public final C32271lU A0T;
    public final C31371ja A0U;
    public final Object A0V = new Object();
    public final Object A0W = new Object();
    public final Map A0X = new EnumMap(C10700ki.class);
    public final C04310Tq A0Y;
    private final AtomicLong A0Z = new AtomicLong(0);
    public volatile boolean A0a = false;
    public volatile boolean A0b = false;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0066, code lost:
        if ((r5.A01.now() - r5.A00) >= r5.A02.At0(563903436161634L)) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a8, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0124, code lost:
        if ((!r5.A01.isEmpty()) != false) goto L_0x0126;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0126, code lost:
        if (r1 == false) goto L_0x0128;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00d1 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A04(X.AnonymousClass16R r14, java.lang.Integer r15, java.lang.String r16) {
        /*
            r3 = r14
            X.1Y6 r1 = r14.A02
            boolean r0 = r1.BHM()
            r4 = r15
            if (r0 != 0) goto L_0x0015
            X.3hk r0 = new X.3hk
            r2 = r16
            r0.<init>(r14, r15, r2)
            r1.C4C(r0)
            return
        L_0x0015:
            X.0sE r9 = new X.0sE
            X.101 r10 = r14.A0B
            X.0sF r11 = r14.A09()
            X.16S r0 = r14.A0A
            X.0w2 r12 = r0.A05()
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r13 = r14.A0D
            X.1qC r14 = r14.A09
            if (r14 != 0) goto L_0x002b
            X.1qC r14 = X.C34801qC.A03
        L_0x002b:
            r2 = 22
            int r1 = X.AnonymousClass1Y3.BQP
            X.0UN r0 = r3.A08
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1G8 r2 = (X.AnonymousClass1G8) r2
            X.1Yd r5 = r2.A04
            r0 = 282428460434820(0x100de000f0584, double:1.395381997086783E-309)
            boolean r0 = r5.Aem(r0)
            r15 = 0
            if (r0 == 0) goto L_0x00f5
            X.1G9 r5 = r2.A02
            long r0 = r5.A00
            r7 = -1
            int r6 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r6 == 0) goto L_0x0068
            X.069 r0 = r5.A01
            long r7 = r0.now()
            long r0 = r5.A00
            long r7 = r7 - r0
            X.1Yd r5 = r5.A02
            r0 = 563903436161634(0x200de00000262, double:2.78605315379296E-309)
            long r5 = r5.At0(r0)
            int r1 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            r0 = 1
            if (r1 < 0) goto L_0x0069
        L_0x0068:
            r0 = 0
        L_0x0069:
            if (r0 == 0) goto L_0x00f5
            r2.A00 = r15
        L_0x006d:
            X.0ki r1 = X.C10700ki.PINNED
            java.util.Map r0 = r3.A0X
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x00f2
            java.util.Map r0 = r3.A0X
            java.lang.Object r0 = r0.get(r1)
            X.0sF r0 = (X.C13890sF) r0
        L_0x007f:
            r16 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            r3.A0H = r9
            X.15b r6 = r3.A0G
            com.facebook.orca.threadlist.ThreadListFragment r5 = r6.A00
            X.0sF r0 = r9.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            com.google.common.collect.ImmutableList r0 = r0.A00
            X.1Xv r2 = r0.iterator()
        L_0x0094:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00d1
            java.lang.Object r0 = r2.next()
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            X.1fF r1 = r0.A05
            X.1fF r0 = X.C28711fF.GROUP
            if (r1 != r0) goto L_0x0094
        L_0x00a8:
            r0 = 1
        L_0x00a9:
            r5.A1b = r0
            com.facebook.orca.threadlist.ThreadListFragment r5 = r6.A00
            r0 = 1
            r5.A1Y = r0
            X.0vF r2 = r5.A16
            boolean r1 = r2.A00
            if (r1 != 0) goto L_0x00b9
            r2.A01 = r0
            r0 = 0
        L_0x00b9:
            if (r0 == 0) goto L_0x00c0
            X.16N r0 = r5.A11
            r0.A03(r4, r9)
        L_0x00c0:
            r2 = 7
            int r1 = X.AnonymousClass1Y3.Aj7
            X.0UN r0 = r3.A08
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1G2 r5 = (X.AnonymousClass1G2) r5
            X.0sE r4 = r3.A0H
            monitor-enter(r5)
            if (r4 == 0) goto L_0x015c
            goto L_0x013b
        L_0x00d1:
            X.0sF r0 = r9.A05
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            com.google.common.collect.ImmutableList r0 = r0.A00
            X.1Xv r2 = r0.iterator()
        L_0x00db:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00f0
            java.lang.Object r0 = r2.next()
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            X.1fF r1 = r0.A05
            X.1fF r0 = X.C28711fF.GROUP
            if (r1 != r0) goto L_0x00db
            goto L_0x00a8
        L_0x00f0:
            r0 = 0
            goto L_0x00a9
        L_0x00f2:
            X.0sF r0 = X.C13890sF.A03
            goto L_0x007f
        L_0x00f5:
            X.1Yd r5 = r2.A04
            r0 = 282428460369283(0x100de000e0583, double:1.395381996762987E-309)
            boolean r0 = r5.Aem(r0)
            if (r0 == 0) goto L_0x0128
            X.1Ac r5 = r2.A03
            boolean r0 = r5.A00
            r1 = 1
            if (r0 == 0) goto L_0x0113
            java.util.Set r0 = r5.A01
            r0.isEmpty()
            java.util.Set r0 = r5.A01
            r0.isEmpty()
        L_0x0113:
            boolean r0 = r5.A00
            if (r0 == 0) goto L_0x0139
            java.util.Set r0 = r5.A01
            r0.isEmpty()
            java.util.Set r0 = r5.A01
            boolean r0 = r0.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0139
        L_0x0126:
            if (r1 != 0) goto L_0x012e
        L_0x0128:
            X.101 r0 = r2.A01
            if (r0 == 0) goto L_0x012e
            r2.A00 = r0
        L_0x012e:
            X.101 r1 = r2.A01
            X.101 r0 = r2.A00
            if (r1 != r0) goto L_0x0136
            r2.A01 = r15
        L_0x0136:
            r15 = r0
            goto L_0x006d
        L_0x0139:
            r1 = 0
            goto L_0x0126
        L_0x013b:
            boolean r0 = r5.A07     // Catch:{ all -> 0x0159 }
            if (r0 == 0) goto L_0x015c
            X.0sE r0 = r5.A03     // Catch:{ all -> 0x0159 }
            if (r0 == 0) goto L_0x014c
            X.0sF r1 = r0.A06     // Catch:{ all -> 0x0159 }
            X.0sF r0 = r4.A06     // Catch:{ all -> 0x0159 }
            if (r1 != r0) goto L_0x014c
        L_0x0149:
            r5.A03 = r4     // Catch:{ all -> 0x0159 }
            goto L_0x015c
        L_0x014c:
            X.0sF r0 = r4.A06     // Catch:{ all -> 0x0159 }
            com.facebook.messaging.model.threads.ThreadsCollection r2 = r0.A02     // Catch:{ all -> 0x0159 }
            X.0sF r0 = r4.A05     // Catch:{ all -> 0x0159 }
            com.facebook.messaging.model.threads.ThreadsCollection r1 = r0.A02     // Catch:{ all -> 0x0159 }
            r0 = 0
            X.AnonymousClass1G2.A02(r5, r2, r1, r0)     // Catch:{ all -> 0x0159 }
            goto L_0x0149
        L_0x0159:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x015c:
            monitor-exit(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass16R.A04(X.16R, java.lang.Integer, java.lang.String):void");
    }

    public static void A00(AnonymousClass16R r3) {
        if (!r3.A0a) {
            r3.A0a = true;
            ((C28461eq) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aq4, r3.A08)).A06(r3.A0V);
        }
    }

    public static void A01(AnonymousClass16R r3) {
        if (!r3.A0b) {
            r3.A0b = true;
            ((C28461eq) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aq4, r3.A08)).A06(r3.A0W);
        }
    }

    public static void A02(AnonymousClass16R r6, AnonymousClass1FD r7, String str) {
        if (r7 != AnonymousClass1FD.EXPLICIT_USER_REFRESH || !((Boolean) r6.A0Q.get()).booleanValue()) {
            C10950l8 r1 = C10950l8.A05;
            r6.A0E = r1;
            r6.A0F.A0H(r1);
            C31371ja r3 = r6.A0F;
            ImmutableSet A042 = ((C10960l9) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BOk, r6.A08)).A04();
            Preconditions.checkNotNull(A042);
            r3.A08 = A042;
            r6.A0F.CHB(r7.A00(r6.A0G.A00.A0g, str));
        } else if (r6.A04 == null) {
            C27211cp A022 = r6.A0T.A02(AnonymousClass07W.A01(r6.A07, "ensure_sync", r6.A0I.A03(C162667fk.REFRESH_CONNECTION), CallerContext.A09("InboxLoaderCoordinator"), 1674235505));
            AnonymousClass1GQ r12 = new AnonymousClass1GQ(r6);
            r6.A04 = AnonymousClass1G0.A00(A022, r12);
            C05350Yp.A07(A022, r12);
        }
    }

    public static void A03(AnonymousClass16R r3, AnonymousClass1FD r4, String str) {
        if (((Boolean) AnonymousClass1XX.A02(25, AnonymousClass1Y3.BAo, r3.A08)).booleanValue()) {
            r3.A0U.A0H(C10950l8.A0B);
            r3.A0U.CHB(r4.A00(r3.A0G.A00.A0g, str));
        }
    }

    public static void A05(AnonymousClass16R r5, Throwable th) {
        String message;
        if (th instanceof CancellationException) {
            C20921Ei r3 = (C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, r5.A08);
            if (C20921Ei.A03(r3)) {
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r3.A00)).markerPoint(5505198, "merging_cancelled");
            }
        } else {
            if (th != null) {
                C010708t.A0N("InboxLoaderCoordinator", "[MontageAndActiveNowMergeRanker] onFailure", th);
            } else {
                C010708t.A0K("InboxLoaderCoordinator", "[MontageAndActiveNowMergeRanker] onSuccess but null result");
            }
            C20921Ei r4 = (C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, r5.A08);
            if (th == null) {
                message = null;
            } else {
                message = th.getMessage();
            }
            if (C20921Ei.A03(r4)) {
                C21061Ew withMarker = ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r4.A00)).withMarker(5505198);
                if (message != null) {
                    withMarker.A08("merging_error", message);
                }
                withMarker.A03("merging_fail");
                withMarker.BK9();
            }
        }
        AnonymousClass16S r1 = r5.A0A;
        ImmutableList immutableList = RegularImmutableList.A02;
        r1.A06 = immutableList;
        r1.A05 = immutableList;
        AnonymousClass2Q1 r2 = (AnonymousClass2Q1) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BMF, r5.A08);
        A04(r5, C74373hj.A00(r2.A03), r2.A02);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0034, code lost:
        if (r1 == false) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A06(X.AnonymousClass16R r4) {
        /*
            X.16k r0 = r4.A0P
            X.1YI r2 = r0.A00
            r1 = 206(0xce, float:2.89E-43)
            r0 = 0
            boolean r0 = r2.AbO(r1, r0)
            if (r0 == 0) goto L_0x004b
            X.17O r0 = r4.A0C
            X.0Zh r4 = r0.A03
            X.1a7 r0 = X.C25611a7.ALL
            X.1a7 r1 = X.C25611a7.TOP
            java.util.EnumSet r0 = java.util.EnumSet.of(r0, r1)
            boolean r3 = r0.contains(r1)
            X.1Zx r0 = r4.A06
            com.facebook.prefs.shared.FbSharedPreferences r2 = r0.A07
            X.1Y7 r1 = X.C05690aA.A0c
            r0 = 0
            boolean r1 = r2.Aep(r1, r0)
            java.lang.Boolean r0 = r4.A02
            if (r0 != 0) goto L_0x0042
            monitor-enter(r4)
            java.lang.Boolean r0 = r4.A02     // Catch:{ all -> 0x003f }
            if (r0 != 0) goto L_0x003d
            if (r3 == 0) goto L_0x0036
            r0 = 1
            if (r1 != 0) goto L_0x0037
        L_0x0036:
            r0 = 0
        L_0x0037:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x003f }
            r4.A02 = r0     // Catch:{ all -> 0x003f }
        L_0x003d:
            monitor-exit(r4)     // Catch:{ all -> 0x003f }
            goto L_0x0042
        L_0x003f:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x003f }
            throw r0
        L_0x0042:
            java.lang.Boolean r0 = r4.A02
            boolean r1 = r0.booleanValue()
            r0 = 1
            if (r1 != 0) goto L_0x004c
        L_0x004b:
            r0 = 0
        L_0x004c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass16R.A06(X.16R):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r0 == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A07(X.AnonymousClass16R r4, X.C13890sF r5) {
        /*
            com.facebook.fbservice.results.DataFetchDisposition r3 = r5.A01
            boolean r0 = r3.A08
            r2 = 0
            if (r0 != 0) goto L_0x0011
            X.1ja r0 = r4.A0F
            X.1G0 r1 = r0.A02
            r0 = 0
            if (r1 == 0) goto L_0x000f
            r0 = 1
        L_0x000f:
            if (r0 != 0) goto L_0x001b
        L_0x0011:
            com.facebook.common.util.TriState r0 = r3.A05
            boolean r0 = r0.asBoolean(r2)
            if (r0 != 0) goto L_0x001b
            r0 = 1
            return r0
        L_0x001b:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass16R.A07(X.16R, X.0sF):boolean");
    }

    /* JADX INFO: finally extract failed */
    public static boolean A08(AnonymousClass16R r10, Integer num, String str) {
        ImmutableList immutableList;
        String str2;
        boolean z;
        if (r10.A0K == null) {
            return false;
        }
        C20921Ei r3 = (C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, r10.A08);
        if (C20921Ei.A03(r3)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r3.A00)).markerPoint(5505198, "merging_start");
        }
        AnonymousClass2Q1 r4 = (AnonymousClass2Q1) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BMF, r10.A08);
        C34801qC r0 = r10.A09;
        if (r0 != null) {
            immutableList = r0.A00;
        } else {
            immutableList = RegularImmutableList.A02;
        }
        C15820w2 A052 = r10.A0A.A05();
        C04810Wg r7 = r10.A0K;
        switch (num.intValue()) {
            case 1:
                str2 = "THREADS";
                break;
            case 2:
                str2 = "INBOX_TOP_UNITS";
                break;
            case 3:
                str2 = "ACTIVE_NOW";
                break;
            case 4:
                str2 = "INBOX_ADS";
                break;
            case 5:
                str2 = "WORKCHAT_USER_STATUS";
                break;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                str2 = "OTHER";
                break;
            default:
                str2 = "USER_ACTION";
                break;
        }
        C005505z.A03("ANL:MontageAndActiveNowMergeRanker:mergeMontageAndActiveNowResult", -1641634163);
        try {
            C07410dQ A012 = ImmutableSet.A01();
            ImmutableList.Builder builder = new ImmutableList.Builder();
            if (C013509w.A01(A052.A00.A00)) {
                C24971Xv it = A052.A00.A00.iterator();
                while (it.hasNext()) {
                    User user = (User) it.next();
                    builder.add((Object) user.A0Q);
                    r4.A04.add(user.A0Q);
                }
            }
            A012.A00(builder.build());
            ImmutableList.Builder builder2 = new ImmutableList.Builder();
            if (C013509w.A01(immutableList)) {
                for (int i = 0; i < immutableList.size(); i++) {
                    BasicMontageThreadInfo basicMontageThreadInfo = (BasicMontageThreadInfo) immutableList.get(i);
                    builder2.add((Object) basicMontageThreadInfo.A03);
                    r4.A05.add(basicMontageThreadInfo.A03);
                }
            }
            A012.A00(builder2.build());
            r4.A03 = str2;
            r4.A02 = str;
            r4.A01 = A012.build().asList();
            int i2 = AnonymousClass1Y3.A6N;
            AnonymousClass0UN r6 = r4.A00;
            C05350Yp.A08(((AnonymousClass1YG) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A8u, r6)).CII(AnonymousClass08S.A09("MontageAndActiveNowMergeRanker-", System.identityHashCode((AnonymousClass2RK) AnonymousClass1XX.A02(3, i2, r6))), new C75363ji(r4)), r7, C25141Ym.INSTANCE);
            C005505z.A00(-2012111469);
            if (!AnonymousClass10R.A01((AnonymousClass10R) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BO4, r4.A00), true)) {
                return true;
            }
            AnonymousClass2RK r62 = (AnonymousClass2RK) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A6N, r4.A00);
            synchronized (r62) {
                long At0 = ((AnonymousClass10R) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BO4, r62.A01)).A00.At0(563800361206362L);
                z = true;
                if (At0 <= 0 || ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, r62.A01)).now() - r62.A00 <= At0) {
                    z = false;
                }
            }
            if (!z) {
                return true;
            }
            AnonymousClass07A.A04((AnonymousClass0VL) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ASK, r4.A00), new C83663xz(r4), -1511335744);
            return true;
        } catch (Throwable th) {
            C005505z.A00(-1434602023);
            throw th;
        }
    }

    public C13890sF A09() {
        C10700ki r1 = this.A0G.A00.A0g;
        if (this.A0X.containsKey(r1)) {
            return (C13890sF) this.A0X.get(r1);
        }
        return C13890sF.A03;
    }

    public void A0A() {
        C32971md r4 = new C32971md(false, true, C35381r8.MORE_THREADS, this.A0G.A00.A0g, false, CallerContext.A08(AnonymousClass16R.class, "messages", "thread_list_load_more"));
        C31371ja r3 = this.A0F;
        ImmutableSet A042 = ((C10960l9) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BOk, this.A08)).A04();
        Preconditions.checkNotNull(A042);
        r3.A08 = A042;
        this.A0F.CHB(r4);
    }

    public void A0B(AnonymousClass1FD r7, String str) {
        if (!((AnonymousClass1GE) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BHw, this.A08)).A02()) {
            ((C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, this.A08)).A09("feature_not_enabled");
            return;
        }
        this.A0Z.set(this.A06.now());
        if ("onResume".equals(str)) {
            AnonymousClass16S r2 = this.A0A;
            if (!(true == r2.A09 && 1 == 0)) {
                r2.A09 = true;
                AnonymousClass16S.A02(r2);
                AnonymousClass16S.A01(r2);
            }
            AnonymousClass0XV r4 = AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE;
            AnonymousClass0XX r0 = this.A03;
            if (r0 == null || r0.isDone()) {
                this.A03 = this.A01.CIG("delayRefreshMontageComposer", new C35441rE(this, r7), r4, AnonymousClass07B.A00);
                return;
            }
            return;
        }
        if (this.A05 == null) {
            this.A05 = new AnonymousClass17R(this);
        }
        ((AnonymousClass1TF) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Atk, this.A08)).C6Z(this.A05);
        if (this.A0S.A00.Aer(282325381809462L, AnonymousClass0XE.A07)) {
            this.A0K = new AnonymousClass17V(this);
        }
        boolean equals = AnonymousClass1FD.EXPLICIT_USER_REFRESH.equals(r7);
        int i = AnonymousClass1Y3.Atk;
        AnonymousClass0UN r22 = this.A08;
        ((AnonymousClass1TF) AnonymousClass1XX.A02(8, i, r22)).CHB(new AnonymousClass1TO(equals, false, ((C17350yl) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BRG, r22)).A03(), false));
    }

    public void A0C(AnonymousClass1FD r10, String str, String str2) {
        C32971md A002 = r10.A00(this.A0G.A00.A0g, str2);
        C10950l8 r6 = this.A0E;
        boolean z = false;
        if (r6 != null) {
            int i = AnonymousClass1Y3.AuB;
            AnonymousClass0UN r5 = this.A08;
            if (((C26681bq) AnonymousClass1XX.A02(2, i, r5)).A09(r6, A002.A01, ((C10960l9) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BOk, r5)).A04()) != null) {
                int i2 = AnonymousClass1Y3.AuB;
                AnonymousClass0UN r62 = this.A08;
                if (((C26681bq) AnonymousClass1XX.A02(2, i2, r62)).A09(this.A0E, A002.A01, ((C10960l9) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BOk, r62)).A04()).A00 != null) {
                    int i3 = AnonymousClass1Y3.ACN;
                    if (((C35391r9) AnonymousClass1XX.A02(30, i3, this.A08)).A02() && ((C35391r9) AnonymousClass1XX.A02(30, i3, this.A08)).A00.Aem(282325383185737L)) {
                        int i4 = AnonymousClass1Y3.AgT;
                        AnonymousClass0UN r63 = this.A08;
                        AnonymousClass1TR r52 = (AnonymousClass1TR) AnonymousClass1XX.A02(29, i4, r63);
                        AnonymousClass07A.A04((AnonymousClass0VL) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ASK, r52.A00), new C29606EeE(r52, AnonymousClass1TR.A01(r52, ((C26681bq) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AuB, r63)).A09(this.A0E, A002.A01, ((C10960l9) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BOk, r63)).A04()).A00, 0), 0), -370004926);
                    }
                }
            }
        }
        A03(this, r10, str2);
        if (r10 == AnonymousClass1FD.EXPLICIT_USER_REFRESH) {
            A0E(true);
        }
        A02(this, r10, str2);
        A0B(r10, str);
        if (r10 == AnonymousClass1FD.EXPLICIT_USER_REFRESH) {
            z = true;
        }
        A0D(z);
    }

    public void A0D(boolean z) {
        C09510hU r3;
        if (((AnonymousClass1G4) AnonymousClass1XX.A02(20, AnonymousClass1Y3.BGY, this.A08)).A04.Aem(282428460500357L)) {
            if (z) {
                r3 = C09510hU.CHECK_SERVER_FOR_NEW_DATA;
            } else {
                r3 = C09510hU.STALE_DATA_OKAY;
            }
            ((AnonymousClass17S) AnonymousClass1XX.A02(21, AnonymousClass1Y3.BSW, this.A08)).CHB(new C33461nc(r3));
        }
    }

    public void A0E(boolean z) {
        Integer num;
        if (z) {
            num = AnonymousClass07B.A00;
        } else {
            num = AnonymousClass07B.A01;
        }
        this.A0C.CHB(new AnonymousClass1G5(num, "REGULAR"));
    }

    public AnonymousClass16R(AnonymousClass1XY r4, C187815b r5) {
        this.A08 = new AnonymousClass0UN(36, r4);
        this.A01 = C25091Yh.A00(r4);
        this.A07 = C30111hV.A00(r4);
        AnonymousClass16T A022 = AnonymousClass16T.A02(r4);
        C14970uR.A00(r4);
        this.A0A = new AnonymousClass16S(r4, A022);
        this.A0Q = AnonymousClass0VG.A00(AnonymousClass1Y3.BKW, r4);
        this.A0I = C32771mJ.A01(r4);
        this.A0F = C31371ja.A01(r4);
        this.A0L = AnonymousClass0UX.A0e(r4);
        this.A0P = C190016k.A00(r4);
        this.A0C = AnonymousClass17N.A00(r4).A01("MESSENGER_INBOX2");
        this.A06 = AnonymousClass067.A02();
        this.A02 = AnonymousClass0UX.A07(r4);
        this.A0Y = AnonymousClass0VG.A00(AnonymousClass1Y3.ASp, r4);
        this.A0T = C32271lU.A01(r4);
        this.A0S = C32421lj.A00(r4);
        this.A0R = AnonymousClass10R.A00(r4);
        this.A0U = C31371ja.A01(r4);
        Preconditions.checkNotNull(r5);
        this.A0G = r5;
    }
}
