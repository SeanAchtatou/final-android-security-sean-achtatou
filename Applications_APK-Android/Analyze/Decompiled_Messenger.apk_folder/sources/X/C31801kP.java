package X;

/* renamed from: X.1kP  reason: invalid class name and case insensitive filesystem */
public final class C31801kP {
    public static final int A00;

    static {
        int i;
        Integer num = AnonymousClass07B.A00;
        int A002 = (AnonymousClass10E.A00(num) << 1) & 6;
        switch (num.intValue()) {
            case 3:
            case 4:
                i = 8;
                break;
            default:
                i = 0;
                break;
        }
        A00 = 16 | 1 | A002 | i | 0 | -256;
    }

    public static Integer A00(int i) {
        int i2 = (i & 6) >>> 1;
        if (i2 == 1) {
            return AnonymousClass07B.A01;
        }
        if (i2 == 2) {
            return AnonymousClass07B.A0C;
        }
        if (i2 == 129) {
            return AnonymousClass07B.A0N;
        }
        if (i2 != 130) {
            return AnonymousClass07B.A00;
        }
        return AnonymousClass07B.A0Y;
    }
}
