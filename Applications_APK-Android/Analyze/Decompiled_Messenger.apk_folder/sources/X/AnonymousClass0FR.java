package X;

import android.hardware.Camera;
import android.hardware.camera2.CameraDevice;
import android.os.Build;
import android.os.SystemClock;
import android.util.SparseArray;

/* renamed from: X.0FR  reason: invalid class name */
public final class AnonymousClass0FR extends C007907e {
    public long A00;
    public long A01;
    public boolean A02 = true;
    public final SparseArray A03 = new SparseArray();
    public final SparseArray A04 = new SparseArray();

    public static synchronized long A00(int i, SparseArray sparseArray) {
        long j;
        synchronized (AnonymousClass0FR.class) {
            long uptimeMillis = SystemClock.uptimeMillis();
            j = 0;
            Long l = (Long) sparseArray.get(i);
            if (l != null) {
                j = uptimeMillis - l.longValue();
                sparseArray.remove(i);
            } else {
                AnonymousClass0KZ.A00("CameraMetricsCollector", "Stopped recording details for a camera that hasn't been added yet", null);
            }
        }
        return j;
    }

    public static synchronized void A01(int i, SparseArray sparseArray) {
        synchronized (AnonymousClass0FR.class) {
            long uptimeMillis = SystemClock.uptimeMillis();
            if (sparseArray.get(i) == null) {
                sparseArray.append(i, Long.valueOf(uptimeMillis));
            }
        }
    }

    public static void A02(Object obj) {
        if (obj instanceof Camera) {
            return;
        }
        if (Build.VERSION.SDK_INT < 21 || !(obj instanceof CameraDevice)) {
            throw new IllegalArgumentException("Must pass in a Camera or a CameraDevice");
        }
    }

    public AnonymousClass0FM A03() {
        return new AnonymousClass0FP();
    }

    public boolean A04(AnonymousClass0FM r14) {
        AnonymousClass0FP r142 = (AnonymousClass0FP) r14;
        synchronized (this) {
            C02740Gd.A00(r142, "Null value passed to getSnapshot!");
            if (!this.A02) {
                return false;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            long j = this.A00;
            SparseArray sparseArray = this.A03;
            int size = sparseArray.size();
            long j2 = 0;
            for (int i = 0; i < size; i++) {
                j2 += uptimeMillis - ((Long) sparseArray.valueAt(i)).longValue();
            }
            r142.cameraOpenTimeMs = j + j2;
            long j3 = this.A01;
            SparseArray sparseArray2 = this.A04;
            int size2 = sparseArray2.size();
            long j4 = 0;
            for (int i2 = 0; i2 < size2; i2++) {
                j4 += uptimeMillis - ((Long) sparseArray2.valueAt(i2)).longValue();
            }
            r142.cameraPreviewTimeMs = j3 + j4;
            return true;
        }
    }
}
