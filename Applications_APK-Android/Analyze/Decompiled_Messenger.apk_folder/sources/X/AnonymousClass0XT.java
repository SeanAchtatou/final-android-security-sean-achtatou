package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.0XT  reason: invalid class name */
public final class AnonymousClass0XT extends C06020ai {
    public final /* synthetic */ AnonymousClass0Y6 A00;

    public AnonymousClass0XT(AnonymousClass0Y6 r1) {
        this.A00 = r1;
    }

    public void A03(CancellationException cancellationException) {
        this.A00.A0O.lock();
        try {
            AnonymousClass0Y6 r1 = this.A00;
            r1.A0F = true;
            AnonymousClass0Y6.A08(r1);
            r1.A0O.unlock();
        } catch (Throwable th) {
            this.A00.A0O.unlock();
            throw th;
        }
    }
}
