package X;

import android.os.SystemClock;

/* renamed from: X.0Fm  reason: invalid class name and case insensitive filesystem */
public final class C02580Fm extends C007907e {
    public AnonymousClass0FM A03() {
        return new C02570Fl();
    }

    public boolean A04(AnonymousClass0FM r3) {
        C02570Fl r32 = (C02570Fl) r3;
        C02740Gd.A00(r32, "Null value passed to getSnapshot!");
        r32.realtimeMs = SystemClock.elapsedRealtime();
        r32.uptimeMs = SystemClock.uptimeMillis();
        return true;
    }
}
