package X;

import android.view.animation.DecelerateInterpolator;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1RF  reason: invalid class name */
public final class AnonymousClass1RF extends AnonymousClass11I implements C21371Gr {
    public C17760zQ _transition;
    @Comparable(type = 3)
    public boolean pressed;

    public void applyStateUpdate(C61322yh r6) {
        Object[] objArr = r6.A01;
        if (r6.A00 == 0) {
            C23871Rg r4 = new C23871Rg();
            r4.A00(Boolean.valueOf(this.pressed));
            r4.A00(Boolean.valueOf(((Boolean) objArr[0]).booleanValue()));
            C49302bz A01 = C17760zQ.A01(AnonymousClass07B.A00, "montage_press");
            A01.A03(C17700zK.A03);
            A01.A04 = new AnonymousClass3Z8(AnonymousClass1Y3.A1B, new DecelerateInterpolator());
            this._transition = A01;
            this.pressed = ((Boolean) r4.A00).booleanValue();
        }
    }

    public C17760zQ consumeTransition() {
        C17760zQ r1 = this._transition;
        this._transition = null;
        return r1;
    }
}
