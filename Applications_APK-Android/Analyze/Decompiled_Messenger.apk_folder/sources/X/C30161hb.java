package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.fbservice.service.OperationResult;

/* renamed from: X.1hb  reason: invalid class name and case insensitive filesystem */
public final class C30161hb implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new OperationResult(parcel);
    }

    public Object[] newArray(int i) {
        return new OperationResult[i];
    }
}
