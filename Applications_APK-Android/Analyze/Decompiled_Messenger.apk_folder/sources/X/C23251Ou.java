package X;

/* renamed from: X.1Ou  reason: invalid class name and case insensitive filesystem */
public final class C23251Ou {
    public final int A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final AnonymousClass1OH A04;
    public final AnonymousClass1OL A05;
    public final C23241Ot A06;
    public final AnonymousClass1OI A07;
    public final C23111Og A08;
    public final String A09;

    public C23251Ou(C23231Os r3) {
        this.A00 = r3.A00;
        String str = r3.A09;
        C05520Zg.A02(str);
        this.A09 = str;
        C23111Og r0 = r3.A08;
        C05520Zg.A02(r0);
        this.A08 = r0;
        this.A01 = r3.A01;
        this.A02 = r3.A02;
        this.A03 = r3.A03;
        C23241Ot r02 = r3.A06;
        C05520Zg.A02(r02);
        this.A06 = r02;
        AnonymousClass1OH r03 = r3.A04;
        if (r03 == null) {
            synchronized (C191458wG.class) {
                if (C191458wG.A00 == null) {
                    C191458wG.A00 = new C191458wG();
                }
                r03 = C191458wG.A00;
            }
        }
        this.A04 = r03;
        AnonymousClass1OL r04 = r3.A05;
        if (r04 == null) {
            synchronized (C191478wI.class) {
                if (C191478wI.A00 == null) {
                    C191478wI.A00 = new C191478wI();
                }
                r04 = C191478wI.A00;
            }
        }
        this.A05 = r04;
        AnonymousClass1OI r05 = r3.A07;
        if (r05 == null) {
            synchronized (C191448wF.class) {
                if (C191448wF.A00 == null) {
                    C191448wF.A00 = new C191448wF();
                }
                r05 = C191448wF.A00;
            }
        }
        this.A07 = r05;
    }
}
