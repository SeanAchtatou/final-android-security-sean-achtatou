package X;

import java.io.IOException;

/* renamed from: X.0IC  reason: invalid class name */
public final class AnonymousClass0IC implements AnonymousClass1YQ {
    private final AnonymousClass0Ej A00;

    public String getSimpleName() {
        return "VoltronInitHandler";
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.AWw, r1);
    }

    public static final AnonymousClass0IC A01(AnonymousClass1XY r1) {
        return new AnonymousClass0IC(r1);
    }

    private AnonymousClass0IC(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0Ei.A05(r2);
    }

    public void init() {
        int A03 = C000700l.A03(225868106);
        try {
            this.A00.A04();
        } catch (IOException e) {
            C010708t.A0T("VoltronInitHandler", e, "Init failure");
        }
        C000700l.A09(-2142986818, A03);
    }
}
