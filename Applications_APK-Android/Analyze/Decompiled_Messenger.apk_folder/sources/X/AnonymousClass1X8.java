package X;

import android.content.ContentProvider;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.facebook.contacts.data.FbContactsContentProvider;
import com.facebook.messaging.database.threads.MessagesDbContentProvider;
import com.facebook.messaging.database.threads.ThreadsDbPropertiesContentProvider;
import com.facebook.messaging.platform.MessengerPlatformProvider;
import com.facebook.messaging.sms.defaultapp.MmsFileProvider;
import com.facebook.messaging.tincan.attachments.DecryptedAttachmentProvider;
import com.facebook.platform.common.provider.PlatformProviderBase;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1X8  reason: invalid class name */
public abstract class AnonymousClass1X8 extends ContentProvider {
    private final AtomicBoolean A00 = new AtomicBoolean();

    public int A08(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        if (!(this instanceof PlatformProviderBase)) {
            if (this instanceof MmsFileProvider) {
                return 0;
            }
            if (!(this instanceof MessengerPlatformProvider)) {
                if (this instanceof DecryptedAttachmentProvider) {
                    return 0;
                }
                if (!(this instanceof ThreadsDbPropertiesContentProvider) && !(this instanceof MessagesDbContentProvider) && !(this instanceof FbContactsContentProvider) && (this instanceof AnonymousClass0TA)) {
                    throw new RuntimeException("update should not be called on this content provider");
                }
            }
        }
        throw new UnsupportedOperationException();
    }

    public int A09(Uri uri, String str, String[] strArr) {
        if (!(this instanceof PlatformProviderBase)) {
            if (this instanceof MmsFileProvider) {
                MmsFileProvider mmsFileProvider = (MmsFileProvider) this;
                if (MmsFileProvider.A00.match(uri) != 1) {
                    return 0;
                }
                return MmsFileProvider.A02(mmsFileProvider.getContext(), uri).delete() ? 1 : 0;
            } else if (!(this instanceof MessengerPlatformProvider)) {
                if (this instanceof DecryptedAttachmentProvider) {
                    return 0;
                }
                if (this instanceof ThreadsDbPropertiesContentProvider) {
                    ThreadsDbPropertiesContentProvider threadsDbPropertiesContentProvider = (ThreadsDbPropertiesContentProvider) this;
                    C005505z.A03("ThreadsDbPropertiesContentProvider.doDelete", 1751956885);
                    try {
                        return threadsDbPropertiesContentProvider.A00.A00(uri).A04(uri, str, strArr);
                    } finally {
                        C005505z.A01(537356397);
                    }
                } else if (this instanceof MessagesDbContentProvider) {
                    MessagesDbContentProvider messagesDbContentProvider = (MessagesDbContentProvider) this;
                    if (((AnonymousClass4XE) messagesDbContentProvider.A01.get()).A00.equals(uri)) {
                        ((C25771aN) messagesDbContentProvider.A03.get()).A07();
                        return 0;
                    }
                } else if (this instanceof FbContactsContentProvider) {
                    FbContactsContentProvider fbContactsContentProvider = (FbContactsContentProvider) this;
                    if (fbContactsContentProvider.A01.A02.A00.equals(uri)) {
                        fbContactsContentProvider.A02.A07();
                        return 0;
                    }
                } else if (this instanceof AnonymousClass0TA) {
                    throw new RuntimeException("delete should not be called on this content provider");
                }
            }
        }
        throw new UnsupportedOperationException();
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.res.AssetFileDescriptor A0B(android.net.Uri r12, java.lang.String r13, android.os.Bundle r14) {
        /*
            r11 = this;
            boolean r0 = r11 instanceof com.facebook.messaging.tincan.attachments.DecryptedAttachmentProvider
            if (r0 != 0) goto L_0x0014
            boolean r0 = r11 instanceof com.facebook.messaging.sms.defaultapp.MmsFileProvider
            if (r0 != 0) goto L_0x000a
            r0 = 0
            return r0
        L_0x000a:
            r1 = r11
            com.facebook.messaging.sms.defaultapp.MmsFileProvider r1 = (com.facebook.messaging.sms.defaultapp.MmsFileProvider) r1
            java.lang.String r0 = "r"
            android.content.res.AssetFileDescriptor r0 = r1.openAssetFile(r12, r0)
            return r0
        L_0x0014:
            r4 = r11
            com.facebook.messaging.tincan.attachments.DecryptedAttachmentProvider r4 = (com.facebook.messaging.tincan.attachments.DecryptedAttachmentProvider) r4
            android.content.UriMatcher r0 = X.C1060655e.A00
            int r1 = r0.match(r12)
            r0 = 1
            if (r1 == r0) goto L_0x0021
            r0 = 0
        L_0x0021:
            com.google.common.base.Preconditions.checkState(r0)
            X.1Yd r2 = r4.A05
            r0 = 283326109059412(0x101af00160954, double:1.39981697056126E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0049
            X.29r r0 = r4.A03     // Catch:{ 1wY | 1wZ | IOException | InterruptedException | ExecutionException -> 0x0067 }
            java.io.File r1 = r0.A01(r12)     // Catch:{ 1wY | 1wZ | IOException | InterruptedException | ExecutionException -> 0x0067 }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            android.os.ParcelFileDescriptor r6 = android.os.ParcelFileDescriptor.open(r1, r0)     // Catch:{ 1wY | 1wZ | IOException | InterruptedException | ExecutionException -> 0x0067 }
            android.content.res.AssetFileDescriptor r5 = new android.content.res.AssetFileDescriptor     // Catch:{ 1wY | 1wZ | IOException | InterruptedException | ExecutionException -> 0x0067 }
            r7 = 0
            long r9 = r1.length()     // Catch:{ 1wY | 1wZ | IOException | InterruptedException | ExecutionException -> 0x0067 }
            r5.<init>(r6, r7, r9)     // Catch:{ 1wY | 1wZ | IOException | InterruptedException | ExecutionException -> 0x0067 }
            return r5
        L_0x0049:
            android.os.ParcelFileDescriptor[] r3 = android.os.ParcelFileDescriptor.createPipe()     // Catch:{  }
            android.content.res.AssetFileDescriptor r5 = new android.content.res.AssetFileDescriptor     // Catch:{  }
            r0 = 0
            r6 = r3[r0]     // Catch:{  }
            r7 = 0
            r9 = -1
            r5.<init>(r6, r7, r9)     // Catch:{  }
            java.util.concurrent.ExecutorService r2 = r4.A06     // Catch:{  }
            X.ApR r1 = new X.ApR     // Catch:{  }
            r1.<init>(r4, r12, r3)     // Catch:{  }
            r0 = 164555543(0x9ceeb17, float:4.9813773E-33)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{  }
            return r5
        L_0x0067:
            r2 = move-exception
            java.lang.Class r1 = com.facebook.messaging.tincan.attachments.DecryptedAttachmentProvider.A07
            java.lang.String r0 = "Error during file download or decryption"
            X.C010708t.A08(r1, r0, r2)
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1X8.A0B(android.net.Uri, java.lang.String, android.os.Bundle):android.content.res.AssetFileDescriptor");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0083, code lost:
        if (r1.equals(r6) == false) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a0, code lost:
        if (r0 != false) goto L_0x00a2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009b A[SYNTHETIC, Splitter:B:40:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00a5 A[SYNTHETIC, Splitter:B:45:0x00a5] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.database.Cursor A0C(android.net.Uri r11, java.lang.String[] r12, java.lang.String r13, java.lang.String[] r14, java.lang.String r15) {
        /*
            r10 = this;
            boolean r0 = r10 instanceof com.facebook.platform.common.provider.PlatformProviderBase
            r5 = r11
            if (r0 != 0) goto L_0x01b8
            boolean r0 = r10 instanceof com.facebook.messaging.sms.defaultapp.MmsFileProvider
            if (r0 != 0) goto L_0x01b6
            boolean r0 = r10 instanceof com.facebook.messaging.platform.MessengerPlatformProvider
            if (r0 != 0) goto L_0x0140
            boolean r0 = r10 instanceof com.facebook.messaging.tincan.attachments.DecryptedAttachmentProvider
            if (r0 != 0) goto L_0x01b6
            boolean r0 = r10 instanceof com.facebook.messaging.database.threads.ThreadsDbPropertiesContentProvider
            r6 = r12
            r7 = r13
            r9 = r15
            r8 = r14
            if (r0 != 0) goto L_0x011c
            boolean r0 = r10 instanceof com.facebook.messaging.database.threads.MessagesDbContentProvider
            if (r0 != 0) goto L_0x00f8
            boolean r0 = r10 instanceof com.facebook.contacts.data.FbContactsContentProvider
            if (r0 != 0) goto L_0x00d4
            boolean r0 = r10 instanceof X.AnonymousClass0TA
            if (r0 != 0) goto L_0x0049
            r2 = r10
            com.facebook.abtest.qe.db.QuickExperimentContentProvider r2 = (com.facebook.abtest.qe.db.QuickExperimentContentProvider) r2
            java.lang.String r1 = "QuickExperimentContentProvider.doQuery"
            r0 = 124693693(0x76eacbd, float:1.7955893E-34)
            X.C005505z.A03(r1, r0)
            X.0TF r0 = r2.A02     // Catch:{ all -> 0x0041 }
            X.0j5 r4 = r0.A00(r11)     // Catch:{ all -> 0x0041 }
            android.database.Cursor r1 = r4.A05(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0041 }
            r0 = -773353689(0xffffffffd1e78f27, float:-1.24317393E11)
            X.C005505z.A01(r0)
            return r1
        L_0x0041:
            r1 = move-exception
            r0 = 2057196078(0x7a9e522e, float:4.1102485E35)
            X.C005505z.A01(r0)
            throw r1
        L_0x0049:
            r5 = r10
            X.0TA r5 = (X.AnonymousClass0TA) r5
            android.database.MatrixCursor r4 = new android.database.MatrixCursor
            java.lang.String r3 = "li"
            java.lang.String r2 = "push_receiver"
            java.lang.String r1 = "oli"
            java.lang.String r0 = "switch_account_available"
            java.lang.String[] r0 = new java.lang.String[]{r3, r2, r1, r0}
            r4.<init>(r0)
            android.content.Context r0 = r5.getContext()
            X.C07620dr.A00(r0)
            X.0Tq r0 = r5.A02
            java.lang.Object r6 = r0.get()
            java.lang.String r6 = (java.lang.String) r6
            X.0jJ r0 = r5.A01     // Catch:{ IOException -> 0x00aa }
            com.fasterxml.jackson.databind.JsonNode r1 = r0.readTree(r13)     // Catch:{ IOException -> 0x00aa }
            java.lang.String r0 = "userId"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)     // Catch:{ IOException -> 0x00aa }
            java.lang.String r1 = r0.textValue()     // Catch:{ IOException -> 0x00aa }
            if (r1 == 0) goto L_0x0085
            boolean r0 = r1.equals(r6)     // Catch:{ IOException -> 0x00aa }
            r3 = 1
            if (r0 != 0) goto L_0x0086
        L_0x0085:
            r3 = 0
        L_0x0086:
            if (r1 == 0) goto L_0x0098
            X.2c5 r0 = r5.A00     // Catch:{ IOException -> 0x0096 }
            java.lang.String r0 = r0.A01()     // Catch:{ IOException -> 0x0096 }
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0096 }
            r2 = 1
            if (r0 != 0) goto L_0x0099
            goto L_0x0098
        L_0x0096:
            r1 = 0
            goto L_0x00ac
        L_0x0098:
            r2 = 0
        L_0x0099:
            if (r6 == 0) goto L_0x00a2
            boolean r0 = r6.equals(r1)     // Catch:{ IOException -> 0x00ae }
            r1 = 1
            if (r0 == 0) goto L_0x00a3
        L_0x00a2:
            r1 = 0
        L_0x00a3:
            if (r1 == 0) goto L_0x00af
            boolean r0 = r5.A0J()     // Catch:{ IOException -> 0x00af }
            goto L_0x00b0
        L_0x00aa:
            r1 = 0
            r3 = 0
        L_0x00ac:
            r2 = 0
            goto L_0x00af
        L_0x00ae:
            r1 = 0
        L_0x00af:
            r0 = 0
        L_0x00b0:
            if (r3 == 0) goto L_0x00d1
            java.lang.Integer r3 = X.C49452cF.A01
        L_0x00b4:
            if (r2 == 0) goto L_0x00ce
            java.lang.Integer r2 = X.C49452cF.A01
        L_0x00b8:
            if (r1 == 0) goto L_0x00cb
            java.lang.Integer r1 = X.C49452cF.A01
        L_0x00bc:
            if (r0 == 0) goto L_0x00c8
            java.lang.Integer r0 = X.C49452cF.A01
        L_0x00c0:
            java.lang.Object[] r0 = new java.lang.Object[]{r3, r2, r1, r0}
            r4.addRow(r0)
            return r4
        L_0x00c8:
            java.lang.Integer r0 = X.C49452cF.A00
            goto L_0x00c0
        L_0x00cb:
            java.lang.Integer r1 = X.C49452cF.A00
            goto L_0x00bc
        L_0x00ce:
            java.lang.Integer r2 = X.C49452cF.A00
            goto L_0x00b8
        L_0x00d1:
            java.lang.Integer r3 = X.C49452cF.A00
            goto L_0x00b4
        L_0x00d4:
            r2 = r10
            com.facebook.contacts.data.FbContactsContentProvider r2 = (com.facebook.contacts.data.FbContactsContentProvider) r2
            java.lang.String r1 = "FbContactsContentProvider.doQuery"
            r0 = -1254528280(0xffffffffb5396ae8, float:-6.907344E-7)
            X.C005505z.A03(r1, r0)
            X.0TF r0 = r2.A03     // Catch:{ all -> 0x00f0 }
            X.0j5 r4 = r0.A00(r11)     // Catch:{ all -> 0x00f0 }
            android.database.Cursor r1 = r4.A05(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x00f0 }
            r0 = 267054265(0xfeaecb9, float:2.3165364E-29)
            X.C005505z.A01(r0)
            return r1
        L_0x00f0:
            r1 = move-exception
            r0 = -571451995(0xffffffffddf055a5, float:-2.16474117E18)
            X.C005505z.A01(r0)
            throw r1
        L_0x00f8:
            r2 = r10
            com.facebook.messaging.database.threads.MessagesDbContentProvider r2 = (com.facebook.messaging.database.threads.MessagesDbContentProvider) r2
            java.lang.String r1 = "MessagesDbContentProvider.doQuery"
            r0 = -1693251778(0xffffffff9b13073e, float:-1.2161895E-22)
            X.C005505z.A03(r1, r0)
            X.0TF r0 = r2.A00     // Catch:{ all -> 0x0114 }
            X.0j5 r4 = r0.A00(r11)     // Catch:{ all -> 0x0114 }
            android.database.Cursor r1 = r4.A05(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0114 }
            r0 = -1885984025(0xffffffff8f962ae7, float:-1.4807667E-29)
            X.C005505z.A01(r0)
            return r1
        L_0x0114:
            r1 = move-exception
            r0 = 641146428(0x26371e3c, float:6.3531854E-16)
            X.C005505z.A01(r0)
            throw r1
        L_0x011c:
            r2 = r10
            com.facebook.messaging.database.threads.ThreadsDbPropertiesContentProvider r2 = (com.facebook.messaging.database.threads.ThreadsDbPropertiesContentProvider) r2
            java.lang.String r1 = "ThreadsDbPropertiesContentProvider.doQuery"
            r0 = -1419451171(0xffffffffab64e4dd, float:-8.1319483E-13)
            X.C005505z.A03(r1, r0)
            X.0TF r0 = r2.A00     // Catch:{ all -> 0x0138 }
            X.0j5 r4 = r0.A00(r11)     // Catch:{ all -> 0x0138 }
            android.database.Cursor r1 = r4.A05(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0138 }
            r0 = -745958936(0xffffffffd38991e8, float:-1.18171684E12)
            X.C005505z.A01(r0)
            return r1
        L_0x0138:
            r1 = move-exception
            r0 = -1665799995(0xffffffff9cb5e8c5, float:-1.2037745E-21)
            X.C005505z.A01(r0)
            throw r1
        L_0x0140:
            r3 = r10
            com.facebook.messaging.platform.MessengerPlatformProvider r3 = (com.facebook.messaging.platform.MessengerPlatformProvider) r3
            monitor-enter(r3)
            android.content.UriMatcher r0 = com.facebook.messaging.platform.MessengerPlatformProvider.A01     // Catch:{ all -> 0x01b3 }
            int r1 = r0.match(r11)     // Catch:{ all -> 0x01b3 }
            r0 = 1
            if (r1 != r0) goto L_0x019c
            android.database.MatrixCursor r6 = new android.database.MatrixCursor     // Catch:{ all -> 0x01b3 }
            java.lang.String r0 = "version"
            java.lang.String[] r0 = new java.lang.String[]{r0}     // Catch:{ all -> 0x01b3 }
            r6.<init>(r0)     // Catch:{ all -> 0x01b3 }
            X.26S r5 = r3.A00     // Catch:{ all -> 0x01b3 }
            X.0dQ r4 = com.google.common.collect.ImmutableSet.A01()     // Catch:{ all -> 0x01b3 }
            com.google.common.collect.ImmutableSet r0 = X.AnonymousClass26S.A01     // Catch:{ all -> 0x01b3 }
            X.1Xv r2 = r0.iterator()     // Catch:{ all -> 0x01b3 }
        L_0x0164:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x01b3 }
            if (r0 == 0) goto L_0x017e
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x01b3 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ all -> 0x01b3 }
            int r0 = r1.intValue()     // Catch:{ all -> 0x01b3 }
            boolean r0 = r5.A01(r0)     // Catch:{ all -> 0x01b3 }
            if (r0 == 0) goto L_0x0164
            r4.A01(r1)     // Catch:{ all -> 0x01b3 }
            goto L_0x0164
        L_0x017e:
            com.google.common.collect.ImmutableSet r0 = r4.build()     // Catch:{ all -> 0x01b3 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x01b3 }
        L_0x0186:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x01b3 }
            if (r0 == 0) goto L_0x019a
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x01b3 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x01b3 }
            java.lang.Integer[] r0 = new java.lang.Integer[]{r0}     // Catch:{ all -> 0x01b3 }
            r6.addRow(r0)     // Catch:{ all -> 0x01b3 }
            goto L_0x0186
        L_0x019a:
            monitor-exit(r3)
            return r6
        L_0x019c:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x01b3 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b3 }
            r1.<init>()     // Catch:{ all -> 0x01b3 }
            java.lang.String r0 = "Unknown URI "
            r1.append(r0)     // Catch:{ all -> 0x01b3 }
            r1.append(r11)     // Catch:{ all -> 0x01b3 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x01b3 }
            r2.<init>(r0)     // Catch:{ all -> 0x01b3 }
            throw r2     // Catch:{ all -> 0x01b3 }
        L_0x01b3:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x01b6:
            r0 = 0
            return r0
        L_0x01b8:
            r3 = r10
            com.facebook.platform.common.provider.PlatformProviderBase r3 = (com.facebook.platform.common.provider.PlatformProviderBase) r3
            monitor-enter(r3)
            android.content.UriMatcher r0 = com.facebook.platform.common.provider.PlatformProviderBase.A01     // Catch:{ all -> 0x0203 }
            int r1 = r0.match(r11)     // Catch:{ all -> 0x0203 }
            r0 = 1
            if (r1 != r0) goto L_0x01ec
            android.database.MatrixCursor r2 = new android.database.MatrixCursor     // Catch:{ all -> 0x0203 }
            java.lang.String r0 = "version"
            java.lang.String[] r0 = new java.lang.String[]{r0}     // Catch:{ all -> 0x0203 }
            r2.<init>(r0)     // Catch:{ all -> 0x0203 }
            java.util.List r0 = X.C189158qT.A00     // Catch:{ all -> 0x0203 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0203 }
        L_0x01d6:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0203 }
            if (r0 == 0) goto L_0x01ea
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0203 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x0203 }
            java.lang.Integer[] r0 = new java.lang.Integer[]{r0}     // Catch:{ all -> 0x0203 }
            r2.addRow(r0)     // Catch:{ all -> 0x0203 }
            goto L_0x01d6
        L_0x01ea:
            monitor-exit(r3)
            return r2
        L_0x01ec:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0203 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0203 }
            r1.<init>()     // Catch:{ all -> 0x0203 }
            java.lang.String r0 = "Unknown URI "
            r1.append(r0)     // Catch:{ all -> 0x0203 }
            r1.append(r11)     // Catch:{ all -> 0x0203 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0203 }
            r2.<init>(r0)     // Catch:{ all -> 0x0203 }
            throw r2     // Catch:{ all -> 0x0203 }
        L_0x0203:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1X8.A0C(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String):android.database.Cursor");
    }

    public Uri A0D(Uri uri, ContentValues contentValues) {
        if (!(this instanceof PlatformProviderBase)) {
            if (this instanceof MmsFileProvider) {
                return null;
            }
            if (!(this instanceof MessengerPlatformProvider)) {
                if (this instanceof DecryptedAttachmentProvider) {
                    return null;
                }
                if (this instanceof ThreadsDbPropertiesContentProvider) {
                    ThreadsDbPropertiesContentProvider threadsDbPropertiesContentProvider = (ThreadsDbPropertiesContentProvider) this;
                    C005505z.A03("ThreadsDbPropertiesContentProvider.doInsert", -1195814234);
                    try {
                        return threadsDbPropertiesContentProvider.A00.A00(uri).A06(uri, contentValues);
                    } finally {
                        C005505z.A01(-1258340578);
                    }
                } else if (!(this instanceof MessagesDbContentProvider) && !(this instanceof FbContactsContentProvider) && (this instanceof AnonymousClass0TA)) {
                    throw new RuntimeException("insert should not be called on this content provider");
                }
            }
        }
        throw new UnsupportedOperationException();
    }

    public String A0F(Uri uri) {
        if (!(this instanceof PlatformProviderBase)) {
            if (this instanceof MmsFileProvider) {
                return null;
            }
            if (!(this instanceof MessengerPlatformProvider)) {
                if (this instanceof DecryptedAttachmentProvider) {
                    return null;
                }
                if (!(this instanceof ThreadsDbPropertiesContentProvider) && !(this instanceof MessagesDbContentProvider) && !(this instanceof FbContactsContentProvider) && (this instanceof AnonymousClass0TA)) {
                    return AnonymousClass08S.A0J("vnd.android.cursor.dir/", ((AnonymousClass0TA) this).A0I());
                }
            }
        }
        throw new UnsupportedOperationException();
    }

    public void A0G() {
    }

    public boolean A0H() {
        if (!(this instanceof AnonymousClass0TB)) {
            return this instanceof AnonymousClass0TE;
        }
        Context context = ((AnonymousClass0TB) this).getContext();
        try {
            return C28061eC.A04(context, context.getApplicationInfo().uid, Binder.getCallingUid());
        } catch (SecurityException unused) {
            return false;
        }
    }

    private static void A03() {
        AnonymousClass00C.A00(512, 73725768);
    }

    private final void A04() {
        synchronized (this.A00) {
            if (!this.A00.get()) {
                A0G();
                this.A00.set(true);
            }
        }
    }

    private void A07(String str) {
        if (AnonymousClass08Z.A05(512)) {
            AnonymousClass00C.A01(512, AnonymousClass08S.A0P(getClass().getSimpleName(), ".", str), 1365513260);
        }
    }

    public AssetFileDescriptor A0A(Uri uri, String str) {
        if (!(this instanceof MmsFileProvider)) {
            return super.openAssetFile(uri, str);
        }
        ParcelFileDescriptor openFile = ((MmsFileProvider) this).openFile(uri, str);
        if (openFile != null) {
            return new AssetFileDescriptor(openFile, 0, -1);
        }
        return null;
    }

    public ParcelFileDescriptor A0E(Uri uri, String str) {
        if (!(this instanceof MmsFileProvider)) {
            return super.openFile(uri, str);
        }
        MmsFileProvider mmsFileProvider = (MmsFileProvider) this;
        if (MmsFileProvider.A00.match(uri) == 1) {
            File A02 = MmsFileProvider.A02(mmsFileProvider.getContext(), uri);
            int i = 738197504;
            if (TextUtils.equals(str, "r")) {
                i = 268435456;
            }
            return ParcelFileDescriptor.open(A02, i);
        }
        throw new FileNotFoundException();
    }

    public final ContentProviderResult[] applyBatch(ArrayList arrayList) {
        A07("applyBatch");
        try {
            A05();
            return super.applyBatch(arrayList);
        } finally {
            A03();
        }
    }

    public final int bulkInsert(Uri uri, ContentValues[] contentValuesArr) {
        A07("bulkInsert");
        try {
            A05();
            return super.bulkInsert(uri, contentValuesArr);
        } finally {
            A03();
        }
    }

    public final Bundle call(String str, String str2, Bundle bundle) {
        A07("call");
        try {
            A05();
            return null;
        } finally {
            A03();
        }
    }

    public final int delete(Uri uri, String str, String[] strArr) {
        A07("delete");
        try {
            A05();
            return A09(uri, str, strArr);
        } finally {
            A03();
        }
    }

    public final String[] getStreamTypes(Uri uri, String str) {
        A07("getStreamTypes");
        try {
            A06();
            return null;
        } finally {
            A03();
        }
    }

    public final String getType(Uri uri) {
        A07("getType");
        try {
            A06();
            return A0F(uri);
        } finally {
            A03();
        }
    }

    public final Uri insert(Uri uri, ContentValues contentValues) {
        A07("insert");
        try {
            A05();
            return A0D(uri, contentValues);
        } finally {
            A03();
        }
    }

    public final boolean isTemporary() {
        A07("isTemporary");
        try {
            A06();
            return super.isTemporary();
        } finally {
            A03();
        }
    }

    public final void onConfigurationChanged(Configuration configuration) {
        A07("onConfigurationChanged");
        try {
            if (this.A00.get()) {
                super.onConfigurationChanged(configuration);
            }
        } finally {
            A03();
        }
    }

    public final boolean onCreate() {
        A07("onCreate");
        A03();
        return true;
    }

    public final void onLowMemory() {
        A07("onLowMemory");
        try {
            if (this.A00.get()) {
                super.onLowMemory();
            }
        } finally {
            A03();
        }
    }

    public final void onTrimMemory(int i) {
        A07("onTrimMemory");
        try {
            if (this.A00.get()) {
                super.onTrimMemory(i);
            }
        } finally {
            A03();
        }
    }

    public final AssetFileDescriptor openAssetFile(Uri uri, String str) {
        A07("openAssetFile");
        try {
            if (str.contains("w")) {
                A05();
            } else {
                A06();
            }
            return A0A(uri, str);
        } finally {
            A03();
        }
    }

    public final ParcelFileDescriptor openFile(Uri uri, String str) {
        A07("openFile");
        try {
            if (str.contains("w")) {
                A05();
            } else {
                A06();
            }
            return A0E(uri, str);
        } finally {
            A03();
        }
    }

    public final AssetFileDescriptor openTypedAssetFile(Uri uri, String str, Bundle bundle) {
        A07("openTypedAssetFile");
        try {
            A06();
            return A0B(uri, str, bundle);
        } finally {
            A03();
        }
    }

    public final void shutdown() {
        A07("shutdown");
        try {
            this.A00.get();
        } finally {
            A03();
        }
    }

    public final int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        A07("update");
        try {
            A05();
            return A08(uri, contentValues, str, strArr);
        } finally {
            A03();
        }
    }

    private final void A05() {
        A04();
        if (!A0H()) {
            throw new SecurityException("Component access not allowed.");
        }
    }

    private final void A06() {
        A04();
        if (!A0H()) {
            throw new SecurityException("Component access not allowed.");
        }
    }

    public final Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        A07("query");
        try {
            A06();
            return A0C(uri, strArr, str, strArr2, str2);
        } finally {
            A03();
        }
    }

    public final Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2, CancellationSignal cancellationSignal) {
        A07("query");
        try {
            A06();
            return A0C(uri, strArr, str, strArr2, str2);
        } finally {
            A03();
        }
    }
}
