package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.common.connectionstatus.FbDataConnectionManager;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1he  reason: invalid class name and case insensitive filesystem */
public final class C30191he implements AnonymousClass06U {
    public final /* synthetic */ FbDataConnectionManager A00;

    public C30191he(FbDataConnectionManager fbDataConnectionManager) {
        this.A00 = fbDataConnectionManager;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r10) {
        int A002 = AnonymousClass09Y.A00(1956096189);
        FbDataConnectionManager fbDataConnectionManager = this.A00;
        fbDataConnectionManager.A05 = null;
        ((ScheduledExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ADt, fbDataConnectionManager.A00)).schedule(new C104994zw(fbDataConnectionManager, FbDataConnectionManager.A02(fbDataConnectionManager)), (long) StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS, TimeUnit.MILLISECONDS);
        AnonymousClass09Y.A01(1641797987, A002);
    }
}
