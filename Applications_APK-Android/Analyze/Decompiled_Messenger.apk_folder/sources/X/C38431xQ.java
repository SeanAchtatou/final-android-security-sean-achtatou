package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.ImmutableList;

@UserScoped
/* renamed from: X.1xQ  reason: invalid class name and case insensitive filesystem */
public final class C38431xQ extends AnonymousClass1BV {
    private static C05540Zi A02;
    public static final InterstitialTrigger A03 = new InterstitialTrigger(InterstitialTrigger.Action.A3z);
    public final AnonymousClass3KE A00;
    public final FbSharedPreferences A01;

    public String Aqc() {
        return "5665";
    }

    public static final C38431xQ A00(AnonymousClass1XY r4) {
        C38431xQ r0;
        synchronized (C38431xQ.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new C38431xQ((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (C38431xQ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public C70753bE B3h(InterstitialTrigger interstitialTrigger) {
        boolean z = true;
        if (!this.A00.A09(null) || this.A01.AqN(C68413Ss.A07, 0) >= 1 || !this.A01.Aep(C68413Ss.A01(null.A0S), false)) {
            z = false;
        }
        if (z) {
            return C70753bE.ELIGIBLE;
        }
        return C70753bE.INELIGIBLE;
    }

    public ImmutableList B7C() {
        return ImmutableList.of(A03);
    }

    private C38431xQ(AnonymousClass1XY r2) {
        this.A01 = FbSharedPreferencesModule.A00(r2);
        this.A00 = AnonymousClass3KE.A00(r2);
    }
}
