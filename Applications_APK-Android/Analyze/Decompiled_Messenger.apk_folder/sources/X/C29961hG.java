package X;

import android.app.Application;
import android.content.Context;
import com.facebook.common.util.TriState;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.1hG  reason: invalid class name and case insensitive filesystem */
public final class C29961hG {
    private static C29961hG A05;
    public TriState A00 = TriState.UNSET;
    public EDr A01;
    public List A02 = new ArrayList();
    public ScheduledExecutorService A03;
    public final Application A04;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (r3.A00 == com.facebook.common.util.TriState.NO) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A01() {
        /*
            r3 = this;
            monitor-enter(r3)
            android.app.ActivityManager$RunningAppProcessInfo r0 = new android.app.ActivityManager$RunningAppProcessInfo     // Catch:{ all -> 0x0019 }
            r0.<init>()     // Catch:{ all -> 0x0019 }
            android.app.ActivityManager.getMyMemoryState(r0)     // Catch:{ all -> 0x0019 }
            int r1 = r0.importance     // Catch:{ all -> 0x0019 }
            r0 = 100
            if (r1 != r0) goto L_0x0016
            com.facebook.common.util.TriState r2 = r3.A00     // Catch:{ all -> 0x0019 }
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.NO     // Catch:{ all -> 0x0019 }
            r0 = 1
            if (r2 != r1) goto L_0x0017
        L_0x0016:
            r0 = 0
        L_0x0017:
            monitor-exit(r3)
            return r0
        L_0x0019:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29961hG.A01():boolean");
    }

    public static C29961hG A00(Context context) {
        if (A05 == null) {
            A05 = new C29961hG(context);
        }
        return A05;
    }

    private C29961hG(Context context) {
        this.A04 = (Application) context.getApplicationContext();
    }
}
