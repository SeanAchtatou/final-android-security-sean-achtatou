package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.0cb  reason: invalid class name and case insensitive filesystem */
public final class C07080cb extends C07090cc {
    private static C05540Zi A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C07080cb(X.C12730ps r6, android.content.ContentResolver r7) {
        /*
            r5 = this;
            X.0Tq r0 = r6.A01
            java.lang.Object r4 = r0.get()
            com.facebook.auth.viewercontext.ViewerContext r4 = (com.facebook.auth.viewercontext.ViewerContext) r4
            java.lang.String r3 = "content://"
            android.content.Context r0 = r6.A00
            java.lang.String r2 = r0.getPackageName()
            java.lang.String r1 = "."
            java.lang.String r0 = "threads_properties"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r3, r0)
            android.net.Uri r1 = android.net.Uri.parse(r0)
            java.lang.String r0 = "properties"
            android.net.Uri r3 = android.net.Uri.withAppendedPath(r1, r0)
            android.net.Uri$Builder r1 = new android.net.Uri$Builder
            r1.<init>()
            java.lang.String r0 = r3.getScheme()
            android.net.Uri$Builder r1 = r1.scheme(r0)
            java.lang.String r0 = r3.getAuthority()
            android.net.Uri$Builder r1 = r1.authority(r0)
            java.lang.String r0 = r3.getPath()
            android.net.Uri$Builder r2 = r1.path(r0)
            java.lang.String r1 = r4.mUserId
            java.lang.String r0 = "vc"
            android.net.Uri$Builder r2 = r2.appendQueryParameter(r0, r1)
            boolean r0 = r4.mIsPageContext
            java.lang.String r1 = java.lang.Boolean.toString(r0)
            java.lang.String r0 = "isPage"
            android.net.Uri$Builder r1 = r2.appendQueryParameter(r0, r1)
            java.lang.String r0 = r3.getFragment()
            android.net.Uri$Builder r0 = r1.fragment(r0)
            android.net.Uri r0 = r0.build()
            r5.<init>(r7, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07080cb.<init>(X.0ps, android.content.ContentResolver):void");
    }

    public static final C07080cb A00(AnonymousClass1XY r5) {
        C07080cb r0;
        synchronized (C07080cb.class) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C07080cb(C12730ps.A00(r02), C04490Ux.A0E(r02));
                }
                C05540Zi r1 = A00;
                r0 = (C07080cb) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }
}
