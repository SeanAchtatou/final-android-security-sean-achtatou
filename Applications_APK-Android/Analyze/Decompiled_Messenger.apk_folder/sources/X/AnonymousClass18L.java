package X;

import com.facebook.common.dextricks.DexStore;
import com.google.common.base.Preconditions;

/* renamed from: X.18L  reason: invalid class name */
public final class AnonymousClass18L extends AnonymousClass18I {
    public final char[] A00 = new char[512];

    public AnonymousClass18L(C30481i7 r6) {
        super(r6, null);
        Preconditions.checkArgument(r6.A05.length == 16);
        for (int i = 0; i < 256; i++) {
            char[] cArr = this.A00;
            char[] cArr2 = r6.A05;
            cArr[i] = cArr2[i >>> 4];
            cArr[i | DexStore.LOAD_RESULT_OATMEAL_QUICKENED] = cArr2[i & 15];
        }
    }
}
