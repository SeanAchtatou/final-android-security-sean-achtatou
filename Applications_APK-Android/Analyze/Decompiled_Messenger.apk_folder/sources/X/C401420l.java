package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.20l  reason: invalid class name and case insensitive filesystem */
public final class C401420l {
    private final C12480pR A00;

    public static final C401420l A00(AnonymousClass1XY r1) {
        return new C401420l(r1);
    }

    public void A01(Context context) {
        Intent intent = new Intent(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1R));
        intent.addCategory(AnonymousClass80H.$const$string(16));
        intent.setFlags(268435456);
        this.A00.A08.A0B(intent, context);
    }

    private C401420l(AnonymousClass1XY r2) {
        this.A00 = C12480pR.A00(r2);
    }
}
