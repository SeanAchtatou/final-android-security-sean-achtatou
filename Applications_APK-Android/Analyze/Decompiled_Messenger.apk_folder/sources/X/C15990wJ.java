package X;

import android.view.Window;

/* renamed from: X.0wJ  reason: invalid class name and case insensitive filesystem */
public final class C15990wJ {
    public static void A00(Window window, int i) {
        if (!C012709o.A00(23)) {
            i = C15320v6.MEASURED_STATE_MASK;
        }
        C184913s.A06(window, i);
    }

    public static void A01(Window window, int i, int i2) {
        C16180wc r2 = new C16180wc();
        r2.A03 = true;
        r2.A08 = true;
        r2.A05 = !C012609n.A02(i);
        r2.A04 = true ^ C012609n.A02(i2);
        C184913s.A04(window, r2.A00());
        A00(window, i);
        if (C012709o.A00(26)) {
            C37161un.A00(window, i2);
        }
    }
}
