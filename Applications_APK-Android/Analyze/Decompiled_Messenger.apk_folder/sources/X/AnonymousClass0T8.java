package X;

import android.net.Uri;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0T8  reason: invalid class name */
public final class AnonymousClass0T8 implements AnonymousClass0T9 {
    public final HashMap A00 = new HashMap();
    private final String A01;

    public AnonymousClass0T8(String str) {
        this.A01 = str;
    }

    public File Amn(Uri uri) {
        String encodedPath = uri.getEncodedPath();
        int indexOf = encodedPath.indexOf(47, 1);
        String decode = Uri.decode(encodedPath.substring(1, indexOf));
        String decode2 = Uri.decode(encodedPath.substring(indexOf + 1));
        File file = (File) this.A00.get(decode);
        if (file != null) {
            File file2 = new File(file, decode2);
            try {
                File canonicalFile = file2.getCanonicalFile();
                if (canonicalFile.getPath().startsWith(file.getPath())) {
                    return canonicalFile;
                }
                throw new SecurityException("Resolved path jumped beyond configured root");
            } catch (IOException unused) {
                throw new IllegalArgumentException("Failed to resolve canonical path for " + file2);
            }
        } else {
            throw new IllegalArgumentException("Unable to find configured root for " + uri);
        }
    }

    public Uri B7q(File file) {
        String substring;
        try {
            String canonicalPath = file.getCanonicalPath();
            Map.Entry entry = null;
            for (Map.Entry entry2 : this.A00.entrySet()) {
                String path = ((File) entry2.getValue()).getPath();
                if (canonicalPath.startsWith(path) && (entry == null || path.length() > ((File) entry.getValue()).getPath().length())) {
                    entry = entry2;
                }
            }
            if (entry != null) {
                String path2 = ((File) entry.getValue()).getPath();
                boolean endsWith = path2.endsWith("/");
                int length = path2.length();
                if (endsWith) {
                    substring = canonicalPath.substring(length);
                } else {
                    substring = canonicalPath.substring(length + 1);
                }
                return new Uri.Builder().scheme("content").authority(this.A01).encodedPath(AnonymousClass08S.A07(Uri.encode((String) entry.getKey()), '/', Uri.encode(substring, "/"))).build();
            }
            throw new IllegalArgumentException(AnonymousClass08S.A0J("Failed to find configured root that contains ", canonicalPath));
        } catch (IOException unused) {
            throw new IllegalArgumentException("Failed to resolve canonical path for " + file);
        }
    }
}
