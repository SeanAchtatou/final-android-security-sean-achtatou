package X;

/* renamed from: X.19X  reason: invalid class name */
public final class AnonymousClass19X {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("timestamp_ms", "INTEGER");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6("id", "TEXT");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("key", "BLOB");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("master_key_version", "INTEGER");
}
