package X;

import android.app.Activity;
import android.content.IntentFilter;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;

/* renamed from: X.0yf  reason: invalid class name and case insensitive filesystem */
public final class C17290yf implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.auth.activity.AuthenticatedActivityHelper$1";
    public final /* synthetic */ C27871dt A00;
    public final /* synthetic */ Activity A01;

    public C17290yf(C27871dt r1, Activity activity) {
        this.A00 = r1;
        this.A01 = activity;
    }

    public void run() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AnonymousClass80H.$const$string(18));
        this.A00.A04 = new C17300yg(this, this.A01, intentFilter);
        this.A00.A04.A00();
        C27871dt r3 = this.A00;
        C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Aym, r3.A06)).BMm();
        BMm.A02("ACTION_MQTT_NO_AUTH", new C17330yj(this));
        r3.A02 = BMm.A00();
        this.A00.A02.A00();
        C27871dt r32 = this.A00;
        C06600bl BMm2 = ((C04460Ut) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Aym, r32.A06)).BMm();
        BMm2.A02(BlueServiceOperationFactory.BLUESERVICE_NO_AUTH, new C17360ym(this));
        r32.A00 = BMm2.A00();
        this.A00.A00.A00();
        C27871dt r33 = this.A00;
        C06600bl BMm3 = ((C04460Ut) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Aym, r33.A06)).BMm();
        BMm3.A02("TIGON_RESPONSE_NO_AUTH", new AnonymousClass14Z(this));
        r33.A03 = BMm3.A00();
        this.A00.A03.A00();
        C27871dt r34 = this.A00;
        C06600bl BMm4 = ((C04460Ut) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Aym, r34.A06)).BMm();
        BMm4.A02("com.facebook.auth.activity.DISMiSS_REAUTH_DIALOG", new C185314a(this));
        r34.A01 = BMm4.A00();
        this.A00.A01.A00();
    }
}
