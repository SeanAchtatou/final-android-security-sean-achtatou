package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.collect.ImmutableSet;

/* renamed from: X.1y2  reason: invalid class name and case insensitive filesystem */
public final class C38811y2 implements AnonymousClass0lE {
    public static final AnonymousClass1Y8 A01 = C04350Ue.A0B.A09("search_cache_db_initial_index_complete");
    public AnonymousClass0UN A00;

    public static final C38811y2 A00(AnonymousClass1XY r1) {
        return new C38811y2(r1);
    }

    public void A01(boolean z) {
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A00)).edit();
        edit.putBoolean(A01, z);
        edit.commit();
    }

    public ImmutableSet Avs() {
        return ImmutableSet.A04(A01);
    }

    public C38811y2(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
