package X;

import com.facebook.messaging.sync.common.MessagesSyncQueue;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0lg  reason: invalid class name */
public final class AnonymousClass0lg implements AnonymousClass0lf {
    private static volatile AnonymousClass0lg A01;
    private final C04310Tq A00;

    public void BhF(Class cls, C11010lb r2, ImmutableList immutableList, long j, long j2, long j3) {
    }

    public void Bki(Class cls) {
    }

    public static final AnonymousClass0lg A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0lg.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0lg(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void BhH(Class cls, C11010lb r4) {
        if (cls == MessagesSyncQueue.class) {
            ((C32271lU) this.A00.get()).A03(r4.A01);
        }
    }

    private AnonymousClass0lg(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0VG.A00(AnonymousClass1Y3.ALj, r2);
    }
}
