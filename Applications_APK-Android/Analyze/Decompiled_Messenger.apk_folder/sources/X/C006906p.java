package X;

/* renamed from: X.06p  reason: invalid class name and case insensitive filesystem */
public final class C006906p implements AnonymousClass03a {
    private AnonymousClass0QR A00;
    public final /* synthetic */ AnonymousClass09Z A01;

    public synchronized void Bse() {
        if (AnonymousClass08Z.A05(4096)) {
            Bsf();
            AnonymousClass0QR A02 = this.A01.A02();
            this.A00 = A02;
            synchronized (A02) {
                if (!A02.A00 && AnonymousClass08Z.A05(4096)) {
                    A02.A01 = false;
                    A02.A02();
                    A02.A00 = true;
                }
            }
        }
    }

    public synchronized void Bsf() {
        AnonymousClass0QR r3 = this.A00;
        if (r3 != null) {
            synchronized (r3) {
                if (r3.A00) {
                    if (r3.A01) {
                        AnonymousClass00C.A00(4096, -274685169);
                    }
                    r3.A01();
                    r3.A00 = false;
                }
            }
            this.A00 = null;
        }
    }

    public C006906p(AnonymousClass09Z r1) {
        this.A01 = r1;
    }
}
