package X;

import com.facebook.acra.ACRA;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.acra.util.StatFsUtil;
import com.facebook.common.dextricks.DexStore;
import com.facebook.common.dextricks.OdexSchemeArtXdex;
import com.facebook.forker.Process;
import com.facebook.proxygen.LigerSamplePolicy;
import io.card.payment.BuildConfig;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0Zc  reason: invalid class name and case insensitive filesystem */
public final class C05480Zc {
    public static double A00(long j) {
        switch (AnonymousClass0XG.A01(j)) {
            case 0:
            case 48:
            case AnonymousClass1Y3.A0N /*52*/:
            case AnonymousClass1Y3.A0Q /*58*/:
            case AnonymousClass1Y3.A0i /*101*/:
            case AnonymousClass1Y3.A0r /*117*/:
            case 129:
            case AnonymousClass1Y3.A12 /*136*/:
            case AnonymousClass1Y3.A18 /*147*/:
            case AnonymousClass1Y3.A1Q /*180*/:
            case 202:
            case 205:
            case AnonymousClass1Y3.A1n /*229*/:
            case 230:
            case AnonymousClass1Y3.A2B /*279*/:
            case AnonymousClass1Y3.A2D /*282*/:
            case 284:
            case AnonymousClass1Y3.A2E /*285*/:
            case 286:
            case AnonymousClass1Y3.A2F /*287*/:
            case AnonymousClass1Y3.A2K /*294*/:
            case AnonymousClass1Y3.A2N /*298*/:
            case 299:
            case AnonymousClass1Y3.A2v /*367*/:
            case AnonymousClass1Y3.A2w /*368*/:
            case AnonymousClass1Y3.A34 /*379*/:
            case AnonymousClass1Y3.A35 /*380*/:
            case AnonymousClass1Y3.A38 /*384*/:
                return 0.5d;
            case 1:
                return 0.33d;
            case 2:
            case 20:
            case AnonymousClass1Y3.A0C /*34*/:
            case AnonymousClass1Y3.A0E /*38*/:
            case 54:
            case 62:
            case 67:
            case AnonymousClass1Y3.A0U /*68*/:
            case 79:
            case 87:
            case 91:
            case AnonymousClass1Y3.A0f /*94*/:
            case 95:
            case 99:
            case 100:
            case AnonymousClass1Y3.A0k /*104*/:
            case AnonymousClass1Y3.A0m /*106*/:
            case AnonymousClass1Y3.A0n /*107*/:
            case 112:
            case AnonymousClass1Y3.A0t /*120*/:
            case 124:
            case 139:
            case 155:
            case 160:
            case AnonymousClass1Y3.A1I /*167*/:
            case 173:
            case AnonymousClass1Y3.A1N /*175*/:
            case 183:
            case AnonymousClass1Y3.A1Y /*194*/:
            case AnonymousClass1Y3.A1b /*199*/:
            case AnonymousClass1Y3.A1d /*201*/:
            case 206:
            case 211:
            case 213:
            case 216:
            case 220:
            case 222:
            case 226:
            case AnonymousClass1Y3.A1m /*227*/:
            case 283:
            case AnonymousClass1Y3.A2G /*288*/:
            case AnonymousClass1Y3.A2O /*303*/:
            case 305:
            case AnonymousClass1Y3.A2P /*307*/:
            case AnonymousClass1Y3.A2R /*309*/:
            case 312:
            case 316:
            case AnonymousClass1Y3.A2V /*317*/:
            case AnonymousClass1Y3.A2Z /*322*/:
            case 331:
            case 332:
            case 333:
            case 334:
            case 339:
            case AnonymousClass1Y3.A2g /*343*/:
            case AnonymousClass1Y3.A2j /*346*/:
            case AnonymousClass1Y3.A31 /*376*/:
                return 1.0d;
            case 3:
            case AnonymousClass1Y3.A1h /*215*/:
            case AnonymousClass1Y3.A39 /*386*/:
                return 0.25d;
            case 4:
            case 5:
            case 123:
            case 137:
            case 146:
            case AnonymousClass1Y3.A1F /*161*/:
            case AnonymousClass1Y3.A1H /*163*/:
            case 306:
            case 311:
            case AnonymousClass1Y3.A2Y /*320*/:
                return 0.2d;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
            case AnonymousClass1Y3.A03 /*12*/:
            case 50:
            case 66:
            case 92:
            case 115:
            case AnonymousClass1Y3.A0y /*130*/:
            case AnonymousClass1Y3.A2y /*371*/:
                return 0.75d;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case 8:
            case 103:
            case AnonymousClass1Y3.A10 /*133*/:
            case 134:
            case 315:
                return 1.2d;
            case Process.SIGKILL:
            case 193:
                return 1.1d;
            case AnonymousClass1Y3.A01 /*10*/:
            case Process.SIGCONT:
            case AnonymousClass1Y3.A09 /*27*/:
            case 28:
            case 29:
            case AnonymousClass1Y3.A0A /*30*/:
            case AnonymousClass1Y3.A0B /*31*/:
            case 33:
            case 35:
            case 36:
            case AnonymousClass1Y3.A0F /*39*/:
            case AnonymousClass1Y3.A0G /*40*/:
            case AnonymousClass1Y3.A0H /*41*/:
            case 42:
            case 43:
            case AnonymousClass1Y3.A0I /*44*/:
            case AnonymousClass1Y3.A0J /*45*/:
            case AnonymousClass1Y3.A0K /*46*/:
            case 55:
            case AnonymousClass1Y3.A0P /*56*/:
            case AnonymousClass1Y3.A0S /*63*/:
            case 69:
            case 70:
            case AnonymousClass1Y3.A0W /*73*/:
            case AnonymousClass1Y3.A0a /*80*/:
            case 85:
            case 86:
            case AnonymousClass1Y3.A0j /*102*/:
            case AnonymousClass1Y3.A0q /*113*/:
            case 114:
            case 118:
            case AnonymousClass1Y3.A1D /*156*/:
            case 166:
            case 184:
            case AnonymousClass1Y3.A1S /*185*/:
            case 186:
            case AnonymousClass1Y3.A1U /*188*/:
            case AnonymousClass1Y3.A1V /*189*/:
            case AnonymousClass1Y3.A1W /*190*/:
            case AnonymousClass1Y3.A1X /*191*/:
            case 192:
            case 209:
            case AnonymousClass1Y3.A1i /*217*/:
            case 218:
            case 231:
            case 232:
            case AnonymousClass1Y3.A1o /*233*/:
            case 234:
            case 235:
            case AnonymousClass1Y3.A1p /*236*/:
            case 237:
            case 238:
            case 239:
            case AnonymousClass1Y3.A1q /*240*/:
            case 241:
            case AnonymousClass1Y3.A1r /*242*/:
            case AnonymousClass1Y3.A1s /*243*/:
            case AnonymousClass1Y3.A1t /*244*/:
            case AnonymousClass1Y3.A1u /*245*/:
            case AnonymousClass1Y3.A1v /*246*/:
            case AnonymousClass1Y3.A1w /*247*/:
            case 248:
            case AnonymousClass1Y3.A1x /*249*/:
            case AnonymousClass1Y3.A1y /*250*/:
            case AnonymousClass1Y3.A1z /*251*/:
            case 252:
            case AnonymousClass1Y3.A20 /*253*/:
            case 254:
            case 255:
            case DexStore.LOAD_RESULT_OATMEAL_QUICKENED:
            case AnonymousClass1Y3.A21 /*257*/:
            case 258:
            case 259:
            case AnonymousClass1Y3.A22 /*260*/:
            case 261:
            case 262:
            case 263:
            case 264:
            case 265:
            case AnonymousClass1Y3.A23 /*266*/:
            case AnonymousClass1Y3.A24 /*267*/:
            case 268:
            case AnonymousClass1Y3.A25 /*269*/:
            case AnonymousClass1Y3.A26 /*270*/:
            case 271:
            case AnonymousClass1Y3.A27 /*272*/:
            case 273:
            case 274:
            case AnonymousClass1Y3.A28 /*275*/:
            case 276:
            case AnonymousClass1Y3.A29 /*277*/:
            case AnonymousClass1Y3.A2C /*281*/:
            case AnonymousClass1Y3.A2H /*289*/:
            case AnonymousClass1Y3.A2I /*290*/:
            case AnonymousClass1Y3.A2J /*292*/:
            case AnonymousClass1Y3.A2L /*296*/:
            case AnonymousClass1Y3.A2M /*297*/:
            case 301:
            case AnonymousClass1Y3.A2T /*313*/:
            case AnonymousClass1Y3.A2U /*314*/:
            case 323:
            case 329:
            case AnonymousClass1Y3.A2k /*349*/:
            case AnonymousClass1Y3.A2l /*350*/:
            case AnonymousClass1Y3.A2p /*354*/:
            case AnonymousClass1Y3.A2s /*361*/:
            case 366:
            case AnonymousClass1Y3.A2x /*370*/:
            case 374:
            case 375:
            case AnonymousClass1Y3.A32 /*377*/:
            case 389:
            case AnonymousClass1Y3.A3A /*390*/:
            case 391:
            default:
                return 0.0d;
            case AnonymousClass1Y3.A02 /*11*/:
            case AnonymousClass1Y3.A0c /*84*/:
            case AnonymousClass1Y3.A1k /*221*/:
            case 302:
            case AnonymousClass1Y3.A2X /*319*/:
            case AnonymousClass1Y3.A2b /*327*/:
            case AnonymousClass1Y3.A2t /*362*/:
            case 369:
            case 387:
                return 0.1d;
            case 13:
                return 75.0d;
            case 14:
                return 4.3d;
            case 15:
                return 8.3d;
            case 16:
            case AnonymousClass1Y3.A11 /*135*/:
            case AnonymousClass1Y3.A13 /*138*/:
            case AnonymousClass1Y3.A2A /*278*/:
            case AnonymousClass1Y3.A2c /*328*/:
            case 330:
            case 336:
            case AnonymousClass1Y3.A2d /*337*/:
                return 1.5d;
            case 17:
                return 1.28d;
            case Process.SIGSTOP:
            case AnonymousClass1Y3.A16 /*142*/:
            case 143:
            case AnonymousClass1Y3.A17 /*144*/:
            case AnonymousClass1Y3.A1C /*153*/:
            case 228:
            case 388:
                return 0.01d;
            case AnonymousClass1Y3.A05 /*21*/:
            case AnonymousClass1Y3.A2a /*324*/:
                return 30.0d;
            case AnonymousClass1Y3.A06 /*22*/:
            case 49:
            case AnonymousClass1Y3.A0e /*90*/:
            case 93:
            case AnonymousClass1Y3.A0g /*96*/:
            case 97:
            case AnonymousClass1Y3.A2Q /*308*/:
            case AnonymousClass1Y3.A2u /*363*/:
                return 0.7d;
            case 23:
            case AnonymousClass1Y3.A0O /*53*/:
            case AnonymousClass1Y3.A0l /*105*/:
            case 132:
            case AnonymousClass1Y3.A1g /*214*/:
            case 280:
            case 295:
                return 2.0d;
            case AnonymousClass1Y3.A07 /*24*/:
            case AnonymousClass1Y3.A0p /*110*/:
            case 111:
            case AnonymousClass1Y3.A15 /*141*/:
            case AnonymousClass1Y3.A1A /*149*/:
            case AnonymousClass1Y3.A1G /*162*/:
            case 223:
                return 3.0d;
            case 25:
                return 14000.0d;
            case AnonymousClass1Y3.A08 /*26*/:
                return 10000.0d;
            case 32:
                return -18.0d;
            case AnonymousClass1Y3.A0D /*37*/:
                return 12.0d;
            case AnonymousClass1Y3.A0L /*47*/:
            case 82:
            case AnonymousClass1Y3.A0u /*121*/:
            case AnonymousClass1Y3.A0v /*122*/:
            case 127:
            case 145:
            case 304:
            case AnonymousClass1Y3.A37 /*383*/:
                return 0.8d;
            case AnonymousClass1Y3.A0M /*51*/:
            case 83:
                return 0.001d;
            case 57:
            case 326:
                return 3600.0d;
            case 59:
            case AnonymousClass1Y3.A0R /*60*/:
                return 86400.0d;
            case 61:
            case AnonymousClass1Y3.A0b /*81*/:
            case AnonymousClass1Y3.A1l /*225*/:
            case AnonymousClass1Y3.A2e /*340*/:
                return 10.0d;
            case 64:
            case 224:
                return 5.0d;
            case AnonymousClass1Y3.A0T /*65*/:
                return 600.0d;
            case AnonymousClass1Y3.A0V /*71*/:
            case 179:
            case AnonymousClass1Y3.A1j /*219*/:
            case 364:
            case 365:
            case AnonymousClass1Y3.A36 /*382*/:
                return 0.4d;
            case 72:
            case 128:
            case 152:
                return 0.6d;
            case 74:
            case AnonymousClass1Y3.A0Y /*76*/:
            case AnonymousClass1Y3.A0Z /*77*/:
            case 116:
            case 151:
            case 347:
            case 348:
            case AnonymousClass1Y3.A2o /*353*/:
            case 356:
            case AnonymousClass1Y3.A2r /*360*/:
                return -1.0d;
            case AnonymousClass1Y3.A0X /*75*/:
            case 108:
            case AnonymousClass1Y3.A0o /*109*/:
            case AnonymousClass1Y3.A14 /*140*/:
            case 164:
            case 338:
            case AnonymousClass1Y3.A2h /*344*/:
            case AnonymousClass1Y3.A2m /*351*/:
            case AnonymousClass1Y3.A2q /*355*/:
            case 358:
                return 0.3d;
            case 78:
                return 42.5d;
            case 88:
            case AnonymousClass1Y3.A0d /*89*/:
            case AnonymousClass1Y3.A0w /*125*/:
            case AnonymousClass1Y3.A0x /*126*/:
            case AnonymousClass1Y3.A19 /*148*/:
            case 325:
                return 0.9d;
            case 98:
            case AnonymousClass1Y3.A2W /*318*/:
            case AnonymousClass1Y3.A33 /*378*/:
                return 0.65d;
            case AnonymousClass1Y3.A0s /*119*/:
            case 165:
                return 300.0d;
            case AnonymousClass1Y3.A0z /*131*/:
            case 208:
            case 357:
                return 2.5d;
            case AnonymousClass1Y3.A1B /*150*/:
                return 100.0d;
            case 154:
                return 1.8E-4d;
            case AnonymousClass1Y3.A1E /*157*/:
                return -5.0d;
            case 158:
                return 0.125d;
            case 159:
            case 342:
                return 0.05d;
            case 168:
            case 169:
            case AnonymousClass1Y3.A1J /*170*/:
            case AnonymousClass1Y3.A1K /*171*/:
                return 0.00535d;
            case AnonymousClass1Y3.A1L /*172*/:
            case 210:
                return 20.0d;
            case AnonymousClass1Y3.A1M /*174*/:
                return 0.999d;
            case AnonymousClass1Y3.A1O /*176*/:
                return 4.0d;
            case AnonymousClass1Y3.A1P /*177*/:
            case 204:
            case AnonymousClass1Y3.A2z /*372*/:
                return 16.0d;
            case 178:
            case AnonymousClass1Y3.A1e /*203*/:
                return 9.0d;
            case AnonymousClass1Y3.A1R /*181*/:
                return 1.77778d;
            case 182:
                return 3.2d;
            case AnonymousClass1Y3.A1T /*187*/:
                return 13.0d;
            case 195:
                return 1.75d;
            case AnonymousClass1Y3.A1Z /*196*/:
            case 300:
                return 0.02d;
            case AnonymousClass1Y3.A1a /*197*/:
                return 0.72d;
            case 198:
                return 7.2d;
            case AnonymousClass1Y3.A1c /*200*/:
                return 0.35d;
            case AnonymousClass1Y3.A1f /*207*/:
                return 2.8d;
            case 212:
                return 0.223d;
            case 291:
                return -142.5d;
            case 293:
                return 9.876543210125E9d;
            case AnonymousClass1Y3.A2S /*310*/:
            case 359:
                return 0.15d;
            case 321:
                return 0.025d;
            case 335:
                return 4.5d;
            case AnonymousClass1Y3.A2f /*341*/:
                return 0.562d;
            case AnonymousClass1Y3.A2i /*345*/:
                return 0.083d;
            case AnonymousClass1Y3.A2n /*352*/:
                return 0.003d;
            case AnonymousClass1Y3.A30 /*373*/:
                return 8.0d;
            case 381:
                return 1000.0d;
            case 385:
                return 604800.0d;
            case AnonymousClass1Y3.A3B /*392*/:
                return 300000.0d;
        }
    }

    public static long A01(long j) {
        switch (AnonymousClass0XG.A01(j)) {
            case 0:
            case 25:
            case 48:
            case 49:
            case AnonymousClass1Y3.A0N /*52*/:
            case AnonymousClass1Y3.A0O /*53*/:
            case AnonymousClass1Y3.A1Y /*194*/:
            case AnonymousClass1Y3.A1e /*203*/:
            case AnonymousClass1Y3.A1g /*214*/:
            case 252:
            case AnonymousClass1Y3.A20 /*253*/:
            case 254:
            case AnonymousClass1Y3.A22 /*260*/:
            case 273:
            case 274:
            case 284:
            case 415:
            case AnonymousClass1Y3.A3k /*454*/:
            case AnonymousClass1Y3.A3l /*455*/:
            case 456:
            case AnonymousClass1Y3.A3m /*457*/:
            case 484:
            case 550:
            case 604:
            case AnonymousClass1Y3.A5O /*669*/:
            case 670:
            case AnonymousClass1Y3.A5s /*719*/:
            case 750:
            case 754:
            case AnonymousClass1Y3.A6D /*762*/:
            case 764:
            case 792:
            case 793:
            case 853:
            case 919:
            case 924:
            case 943:
            case 944:
            case AnonymousClass1Y3.A7f /*949*/:
            case AnonymousClass1Y3.A8S /*1028*/:
            case 1044:
            case 1149:
            case 1171:
            case 1172:
            case 1173:
            case 1174:
            case AnonymousClass1Y3.A9a /*1176*/:
            case 1177:
            case 1188:
            case AnonymousClass1Y3.A9f /*1189*/:
            case 1198:
            case AnonymousClass1Y3.AAF /*1257*/:
            case AnonymousClass1Y3.AAv /*1329*/:
            case AnonymousClass1Y3.AB8 /*1353*/:
            case 1354:
            case 1366:
            case AnonymousClass1Y3.ABK /*1381*/:
            case AnonymousClass1Y3.ABO /*1386*/:
            case 1393:
            case AnonymousClass1Y3.ABn /*1439*/:
            case AnonymousClass1Y3.ABo /*1442*/:
            case AnonymousClass1Y3.ACC /*1490*/:
            case AnonymousClass1Y3.ACf /*1546*/:
            case AnonymousClass1Y3.ACv /*1586*/:
            case 1611:
            case 1630:
            case AnonymousClass1Y3.ADS /*1649*/:
            case AnonymousClass1Y3.ADz /*1717*/:
            case AnonymousClass1Y3.AE8 /*1735*/:
            case 1736:
            case AnonymousClass1Y3.AE9 /*1737*/:
            case AnonymousClass1Y3.AEB /*1739*/:
            case 1755:
            case 1759:
            case 1782:
            case 1783:
            case 1809:
            case 1810:
            case AnonymousClass1Y3.AEv /*1826*/:
            case 1895:
            case AnonymousClass1Y3.AG3 /*1950*/:
            case 1964:
            case AnonymousClass1Y3.AGv /*2044*/:
            case AnonymousClass1Y3.AGw /*2045*/:
            case 2136:
            case 2152:
            case 2174:
            case 2202:
            case AnonymousClass1Y3.AIA /*2215*/:
            case 2257:
            case 2260:
            case 2266:
            case AnonymousClass1Y3.AIn /*2289*/:
            case AnonymousClass1Y3.AJL /*2350*/:
            case AnonymousClass1Y3.AJN /*2353*/:
            case AnonymousClass1Y3.AJO /*2354*/:
            case AnonymousClass1Y3.AJQ /*2361*/:
            case 2363:
            case 2399:
            case AnonymousClass1Y3.AKC /*2449*/:
            case 2495:
            case 2572:
            case 2579:
            case AnonymousClass1Y3.AO3 /*2928*/:
            case AnonymousClass1Y3.AO4 /*2929*/:
                return 1;
            case 1:
            case AnonymousClass1Y3.A0L /*47*/:
            case 50:
            case 211:
            case 220:
            case AnonymousClass1Y3.A1z /*251*/:
            case 259:
            case 283:
            case AnonymousClass1Y3.A2i /*345*/:
            case 398:
            case AnonymousClass1Y3.A3H /*399*/:
            case AnonymousClass1Y3.A3h /*451*/:
            case AnonymousClass1Y3.A3i /*452*/:
            case AnonymousClass1Y3.A3j /*453*/:
            case 504:
            case 574:
            case 579:
            case AnonymousClass1Y3.A4s /*585*/:
            case 586:
            case 587:
            case 588:
            case 599:
            case AnonymousClass1Y3.A51 /*623*/:
            case AnonymousClass1Y3.A52 /*624*/:
            case 631:
            case AnonymousClass1Y3.A5o /*713*/:
            case AnonymousClass1Y3.A5p /*714*/:
            case AnonymousClass1Y3.A5q /*716*/:
            case 755:
            case 759:
            case 850:
            case AnonymousClass1Y3.A6u /*862*/:
            case 863:
            case AnonymousClass1Y3.A6v /*864*/:
            case 866:
            case 879:
            case AnonymousClass1Y3.A7b /*939*/:
            case AnonymousClass1Y3.A7e /*948*/:
            case AnonymousClass1Y3.A7o /*963*/:
            case AnonymousClass1Y3.A8G /*1015*/:
            case AnonymousClass1Y3.A8a /*1053*/:
            case 1121:
            case 1123:
            case AnonymousClass1Y3.A9F /*1125*/:
            case 1131:
            case 1138:
            case AnonymousClass1Y3.A9R /*1151*/:
            case 1166:
            case AnonymousClass1Y3.AAK /*1263*/:
            case AnonymousClass1Y3.AB6 /*1347*/:
            case 1425:
            case 1426:
            case AnonymousClass1Y3.ABl /*1431*/:
            case 1447:
            case 1526:
            case 1527:
            case 1531:
            case AnonymousClass1Y3.ACa /*1534*/:
            case 1535:
            case 1684:
            case AnonymousClass1Y3.ADj /*1686*/:
            case 1687:
            case 1695:
            case AnonymousClass1Y3.ADx /*1714*/:
            case 1715:
            case AnonymousClass1Y3.AE4 /*1725*/:
            case AnonymousClass1Y3.AE7 /*1734*/:
            case AnonymousClass1Y3.AEN /*1752*/:
            case 1772:
            case AnonymousClass1Y3.AF2 /*1840*/:
            case AnonymousClass1Y3.AFF /*1865*/:
            case 1868:
            case AnonymousClass1Y3.AFG /*1869*/:
            case 1876:
            case 1886:
            case 1888:
            case 1889:
            case AnonymousClass1Y3.AFT /*1890*/:
            case AnonymousClass1Y3.AFW /*1894*/:
            case 1933:
            case AnonymousClass1Y3.AG8 /*1960*/:
            case 1961:
            case 1962:
            case 1963:
            case AnonymousClass1Y3.AHJ /*2095*/:
            case AnonymousClass1Y3.AHK /*2096*/:
            case AnonymousClass1Y3.AHL /*2097*/:
            case AnonymousClass1Y3.AHM /*2098*/:
            case AnonymousClass1Y3.AHN /*2099*/:
            case AnonymousClass1Y3.AHO /*2100*/:
            case AnonymousClass1Y3.AHc /*2132*/:
            case 2137:
            case AnonymousClass1Y3.AHn /*2156*/:
            case AnonymousClass1Y3.AHw /*2178*/:
            case 2187:
            case 2212:
            case AnonymousClass1Y3.AIC /*2220*/:
            case 2221:
            case AnonymousClass1Y3.AID /*2222*/:
            case AnonymousClass1Y3.AII /*2229*/:
            case AnonymousClass1Y3.AId /*2274*/:
            case 2306:
            case 2309:
            case 2321:
            case 2400:
            case 2511:
            case AnonymousClass1Y3.ALT /*2608*/:
            case 2613:
            case AnonymousClass1Y3.ALU /*2614*/:
            case AnonymousClass1Y3.AO5 /*2930*/:
            case AnonymousClass1Y3.AOb /*2986*/:
            case 2987:
                return 3;
            case 2:
                return 13;
            case 3:
            case 55:
                return 106;
            case 4:
                return 3500000;
            case 5:
            case AnonymousClass1Y3.A0m /*106*/:
            case 112:
            case AnonymousClass1Y3.A1H /*163*/:
            case 295:
            case AnonymousClass1Y3.A44 /*495*/:
            case 600:
            case AnonymousClass1Y3.A5A /*638*/:
            case 729:
            case 934:
            case AnonymousClass1Y3.A7Z /*937*/:
            case AnonymousClass1Y3.A8Y /*1043*/:
            case AnonymousClass1Y3.A9z /*1222*/:
            case AnonymousClass1Y3.AAj /*1305*/:
            case AnonymousClass1Y3.AAu /*1328*/:
            case 1412:
            case 1493:
            case AnonymousClass1Y3.ACT /*1521*/:
            case 1523:
            case 1557:
            case AnonymousClass1Y3.AHd /*2133*/:
            case 2183:
            case AnonymousClass1Y3.AIB /*2218*/:
            case AnonymousClass1Y3.AIP /*2240*/:
            case 2349:
            case 2448:
            case AnonymousClass1Y3.AOX /*2982*/:
            case AnonymousClass1Y3.AOZ /*2984*/:
            case AnonymousClass1Y3.AOa /*2985*/:
                return 30;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
            case 54:
            case AnonymousClass1Y3.A0P /*56*/:
            case 57:
            case AnonymousClass1Y3.A0Z /*77*/:
            case 83:
            case 98:
            case AnonymousClass1Y3.A1c /*200*/:
            case AnonymousClass1Y3.A1h /*215*/:
            case 235:
            case 330:
            case 331:
            case AnonymousClass1Y3.A2k /*349*/:
            case AnonymousClass1Y3.A37 /*383*/:
            case AnonymousClass1Y3.A3G /*397*/:
            case AnonymousClass1Y3.A3I /*400*/:
            case AnonymousClass1Y3.A3W /*429*/:
            case AnonymousClass1Y3.A3o /*460*/:
            case 476:
            case AnonymousClass1Y3.A3y /*478*/:
            case AnonymousClass1Y3.A47 /*498*/:
            case AnonymousClass1Y3.A48 /*499*/:
            case 589:
            case 595:
            case 605:
            case 614:
            case 634:
            case 643:
            case 687:
            case AnonymousClass1Y3.A5b /*693*/:
            case AnonymousClass1Y3.A5u /*725*/:
            case AnonymousClass1Y3.A5v /*726*/:
            case AnonymousClass1Y3.A7U /*930*/:
            case AnonymousClass1Y3.A7d /*941*/:
            case AnonymousClass1Y3.A7g /*950*/:
            case AnonymousClass1Y3.A8C /*1009*/:
            case AnonymousClass1Y3.A8F /*1013*/:
            case AnonymousClass1Y3.A8L /*1020*/:
            case 1041:
            case AnonymousClass1Y3.A9E /*1124*/:
            case 1144:
            case AnonymousClass1Y3.A9S /*1152*/:
            case AnonymousClass1Y3.AA9 /*1244*/:
            case 1247:
            case 1256:
            case AnonymousClass1Y3.AAS /*1275*/:
            case 1339:
            case 1340:
            case AnonymousClass1Y3.AB2 /*1341*/:
            case AnonymousClass1Y3.AB9 /*1357*/:
            case AnonymousClass1Y3.AC6 /*1481*/:
            case AnonymousClass1Y3.AC7 /*1483*/:
            case 1484:
            case 1492:
            case AnonymousClass1Y3.ADX /*1662*/:
            case AnonymousClass1Y3.ADZ /*1665*/:
            case AnonymousClass1Y3.ADb /*1668*/:
            case 1671:
            case AnonymousClass1Y3.ADu /*1710*/:
            case AnonymousClass1Y3.AEM /*1751*/:
            case AnonymousClass1Y3.AEP /*1760*/:
            case AnonymousClass1Y3.AEp /*1812*/:
            case 1871:
            case AnonymousClass1Y3.AFI /*1872*/:
            case AnonymousClass1Y3.AFK /*1874*/:
            case 1881:
            case AnonymousClass1Y3.AFV /*1893*/:
            case 1919:
            case AnonymousClass1Y3.AFk /*1921*/:
            case AnonymousClass1Y3.AGB /*1969*/:
            case AnonymousClass1Y3.AH9 /*2071*/:
            case 2129:
            case 2130:
            case 2272:
            case AnonymousClass1Y3.AIj /*2280*/:
            case 2347:
            case AnonymousClass1Y3.ALO /*2600*/:
            case 2601:
            case AnonymousClass1Y3.ALQ /*2603*/:
            case 2905:
            case AnonymousClass1Y3.AO7 /*2934*/:
            case 2935:
            case AnonymousClass1Y3.AOE /*2946*/:
                return 5;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case 1373:
            case AnonymousClass1Y3.AIF /*2225*/:
                return 1280;
            case 8:
            case AnonymousClass1Y3.A01 /*10*/:
            case 311:
            case AnonymousClass1Y3.A42 /*493*/:
            case AnonymousClass1Y3.A6F /*767*/:
            case AnonymousClass1Y3.A6G /*768*/:
            case AnonymousClass1Y3.ACM /*1509*/:
            case AnonymousClass1Y3.AFY /*1897*/:
            case AnonymousClass1Y3.AHu /*2171*/:
            case AnonymousClass1Y3.AIJ /*2230*/:
            case AnonymousClass1Y3.ALN /*2598*/:
            case 2599:
                return 86400000;
            case Process.SIGKILL:
                return 1209600000;
            case AnonymousClass1Y3.A02 /*11*/:
            case 123:
            case AnonymousClass1Y3.A1q /*240*/:
            case 306:
            case AnonymousClass1Y3.A2P /*307*/:
            case AnonymousClass1Y3.A3A /*390*/:
            case AnonymousClass1Y3.A3D /*394*/:
            case 438:
            case 509:
            case AnonymousClass1Y3.A4I /*515*/:
            case AnonymousClass1Y3.A4J /*516*/:
            case 547:
            case 613:
            case AnonymousClass1Y3.A54 /*627*/:
            case 868:
            case AnonymousClass1Y3.A7N /*916*/:
            case AnonymousClass1Y3.A7a /*938*/:
            case AnonymousClass1Y3.A7t /*972*/:
            case 975:
            case 1080:
            case AnonymousClass1Y3.A8t /*1084*/:
            case AnonymousClass1Y3.A8u /*1085*/:
            case 1139:
            case AnonymousClass1Y3.A9T /*1153*/:
            case AnonymousClass1Y3.A9Y /*1162*/:
            case 1266:
            case AnonymousClass1Y3.AAo /*1311*/:
            case 1322:
            case 1415:
            case 1553:
            case AnonymousClass1Y3.ACi /*1554*/:
            case AnonymousClass1Y3.ACj /*1555*/:
            case AnonymousClass1Y3.ACk /*1556*/:
            case AnonymousClass1Y3.ADt /*1706*/:
            case 1726:
            case 1850:
            case 1898:
            case AnonymousClass1Y3.AGm /*2032*/:
            case AnonymousClass1Y3.AGr /*2039*/:
            case 2109:
            case 2192:
            case 2253:
            case 2355:
            case 2360:
            case AnonymousClass1Y3.ALR /*2605*/:
            case 2943:
            case AnonymousClass1Y3.AOD /*2944*/:
                return StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS;
            case AnonymousClass1Y3.A03 /*12*/:
            case 234:
            case AnonymousClass1Y3.A1v /*246*/:
            case 426:
            case AnonymousClass1Y3.A7T /*929*/:
            case AnonymousClass1Y3.AAN /*1268*/:
            case AnonymousClass1Y3.AAl /*1307*/:
            case AnonymousClass1Y3.AB5 /*1346*/:
            case AnonymousClass1Y3.ABU /*1401*/:
            case AnonymousClass1Y3.AC2 /*1469*/:
            case 1859:
            case 2090:
            case AnonymousClass1Y3.AI2 /*2194*/:
            case 2283:
                return 60000;
            case 13:
            case 127:
            case 437:
            case 936:
            case 2114:
                return 90000;
            case 14:
            case 111:
            case AnonymousClass1Y3.A1K /*171*/:
            case AnonymousClass1Y3.A2X /*319*/:
            case 342:
            case AnonymousClass1Y3.A2l /*350*/:
            case AnonymousClass1Y3.A2n /*352*/:
            case 359:
            case 401:
            case 419:
            case AnonymousClass1Y3.A4E /*507*/:
            case 508:
            case AnonymousClass1Y3.A4L /*518*/:
            case AnonymousClass1Y3.A4p /*580*/:
            case 593:
            case 620:
            case 658:
            case 752:
            case AnonymousClass1Y3.A6z /*870*/:
            case 946:
            case AnonymousClass1Y3.A9e /*1184*/:
            case 1185:
            case 1186:
            case AnonymousClass1Y3.AAH /*1259*/:
            case 1421:
            case AnonymousClass1Y3.ACd /*1541*/:
            case AnonymousClass1Y3.AGC /*1970*/:
            case 2166:
            case 2271:
            case 2322:
            case 2914:
                return 60;
            case 15:
            case AnonymousClass1Y3.AA3 /*1228*/:
                return StatFsUtil.IN_MEGA_BYTE;
            case 16:
            case 17:
            case Process.SIGCONT:
            case Process.SIGSTOP:
            case AnonymousClass1Y3.A05 /*21*/:
            case AnonymousClass1Y3.A06 /*22*/:
            case 23:
            case AnonymousClass1Y3.A07 /*24*/:
            case 33:
            case AnonymousClass1Y3.A0H /*41*/:
            case 42:
            case 43:
            case AnonymousClass1Y3.A0K /*46*/:
            case AnonymousClass1Y3.A0T /*65*/:
            case 67:
            case AnonymousClass1Y3.A0U /*68*/:
            case 70:
            case 72:
            case 82:
            case AnonymousClass1Y3.A0c /*84*/:
            case 87:
            case AnonymousClass1Y3.A0e /*90*/:
            case 91:
            case 92:
            case 93:
            case 100:
            case AnonymousClass1Y3.A0n /*107*/:
            case AnonymousClass1Y3.A0v /*122*/:
            case 169:
            case 173:
            case 179:
            case 184:
            case AnonymousClass1Y3.A1U /*188*/:
            case 195:
            case AnonymousClass1Y3.A1d /*201*/:
            case 208:
            case 210:
            case 216:
            case AnonymousClass1Y3.A1i /*217*/:
            case 223:
            case AnonymousClass1Y3.A1p /*236*/:
            case 237:
            case 239:
            case AnonymousClass1Y3.A1u /*245*/:
            case AnonymousClass1Y3.A1w /*247*/:
            case AnonymousClass1Y3.A1x /*249*/:
            case DexStore.LOAD_RESULT_OATMEAL_QUICKENED:
            case 261:
            case 262:
            case 264:
            case 265:
            case AnonymousClass1Y3.A24 /*267*/:
            case 271:
            case 276:
            case AnonymousClass1Y3.A29 /*277*/:
            case AnonymousClass1Y3.A2A /*278*/:
            case AnonymousClass1Y3.A2B /*279*/:
            case 280:
            case AnonymousClass1Y3.A2H /*289*/:
            case AnonymousClass1Y3.A2I /*290*/:
            case 291:
            case AnonymousClass1Y3.A2J /*292*/:
            case 293:
            case 301:
            case 302:
            case AnonymousClass1Y3.A2h /*344*/:
            case AnonymousClass1Y3.A2t /*362*/:
            case 369:
            case AnonymousClass1Y3.A2x /*370*/:
            case AnonymousClass1Y3.A2y /*371*/:
            case 375:
            case AnonymousClass1Y3.A31 /*376*/:
            case AnonymousClass1Y3.A33 /*378*/:
            case AnonymousClass1Y3.A34 /*379*/:
            case AnonymousClass1Y3.A35 /*380*/:
            case AnonymousClass1Y3.A39 /*386*/:
            case 387:
            case 388:
            case 389:
            case AnonymousClass1Y3.A3M /*408*/:
            case 409:
            case 410:
            case 418:
            case 427:
            case 436:
            case AnonymousClass1Y3.A3d /*444*/:
            case AnonymousClass1Y3.A3e /*445*/:
            case AnonymousClass1Y3.A3f /*446*/:
            case 447:
            case AnonymousClass1Y3.A3g /*448*/:
            case 449:
            case 450:
            case AnonymousClass1Y3.A3n /*458*/:
            case 482:
            case AnonymousClass1Y3.A3z /*483*/:
            case 489:
            case 490:
            case AnonymousClass1Y3.A40 /*491*/:
            case AnonymousClass1Y3.A45 /*496*/:
            case AnonymousClass1Y3.A46 /*497*/:
            case AnonymousClass1Y3.A4A /*501*/:
            case 511:
            case 512:
            case AnonymousClass1Y3.A4H /*513*/:
            case 514:
            case 530:
            case AnonymousClass1Y3.A4U /*531*/:
            case AnonymousClass1Y3.A4V /*532*/:
            case AnonymousClass1Y3.A4W /*533*/:
            case 534:
            case AnonymousClass1Y3.A4X /*535*/:
            case AnonymousClass1Y3.A4Y /*536*/:
            case AnonymousClass1Y3.A4Z /*537*/:
            case 538:
            case AnonymousClass1Y3.A4a /*539*/:
            case 540:
            case AnonymousClass1Y3.A4b /*541*/:
            case 545:
            case 546:
            case 556:
            case AnonymousClass1Y3.A4f /*557*/:
            case AnonymousClass1Y3.A4i /*562*/:
            case AnonymousClass1Y3.A4j /*563*/:
            case 564:
            case 567:
            case AnonymousClass1Y3.A4l /*568*/:
            case 569:
            case 570:
            case 571:
            case AnonymousClass1Y3.A4m /*572*/:
            case 582:
            case AnonymousClass1Y3.A4r /*583*/:
            case 584:
            case 592:
            case 602:
            case 603:
            case AnonymousClass1Y3.A4z /*618*/:
            case 619:
            case 641:
            case 642:
            case 644:
            case 645:
            case AnonymousClass1Y3.A5F /*648*/:
            case 649:
            case 650:
            case 651:
            case 655:
            case AnonymousClass1Y3.A5K /*659*/:
            case 660:
            case 661:
            case 662:
            case AnonymousClass1Y3.A5L /*663*/:
            case AnonymousClass1Y3.A5W /*682*/:
            case 689:
            case AnonymousClass1Y3.A5a /*690*/:
            case 691:
            case 692:
            case AnonymousClass1Y3.A5d /*695*/:
            case AnonymousClass1Y3.A5f /*697*/:
            case 698:
            case AnonymousClass1Y3.A5g /*699*/:
            case AnonymousClass1Y3.A5l /*707*/:
            case 712:
            case AnonymousClass1Y3.A5t /*721*/:
            case AnonymousClass1Y3.A5w /*727*/:
            case AnonymousClass1Y3.A5z /*734*/:
            case 735:
            case AnonymousClass1Y3.A60 /*736*/:
            case 737:
            case 746:
            case AnonymousClass1Y3.A65 /*747*/:
            case AnonymousClass1Y3.A66 /*748*/:
            case AnonymousClass1Y3.A67 /*749*/:
            case 757:
            case AnonymousClass1Y3.A6B /*760*/:
            case AnonymousClass1Y3.A6C /*761*/:
            case 763:
            case 765:
            case 770:
            case 776:
            case AnonymousClass1Y3.A6J /*777*/:
            case 778:
            case AnonymousClass1Y3.A6K /*779*/:
            case AnonymousClass1Y3.A6L /*780*/:
            case AnonymousClass1Y3.A6M /*781*/:
            case AnonymousClass1Y3.A6N /*782*/:
            case 790:
            case 791:
            case AnonymousClass1Y3.A6T /*797*/:
            case 833:
            case 851:
            case 852:
            case AnonymousClass1Y3.A6x /*867*/:
            case AnonymousClass1Y3.A6y /*869*/:
            case AnonymousClass1Y3.A70 /*871*/:
            case AnonymousClass1Y3.A75 /*876*/:
            case AnonymousClass1Y3.A76 /*877*/:
            case AnonymousClass1Y3.A77 /*878*/:
            case AnonymousClass1Y3.A7O /*917*/:
            case 918:
            case AnonymousClass1Y3.A7P /*920*/:
            case 921:
            case AnonymousClass1Y3.A7Q /*922*/:
            case AnonymousClass1Y3.A7R /*923*/:
            case AnonymousClass1Y3.A7W /*932*/:
            case AnonymousClass1Y3.A7h /*951*/:
            case 952:
            case AnonymousClass1Y3.A7i /*953*/:
            case 954:
            case AnonymousClass1Y3.A7j /*955*/:
            case AnonymousClass1Y3.A7k /*956*/:
            case 959:
            case AnonymousClass1Y3.A7m /*961*/:
            case AnonymousClass1Y3.A7n /*962*/:
            case AnonymousClass1Y3.A7s /*971*/:
            case AnonymousClass1Y3.A7v /*977*/:
            case AnonymousClass1Y3.A7w /*978*/:
            case 987:
            case 994:
            case 995:
            case AnonymousClass1Y3.A86 /*996*/:
            case 997:
            case 999:
            case AnonymousClass1Y3.A87 /*1000*/:
            case 1001:
            case 1002:
            case AnonymousClass1Y3.A8E /*1012*/:
            case AnonymousClass1Y3.A8J /*1018*/:
            case AnonymousClass1Y3.A8K /*1019*/:
            case AnonymousClass1Y3.A8P /*1025*/:
            case AnonymousClass1Y3.A8Q /*1026*/:
            case AnonymousClass1Y3.A8R /*1027*/:
            case AnonymousClass1Y3.A8U /*1031*/:
            case 1047:
            case 1048:
            case 1049:
            case AnonymousClass1Y3.A8Z /*1050*/:
            case 1051:
            case 1052:
            case 1054:
            case AnonymousClass1Y3.A8e /*1059*/:
            case 1066:
            case AnonymousClass1Y3.A8i /*1067*/:
            case 1092:
            case AnonymousClass1Y3.A92 /*1097*/:
            case AnonymousClass1Y3.A93 /*1098*/:
            case 1099:
            case 1100:
            case 1101:
            case AnonymousClass1Y3.A95 /*1105*/:
            case AnonymousClass1Y3.A96 /*1106*/:
            case AnonymousClass1Y3.A99 /*1112*/:
            case 1113:
            case AnonymousClass1Y3.A9C /*1119*/:
            case AnonymousClass1Y3.A9G /*1126*/:
            case 1128:
            case AnonymousClass1Y3.A9H /*1129*/:
            case 1136:
            case AnonymousClass1Y3.A9K /*1137*/:
            case AnonymousClass1Y3.A9M /*1142*/:
            case AnonymousClass1Y3.A9U /*1155*/:
            case AnonymousClass1Y3.A9V /*1157*/:
            case 1158:
            case AnonymousClass1Y3.A9W /*1159*/:
            case 1179:
            case 1180:
            case 1181:
            case AnonymousClass1Y3.A9c /*1182*/:
            case 1190:
            case AnonymousClass1Y3.AA0 /*1223*/:
            case 1225:
            case 1226:
            case 1230:
            case 1237:
            case 1238:
            case AnonymousClass1Y3.AA7 /*1239*/:
            case AnonymousClass1Y3.AAA /*1245*/:
            case 1246:
            case 1248:
            case AnonymousClass1Y3.AAC /*1250*/:
            case 1252:
            case AnonymousClass1Y3.AAQ /*1271*/:
            case AnonymousClass1Y3.AAR /*1272*/:
            case 1273:
            case 1274:
            case AnonymousClass1Y3.AAV /*1280*/:
            case 1282:
            case AnonymousClass1Y3.AAW /*1283*/:
            case AnonymousClass1Y3.AAY /*1285*/:
            case 1286:
            case AnonymousClass1Y3.AAa /*1291*/:
            case AnonymousClass1Y3.AAb /*1292*/:
            case AnonymousClass1Y3.AAc /*1293*/:
            case 1294:
            case AnonymousClass1Y3.AAd /*1295*/:
            case AnonymousClass1Y3.AAf /*1299*/:
            case 1312:
            case 1313:
            case 1314:
            case AnonymousClass1Y3.AAp /*1315*/:
            case AnonymousClass1Y3.AAz /*1336*/:
            case AnonymousClass1Y3.AB0 /*1337*/:
            case 1344:
            case AnonymousClass1Y3.AB7 /*1349*/:
            case 1350:
            case 1351:
            case AnonymousClass1Y3.ABC /*1361*/:
            case 1363:
            case 1365:
            case 1374:
            case AnonymousClass1Y3.ABL /*1382*/:
            case 1383:
            case 1389:
            case 1390:
            case AnonymousClass1Y3.ABP /*1391*/:
            case 1392:
            case AnonymousClass1Y3.ABQ /*1394*/:
            case 1395:
            case AnonymousClass1Y3.ABR /*1396*/:
            case AnonymousClass1Y3.ABS /*1397*/:
            case AnonymousClass1Y3.ABZ /*1407*/:
            case AnonymousClass1Y3.ABa /*1408*/:
            case AnonymousClass1Y3.ABg /*1419*/:
            case AnonymousClass1Y3.ABj /*1428*/:
            case AnonymousClass1Y3.ABk /*1429*/:
            case 1430:
            case 1440:
            case 1450:
            case AnonymousClass1Y3.ABt /*1451*/:
            case AnonymousClass1Y3.ABu /*1452*/:
            case AnonymousClass1Y3.ABv /*1455*/:
            case AnonymousClass1Y3.ABw /*1456*/:
            case AnonymousClass1Y3.ABx /*1457*/:
            case 1458:
            case 1459:
            case 1460:
            case 1461:
            case AnonymousClass1Y3.ABy /*1462*/:
            case AnonymousClass1Y3.ABz /*1463*/:
            case AnonymousClass1Y3.AC0 /*1464*/:
            case 1465:
            case 1466:
            case 1470:
            case 1471:
            case AnonymousClass1Y3.AC3 /*1472*/:
            case 1473:
            case 1477:
            case 1478:
            case AnonymousClass1Y3.ACD /*1491*/:
            case 1494:
            case AnonymousClass1Y3.ACE /*1495*/:
            case 1496:
            case 1505:
            case AnonymousClass1Y3.ACK /*1506*/:
            case 1513:
            case 1515:
            case 1516:
            case AnonymousClass1Y3.ACW /*1525*/:
            case AnonymousClass1Y3.ACZ /*1533*/:
            case AnonymousClass1Y3.ACe /*1543*/:
            case 1544:
            case 1560:
            case AnonymousClass1Y3.ACl /*1561*/:
            case AnonymousClass1Y3.ACm /*1562*/:
            case 1563:
            case 1564:
            case 1565:
            case AnonymousClass1Y3.ACn /*1566*/:
            case AnonymousClass1Y3.ACo /*1567*/:
            case 1568:
            case AnonymousClass1Y3.ACp /*1569*/:
            case 1570:
            case AnonymousClass1Y3.ACq /*1571*/:
            case 1572:
            case 1573:
            case 1574:
            case 1575:
            case AnonymousClass1Y3.ACr /*1576*/:
            case 1577:
            case 1578:
            case 1579:
            case AnonymousClass1Y3.ACs /*1580*/:
            case AnonymousClass1Y3.ACt /*1581*/:
            case 1583:
            case AnonymousClass1Y3.ACu /*1584*/:
            case 1585:
            case 1587:
            case 1588:
            case 1589:
            case AnonymousClass1Y3.ACw /*1590*/:
            case 1591:
            case 1592:
            case AnonymousClass1Y3.ACx /*1595*/:
            case AnonymousClass1Y3.ACy /*1596*/:
            case 1597:
            case 1598:
            case AnonymousClass1Y3.ACz /*1599*/:
            case AnonymousClass1Y3.AD0 /*1600*/:
            case AnonymousClass1Y3.AD1 /*1601*/:
            case AnonymousClass1Y3.AD2 /*1602*/:
            case 1603:
            case 1604:
            case AnonymousClass1Y3.AD3 /*1605*/:
            case AnonymousClass1Y3.AD4 /*1606*/:
            case 1608:
            case 1609:
            case AnonymousClass1Y3.AD6 /*1610*/:
            case AnonymousClass1Y3.AD7 /*1612*/:
            case AnonymousClass1Y3.AD8 /*1613*/:
            case 1614:
            case 1615:
            case AnonymousClass1Y3.AD9 /*1616*/:
            case AnonymousClass1Y3.ADA /*1617*/:
            case 1618:
            case 1619:
            case 1620:
            case 1621:
            case AnonymousClass1Y3.ADB /*1622*/:
            case AnonymousClass1Y3.ADC /*1623*/:
            case 1624:
            case AnonymousClass1Y3.ADD /*1625*/:
            case 1627:
            case AnonymousClass1Y3.ADE /*1628*/:
            case AnonymousClass1Y3.ADF /*1629*/:
            case AnonymousClass1Y3.ADG /*1631*/:
            case AnonymousClass1Y3.ADH /*1632*/:
            case AnonymousClass1Y3.ADI /*1633*/:
            case AnonymousClass1Y3.ADJ /*1634*/:
            case 1635:
            case AnonymousClass1Y3.ADK /*1636*/:
            case 1637:
            case 1638:
            case 1639:
            case AnonymousClass1Y3.ADL /*1640*/:
            case AnonymousClass1Y3.ADM /*1641*/:
            case AnonymousClass1Y3.ADN /*1642*/:
            case 1643:
            case AnonymousClass1Y3.ADO /*1644*/:
            case AnonymousClass1Y3.ADQ /*1646*/:
            case 1647:
            case AnonymousClass1Y3.ADR /*1648*/:
            case 1650:
            case 1651:
            case 1652:
            case 1653:
            case AnonymousClass1Y3.ADT /*1654*/:
            case 1655:
            case 1656:
            case AnonymousClass1Y3.ADV /*1659*/:
            case 1672:
            case AnonymousClass1Y3.ADc /*1673*/:
            case AnonymousClass1Y3.ADd /*1674*/:
            case AnonymousClass1Y3.ADg /*1680*/:
            case AnonymousClass1Y3.ADk /*1689*/:
            case 1690:
            case AnonymousClass1Y3.ADl /*1691*/:
            case 1699:
            case 1705:
            case 1707:
            case 1709:
            case AnonymousClass1Y3.ADy /*1716*/:
            case AnonymousClass1Y3.AE2 /*1720*/:
            case 1733:
            case AnonymousClass1Y3.AEE /*1743*/:
            case AnonymousClass1Y3.AEG /*1745*/:
            case AnonymousClass1Y3.AEJ /*1748*/:
            case AnonymousClass1Y3.AEK /*1749*/:
            case AnonymousClass1Y3.AEL /*1750*/:
            case 1753:
            case 1754:
            case AnonymousClass1Y3.AEO /*1757*/:
            case 1765:
            case AnonymousClass1Y3.AER /*1766*/:
            case 1770:
            case AnonymousClass1Y3.AEf /*1792*/:
            case 1793:
            case 1794:
            case AnonymousClass1Y3.AEg /*1795*/:
            case AnonymousClass1Y3.AEh /*1796*/:
            case 1797:
            case 1798:
            case AnonymousClass1Y3.AEi /*1799*/:
            case 1800:
            case AnonymousClass1Y3.AEs /*1817*/:
            case 1823:
            case 1824:
            case AnonymousClass1Y3.AEw /*1827*/:
            case 1828:
            case AnonymousClass1Y3.AEx /*1829*/:
            case AnonymousClass1Y3.AEy /*1830*/:
            case AnonymousClass1Y3.AEz /*1831*/:
            case 1832:
            case 1833:
            case AnonymousClass1Y3.AF0 /*1834*/:
            case AnonymousClass1Y3.AF1 /*1835*/:
            case 1836:
            case 1837:
            case 1838:
            case 1839:
            case 1841:
            case 1844:
            case AnonymousClass1Y3.AF4 /*1845*/:
            case AnonymousClass1Y3.AF5 /*1846*/:
            case 1851:
            case AnonymousClass1Y3.AFB /*1856*/:
            case AnonymousClass1Y3.AFL /*1877*/:
            case AnonymousClass1Y3.AFM /*1878*/:
            case 1915:
            case AnonymousClass1Y3.AFi /*1916*/:
            case 1922:
            case 1925:
            case AnonymousClass1Y3.AFq /*1929*/:
            case AnonymousClass1Y3.AFu /*1936*/:
            case AnonymousClass1Y3.AFx /*1941*/:
            case AnonymousClass1Y3.AFy /*1942*/:
            case 1943:
            case AnonymousClass1Y3.AFz /*1944*/:
            case AnonymousClass1Y3.AG0 /*1946*/:
            case AnonymousClass1Y3.AG1 /*1947*/:
            case 1948:
            case AnonymousClass1Y3.AG2 /*1949*/:
            case AnonymousClass1Y3.AG4 /*1951*/:
            case AnonymousClass1Y3.AG5 /*1952*/:
            case 1954:
            case 1956:
            case 1965:
            case AnonymousClass1Y3.AGF /*1974*/:
            case 1977:
            case 1978:
            case 1979:
            case 1980:
            case AnonymousClass1Y3.AGH /*1981*/:
            case 1982:
            case AnonymousClass1Y3.AGI /*1983*/:
            case 1984:
            case AnonymousClass1Y3.AGJ /*1985*/:
            case AnonymousClass1Y3.AGK /*1986*/:
            case AnonymousClass1Y3.AGL /*1987*/:
            case 1988:
            case AnonymousClass1Y3.AGo /*2034*/:
            case AnonymousClass1Y3.AGq /*2037*/:
            case AnonymousClass1Y3.AGu /*2043*/:
            case 2046:
            case 2050:
            case AnonymousClass1Y3.AGz /*2051*/:
            case 2052:
            case 2053:
            case 2054:
            case AnonymousClass1Y3.AH1 /*2056*/:
            case 2057:
            case 2058:
            case 2061:
            case 2062:
            case AnonymousClass1Y3.AH7 /*2067*/:
            case 2070:
            case 2075:
            case AnonymousClass1Y3.AHC /*2076*/:
            case 2077:
            case 2103:
            case 2104:
            case 2123:
            case 2124:
            case AnonymousClass1Y3.AHZ /*2125*/:
            case 2131:
            case AnonymousClass1Y3.AHg /*2138*/:
            case 2139:
            case 2140:
            case 2141:
            case 2142:
            case AnonymousClass1Y3.AHh /*2143*/:
            case 2160:
            case 2162:
            case 2172:
            case 2176:
            case AnonymousClass1Y3.AHv /*2177*/:
            case 2179:
            case 2180:
            case 2181:
            case 2182:
            case AnonymousClass1Y3.AHy /*2185*/:
            case AnonymousClass1Y3.AHz /*2186*/:
            case 2189:
            case AnonymousClass1Y3.AI1 /*2191*/:
            case 2205:
            case AnonymousClass1Y3.AI7 /*2209*/:
            case 2219:
            case 2255:
            case 2256:
            case 2258:
            case 2273:
            case AnonymousClass1Y3.AIe /*2275*/:
            case 2291:
            case 2295:
            case 2318:
            case AnonymousClass1Y3.AJ5 /*2319*/:
            case AnonymousClass1Y3.AJK /*2346*/:
            case 2351:
            case AnonymousClass1Y3.AJM /*2352*/:
            case AnonymousClass1Y3.AJP /*2357*/:
            case 2358:
            case AnonymousClass1Y3.AJS /*2365*/:
            case 2371:
            case AnonymousClass1Y3.AJY /*2373*/:
            case AnonymousClass1Y3.AKD /*2452*/:
            case 2453:
            case AnonymousClass1Y3.AKG /*2457*/:
            case AnonymousClass1Y3.AKM /*2468*/:
            case 2469:
            case AnonymousClass1Y3.AKN /*2470*/:
            case 2471:
            case 2472:
            case AnonymousClass1Y3.AKO /*2473*/:
            case AnonymousClass1Y3.AKV /*2483*/:
            case 2513:
            case 2514:
            case AnonymousClass1Y3.AKk /*2516*/:
            case 2517:
            case AnonymousClass1Y3.AKl /*2518*/:
            case AnonymousClass1Y3.AKq /*2529*/:
            case 2595:
            case AnonymousClass1Y3.ALP /*2602*/:
            case AnonymousClass1Y3.ALS /*2606*/:
            case 2607:
            case 2610:
            case 2611:
            case 2612:
            case 2904:
            case 2906:
            case AnonymousClass1Y3.ANt /*2907*/:
            case AnonymousClass1Y3.ANu /*2913*/:
            case AnonymousClass1Y3.ANv /*2918*/:
            case 2925:
            case AnonymousClass1Y3.AO1 /*2926*/:
            case AnonymousClass1Y3.AO2 /*2927*/:
            case AnonymousClass1Y3.AO8 /*2936*/:
            case 2939:
            case AnonymousClass1Y3.AOC /*2942*/:
            default:
                return 0;
            case 20:
            case AnonymousClass1Y3.A0A /*30*/:
            case 35:
            case AnonymousClass1Y3.A0w /*125*/:
            case AnonymousClass1Y3.A0y /*130*/:
            case AnonymousClass1Y3.A1J /*170*/:
            case AnonymousClass1Y3.A4t /*590*/:
            case AnonymousClass1Y3.A55 /*628*/:
            case 799:
            case 915:
            case 1276:
            case 1368:
            case 1784:
            case AnonymousClass1Y3.AEZ /*1785*/:
            case 1788:
            case AnonymousClass1Y3.AIt /*2302*/:
            case AnonymousClass1Y3.AIx /*2307*/:
            case 2348:
            case 2941:
                return 50;
            case AnonymousClass1Y3.A08 /*26*/:
            case AnonymousClass1Y3.A09 /*27*/:
                return 14400000;
            case 28:
            case AnonymousClass1Y3.A2R /*309*/:
                return 7200000;
            case 29:
                return 10800000;
            case AnonymousClass1Y3.A0B /*31*/:
                return 1800000;
            case 32:
                return 2700000;
            case AnonymousClass1Y3.A0C /*34*/:
            case AnonymousClass1Y3.A6E /*766*/:
            case AnonymousClass1Y3.AC1 /*1468*/:
            case AnonymousClass1Y3.ACJ /*1504*/:
            case 1773:
                return 900000;
            case 36:
            case AnonymousClass1Y3.A0D /*37*/:
                return 6144;
            case AnonymousClass1Y3.A0E /*38*/:
            case AnonymousClass1Y3.A0X /*75*/:
            case AnonymousClass1Y3.A0Y /*76*/:
            case 155:
            case AnonymousClass1Y3.A1I /*167*/:
            case AnonymousClass1Y3.A2Y /*320*/:
            case AnonymousClass1Y3.A3E /*395*/:
            case 477:
            case 479:
            case AnonymousClass1Y3.A41 /*492*/:
            case 745:
            case AnonymousClass1Y3.A9N /*1143*/:
            case AnonymousClass1Y3.A9X /*1160*/:
            case 1326:
            case 1771:
            case 1899:
            case AnonymousClass1Y3.AFn /*1926*/:
            case 1959:
            case AnonymousClass1Y3.AH3 /*2063*/:
            case AnonymousClass1Y3.AH5 /*2065*/:
            case 2072:
            case AnonymousClass1Y3.AIL /*2233*/:
            case AnonymousClass1Y3.AIw /*2305*/:
            case 2933:
                return 200;
            case AnonymousClass1Y3.A0F /*39*/:
            case 357:
            case AnonymousClass1Y3.A9x /*1220*/:
            case AnonymousClass1Y3.AF8 /*1853*/:
            case AnonymousClass1Y3.AF9 /*1854*/:
                return 360;
            case AnonymousClass1Y3.A0G /*40*/:
            case AnonymousClass1Y3.AFa /*1902*/:
                return 200000;
            case AnonymousClass1Y3.A0I /*44*/:
            case 69:
            case AnonymousClass1Y3.A0b /*81*/:
            case AnonymousClass1Y3.A0f /*94*/:
            case AnonymousClass1Y3.A0j /*102*/:
            case 108:
            case AnonymousClass1Y3.A12 /*136*/:
            case 158:
            case 209:
            case 391:
            case AnonymousClass1Y3.A3B /*392*/:
            case 417:
            case AnonymousClass1Y3.A3Z /*434*/:
            case 441:
            case AnonymousClass1Y3.A4M /*519*/:
            case 1060:
            case AnonymousClass1Y3.AAJ /*1261*/:
            case 1267:
            case 1914:
            case AnonymousClass1Y3.AIX /*2259*/:
            case 2264:
                return 30000;
            case AnonymousClass1Y3.A0J /*45*/:
            case 226:
            case 228:
            case 268:
            case AnonymousClass1Y3.A26 /*270*/:
            case AnonymousClass1Y3.A2C /*281*/:
            case AnonymousClass1Y3.A2T /*313*/:
            case AnonymousClass1Y3.A2d /*337*/:
            case AnonymousClass1Y3.A3b /*439*/:
            case AnonymousClass1Y3.A4P /*523*/:
            case 526:
            case AnonymousClass1Y3.A7y /*981*/:
            case AnonymousClass1Y3.A8c /*1057*/:
            case AnonymousClass1Y3.A8v /*1088*/:
            case 1164:
            case AnonymousClass1Y3.A9u /*1216*/:
            case 1254:
            case AnonymousClass1Y3.AAI /*1260*/:
            case AnonymousClass1Y3.AAe /*1298*/:
            case AnonymousClass1Y3.AAs /*1325*/:
            case 1334:
            case 1467:
            case 1682:
            case AnonymousClass1Y3.AET /*1769*/:
            case AnonymousClass1Y3.AF3 /*1842*/:
            case 2016:
            case AnonymousClass1Y3.AHB /*2074*/:
            case 2094:
            case 2105:
            case 2113:
            case AnonymousClass1Y3.AHY /*2122*/:
            case 2126:
            case AnonymousClass1Y3.AHa /*2127*/:
            case 2161:
            case AnonymousClass1Y3.AIK /*2231*/:
            case 2232:
            case 2234:
            case AnonymousClass1Y3.AIM /*2236*/:
            case AnonymousClass1Y3.AIf /*2276*/:
            case AnonymousClass1Y3.AIg /*2277*/:
            case AnonymousClass1Y3.AJ2 /*2313*/:
            case 2315:
            case AnonymousClass1Y3.AJ4 /*2317*/:
            case 2902:
            case AnonymousClass1Y3.ANz /*2923*/:
                return 1000;
            case AnonymousClass1Y3.A0M /*51*/:
            case AnonymousClass1Y3.A4k /*566*/:
            case 1774:
            case AnonymousClass1Y3.AI8 /*2210*/:
                return 14;
            case AnonymousClass1Y3.A0Q /*58*/:
            case 143:
            case 218:
            case AnonymousClass1Y3.A1k /*221*/:
            case 222:
            case 325:
            case AnonymousClass1Y3.A2c /*328*/:
            case AnonymousClass1Y3.A2v /*367*/:
            case AnonymousClass1Y3.A2w /*368*/:
            case AnonymousClass1Y3.A3N /*411*/:
            case 480:
            case 548:
            case AnonymousClass1Y3.A5G /*652*/:
            case AnonymousClass1Y3.A5r /*718*/:
            case AnonymousClass1Y3.A6I /*775*/:
            case AnonymousClass1Y3.A6P /*787*/:
            case 926:
            case 1005:
            case AnonymousClass1Y3.A8V /*1032*/:
            case 1042:
            case 1104:
            case AnonymousClass1Y3.A9L /*1141*/:
            case AnonymousClass1Y3.AA1 /*1224*/:
            case AnonymousClass1Y3.AA2 /*1227*/:
            case AnonymousClass1Y3.AAn /*1309*/:
            case 1310:
            case AnonymousClass1Y3.AAq /*1316*/:
            case 1317:
            case AnonymousClass1Y3.AAw /*1330*/:
            case AnonymousClass1Y3.AB1 /*1338*/:
            case AnonymousClass1Y3.AB3 /*1342*/:
            case AnonymousClass1Y3.AB4 /*1343*/:
            case AnonymousClass1Y3.ABF /*1371*/:
            case 1379:
            case AnonymousClass1Y3.ABd /*1413*/:
            case 1453:
            case AnonymousClass1Y3.AC4 /*1474*/:
            case AnonymousClass1Y3.AC5 /*1475*/:
            case 1482:
            case AnonymousClass1Y3.AC8 /*1485*/:
            case 1681:
            case 1712:
            case AnonymousClass1Y3.AEH /*1746*/:
            case AnonymousClass1Y3.AEI /*1747*/:
            case 1875:
            case 1913:
            case AnonymousClass1Y3.AG9 /*1966*/:
            case AnonymousClass1Y3.AHR /*2106*/:
            case AnonymousClass1Y3.AHV /*2116*/:
            case 2120:
            case AnonymousClass1Y3.AHm /*2155*/:
            case AnonymousClass1Y3.AIa /*2263*/:
            case AnonymousClass1Y3.AIi /*2279*/:
            case 2596:
            case AnonymousClass1Y3.ALM /*2597*/:
            case 2945:
                return 2;
            case 59:
            case AnonymousClass1Y3.A1s /*243*/:
            case 248:
            case AnonymousClass1Y3.A28 /*275*/:
            case AnonymousClass1Y3.A2E /*285*/:
            case AnonymousClass1Y3.A2p /*354*/:
            case AnonymousClass1Y3.A4O /*522*/:
            case 855:
            case AnonymousClass1Y3.A8b /*1056*/:
            case 1070:
            case AnonymousClass1Y3.A8s /*1083*/:
            case 1134:
            case AnonymousClass1Y3.A9P /*1147*/:
            case 1154:
            case 1296:
            case AnonymousClass1Y3.AAh /*1303*/:
            case 1318:
            case AnonymousClass1Y3.AAr /*1319*/:
            case AnonymousClass1Y3.AAy /*1332*/:
            case AnonymousClass1Y3.ABb /*1409*/:
            case 1438:
            case 1476:
            case 1479:
            case AnonymousClass1Y3.AC9 /*1486*/:
            case 1488:
            case 1530:
            case 1536:
            case 1537:
            case 1559:
            case 1696:
            case 1968:
            case AnonymousClass1Y3.AHQ /*2102*/:
            case 2153:
            case AnonymousClass1Y3.AHl /*2154*/:
            case AnonymousClass1Y3.AIv /*2304*/:
                return 3000;
            case AnonymousClass1Y3.A0R /*60*/:
            case AnonymousClass1Y3.A27 /*272*/:
            case AnonymousClass1Y3.A32 /*377*/:
            case 1107:
            case 1109:
            case AnonymousClass1Y3.AAm /*1308*/:
            case 2029:
            case AnonymousClass1Y3.AGk /*2030*/:
            case AnonymousClass1Y3.AGl /*2031*/:
            case 2091:
            case AnonymousClass1Y3.AHx /*2184*/:
            case 2196:
            case AnonymousClass1Y3.AI3 /*2197*/:
            case AnonymousClass1Y3.AI4 /*2198*/:
            case 2199:
                return 4000;
            case 61:
            case 66:
            case 88:
            case AnonymousClass1Y3.A1Q /*180*/:
            case AnonymousClass1Y3.A1V /*189*/:
            case AnonymousClass1Y3.A3a /*435*/:
            case 1323:
            case AnonymousClass1Y3.ABA /*1358*/:
                return 120000;
            case 62:
            case 115:
            case AnonymousClass1Y3.A1C /*153*/:
            case AnonymousClass1Y3.A1G /*162*/:
            case AnonymousClass1Y3.A1P /*177*/:
            case 238:
            case AnonymousClass1Y3.A21 /*257*/:
            case AnonymousClass1Y3.A30 /*373*/:
            case AnonymousClass1Y3.A3Q /*416*/:
            case 521:
            case AnonymousClass1Y3.A4R /*527*/:
            case AnonymousClass1Y3.A4T /*529*/:
            case AnonymousClass1Y3.A4x /*610*/:
            case 611:
            case 612:
            case AnonymousClass1Y3.A4y /*616*/:
            case 617:
            case 666:
            case 667:
            case 709:
            case 720:
            case 722:
            case 723:
            case AnonymousClass1Y3.A5x /*728*/:
            case AnonymousClass1Y3.A5y /*730*/:
            case 732:
            case 733:
            case AnonymousClass1Y3.A62 /*740*/:
            case 741:
            case AnonymousClass1Y3.A63 /*743*/:
            case AnonymousClass1Y3.A64 /*744*/:
            case 783:
            case 785:
            case AnonymousClass1Y3.A6Q /*788*/:
            case AnonymousClass1Y3.A6R /*789*/:
            case AnonymousClass1Y3.A6S /*794*/:
            case 802:
            case AnonymousClass1Y3.A6W /*803*/:
            case AnonymousClass1Y3.A6X /*804*/:
            case 805:
            case AnonymousClass1Y3.A6Y /*806*/:
            case AnonymousClass1Y3.A6Z /*807*/:
            case 808:
            case AnonymousClass1Y3.A6a /*809*/:
            case 810:
            case 811:
            case 812:
            case 813:
            case 814:
            case AnonymousClass1Y3.A6b /*815*/:
            case AnonymousClass1Y3.A6c /*816*/:
            case 817:
            case AnonymousClass1Y3.A6d /*818*/:
            case AnonymousClass1Y3.A6e /*819*/:
            case 820:
            case AnonymousClass1Y3.A6f /*821*/:
            case AnonymousClass1Y3.A6g /*822*/:
            case AnonymousClass1Y3.A6h /*823*/:
            case 824:
            case AnonymousClass1Y3.A6i /*825*/:
            case AnonymousClass1Y3.A6j /*826*/:
            case AnonymousClass1Y3.A6k /*827*/:
            case 828:
            case 829:
            case 830:
            case AnonymousClass1Y3.A6l /*831*/:
            case AnonymousClass1Y3.A6m /*832*/:
            case 834:
            case 835:
            case 836:
            case 837:
            case AnonymousClass1Y3.A6n /*838*/:
            case AnonymousClass1Y3.A6o /*839*/:
            case AnonymousClass1Y3.A6p /*840*/:
            case AnonymousClass1Y3.A6q /*841*/:
            case 842:
            case 843:
            case 844:
            case 845:
            case 846:
            case 847:
            case 848:
            case 849:
            case AnonymousClass1Y3.A71 /*872*/:
            case 880:
            case 881:
            case 882:
            case 883:
            case 884:
            case AnonymousClass1Y3.A78 /*885*/:
            case 886:
            case 887:
            case 888:
            case 889:
            case AnonymousClass1Y3.A79 /*890*/:
            case AnonymousClass1Y3.A7A /*891*/:
            case AnonymousClass1Y3.A7B /*892*/:
            case AnonymousClass1Y3.A7C /*893*/:
            case 896:
            case AnonymousClass1Y3.A7E /*897*/:
            case 898:
            case 899:
            case AnonymousClass1Y3.A7F /*900*/:
            case 901:
            case AnonymousClass1Y3.A7G /*902*/:
            case 903:
            case AnonymousClass1Y3.A7H /*904*/:
            case 905:
            case 906:
            case AnonymousClass1Y3.A7I /*907*/:
            case AnonymousClass1Y3.A7J /*908*/:
            case AnonymousClass1Y3.A7K /*909*/:
            case AnonymousClass1Y3.A7L /*910*/:
            case 911:
            case AnonymousClass1Y3.A7M /*912*/:
            case 913:
            case 914:
            case AnonymousClass1Y3.A7Y /*935*/:
            case AnonymousClass1Y3.A7l /*960*/:
            case 974:
            case 980:
            case AnonymousClass1Y3.A83 /*988*/:
            case 989:
            case 998:
            case AnonymousClass1Y3.A88 /*1003*/:
            case AnonymousClass1Y3.A89 /*1004*/:
            case AnonymousClass1Y3.A8A /*1006*/:
            case 1007:
            case AnonymousClass1Y3.A8h /*1065*/:
            case 1086:
            case AnonymousClass1Y3.A97 /*1110*/:
            case AnonymousClass1Y3.A9y /*1221*/:
            case AnonymousClass1Y3.AA4 /*1229*/:
            case 1240:
            case 1241:
            case AnonymousClass1Y3.AA8 /*1242*/:
            case 1243:
            case AnonymousClass1Y3.AAB /*1249*/:
            case AnonymousClass1Y3.AAL /*1264*/:
            case AnonymousClass1Y3.AAZ /*1290*/:
            case 1387:
            case 1388:
            case 1399:
            case AnonymousClass1Y3.ABh /*1420*/:
            case 1427:
            case AnonymousClass1Y3.ABm /*1432*/:
            case 1433:
            case 1503:
            case 1507:
            case 1548:
            case AnonymousClass1Y3.ACh /*1550*/:
            case AnonymousClass1Y3.ADU /*1658*/:
            case 1688:
            case AnonymousClass1Y3.AE5 /*1728*/:
            case AnonymousClass1Y3.AEQ /*1764*/:
            case AnonymousClass1Y3.AEd /*1790*/:
            case AnonymousClass1Y3.AEe /*1791*/:
            case 1801:
            case 1802:
            case AnonymousClass1Y3.AEj /*1803*/:
            case AnonymousClass1Y3.AEk /*1804*/:
            case 1805:
            case AnonymousClass1Y3.AEl /*1806*/:
            case AnonymousClass1Y3.AEn /*1808*/:
            case AnonymousClass1Y3.AFj /*1918*/:
            case 1932:
            case AnonymousClass1Y3.AFs /*1934*/:
            case AnonymousClass1Y3.AFt /*1935*/:
            case 1937:
            case AnonymousClass1Y3.AFv /*1938*/:
            case AnonymousClass1Y3.AFw /*1939*/:
            case 1940:
            case 1957:
            case AnonymousClass1Y3.AG7 /*1958*/:
            case AnonymousClass1Y3.AGA /*1967*/:
            case 2092:
            case 2108:
            case 2144:
            case 2145:
            case 2147:
            case AnonymousClass1Y3.AHj /*2148*/:
            case AnonymousClass1Y3.AHk /*2149*/:
            case 2175:
            case AnonymousClass1Y3.AI0 /*2190*/:
            case 2203:
            case 2204:
            case AnonymousClass1Y3.AIH /*2227*/:
            case 2237:
            case AnonymousClass1Y3.AIZ /*2262*/:
            case 2297:
            case 2327:
            case 2328:
            case AnonymousClass1Y3.AJ9 /*2329*/:
            case AnonymousClass1Y3.AJA /*2330*/:
            case AnonymousClass1Y3.AJB /*2331*/:
            case AnonymousClass1Y3.AJC /*2332*/:
            case 2333:
            case AnonymousClass1Y3.AJD /*2334*/:
            case 2335:
            case AnonymousClass1Y3.AJE /*2336*/:
            case AnonymousClass1Y3.AJF /*2337*/:
            case 2338:
            case AnonymousClass1Y3.AJG /*2339*/:
            case AnonymousClass1Y3.AJH /*2340*/:
            case AnonymousClass1Y3.AJI /*2341*/:
            case AnonymousClass1Y3.AJJ /*2342*/:
            case 2343:
            case 2344:
            case 2359:
            case AnonymousClass1Y3.AJT /*2366*/:
            case AnonymousClass1Y3.AJU /*2367*/:
            case 2368:
            case AnonymousClass1Y3.AJV /*2369*/:
            case AnonymousClass1Y3.AJW /*2370*/:
            case AnonymousClass1Y3.AJX /*2372*/:
            case AnonymousClass1Y3.AJZ /*2374*/:
            case AnonymousClass1Y3.AJa /*2375*/:
            case AnonymousClass1Y3.AJb /*2376*/:
            case AnonymousClass1Y3.AJc /*2377*/:
            case 2378:
            case 2379:
            case AnonymousClass1Y3.AJd /*2380*/:
            case 2381:
            case AnonymousClass1Y3.AJe /*2382*/:
            case AnonymousClass1Y3.AJf /*2383*/:
            case AnonymousClass1Y3.AJg /*2384*/:
            case 2385:
            case AnonymousClass1Y3.AJh /*2386*/:
            case AnonymousClass1Y3.AJi /*2387*/:
            case 2388:
            case 2389:
            case 2390:
            case AnonymousClass1Y3.AJj /*2391*/:
            case AnonymousClass1Y3.AJk /*2392*/:
            case 2393:
            case 2394:
            case 2395:
            case AnonymousClass1Y3.AJl /*2396*/:
            case AnonymousClass1Y3.AJm /*2397*/:
            case 2398:
            case 2401:
            case 2402:
            case 2403:
            case AnonymousClass1Y3.AJn /*2404*/:
            case 2405:
            case 2406:
            case 2407:
            case AnonymousClass1Y3.AJo /*2408*/:
            case AnonymousClass1Y3.AJp /*2409*/:
            case AnonymousClass1Y3.AJq /*2410*/:
            case AnonymousClass1Y3.AJr /*2411*/:
            case AnonymousClass1Y3.AJs /*2412*/:
            case AnonymousClass1Y3.AJt /*2413*/:
            case 2414:
            case AnonymousClass1Y3.AJu /*2415*/:
            case 2416:
            case 2417:
            case 2418:
            case 2419:
            case AnonymousClass1Y3.AJv /*2420*/:
            case AnonymousClass1Y3.AJw /*2421*/:
            case AnonymousClass1Y3.AJx /*2422*/:
            case 2423:
            case AnonymousClass1Y3.AJy /*2424*/:
            case AnonymousClass1Y3.AJz /*2425*/:
            case 2426:
            case AnonymousClass1Y3.AK0 /*2427*/:
            case 2428:
            case 2429:
            case AnonymousClass1Y3.AK1 /*2430*/:
            case 2431:
            case 2432:
            case 2433:
            case AnonymousClass1Y3.AK2 /*2434*/:
            case AnonymousClass1Y3.AK3 /*2435*/:
            case 2436:
            case AnonymousClass1Y3.AK4 /*2437*/:
            case AnonymousClass1Y3.AK5 /*2438*/:
            case AnonymousClass1Y3.AK6 /*2439*/:
            case AnonymousClass1Y3.AK7 /*2440*/:
            case 2441:
            case AnonymousClass1Y3.AK8 /*2442*/:
            case AnonymousClass1Y3.AKA /*2445*/:
            case AnonymousClass1Y3.AKB /*2446*/:
            case 2447:
            case 2450:
            case 2451:
            case AnonymousClass1Y3.AKE /*2454*/:
            case AnonymousClass1Y3.AKF /*2455*/:
            case 2456:
            case AnonymousClass1Y3.AKH /*2458*/:
            case 2459:
            case AnonymousClass1Y3.AKI /*2460*/:
            case 2461:
            case AnonymousClass1Y3.AKJ /*2462*/:
            case 2463:
            case 2464:
            case AnonymousClass1Y3.AKK /*2465*/:
            case AnonymousClass1Y3.AKL /*2466*/:
            case 2467:
            case AnonymousClass1Y3.AKP /*2474*/:
            case 2475:
            case 2478:
            case 2479:
            case AnonymousClass1Y3.AKS /*2480*/:
            case AnonymousClass1Y3.AKT /*2481*/:
            case AnonymousClass1Y3.AKU /*2482*/:
            case AnonymousClass1Y3.AKW /*2484*/:
            case 2485:
            case 2486:
            case 2487:
            case AnonymousClass1Y3.AKX /*2488*/:
            case AnonymousClass1Y3.AKY /*2489*/:
            case AnonymousClass1Y3.AKZ /*2490*/:
            case 2491:
            case AnonymousClass1Y3.AKa /*2492*/:
            case AnonymousClass1Y3.AKb /*2493*/:
            case 2494:
            case 2496:
            case 2497:
            case 2498:
            case AnonymousClass1Y3.AKc /*2499*/:
            case AnonymousClass1Y3.AKd /*2500*/:
            case 2501:
            case AnonymousClass1Y3.AKe /*2502*/:
            case 2503:
            case AnonymousClass1Y3.AKf /*2504*/:
            case 2505:
            case 2506:
            case 2507:
            case AnonymousClass1Y3.AKg /*2508*/:
            case AnonymousClass1Y3.AKh /*2509*/:
            case AnonymousClass1Y3.AKi /*2510*/:
            case AnonymousClass1Y3.AKj /*2515*/:
            case AnonymousClass1Y3.AKm /*2519*/:
            case 2520:
            case 2521:
            case 2522:
            case AnonymousClass1Y3.AKn /*2523*/:
            case 2524:
            case 2525:
            case AnonymousClass1Y3.AKo /*2526*/:
            case AnonymousClass1Y3.AKp /*2527*/:
            case 2528:
            case AnonymousClass1Y3.AKr /*2530*/:
            case AnonymousClass1Y3.AKs /*2531*/:
            case 2532:
            case AnonymousClass1Y3.AKt /*2533*/:
            case AnonymousClass1Y3.AKu /*2534*/:
            case 2535:
            case AnonymousClass1Y3.AKv /*2536*/:
            case AnonymousClass1Y3.AKw /*2537*/:
            case AnonymousClass1Y3.AKx /*2538*/:
            case 2539:
            case 2540:
            case 2541:
            case 2542:
            case 2543:
            case AnonymousClass1Y3.AKy /*2544*/:
            case AnonymousClass1Y3.AKz /*2545*/:
            case 2546:
            case 2547:
            case AnonymousClass1Y3.AL0 /*2548*/:
            case AnonymousClass1Y3.AL1 /*2549*/:
            case AnonymousClass1Y3.AL2 /*2550*/:
            case AnonymousClass1Y3.AL3 /*2551*/:
            case AnonymousClass1Y3.AL4 /*2552*/:
            case 2553:
            case AnonymousClass1Y3.AL5 /*2554*/:
            case 2555:
            case 2556:
            case AnonymousClass1Y3.AL6 /*2557*/:
            case 2558:
            case 2559:
            case 2560:
            case 2561:
            case 2562:
            case 2563:
            case AnonymousClass1Y3.AL7 /*2564*/:
            case 2565:
            case AnonymousClass1Y3.AL8 /*2566*/:
            case 2567:
            case AnonymousClass1Y3.AL9 /*2568*/:
            case 2569:
            case 2570:
            case 2571:
            case AnonymousClass1Y3.ALA /*2573*/:
            case AnonymousClass1Y3.ALB /*2574*/:
            case AnonymousClass1Y3.ALC /*2575*/:
            case AnonymousClass1Y3.ALD /*2576*/:
            case AnonymousClass1Y3.ALE /*2577*/:
            case 2578:
            case AnonymousClass1Y3.ALF /*2580*/:
            case AnonymousClass1Y3.ALG /*2581*/:
            case AnonymousClass1Y3.ALH /*2582*/:
            case AnonymousClass1Y3.ALI /*2583*/:
            case AnonymousClass1Y3.ALJ /*2584*/:
            case AnonymousClass1Y3.ALK /*2585*/:
            case 2586:
            case 2587:
            case 2588:
            case 2589:
            case 2590:
            case 2591:
            case 2592:
            case 2593:
            case AnonymousClass1Y3.ALL /*2594*/:
            case 2616:
            case 2617:
            case 2618:
            case 2619:
            case AnonymousClass1Y3.ALW /*2620*/:
            case 2621:
            case 2622:
            case 2623:
            case AnonymousClass1Y3.ALX /*2624*/:
            case AnonymousClass1Y3.ALY /*2625*/:
            case 2626:
            case 2627:
            case AnonymousClass1Y3.ALZ /*2628*/:
            case AnonymousClass1Y3.ALa /*2629*/:
            case AnonymousClass1Y3.ALb /*2630*/:
            case 2631:
            case AnonymousClass1Y3.ALc /*2632*/:
            case 2633:
            case AnonymousClass1Y3.ALd /*2634*/:
            case 2635:
            case AnonymousClass1Y3.ALe /*2636*/:
            case AnonymousClass1Y3.ALf /*2637*/:
            case AnonymousClass1Y3.ALg /*2638*/:
            case AnonymousClass1Y3.ALh /*2639*/:
            case AnonymousClass1Y3.ALi /*2640*/:
            case 2641:
            case 2642:
            case AnonymousClass1Y3.ALj /*2643*/:
            case 2644:
            case 2645:
            case 2646:
            case 2647:
            case AnonymousClass1Y3.ALk /*2648*/:
            case AnonymousClass1Y3.ALl /*2649*/:
            case 2650:
            case 2651:
            case 2652:
            case AnonymousClass1Y3.ALm /*2653*/:
            case 2654:
            case AnonymousClass1Y3.ALn /*2655*/:
            case 2656:
            case AnonymousClass1Y3.ALo /*2657*/:
            case AnonymousClass1Y3.ALp /*2658*/:
            case 2659:
            case AnonymousClass1Y3.ALq /*2660*/:
            case AnonymousClass1Y3.ALr /*2661*/:
            case 2662:
            case AnonymousClass1Y3.ALs /*2663*/:
            case AnonymousClass1Y3.ALt /*2664*/:
            case AnonymousClass1Y3.ALu /*2665*/:
            case 2666:
            case 2667:
            case AnonymousClass1Y3.ALv /*2668*/:
            case AnonymousClass1Y3.ALw /*2669*/:
            case 2670:
            case AnonymousClass1Y3.ALx /*2671*/:
            case 2672:
            case AnonymousClass1Y3.ALy /*2673*/:
            case 2674:
            case 2675:
            case 2676:
            case 2677:
            case 2678:
            case 2679:
            case AnonymousClass1Y3.ALz /*2680*/:
            case AnonymousClass1Y3.AM0 /*2681*/:
            case AnonymousClass1Y3.AM1 /*2682*/:
            case 2683:
            case 2684:
            case AnonymousClass1Y3.AM2 /*2685*/:
            case AnonymousClass1Y3.AM3 /*2686*/:
            case 2687:
            case 2688:
            case 2689:
            case 2690:
            case AnonymousClass1Y3.AM4 /*2691*/:
            case AnonymousClass1Y3.AM5 /*2692*/:
            case 2693:
            case AnonymousClass1Y3.AM6 /*2694*/:
            case AnonymousClass1Y3.AM7 /*2695*/:
            case AnonymousClass1Y3.AM8 /*2696*/:
            case AnonymousClass1Y3.AM9 /*2697*/:
            case AnonymousClass1Y3.AMA /*2698*/:
            case 2699:
            case AnonymousClass1Y3.AMB /*2700*/:
            case AnonymousClass1Y3.AMC /*2701*/:
            case AnonymousClass1Y3.AMD /*2702*/:
            case AnonymousClass1Y3.AME /*2703*/:
            case AnonymousClass1Y3.AMF /*2704*/:
            case 2705:
            case 2706:
            case 2707:
            case 2708:
            case 2709:
            case 2710:
            case AnonymousClass1Y3.AMG /*2711*/:
            case AnonymousClass1Y3.AMH /*2712*/:
            case AnonymousClass1Y3.AMI /*2713*/:
            case AnonymousClass1Y3.AMJ /*2714*/:
            case AnonymousClass1Y3.AMK /*2715*/:
            case 2716:
            case 2717:
            case 2718:
            case AnonymousClass1Y3.AML /*2719*/:
            case AnonymousClass1Y3.AMM /*2720*/:
            case 2721:
            case AnonymousClass1Y3.AMN /*2722*/:
            case AnonymousClass1Y3.AMO /*2723*/:
            case AnonymousClass1Y3.AMP /*2724*/:
            case AnonymousClass1Y3.AMQ /*2725*/:
            case 2726:
            case AnonymousClass1Y3.AMR /*2727*/:
            case 2728:
            case AnonymousClass1Y3.AMS /*2729*/:
            case AnonymousClass1Y3.AMT /*2730*/:
            case 2731:
            case 2732:
            case 2733:
            case 2734:
            case AnonymousClass1Y3.AMU /*2735*/:
            case 2736:
            case 2737:
            case AnonymousClass1Y3.AMV /*2738*/:
            case AnonymousClass1Y3.AMW /*2739*/:
            case AnonymousClass1Y3.AMX /*2740*/:
            case AnonymousClass1Y3.AMY /*2741*/:
            case AnonymousClass1Y3.AMZ /*2742*/:
            case AnonymousClass1Y3.AMa /*2743*/:
            case AnonymousClass1Y3.AMb /*2744*/:
            case 2745:
            case AnonymousClass1Y3.AMc /*2746*/:
            case 2747:
            case 2748:
            case 2749:
            case AnonymousClass1Y3.AMd /*2750*/:
            case AnonymousClass1Y3.AMe /*2751*/:
            case 2752:
            case 2753:
            case AnonymousClass1Y3.AMf /*2754*/:
            case AnonymousClass1Y3.AMg /*2755*/:
            case AnonymousClass1Y3.AMh /*2756*/:
            case AnonymousClass1Y3.AMi /*2757*/:
            case AnonymousClass1Y3.AMj /*2758*/:
            case 2759:
            case AnonymousClass1Y3.AMk /*2760*/:
            case 2761:
            case AnonymousClass1Y3.AMl /*2762*/:
            case 2763:
            case AnonymousClass1Y3.AMm /*2764*/:
            case AnonymousClass1Y3.AMn /*2765*/:
            case AnonymousClass1Y3.AMo /*2766*/:
            case 2767:
            case AnonymousClass1Y3.AMp /*2768*/:
            case 2769:
            case 2770:
            case 2771:
            case 2772:
            case 2773:
            case 2774:
            case 2775:
            case 2776:
            case 2777:
            case AnonymousClass1Y3.AMq /*2778*/:
            case AnonymousClass1Y3.AMr /*2779*/:
            case AnonymousClass1Y3.AMs /*2780*/:
            case 2781:
            case AnonymousClass1Y3.AMt /*2782*/:
            case AnonymousClass1Y3.AMu /*2783*/:
            case 2784:
            case AnonymousClass1Y3.AMv /*2785*/:
            case 2786:
            case AnonymousClass1Y3.AMw /*2787*/:
            case 2788:
            case 2789:
            case 2790:
            case AnonymousClass1Y3.AMx /*2791*/:
            case AnonymousClass1Y3.AMy /*2792*/:
            case 2793:
            case 2794:
            case AnonymousClass1Y3.AMz /*2795*/:
            case AnonymousClass1Y3.AN0 /*2796*/:
            case 2797:
            case AnonymousClass1Y3.AN1 /*2798*/:
            case AnonymousClass1Y3.AN2 /*2799*/:
            case 2800:
            case AnonymousClass1Y3.AN3 /*2801*/:
            case 2802:
            case 2803:
            case 2804:
            case 2805:
            case AnonymousClass1Y3.AN4 /*2806*/:
            case 2807:
            case 2808:
            case AnonymousClass1Y3.AN5 /*2809*/:
            case AnonymousClass1Y3.AN6 /*2810*/:
            case 2811:
            case AnonymousClass1Y3.AN7 /*2812*/:
            case 2813:
            case AnonymousClass1Y3.AN8 /*2814*/:
            case 2815:
            case 2816:
            case 2817:
            case AnonymousClass1Y3.AN9 /*2818*/:
            case 2819:
            case 2820:
            case AnonymousClass1Y3.ANA /*2821*/:
            case AnonymousClass1Y3.ANB /*2822*/:
            case 2823:
            case 2824:
            case AnonymousClass1Y3.ANC /*2825*/:
            case AnonymousClass1Y3.AND /*2826*/:
            case 2827:
            case 2828:
            case AnonymousClass1Y3.ANE /*2829*/:
            case AnonymousClass1Y3.ANF /*2830*/:
            case 2831:
            case 2832:
            case AnonymousClass1Y3.ANG /*2833*/:
            case AnonymousClass1Y3.ANH /*2834*/:
            case AnonymousClass1Y3.ANI /*2835*/:
            case AnonymousClass1Y3.ANJ /*2836*/:
            case 2837:
            case AnonymousClass1Y3.ANK /*2838*/:
            case 2839:
            case AnonymousClass1Y3.ANL /*2840*/:
            case AnonymousClass1Y3.ANM /*2841*/:
            case AnonymousClass1Y3.ANN /*2842*/:
            case 2843:
            case 2844:
            case AnonymousClass1Y3.ANO /*2845*/:
            case 2846:
            case 2847:
            case 2848:
            case 2849:
            case 2850:
            case 2851:
            case 2852:
            case AnonymousClass1Y3.ANP /*2853*/:
            case AnonymousClass1Y3.ANQ /*2854*/:
            case 2855:
            case AnonymousClass1Y3.ANR /*2856*/:
            case 2857:
            case AnonymousClass1Y3.ANS /*2858*/:
            case 2859:
            case 2860:
            case 2861:
            case AnonymousClass1Y3.ANT /*2862*/:
            case AnonymousClass1Y3.ANU /*2863*/:
            case 2864:
            case AnonymousClass1Y3.ANV /*2865*/:
            case AnonymousClass1Y3.ANW /*2866*/:
            case AnonymousClass1Y3.ANX /*2867*/:
            case AnonymousClass1Y3.ANY /*2868*/:
            case AnonymousClass1Y3.ANZ /*2869*/:
            case AnonymousClass1Y3.ANa /*2870*/:
            case AnonymousClass1Y3.ANb /*2871*/:
            case 2872:
            case 2873:
            case AnonymousClass1Y3.ANc /*2874*/:
            case 2875:
            case 2876:
            case 2877:
            case AnonymousClass1Y3.ANd /*2878*/:
            case AnonymousClass1Y3.ANe /*2879*/:
            case AnonymousClass1Y3.ANf /*2880*/:
            case 2881:
            case AnonymousClass1Y3.ANg /*2882*/:
            case AnonymousClass1Y3.ANh /*2883*/:
            case AnonymousClass1Y3.ANi /*2884*/:
            case 2885:
            case 2886:
            case AnonymousClass1Y3.ANj /*2887*/:
            case AnonymousClass1Y3.ANk /*2888*/:
            case AnonymousClass1Y3.ANl /*2889*/:
            case AnonymousClass1Y3.ANm /*2890*/:
            case 2891:
            case 2892:
            case 2893:
            case AnonymousClass1Y3.ANn /*2894*/:
            case AnonymousClass1Y3.ANo /*2895*/:
            case AnonymousClass1Y3.ANp /*2896*/:
            case AnonymousClass1Y3.ANq /*2897*/:
            case AnonymousClass1Y3.ANr /*2898*/:
            case 2899:
            case AnonymousClass1Y3.ANs /*2900*/:
            case 2916:
            case 2917:
            case AnonymousClass1Y3.AOF /*2947*/:
            case 2948:
            case AnonymousClass1Y3.AOG /*2949*/:
            case AnonymousClass1Y3.AOH /*2950*/:
            case 2951:
            case AnonymousClass1Y3.AOI /*2952*/:
            case AnonymousClass1Y3.AOJ /*2953*/:
            case AnonymousClass1Y3.AOK /*2954*/:
            case 2955:
            case AnonymousClass1Y3.AOL /*2956*/:
            case 2957:
            case 2958:
            case AnonymousClass1Y3.AOM /*2959*/:
            case 2960:
            case 2961:
            case AnonymousClass1Y3.AON /*2962*/:
            case AnonymousClass1Y3.AOO /*2964*/:
            case 2966:
            case 2967:
            case 2968:
            case AnonymousClass1Y3.AOQ /*2969*/:
            case 2970:
            case AnonymousClass1Y3.AOR /*2971*/:
            case AnonymousClass1Y3.AOS /*2972*/:
            case 2973:
            case 2974:
            case 2975:
            case 2976:
            case AnonymousClass1Y3.AOT /*2977*/:
            case AnonymousClass1Y3.AOU /*2978*/:
            case 2979:
            case AnonymousClass1Y3.AOV /*2980*/:
                return -1;
            case AnonymousClass1Y3.A0S /*63*/:
                return 480000;
            case 64:
            case AnonymousClass1Y3.A13 /*138*/:
            case 145:
            case 146:
            case AnonymousClass1Y3.A18 /*147*/:
            case AnonymousClass1Y3.A19 /*148*/:
            case AnonymousClass1Y3.A1A /*149*/:
            case AnonymousClass1Y3.A1B /*150*/:
            case 151:
            case AnonymousClass1Y3.A1N /*175*/:
            case AnonymousClass1Y3.A2e /*340*/:
            case AnonymousClass1Y3.A2f /*341*/:
            case AnonymousClass1Y3.A2o /*353*/:
            case AnonymousClass1Y3.A3O /*412*/:
            case 432:
            case 485:
            case 486:
            case AnonymousClass1Y3.A4D /*506*/:
            case 551:
            case AnonymousClass1Y3.A5I /*654*/:
            case AnonymousClass1Y3.A61 /*738*/:
            case AnonymousClass1Y3.A6A /*758*/:
            case AnonymousClass1Y3.A6O /*786*/:
            case AnonymousClass1Y3.A6U /*800*/:
            case AnonymousClass1Y3.A6w /*865*/:
            case 1140:
            case 1195:
            case AnonymousClass1Y3.A9p /*1206*/:
            case AnonymousClass1Y3.AAO /*1269*/:
            case AnonymousClass1Y3.AAU /*1279*/:
            case 1369:
            case AnonymousClass1Y3.ABE /*1370*/:
            case 1417:
            case AnonymousClass1Y3.ADm /*1692*/:
            case 1732:
            case 1767:
            case 1852:
            case AnonymousClass1Y3.AFA /*1855*/:
            case AnonymousClass1Y3.AFl /*1923*/:
            case AnonymousClass1Y3.AH0 /*2055*/:
            case AnonymousClass1Y3.AIk /*2286*/:
            case 2301:
            case 2345:
            case 2604:
            case AnonymousClass1Y3.ALV /*2615*/:
            case AnonymousClass1Y3.AO0 /*2924*/:
                return 100;
            case AnonymousClass1Y3.A0V /*71*/:
            case 1169:
            case AnonymousClass1Y3.A9Z /*1170*/:
                return 7200;
            case AnonymousClass1Y3.A0W /*73*/:
            case AnonymousClass1Y3.A0g /*96*/:
            case AnonymousClass1Y3.A0r /*117*/:
            case 168:
            case AnonymousClass1Y3.A2O /*303*/:
            case 304:
            case AnonymousClass1Y3.A2s /*361*/:
            case 798:
            case AnonymousClass1Y3.AHp /*2159*/:
            case 2206:
            case 2216:
            case 2217:
            case AnonymousClass1Y3.AIO /*2239*/:
            case 2911:
                return 600;
            case 74:
            case AnonymousClass1Y3.A0p /*110*/:
            case 356:
            case AnonymousClass1Y3.A4F /*510*/:
            case 1499:
            case AnonymousClass1Y3.ACV /*1524*/:
            case AnonymousClass1Y3.ADr /*1701*/:
            case 1703:
            case AnonymousClass1Y3.AHX /*2119*/:
            case AnonymousClass1Y3.AI9 /*2211*/:
            case 2915:
                return 3600;
            case 78:
            case AnonymousClass1Y3.A10 /*133*/:
            case AnonymousClass1Y3.A15 /*141*/:
            case AnonymousClass1Y3.A53 /*626*/:
            case AnonymousClass1Y3.ABB /*1359*/:
                return 600000;
            case 79:
            case 134:
            case AnonymousClass1Y3.A2a /*324*/:
                return 45000;
            case AnonymousClass1Y3.A0a /*80*/:
            case 86:
            case 129:
            case AnonymousClass1Y3.A11 /*135*/:
            case AnonymousClass1Y3.A17 /*144*/:
            case 159:
            case AnonymousClass1Y3.A1M /*174*/:
            case 178:
            case 488:
            case AnonymousClass1Y3.A8g /*1064*/:
            case 1102:
            case 1324:
            case AnonymousClass1Y3.ABT /*1400*/:
            case 1532:
            case AnonymousClass1Y3.AEu /*1822*/:
            case 1857:
            case 2167:
            case AnonymousClass1Y3.AO9 /*2937*/:
                return 300000;
            case 85:
            case 103:
            case AnonymousClass1Y3.A5R /*674*/:
            case AnonymousClass1Y3.ABW /*1403*/:
                return 3600000;
            case AnonymousClass1Y3.A0d /*89*/:
            case AnonymousClass1Y3.AIq /*2296*/:
                return 14400;
            case 95:
            case AnonymousClass1Y3.A0k /*104*/:
            case AnonymousClass1Y3.A0o /*109*/:
            case 137:
            case 160:
            case 463:
            case AnonymousClass1Y3.A4N /*520*/:
            case 524:
            case AnonymousClass1Y3.A4Q /*525*/:
            case 1866:
                return 20000;
            case 97:
            case AnonymousClass1Y3.AIG /*2226*/:
                return 96;
            case 99:
            case AnonymousClass1Y3.A0z /*131*/:
            case 205:
            case AnonymousClass1Y3.A1j /*219*/:
            case AnonymousClass1Y3.A1l /*225*/:
            case 230:
            case AnonymousClass1Y3.A2K /*294*/:
            case 316:
            case 333:
            case 413:
            case AnonymousClass1Y3.A3P /*414*/:
            case AnonymousClass1Y3.A3w /*474*/:
            case 481:
            case AnonymousClass1Y3.A4C /*505*/:
            case AnonymousClass1Y3.A4e /*549*/:
            case AnonymousClass1Y3.A4n /*573*/:
            case AnonymousClass1Y3.A4q /*581*/:
            case 622:
            case 629:
            case AnonymousClass1Y3.A58 /*635*/:
            case AnonymousClass1Y3.A5B /*639*/:
            case 656:
            case AnonymousClass1Y3.A5J /*657*/:
            case AnonymousClass1Y3.A5c /*694*/:
            case AnonymousClass1Y3.A5j /*703*/:
            case 704:
            case AnonymousClass1Y3.A5m /*708*/:
            case 710:
            case 724:
            case 739:
            case AnonymousClass1Y3.A6t /*861*/:
            case AnonymousClass1Y3.A74 /*875*/:
            case AnonymousClass1Y3.A7V /*931*/:
            case AnonymousClass1Y3.A7c /*940*/:
            case 942:
            case 947:
            case AnonymousClass1Y3.A8B /*1008*/:
            case 1014:
            case 1046:
            case AnonymousClass1Y3.A8j /*1068*/:
            case AnonymousClass1Y3.A8w /*1089*/:
            case 1165:
            case AnonymousClass1Y3.A9g /*1191*/:
            case AnonymousClass1Y3.A9k /*1199*/:
            case AnonymousClass1Y3.A9l /*1200*/:
            case 1201:
            case AnonymousClass1Y3.A9m /*1202*/:
            case AnonymousClass1Y3.A9n /*1203*/:
            case AnonymousClass1Y3.A9o /*1204*/:
            case 1205:
            case 1217:
            case AnonymousClass1Y3.AAD /*1253*/:
            case 1289:
            case 1297:
            case 1345:
            case 1364:
            case AnonymousClass1Y3.ABc /*1411*/:
            case 1434:
            case AnonymousClass1Y3.ABq /*1446*/:
            case 1593:
            case 1594:
            case AnonymousClass1Y3.ADo /*1694*/:
            case AnonymousClass1Y3.AE3 /*1721*/:
            case 1756:
            case 1843:
            case 1858:
            case AnonymousClass1Y3.AFJ /*1873*/:
            case AnonymousClass1Y3.AFm /*1924*/:
            case AnonymousClass1Y3.AGD /*1971*/:
            case 2036:
            case AnonymousClass1Y3.AH8 /*2068*/:
            case AnonymousClass1Y3.AHA /*2073*/:
            case 2121:
            case 2290:
            case 2320:
            case AnonymousClass1Y3.AJ7 /*2325*/:
            case 2909:
            case 2910:
                return 10;
            case AnonymousClass1Y3.A0i /*101*/:
            case AnonymousClass1Y3.A0t /*120*/:
            case AnonymousClass1Y3.A2W /*318*/:
            case AnonymousClass1Y3.ABX /*1404*/:
                return 900;
            case AnonymousClass1Y3.A0l /*105*/:
            case 321:
            case 332:
            case 347:
            case 424:
            case 607:
            case AnonymousClass1Y3.A4w /*608*/:
            case AnonymousClass1Y3.A5D /*646*/:
            case 688:
            case 715:
            case 717:
            case 795:
            case 965:
            case 1127:
            case 1175:
            case AnonymousClass1Y3.A9b /*1178*/:
            case 1208:
            case 1210:
            case AnonymousClass1Y3.A9r /*1211*/:
            case 1362:
            case AnonymousClass1Y3.ADf /*1679*/:
            case AnonymousClass1Y3.ADi /*1685*/:
            case AnonymousClass1Y3.AES /*1768*/:
            case AnonymousClass1Y3.AIh /*2278*/:
            case 2284:
            case 2292:
            case AnonymousClass1Y3.AOB /*2940*/:
                return 20;
            case AnonymousClass1Y3.A0q /*113*/:
                return 8640;
            case 114:
            case AnonymousClass1Y3.A1y /*250*/:
            case 339:
            case AnonymousClass1Y3.A3R /*420*/:
            case AnonymousClass1Y3.A4d /*544*/:
            case AnonymousClass1Y3.A8f /*1063*/:
            case AnonymousClass1Y3.A9Q /*1150*/:
            case AnonymousClass1Y3.AAg /*1300*/:
            case 1356:
            case 2038:
                return 15000;
            case 116:
            case AnonymousClass1Y3.A0s /*119*/:
            case AnonymousClass1Y3.A1R /*181*/:
            case 299:
            case 606:
            case 966:
            case AnonymousClass1Y3.AIr /*2298*/:
            case AnonymousClass1Y3.AJ6 /*2323*/:
                return 1800;
            case 118:
            case 124:
            case 152:
            case 231:
            case 335:
            case AnonymousClass1Y3.A4S /*528*/:
            case 561:
            case 576:
            case AnonymousClass1Y3.A5Q /*673*/:
            case AnonymousClass1Y3.A8H /*1016*/:
            case AnonymousClass1Y3.A8r /*1078*/:
            case AnonymousClass1Y3.A8x /*1090*/:
            case 1376:
            case 1424:
            case AnonymousClass1Y3.ADn /*1693*/:
            case AnonymousClass1Y3.AEU /*1775*/:
            case AnonymousClass1Y3.AEW /*1779*/:
            case AnonymousClass1Y3.AGO /*1991*/:
            case AnonymousClass1Y3.AGP /*1992*/:
            case AnonymousClass1Y3.AGQ /*1993*/:
            case AnonymousClass1Y3.AGR /*1994*/:
            case AnonymousClass1Y3.AGS /*1995*/:
            case 1996:
            case AnonymousClass1Y3.AGV /*2003*/:
            case 2004:
            case 2005:
            case AnonymousClass1Y3.AGW /*2006*/:
            case AnonymousClass1Y3.AGX /*2007*/:
            case 2008:
            case AnonymousClass1Y3.AGe /*2017*/:
            case AnonymousClass1Y3.AHS /*2107*/:
            case 2213:
            case 2324:
            case 2922:
                return 500;
            case AnonymousClass1Y3.A0u /*121*/:
            case 859:
            case AnonymousClass1Y3.AE1 /*1719*/:
                return 172800;
            case AnonymousClass1Y3.A0x /*126*/:
                return 180000;
            case 128:
                return 420000;
            case 132:
                return -85;
            case 139:
            case 165:
            case AnonymousClass1Y3.A4g /*559*/:
            case AnonymousClass1Y3.A4h /*560*/:
            case 1197:
            case 1235:
            case AnonymousClass1Y3.ADw /*1713*/:
            case 1731:
                return 25;
            case AnonymousClass1Y3.A14 /*140*/:
            case 166:
            case AnonymousClass1Y3.A1T /*187*/:
            case 860:
                return 43200;
            case AnonymousClass1Y3.A16 /*142*/:
            case AnonymousClass1Y3.A3S /*421*/:
            case AnonymousClass1Y3.A7u /*973*/:
            case AnonymousClass1Y3.A98 /*1111*/:
            case 1727:
            case AnonymousClass1Y3.AO6 /*2931*/:
            case 2932:
            case AnonymousClass1Y3.AOY /*2983*/:
                return 50000;
            case 154:
            case AnonymousClass1Y3.A1D /*156*/:
            case 329:
            case 374:
            case AnonymousClass1Y3.A9q /*1209*/:
            case AnonymousClass1Y3.A9s /*1213*/:
            case 1278:
            case AnonymousClass1Y3.AHi /*2146*/:
            case 2173:
                return 150;
            case AnonymousClass1Y3.A1E /*157*/:
                return 56;
            case AnonymousClass1Y3.A1F /*161*/:
            case 164:
                return 140;
            case AnonymousClass1Y3.A1L /*172*/:
            case 183:
            case 633:
            case 1813:
            case 2224:
                return 120;
            case AnonymousClass1Y3.A1O /*176*/:
            case AnonymousClass1Y3.A8D /*1011*/:
            case AnonymousClass1Y3.AEo /*1811*/:
                return 800;
            case 182:
            case AnonymousClass1Y3.A1S /*185*/:
            case 202:
            case AnonymousClass1Y3.A2q /*355*/:
            case AnonymousClass1Y3.A3C /*393*/:
            case 464:
            case 465:
            case AnonymousClass1Y3.A3s /*467*/:
            case 468:
            case AnonymousClass1Y3.A4K /*517*/:
            case 702:
            case 769:
            case 771:
            case 1021:
            case AnonymousClass1Y3.A8M /*1022*/:
            case AnonymousClass1Y3.A8N /*1023*/:
            case AnonymousClass1Y3.ABI /*1377*/:
            case 1538:
            case AnonymousClass1Y3.AFH /*1870*/:
                return 300;
            case 186:
                return 1200;
            case AnonymousClass1Y3.A1W /*190*/:
            case 1398:
            case AnonymousClass1Y3.AEq /*1814*/:
                return 48;
            case AnonymousClass1Y3.A1X /*191*/:
            case 193:
            case AnonymousClass1Y3.A36 /*382*/:
            case AnonymousClass1Y3.A4c /*543*/:
            case 558:
            case 731:
            case 742:
            case AnonymousClass1Y3.A8I /*1017*/:
            case AnonymousClass1Y3.A8q /*1077*/:
            case 1087:
            case 1214:
            case AnonymousClass1Y3.ABs /*1449*/:
            case 1976:
            case AnonymousClass1Y3.AIp /*2294*/:
            case AnonymousClass1Y3.AK9 /*2443*/:
                return 6;
            case 192:
            case 565:
                return 64;
            case AnonymousClass1Y3.A1Z /*196*/:
            case AnonymousClass1Y3.A68 /*753*/:
            case 858:
            case 1917:
            case 2908:
            case 2912:
                return 1440;
            case AnonymousClass1Y3.A1a /*197*/:
            case AnonymousClass1Y3.A2F /*287*/:
            case 305:
            case AnonymousClass1Y3.A2z /*372*/:
            case AnonymousClass1Y3.A3T /*423*/:
            case AnonymousClass1Y3.A3X /*430*/:
            case AnonymousClass1Y3.A3Y /*431*/:
            case 433:
            case 442:
            case AnonymousClass1Y3.A3p /*461*/:
            case AnonymousClass1Y3.A3q /*462*/:
            case 554:
            case 555:
            case AnonymousClass1Y3.A56 /*630*/:
            case AnonymousClass1Y3.A5Z /*686*/:
            case AnonymousClass1Y3.A6H /*773*/:
            case AnonymousClass1Y3.A7S /*927*/:
            case AnonymousClass1Y3.A8T /*1029*/:
            case AnonymousClass1Y3.A8d /*1058*/:
            case 1061:
            case 1062:
            case AnonymousClass1Y3.A8p /*1075*/:
            case 1132:
            case AnonymousClass1Y3.A9J /*1133*/:
            case AnonymousClass1Y3.A9O /*1145*/:
            case 1156:
            case 1161:
            case AnonymousClass1Y3.A9i /*1193*/:
            case 1288:
            case AnonymousClass1Y3.AAi /*1304*/:
            case 1410:
            case AnonymousClass1Y3.ACY /*1529*/:
            case 1545:
            case 1547:
            case 1657:
            case 1762:
            case AnonymousClass1Y3.AF6 /*1847*/:
            case AnonymousClass1Y3.AF7 /*1848*/:
            case AnonymousClass1Y3.AFC /*1860*/:
            case 1867:
            case AnonymousClass1Y3.AH4 /*2064*/:
            case AnonymousClass1Y3.AH6 /*2066*/:
            case AnonymousClass1Y3.AHU /*2115*/:
            case AnonymousClass1Y3.AIb /*2265*/:
            case 2267:
            case AnonymousClass1Y3.AIc /*2268*/:
            case 2269:
            case 2270:
            case 2285:
            case 2362:
            case AnonymousClass1Y3.AJR /*2364*/:
            case 2444:
                return LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT;
            case 198:
            case 206:
            case AnonymousClass1Y3.A4B /*503*/:
            case 1414:
            case 1441:
            case 1708:
            case AnonymousClass1Y3.AFU /*1891*/:
            case 1892:
            case AnonymousClass1Y3.AHf /*2135*/:
                return 7;
            case AnonymousClass1Y3.A1b /*199*/:
                return 45;
            case 204:
            case AnonymousClass1Y3.AAM /*1265*/:
            case 1302:
            case AnonymousClass1Y3.ADp /*1697*/:
            case 2235:
                return 1500;
            case AnonymousClass1Y3.A1f /*207*/:
                return 500000;
            case 212:
            case AnonymousClass1Y3.A2u /*363*/:
            case 385:
                return 11;
            case 213:
            case 1702:
                return -3;
            case 224:
            case AnonymousClass1Y3.A3U /*425*/:
            case AnonymousClass1Y3.A5i /*701*/:
            case 1039:
            case 1055:
            case 1148:
            case AnonymousClass1Y3.AG6 /*1955*/:
            case 2241:
                return 90;
            case AnonymousClass1Y3.A1m /*227*/:
            case 263:
            case 976:
            case AnonymousClass1Y3.A7x /*979*/:
            case AnonymousClass1Y3.A82 /*986*/:
            case 992:
            case AnonymousClass1Y3.A94 /*1103*/:
            case 1108:
            case 1355:
            case 1378:
            case AnonymousClass1Y3.ACg /*1549*/:
            case AnonymousClass1Y3.AFE /*1862*/:
            case 1864:
            case AnonymousClass1Y3.AFf /*1908*/:
            case 1912:
            case AnonymousClass1Y3.AGh /*2023*/:
            case AnonymousClass1Y3.AGi /*2024*/:
            case 2025:
            case AnonymousClass1Y3.AGj /*2026*/:
            case 2027:
            case 2028:
            case 2078:
            case 2079:
            case 2080:
            case AnonymousClass1Y3.AHD /*2081*/:
            case AnonymousClass1Y3.AHE /*2082*/:
            case AnonymousClass1Y3.AHF /*2083*/:
            case AnonymousClass1Y3.AHG /*2084*/:
            case 2085:
            case 2086:
            case AnonymousClass1Y3.AHH /*2087*/:
            case 2088:
            case AnonymousClass1Y3.AHI /*2089*/:
            case AnonymousClass1Y3.AIT /*2248*/:
            case 2252:
                return 8000;
            case AnonymousClass1Y3.A1n /*229*/:
            case AnonymousClass1Y3.A1o /*233*/:
            case 334:
            case AnonymousClass1Y3.A6V /*801*/:
            case AnonymousClass1Y3.A9D /*1120*/:
            case 2018:
            case 2242:
            case AnonymousClass1Y3.AIu /*2303*/:
            case AnonymousClass1Y3.ANy /*2921*/:
                return 250;
            case 232:
            case 422:
            case 1081:
                return 2500;
            case 241:
            case AnonymousClass1Y3.AKQ /*2476*/:
            case AnonymousClass1Y3.AKR /*2477*/:
                return 256;
            case AnonymousClass1Y3.A1r /*242*/:
            case 751:
            case 1187:
                return 75;
            case AnonymousClass1Y3.A1t /*244*/:
            case 300:
            case 502:
            case 1114:
                return 12000;
            case 255:
            case 542:
            case AnonymousClass1Y3.AEb /*1787*/:
                return 80;
            case 258:
            case AnonymousClass1Y3.A2M /*297*/:
            case AnonymousClass1Y3.A81 /*984*/:
            case AnonymousClass1Y3.A84 /*991*/:
            case AnonymousClass1Y3.A85 /*993*/:
            case 1551:
            case 1552:
                return 16000;
            case AnonymousClass1Y3.A23 /*266*/:
            case AnonymousClass1Y3.A2D /*282*/:
            case AnonymousClass1Y3.A2N /*298*/:
            case AnonymousClass1Y3.A57 /*632*/:
            case 672:
            case 678:
            case AnonymousClass1Y3.A9A /*1115*/:
            case AnonymousClass1Y3.AAk /*1306*/:
            case AnonymousClass1Y3.AAx /*1331*/:
            case 1352:
            case AnonymousClass1Y3.ABe /*1416*/:
            case 1435:
            case AnonymousClass1Y3.AFd /*1905*/:
            case 1906:
            case AnonymousClass1Y3.AFe /*1907*/:
            case AnonymousClass1Y3.AFg /*1909*/:
            case AnonymousClass1Y3.AFh /*1910*/:
            case 1911:
            case AnonymousClass1Y3.AGM /*1989*/:
            case 2151:
            case AnonymousClass1Y3.AIS /*2245*/:
            case 2246:
            case 2247:
            case AnonymousClass1Y3.AIU /*2249*/:
            case AnonymousClass1Y3.AIV /*2250*/:
            case 2251:
                return 6000;
            case AnonymousClass1Y3.A25 /*269*/:
                return 1440000;
            case 286:
            case AnonymousClass1Y3.AGs /*2040*/:
                return 6500;
            case AnonymousClass1Y3.A2G /*288*/:
            case AnonymousClass1Y3.A5S /*675*/:
                return 4500;
            case AnonymousClass1Y3.A2L /*296*/:
            case AnonymousClass1Y3.A5E /*647*/:
            case 664:
            case AnonymousClass1Y3.A5n /*711*/:
            case AnonymousClass1Y3.ADh /*1683*/:
            case AnonymousClass1Y3.AFo /*1927*/:
            case AnonymousClass1Y3.AFp /*1928*/:
            case 2214:
                return 40;
            case AnonymousClass1Y3.A2Q /*308*/:
                return 54000000;
            case AnonymousClass1Y3.A2S /*310*/:
                return 46800000;
            case 312:
            case 443:
            case AnonymousClass1Y3.A3t /*469*/:
            case 470:
            case AnonymousClass1Y3.A3v /*472*/:
            case 473:
            case 577:
            case 772:
            case 784:
            case AnonymousClass1Y3.ADq /*1700*/:
            case AnonymousClass1Y3.AE0 /*1718*/:
            case 1722:
            case 1723:
            case AnonymousClass1Y3.AEA /*1738*/:
            case 1740:
            case AnonymousClass1Y3.AFN /*1879*/:
            case AnonymousClass1Y3.AFR /*1885*/:
            case AnonymousClass1Y3.AFS /*1887*/:
            case AnonymousClass1Y3.ANw /*2919*/:
            case AnonymousClass1Y3.ANx /*2920*/:
                return 86400;
            case AnonymousClass1Y3.A2U /*314*/:
                return 1944600828938713L;
            case 315:
                return 2042258622461435L;
            case AnonymousClass1Y3.A2V /*317*/:
            case 323:
            case 338:
            case AnonymousClass1Y3.A2j /*346*/:
            case 591:
            case AnonymousClass1Y3.A50 /*621*/:
            case 625:
            case AnonymousClass1Y3.A5V /*679*/:
            case 681:
            case AnonymousClass1Y3.A5e /*696*/:
            case 705:
            case AnonymousClass1Y3.A5k /*706*/:
            case 1040:
            case 1045:
            case AnonymousClass1Y3.A9j /*1194*/:
            case AnonymousClass1Y3.ABM /*1384*/:
            case AnonymousClass1Y3.ABY /*1405*/:
            case 1406:
            case AnonymousClass1Y3.ABi /*1423*/:
            case 1443:
            case 1445:
            case AnonymousClass1Y3.ACX /*1528*/:
            case AnonymousClass1Y3.ACb /*1539*/:
            case 2300:
                return 15;
            case AnonymousClass1Y3.A2Z /*322*/:
            case 1519:
                return 70;
            case 326:
            case 381:
            case 459:
            case 487:
            case 680:
            case 774:
            case 1030:
            case AnonymousClass1Y3.A8y /*1091*/:
            case 1122:
            case 1146:
            case AnonymousClass1Y3.AAX /*1284*/:
            case 1422:
            case 1437:
            case 1480:
            case AnonymousClass1Y3.ACA /*1487*/:
            case AnonymousClass1Y3.ACB /*1489*/:
            case AnonymousClass1Y3.AGN /*1990*/:
            case 1997:
            case AnonymousClass1Y3.AGT /*1998*/:
            case 1999:
            case 2000:
            case AnonymousClass1Y3.AGU /*2001*/:
            case 2002:
            case AnonymousClass1Y3.AGY /*2009*/:
            case AnonymousClass1Y3.AGZ /*2010*/:
            case 2011:
            case AnonymousClass1Y3.AGa /*2012*/:
            case AnonymousClass1Y3.AGb /*2013*/:
            case AnonymousClass1Y3.AGc /*2014*/:
            case 2019:
            case AnonymousClass1Y3.AGt /*2041*/:
            case 2110:
            case 2112:
            case 2118:
            case 2150:
            case AnonymousClass1Y3.AIl /*2287*/:
                return 2000;
            case AnonymousClass1Y3.A2b /*327*/:
                return 550;
            case 336:
            case AnonymousClass1Y3.A9v /*1218*/:
            case AnonymousClass1Y3.AGd /*2015*/:
            case AnonymousClass1Y3.AHe /*2134*/:
            case 2512:
                return 100000;
            case AnonymousClass1Y3.A2g /*343*/:
            case AnonymousClass1Y3.A5Y /*685*/:
            case AnonymousClass1Y3.AFQ /*1884*/:
                return 259200;
            case 348:
                return 35;
            case AnonymousClass1Y3.A2m /*351*/:
            case AnonymousClass1Y3.A2r /*360*/:
            case AnonymousClass1Y3.A4u /*594*/:
            case 596:
            case AnonymousClass1Y3.A5h /*700*/:
            case 796:
            case AnonymousClass1Y3.A9d /*1183*/:
            case 1542:
            case AnonymousClass1Y3.AEc /*1789*/:
            case 1815:
            case 1930:
            case AnonymousClass1Y3.AJ0 /*2311*/:
                return 24;
            case 358:
            case 683:
            case AnonymousClass1Y3.A6r /*856*/:
            case 1287:
                return 180;
            case 364:
            case 365:
            case AnonymousClass1Y3.A9I /*1130*/:
            case 2963:
            case AnonymousClass1Y3.AOP /*2965*/:
                return -2;
            case 366:
            case AnonymousClass1Y3.A5H /*653*/:
            case AnonymousClass1Y3.A5P /*671*/:
            case AnonymousClass1Y3.A72 /*873*/:
            case AnonymousClass1Y3.A8W /*1033*/:
            case AnonymousClass1Y3.A8z /*1093*/:
            case AnonymousClass1Y3.A90 /*1094*/:
            case AnonymousClass1Y3.A91 /*1095*/:
            case 1096:
            case 1167:
            case 1168:
            case 1207:
            case AnonymousClass1Y3.ABN /*1385*/:
            case 1500:
            case 1558:
            case 1724:
            case 1763:
            case AnonymousClass1Y3.AFZ /*1900*/:
            case 1920:
            case 1953:
            case 2069:
            case AnonymousClass1Y3.AHP /*2101*/:
            case AnonymousClass1Y3.AHW /*2117*/:
            case AnonymousClass1Y3.AI6 /*2208*/:
            case AnonymousClass1Y3.AIY /*2261*/:
            case 2609:
                return 4;
            case AnonymousClass1Y3.A38 /*384*/:
                return 1255;
            case AnonymousClass1Y3.A3F /*396*/:
            case 1163:
                return 9000;
            case AnonymousClass1Y3.A3J /*402*/:
            case AnonymousClass1Y3.A3K /*403*/:
            case 404:
            case 405:
            case 406:
            case AnonymousClass1Y3.A3L /*407*/:
                return 163840;
            case AnonymousClass1Y3.A3V /*428*/:
            case 1660:
            case AnonymousClass1Y3.ADW /*1661*/:
            case AnonymousClass1Y3.ADY /*1663*/:
            case 1664:
            case AnonymousClass1Y3.ADa /*1666*/:
            case 1667:
            case 1669:
            case 1670:
            case 2193:
            case 2314:
            case AnonymousClass1Y3.AJ3 /*2316*/:
                return -4;
            case AnonymousClass1Y3.A3c /*440*/:
                return -150;
            case AnonymousClass1Y3.A3r /*466*/:
            case AnonymousClass1Y3.A3u /*471*/:
            case AnonymousClass1Y3.A9h /*1192*/:
            case 1761:
                return 604800;
            case AnonymousClass1Y3.A3x /*475*/:
                return -14;
            case AnonymousClass1Y3.A43 /*494*/:
                return 259200000;
            case 500:
                return 2013;
            case 552:
            case 1776:
            case AnonymousClass1Y3.AEX /*1780*/:
            case AnonymousClass1Y3.AEm /*1807*/:
                return StatFsUtil.IN_KILO_BYTE;
            case 553:
                return 16384;
            case AnonymousClass1Y3.A4o /*575*/:
                return 216000;
            case 578:
            case AnonymousClass1Y3.A5X /*684*/:
            case 945:
                return 10800;
            case AnonymousClass1Y3.A4v /*597*/:
            case 598:
            case 601:
            case AnonymousClass1Y3.AFr /*1931*/:
                return 168;
            case 609:
                return 36;
            case 615:
            case 1582:
            case AnonymousClass1Y3.AD5 /*1607*/:
            case 1626:
            case AnonymousClass1Y3.ADP /*1645*/:
            case 2188:
                return 9;
            case 636:
                return 16777216;
            case AnonymousClass1Y3.A59 /*637*/:
            case 970:
            case AnonymousClass1Y3.ABG /*1372*/:
            case AnonymousClass1Y3.AHo /*2158*/:
                return 720;
            case AnonymousClass1Y3.A5C /*640*/:
            case AnonymousClass1Y3.A7r /*969*/:
                return 640;
            case AnonymousClass1Y3.A5M /*665*/:
                return 15300;
            case AnonymousClass1Y3.A5N /*668*/:
                return 36000000;
            case AnonymousClass1Y3.A5T /*676*/:
            case 990:
            case 1082:
                return 7000;
            case AnonymousClass1Y3.A5U /*677*/:
                return 99;
            case AnonymousClass1Y3.A69 /*756*/:
                return 26214400;
            case 854:
                return 101;
            case AnonymousClass1Y3.A6s /*857*/:
            case 1036:
            case AnonymousClass1Y3.ABH /*1375*/:
            case AnonymousClass1Y3.ABr /*1448*/:
            case AnonymousClass1Y3.ACU /*1522*/:
            case AnonymousClass1Y3.AHs /*2168*/:
            case AnonymousClass1Y3.AIo /*2293*/:
                return 12;
            case AnonymousClass1Y3.A73 /*874*/:
            case 1135:
            case AnonymousClass1Y3.ACF /*1497*/:
            case AnonymousClass1Y3.ACG /*1498*/:
            case AnonymousClass1Y3.ACI /*1502*/:
            case AnonymousClass1Y3.ACS /*1520*/:
            case AnonymousClass1Y3.AEr /*1816*/:
                return 16;
            case AnonymousClass1Y3.A7D /*894*/:
            case 895:
                return -17;
            case 925:
            case AnonymousClass1Y3.ABp /*1444*/:
                return 21;
            case 928:
            case 2901:
                return 66;
            case AnonymousClass1Y3.A7X /*933*/:
                return 307200;
            case 957:
            case 958:
            case AnonymousClass1Y3.AIW /*2254*/:
                return 1000000;
            case AnonymousClass1Y3.A7p /*964*/:
            case 967:
            case 1236:
                return 480;
            case AnonymousClass1Y3.A7q /*968*/:
            case AnonymousClass1Y3.A8l /*1071*/:
            case AnonymousClass1Y3.A8m /*1072*/:
            case AnonymousClass1Y3.A8n /*1073*/:
            case AnonymousClass1Y3.A8o /*1074*/:
                return 100000000;
            case AnonymousClass1Y3.A7z /*982*/:
            case 2195:
                return 262144;
            case AnonymousClass1Y3.A80 /*983*/:
                return 131072;
            case 985:
                return 400000;
            case 1010:
                return 864000000;
            case 1024:
                return 31536000;
            case 1034:
            case 1035:
            case AnonymousClass1Y3.A8X /*1037*/:
            case 1038:
            case AnonymousClass1Y3.AAt /*1327*/:
            case AnonymousClass1Y3.ABJ /*1380*/:
            case 1454:
            case AnonymousClass1Y3.ACH /*1501*/:
            case AnonymousClass1Y3.ADv /*1711*/:
            case 2048:
            case 2049:
            case AnonymousClass1Y3.AIE /*2223*/:
                return 8;
            case AnonymousClass1Y3.A8k /*1069*/:
            case 1076:
            case 1849:
                return 25000;
            case 1079:
                return 1100;
            case 1116:
                return 65536;
            case 1117:
            case AnonymousClass1Y3.AAT /*1277*/:
                return 32;
            case AnonymousClass1Y3.A9B /*1118*/:
                return 320;
            case 1196:
                return 125;
            case 1212:
                return 130;
            case AnonymousClass1Y3.A9t /*1215*/:
                return 136;
            case AnonymousClass1Y3.A9w /*1219*/:
                return 12500;
            case AnonymousClass1Y3.AA5 /*1231*/:
                return 20971520;
            case 1232:
                return 52428800;
            case 1233:
            case AnonymousClass1Y3.AGn /*2033*/:
                return 104857600;
            case AnonymousClass1Y3.AA6 /*1234*/:
                return ErrorReporter.DEFAULT_MAX_REPORT_SIZE;
            case 1251:
            case 1281:
                return 128000;
            case AnonymousClass1Y3.AAE /*1255*/:
            case AnonymousClass1Y3.AEV /*1777*/:
            case 1778:
            case AnonymousClass1Y3.AEY /*1781*/:
            case AnonymousClass1Y3.AEa /*1786*/:
                return 28;
            case AnonymousClass1Y3.AAG /*1258*/:
                return 23;
            case 1262:
            case 2200:
                return 102400;
            case AnonymousClass1Y3.AAP /*1270*/:
                return 110;
            case 1301:
            case AnonymousClass1Y3.AGp /*2035*/:
            case AnonymousClass1Y3.AIN /*2238*/:
            case AnonymousClass1Y3.AJ1 /*2312*/:
                return 400;
            case 1320:
            case 1321:
                return 1200000;
            case 1333:
            case 1335:
                return 512;
            case 1348:
                return 83333333;
            case 1360:
                return 10485760;
            case AnonymousClass1Y3.ABD /*1367*/:
                return 71;
            case AnonymousClass1Y3.ABV /*1402*/:
                return 375;
            case AnonymousClass1Y3.ABf /*1418*/:
                return 3500;
            case 1436:
                return 44;
            case AnonymousClass1Y3.ACL /*1508*/:
            case 1510:
                return 21600000;
            case AnonymousClass1Y3.ACN /*1511*/:
                return 2419200000L;
            case AnonymousClass1Y3.ACO /*1512*/:
                return 43200000;
            case AnonymousClass1Y3.ACP /*1514*/:
            case 1945:
                return 19;
            case AnonymousClass1Y3.ACQ /*1517*/:
            case AnonymousClass1Y3.ACR /*1518*/:
            case AnonymousClass1Y3.AIs /*2299*/:
                return 21600;
            case AnonymousClass1Y3.ACc /*1540*/:
                return 999;
            case AnonymousClass1Y3.ADe /*1675*/:
                return 1000000000;
            case 1676:
            case 1677:
            case 1678:
                return 750;
            case 1698:
            case AnonymousClass1Y3.AHq /*2163*/:
            case 2281:
                return ErrorReporter.MAX_REPORT_AGE;
            case AnonymousClass1Y3.ADs /*1704*/:
            case 2207:
                return 240;
            case AnonymousClass1Y3.AE6 /*1729*/:
                return 65;
            case 1730:
                return 4096;
            case AnonymousClass1Y3.AEC /*1741*/:
                return 2592000;
            case AnonymousClass1Y3.AED /*1742*/:
                return 975612;
            case AnonymousClass1Y3.AEF /*1744*/:
                return -4321;
            case 1758:
                return 626059347767118L;
            case AnonymousClass1Y3.AEt /*1818*/:
            case 1819:
            case 1820:
            case 1821:
                return 2147483647L;
            case 1825:
                return 67108864;
            case AnonymousClass1Y3.AFD /*1861*/:
            case 1863:
                return 12200;
            case AnonymousClass1Y3.AFO /*1880*/:
                return 2048;
            case 1882:
                return 2009;
            case AnonymousClass1Y3.AFP /*1883*/:
                return 540;
            case AnonymousClass1Y3.AFX /*1896*/:
                return 64800000;
            case 1901:
                return 80000;
            case AnonymousClass1Y3.AFb /*1903*/:
            case AnonymousClass1Y3.AIQ /*2243*/:
                return 4191304;
            case AnonymousClass1Y3.AFc /*1904*/:
            case AnonymousClass1Y3.AIR /*2244*/:
                return 4194304;
            case 1972:
                return 34;
            case AnonymousClass1Y3.AGE /*1973*/:
                return 162;
            case AnonymousClass1Y3.AGG /*1975*/:
                return 22;
            case AnonymousClass1Y3.AGf /*2020*/:
                return 150000;
            case 2021:
                return 550000;
            case AnonymousClass1Y3.AGg /*2022*/:
            case 2042:
                return 2000000;
            case AnonymousClass1Y3.AGx /*2047*/:
                return 786432;
            case 2059:
                return 13107200;
            case AnonymousClass1Y3.AH2 /*2060*/:
                return 3538944;
            case 2093:
                return 8000000;
            case AnonymousClass1Y3.AHT /*2111*/:
                return 48000;
            case AnonymousClass1Y3.AHb /*2128*/:
                return 10240;
            case 2157:
                return 2160;
            case AnonymousClass1Y3.AHr /*2164*/:
            case 2169:
                return 409600;
            case 2165:
                return OdexSchemeArtXdex.MIN_DISK_FREE_FOR_MIXED_MODE;
            case AnonymousClass1Y3.AHt /*2170*/:
                return 5120;
            case AnonymousClass1Y3.AI5 /*2201*/:
                return 20480;
            case 2228:
                return 54;
            case 2282:
                return 400000000;
            case AnonymousClass1Y3.AIm /*2288*/:
            case AnonymousClass1Y3.AIz /*2310*/:
            case AnonymousClass1Y3.AOA /*2938*/:
                return 18;
            case AnonymousClass1Y3.AIy /*2308*/:
                return 2700;
            case AnonymousClass1Y3.AJ8 /*2326*/:
                return 72;
            case 2356:
                return 501;
            case 2903:
                return 1080;
            case AnonymousClass1Y3.AOW /*2981*/:
                return 25000000;
        }
    }

    public static String A02(long j) {
        int i;
        int i2;
        int i3;
        int A01 = AnonymousClass0XG.A01(j);
        if (A01 == 0) {
            return "{   \"name\": \"compact_disk/fresco\",   \"cctype\": \"multi-output-resolved\",   \"version\": 1,   \"policy_id\": \"static\",   \"sample_rate\": 100,   \"outputs\": [     {       \"name\": \"cache_size_limit\",       \"type\": \"INT\"     },     {       \"name\": \"cache_size_limit_low_space\",       \"type\": \"INT\"     },     {       \"name\": \"cache_size_limit_min_space\",       \"type\": \"INT\"     }   ],   \"values\": [     {       \"name\": \"cache_size_limit\",       \"value\": \"62914560\"     },     {       \"name\": \"cache_size_limit_low_space\",       \"value\": \"20971520\"     },     {       \"name\": \"cache_size_limit_min_space\",       \"value\": \"4194304\"     }   ],   \"timestamp\": 1492015876 }";
        }
        if (A01 == 1) {
            return "fbandroid_app_info,mqtt_publish_debug";
        }
        if (A01 == 2 || A01 == 3) {
            return "NO_ANIMATION";
        }
        if (A01 == 4) {
            return "STILL,WALKING";
        }
        if (A01 == 5) {
            return "MOVING";
        }
        if (A01 == 67) {
            return "facebook.com,workplace.com,fbpigeon.com";
        }
        if (A01 == 68) {
            return "none";
        }
        if (A01 == 70) {
            return "StoriesTraySubscription";
        }
        if (A01 == 71) {
            return "comment_create_subscribe,feedback_typing_subscribe,privacy_scope_edit_subscribe,video_home_badge_update_subscribe,live_video_broadcast_status_update_subscribe,user_search_awareness_suggestion_subscribe,creator_live_status_changed,pinned_comment_event_create_subscribe,gametime_match_data_update_subscribe,device_discovery_subscribe,tip_jar_tip_event_subscribe,backstage_posts_create_subscribe,direct_message_inbox_badge_subscribe,direct_inbox_badge_subscribe,direct_thread_subscribe,video_broadcast_schedule_set_start_time_subscribe,stories_tray_subscribe,video_home_badge_update_subscribe,marketplace_notification_subscribe,live_donation_video_donate_event_subscribe,video_monetization_creator_update_subscribe,video_live_video_suicide_prevention_flag_subscribe,live_video_product_tagging_event_subscribe";
        }
        switch (A01) {
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "STILL";
            case 15:
            case 258:
            case AnonymousClass1Y3.A2F /*287*/:
            case 601:
                return " ";
            case Process.SIGCONT:
                return "0.75";
            case Process.SIGSTOP:
                return "http://www1.cleanwaterpriority.org/w_mf_in_801?,https://0.freebasics.com/,http://our.intern.facebook.com/intern/px,https://pxl.cl,https://fburl.com,https://fb.quip.com,https://phabricator.fb.com,https://phabricator.intern.facebook.com,https://our.intern.facebook.com/intern,https://intern.facebook.com/intern,https://www.facebook.com/careers,https://bigzipfiles.facebook.com";
            case 20:
                return "https://work.facebook.com/company_creation/invite/,https://work.alpha.facebook.com/company_creation/invite/";
            case AnonymousClass1Y3.A05 /*21*/:
            case 792:
                return "[]";
            case AnonymousClass1Y3.A06 /*22*/:
                return "[\"netflix.com\", \"m.me\"]";
            case 23:
                return "{\"Referer\":\"http://m.facebook.com/\", \"Cache-Control\":\"no-cache\", \"Accept\":\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\"}";
            case AnonymousClass1Y3.A07 /*24*/:
                return "{\"Referer\":\"http://m.facebook.com/\", \"User-Agent\":\"Mozilla/5.0 (Linux; Android 4.0.0;) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36 [FB_IAB/FB4A;]\", \"X-Purpose\":\"preview\", \"Cache-Control\":\"no-cache\", \"Accept\":\"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\"}";
            case 25:
                return "[10, 0, 20, 0]";
            case AnonymousClass1Y3.A08 /*26*/:
                return "com.android.chrome,org.mozilla.firefox,com.opera.browser,com.UCMobile.intl";
            case AnonymousClass1Y3.A09 /*27*/:
            case AnonymousClass1Y3.A1Q /*180*/:
            case AnonymousClass1Y3.A27 /*272*/:
                return "default";
            case 28:
                return "69844D9A";
            case 29:
                return "urn:x-cast:com.facebook.fb";
            case AnonymousClass1Y3.A0A /*30*/:
                return "https://www.facebook.com/help";
            case 33:
            case AnonymousClass1Y3.A2q /*355*/:
            case AnonymousClass1Y3.A5r /*718*/:
                return "control";
            case 62:
                return "zstd";
            case 64:
                return "api.facebook.com,b-api.facebook.com,graph.facebook.com,b-graph.facebook.com";
            case 97:
                return "1";
            case 99:
                return "2,3,5,10";
            case AnonymousClass1Y3.A0i /*101*/:
                return "giphy.com";
            case AnonymousClass1Y3.A0j /*102*/:
                return "www.nytimes.com,embed.fbsbx.com,www.instagram.com,www.facebook.com,wp.kit.se,gizmodo.com,kotaku.com,lifehacker.com,jezebel.com,deadspin.com,jalopnik.com,embed.theguardian.com,m.mic.com,kinja.com";
            case 103:
                return "disable";
            case AnonymousClass1Y3.A0m /*106*/:
                return "299947508549";
            case AnonymousClass1Y3.A0o /*109*/:
                return "1855503671337940,1429084553777413";
            case 112:
            case AnonymousClass1Y3.A2P /*307*/:
            case AnonymousClass1Y3.A2Q /*308*/:
            case AnonymousClass1Y3.A2R /*309*/:
                return "invalid config";
            case AnonymousClass1Y3.A0r /*117*/:
            case AnonymousClass1Y3.A4X /*535*/:
            case AnonymousClass1Y3.A54 /*627*/:
            case 704:
                return "\"\"";
            case AnonymousClass1Y3.A0u /*121*/:
                i = 791;
                return C99084oO.$const$string(i);
            case AnonymousClass1Y3.A0v /*122*/:
            case 123:
                return "{\"ctr_multiply_values\" : {\"base_values\" :  {\"!is_active && inbox_unit_has_story && inbox_unit_has_unread_story\" : {\"viewed\" : \"montage_user\"}, \"!is_active && inbox_unit_has_story && !inbox_unit_has_unread_story\" : {\"viewed\" : \"montage_user-1000\"}, \"is_active && !inbox_unit_has_story\" : {\"viewed\" : \"inbox_active_now\"}, \"is_active && inbox_unit_has_story && inbox_unit_has_unread_story\" : {\"viewed\" : \"Max(inbox_active_now,montage_user)\"}, \"is_active && inbox_unit_has_story && !inbox_unit_has_unread_story\" : {\"viewed\" : \"inbox_active_now\"}}},\"ctr_value_features\": {\"inbox_unit_has_story\": \"inbox_unit_has_story\",\"inbox_unit_has_unread_story\": \"inbox_unit_has_unread_story\",\"inbox_active_now\": \"inbox_active_now\",\"montage_user\": \"montage_user\",\"is_active\": \"is_active\"}}";
            case 128:
                return "camera_gallery_mic";
            case 129:
            case AnonymousClass1Y3.A0y /*130*/:
                return "stickers";
            case AnonymousClass1Y3.A0z /*131*/:
                return "edge-mqtt-merge.facebook.com";
            case AnonymousClass1Y3.A10 /*133*/:
                return "add";
            case 134:
                return "'Keep in mind you can still report a chat, even if messages have been removed.'";
            case AnonymousClass1Y3.A11 /*135*/:
                return "[home_nested, calltab, montage, pinned_groups, people, me]";
            case AnonymousClass1Y3.A12 /*136*/:
                return "Evan Rubin";
            case 137:
                return "499657941";
            case AnonymousClass1Y3.A13 /*138*/:
                return "profile_picture_only";
            case 139:
            case AnonymousClass1Y3.A4W /*533*/:
                return "FANeflaawkeANLGireg43";
            case AnonymousClass1Y3.A14 /*140*/:
                return "https://www.facebook.com/help/336759363070078/";
            case AnonymousClass1Y3.A15 /*141*/:
                return "android_public_test_v2";
            case AnonymousClass1Y3.A17 /*144*/:
                i = 69;
                return C99084oO.$const$string(i);
            case 146:
                return "send";
            case AnonymousClass1Y3.A19 /*148*/:
                return "TlK63EA9ScJNKU3AZwY";
            case AnonymousClass1Y3.A1A /*149*/:
                return "v1/stickers/search";
            case AnonymousClass1Y3.A1B /*150*/:
                return "v1/stickers/trending";
            case 151:
                return "406655189415060";
            case 152:
            case AnonymousClass1Y3.A1C /*153*/:
                return "fixed_height";
            case AnonymousClass1Y3.A1E /*157*/:
                return "config=baseline&map=baseline";
            case 159:
                return "old_design";
            case AnonymousClass1Y3.A1M /*174*/:
                return "1437802903174548,1520974658132631,154466594717516,162333030618222,194382707352399,201013910048543,209575205899648,210852332406135,364384037058235,372249609618346,595562630551173,595562723884497,631487306879344,645898258824359,645898375491014,645898722157646,652574521431361,652574618098018,776940105766163,907259869345810,990066261014590";
            case AnonymousClass1Y3.A1N /*175*/:
                return "[128076,128074,9996,10084,128139,128514,128525,128536,128541,128521,128540,128513,128531,128565,128561,128563,128591,128073,128170,128075,128588,128169,128701,127867,127864,128131,128293,128273,128142,127752,128150,128640,9728,127775,127769,127880,127881,127873,127801,127802,9924,128132,127926,127754,127872,128096,9973,9917,127944,127936,127934,9918,127921,127937,127942,128176,128545,128125,128118,128127,128128,128120,128059,128053,128048,128054,128046,128055,128044,128041,128013,128031,128014,128043,127808,128163,128299,128064,128067,128068,128663,128066,128690,127822,127827,127817,128157,128152,128155,128153,128156,128154]";
            case AnonymousClass1Y3.A1O /*176*/:
                return "1064722233555910,1064722366889230,126362100881920,1398251820393001,1400298193516573,1435018290122461,1435018460122444,1435018950122395,1435019323455691,1435019863455637,1435019936788963,1435020103455613,1435020546788902,1435020633455560,144885035685763,147663618749235,1498876530353477,1520973618132735,1528732074026137,1529175807332670,1560932214122348,1573846492838535,1573846502838534,1573846662838518,1573846696171848,1578461442389306,1578461899055927,1601168396781745,162333010618224,162333160618209,165989743596651,167788116751808,167788176751802,168400876649624,168401079982937,194382547352415,209575129232989,209575225899646,233289163496181,275796379225017,279478012246345,288280931382636,320785064782306,335520473317640,359532884245386,364384197058219,373607019482605,377001612445899,383649078463198,383649095129863,383649128463193,383649288463177,383649305129842,383649338463172,387545514704666,387545641371320,392309990866313,396449133832526,396449193832520,396449220499184,396449263832513,396449307165842,396449313832508,396449327165840,396449390499167,396449427165830,400196913465686,401768386625393,443280992445212,443281049111873,460356747411193,460938407361341,460938577361324,488541141259331,499671100115393,505116989646476,520074081469422,531111320292243,531111456958896,553453268115348,567099130055250,575284855890892,582402921770687,583052295121841,583052301788507,583052308455173,583052315121839,583052335121837,583052345121836,583052351788502,583052361788501,583052381788499,583052388455165,583052408455163,583052425121828,583052458455158,583052475121823,583052501788487,583052515121819,583052545121816,583052565121814,583052581788479,617701408285470,619434111483659,619434141483656,619434168150320,619434188150318,623427284358672,629262667190050,633722693313707,645898405491011,665073386837515,677508925613598,698963303549479,700158816686950,708211885877063,709740975714981,763929953629103,767334499959626,782436601841289,785629094833616,785630028166856,789355311153383,792373887546419,802339523173068,818814574812718,830546393633249,832506643526141,832506790192793,832506943526111,872063532855491,907259782679152,973617562690300,446661972206728,446662025540056,944849858898288,1632953856969811,1632952510303279,1523526091274709,1523525984608053,1523526104608041,1523526184608033,1523526144608037";
            case AnonymousClass1Y3.A1P /*177*/:
                return "736122449816802,736122399816807,652574398098040,652574271431386,736135436482170,652574594764687,324210667722010,516137875201580,658787710830597,658787654163936,658787784163923,516137741868260,652574258098054,516139231868111,658787704163931,516137815201586,516122918536409,516136925201675,658787674163934,652574584764688,658787730830595,658787647497270,324210877721989,658787697497265,652574538098026,652574631431350,652574654764681,652574371431376,658787667497268,324210871055323,658787724163929,658787800830588,658787684163933,658787717497263,658787774163924,658787690830599,652574404764706,677962302234927,658787767497258,652574548098025,652574531431360,677509172280240,677509158946908,677986355565855,677961725568318,677509085613582,658787760830592,652574681431345,677509108946913,652574724764674,652574484764698,677509122280245,677508982280259,677508972280260,678563048841519,677961945568296,677509135613577,677960772235080,677509025613588,677509115613579,677509058946918,677508952280262,453843684720738,446661945540064,446662038873388,446661825540076,446661998873392,1632952386969958,1632952700303260,1632952616969935,1632953976969799,944850028898271,652781911483064,1632954263636437";
            case 178:
                return "360105817528572,1398251863726330,144885325685734,210852292406139,213405902141968";
            case 179:
                return "456205387826240,1598049900458312,134873503361580,394507800693326,615507871865804,380362042125235,628030680646582,597727260347990,126361870881943,350357561732812";
            case AnonymousClass1Y3.A1R /*181*/:
                return "add_number";
            case AnonymousClass1Y3.A1S /*185*/:
            case 186:
                return "payments";
            case AnonymousClass1Y3.A1T /*187*/:
                return "1134437769956220";
            case AnonymousClass1Y3.A1U /*188*/:
                return "ads_invoice, mor_dummy_third_party, mor_none, mor_event_ticketing, mor_messenger_commerce, mor_p2p_transfer, nmor_tip_jar, nmor_mfs, payment_settings, mor_game_tipping_token";
            case AnonymousClass1Y3.A1V /*189*/:
                return "nmor_checkout_experiences, nmor_c2c_checkout_experiences";
            case AnonymousClass1Y3.A1W /*190*/:
                return "mor_p2p_transfer";
            case AnonymousClass1Y3.A1X /*191*/:
                return "mor_messenger_commerce, nmor_synchronous_component_flow, nmor_pages_commerce";
            case 192:
                return "USA";
            case 193:
                return "CU,IR,KP,SD,SS,SY";
            case AnonymousClass1Y3.A1Y /*194*/:
                i2 = 70;
                return AnonymousClass80H.$const$string(i2);
            case AnonymousClass1Y3.A1Z /*196*/:
                return "mor_sotto";
            case AnonymousClass1Y3.A1c /*200*/:
                return "default_smooth_scroll";
            case 205:
                return "^fbrpc://facebook/call_return/\\?.*$";
            case 206:
                return "https://graph.facebook.com/v3.2/cdn_rmd";
            case 224:
                return "TOP_FB_FRIENDS";
            case AnonymousClass1Y3.A1l /*225*/:
                return "BROADCAST_FLOW";
            case 235:
                return "{\"com.facebook.katana.IntentUriHandler\": {\"enforce_scheme\": true, \"enforce_scheme_and_authority\": false, \"whitelist\": {\"fb\": \".*\", \"fb-work\": \".*\", \"facebook\": \".*\", \"dialtone\": \".*\", \"http\": \".*\", \"https\": \".*\"}}}";
            case AnonymousClass1Y3.A1v /*246*/:
                return "graphservices,";
            case AnonymousClass1Y3.A1w /*247*/:
                return "imagepipeline,";
            case 261:
                return "Autoplay Setting is now";
            case 264:
                return "info";
            case 265:
                return "CHANNEL_VIEW_FROM_NEWSFEED";
            case AnonymousClass1Y3.A24 /*267*/:
                return "newsfeed,";
            case 268:
                return "CHAINING_GAMING_PROMOTION";
            case AnonymousClass1Y3.A25 /*269*/:
                return "DEFAULT_SMOOTH_SCROLL";
            case 271:
                return "HIGH";
            case AnonymousClass1Y3.A28 /*275*/:
                return "GOOD";
            case AnonymousClass1Y3.A29 /*277*/:
                return "{\"use_network_quality\":true,\"use_network_type\":false,\"excellent_value\":2000,\"good_value\":2500,\"moderate_value\":3000,\"poor_value\":3500,\"wifi_value\":2000,\"cell_4g_value\":2000,\"cell_3g_value\":2000,\"cell_2g_value\":2000,\"default_value\":2000}";
            case AnonymousClass1Y3.A2C /*281*/:
                return ErrorReportingConstants.ANR_DEFAULT_RECOVERY_DELAY_VAL;
            case 283:
            case 514:
                return "{\"use_network_quality\":false,\"use_network_type\":true,\"excellent_value\":6000,\"good_value\":6000,\"moderate_value\":6000,\"poor_value\":6000,\"wifi_value\":28136,\"cell_4g_value\":14151,\"cell_3g_value\":14151,\"cell_2g_value\":14151,\"default_value\":6000}";
            case 284:
            case AnonymousClass1Y3.A4H /*513*/:
                return "{\"use_network_quality\":false,\"use_network_type\":true,\"excellent_value\":6000,\"good_value\":6000,\"moderate_value\":6000,\"poor_value\":6000,\"wifi_value\":26989,\"cell_4g_value\":12323,\"cell_3g_value\":12323,\"cell_2g_value\":12323,\"default_value\":6000}";
            case AnonymousClass1Y3.A2E /*285*/:
            case 511:
            case 578:
            case AnonymousClass1Y3.A5Q /*673*/:
                return "{\"use_network_quality\":true,\"use_network_type\":false,\"excellent_value\":500,\"good_value\":500,\"moderate_value\":600,\"poor_value\":1250,\"degraded_value\":1500, \"default_value\":500}";
            case 286:
            case 512:
            case 579:
            case AnonymousClass1Y3.A5R /*674*/:
                return "{\"use_network_quality\":true,\"excellent_value\":2000,\"good_value\":2500,\"moderate_value\":2800,\"poor_value\":4000,\"default_value\":2000}";
            case AnonymousClass1Y3.A2H /*289*/:
                return "sp";
            case AnonymousClass1Y3.A2I /*290*/:
                return "1746772088779222,2230845120479979";
            case 291:
                return "#ff1c1e21";
            case AnonymousClass1Y3.A2J /*292*/:
                return "#ffffffff";
            case 293:
                return "UP_NEXT";
            case AnonymousClass1Y3.A2L /*296*/:
                return "playpause";
            case AnonymousClass1Y3.A2M /*297*/:
                return "exit";
            case 300:
                return "/dialog/oauth:,/sharer/sharer.php:/sharer.php";
            case 301:
                return "ams,arn,ash,atl,atn,bru,cai,cdg,dfw,fna,fra,frc,frt,gru,hkg,iad,icn,kul,lax,lga,lhr,lla,mad,mia,mrs,mxp,nrt,ord,sea,sin,sjc,syd,tpe,vie,waw,yyz";
            case 302:
                return "{}";
            case 304:
                return "[\"graph.facebook.com\",\"b-graph.facebook.com\",\"api.facebook.com\",\"b-api.facebook.com\",\"graph2.facebook.com\",\"b-graph2.facebook.com\"]";
            case 306:
                return "10";
            case AnonymousClass1Y3.A2S /*310*/:
                return "2048";
            case 311:
                return "44100";
            case AnonymousClass1Y3.A2U /*314*/:
            case AnonymousClass1Y3.A2V /*317*/:
            case 437:
            case 438:
            case 463:
            case 464:
            case 473:
            case AnonymousClass1Y3.A4o /*575*/:
            case 576:
            case 596:
            case 611:
            case 620:
            case 634:
            case AnonymousClass1Y3.A6R /*789*/:
                return "none";
            case 315:
            case 619:
                return "UNKNOWN";
            case 316:
            case AnonymousClass1Y3.A3t /*469*/:
            case 470:
            case AnonymousClass1Y3.A3u /*471*/:
            case AnonymousClass1Y3.A3v /*472*/:
            case AnonymousClass1Y3.A4L /*518*/:
                return "0";
            case AnonymousClass1Y3.A2W /*318*/:
                return "omx\\.mtk\\.video\\.encoder\\.avc|omx\\.google\\.h264\\.encoder";
            case AnonymousClass1Y3.A2X /*319*/:
                return "LTE|HSPA+";
            case AnonymousClass1Y3.A2Y /*320*/:
                return "baseline";
            case 321:
                return "friend_share";
            case AnonymousClass1Y3.A2Z /*322*/:
                return "copa";
            case 323:
                return "top";
            case 325:
                return "LEFT";
            case 329:
                return "timestamp";
            case 347:
                return "{\"player_type_filters\":{\"channel\":[]}}";
            case AnonymousClass1Y3.A2m /*351*/:
            case AnonymousClass1Y3.A2p /*354*/:
                return "HEIGHT_9_16";
            case AnonymousClass1Y3.A2n /*352*/:
                return "SIZE_3_4";
            case AnonymousClass1Y3.A2o /*353*/:
                return "SIZE_3_2";
            case 356:
                return "frame";
            case AnonymousClass1Y3.A2r /*360*/:
                return "320019911950963,230139674528442,329649934426651";
            case AnonymousClass1Y3.A2s /*361*/:
                return "warm,neutral,cool";
            case AnonymousClass1Y3.A2t /*362*/:
                return "2253189384809325";
            case AnonymousClass1Y3.A2u /*363*/:
                return ":#66d4d4d4,360067247918354:#fffce8d8,640328819733239:#fff7ddbc,2017568918352710:#ffe6eff4";
            case 364:
                return "360067247918354,640328819733239,2017568918352710,1149677235233114,160369897892241,368935753709403,188686198386625,2233534703375089";
            case 365:
                return "700592880291497";
            case 366:
                return "2235294959849010";
            case AnonymousClass1Y3.A2v /*367*/:
                return "266941467370497";
            case AnonymousClass1Y3.A2y /*371*/:
                return "LOCATION,TIME,MUSIC_PICKER,POLL,FEELINGS,NAME,WEATHER,RATING";
            case AnonymousClass1Y3.A2z /*372*/:
                return "😍,❤,😂,💕,😘,💋,😎,🙏";
            case AnonymousClass1Y3.A30 /*373*/:
                return "https://lithium.facebook.com/lithium/feed/hadouken";
            case AnonymousClass1Y3.A36 /*382*/:
                return "notifications_tab";
            case 385:
                return "#FFFFFFFF";
            case AnonymousClass1Y3.A39 /*386*/:
            case 387:
                return "#FFFA383E";
            case 388:
            case 389:
                return "number_top_right";
            case AnonymousClass1Y3.A3A /*390*/:
                return "#64B5F0,#6F62D2,#E99781";
            case 391:
                return "#2C3B65,#748CD3,#BB7DBE";
            case AnonymousClass1Y3.A3B /*392*/:
                return "#FF8A8D91";
            case AnonymousClass1Y3.A3C /*393*/:
                return "#F2F3F5";
            case AnonymousClass1Y3.A3F /*396*/:
                return "765518473459969,193356651002223,176339666193022,";
            case AnonymousClass1Y3.A3H /*399*/:
                return "ffffff";
            case 401:
                return "1877F2";
            case AnonymousClass1Y3.A3J /*402*/:
                return "000000";
            case 443:
                return "fb4a_srib_enabled";
            case AnonymousClass1Y3.A3m /*457*/:
                return "wide_tile";
            case 459:
                return "solid_heart";
            case AnonymousClass1Y3.A3o /*460*/:
                return "gif,sticker,emoji";
            case AnonymousClass1Y3.A3q /*462*/:
            case 737:
                return "DEFAULT";
            case AnonymousClass1Y3.A3w /*474*/:
                return "[{\"package_name\":\"com.lenovo.anyshare.gps\",\"download_directory\":\"SHAREit/apps\"},{\"package_name\":\"cn.xender\",\"download_directory\":\"Xender/app\"},{\"package_name\":\"com.vivo.easyshare\",\"download_directory\":\"EasyShare/app/\"},{\"package_name\":\"com.dewmobile.kuaiya.play\",\"download_directory\":\"Zapya/app\"}]";
            case AnonymousClass1Y3.A3x /*475*/:
                return "com.android.vending,com.google.market";
            case 476:
                return "{\"amzn\":[\"com.amazon.venezia\"]}";
            case 477:
                return "MODERATE,GOOD,EXCELLENT,UNKNOWN";
            case AnonymousClass1Y3.A3y /*478*/:
                return "GOOD,EXCELLENT";
            case 479:
                return "/fb4a_upgrade/?app_referrer=self_update_fallback_hc";
            case 480:
            case 484:
            case 485:
                return "86400000, 259200000, 1209600000";
            case AnonymousClass1Y3.A3z /*483*/:
                return "43200000, 21600000, 10800000, 5400000";
            case 486:
                return "fb://faceweb/f?href=/fb4a_upgrade/?app_referrer=in_app_update_fallback_hc";
            case 487:
                return "pk.eyJ1IjoiZmJtYXBzIiwiYSI6ImNqOGFmamkxdTBmbzUyd28xY3lybnEwamIifQ.oabgbuGc81ENlOJoPhv4OQ";
            case 488:
                return "mapbox://styles/fbmaps/ck3apprfh1eh91cm9dxj6a6xg";
            case 489:
                return "neutral";
            case AnonymousClass1Y3.A40 /*491*/:
                return "144.0";
            case AnonymousClass1Y3.A41 /*492*/:
                return "[\"fb_homescreen_shortcut\", \"home_screen_shortcut\"]";
            case AnonymousClass1Y3.A42 /*493*/:
                return "close_with_shape";
            case AnonymousClass1Y3.A44 /*495*/:
                return "[\"fb_feed_quick_promotion\"]";
            case AnonymousClass1Y3.A45 /*496*/:
                return "[{\"game_id\":\"141184676316522\", \"sticker_ids\":\"1302629199866051, 1302631776532460, 1302636003198704, 1302639676531670, 1302643773197927, 1302629826532655, 1302634093198895, 1302635756532062, 1302630649865906, 1302635356532102, 1302635016532136, 1302633656532272, 1302632663199038, 1302634706532167, 1302631499865821, 1302636786531959, 1302636279865343, 1302645076531130, 1302642226531415, 1302645766531061\"}, {\"game_id\":\"161713051139749\", \"sticker_ids\":\"195758681150030, 195758857816679, 195758974483334, 195765201149378, 195759261149972, 195765411149357, 195762761149622, 195762644482967, 195762854482946, 195762921149606, 195763024482929, 195763094482922, 195763167816248, 195763234482908, 195763337816231, 195763437816221, 195763707816194, 195764021149496, 195764424482789, 195765614482670, 195764647816100, 195764784482753, 195764977816067, 195759587816606\"}]";
            case AnonymousClass1Y3.A46 /*497*/:
                return "[\".apps.fbsbx.com\"]";
            case 500:
                return "POLLING";
            case AnonymousClass1Y3.A4A /*501*/:
                return "MANUAL_TRIGGER";
            case 502:
                return ECX.$const$string(52);
            case AnonymousClass1Y3.A4B /*503*/:
                return "override_sampling";
            case AnonymousClass1Y3.A4C /*505*/:
                return "https://www.facebook.com/marketplace/meeting_location/popover/";
            case AnonymousClass1Y3.A4D /*506*/:
                return "Pay Seller";
            case AnonymousClass1Y3.A4I /*515*/:
            case AnonymousClass1Y3.A4J /*516*/:
            case 603:
            case 604:
                return "{\"use_network_quality\":true,\"use_network_type\":false,\"excellent_value\":0,\"good_value\":0,\"moderate_value\":6500,\"poor_value\":6500,\"degraded_value\":6500,\"wifi_value\":0,\"cell_4g_value\":0,\"cell_3g_value\":0,\"cell_2g_value\":0,\"default_value\":0}";
            case AnonymousClass1Y3.A4N /*520*/:
                return "Phone Contacts Uploaded";
            case 521:
                return "invite";
            case AnonymousClass1Y3.A4O /*522*/:
                return "900000,3600000,28800000,86400000,-1";
            case 524:
                return "More Games";
            case AnonymousClass1Y3.A4Q /*525*/:
                return "fb-messenger://instant_games/list";
            case 526:
                return "android.resource://com.facebook.orca/drawable/messenger_button_icons_games_36";
            case AnonymousClass1Y3.A4S /*528*/:
                return "reminder/Set a reminder;gif/Send a GIF;sticker/Choose a sticker;weather/What's the weather?;event/Show me events nearby;music/Play a song";
            case AnonymousClass1Y3.A4T /*529*/:
                return "881263441913087";
            case 530:
                return "1579802578966277";
            case AnonymousClass1Y3.A4U /*531*/:
                return "506060876211905";
            case AnonymousClass1Y3.A4a /*539*/:
                return "starwarsid";
            case AnonymousClass1Y3.A4b /*541*/:
                return "https://www.facebook.com/help/messenger-app/2271207603094031";
            case AnonymousClass1Y3.A4c /*543*/:
                return "Messenger Kids";
            case AnonymousClass1Y3.A4d /*544*/:
                return "ALWAYS_SHOW";
            case 548:
            case AnonymousClass1Y3.A4e /*549*/:
                return "EMPTY";
            case 551:
                return "2017-08-03 08:16:33";
            case 552:
                return "Lorem Ipsum";
            case 554:
                return "MobileConfig is a cross-platform framework for Android and iOS apps to access server-side configurations";
            case 555:
                return "GOOGLE";
            case 561:
                return "pill-click-to-populate";
            case AnonymousClass1Y3.A4i /*562*/:
            case 564:
                return "app_foregrounded";
            case AnonymousClass1Y3.A4j /*563*/:
                return "app_backgrounded";
            case 565:
            case AnonymousClass1Y3.A4k /*566*/:
                return "background";
            case 570:
                return "legacy";
            case 574:
                return "COINFLIP";
            case AnonymousClass1Y3.A4p /*580*/:
                return "tfbid_972215336239651";
            case AnonymousClass1Y3.A4r /*583*/:
            case 584:
                return "viewer_rvp";
            case AnonymousClass1Y3.A4s /*585*/:
                return "initial_nt_view_id=financial_home";
            case 586:
                i3 = AnonymousClass1Y3.A1q;
                return C22298Ase.$const$string(i3);
            case 591:
                return "light_background_blue_text";
            case 592:
                return "#19FFFFFF";
            case 593:
                return "#CCFFFFFF";
            case AnonymousClass1Y3.A4u /*594*/:
                return "#FF4A4A4A";
            case 595:
                return "#33000000";
            case AnonymousClass1Y3.A4v /*597*/:
            case 598:
                return "900, 3600, 7200, 14400, 28800";
            case 599:
                return "*";
            case 606:
                i2 = 49;
                return AnonymousClass80H.$const$string(i2);
            case 607:
                return "video/avc";
            case AnonymousClass1Y3.A4w /*608*/:
                return "v0";
            case 612:
                return "#FF5496FF";
            case 613:
                return "TL_BR";
            case 614:
                return "#FF0548B3";
            case 615:
            case AnonymousClass1Y3.A4y /*616*/:
                return "465801630575151";
            case AnonymousClass1Y3.A4z /*618*/:
                return "536084263444203,747812612080694,1919119914775364,288507701553666,813394465510033";
            case AnonymousClass1Y3.A51 /*623*/:
                return "#80000000";
            case AnonymousClass1Y3.A53 /*626*/:
                return "/files/com.facebook.katana/fb_temp";
            case 629:
                return "https://lithium.facebook.com/lithium/stream";
            case AnonymousClass1Y3.A57 /*632*/:
                return "n_only";
            case 633:
                return "FOREGROUND";
            case AnonymousClass1Y3.A5A /*638*/:
                return "single_button";
            case AnonymousClass1Y3.A5C /*640*/:
                return "SATP_TEXT,SELFIE,BOOMERANG,POLL";
            case 642:
                return "CIRCLE_CAMERA";
            case AnonymousClass1Y3.A5D /*646*/:
                return "Animal,Pet,Food,Sports,Accessory,Transportation,Event";
            case AnonymousClass1Y3.A5E /*647*/:
                return "387985308700603";
            case 649:
                i3 = AnonymousClass1Y3.A1k;
                return C22298Ase.$const$string(i3);
            case AnonymousClass1Y3.A5G /*652*/:
                return "Nature,Landmark,Outdoor,Indoor,Accessory";
            case AnonymousClass1Y3.A5H /*653*/:
                return "STOP_MOTION#3, SELFIE#2, FUNFECT#1";
            case 655:
                return "293471731575187";
            case 656:
                return "108857293121207";
            case 658:
                return "maskrcnn2go_v1";
            case AnonymousClass1Y3.A5K /*659*/:
                return "mobile_photo_sharing_v2";
            case 661:
                return "309806243040912";
            case AnonymousClass1Y3.A5L /*663*/:
                return "438366093260424";
            case AnonymousClass1Y3.A5M /*665*/:
                return "606374399837094";
            case 667:
                return "1545465795475278";
            case AnonymousClass1Y3.A5O /*669*/:
                return "xraymobile_v3_embedding_calibrated";
            case 672:
                return "1590635757730412";
            case AnonymousClass1Y3.A5V /*679*/:
                return "opt_in_extended";
            case 688:
                return "image/jpeg";
            case AnonymousClass1Y3.A5a /*690*/:
                return "MobileConfigModule,PlatformConstants,UIManager,DeviceInfo,FrameRateLogger,PrimedStorage,I18nAssets,RelayNativeQueryVariables,FBMarketplaceNativeModule,RelayAPIConfig,RelayPrefetcher,VisitationManager,FbIcon,I18n,I18nManager,AnalyticsFunnelLogger,Analytics,ReactPerformanceLogger";
            case 691:
                return "1967355726851672";
            case 692:
                return "217";
            case AnonymousClass1Y3.A5b /*693*/:
                return "Send money to your friends for free by using Paymaya or GCash. No account needed to get started.";
            case AnonymousClass1Y3.A5c /*694*/:
                return "Sign Up to Send Money";
            case AnonymousClass1Y3.A5d /*695*/:
                return "GCash";
            case AnonymousClass1Y3.A5e /*696*/:
                return "899232800166159";
            case AnonymousClass1Y3.A5f /*697*/:
                return "PayMaya";
            case 698:
                return "115016608835194";
            case AnonymousClass1Y3.A5g /*699*/:
                return "PHP";
            case AnonymousClass1Y3.A5h /*700*/:
                return "Tap [ICON] Digital Wallet to send money from GCash with no fee!";
            case 705:
                return "655382,655379";
            case AnonymousClass1Y3.A5k /*706*/:
                return "919393791527666,1223100494419426";
            case 722:
                return "236096983481914";
            case AnonymousClass1Y3.A61 /*738*/:
                return "on_call_end";
            case 752:
                return "Sparkles_universal";
            case 765:
                return "scout_det_gbdt_v0";
            case AnonymousClass1Y3.A6G /*768*/:
                return "{\"feedback_reactions\": 4, \"messaging_reactions\": 4, \"messaging_video_reactions\": 4}";
            case 769:
                return "compactdisk";
            case 770:
                return "filesreactmobileconfigmetadatajson,mixed_cache__cold_effect_asset_disk_cache,mixed_cache__hot_effect_asset_disk_cache,msqrd_effect_asset_disk_cache_fixed_sessionless,msqrd_effect_asset_disk_cache_fixed,msqrd_effect_asset_disk_cache_pinned_sessionlesss,msqrd_effect_asset_disk_cache_secure,msqrd_hand_tracking_asset_disk_cache_sessionless,msqrd_model_asset_disk_cache_sessionless,msqrd_model_asset_disk_cache_sessionless_sdcard_model,msqrd_segmentation_asset_disk_cache_sessionlesss,msqrd_target_recognition_asset_disk_cache_sessionless,msqrd_xray_asset_disk_cache_sessionless,msqrd_,mixed_cache_";
            case 772:
            case 774:
                return "https://graph.facebook.com/glb_map";
            case AnonymousClass1Y3.A6H /*773*/:
                return "access_token=432827354065804|cb9c2da18237a3bb72878cc3a28019ad&get_fna_candidates=true";
            case AnonymousClass1Y3.A6I /*775*/:
                return "access_token=432827354065804|cb9c2da18237a3bb72878cc3a28019ad&get_all_clusters=true";
            case AnonymousClass1Y3.A6J /*777*/:
                return "send_stars_with_sound";
            case 778:
                return "https://lookaside.facebook.com/assets/691964817853488/";
            case AnonymousClass1Y3.A6K /*779*/:
            case AnonymousClass1Y3.A6Q /*788*/:
                return "1019203741621038";
            case 790:
                return "404182480147564:WOW,346652875948021:Hello,392866144826317:fire,438126933591755:OMG,551065192044557:COOL,776485922684447:LOL";
            case 793:
                return "^(https?)://(graph|graph2|z-m-graph|b-graph|z-p3-graph|z-p4-graph)\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5})($|\\?.*$|/.*$)";
            case AnonymousClass1Y3.A6S /*794*/:
                return "$1://z-m-graph.$3facebook.com$4$5";
            case 795:
                return "[\"ComposerPrivacyOptionsQuery\",\"ComposerStoryCreateMutation\",\"FetchComposerPrivacyGuardrailInfo\"]";
            case 796:
                return "[\"zero_balance_webview\"]";
            case AnonymousClass1Y3.A6T /*797*/:
                return "[\"mobile_data_consumption_limit\",\"disabled_by_position_budget\",\"disabled_by_data_sensitivity_test\"]";
            case 798:
                return "_nc_bt=";
            case 799:
                return "(<BaseURL>)(https?://(?:z-1-|z-m-|z-p3-|z-p4-|)(?:[0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(?:\\.xx\\.fbcdn\\.net:?[0-9]{0,5})/.*?(?:\\.mp4|\\.hls|\\.flv)\\?.*?(?:_nc_ad=z-m&amp;_nc_pv=5)(?:.*?oh=.*?))(</BaseURL>)";
            case AnonymousClass1Y3.A6U /*800*/:
                return "(<BaseURL>)(.*?)(</BaseURL>)";
            case AnonymousClass1Y3.A6V /*801*/:
                return "$1$2&amp;_nc_bt=$3";
            case 802:
                return "$1$2&amp;_nc_pf=1$3";
            case AnonymousClass1Y3.A6W /*803*/:
            case 808:
                return "$1$2_nc_ad=z-m$8$9";
            case AnonymousClass1Y3.A6X /*804*/:
                return "(<BaseURL>)(https?://(z-1-|z-m-|z-p3-|z-p4-|)([0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(\\.xx\\.fbcdn\\.net:?[0-9]{0,5})/.*?(\\.mp4|\\.hls|\\.flv)\\?.*?)(_nc_ad=z-m&amp;_nc_pv=5)(.*?oh=.*?)(</BaseURL>)";
            case 805:
            case 810:
                return "$1$2_nc_ad=z-m&amp;_nc_pv=5$8$9";
            case AnonymousClass1Y3.A6Y /*806*/:
                return "(<BaseURL>)(https?://(z-1-|z-m-|z-p3-|z-p4-|)([0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(\\.xx\\.fbcdn\\.net:?[0-9]{0,5})/.*?(\\.mp4|\\.hls|\\.flv)\\?.*?)(_nc_ad=z-m)(?!.*?_nc_pv=)(.*?oh=.*?)(</BaseURL>)";
            case AnonymousClass1Y3.A6Z /*807*/:
                return "(<BaseURL>)(https?://(?:z-1-|z-m-|z-p3-|z-p4-|)(?:[0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(?:f\\w+-\\w+\\.fna\\.fbcdn\\.net:?[0-9]{0,5})/.*?(?:\\.mp4|\\.hls|\\.flv)\\?.*?(?:_nc_ad=z-m&amp;_nc_pv=5)(?:.*?oh=.*?))(</BaseURL>)";
            case AnonymousClass1Y3.A6a /*809*/:
                return "(<BaseURL>)(https?://(z-1-|z-m-|z-p3-|z-p4-|)([0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(f\\w+-\\w+\\.fna\\.fbcdn\\.net:?[0-9]{0,5})/.*?(\\.mp4|\\.hls|\\.flv)\\?.*?)(_nc_ad=z-m&amp;_nc_pv=5)(.*?oh=.*?)(</BaseURL>)";
            case 811:
                return "(<BaseURL>)(https?://(z-1-|z-m-|z-p3-|z-p4-|)([0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(f\\w+-\\w+\\.fna\\.fbcdn\\.net:?[0-9]{0,5})/.*?(\\.mp4|\\.hls|\\.flv)\\?.*?)(_nc_ad=z-m)(?!.*?_nc_pv=)(.*?oh=.*?)(</BaseURL>)";
            case 812:
            case 817:
                return "$1_nc_ad=z-m$7$8";
            case 813:
                return "^(https?://(z-1-|z-m-|z-p3-|z-p4-|)([0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(f\\w+-\\w+\\.fna\\.fbcdn\\.net:?[0-9]{0,5})/.*?(\\.mp4|\\.hls|\\.flv)\\?.*?)(_nc_ad=z-m&_nc_pv=5)(.*?oh=.*?)(.*$)";
            case 814:
            case AnonymousClass1Y3.A6e /*819*/:
                return "$1_nc_ad=z-m&_nc_pv=5$7$8";
            case AnonymousClass1Y3.A6b /*815*/:
                return "^(https?://(z-1-|z-m-|z-p3-|z-p4-|)([0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(f\\w+-\\w+\\.fna\\.fbcdn\\.net:?[0-9]{0,5})/.*?(\\.mp4|\\.hls|\\.flv)\\?.*?)(_nc_ad=z-m)(?!.*?_nc_pv=)(.*?oh=.*?)(.*$)";
            case AnonymousClass1Y3.A6c /*816*/:
                return "_nc_pf=1";
            case AnonymousClass1Y3.A6d /*818*/:
                return "^(https?://(z-1-|z-m-|z-p3-|z-p4-|)([0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(\\.xx\\.fbcdn\\.net:?[0-9]{0,5})/.*?(\\.mp4|\\.hls|\\.flv)\\?.*?)(_nc_ad=z-m&_nc_pv=5)(.*?oh=.*?)(.*$)";
            case 820:
                return "^(https?://(z-1-|z-m-|z-p3-|z-p4-|)([0-9a-zA-Z\\.-]+)(?:-[0-9a-zA-Z]+-sonar)?(\\.xx\\.fbcdn\\.net:?[0-9]{0,5})/.*?(\\.mp4|\\.hls|\\.flv)\\?.*?)(_nc_ad=z-m)(?!.*?_nc_pv=)(.*?oh=.*?)(.*$)";
            default:
                switch (A01) {
                    case Process.SIGKILL:
                    case 13:
                        return "BALANCED_POWER_AND_ACCURACY";
                    case AnonymousClass1Y3.A01 /*10*/:
                        return "gcm";
                    case AnonymousClass1Y3.A02 /*11*/:
                    case AnonymousClass1Y3.A03 /*12*/:
                        return "centroid";
                    default:
                        switch (A01) {
                            case AnonymousClass1Y3.A0D /*37*/:
                                return "profile_with_link";
                            case AnonymousClass1Y3.A0E /*38*/:
                                return "thumbnail";
                            case AnonymousClass1Y3.A0F /*39*/:
                                return "on_context_card";
                            case AnonymousClass1Y3.A0G /*40*/:
                                return "format-optimization-v6";
                            case AnonymousClass1Y3.A0H /*41*/:
                                return "regular";
                            default:
                                switch (A01) {
                                    case AnonymousClass1Y3.A0J /*45*/:
                                        return "directshow";
                                    case AnonymousClass1Y3.A0K /*46*/:
                                    case AnonymousClass1Y3.A0L /*47*/:
                                        return "default";
                                    case 48:
                                        return "showreel_cache";
                                    default:
                                        switch (A01) {
                                            case 50:
                                                return "[\"com.facebook.katana.activity.media.ViewVideoActivity\",\"com.facebook.photos.albums.video.VideoAlbumLaunchPlayerActivity\",\"com.facebook.photos.taggablegallery.ProductionVideoGalleryActivity\"]";
                                            case AnonymousClass1Y3.A0M /*51*/:
                                                return "oh";
                                            case AnonymousClass1Y3.A0N /*52*/:
                                                return "[\"com.facebook.messaging.games.GamesSelectionActivity\",\"com.facebook.payments.p2p.P2pPaymentActivity\",\"com.facebook.messaging.business.common.activity.BusinessActivity\"]";
                                            case AnonymousClass1Y3.A0O /*53*/:
                                                return "[\"^/report.*\",\"^/follow/feedsources.*\",\"^/ads/preference.*\",\"^/settings.*\",\"^/help/android-app.*\",\"^/\\\\d.*/allactivity.*\",\"^/privacy.*\",\"^/about/privacy.*\",\"^/policies.*\",\"^/about/basics/.*\",\"https://m.facebook.com/help/contact/.*\",\"^/terms.*\",\"^/policy.*\",\"^/trust.*\",\"^/communitystandards.*\",\"^/ad_guidelines.*\",\"^/page_guidelines.*\",\"^/payments_terms.*\",\"^/help.*\",\"^/pages/create.*\",\"https://m.facebook.com/groups/create/.*\",\"^/invite/history.*\",\"https://(www|m).facebook.com/safetycheck.*\",\"^(https://m.facebook.com)?/zero/toggle/settings($|\\\\?.*$|/.*$)\",\"https://(www|m).facebook.com/events/birthdays.*\",\"https://m.facebook.com/.*/about.*\",\"https://m.facebook.com/timeline/app_section/.*\",\"^/allactivity/options\\\\?id=.*\",\"^/survey.*\",\"^https://m.facebook.com/a/approval_queue/.*\",\"^/legal/thirdpartynotices\",\"^/deactivatewhitelist/.*\",\"^/upsell/loyaltytopup/accept/.*\"]";
                                            case 54:
                                                return "[\"LIGHTSWITCH_OPTIN_INTERSTITIAL\",\"DIALTONE_OPTIN_INTERSTITIAL\",\"ZERO_OPTIN_INTERSTITIAL\",\"thumbnail\",\"map\",\"about\",\"privacy\",\"attachment_icon\",\"search\",\"MODULE_TYPEAHEAD_SEARCH\",\"MODULE_BOOKMARKS\",\"MODULE_EVENT_DASHBOARD\",\"MODULE_EVENT_SUGGESTIONS\",\"MODULE_EVENT_SUBSCRIPTIONS\",\"PAGE_EVENTS_LIST\",\"event_dashboard\",\"event_subscriptions\",\"event_profile_pic\",\"COMPOSER\",\"INLINE_COMPOSER\",\"NOTIFICATIONS_VIEW\",\"DEVICE_BASED_LOGIN\",\"REACTION_DIALOG\",\"IORG_COMMON\",\"MODULE_QRCODE\",\"FEED_AWESOMIZER\",\"^http://www.facebook.com/images/emoji/unicode.*\",\"group_admin_cog_icon\",\"event_profile_pic\",\"fresco_impl\",\"bookmarks\",\"new_place_creation\",\"qr_code\", \"landing\", \"FullscreenRichVideoPlayerPluginSelector\", \"effects_bottom_tray_in_stories\", \"creative_editing_in_composer\", \"photo_thread_view\", \"effects_bottom_tray_in_messenger_day\", \"folder_item\", \"NTZeroUpgradeController\", \"GroupsTargetedTabGroupListItemThumbnail\", \"InlineComposerProfileComponentSpec\", \"NotificationsComponentSpec\"]";
                                            case 55:
                                                return "[\"^(https?)://(m-|z-m-|z-1-|z-n-|)static\\\\.(ak|xx)\\\\.fbcdn\\\\.net/rsrc\\\\.php($|\\\\?.*$|/.*$)\",\"^file:/.*\",\"^res:/.*\",\"^(https?)://z-m.*t[^/-]+\\\\.2200-6.*\"]";
                                            case AnonymousClass1Y3.A0P /*56*/:
                                                return "[\"https://0.freebasics.com/?ref=android_bookmark\", \"https://free.facebook.com/zero/optin/legal/\"]";
                                            case 57:
                                                return "[\"video_cover_image\",\"inline_video_cover_image\",\"PAGE_MODULE_IDENTITY_VIDEO\",\"REACTION_DIALOG_VIDEOS\"]";
                                            case AnonymousClass1Y3.A0Q /*58*/:
                                                return "_nc_ad";
                                            case 59:
                                                return "callsite_a,callsite_b";
                                            default:
                                                switch (A01) {
                                                    case AnonymousClass1Y3.A0W /*73*/:
                                                        return "H4sIAAAAAAAAA9XabW/aMBAA4L8S+TNZ47fY4RvqUGAFhkq3Sl2rKAID1kKCklRlqvrfp0A1gXqmL2sb5zOWeTjO58vBPSpWcV6ul1mqonWcZpFO51mU6KJE7V/3aBqvVB5Hq2ymku1LqH2PVvFvhdqos14nCrXQ9kXURnpc7eKwCXpoofltkuw2vNOzconaOKCUtdBKp1GW60W0VHqxLFGbEOm10DybxkmUqHRRrUXVrvEmipMku1Oz3RbRTC1ypQrUpr73+K6RLqJ1ruZ6g9rzOCnUQ+tNZg6TJSfYWvK0geYCNgsiAlvNvoEsKBhmz7OA7IyTW1OobXbDZoIFAdNDWmE2B9tq+KTbPLMwkLc2O3NaHEuPV7hpEFyfYM+zyM+oD/sJ5KfXJ7wePNk0jx/PnI7O4RLOJIbcWJJa031HdoipLcEEvuJZzeihTrVJbW+ot2rWtFiP88xicphliwPzWG9UAoM5Y9DTDOaCfx64iFfFbbrYE0+Gbod7Hmj2mfQBsiTkuI9xwFfmt+rpB30TOOQUBgsmoV5VElYv+JswRVhQqHeSmNcLHgUGMJYUDDHBuNakCHtuXzC43ZOcQjU5EPJ5MXAw3+vgVWRO4UGCveSBwHBm2EvuUwonBpZyq3vaTwtRK/q0Sg1mmjFZih5XyUFMZcNSdNhzhwEOmoU+7bnngWhWpMMLtx/wjzB/3KVSHcOANyw5wp478ppWO7bXN4GrNPXB+dKuD6mzC5WB1zH1+hye0/AXNKIfGOahGwaGIyg8cLaBdyOPWltR3DBxGBiGA5yDaYFZ3eLRa8UWxJjCI1KPHBmR1iuGa3IlA8WUQY/5sOi5ecEbs8LiGPd+dC67/T1wZ9B1BwSuFQTLasryVMxecom8V0EehE43UdMyz1I9Lfbog9D9Kg3ZQQQHb2yffd71dwzek4aQc59BIadUfF6SHIMPJoHhZvk/+mdky89JIOGfmRthtzruVxfdPfDVRdfpEA8bBghUcgqO+713HODdtFC5zJWKCr0p/0QH+H//u3nU53qaLff85VKVsVNsT8rRJSvM9xeBF8Ip2U4FX/Q1n2PvYMNR/+z7aG/Jmfoz1EWhs9Sh/sGunanKnX46/bK3+tTDzy65rNbcPPwFEaPV+KEkAAA=";
                                                    case 74:
                                                        return "fleetbeacon_exp_polling_alpha";
                                                    case AnonymousClass1Y3.A0X /*75*/:
                                                        return "fleetbeacon_exp_polling_beta";
                                                    case AnonymousClass1Y3.A0Y /*76*/:
                                                        return "fleetbeacon_exp_polling_gamma";
                                                    case AnonymousClass1Y3.A0Z /*77*/:
                                                        return "fleetbeacon_exp_exl_alpha";
                                                    case 78:
                                                        return "fleetbeacon_exp_exl_beta";
                                                    case 79:
                                                        return "fleetbeacon_exp_exl_gamma";
                                                    case AnonymousClass1Y3.A0a /*80*/:
                                                    case AnonymousClass1Y3.A0b /*81*/:
                                                    case 82:
                                                    case 83:
                                                        return "medium_power";
                                                    default:
                                                        switch (A01) {
                                                            case 87:
                                                                return "invalid config";
                                                            case 88:
                                                                return "DEFAULT";
                                                            case AnonymousClass1Y3.A0d /*89*/:
                                                                return "{}";
                                                            case AnonymousClass1Y3.A0e /*90*/:
                                                            case 91:
                                                            case 92:
                                                            case 93:
                                                                return "[]";
                                                            case AnonymousClass1Y3.A0f /*94*/:
                                                                return "live_video_comment_create_subscribe";
                                                            case 95:
                                                                return "420:2;3";
                                                            default:
                                                                return BuildConfig.FLAVOR;
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
    }
}
