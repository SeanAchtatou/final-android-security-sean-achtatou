package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.messaging.neue.nux.NeueNuxActivity;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;

/* renamed from: X.0zu  reason: invalid class name and case insensitive filesystem */
public final class C18000zu implements C186714q {
    private boolean A00;
    private final AnonymousClass0XN A01;
    private final FbSharedPreferences A02;
    private final C04310Tq A03;
    private final C04310Tq A04;
    private final C04310Tq A05;
    private final C04310Tq A06;
    private final boolean A07;

    public boolean CE5() {
        return false;
    }

    public static final C18000zu A00(AnonymousClass1XY r8) {
        return new C18000zu(AnonymousClass0VG.A00(AnonymousClass1Y3.AJK, r8), AnonymousClass0VG.A00(AnonymousClass1Y3.AWa, r8), AnonymousClass0VG.A00(AnonymousClass1Y3.BHj, r8), FbSharedPreferencesModule.A00(r8), AnonymousClass0VG.A00(AnonymousClass1Y3.BDv, r8), AnonymousClass0XM.A00(r8), AnonymousClass0UU.A08(r8));
    }

    public Integer AeZ() {
        return AnonymousClass07B.A0Y;
    }

    public Intent AqX(Activity activity) {
        Bundle bundle;
        Intent intent = new Intent(activity, NeueNuxActivity.class);
        String str = null;
        if (activity.getIntent() != null) {
            bundle = activity.getIntent().getExtras();
        } else {
            bundle = null;
        }
        if (bundle != null && "pick_media_surface".equals(bundle.getString("pick_media_dialog_surface", BuildConfig.FLAVOR))) {
            this.A00 = true;
            return null;
        } else if (!this.A00 || bundle == null || !bundle.containsKey(AnonymousClass24B.$const$string(70))) {
            if (((Boolean) this.A06.get()).booleanValue()) {
                if (this.A07) {
                    str = "workchat_nux_flow";
                } else if (this.A02.Aep(C10980lB.A0B, false)) {
                    str = AnonymousClass80H.$const$string(AnonymousClass1Y3.A2v);
                } else if (this.A02.Aep(C10980lB.A09, false)) {
                    str = AnonymousClass80H.$const$string(AnonymousClass1Y3.A4z);
                } else if (this.A02.Aep(AnonymousClass4YU.A01, false)) {
                    str = AnonymousClass80H.$const$string(570);
                } else if (((Boolean) this.A04.get()).booleanValue()) {
                    str = AnonymousClass80H.$const$string(AnonymousClass1Y3.A4j);
                } else {
                    str = AnonymousClass80H.$const$string(486);
                }
            } else if (((Boolean) this.A03.get()).booleanValue()) {
                str = AnonymousClass80H.$const$string(AnonymousClass1Y3.A3j);
            }
            Preconditions.checkNotNull(str, "No NUX to show!");
            intent.putExtra(C99084oO.$const$string(AnonymousClass1Y3.A1L), str);
            return intent;
        } else {
            this.A00 = false;
            return null;
        }
    }

    public boolean BDw(Context context) {
        if (!this.A01.A0I() || C006006f.A01()) {
            return false;
        }
        return ((Boolean) this.A05.get()).booleanValue();
    }

    public boolean CEE(Activity activity) {
        if (!(activity instanceof C27891dv)) {
            if (!C138546dM.class.isAssignableFrom(activity.getClass())) {
                return true;
            }
        }
        return false;
    }

    private C18000zu(C04310Tq r2, C04310Tq r3, C04310Tq r4, FbSharedPreferences fbSharedPreferences, C04310Tq r6, AnonymousClass0XN r7, Boolean bool) {
        this.A05 = r2;
        this.A06 = r3;
        this.A03 = r4;
        this.A02 = fbSharedPreferences;
        this.A04 = r6;
        this.A01 = r7;
        this.A07 = bool.booleanValue();
    }
}
