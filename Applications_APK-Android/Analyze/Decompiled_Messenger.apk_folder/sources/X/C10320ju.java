package X;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/* renamed from: X.0ju  reason: invalid class name and case insensitive filesystem */
public final class C10320ju implements Serializable {
    private static final long serialVersionUID = 1;
    public final C10300js _factory;

    public static IllegalArgumentException _problem(C189068qJ r6, String str) {
        String str2 = r6._input;
        return new IllegalArgumentException(AnonymousClass08S.A0U("Failed to parse type '", str2, "' (remaining: '", str2.substring(r6._index), "'): ", str));
    }

    public C10320ju(C10300js r1) {
        this._factory = r1;
    }

    public static C10030jR parseType(C10320ju r6, C189068qJ r7) {
        String str;
        C10030jR r1;
        if (r7.hasMoreTokens()) {
            String nextToken = r7.nextToken();
            try {
                Class findClass = C29081fq.findClass(nextToken);
                if (r7.hasMoreTokens()) {
                    String nextToken2 = r7.nextToken();
                    if ("<".equals(nextToken2)) {
                        C10300js r3 = r6._factory;
                        ArrayList arrayList = new ArrayList();
                        while (true) {
                            if (!r7.hasMoreTokens()) {
                                break;
                            }
                            arrayList.add(parseType(r6, r7));
                            if (!r7.hasMoreTokens()) {
                                break;
                            }
                            String nextToken3 = r7.nextToken();
                            if (!">".equals(nextToken3)) {
                                if (!",".equals(nextToken3)) {
                                    str = AnonymousClass08S.A0P("Unexpected token '", nextToken3, "', expected ',' or '>')");
                                    break;
                                }
                            } else if (findClass.isArray()) {
                                return BMA.construct(r3._constructType(findClass.getComponentType(), null), null, null);
                            } else {
                                if (!findClass.isEnum()) {
                                    if (Map.class.isAssignableFrom(findClass)) {
                                        if (arrayList.size() <= 0) {
                                            return C10300js._mapType(r3, findClass);
                                        }
                                        C10030jR r32 = (C10030jR) arrayList.get(0);
                                        if (arrayList.size() >= 2) {
                                            r1 = (C10030jR) arrayList.get(1);
                                        } else {
                                            r1 = new C10020jP(Object.class);
                                        }
                                        return C180610a.construct(findClass, r32, r1);
                                    } else if (Collection.class.isAssignableFrom(findClass)) {
                                        if (arrayList.size() >= 1) {
                                            return C28331ed.construct(findClass, (C10030jR) arrayList.get(0));
                                        }
                                        return C10300js._collectionType(r3, findClass);
                                    } else if (arrayList.size() != 0) {
                                        return C10300js.constructSimpleType(findClass, (C10030jR[]) arrayList.toArray(new C10030jR[arrayList.size()]));
                                    }
                                }
                                return new C10020jP(findClass);
                            }
                        }
                    } else {
                        r7._pushbackToken = nextToken2;
                        r7._index -= nextToken2.length();
                    }
                }
                return r6._factory._fromClass(findClass, null);
            } catch (Exception e) {
                if (e instanceof RuntimeException) {
                    throw ((RuntimeException) e);
                }
                str = AnonymousClass08S.A0S("Can not locate class '", nextToken, "', problem: ", e.getMessage());
            }
        }
        str = "Unexpected end-of-string";
        throw _problem(r7, str);
    }
}
