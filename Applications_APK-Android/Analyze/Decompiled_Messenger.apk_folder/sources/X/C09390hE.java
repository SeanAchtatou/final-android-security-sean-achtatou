package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0hE  reason: invalid class name and case insensitive filesystem */
public final class C09390hE {
    private static volatile C09390hE A07;
    public int A00;
    public long A01;
    public AnonymousClass0UN A02;
    public boolean A03;
    public final C183111u A04;
    public volatile C22361La A05;
    public volatile EVQ A06;

    public static final C09390hE A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C09390hE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C09390hE(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static EVQ A01(C09390hE r4) {
        if (!r4.A03 || !AnonymousClass1QP.A01()) {
            return null;
        }
        if (r4.A06 == null) {
            synchronized (r4) {
                if (r4.A06 == null) {
                    r4.A00 = ((C25051Yd) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AOJ, r4.A02)).AqL(564302868120321L, 300);
                    C22361La A042 = ((DeprecatedAnalyticsLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AR0, r4.A02)).A04("android_transient_analytics", false);
                    if (A042.A0B()) {
                        r4.A05 = A042;
                        C183111u r0 = r4.A04;
                        r4.A06 = new EVQ(AnonymousClass0Ud.A00(r0), AnonymousClass067.A03(r0), r4.A00);
                    }
                }
            }
        }
        return r4.A06;
    }

    private C09390hE(AnonymousClass1XY r3) {
        this.A02 = new AnonymousClass0UN(5, r3);
        this.A04 = new C183111u(r3);
    }

    public void A02(String str, String str2) {
        EVQ A012 = A01(this);
        if (A012 != null) {
            A012.A01("click", "module: %s, object_type: %s", str, str2);
        }
    }
}
