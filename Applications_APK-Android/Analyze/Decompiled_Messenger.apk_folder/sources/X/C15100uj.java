package X;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import com.facebook.auth.viewercontext.ViewerContext;
import com.google.common.collect.ImmutableMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0uj  reason: invalid class name and case insensitive filesystem */
public final class C15100uj {
    private static volatile C15100uj A04;
    public AnonymousClass0UN A00;
    public Map A01;
    public final NotificationManager A02;
    public final Context A03;

    private Uri A02(String str, Uri uri) {
        if (uri == null || str.contains("messenger_orca_749_voip_incoming")) {
            return null;
        }
        if (str.contains("messenger_orca_100_mentioned")) {
            return ((AnonymousClass32J) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BAr, this.A00)).A03();
        }
        return ((AnonymousClass32J) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BAr, this.A00)).A04();
    }

    private NotificationChannelGroup A01(String str) {
        for (NotificationChannelGroup next : this.A02.getNotificationChannelGroups()) {
            if (next.getId().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public static final C15100uj A03(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C15100uj.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C15100uj(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static AnonymousClass1Y7 A04(C15100uj r3) {
        String str;
        ViewerContext viewerContext = (ViewerContext) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AfE, r3.A00);
        if (viewerContext == null || (str = viewerContext.mUserId) == null) {
            return null;
        }
        return (AnonymousClass1Y7) C05690aA.A0v.A09(str);
    }

    public static List A06(C15100uj r2) {
        try {
            return r2.A02.getNotificationChannels();
        } catch (NullPointerException e) {
            C010708t.A0L("MessengerNotificationChannelManager", "Unexpected NullPointerException from NotificationManager.getNotificationChannels()", e);
            return Collections.emptyList();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:150:0x02e1, code lost:
        if (r3 == 0) goto L_0x02e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0321, code lost:
        if (r3 == 0) goto L_0x0323;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01ae A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x01be  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x01ed  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0207  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0220  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x03e6  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0088 A[SYNTHETIC, Splitter:B:26:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0096 A[SYNTHETIC, Splitter:B:35:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0112  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A08(X.C15100uj r17) {
        /*
            int r2 = X.AnonymousClass1Y3.Ady
            r0 = r17
            X.0UN r1 = r0.A00
            r6 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1lk r1 = (X.C32431lk) r1
            r1.A05()
            X.1Y7 r4 = A04(r0)
            r5 = 1
            if (r4 == 0) goto L_0x010e
            java.util.List r1 = A06(r0)
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0068
            java.util.List r1 = A06(r0)
            java.util.Iterator r2 = r1.iterator()
        L_0x0029:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x0068
            java.lang.Object r1 = r2.next()
            android.app.NotificationChannel r1 = (android.app.NotificationChannel) r1
            java.lang.String r1 = r1.getId()
            boolean r1 = A0A(r1)
            if (r1 == 0) goto L_0x0029
            r1 = 0
        L_0x0040:
            if (r1 == 0) goto L_0x010e
            int r2 = X.AnonymousClass1Y3.BCf
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)
            X.0ui r1 = (X.C15090ui) r1
            boolean r1 = r1.A05()
            if (r1 == 0) goto L_0x010e
            r3 = 2
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1
            r7 = 0
            java.lang.String r2 = r1.B4F(r4, r7)
            java.lang.String r4 = "Failed to deserialize the channel config"
            r3 = 0
            if (r2 == 0) goto L_0x00a0
            goto L_0x006a
        L_0x0068:
            r1 = 1
            goto L_0x0040
        L_0x006a:
            byte[] r1 = android.util.Base64.decode(r2, r6)     // Catch:{ IOException | ClassNotFoundException -> 0x0093, all -> 0x0085 }
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ IOException | ClassNotFoundException -> 0x0093, all -> 0x0085 }
            r2.<init>(r1)     // Catch:{ IOException | ClassNotFoundException -> 0x0093, all -> 0x0085 }
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ IOException | ClassNotFoundException -> 0x0093, all -> 0x0085 }
            r1.<init>(r2)     // Catch:{ IOException | ClassNotFoundException -> 0x0093, all -> 0x0085 }
            java.lang.Object r3 = r1.readObject()     // Catch:{ IOException | ClassNotFoundException -> 0x0094, all -> 0x0082 }
            X.1ll r3 = (X.C32441ll) r3     // Catch:{ IOException | ClassNotFoundException -> 0x0094, all -> 0x0082 }
            r1.close()     // Catch:{ IOException -> 0x00a2 }
            goto L_0x00a8
        L_0x0082:
            r2 = move-exception
            r3 = r1
            goto L_0x0086
        L_0x0085:
            r2 = move-exception
        L_0x0086:
            if (r3 == 0) goto L_0x0092
            r3.close()     // Catch:{ IOException -> 0x008c }
            goto L_0x0092
        L_0x008c:
            r1 = move-exception
            java.lang.String r0 = "MessengerNotificationChannelManager"
            X.C010708t.A0R(r0, r1, r4)
        L_0x0092:
            throw r2
        L_0x0093:
            r1 = r7
        L_0x0094:
            if (r1 == 0) goto L_0x00a0
            r1.close()     // Catch:{ IOException -> 0x009a }
            goto L_0x00a0
        L_0x009a:
            r2 = move-exception
            java.lang.String r1 = "MessengerNotificationChannelManager"
            X.C010708t.A0R(r1, r2, r4)
        L_0x00a0:
            r3 = r7
            goto L_0x00a8
        L_0x00a2:
            r2 = move-exception
            java.lang.String r1 = "MessengerNotificationChannelManager"
            X.C010708t.A0R(r1, r2, r4)
        L_0x00a8:
            if (r3 == 0) goto L_0x010e
            r2 = 1
            if (r3 == 0) goto L_0x00b2
            int r1 = r3.mVersion
            if (r1 < r5) goto L_0x00b2
            r2 = 0
        L_0x00b2:
            if (r2 != 0) goto L_0x010e
            java.util.Map r1 = r3.mChannelSettings
            java.util.Collection r1 = r1.values()
            java.util.Iterator r8 = r1.iterator()
        L_0x00be:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x0101
            java.lang.Object r4 = r8.next()
            X.0sc r4 = (X.C14100sc) r4
            java.lang.String r1 = r4.mSoundUri
            if (r1 != 0) goto L_0x00fc
            r1 = 0
        L_0x00cf:
            if (r1 == 0) goto L_0x00be
            android.content.Context r2 = r0.A03
            java.lang.String r1 = r4.mSoundUri
            if (r1 != 0) goto L_0x00f7
            r1 = r7
        L_0x00d8:
            android.net.Uri r1 = X.C71283c8.A00(r2, r1)
            if (r1 != 0) goto L_0x00be
            java.lang.String r2 = r4.mChannelId
            java.lang.String r1 = r4.mSoundUri
            if (r1 != 0) goto L_0x00f2
            r1 = r7
        L_0x00e5:
            android.net.Uri r1 = r0.A02(r2, r1)
            if (r1 == 0) goto L_0x00be
            java.lang.String r1 = r1.toString()
            r4.mSoundUri = r1
            goto L_0x00be
        L_0x00f2:
            android.net.Uri r1 = android.net.Uri.parse(r1)
            goto L_0x00e5
        L_0x00f7:
            android.net.Uri r1 = android.net.Uri.parse(r1)
            goto L_0x00d8
        L_0x00fc:
            android.net.Uri r1 = android.net.Uri.parse(r1)
            goto L_0x00cf
        L_0x0101:
            int r2 = X.AnonymousClass1Y3.Ady
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1lk r1 = (X.C32431lk) r1
            r1.A06(r3)
        L_0x010e:
            java.util.Map r1 = r0.A01
            if (r1 != 0) goto L_0x0198
            int r2 = X.AnonymousClass1Y3.BCf
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r2, r1)
            X.0ui r2 = (X.C15090ui) r2
            boolean r1 = r2.A03()
            if (r1 != 0) goto L_0x0127
            r13 = 0
        L_0x0123:
            r8 = 0
            if (r13 == 0) goto L_0x0196
            goto L_0x0133
        L_0x0127:
            X.1Yd r3 = r2.A01
            r1 = 845580276662430(0x3010d0005009e, double:4.177721654998434E-309)
            java.lang.String r13 = r3.B4C(r1)
            goto L_0x0123
        L_0x0133:
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            r11.<init>(r13)     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            java.util.Iterator r12 = r11.keys()     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            java.util.HashMap r10 = new java.util.HashMap     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            r10.<init>()     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
        L_0x0141:
            boolean r1 = r12.hasNext()     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            if (r1 == 0) goto L_0x0195
            java.lang.Object r9 = r12.next()     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            r7.<init>()     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            java.lang.Object r4 = r11.get(r9)     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            org.json.JSONObject r4 = (org.json.JSONObject) r4     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            java.util.Iterator r3 = r4.keys()     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
        L_0x015c:
            boolean r1 = r3.hasNext()     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            if (r1 == 0) goto L_0x017a
            java.lang.Object r2 = r3.next()     // Catch:{ NumberFormatException -> 0x0196 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ NumberFormatException -> 0x0196 }
            int r1 = java.lang.Integer.parseInt(r2)     // Catch:{ NumberFormatException -> 0x0196 }
            java.lang.Object r2 = r4.get(r2)     // Catch:{ NumberFormatException -> 0x0196 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ NumberFormatException -> 0x0196 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0196 }
            r7.put(r1, r2)     // Catch:{ NumberFormatException -> 0x0196 }
            goto L_0x015c
        L_0x017a:
            r10.put(r9, r7)     // Catch:{ JSONException -> 0x0188, Exception -> 0x017e }
            goto L_0x0141
        L_0x017e:
            r4 = move-exception
            java.lang.String r3 = X.C15010uZ.A00
            java.lang.Object[] r2 = new java.lang.Object[]{r13}
            java.lang.String r1 = "Failed to pase the the raw data for %s"
            goto L_0x0191
        L_0x0188:
            r4 = move-exception
            java.lang.String r3 = X.C15010uZ.A00
            java.lang.Object[] r2 = new java.lang.Object[]{r13}
            java.lang.String r1 = "Failed to pase the JSON object for %s"
        L_0x0191:
            X.C010708t.A0U(r3, r4, r1, r2)
            goto L_0x0196
        L_0x0195:
            r8 = r10
        L_0x0196:
            r0.A01 = r8
        L_0x0198:
            java.util.Map r4 = r0.A01
            int r2 = X.AnonymousClass1Y3.BCf
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r2, r1)
            X.0ui r2 = (X.C15090ui) r2
            boolean r1 = r2.A03()
            if (r1 != 0) goto L_0x0220
            r1 = 0
        L_0x01ab:
            r2 = r1
            if (r4 == 0) goto L_0x021e
            if (r1 == 0) goto L_0x021e
            boolean r1 = r4.containsKey(r1)
            if (r1 == 0) goto L_0x021e
            java.lang.Object r4 = r4.get(r2)
            java.util.Map r4 = (java.util.Map) r4
        L_0x01bc:
            if (r4 != 0) goto L_0x0207
            r3 = 6
            int r2 = X.AnonymousClass1Y3.ALw
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.3dX r3 = (X.C72023dX) r3
            java.lang.String r2 = "channel_sitevar_error"
            r1 = 0
            X.C72023dX.A01(r3, r2, r1)
        L_0x01cf:
            int r3 = X.AnonymousClass1Y3.Ady
            X.0UN r2 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.1lk r1 = (X.C32431lk) r1
            java.util.Map r1 = r1.A07
            com.google.common.collect.ImmutableMap r1 = com.google.common.collect.ImmutableMap.copyOf(r1)
            com.google.common.collect.ImmutableCollection r1 = r1.values()
            X.1Xv r4 = r1.iterator()
        L_0x01e7:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x022c
            java.lang.Object r3 = r4.next()
            android.app.NotificationChannelGroup r3 = (android.app.NotificationChannelGroup) r3
            java.lang.String r1 = r3.getId()
            android.app.NotificationChannelGroup r2 = r0.A01(r1)
            r1 = 0
            if (r2 == 0) goto L_0x01ff
            r1 = 1
        L_0x01ff:
            if (r1 != 0) goto L_0x01e7
            android.app.NotificationManager r1 = r0.A02
            r1.createNotificationChannelGroup(r3)
            goto L_0x01e7
        L_0x0207:
            int r2 = X.AnonymousClass1Y3.Ady
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1lk r3 = (X.C32431lk) r3
            if (r4 == 0) goto L_0x01cf
            java.util.Map r1 = r3.A09
            r1.clear()
            java.util.Map r1 = r3.A09
            r1.putAll(r4)
            goto L_0x01cf
        L_0x021e:
            r4 = 0
            goto L_0x01bc
        L_0x0220:
            X.1Yd r3 = r2.A01
            r1 = 845580276400285(0x3010d0001009d, double:4.177721653703266E-309)
            java.lang.String r1 = r3.B4C(r1)
            goto L_0x01ab
        L_0x022c:
            int r3 = X.AnonymousClass1Y3.Ady
            X.0UN r2 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.1lk r1 = (X.C32431lk) r1
            java.util.Map r1 = r1.A08
            com.google.common.collect.ImmutableMap r1 = com.google.common.collect.ImmutableMap.copyOf(r1)
            com.google.common.collect.ImmutableSet r1 = r1.entrySet()
            X.1Xv r8 = r1.iterator()
        L_0x0244:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x03d8
            java.lang.Object r1 = r8.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r6 = r1.getKey()
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r1 = r1.getValue()
            android.app.NotificationChannel r1 = (android.app.NotificationChannel) r1
            android.app.NotificationChannel r3 = r0.A0C(r6)
            r2 = 0
            if (r3 == 0) goto L_0x0264
            r2 = 1
        L_0x0264:
            if (r2 != 0) goto L_0x026a
            r0.A00(r1)
            goto L_0x0244
        L_0x026a:
            android.app.NotificationChannel r5 = r0.A0C(r6)
            int r4 = X.AnonymousClass1Y3.Ady
            X.0UN r3 = r0.A00
            r2 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.1lk r2 = (X.C32431lk) r2
            android.app.NotificationChannel r4 = r2.A03(r6)
            if (r5 == 0) goto L_0x029b
            if (r4 == 0) goto L_0x029b
            java.lang.CharSequence r3 = r5.getName()
            java.lang.CharSequence r2 = r4.getName()
            boolean r2 = r3.equals(r2)
            if (r2 != 0) goto L_0x029b
            java.lang.CharSequence r2 = r4.getName()
            r5.setName(r2)
            android.app.NotificationManager r2 = r0.A02
            r2.createNotificationChannel(r5)
        L_0x029b:
            java.lang.String r4 = r1.getGroup()
            int r5 = X.AnonymousClass1Y3.Ady
            X.0UN r3 = r0.A00
            r2 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r5, r3)
            X.1lk r2 = (X.C32431lk) r2
            android.app.NotificationChannel r9 = r2.A03(r6)
            if (r9 == 0) goto L_0x02c8
            android.app.NotificationChannel r3 = r0.A0C(r6)
            if (r3 == 0) goto L_0x03d3
            java.lang.String r2 = r3.getGroup()
            if (r2 == 0) goto L_0x03b2
            if (r4 == 0) goto L_0x03b2
            java.lang.String r2 = r3.getGroup()
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x03b2
        L_0x02c8:
            java.lang.String r2 = r1.getId()
            android.app.NotificationChannel r7 = r0.A0C(r2)
            if (r7 == 0) goto L_0x02f2
            java.lang.String r3 = r1.getId()
            android.app.NotificationChannel r2 = r0.A0C(r3)
            if (r2 == 0) goto L_0x02e3
            int r3 = r2.getImportance()
            r2 = 0
            if (r3 != 0) goto L_0x02e4
        L_0x02e3:
            r2 = 1
        L_0x02e4:
            if (r2 != 0) goto L_0x02f2
            android.net.Uri r6 = r7.getSound()
            android.net.Uri r4 = r1.getSound()
            if (r4 != 0) goto L_0x0369
            if (r6 != 0) goto L_0x0369
        L_0x02f2:
            int r4 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r0.A00
            r2 = 4
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.1Yd r4 = (X.C25051Yd) r4
            r2 = 282789236770675(0x1013200010773, double:1.397164469020567E-309)
            boolean r2 = r4.Aem(r2)
            if (r2 == 0) goto L_0x0244
            java.lang.String r2 = r1.getId()
            android.app.NotificationChannel r5 = r0.A0C(r2)
            if (r5 == 0) goto L_0x0244
            java.lang.String r3 = r1.getId()
            android.app.NotificationChannel r2 = r0.A0C(r3)
            if (r2 == 0) goto L_0x0323
            int r3 = r2.getImportance()
            r2 = 0
            if (r3 != 0) goto L_0x0324
        L_0x0323:
            r2 = 1
        L_0x0324:
            if (r2 != 0) goto L_0x0244
            long[] r4 = r5.getVibrationPattern()
            long[] r3 = r1.getVibrationPattern()
            boolean r2 = r1.shouldVibrate()
            if (r2 == 0) goto L_0x0244
            boolean r2 = java.util.Arrays.equals(r4, r3)
            if (r2 != 0) goto L_0x0244
            r5.getName()
            java.util.Arrays.toString(r4)
            java.util.Arrays.toString(r3)
            java.lang.CharSequence r10 = r5.getName()
            int r11 = r5.getImportance()
            boolean r12 = r5.shouldShowLights()
            int r13 = r5.getLightColor()
            boolean r14 = r1.shouldVibrate()
            android.net.Uri r16 = r5.getSound()
            java.lang.String r17 = r5.getGroup()
            r9 = r1
            r15 = r3
            A07(r9, r10, r11, r12, r13, r14, r15, r16, r17)
            A05(r0, r1)
            goto L_0x0244
        L_0x0369:
            int r5 = X.AnonymousClass1Y3.B6q
            X.0UN r3 = r0.A00
            r2 = 2
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r5, r3)
            com.facebook.prefs.shared.FbSharedPreferences r5 = (com.facebook.prefs.shared.FbSharedPreferences) r5
            X.1Y7 r3 = X.C05690aA.A16
            r2 = 0
            boolean r2 = r5.Aep(r3, r2)
            if (r2 == 0) goto L_0x0385
            android.content.Context r2 = r0.A03
            android.net.Uri r2 = X.C71283c8.A00(r2, r6)
            if (r2 != 0) goto L_0x02f2
        L_0x0385:
            java.lang.CharSequence r10 = r7.getName()
            int r11 = r7.getImportance()
            boolean r12 = r7.shouldShowLights()
            int r13 = r7.getLightColor()
            boolean r14 = r7.shouldVibrate()
            long[] r15 = r7.getVibrationPattern()
            java.lang.String r2 = r1.getId()
            android.net.Uri r16 = r0.A02(r2, r4)
            java.lang.String r17 = r7.getGroup()
            r9 = r1
            A07(r9, r10, r11, r12, r13, r14, r15, r16, r17)
            A05(r0, r1)
            goto L_0x02f2
        L_0x03b2:
            java.lang.CharSequence r10 = r3.getName()
            int r11 = r3.getImportance()
            boolean r12 = r3.shouldShowLights()
            int r13 = r3.getLightColor()
            boolean r14 = r3.shouldVibrate()
            long[] r15 = r3.getVibrationPattern()
            android.net.Uri r16 = r3.getSound()
            r17 = r4
            A07(r9, r10, r11, r12, r13, r14, r15, r16, r17)
        L_0x03d3:
            A05(r0, r9)
            goto L_0x02c8
        L_0x03d8:
            java.util.List r1 = A06(r0)
            java.util.Iterator r5 = r1.iterator()
        L_0x03e0:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x041d
            java.lang.Object r3 = r5.next()
            android.app.NotificationChannel r3 = (android.app.NotificationChannel) r3
            java.lang.String r2 = r3.getId()
            java.lang.String r1 = "miscellaneous"
            boolean r1 = r2.equals(r1)
            if (r1 != 0) goto L_0x03e0
            java.lang.String r1 = r3.getId()
            java.lang.String r4 = X.C15010uZ.A01(r1)
            if (r4 == 0) goto L_0x03e0
            r3 = 0
            int r2 = X.AnonymousClass1Y3.Ady
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1lk r1 = (X.C32431lk) r1
            java.util.Map r1 = r1.A08
            com.google.common.collect.ImmutableMap r1 = com.google.common.collect.ImmutableMap.copyOf(r1)
            boolean r1 = r1.containsKey(r4)
            if (r1 != 0) goto L_0x03e0
            r0.A09(r4)
            goto L_0x03e0
        L_0x041d:
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r1 = r0.A00
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0
            X.1hn r2 = r0.edit()
            X.1Y7 r1 = X.C05690aA.A16
            r0 = 1
            r2.putBoolean(r1, r0)
            r2.commit()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15100uj.A08(X.0uj):void");
    }

    public static boolean A0A(String str) {
        if (str.indexOf("messenger_orca_") < 0) {
            C24971Xv it = C32431lk.A0A.iterator();
            while (it.hasNext()) {
                if (str.indexOf((String) it.next()) >= 0) {
                }
            }
            return false;
        }
        return true;
    }

    public String A0D() {
        NotificationChannel A0C;
        NotificationChannel A002 = C32431lk.A00((NotificationChannel) ((C32431lk) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ady, this.A00)).A08.get("messenger_orca_750_voip"));
        if (A002 == null || (A0C = A0C(A002.getId())) == null) {
            return null;
        }
        return A0C.getId();
    }

    public String A0E() {
        NotificationChannel A002 = C32431lk.A00((NotificationChannel) ((C32431lk) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ady, this.A00)).A08.get("messenger_orca_050_messaging"));
        if (A002 == null) {
            return null;
        }
        String id = A002.getId();
        NotificationChannel A0C = A0C(id);
        if (A0C == null) {
            return id;
        }
        return A0C.getId();
    }

    public boolean A0F() {
        int i = AnonymousClass1Y3.BCf;
        AnonymousClass0UN r2 = this.A00;
        if (!((C15090ui) AnonymousClass1XX.A02(1, i, r2)).A03() || ImmutableMap.copyOf(((C32431lk) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ady, r2)).A08).isEmpty() || A0E() == null) {
            return false;
        }
        return true;
    }

    private C15100uj(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(7, r3);
        Context A022 = AnonymousClass1YA.A02(r3);
        this.A03 = A022;
        this.A02 = (NotificationManager) A022.getSystemService("notification");
    }

    private NotificationChannel A00(NotificationChannel notificationChannel) {
        boolean z = false;
        if (A0C(notificationChannel.getId()) != null) {
            z = true;
        }
        if (z) {
            return A0C(notificationChannel.getId());
        }
        NotificationChannel notificationChannel2 = new NotificationChannel(AnonymousClass08S.A07(notificationChannel.getId(), '@', C188215g.A00().toString()), notificationChannel.getName(), notificationChannel.getImportance());
        if (!C06850cB.A0B(notificationChannel.getGroup())) {
            notificationChannel2.setGroup(notificationChannel.getGroup());
        }
        notificationChannel2.setLightColor(notificationChannel.getLightColor());
        notificationChannel2.enableLights(notificationChannel.shouldShowLights());
        notificationChannel2.setVibrationPattern(notificationChannel.getVibrationPattern());
        notificationChannel2.enableVibration(notificationChannel.shouldVibrate());
        notificationChannel2.setSound(notificationChannel.getSound(), notificationChannel.getAudioAttributes());
        notificationChannel2.setShowBadge(notificationChannel.canShowBadge());
        this.A02.createNotificationChannel(notificationChannel2);
        ((C72023dX) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ALw, this.A00)).A02("channel_created", notificationChannel.getId(), C15010uZ.A00(notificationChannel2));
        return notificationChannel2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0028, code lost:
        if (r0 == false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A05(X.C15100uj r6, android.app.NotificationChannel r7) {
        /*
            java.lang.String r0 = r7.getId()
            r6.A09(r0)
            int r2 = X.AnonymousClass1Y3.Ady
            X.0UN r1 = r6.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1lk r1 = (X.C32431lk) r1
            java.lang.String r0 = r7.getId()
            android.app.NotificationChannel r0 = r1.A03(r0)
            if (r0 == 0) goto L_0x002a
            java.lang.String r0 = r7.getGroup()
            android.app.NotificationChannelGroup r1 = r6.A01(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0028
            r0 = 1
        L_0x0028:
            if (r0 != 0) goto L_0x002d
        L_0x002a:
            A08(r6)
        L_0x002d:
            android.app.NotificationChannel r5 = r6.A00(r7)
            r2 = 6
            int r1 = X.AnonymousClass1Y3.ALw
            X.0UN r0 = r6.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3dX r4 = (X.C72023dX) r4
            java.lang.String r3 = r7.getId()
            java.lang.String r2 = "channel_forced_recreated"
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.lang.String r0 = "channel_id"
            r1.put(r0, r3)
            X.C72023dX.A01(r4, r2, r1)
            java.lang.String r0 = r5.getId()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15100uj.A05(X.0uj, android.app.NotificationChannel):java.lang.String");
    }

    public static void A07(NotificationChannel notificationChannel, CharSequence charSequence, int i, boolean z, int i2, boolean z2, long[] jArr, Uri uri, String str) {
        notificationChannel.setName(charSequence);
        notificationChannel.setImportance(i);
        if (z) {
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(i2);
        } else {
            notificationChannel.enableLights(false);
        }
        if (z2) {
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(jArr);
        } else {
            notificationChannel.enableVibration(false);
        }
        notificationChannel.setSound(uri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
        notificationChannel.setGroup(str);
    }

    private void A09(String str) {
        NotificationChannel A0C = A0C(str);
        if (A0C != null && !A0C.getId().equals("miscellaneous") && A0A(str)) {
            this.A02.deleteNotificationChannel(A0C.getId());
            ((C72023dX) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ALw, this.A00)).A02("channel_deleted", str, C15010uZ.A00(A0C));
        }
    }

    public NotificationChannel A0B(String str) {
        NotificationChannel A0C = A0C(str);
        if (A0C != null) {
            return A0C;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("channel_id", str);
        C72023dX.A01((C72023dX) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ALw, this.A00), "channel_missing", hashMap);
        A08(this);
        NotificationChannel A032 = ((C32431lk) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ady, this.A00)).A03(str);
        if (A032 != null) {
            return A00(A032);
        }
        NotificationChannel notificationChannel = this.A02.getNotificationChannel("miscellaneous");
        if (notificationChannel == null) {
            return null;
        }
        return notificationChannel;
    }

    public NotificationChannel A0C(String str) {
        for (NotificationChannel notificationChannel : A06(this)) {
            String A012 = C15010uZ.A01(notificationChannel.getId());
            if (A012 != null && A012.equals(str)) {
                return notificationChannel;
            }
        }
        return null;
    }

    public boolean A0G(String str) {
        NotificationChannelGroup A012 = A01(str);
        if (A012 == null || (Build.VERSION.SDK_INT >= 28 && A012.isBlocked())) {
            return false;
        }
        return true;
    }
}
