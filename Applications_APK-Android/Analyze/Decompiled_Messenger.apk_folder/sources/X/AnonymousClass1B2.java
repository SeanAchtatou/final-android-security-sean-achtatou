package X;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.messaging.dialog.MenuDialogFragment;
import com.facebook.messaging.dialog.MenuDialogParams;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1B2  reason: invalid class name */
public final class AnonymousClass1B2 implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.orca.threadlist.common.ThreadListMenuController";
    public Context A00;
    public C13060qW A01;
    public AnonymousClass0UN A02;
    public C149716wu A03;
    public C25051Yd A04;
    public C20121Az A05;
    public AnonymousClass6LI A06;
    public AnonymousClass1B5 A07;
    private Activity A08;
    private C001500z A09;
    private C20961Em A0A;
    private String A0B;
    private C04310Tq A0C;
    public final AnonymousClass1B3 A0D;
    public final AnonymousClass1B4 A0E;
    private final AnonymousClass1B1 A0F;

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0043, code lost:
        if (com.facebook.messaging.model.threadkey.ThreadKey.A0F(r1) == false) goto L_0x0046;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01f9  */
    /* JADX WARNING: Removed duplicated region for block: B:73:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(com.facebook.messaging.inbox2.items.InboxUnitThreadItem r11, X.C13060qW r12, X.C123425rb r13) {
        /*
            r10 = this;
            r8 = r11
            com.facebook.messaging.model.threads.ThreadSummary r0 = r11.A01
            r4 = 0
            if (r0 == 0) goto L_0x0046
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            if (r0 == 0) goto L_0x0046
            X.1Yd r2 = r10.A04
            r0 = 282235187954943(0x100b1001f04ff, double:1.394427104160845E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0046
            com.facebook.messaging.model.threads.ThreadSummary r3 = r11.A01
            r2 = 24
            int r1 = X.AnonymousClass1Y3.Anz
            X.0UN r0 = r10.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.23R r0 = (X.AnonymousClass23R) r0
            com.facebook.user.model.User r0 = r0.A03(r3)
            if (r0 == 0) goto L_0x0031
            boolean r0 = r0.A0F()
            if (r0 != 0) goto L_0x0045
        L_0x0031:
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r3.A0S
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A09(r1)
            if (r0 != 0) goto L_0x0045
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0B(r1)
            if (r0 != 0) goto L_0x0045
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r1)
            if (r0 == 0) goto L_0x0046
        L_0x0045:
            r4 = 1
        L_0x0046:
            if (r4 == 0) goto L_0x01b1
            int r2 = X.AnonymousClass1Y3.BSY
            X.0UN r1 = r10.A02
            r0 = 2
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.6Vz r6 = (X.C135676Vz) r6
            boolean r0 = r10.A04()
            r6.A01 = r0
            com.facebook.messaging.model.threads.ThreadSummary r5 = r11.A01
            com.google.common.collect.ImmutableMap r4 = X.C135676Vz.A03(r6, r5)
            com.google.common.collect.ImmutableList$Builder r2 = com.google.common.collect.ImmutableList.builder()
            boolean r0 = r5.A0A()
            if (r0 != 0) goto L_0x007e
            r1 = 1
            boolean r0 = X.C135676Vz.A08(r6, r1, r5)
            if (r0 == 0) goto L_0x007e
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            java.lang.Object r0 = r4.get(r0)
            com.google.common.base.Preconditions.checkNotNull(r0)
            r2.add(r0)
        L_0x007e:
            int r7 = X.AnonymousClass1Y3.AFU
            X.0UN r1 = r6.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r7, r1)
            X.1BS r0 = (X.AnonymousClass1BS) r0
            boolean r1 = r0.A01(r5)
            r0 = 9
            if (r1 == 0) goto L_0x0093
            r0 = 8
        L_0x0093:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r0 = r4.get(r0)
            X.1xf r0 = (X.C38581xf) r0
            com.google.common.base.Preconditions.checkNotNull(r0)
            r2.add(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r5.A0S
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0B(r1)
            if (r0 == 0) goto L_0x014a
            boolean r0 = X.C135676Vz.A09(r6, r5)
            if (r0 != 0) goto L_0x0129
            boolean r0 = r5.A0A()
            if (r0 == 0) goto L_0x00ff
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 5
        L_0x00c5:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 10
        L_0x00ce:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            java.util.Iterator r7 = r1.iterator()
        L_0x00d9:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x01ac
            java.lang.Object r0 = r7.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r1 = r0.intValue()
            boolean r0 = X.C135676Vz.A08(r6, r1, r5)
            if (r0 == 0) goto L_0x00d9
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            java.lang.Object r0 = r4.get(r0)
            X.1xf r0 = (X.C38581xf) r0
            if (r0 == 0) goto L_0x00d9
            r2.add(r0)
            goto L_0x00d9
        L_0x00ff:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 5
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 26
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 27
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 20
            goto L_0x00c5
        L_0x0129:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 5
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 10
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 15
            goto L_0x00ce
        L_0x014a:
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A09(r1)
            if (r0 == 0) goto L_0x0186
            boolean r0 = r5.A0A()
            if (r0 == 0) goto L_0x016e
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 5
        L_0x0164:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 4
            goto L_0x00ce
        L_0x016e:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 5
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 22
            goto L_0x0164
        L_0x0186:
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r1)
            if (r0 == 0) goto L_0x01f1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0 = 0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 3
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
            r0 = 7
            goto L_0x00c5
        L_0x01ac:
            com.google.common.collect.ImmutableList r7 = r2.build()
            goto L_0x01f3
        L_0x01b1:
            com.facebook.messaging.model.threads.ThreadSummary r4 = r11.A01
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BSY
            X.0UN r0 = r10.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6Vz r1 = (X.C135676Vz) r1
            boolean r0 = r10.A04()
            r1.A01 = r0
            com.facebook.messaging.model.threads.ThreadSummary r3 = r11.A01
            X.6Lf r2 = X.C135676Vz.A01(r1, r3)
            com.facebook.common.util.ParcelablePair r0 = new com.facebook.common.util.ParcelablePair
            r0.<init>(r3, r11)
            r2.A01 = r0
            r0 = 1
            r2.A04 = r0
            X.C135676Vz.A04(r2, r11)
            java.util.List r0 = r2.A03
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x01eb
            r1 = 0
        L_0x01e0:
            if (r1 == 0) goto L_0x0214
            X.5ra r0 = new X.5ra
            r0.<init>(r10, r13)
            r10.A01(r4, r12, r1, r0)
            return
        L_0x01eb:
            com.facebook.messaging.dialog.MenuDialogParams r1 = new com.facebook.messaging.dialog.MenuDialogParams
            r1.<init>(r2)
            goto L_0x01e0
        L_0x01f1:
            com.google.common.collect.ImmutableList r7 = com.google.common.collect.RegularImmutableList.A02
        L_0x01f3:
            boolean r0 = r7.isEmpty()
            if (r0 != 0) goto L_0x0214
            X.6LJ r9 = new X.6LJ
            r9.<init>(r10, r11)
            int r1 = X.AnonymousClass1Y3.Anh
            X.0UN r0 = r10.A02
            java.lang.Object r6 = X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = (com.facebook.mig.scheme.interfaces.MigColorScheme) r6
            android.content.Context r5 = r10.A00
            X.6LI r4 = new X.6LI
            r4.<init>(r5, r6, r7, r8, r9)
            r10.A06 = r4
            r4.show()
        L_0x0214:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1B2.A05(com.facebook.messaging.inbox2.items.InboxUnitThreadItem, X.0qW, X.5rb):void");
    }

    private List A00(ThreadSummary threadSummary) {
        ThreadKey threadKey;
        if (threadSummary == null || (threadKey = threadSummary.A0S) == null) {
            return null;
        }
        long A0G = threadKey.A0G();
        C50542eC r0 = (C50542eC) AnonymousClass1XX.A02(12, AnonymousClass1Y3.ASZ, this.A02);
        C50542eC.A01(r0);
        return (List) r0.A01.A07(A0G);
    }

    private void A01(ThreadSummary threadSummary, C13060qW r6, MenuDialogParams menuDialogParams, AnonymousClass3IA r8) {
        ((C133196Lg) AnonymousClass1XX.A02(23, AnonymousClass1Y3.ADM, this.A02)).A00.CH1(C133196Lg.A01);
        ((C133196Lg) AnonymousClass1XX.A02(23, AnonymousClass1Y3.ADM, this.A02)).A01("open_context_menu");
        ((C14220so) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6G, this.A02)).A01(AnonymousClass08S.A0G("Long click on thread: ", threadSummary.A0S.A0G()), AnonymousClass07B.A01);
        MenuDialogFragment A002 = MenuDialogFragment.A00(menuDialogParams);
        A002.A25(r6, "thread_menu_dialog");
        A002.A00 = r8;
    }

    public static void A03(AnonymousClass1B2 r4, ThreadSummary threadSummary) {
        ((C15870w7) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AlX, r4.A02)).A08(threadSummary.A0S, r4.A01, "context_pop_out_selected");
    }

    private boolean A04() {
        if (this.A09 == C001500z.A07 && ((Boolean) this.A0C.get()).booleanValue() && AnonymousClass065.A00(this.A00, Service.class) == null) {
            return true;
        }
        return false;
    }

    public void A06(ThreadSummary threadSummary, C13060qW r5) {
        C135676Vz r1 = (C135676Vz) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BSY, this.A02);
        r1.A01 = A04();
        C133186Lf A012 = C135676Vz.A01(r1, threadSummary);
        A012.A01 = threadSummary;
        MenuDialogParams menuDialogParams = new MenuDialogParams(A012);
        ImmutableList immutableList = menuDialogParams.A01;
        if (immutableList != null && immutableList.size() > 0) {
            A01(threadSummary, r5, menuDialogParams, new C123405rZ(this));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001e, code lost:
        if (r0 != false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A07(int r14, java.lang.String r15, com.facebook.messaging.model.threads.ThreadSummary r16) {
        /*
            r13 = this;
            X.6wu r0 = r13.A03
            r4 = 1
            r10 = r16
            if (r0 == 0) goto L_0x0023
            r3 = 4
            if (r14 != r3) goto L_0x0021
            int r1 = X.AnonymousClass1Y3.B24
            X.6vB r2 = r0.A00
            X.0UN r0 = r2.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.2Kr r1 = (X.C45072Kr) r1
            X.0qW r0 = r2.A17()
            r1.A03(r10, r0)
            r0 = 1
        L_0x001e:
            if (r0 == 0) goto L_0x0023
        L_0x0020:
            return r4
        L_0x0021:
            r0 = 0
            goto L_0x001e
        L_0x0023:
            if (r15 != 0) goto L_0x0027
            java.lang.String r15 = "not recognized"
        L_0x0027:
            r2 = 3
            int r1 = X.AnonymousClass1Y3.A5w
            X.0UN r0 = r13.A02
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.146 r5 = (X.AnonymousClass146) r5
            android.app.Activity r7 = r13.A08
            r9 = 0
            java.lang.String r2 = "context_menu_item"
            monitor-enter(r5)
            r3 = 10
            int r1 = X.AnonymousClass1Y3.BP0     // Catch:{ all -> 0x053d }
            X.0UN r0 = r5.A03     // Catch:{ all -> 0x053d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x053d }
            X.0hE r1 = (X.C09390hE) r1     // Catch:{ all -> 0x053d }
            java.lang.String r0 = X.AnonymousClass146.A03(r7)     // Catch:{ all -> 0x053d }
            r1.A02(r0, r2)     // Catch:{ all -> 0x053d }
            r3 = 14
            int r1 = X.AnonymousClass1Y3.AR0     // Catch:{ all -> 0x053d }
            X.0UN r0 = r5.A03     // Catch:{ all -> 0x053d }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x053d }
            com.facebook.analytics.DeprecatedAnalyticsLogger r6 = (com.facebook.analytics.DeprecatedAnalyticsLogger) r6     // Catch:{ all -> 0x053d }
            r8 = r15
            r0 = r9
            if (r7 == 0) goto L_0x006b
            boolean r1 = r7 instanceof X.C11510nI     // Catch:{ all -> 0x053d }
            if (r1 == 0) goto L_0x006b
            r1 = r7
            X.0nI r1 = (X.C11510nI) r1     // Catch:{ all -> 0x053d }
            java.lang.String r3 = r1.Act()     // Catch:{ all -> 0x053d }
        L_0x0066:
            boolean r1 = r7 instanceof X.C10890l1     // Catch:{ all -> 0x053d }
            if (r1 == 0) goto L_0x0083
            goto L_0x006d
        L_0x006b:
            r3 = 0
            goto L_0x0066
        L_0x006d:
            if (r2 != 0) goto L_0x007a
            r1 = r7
            X.0l1 r1 = (X.C10890l1) r1     // Catch:{ all -> 0x053d }
            java.lang.Integer r1 = r1.getObjectType$REDEX$2GJxpwjwFN8()     // Catch:{ all -> 0x053d }
            if (r1 == 0) goto L_0x007a
            java.lang.String r2 = "pages"
        L_0x007a:
            if (r15 != 0) goto L_0x0083
            r1 = r7
            X.0l1 r1 = (X.C10890l1) r1     // Catch:{ all -> 0x053d }
            java.lang.String r8 = r1.getObjectId()     // Catch:{ all -> 0x053d }
        L_0x0083:
            boolean r1 = r7 instanceof X.C11570nO     // Catch:{ all -> 0x053d }
            if (r1 == 0) goto L_0x0090
            X.0nO r7 = (X.C11570nO) r7     // Catch:{ all -> 0x053d }
            java.util.Map r0 = r7.Acr()     // Catch:{ all -> 0x053d }
            if (r0 != 0) goto L_0x0090
            r0 = r9
        L_0x0090:
            X.0nb r0 = X.C1056253i.A00(r3, r2, r8, r0)     // Catch:{ all -> 0x053d }
            r6.A07(r0)     // Catch:{ all -> 0x053d }
            monitor-exit(r5)
            java.lang.String r0 = "CLick on Menu Item: "
            java.lang.String r2 = X.AnonymousClass08S.A0J(r0, r15)
            int r1 = X.AnonymousClass1Y3.A6G
            X.0UN r0 = r13.A02
            r8 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.0so r1 = (X.C14220so) r1
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r1.A01(r2, r0)
            int r1 = X.AnonymousClass1Y3.A4e
            X.0UN r0 = r13.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.6fk r0 = (X.C139926fk) r0
            r3 = 11
            r7 = 6
            java.lang.String r6 = "notificationSettingsDialog"
            r2 = 5
            switch(r14) {
                case 0: goto L_0x03b7;
                case 1: goto L_0x00c2;
                case 2: goto L_0x03bd;
                case 3: goto L_0x03e3;
                case 4: goto L_0x03ef;
                case 5: goto L_0x0431;
                case 6: goto L_0x00c1;
                case 7: goto L_0x014a;
                case 8: goto L_0x0443;
                case 9: goto L_0x0489;
                case 10: goto L_0x0161;
                case 11: goto L_0x00c1;
                case 12: goto L_0x00c1;
                case 13: goto L_0x00c1;
                case 14: goto L_0x00c1;
                case 15: goto L_0x0227;
                case 16: goto L_0x00c1;
                case 17: goto L_0x02c5;
                case 18: goto L_0x0353;
                case 19: goto L_0x02c5;
                case 20: goto L_0x037c;
                case 21: goto L_0x00c1;
                case 22: goto L_0x037c;
                case 23: goto L_0x0392;
                case 24: goto L_0x04cf;
                case 25: goto L_0x04cf;
                case 26: goto L_0x04e1;
                case 27: goto L_0x04fe;
                default: goto L_0x00c1;
            }
        L_0x00c1:
            return r8
        L_0x00c2:
            int r2 = X.AnonymousClass1Y3.AxZ
            X.0UN r1 = r13.A02
            r0 = 14
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0tP r3 = (X.C14480tP) r3
            com.google.common.collect.ImmutableList r8 = com.google.common.collect.ImmutableList.of(r10)
            X.3YF r6 = new X.3YF
            r6.<init>()
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r6.A00 = r0
            java.util.Iterator r5 = r8.iterator()
        L_0x00df:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0105
            java.lang.Object r1 = r5.next()
            com.facebook.messaging.model.threads.ThreadSummary r1 = (com.facebook.messaging.model.threads.ThreadSummary) r1
            X.3YC r2 = new X.3YC
            r2.<init>()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A0S
            r2.A05 = r0
            r2.A06 = r4
            long r0 = r1.A09
            r2.A01 = r0
            com.facebook.messaging.service.model.MarkThreadFields r1 = new com.facebook.messaging.service.model.MarkThreadFields
            r1.<init>(r2)
            com.google.common.collect.ImmutableList$Builder r0 = r6.A02
            r0.add(r1)
            goto L_0x00df
        L_0x0105:
            com.facebook.messaging.service.model.MarkThreadsParams r7 = new com.facebook.messaging.service.model.MarkThreadsParams
            r7.<init>(r6)
            android.os.Bundle r6 = new android.os.Bundle
            r6.<init>()
            java.lang.String r0 = "markThreadsParams"
            r6.putParcelable(r0, r7)
            com.facebook.fbservice.ops.BlueServiceOperationFactory r5 = r3.A00
            java.lang.Class r0 = r3.getClass()
            com.facebook.common.callercontext.CallerContext r1 = com.facebook.common.callercontext.CallerContext.A04(r0)
            java.lang.String r0 = "mark_threads"
            X.0lL r0 = r5.newInstance(r0, r6, r4, r1)
            X.1cp r2 = r0.CGe()
            X.4c0 r1 = new X.4c0
            r1.<init>(r3, r7)
            X.1Ym r0 = X.C25141Ym.INSTANCE
            X.C05350Yp.A08(r2, r1, r0)
            java.util.Iterator r2 = r8.iterator()
        L_0x0136:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0020
            java.lang.Object r0 = r2.next()
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0
            java.util.Set r1 = r3.A02
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            r1.add(r0)
            goto L_0x0136
        L_0x014a:
            int r1 = X.AnonymousClass1Y3.BLe
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.0u1 r1 = (X.C14780u1) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            r1.A08(r0)
            X.1B5 r0 = r13.A07
            if (r0 == 0) goto L_0x0020
            r0.BDQ()
            return r4
        L_0x0161:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r0)
            if (r0 == 0) goto L_0x01d9
            java.util.List r2 = r13.A00(r10)
            if (r2 == 0) goto L_0x0020
            int r1 = r2.size()
            r0 = 1
            if (r1 != r0) goto L_0x0020
            java.lang.Object r8 = r2.get(r8)
            java.lang.String r8 = (java.lang.String) r8
            r2 = 13
            int r1 = X.AnonymousClass1Y3.AaZ
            X.0UN r0 = r13.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2eE r0 = (X.C50562eE) r0
            java.lang.String r7 = r0.A09(r8)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r8)
            if (r0 != 0) goto L_0x0020
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r7)
            if (r0 != 0) goto L_0x0020
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            long r1 = r0.A0G()
            X.6dA r6 = X.C138436dA.A09
            com.facebook.messaging.blocking.ManageBlockingSmsFragment r5 = new com.facebook.messaging.blocking.ManageBlockingSmsFragment
            r5.<init>()
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            r0 = 44
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r3.putString(r0, r8)
            r0 = 404(0x194, float:5.66E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r3.putString(r0, r7)
            r0 = 406(0x196, float:5.69E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r3.putLong(r0, r1)
            r0 = 402(0x192, float:5.63E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r3.putSerializable(r0, r6)
            r5.A1P(r3)
            X.0qW r1 = r13.A01
            java.lang.String r0 = "manageBlockingSmsFragment"
            r5.A25(r1, r0)
            return r4
        L_0x01d9:
            r2 = 20
            int r1 = X.AnonymousClass1Y3.BFU
            X.0UN r0 = r13.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2Bv r0 = (X.C42782Bv) r0
            com.facebook.user.model.User r5 = r0.A05(r10)
            if (r5 == 0) goto L_0x0020
            boolean r0 = r5.A0F()
            if (r0 == 0) goto L_0x052b
            boolean r1 = r5.A0F()
            java.lang.String r0 = "user is not a page when handling page block"
            com.google.common.base.Preconditions.checkState(r1, r0)
            if (r16 == 0) goto L_0x0225
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r10.A0S
        L_0x01fe:
            java.lang.String r1 = r5.A0j
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            X.6bu r0 = X.C137496bT.A00(r0, r1)
            r0.A00 = r2
            X.6bT r3 = new X.6bT
            r3.<init>(r0)
            java.lang.Integer r1 = r5.A06()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r2 = 28
            if (r1 == r0) goto L_0x051d
            int r1 = X.AnonymousClass1Y3.B23
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6bS r2 = (X.C137486bS) r2
            X.C137486bS.A01(r2, r3, r9, r8)
            return r4
        L_0x0225:
            r2 = 0
            goto L_0x01fe
        L_0x0227:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            if (r0 == 0) goto L_0x0020
            r2 = 27
            int r1 = X.AnonymousClass1Y3.ArX
            X.0UN r0 = r13.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6Wl r0 = (X.C135796Wl) r0
            X.6go r6 = r0.A01(r10)
            if (r6 == 0) goto L_0x025b
            X.6go r0 = X.C140416go.A01
            if (r6 == r0) goto L_0x0245
            X.6go r0 = X.C140416go.A05
            if (r6 != r0) goto L_0x025b
        L_0x0245:
            X.1Az r0 = r13.A05
            if (r0 == 0) goto L_0x025b
            X.0qW r5 = r13.A01
            java.lang.Integer r3 = X.AnonymousClass07B.A03
            int r2 = X.AnonymousClass1Y3.AWF
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r2, r1)
            X.3Jj r0 = (X.C66233Jj) r0
            r0.A03(r5, r10, r6, r3)
            return r4
        L_0x025b:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            com.facebook.user.model.UserKey r3 = com.facebook.messaging.model.threadkey.ThreadKey.A07(r0)
            com.google.common.base.Preconditions.checkNotNull(r3)
            r2 = 7
            int r1 = X.AnonymousClass1Y3.AxE
            X.0UN r0 = r13.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0t0 r0 = (X.C14300t0) r0
            com.facebook.user.model.User r3 = r0.A03(r3)
            if (r3 == 0) goto L_0x02ac
            boolean r0 = r3.A0F()
            if (r0 == 0) goto L_0x02ac
            r2 = 25
            int r1 = X.AnonymousClass1Y3.B0b
            X.0UN r0 = r13.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6YL r0 = (X.AnonymousClass6YL) r0
            X.1Yd r2 = r0.A00
            r0 = 286835095903489(0x104e000001d01, double:1.41715366907493E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x02ac
            X.1Az r0 = r13.A05
            if (r0 == 0) goto L_0x02ac
            X.0qW r3 = r13.A01
            int r2 = X.AnonymousClass1Y3.AWF
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r8, r2, r1)
            X.3Jj r2 = (X.C66233Jj) r2
            X.6go r1 = X.C140416go.A0L
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            r2.A03(r3, r10, r1, r0)
            return r4
        L_0x02ac:
            if (r3 == 0) goto L_0x0020
            android.content.Context r0 = r13.A00
            android.content.Intent r3 = X.DTn.A00(r0, r3, r10)
            r2 = 4
            int r1 = X.AnonymousClass1Y3.A7S
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.content.SecureContextHelper r1 = (com.facebook.content.SecureContextHelper) r1
            android.content.Context r0 = r13.A00
            r1.startFacebookActivity(r3, r0)
            return r4
        L_0x02c5:
            java.util.List r1 = r13.A00(r10)
            if (r1 == 0) goto L_0x0020
            int r0 = r1.size()
            if (r0 != r4) goto L_0x0020
            java.lang.Object r5 = r1.get(r8)
            java.lang.String r5 = (java.lang.String) r5
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 != 0) goto L_0x0020
            com.google.common.collect.ImmutableList$Builder r3 = new com.google.common.collect.ImmutableList$Builder
            r3.<init>()
            r0 = -102(0xffffffffffffff9a, double:NaN)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A03(r0)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r0)
            r3.addAll(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            r3.add(r0)
            r2 = 17
            if (r14 != r2) goto L_0x032f
            int r1 = X.AnonymousClass1Y3.AjV
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2eB r2 = (X.C50532eB) r2
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.of(r5)
            com.google.common.collect.ImmutableList r0 = r3.build()
            r2.A03(r1, r0)
            int r1 = X.AnonymousClass1Y3.AFw
            X.0UN r0 = r13.A02
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1Ha r5 = (X.C21451Ha) r5
            java.lang.String r3 = r13.A0B
            java.lang.String r2 = "report business"
        L_0x031b:
            java.lang.String r0 = "sms_takeover_report_business_action"
            X.0nb r1 = X.C21451Ha.A01(r0)
            java.lang.String r0 = "call_context"
            r1.A0D(r0, r3)
            java.lang.String r0 = "report_business_thread_action"
            r1.A0D(r0, r2)
            X.C21451Ha.A05(r5, r1)
            return r4
        L_0x032f:
            int r1 = X.AnonymousClass1Y3.AjV
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2eB r2 = (X.C50532eB) r2
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.of(r5)
            com.google.common.collect.ImmutableList r0 = r3.build()
            r2.A04(r1, r0)
            int r1 = X.AnonymousClass1Y3.AFw
            X.0UN r0 = r13.A02
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1Ha r5 = (X.C21451Ha) r5
            java.lang.String r3 = r13.A0B
            java.lang.String r2 = "report not business"
            goto L_0x031b
        L_0x0353:
            if (r16 == 0) goto L_0x0020
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r10.A0S
            if (r3 == 0) goto L_0x0020
            int r1 = X.AnonymousClass1Y3.AGr
            X.0UN r0 = r13.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14h r0 = (X.C185914h) r0
            boolean r0 = r0.A09(r3)
            if (r0 == 0) goto L_0x0539
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14h r2 = (X.C185914h) r2
            X.6dA r1 = X.C138436dA.A0A
            X.5rY r0 = new X.5rY
            r0.<init>(r13, r3)
            r2.A07(r1, r0)
            return r4
        L_0x037c:
            if (r16 == 0) goto L_0x0020
            X.0qW r0 = r13.A01
            boolean r0 = X.C16000wK.A01(r0)
            if (r0 == 0) goto L_0x0020
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            com.facebook.messaging.ignore.IgnoreMessagesDialogFragment r1 = com.facebook.messaging.ignore.IgnoreMessagesDialogFragment.A00(r10, r0)
            X.0qW r0 = r13.A01
            r1.A2G(r0)
            return r4
        L_0x0392:
            if (r16 == 0) goto L_0x0020
            r2 = 22
            int r1 = X.AnonymousClass1Y3.ABt
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6Le r1 = (X.C133176Le) r1
            X.0qW r0 = r13.A01
            r1.A01(r0, r10)
            r2 = 23
            int r1 = X.AnonymousClass1Y3.ADM
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6Lg r1 = (X.C133196Lg) r1
            java.lang.String r0 = "open_reminder_menu_from_context_menu"
            r1.A01(r0)
            return r4
        L_0x03b7:
            X.1Em r0 = r13.A0A
            r0.A02(r10)
            return r4
        L_0x03bd:
            boolean r0 = r0.A02(r10)
            if (r0 == 0) goto L_0x03df
            int r1 = X.AnonymousClass1Y3.A0s
            X.0UN r0 = r13.A02
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r1, r0)
            X.3xw r8 = (X.C83633xw) r8
            X.6LN r12 = new X.6LN
            r12.<init>(r13, r10)
            android.content.Context r9 = r13.A00
            X.0qW r11 = r13.A01
            X.6fh r7 = new X.6fh
            r7.<init>(r8, r9, r10, r11, r12)
            r7.A00()
            return r4
        L_0x03df:
            A03(r13, r10)
            return r4
        L_0x03e3:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            com.facebook.messaging.mutators.ThreadNotificationsDialogFragment r1 = com.facebook.messaging.mutators.ThreadNotificationsDialogFragment.A00(r0)
            X.0qW r0 = r13.A01
            r1.A25(r0, r6)
            return r4
        L_0x03ef:
            int r2 = X.AnonymousClass1Y3.A3x
            X.0UN r1 = r13.A02
            r0 = 26
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0yd r0 = (X.C17270yd) r0
            boolean r0 = r0.A0E(r10)
            if (r0 == 0) goto L_0x041f
            X.1B4 r0 = r13.A0E
            boolean r0 = r0.A01()
            if (r0 == 0) goto L_0x041f
            X.6Uk r5 = new X.6Uk
            r5.<init>(r13, r10)
            X.1B3 r3 = r13.A0D
            android.content.Context r2 = r13.A00
            java.lang.Integer r1 = X.AnonymousClass07B.A0E
            X.23M r0 = new X.23M
            r0.<init>(r3, r2, r10, r1)
            r0.A02 = r5
            r0.A00()
            return r4
        L_0x041f:
            r2 = 9
            int r1 = X.AnonymousClass1Y3.BNP
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6UM r2 = (X.AnonymousClass6UM) r2
            X.0qW r1 = r13.A01
            r2.A01(r1, r10, r9, r9)
            return r4
        L_0x0431:
            r2 = 10
            int r1 = X.AnonymousClass1Y3.An3
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.5Xc r1 = (X.C112495Xc) r1
            java.lang.String r0 = "context_menu_click"
            r1.A01(r10, r0)
            return r4
        L_0x0443:
            int r1 = X.AnonymousClass1Y3.AGr
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14h r1 = (X.C185914h) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            boolean r0 = r1.A09(r0)
            if (r0 == 0) goto L_0x046a
            int r1 = X.AnonymousClass1Y3.AGr
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14h r2 = (X.C185914h) r2
            X.6dA r1 = X.C138436dA.A0A
            X.6Us r0 = new X.6Us
            r0.<init>(r13, r10)
            r2.A07(r1, r0)
            return r4
        L_0x046a:
            r2 = 21
            int r1 = X.AnonymousClass1Y3.BTA
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3T5 r1 = (X.AnonymousClass3T5) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            r1.A06(r0)
            int r1 = X.AnonymousClass1Y3.Art
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.37x r1 = (X.C636337x) r1
            X.C636337x.A04(r1, r10, r4)
            return r4
        L_0x0489:
            int r1 = X.AnonymousClass1Y3.AGr
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14h r1 = (X.C185914h) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            boolean r0 = r1.A09(r0)
            if (r0 == 0) goto L_0x04c1
            int r1 = X.AnonymousClass1Y3.AGr
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14h r2 = (X.C185914h) r2
            X.6dA r1 = X.C138436dA.A0B
            X.6Ur r0 = new X.6Ur
            r0.<init>(r13, r10)
            r2.A07(r1, r0)
        L_0x04af:
            r2 = 23
            int r1 = X.AnonymousClass1Y3.ADM
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6Lg r1 = (X.C133196Lg) r1
            java.lang.String r0 = "select_mark_as_unread_from_context_menu"
            r1.A01(r0)
            return r4
        L_0x04c1:
            int r1 = X.AnonymousClass1Y3.Art
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.37x r1 = (X.C636337x) r1
            X.C636337x.A04(r1, r10, r8)
            goto L_0x04af
        L_0x04cf:
            int r1 = X.AnonymousClass1Y3.AOB
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)
            X.2Qg r2 = (X.C46492Qg) r2
            android.content.Context r1 = r13.A00
            X.2IC r0 = new X.2IC
            r0.<init>(r2, r1)
            return r4
        L_0x04e1:
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r10.A0S
            com.google.common.base.Preconditions.checkNotNull(r3)
            com.facebook.messaging.mutators.ThreadCallNotificationsDialogFragment r2 = new com.facebook.messaging.mutators.ThreadCallNotificationsDialogFragment
            r2.<init>()
            android.os.Bundle r1 = new android.os.Bundle
            r1.<init>()
            java.lang.String r0 = "thread_key"
            r1.putParcelable(r0, r3)
            r2.A1P(r1)
            X.0qW r0 = r13.A01
            r2.A25(r0, r6)
            return r4
        L_0x04fe:
            int r1 = X.AnonymousClass1Y3.BLe
            X.0UN r0 = r13.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.0u1 r1 = (X.C14780u1) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            X.1Y7 r3 = X.C05690aA.A06(r0)
            com.facebook.prefs.shared.FbSharedPreferences r0 = r1.A03
            X.1hn r2 = r0.edit()
            r0 = 0
            r2.BzA(r3, r0)
            r2.commit()
            return r4
        L_0x051d:
            int r1 = X.AnonymousClass1Y3.B23
            X.0UN r0 = r13.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6bS r2 = (X.C137486bS) r2
            X.C137486bS.A01(r2, r3, r9, r4)
            return r4
        L_0x052b:
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            com.facebook.messaging.integrity.block.user.BlockUserFragment r2 = com.facebook.messaging.integrity.block.user.BlockUserFragment.A00(r5, r10, r0)
            X.0qW r1 = r13.A01
            java.lang.String r0 = "blockUserFragment"
            r2.A25(r1, r0)
            return r4
        L_0x0539:
            A02(r13, r3)
            return r4
        L_0x053d:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1B2.A07(int, java.lang.String, com.facebook.messaging.model.threads.ThreadSummary):boolean");
    }

    public AnonymousClass1B2(AnonymousClass1XY r3, CallerContext callerContext, Activity activity, Context context, C13060qW r7, C20121Az r8) {
        this.A02 = new AnonymousClass0UN(29, r3);
        this.A0C = AnonymousClass0VG.A00(AnonymousClass1Y3.AUX, r3);
        this.A09 = AnonymousClass0UU.A05(r3);
        this.A04 = AnonymousClass0WT.A00(r3);
        this.A0F = new AnonymousClass1B1(r3);
        this.A0D = new AnonymousClass1B3(r3);
        this.A0E = new AnonymousClass1B4(r3);
        this.A08 = activity;
        this.A00 = context;
        this.A0B = callerContext.A0F();
        this.A01 = r7;
        this.A05 = r8;
        this.A0A = new C20961Em(this.A0F, callerContext, context, r7);
    }

    public static void A02(AnonymousClass1B2 r4, ThreadKey threadKey) {
        C10950l8 r3;
        if (((int) threadKey.A0G()) != -102) {
            r3 = C10950l8.A05;
        } else {
            r3 = C10950l8.A0C;
        }
        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(18, AnonymousClass1Y3.Aoc, r4.A02), new AnonymousClass7ZJ(r4, r3), 1398018284);
    }
}
