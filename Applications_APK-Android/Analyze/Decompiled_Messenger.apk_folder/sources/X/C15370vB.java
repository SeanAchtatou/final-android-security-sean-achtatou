package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0vB  reason: invalid class name and case insensitive filesystem */
public final class C15370vB extends C15380vC {
    private static volatile C15370vB A02;
    public AnonymousClass0UN A00;
    public C199819ap A01;

    public static final C15370vB A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C15370vB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C15370vB(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static void A01(C15370vB r11) {
        AnonymousClass10O r8;
        int i;
        int i2;
        if (r11.A01 != null) {
            if (((AnonymousClass22D) AnonymousClass1XX.A03(AnonymousClass1Y3.Aay, r11.A00)).A00.Aem(282235187627258L)) {
                r8 = AnonymousClass10O.RED_DOT;
            } else {
                r8 = AnonymousClass10O.RED_WITH_TEXT_SMALL;
            }
            C199819ap r3 = r11.A01;
            if (r3.A0E) {
                i2 = 0;
            } else {
                int AqN = r3.A0D.AqN(C10980lB.A06, 0);
                boolean A022 = r3.A0C.A02();
                if (r3.A0B.A02()) {
                    i = r3.A07.A04(AnonymousClass2MG.A08);
                } else {
                    i = 0;
                }
                int i3 = AnonymousClass1Y3.AZ0;
                AnonymousClass0UN r1 = r3.A00;
                int i4 = ((C100384ql) AnonymousClass1XX.A02(1, i3, r1)).Ae7().A00;
                int i5 = 0;
                if (i4 != 0) {
                    i5 = i4;
                }
                C93254cP r2 = (C93254cP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BOY, r1);
                r2.A03 = AqN;
                r2.A02 = A022 ? 1 : 0;
                r2.A01 = 0;
                r2.A00 = i;
                r2.A04 = i5;
                i2 = i5 + AqN + A022 + 0 + i;
            }
            r11.A03(new C15400vE(i2, r8));
        }
    }

    private C15370vB(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
