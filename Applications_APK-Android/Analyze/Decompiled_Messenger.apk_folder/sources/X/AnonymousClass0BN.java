package X;

import android.os.SystemClock;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.0BN  reason: invalid class name */
public final class AnonymousClass0BN {
    public final AnonymousClass0B6 A00;
    public final AnonymousClass0B7 A01;
    public final C01520Ao A02;
    public final C01530Ap A03;
    public final Map A04 = new HashMap();

    public AnonymousClass0CV A01(AnonymousClass0C8 r13, String str, AnonymousClass0CL r15, int i, int i2) {
        AnonymousClass0CV r2;
        AnonymousClass0A1.A00(r13);
        AnonymousClass0CV r5 = new AnonymousClass0CV(r13, str, r15, i, SystemClock.elapsedRealtime());
        synchronized (this.A04) {
            r2 = (AnonymousClass0CV) this.A04.put(Integer.valueOf(r5.A01), r5);
        }
        if (r2 != null) {
            r2.A01();
            C010708t.A0O("MqttOperationManager", "operation/add/duplicate; id=%d, name=%s", Integer.valueOf(r2.A01), r2.A04.name());
        }
        r5.A02(this.A03.schedule(new C01890Ca(this, r5), (long) i2, TimeUnit.SECONDS));
        return r5;
    }

    public static void A00(AnonymousClass0BN r14, AnonymousClass0CV r15) {
        Integer valueOf;
        boolean z;
        AnonymousClass0CG r2;
        int i;
        long j;
        AnonymousClass0CL r3 = r15.A04;
        int i2 = r15.A01;
        AnonymousClass0C8 r1 = r15.A03;
        synchronized (r14.A04) {
            Map map = r14.A04;
            valueOf = Integer.valueOf(i2);
            if (map.get(valueOf) == r15) {
                r14.A04.remove(valueOf);
                z = true;
            } else {
                z = false;
            }
        }
        if (z) {
            int AwM = r15.AwM();
            boolean z2 = r15 instanceof C02070Cs;
            if (z2) {
                AwM = ((C02070Cs) r15).A00;
            }
            if (z2) {
                i = ((C02070Cs) r15).A01;
            } else {
                i = 0;
            }
            if (r1 == null) {
                j = 0;
            } else {
                j = r1.A0W;
            }
            r14.A00.A05("timeout", r15.A05, AnonymousClass08G.A00(AnonymousClass07B.A01), i2, AwM, null, i, j);
        } else {
            C010708t.A0P("MqttOperationManager", "operation/timeout/duplicate; id=%d, operation=%s, client=%s", valueOf, r3.name(), r1);
        }
        r15.A01();
        if (r3.equals(AnonymousClass0CL.A06) || r3.equals(AnonymousClass0CL.A07)) {
            TimeoutException timeoutException = new TimeoutException();
            if (r3.equals(AnonymousClass0CL.A06)) {
                r2 = AnonymousClass0CG.A03;
            } else {
                r2 = AnonymousClass0CG.A06;
            }
            synchronized (r1) {
                AnonymousClass0C8.A04(r1, AnonymousClass0CE.A09, r2, timeoutException);
            }
        }
    }

    public AnonymousClass0BN(C01520Ao r2, C01530Ap r3, AnonymousClass0B6 r4, AnonymousClass0B7 r5) {
        this.A02 = r2;
        this.A03 = r3;
        this.A00 = r4;
        this.A01 = r5;
    }
}
