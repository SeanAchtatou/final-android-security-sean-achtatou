package X;

/* renamed from: X.1OF  reason: invalid class name */
public final class AnonymousClass1OF implements AnonymousClass1O4 {
    private static final byte[] A00 = AnonymousClass1O5.A01("CD_");

    public AnonymousClass1O3 AXH(byte[] bArr, int i) {
        if (AnonymousClass1O5.A00(bArr, A00, 0)) {
            return AnonymousClass1OE.A00;
        }
        return null;
    }

    public int Aoc() {
        return A00.length;
    }
}
