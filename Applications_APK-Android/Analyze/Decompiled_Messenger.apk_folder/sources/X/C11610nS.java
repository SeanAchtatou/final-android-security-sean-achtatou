package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

/* renamed from: X.0nS  reason: invalid class name and case insensitive filesystem */
public abstract class C11610nS {
    public View A08(int i) {
        C11600nR r0 = (C11600nR) this;
        C11600nR.A02(r0);
        return r0.A0f.findViewById(i);
    }

    public C24367ByW A09() {
        C11600nR r3 = (C11600nR) this;
        C11600nR.A02(r3);
        if (r3.A0M && r3.A07 == null) {
            Window.Callback callback = r3.A0e;
            if (callback instanceof Activity) {
                r3.A07 = new C27941Dlf((Activity) callback, r3.A0R);
            } else if (callback instanceof Dialog) {
                r3.A07 = new C27941Dlf((Dialog) callback);
            }
            C24367ByW byW = r3.A07;
            if (byW != null) {
                byW.A0K(r3.A0J);
            }
        }
        return r3.A07;
    }

    public void A0A() {
        C11600nR r2 = (C11600nR) this;
        LayoutInflater from = LayoutInflater.from(r2.A0T());
        if (from.getFactory() == null) {
            AnonymousClass14W.A00(from, r2);
        } else {
            from.getFactory2();
        }
    }

    public void A0B() {
        C11600nR r1 = (C11600nR) this;
        C24367ByW A09 = r1.A09();
        if (A09 == null || !A09.A0N()) {
            C11600nR.A04(r1, 0);
        }
    }

    public void A0C() {
        C11600nR r2 = (C11600nR) this;
        if (r2.A0N) {
            r2.A0f.getDecorView().removeCallbacks(r2.A0h);
        }
        r2.A0O = true;
        C24367ByW byW = r2.A07;
        if (byW != null) {
            byW.A07();
        }
        C27630DgE dgE = r2.A08;
        if (dgE != null) {
            dgE.A00();
        }
    }

    public void A0D() {
        C24367ByW A09 = ((C11600nR) this).A09();
        if (A09 != null) {
            A09.A0L(true);
        }
    }

    public void A0E() {
        ((C11600nR) this).A0R();
    }

    public void A0F() {
        C11600nR r2 = (C11600nR) this;
        C24367ByW A09 = r2.A09();
        if (A09 != null) {
            A09.A0L(false);
        }
        C27630DgE dgE = r2.A08;
        if (dgE != null) {
            dgE.A00();
        }
    }

    public void A0G(int i) {
        C11600nR r0 = (C11600nR) this;
        r0.A02 = i;
        Context context = r0.A03;
        if (context != null) {
            context.setTheme(i);
        }
    }

    public void A0H(int i) {
        C11600nR r3 = (C11600nR) this;
        C11600nR.A02(r3);
        ViewGroup viewGroup = (ViewGroup) r3.A04.findViewById(16908290);
        viewGroup.removeAllViews();
        if (r3.A03 != null) {
            LayoutInflater.from(r3.A0d).cloneInContext(r3.A03).inflate(i, viewGroup);
        } else {
            LayoutInflater.from(r3.A0d).inflate(i, viewGroup);
        }
        r3.A0e.onContentChanged();
    }

    public void A0I(Configuration configuration) {
        C24367ByW A09;
        C11600nR r2 = (C11600nR) this;
        if (r2.A0M && r2.A0T && (A09 = r2.A09()) != null) {
            A09.A0F(configuration);
        }
        C27709Dhd A05 = C27709Dhd.A05();
        A05.A0F(r2.A0d);
        Context context = r2.A03;
        if (context != null) {
            A05.A0F(context);
        }
        r2.A0R();
    }

    public void A0J(Bundle bundle) {
        C11600nR r3 = (C11600nR) this;
        Context A0T = r3.A0T();
        String str = null;
        C27730Di0 di0 = new C27730Di0(A0T, A0T.obtainStyledAttributes((AttributeSet) null, C11600nR.A0j));
        Drawable A02 = di0.A02(0);
        if (A02 != null) {
            r3.A0f.setBackgroundDrawable(A02);
        }
        di0.A03();
        Window.Callback callback = r3.A0e;
        if (callback instanceof Activity) {
            try {
                str = AnonymousClass2Sm.A00((Activity) callback);
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                C24367ByW byW = r3.A07;
                if (byW == null) {
                    r3.A0J = true;
                } else {
                    byW.A0K(true);
                }
            }
        }
        if (bundle != null && r3.A01 == -100) {
            r3.A01 = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    public void A0K(Bundle bundle) {
        C11600nR.A02((C11600nR) this);
    }

    public void A0L(Bundle bundle) {
        int i = ((C11600nR) this).A01;
        if (i != -100) {
            bundle.putInt("appcompat:local_night_mode", i);
        }
    }

    public void A0M(View view) {
        C11600nR r2 = (C11600nR) this;
        C11600nR.A02(r2);
        ViewGroup viewGroup = (ViewGroup) r2.A04.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        r2.A0e.onContentChanged();
    }

    public void A0N(View view, ViewGroup.LayoutParams layoutParams) {
        C11600nR r2 = (C11600nR) this;
        C11600nR.A02(r2);
        ((ViewGroup) r2.A04.findViewById(16908290)).addView(view, layoutParams);
        r2.A0e.onContentChanged();
    }

    public void A0O(View view, ViewGroup.LayoutParams layoutParams) {
        C11600nR r2 = (C11600nR) this;
        C11600nR.A02(r2);
        ViewGroup viewGroup = (ViewGroup) r2.A04.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        r2.A0e.onContentChanged();
    }

    public void A0P(CharSequence charSequence) {
        C11600nR r1 = (C11600nR) this;
        r1.A0G = charSequence;
        C27998Dme dme = r1.A0E;
        if (dme != null) {
            dme.CDM(charSequence);
            return;
        }
        C24367ByW byW = r1.A07;
        if (byW != null) {
            byW.A0I(charSequence);
            return;
        }
        TextView textView = r1.A06;
        if (textView != null) {
            textView.setText(charSequence);
        }
    }

    public boolean A0Q(int i) {
        C11600nR r4 = (C11600nR) this;
        if (i == 8) {
            i = 108;
        } else if (i == 9) {
            i = AnonymousClass1Y3.A0o;
        }
        if (r4.A0U && i == 108) {
            return false;
        }
        if (r4.A0M && i == 1) {
            r4.A0M = false;
        }
        if (i == 1) {
            C11600nR.A03(r4);
            r4.A0U = true;
            return true;
        } else if (i == 2) {
            C11600nR.A03(r4);
            r4.A0L = true;
            return true;
        } else if (i == 5) {
            C11600nR.A03(r4);
            r4.A0K = true;
            return true;
        } else if (i == 10) {
            C11600nR.A03(r4);
            r4.A0S = true;
            return true;
        } else if (i == 108) {
            C11600nR.A03(r4);
            r4.A0M = true;
            return true;
        } else if (i != 109) {
            return r4.A0f.requestFeature(i);
        } else {
            C11600nR.A03(r4);
            r4.A0R = true;
            return true;
        }
    }

    public abstract boolean A0R();
}
