package X;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1Gd  reason: invalid class name and case insensitive filesystem */
public final class C21321Gd<E> implements Collection<E>, Set<E> {
    private static int A04;
    private static int A05;
    private static Object[] A06;
    private static Object[] A07;
    private static final Object A08 = new Object();
    private static final Object A09 = new Object();
    private static final int[] A0A = new int[0];
    private static final Object[] A0B = new Object[0];
    public int A00;
    public C04180St A01;
    public int[] A02;
    public Object[] A03;

    private void A02(int i) {
        if (i == 8) {
            synchronized (A09) {
                try {
                    Object[] objArr = A07;
                    if (objArr != null) {
                        try {
                            this.A03 = objArr;
                            A07 = (Object[]) objArr[0];
                            int[] iArr = (int[]) objArr[1];
                            this.A02 = iArr;
                            if (iArr != null) {
                                objArr[1] = null;
                                objArr[0] = null;
                                A05--;
                                return;
                            }
                        } catch (ClassCastException unused) {
                        }
                        System.out.println("ArraySet Found corrupt ArraySet cache: [0]=" + objArr[0] + " [1]=" + objArr[1]);
                        A07 = null;
                        A05 = 0;
                    }
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
        } else if (i == 4) {
            synchronized (A08) {
                try {
                    Object[] objArr2 = A06;
                    if (objArr2 != null) {
                        try {
                            this.A03 = objArr2;
                            A06 = (Object[]) objArr2[0];
                            int[] iArr2 = (int[]) objArr2[1];
                            this.A02 = iArr2;
                            if (iArr2 != null) {
                                objArr2[1] = null;
                                objArr2[0] = null;
                                A04--;
                                return;
                            }
                        } catch (ClassCastException unused2) {
                        }
                        System.out.println("ArraySet Found corrupt ArraySet cache: [0]=" + objArr2[0] + " [1]=" + objArr2[1]);
                        A06 = null;
                        A04 = 0;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        this.A02 = new int[i];
        this.A03 = new Object[i];
    }

    private static void A03(int[] iArr, Object[] objArr, int i) {
        int length = iArr.length;
        if (length == 8) {
            synchronized (A09) {
                try {
                    int i2 = A05;
                    if (i2 < 10) {
                        objArr[0] = A07;
                        objArr[1] = iArr;
                        for (int i3 = i - 1; i3 >= 2; i3--) {
                            objArr[i3] = null;
                        }
                        A07 = objArr;
                        A05 = i2 + 1;
                    }
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
        } else if (length == 4) {
            synchronized (A08) {
                try {
                    int i4 = A04;
                    if (i4 < 10) {
                        objArr[0] = A06;
                        objArr[1] = iArr;
                        for (int i5 = i - 1; i5 >= 2; i5--) {
                            objArr[i5] = null;
                        }
                        A06 = objArr;
                        A04 = i4 + 1;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Set) {
                Set set = (Set) obj;
                if (size() == set.size()) {
                    int i = 0;
                    while (i < this.A00) {
                        try {
                            if (set.contains(this.A03[i])) {
                                i++;
                            }
                        } catch (ClassCastException | NullPointerException unused) {
                        }
                    }
                }
                return false;
            }
            return false;
        }
        return true;
    }

    private int A00() {
        int i = this.A00;
        if (i == 0) {
            return -1;
        }
        int[] iArr = this.A02;
        try {
            int A022 = AnonymousClass04d.A02(iArr, i, 0);
            if (A022 >= 0) {
                Object[] objArr = this.A03;
                if (objArr[A022] != null) {
                    int i2 = A022 + 1;
                    while (i2 < i && iArr[i2] == 0) {
                        if (objArr[i2] == null) {
                            return i2;
                        }
                        i2++;
                    }
                    do {
                        A022--;
                        if (A022 < 0 || iArr[A022] != 0) {
                            return i2 ^ -1;
                        }
                    } while (objArr[A022] != null);
                    return A022;
                }
            }
            return A022;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    private int A01(Object obj, int i) {
        int i2 = this.A00;
        if (i2 == 0) {
            return -1;
        }
        try {
            int A022 = AnonymousClass04d.A02(this.A02, i2, i);
            if (A022 < 0 || obj.equals(this.A03[A022])) {
                return A022;
            }
            int i3 = A022 + 1;
            while (i3 < i2 && this.A02[i3] == i) {
                if (obj.equals(this.A03[i3])) {
                    return i3;
                }
                i3++;
            }
            do {
                A022--;
                if (A022 < 0 || this.A02[A022] != i) {
                    return i3 ^ -1;
                }
            } while (!obj.equals(this.A03[A022]));
            return A022;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    public int A04(Object obj) {
        if (obj == null) {
            return A00();
        }
        return A01(obj, obj.hashCode());
    }

    public void A05(int i) {
        int i2 = this.A00;
        Object[] objArr = this.A03;
        if (i2 <= 1) {
            clear();
            return;
        }
        int i3 = i2 - 1;
        int[] iArr = this.A02;
        int length = iArr.length;
        int i4 = 8;
        if (length <= 8 || i2 >= length / 3) {
            if (i < i3) {
                int i5 = i + 1;
                int i6 = i3 - i;
                System.arraycopy(iArr, i5, iArr, i, i6);
                Object[] objArr2 = this.A03;
                System.arraycopy(objArr2, i5, objArr2, i, i6);
            }
            this.A03[i3] = null;
        } else {
            if (i2 > 8) {
                i4 = i2 + (i2 >> 1);
            }
            A02(i4);
            if (i > 0) {
                System.arraycopy(iArr, 0, this.A02, 0, i);
                System.arraycopy(objArr, 0, this.A03, 0, i);
            }
            if (i < i3) {
                int i7 = i + 1;
                int i8 = i3 - i;
                System.arraycopy(iArr, i7, this.A02, i, i8);
                System.arraycopy(objArr, i7, this.A03, i, i8);
            }
        }
        if (i2 == this.A00) {
            this.A00 = i3;
            return;
        }
        throw new ConcurrentModificationException();
    }

    public boolean add(Object obj) {
        int hashCode;
        int A012;
        int i = this.A00;
        if (obj == null) {
            A012 = A00();
            hashCode = 0;
        } else {
            hashCode = obj.hashCode();
            A012 = A01(obj, hashCode);
        }
        if (A012 >= 0) {
            return false;
        }
        int i2 = A012 ^ -1;
        int[] iArr = this.A02;
        if (i >= iArr.length) {
            int i3 = 4;
            if (i >= 8) {
                i3 = (i >> 1) + i;
            } else if (i >= 4) {
                i3 = 8;
            }
            Object[] objArr = this.A03;
            A02(i3);
            if (i == this.A00) {
                int[] iArr2 = this.A02;
                if (iArr2.length > 0) {
                    System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                    System.arraycopy(objArr, 0, this.A03, 0, objArr.length);
                }
                A03(iArr, objArr, i);
            }
            throw new ConcurrentModificationException();
        }
        if (i2 < i) {
            int[] iArr3 = this.A02;
            int i4 = i2 + 1;
            int i5 = i - i2;
            System.arraycopy(iArr3, i2, iArr3, i4, i5);
            Object[] objArr2 = this.A03;
            System.arraycopy(objArr2, i2, objArr2, i4, i5);
        }
        int i6 = this.A00;
        if (i == i6) {
            int[] iArr4 = this.A02;
            if (i2 < iArr4.length) {
                iArr4[i2] = hashCode;
                this.A03[i2] = obj;
                this.A00 = i6 + 1;
                return true;
            }
        }
        throw new ConcurrentModificationException();
    }

    public boolean addAll(Collection collection) {
        int size = this.A00 + collection.size();
        int i = this.A00;
        int[] iArr = this.A02;
        if (iArr.length < size) {
            Object[] objArr = this.A03;
            A02(size);
            int i2 = this.A00;
            if (i2 > 0) {
                System.arraycopy(iArr, 0, this.A02, 0, i2);
                System.arraycopy(objArr, 0, this.A03, 0, this.A00);
            }
            A03(iArr, objArr, this.A00);
        }
        if (this.A00 == i) {
            boolean z = false;
            for (Object add : collection) {
                z |= add(add);
            }
            return z;
        }
        throw new ConcurrentModificationException();
    }

    public void clear() {
        int i = this.A00;
        if (i != 0) {
            int[] iArr = this.A02;
            Object[] objArr = this.A03;
            this.A02 = A0A;
            this.A03 = A0B;
            this.A00 = 0;
            A03(iArr, objArr, i);
        }
        if (this.A00 != 0) {
            throw new ConcurrentModificationException();
        }
    }

    public int hashCode() {
        int[] iArr = this.A02;
        int i = this.A00;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            i2 += iArr[i3];
        }
        return i2;
    }

    public boolean isEmpty() {
        if (this.A00 <= 0) {
            return true;
        }
        return false;
    }

    public Iterator iterator() {
        if (this.A01 == null) {
            this.A01 = new C74473hv(this);
        }
        C04180St r1 = this.A01;
        if (r1.A01 == null) {
            r1.A01 = new AnonymousClass1WP(r1);
        }
        return r1.A01.iterator();
    }

    public boolean retainAll(Collection collection) {
        boolean z = false;
        for (int i = this.A00 - 1; i >= 0; i--) {
            if (!collection.contains(this.A03[i])) {
                A05(i);
                z = true;
            }
        }
        return z;
    }

    public int size() {
        return this.A00;
    }

    public boolean contains(Object obj) {
        if (A04(obj) >= 0) {
            return true;
        }
        return false;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean remove(Object obj) {
        int A042 = A04(obj);
        if (A042 < 0) {
            return false;
        }
        A05(A042);
        return true;
    }

    public boolean removeAll(Collection collection) {
        boolean z = false;
        for (Object remove : collection) {
            z |= remove(remove);
        }
        return z;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.A00 * 14);
        sb.append('{');
        for (int i = 0; i < this.A00; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            Object obj = this.A03[i];
            if (obj != this) {
                sb.append(obj);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public C21321Gd() {
        this(0);
    }

    private C21321Gd(int i) {
        if (i == 0) {
            this.A02 = A0A;
            this.A03 = A0B;
        } else {
            A02(i);
        }
        this.A00 = 0;
    }

    public Object[] toArray() {
        int i = this.A00;
        Object[] objArr = new Object[i];
        System.arraycopy(this.A03, 0, objArr, 0, i);
        return objArr;
    }

    public Object[] toArray(Object[] objArr) {
        if (objArr.length < this.A00) {
            objArr = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), this.A00);
        }
        System.arraycopy(this.A03, 0, objArr, 0, this.A00);
        int length = objArr.length;
        int i = this.A00;
        if (length > i) {
            objArr[i] = null;
        }
        return objArr;
    }
}
