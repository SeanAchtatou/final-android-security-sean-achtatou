package X;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;

/* renamed from: X.12t  reason: invalid class name and case insensitive filesystem */
public final class C183812t implements Iterable {
    public LinkedHashMap _methods;

    public void add(C29141fw r5) {
        if (this._methods == null) {
            this._methods = new LinkedHashMap();
        }
        LinkedHashMap linkedHashMap = this._methods;
        Method method = r5._method;
        linkedHashMap.put(new C184212z(method.getName(), method.getParameterTypes()), r5);
    }

    public Iterator iterator() {
        LinkedHashMap linkedHashMap = this._methods;
        if (linkedHashMap != null) {
            return linkedHashMap.values().iterator();
        }
        return Collections.emptyList().iterator();
    }
}
