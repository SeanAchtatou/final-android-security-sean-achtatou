package X;

import com.facebook.cameracore.camerasdk.camera.FbCameraPhysicalDeviceLifecycleWrapperV2;
import com.facebook.cameracore.camerasdk.fboptic.Camera1Device;

/* renamed from: X.1xM  reason: invalid class name and case insensitive filesystem */
public final class C38391xM implements DXU {
    public final /* synthetic */ FbCameraPhysicalDeviceLifecycleWrapperV2 A00;
    public final /* synthetic */ DXU A01;
    public final /* synthetic */ DWR A02;

    public C38391xM(FbCameraPhysicalDeviceLifecycleWrapperV2 fbCameraPhysicalDeviceLifecycleWrapperV2, DXU dxu, DWR dwr) {
        this.A00 = fbCameraPhysicalDeviceLifecycleWrapperV2;
        this.A01 = dxu;
        this.A02 = dwr;
    }

    public void BXw(Throwable th) {
        this.A00.A09 = false;
        this.A02.ARG(AnonymousClass07B.A01);
        FbCameraPhysicalDeviceLifecycleWrapperV2.A04(this.A01, th);
    }

    public void Bbf() {
        this.A00.A09 = false;
        this.A02.ARG(AnonymousClass07B.A0C);
        FbCameraPhysicalDeviceLifecycleWrapperV2.A03(this.A01);
    }

    public void onSuccess() {
        this.A00.A09 = false;
        FbCameraPhysicalDeviceLifecycleWrapperV2 fbCameraPhysicalDeviceLifecycleWrapperV2 = this.A00;
        Camera1Device.A02(fbCameraPhysicalDeviceLifecycleWrapperV2.A07, new C29174EOw(fbCameraPhysicalDeviceLifecycleWrapperV2, fbCameraPhysicalDeviceLifecycleWrapperV2.A01.A00(), this.A01), null, this.A00.A01);
    }
}
