package X;

/* renamed from: X.1oW  reason: invalid class name and case insensitive filesystem */
public final class C34021oW implements C01690Bg {
    public final /* synthetic */ C34351pR A00;

    public C34021oW(C34351pR r1) {
        this.A00 = r1;
    }

    public Object get() {
        C16900xx r6 = this.A00.A06;
        C001500z r1 = r6.A01;
        String str = null;
        if (r1 == C001500z.A07 || r1 == C001500z.A08) {
            String B4E = r6.A02.B4E(845567391432859L, null);
            str = r6.A03.B4F(C05690aA.A0p, null);
            long now = r6.A00.now();
            long At2 = now - r6.A03.At2(C05690aA.A0q, now);
            if (At2 == 0) {
                return B4E;
            }
            if (At2 < 0 || At2 > 14400000) {
                C30281hn edit = r6.A03.edit();
                edit.C1B(C05690aA.A0q);
                edit.C1B(C05690aA.A0p);
                edit.commit();
                return B4E;
            }
        }
        return str;
    }
}
