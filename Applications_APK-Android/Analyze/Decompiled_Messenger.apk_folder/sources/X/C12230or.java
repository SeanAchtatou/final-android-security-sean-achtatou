package X;

import com.facebook.xanalytics.XAnalyticsAdapterHolder;
import com.facebook.xanalytics.XAnalyticsHolder;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0or  reason: invalid class name and case insensitive filesystem */
public final class C12230or implements C25801aQ {
    private static final Class A04 = C12230or.class;
    private static volatile C12230or A05;
    public final C07380dK A00;
    public final C05030Xj A01;
    public final C06230bA A02;
    private final XAnalyticsAdapterHolder A03 = new XAnalyticsAdapterHolder(new C09310gz(this));

    public static final C12230or A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C12230or.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C12230or(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:34|35|36|37|38) */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0072, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0076 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x007d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(java.lang.String r5, X.AnonymousClass0ZF r6) {
        /*
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x008b, AssertionError -> 0x007e }
            java.lang.String r0 = "UTF8"
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r0)     // Catch:{ IOException -> 0x008b, AssertionError -> 0x007e }
            byte[] r0 = r5.getBytes(r0)     // Catch:{ IOException -> 0x008b, AssertionError -> 0x007e }
            r2.<init>(r0)     // Catch:{ IOException -> 0x008b, AssertionError -> 0x007e }
            android.util.JsonReader r3 = new android.util.JsonReader     // Catch:{ all -> 0x0077 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ all -> 0x0077 }
            r0.<init>(r2)     // Catch:{ all -> 0x0077 }
            r3.<init>(r0)     // Catch:{ all -> 0x0077 }
            android.util.JsonToken r1 = r3.peek()     // Catch:{ all -> 0x0070 }
            android.util.JsonToken r0 = android.util.JsonToken.BEGIN_OBJECT     // Catch:{ all -> 0x0070 }
            if (r1 == r0) goto L_0x0028
            r3.close()     // Catch:{ all -> 0x0077 }
            r2.close()     // Catch:{ IOException -> 0x008b, AssertionError -> 0x007e }
            return
        L_0x0028:
            r3.beginObject()     // Catch:{ all -> 0x0070 }
        L_0x002b:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0070 }
            if (r0 == 0) goto L_0x0069
            java.lang.String r4 = r3.nextName()     // Catch:{ all -> 0x0070 }
            android.util.JsonToken r1 = r3.peek()     // Catch:{ all -> 0x0070 }
            android.util.JsonToken r0 = android.util.JsonToken.NUMBER     // Catch:{ all -> 0x0070 }
            if (r1 != r0) goto L_0x0049
            double r0 = r3.nextDouble()     // Catch:{ all -> 0x0070 }
            java.lang.Double r0 = java.lang.Double.valueOf(r0)     // Catch:{ all -> 0x0070 }
            r6.A09(r4, r0)     // Catch:{ all -> 0x0070 }
            goto L_0x002b
        L_0x0049:
            android.util.JsonToken r0 = android.util.JsonToken.STRING     // Catch:{ all -> 0x0070 }
            if (r1 != r0) goto L_0x0055
            java.lang.String r0 = r3.nextString()     // Catch:{ all -> 0x0070 }
            r6.A0A(r4, r0)     // Catch:{ all -> 0x0070 }
            goto L_0x002b
        L_0x0055:
            android.util.JsonToken r0 = android.util.JsonToken.BOOLEAN     // Catch:{ all -> 0x0070 }
            if (r1 != r0) goto L_0x0065
            boolean r0 = r3.nextBoolean()     // Catch:{ all -> 0x0070 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0070 }
            r6.A08(r4, r0)     // Catch:{ all -> 0x0070 }
            goto L_0x002b
        L_0x0065:
            r3.skipValue()     // Catch:{ all -> 0x0070 }
            goto L_0x002b
        L_0x0069:
            r3.close()     // Catch:{ all -> 0x0077 }
            r2.close()     // Catch:{ IOException -> 0x008b, AssertionError -> 0x007e }
            return
        L_0x0070:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0072 }
        L_0x0072:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x0076 }
        L_0x0076:
            throw r0     // Catch:{ all -> 0x0077 }
        L_0x0077:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x0079:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x007d }
        L_0x007d:
            throw r0     // Catch:{ IOException -> 0x008b, AssertionError -> 0x007e }
        L_0x007e:
            r3 = move-exception
            java.lang.Class r2 = X.C12230or.A04
            java.lang.Object[] r1 = new java.lang.Object[]{r5}
            java.lang.String r0 = "AssertionError from JsonReader.peek() for : %s "
            X.C010708t.A0E(r2, r3, r0, r1)
            throw r3
        L_0x008b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12230or.A01(java.lang.String, X.0ZF):void");
    }

    public XAnalyticsHolder BA9() {
        return this.A03;
    }

    private C12230or(AnonymousClass1XY r3) {
        this.A02 = AnonymousClass1ZF.A02(r3);
        this.A00 = C07380dK.A00(r3);
        this.A01 = C05020Xi.A00(r3);
    }
}
