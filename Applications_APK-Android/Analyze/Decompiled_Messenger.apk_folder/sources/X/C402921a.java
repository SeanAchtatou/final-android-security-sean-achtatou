package X;

import android.view.View;
import com.facebook.messaging.composer.OneLineComposerView;

/* renamed from: X.21a  reason: invalid class name and case insensitive filesystem */
public final class C402921a implements View.OnFocusChangeListener {
    public final /* synthetic */ AnonymousClass3OX A00;

    public C402921a(AnonymousClass3OX r1) {
        this.A00 = r1;
    }

    public void onFocusChange(View view, boolean z) {
        if (z) {
            AnonymousClass3OW r1 = this.A00.A0D;
            r1.A00.A0D.A00();
            OneLineComposerView.A0I(r1.A00, AnonymousClass07B.A00);
        }
    }
}
