package X;

import android.text.TextUtils;
import io.card.payment.BuildConfig;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONException;

/* renamed from: X.0RF  reason: invalid class name */
public final class AnonymousClass0RF {
    public final AnonymousClass0AW A00;

    public static AnonymousClass0RG A00(String str, AnonymousClass0B0 r3) {
        String str2;
        try {
            str2 = r3.getString(str, BuildConfig.FLAVOR);
        } catch (Exception e) {
            C010708t.A0R("RegistrationState", e, "get reg state string failed");
            str2 = null;
        }
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        try {
            return AnonymousClass0RG.A00(str2);
        } catch (JSONException e2) {
            C010708t.A0R("RegistrationState", e2, "Parse failed");
            return null;
        }
    }

    public List A04() {
        Map all = this.A00.AbP(AnonymousClass07B.A0o).getAll();
        LinkedList linkedList = new LinkedList();
        for (Map.Entry entry : all.entrySet()) {
            try {
                entry.getKey();
                entry.getValue();
                AnonymousClass0RG A002 = AnonymousClass0RG.A00(entry.getValue().toString());
                if (A002 != null && !A002.A04) {
                    linkedList.add(A002);
                }
            } catch (JSONException e) {
                C010708t.A0R("RegistrationState", e, "Parse failed");
            }
        }
        return linkedList;
    }

    public void A05() {
        AnonymousClass0B0 AbP = this.A00.AbP(AnonymousClass07B.A0o);
        AnonymousClass0DD AY8 = AbP.AY8();
        for (String str : AbP.getAll().keySet()) {
            AnonymousClass0RG A002 = A00(str, AbP);
            if (A002 == null) {
                C010708t.A0O("RegistrationState", "invalid value for %s", str);
            } else {
                A002.A03 = BuildConfig.FLAVOR;
                A002.A00 = Long.valueOf(System.currentTimeMillis());
                try {
                    AY8.BzD(str, A002.A01());
                } catch (JSONException e) {
                    C010708t.A0R("RegistrationState", e, "RegistrationCacheEntry serialization failed");
                }
            }
        }
        AY8.commit();
    }

    public AnonymousClass0RF(C01600Aw r5, AnonymousClass0AW r6) {
        this.A00 = r6;
        AnonymousClass0B0 AbP = r6.AbP(AnonymousClass07B.A0N);
        String string = AbP.getString("mqtt_version", BuildConfig.FLAVOR);
        String str = r5.A00;
        if (!string.equals(str)) {
            A05();
            AnonymousClass0DD AY8 = AbP.AY8();
            AY8.BzD("mqtt_version", str);
            AY8.commit();
        }
    }

    public static boolean A01(String str, AnonymousClass0RG r3, AnonymousClass0B0 r4) {
        try {
            String A01 = r3.A01();
            AnonymousClass0DD AY8 = r4.AY8();
            AY8.BzD(str, A01);
            AY8.commit();
            return true;
        } catch (JSONException e) {
            C010708t.A0R("RegistrationState", e, "RegistrationCacheEntry serialization failed");
            return false;
        }
    }

    public String A02(String str) {
        AnonymousClass0A1.A01(!TextUtils.isEmpty(str));
        AnonymousClass0RG A002 = A00(str, this.A00.AbP(AnonymousClass07B.A0o));
        if (A002 == null) {
            return null;
        }
        return A002.A01;
    }

    public String A03(String str) {
        AnonymousClass0A1.A01(!TextUtils.isEmpty(str));
        AnonymousClass0RG A002 = A00(str, this.A00.AbP(AnonymousClass07B.A0o));
        if (A002 != null && !A002.A04) {
            long currentTimeMillis = System.currentTimeMillis();
            long longValue = A002.A00.longValue();
            if (longValue + 86400000 >= currentTimeMillis && longValue <= currentTimeMillis) {
                return A002.A03;
            }
        }
        return null;
    }
}
