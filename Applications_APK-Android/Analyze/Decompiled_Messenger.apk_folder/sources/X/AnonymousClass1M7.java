package X;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.facebook.litho.LithoView;

/* renamed from: X.1M7  reason: invalid class name */
public final class AnonymousClass1M7 implements AnonymousClass1M8 {
    public String getName() {
        return "x";
    }

    public float Ab1(C17730zN r2) {
        return (float) r2.A08.left;
    }

    public float Ab2(Object obj) {
        if (obj instanceof LithoView) {
            return ((LithoView) obj).getX();
        }
        if (obj instanceof View) {
            return C17700zK.A00((View) obj, true);
        }
        if (obj instanceof Drawable) {
            Drawable drawable = (Drawable) obj;
            return C17700zK.A00(C17700zK.A01(drawable), true) + ((float) drawable.getBounds().left);
        }
        throw new UnsupportedOperationException("Getting X from unsupported mount content: " + obj);
    }

    public void C3Y(Object obj) {
        if (obj instanceof View) {
            ((View) obj).setTranslationX(0.0f);
        }
    }

    public void C5f(Object obj, float f) {
        if (obj instanceof LithoView) {
            ((View) obj).setX(f);
        } else if (obj instanceof View) {
            View view = (View) obj;
            view.setX(f - C17700zK.A00((View) view.getParent(), true));
        } else if (obj instanceof Drawable) {
            Drawable drawable = (Drawable) obj;
            int A00 = (int) (f - C17700zK.A00(C17700zK.A01(drawable), true));
            int i = drawable.getBounds().top;
            Rect bounds = drawable.getBounds();
            drawable.setBounds(A00, i, bounds.width() + A00, bounds.height() + i);
        } else {
            throw new UnsupportedOperationException("Setting X on unsupported mount content: " + obj);
        }
    }
}
