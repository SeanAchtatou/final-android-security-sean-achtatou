package X;

import android.app.Activity;
import android.net.Uri;

/* renamed from: X.14z  reason: invalid class name and case insensitive filesystem */
public final class C187614z {
    public static final C187614z A00() {
        return new C187614z();
    }

    public static String A01(Activity activity) {
        String contentUri;
        String name = activity.getClass().getName();
        if ((activity instanceof AnonymousClass151) && (contentUri = ((AnonymousClass151) activity).getContentUri()) != null) {
            Uri parse = Uri.parse(contentUri);
            String scheme = parse.getScheme();
            if ("fb".equalsIgnoreCase(scheme) || "http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {
                name = AnonymousClass08S.A0P(name, " | ", new Uri.Builder().scheme(scheme).authority(parse.getAuthority()).build().toString());
            }
        }
        return name + " | window: " + activity.getWindow();
    }
}
