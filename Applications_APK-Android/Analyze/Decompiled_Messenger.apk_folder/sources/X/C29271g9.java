package X;

import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.OmnistoreCollections;
import com.facebook.omnistore.OmnistoreMqtt;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import com.facebook.omnistore.util.DeviceIdUtil;
import java.util.Collection;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1g9  reason: invalid class name and case insensitive filesystem */
public final class C29271g9 {
    public static final Class A0B = C29271g9.class;
    private static volatile C29271g9 A0C;
    public Omnistore A00 = null;
    public OmnistoreCollections A01 = null;
    public OmnistoreMqtt A02;
    public boolean A03 = false;
    public boolean A04 = true;
    public final AnonymousClass09P A05;
    public final C001300x A06;
    public final C29421gO A07;
    public final AnonymousClass1U4 A08;
    private final C29431gP A09;
    private final Set A0A;

    public static synchronized Omnistore A00(C29271g9 r2) {
        Omnistore omnistore;
        AnonymousClass3LZ r1;
        synchronized (r2) {
            omnistore = r2.A00;
            if (omnistore == null) {
                if (!DeviceIdUtil.isSupportedApp(r2.A06.A04)) {
                    r1 = new AnonymousClass3LZ("Trying to use omnistore from unexpected app");
                } else if (r2.A04) {
                    AnonymousClass1V4 A012 = r2.A08.A01(r2.A02.getProtocolProvider());
                    Omnistore omnistore2 = A012.A00;
                    r2.A00 = omnistore2;
                    r2.A01 = A012.A01;
                    C29421gO r0 = r2.A07;
                    omnistore2.addDeltaReceivedCallback(r0);
                    omnistore2.setCollectionIndexerFunction(r0);
                    omnistore2.addDeltaClusterCallback(r0);
                    omnistore2.addSnapshotStateChangedCallback(r0);
                    omnistore = r2.A00;
                } else {
                    r1 = new AnonymousClass3LZ("Trying to open omnistore between logout and login");
                }
                throw r1;
            }
        }
        return omnistore;
    }

    public static final C29271g9 A01(AnonymousClass1XY r11) {
        if (A0C == null) {
            synchronized (C29271g9.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        C001300x A022 = AnonymousClass0UU.A02(applicationInjector);
                        AnonymousClass0X5 r6 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A1u);
                        C24371Tj A003 = C24371Tj.A00(applicationInjector);
                        AnonymousClass1U4 A004 = AnonymousClass1U4.A00(applicationInjector);
                        AnonymousClass09P A012 = C04750Wa.A01(applicationInjector);
                        C29411gN.A00(applicationInjector);
                        A0C = new C29271g9(A022, r6, A003, A004, A012, new C29421gO(C04750Wa.A01(applicationInjector)), C29431gP.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    public static Iterable A02(C29271g9 r5) {
        Collection values;
        Collection values2;
        Set set = r5.A0A;
        C29431gP r2 = r5.A09;
        synchronized (r2) {
            for (OmnistoreComponent A012 : r2.A02) {
                r2.A01(A012);
            }
            values = r2.A00.values();
        }
        C29431gP r22 = r5.A09;
        synchronized (r22) {
            for (OmnistoreStoredProcedureComponent A022 : r22.A03) {
                r22.A02(A022);
            }
            values2 = r22.A01.values();
        }
        AnonymousClass064.A03(true);
        return new C33861oG(new Iterable[]{set, values, values2});
    }

    private C29271g9(C001300x r3, Set set, C24371Tj r5, AnonymousClass1U4 r6, AnonymousClass09P r7, C29421gO r8, C29431gP r9) {
        this.A06 = r3;
        this.A0A = set;
        this.A02 = new OmnistoreMqtt(r5, new C14200sm());
        this.A08 = r6;
        this.A05 = r7;
        this.A07 = r8;
        this.A09 = r9;
    }
}
