package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.18l  reason: invalid class name and case insensitive filesystem */
public final class C195118l {
    public static final String[] A02;
    public static final String[] A03;
    private static volatile C195118l A04;
    private final C195318n A00;
    private final C04310Tq A01;

    static {
        String str = AnonymousClass182.A05.A00;
        A03 = new String[]{str};
        A02 = new String[]{str, AnonymousClass182.A00.A00, AnonymousClass182.A02.A00, AnonymousClass182.A03.A00, AnonymousClass182.A04.A00};
    }

    public static final C195118l A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C195118l.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C195118l(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0085, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0086, code lost:
        if (r5 != null) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x008b, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.user.model.User A01(long r12) {
        /*
            r11 = this;
            X.0W6 r2 = X.AnonymousClass182.A05
            java.lang.String r1 = r2.A00
            java.lang.String r0 = java.lang.Long.toString(r12)
            X.0av r1 = X.C06160ax.A03(r1, r0)
            X.0Tq r0 = r11.A01
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r3 = r0.A01()
            java.lang.String[] r5 = X.C195118l.A02
            java.lang.String r6 = r1.A02()
            java.lang.String[] r7 = r1.A04()
            java.lang.String r4 = "thread_participants"
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r5 = r3.query(r4, r5, r6, r7, r8, r9, r10)
            boolean r0 = r5.moveToNext()     // Catch:{ all -> 0x0083 }
            if (r0 == 0) goto L_0x007f
            X.18n r9 = r11.A00     // Catch:{ all -> 0x0083 }
            java.lang.String r8 = r2.A05(r5)     // Catch:{ all -> 0x0083 }
            X.0W6 r0 = X.AnonymousClass182.A00     // Catch:{ all -> 0x0083 }
            java.lang.String r7 = r0.A05(r5)     // Catch:{ all -> 0x0083 }
            X.0W6 r0 = X.AnonymousClass182.A02     // Catch:{ all -> 0x0083 }
            java.lang.String r6 = r0.A05(r5)     // Catch:{ all -> 0x0083 }
            X.0W6 r0 = X.AnonymousClass182.A03     // Catch:{ all -> 0x0083 }
            java.lang.String r4 = r0.A05(r5)     // Catch:{ all -> 0x0083 }
            X.0W6 r0 = X.AnonymousClass182.A04     // Catch:{ all -> 0x0083 }
            java.lang.String r3 = r0.A05(r5)     // Catch:{ all -> 0x0083 }
            com.facebook.user.model.Name r2 = new com.facebook.user.model.Name     // Catch:{ all -> 0x0083 }
            r2.<init>(r7, r6, r4)     // Catch:{ all -> 0x0083 }
            X.0cv r1 = new X.0cv     // Catch:{ all -> 0x0083 }
            r1.<init>()     // Catch:{ all -> 0x0083 }
            r0 = 1
            r1.A1B = r0     // Catch:{ all -> 0x0083 }
            X.1aB r0 = X.C25651aB.A03     // Catch:{ all -> 0x0083 }
            r1.A03(r0, r8)     // Catch:{ all -> 0x0083 }
            r1.A0h = r7     // Catch:{ all -> 0x0083 }
            r1.A0j = r6     // Catch:{ all -> 0x0083 }
            r1.A0g = r4     // Catch:{ all -> 0x0083 }
            r1.A0J = r2     // Catch:{ all -> 0x0083 }
            if (r3 == 0) goto L_0x0077
            X.0jD r0 = r9.A00     // Catch:{ all -> 0x0083 }
            com.fasterxml.jackson.databind.JsonNode r0 = r0.A02(r3)     // Catch:{ all -> 0x0083 }
            com.facebook.user.profilepic.PicSquare r0 = X.C12630pi.A03(r0)     // Catch:{ all -> 0x0083 }
            r1.A0P = r0     // Catch:{ all -> 0x0083 }
        L_0x0077:
            com.facebook.user.model.User r0 = r1.A02()     // Catch:{ all -> 0x0083 }
            r5.close()
            return r0
        L_0x007f:
            r5.close()
            return r8
        L_0x0083:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0085 }
        L_0x0085:
            r0 = move-exception
            if (r5 == 0) goto L_0x008b
            r5.close()     // Catch:{ all -> 0x008b }
        L_0x008b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C195118l.A01(long):com.facebook.user.model.User");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003d, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0037, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0038, code lost:
        if (r1 != null) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(long r10) {
        /*
            r9 = this;
            X.0W6 r0 = X.AnonymousClass182.A05
            java.lang.String r1 = r0.A00
            java.lang.String r0 = java.lang.Long.toString(r10)
            X.0av r1 = X.C06160ax.A03(r1, r0)
            X.0Tq r0 = r9.A01
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r0 = r0.A01()
            java.lang.String[] r2 = X.C195118l.A03
            java.lang.String r3 = r1.A02()
            java.lang.String[] r4 = r1.A04()
            java.lang.String r1 = "thread_participants"
            r5 = 0
            r6 = 0
            r7 = 0
            java.lang.String r8 = "1"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)
            boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x0035 }
            r1.close()
            return r0
        L_0x0035:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ all -> 0x003d }
        L_0x003d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C195118l.A02(long):boolean");
    }

    private C195118l(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, r2);
        this.A00 = C195318n.A00(r2);
    }
}
