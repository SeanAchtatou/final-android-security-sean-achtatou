package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1bc  reason: invalid class name and case insensitive filesystem */
public final class C26541bc extends AnonymousClass0f8 {
    private static volatile C26541bc A01;
    private AnonymousClass0UN A00;

    public String Azg() {
        return "active_ttrc_markers";
    }

    public static final C26541bc A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C26541bc.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C26541bc(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        int[] iArr = (int[]) obj;
        int[] iArr2 = (int[]) obj2;
        if (iArr != null && iArr2 != null) {
            if (performanceLoggingEvent.A0K == null) {
                performanceLoggingEvent.A0K = new AnonymousClass1GD();
            }
            AnonymousClass1GD.A00(performanceLoggingEvent.A0K, "active_ttrc_markers_at_start", iArr);
            if (performanceLoggingEvent.A0K == null) {
                performanceLoggingEvent.A0K = new AnonymousClass1GD();
            }
            AnonymousClass1GD.A00(performanceLoggingEvent.A0K, "active_ttrc_markers_at_stop", iArr2);
        }
    }

    public long Azh() {
        return C08350fD.A00;
    }

    public Class B3O() {
        return int[].class;
    }

    public Object CGO() {
        int[] A02;
        C22155Apf apf = (C22155Apf) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AbD, this.A00);
        synchronized (apf) {
            A02 = C06950cM.A02(apf.A00);
        }
        return A02;
    }

    private C26541bc(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A00;
    }
}
