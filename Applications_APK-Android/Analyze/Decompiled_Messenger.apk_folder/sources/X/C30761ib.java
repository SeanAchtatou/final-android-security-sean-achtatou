package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.1ib  reason: invalid class name and case insensitive filesystem */
public final class C30761ib implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.storage.monitor.core.StorageResourceMonitor$1";
    public final /* synthetic */ C30751ia A00;

    public C30761ib(C30751ia r1) {
        this.A00 = r1;
    }

    public void run() {
        C30751ia r7 = this.A00;
        short s = 2;
        boolean z = false;
        try {
            r7.A04.markerStart(43253761);
            long A05 = r7.A03.A05(AnonymousClass07B.A00);
            if (r7.A00 == A05) {
                r7.A04.markerEnd(43253761, 4);
                return;
            }
            z = true;
            r7.A00 = A05;
            r7.A04.markerPoint(43253761, "last_available_space_changed");
            long j = r7.A00;
            HashSet hashSet = new HashSet();
            synchronized (r7.A06) {
                hashSet.addAll(r7.A06.keySet());
            }
            r7.A04.markerPoint(43253761, "notify_updates", AnonymousClass08S.A09("listener_count:", hashSet.size()));
            int i = 0;
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                ((C30741iZ) it.next()).BtP(j);
                i++;
            }
            r7.A04.markerPoint(43253761, "notify_updates_completed", AnonymousClass08S.A09("success_count:", i));
            r7.A04.markerEnd(43253761, 2);
        } catch (Exception e) {
            r7.A02.softReport("StorageResourceMonitor onAvailableSpaceChanged", e);
        } catch (Throwable th) {
            QuickPerformanceLogger quickPerformanceLogger = r7.A04;
            if (!z) {
                s = 4;
            }
            quickPerformanceLogger.markerEnd(43253761, s);
            throw th;
        }
    }
}
