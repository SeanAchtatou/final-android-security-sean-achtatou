package X;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import com.facebook.analytics.appstatelogger.AppStateLogFile;
import java.util.List;

/* renamed from: X.08i  reason: invalid class name and case insensitive filesystem */
public final class C009908i implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.appstatelogger.ProcessImportanceProvider";
    public C03330Nh A00;
    public final Object A01 = new Object();
    private final int A02;
    private final ActivityManager.RunningAppProcessInfo A03;
    private final Context A04;
    private final String A05;
    public volatile boolean A06;
    private volatile boolean A07;

    public static boolean A02(ActivityManager.RunningAppProcessInfo runningAppProcessInfo) {
        synchronized (runningAppProcessInfo) {
            try {
                ActivityManager.getMyMemoryState(runningAppProcessInfo);
            } catch (RuntimeException e) {
                C010708t.A0L("ProcessImportanceProvider", "Could not get current importance", e);
                return false;
            }
        }
        return true;
    }

    public static void A00(ActivityManager.RunningAppProcessInfo runningAppProcessInfo, ActivityManager.RunningAppProcessInfo runningAppProcessInfo2) {
        runningAppProcessInfo2.lastTrimLevel = runningAppProcessInfo.lastTrimLevel;
    }

    private boolean A01() {
        int i;
        boolean z;
        boolean z2;
        int i2;
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        synchronized (this.A03) {
            try {
                ActivityManager.RunningAppProcessInfo runningAppProcessInfo = this.A03;
                i = runningAppProcessInfo.importance;
                z = false;
                if (Build.VERSION.SDK_INT >= 16) {
                    z2 = A02(runningAppProcessInfo);
                } else {
                    ActivityManager activityManager = (ActivityManager) this.A04.getSystemService("activity");
                    if (activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null) {
                        z2 = false;
                    } else {
                        z2 = false;
                        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                            String str = next.processName;
                            if (str != null && str.equals(this.A05)) {
                                this.A03.importance = next.importance;
                                z2 = true;
                            }
                        }
                    }
                }
                if (z2) {
                    i2 = this.A03.importance;
                    if (!this.A07) {
                        z = true;
                    }
                    this.A07 = true;
                } else {
                    i2 = i;
                    z = true;
                }
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (!z2 || z || i == i2) {
            return z2;
        }
        synchronized (this.A01) {
            try {
                C03330Nh r0 = this.A00;
                if (r0 != null) {
                    AnonymousClass01P r5 = r0.A00;
                    if (r5.A0P) {
                        C001901i r1 = r5.A0B;
                        char A002 = AnonymousClass0KD.A00(i2);
                        if (r1.A0R) {
                            AppStateLogFile appStateLogFile = r1.A09;
                            if (appStateLogFile.mIsEnabled) {
                                AppStateLogFile.assertIsAscii(A002);
                                appStateLogFile.mMappedByteBuffer.put(4, (byte) A002);
                            }
                        }
                    }
                    if (r5.A0N) {
                        if (i <= 100 && i2 > 100) {
                            AnonymousClass01P.A09(r5, null, AnonymousClass01Y.IN_BACKGROUND_DUE_TO_LOW_IMPORTANCE);
                        } else if (i > 100 && i2 <= 100) {
                            AnonymousClass01P.A09(r5, r5.A0F, AnonymousClass01Y.IN_FOREGROUND);
                        }
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
        return z2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        if (r5.A07 != false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(android.app.ActivityManager.RunningAppProcessInfo r6) {
        /*
            r5 = this;
            int r0 = r5.A02
            r2 = 0
            r4 = 1
            if (r0 <= 0) goto L_0x0012
            boolean r0 = r5.A06
            if (r0 != 0) goto L_0x0012
            boolean r0 = r5.A07
        L_0x000c:
            if (r0 == 0) goto L_0x0033
            android.app.ActivityManager$RunningAppProcessInfo r3 = r5.A03
            monitor-enter(r3)
            goto L_0x001f
        L_0x0012:
            boolean r0 = r5.A01()
            if (r0 != 0) goto L_0x001d
            boolean r1 = r5.A07
            r0 = 0
            if (r1 == 0) goto L_0x000c
        L_0x001d:
            r0 = 1
            goto L_0x000c
        L_0x001f:
            android.app.ActivityManager$RunningAppProcessInfo r2 = r5.A03     // Catch:{ all -> 0x0030 }
            int r0 = r2.importance     // Catch:{ all -> 0x0030 }
            r6.importance = r0     // Catch:{ all -> 0x0030 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0030 }
            r0 = 16
            if (r1 < r0) goto L_0x002e
            A00(r2, r6)     // Catch:{ all -> 0x0030 }
        L_0x002e:
            monitor-exit(r3)     // Catch:{ all -> 0x0030 }
            return r4
        L_0x0030:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0030 }
            throw r0
        L_0x0033:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C009908i.A03(android.app.ActivityManager$RunningAppProcessInfo):boolean");
    }

    public C009908i(Context context, String str, int i, C03330Nh r6) {
        this.A04 = context;
        this.A05 = str;
        this.A02 = i;
        this.A03 = new ActivityManager.RunningAppProcessInfo();
        this.A00 = r6;
        if (i > 0) {
            this.A06 = false;
            new Thread(this, "ProcessImportanceProviderThread").start();
        }
    }

    public void run() {
        do {
            A01();
            try {
                Thread.sleep((long) this.A02);
            } catch (InterruptedException unused) {
            }
        } while (!this.A06);
    }
}
