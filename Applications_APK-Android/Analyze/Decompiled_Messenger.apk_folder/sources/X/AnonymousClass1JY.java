package X;

import android.net.Uri;
import com.facebook.user.model.Name;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.facebook.user.profilepic.PicSquare;
import com.google.common.base.Objects;
import java.util.Arrays;

/* renamed from: X.1JY  reason: invalid class name */
public final class AnonymousClass1JY {
    public final int A00;
    public final int A01;
    public final Uri A02;
    public final UserKey A03;
    public final PicSquare A04;
    public final AnonymousClass1KS A05;
    public final C21381Gs A06;
    public final String A07;
    public final String A08;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                AnonymousClass1JY r5 = (AnonymousClass1JY) obj;
                if (!this.A05.equals(r5.A05) || !Objects.equal(this.A04, r5.A04) || !Objects.equal(this.A03, r5.A03) || !Objects.equal(this.A06, r5.A06) || !Objects.equal(this.A07, r5.A07) || !Objects.equal(this.A08, r5.A08) || !Objects.equal(this.A02, r5.A02) || this.A01 != r5.A01) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public static AnonymousClass1JY A00(Uri uri) {
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.USER_URI;
        r1.A02 = uri;
        return new AnonymousClass1JY(r1);
    }

    public static AnonymousClass1JY A03(UserKey userKey) {
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.USER_KEY;
        r1.A03 = userKey;
        return new AnonymousClass1JY(r1);
    }

    public static AnonymousClass1JY A04(UserKey userKey) {
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.USER_KEY;
        r1.A03 = userKey;
        r1.A06 = C21381Gs.A0L;
        return new AnonymousClass1JY(r1);
    }

    public static AnonymousClass1JY A05(UserKey userKey, C21381Gs r3) {
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.USER_KEY;
        r1.A03 = userKey;
        r1.A06 = r3;
        return new AnonymousClass1JY(r1);
    }

    public static AnonymousClass1JY A06(PicSquare picSquare) {
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.PIC_SQUARE;
        r1.A04 = picSquare;
        return new AnonymousClass1JY(r1);
    }

    public static AnonymousClass1JY A07(PicSquare picSquare, C21381Gs r3) {
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.PIC_SQUARE;
        r1.A04 = picSquare;
        r1.A06 = r3;
        return new AnonymousClass1JY(r1);
    }

    public static AnonymousClass1JY A08(String str, Name name, C21381Gs r4, int i) {
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.SMS_CONTACT;
        r1.A07 = str;
        r1.A08 = name.A00();
        r1.A06 = r4;
        r1.A01 = i;
        return new AnonymousClass1JY(r1);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A05, this.A04, this.A03, this.A06, this.A07, this.A08, this.A02, Integer.valueOf(this.A01)});
    }

    public AnonymousClass1JY(AnonymousClass1KR r2) {
        this.A05 = r2.A05;
        this.A03 = r2.A03;
        this.A04 = r2.A04;
        this.A06 = r2.A06;
        this.A07 = r2.A07;
        this.A08 = r2.A08;
        this.A00 = r2.A00;
        this.A02 = r2.A02;
        this.A01 = r2.A01;
    }

    public static AnonymousClass1JY A01(User user) {
        if (!user.A0H()) {
            return A02(user, null);
        }
        User user2 = user.A0N;
        if (user2 != null) {
            return A05(user2.A0Q, null);
        }
        return A08(user.A07(), user.A0L, C21381Gs.A0L, 0);
    }

    public static AnonymousClass1JY A02(User user, C21381Gs r5) {
        PicSquare A042 = user.A04();
        if (A042 == null) {
            return A05(user.A0Q, r5);
        }
        UserKey userKey = user.A0Q;
        AnonymousClass1KR r1 = new AnonymousClass1KR();
        r1.A05 = AnonymousClass1KS.USER_KEY_WITH_FALLBACK_PIC_SQUARE;
        r1.A03 = userKey;
        r1.A04 = A042;
        r1.A06 = r5;
        return new AnonymousClass1JY(r1);
    }
}
