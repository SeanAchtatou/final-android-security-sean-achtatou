package X;

import java.util.Map;

/* renamed from: X.1pj  reason: invalid class name and case insensitive filesystem */
public final class C34511pj implements C01400Ab {
    public final /* synthetic */ C34351pR A00;

    public C34511pj(C34351pR r1) {
        this.A00 = r1;
    }

    public void C2i(C01750Bm r5) {
        AnonymousClass0ZF A03 = this.A00.A04.A03(C32411li.A00(r5.A00, r5.A01));
        if (A03.A0G()) {
            for (Map.Entry entry : r5.A02.entrySet()) {
                A03.A0A((String) entry.getKey(), (String) entry.getValue());
            }
            A03.A0E();
        }
    }
}
