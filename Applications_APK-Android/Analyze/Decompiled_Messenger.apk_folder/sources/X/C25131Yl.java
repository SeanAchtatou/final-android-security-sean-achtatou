package X;

import java.util.concurrent.Callable;

/* renamed from: X.1Yl  reason: invalid class name and case insensitive filesystem */
public abstract class C25131Yl implements Callable, AnonymousClass09S {
    public final Runnable A00;
    public final Callable A01;

    public Object getInnerRunnable() {
        Callable callable = this.A01;
        if (callable != null) {
            return callable;
        }
        Runnable runnable = this.A00;
        if (runnable != null) {
            return runnable;
        }
        return this;
    }

    public C25131Yl(Runnable runnable, Callable callable) {
        this.A00 = runnable;
        this.A01 = callable;
    }

    public Object A00(String str, AnonymousClass0XV r4, String str2) {
        Object obj;
        C005505z.A06(str, Integer.valueOf(r4.ordinal()), str2, 1695618842);
        try {
            Callable callable = this.A01;
            if (callable != null) {
                obj = callable.call();
            } else {
                Runnable runnable = this.A00;
                obj = null;
                if (runnable != null) {
                    runnable.run();
                }
            }
            return obj;
        } finally {
            C005505z.A00(-1310918057);
        }
    }
}
