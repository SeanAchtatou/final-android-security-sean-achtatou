package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.util.HashSet;
import java.util.Set;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0At  reason: invalid class name and case insensitive filesystem */
public final class C01570At {
    public long A00 = 0;
    public long A01 = -1;
    private long A02 = -1;
    private long A03;
    public final BroadcastReceiver A04;
    public final Context A05;
    public final Handler A06;
    public final Set A07 = new HashSet();
    private final C01540Aq A08;
    private final C01420Ad A09;

    public static synchronized void A00(C01570At r10, NetworkInfo networkInfo) {
        synchronized (r10) {
            if (networkInfo != null) {
                if (networkInfo.isConnected()) {
                    if (r10.A03 == 0) {
                        long elapsedRealtime = SystemClock.elapsedRealtime();
                        r10.A03 = elapsedRealtime;
                        long j = r10.A02;
                        if (j != -1) {
                            r10.A01 = elapsedRealtime - j;
                        }
                    }
                }
            }
            long elapsedRealtime2 = SystemClock.elapsedRealtime();
            r10.A02 = elapsedRealtime2;
            long j2 = r10.A03;
            if (j2 != 0) {
                r10.A00 += elapsedRealtime2 - j2;
            }
            r10.A01 = -1;
            r10.A03 = 0;
        }
    }

    public synchronized long A01() {
        long j;
        long j2 = this.A03;
        j = 0;
        if (j2 != 0) {
            j = SystemClock.elapsedRealtime() - j2;
        }
        return j;
    }

    public synchronized long A02() {
        return this.A03;
    }

    public NetworkInfo A03() {
        try {
            C01540Aq r1 = this.A08;
            if (r1.A02()) {
                return ((ConnectivityManager) r1.A01()).getActiveNetworkInfo();
            }
            return null;
        } catch (RuntimeException e) {
            C010708t.A0M("MqttNetworkManager", "getActiveNetworkInfoSafe caught Exception", e);
            return null;
        }
    }

    public boolean A06() {
        try {
            C01540Aq A002 = this.A09.A00("power", PowerManager.class);
            return Build.VERSION.SDK_INT >= 23 && A002.A02() && ((PowerManager) A002.A01()).isDeviceIdleMode();
        } catch (Exception unused) {
            C010708t.A0I("MqttNetworkManager", "Exception in getting DeviceIdleMode");
            return false;
        }
    }

    public C01570At(C01420Ad r6, Context context, Handler handler) {
        this.A09 = r6;
        this.A08 = r6.A00("connectivity", ConnectivityManager.class);
        this.A05 = context;
        this.A06 = handler;
        this.A04 = new C01580Au(this);
        A00(this, A03());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.A05.registerReceiver(this.A04, intentFilter, null, this.A06);
    }

    public NetworkInfo A04() {
        NetworkInfo A032 = A03();
        if (A032 == null || !A032.isConnected()) {
            return null;
        }
        return A032;
    }

    public Integer A05() {
        NetworkInfo A032 = A03();
        if (A032 == null || !A032.isConnected()) {
            return AnonymousClass07B.A01;
        }
        int type = A032.getType();
        int subtype = A032.getSubtype();
        if (type != 0) {
            if (type == 1) {
                return AnonymousClass07B.A0C;
            }
            if (!(type == 2 || type == 3 || type == 4 || type == 5)) {
                return AnonymousClass07B.A0o;
            }
        }
        switch (subtype) {
            case 1:
            case 2:
            case 4:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case AnonymousClass1Y3.A02 /*11*/:
                return AnonymousClass07B.A0N;
            case 3:
            case 5:
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
            case 8:
            case Process.SIGKILL /*9*/:
            case AnonymousClass1Y3.A01 /*10*/:
            case AnonymousClass1Y3.A03 /*12*/:
            case 14:
            case 15:
                return AnonymousClass07B.A0Y;
            case 13:
                return AnonymousClass07B.A0i;
            default:
                return AnonymousClass07B.A0n;
        }
    }
}
