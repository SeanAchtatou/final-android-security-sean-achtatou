package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1Lg  reason: invalid class name and case insensitive filesystem */
public final class C22421Lg extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public MigColorScheme A01;

    public C22421Lg(Context context) {
        super("MontageComposeTileComponent");
        this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }
}
