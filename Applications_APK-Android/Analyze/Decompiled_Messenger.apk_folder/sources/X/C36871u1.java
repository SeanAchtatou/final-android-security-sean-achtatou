package X;

import com.facebook.messaging.business.commerce.model.retail.CommerceData;
import com.facebook.messaging.model.payment.PaymentRequestData;
import com.facebook.messaging.model.payment.PaymentTransactionData;
import com.facebook.messaging.model.share.SentShareAttachment;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1u1  reason: invalid class name and case insensitive filesystem */
public interface C36871u1 {
    CommerceData Ahe();

    PaymentRequestData Axe();

    PaymentTransactionData Axg();

    SentShareAttachment B2Y();

    ImmutableList B2v();

    C61192yU BAB();
}
