package X;

import com.facebook.graphservice.interfaces.GraphQLQuery;
import com.facebook.graphservice.interfaces.GraphQLService;
import com.google.common.util.concurrent.SettableFuture;

/* renamed from: X.109  reason: invalid class name */
public final class AnonymousClass109 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.graphql.executor.GraphServiceQueryExecutor$1";
    public final /* synthetic */ AnonymousClass1BU A00;
    public final /* synthetic */ C09290gx A01;
    public final /* synthetic */ C10880l0 A02;
    public final /* synthetic */ GraphQLQuery A03;
    public final /* synthetic */ GraphQLService A04;
    public final /* synthetic */ SettableFuture A05;

    public AnonymousClass109(AnonymousClass1BU r1, GraphQLService graphQLService, GraphQLQuery graphQLQuery, C10880l0 r4, SettableFuture settableFuture, C09290gx r6) {
        this.A00 = r1;
        this.A04 = graphQLService;
        this.A03 = graphQLQuery;
        this.A02 = r4;
        this.A05 = settableFuture;
        this.A01 = r6;
    }

    public void run() {
        GraphQLService graphQLService = this.A04;
        GraphQLQuery graphQLQuery = this.A03;
        C10880l0 r6 = this.A02;
        SettableFuture settableFuture = this.A05;
        C09290gx r8 = this.A01;
        int i = AnonymousClass1Y3.AgK;
        AnonymousClass0UN r2 = this.A00.A00;
        graphQLService.handleQuery(graphQLQuery, new AnonymousClass2SN(r6, settableFuture, r8, (AnonymousClass06B) AnonymousClass1XX.A02(3, i, r2), (AnonymousClass200) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AtA, r2)), C25141Ym.INSTANCE);
    }
}
