package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import io.card.payment.BuildConfig;

/* renamed from: X.1OO  reason: invalid class name */
public class AnonymousClass1OO implements AnonymousClass1OL {
    public void BSA() {
    }

    public void BY8(C33211nD r6) {
        if (this instanceof AnonymousClass1ON) {
            AnonymousClass1ON r4 = (AnonymousClass1ON) this;
            if (r4.A02.B4F(AnonymousClass1ON.A0N, BuildConfig.FLAVOR).equals(r6.A02)) {
                FbSharedPreferences fbSharedPreferences = r4.A02;
                AnonymousClass1Y7 r3 = AnonymousClass1ON.A08;
                if (!fbSharedPreferences.BBh(r3)) {
                    C30281hn edit = r4.A02.edit();
                    edit.BzA(r3, r4.A01.now());
                    edit.commit();
                }
            }
        }
    }

    public void Baj(C33211nD r1) {
    }

    public void BfG(C33211nD r1) {
    }

    public void Bkx(C33211nD r1) {
    }

    public void BvC(C33211nD r1) {
    }

    public void BvD(C33211nD r1) {
    }

    public void BvE(C33211nD r1) {
    }
}
