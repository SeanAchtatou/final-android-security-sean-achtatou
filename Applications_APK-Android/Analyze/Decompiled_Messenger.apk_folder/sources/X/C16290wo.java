package X;

import androidx.fragment.app.Fragment;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/* renamed from: X.0wo  reason: invalid class name and case insensitive filesystem */
public abstract class C16290wo {
    public int A00;
    public int A01;
    public CharSequence A02;
    public CharSequence A03;
    public String A04;
    public boolean A05;
    public boolean A06 = true;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public ArrayList A0C = new ArrayList();
    public ArrayList A0D;
    public ArrayList A0E;
    public boolean A0F = false;

    public int A02() {
        return C16280wn.A00((C16280wn) this, false);
    }

    public int A03() {
        return C16280wn.A00((C16280wn) this, true);
    }

    public void A04() {
        C16280wn r2 = (C16280wn) this;
        r2.A06();
        r2.A02.A0x(r2, false);
    }

    public void A05() {
        C16280wn r2 = (C16280wn) this;
        r2.A06();
        r2.A02.A0x(r2, true);
    }

    public void A07(int i, int i2) {
        this.A07 = i;
        this.A08 = i2;
        this.A09 = 0;
        this.A0A = 0;
    }

    public void A08(int i, Fragment fragment) {
        A0L(i, fragment, null, 1);
    }

    public void A09(int i, Fragment fragment) {
        A0B(i, fragment, null);
    }

    public void A0A(int i, Fragment fragment, String str) {
        A0L(i, fragment, str, 1);
    }

    public void A0C(Fragment fragment, String str) {
        A0L(0, fragment, str, 1);
    }

    public void A06() {
        if (!this.A05) {
            this.A06 = false;
            return;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }

    public void A0B(int i, Fragment fragment, String str) {
        if (i != 0) {
            A0L(i, fragment, str, 2);
            return;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    public void A0D(C29471gT r2) {
        this.A0C.add(r2);
        r2.A01 = this.A07;
        r2.A02 = this.A08;
        r2.A03 = this.A09;
        r2.A04 = this.A0A;
    }

    public void A0E(String str) {
        if (this.A06) {
            this.A05 = true;
            this.A04 = str;
            return;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }

    public boolean A0F() {
        ArrayList arrayList;
        if (!(this instanceof C16280wn)) {
            arrayList = this.A0C;
        } else {
            arrayList = ((C16280wn) this).A0C;
        }
        return arrayList.isEmpty();
    }

    public C16290wo A0G(Fragment fragment) {
        A0D(new C29471gT(6, fragment));
        return this;
    }

    public C16290wo A0H(Fragment fragment) {
        A0D(new C29471gT(4, fragment));
        return this;
    }

    public C16290wo A0I(Fragment fragment) {
        A0D(new C29471gT(3, fragment));
        return this;
    }

    public C16290wo A0J(Fragment fragment) {
        A0D(new C29471gT(5, fragment));
        return this;
    }

    public C16290wo A0K(Fragment fragment, AnonymousClass1XL r4) {
        A0D(new C29471gT(10, fragment, r4));
        return this;
    }

    public C16290wo(C27801dm r2, ClassLoader classLoader) {
    }

    public void A0L(int i, Fragment fragment, String str, int i2) {
        Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException(AnonymousClass08S.A0P("Fragment ", cls.getCanonicalName(), " must be a public static class to be  properly recreated from instance state."));
        }
        if (str != null) {
            String str2 = fragment.A0T;
            if (str2 == null || str.equals(str2)) {
                fragment.A0T = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.A0T + " now " + str);
            }
        }
        if (i != 0) {
            if (i != -1) {
                int i3 = fragment.A0E;
                if (i3 == 0 || i3 == i) {
                    fragment.A0E = i;
                    fragment.A0D = i;
                } else {
                    throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.A0E + " now " + i);
                }
            } else {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            }
        }
        A0D(new C29471gT(i2, fragment));
    }
}
