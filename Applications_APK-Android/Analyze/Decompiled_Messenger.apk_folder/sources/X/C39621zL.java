package X;

import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;

/* renamed from: X.1zL  reason: invalid class name and case insensitive filesystem */
public final class C39621zL {
    public static Feature A00(Feature feature, LocationComponentOptions locationComponentOptions) {
        if (feature != null) {
            return feature;
        }
        Feature fromGeometry = Feature.fromGeometry(Point.fromLngLat(0.0d, 0.0d));
        Float valueOf = Float.valueOf(0.0f);
        fromGeometry.addNumberProperty(C22298Ase.$const$string(264), valueOf);
        fromGeometry.addNumberProperty(C22298Ase.$const$string(263), valueOf);
        fromGeometry.addBooleanProperty(C22298Ase.$const$string(265), Boolean.valueOf(locationComponentOptions.A0U));
        return fromGeometry;
    }
}
