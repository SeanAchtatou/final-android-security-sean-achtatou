package X;

/* renamed from: X.0O5  reason: invalid class name */
public final class AnonymousClass0O5 {
    public static boolean A00(float f, float f2) {
        boolean isNaN = Float.isNaN(f);
        if (isNaN || Float.isNaN(f2)) {
            if (!isNaN || !Float.isNaN(f2)) {
                return false;
            }
            return true;
        } else if (Math.abs(f2 - f) < 1.0E-5f) {
            return true;
        } else {
            return false;
        }
    }
}
