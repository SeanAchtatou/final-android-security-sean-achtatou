package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eT  reason: invalid class name and case insensitive filesystem */
public final class C07970eT extends C07980eU {
    private static volatile C07970eT A00;

    public static final C07970eT A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C07970eT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C07970eT();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
