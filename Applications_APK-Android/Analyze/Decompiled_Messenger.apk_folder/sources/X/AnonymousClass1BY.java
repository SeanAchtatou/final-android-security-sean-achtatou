package X;

import com.google.common.base.Objects;
import java.util.Set;

/* renamed from: X.1BY  reason: invalid class name */
public final class AnonymousClass1BY {
    public C22261Kq A00;
    public final Set A01 = new AnonymousClass1BZ();

    public static final AnonymousClass1BY A00() {
        return new AnonymousClass1BY();
    }

    public void A01() {
        C22261Kq r0 = this.A00;
        if (r0 != null) {
            r0.A05();
            this.A00 = null;
        }
    }

    public void A02(String str) {
        for (C22261Kq r1 : this.A01) {
            if (Objects.equal(r1.A0B, str)) {
                r1.A06();
                return;
            }
        }
    }
}
