package X;

import com.facebook.profilo.core.TriggerRegistry;
import com.facebook.profilo.ipc.TraceContext;

/* renamed from: X.0Rx  reason: invalid class name and case insensitive filesystem */
public final class C04110Rx implements AnonymousClass04j {
    public static final int A00 = TriggerRegistry.A00.A02("multi_process");

    public boolean AUJ(long j, Object obj, long j2, Object obj2) {
        if (j == j2) {
            if (obj == obj2) {
                return true;
            }
            if (!(obj == null || obj2 == null)) {
                return obj.equals(obj2);
            }
        }
        return false;
    }

    public int AYs(long j, Object obj, AnonymousClass057 r5) {
        return 0;
    }

    public boolean BE8() {
        return false;
    }

    public TraceContext.TraceConfigExtras B6o(long j, Object obj, AnonymousClass057 r5) {
        return TraceContext.TraceConfigExtras.A03;
    }
}
