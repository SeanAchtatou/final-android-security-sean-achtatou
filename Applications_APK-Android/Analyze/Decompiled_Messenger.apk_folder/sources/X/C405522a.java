package X;

import android.content.Context;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem;

/* renamed from: X.22a  reason: invalid class name and case insensitive filesystem */
public final class C405522a {
    public AnonymousClass0UN A00;
    public final C25051Yd A01;
    private final C186514o A02;

    public static final C405522a A00(AnonymousClass1XY r1) {
        return new C405522a(r1);
    }

    public static void A01(C405522a r6, Context context, InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem) {
        new C122495q0(r6.A02, context).A01(inboxUnitMontageActiveNowItem.A0J().A0Q, inboxUnitMontageActiveNowItem.A0J().A08(), new C122645qG(true, false, false, inboxUnitMontageActiveNowItem.A00, inboxUnitMontageActiveNowItem.A04));
    }

    private C405522a(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        C04490Ux.A0L(r3);
        this.A01 = AnonymousClass0WT.A00(r3);
        this.A02 = new C186514o(r3);
    }
}
