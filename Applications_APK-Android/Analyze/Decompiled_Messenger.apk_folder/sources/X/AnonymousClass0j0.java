package X;

/* renamed from: X.0j0  reason: invalid class name */
public final class AnonymousClass0j0 {
    public static Integer A00(String str) {
        if (str.equals("UNSET")) {
            return AnonymousClass07B.A00;
        }
        if (str.equals("PUBLIC")) {
            return AnonymousClass07B.A01;
        }
        if (str.equals("FRIENDS_AND_CONNECTIONS")) {
            return AnonymousClass07B.A0C;
        }
        if (str.equals("FRIENDS")) {
            return AnonymousClass07B.A0N;
        }
        if (str.equals("CUSTOM")) {
            return AnonymousClass07B.A0Y;
        }
        throw new IllegalArgumentException(str);
    }

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "PUBLIC";
            case 2:
                return "FRIENDS_AND_CONNECTIONS";
            case 3:
                return "FRIENDS";
            case 4:
                return "CUSTOM";
            default:
                return "UNSET";
        }
    }
}
