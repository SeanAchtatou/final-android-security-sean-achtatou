package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.ThreadCustomization;

/* renamed from: X.1g6  reason: invalid class name and case insensitive filesystem */
public final class C29241g6 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadCustomization(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadCustomization[i];
    }
}
