package X;

import java.util.concurrent.Future;

/* renamed from: X.0Xz  reason: invalid class name and case insensitive filesystem */
public final class C05190Xz {
    public Throwable A00;
    public boolean A01;

    public synchronized void A00() {
        this.A01 = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r1 == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(java.util.concurrent.Future r3) {
        /*
            r2 = this;
            X.00K r0 = X.AnonymousClass00J.A01
            if (r0 == 0) goto L_0x0009
            boolean r1 = r0.A1h
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x0031
            boolean r0 = r3.isDone()
            if (r0 == 0) goto L_0x0031
            boolean r0 = r3.isCancelled()
            if (r0 != 0) goto L_0x0031
            r3.get()     // Catch:{ ExecutionException -> 0x001c, Exception -> 0x002f }
            goto L_0x0030
        L_0x001c:
            r1 = move-exception
            java.lang.Throwable r0 = r1.getCause()
            if (r0 == 0) goto L_0x002f
            monitor-enter(r2)
            java.lang.Throwable r0 = r1.getCause()     // Catch:{ all -> 0x002c }
            r2.A00 = r0     // Catch:{ all -> 0x002c }
            monitor-exit(r2)     // Catch:{ all -> 0x002c }
            return
        L_0x002c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x002c }
            throw r0
        L_0x002f:
            return
        L_0x0030:
            return
        L_0x0031:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05190Xz.A01(java.util.concurrent.Future):void");
    }

    public C05190Xz(Future future) {
        C05300Yk r2 = AnonymousClass0Y4.A00;
        C05340Yo r1 = new C05340Yo(future, this, r2.A02);
        synchronized (r2) {
            if (!r2.A03.add(r1)) {
                throw new IllegalStateException((String) null);
            } else if (!r2.A00) {
                r2.A00 = true;
                new C05380Ys(r2, "PhantomDestructor").start();
            }
        }
    }
}
