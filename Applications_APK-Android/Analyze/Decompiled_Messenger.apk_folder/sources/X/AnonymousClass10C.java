package X;

import com.google.common.base.Preconditions;

/* renamed from: X.10C  reason: invalid class name */
public final class AnonymousClass10C implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.orca.threadlist.common.ThreadListFragmentUpdateOrchestrator$1";
    public final /* synthetic */ AnonymousClass16N A00;

    public AnonymousClass10C(AnonymousClass16N r1) {
        this.A00 = r1;
    }

    public void run() {
        AnonymousClass16N r1 = this.A00;
        r1.A03 = null;
        Preconditions.checkNotNull(r1.A02);
        AnonymousClass16N r12 = this.A00;
        AnonymousClass16N.A02(r12, r12.A02);
    }
}
