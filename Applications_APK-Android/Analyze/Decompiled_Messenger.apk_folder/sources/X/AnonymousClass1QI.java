package X;

import java.util.Map;

/* renamed from: X.1QI  reason: invalid class name */
public class AnonymousClass1QI implements AnonymousClass1MN {
    private final AnonymousClass1MN A00;
    private final C23421Pl A01;

    public void Bk0(AnonymousClass1QK r3, String str, String str2) {
        C23421Pl r1 = this.A01;
        if (r1 != null) {
            r1.Bk1(r3.A0B, str, str2);
        }
        AnonymousClass1MN r0 = this.A00;
        if (r0 != null) {
            r0.Bk0(r3, str, str2);
        }
    }

    public void Bk2(AnonymousClass1QK r3, String str, Map map) {
        C23421Pl r1 = this.A01;
        if (r1 != null) {
            r1.Bk3(r3.A0B, str, map);
        }
        AnonymousClass1MN r0 = this.A00;
        if (r0 != null) {
            r0.Bk2(r3, str, map);
        }
    }

    public void Bk4(AnonymousClass1QK r3, String str, Throwable th, Map map) {
        C23421Pl r1 = this.A01;
        if (r1 != null) {
            r1.Bk5(r3.A0B, str, th, map);
        }
        AnonymousClass1MN r0 = this.A00;
        if (r0 != null) {
            r0.Bk4(r3, str, th, map);
        }
    }

    public void Bk6(AnonymousClass1QK r3, String str, Map map) {
        C23421Pl r1 = this.A01;
        if (r1 != null) {
            r1.Bk7(r3.A0B, str, map);
        }
        AnonymousClass1MN r0 = this.A00;
        if (r0 != null) {
            r0.Bk6(r3, str, map);
        }
    }

    public void Bk8(AnonymousClass1QK r3, String str) {
        C23421Pl r1 = this.A01;
        if (r1 != null) {
            r1.Bk9(r3.A0B, str);
        }
        AnonymousClass1MN r0 = this.A00;
        if (r0 != null) {
            r0.Bk8(r3, str);
        }
    }

    public void Bt8(AnonymousClass1QK r3, String str, boolean z) {
        C23421Pl r1 = this.A01;
        if (r1 != null) {
            r1.Bt9(r3.A0B, str, z);
        }
        AnonymousClass1MN r0 = this.A00;
        if (r0 != null) {
            r0.Bt8(r3, str, z);
        }
    }

    public boolean C3Q(AnonymousClass1QK r3, String str) {
        boolean z;
        AnonymousClass1MN r0;
        C23421Pl r1 = this.A01;
        if (r1 != null) {
            z = r1.C3R(r3.A0B);
        } else {
            z = false;
        }
        if (z || (r0 = this.A00) == null) {
            return z;
        }
        return r0.C3Q(r3, str);
    }

    public AnonymousClass1QI(C23421Pl r1, AnonymousClass1MN r2) {
        this.A01 = r1;
        this.A00 = r2;
    }
}
