package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.19z  reason: invalid class name and case insensitive filesystem */
public final class C198719z implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.litho.widget.RecyclerBinder$9";
    public final /* synthetic */ AnonymousClass1Ri A00;

    public C198719z(AnonymousClass1Ri r1) {
        this.A00 = r1;
    }

    public void run() {
        RecyclerView recyclerView = this.A00.A08;
        if (recyclerView == null || !recyclerView.A1B()) {
            AnonymousClass1A7 r1 = this.A00.A0a;
            if (r1.A03()) {
                r1.A00(1);
            }
        } else {
            RecyclerView recyclerView2 = this.A00.A08;
            if (recyclerView2.isAttachedToWindow() && recyclerView2.getVisibility() != 8) {
                AnonymousClass1Ri r2 = this.A00;
                int i = r2.A07;
                if (i >= 3) {
                    r2.A07 = 0;
                    AnonymousClass1A7 r12 = r2.A0a;
                    if (r12.A03()) {
                        r12.A00(1);
                        return;
                    }
                    return;
                }
                r2.A07 = i + 1;
                C15320v6.postOnAnimation(r2.A08, r2.A0d);
                return;
            }
        }
        this.A00.A07 = 0;
    }
}
