package X;

import com.google.common.base.Optional;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0lk  reason: invalid class name */
public final class AnonymousClass0lk {
    public final C172437xH A00;
    public final Map A01 = new HashMap();
    public final Map A02 = new HashMap();
    private final Map A03 = new HashMap();

    public static Optional A00(AnonymousClass7WQ r3, int i) {
        AnonymousClass85I r2;
        int i2;
        if (i == 0) {
            r2 = new AnonymousClass85I();
            i2 = 4;
        } else if (i == 1) {
            r2 = new AnonymousClass85I();
            i2 = 8;
        } else if (i == 2) {
            r2 = new AnonymousClass85I();
            i2 = 6;
        } else {
            throw new RuntimeException("Unexpected Gender");
        }
        int A022 = r3.A02(i2);
        if (A022 != 0) {
            int A012 = r3.A01(A022 + r3.A00);
            ByteBuffer byteBuffer = r3.A01;
            r2.A00 = A012;
            r2.A01 = byteBuffer;
        } else {
            r2 = null;
        }
        return Optional.fromNullable(r2);
    }

    public AnonymousClass0lk(C172437xH r8) {
        int i;
        int i2;
        int i3;
        String str;
        int i4;
        int i5;
        this.A00 = r8;
        int i6 = 0;
        int i7 = 0;
        while (true) {
            C172437xH r1 = this.A00;
            int A022 = r1.A02(6);
            if (A022 != 0) {
                i = r1.A04(A022);
            } else {
                i = 0;
            }
            if (i7 >= i) {
                break;
            }
            Map map = this.A02;
            C172457xJ A07 = r1.A07(i7);
            int A023 = A07.A02(4);
            if (A023 != 0) {
                i5 = A07.A01.getInt(A023 + A07.A00);
            } else {
                i5 = 0;
            }
            map.put(Integer.valueOf(i5), Integer.valueOf(i7));
            i7++;
        }
        int i8 = 0;
        while (true) {
            C172437xH r2 = this.A00;
            int A024 = r2.A02(8);
            if (A024 != 0) {
                i2 = r2.A04(A024);
            } else {
                i2 = 0;
            }
            if (i8 >= i2) {
                break;
            }
            Map map2 = this.A01;
            C172447xI A06 = r2.A06(i8);
            int A025 = A06.A02(4);
            if (A025 != 0) {
                i4 = A06.A01.getInt(A025 + A06.A00);
            } else {
                i4 = 0;
            }
            map2.put(Integer.valueOf(i4), Integer.valueOf(i8));
            i8++;
        }
        while (true) {
            C172437xH r3 = this.A00;
            int A026 = r3.A02(12);
            if (A026 != 0) {
                i3 = r3.A04(A026);
            } else {
                i3 = 0;
            }
            if (i6 < i3) {
                Map map3 = this.A03;
                int A027 = r3.A02(12);
                if (A027 != 0) {
                    str = r3.A05(r3.A03(A027) + (i6 << 2));
                } else {
                    str = null;
                }
                map3.put(str, Integer.valueOf(i6));
                i6++;
            } else {
                return;
            }
        }
    }
}
