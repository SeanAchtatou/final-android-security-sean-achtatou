package X;

/* renamed from: X.1vo  reason: invalid class name and case insensitive filesystem */
public final class C37521vo extends C37381va implements C22881Ne {
    public boolean A00;

    public void trim(C133746Nn r3) {
        if (this.A00) {
            A09((int) (((float) this.A07.A03) * 0.5f));
        } else {
            super.trim(r3);
        }
    }

    public C37521vo(C14320t5 r1, AnonymousClass1N7 r2, AnonymousClass1N4 r3, boolean z) {
        super(r1, r2, r3, z);
    }
}
