package X;

import android.widget.ImageView;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1I8  reason: invalid class name */
public final class AnonymousClass1I8 extends C17770zR {
    public static final ImageView.ScaleType A07 = ImageView.ScaleType.FIT_CENTER;
    public static final AnonymousClass1I9 A08 = AnonymousClass1I9.A02;
    @Comparable(type = 3)
    public int A00;
    @Comparable(type = 3)
    public int A01;
    @Comparable(type = 13)
    public ImageView.ScaleType A02 = A07;
    @Comparable(type = 13)
    public AnonymousClass1I9 A03 = A08;
    @Comparable(type = 13)
    public CharSequence A04;
    @Comparable(type = 13)
    public Integer A05;
    @Comparable(type = 3)
    public boolean A06;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1I8() {
        super("BorderlessClickableImage");
    }
}
