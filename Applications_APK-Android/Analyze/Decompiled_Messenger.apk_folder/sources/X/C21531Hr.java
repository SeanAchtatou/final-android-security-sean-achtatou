package X;

import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1Hr  reason: invalid class name and case insensitive filesystem */
public final class C21531Hr {
    public final InboxUnitItem A00;
    public final MigColorScheme A01;

    public C21531Hr(InboxUnitItem inboxUnitItem, MigColorScheme migColorScheme) {
        this.A01 = migColorScheme;
        this.A00 = inboxUnitItem;
    }
}
