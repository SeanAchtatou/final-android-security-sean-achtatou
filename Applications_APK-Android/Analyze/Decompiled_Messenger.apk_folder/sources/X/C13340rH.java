package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0rH  reason: invalid class name and case insensitive filesystem */
public class C13340rH extends C28141eK {
    public final AnonymousClass0TW A00;
    public final String A01;
    public final boolean A02;

    public static Intent A00(C13340rH r8, Intent intent, Context context, String str, List list) {
        boolean z;
        if (r8.A02) {
            C13360rJ.A02(intent, context, str, r8.A00);
        }
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ComponentInfo componentInfo = (ComponentInfo) it.next();
            if (A01(r8, context, componentInfo.applicationInfo)) {
                arrayList.add(componentInfo);
            }
        }
        if (arrayList.isEmpty()) {
            r8.A00.C2T(r8.A01, "No matching packages available.", null);
            return null;
        } else if (r8.A02 && arrayList.size() > 1) {
            return C28141eK.A03(C28141eK.A08(arrayList, intent));
        } else {
            ComponentInfo componentInfo2 = (ComponentInfo) arrayList.get(0);
            if (arrayList.size() > 1) {
                Iterator it2 = arrayList.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    ComponentInfo componentInfo3 = (ComponentInfo) it2.next();
                    try {
                        String str2 = componentInfo3.packageName;
                        ApplicationInfo applicationInfo = C28061eC.A00(context, context.getPackageName()).applicationInfo;
                        if (applicationInfo != null) {
                            ApplicationInfo applicationInfo2 = C28061eC.A00(context, str2).applicationInfo;
                            if (applicationInfo2 != null) {
                                z = !C28061eC.A04(context, applicationInfo.uid, applicationInfo2.uid);
                                continue;
                                if (z) {
                                    componentInfo2 = componentInfo3;
                                    break;
                                }
                            } else {
                                throw new C42922Cj(str2);
                            }
                        } else {
                            throw new C42922Cj(context.getPackageName());
                        }
                    } catch (SecurityException e) {
                        r8.A00.C2T(r8.A01, AnonymousClass08S.A0J("Error verifying the signature for ", componentInfo3.packageName), e);
                        z = false;
                        continue;
                    }
                }
            }
            intent.setComponent(new ComponentName(componentInfo2.packageName, componentInfo2.name));
            return intent;
        }
    }

    public static boolean A01(C13340rH r6, Context context, ApplicationInfo applicationInfo) {
        String str = applicationInfo.packageName;
        try {
            if (AnonymousClass0TW.A04(r6.A00, AnonymousClass0TW.A00(applicationInfo.uid, context), context)) {
                return true;
            }
            if (!r6.A0H()) {
                return false;
            }
            r6.A00.C2T(r6.A01, AnonymousClass08S.A0J(str, " is not an app defined the targeted app whitelist but fail-open."), null);
            return true;
        } catch (SecurityException e) {
            r6.A00.C2T(r6.A01, AnonymousClass08S.A0J("Unexpected exception in checking trusted app for ", str), e);
            boolean z = false;
            if (C28141eK.A04(r6) == AnonymousClass07B.A0N) {
                z = true;
            }
            return !z;
        }
    }

    public C13340rH(C008807t r1, AnonymousClass04e r2, AnonymousClass0TW r3, String str, boolean z, boolean z2) {
        super(r1, r2, z2);
        this.A01 = str;
        this.A00 = r3;
        this.A02 = z;
    }
}
