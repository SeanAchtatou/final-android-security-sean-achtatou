package X;

import com.facebook.acra.util.StatFsUtil;
import io.card.payment.BuildConfig;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0d8  reason: invalid class name and case insensitive filesystem */
public final class C07260d8 implements C25721aI {
    private static final long A01;
    private static final long A02;
    private static final long A03;
    private static volatile C07260d8 A04;
    private AnonymousClass0UN A00;

    static {
        TimeUnit timeUnit = TimeUnit.MINUTES;
        A03 = timeUnit.toMillis(15);
        A02 = timeUnit.toMillis(45);
        A01 = timeUnit.toMillis(30);
    }

    public static final C07260d8 A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C07260d8.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C07260d8(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public int Aco() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).AqL(566845488826120L, 0);
    }

    public int AfE() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).AqL(566845489678095L, 0);
    }

    public boolean AlE() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(285370512053857L);
    }

    public boolean AlH() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aeo(281522222137376L, false);
    }

    public String Alo() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).B4E(844472174772225L, BuildConfig.FLAVOR);
    }

    public long Alt() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198127119L, StatFsUtil.IN_MEGA_BYTE);
    }

    public long Aok() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198192656L, 0);
    }

    public long Aol() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198258193L, 0);
    }

    public long Aom() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198323730L, 0);
    }

    public long Aon() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198389267L, 0);
    }

    public long Aoo() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198520341L, 0);
    }

    public long Aop() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198585878L, 0);
    }

    public long Aoq() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198651415L, 0);
    }

    public long Aor() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198716952L, 0);
    }

    public int Aos() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).AqL(562997198454804L, 50);
    }

    public int Aot() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).AqL(562997198782489L, 1);
    }

    public long B0P() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198913562L, A01);
    }

    public long B0Q() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997198979099L, A02);
    }

    public long B0R() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997199044636L, 0);
    }

    public long B0S() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997199110173L, A03);
    }

    public long B0T() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997199241247L, A01);
    }

    public long B0U() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997199306784L, A02);
    }

    public long B0V() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997199372321L, 0);
    }

    public long B0W() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At1(562997199437858L, A03);
    }

    public int B0X() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).AqL(562997199175710L, 50);
    }

    public int B0Y() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).AqL(562997199503395L, 50);
    }

    public boolean B31() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aeo(2306124531436617762L, true);
    }

    private C07260d8(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
