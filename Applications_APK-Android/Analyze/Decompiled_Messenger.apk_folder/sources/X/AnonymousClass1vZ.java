package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.graphics.Rect;
import android.os.Build;
import com.facebook.common.dextricks.DexStore;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* renamed from: X.1vZ  reason: invalid class name */
public abstract class AnonymousClass1vZ implements C30591iK {
    private static final Class A03 = AnonymousClass1vZ.class;
    private static final byte[] A04 = {-1, -39};
    public final AnonymousClass0ZG A00;
    private final C37391vb A01;
    private final C37411vd A02;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r6.inPreferredConfig == android.graphics.Bitmap.Config.RGBA_F16) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A02(int r4, int r5, android.graphics.BitmapFactory.Options r6) {
        /*
            r3 = this;
            android.graphics.ColorSpace r0 = r6.outColorSpace
            if (r0 == 0) goto L_0x0011
            boolean r0 = r0.isWideGamut()
            if (r0 == 0) goto L_0x0011
            android.graphics.Bitmap$Config r2 = r6.inPreferredConfig
            android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.RGBA_F16
            r0 = 1
            if (r2 != r1) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x0018
            int r4 = r4 * r5
            int r4 = r4 << 3
            return r4
        L_0x0018:
            android.graphics.Bitmap$Config r0 = r6.inPreferredConfig
            int r4 = r4 * r5
            int r0 = X.AnonymousClass1SJ.A00(r0)
            int r4 = r4 * r0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1vZ.A02(int, int, android.graphics.BitmapFactory$Options):int");
    }

    public AnonymousClass1PS decodeJPEGFromEncodedImage(AnonymousClass1NY r7, Bitmap.Config config, Rect rect, int i) {
        return decodeJPEGFromEncodedImageWithColorSpace(r7, config, rect, i, null);
    }

    public AnonymousClass1PS decodeJPEGFromEncodedImageWithColorSpace(AnonymousClass1NY r11, Bitmap.Config config, Rect rect, int i, ColorSpace colorSpace) {
        InputStream inputStream;
        AnonymousClass1NY r5 = r11;
        int i2 = i;
        boolean A0D = r11.A0D(i);
        BitmapFactory.Options A002 = A00(r11, config);
        InputStream A09 = r11.A09();
        C05520Zg.A02(A09);
        if (r11.A08() > i) {
            A09 = new C1934894t(A09, i);
        }
        if (!A0D) {
            inputStream = new C1934794s(A09, A04);
        } else {
            inputStream = A09;
        }
        boolean z = false;
        if (A002.inPreferredConfig != Bitmap.Config.ARGB_8888) {
            z = true;
        }
        Rect rect2 = rect;
        ColorSpace colorSpace2 = colorSpace;
        try {
            return A01(inputStream, A002, rect, colorSpace);
        } catch (RuntimeException e) {
            if (z) {
                return decodeJPEGFromEncodedImageWithColorSpace(r5, Bitmap.Config.ARGB_8888, rect2, i2, colorSpace2);
            }
            throw e;
        }
    }

    private static BitmapFactory.Options A00(AnonymousClass1NY r4, Bitmap.Config config) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = r4.A03;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(r4.A09(), null, options);
        if (options.outWidth == -1 || options.outHeight == -1) {
            throw new IllegalArgumentException();
        }
        options.inJustDecodeBounds = false;
        options.inDither = true;
        options.inPreferredConfig = config;
        options.inMutable = true;
        return options;
    }

    public AnonymousClass1vZ(C37391vb r4, int i, AnonymousClass0ZG r6) {
        C37411vd r0;
        if (Build.VERSION.SDK_INT >= 26) {
            r0 = new C37411vd();
        } else {
            r0 = null;
        }
        this.A02 = r0;
        this.A01 = r4;
        this.A00 = r6;
        for (int i2 = 0; i2 < i; i2++) {
            this.A00.C0w(ByteBuffer.allocate(DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.BitmapRegionDecoder.newInstance(java.io.InputStream, boolean):android.graphics.BitmapRegionDecoder throws java.io.IOException}
     arg types: [java.io.InputStream, int]
     candidates:
      ClspMth{android.graphics.BitmapRegionDecoder.newInstance(java.io.FileDescriptor, boolean):android.graphics.BitmapRegionDecoder throws java.io.IOException}
      ClspMth{android.graphics.BitmapRegionDecoder.newInstance(java.lang.String, boolean):android.graphics.BitmapRegionDecoder throws java.io.IOException}
      ClspMth{android.graphics.BitmapRegionDecoder.newInstance(java.io.InputStream, boolean):android.graphics.BitmapRegionDecoder throws java.io.IOException} */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        X.AnonymousClass02w.A06(X.AnonymousClass1vZ.A03, "Could not decode region %s, decoding full bitmap instead.", r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a4, code lost:
        if (r5 != null) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r5.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00aa, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        r5.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        if (X.C37411vd.A00(r10.inPreferredConfig) == false) goto L_0x002c;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x0099 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:81:0x00ff */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ad A[SYNTHETIC, Splitter:B:51:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b4 A[Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6, all -> 0x0100, IOException -> 0x00ff }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.AnonymousClass1PS A01(java.io.InputStream r9, android.graphics.BitmapFactory.Options r10, android.graphics.Rect r11, android.graphics.ColorSpace r12) {
        /*
            r8 = this;
            X.C05520Zg.A02(r9)
            int r7 = r10.outWidth
            int r5 = r10.outHeight
            if (r11 == 0) goto L_0x0017
            int r7 = r11.width()
            int r0 = r10.inSampleSize
            int r7 = r7 / r0
            int r5 = r11.height()
            int r0 = r10.inSampleSize
            int r5 = r5 / r0
        L_0x0017:
            int r0 = android.os.Build.VERSION.SDK_INT
            r3 = 26
            r2 = 1
            r4 = 0
            if (r0 < r3) goto L_0x002c
            X.1vd r0 = r8.A02
            if (r0 == 0) goto L_0x002c
            android.graphics.Bitmap$Config r0 = r10.inPreferredConfig
            boolean r1 = X.C37411vd.A00(r0)
            r0 = 1
            if (r1 != 0) goto L_0x002d
        L_0x002c:
            r0 = 0
        L_0x002d:
            r6 = 0
            if (r11 != 0) goto L_0x0056
            if (r0 == 0) goto L_0x0056
            r10.inMutable = r4
            r4 = r6
        L_0x0035:
            r10.inBitmap = r4
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r3) goto L_0x0045
            if (r12 != 0) goto L_0x0043
            android.graphics.ColorSpace$Named r0 = android.graphics.ColorSpace.Named.SRGB
            android.graphics.ColorSpace r12 = android.graphics.ColorSpace.get(r0)
        L_0x0043:
            r10.inPreferredColorSpace = r12
        L_0x0045:
            X.0ZG r0 = r8.A00
            java.lang.Object r3 = r0.ALa()
            java.nio.ByteBuffer r3 = (java.nio.ByteBuffer) r3
            if (r3 != 0) goto L_0x0074
            r0 = 16384(0x4000, float:2.2959E-41)
            java.nio.ByteBuffer r3 = java.nio.ByteBuffer.allocate(r0)
            goto L_0x0074
        L_0x0056:
            if (r11 == 0) goto L_0x005e
            if (r0 == 0) goto L_0x005e
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888
            r10.inPreferredConfig = r0
        L_0x005e:
            int r1 = r8.A02(r7, r5, r10)
            X.1vb r0 = r8.A01
            java.lang.Object r4 = r0.get(r1)
            android.graphics.Bitmap r4 = (android.graphics.Bitmap) r4
            if (r4 != 0) goto L_0x0035
            java.lang.NullPointerException r1 = new java.lang.NullPointerException
            java.lang.String r0 = "BitmapPool.get returned null"
            r1.<init>(r0)
            throw r1
        L_0x0074:
            byte[] r0 = r3.array()     // Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6 }
            r10.inTempStorage = r0     // Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6 }
            r0 = 19
            if (r1 < r0) goto L_0x00b1
            if (r11 == 0) goto L_0x00b1
            if (r4 == 0) goto L_0x00b1
            android.graphics.Bitmap$Config r0 = r10.inPreferredConfig     // Catch:{ IOException -> 0x0098, all -> 0x0095 }
            r4.reconfigure(r7, r5, r0)     // Catch:{ IOException -> 0x0098, all -> 0x0095 }
            android.graphics.BitmapRegionDecoder r5 = android.graphics.BitmapRegionDecoder.newInstance(r9, r2)     // Catch:{ IOException -> 0x0098, all -> 0x0095 }
            android.graphics.Bitmap r1 = r5.decodeRegion(r11, r10)     // Catch:{ IOException -> 0x0099 }
            r5.recycle()     // Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6 }
            goto L_0x00b2
        L_0x0095:
            r0 = move-exception
            r5 = r6
            goto L_0x00ab
        L_0x0098:
            r5 = r6
        L_0x0099:
            java.lang.Class r2 = X.AnonymousClass1vZ.A03     // Catch:{ all -> 0x00aa }
            java.lang.String r1 = "Could not decode region %s, decoding full bitmap instead."
            java.lang.Object[] r0 = new java.lang.Object[]{r11}     // Catch:{ all -> 0x00aa }
            X.AnonymousClass02w.A06(r2, r1, r0)     // Catch:{ all -> 0x00aa }
            if (r5 == 0) goto L_0x00b1
            r5.recycle()     // Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6 }
            goto L_0x00b1
        L_0x00aa:
            r0 = move-exception
        L_0x00ab:
            if (r5 == 0) goto L_0x00b0
            r5.recycle()     // Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6 }
        L_0x00b0:
            throw r0     // Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6 }
        L_0x00b1:
            r1 = r6
        L_0x00b2:
            if (r1 != 0) goto L_0x00b8
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r9, r6, r10)     // Catch:{ IllegalArgumentException -> 0x00df, RuntimeException -> 0x00d6 }
        L_0x00b8:
            X.0ZG r0 = r8.A00
            r0.C0w(r3)
            if (r4 == 0) goto L_0x00cf
            if (r4 == r1) goto L_0x00cf
            X.1vb r0 = r8.A01
            r0.C0t(r4)
            r1.recycle()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x00cf:
            X.1vb r0 = r8.A01
            X.1PS r0 = X.AnonymousClass1PS.A02(r1, r0)
            return r0
        L_0x00d6:
            r1 = move-exception
            if (r4 == 0) goto L_0x00de
            X.1vb r0 = r8.A01     // Catch:{ all -> 0x0100 }
            r0.C0t(r4)     // Catch:{ all -> 0x0100 }
        L_0x00de:
            throw r1     // Catch:{ all -> 0x0100 }
        L_0x00df:
            r2 = move-exception
            if (r4 == 0) goto L_0x00e7
            X.1vb r0 = r8.A01     // Catch:{ all -> 0x0100 }
            r0.C0t(r4)     // Catch:{ all -> 0x0100 }
        L_0x00e7:
            r9.reset()     // Catch:{ IOException -> 0x00ff }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r9)     // Catch:{ IOException -> 0x00ff }
            if (r1 == 0) goto L_0x00fe
            X.9Mk r0 = X.C196549Mk.A00()     // Catch:{ IOException -> 0x00ff }
            X.1PS r1 = X.AnonymousClass1PS.A02(r1, r0)     // Catch:{ IOException -> 0x00ff }
            X.0ZG r0 = r8.A00
            r0.C0w(r3)
            return r1
        L_0x00fe:
            throw r2     // Catch:{ IOException -> 0x00ff }
        L_0x00ff:
            throw r2     // Catch:{ all -> 0x0100 }
        L_0x0100:
            r1 = move-exception
            X.0ZG r0 = r8.A00
            r0.C0w(r3)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1vZ.A01(java.io.InputStream, android.graphics.BitmapFactory$Options, android.graphics.Rect, android.graphics.ColorSpace):X.1PS");
    }

    public AnonymousClass1PS decodeFromEncodedImageWithColorSpace(AnonymousClass1NY r5, Bitmap.Config config, Rect rect, ColorSpace colorSpace) {
        BitmapFactory.Options A002 = A00(r5, config);
        boolean z = false;
        if (A002.inPreferredConfig != Bitmap.Config.ARGB_8888) {
            z = true;
        }
        try {
            return A01(r5.A09(), A002, rect, colorSpace);
        } catch (RuntimeException e) {
            if (z) {
                return decodeFromEncodedImageWithColorSpace(r5, Bitmap.Config.ARGB_8888, rect, colorSpace);
            }
            throw e;
        }
    }
}
