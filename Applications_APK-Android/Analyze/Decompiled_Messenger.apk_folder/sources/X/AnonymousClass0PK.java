package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import io.card.payment.BuildConfig;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0PK  reason: invalid class name */
public final class AnonymousClass0PK implements AnonymousClass0C7 {
    public C03990Ri A00;
    public AnonymousClass0Rh A01;
    public String A02;
    public InetAddress A03;
    public InetAddress A04;
    public Socket A05;
    public final AnonymousClass0B6 A06;
    public final AnonymousClass0BD A07;
    public final AnonymousClass0BE A08;
    public final AnonymousClass0C6 A09;
    public final AnonymousClass0AY A0A;
    public final ScheduledExecutorService A0B;
    public volatile AnonymousClass0CA A0C;
    public volatile AnonymousClass0CB A0D;
    public volatile boolean A0E = false;

    public synchronized void AU1(String str, int i, boolean z, AnonymousClass0CK r14, int i2, boolean z2) {
        Thread thread = new Thread(new AnonymousClass0S8(this, str, i, z, r14, i2, z2), "MqttClient-Network-Thread");
        thread.setPriority(this.A09.A0A);
        thread.start();
    }

    public byte AhL() {
        return 3;
    }

    public synchronized String B0g() {
        SocketAddress remoteSocketAddress;
        Socket socket = this.A05;
        if (socket == null || (remoteSocketAddress = socket.getRemoteSocketAddress()) == null) {
            return "N/A";
        }
        String str = this.A02;
        if (str != null) {
            return AnonymousClass08S.A0P(str, "|", remoteSocketAddress.toString());
        }
        return remoteSocketAddress.toString();
    }

    public synchronized void C5M() {
        A00(this, this.A01, new C02020Cn(new C01990Ck(AnonymousClass0CL.A05), null, null));
    }

    public synchronized void C5N() {
        A00(this, this.A01, new C02020Cn(new C01990Ck(AnonymousClass0CL.A06), null, null));
    }

    public void C5P(int i, Object obj) {
        try {
            synchronized (this) {
                AnonymousClass0CB r0 = this.A0D;
                if (!r0.A00.A0Z.equals(AnonymousClass089.CONNECTED)) {
                    this.A0D.A08(false, "not_connected", obj);
                    return;
                }
                A00(this, this.A01, new C02010Cm(new C01990Ck(AnonymousClass0CL.A07), new C02000Cl(i)));
                this.A0D.A08(true, null, obj);
            }
        } catch (Throwable th) {
            this.A0D.A01(AnonymousClass0CE.A01(th), AnonymousClass0CG.A05, th);
            this.A0D.A08(false, AnonymousClass08S.A0J("puback_exception:", th.getMessage()), obj);
        }
    }

    public synchronized void C5Q(String str, byte[] bArr, int i, int i2) {
        Integer A002 = AnonymousClass0AI.A00(str);
        if (A002 != null) {
            str = A002.toString();
        } else {
            this.A0D.A07(null, "mqtt_enum_topic", String.format(null, "Failed to encode topic %s", str));
        }
        A00(this, this.A01, new C02040Cp(new C01990Ck(AnonymousClass0CL.A08, i), new AnonymousClass0G8(str, i2), bArr));
    }

    public synchronized void C5T(List list, int i) {
        A00(this, this.A01, new AnonymousClass0P2(new C01990Ck(AnonymousClass0CL.A0A), new C02000Cl(i), new C04050Rq(list)));
    }

    public synchronized void C5U(List list, int i) {
        A00(this, this.A01, new C03630Ou(new C01990Ck(AnonymousClass0CL.A0C), new C02000Cl(i), new AnonymousClass0Rp(list)));
    }

    public void CHp() {
        boolean z;
        synchronized (this) {
            z = true;
            if (!this.A0E) {
                this.A0E = true;
            } else {
                z = false;
            }
        }
        if (z) {
            this.A0D.A01(AnonymousClass0CE.A01, AnonymousClass0CG.A02, null);
        }
    }

    public static byte[] A01(String str) {
        try {
            return str.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ASP() {
        Socket socket = this.A05;
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException unused) {
            }
        }
        synchronized (this) {
            this.A05 = null;
            this.A02 = null;
            this.A00 = null;
            this.A01 = null;
            this.A0D.A03(AnonymousClass089.DISCONNECTED);
        }
        this.A0D.A00();
    }

    public String AiA() {
        String sb;
        StringBuilder sb2 = new StringBuilder();
        InetAddress inetAddress = this.A04;
        if (inetAddress != null) {
            sb2.append("Remote:");
            sb2.append(inetAddress.toString());
            sb2.append(10);
        }
        InetAddress inetAddress2 = this.A03;
        if (inetAddress2 != null) {
            sb2.append("Local:");
            sb2.append(inetAddress2.toString());
            sb2.append(10);
        }
        AnonymousClass0BE r3 = this.A08;
        synchronized (r3) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Cache{");
            Iterator it = r3.A00.A01().iterator();
            while (it.hasNext()) {
                sb3.append(((AnonymousClass0Ry) it.next()).toString());
                sb3.append(',');
            }
            sb3.append("}\n");
            sb = sb3.toString();
        }
        sb2.append(sb);
        return sb2.toString();
    }

    public void C6f(AnonymousClass0CB r1, AnonymousClass0CA r2) {
        this.A0D = r1;
        this.A0C = r2;
    }

    public AnonymousClass0PK(AnonymousClass0BD r2, AnonymousClass0B6 r3, AnonymousClass0C6 r4, ScheduledExecutorService scheduledExecutorService, AnonymousClass0BE r6, AnonymousClass0AY r7) {
        this.A07 = r2;
        this.A06 = r3;
        this.A09 = r4;
        this.A0B = scheduledExecutorService;
        this.A08 = r6;
        this.A0A = r7;
    }

    public String AWi(C02040Cp r6) {
        String str = r6.A02().A01;
        String A012 = AnonymousClass0AI.A01(str);
        if (A012 != null) {
            return A012;
        }
        this.A0D.A07(null, "mqtt_enum_topic", String.format(null, "Failed to decode topic %s", str));
        return str;
    }

    public static void A00(AnonymousClass0PK r12, AnonymousClass0Rh r13, C02020Cn r14) {
        C01540Aq r0;
        String str;
        AssertionError assertionError;
        int i;
        int i2;
        if (r13 != null) {
            if (r14 == null || !(r14 instanceof C02040Cp)) {
                r0 = C01660Bc.A00;
            } else {
                r0 = C01540Aq.A00(((C02040Cp) r14).A02().A01);
            }
            if (r0.A02()) {
                str = AnonymousClass0AI.A01((String) r0.A01());
                if (str == null) {
                    str = (String) r0.A01();
                }
            } else {
                str = BuildConfig.FLAVOR;
            }
            try {
                synchronized (r13) {
                    C01990Ck r2 = r14.A00;
                    AnonymousClass0CL r6 = r2.A03;
                    int i3 = 0;
                    switch (r6.ordinal()) {
                        case 0:
                            if (r14 instanceof AnonymousClass0P6) {
                                AnonymousClass0P6 r22 = (AnonymousClass0P6) r14;
                                r22.A03();
                                i3 = r13.A03.BAV(r13.A00, r22);
                                break;
                            } else {
                                assertionError = new AssertionError("Unexpected type: " + r14);
                                throw assertionError;
                            }
                        case 1:
                            if (r14 instanceof AnonymousClass0P9) {
                                AnonymousClass0P9 r23 = (AnonymousClass0P9) r14;
                                r23.A03();
                                i3 = 4;
                                boolean z = true;
                                if (r23.A03().A00 != 0) {
                                    if (r23.A02() != null) {
                                        z = false;
                                    }
                                    AnonymousClass0A1.A02(z);
                                    r13.A00.writeByte(C04000Rj.A01(r23.A00));
                                    r13.A00.writeByte(2);
                                    r13.A00.writeByte(0);
                                    r13.A00.writeByte(r23.A03().A00);
                                    r13.A00.flush();
                                    break;
                                } else {
                                    boolean z2 = false;
                                    if (r23.A02() != null) {
                                        z2 = true;
                                    }
                                    AnonymousClass0A1.A02(z2);
                                    byte[] A012 = A01(r23.A02().toString());
                                    r13.A00.writeByte(C04000Rj.A01(r23.A00));
                                    DataOutputStream dataOutputStream = r13.A00;
                                    int length = A012.length;
                                    int A022 = 1 + C04000Rj.A02(dataOutputStream, length + 4);
                                    r13.A00.writeByte(0);
                                    r13.A00.writeByte(r23.A03().A00);
                                    r13.A00.writeShort(length);
                                    r13.A00.write(A012);
                                    i3 = A022 + 4 + length;
                                    r13.A00.flush();
                                    break;
                                }
                            } else {
                                assertionError = new AssertionError("Unexpected type: " + r14);
                                throw assertionError;
                            }
                        case 2:
                            if (r14 instanceof C02040Cp) {
                                C02040Cp r1 = (C02040Cp) r14;
                                r1.A02();
                                r1.A02();
                                C010708t.A01.BFj(2);
                                C01990Ck r10 = r1.A00;
                                AnonymousClass0G8 A023 = r1.A02();
                                byte[] A032 = r1.A03();
                                int i4 = 2;
                                if (r13.A01 != 0) {
                                    byte[] A002 = AnonymousClass0Q0.A00(A032);
                                    if (2 != r13.A01 || A002.length <= A032.length) {
                                        A032 = A002;
                                    } else {
                                        r10.A01 = true;
                                    }
                                }
                                byte[] A013 = A01(A023.A01);
                                int length2 = A013.length;
                                int i5 = length2 + 2;
                                if (r10.A02 <= 0) {
                                    i4 = 0;
                                }
                                int i6 = i5 + i4;
                                int length3 = A032.length;
                                i = i6 + length3;
                                r13.A00.writeByte(C04000Rj.A01(r10));
                                i2 = 1 + C04000Rj.A02(r13.A00, i);
                                r13.A00.writeShort(length2);
                                r13.A00.write(A013, 0, length2);
                                if (r10.A02 > 0) {
                                    r13.A00.writeShort(A023.A00);
                                }
                                r13.A00.write(A032, 0, length3);
                                r13.A00.flush();
                                i3 = i2 + i;
                                break;
                            } else {
                                assertionError = new AssertionError("Unexpected type: " + r14);
                                throw assertionError;
                            }
                        case 3:
                            if (r14 instanceof C02010Cm) {
                                C02010Cm r15 = (C02010Cm) r14;
                                r15.A02();
                                C01990Ck r02 = r15.A00;
                                C02000Cl A024 = r15.A02();
                                r13.A00.writeByte(C04000Rj.A01(r02));
                                int A025 = C04000Rj.A02(r13.A00, 2) + 1;
                                r13.A00.writeShort(A024.A00);
                                i3 = A025 + 2;
                                r13.A00.flush();
                                break;
                            } else {
                                assertionError = new AssertionError("Unexpected type: " + r14);
                                throw assertionError;
                            }
                        case 4:
                        case 5:
                        case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                        default:
                            C010708t.A0V("MessageEncoder", new IllegalArgumentException("Unknown message type: " + r6), "send/unexpected; type=%s", r14.A00.A03);
                            break;
                        case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                            if (r14 instanceof AnonymousClass0P2) {
                                AnonymousClass0P2 r03 = (AnonymousClass0P2) r14;
                                r03.A03();
                                C01990Ck r8 = r03.A00;
                                C02000Cl A026 = r03.A02();
                                C04050Rq A033 = r03.A03();
                                int i7 = 0;
                                for (SubscribeTopic subscribeTopic : A033.A00) {
                                    i7 = i7 + A01(subscribeTopic.A01).length + 2 + 1;
                                }
                                i = 2 + i7;
                                r13.A00.writeByte(C04000Rj.A01(r8));
                                i2 = 1 + C04000Rj.A02(r13.A00, i);
                                r13.A00.writeShort(A026.A00);
                                for (SubscribeTopic subscribeTopic2 : A033.A00) {
                                    byte[] A014 = A01(subscribeTopic2.A01);
                                    DataOutputStream dataOutputStream2 = r13.A00;
                                    int length4 = A014.length;
                                    dataOutputStream2.writeShort(length4);
                                    r13.A00.write(A014, 0, length4);
                                    r13.A00.write(subscribeTopic2.A00);
                                }
                                r13.A00.flush();
                                i3 = i2 + i;
                                break;
                            } else {
                                assertionError = new AssertionError("Unexpected type: " + r14);
                                throw assertionError;
                            }
                        case 8:
                            if (r14 instanceof AnonymousClass0P3) {
                                AnonymousClass0P3 r24 = (AnonymousClass0P3) r14;
                                r24.A02();
                                int size = r24.A03().A00.size() + 2;
                                r13.A00.writeByte(C04000Rj.A01(r24.A00));
                                int A027 = C04000Rj.A02(r13.A00, size) + 1;
                                r13.A00.writeShort(r24.A02().A00);
                                for (Integer intValue : r24.A03().A00) {
                                    r13.A00.writeByte(intValue.intValue());
                                }
                                r13.A00.flush();
                                i3 = A027 + size;
                                break;
                            } else {
                                assertionError = new AssertionError("Unexpected type: " + r14);
                                throw assertionError;
                            }
                        case Process.SIGKILL /*9*/:
                            if (r14 instanceof C03630Ou) {
                                C03630Ou r04 = (C03630Ou) r14;
                                r04.A03();
                                C01990Ck r82 = r04.A00;
                                C02000Cl A028 = r04.A02();
                                AnonymousClass0Rp A034 = r04.A03();
                                int i8 = 0;
                                for (String A015 : A034.A00) {
                                    i8 += A01(A015).length + 2;
                                }
                                i = 2 + i8;
                                r13.A00.writeByte(C04000Rj.A01(r82));
                                i2 = 1 + C04000Rj.A02(r13.A00, i);
                                r13.A00.writeShort(A028.A00);
                                for (String A016 : A034.A00) {
                                    byte[] A017 = A01(A016);
                                    DataOutputStream dataOutputStream3 = r13.A00;
                                    int length5 = A017.length;
                                    dataOutputStream3.writeShort(length5);
                                    r13.A00.write(A017, 0, length5);
                                }
                                r13.A00.flush();
                                i3 = i2 + i;
                                break;
                            } else {
                                assertionError = new AssertionError("Unexpected type: " + r14);
                                throw assertionError;
                            }
                        case AnonymousClass1Y3.A01 /*10*/:
                            if (r14 instanceof AnonymousClass0P1) {
                                AnonymousClass0P1 r3 = (AnonymousClass0P1) r14;
                                r3.A02();
                                r13.A00.writeByte(C04000Rj.A01(r3.A00));
                                int A029 = C04000Rj.A02(r13.A00, 2) + 1;
                                r13.A00.writeShort(r3.A02().A00);
                                r13.A00.flush();
                                i3 = A029 + 2;
                                break;
                            } else {
                                assertionError = new AssertionError("Unexpected type: " + r14);
                                throw assertionError;
                            }
                        case AnonymousClass1Y3.A02 /*11*/:
                            r13.A00.writeByte(C04000Rj.A01(r2));
                            r13.A00.writeByte(0);
                            r13.A00.flush();
                            break;
                        case AnonymousClass1Y3.A03 /*12*/:
                            r13.A00.writeByte(C04000Rj.A01(r2));
                            r13.A00.writeByte(0);
                            r13.A00.flush();
                            break;
                    }
                    String name = r14.A00.A03.name();
                    if (r14 instanceof C02040Cp) {
                        name = "PUBLISH_".concat(((C02040Cp) r14).A02().A01);
                    }
                    r13.A02.A01(i3);
                    r13.A02.A02(name, i3);
                }
                r12.A0D.A06(r14.A00.A03.name(), str);
            } catch (IOException e) {
                r12.A0D.A06(AnonymousClass08S.A0J(r14.A00.A03.name(), "-failed"), str);
                throw e;
            }
        } else {
            throw new IOException("No message encoder");
        }
    }
}
