package X;

import com.facebook.gk.sessionless.GkSessionlessModule;
import java.util.concurrent.Executor;

/* renamed from: X.0Wh  reason: invalid class name and case insensitive filesystem */
public final class C04820Wh implements C04830Wi {
    private final AnonymousClass1Z9 A00;
    private final AnonymousClass0US[] A01;

    public static final C04820Wh A00(AnonymousClass1XY r13) {
        C001500z A05 = AnonymousClass0UU.A05(r13);
        AnonymousClass1YI A002 = AnonymousClass0WA.A00(r13);
        C04840Wj A012 = C04840Wj.A01(r13);
        GkSessionlessModule.A00(r13);
        return new C04820Wh(A05, A002, A012, AnonymousClass0UQ.A00(AnonymousClass1Y3.A8i, r13), AnonymousClass0UQ.A00(AnonymousClass1Y3.BFP, r13), AnonymousClass0VB.A00(AnonymousClass1Y3.Ahv, r13), AnonymousClass0VB.A00(AnonymousClass1Y3.A1c, r13), AnonymousClass0VB.A00(AnonymousClass1Y3.BST, r13), AnonymousClass0UQ.A00(AnonymousClass1Y3.Aw6, r13), AnonymousClass0UQ.A00(AnonymousClass1Y3.B8M, r13), AnonymousClass0VB.A00(AnonymousClass1Y3.BFG, r13), AnonymousClass06L.A00(r13));
    }

    public int BKI() {
        return this.A00.A02.length;
    }

    public AnonymousClass1Z5 BLq() {
        AnonymousClass0US A02 = this.A00.A02();
        if (A02 != null) {
            return (AnonymousClass1Z5) A02.get();
        }
        return null;
    }

    public void C0T(AnonymousClass0VL r2, C04800Wf r3, Executor executor) {
        this.A00.A03(r2, r3, executor);
    }

    private C04820Wh(C001500z r21, AnonymousClass1YI r22, C04840Wj r23, AnonymousClass0US r24, AnonymousClass0US r25, AnonymousClass0US r26, AnonymousClass0US r27, AnonymousClass0US r28, AnonymousClass0US r29, AnonymousClass0US r30, AnonymousClass0US r31, AnonymousClass0US r32) {
        AnonymousClass0US r3 = r24;
        AnonymousClass0US r8 = r29;
        AnonymousClass0US r7 = r28;
        AnonymousClass0US r6 = r27;
        this.A01 = new AnonymousClass0US[]{r3, r25, r26, r6, r7, r8, r30, r31, r32};
        AnonymousClass0US[] r11 = this.A01;
        boolean[] zArr = {false, false, false, false, false, false, false, false, false};
        this.A00 = new AnonymousClass1Z9(r11, new C001500z[]{null, null, null, null, null, null, null, null, null}, r21, new Integer[]{null, null, null, null, null, null, null, null, null}, new boolean[]{true, true, true, true, true, true, true, true, true}, zArr, new C04860Wo[]{null, null, null, null, null, null, null, null, null}, r22, r23);
    }
}
