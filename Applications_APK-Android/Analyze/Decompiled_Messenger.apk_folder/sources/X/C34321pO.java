package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.rti.mqtt.protocol.messages.GqlsTopicExtraInfo;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1pO  reason: invalid class name and case insensitive filesystem */
public final class C34321pO {
    public static final Class A02 = C34321pO.class;
    private static volatile C34321pO A03;
    public final DeprecatedAnalyticsLogger A00;
    public final C21171Fm A01;

    public void A01(String str, String str2, C01670Bd r9, Collection collection) {
        A02(str, str2, r9, collection, null);
    }

    public static final C34321pO A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C34321pO.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C34321pO(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public void A02(String str, String str2, C01670Bd r13, Collection collection, String str3) {
        ObjectNode objectNode;
        String str4;
        if (collection != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                SubscribeTopic subscribeTopic = (SubscribeTopic) it.next();
                if (subscribeTopic.A01.startsWith(TurboLoader.Locator.$const$string(27))) {
                    GqlsTopicExtraInfo gqlsTopicExtraInfo = subscribeTopic.A02;
                    String str5 = str;
                    Boolean bool = false;
                    String str6 = null;
                    if (gqlsTopicExtraInfo != null) {
                        str6 = gqlsTopicExtraInfo.A01;
                        Map map = gqlsTopicExtraInfo.A02;
                        Map map2 = map;
                        if (map == null) {
                            objectNode = null;
                        } else {
                            objectNode = new ObjectNode(JsonNodeFactory.instance);
                            for (String str7 : map2.keySet()) {
                                objectNode.put(str7, (String) map2.get(str7));
                            }
                        }
                        bool = gqlsTopicExtraInfo.A00;
                    } else {
                        objectNode = null;
                    }
                    try {
                        if (bool.booleanValue()) {
                            str5 = str2;
                        }
                        if (str5 != null) {
                            C22361La A04 = this.A00.A04(str5, false);
                            if (A04.A0B()) {
                                A04.A06("mqtt_client_checkpoint", r13.name());
                                if (str6 != null) {
                                    A04.A06(AnonymousClass24B.$const$string(1414), str6);
                                }
                                if (objectNode != null) {
                                    A04.A04("query_params", objectNode);
                                }
                                A04.A06(AnonymousClass80H.$const$string(168), this.A01.A02());
                                if (this.A01.A01) {
                                    str4 = "active";
                                } else {
                                    str4 = "inactive";
                                }
                                A04.A06("app_state", str4);
                                if (str3 != null) {
                                    A04.A06("reason", str3);
                                }
                                A04.A0A();
                                C010708t.A01.BFj(2);
                            }
                        }
                    } catch (Exception e) {
                        C010708t.A0F(A02, e, "GraphQL Subscription mqtt checkpoint logging encountered an error.", new Object[0]);
                    }
                }
            }
        }
    }

    private C34321pO(AnonymousClass1XY r2) {
        this.A00 = C06920cI.A00(r2);
        this.A01 = C21171Fm.A00(r2);
    }
}
