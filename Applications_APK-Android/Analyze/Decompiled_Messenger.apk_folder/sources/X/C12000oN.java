package X;

import com.facebook.messaging.model.share.SentShareAttachment;
import com.facebook.messaging.model.share.brandedcamera.SentBrandedCameraShare;
import com.facebook.payments.p2p.service.model.transactions.SendPaymentMessageParams;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

/* renamed from: X.0oN  reason: invalid class name and case insensitive filesystem */
public final class C12000oN {
    private final AnonymousClass0jD A00;
    private final C12020oP A01;
    private final C12470pQ A02 = new C12470pQ(new C11990oM());

    public static final C12000oN A00(AnonymousClass1XY r1) {
        return new C12000oN(r1);
    }

    public String A02(SentShareAttachment sentShareAttachment) {
        String objectNode;
        ObjectNode objectNode2;
        if (sentShareAttachment == null) {
            return null;
        }
        ObjectNode objectNode3 = new ObjectNode(JsonNodeFactory.instance);
        objectNode3.put("type", sentShareAttachment.A00.DBSerialValue);
        switch (sentShareAttachment.A00.ordinal()) {
            case 0:
                objectNode3.put("attachment", this.A02.A02(sentShareAttachment.A01));
                break;
            case 1:
                SendPaymentMessageParams sendPaymentMessageParams = sentShareAttachment.A03;
                if (sendPaymentMessageParams == null) {
                    objectNode2 = null;
                } else {
                    objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                    objectNode2.put("amount", sendPaymentMessageParams.A00.A01.toString());
                    objectNode2.put("currency", sendPaymentMessageParams.A00.A00);
                    objectNode2.put("senderCredentialId", sendPaymentMessageParams.A0H);
                    objectNode2.put("recipientId", sendPaymentMessageParams.A0F);
                    objectNode2.put("memoText", sendPaymentMessageParams.A0A);
                    objectNode2.put("pin", sendPaymentMessageParams.A0C);
                    objectNode2.put("fromPaymentTrigger", sendPaymentMessageParams.A0L);
                    objectNode2.put("offlineThreadingId", sendPaymentMessageParams.A0B);
                    objectNode2.put("groupThreadId", sendPaymentMessageParams.A09);
                    C23632Biw biw = sendPaymentMessageParams.A02;
                    if (biw != null) {
                        objectNode2.put("paymentMethodType", biw.mValue);
                    }
                }
                objectNode3.put("attachment", objectNode2);
                break;
            case 2:
                C12020oP r6 = this.A01;
                SentBrandedCameraShare sentBrandedCameraShare = sentShareAttachment.A02;
                if (sentBrandedCameraShare == null) {
                    objectNode = null;
                } else {
                    ObjectNode objectNode4 = new ObjectNode(JsonNodeFactory.instance);
                    objectNode4.put("page_id", sentBrandedCameraShare.A05);
                    objectNode4.put("camera_content_id", sentBrandedCameraShare.A03);
                    objectNode4.put("cta_title", sentBrandedCameraShare.A04);
                    objectNode4.put("attachment_title", sentBrandedCameraShare.A02);
                    objectNode4.put("attachment_subtitle", sentBrandedCameraShare.A01);
                    objectNode4.put("media_resource", r6.A02.A02(sentBrandedCameraShare.A00));
                    objectNode = objectNode4.toString();
                }
                objectNode3.put("attachment", objectNode);
                break;
        }
        return objectNode3.toString();
    }

    public C12000oN(AnonymousClass1XY r3) {
        new C12010oO(r3);
        this.A01 = new C12020oP(r3);
        this.A00 = AnonymousClass0jD.A00(r3);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.share.SentShareAttachment A01(java.lang.String r7) {
        /*
            r6 = this;
            boolean r0 = X.C06850cB.A0B(r7)
            r3 = 0
            if (r0 != 0) goto L_0x0028
            X.0jD r0 = r6.A00
            com.fasterxml.jackson.databind.JsonNode r2 = r0.A02(r7)
            java.lang.String r0 = "type"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            X.B5y r1 = X.C22652B5y.A00(r0)
            java.lang.String r0 = "attachment"
            com.fasterxml.jackson.databind.JsonNode r2 = r2.get(r0)
            int r0 = r1.ordinal()
            switch(r0) {
                case 0: goto L_0x00c2;
                case 1: goto L_0x00cd;
                case 2: goto L_0x0029;
                default: goto L_0x0028;
            }
        L_0x0028:
            return r3
        L_0x0029:
            if (r2 == 0) goto L_0x0038
            java.lang.String r1 = r2.textValue()
        L_0x002f:
            X.0oP r4 = r6.A01
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x00ab
            goto L_0x003a
        L_0x0038:
            r1 = r3
            goto L_0x002f
        L_0x003a:
            X.0jE r0 = r4.A01     // Catch:{ IOException -> 0x00ad }
            com.fasterxml.jackson.databind.JsonNode r5 = r0.readTree(r1)     // Catch:{ IOException -> 0x00ad }
            if (r5 == 0) goto L_0x00ab
            X.CcG r2 = new X.CcG     // Catch:{ IOException -> 0x00ad }
            r2.<init>()     // Catch:{ IOException -> 0x00ad }
            java.lang.String r0 = "page_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r0)     // Catch:{ IOException -> 0x00ad }
            if (r0 == 0) goto L_0x00a9
            java.lang.String r0 = r0.textValue()     // Catch:{ IOException -> 0x00ad }
        L_0x0053:
            r2.A05 = r0     // Catch:{ IOException -> 0x00ad }
            java.lang.String r0 = "camera_content_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r0)     // Catch:{ IOException -> 0x00ad }
            if (r0 == 0) goto L_0x00a7
            java.lang.String r0 = r0.textValue()     // Catch:{ IOException -> 0x00ad }
        L_0x0061:
            r2.A03 = r0     // Catch:{ IOException -> 0x00ad }
            java.lang.String r0 = "attachment_title"
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r0)     // Catch:{ IOException -> 0x00ad }
            if (r0 == 0) goto L_0x00a5
            java.lang.String r0 = r0.textValue()     // Catch:{ IOException -> 0x00ad }
        L_0x006f:
            r2.A02 = r0     // Catch:{ IOException -> 0x00ad }
            java.lang.String r0 = "attachment_subtitle"
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r0)     // Catch:{ IOException -> 0x00ad }
            if (r0 == 0) goto L_0x00a3
            java.lang.String r0 = r0.textValue()     // Catch:{ IOException -> 0x00ad }
        L_0x007d:
            r2.A01 = r0     // Catch:{ IOException -> 0x00ad }
            java.lang.String r0 = "cta_title"
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r0)     // Catch:{ IOException -> 0x00ad }
            if (r0 == 0) goto L_0x00a1
            java.lang.String r0 = r0.textValue()     // Catch:{ IOException -> 0x00ad }
        L_0x008b:
            r2.A04 = r0     // Catch:{ IOException -> 0x00ad }
            X.0oF r1 = r4.A02     // Catch:{ IOException -> 0x00ad }
            java.lang.String r0 = "media_resource"
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r0)     // Catch:{ IOException -> 0x00ad }
            com.facebook.ui.media.attachments.model.MediaResource r0 = r1.A01(r0)     // Catch:{ IOException -> 0x00ad }
            r2.A00 = r0     // Catch:{ IOException -> 0x00ad }
            com.facebook.messaging.model.share.brandedcamera.SentBrandedCameraShare r0 = new com.facebook.messaging.model.share.brandedcamera.SentBrandedCameraShare     // Catch:{ IOException -> 0x00ad }
            r0.<init>(r2)     // Catch:{ IOException -> 0x00ad }
            goto L_0x00b7
        L_0x00a1:
            r0 = r3
            goto L_0x008b
        L_0x00a3:
            r0 = r3
            goto L_0x007d
        L_0x00a5:
            r0 = r3
            goto L_0x006f
        L_0x00a7:
            r0 = r3
            goto L_0x0061
        L_0x00a9:
            r0 = r3
            goto L_0x0053
        L_0x00ab:
            r0 = r3
            goto L_0x00b7
        L_0x00ad:
            X.09P r2 = r4.A00
            java.lang.String r1 = "BrandedCameraShareMetadataSerialization"
            java.lang.String r0 = "Failed to deserialize branded camera share metadata"
            r2.CGS(r1, r0)
            r0 = r3
        L_0x00b7:
            if (r0 == 0) goto L_0x0028
            com.facebook.messaging.model.share.SentShareAttachment r3 = new com.facebook.messaging.model.share.SentShareAttachment
            X.B5y r2 = X.C22652B5y.BRANDED_CAMERA
            r1 = 0
            r3.<init>(r2, r1, r1, r0)
            return r3
        L_0x00c2:
            X.0pQ r0 = r6.A02
            com.facebook.messaging.model.share.Share r0 = r0.A01(r2)
            com.facebook.messaging.model.share.SentShareAttachment r3 = com.facebook.messaging.model.share.SentShareAttachment.A00(r0)
            return r3
        L_0x00cd:
            com.facebook.payments.currency.CurrencyAmount r4 = new com.facebook.payments.currency.CurrencyAmount
            java.lang.String r0 = "currency"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r3 = com.facebook.common.util.JSONUtil.A0N(r0)
            java.math.BigDecimal r1 = new java.math.BigDecimal
            java.lang.String r0 = "amount"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r1.<init>(r0)
            r4.<init>(r3, r1)
            X.A3y r3 = new X.A3y
            r3.<init>()
            r3.A00 = r4
            java.lang.String r0 = "senderCredentialId"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r3.A0G = r0
            java.lang.String r0 = "recipientId"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r3.A0E = r0
            java.lang.String r0 = "memoText"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r3.A0A = r0
            java.lang.String r0 = "pin"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r3.A0C = r0
            java.lang.String r0 = "fromPaymentTrigger"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r3.A0I = r0
            java.lang.String r0 = "offlineThreadingId"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r3.A0B = r0
            java.lang.String r0 = "groupThreadId"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r3.A09 = r0
            java.lang.String r1 = "paymentMethodType"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r1)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            if (r0 != 0) goto L_0x016f
            r0 = 0
        L_0x0153:
            r3.A02 = r0
            java.lang.String r0 = "fundingOptionId"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r3.A08 = r0
            com.facebook.payments.p2p.service.model.transactions.SendPaymentMessageParams r2 = new com.facebook.payments.p2p.service.model.transactions.SendPaymentMessageParams
            r2.<init>(r3)
            com.facebook.messaging.model.share.SentShareAttachment r3 = new com.facebook.messaging.model.share.SentShareAttachment
            X.B5y r1 = X.C22652B5y.PAYMENT
            r0 = 0
            r3.<init>(r1, r0, r2, r0)
            return r3
        L_0x016f:
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r1)
            java.lang.String r1 = com.facebook.common.util.JSONUtil.A0N(r0)
            X.Biw[] r0 = X.C23632Biw.values()
            X.2lz r1 = X.C23636Bj3.A00(r0, r1)
            X.Biw r0 = X.C23632Biw.UNKNOWN
            java.lang.Object r0 = com.google.common.base.MoreObjects.firstNonNull(r1, r0)
            X.Biw r0 = (X.C23632Biw) r0
            goto L_0x0153
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12000oN.A01(java.lang.String):com.facebook.messaging.model.share.SentShareAttachment");
    }
}
