package X;

import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.inject.ContextScoped;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@ContextScoped
/* renamed from: X.0mD  reason: invalid class name and case insensitive filesystem */
public final class C11170mD {
    private static C04470Uu A06;
    public static volatile ReadWriteLock A07 = new ReentrantReadWriteLock();
    public AnonymousClass0UN A00;
    public final C05920aY A01;
    public final C27451dD A02;
    public final AnonymousClass0mJ A03;
    public final QuickPerformanceLogger A04;
    private final AnonymousClass0VL A05;

    public static ListenableFuture A02(C11170mD r8, C26931cN r9, int i) {
        C26931cN r5 = r9;
        C10880l0 r3 = r9.A0B;
        if (!r3.A0B()) {
            AnonymousClass1BU r4 = (AnonymousClass1BU) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AUs, r8.A00);
            ((AnonymousClass1U5) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AaV, r4.A00)).A01(r3);
            C005505z.A03(r5.A0B.A06, -748794716);
            try {
                return AnonymousClass1BU.A01(r4, r5, r5.A0B, ((C33421nY) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ApB, r4.A00)).A03(r5, false, i), r5.A02, null);
            } finally {
                C005505z.A00(920373241);
            }
        } else {
            throw new IllegalArgumentException("GraphQLQueryExecutor.start() cannot be used with mutations, use .mutate() instead");
        }
    }

    public C10910l3 A04(C26931cN r3) {
        return new C10910l3(A02(this, r3, 0), r3);
    }

    static {
        new GraphQLResult(null, AnonymousClass102.NO_DATA, 0);
    }

    public static final C11170mD A01(AnonymousClass1XY r5) {
        C11170mD r0;
        synchronized (C11170mD.class) {
            C04470Uu A002 = C04470Uu.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r3 = (AnonymousClass1XY) A06.A01();
                    A06.A00 = new C11170mD(r3, AnonymousClass0ZD.A03(r3));
                }
                C04470Uu r1 = A06;
                r0 = (C11170mD) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    public static ListenableFuture A03(ListenableFuture listenableFuture) {
        return AnonymousClass169.A00(listenableFuture, new AnonymousClass4AO());
    }

    public ListenableFuture A05(C21899AkD akD) {
        return A06(akD, C137796c1.A01);
    }

    public ListenableFuture A06(C21899AkD akD, C137796c1 r3) {
        BMW bmw = new BMW();
        bmw.A02(akD);
        return A07(bmw.A01(), r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a6, code lost:
        if (r0 != false) goto L_0x00a8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.util.concurrent.ListenableFuture A07(X.BMX r16, X.C137796c1 r17) {
        /*
            r15 = this;
            r9 = r16
            X.AkD r10 = r9.A01()     // Catch:{ Exception -> 0x0142 }
            com.google.common.collect.ImmutableList r0 = r10.A09
            boolean r0 = r0.isEmpty()
            r3 = 0
            r6 = 1
            r11 = r17
            if (r0 != 0) goto L_0x001d
            X.6c1 r0 = X.C137796c1.A01
            r1 = 0
            if (r11 != r0) goto L_0x0018
            r1 = 1
        L_0x0018:
            java.lang.String r0 = "File attachments not yet supported with offline retries"
            com.google.common.base.Preconditions.checkState(r1, r0)
        L_0x001d:
            java.util.concurrent.atomic.AtomicInteger r0 = X.C26951cP.A00
            int r14 = r0.incrementAndGet()
            com.facebook.quicklog.QuickPerformanceLogger r0 = r15.A04
            r4 = 3211305(0x310029, float:4.499997E-39)
            r0.markerStart(r4, r14)
            com.facebook.quicklog.QuickPerformanceLogger r2 = r15.A04
            X.0dZ r0 = r10.A08
            java.lang.String r1 = r0.A06
            java.lang.String r0 = "mutation_name"
            r2.markerAnnotate(r4, r14, r0, r1)
            X.6c1 r0 = X.C137796c1.A01
            if (r11 == r0) goto L_0x0052
            com.facebook.quicklog.QuickPerformanceLogger r1 = r15.A04
            java.lang.String r0 = "offline_supported"
            r1.markerTag(r4, r14, r0)
            com.facebook.quicklog.QuickPerformanceLogger r2 = r15.A04
            int r0 = r9.A00
            java.lang.String r1 = java.lang.String.valueOf(r0)
            r0 = 23
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            r2.markerAnnotate(r4, r14, r0, r1)
        L_0x0052:
            if (r10 == 0) goto L_0x00bc
            X.0dZ r8 = r10.A08
            if (r8 == 0) goto L_0x0099
            int r2 = r8.A01
            r1 = 32
            r2 = r2 & r1
            r0 = 0
            if (r2 != r1) goto L_0x0061
            r0 = 1
        L_0x0061:
            if (r0 == 0) goto L_0x0099
            java.util.UUID r0 = X.C188215g.A00()
            java.lang.String r5 = r0.toString()
            java.lang.String r4 = "client_mutation_id"
            java.lang.String r7 = r8.A00
            com.google.common.base.Preconditions.checkNotNull(r7)
            com.facebook.graphql.query.GraphQlQueryParamSet r0 = r8.A00
            if (r0 == 0) goto L_0x0099
            com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000 r0 = r0.A00
            X.0os r0 = r0.A00
            r2 = r0
            if (r0 == 0) goto L_0x0099
            r1 = 0
        L_0x007e:
            int r0 = r2.A00
            if (r1 >= r0) goto L_0x0099
            java.lang.String r0 = r2.A0H(r1)
            boolean r0 = r0.equals(r7)
            if (r0 == 0) goto L_0x013e
            java.lang.Object r1 = r2.A0G(r1)
            boolean r0 = r1 instanceof X.C12240os
            if (r0 == 0) goto L_0x0099
            X.0os r1 = (X.C12240os) r1
            r1.A0K(r4, r5)
        L_0x0099:
            X.0dZ r5 = r10.A08
            if (r5 == 0) goto L_0x00a8
            int r2 = r5.A01
            r1 = 64
            r2 = r2 & r1
            r0 = 0
            if (r2 != r1) goto L_0x00a6
            r0 = 1
        L_0x00a6:
            if (r0 == 0) goto L_0x00bc
        L_0x00a8:
            com.facebook.auth.viewercontext.ViewerContext r1 = r10.B9R()
            X.0aY r0 = r15.A01
            X.7Xk r4 = new X.7Xk
            r4.<init>(r1, r0)
            java.lang.String r2 = "actor_id"
            com.facebook.graphql.query.GraphQlQueryParamSet r1 = r5.A00
            java.lang.String r0 = r5.A00
            X.AnonymousClass0mJ.A02(r1, r0, r2, r4)
        L_0x00bc:
            int r1 = X.AnonymousClass1Y3.AKm
            X.0UN r0 = r15.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)
            com.facebook.graphql.executor.OfflineMutationsManager r0 = (com.facebook.graphql.executor.OfflineMutationsManager) r0
            r0.init()
            java.util.Map r0 = r0.A08
            java.lang.Object r12 = r0.get(r9)
            X.BMo r12 = (X.BMo) r12
            if (r12 != 0) goto L_0x00fe
            X.1dD r1 = r15.A02
            X.BMo r12 = new X.BMo
            X.BMk r0 = new X.BMk
            r0.<init>()
            r12.<init>(r1, r0)
            java.util.ArrayList r0 = r10.A07
            java.util.List r0 = java.util.Collections.unmodifiableList(r0)
            X.BMk r4 = r12.A01
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>(r0)
            X.0dQ r1 = com.google.common.collect.ImmutableSet.A01()
            com.google.common.collect.ImmutableSet r0 = r4.A00
            r1.A00(r0)
            r1.A00(r2)
            com.google.common.collect.ImmutableSet r0 = r1.build()
            r4.A00 = r0
        L_0x00fe:
            X.BMp r8 = new X.BMp
            java.util.concurrent.locks.ReadWriteLock r13 = X.C11170mD.A07
            r8.<init>(r9, r10, r11, r12, r13, r14)
            com.facebook.auth.viewercontext.ViewerContext r0 = r10.B9R()
            if (r0 != 0) goto L_0x011b
            X.0aY r0 = r15.A01
            com.facebook.auth.viewercontext.ViewerContext r0 = r0.Awn()
            if (r0 == 0) goto L_0x011b
            X.0aY r0 = r15.A01
            com.facebook.auth.viewercontext.ViewerContext r0 = r0.Awn()
            r10.A01 = r0
        L_0x011b:
            int r1 = X.AnonymousClass1Y3.BLm
            X.0UN r0 = r15.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.BMn r0 = (X.BMn) r0
            com.google.common.util.concurrent.SettableFuture r4 = com.google.common.util.concurrent.SettableFuture.create()
            X.BMo r3 = r8.A01
            X.0VL r2 = r0.A09
            X.BMl r1 = new X.BMl
            r1.<init>(r0, r8, r4)
            r0 = 748070217(0x2c96a549, float:4.2816068E-12)
            X.AnonymousClass07A.A04(r2, r1, r0)
            X.51Z r0 = new X.51Z
            r0.<init>(r4, r3)
            return r0
        L_0x013e:
            int r1 = r1 + 1
            goto L_0x007e
        L_0x0142:
            r0 = move-exception
            java.lang.RuntimeException r0 = com.google.common.base.Throwables.propagate(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11170mD.A07(X.BMX, X.6c1):com.google.common.util.concurrent.ListenableFuture");
    }

    private C11170mD(AnonymousClass1XY r4, QuickPerformanceLogger quickPerformanceLogger) {
        this.A00 = new AnonymousClass0UN(3, r4);
        this.A05 = AnonymousClass0UX.A0L(r4);
        this.A02 = C27451dD.A00(r4);
        this.A01 = C10580kT.A01(r4);
        this.A03 = AnonymousClass0mJ.A00(r4);
        this.A04 = quickPerformanceLogger;
        quickPerformanceLogger.setAlwaysOnSampleRate(3211302, AnonymousClass1Y3.A1y);
        this.A04.setAlwaysOnSampleRate(3211305, AnonymousClass1Y3.A1y);
        this.A04.setAlwaysOnSampleRate(3211303, AnonymousClass1Y3.A1y);
    }

    public static final C11170mD A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
