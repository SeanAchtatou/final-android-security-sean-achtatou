package X;

import java.io.File;

/* renamed from: X.04u  reason: invalid class name */
public final class AnonymousClass04u extends AnonymousClass04v {
    public void Bsa(File file, long j) {
        AnonymousClass0P7 r4 = AnonymousClass0P7.A01;
        synchronized (r4) {
            AnonymousClass09N r3 = (AnonymousClass09N) r4.A00.get(j);
            if (r3 != null) {
                if (r3.A00 == j) {
                    r3.A01.open();
                }
                r4.A00.remove(j);
            }
        }
    }
}
