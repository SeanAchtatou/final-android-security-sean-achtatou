package X;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1jB  reason: invalid class name and case insensitive filesystem */
public final class C31121jB {
    private static volatile C31121jB A08;
    public int A00;
    public C48552ac A01;
    public AtomicReference A02 = new AtomicReference(AnonymousClass1Vy.UNKNOWN);
    public AtomicReference A03;
    public boolean A04 = false;
    public final C25051Yd A05;
    public final List A06 = Collections.synchronizedList(C04300To.A00());
    public final double[] A07 = new double[6];

    public static final C31121jB A00(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (C31121jB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new C31121jB(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public static AnonymousClass1Vy A01(C31121jB r9) {
        C48552ac r0 = r9.A01;
        if (r0 != null) {
            double Adp = r0.Adp();
            double[] dArr = r9.A07;
            if (dArr[0] == 0.0d) {
                dArr[0] = (double) r9.A05.At1(563276371591504L, 100000);
            }
            double[] dArr2 = r9.A07;
            if (Adp > dArr2[0]) {
                return AnonymousClass1Vy.A01;
            }
            double Adp2 = r9.A01.Adp();
            if (dArr2[1] == 0.0d) {
                dArr2[1] = (double) r9.A05.At1(563276371657041L, 1000);
            }
            double[] dArr3 = r9.A07;
            if (Adp2 > dArr3[1]) {
                return AnonymousClass1Vy.POOR;
            }
            double Adp3 = r9.A01.Adp();
            if (dArr3[2] == 0.0d) {
                dArr3[2] = (double) r9.A05.At1(563276371525967L, 500);
            }
            double[] dArr4 = r9.A07;
            if (Adp3 > dArr4[2]) {
                return AnonymousClass1Vy.A04;
            }
            double Adp4 = r9.A01.Adp();
            if (dArr4[3] == 0.0d) {
                dArr4[3] = (double) r9.A05.At1(563276371460430L, 250);
            }
            if (Adp4 > r9.A07[3]) {
                return AnonymousClass1Vy.GOOD;
            }
            if (r9.A01.Adp() > 0.0d) {
                return AnonymousClass1Vy.A02;
            }
        }
        return AnonymousClass1Vy.UNKNOWN;
    }

    private C31121jB(AnonymousClass1XY r3) {
        this.A05 = AnonymousClass0WT.A00(r3);
    }
}
