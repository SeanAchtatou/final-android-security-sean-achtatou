package X;

import java.util.Iterator;

/* renamed from: X.1oG  reason: invalid class name and case insensitive filesystem */
public final class C33861oG implements Iterable {
    public final /* synthetic */ Iterable[] A00;

    public C33861oG(Iterable[] iterableArr) {
        this.A00 = iterableArr;
    }

    public Iterator iterator() {
        Iterable[] iterableArr = this.A00;
        boolean z = false;
        if (iterableArr != null) {
            z = true;
        }
        AnonymousClass064.A03(z);
        return new C36791to(iterableArr);
    }
}
