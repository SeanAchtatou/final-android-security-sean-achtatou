package X;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Kt  reason: invalid class name and case insensitive filesystem */
public final class C22291Kt extends C17770zR {
    @Comparable(type = 3)
    public int A00;
    @Comparable(type = 13)
    public Drawable A01;
    @Comparable(type = 13)
    public ImageView.ScaleType A02;
    @Comparable(type = 13)
    public String A03;

    public static ComponentBuilderCBuilderShape0_0S0100000 A00(AnonymousClass0p4 r3) {
        ComponentBuilderCBuilderShape0_0S0100000 componentBuilderCBuilderShape0_0S0100000 = new ComponentBuilderCBuilderShape0_0S0100000(0);
        ComponentBuilderCBuilderShape0_0S0100000.A00(componentBuilderCBuilderShape0_0S0100000, r3, 0, 0, new C22291Kt());
        return componentBuilderCBuilderShape0_0S0100000;
    }

    public C22291Kt() {
        super("Icon");
    }
}
