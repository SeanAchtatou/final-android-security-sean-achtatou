package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0Dz  reason: invalid class name and case insensitive filesystem */
public final class C02310Dz extends Enum {
    private static final /* synthetic */ C02310Dz[] A00;
    public static final C02310Dz A01;
    public static final C02310Dz A02;
    public static final C02310Dz A03;
    public static final C02310Dz A04;
    public static final C02310Dz A05;
    public static final C02310Dz A06;
    public final int exifValue;

    static {
        C02310Dz r2 = new C02310Dz("UNDEFINED", 0, 0);
        A06 = r2;
        C02310Dz r3 = new C02310Dz("NORMAL", 1, 1);
        A02 = r3;
        C02310Dz r4 = new C02310Dz("FLIP_HORIZONTAL", 2, 2);
        A01 = r4;
        C02310Dz r5 = new C02310Dz("ROTATE_180", 3, 3);
        A03 = r5;
        C02310Dz r6 = new C02310Dz("FLIP_VERTICAL", 4, 4);
        C02310Dz r7 = new C02310Dz("TRANSPOSE", 5, 5);
        C02310Dz r8 = new C02310Dz("ROTATE_90", 6, 6);
        A05 = r8;
        C02310Dz r9 = new C02310Dz("TRANSVERSE", 7, 7);
        C02310Dz r10 = new C02310Dz("ROTATE_270", 8, 8);
        A04 = r10;
        A00 = new C02310Dz[]{r2, r3, r4, r5, r6, r7, r8, r9, r10};
    }

    public static C02310Dz valueOf(String str) {
        return (C02310Dz) Enum.valueOf(C02310Dz.class, str);
    }

    public static C02310Dz[] values() {
        return (C02310Dz[]) A00.clone();
    }

    private C02310Dz(String str, int i, int i2) {
        this.exifValue = i2;
    }

    public static C02310Dz A00(int i) {
        for (C02310Dz r1 : values()) {
            if (i == r1.exifValue) {
                return r1;
            }
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Invalid ExifInterface Orientation: ", i));
    }
}
