package X;

import android.os.Build;
import android.system.Os;
import android.system.OsConstants;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.0Gf  reason: invalid class name */
public final class AnonymousClass0Gf {
    public static long A00(long j) {
        long j2;
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            j2 = Os.sysconf(OsConstants._SC_CLK_TCK);
        } else if (i >= 14) {
            j2 = A02("_SC_CLK_TCK", j);
        } else {
            j2 = j;
        }
        if (j2 > 0) {
            return j2;
        }
        return j;
    }

    public static long A01(long j) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            return Os.sysconf(OsConstants._SC_NPROCESSORS_CONF);
        }
        if (i >= 14) {
            return A02("_SC_NPROCESSORS_CONF", j);
        }
        return j;
    }

    private static long A02(String str, long j) {
        try {
            int i = Class.forName("libcore.io.OsConstants").getField(str).getInt(null);
            Class<?> cls = Class.forName("libcore.io.Libcore");
            Class<?> cls2 = Class.forName("libcore.io.Os");
            return ((Long) cls2.getMethod("sysconf", Integer.TYPE).invoke(cls.getField("os").get(null), Integer.valueOf(i))).longValue();
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
            AnonymousClass0KZ.A00("Sysconf", "Unable to read _SC_CLK_TCK by reflection", e);
            return j;
        }
    }
}
