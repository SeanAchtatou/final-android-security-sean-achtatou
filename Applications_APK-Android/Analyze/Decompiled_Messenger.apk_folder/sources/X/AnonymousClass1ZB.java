package X;

/* renamed from: X.1ZB  reason: invalid class name */
public enum AnonymousClass1ZB {
    ZERO,
    ONE,
    TWO,
    FEW,
    MANY,
    OTHER;
    
    public static final AnonymousClass1ZB[] A00;

    /* access modifiers changed from: public */
    static {
        AnonymousClass1ZB r2;
        AnonymousClass1ZB r3;
        AnonymousClass1ZB r4;
        AnonymousClass1ZB r5;
        AnonymousClass1ZB r6;
        AnonymousClass1ZB r7;
        A00 = new AnonymousClass1ZB[]{r2, r3, r4, r5, r6, r7};
    }
}
