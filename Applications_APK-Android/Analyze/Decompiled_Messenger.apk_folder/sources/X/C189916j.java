package X;

import android.os.SystemClock;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.16j  reason: invalid class name and case insensitive filesystem */
public final class C189916j implements C20021Ap {
    public final /* synthetic */ AnonymousClass16S A00;

    public C189916j(AnonymousClass16S r1) {
        this.A00 = r1;
    }

    public void Bd1(Object obj, Object obj2) {
        AnonymousClass1G3 r0;
        String message;
        Throwable th = (Throwable) obj2;
        AnonymousClass16S r1 = this.A00;
        r1.A0A = false;
        if (r1.A08 && (r0 = r1.A04) != null) {
            C20921Ei r3 = (C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, r0.A00.A08);
            if (th == null) {
                message = null;
            } else {
                message = th.getMessage();
            }
            if (C20921Ei.A03(r3)) {
                synchronized (r3.A06) {
                    r3.A01 = true;
                }
                C21061Ew withMarker = ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r3.A00)).withMarker(5505198);
                withMarker.A03("active_now_load_fail");
                if (message != null) {
                    withMarker.A08("active_now_load_error", message);
                }
                withMarker.BK9();
            }
        }
    }

    public void BdJ(Object obj, Object obj2) {
        this.A00.A0A = false;
    }

    public void BdU(Object obj, ListenableFuture listenableFuture) {
        this.A00.A0A = true;
    }

    public void Bgh(Object obj, Object obj2) {
        AnonymousClass1G3 r1;
        C189816i r11 = (C189816i) obj2;
        AnonymousClass16S r4 = this.A00;
        C005505z.A03("ANL:InboxActiveNowController:updateRows", -1554252882);
        try {
            C34801qC r7 = r11.A00;
            boolean z = true;
            if (r7 != null) {
                if (r7.A00.isEmpty()) {
                    if (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, ((C17350yl) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BRG, r4.A01)).A00)).Aem(282501482415648L)) {
                        if (r4.A00 == -1) {
                        }
                    }
                }
                z = false;
            }
            if (z) {
                ((C20921Ei) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BMw, r4.A01)).A06(5505198, "active_now_loading_no_data");
                return;
            }
            if (!r7.equals(r4.A03)) {
                r7.A00.size();
                r4.A03 = r7;
                r4.A00 = SystemClock.elapsedRealtime();
                if (r4.A08 && (r1 = r4.A04) != null) {
                    r1.A00(false);
                }
            }
            ((C20921Ei) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BMw, r4.A01)).A06(5505198, "active_now_loading_contacts_loaded");
            C005505z.A00(242706171);
        } finally {
            C005505z.A00(1203257157);
        }
    }
}
