package X;

import com.facebook.http.interfaces.RequestPriority;

/* renamed from: X.0lm  reason: invalid class name */
public final class AnonymousClass0lm {
    public static final ThreadLocal A00 = new ThreadLocal();

    public static C16890xw A00(String str, RequestPriority requestPriority) {
        C16890xw r0 = (C16890xw) A00.get();
        if (r0 == null) {
            return new C16890xw(str, requestPriority);
        }
        if (requestPriority != null) {
            r0.A04 = requestPriority;
        }
        return r0;
    }
}
