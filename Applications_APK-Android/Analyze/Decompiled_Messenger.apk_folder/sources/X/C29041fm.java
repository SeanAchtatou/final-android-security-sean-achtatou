package X;

import java.io.Serializable;

/* renamed from: X.1fm  reason: invalid class name and case insensitive filesystem */
public final class C29041fm implements Comparable, Serializable {
    private static final long serialVersionUID = 1;
    private Class _class;
    public String _className;
    private int _hashCode;

    public boolean equals(Object obj) {
        if (obj != this) {
            return obj != null && obj.getClass() == getClass() && ((C29041fm) obj)._class == this._class;
        }
        return true;
    }

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return this._className.compareTo(((C29041fm) obj)._className);
    }

    public int hashCode() {
        return this._hashCode;
    }

    public String toString() {
        return this._className;
    }

    public C29041fm() {
        this._class = null;
        this._className = null;
        this._hashCode = 0;
    }

    public C29041fm(Class cls) {
        this._class = cls;
        String name = cls.getName();
        this._className = name;
        this._hashCode = name.hashCode();
    }
}
