package X;

import com.facebook.acra.ACRA;
import com.facebook.proxygen.AnalyticsLogger;
import java.util.Map;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1sa  reason: invalid class name and case insensitive filesystem */
public final class C36161sa implements AnalyticsLogger {
    public Integer A00;
    public final C01570At A01;
    private final AnonymousClass0B6 A02;
    private final AnonymousClass0C6 A03;

    public void reportEvent(Map map, String str, String str2) {
        String str3;
        if (this.A03 != null) {
            StringBuilder sb = new StringBuilder();
            if (sb.length() > 0) {
                map.put("settings", sb.toString());
            }
        }
        Integer num = this.A00;
        if (num != null) {
            switch (num.intValue()) {
                case 1:
                    str3 = "NoNetwork";
                    break;
                case 2:
                    str3 = "Wifi";
                    break;
                case 3:
                    str3 = "Cell_2G";
                    break;
                case 4:
                    str3 = "Cell_3G";
                    break;
                case 5:
                    str3 = "Cell_4G";
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    str3 = "Cell_other";
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    str3 = "Other";
                    break;
                default:
                    str3 = "Unknown";
                    break;
            }
            map.put("network_type", str3);
        }
        this.A02.A07(str, map);
    }

    public C36161sa(AnonymousClass0B6 r1, AnonymousClass0C6 r2, C01570At r3) {
        this.A02 = r1;
        this.A03 = r2;
        this.A01 = r3;
    }
}
