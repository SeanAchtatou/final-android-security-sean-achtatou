package X;

import android.database.sqlite.SQLiteDatabase;

/* renamed from: X.18i  reason: invalid class name and case insensitive filesystem */
public final class C194818i extends AnonymousClass0W4 {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("add_timestamp_ms", "INTEGER");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6(ECX.$const$string(43), "INTEGER");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("key", "BLOB");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("timestamp_ms", "INTEGER");
    public static final AnonymousClass0W6 A04 = new AnonymousClass0W6("name", "TEXT");
    public static final AnonymousClass0W6 A05 = new AnonymousClass0W6("other_user_device_id", "TEXT");
    public static final AnonymousClass0W6 A06 = new AnonymousClass0W6("sender_key_status", "INTEGER");
    public static final AnonymousClass0W6 A07 = new AnonymousClass0W6("state", "BLOB");
    public static final AnonymousClass0W6 A08 = new AnonymousClass0W6("format", "INTEGER");
    public static final AnonymousClass0W6 A09 = new AnonymousClass0W6("thread_encryption_key_version", "INTEGER");
    public static final AnonymousClass0W6 A0A = new AnonymousClass0W6("thread_key", "TEXT");

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C194818i() {
        /*
            r11 = this;
            X.0W6 r0 = X.C194818i.A0A
            X.0W6 r1 = X.C194818i.A01
            X.0W6 r2 = X.C194818i.A05
            X.0W6 r3 = X.C194818i.A04
            X.0W6 r4 = X.C194818i.A06
            X.0W6 r5 = X.C194818i.A03
            X.0W6 r6 = X.C194818i.A08
            X.0W6 r7 = X.C194818i.A02
            X.0W6 r8 = X.C194818i.A07
            X.0W6 r9 = X.C194818i.A00
            X.0W6 r10 = X.C194818i.A09
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
            X.0al r3 = new X.0al
            X.0W6 r2 = X.C194818i.A0A
            X.0W6 r0 = X.C194818i.A05
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r2, r1, r0)
            r3.<init>(r0)
            java.lang.String r0 = "thread_devices"
            r11.<init>(r0, r4, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C194818i.<init>():void");
    }
}
