package X;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;

/* renamed from: X.0v0  reason: invalid class name and case insensitive filesystem */
public final class C15270v0 {
    public ViewParent A00;
    public ViewParent A01;
    public boolean A02;
    public int[] A03;
    public final View A04;

    public static ViewParent A00(C15270v0 r1, int i) {
        if (i == 0) {
            return r1.A01;
        }
        if (i != 1) {
            return null;
        }
        return r1.A00;
    }

    public static boolean A01(C15270v0 r15, int i, int i2, int i3, int i4, int[] iArr, int i5, int[] iArr2) {
        int i6;
        ViewParent A002;
        int i7;
        int i8;
        int[] iArr3 = iArr2;
        C15270v0 r3 = r15;
        if (r15.A02 && (A002 = A00(r3, (i6 = i5))) != null) {
            int i9 = i4;
            int[] iArr4 = iArr;
            int i10 = i;
            int i11 = i2;
            int i12 = i3;
            if (i != 0 || i2 != 0 || i3 != 0 || i4 != 0) {
                if (iArr != null) {
                    r3.A04.getLocationInWindow(iArr4);
                    i7 = iArr[0];
                    i8 = iArr[1];
                } else {
                    i7 = 0;
                    i8 = 0;
                }
                if (iArr2 == null) {
                    if (r3.A03 == null) {
                        r3.A03 = new int[2];
                    }
                    iArr3 = r3.A03;
                    iArr3[0] = 0;
                    iArr3[1] = 0;
                }
                View view = r3.A04;
                if (A002 instanceof C15190us) {
                    ((C15190us) A002).BgL(view, i10, i11, i12, i9, i6, iArr3);
                } else {
                    iArr3[0] = iArr3[0] + i3;
                    iArr3[1] = iArr3[1] + i4;
                    if (A002 instanceof C15200ut) {
                        ((C15200ut) A002).BgK(view, i10, i11, i12, i9, i6);
                    } else if (i5 == 0) {
                        if (Build.VERSION.SDK_INT >= 21) {
                            try {
                                A002.onNestedScroll(view, i10, i11, i12, i9);
                            } catch (AbstractMethodError e) {
                                Log.e("ViewParentCompat", "ViewParent " + A002 + " does not implement interface method onNestedScroll", e);
                            }
                        } else if (A002 instanceof C15310v5) {
                            ((C15310v5) A002).onNestedScroll(view, i10, i11, i12, i9);
                        }
                    }
                }
                if (iArr != null) {
                    r3.A04.getLocationInWindow(iArr4);
                    iArr[0] = iArr[0] - i7;
                    iArr[1] = iArr[1] - i8;
                }
                return true;
            } else if (iArr != null) {
                iArr[0] = 0;
                iArr[1] = 0;
            }
        }
        return false;
    }

    public boolean A03(float f, float f2) {
        ViewParent A002;
        if (!this.A02 || (A002 = A00(this, 0)) == null) {
            return false;
        }
        View view = this.A04;
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                return A002.onNestedPreFling(view, f, f2);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + A002 + " does not implement interface method onNestedPreFling", e);
                return false;
            }
        } else if (A002 instanceof C15310v5) {
            return ((C15310v5) A002).onNestedPreFling(view, f, f2);
        } else {
            return false;
        }
    }

    public boolean A04(float f, float f2, boolean z) {
        ViewParent A002;
        if (!this.A02 || (A002 = A00(this, 0)) == null) {
            return false;
        }
        View view = this.A04;
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                return A002.onNestedFling(view, f, f2, z);
            } catch (AbstractMethodError e) {
                Log.e("ViewParentCompat", "ViewParent " + A002 + " does not implement interface method onNestedFling", e);
                return false;
            }
        } else if (A002 instanceof C15310v5) {
            return ((C15310v5) A002).onNestedFling(view, f, f2, z);
        } else {
            return false;
        }
    }

    public boolean A06(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        int i4;
        ViewParent A002;
        int i5;
        int i6;
        int[] iArr3 = iArr;
        if (this.A02 && (A002 = A00(this, (i4 = i3))) != null) {
            int i7 = i2;
            int i8 = i;
            int[] iArr4 = iArr2;
            if (i != 0 || i2 != 0) {
                if (iArr2 != null) {
                    this.A04.getLocationInWindow(iArr4);
                    i5 = iArr2[0];
                    i6 = iArr2[1];
                } else {
                    i5 = 0;
                    i6 = 0;
                }
                if (iArr == null) {
                    if (this.A03 == null) {
                        this.A03 = new int[2];
                    }
                    iArr3 = this.A03;
                }
                iArr3[0] = 0;
                iArr3[1] = 0;
                View view = this.A04;
                if (A002 instanceof C15200ut) {
                    ((C15200ut) A002).BgJ(view, i8, i7, iArr3, i4);
                } else if (i3 == 0) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        try {
                            A002.onNestedPreScroll(view, i, i7, iArr3);
                        } catch (AbstractMethodError e) {
                            Log.e("ViewParentCompat", "ViewParent " + A002 + " does not implement interface method onNestedPreScroll", e);
                        }
                    } else if (A002 instanceof C15310v5) {
                        ((C15310v5) A002).onNestedPreScroll(view, i, i7, iArr3);
                    }
                }
                if (iArr2 != null) {
                    this.A04.getLocationInWindow(iArr4);
                    iArr2[0] = iArr2[0] - i5;
                    iArr2[1] = iArr2[1] - i6;
                }
                if (iArr3[0] == 0 && iArr3[1] == 0) {
                    return false;
                }
                return true;
            } else if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
        }
        return false;
    }

    public C15270v0(View view) {
        this.A04 = view;
    }

    public void A02(int i) {
        ViewParent A002 = A00(this, i);
        if (A002 != null) {
            View view = this.A04;
            if (A002 instanceof C15200ut) {
                ((C15200ut) A002).Bq8(view, i);
            } else if (i == 0) {
                if (Build.VERSION.SDK_INT >= 21) {
                    try {
                        A002.onStopNestedScroll(view);
                    } catch (AbstractMethodError e) {
                        Log.e("ViewParentCompat", "ViewParent " + A002 + " does not implement interface method onStopNestedScroll", e);
                    }
                } else if (A002 instanceof C15310v5) {
                    ((C15310v5) A002).onStopNestedScroll(view);
                }
            }
            if (i == 0) {
                this.A01 = null;
            } else if (i == 1) {
                this.A00 = null;
            }
        }
    }

    public boolean A05(int i, int i2) {
        boolean z;
        boolean z2 = false;
        if (A00(this, i2) != null) {
            z2 = true;
        }
        if (!z2) {
            if (!this.A02) {
                return false;
            }
            ViewParent parent = this.A04.getParent();
            View view = this.A04;
            while (parent != null) {
                View view2 = this.A04;
                boolean z3 = parent instanceof C15200ut;
                if (z3) {
                    z = ((C15200ut) parent).Bph(view, view2, i, i2);
                } else {
                    if (i2 == 0) {
                        if (Build.VERSION.SDK_INT >= 21) {
                            try {
                                z = parent.onStartNestedScroll(view, view2, i);
                            } catch (AbstractMethodError e) {
                                Log.e("ViewParentCompat", "ViewParent " + parent + " does not implement interface method onStartNestedScroll", e);
                            }
                        } else if (parent instanceof C15310v5) {
                            z = ((C15310v5) parent).onStartNestedScroll(view, view2, i);
                        }
                    }
                    z = false;
                }
                if (z) {
                    if (i2 == 0) {
                        this.A01 = parent;
                    } else if (i2 == 1) {
                        this.A00 = parent;
                    }
                    View view3 = this.A04;
                    if (z3) {
                        ((C15200ut) parent).BgM(view, view3, i, i2);
                    } else if (i2 == 0) {
                        if (Build.VERSION.SDK_INT >= 21) {
                            try {
                                parent.onNestedScrollAccepted(view, view3, i);
                                return true;
                            } catch (AbstractMethodError e2) {
                                Log.e("ViewParentCompat", "ViewParent " + parent + " does not implement interface method onNestedScrollAccepted", e2);
                                return true;
                            }
                        } else if (parent instanceof C15310v5) {
                            ((C15310v5) parent).onNestedScrollAccepted(view, view3, i);
                            return true;
                        }
                    }
                } else {
                    if (parent instanceof View) {
                        view = (View) parent;
                    }
                    parent = parent.getParent();
                }
            }
            return false;
        }
        return true;
    }
}
