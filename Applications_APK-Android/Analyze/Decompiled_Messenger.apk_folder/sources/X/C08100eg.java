package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0eg  reason: invalid class name and case insensitive filesystem */
public final class C08100eg extends AnonymousClass0UV {
    private static volatile C26131ax A00;

    public static final C26131ax A00(AnonymousClass1XY r6) {
        if (A00 == null) {
            synchronized (C26131ax.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A00 = new C26131ax(new C26451bT(C08070ed.A02(applicationInjector), C08030eZ.A00(applicationInjector), AnonymousClass1YA.A02(applicationInjector)));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
