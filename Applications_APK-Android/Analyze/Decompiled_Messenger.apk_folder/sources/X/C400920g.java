package X;

import android.database.sqlite.SQLiteDatabase;
import com.facebook.graphql.enums.GraphQLMessengerContactUpsellFeatureType;
import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.20g  reason: invalid class name and case insensitive filesystem */
public final class C400920g implements C04810Wg {
    public final /* synthetic */ C92904bl A00;

    public C400920g(C92904bl r1) {
        this.A00 = r1;
    }

    public void BYh(Throwable th) {
        C010708t.A0R("ClientFeatureFetchController", th, "GraphQL fetch failed");
    }

    public void Bqf(Object obj) {
        ImmutableList A0M;
        String str;
        C74973it r2;
        GraphQLResult graphQLResult = (GraphQLResult) obj;
        if (graphQLResult != null) {
            GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) graphQLResult.A03;
            if (gSTModelShape1S0000000 == null) {
                A0M = RegularImmutableList.A02;
            } else {
                A0M = gSTModelShape1S0000000.A0M(293656600, GSTModelShape1S0000000.class, -1516387829);
                Preconditions.checkNotNull(A0M);
            }
            HashMap hashMap = new HashMap();
            hashMap.put(C74973it.A0M, new AnonymousClass04a());
            C24971Xv it = A0M.iterator();
            while (it.hasNext()) {
                GSTModelShape1S0000000 gSTModelShape1S00000002 = (GSTModelShape1S0000000) it.next();
                String A0P = gSTModelShape1S00000002.A0P(3136215);
                if (!Platform.stringIsNullOrEmpty(A0P)) {
                    ImmutableList A0M2 = gSTModelShape1S00000002.A0M(-290659267, GSTModelShape1S0000000.class, -1668718021);
                    if (!A0M2.isEmpty()) {
                        C24971Xv it2 = A0M2.iterator();
                        while (it2.hasNext()) {
                            GSTModelShape1S0000000 gSTModelShape1S00000003 = (GSTModelShape1S0000000) it2.next();
                            double doubleValue = gSTModelShape1S00000003.getDoubleValue(109264530);
                            GraphQLMessengerContactUpsellFeatureType graphQLMessengerContactUpsellFeatureType = (GraphQLMessengerContactUpsellFeatureType) gSTModelShape1S00000003.A0O(1209218787, GraphQLMessengerContactUpsellFeatureType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE);
                            if (graphQLMessengerContactUpsellFeatureType != null) {
                                if (graphQLMessengerContactUpsellFeatureType.ordinal() != 1) {
                                    r2 = null;
                                } else {
                                    r2 = C74973it.A0M;
                                }
                                if (r2 != null) {
                                    Map map = (Map) hashMap.get(r2);
                                    if (map == null) {
                                        map = new AnonymousClass04a();
                                    }
                                    map.put(A0P, Double.valueOf(doubleValue));
                                    hashMap.put(r2, map);
                                }
                            }
                        }
                    }
                }
            }
            C75893kc r3 = (C75893kc) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AQi, this.A00.A00);
            if (hashMap.isEmpty()) {
                C010708t.A0I("ClientFeatureStoreHelper", "Map should not be empty at this point.");
                return;
            }
            if (hashMap.isEmpty()) {
                C010708t.A0I("ClientFeatureStoreHelper", "Map should not be empty at this point.");
            } else {
                String str2 = "DELETE FROM client_features WHERE ";
                int i = 0;
                for (Map.Entry key : hashMap.entrySet()) {
                    C74973it r0 = (C74973it) key.getKey();
                    if (i == 0) {
                        str = "feature_name = '";
                    } else {
                        str = " OR feature_name = '";
                    }
                    str2 = AnonymousClass08S.A0J(str2, AnonymousClass08S.A0P(str, r0.toString(), "'"));
                    i++;
                }
                SQLiteDatabase Ab4 = AnonymousClass2RP.A00(r3.A00).Ab4();
                C007406x.A00(348503756);
                Ab4.execSQL(str2);
                C007406x.A00(-1658820013);
            }
            C23294Bbl A01 = C23298Bbp.A00(r3.A00).A01();
            try {
                C23295Bbm A012 = A01.A01(new C22902BJv());
                for (Map.Entry entry : hashMap.entrySet()) {
                    C74973it r8 = (C74973it) entry.getKey();
                    for (Map.Entry entry2 : ((Map) entry.getValue()).entrySet()) {
                        C22904BJx bJx = (C22904BJx) A012.A00();
                        bJx.A00.A05(0, (String) entry2.getKey());
                        bJx.A00.A05(1, r8.toString());
                        bJx.A00.A02(2, (Double) entry2.getValue());
                        bJx.AOV();
                    }
                }
                A01.A03();
            } catch (Exception e) {
                C010708t.A0R("ClientFeatureStoreHelper", e, "Exception when replacing multiple feature types");
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
            A01.A02();
            for (C74973it remove : hashMap.keySet()) {
                r3.A01.A00.remove(remove);
            }
        }
    }
}
