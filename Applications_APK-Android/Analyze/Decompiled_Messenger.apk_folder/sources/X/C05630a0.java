package X;

import com.facebook.auth.viewercontext.ViewerContext;
import com.google.common.base.Objects;
import java.util.List;
import java.util.NoSuchElementException;

/* renamed from: X.0a0  reason: invalid class name and case insensitive filesystem */
public final class C05630a0 implements C05920aY {
    private ThreadLocal A00 = new C05640a1();
    public final AnonymousClass09P A01;
    private final AnonymousClass0XN A02;
    private final ViewerContext A03;
    private final String A04;

    public ViewerContext Asn() {
        return this.A02.A08();
    }

    public ViewerContext Awc() {
        throw new UnsupportedOperationException();
    }

    public ViewerContext Awn() {
        if (Objects.equal(this.A03.mUserId, this.A04)) {
            return null;
        }
        return this.A03;
    }

    public ViewerContext B9R() {
        List list = (List) this.A00.get();
        if (!list.isEmpty()) {
            return (ViewerContext) list.get(list.size() - 1);
        }
        if (!Objects.equal(this.A03.mUserId, this.A04)) {
            return this.A03;
        }
        ViewerContext A08 = this.A02.A08();
        if (A08 == null) {
            this.A02.A0I();
            return this.A03;
        } else if (Objects.equal(A08.mUserId, this.A04)) {
            return A08;
        } else {
            this.A02.A0I();
            return this.A03;
        }
    }

    public void BxG() {
        List list = (List) this.A00.get();
        if (!list.isEmpty()) {
            list.remove(list.size() - 1);
            return;
        }
        throw new NoSuchElementException();
    }

    public C09830il Byt(ViewerContext viewerContext) {
        if (viewerContext == null) {
            return C09830il.A00;
        }
        ((List) this.A00.get()).add(viewerContext);
        return new AnonymousClass4UD(this, viewerContext);
    }

    public void CA8(ViewerContext viewerContext) {
        throw new UnsupportedOperationException();
    }

    public C05630a0(AnonymousClass0XN r2, ViewerContext viewerContext, AnonymousClass09P r4) {
        ViewerContext viewerContext2;
        this.A02 = r2;
        this.A03 = viewerContext == null ? C05570Zl.A00 : viewerContext;
        this.A01 = r4;
        if (r2.A08() != null) {
            viewerContext2 = r2.A08();
        } else {
            viewerContext2 = this.A03;
        }
        this.A04 = viewerContext2.mUserId;
    }

    public ViewerContext B9S() {
        return B9R();
    }
}
