package X;

import com.google.common.base.Predicate;

/* renamed from: X.0iL  reason: invalid class name and case insensitive filesystem */
public final class C09710iL implements Predicate {
    public final /* synthetic */ C09670iH A00;

    public C09710iL(C09670iH r1) {
        this.A00 = r1;
    }

    public boolean apply(Object obj) {
        String str = (String) obj;
        if (str == null) {
            return false;
        }
        C10560kP r0 = new C10560kP(str);
        int A03 = r0.A03();
        if (!r0.A02.matches() || A03 == 0 || A03 == this.A00.A02.A01()) {
            return false;
        }
        return true;
    }
}
