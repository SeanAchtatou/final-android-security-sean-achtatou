package X;

/* renamed from: X.07m  reason: invalid class name and case insensitive filesystem */
public final class C008207m implements C01840Bv {
    private int A00 = 0;
    private final int A01;
    private final int A02;
    private final int A03;

    public boolean BBa(boolean z) {
        return z ? this.A00 < this.A01 : this.A00 < this.A02;
    }

    public String toString() {
        return String.format(null, "BackToBackRetryStrategy: attempt:%d/%d/%d, delay:%d seconds", Integer.valueOf(this.A00), Integer.valueOf(this.A01), Integer.valueOf(this.A02), Integer.valueOf(this.A03));
    }

    public C008207m(int i, int i2, int i3) {
        this.A01 = i;
        this.A02 = i2;
        this.A03 = i3;
    }

    public C008107l B48() {
        return C008107l.BACK_TO_BACK;
    }

    public int BLm(boolean z) {
        if (!BBa(z)) {
            return -1;
        }
        this.A00++;
        return this.A03;
    }
}
