package X;

import java.util.Comparator;

/* renamed from: X.1IE  reason: invalid class name */
public final class AnonymousClass1IE {
    private static final Comparator A00 = new AnonymousClass1IO();

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0086, code lost:
        if (r7[r15 - 1] < r7[r15 + r1]) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ae, code lost:
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00af, code lost:
        if (r3 > r5) goto L_0x011b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00b1, code lost:
        r2 = r3 + r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00b5, code lost:
        if (r2 == (r5 + r19)) goto L_0x0119;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b9, code lost:
        if (r2 == (r4 + r19)) goto L_0x0110;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00bb, code lost:
        r16 = r21 + r2;
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c6, code lost:
        if (r6[r16 - 1] >= r6[r16 + 1]) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c8, code lost:
        r0 = r6[(r21 + r2) - r1];
        r15 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ce, code lost:
        r17 = r0 - r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d0, code lost:
        if (r0 <= 0) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00d2, code lost:
        if (r17 <= 0) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e4, code lost:
        if (r23.A03((r14 + r0) - 1, (r12 + r17) - 1) == false) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e6, code lost:
        r0 = r0 - 1;
        r17 = r17 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00eb, code lost:
        r1 = r21 + r2;
        r6[r1] = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ef, code lost:
        if (r18 != false) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f1, code lost:
        if (r2 < r4) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f3, code lost:
        if (r2 > r5) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f5, code lost:
        r1 = r7[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00f7, code lost:
        if (r1 < r0) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f9, code lost:
        r3 = new X.AnonymousClass1IR();
        r3.A01 = r0;
        r3.A02 = r0 - r2;
        r3.A00 = r1 - r0;
        r3.A03 = r15;
        r3.A04 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x010d, code lost:
        r3 = r3 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0110, code lost:
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0111, code lost:
        r0 = r6[(r21 + r2) + r1] - r1;
        r15 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0119, code lost:
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x011b, code lost:
        r5 = r5 + 1;
        r1 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0189  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01af  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1IQ A00(X.AnonymousClass1ID r25, boolean r26) {
        /*
            r23 = r25
            int r3 = r23.A01()
            int r2 = r23.A00()
            java.util.ArrayList r22 = new java.util.ArrayList
            r22.<init>()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            X.1IP r1 = new X.1IP
            r0 = 0
            r1.<init>(r0, r3, r0, r2)
            r10.add(r1)
            int r0 = r3 + r2
            int r3 = r3 - r2
            int r21 = java.lang.Math.abs(r3)
            int r21 = r21 + r0
            int r0 = r21 << 1
            int[] r7 = new int[r0]
            int[] r6 = new int[r0]
            java.util.ArrayList r20 = new java.util.ArrayList
            r20.<init>()
        L_0x0031:
            boolean r0 = r10.isEmpty()
            if (r0 != 0) goto L_0x01ec
            int r0 = r10.size()
            int r0 = r0 + -1
            java.lang.Object r9 = r10.remove(r0)
            X.1IP r9 = (X.AnonymousClass1IP) r9
            int r14 = r9.A03
            int r13 = r9.A02
            int r12 = r9.A01
            int r11 = r9.A00
            int r13 = r13 - r14
            int r11 = r11 - r12
            r1 = 1
            if (r13 < r1) goto L_0x0120
            if (r11 < r1) goto L_0x0120
            int r19 = r13 - r11
            int r0 = r13 + r11
            int r0 = r0 + r1
            int r8 = r0 >> 1
            int r3 = r21 - r8
            int r3 = r3 - r1
            int r2 = r21 + r8
            int r2 = r2 + r1
            r0 = 0
            java.util.Arrays.fill(r7, r3, r2, r0)
            int r3 = r3 + r19
            int r2 = r2 + r19
            java.util.Arrays.fill(r6, r3, r2, r13)
            int r0 = r19 % 2
            r18 = 0
            if (r0 == 0) goto L_0x0072
            r18 = 1
        L_0x0072:
            r5 = 0
        L_0x0073:
            if (r5 > r8) goto L_0x01e4
            int r4 = -r5
            r2 = r4
        L_0x0077:
            if (r2 > r5) goto L_0x00ae
            if (r2 == r4) goto L_0x0088
            if (r2 == r5) goto L_0x00a7
            int r15 = r21 + r2
            int r0 = r15 + -1
            r3 = r7[r0]
            int r15 = r15 + r1
            r0 = r7[r15]
            if (r3 >= r0) goto L_0x00a7
        L_0x0088:
            int r0 = r21 + r2
            int r0 = r0 + r1
            r15 = r7[r0]
            r1 = 0
        L_0x008e:
            int r3 = r15 - r2
        L_0x0090:
            if (r15 >= r13) goto L_0x0122
            if (r3 >= r11) goto L_0x0122
            int r16 = r14 + r15
            int r0 = r12 + r3
            r24 = r16
            r25 = r0
            boolean r0 = r23.A03(r24, r25)
            if (r0 == 0) goto L_0x0122
            int r15 = r15 + 1
            int r3 = r3 + 1
            goto L_0x0090
        L_0x00a7:
            int r0 = r21 + r2
            int r0 = r0 - r1
            r15 = r7[r0]
            int r15 = r15 + r1
            goto L_0x008e
        L_0x00ae:
            r3 = r4
        L_0x00af:
            if (r3 > r5) goto L_0x011b
            int r2 = r3 + r19
            int r0 = r5 + r19
            if (r2 == r0) goto L_0x0119
            int r0 = r4 + r19
            if (r2 == r0) goto L_0x0110
            int r16 = r21 + r2
            int r0 = r16 + -1
            r15 = r6[r0]
            r1 = 1
            int r16 = r16 + r1
            r0 = r6[r16]
            if (r15 >= r0) goto L_0x0111
        L_0x00c8:
            int r0 = r21 + r2
            int r0 = r0 - r1
            r0 = r6[r0]
            r15 = 0
        L_0x00ce:
            int r17 = r0 - r2
        L_0x00d0:
            if (r0 <= 0) goto L_0x00eb
            if (r17 <= 0) goto L_0x00eb
            int r1 = r14 + r0
            int r16 = r1 + -1
            int r1 = r12 + r17
            int r1 = r1 + -1
            r24 = r16
            r25 = r1
            boolean r1 = r23.A03(r24, r25)
            if (r1 == 0) goto L_0x00eb
            int r0 = r0 + -1
            int r17 = r17 + -1
            goto L_0x00d0
        L_0x00eb:
            int r1 = r21 + r2
            r6[r1] = r0
            if (r18 != 0) goto L_0x010d
            if (r2 < r4) goto L_0x010d
            if (r2 > r5) goto L_0x010d
            r1 = r7[r1]
            if (r1 < r0) goto L_0x010d
            X.1IR r3 = new X.1IR
            r3.<init>()
            r3.A01 = r0
            int r2 = r0 - r2
            r3.A02 = r2
            int r1 = r1 - r0
            r3.A00 = r1
            r3.A03 = r15
            r0 = 1
            r3.A04 = r0
            goto L_0x014a
        L_0x010d:
            int r3 = r3 + 2
            goto L_0x00af
        L_0x0110:
            r1 = 1
        L_0x0111:
            int r0 = r21 + r2
            int r0 = r0 + r1
            r0 = r6[r0]
            int r0 = r0 - r1
            r15 = 1
            goto L_0x00ce
        L_0x0119:
            r1 = 1
            goto L_0x00c8
        L_0x011b:
            int r5 = r5 + 1
            r1 = 1
            goto L_0x0073
        L_0x0120:
            r3 = 0
            goto L_0x014a
        L_0x0122:
            int r16 = r21 + r2
            r7[r16] = r15
            if (r18 == 0) goto L_0x01df
            int r0 = r19 - r5
            r3 = 1
            int r0 = r0 + r3
            if (r2 < r0) goto L_0x01df
            int r0 = r19 + r5
            int r0 = r0 - r3
            if (r2 > r0) goto L_0x01df
            r0 = r6[r16]
            if (r15 < r0) goto L_0x01df
            X.1IR r3 = new X.1IR
            r3.<init>()
            r3.A01 = r0
            int r2 = r0 - r2
            r3.A02 = r2
            int r15 = r15 - r0
            r3.A00 = r15
            r3.A03 = r1
            r0 = 0
            r3.A04 = r0
        L_0x014a:
            if (r3 == 0) goto L_0x01d8
            int r0 = r3.A00
            if (r0 <= 0) goto L_0x0155
            r0 = r22
            r0.add(r3)
        L_0x0155:
            int r1 = r3.A01
            int r0 = r9.A03
            int r1 = r1 + r0
            r3.A01 = r1
            int r1 = r3.A02
            int r0 = r9.A01
            int r1 = r1 + r0
            r3.A02 = r1
            boolean r0 = r20.isEmpty()
            if (r0 == 0) goto L_0x01c9
            X.1IP r0 = new X.1IP
            r0.<init>()
        L_0x016e:
            int r1 = r9.A03
            r0.A03 = r1
            int r1 = r9.A01
            r0.A01 = r1
            boolean r1 = r3.A04
            if (r1 == 0) goto L_0x01b5
            int r1 = r3.A01
        L_0x017c:
            r0.A02 = r1
            int r1 = r3.A02
            r0.A00 = r1
        L_0x0182:
            r10.add(r0)
            boolean r0 = r3.A04
            if (r0 == 0) goto L_0x01af
            boolean r0 = r3.A03
            if (r0 == 0) goto L_0x01a0
            int r0 = r3.A01
            int r1 = r3.A00
            int r0 = r0 + r1
            int r0 = r0 + 1
        L_0x0194:
            r9.A03 = r0
            int r0 = r3.A02
            int r0 = r0 + r1
            r9.A01 = r0
        L_0x019b:
            r10.add(r9)
            goto L_0x0031
        L_0x01a0:
            int r0 = r3.A01
            int r1 = r3.A00
            int r0 = r0 + r1
            r9.A03 = r0
            int r0 = r3.A02
            int r0 = r0 + r1
            int r0 = r0 + 1
            r9.A01 = r0
            goto L_0x019b
        L_0x01af:
            int r0 = r3.A01
            int r1 = r3.A00
            int r0 = r0 + r1
            goto L_0x0194
        L_0x01b5:
            boolean r1 = r3.A03
            if (r1 == 0) goto L_0x01be
            int r1 = r3.A01
            int r1 = r1 + -1
            goto L_0x017c
        L_0x01be:
            int r1 = r3.A01
            r0.A02 = r1
            int r1 = r3.A02
            int r1 = r1 + -1
            r0.A00 = r1
            goto L_0x0182
        L_0x01c9:
            int r0 = r20.size()
            int r1 = r0 + -1
            r0 = r20
            java.lang.Object r0 = r0.remove(r1)
            X.1IP r0 = (X.AnonymousClass1IP) r0
            goto L_0x016e
        L_0x01d8:
            r0 = r20
            r0.add(r9)
            goto L_0x0031
        L_0x01df:
            int r2 = r2 + 2
            r1 = 1
            goto L_0x0077
        L_0x01e4:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "DiffUtil hit an unexpected case while trying to calculate the optimal path. Please make sure your data is not changing during the diff calculation."
            r1.<init>(r0)
            throw r1
        L_0x01ec:
            java.util.Comparator r1 = X.AnonymousClass1IE.A00
            r0 = r22
            java.util.Collections.sort(r0, r1)
            X.1IQ r0 = new X.1IQ
            r1 = r23
            r2 = r22
            r3 = r7
            r4 = r6
            r5 = r26
            r0.<init>(r1, r2, r3, r4, r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IE.A00(X.1ID, boolean):X.1IQ");
    }
}
