package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0fu  reason: invalid class name and case insensitive filesystem */
public final class C08760fu {
    public static final C08760fu A01 = new C08760fu(new AnonymousClass4DW("Failure occurred while trying to finish a future."));
    public final Throwable A00;

    public C08760fu(Throwable th) {
        Preconditions.checkNotNull(th);
        this.A00 = th;
    }
}
