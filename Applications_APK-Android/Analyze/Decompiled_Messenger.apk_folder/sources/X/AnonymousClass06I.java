package X;

import com.facebook.acra.NonCrashException;

/* renamed from: X.06I  reason: invalid class name */
public final class AnonymousClass06I extends Exception implements NonCrashException {
    public String getExceptionFriendlyName() {
        return "soft error";
    }

    public AnonymousClass06I(String str, Throwable th) {
        super(str, th);
    }
}
