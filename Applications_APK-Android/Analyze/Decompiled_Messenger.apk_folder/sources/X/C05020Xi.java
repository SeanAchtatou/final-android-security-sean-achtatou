package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0Xi  reason: invalid class name and case insensitive filesystem */
public final class C05020Xi extends AnonymousClass0UV {
    private static volatile C05030Xj A00;

    public static final C05030Xj A00(AnonymousClass1XY r10) {
        if (A00 == null) {
            synchronized (C05030Xj.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r10);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r10.getApplicationInjector();
                        AnonymousClass0UQ A003 = AnonymousClass0UQ.A00(AnonymousClass1Y3.AmL, applicationInjector);
                        AnonymousClass0W9.A01();
                        AnonymousClass0XQ A004 = AnonymousClass0XP.A00(applicationInjector);
                        C05050Xl A005 = C05050Xl.A00(applicationInjector);
                        AnonymousClass0X5 r102 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A0B);
                        C05770aI A006 = A004.A00("analytics_flexible_sampling_policy");
                        A00 = new C05030Xj(A003, A006, new C05060Xm(A006), A005, r102);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
