package X;

import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.share.SentShareAttachment;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.base.Platform;

/* renamed from: X.1mR  reason: invalid class name and case insensitive filesystem */
public final class C32851mR {
    public AnonymousClass0UN A00;

    public static C30189ErG A00(Message message) {
        C30189ErG erG = new C30189ErG();
        C24971Xv it = message.A04().iterator();
        while (it.hasNext()) {
            MediaResource mediaResource = (MediaResource) it.next();
            C421828p r3 = mediaResource.A0L;
            if (r3 == C421828p.AUDIO) {
                erG.A00++;
            } else if (r3 == C421828p.PHOTO) {
                erG.A04++;
            } else if (r3 == C421828p.VIDEO) {
                erG.A07++;
            }
            erG.A09.Byx(r3, mediaResource.A0J);
            String str = mediaResource.A0a;
            if (str != null) {
                erG.A0A.add(str);
            } else {
                erG.A0A.add("unknown");
            }
        }
        SentShareAttachment sentShareAttachment = message.A0T;
        if (sentShareAttachment != null) {
            C22652B5y b5y = sentShareAttachment.A00;
            if (b5y == C22652B5y.SHARE) {
                erG.A05++;
            } else if (b5y == C22652B5y.PAYMENT) {
                erG.A03++;
            } else if (b5y == C22652B5y.BRANDED_CAMERA) {
                erG.A01++;
            }
            erG.A05 += message.A0c.toArray().length;
        }
        String str2 = message.A0z;
        if (!Platform.stringIsNullOrEmpty(str2)) {
            if (C72043dZ.A03(str2)) {
                erG.A02++;
            } else {
                erG.A06++;
                return erG;
            }
        }
        return erG;
    }

    public static final C32851mR A01(AnonymousClass1XY r1) {
        return new C32851mR(r1);
    }

    public C32851mR(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public C30188ErF A02(Message message) {
        C30189ErG A002 = A00(message);
        C24971Xv it = message.A04().iterator();
        long j = 0;
        while (it.hasNext()) {
            j += ((C192438zr) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aq2, this.A00)).A02(((MediaResource) it.next()).A0D);
        }
        long j2 = A002.A08;
        if (j2 == -1) {
            A002.A08 = j;
        } else {
            A002.A08 = j2 + j;
        }
        return new C30188ErF(A002);
    }
}
