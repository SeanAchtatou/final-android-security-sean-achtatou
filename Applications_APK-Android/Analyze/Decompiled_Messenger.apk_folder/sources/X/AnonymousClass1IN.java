package X;

import android.os.Looper;

/* renamed from: X.1IN  reason: invalid class name */
public abstract class AnonymousClass1IN extends C15730vo {
    public void A07() {
        if (this instanceof C16410x1) {
            ((C16410x1) this).A00.A0B = false;
        } else if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            throw new RuntimeException("The adapter must be notified of changes on the UI thread.");
        }
    }
}
