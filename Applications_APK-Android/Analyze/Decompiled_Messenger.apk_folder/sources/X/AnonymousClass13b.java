package X;

import android.net.NetworkInfo;
import io.card.payment.BuildConfig;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.13b  reason: invalid class name */
public final class AnonymousClass13b {
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C;
    public static final AnonymousClass1Y7 A0D;
    public static final AnonymousClass1Y7 A0E;
    private static final AnonymousClass1Y7 A0F;
    private static volatile AnonymousClass13b A0G;
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public AnonymousClass0UN A04;
    private long A05;
    private long A06;
    private long A07;
    private long A08;
    private long A09;
    private long A0A;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A06.A09("data_analytics");
        A0F = r1;
        A0C = (AnonymousClass1Y7) r1.A09("total_bytes_received_foreground");
        AnonymousClass1Y7 r12 = A0F;
        A0B = (AnonymousClass1Y7) r12.A09("total_bytes_received_background");
        A0E = (AnonymousClass1Y7) r12.A09("total_bytes_sent_foreground");
        A0D = (AnonymousClass1Y7) r12.A09("total_bytes_sent_background");
    }

    public static final AnonymousClass13b A00(AnonymousClass1XY r5) {
        if (A0G == null) {
            synchronized (AnonymousClass13b.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0G, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0G = new AnonymousClass13b(applicationInjector, AnonymousClass0UX.A0Z(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0G;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String A01(NetworkInfo networkInfo) {
        if (networkInfo == null || C06850cB.A0B(networkInfo.getTypeName())) {
            return "unknown";
        }
        if (C06850cB.A0B(networkInfo.getSubtypeName())) {
            return networkInfo.getTypeName();
        }
        return AnonymousClass08S.A0P(networkInfo.getTypeName(), ".", networkInfo.getSubtypeName().replace('.', '_').replace(' ', '_'));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035 A[SYNTHETIC, Splitter:B:12:0x0035] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(X.AnonymousClass13b r7) {
        /*
            int r2 = X.AnonymousClass1Y3.BN4
            X.0UN r1 = r7.A04
            r0 = 2
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0f9 r6 = (X.AnonymousClass0f9) r6
            boolean r0 = r6.A01
            if (r0 == 0) goto L_0x0090
            int r5 = android.os.Process.myUid()
            r1 = 0
            com.facebook.device.resourcemonitor.DataUsageBytes r4 = r6.A04(r5, r1)
            boolean r0 = X.AnonymousClass0f9.A03(r6)
            if (r0 == 0) goto L_0x002c
            r0 = 1
            com.facebook.device.resourcemonitor.DataUsageBytes r3 = X.C31921kn.A00(r5, r1, r0)     // Catch:{ 1vu -> 0x0024 }
            goto L_0x002e
        L_0x0024:
            r2 = move-exception
            java.lang.String r1 = "FbTrafficStats"
            java.lang.String r0 = "Unable to parse data usage from system file"
            X.C010708t.A0L(r1, r0, r2)
        L_0x002c:
            com.facebook.device.resourcemonitor.DataUsageBytes r3 = com.facebook.device.resourcemonitor.DataUsageBytes.A02
        L_0x002e:
            r1 = 0
            boolean r0 = X.AnonymousClass0f9.A03(r6)
            if (r0 == 0) goto L_0x0042
            com.facebook.device.resourcemonitor.DataUsageBytes r2 = X.C31921kn.A00(r5, r1, r1)     // Catch:{ 1vu -> 0x003a }
            goto L_0x0044
        L_0x003a:
            r2 = move-exception
            java.lang.String r1 = "FbTrafficStats"
            java.lang.String r0 = "Unable to parse data usage from system file"
            X.C010708t.A0L(r1, r0, r2)
        L_0x0042:
            com.facebook.device.resourcemonitor.DataUsageBytes r2 = com.facebook.device.resourcemonitor.DataUsageBytes.A02
        L_0x0044:
            long r0 = r4.A00
            r7.A05 = r0
            long r0 = r4.A01
            r7.A06 = r0
            long r0 = r2.A00
            r7.A00 = r0
            long r0 = r3.A00
            r7.A01 = r0
            long r0 = r2.A01
            r7.A02 = r0
            long r0 = r3.A01
            r7.A03 = r0
            long r0 = android.net.TrafficStats.getTotalRxBytes()     // Catch:{ RuntimeException -> 0x0061 }
            goto L_0x0067
        L_0x0061:
            r0 = move-exception
            X.AnonymousClass0f9.A02(r0)
            r0 = 0
        L_0x0067:
            r7.A07 = r0
            long r0 = android.net.TrafficStats.getTotalTxBytes()     // Catch:{ RuntimeException -> 0x006e }
            goto L_0x0074
        L_0x006e:
            r0 = move-exception
            X.AnonymousClass0f9.A02(r0)
            r0 = 0
        L_0x0074:
            r7.A08 = r0
            long r0 = android.net.TrafficStats.getMobileRxBytes()     // Catch:{ RuntimeException -> 0x007b }
            goto L_0x0081
        L_0x007b:
            r0 = move-exception
            X.AnonymousClass0f9.A02(r0)
            r0 = 0
        L_0x0081:
            r7.A09 = r0
            long r0 = android.net.TrafficStats.getMobileTxBytes()     // Catch:{ RuntimeException -> 0x0088 }
            goto L_0x008e
        L_0x0088:
            r0 = move-exception
            X.AnonymousClass0f9.A02(r0)
            r0 = 0
        L_0x008e:
            r7.A0A = r0
        L_0x0090:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13b.A02(X.13b):void");
    }

    public static synchronized void A03(AnonymousClass13b r16) {
        AnonymousClass13b r0 = r16;
        synchronized (r0) {
            long j = r0.A05;
            long j2 = r0.A06;
            long j3 = r0.A07;
            long j4 = r0.A08;
            long j5 = r0.A09;
            long j6 = r0.A0A;
            A02(r0);
            AnonymousClass13b r9 = r0;
            A04(r9, "total_bytes_received", r0.A05, j);
            A04(r0, "total_bytes_sent", r0.A06, j2);
            A04(r9, "total_device_bytes_received", r0.A07, j3);
            A04(r9, "total_device_bytes_sent", r0.A08, j4);
            A04(r9, "total_mobile_bytes_received", r0.A09, j5);
            A04(r0, "total_mobile_bytes_sent", r0.A0A, j6);
            AnonymousClass2ZS r4 = (AnonymousClass2ZS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AhD, r0.A04);
            r4.A05("device_bytes_received_since_boot", r0.A07);
            r4.A05("device_bytes_sent_since_boot", r0.A08);
            r4.A05("mobile_device_bytes_received_since_boot", r0.A09);
            r4.A05("mobile_device_bytes_sent_since_boot", r0.A0A);
            r4.A05("app_bytes_received_since_boot", r0.A05);
            r4.A05("app_bytes_sent_since_boot", r0.A06);
        }
    }

    public static void A04(AnonymousClass13b r5, String str, long j, long j2) {
        long j3 = j - j2;
        if (j < 0 || j3 < 0 || j3 > 524288000) {
            j3 = -1;
        }
        ((AnonymousClass2ZS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AhD, r5.A04)).A05(str, j3);
    }

    public void A05(C11670nb r7) {
        NetworkInfo A0E2 = ((C09340h3) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AeN, this.A04)).A0E();
        if (A0E2 != null) {
            String typeName = A0E2.getTypeName();
            String A0J = AnonymousClass08S.A0J(BuildConfig.FLAVOR, "connection");
            if (C06850cB.A0B(typeName)) {
                typeName = "none";
            }
            r7.A0D(A0J, typeName);
            String subtypeName = A0E2.getSubtypeName();
            if (C06850cB.A0B(subtypeName) && A0E2.getType() == 1 && ((C09340h3) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AeN, this.A04)).A0P()) {
                subtypeName = "HOTSPOT";
            }
            if (!C06850cB.A0B(subtypeName)) {
                r7.A0D(AnonymousClass08S.A0J(BuildConfig.FLAVOR, "connection_subtype"), subtypeName);
            }
        }
    }

    private AnonymousClass13b(AnonymousClass1XY r3, ExecutorService executorService) {
        this.A04 = new AnonymousClass0UN(5, r3);
        AnonymousClass07A.A04(executorService, new AnonymousClass13c(this), 1062941297);
    }
}
