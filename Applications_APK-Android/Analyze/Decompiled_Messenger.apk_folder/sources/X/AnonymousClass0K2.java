package X;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.PowerManager;
import android.util.SparseArray;

/* renamed from: X.0K2  reason: invalid class name */
public abstract class AnonymousClass0K2 extends BroadcastReceiver {
    public static int A00 = 1;
    public static final SparseArray A01 = new SparseArray();

    public static void A00(Intent intent) {
        int intExtra = intent.getIntExtra("androidx.contentpager.content.wakelockid", 0);
        if (intExtra != 0) {
            SparseArray sparseArray = A01;
            synchronized (sparseArray) {
                PowerManager.WakeLock wakeLock = (PowerManager.WakeLock) sparseArray.get(intExtra);
                if (wakeLock != null) {
                    C009007v.A01(wakeLock);
                    sparseArray.remove(intExtra);
                } else {
                    C010708t.A0P("WakefulBroadcastReceiver", "No active wake lock id #%s", Integer.valueOf(intExtra));
                }
            }
        }
    }
}
