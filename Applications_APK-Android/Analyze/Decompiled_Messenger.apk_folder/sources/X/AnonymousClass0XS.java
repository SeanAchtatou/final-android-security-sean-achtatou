package X;

import android.util.Base64OutputStream;
import com.facebook.acra.ACRA;
import com.facebook.acra.LogCatCollector;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0XS  reason: invalid class name */
public final class AnonymousClass0XS {
    public static int A02 = 1;
    public static final Map A03 = new HashMap();
    public final File A00;
    private final Object A01 = new Object();

    public static synchronized void A00(int i) {
        synchronized (AnonymousClass0XS.class) {
            A02 = i;
            if (i != 0) {
                A02 = i;
                for (Map.Entry entry : A03.entrySet()) {
                    try {
                        A01((AnonymousClass0XS) entry.getKey(), (Map) entry.getValue());
                    } catch (IOException e) {
                        C010708t.A0M("LightSharedPreferencesStorage", "Could not write shared preferences to disk!", e);
                    }
                }
                A03.clear();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public static void A01(AnonymousClass0XS r7, Map map) {
        int i;
        File createTempFile = File.createTempFile(AnonymousClass08S.A0J(r7.A00.getName(), "."), ".tmp", r7.A00.getParentFile());
        DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(createTempFile), 512));
        try {
            dataOutputStream.writeByte(1);
            dataOutputStream.writeInt(map.size());
            for (Map.Entry entry : map.entrySet()) {
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                if (value instanceof Boolean) {
                    i = 0;
                } else if (value instanceof Integer) {
                    i = 1;
                } else if (value instanceof Long) {
                    i = 2;
                } else if (value instanceof Float) {
                    i = 3;
                } else if (value instanceof Double) {
                    i = 4;
                } else if (value instanceof String) {
                    i = 5;
                } else if (value instanceof Set) {
                    i = 6;
                } else {
                    throw new IllegalArgumentException("Unsupported type: " + value.getClass());
                }
                dataOutputStream.write(i);
                dataOutputStream.writeUTF(str);
                switch (i) {
                    case 0:
                        dataOutputStream.writeBoolean(((Boolean) value).booleanValue());
                        break;
                    case 1:
                        dataOutputStream.writeInt(((Integer) value).intValue());
                        break;
                    case 2:
                        dataOutputStream.writeLong(((Long) value).longValue());
                        break;
                    case 3:
                        dataOutputStream.writeFloat(((Float) value).floatValue());
                        break;
                    case 4:
                        dataOutputStream.writeDouble(((Double) value).doubleValue());
                        break;
                    case 5:
                        dataOutputStream.writeUTF((String) value);
                        break;
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                        Set<String> set = (Set) value;
                        dataOutputStream.writeInt(set.size());
                        for (String writeUTF : set) {
                            dataOutputStream.writeUTF(writeUTF);
                        }
                        break;
                    default:
                        throw new IllegalArgumentException(AnonymousClass08S.A09("Unsupported type with ordinal: ", i));
                }
            }
            dataOutputStream.close();
            synchronized (r7.A01) {
                if (!createTempFile.renameTo(r7.A00)) {
                    createTempFile.delete();
                    throw new IOException("Failed to replace the current preference file!");
                }
            }
        } catch (Throwable th) {
            dataOutputStream.close();
            throw th;
        }
    }

    public String A02() {
        String str;
        FileInputStream fileInputStream;
        Base64OutputStream base64OutputStream;
        try {
            fileInputStream = new FileInputStream(this.A00);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream((int) this.A00.length());
            base64OutputStream = new Base64OutputStream(byteArrayOutputStream, 0);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    base64OutputStream.write(bArr, 0, read);
                } else {
                    base64OutputStream.close();
                    String byteArrayOutputStream2 = byteArrayOutputStream.toString(LogCatCollector.UTF_8_ENCODING);
                    fileInputStream.close();
                    base64OutputStream.close();
                    return byteArrayOutputStream2;
                }
            }
        } catch (IOException e) {
            if (e.getMessage() != null) {
                str = e.getMessage();
            } else {
                str = "description N/A";
            }
            return AnonymousClass08S.A0P("[I/O error: ", str, "]");
        } catch (Throwable th) {
            fileInputStream.close();
            base64OutputStream.close();
            throw th;
        }
    }

    public AnonymousClass0XS(File file) {
        this.A00 = file;
    }
}
