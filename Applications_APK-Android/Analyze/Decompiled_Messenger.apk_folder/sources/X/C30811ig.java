package X;

/* renamed from: X.1ig  reason: invalid class name and case insensitive filesystem */
public final class C30811ig implements C23111Og {
    private final C25051Yd A00;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    private int A00(long j) {
        return (int) Math.min(2147483647L, this.A00.At0(j));
    }

    public static final C30811ig A01(AnonymousClass1XY r2) {
        return new C30811ig(AnonymousClass0WT.A00(r2));
    }

    public C30811ig(C25051Yd r1) {
        this.A00 = r1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public Object get() {
        return new C30851ik((int) (((double) ((int) Math.min(2147483647L, Runtime.getRuntime().maxMemory()))) * this.A00.Aki(1129859867083052L)), A00(566909913728794L), A00(566909913794331L), A00(566909913859868L), A00(566909913925405L), (long) A00(566909913990942L));
    }
}
