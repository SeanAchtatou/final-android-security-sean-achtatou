package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ag  reason: invalid class name and case insensitive filesystem */
public final class C06000ag extends C25391Zl {
    private static volatile C06000ag A02;
    public AnonymousClass0UN A00;
    public final int[][] A01 = {new int[]{67}, new int[]{484}, new int[]{165}, new int[]{AnonymousClass1Y3.A28}, new int[]{AnonymousClass1Y3.A4C}, new int[]{582, 855}};

    public static final C06000ag A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C06000ag.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C06000ag(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C06000ag(AnonymousClass1XY r9) {
        this.A00 = new AnonymousClass0UN(0, r9);
    }
}
