package X;

import java.nio.MappedByteBuffer;

/* renamed from: X.019  reason: invalid class name */
public final class AnonymousClass019 {
    public final int A00;
    public final MappedByteBuffer A01;

    public int A00(int i) {
        MappedByteBuffer mappedByteBuffer = this.A01;
        int i2 = this.A00;
        long currentTimeMillis = System.currentTimeMillis();
        long j = currentTimeMillis - ((long) (i * AnonymousClass1Y3.A87));
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            long j2 = mappedByteBuffer.getLong(i4 << 3);
            if (j2 >= j && j2 <= currentTimeMillis) {
                i3++;
            }
        }
        return i3;
    }

    public void A01() {
        MappedByteBuffer mappedByteBuffer = this.A01;
        int i = this.A00;
        int i2 = -1;
        long j = -1;
        for (int i3 = 0; i3 < i; i3++) {
            int i4 = i3 << 3;
            long j2 = mappedByteBuffer.getLong(i4);
            if (j == -1 || j2 < j) {
                i2 = i4;
                j = j2;
            }
        }
        mappedByteBuffer.putLong(i2, System.currentTimeMillis());
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0082 A[Catch:{ FileNotFoundException -> 0x00cb, all -> 0x00e2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass019(android.content.Context r10, java.io.File r11, int r12) {
        /*
            r9 = this;
            r9.<init>()
            r9.A00 = r12
            r7 = 0
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x002d }
            java.lang.String r0 = "rw"
            r2.<init>(r11, r0)     // Catch:{ FileNotFoundException -> 0x002d }
            int r0 = r12 << 3
            long r7 = (long) r0     // Catch:{ FileNotFoundException -> 0x002d }
            r2.setLength(r7)     // Catch:{ FileNotFoundException -> 0x002a, all -> 0x00e4 }
            java.nio.channels.FileChannel r3 = r2.getChannel()     // Catch:{ FileNotFoundException -> 0x002a, all -> 0x00e4 }
            java.nio.channels.FileChannel$MapMode r4 = java.nio.channels.FileChannel.MapMode.READ_WRITE     // Catch:{ FileNotFoundException -> 0x002a, all -> 0x00e4 }
            r5 = 0
            java.nio.MappedByteBuffer r1 = r3.map(r4, r5, r7)     // Catch:{ FileNotFoundException -> 0x002a, all -> 0x00e4 }
            r9.A01 = r1     // Catch:{ FileNotFoundException -> 0x002a, all -> 0x00e4 }
            java.nio.ByteOrder r0 = java.nio.ByteOrder.LITTLE_ENDIAN     // Catch:{ FileNotFoundException -> 0x002a, all -> 0x00e4 }
            r1.order(r0)     // Catch:{ FileNotFoundException -> 0x002a, all -> 0x00e4 }
            r2.close()
            return
        L_0x002a:
            r0 = move-exception
            r7 = r2
            goto L_0x002e
        L_0x002d:
            r0 = move-exception
        L_0x002e:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00e2 }
            r3.<init>(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = " path: "
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r11.getCanonicalPath()     // Catch:{ all -> 0x00e2 }
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = " exists: "
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            boolean r0 = r11.exists()     // Catch:{ all -> 0x00e2 }
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = " canWrite: "
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            boolean r0 = r11.canWrite()     // Catch:{ all -> 0x00e2 }
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = " parent exists: "
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.io.File r0 = r11.getParentFile()     // Catch:{ all -> 0x00e2 }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x00e2 }
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = " parent canWrite: "
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.io.File r0 = r11.getParentFile()     // Catch:{ all -> 0x00e2 }
            boolean r0 = r0.canWrite()     // Catch:{ all -> 0x00e2 }
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x00e2 }
            r0 = 17
            r6 = 0
            if (r1 < r0) goto L_0x00aa
            java.lang.String r0 = "user"
            java.lang.Object r1 = r10.getSystemService(r0)     // Catch:{ all -> 0x00e2 }
            android.os.UserManager r1 = (android.os.UserManager) r1     // Catch:{ all -> 0x00e2 }
            android.os.UserHandle r0 = android.os.Process.myUserHandle()     // Catch:{ all -> 0x00e2 }
            long r0 = r1.getSerialNumberForUser(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r2 = " user id: "
            r3.append(r2)     // Catch:{ all -> 0x00e2 }
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r2 = " is system user: "
            r3.append(r2)     // Catch:{ all -> 0x00e2 }
            r4 = 0
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            r0 = 0
            if (r2 != 0) goto L_0x00a7
            r0 = 1
        L_0x00a7:
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
        L_0x00aa:
            java.lang.String r0 = r11.getName()     // Catch:{ FileNotFoundException -> 0x00cb }
            r10.openFileOutput(r0, r6)     // Catch:{ FileNotFoundException -> 0x00cb }
            java.io.File r2 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00cb }
            java.io.File r1 = r10.getFilesDir()     // Catch:{ FileNotFoundException -> 0x00cb }
            java.lang.String r0 = r11.getName()     // Catch:{ FileNotFoundException -> 0x00cb }
            r2.<init>(r1, r0)     // Catch:{ FileNotFoundException -> 0x00cb }
            java.lang.String r0 = " openFileOutput canwrite: "
            r3.append(r0)     // Catch:{ FileNotFoundException -> 0x00cb }
            boolean r0 = r2.canWrite()     // Catch:{ FileNotFoundException -> 0x00cb }
            r3.append(r0)     // Catch:{ FileNotFoundException -> 0x00cb }
            goto L_0x00d8
        L_0x00cb:
            r1 = move-exception
            java.lang.String r0 = " openFileOutput error: "
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r1.getMessage()     // Catch:{ all -> 0x00e2 }
            r3.append(r0)     // Catch:{ all -> 0x00e2 }
        L_0x00d8:
            java.io.FileNotFoundException r1 = new java.io.FileNotFoundException     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x00e2 }
            r1.<init>(r0)     // Catch:{ all -> 0x00e2 }
            throw r1     // Catch:{ all -> 0x00e2 }
        L_0x00e2:
            r0 = move-exception
            goto L_0x00e6
        L_0x00e4:
            r0 = move-exception
            r7 = r2
        L_0x00e6:
            if (r7 == 0) goto L_0x00eb
            r7.close()
        L_0x00eb:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass019.<init>(android.content.Context, java.io.File, int):void");
    }
}
