package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.0s1  reason: invalid class name and case insensitive filesystem */
public final class C13760s1 {
    private static C05540Zi A01;
    public final C13770s2 A00 = new C13770s2();

    public static final C13760s1 A00(AnonymousClass1XY r3) {
        C13760s1 r0;
        synchronized (C13760s1.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r3)) {
                    A01.A01();
                    A01.A00 = new C13760s1();
                }
                C05540Zi r1 = A01;
                r0 = (C13760s1) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }
}
