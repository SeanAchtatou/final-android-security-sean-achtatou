package X;

import com.facebook.messaging.sharing.ShareLauncherActivity;
import java.util.concurrent.CancellationException;

/* renamed from: X.1Dc  reason: invalid class name and case insensitive filesystem */
public final class C20601Dc extends C06020ai {
    public final /* synthetic */ ShareLauncherActivity A00;

    public C20601Dc(ShareLauncherActivity shareLauncherActivity) {
        this.A00 = shareLauncherActivity;
    }

    public void A03(CancellationException cancellationException) {
        ShareLauncherActivity shareLauncherActivity = this.A00;
        shareLauncherActivity.A0N = null;
        shareLauncherActivity.setResult(0);
        this.A00.finish();
    }
}
