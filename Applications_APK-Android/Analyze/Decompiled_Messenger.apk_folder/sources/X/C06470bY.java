package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0bY  reason: invalid class name and case insensitive filesystem */
public final class C06470bY {
    private static volatile C06470bY A01;
    public final AnonymousClass0WP A00;

    public static final C06470bY A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C06470bY.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C06470bY(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C06470bY(AnonymousClass1XY r2) {
        this.A00 = C25091Yh.A00(r2);
    }
}
