package X;

import android.content.BroadcastReceiver;
import android.content.Context;

/* renamed from: X.0KJ  reason: invalid class name */
public abstract class AnonymousClass0KJ extends BroadcastReceiver {
    public abstract boolean A00(Context context);

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002c, code lost:
        if (r1 == 0) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r6, android.content.Intent r7) {
        /*
            r5 = this;
            r0 = -1383463471(0xffffffffad8a05d1, float:-1.5691367E-11)
            int r4 = X.C000700l.A01(r0)
            r7.getAction()
            java.lang.String r1 = r7.getAction()
            java.lang.String r0 = "android.intent.action.MY_PACKAGE_REPLACED"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x004c
            java.lang.String r1 = "appcomponents"
            r0 = 0
            java.io.File r2 = r6.getDir(r1, r0)
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "versions"
            r1.<init>(r2, r0)
            java.lang.String[] r0 = r1.list()
            if (r0 == 0) goto L_0x002e
            int r1 = r0.length
            r0 = 1
            if (r1 != 0) goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            if (r0 != 0) goto L_0x003e
            boolean r0 = r5.A00(r6)
            if (r0 != 0) goto L_0x003e
            r0 = -753553021(0xffffffffd315b183, float:-6.4292828E11)
            X.C000700l.A0D(r7, r0, r4)
            return
        L_0x003e:
            java.lang.String r3 = "com.facebook.appcomponentmanager.ACTION_ENABLE_COMPONENTS"
            java.lang.Class<com.facebook.appcomponentmanager.AppComponentManagerService> r2 = com.facebook.appcomponentmanager.AppComponentManagerService.class
            int r1 = com.facebook.appcomponentmanager.AppComponentManagerService.A00
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r3)
            X.AnonymousClass0E1.enqueueWork(r6, r2, r1, r0)
        L_0x004c:
            r0 = -1079568247(0xffffffffbfa71889, float:-1.3054363)
            X.C000700l.A0D(r7, r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0KJ.onReceive(android.content.Context, android.content.Intent):void");
    }
}
