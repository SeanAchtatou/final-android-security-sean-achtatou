package X;

import android.content.Context;
import com.facebook.base.app.AbstractApplicationLike;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReferenceArray;

/* renamed from: X.1XX  reason: invalid class name */
public abstract class AnonymousClass1XX implements AnonymousClass1XY {
    public static AbstractApplicationLike A00;
    public static final Object A01 = new Object();
    private static final C04290Tm A02 = new C04290Tm(new AnonymousClass1XZ(), 16);

    public static C22916BKm A00(C22916BKm bKm) {
        C22914BKk bKk = new C22914BKk(new C22911BKh(null, List.class, bKm.A01.A02));
        C22922BKw bKw = bKm.A00;
        Annotation Ad5 = bKw.Ad5();
        Class Ad6 = bKw.Ad6();
        if (Ad5 != null) {
            return new C22916BKm(bKk, C22916BKm.A01(Ad5));
        }
        if (Ad6 != null) {
            return new C22916BKm(bKk, C22916BKm.A00(Ad6));
        }
        return new C22916BKm(bKk, C22919BKp.INSTANCE);
    }

    public static C22916BKm A01(C22916BKm bKm) {
        C22914BKk bKk = new C22914BKk(new C22911BKh(null, Set.class, bKm.A01.A02));
        C22922BKw bKw = bKm.A00;
        Annotation Ad5 = bKw.Ad5();
        Class Ad6 = bKw.Ad6();
        if (Ad5 != null) {
            return new C22916BKm(bKk, C22916BKm.A01(Ad5));
        }
        if (Ad6 != null) {
            return new C22916BKm(bKk, C22916BKm.A00(Ad6));
        }
        return new C22916BKm(bKk, C22919BKp.INSTANCE);
    }

    public static Object A02(int i, int i2, AnonymousClass0UN r10) {
        Object obj;
        if (r10 == null) {
            return null;
        }
        byte b = r10.A00;
        C24851Xi r7 = r10.A01;
        AtomicReferenceArray atomicReferenceArray = r10.A02;
        Object obj2 = atomicReferenceArray.get(i);
        if (obj2 == null) {
            AnonymousClass0UO A002 = AnonymousClass0UO.A00();
            byte b2 = A002.A00;
            A002.A00 = (byte) (b | b2);
            Object AYf = r7.AYf();
            try {
                obj2 = AnonymousClass1Y4.A00(i2, r7.getScopeUnawareInjector());
                r7.AZH(AYf);
                A002.A00 = b2;
                if (obj2 == null) {
                    obj = A01;
                } else {
                    obj = obj2;
                }
                if (!atomicReferenceArray.compareAndSet(i, null, obj)) {
                    obj2 = atomicReferenceArray.get(i);
                }
            } catch (RuntimeException e) {
                throw new RuntimeException(AnonymousClass08S.A0B("Failed to lazy inject class with binding ID ", i2, " and localId ", i), e);
            } catch (Throwable th) {
                r7.AZH(AYf);
                A002.A00 = b2;
                throw th;
            }
        }
        if (obj2 == A01) {
            return null;
        }
        return obj2;
    }

    public static Object A03(int i, AnonymousClass0UN r6) {
        if (r6 == null) {
            throw new AnonymousClass37z("A local injection was attempted before the constructor completed or before injectMe was called.");
        }
        C24851Xi r4 = r6.A01;
        Object AYf = r4.AYf();
        try {
            Object A002 = AnonymousClass1Y4.A00(i, r4.getScopeUnawareInjector());
            r4.AZH(AYf);
            return A002;
        } catch (RuntimeException e) {
            throw new RuntimeException(AnonymousClass08S.A09("Failed to local inject class with binding ID ", i), e);
        } catch (Throwable th) {
            r4.AZH(AYf);
            throw th;
        }
    }

    public static AnonymousClass1XX get(Context context) {
        return (AnonymousClass1XX) A02.A00(context);
    }
}
