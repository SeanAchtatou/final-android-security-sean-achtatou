package X;

import android.view.ViewTreeObserver;
import com.facebook.base.app.SplashScreenActivity;

/* renamed from: X.0KT  reason: invalid class name */
public final class AnonymousClass0KT implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ ViewTreeObserver A00;
    public final /* synthetic */ SplashScreenActivity A01;

    public AnonymousClass0KT(SplashScreenActivity splashScreenActivity, ViewTreeObserver viewTreeObserver) {
        this.A01 = splashScreenActivity;
        this.A00 = viewTreeObserver;
    }

    public boolean onPreDraw() {
        new AnonymousClass0KS(this).sendEmptyMessage(0);
        this.A00.removeOnPreDrawListener(this);
        return true;
    }
}
