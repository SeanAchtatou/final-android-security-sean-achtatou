package X;

import android.os.Build;
import android.os.ConditionVariable;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Map;
import java.util.concurrent.Executor;

/* renamed from: X.0Y7  reason: invalid class name */
public final class AnonymousClass0Y7 implements AnonymousClass0Y8 {
    public static final int A0B;
    public static final AnonymousClass0VS A0C = AnonymousClass0VS.A00(0);
    public static final Class A0D = AnonymousClass0Y7.class;
    public long A00;
    public AnonymousClass0UN A01;
    public AnonymousClass0US A02;
    public AnonymousClass0US A03;
    public boolean A04;
    public boolean A05 = false;
    public final ConditionVariable A06 = new ConditionVariable();
    public final AnonymousClass0US A07;
    public final AnonymousClass0US A08;
    public final Map A09 = AnonymousClass0TG.A04();
    public volatile boolean A0A = false;

    static {
        int i;
        if (Build.VERSION.SDK_INT > 17) {
            i = Runtime.getRuntime().availableProcessors();
        } else {
            i = 2;
        }
        A0B = i;
    }

    public static final AnonymousClass0Y7 A00(AnonymousClass1XY r1) {
        return new AnonymousClass0Y7(r1);
    }

    public static void A01(AnonymousClass0Y7 r3) {
        C005505z.A03("HiPri-Completed-Setup", -2107714174);
        try {
            ((AnonymousClass0UW) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ALy, r3.A01)).A02();
        } finally {
            C005505z.A00(-1385956923);
        }
    }

    public static void A02(AnonymousClass0Y7 r4) {
        C005505z.A03("initializeSharedPrefs", 1120252251);
        try {
            if (!r4.A0A) {
                C005505z.A03("HiPri-init-call-shared-prefs", -1161295212);
                r4.A08(new C05450Yz((FbSharedPreferences) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B6q, r4.A01), r4.A07), "INeedInit.HighPriorityInitOnBackgroundThread");
                C005505z.A00(1006887587);
            }
            r4.A0A = true;
            C005505z.A00(1374085759);
        } catch (Throwable th) {
            throw th;
        } finally {
        }
    }

    public static void A03(AnonymousClass0Y7 r5, AnonymousClass0WR r6, boolean z) {
        AnonymousClass0XX CIG = ((AnonymousClass0WP) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A8a, r5.A01)).CIG("FbAppInitializer-LowPriWorkerThread:", new C05120Xs(r5, z, r6), AnonymousClass0XV.APPLICATION_LOADED_HIGH_PRIORITY, AnonymousClass07B.A01);
        int i = AnonymousClass1Y3.A3Z;
        AnonymousClass0UN r3 = r5.A01;
        C05350Yp.A08(CIG, (C04800Wf) AnonymousClass1XX.A02(5, i, r3), (Executor) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BS7, r3));
    }

    public static void A04(AnonymousClass0Y7 r2, C04830Wi r3, String str) {
        C005505z.A03("HiPri-execute-tasks-synch", 224631385);
        int i = 0;
        while (i < r3.BKI()) {
            try {
                AnonymousClass1Z5 BLq = r3.BLq();
                if (BLq != null) {
                    r2.A08(BLq, str);
                }
                i++;
            } finally {
                C005505z.A00(-1302930854);
            }
        }
    }

    public static void A05(AnonymousClass0Y7 r3, AnonymousClass0VL r4) {
        ListenableFuture CIC = r4.CIC(new C14660tl(r3));
        int i = AnonymousClass1Y3.A3Z;
        AnonymousClass0UN r32 = r3.A01;
        C05350Yp.A08(CIC, (C04800Wf) AnonymousClass1XX.A02(5, i, r32), (Executor) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BS7, r32));
    }

    public static void A06(AnonymousClass0Y7 r8, boolean z) {
        long now = ((AnonymousClass069) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BBa, r8.A01)).now() - r8.A00;
        QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BBd, r8.A01);
        quickPerformanceLogger.setAlwaysOnSampleRate(3997697, 5);
        synchronized (r8.A09) {
            quickPerformanceLogger.markerGenerateWithAnnotations(3997697, 2, (int) now, r8.A09);
        }
        A01(r8);
        ((AnonymousClass0YZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AZQ, r8.A01)).BM0();
        if (!z) {
            AnonymousClass0WR r1 = (AnonymousClass0WR) r8.A03.get();
            if (r1 != null) {
                A03(r8, r1, true);
            }
            r8.A03 = null;
        }
        r8.A06.open();
    }

    public static boolean A07(AnonymousClass0Y7 r3) {
        int A072 = ((AnonymousClass0VP) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BDp, r3.A01)).A07();
        if (A072 == -1 || A072 <= 1) {
            return false;
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    public void A08(AnonymousClass1Z5 r9, String str) {
        long now = ((AnonymousClass069) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BBa, this.A01)).now();
        Class<?> cls = r9.getClass();
        C005505z.A03(C05260Yg.A00(cls), 19465520);
        try {
            r9.init();
            C005505z.A00(1199575632);
            boolean z = this.A05;
            if (z && z) {
                ((C04840Wj) AnonymousClass1XX.A02(15, AnonymousClass1Y3.AVm, this.A01)).A02(r9.AkC());
            }
            String simpleName = cls.getSimpleName();
            if (C06850cB.A0B(simpleName)) {
                simpleName = cls.getName();
            }
            synchronized (this.A09) {
                this.A09.put(AnonymousClass08S.A0P(str, ".", simpleName), Long.toString(((AnonymousClass069) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BBa, this.A01)).now() - now));
            }
        } catch (Throwable th) {
            C005505z.A00(-273643386);
            throw th;
        }
    }

    public void BVt(Object obj) {
        A06(this, ((Boolean) obj).booleanValue());
    }

    public AnonymousClass0Y7(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(17, r3);
        this.A02 = AnonymousClass0UQ.A00(AnonymousClass1Y3.BNi, r3);
        this.A08 = AnonymousClass0VB.A00(AnonymousClass1Y3.BEI, r3);
        this.A07 = AnonymousClass0VB.A00(AnonymousClass1Y3.Adq, r3);
        this.A03 = AnonymousClass0UQ.A00(AnonymousClass1Y3.BS2, r3);
    }
}
