package X;

import java.util.Comparator;

/* renamed from: X.1KJ  reason: invalid class name */
public final class AnonymousClass1KJ implements Comparator {
    public int compare(Object obj, Object obj2) {
        C17730zN r3 = (C17730zN) obj;
        C17730zN r4 = (C17730zN) obj2;
        int i = r3.A08.top;
        int i2 = r4.A08.top;
        if (i == i2) {
            return r3.A00 - r4.A00;
        }
        return i - i2;
    }
}
