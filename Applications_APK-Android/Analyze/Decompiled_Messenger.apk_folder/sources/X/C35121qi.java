package X;

import java.util.HashSet;

/* renamed from: X.1qi  reason: invalid class name and case insensitive filesystem */
public final class C35121qi implements C04310Tq {
    public final /* synthetic */ C35091qf A00;

    public C35121qi(C35091qf r1) {
        this.A00 = r1;
    }

    public Object get() {
        HashSet hashSet = new HashSet();
        C35091qf r8 = this.A00;
        C29749Ehc ehc = new C29749Ehc();
        Class<C29767Ehu> cls = C29767Ehu.class;
        ehc.A00.put(cls, new C29765Ehs());
        Class<C29764Ehr> cls2 = C29764Ehr.class;
        ehc.A00.put(cls2, new C29763Ehq());
        Class<C29761Eho> cls3 = C29761Eho.class;
        ehc.A00.put(cls3, new C29760Ehn());
        Class<C29770Ehx> cls4 = C29770Ehx.class;
        ehc.A00.put(cls4, new C29768Ehv());
        Class<C29758Ehl> cls5 = C29758Ehl.class;
        ehc.A00.put(cls5, new C29756Ehj());
        Class<C404421p> cls6 = C404421p.class;
        ehc.A00.put(cls6, new C188538p8());
        C29747Eha eha = new C29747Eha(ehc);
        AnonymousClass4O6 r9 = new AnonymousClass4O6();
        r9.A00.put(cls, new C29766Eht());
        r9.A00.put(cls2, new C29762Ehp());
        r9.A00.put(cls3, new C29759Ehm());
        r9.A00.put(cls4, new C29769Ehw());
        r9.A00.put(cls5, new C29757Ehk());
        r9.A00.put(cls6, new C188548p9());
        hashSet.add(new C29742EhV(eha, new C43022Ct(r9), C35091qf.A00(r8), new C29750Ehd(r8), new C29753Ehg(r8)));
        C35091qf r92 = this.A00;
        C29749Ehc ehc2 = new C29749Ehc();
        ehc2.A00.put(cls, new C29765Ehs());
        ehc2.A00.put(cls2, new C29763Ehq());
        ehc2.A00.put(cls3, new C29760Ehn());
        ehc2.A00.put(cls4, new C29768Ehv());
        ehc2.A00.put(cls5, new C29756Ehj());
        Class<AnonymousClass8NO> cls7 = AnonymousClass8NO.class;
        ehc2.A00.put(cls7, new AnonymousClass8NN());
        Class<C404421p> cls8 = C404421p.class;
        ehc2.A00.put(cls8, new C188538p8());
        C29747Eha eha2 = new C29747Eha(ehc2);
        AnonymousClass4O6 r10 = new AnonymousClass4O6();
        r10.A00.put(cls, new C29766Eht());
        r10.A00.put(cls2, new C29762Ehp());
        r10.A00.put(cls3, new C29759Ehm());
        r10.A00.put(cls4, new C29769Ehw());
        C29757Ehk ehk = new C29757Ehk();
        r10.A00.put(C29758Ehl.class, ehk);
        r10.A00.put(cls7, new C188558pA());
        r10.A00.put(cls8, new C188548p9());
        C43022Ct r82 = new C43022Ct(r10);
        C43022Ct r16 = r82;
        hashSet.add(new C29741EhU(eha2, r16, C35091qf.A00(r92), new C29750Ehd(r92), new C29752Ehf(r92)));
        C35091qf r5 = this.A00;
        C29749Ehc ehc3 = new C29749Ehc();
        ehc3.A00.put(cls, new C29765Ehs());
        ehc3.A00.put(cls2, new C29763Ehq());
        ehc3.A00.put(cls3, new C29760Ehn());
        ehc3.A00.put(cls4, new C29768Ehv());
        C29747Eha eha3 = new C29747Eha(ehc3);
        AnonymousClass4O6 r7 = new AnonymousClass4O6();
        r7.A00.put(cls, new C29766Eht());
        r7.A00.put(cls2, new C29762Ehp());
        r7.A00.put(cls3, new C29759Ehm());
        r7.A00.put(cls4, new C29769Ehw());
        hashSet.add(new C29743EhW(eha3, new C43022Ct(r7), C35091qf.A00(r5), new C29750Ehd(r5), new C29751Ehe(r5)));
        return hashSet;
    }
}
