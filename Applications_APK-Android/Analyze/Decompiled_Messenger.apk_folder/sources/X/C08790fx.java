package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fx  reason: invalid class name and case insensitive filesystem */
public final class C08790fx {
    private static volatile C08790fx A09;
    public AnonymousClass0UN A00;
    private Handler A01;
    private SparseBooleanArray A02;
    public final SparseArray A03;
    public final Object A04 = new Object();
    public final Object A05 = new Object();
    public final WeakHashMap A06;
    private final ConcurrentLinkedQueue A07 = new ConcurrentLinkedQueue();
    private final AtomicBoolean A08 = new AtomicBoolean();

    public void A08(int i) {
        A05(this, i, 2);
    }

    public static long A00(C08790fx r3, int i) {
        return ((C07030cV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A3l, r3.A00)).A00.Aik().Ail(i);
    }

    private SparseBooleanArray A01() {
        SparseBooleanArray sparseBooleanArray;
        synchronized (this.A04) {
            sparseBooleanArray = new SparseBooleanArray();
            int size = this.A03.size();
            for (int i = 0; i < size; i++) {
                sparseBooleanArray.append(this.A03.keyAt(i), true);
            }
        }
        return sparseBooleanArray;
    }

    public static final C08790fx A02(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (C08790fx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new C08790fx(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    private void A03() {
        this.A08.set(false);
        while (((C37191uq) this.A07.poll()) != null) {
            A05(this, 0, 0);
        }
    }

    public static void A04(C08790fx r11, int i) {
        if (((C07030cV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A3l, r11.A00)).A00.Aik().BEI(i)) {
            synchronized (r11.A04) {
                C08980gI r2 = (C08980gI) r11.A03.get(i);
                if (r2 != null) {
                    r2.A00 = ((AnonymousClass069) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBa, r11.A00)).now();
                    Handler handler = r11.A01;
                    handler.sendMessageDelayed(Message.obtain(handler, 1, Integer.valueOf(i)), A00(r11, i));
                } else {
                    C08980gI r8 = new C08980gI(((AnonymousClass069) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBa, r11.A00)).now());
                    AnonymousClass08Z.A01(8, "Critical Path Entered", (int) r8.A01);
                    if (r11.A03.size() == 0) {
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r11.A00)).markerStart(5505191);
                    }
                    r11.A03.put(i, r8);
                    Handler handler2 = r11.A01;
                    handler2.sendMessageDelayed(Message.obtain(handler2, 1, Integer.valueOf(i)), A00(r11, i));
                    r11.A03.size();
                }
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r11.A00)).markerPoint(5505191, AnonymousClass08S.A0M("enter", "_critical_path(", i, ")"), (String) null);
            }
            A06(r11, false);
        }
    }

    public static void A05(C08790fx r11, int i, short s) {
        if (((C07030cV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A3l, r11.A00)).A00.Aik().BEI(i)) {
            int i2 = -1;
            synchronized (r11.A04) {
                C08980gI r0 = (C08980gI) r11.A03.get(i);
                if (r0 != null) {
                    i2 = (int) r0.A01;
                    r11.A03.remove(i);
                    r11.A03.size();
                    int i3 = AnonymousClass1Y3.BBd;
                    AnonymousClass0UN r10 = r11.A00;
                    QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(1, i3, r10);
                    String A0M = AnonymousClass08S.A0M("exit", "_critical_path(", i, ")");
                    StringBuilder sb = new StringBuilder();
                    sb.append((String) null);
                    sb.append(",");
                    sb.append(AnonymousClass08S.A09("action_id:", s));
                    if (((C07030cV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A3l, r10)).A00.Aik().Ast().get(i)) {
                        sb.append(",");
                        sb.append("logging_only:true");
                    }
                    quickPerformanceLogger.markerPoint(5505191, A0M, sb.toString());
                }
                if (s == 216) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r11.A00)).markerDrop(5505191);
                    if (r11.A03.size() != 0) {
                        ((C07380dK) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BQ3, r11.A00)).A03("android.messenger.perf.critical_path.multiple_paths_dropped");
                    }
                } else if (r11.A03.size() == 0) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r11.A00)).markerEnd(5505191, s);
                }
            }
            if (i2 > 0) {
                AnonymousClass08Z.A02(8, "Critical Path Entered", i2);
            }
            A06(r11, false);
        }
    }

    public static void A06(C08790fx r4, boolean z) {
        boolean z2;
        ArrayList<AnonymousClass0g4> arrayList;
        new SparseBooleanArray();
        synchronized (r4.A04) {
            try {
                SparseBooleanArray A012 = r4.A01();
                z2 = false;
                if (A012 != r4.A02) {
                    z2 = true;
                    r4.A02 = A012;
                }
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (z2) {
            synchronized (r4.A05) {
                try {
                    arrayList = new ArrayList<>(r4.A06.keySet());
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
            for (AnonymousClass0g4 A022 : arrayList) {
                AnonymousClass0g4.A02(A022, z);
            }
            return;
        }
        return;
        throw th;
    }

    private C08790fx(AnonymousClass1XY r6) {
        this.A00 = new AnonymousClass0UN(4, r6);
        this.A03 = new SparseArray();
        this.A06 = new WeakHashMap();
        for (AnonymousClass0g4 r3 : ((C07030cV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A3l, this.A00)).A00.Aim().Ain()) {
            synchronized (this.A05) {
                this.A06.put(r3, true);
            }
            A01();
            AnonymousClass0g4.A02(r3, false);
        }
        this.A01 = new Handler(Looper.getMainLooper(), new C26981cS(this));
    }

    public String A07() {
        A03();
        StringBuilder sb = new StringBuilder("[");
        synchronized (this.A04) {
            int size = this.A03.size();
            for (int i = 0; i < size; i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(this.A03.keyAt(i));
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public boolean A09() {
        boolean z;
        A03();
        synchronized (this.A04) {
            z = false;
            if (this.A03.size() > 0) {
                z = true;
            }
        }
        return z;
    }

    public boolean A0A(int i) {
        boolean z;
        A03();
        synchronized (this.A04) {
            z = false;
            if (this.A03.get(i) != null) {
                z = true;
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005a, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0B(X.AnonymousClass1GL r8) {
        /*
            r7 = this;
            r7.A03()
            java.lang.Object r5 = r7.A04
            monitor-enter(r5)
            android.util.SparseArray r0 = r7.A03     // Catch:{ all -> 0x005b }
            int r6 = r0.size()     // Catch:{ all -> 0x005b }
            int r2 = X.AnonymousClass1Y3.A3l     // Catch:{ all -> 0x005b }
            X.0UN r1 = r7.A00     // Catch:{ all -> 0x005b }
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x005b }
            X.0cV r0 = (X.C07030cV) r0     // Catch:{ all -> 0x005b }
            X.0kK r0 = r0.A00     // Catch:{ all -> 0x005b }
            X.0gn r0 = r0.Aik()     // Catch:{ all -> 0x005b }
            android.util.SparseBooleanArray r4 = r0.Ast()     // Catch:{ all -> 0x005b }
            r3 = 0
            r2 = 0
        L_0x0023:
            int r0 = r4.size()     // Catch:{ all -> 0x005b }
            if (r3 >= r0) goto L_0x003a
            android.util.SparseArray r1 = r7.A03     // Catch:{ all -> 0x005b }
            int r0 = r4.keyAt(r3)     // Catch:{ all -> 0x005b }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0037
            int r2 = r2 + 1
        L_0x0037:
            int r3 = r3 + 1
            goto L_0x0023
        L_0x003a:
            int r6 = r6 - r2
            r4 = 1
            if (r6 == 0) goto L_0x0059
            android.util.SparseArray r0 = r7.A03     // Catch:{ all -> 0x005b }
            int r3 = r0.size()     // Catch:{ all -> 0x005b }
            r2 = 0
            r1 = 0
        L_0x0046:
            if (r1 >= r3) goto L_0x0057
            android.util.SparseArray r0 = r7.A03     // Catch:{ all -> 0x005b }
            int r0 = r0.keyAt(r1)     // Catch:{ all -> 0x005b }
            boolean r0 = r8.BEJ(r0)     // Catch:{ all -> 0x005b }
            if (r0 != 0) goto L_0x0059
            int r1 = r1 + 1
            goto L_0x0046
        L_0x0057:
            monitor-exit(r5)     // Catch:{ all -> 0x005b }
            return r2
        L_0x0059:
            monitor-exit(r5)     // Catch:{ all -> 0x005b }
            return r4
        L_0x005b:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x005b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08790fx.A0B(X.1GL):boolean");
    }
}
