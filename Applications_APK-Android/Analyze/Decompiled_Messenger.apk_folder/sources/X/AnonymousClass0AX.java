package X;

import android.os.Handler;
import com.facebook.rti.common.time.RealtimeSinceBootClock;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0AX  reason: invalid class name */
public class AnonymousClass0AX {
    public Handler A00;
    public C01400Ab A01;
    public AnonymousClass07g A02;
    public AnonymousClass0AW A03;
    public RealtimeSinceBootClock A04;
    public AnonymousClass0AS A05;
    public C01600Aw A06;
    public C009207y A07;
    public C01460Ai A08;
    public AnonymousClass0BA A09;
    public AnonymousClass0B6 A0A;
    public C01440Ag A0B;
    public AnonymousClass0B7 A0C;
    public C008007h A0D;
    public C01520Ao A0E;
    public C01530Ap A0F;
    public C01590Av A0G;
    public C01570At A0H;
    public C01470Aj A0I;
    public AnonymousClass0BT A0J;
    public AnonymousClass0BO A0K;
    public AnonymousClass0BW A0L;
    public C01710Bi A0M;
    public AnonymousClass0AH A0N;
    public AnonymousClass0BI A0O;
    public AnonymousClass0BN A0P;
    public AnonymousClass0B1 A0Q;
    public C01610Ax A0R;
    public AnonymousClass0BL A0S;
    public AtomicInteger A0T;
    public boolean A0U = false;
    public C01420Ad A0V;

    private static ThreadPoolExecutor A00(int i, int i2, int i3, String str) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(i, i2, (long) i3, TimeUnit.SECONDS, new LinkedBlockingQueue(), new AnonymousClass0FF(str));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0048, code lost:
        if ((!r0.A06.A02) != false) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.C01410Ac r38, java.util.List r39) {
        /*
            r37 = this;
            r0 = r37
            r1 = r38
            android.content.Context r4 = r1.A05
            X.0Ad r5 = new X.0Ad
            android.content.Context r3 = r4.getApplicationContext()
            X.09P r2 = r1.A07
            r5.<init>(r3, r2)
            r0.A0V = r5
            java.lang.Integer r2 = r1.A0Q
            java.lang.String r14 = X.C01430Ae.A00(r2)
            X.09P r3 = r1.A07
            if (r3 != 0) goto L_0x0092
            X.07y r2 = X.C009207y.A01
        L_0x001f:
            r0.A07 = r2
            X.0Aw r2 = X.C01600Aw.A00(r4)
            r0.A06 = r2
            java.lang.String r2 = r4.getPackageName()
            boolean r30 = X.AnonymousClass0A9.A01(r2)
            X.0Aw r2 = r0.A06
            boolean r2 = r2.A02
            r2 = r2 ^ 1
            X.C01700Bh.A00 = r2
            X.0AW r8 = r1.A0B
            r0.A03 = r8
            X.0Ag r7 = new X.0Ag
            r6 = 1
            r5 = 0
            if (r30 == 0) goto L_0x004a
            X.0Aw r2 = r0.A06
            boolean r2 = r2.A02
            r3 = r2 ^ 1
            r2 = 1
            if (r3 == 0) goto L_0x004b
        L_0x004a:
            r2 = 0
        L_0x004b:
            r7.<init>(r4, r14, r2, r8)
            r0.A0B = r7
            X.0AS r2 = X.AnonymousClass0AS.A00
            r0.A05 = r2
            com.facebook.rti.common.time.RealtimeSinceBootClock r2 = com.facebook.rti.common.time.RealtimeSinceBootClock.INSTANCE
            r0.A04 = r2
            X.0Ai r8 = new X.0Ai
            X.0Aw r7 = r0.A06
            X.0AU r2 = r1.A0M
            java.lang.String r11 = r2.AdI()
            java.util.Map r3 = r1.A0V
            java.lang.String r2 = r1.A0T
            r9 = r4
            r10 = r7
            r12 = r3
            r13 = r2
            r8.<init>(r9, r10, r11, r12, r13)
            r0.A08 = r8
            X.0Aj r7 = new X.0Aj
            X.0Ad r3 = r0.A0V
            android.os.Handler r2 = r1.A06
            r7.<init>(r4, r3, r2)
            r0.A0I = r7
            boolean r2 = r1.A0j
            if (r2 == 0) goto L_0x008f
            X.0Al r18 = new X.0Al
            X.0Ad r3 = r0.A0V
            r2 = r18
            r2.<init>(r3)
        L_0x0087:
            int r3 = r1.A04
            if (r3 < 0) goto L_0x009f
            java.lang.Class<X.0CX> r2 = X.AnonymousClass0CX.class
            monitor-enter(r2)
            goto L_0x0098
        L_0x008f:
            X.0Al r18 = X.C01490Al.A01
            goto L_0x0087
        L_0x0092:
            X.07y r2 = new X.07y
            r2.<init>(r3)
            goto L_0x001f
        L_0x0098:
            X.AnonymousClass0CX.A00 = r3     // Catch:{ all -> 0x009b }
            goto L_0x009e
        L_0x009b:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x009e:
            monitor-exit(r2)
        L_0x009f:
            boolean r2 = r1.A0d
            if (r2 == 0) goto L_0x016f
            int r3 = r1.A03
            if (r3 <= 0) goto L_0x016f
            java.lang.String r2 = "writeInMqttClient"
            java.util.concurrent.ThreadPoolExecutor r36 = A00(r5, r6, r3, r2)
        L_0x00ad:
            boolean r2 = r1.A0c
            if (r2 == 0) goto L_0x0169
            int r3 = r1.A03
            if (r3 <= 0) goto L_0x0169
            java.lang.String r2 = "mqttAddressResolver"
            java.util.concurrent.ThreadPoolExecutor r9 = A00(r5, r6, r3, r2)
        L_0x00bb:
            int r5 = r1.A02
            r7 = 2
            if (r5 < 0) goto L_0x0163
            int r3 = r1.A03
            if (r3 <= 0) goto L_0x0163
            java.lang.String r2 = "mqttSsl"
            java.util.concurrent.ThreadPoolExecutor r8 = A00(r5, r7, r3, r2)
        L_0x00ca:
            int r6 = r1.A01
            if (r6 < 0) goto L_0x015e
            int r5 = r1.A03
            if (r5 <= 0) goto L_0x015e
            java.lang.String r3 = "HEInMqttClient"
            java.util.concurrent.ScheduledThreadPoolExecutor r7 = new java.util.concurrent.ScheduledThreadPoolExecutor
            X.0FF r2 = new X.0FF
            r2.<init>(r3)
            r7.<init>(r6, r2)
            long r2 = (long) r5
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS
            r7.setKeepAliveTime(r2, r5)
            r2 = 1
            r7.allowCoreThreadTimeOut(r2)
        L_0x00e8:
            android.os.Handler r3 = r1.A06
            r0.A00 = r3
            X.0Am r2 = new X.0Am
            r2.<init>(r3)
            r0.A0E = r2
            X.0Ap r12 = new X.0Ap
            X.0Ad r13 = r0.A0V
            android.os.Handler r3 = r0.A00
            X.07y r2 = r0.A07
            r15 = r4
            r16 = r3
            r17 = r2
            r12.<init>(r13, r14, r15, r16, r17)
            r0.A0F = r12
            X.0At r6 = new X.0At
            X.0Ad r5 = r0.A0V
            android.content.Context r3 = r4.getApplicationContext()
            android.os.Handler r2 = r1.A06
            r6.<init>(r5, r3, r2)
            r0.A0H = r6
            X.0Av r2 = new X.0Av
            r2.<init>(r4)
            r0.A0G = r2
            X.07h r10 = r1.A0H
            r0.A0D = r10
            X.0Ax r6 = new X.0Ax
            java.lang.Integer r5 = r1.A0Q
            X.09P r3 = r1.A07
            X.0AW r2 = r1.A0B
            r20 = r4
            r21 = r10
            r22 = r5
            r23 = r3
            r24 = r2
            r19 = r6
            r19.<init>(r20, r21, r22, r23, r24)
            r0.A0R = r6
            X.0Bi r2 = new X.0Bi
            r2.<init>(r4)
            r0.A0M = r2
            X.0B1 r10 = new X.0B1
            X.07h r6 = r0.A0D
            java.lang.Integer r5 = r1.A0Q
            X.09P r3 = r1.A07
            X.0AW r2 = r1.A0B
            r21 = r6
            r22 = r5
            r23 = r3
            r24 = r2
            r19 = r10
            r19.<init>(r20, r21, r22, r23, r24)
            r0.A0Q = r10
            X.07h r5 = r0.A0D
            X.0Ax r3 = r0.A0R
            monitor-enter(r5)
            goto L_0x0175
        L_0x015e:
            java.util.concurrent.ScheduledExecutorService r7 = java.util.concurrent.Executors.newScheduledThreadPool(r7)
            goto L_0x00e8
        L_0x0163:
            java.util.concurrent.ExecutorService r8 = java.util.concurrent.Executors.newFixedThreadPool(r7)
            goto L_0x00ca
        L_0x0169:
            java.util.concurrent.ExecutorService r9 = java.util.concurrent.Executors.newSingleThreadExecutor()
            goto L_0x00bb
        L_0x016f:
            java.util.concurrent.ExecutorService r36 = java.util.concurrent.Executors.newSingleThreadExecutor()
            goto L_0x00ad
        L_0x0175:
            java.util.List r2 = r5.A00     // Catch:{ all -> 0x042f }
            r2.add(r3)     // Catch:{ all -> 0x042f }
            monitor-exit(r5)
            X.07h r5 = r0.A0D
            X.0Bi r3 = r0.A0M
            monitor-enter(r5)
            java.util.List r2 = r5.A00     // Catch:{ all -> 0x042c }
            r2.add(r3)     // Catch:{ all -> 0x042c }
            monitor-exit(r5)
            X.07h r2 = r0.A0D
            r2.A04()
            X.0Ab r11 = r1.A08
            r0.A01 = r11
            X.07g r10 = r1.A0A
            r0.A02 = r10
            X.0B6 r6 = new X.0B6
            java.lang.String r5 = r1.A0S
            X.0At r3 = r0.A0H
            X.0Av r2 = r0.A0G
            r21 = r14
            r22 = r5
            r23 = r3
            r24 = r2
            r25 = r11
            r26 = r10
            r19 = r6
            r19.<init>(r20, r21, r22, r23, r24, r25, r26)
            r0.A0A = r6
            X.0B7 r13 = new X.0B7
            X.0Ad r15 = r0.A0V
            X.0At r12 = r0.A0H
            X.0Aj r11 = r0.A0I
            com.facebook.rti.common.time.RealtimeSinceBootClock r10 = r0.A04
            X.0AS r6 = r0.A05
            X.0Bg r5 = r1.A0E
            boolean r3 = r1.A0f
            X.0AW r2 = r0.A03
            r19 = r13
            r21 = r15
            r22 = r14
            r23 = r12
            r24 = r11
            r25 = r10
            r26 = r6
            r27 = r5
            r28 = r3
            r29 = r2
            r19.<init>(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29)
            r0.A0C = r13
            X.0AZ r5 = r1.A0N
            X.0At r3 = r0.A0H
            X.0AS r2 = r0.A05
            X.0B6 r6 = r0.A0A
            r5.A00 = r2
            r5.A01 = r6
            r5.A02 = r3
            X.0BA r5 = new X.0BA
            X.0Bg r2 = r1.A0D
            java.lang.Object r2 = r2.get()
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r3 = r2.booleanValue()
            X.0AW r2 = r0.A03
            r5.<init>(r6, r3, r2)
            r0.A09 = r5
            java.util.concurrent.atomic.AtomicInteger r3 = new java.util.concurrent.atomic.AtomicInteger
            X.07h r2 = r0.A0D
            X.07i r2 = r2.A03()
            int r2 = r2.A06
            r3.<init>(r2)
            r0.A0T = r3
            X.0BC r3 = new X.0BC
            X.0BB r2 = new X.0BB
            r2.<init>()
            r3.<init>(r2)
            X.0BD r10 = new X.0BD
            r10.<init>(r8, r3)
            X.0BF r6 = new X.0BF
            r6.<init>()
            X.0AW r5 = r0.A03
            r2 = 0
            if (r2 == 0) goto L_0x0381
            X.0PR r3 = new X.0PR
            r3.<init>(r9, r6, r5)
        L_0x0229:
            X.0BI r2 = new X.0BI
            r34 = r4
            r27 = r2
            r28 = r0
            r29 = r1
            r31 = r10
            r32 = r7
            r33 = r3
            r35 = r18
            r27.<init>(r28, r29, r30, r31, r32, r33, r34, r35, r36)
            r0.A0O = r2
            X.07h r2 = r0.A0D
            X.07i r2 = r2.A03()
            int r6 = r2.A0A
            X.07h r2 = r0.A0D
            X.07i r2 = r2.A03()
            int r2 = r2.A09
            if (r6 <= 0) goto L_0x037a
            if (r2 <= 0) goto L_0x037a
            X.0BJ r11 = new X.0BJ
            com.facebook.rti.common.time.RealtimeSinceBootClock r5 = r0.A04
            long r2 = (long) r2
            r11.<init>(r5, r6, r2)
        L_0x025c:
            X.0BL r5 = new X.0BL
            X.0Bg r6 = r1.A0E
            if (r6 != 0) goto L_0x0267
            X.0PY r6 = new X.0PY
            r6.<init>(r0)
        L_0x0267:
            X.0Ao r7 = r0.A0E
            X.0Ap r8 = r0.A0F
            android.os.Handler r9 = r0.A00
            X.07h r10 = r0.A0D
            X.07g r12 = r0.A02
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            r0.A0S = r5
            X.0BN r7 = new X.0BN
            X.0Ao r6 = r0.A0E
            X.0Ap r5 = r0.A0F
            X.0B6 r3 = r0.A0A
            X.0B7 r2 = r0.A0C
            r7.<init>(r6, r5, r3, r2)
            r0.A0P = r7
            X.0BO r7 = new X.0BO
            X.0Ad r6 = r0.A0V
            java.util.concurrent.atomic.AtomicInteger r5 = r0.A0T
            android.os.Handler r3 = r0.A00
            X.07y r2 = r0.A07
            r8 = r4
            r9 = r6
            r10 = r14
            r11 = r5
            r12 = r3
            r13 = r2
            r7.<init>(r8, r9, r10, r11, r12, r13)
            r0.A0K = r7
            X.0BT r2 = new X.0BT
            r2.<init>()
            r0.A0J = r2
            java.util.Set r5 = r2.A00
            X.0BV r3 = new X.0BV
            X.0At r2 = r0.A0H
            r3.<init>(r2)
            r5.add(r3)
            X.0BW r7 = new X.0BW
            X.0Ad r6 = r0.A0V
            android.os.Handler r5 = r0.A00
            X.0AV r3 = r1.A0J
            X.07y r2 = r0.A07
            r9 = r6
            r11 = r5
            r12 = r3
            r13 = r2
            r7.<init>(r8, r9, r10, r11, r12, r13)
            r0.A0L = r7
            X.0AH r2 = r1.A0L
            r0.A0N = r2
            X.0Ad r3 = r0.A0V
            r30 = r3
            X.0AG r3 = r1.A0K
            r29 = r3
            X.0AT r3 = r1.A0O
            r28 = r3
            java.lang.String r17 = r4.getPackageName()
            X.0BI r3 = r0.A0O
            r27 = r3
            X.0BL r3 = r0.A0S
            r16 = r3
            X.0AQ r3 = r1.A0I
            r26 = r3
            X.0AU r3 = r1.A0M
            r25 = r3
            X.0BN r3 = r0.A0P
            r24 = r3
            X.0BO r3 = r0.A0K
            r23 = r3
            X.0BW r3 = r0.A0L
            r22 = r3
            java.util.concurrent.atomic.AtomicInteger r3 = r0.A0T
            r21 = r3
            X.0B6 r3 = r0.A0A
            r20 = r3
            X.07g r13 = r0.A02
            X.0B7 r12 = r0.A0C
            android.os.Handler r11 = r0.A00
            X.07h r10 = r0.A0D
            X.0AV r9 = r1.A0J
            X.0AY r8 = r1.A0P
            X.0At r7 = r0.A0H
            X.0Aj r6 = r0.A0I
            X.0Ax r5 = r0.A0R
            X.0B1 r14 = r0.A0Q
            java.lang.String r15 = "MQTT_Subscription"
            java.util.concurrent.ExecutorService r0 = java.util.concurrent.Executors.newCachedThreadPool()
            X.0BY r3 = new X.0BY
            r3.<init>(r0)
            r3.A00 = r15
            X.0BZ r0 = new X.0BZ
            r0.<init>(r3)
            boolean r3 = r1.A0f
            r19 = r3
            boolean r3 = r1.A0e
            boolean r1 = r1.A0i
            r15 = r30
            r2.A08 = r15
            r2.A04 = r4
            r4 = r29
            r2.A0I = r4
            r4 = r28
            r2.A0p = r4
            r4 = r17
            r2.A0Q = r4
            r4 = r27
            r2.A0J = r4
            r4 = r16
            r2.A0P = r4
            r4 = r25
            r2.A0K = r4
            r4 = r26
            r2.A0E = r4
            r4 = r24
            r2.A0L = r4
            r4 = r23
            r2.A0F = r4
            r4 = r22
            r2.A0H = r4
            r4 = r21
            r2.A0U = r4
            r4 = r20
            r2.A09 = r4
            r2.A06 = r13
            r2.A0A = r12
            r2.A05 = r11
            r2.A0B = r10
            r2.A0G = r9
            r2.A0O = r8
            r2.A0C = r7
            r4 = r18
            r2.A07 = r4
            r2.A0D = r6
            r2.A0N = r5
            r2.A0M = r14
            r2.A0S = r0
            java.lang.Runnable r5 = r2.A0h
            monitor-enter(r16)
            goto L_0x0388
        L_0x037a:
            X.0QB r11 = new X.0QB
            r11.<init>()
            goto L_0x025c
        L_0x0381:
            X.0Bj r3 = new X.0Bj
            r3.<init>(r9, r5)
            goto L_0x0229
        L_0x0388:
            r0 = r16
            java.lang.Runnable r4 = r0.A05     // Catch:{ all -> 0x0429 }
            r0 = 0
            if (r4 != 0) goto L_0x0390
            r0 = 1
        L_0x0390:
            X.AnonymousClass0A1.A02(r0)     // Catch:{ all -> 0x0429 }
            r0 = r16
            r0.A05 = r5     // Catch:{ all -> 0x0429 }
            monitor-exit(r16)
            X.0BO r0 = r2.A0F
            java.lang.Runnable r5 = r2.A0f
            monitor-enter(r0)
            java.lang.Runnable r4 = r0.A0L     // Catch:{ all -> 0x0426 }
            if (r4 != 0) goto L_0x03da
            r0.A0L = r5     // Catch:{ all -> 0x0426 }
            X.07y r5 = r0.A0E     // Catch:{ all -> 0x0426 }
            android.content.Context r6 = r0.A0C     // Catch:{ all -> 0x0426 }
            android.content.BroadcastReceiver r7 = r0.A0A     // Catch:{ all -> 0x0426 }
            android.content.IntentFilter r8 = new android.content.IntentFilter     // Catch:{ all -> 0x0426 }
            java.lang.String r4 = r0.A0G     // Catch:{ all -> 0x0426 }
            r8.<init>(r4)     // Catch:{ all -> 0x0426 }
            r9 = 0
            android.os.Handler r10 = r0.A0D     // Catch:{ all -> 0x0426 }
            r5.A08(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0426 }
            X.07y r5 = r0.A0E     // Catch:{ all -> 0x0426 }
            android.content.Context r6 = r0.A0C     // Catch:{ all -> 0x0426 }
            android.content.BroadcastReceiver r7 = r0.A0B     // Catch:{ all -> 0x0426 }
            android.content.IntentFilter r8 = new android.content.IntentFilter     // Catch:{ all -> 0x0426 }
            java.lang.String r4 = r0.A0H     // Catch:{ all -> 0x0426 }
            r8.<init>(r4)     // Catch:{ all -> 0x0426 }
            android.os.Handler r10 = r0.A0D     // Catch:{ all -> 0x0426 }
            r5.A08(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0426 }
            X.07y r5 = r0.A0E     // Catch:{ all -> 0x0426 }
            android.content.Context r6 = r0.A0C     // Catch:{ all -> 0x0426 }
            android.content.BroadcastReceiver r7 = r0.A09     // Catch:{ all -> 0x0426 }
            android.content.IntentFilter r8 = new android.content.IntentFilter     // Catch:{ all -> 0x0426 }
            java.lang.String r4 = r0.A0F     // Catch:{ all -> 0x0426 }
            r8.<init>(r4)     // Catch:{ all -> 0x0426 }
            android.os.Handler r10 = r0.A0D     // Catch:{ all -> 0x0426 }
            r5.A08(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0426 }
        L_0x03da:
            monitor-exit(r0)
            X.0BW r5 = r2.A0H
            java.lang.Runnable r4 = r2.A0g
            monitor-enter(r5)
            java.lang.Runnable r0 = r5.A0C     // Catch:{ all -> 0x0423 }
            if (r0 != 0) goto L_0x03e6
            r5.A0C = r4     // Catch:{ all -> 0x0423 }
        L_0x03e6:
            monitor-exit(r5)
            java.util.Iterator r6 = r39.iterator()
        L_0x03eb:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x03ff
            java.lang.Object r5 = r6.next()
            com.facebook.rti.mqtt.protocol.messages.SubscribeTopic r5 = (com.facebook.rti.mqtt.protocol.messages.SubscribeTopic) r5
            java.util.Map r4 = r2.A0R
            java.lang.String r0 = r5.A01
            r4.put(r0, r5)
            goto L_0x03eb
        L_0x03ff:
            X.07h r0 = r2.A0B
            r0.A04()
            X.07h r0 = r2.A0B
            X.07i r0 = r0.A03()
            r2.A0m = r0
            r0 = r19
            r2.A0Y = r0
            r2.A0W = r3
            r2.A0X = r1
            X.07i r0 = r2.A0m
            boolean r0 = r0.A0T
            if (r0 == 0) goto L_0x0422
            r1 = 1
            r2.A0X = r1
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.A0T
            r0.set(r1)
        L_0x0422:
            return
        L_0x0423:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0426:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0429:
            r0 = move-exception
            monitor-exit(r16)
            throw r0
        L_0x042c:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x042f:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AX.A01(X.0Ac, java.util.List):void");
    }
}
