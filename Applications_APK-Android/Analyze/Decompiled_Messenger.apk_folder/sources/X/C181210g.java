package X;

import com.facebook.proxygen.RootCACallbacks;

/* renamed from: X.10g  reason: invalid class name and case insensitive filesystem */
public final class C181210g implements RootCACallbacks {
    public final /* synthetic */ AnonymousClass09P A00;
    public final /* synthetic */ C17910zg A01;

    public C181210g(C17910zg r1, AnonymousClass09P r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public byte[][] getSystemRootCAs() {
        byte[][] A012 = this.A01.A01();
        if (A012.length == 0) {
            this.A00.CGS(C10710kj.A00.getSimpleName(), "Could not get system root CAs from device.");
        }
        return A012;
    }
}
