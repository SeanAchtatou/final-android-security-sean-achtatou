package X;

/* renamed from: X.0HH  reason: invalid class name */
public final class AnonymousClass0HH {
    private static String A02(int i) {
        if (i == 1) {
            return "Activity";
        }
        if (i == 2) {
            return "Receiver";
        }
        if (i == 4) {
            return "Service";
        }
        if (i != 8) {
            return AnonymousClass08S.A0A("Unknown (type = ", i, ")");
        }
        return "Provider";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0033, code lost:
        if (A01(r6, r7, r8, false) == null) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.content.pm.ComponentInfo A01(android.content.pm.PackageManager r6, android.content.ComponentName r7, int r8, boolean r9) {
        /*
            r3 = 33280(0x8200, float:4.6635E-41)
            if (r9 == 0) goto L_0x0008
            r3 = 33408(0x8280, float:4.6815E-41)
        L_0x0008:
            r2 = 4
            r1 = 2
            r0 = 1
            r5 = 0
            if (r8 == r0) goto L_0x0026
            if (r8 == r1) goto L_0x0021
            if (r8 == r2) goto L_0x001c
            r0 = 8
            if (r8 != r0) goto L_0x001b
            android.content.pm.ProviderInfo r5 = r6.getProviderInfo(r7, r3)     // Catch:{ NameNotFoundException -> 0x002b }
            return r5
        L_0x001b:
            return r5
        L_0x001c:
            android.content.pm.ServiceInfo r5 = r6.getServiceInfo(r7, r3)     // Catch:{ NameNotFoundException -> 0x002b }
            return r5
        L_0x0021:
            android.content.pm.ActivityInfo r5 = r6.getReceiverInfo(r7, r3)     // Catch:{ NameNotFoundException -> 0x002b }
            return r5
        L_0x0026:
            android.content.pm.ActivityInfo r5 = r6.getActivityInfo(r7, r3)     // Catch:{ NameNotFoundException -> 0x002b }
            return r5
        L_0x002b:
            r0 = 0
            if (r9 == 0) goto L_0x0035
            android.content.pm.ComponentInfo r1 = A01(r6, r7, r8, r0)     // Catch:{ Exception -> 0x0035 }
            r0 = 1
            if (r1 != 0) goto L_0x0036
        L_0x0035:
            r0 = 0
        L_0x0036:
            java.lang.String r4 = "AppComponentManager"
            java.lang.String r3 = r7.getClassName()
            java.lang.String r2 = A02(r8)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r9)
            if (r9 == 0) goto L_0x0054
            if (r0 == 0) goto L_0x0054
            java.lang.String r0 = ", BUT succeeded without asking for metadata."
        L_0x004a:
            java.lang.Object[] r1 = new java.lang.Object[]{r3, r2, r1, r0}
            java.lang.String r0 = "getComponentInfo couldn't find component name[%s] type[%s] getMetaData[%s]%s"
            X.C010708t.A0O(r4, r0, r1)
            return r5
        L_0x0054:
            java.lang.String r0 = "."
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0HH.A01(android.content.pm.PackageManager, android.content.ComponentName, int, boolean):android.content.pm.ComponentInfo");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:19|20|21|22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0075, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0079 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x007a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(android.content.Context r10, int r11, java.lang.String r12) {
        /*
            android.content.pm.PackageManager r8 = r10.getPackageManager()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r11)
            r10.getPackageName()
            long r5 = java.lang.System.currentTimeMillis()
            r4 = 4
            r0 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            r0 = 2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            r0 = 8
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)
            java.lang.Integer[] r0 = new java.lang.Integer[]{r3, r2, r1, r0}
            int r0 = A00(r10, r8, r11, r4, r0)
            long r3 = java.lang.System.currentTimeMillis()
            long r3 = r3 - r5
            r5 = 1000(0x3e8, double:4.94E-321)
            if (r0 < 0) goto L_0x012c
            com.facebook.appcomponentmanager.AppComponentManagerProfiledRun r6 = new com.facebook.appcomponentmanager.AppComponentManagerProfiledRun
            r6.<init>(r12, r0, r3)
            java.lang.String r1 = "appcomponents"
            r0 = 0
            java.io.File r2 = r10.getDir(r1, r0)
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "versions"
            r1.<init>(r2, r0)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.io.File[] r9 = r1.listFiles()
            if (r9 == 0) goto L_0x009e
            int r8 = r9.length
            r7 = 0
        L_0x0055:
            if (r7 >= r8) goto L_0x009e
            r4 = r9[r7]
            boolean r0 = A04(r10)     // Catch:{ Exception -> 0x0083 }
            if (r0 == 0) goto L_0x0093
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x007a }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x007a }
            r0.<init>(r4)     // Catch:{ Exception -> 0x007a }
            r1.<init>(r0)     // Catch:{ Exception -> 0x007a }
            java.lang.Object r0 = r1.readObject()     // Catch:{ all -> 0x0073 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x0073 }
            r1.close()     // Catch:{ Exception -> 0x007a }
            goto L_0x007f
        L_0x0073:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0075 }
        L_0x0075:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0079 }
        L_0x0079:
            throw r0     // Catch:{ Exception -> 0x007a }
        L_0x007a:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0083 }
            r0.<init>()     // Catch:{ Exception -> 0x0083 }
        L_0x007f:
            r5.addAll(r0)     // Catch:{ Exception -> 0x0083 }
            goto L_0x0093
        L_0x0083:
            r3 = move-exception
            java.lang.String r2 = "AppComponentManager"
            java.lang.String r1 = "Error reading entries from stale logfile: "
            java.lang.String r0 = r4.getName()     // Catch:{ all -> 0x0099 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0099 }
            X.C010708t.A0R(r2, r3, r0)     // Catch:{ all -> 0x0099 }
        L_0x0093:
            r4.delete()
            int r7 = r7 + 1
            goto L_0x0055
        L_0x0099:
            r0 = move-exception
            r4.delete()
            throw r0
        L_0x009e:
            r5.add(r6)
            int r3 = com.facebook.common.build.BuildConstants.A00()
            java.io.File r4 = new java.io.File
            java.lang.String r1 = "appcomponents"
            r0 = 0
            java.io.File r2 = r10.getDir(r1, r0)
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "versions"
            r1.<init>(r2, r0)
            int r0 = com.facebook.common.build.BuildConstants.A00()
            java.lang.String r0 = java.lang.Integer.toString(r0)
            r4.<init>(r1, r0)
            java.io.File r0 = r4.getParentFile()
            r0.mkdirs()
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0117 }
            r0.<init>(r4)     // Catch:{ IOException -> 0x0117 }
            r0.close()     // Catch:{ IOException -> 0x0117 }
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x00f5
            boolean r0 = A04(r10)
            if (r0 == 0) goto L_0x00f5
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00ed }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00ed }
            r0 = 0
            r1.<init>(r4, r0)     // Catch:{ Exception -> 0x00ed }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00ed }
            r2.writeObject(r5)     // Catch:{ Exception -> 0x00ed }
            r2.close()     // Catch:{ Exception -> 0x00ed }
            goto L_0x00f5
        L_0x00ed:
            r2 = move-exception
            java.lang.String r1 = "AppComponentManager"
            java.lang.String r0 = "Error writing entries to logfile."
            X.C010708t.A0R(r1, r2, r0)
        L_0x00f5:
            r0 = 1
            if (r3 != r0) goto L_0x0113
            android.content.pm.PackageManager r2 = r10.getPackageManager()
            java.lang.String r1 = r10.getPackageName()     // Catch:{ NameNotFoundException -> 0x0108 }
            r0 = 0
            android.content.pm.PackageInfo r0 = r2.getPackageInfo(r1, r0)     // Catch:{ NameNotFoundException -> 0x0108 }
            long r0 = r0.lastUpdateTime     // Catch:{ NameNotFoundException -> 0x0108 }
            goto L_0x0110
        L_0x0108:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Can't get package info for this package."
            r1.<init>(r0)
            throw r1
        L_0x0110:
            r4.setLastModified(r0)
        L_0x0113:
            r10.getPackageName()
            return
        L_0x0117:
            r3 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Failed to touch file: "
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0, r3)
            throw r2
        L_0x012c:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r1 = r10.getPackageName()
            long r3 = r3 / r5
            java.lang.Long r0 = java.lang.Long.valueOf(r3)
            java.lang.Object[] r1 = new java.lang.Object[]{r7, r1, r0}
            java.lang.String r0 = "Failed to set enable stage %d for pkg[%s], can't resume. Duration[%s]"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0HH.A03(android.content.Context, int, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        if (r4.equals("com.facebook.wakizashi") == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if (r4.equals("com.facebook.katana") == false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean A04(android.content.Context r4) {
        /*
            java.lang.String r4 = r4.getPackageName()
            int r1 = r4.hashCode()
            r0 = 419128298(0x18fb63ea, float:6.4982867E-24)
            r3 = 0
            r2 = 1
            if (r1 == r0) goto L_0x0023
            r0 = 714499313(0x2a9664f1, float:2.6715395E-13)
            if (r1 != r0) goto L_0x001d
            java.lang.String r0 = "com.facebook.katana"
            boolean r0 = r4.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x001e
        L_0x001d:
            r1 = -1
        L_0x001e:
            if (r1 == 0) goto L_0x002d
            if (r1 == r2) goto L_0x002d
            return r3
        L_0x0023:
            java.lang.String r0 = "com.facebook.wakizashi"
            boolean r0 = r4.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x001e
            goto L_0x001d
        L_0x002d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0HH.A04(android.content.Context):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:75:0x0134 A[Catch:{ RuntimeException -> 0x01e7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0141 A[Catch:{ RuntimeException -> 0x01e7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0150 A[Catch:{ RuntimeException -> 0x01e7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x016c A[Catch:{ RuntimeException -> 0x01e7 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int A00(android.content.Context r26, android.content.pm.PackageManager r27, int r28, int r29, java.lang.Integer... r30) {
        /*
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            r20 = r30
            r0 = r20
            int r15 = r0.length
            r12 = 0
            r11 = 0
            r19 = 0
        L_0x000e:
            r10 = 1
            r14 = r27
            r3 = r26
            r25 = r28
            if (r11 >= r15) goto L_0x020a
            r9 = r30[r11]
            java.lang.String r4 = r3.getPackageName()     // Catch:{ RuntimeException -> 0x01e7 }
            int r18 = r9.intValue()     // Catch:{ RuntimeException -> 0x01e7 }
            r1 = r18
            r0 = r1 | 512(0x200, float:7.175E-43)
            r2 = r0 | 512(0x200, float:7.175E-43)
            r0 = 32768(0x8000, float:4.5918E-41)
            r2 = r2 | r0
            android.content.pm.PackageInfo r2 = r14.getPackageInfo(r4, r2)     // Catch:{ Exception -> 0x0055 }
            if (r1 == r10) goto L_0x0032
            goto L_0x0037
        L_0x0032:
            android.content.pm.ActivityInfo[] r0 = r2.activities     // Catch:{ Exception -> 0x0055 }
            r17 = r0
            goto L_0x0043
        L_0x0037:
            r0 = 2
            if (r1 == r0) goto L_0x0050
            r0 = 4
            if (r1 == r0) goto L_0x004b
            r0 = 8
            if (r1 == r0) goto L_0x0046
            r17 = 0
        L_0x0043:
            if (r17 != 0) goto L_0x00c9
            goto L_0x00b2
        L_0x0046:
            android.content.pm.ProviderInfo[] r0 = r2.providers     // Catch:{ Exception -> 0x0055 }
            r17 = r0
            goto L_0x0043
        L_0x004b:
            android.content.pm.ServiceInfo[] r0 = r2.services     // Catch:{ Exception -> 0x0055 }
            r17 = r0
            goto L_0x0043
        L_0x0050:
            android.content.pm.ActivityInfo[] r0 = r2.receivers     // Catch:{ Exception -> 0x0055 }
            r17 = r0
            goto L_0x0043
        L_0x0055:
            r5 = move-exception
            java.lang.String r4 = "AppComponentManager"
            java.lang.String r0 = A02(r1)     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Object[] r2 = new java.lang.Object[]{r0}     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = "Got error while trying to get components of type[%s]. Fallback to manifest parsing.."
            java.lang.String r0 = java.lang.String.format(r0, r2)     // Catch:{ RuntimeException -> 0x01e7 }
            android.util.Log.w(r4, r0, r5)     // Catch:{ RuntimeException -> 0x01e7 }
            java.io.File r2 = new java.io.File     // Catch:{ RuntimeException -> 0x01e7 }
            android.content.pm.ApplicationInfo r0 = r3.getApplicationInfo()     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = r0.sourceDir     // Catch:{ RuntimeException -> 0x01e7 }
            r2.<init>(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            X.0KP r0 = new X.0KP     // Catch:{ RuntimeException -> 0x01e7 }
            r0.<init>()     // Catch:{ RuntimeException -> 0x01e7 }
            X.0KN r2 = r0.A04(r2)     // Catch:{ Exception -> 0x01d1 }
            r0 = 1
            if (r1 == r0) goto L_0x00ab
            r0 = 2
            if (r1 == r0) goto L_0x00a4
            r0 = 4
            if (r1 == r0) goto L_0x009d
            r0 = 8
            if (r1 != r0) goto L_0x0091
            java.util.List r0 = r2.A04     // Catch:{ Exception -> 0x01d1 }
            android.content.pm.ComponentInfo[] r17 = X.AnonymousClass0KN.A00(r2, r0)     // Catch:{ Exception -> 0x01d1 }
            goto L_0x00c9
        L_0x0091:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x01d1 }
            java.lang.String r2 = "Unsupported component type: "
            java.lang.String r0 = X.AnonymousClass08S.A09(r2, r1)     // Catch:{ Exception -> 0x01d1 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x01d1 }
            throw r3     // Catch:{ Exception -> 0x01d1 }
        L_0x009d:
            java.util.List r0 = r2.A06     // Catch:{ Exception -> 0x01d1 }
            android.content.pm.ComponentInfo[] r17 = X.AnonymousClass0KN.A00(r2, r0)     // Catch:{ Exception -> 0x01d1 }
            goto L_0x00c9
        L_0x00a4:
            java.util.List r0 = r2.A05     // Catch:{ Exception -> 0x01d1 }
            android.content.pm.ComponentInfo[] r17 = X.AnonymousClass0KN.A00(r2, r0)     // Catch:{ Exception -> 0x01d1 }
            goto L_0x00c9
        L_0x00ab:
            java.util.List r0 = r2.A03     // Catch:{ Exception -> 0x01d1 }
            android.content.pm.ComponentInfo[] r17 = X.AnonymousClass0KN.A00(r2, r0)     // Catch:{ Exception -> 0x01d1 }
            goto L_0x00c9
        L_0x00b2:
            java.lang.String r2 = "AppComponentManager"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = "getComponentsForType component list was null for type[%s]."
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ RuntimeException -> 0x01e7 }
            android.util.Log.w(r2, r0)     // Catch:{ RuntimeException -> 0x01e7 }
            android.content.pm.ComponentInfo[] r0 = new android.content.pm.ComponentInfo[r12]     // Catch:{ RuntimeException -> 0x01e7 }
            r17 = r0
        L_0x00c9:
            X.0I2 r8 = new X.0I2     // Catch:{ RuntimeException -> 0x01e7 }
            r0 = r17
            int r7 = r0.length     // Catch:{ RuntimeException -> 0x01e7 }
            r8.<init>(r7)     // Catch:{ RuntimeException -> 0x01e7 }
            r6 = 0
        L_0x00d2:
            if (r6 >= r7) goto L_0x0182
            r4 = r17[r6]     // Catch:{ RuntimeException -> 0x01e7 }
            android.os.Bundle r2 = r4.metaData     // Catch:{ RuntimeException -> 0x01e7 }
            r5 = 3
            if (r2 == 0) goto L_0x012c
            java.lang.String r0 = "enable-normal"
            java.lang.String r1 = "enable-stage"
            java.lang.String r3 = r2.getString(r1, r0)     // Catch:{ RuntimeException -> 0x01e7 }
            r2 = -1
            int r16 = r3.hashCode()     // Catch:{ RuntimeException -> 0x01e7 }
            r1 = 2
            switch(r16) {
                case -2055805548: goto L_0x0109;
                case -338306349: goto L_0x00ff;
                case -301967247: goto L_0x00f7;
                case 1434842880: goto L_0x00ed;
                default: goto L_0x00ec;
            }     // Catch:{ RuntimeException -> 0x01e7 }
        L_0x00ec:
            goto L_0x0112
        L_0x00ed:
            java.lang.String r0 = "enable-after-login-urgent"
            boolean r0 = r3.equals(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            if (r0 == 0) goto L_0x0112
            r2 = 2
            goto L_0x0112
        L_0x00f7:
            boolean r0 = r3.equals(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            if (r0 == 0) goto L_0x0112
            r2 = 3
            goto L_0x0112
        L_0x00ff:
            java.lang.String r0 = "enable-warm-pretos"
            boolean r0 = r3.equals(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            if (r0 == 0) goto L_0x0112
            r2 = 1
            goto L_0x0112
        L_0x0109:
            java.lang.String r0 = "enable-cold-pretos"
            boolean r0 = r3.equals(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            if (r0 == 0) goto L_0x0112
            r2 = 0
        L_0x0112:
            if (r2 == 0) goto L_0x012a
            if (r2 == r10) goto L_0x0128
            if (r2 == r1) goto L_0x012e
            if (r2 == r5) goto L_0x012c
            java.lang.String r2 = "AppComponentManager"
            java.lang.String r0 = r4.name     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Object[] r1 = new java.lang.Object[]{r3, r0}     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = "Could not match enable stage value \"%s\" for %s"
            X.C010708t.A0O(r2, r0, r1)     // Catch:{ RuntimeException -> 0x01e7 }
            goto L_0x012c
        L_0x0128:
            r1 = 1
            goto L_0x012f
        L_0x012a:
            r1 = 0
            goto L_0x012f
        L_0x012c:
            r1 = 3
            goto L_0x012f
        L_0x012e:
            r1 = 2
        L_0x012f:
            r2 = 0
            r0 = r25
            if (r1 > r0) goto L_0x0135
            r2 = 1
        L_0x0135:
            android.content.ComponentName r5 = new android.content.ComponentName     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r1 = r4.packageName     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = r4.name     // Catch:{ RuntimeException -> 0x01e7 }
            r5.<init>(r1, r0)     // Catch:{ RuntimeException -> 0x01e7 }
            r3 = 2
            if (r2 == 0) goto L_0x0142
            r3 = 1
        L_0x0142:
            r21 = r14
            r22 = r5
            r23 = r18
            r24 = r10
            android.content.pm.ComponentInfo r0 = A01(r21, r22, r23, r24)     // Catch:{ RuntimeException -> 0x01e7 }
            if (r0 != 0) goto L_0x016c
            java.lang.String r2 = "AppComponentManager"
            java.lang.String r1 = r5.getClassName()     // Catch:{ RuntimeException -> 0x01e7 }
            r0 = r18
            java.lang.String r0 = A02(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = "Error getting component info with meta-data name[%s] type[%s]. Assuming component is not disabled-by-default..."
            X.C010708t.A0O(r2, r0, r1)     // Catch:{ RuntimeException -> 0x01e7 }
        L_0x0165:
            r14.getComponentEnabledSetting(r5)     // Catch:{ RuntimeException -> 0x01e7 }
            r14.setComponentEnabledSetting(r5, r3, r10)     // Catch:{ RuntimeException -> 0x01e7 }
            goto L_0x017e
        L_0x016c:
            android.os.Bundle r2 = r0.metaData     // Catch:{ RuntimeException -> 0x01e7 }
            if (r2 != 0) goto L_0x0171
            goto L_0x0179
        L_0x0171:
            java.lang.String r0 = "default-state"
            boolean r0 = r2.getBoolean(r0, r10)     // Catch:{ RuntimeException -> 0x01e7 }
            r0 = r0 ^ r10
            goto L_0x017a
        L_0x0179:
            r0 = 0
        L_0x017a:
            if (r0 == 0) goto L_0x0165
            r3 = 0
            goto L_0x0165
        L_0x017e:
            int r6 = r6 + 1
            goto L_0x00d2
        L_0x0182:
            r0 = 0
            if (r0 == 0) goto L_0x01b7
            int r0 = r0.size()     // Catch:{ RuntimeException -> 0x01e7 }
            if (r0 <= 0) goto L_0x01b7
            java.lang.String r3 = "AppComponentManager"
            java.lang.String r2 = "updateComponents there were [%s/%s] components failed to be updated. type[%s]"
            r0 = 0
            int r0 = r0.size()     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            int r0 = r8.A00     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0, r9}     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = java.lang.String.format(r2, r0)     // Catch:{ RuntimeException -> 0x01e7 }
            android.util.Log.w(r3, r0)     // Catch:{ RuntimeException -> 0x01e7 }
            r13.add(r9)     // Catch:{ RuntimeException -> 0x01e7 }
            int r1 = r8.A00     // Catch:{ RuntimeException -> 0x01e7 }
            r0 = 0
            int r0 = r0.size()     // Catch:{ RuntimeException -> 0x01e7 }
            int r1 = r1 - r0
            int r19 = r19 + r1
            goto L_0x0206
        L_0x01b7:
            java.lang.String r2 = "AppComponentManager"
            java.lang.String r1 = "updateComponents successfully updated all %s components of type[%s]"
            int r0 = r8.A00     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0, r9}     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ RuntimeException -> 0x01e7 }
            android.util.Log.w(r2, r0)     // Catch:{ RuntimeException -> 0x01e7 }
            int r0 = r8.A00     // Catch:{ RuntimeException -> 0x01e7 }
            int r19 = r19 + r0
            goto L_0x0206
        L_0x01d1:
            r3 = move-exception
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = "Error getting components type[%s] from the XML."
            X.C010708t.A0U(r4, r3, r0, r1)     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ RuntimeException -> 0x01e7 }
            java.lang.String r0 = "Error getting components from the XML"
            r1.<init>(r0, r3)     // Catch:{ RuntimeException -> 0x01e7 }
            throw r1     // Catch:{ RuntimeException -> 0x01e7 }
        L_0x01e7:
            r3 = move-exception
            java.lang.String r2 = "AppComponentManager"
            int r0 = r9.intValue()
            java.lang.String r1 = A02(r0)
            java.lang.String r0 = r3.getMessage()
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "updateComponents failed to update type[%s] error[%s]"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            android.util.Log.e(r2, r0, r3)
            r13.add(r9)
        L_0x0206:
            int r11 = r11 + 1
            goto L_0x000e
        L_0x020a:
            int r0 = r13.size()
            if (r0 != 0) goto L_0x0211
            return r19
        L_0x0211:
            if (r29 != 0) goto L_0x0228
            java.lang.String r2 = "AppComponentManager"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r29)
            java.lang.Object[] r1 = new java.lang.Object[]{r13, r0}
            java.lang.String r0 = "updateComponents Failed updating components for types[%s]. No more retries left."
            java.lang.String r0 = java.lang.String.format(r0, r1)
            android.util.Log.w(r2, r0)
            r0 = -1
            return r0
        L_0x0228:
            java.lang.String r2 = "AppComponentManager"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r29)
            java.lang.Object[] r1 = new java.lang.Object[]{r13, r0}
            java.lang.String r0 = "updateComponents Failed updating components for types[%s]. Retries left[%s]"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            android.util.Log.w(r2, r0)
            int r2 = r29 + -1
            java.lang.Integer[] r0 = new java.lang.Integer[r12]
            java.lang.Object[] r1 = r13.toArray(r0)
            java.lang.Integer[] r1 = (java.lang.Integer[]) r1
            r0 = r25
            int r0 = A00(r3, r14, r0, r2, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0HH.A00(android.content.Context, android.content.pm.PackageManager, int, int, java.lang.Integer[]):int");
    }
}
