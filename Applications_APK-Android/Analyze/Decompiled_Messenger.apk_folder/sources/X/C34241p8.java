package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1p8  reason: invalid class name and case insensitive filesystem */
public final class C34241p8 {
    private static volatile C34241p8 A01;
    public AnonymousClass0UN A00;

    public static final C34241p8 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C34241p8.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C34241p8(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C34241p8(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
