package X;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.1jf  reason: invalid class name and case insensitive filesystem */
public final class C31421jf {
    public final Map A00 = new LinkedHashMap();
    public final Map A01 = new LinkedHashMap();
    public final Map A02 = new HashMap();
    public final Map A03 = new LinkedHashMap();

    public void A00(C17750zP r4) {
        Map map;
        if (this.A02.remove(r4) != null) {
            int i = r4.A00;
            if (i == 1) {
                map = this.A01;
            } else if (i == 2) {
                String str = r4.A01;
                Map map2 = (Map) this.A03.get(str);
                map2.remove(r4.A02);
                if (map2.isEmpty()) {
                    this.A03.remove(str);
                    return;
                }
                return;
            } else if (i == 3) {
                map = this.A00;
            } else {
                return;
            }
            map.remove(r4.A02);
        }
    }
}
