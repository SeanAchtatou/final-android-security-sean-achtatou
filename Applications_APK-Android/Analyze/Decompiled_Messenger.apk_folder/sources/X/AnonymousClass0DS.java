package X;

/* renamed from: X.0DS  reason: invalid class name */
public final class AnonymousClass0DS {
    public final int A00;
    public final int A01;
    public final short[][] A02;
    private final String A03;
    private final boolean A04;

    /* JADX WARNING: Removed duplicated region for block: B:39:0x009c A[SYNTHETIC, Splitter:B:39:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a1 A[Catch:{ IOException -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00ad A[SYNTHETIC, Splitter:B:49:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b2 A[Catch:{ IOException -> 0x00b6 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00() {
        /*
            r17 = this;
            java.lang.String r6 = "Exception thrown while closing..."
            android.content.Context r1 = X.C02470Ey.A00
            r9 = 0
            java.lang.String r3 = "DYNA|MethodStatsWriter"
            if (r1 != 0) goto L_0x000f
            java.lang.String r0 = "No context found!"
            X.C010708t.A0J(r3, r0)
            return r9
        L_0x000f:
            r8 = 0
            r10 = r17
            java.lang.String r0 = r10.A03     // Catch:{ Exception -> 0x0093, all -> 0x0090 }
            java.io.FileOutputStream r7 = r1.openFileOutput(r0, r9)     // Catch:{ Exception -> 0x0093, all -> 0x0090 }
            boolean r0 = r10.A04     // Catch:{ Exception -> 0x008e }
            if (r0 == 0) goto L_0x002f
            com.facebook.zstd.ZstdOutputStream r0 = new com.facebook.zstd.ZstdOutputStream     // Catch:{ Exception -> 0x008e }
            r0.<init>(r7)     // Catch:{ Exception -> 0x008e }
        L_0x0021:
            r8 = r0
            int r14 = r10.A00     // Catch:{ Exception -> 0x008e }
            int r13 = r10.A01     // Catch:{ Exception -> 0x008e }
            int r11 = r14 * r13
            short[] r12 = new short[r11]     // Catch:{ Exception -> 0x008e }
            short[][] r5 = r10.A02     // Catch:{ Exception -> 0x008e }
            int r4 = r5.length     // Catch:{ Exception -> 0x008e }
            r2 = 0
            goto L_0x0035
        L_0x002f:
            java.util.zip.GZIPOutputStream r0 = new java.util.zip.GZIPOutputStream     // Catch:{ Exception -> 0x008e }
            r0.<init>(r7)     // Catch:{ Exception -> 0x008e }
            goto L_0x0021
        L_0x0035:
            if (r2 >= r14) goto L_0x0050
            r1 = 0
        L_0x0038:
            if (r1 >= r13) goto L_0x004d
            int r16 = r2 * r13
            int r16 = r16 + r1
            int r0 = r2 % r4
            r15 = r5[r0]     // Catch:{ Exception -> 0x008e }
            int r0 = r2 / r4
            int r0 = r0 * r13
            int r0 = r0 + r1
            short r0 = r15[r0]     // Catch:{ Exception -> 0x008e }
            r12[r16] = r0     // Catch:{ Exception -> 0x008e }
            int r1 = r1 + 1
            goto L_0x0038
        L_0x004d:
            int r2 = r2 + 1
            goto L_0x0035
        L_0x0050:
            int r5 = r11 << 1
            byte[] r4 = new byte[r5]     // Catch:{ Exception -> 0x008e }
            r2 = 0
        L_0x0055:
            if (r2 >= r11) goto L_0x006e
            int r1 = r2 << 1
            short r13 = r12[r2]     // Catch:{ Exception -> 0x008e }
            r0 = 65280(0xff00, float:9.1477E-41)
            r0 = r0 & r13
            int r0 = r0 >> 8
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x008e }
            r4[r1] = r0     // Catch:{ Exception -> 0x008e }
            int r1 = r1 + 1
            r0 = r13 & 255(0xff, float:3.57E-43)
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x008e }
            r4[r1] = r0     // Catch:{ Exception -> 0x008e }
            int r2 = r2 + 1
            goto L_0x0055
        L_0x006e:
            r8.write(r4, r9, r5)     // Catch:{ Exception -> 0x008e }
            r8.flush()     // Catch:{ Exception -> 0x008e }
            r8.close()     // Catch:{ IOException -> 0x007d }
            if (r7 == 0) goto L_0x0081
            r7.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0081
        L_0x007d:
            r0 = move-exception
            X.C010708t.A0R(r3, r0, r6)
        L_0x0081:
            r2 = 1
            java.lang.String r0 = r10.A03
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Finised writing data for %s"
            X.C010708t.A0P(r3, r0, r1)
            return r2
        L_0x008e:
            r1 = move-exception
            goto L_0x0095
        L_0x0090:
            r1 = move-exception
            r7 = r8
            goto L_0x00ab
        L_0x0093:
            r1 = move-exception
            r7 = r8
        L_0x0095:
            java.lang.String r0 = "Failed to write down stats"
            X.C010708t.A0R(r3, r1, r0)     // Catch:{ all -> 0x00aa }
            if (r8 == 0) goto L_0x009f
            r8.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x009f:
            if (r7 == 0) goto L_0x00a9
            r7.close()     // Catch:{ IOException -> 0x00a5 }
            return r9
        L_0x00a5:
            r0 = move-exception
            X.C010708t.A0R(r3, r0, r6)
        L_0x00a9:
            return r9
        L_0x00aa:
            r1 = move-exception
        L_0x00ab:
            if (r8 == 0) goto L_0x00b0
            r8.close()     // Catch:{ IOException -> 0x00b6 }
        L_0x00b0:
            if (r7 == 0) goto L_0x00ba
            r7.close()     // Catch:{ IOException -> 0x00b6 }
            goto L_0x00ba
        L_0x00b6:
            r0 = move-exception
            X.C010708t.A0R(r3, r0, r6)
        L_0x00ba:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0DS.A00():boolean");
    }

    public AnonymousClass0DS(short[][] sArr, int i, int i2, String str, boolean z) {
        this.A02 = sArr;
        this.A00 = i;
        this.A01 = i2;
        this.A03 = str;
        this.A04 = z;
    }
}
