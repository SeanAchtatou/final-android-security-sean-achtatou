package X;

import android.app.Application;
import android.content.Context;
import android.os.Looper;
import com.facebook.base.app.AbstractApplicationLike;

/* renamed from: X.1XZ  reason: invalid class name */
public final class AnonymousClass1XZ implements C24771Xa {
    public Object BI3(Object obj) {
        AnonymousClass1XX r1;
        Context context = (Context) obj;
        AbstractApplicationLike abstractApplicationLike = AnonymousClass1XX.A00;
        if (abstractApplicationLike == null) {
            abstractApplicationLike = null;
            if (0 == 0) {
                Object applicationContext = context.getApplicationContext();
                if ((applicationContext instanceof Application) && !(applicationContext instanceof AnonymousClass003)) {
                    applicationContext = ((Application) applicationContext).getApplicationContext();
                }
                while (applicationContext instanceof AnonymousClass003) {
                    applicationContext = ((AnonymousClass003) applicationContext).Aq4();
                }
                if (applicationContext instanceof AbstractApplicationLike) {
                    abstractApplicationLike = (AbstractApplicationLike) applicationContext;
                } else {
                    throw new UnsupportedOperationException("Injector is not supported in process " + AnonymousClass00M.A00().A01 + ". Current thread is: " + Thread.currentThread() + " and current Looper is: " + Looper.myLooper());
                }
            }
        }
        synchronized (abstractApplicationLike) {
            while (true) {
                try {
                    r1 = abstractApplicationLike.A00;
                    if (r1 != null) {
                        break;
                    }
                    abstractApplicationLike.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        if (r1 != null) {
            return new C24821Xf(r1, context);
        }
        throw new IllegalStateException("Can NOT get FbInjector instance! Possible reasons: (1) This method was called in ContentProvider's onCreate. (2) This is a test, and you forgot to initialize the MockInjector. For example, using RobolectricTestUtil.initializeMockInjector().");
    }
}
