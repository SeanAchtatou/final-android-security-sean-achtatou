package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.19U  reason: invalid class name */
public final class AnonymousClass19U extends AnonymousClass0W4 {
    private static final C06060am A00 = new C06050al(ImmutableList.of(AnonymousClass19X.A01));
    private static final ImmutableList A01 = ImmutableList.of(AnonymousClass19X.A01, AnonymousClass19X.A02, AnonymousClass19X.A00, AnonymousClass19X.A03);

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public AnonymousClass19U() {
        super("pre_keys", A01, A00);
    }
}
