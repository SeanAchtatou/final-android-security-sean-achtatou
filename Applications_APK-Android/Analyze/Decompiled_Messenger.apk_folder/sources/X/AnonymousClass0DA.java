package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0DA  reason: invalid class name */
public final class AnonymousClass0DA extends Enum {
    private static final /* synthetic */ AnonymousClass0DA[] A00;
    public static final AnonymousClass0DA A01;
    public static final AnonymousClass0DA A02;
    public static final AnonymousClass0DA A03;
    public static final AnonymousClass0DA A04;

    static {
        AnonymousClass0DA r6 = new AnonymousClass0DA("Success", 0);
        AnonymousClass0DA r5 = new AnonymousClass0DA("TimedOut", 1);
        A03 = r5;
        AnonymousClass0DA r4 = new AnonymousClass0DA("UnknownHost", 2);
        A04 = r4;
        AnonymousClass0DA r3 = new AnonymousClass0DA("SecurityException", 3);
        A02 = r3;
        AnonymousClass0DA r2 = new AnonymousClass0DA("ExecutionException", 4);
        A01 = r2;
        A00 = new AnonymousClass0DA[]{r6, r5, r4, r3, r2};
    }

    public static AnonymousClass0DA valueOf(String str) {
        return (AnonymousClass0DA) Enum.valueOf(AnonymousClass0DA.class, str);
    }

    public static AnonymousClass0DA[] values() {
        return (AnonymousClass0DA[]) A00.clone();
    }

    private AnonymousClass0DA(String str, int i) {
    }
}
