package X;

import android.os.SystemClock;
import android.util.Pair;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Po  reason: invalid class name and case insensitive filesystem */
public final class C23451Po implements C23411Pk {
    private final Map A00 = new HashMap();
    private final Map A01 = new HashMap();

    public synchronized void Bk1(String str, String str2, String str3) {
        if (AnonymousClass02w.A0G(2)) {
            this.A00.get(Pair.create(str, str2));
            SystemClock.uptimeMillis();
            SystemClock.uptimeMillis();
        }
    }

    public synchronized void Bk3(String str, String str2, Map map) {
        if (AnonymousClass02w.A0G(2)) {
            this.A00.remove(Pair.create(str, str2));
            SystemClock.uptimeMillis();
        }
    }

    public synchronized void Bk5(String str, String str2, Throwable th, Map map) {
        long j;
        if (AnonymousClass02w.A0G(5)) {
            String str3 = str2;
            String str4 = str;
            Long l = (Long) this.A00.remove(Pair.create(str, str2));
            long uptimeMillis = SystemClock.uptimeMillis();
            Long valueOf = Long.valueOf(uptimeMillis);
            if (l != null) {
                j = uptimeMillis - l.longValue();
            } else {
                j = -1;
            }
            Long valueOf2 = Long.valueOf(j);
            Throwable th2 = th;
            Object[] objArr = {valueOf, str4, str3, valueOf2, map, th2.toString()};
            AnonymousClass02v r1 = AnonymousClass02w.A00;
            if (r1.BFj(5)) {
                r1.CMq("RequestLoggingListener", String.format(null, "time %d: onProducerFinishWithFailure: {requestId: %s, stage: %s, elapsedTime: %d ms, extraMap: %s, throwable: %s}", objArr), th2);
            }
        }
    }

    public synchronized void Bk7(String str, String str2, Map map) {
        if (AnonymousClass02w.A0G(2)) {
            this.A00.remove(Pair.create(str, str2));
            SystemClock.uptimeMillis();
        }
    }

    public synchronized void Bk9(String str, String str2) {
        if (AnonymousClass02w.A0G(2)) {
            this.A00.put(Pair.create(str, str2), Long.valueOf(SystemClock.uptimeMillis()));
        }
    }

    public synchronized void Bls(String str) {
        long j;
        if (AnonymousClass02w.A0G(2)) {
            Long l = (Long) this.A01.remove(str);
            long uptimeMillis = SystemClock.uptimeMillis();
            Long valueOf = Long.valueOf(uptimeMillis);
            if (l != null) {
                j = uptimeMillis - l.longValue();
            } else {
                j = -1;
            }
            Long valueOf2 = Long.valueOf(j);
            AnonymousClass02v r2 = AnonymousClass02w.A00;
            if (r2.BFj(2)) {
                r2.CLT("RequestLoggingListener", String.format(null, "time %d: onRequestCancellation: {requestId: %s, elapsedTime: %d ms}", valueOf, str, valueOf2));
            }
        }
    }

    public synchronized void Bm2(AnonymousClass1Q0 r8, String str, Throwable th, boolean z) {
        long j;
        if (AnonymousClass02w.A0G(5)) {
            Long l = (Long) this.A01.remove(str);
            long uptimeMillis = SystemClock.uptimeMillis();
            Long valueOf = Long.valueOf(uptimeMillis);
            if (l != null) {
                j = uptimeMillis - l.longValue();
            } else {
                j = -1;
            }
            AnonymousClass02w.A0E("RequestLoggingListener", "time %d: onRequestFailure: {requestId: %s, elapsedTime: %d ms, throwable: %s}", valueOf, str, Long.valueOf(j), th.toString());
        }
    }

    public synchronized void Bm7(AnonymousClass1Q0 r7, Object obj, String str, boolean z) {
        if (AnonymousClass02w.A0G(2)) {
            Long valueOf = Long.valueOf(SystemClock.uptimeMillis());
            Boolean valueOf2 = Boolean.valueOf(z);
            AnonymousClass02v r2 = AnonymousClass02w.A00;
            if (r2.BFj(2)) {
                r2.CLT("RequestLoggingListener", String.format(null, "time %d: onRequestSubmit: {requestId: %s, callerContext: %s, isPrefetch: %b}", valueOf, str, obj, valueOf2));
            }
            this.A01.put(str, Long.valueOf(SystemClock.uptimeMillis()));
        }
    }

    public synchronized void Bm9(AnonymousClass1Q0 r9, String str, boolean z) {
        long j;
        if (AnonymousClass02w.A0G(2)) {
            Long l = (Long) this.A01.remove(str);
            long uptimeMillis = SystemClock.uptimeMillis();
            Long valueOf = Long.valueOf(uptimeMillis);
            if (l != null) {
                j = uptimeMillis - l.longValue();
            } else {
                j = -1;
            }
            Long valueOf2 = Long.valueOf(j);
            AnonymousClass02v r2 = AnonymousClass02w.A00;
            if (r2.BFj(2)) {
                r2.CLT("RequestLoggingListener", String.format(null, "time %d: onRequestSuccess: {requestId: %s, elapsedTime: %d ms}", valueOf, str, valueOf2));
            }
        }
    }

    public synchronized void Bt9(String str, String str2, boolean z) {
        if (AnonymousClass02w.A0G(2)) {
            this.A00.remove(Pair.create(str, str2));
            SystemClock.uptimeMillis();
        }
    }

    public boolean C3R(String str) {
        return AnonymousClass02w.A0G(2);
    }
}
