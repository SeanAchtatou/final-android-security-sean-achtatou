package X;

import android.content.Context;
import com.facebook.profilo.provider.binder.BinderProvider;
import com.facebook.profilo.provider.libcio.LibcIOProvider;
import com.facebook.profilo.provider.mappings.MemoryMappingsProvider;
import com.facebook.profilo.provider.memory.MemoryAllocationProvider;
import com.facebook.profilo.provider.stacktrace.StackFrameThread;
import com.facebook.profilo.provider.systemcounters.SystemCounterThread;
import com.facebook.profilo.provider.threadmetadata.ThreadMetadataProvider;
import java.util.ArrayList;

/* renamed from: X.05A  reason: invalid class name */
public final class AnonymousClass05A {
    public static C000900o[] A00(Context context) {
        ArrayList arrayList = new ArrayList(16);
        arrayList.add(new AnonymousClass00n());
        arrayList.add(new ThreadMetadataProvider());
        arrayList.add(new AnonymousClass05C());
        arrayList.add(new StackFrameThread(context));
        arrayList.add(new SystemCounterThread(new AnonymousClass05F()));
        arrayList.add(new AnonymousClass05H());
        arrayList.add(new MemoryAllocationProvider());
        arrayList.add(new BinderProvider());
        arrayList.add(AnonymousClass05K.A00());
        arrayList.add(new AnonymousClass05L());
        arrayList.add(new LibcIOProvider());
        arrayList.add(new AnonymousClass05N(context));
        arrayList.add(new MemoryMappingsProvider());
        arrayList.add(new AnonymousClass05P(context));
        return (C000900o[]) arrayList.toArray(new C000900o[arrayList.size()]);
    }
}
