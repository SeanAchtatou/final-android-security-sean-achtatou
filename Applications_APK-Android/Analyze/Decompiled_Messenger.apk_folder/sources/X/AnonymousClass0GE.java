package X;

/* renamed from: X.0GE  reason: invalid class name */
public final class AnonymousClass0GE {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "DISABLED";
            case 2:
                return "LOCAL";
            case 3:
                return "REMOTE";
            case 4:
                return "DOWNLOADING";
            default:
                return "UNKNOWN";
        }
    }
}
