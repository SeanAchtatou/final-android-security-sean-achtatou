package X;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.0aH  reason: invalid class name and case insensitive filesystem */
public abstract class C05760aH extends Resources {
    public final Resources A00;
    private final C14650tk A01;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: boolean
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void getValue(int r3, android.util.TypedValue r4, boolean r5) {
        /*
            r2 = this;
            r1 = 0
            if (r1 == 0) goto L_0x000a
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            r1.add(r0)
        L_0x000a:
            super.getValue(r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05760aH.getValue(int, android.util.TypedValue, boolean):void");
    }

    public void A07() {
        A09(this.A00.getConfiguration(), this.A00.getDisplayMetrics());
    }

    public void A08(int i) {
        Locale locale = this.A00.getConfiguration().locale;
        C14650tk r4 = this.A01;
        synchronized (r4) {
            if (locale != r4.A02) {
                C14650tk.A00(r4);
                r4.A02 = locale;
            }
            synchronized (r4) {
                int i2 = r4.A00;
                r4.A00 = i2 + 1;
                if (i2 == 0) {
                    USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r4.A04.A01("android_string_impressions"), 40);
                    if (!uSLEBaseShape0S0000000.A0G()) {
                        r4.A01 = null;
                        r4.A03 = null;
                    } else {
                        r4.A01 = uSLEBaseShape0S0000000;
                        r4.A03 = r4.A05;
                    }
                }
                Map map = r4.A03;
                if (map != null) {
                    Integer valueOf = Integer.valueOf(i);
                    Integer num = (Integer) map.get(valueOf);
                    int i3 = 1;
                    if (num != null) {
                        i3 = 1 + num.intValue();
                    }
                    map.put(valueOf, Integer.valueOf(i3));
                }
            }
            if (r4.A00 >= 50) {
                C14650tk.A00(r4);
            }
        }
    }

    public void A09(Configuration configuration, DisplayMetrics displayMetrics) {
        Configuration configuration2 = this.A00.getConfiguration();
        DisplayMetrics displayMetrics2 = this.A00.getDisplayMetrics();
        if (!configuration.equals(configuration2)) {
            Locale locale = configuration2.locale;
            if (Build.VERSION.SDK_INT >= 17) {
                configuration.setLocale(locale);
            } else {
                configuration.locale = locale;
            }
        } else {
            configuration = configuration2;
        }
        if (displayMetrics.equals((Object) getDisplayMetrics())) {
            displayMetrics = displayMetrics2;
        }
        updateConfiguration(configuration, displayMetrics);
    }

    public void A0A(Locale locale) {
        C005505z.A03("updateLocale", -340916930);
        try {
            Configuration configuration = this.A00.getConfiguration();
            if (!locale.equals(configuration.locale)) {
                configuration.locale = locale;
                Resources resources = this.A00;
                resources.updateConfiguration(configuration, resources.getDisplayMetrics());
                A07();
                C005505z.A00(1081026940);
            }
        } finally {
            C005505z.A00(1783237095);
        }
    }

    public C05760aH(Resources resources, C14650tk r5) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.A00 = resources;
        this.A01 = r5;
    }

    public CharSequence getQuantityText(int i, int i2) {
        A08(i);
        return super.getQuantityText(i, i2);
    }

    public CharSequence[] getTextArray(int i) {
        A08(i);
        return super.getTextArray(i);
    }

    public CharSequence getText(int i) {
        A08(i);
        return super.getText(i);
    }

    public CharSequence getText(int i, CharSequence charSequence) {
        return super.getText(i);
    }
}
