package X;

import android.content.Context;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0w8  reason: invalid class name and case insensitive filesystem */
public class C15880w8 implements C32291lW {
    private C32291lW A00;

    public void BvP(Context context) {
        this.A00.BvP(context);
    }

    public void BvS(Context context, NavigationTrigger navigationTrigger, MontageComposerFragmentParams montageComposerFragmentParams) {
        this.A00.BvS(context, navigationTrigger, montageComposerFragmentParams);
    }

    public void BvV(Context context, ImmutableList immutableList) {
        this.A00.BvV(context, immutableList);
    }

    public void Bvd(Context context, ThreadKey threadKey, C149266wB r4) {
        this.A00.Bvd(context, threadKey, r4);
    }

    public void Bvn(Context context, C13060qW r3, String str) {
        this.A00.Bvn(context, r3, str);
    }

    public void Bvo(Context context) {
        this.A00.Bvo(context);
    }

    public void Bvw(Context context) {
        this.A00.Bvw(context);
    }

    public void Bw0(Context context) {
        this.A00.Bw0(context);
    }

    public C15880w8(C32291lW r1) {
        this.A00 = r1;
    }
}
