package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0oZ  reason: invalid class name and case insensitive filesystem */
public final class C12120oZ {
    private static volatile C12120oZ A02;
    public AnonymousClass0UN A00;
    public boolean A01 = false;

    public static final C12120oZ A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C12120oZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C12120oZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static void A01(C12120oZ r5, String str, Map map) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, r5.A00)).A01("fbresources_bad_language_pack_info"), AnonymousClass1Y3.A1I);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("type", str);
            for (Map.Entry entry : map.entrySet()) {
                String str2 = (String) entry.getValue();
                if (str2 != null) {
                    String str3 = (String) entry.getKey();
                    C30143EqN eqN = (C30143EqN) C30134EqE.A00.get(str3);
                    if (eqN != null) {
                        eqN.AOY(uSLEBaseShape0S0000000, str2);
                    } else {
                        C010708t.A0C(C30134EqE.class, "request param <%s> with value <%s> will not be logged as it is not defined in logger <%s>", str3, str2, uSLEBaseShape0S0000000.getClass().getSimpleName());
                    }
                }
            }
            uSLEBaseShape0S0000000.A06();
        }
    }

    public void A02(String str, C09770if r5, boolean z, Throwable th) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("fbresources_loading_failure"), 169);
        if (uSLEBaseShape0S0000000.A0G()) {
            if (r5 != null) {
                uSLEBaseShape0S0000000.A0D("locale", r5.A07.toString());
                uSLEBaseShape0S0000000.A0D("file_format", r5.A04.mValue);
            }
            uSLEBaseShape0S0000000.A08("is_from_waiting_activity_screen", Boolean.valueOf(z));
            if (str != null) {
                uSLEBaseShape0S0000000.A0Q(str);
            }
            if (th != null) {
                uSLEBaseShape0S0000000.A0D("error", th.toString());
            }
            uSLEBaseShape0S0000000.A06();
        }
    }

    private C12120oZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
