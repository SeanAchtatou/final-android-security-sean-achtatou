package X;

import android.content.Context;
import android.widget.Toast;
import java.lang.ref.WeakReference;

/* renamed from: X.0X8  reason: invalid class name */
public final class AnonymousClass0X8 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.errorreporting.FbErrorReporterImpl$ShowToast";
    private WeakReference A00;
    private final AnonymousClass06F A01;

    public void run() {
        Context context = (Context) this.A00.get();
        if (context != null) {
            AnonymousClass06F r0 = this.A01;
            Toast.makeText(context, String.format("[FB-Only] SoftErrorFailHarder: %s\n%s", r0.A02, r0.A03), 1).show();
        }
    }

    public AnonymousClass0X8(AnonymousClass06F r2, Context context) {
        this.A01 = r2;
        this.A00 = new WeakReference(context);
    }
}
