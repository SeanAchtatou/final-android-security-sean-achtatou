package X;

import java.util.ArrayList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0UW  reason: invalid class name */
public class AnonymousClass0UW {
    private static volatile AnonymousClass0UW A04;
    public ArrayList A00 = new ArrayList();
    private boolean A01;
    public final AnonymousClass1Y6 A02;
    private final String A03;

    public void A02() {
        synchronized (this) {
            if (!this.A01) {
                AnonymousClass08Z.A02(8, this.A03, 0);
            }
            this.A01 = true;
            notifyAll();
        }
        this.A02.C4C(new AnonymousClass0ZO(this));
    }

    public synchronized void A03() {
        while (!this.A01) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void A04(AnonymousClass0Y2 r3) {
        boolean z;
        synchronized (this) {
            if (!this.A00.contains(r3)) {
                this.A00.add(r3);
            }
            z = this.A01;
        }
        if (z) {
            this.A02.C4C(new AnonymousClass0ZO(this));
        }
    }

    public synchronized boolean A05() {
        return this.A01;
    }

    public static final AnonymousClass0UW A00(AnonymousClass1XY r5) {
        if (A04 == null) {
            synchronized (AnonymousClass0UW.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r5);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass0UW(AnonymousClass0UX.A07(r5.getApplicationInjector()), "App Init Lock Held");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public AnonymousClass0UW(AnonymousClass1Y6 r4, String str) {
        this.A02 = r4;
        this.A03 = str;
        AnonymousClass08Z.A01(8, str, 0);
    }
}
