package X;

import android.util.Log;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0KZ  reason: invalid class name */
public final class AnonymousClass0KZ {
    public static final AtomicReference A00 = new AtomicReference();

    public static void A00(String str, String str2, Throwable th) {
        AnonymousClass0KY r0 = (AnonymousClass0KY) A00.get();
        if (r0 != null) {
            r0.CNn(str, str2, th);
        } else {
            Log.e(str, str2, th);
        }
    }
}
