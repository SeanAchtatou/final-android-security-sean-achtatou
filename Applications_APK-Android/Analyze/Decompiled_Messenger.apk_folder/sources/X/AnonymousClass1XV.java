package X;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1XV  reason: invalid class name */
public final class AnonymousClass1XV implements AutoCloseable {
    public final int A00;
    public final C27411d9 A01;
    public final C30233Es2 A02;
    public final int A03;
    public final C04260Tf A04;
    public final C30234Es3 A05;
    public final String A06;
    public final AtomicInteger A07;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1XV r5 = (AnonymousClass1XV) obj;
            if (this.A00 != r5.A00 || this.A03 != r5.A03 || !Objects.equals(this.A06, r5.A06) || !Objects.equals(this.A04, r5.A04) || !Objects.equals(this.A05, r5.A05) || !Objects.equals(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public boolean A00(int i) {
        if ((i & this.A00) != 0) {
            return true;
        }
        return false;
    }

    public void close() {
        int i;
        AnonymousClass8t3 r1;
        C27411d9 r3 = this.A01;
        ArrayList arrayList = (ArrayList) r3.A00.get();
        if (arrayList != null && !arrayList.isEmpty()) {
            int size = arrayList.size() - 1;
            if (((AnonymousClass1XV) arrayList.get(size)) == this) {
                arrayList.remove(size);
                if (arrayList.isEmpty()) {
                    r3.A00.set(null);
                }
                AnonymousClass1XT r0 = C27401d8.A02;
                if (r0 != null) {
                    r0.BVi(this);
                }
                AtomicInteger atomicInteger = this.A07;
                if (atomicInteger != null) {
                    boolean z = false;
                    if (false != A00(2)) {
                        z = true;
                    }
                    boolean z2 = false;
                    if (this.A04.A02 != null) {
                        z2 = true;
                    }
                    if (!z2 || !z) {
                        while (true) {
                            int i2 = atomicInteger.get();
                            if (i2 <= 0) {
                                break;
                            }
                            i = i2 - 1;
                            if (atomicInteger.compareAndSet(i2, i)) {
                                if (i == 0) {
                                    atomicInteger.set(-1);
                                }
                            }
                        }
                        if (i == 0 && (r1 = C27401d8.A01) != null) {
                            r1.onChainDeactivate(this, 9);
                            return;
                        }
                    }
                }
                i = -1;
                if (i == 0) {
                }
            }
        }
    }

    public int hashCode() {
        return Objects.hash(this.A06, this.A04, Integer.valueOf(this.A00), this.A05, this.A02, Integer.valueOf(this.A03));
    }

    public AnonymousClass1XV(String str, C04260Tf r8, long j, int i, int i2, C30234Es3 es3, C30233Es2 es2, AtomicInteger atomicInteger, int i3, C27411d9 r17) {
        this.A06 = str;
        this.A04 = new C04260Tf(j, i, r8, atomicInteger == null ? 0 : 1);
        this.A00 = i2;
        this.A05 = es3;
        this.A02 = es2;
        this.A07 = atomicInteger;
        this.A03 = i3;
        this.A01 = r17;
    }
}
