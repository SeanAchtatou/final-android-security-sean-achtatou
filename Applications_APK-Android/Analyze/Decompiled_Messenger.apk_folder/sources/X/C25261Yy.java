package X;

/* renamed from: X.1Yy  reason: invalid class name and case insensitive filesystem */
public abstract class C25261Yy {
    public long A00;
    public String A01;
    public boolean A02;
    public final AnonymousClass1XV A03;
    private final AnonymousClass1YS A04;

    public int A00() {
        int i;
        AnonymousClass0UN r1;
        if (!(this instanceof C25251Yx)) {
            if (!(this instanceof AnonymousClass1Z1)) {
                i = AnonymousClass1Y3.AON;
                r1 = ((AnonymousClass1Z3) this).A00;
            } else {
                i = AnonymousClass1Y3.AH8;
                r1 = ((AnonymousClass1Z1) this).A00;
            }
            return ((AnonymousClass0WR) AnonymousClass1XX.A02(0, i, r1)).BKI();
        }
        C25251Yx r0 = (C25251Yx) this;
        boolean z = r0.A02;
        String str = r0.A01;
        return z ? C25211Yt.A01(str) : C25211Yt.A00(str);
    }

    public String A01(int i) {
        if (!(this instanceof C25251Yx)) {
            return !(this instanceof AnonymousClass1Z1) ? "AppJob_AfterColdStartINeedInit" : "AppJob_AfterUILoadedINeedInit";
        }
        C25251Yx r0 = (C25251Yx) this;
        return AnonymousClass08S.A0S("AppJobRunner#", r0.A01, "#", ((AnonymousClass1ZU) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BDf, r0.A01)).A03(i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:200:0x0366, code lost:
        if (X.AnonymousClass0Eq.A02(X.AnonymousClass0Eq.A00(), X.AnonymousClass0GL.A00(io.card.payment.BuildConfig.FLAVOR), 1) != false) goto L_0x0368;
     */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x034c A[Catch:{ all -> 0x03ce, all -> 0x03da }] */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x036e A[Catch:{ all -> 0x03ce, all -> 0x03da }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x014d A[Catch:{ all -> 0x03ce, all -> 0x03da }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(int r20) {
        /*
            r19 = this;
            r1 = r19
            boolean r0 = r1 instanceof X.C25251Yx
            r6 = r20
            if (r0 != 0) goto L_0x0089
            r5 = r1
            X.1Z2 r5 = (X.AnonymousClass1Z2) r5
            X.0WR r0 = r5.A06()     // Catch:{ all -> 0x0084 }
            X.1YQ r4 = r0.BLp()     // Catch:{ all -> 0x0084 }
            java.util.concurrent.atomic.AtomicInteger r0 = r5.A02     // Catch:{ all -> 0x0084 }
            int r1 = r0.getAndIncrement()     // Catch:{ all -> 0x0084 }
            if (r1 != 0) goto L_0x001e
            r5.A08()     // Catch:{ all -> 0x0084 }
        L_0x001e:
            int r0 = r5.A00()     // Catch:{ all -> 0x0084 }
            if (r1 != r0) goto L_0x0028
            r5.A07()     // Catch:{ all -> 0x0084 }
            goto L_0x0080
        L_0x0028:
            if (r4 == 0) goto L_0x0080
            java.lang.String r7 = r4.getSimpleName()     // Catch:{ all -> 0x0084 }
            X.069 r0 = r5.A01     // Catch:{ all -> 0x0084 }
            long r9 = r0.now()     // Catch:{ all -> 0x0084 }
            long r2 = r5.A00     // Catch:{ all -> 0x0084 }
            long r0 = r9 - r2
            int r8 = (int) r0     // Catch:{ all -> 0x0084 }
            X.1Zi r2 = r5.A00     // Catch:{ all -> 0x0084 }
            java.lang.String r1 = r5.A01     // Catch:{ all -> 0x0084 }
            r0 = -1
            r2.A02(r7, r8, r0, r1)     // Catch:{ all -> 0x0084 }
            java.lang.String r2 = r5.A01(r6)     // Catch:{ all -> 0x0084 }
            java.lang.String r1 = "%s#%s"
            r0 = 872368201(0x33ff4849, float:1.18875114E-7)
            X.C005505z.A06(r1, r2, r7, r0)     // Catch:{ all -> 0x0084 }
            r4.init()     // Catch:{ Exception -> 0x0063 }
            X.1Zi r4 = r5.A00     // Catch:{ Exception -> 0x0063 }
            X.069 r0 = r5.A01     // Catch:{ Exception -> 0x0063 }
            long r2 = r0.now()     // Catch:{ Exception -> 0x0063 }
            long r2 = r2 - r9
            int r1 = (int) r2     // Catch:{ Exception -> 0x0063 }
            java.lang.String r0 = r5.A01     // Catch:{ Exception -> 0x0063 }
            r4.A01(r7, r8, r1, r0)     // Catch:{ Exception -> 0x0063 }
            r0 = -377420690(0xffffffffe981046e, float:-1.9496544E25)
            goto L_0x0074
        L_0x0063:
            r3 = move-exception
            java.lang.String r2 = r5.A01(r6)     // Catch:{ all -> 0x0078 }
            java.lang.String r1 = "INeedInit via AppJob failed: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r7}     // Catch:{ all -> 0x0078 }
            X.C010708t.A0W(r2, r3, r1, r0)     // Catch:{ all -> 0x0078 }
            r0 = 1301726423(0x4d96c4d7, float:3.16185312E8)
        L_0x0074:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0084 }
            goto L_0x0080
        L_0x0078:
            r1 = move-exception
            r0 = 1670518275(0x63921603, float:5.3896215E21)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0084 }
            throw r1     // Catch:{ all -> 0x0084 }
        L_0x0080:
            r5.A02(r6)
            return
        L_0x0084:
            r0 = move-exception
            r5.A02(r6)
            throw r0
        L_0x0089:
            r5 = r1
            X.1Yx r5 = (X.C25251Yx) r5
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x03da }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x03da }
            r9 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x03da }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x03da }
            long r7 = r0.now()     // Catch:{ all -> 0x03da }
            int r0 = X.AnonymousClass1Y3.BDf     // Catch:{ all -> 0x03da }
            X.0UN r3 = r5.A01     // Catch:{ all -> 0x03da }
            r4 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r0, r3)     // Catch:{ all -> 0x03da }
            X.1ZU r2 = (X.AnonymousClass1ZU) r2     // Catch:{ all -> 0x03da }
            r1 = 5
            int r0 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x03da }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r3)     // Catch:{ all -> 0x03da }
            X.1Yd r0 = (X.C25051Yd) r0     // Catch:{ all -> 0x03da }
            boolean r3 = r2.A05(r6, r0)     // Catch:{ all -> 0x03da }
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x03da }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x03da }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x03da }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x03da }
            long r0 = r0.now()     // Catch:{ all -> 0x03da }
            long r0 = r0 - r7
            int r11 = (int) r0     // Catch:{ all -> 0x03da }
            int r0 = X.AnonymousClass1Y3.BDf     // Catch:{ all -> 0x03da }
            X.0UN r2 = r5.A01     // Catch:{ all -> 0x03da }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r0, r2)     // Catch:{ all -> 0x03da }
            X.1ZU r0 = (X.AnonymousClass1ZU) r0     // Catch:{ all -> 0x03da }
            java.lang.String r8 = r0.A03(r6)     // Catch:{ all -> 0x03da }
            if (r3 != 0) goto L_0x0114
            r1 = 3
            int r0 = X.AnonymousClass1Y3.BJm     // Catch:{ all -> 0x03da }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r2)     // Catch:{ all -> 0x03da }
            X.1Zi r0 = (X.C25361Zi) r0     // Catch:{ all -> 0x03da }
            java.lang.String r3 = r5.A01     // Catch:{ all -> 0x03da }
            int r2 = X.AnonymousClass1Y3.Ap7     // Catch:{ all -> 0x03da }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x03da }
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x03da }
            X.1ZE r1 = (X.AnonymousClass1ZE) r1     // Catch:{ all -> 0x03da }
            java.lang.String r0 = "appjobs_android_job_skipped"
            X.0bW r2 = r1.A01(r0)     // Catch:{ all -> 0x03da }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r1 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x03da }
            r0 = 53
            r1.<init>(r2, r0)     // Catch:{ all -> 0x03da }
            boolean r0 = r1.A0G()     // Catch:{ all -> 0x03da }
            if (r0 == 0) goto L_0x03d6
            java.lang.String r0 = "trigger_name"
            r1.A0D(r0, r3)     // Catch:{ all -> 0x03da }
            java.lang.String r0 = "job_name"
            r1.A0D(r0, r8)     // Catch:{ all -> 0x03da }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x03da }
            java.lang.String r0 = "is_needed_time"
            r1.A0B(r0, r2)     // Catch:{ all -> 0x03da }
            r1.A06()     // Catch:{ all -> 0x03da }
            goto L_0x03d6
        L_0x0114:
            r0 = 42
            if (r6 == r0) goto L_0x014e
            r0 = 43
            if (r6 == r0) goto L_0x014e
            r0 = 57
            if (r6 == r0) goto L_0x014e
            r0 = 58
            if (r6 == r0) goto L_0x014e
            r0 = 91
            if (r6 == r0) goto L_0x014e
            r0 = 92
            if (r6 == r0) goto L_0x014e
            r0 = 94
            if (r6 == r0) goto L_0x014e
            r0 = 127(0x7f, float:1.78E-43)
            if (r6 == r0) goto L_0x014e
            r0 = 111(0x6f, float:1.56E-43)
            if (r6 == r0) goto L_0x014e
            r0 = 112(0x70, float:1.57E-43)
            if (r6 == r0) goto L_0x014e
            r0 = 134(0x86, float:1.88E-43)
            if (r6 == r0) goto L_0x014e
            r0 = 135(0x87, float:1.89E-43)
            if (r6 == r0) goto L_0x014e
            switch(r20) {
                case 15: goto L_0x014e;
                case 16: goto L_0x014e;
                case 17: goto L_0x014e;
                case 18: goto L_0x014e;
                case 19: goto L_0x014e;
                case 20: goto L_0x014e;
                default: goto L_0x0147;
            }     // Catch:{ all -> 0x03da }
        L_0x0147:
            switch(r20) {
                case 46: goto L_0x014e;
                case 47: goto L_0x014e;
                case 48: goto L_0x014e;
                default: goto L_0x014a;
            }     // Catch:{ all -> 0x03da }
        L_0x014a:
            r0 = 0
        L_0x014b:
            if (r0 == 0) goto L_0x0346
            goto L_0x0150
        L_0x014e:
            r0 = 1
            goto L_0x014b
        L_0x0150:
            java.lang.String r18 = "com.facebook.common.memory.leaklistener.MemoryLeakListener"
            java.lang.String r17 = "com.facebook.common.i18n.zawgyi.ZawgyiFontDetectorIntegration"
            java.lang.String r16 = "com.facebook.common.combinedthreadpool.asyncinit.CombinedThreadPoolLoggerAppStateListener"
            java.lang.String r15 = "com.facebook.common.activitycleaner.ActivityStackResetter"
            java.lang.String r14 = "com.facebook.common.activitycleaner.ActivityStackManager"
            java.lang.String r13 = "com.facebook.battery.instrumentation.BatteryMetricsReporter"
            java.lang.String r12 = "com.facebook.analytics.timespent.TimeSpentEventReporter"
            java.lang.String r10 = "com.facebook.analytics.counterlogger.CommunicationScheduler"
            java.lang.String r9 = "com.facebook.accessibility.logging.TouchExplorationStateChangeDetector"
            java.lang.String r7 = "com.facebook.surfaces.fb.PrewarmingJobsQueue"
            java.lang.String r4 = "com.facebook.http.networkstatelogger.NetworkStateLogger"
            java.lang.String r3 = "com.facebook.battery.cpuspin.di.FbCpuSpinScheduler"
            java.lang.String r1 = "com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs"
            java.lang.String r2 = "com.facebook.tigon.reliablemedia.ReliableMediaMonitor"
            java.lang.String r0 = "com.facebook.push.mqtt.service.MqttClientStateManager"
            switch(r20) {
                case 0: goto L_0x0343;
                case 1: goto L_0x0343;
                case 2: goto L_0x0340;
                case 3: goto L_0x0340;
                case 4: goto L_0x033c;
                case 5: goto L_0x0339;
                case 6: goto L_0x0339;
                case 7: goto L_0x0335;
                case 8: goto L_0x0331;
                case 9: goto L_0x032e;
                case 10: goto L_0x032e;
                case 11: goto L_0x032e;
                case 12: goto L_0x032b;
                case 13: goto L_0x032b;
                case 14: goto L_0x0327;
                case 15: goto L_0x0324;
                case 16: goto L_0x0324;
                case 17: goto L_0x0321;
                case 18: goto L_0x0321;
                case 19: goto L_0x031d;
                case 20: goto L_0x031d;
                case 21: goto L_0x0319;
                case 22: goto L_0x0315;
                case 23: goto L_0x0311;
                case 24: goto L_0x0311;
                case 25: goto L_0x030d;
                case 26: goto L_0x030d;
                case 27: goto L_0x0309;
                case 28: goto L_0x0305;
                case 29: goto L_0x0301;
                case 30: goto L_0x02fd;
                case 31: goto L_0x02f9;
                case 32: goto L_0x02f5;
                case 33: goto L_0x02f1;
                case 34: goto L_0x02ed;
                case 35: goto L_0x02e9;
                case 36: goto L_0x02e5;
                case 37: goto L_0x02e1;
                case 38: goto L_0x02dd;
                case 39: goto L_0x02d9;
                case 40: goto L_0x02d5;
                case 41: goto L_0x02d1;
                case 42: goto L_0x02cd;
                case 43: goto L_0x02c9;
                case 44: goto L_0x02c5;
                case 45: goto L_0x02c1;
                case 46: goto L_0x02bd;
                case 47: goto L_0x02b9;
                case 48: goto L_0x02b5;
                case 49: goto L_0x02b1;
                case 50: goto L_0x02ad;
                case 51: goto L_0x02a9;
                case 52: goto L_0x02a5;
                case 53: goto L_0x02a2;
                case 54: goto L_0x02a2;
                case 55: goto L_0x02a2;
                case 56: goto L_0x029e;
                case 57: goto L_0x029a;
                case 58: goto L_0x0296;
                case 59: goto L_0x0292;
                case 60: goto L_0x028e;
                case 61: goto L_0x028a;
                case 62: goto L_0x0286;
                case 63: goto L_0x0282;
                case 64: goto L_0x027e;
                case 65: goto L_0x027a;
                case 66: goto L_0x0276;
                case 67: goto L_0x0272;
                case 68: goto L_0x026e;
                case 69: goto L_0x026a;
                case 70: goto L_0x0266;
                case 71: goto L_0x0262;
                case 72: goto L_0x025e;
                case 73: goto L_0x025a;
                case 74: goto L_0x0256;
                case 75: goto L_0x0252;
                case 76: goto L_0x024e;
                case 77: goto L_0x024a;
                case 78: goto L_0x0246;
                case 79: goto L_0x0242;
                case 80: goto L_0x023e;
                case 81: goto L_0x023a;
                case 82: goto L_0x0236;
                case 83: goto L_0x0232;
                case 84: goto L_0x022e;
                case 85: goto L_0x022a;
                case 86: goto L_0x0227;
                case 87: goto L_0x0227;
                case 88: goto L_0x0227;
                case 89: goto L_0x0227;
                case 90: goto L_0x0223;
                case 91: goto L_0x021f;
                case 92: goto L_0x021b;
                case 93: goto L_0x0217;
                case 94: goto L_0x0213;
                case 95: goto L_0x020f;
                case 96: goto L_0x020b;
                case 97: goto L_0x0207;
                case 98: goto L_0x0203;
                case 99: goto L_0x01ff;
                case 100: goto L_0x01fb;
                case 101: goto L_0x01f7;
                case 102: goto L_0x01f3;
                case 103: goto L_0x01f0;
                case 104: goto L_0x01ed;
                case 105: goto L_0x01ea;
                case 106: goto L_0x01e8;
                case 107: goto L_0x01e8;
                case 108: goto L_0x01e8;
                case 109: goto L_0x01e5;
                case 110: goto L_0x01e2;
                case 111: goto L_0x01df;
                case 112: goto L_0x01dc;
                case 113: goto L_0x01da;
                case 114: goto L_0x01da;
                case 115: goto L_0x01da;
                case 116: goto L_0x01da;
                case 117: goto L_0x01d7;
                case 118: goto L_0x01d4;
                case 119: goto L_0x01d1;
                case 120: goto L_0x01ce;
                case 121: goto L_0x01cb;
                case 122: goto L_0x01c4;
                case 123: goto L_0x01bd;
                case 124: goto L_0x01ba;
                case 125: goto L_0x01b7;
                case 126: goto L_0x01b4;
                case 127: goto L_0x01b1;
                case 128: goto L_0x01ae;
                case 129: goto L_0x0173;
                case 130: goto L_0x0173;
                case 131: goto L_0x0173;
                case 132: goto L_0x0173;
                case 133: goto L_0x01ab;
                case 134: goto L_0x01a8;
                case 135: goto L_0x01a5;
                case 136: goto L_0x01a2;
                case 137: goto L_0x019f;
                case 138: goto L_0x019c;
                case 139: goto L_0x0199;
                case 140: goto L_0x0196;
                case 141: goto L_0x0193;
                case 142: goto L_0x0190;
                case 143: goto L_0x018d;
                case 144: goto L_0x018a;
                case 145: goto L_0x0187;
                case 146: goto L_0x0184;
                case 147: goto L_0x0181;
                default: goto L_0x0171;
            }     // Catch:{ all -> 0x03da }
        L_0x0171:
            java.lang.String r1 = ""
        L_0x0173:
            java.util.Set r0 = X.C28081eE.A00     // Catch:{ all -> 0x03da }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x03da }
            if (r0 != 0) goto L_0x0346
            r0 = 2
            X.C010708t.A0X(r0)     // Catch:{ all -> 0x03da }
            goto L_0x0348
        L_0x0181:
            java.lang.String r1 = "com.facebook.profilo.mmapbuf.MmapBufferProcessJob"
            goto L_0x0173
        L_0x0184:
            java.lang.String r1 = "com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager"
            goto L_0x0173
        L_0x0187:
            java.lang.String r1 = "com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager"
            goto L_0x0173
        L_0x018a:
            java.lang.String r1 = "com.facebook.profilo.blackbox.breakpad.BreakpadDumpProcessJob"
            goto L_0x0173
        L_0x018d:
            java.lang.String r1 = "com.facebook.zero.service.ZeroInterstitialEligibilityManager"
            goto L_0x0173
        L_0x0190:
            java.lang.String r1 = "com.facebook.zero.LocalZeroTokenManagerReceiverRegistration"
            goto L_0x0173
        L_0x0193:
            java.lang.String r1 = "com.facebook.zero.paidbalance.PaidBalanceController"
            goto L_0x0173
        L_0x0196:
            java.lang.String r1 = "com.facebook.zero.header.ZeroHeaderRequestManager.LocalZeroHeaderRequestManagerReceiverRegistration"
            goto L_0x0173
        L_0x0199:
            java.lang.String r1 = "com.facebook.zero.header.ZeroHeaderRequestManager.LocalZeroHeaderRequestManagerReceiverRegistration"
            goto L_0x0173
        L_0x019c:
            java.lang.String r1 = "com.facebook.zero.cms.ZeroCmsUtil"
            goto L_0x0173
        L_0x019f:
            java.lang.String r1 = "com.facebook.zero.cms.ZeroCmsUtil"
            goto L_0x0173
        L_0x01a2:
            java.lang.String r1 = "com.facebook.zero.carriersignal.CarrierSignalController"
            goto L_0x0173
        L_0x01a5:
            java.lang.String r1 = "com.facebook.xanalytics.provider.NativeXAnalyticsProvider"
            goto L_0x0173
        L_0x01a8:
            java.lang.String r1 = "com.facebook.xanalytics.provider.NativeXAnalyticsProvider"
            goto L_0x0173
        L_0x01ab:
            java.lang.String r1 = "com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitHandler"
            goto L_0x0173
        L_0x01ae:
            java.lang.String r1 = "com.facebook.voltron.fbdownloader.FbAppJobScheduledPrefetcher"
            goto L_0x0173
        L_0x01b1:
            java.lang.String r1 = "com.facebook.video.plugins.AutoplayIntentSignalMonitor"
            goto L_0x0173
        L_0x01b4:
            java.lang.String r1 = "com.facebook.video.fbgrootplayer.VideoAudioVolumeObserversManager"
            goto L_0x0173
        L_0x01b7:
            java.lang.String r1 = "com.facebook.video.fbgrootplayer.VideoAudioVolumeObserversManager"
            goto L_0x0173
        L_0x01ba:
            java.lang.String r1 = "com.facebook.video.exoserviceclient.HeroServiceInitializerAppJobInit"
            goto L_0x0173
        L_0x01bd:
            r0 = 19
            java.lang.String r1 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ all -> 0x03da }
            goto L_0x0173
        L_0x01c4:
            r0 = 19
            java.lang.String r1 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ all -> 0x03da }
            goto L_0x0173
        L_0x01cb:
            java.lang.String r1 = "com.facebook.uievaluations.uievaluationsrunner.UIEvaluationsRunner"
            goto L_0x0173
        L_0x01ce:
            java.lang.String r1 = "com.facebook.uievaluations.uievaluationsrunner.UIEvaluationsRunner"
            goto L_0x0173
        L_0x01d1:
            java.lang.String r1 = "com.facebook.ui.titlebar.abtest.WhiteChromeActivityStack"
            goto L_0x0173
        L_0x01d4:
            java.lang.String r1 = "com.facebook.tigon.tigonliger.TigonLigerService"
            goto L_0x0173
        L_0x01d7:
            java.lang.String r1 = "com.facebook.tigon.tigonliger.TigonLigerService"
            goto L_0x0173
        L_0x01da:
            r1 = r2
            goto L_0x0173
        L_0x01dc:
            java.lang.String r1 = "com.facebook.tigon.nativeservice.common.NativePlatformContextHolder"
            goto L_0x0173
        L_0x01df:
            java.lang.String r1 = "com.facebook.tigon.nativeservice.common.NativePlatformContextHolder"
            goto L_0x0173
        L_0x01e2:
            java.lang.String r1 = "com.facebook.sync.SyncInitializer"
            goto L_0x0173
        L_0x01e5:
            java.lang.String r1 = "com.facebook.sync.SyncInitializer"
            goto L_0x0173
        L_0x01e8:
            r1 = r7
            goto L_0x0173
        L_0x01ea:
            java.lang.String r1 = "com.facebook.storage.trash.FbTrashManager"
            goto L_0x0173
        L_0x01ed:
            java.lang.String r1 = "com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor"
            goto L_0x0173
        L_0x01f0:
            java.lang.String r1 = "com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor"
            goto L_0x0173
        L_0x01f3:
            java.lang.String r1 = "com.facebook.storage.diskio.ProcIOStatsOverallReporting"
            goto L_0x0173
        L_0x01f7:
            java.lang.String r1 = "com.facebook.storage.diskio.ProcIOStatsOverallReporting"
            goto L_0x0173
        L_0x01fb:
            java.lang.String r1 = "com.facebook.storage.cleaner.PathSizeOverflowCleaner"
            goto L_0x0173
        L_0x01ff:
            java.lang.String r1 = "com.facebook.storage.cask.fbapps.controllers.FBStaleRemovalPluginController"
            goto L_0x0173
        L_0x0203:
            java.lang.String r1 = "com.facebook.storage.cask.fbapps.controllers.FBMaxSizePluginController"
            goto L_0x0173
        L_0x0207:
            java.lang.String r1 = "com.facebook.storage.cask.fbapps.controllers.FBEvictionPluginController"
            goto L_0x0173
        L_0x020b:
            java.lang.String r1 = "com.facebook.storage.bigfoot.apps.fbapps.FBAppsBigFootForegroundWorker"
            goto L_0x0173
        L_0x020f:
            java.lang.String r1 = "com.facebook.storage.bigfoot.apps.fbapps.FBAppsBigFootForegroundWorker"
            goto L_0x0173
        L_0x0213:
            java.lang.String r1 = "com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector"
            goto L_0x0173
        L_0x0217:
            java.lang.String r1 = "com.facebook.resources.impl.DrawableCounterLogger"
            goto L_0x0173
        L_0x021b:
            java.lang.String r1 = "com.facebook.reactivesocket.AndroidLifecycleHandler"
            goto L_0x0173
        L_0x021f:
            java.lang.String r1 = "com.facebook.reactivesocket.AndroidLifecycleHandler"
            goto L_0x0173
        L_0x0223:
            java.lang.String r1 = "com.facebook.quicklog.dataproviders.IoStatsProvider"
            goto L_0x0173
        L_0x0227:
            r1 = r0
            goto L_0x0173
        L_0x022a:
            java.lang.String r1 = "com.facebook.push.crossapp.PendingReportedPackages"
            goto L_0x0173
        L_0x022e:
            java.lang.String r1 = "com.facebook.privacypermissionsnapshots.fb.PrivacyPermissionSnapshotsLoggerController"
            goto L_0x0173
        L_0x0232:
            java.lang.String r1 = "com.facebook.perf.startupstatemachine.StartupStateMachine"
            goto L_0x0173
        L_0x0236:
            java.lang.String r1 = "com.facebook.mobileconfig.init.MobileConfigApi2LoggerImpl"
            goto L_0x0173
        L_0x023a:
            java.lang.String r1 = "com.facebook.messaging.selfupdate2.SelfUpdate2AppStateListener"
            goto L_0x0173
        L_0x023e:
            java.lang.String r1 = "com.facebook.messaging.searchnullstate.PrefetcherManager"
            goto L_0x0173
        L_0x0242:
            java.lang.String r1 = "com.facebook.messaging.screenshotdetection.ThreadScreenshotDetector"
            goto L_0x0173
        L_0x0246:
            java.lang.String r1 = "com.facebook.messaging.screenshotdetection.ThreadScreenshotDetector"
            goto L_0x0173
        L_0x024a:
            java.lang.String r1 = "com.facebook.messaging.notify.logging.conditionalworkerimpl.PushSettingsReporter"
            goto L_0x0173
        L_0x024e:
            java.lang.String r1 = "com.facebook.messaging.instagram.fetch.EligibleInstagramAccountBackgroundFetcher"
            goto L_0x0173
        L_0x0252:
            java.lang.String r1 = "com.facebook.messaging.instagram.contactimport.InstagramContactImportBadgingController.InstagramContactImprtTriggerRegistration"
            goto L_0x0173
        L_0x0256:
            java.lang.String r1 = "com.facebook.messaging.inbox2.activenow.loader.PrefetcherManager"
            goto L_0x0173
        L_0x025a:
            java.lang.String r1 = "com.facebook.messaging.filelogger.MessagingFileLogger"
            goto L_0x0173
        L_0x025e:
            java.lang.String r1 = "com.facebook.messaging.cowatch.tracker.LivingRoomThreadTracker"
            goto L_0x0173
        L_0x0262:
            java.lang.String r1 = "com.facebook.messaging.contactstab.loader.StatusController"
            goto L_0x0173
        L_0x0266:
            java.lang.String r1 = "com.facebook.messaging.contacts.loader.nonwork.ContactObserversRegistrationHandler"
            goto L_0x0173
        L_0x026a:
            java.lang.String r1 = "com.facebook.messaging.contacts.loader.nonwork.ContactObserversRegistrationHandler"
            goto L_0x0173
        L_0x026e:
            java.lang.String r1 = "com.facebook.messaging.chatheads.service.VideoServiceAppStateListener"
            goto L_0x0173
        L_0x0272:
            java.lang.String r1 = "com.facebook.messaging.chatheads.ChatHeadsInitializer"
            goto L_0x0173
        L_0x0276:
            java.lang.String r1 = "com.facebook.messaging.analytics.perf.PostStartupTracker"
            goto L_0x0173
        L_0x027a:
            java.lang.String r1 = "com.facebook.messaging.analytics.perf.MessagingInteractionStateManager"
            goto L_0x0173
        L_0x027e:
            java.lang.String r1 = "com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener"
            goto L_0x0173
        L_0x0282:
            java.lang.String r1 = "com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener"
            goto L_0x0173
        L_0x0286:
            java.lang.String r1 = "com.facebook.memory.fbmemorymanager.FBMemoryManagerHooks"
            goto L_0x0173
        L_0x028a:
            java.lang.String r1 = "com.facebook.memory.fbmemorymanager.FBMemoryManagerHooks"
            goto L_0x0173
        L_0x028e:
            java.lang.String r1 = "com.facebook.location.foreground.ForegroundLocationFrameworkController"
            goto L_0x0173
        L_0x0292:
            java.lang.String r1 = "com.facebook.location.foreground.ForegroundLocationFrameworkController"
            goto L_0x0173
        L_0x0296:
            java.lang.String r1 = "com.facebook.keyframes.fb.FbKeyframesAppStateManager"
            goto L_0x0173
        L_0x029a:
            java.lang.String r1 = "com.facebook.keyframes.fb.FbKeyframesAppStateManager"
            goto L_0x0173
        L_0x029e:
            java.lang.String r1 = "com.facebook.interstitial.manager.InterstitialDataCleaner"
            goto L_0x0173
        L_0x02a2:
            r1 = r4
            goto L_0x0173
        L_0x02a5:
            java.lang.String r1 = "com.facebook.growth.sem.SemColdStartLogger"
            goto L_0x0173
        L_0x02a9:
            java.lang.String r1 = "com.facebook.graphql.subscriptions.core.GraphQLSubscriptionConnectorImpl"
            goto L_0x0173
        L_0x02ad:
            java.lang.String r1 = "com.facebook.graphql.modelutil.parcel.ModelParcelHelperInitQPLAppJob"
            goto L_0x0173
        L_0x02b1:
            java.lang.String r1 = "com.facebook.graphql.fleetbeacon.FleetBeaconStartupTrigger"
            goto L_0x0173
        L_0x02b5:
            java.lang.String r1 = "com.facebook.graphql.executor.OfflineMutationsManager"
            goto L_0x0173
        L_0x02b9:
            java.lang.String r1 = "com.facebook.funnellogger.FunnelLoggerImpl"
            goto L_0x0173
        L_0x02bd:
            java.lang.String r1 = "com.facebook.funnellogger.FunnelLoggerImpl"
            goto L_0x0173
        L_0x02c1:
            java.lang.String r1 = "com.facebook.fos.headersv2.fb4aorca.ZeroHeadersNetworkChangeBroadcastReceiver"
            goto L_0x0173
        L_0x02c5:
            java.lang.String r1 = "com.facebook.fling.analytics.FlingProfileLogger"
            goto L_0x0173
        L_0x02c9:
            java.lang.String r1 = "com.facebook.entitypresence.EntityPresenceManager"
            goto L_0x0173
        L_0x02cd:
            java.lang.String r1 = "com.facebook.entitypresence.EntityPresenceManager"
            goto L_0x0173
        L_0x02d1:
            java.lang.String r1 = "com.facebook.diskfootprint.cleaner.FileCleaner"
            goto L_0x0173
        L_0x02d5:
            java.lang.String r1 = "com.facebook.device_id.UniqueFamilyDeviceIdBroadcastSender"
            goto L_0x0173
        L_0x02d9:
            java.lang.String r1 = "com.facebook.device.resourcemonitor.activemonitoring.ResourceManagerActiveMonitoring"
            goto L_0x0173
        L_0x02dd:
            java.lang.String r1 = "com.facebook.device.resourcemonitor.ResourceMonitor"
            goto L_0x0173
        L_0x02e1:
            java.lang.String r1 = "com.facebook.device.resourcemonitor.ResourceMonitor"
            goto L_0x0173
        L_0x02e5:
            java.lang.String r1 = "com.facebook.device.resourcemonitor.ResourceManager"
            goto L_0x0173
        L_0x02e9:
            java.lang.String r1 = "com.facebook.config.background.impl.ConfigurationConditionalWorkerInfo"
            goto L_0x0173
        L_0x02ed:
            java.lang.String r1 = "com.facebook.conditionalworker.ConditionalWorkerManager"
            goto L_0x0173
        L_0x02f1:
            java.lang.String r1 = "com.facebook.conditionalworker.ConditionalWorkerManager"
            goto L_0x0173
        L_0x02f5:
            java.lang.String r1 = "com.facebook.compactdiskmodule.CompactDiskFlushDispatcher"
            goto L_0x0173
        L_0x02f9:
            java.lang.String r1 = "com.facebook.common.userinteraction.UserInteractionHistory"
            goto L_0x0173
        L_0x02fd:
            java.lang.String r1 = "com.facebook.common.noncriticalinit.NonCriticalInitializer"
            goto L_0x0173
        L_0x0301:
            java.lang.String r1 = "com.facebook.common.netchecker.NetChecker"
            goto L_0x0173
        L_0x0305:
            java.lang.String r1 = "com.facebook.common.memory.LargeHeapOverrideConfig"
            goto L_0x0173
        L_0x0309:
            java.lang.String r1 = "com.facebook.common.memory.manager.MemoryManager"
            goto L_0x0173
        L_0x030d:
            r1 = r18
            goto L_0x0173
        L_0x0311:
            r1 = r17
            goto L_0x0173
        L_0x0315:
            java.lang.String r1 = "com.facebook.common.errorreporting.memory.LeakMemoryDumper"
            goto L_0x0173
        L_0x0319:
            java.lang.String r1 = "com.facebook.common.connectionstatus.FbDataConnectionManager"
            goto L_0x0173
        L_0x031d:
            r1 = r16
            goto L_0x0173
        L_0x0321:
            r1 = r15
            goto L_0x0173
        L_0x0324:
            r1 = r14
            goto L_0x0173
        L_0x0327:
            java.lang.String r1 = "com.facebook.battery.samsung.SamsungWarningNotificationLogger"
            goto L_0x0173
        L_0x032b:
            r1 = r13
            goto L_0x0173
        L_0x032e:
            r1 = r3
            goto L_0x0173
        L_0x0331:
            java.lang.String r1 = "com.facebook.attribution.LatStatusJob"
            goto L_0x0173
        L_0x0335:
            java.lang.String r1 = "com.facebook.apk_testing.ApkTestingExposureLogger"
            goto L_0x0173
        L_0x0339:
            r1 = r12
            goto L_0x0173
        L_0x033c:
            java.lang.String r1 = "com.facebook.analytics.mobileconfigreliability.MobileConfigSampledAccessListenerImpl"
            goto L_0x0173
        L_0x0340:
            r1 = r10
            goto L_0x0173
        L_0x0343:
            r1 = r9
            goto L_0x0173
        L_0x0346:
            r0 = 1
            goto L_0x0349
        L_0x0348:
            r0 = 0
        L_0x0349:
            r3 = 1
            if (r0 == 0) goto L_0x036c
            java.lang.String r0 = ""
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x03da }
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0368
            X.0Eq r2 = X.AnonymousClass0Eq.A00()     // Catch:{ all -> 0x03da }
            java.lang.String r0 = ""
            int r1 = X.AnonymousClass0GL.A00(r0)     // Catch:{ all -> 0x03da }
            r0 = 1
            boolean r1 = X.AnonymousClass0Eq.A02(r2, r1, r0)     // Catch:{ all -> 0x03da }
            r0 = 0
            if (r1 == 0) goto L_0x0369
        L_0x0368:
            r0 = 1
        L_0x0369:
            if (r0 == 0) goto L_0x036c
            r3 = 0
        L_0x036c:
            if (r3 != 0) goto L_0x03d6
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x03da }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x03da }
            r3 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x03da }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x03da }
            long r12 = r0.now()     // Catch:{ all -> 0x03da }
            long r9 = r5.A00     // Catch:{ all -> 0x03da }
            long r0 = r12 - r9
            int r7 = (int) r0     // Catch:{ all -> 0x03da }
            int r1 = X.AnonymousClass1Y3.BJm     // Catch:{ all -> 0x03da }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x03da }
            r9 = 3
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x03da }
            X.1Zi r2 = (X.C25361Zi) r2     // Catch:{ all -> 0x03da }
            java.lang.String r1 = r5.A01     // Catch:{ all -> 0x03da }
            r2.A02(r8, r7, r11, r1)     // Catch:{ all -> 0x03da }
            java.lang.String r1 = "AppJob#%s"
            r0 = -1734126966(0xffffffff98a3528a, float:-4.2217856E-24)
            X.C005505z.A05(r1, r8, r0)     // Catch:{ all -> 0x03da }
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BDf     // Catch:{ all -> 0x03ce }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x03ce }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x03ce }
            X.1ZU r1 = (X.AnonymousClass1ZU) r1     // Catch:{ all -> 0x03ce }
            X.1Yz r0 = r5.A00     // Catch:{ all -> 0x03ce }
            r1.A04(r6, r0)     // Catch:{ all -> 0x03ce }
            int r0 = X.AnonymousClass1Y3.BJm     // Catch:{ all -> 0x03ce }
            X.0UN r1 = r5.A01     // Catch:{ all -> 0x03ce }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r9, r0, r1)     // Catch:{ all -> 0x03ce }
            X.1Zi r4 = (X.C25361Zi) r4     // Catch:{ all -> 0x03ce }
            int r0 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x03ce }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r0, r1)     // Catch:{ all -> 0x03ce }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x03ce }
            long r2 = r0.now()     // Catch:{ all -> 0x03ce }
            long r2 = r2 - r12
            int r1 = (int) r2     // Catch:{ all -> 0x03ce }
            java.lang.String r0 = r5.A01     // Catch:{ all -> 0x03ce }
            r4.A01(r8, r7, r1, r0)     // Catch:{ all -> 0x03ce }
            r0 = 571514479(0x22109e6f, float:1.9599513E-18)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x03da }
            goto L_0x03d6
        L_0x03ce:
            r1 = move-exception
            r0 = 347662885(0x14b8ea25, float:1.8671606E-26)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x03da }
            throw r1     // Catch:{ all -> 0x03da }
        L_0x03d6:
            r5.A02(r6)
            return
        L_0x03da:
            r0 = move-exception
            r5.A02(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25261Yy.A03(int):void");
    }

    public boolean A04() {
        return !(this instanceof AnonymousClass1Z1) && !(this instanceof AnonymousClass1Z3);
    }

    public boolean A05() {
        if (this instanceof AnonymousClass1Z1) {
            return ((AnonymousClass37h) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ANt, ((AnonymousClass1Z1) this).A00)).A02();
        }
        if (!(this instanceof AnonymousClass1Z3)) {
            return true;
        }
        return ((AnonymousClass37h) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ANt, ((AnonymousClass1Z3) this).A00)).A01();
    }

    public void A02(int i) {
        int size;
        AnonymousClass1YS r4 = this.A04;
        String str = this.A01;
        AnonymousClass1Z0 r3 = (AnonymousClass1Z0) r4.A02.peek();
        if (r3 == null || !r3.A02.equals(str)) {
            boolean A0X = C010708t.A0X(2);
            return;
        }
        synchronized (r3) {
            r3.A00.remove(Integer.valueOf(i));
            size = r3.A00.size();
        }
        boolean A0X2 = C010708t.A0X(2);
        if (size == 0) {
            r4.A02.poll();
            AnonymousClass1YS.A03(r4);
            if (AnonymousClass1YS.A04(r4, r3.A02)) {
                r4.A03.decrementAndGet();
            }
        }
    }

    public C25261Yy(AnonymousClass1YS r1, String str, long j, AnonymousClass1XV r5) {
        this.A04 = r1;
        this.A01 = str;
        this.A00 = j;
        this.A03 = r5;
    }
}
