package X;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

/* renamed from: X.1KZ  reason: invalid class name */
public final class AnonymousClass1KZ {
    public static final AnonymousClass1KZ A0C = new C22131Ka().A00();
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final C22141Kb A07;
    public final C22141Kb A08;
    public final Integer A09;
    public final Integer A0A;
    public final String A0B;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1KZ r5 = (AnonymousClass1KZ) obj;
            if (!(this.A00 == r5.A00 && this.A09 == r5.A09 && this.A0A == r5.A0A && this.A01 == r5.A01 && this.A06 == r5.A06 && this.A02 == r5.A02 && this.A05 == r5.A05 && this.A03 == r5.A03 && this.A04 == r5.A04 && Objects.equal(this.A0B, r5.A0B) && Objects.equal(this.A08, r5.A08) && Objects.equal(this.A07, r5.A07))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int hashCode;
        int A002 = (((((((((((((((((AnonymousClass1Y3.A4R + this.A00) * 31) + C90214Sl.A00(this.A09)) * 31) + AnonymousClass4HH.A00(this.A0A)) * 31) + this.A01) * 31) + this.A06) * 31) + this.A02) * 31) + this.A05) * 31) + this.A03) * 31) + this.A04) * 31;
        String str = this.A0B;
        if (str == null) {
            hashCode = 0;
        } else {
            hashCode = str.hashCode();
        }
        return ((((A002 + hashCode) * 31) + this.A08.hashCode()) * 31) + this.A07.hashCode();
    }

    public AnonymousClass1KZ(C22131Ka r4) {
        C22141Kb r1 = r4.A0B;
        C22141Kb r2 = new C22141Kb(r1.size());
        r2.putAll(r1);
        this.A08 = r2;
        this.A00 = r4.A00;
        Integer num = r4.A08;
        Preconditions.checkNotNull(num);
        this.A0A = num;
        Integer num2 = r4.A07;
        Preconditions.checkNotNull(num2);
        this.A09 = num2;
        this.A03 = r4.A03;
        this.A04 = r4.A04;
        this.A01 = r4.A01;
        this.A0B = r4.A09;
        this.A06 = r4.A06;
        this.A02 = r4.A02;
        this.A05 = r4.A05;
        C22141Kb r12 = r4.A0A;
        C22141Kb r22 = new C22141Kb(r12.size());
        r22.putAll(r12);
        this.A07 = r22;
    }
}
