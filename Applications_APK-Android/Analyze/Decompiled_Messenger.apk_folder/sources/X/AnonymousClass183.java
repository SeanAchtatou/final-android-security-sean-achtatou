package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.183  reason: invalid class name */
public final class AnonymousClass183 {
    private static volatile AnonymousClass183 A06;
    public AnonymousClass1YH A00;
    public final Context A01;
    public final C04740Vz A02;
    public final AnonymousClass19C A03;
    private final AnonymousClass0XN A04;
    public volatile String A05 = null;

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0057, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.database.sqlite.SQLiteDatabase A01() {
        /*
            r6 = this;
            monitor-enter(r6)
            X.0XN r0 = r6.A04     // Catch:{ all -> 0x0058 }
            boolean r0 = r0.A0I()     // Catch:{ all -> 0x0058 }
            r2 = 0
            if (r0 == 0) goto L_0x0056
            X.0XN r0 = r6.A04     // Catch:{ all -> 0x0058 }
            boolean r0 = r0.A0J()     // Catch:{ all -> 0x0058 }
            if (r0 != 0) goto L_0x0056
            X.0XN r0 = r6.A04     // Catch:{ all -> 0x0058 }
            com.facebook.user.model.User r0 = r0.A09()     // Catch:{ all -> 0x0058 }
            java.lang.String r1 = r0.A0j     // Catch:{ all -> 0x0058 }
            java.lang.String r0 = r6.A05     // Catch:{ all -> 0x0058 }
            if (r0 == 0) goto L_0x0026
            java.lang.String r0 = r6.A05     // Catch:{ all -> 0x0058 }
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0058 }
            if (r0 != 0) goto L_0x0031
        L_0x0026:
            X.1YH r0 = r6.A00     // Catch:{ all -> 0x0058 }
            if (r0 == 0) goto L_0x002d
            X.AnonymousClass1YH.A03()     // Catch:{ all -> 0x0058 }
        L_0x002d:
            r6.A00 = r2     // Catch:{ all -> 0x0058 }
            r6.A05 = r1     // Catch:{ all -> 0x0058 }
        L_0x0031:
            X.1YH r0 = r6.A00     // Catch:{ all -> 0x0058 }
            if (r0 != 0) goto L_0x004e
            X.1iO r5 = new X.1iO     // Catch:{ all -> 0x0058 }
            android.content.Context r4 = r6.A01     // Catch:{ all -> 0x0058 }
            X.0Vz r3 = r6.A02     // Catch:{ all -> 0x0058 }
            X.19C r0 = r6.A03     // Catch:{ all -> 0x0058 }
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r0)     // Catch:{ all -> 0x0058 }
            java.lang.String r1 = "tincan_db_"
            java.lang.String r0 = r6.A05     // Catch:{ all -> 0x0058 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0058 }
            r5.<init>(r4, r3, r2, r0)     // Catch:{ all -> 0x0058 }
            r6.A00 = r5     // Catch:{ all -> 0x0058 }
        L_0x004e:
            X.1YH r0 = r6.A00     // Catch:{ all -> 0x0058 }
            android.database.sqlite.SQLiteDatabase r0 = r0.A06()     // Catch:{ all -> 0x0058 }
            monitor-exit(r6)
            return r0
        L_0x0056:
            monitor-exit(r6)
            return r2
        L_0x0058:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass183.A01():android.database.sqlite.SQLiteDatabase");
    }

    public static final AnonymousClass183 A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (AnonymousClass183.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new AnonymousClass183(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    private AnonymousClass183(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass1YA.A02(r2);
        this.A02 = C04720Vx.A01(r2);
        this.A03 = AnonymousClass19C.A00(r2);
        this.A04 = AnonymousClass0XM.A00(r2);
    }
}
