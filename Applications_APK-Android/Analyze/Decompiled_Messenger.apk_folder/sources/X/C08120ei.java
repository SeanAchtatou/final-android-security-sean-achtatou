package X;

import android.os.SystemProperties;
import java.util.Locale;

/* renamed from: X.0ei  reason: invalid class name and case insensitive filesystem */
public final class C08120ei {
    public String A00 = "N/A";
    public String A01 = "others";

    public C08120ei() {
        String str = SystemProperties.get("ro.board.platform");
        if ((str != null && !str.isEmpty()) || (((str = SystemProperties.get("ro.mediatek.platform")) != null && !str.isEmpty()) || ((str = SystemProperties.get("ro.mediatek.hardware")) != null && !str.isEmpty()))) {
            String lowerCase = str.toLowerCase(Locale.ENGLISH);
            if (lowerCase.startsWith("msm") || lowerCase.startsWith("apq") || lowerCase.startsWith("sdm") || lowerCase.startsWith("sm")) {
                this.A01 = "qualcomm";
            } else if (lowerCase.startsWith("exynos")) {
                this.A01 = "samsung";
                String str2 = SystemProperties.get("ro.chipname");
                str2 = (str2 == null || str2.isEmpty()) ? SystemProperties.get("ro.hardware.chipname") : str2;
                if (str2 != null) {
                    lowerCase = str2;
                }
            } else if (lowerCase.startsWith("mt")) {
                this.A01 = "mediatek";
            } else if (lowerCase.startsWith("sc")) {
                this.A01 = "spreadtrum";
            } else if (lowerCase.startsWith("hi") || lowerCase.startsWith("kirin")) {
                this.A01 = "hisilicon";
            } else if (lowerCase.startsWith("rk")) {
                this.A01 = "rockchip";
            } else if (lowerCase.startsWith("bcm")) {
                this.A01 = "broadcom";
            }
            this.A00 = lowerCase;
        }
    }
}
