package X;

import com.google.common.collect.ImmutableSet;

/* renamed from: X.14r  reason: invalid class name and case insensitive filesystem */
public final class C186814r implements C05700aB {
    public static final AnonymousClass1Y8 A00;
    public static final AnonymousClass1Y8 A01;
    public static final AnonymousClass1Y8 A02;
    public static final AnonymousClass1Y8 A03;
    public static final AnonymousClass1Y8 A04;
    private static final AnonymousClass1Y8 A05;

    static {
        AnonymousClass1Y8 A0D = C04350Ue.A0B.A09("onboarding/");
        A05 = A0D;
        A01 = A0D.A09("is_onboarding_required");
        AnonymousClass1Y8 r1 = A05;
        A00 = r1.A09("is_onboarding_eligiblity_confirmed");
        A04 = r1.A09("run_contacts_upload_on_progress_screen_start");
        A02 = r1.A09("manually_update_progress_screen_view_states");
        A03 = r1.A09("onboarding_completed_timestamp");
    }

    public static final C186814r A00() {
        return new C186814r();
    }

    public ImmutableSet AzO() {
        return ImmutableSet.A04(A05);
    }
}
