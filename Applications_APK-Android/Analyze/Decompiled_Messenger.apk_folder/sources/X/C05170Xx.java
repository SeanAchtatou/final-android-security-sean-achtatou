package X;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.LongSparseArray;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Xx  reason: invalid class name and case insensitive filesystem */
public final class C05170Xx {
    private static final C07540dj A0B = new C07540dj();
    private static volatile C05170Xx A0C;
    public long A00;
    public AnonymousClass0UN A01;
    public boolean A02;
    public boolean A03;
    public int[] A04 = null;
    public int[] A05 = null;
    private Paint A06;
    private Paint A07;
    private TriState A08;
    private TriState A09;
    public final CountDownLatch A0A = new CountDownLatch(1);

    public void A06(Resources resources) {
        Class cls;
        try {
            if (Build.VERSION.SDK_INT >= 24) {
                try {
                    cls = Class.forName(C22298Ase.$const$string(AnonymousClass1Y3.A1P));
                } catch (ClassNotFoundException unused) {
                    cls = Resources.class;
                }
            } else {
                cls = Resources.class;
            }
            Field declaredField = cls.getDeclaredField("sPreloadedDrawables");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(null);
            if (obj instanceof LongSparseArray) {
                if (!(obj instanceof C07020cT)) {
                    declaredField.set(null, new C07020cT((LongSparseArray) obj, this, resources));
                }
            } else if (obj instanceof LongSparseArray[]) {
                LongSparseArray[] longSparseArrayArr = (LongSparseArray[]) obj;
                for (int i = 0; i < longSparseArrayArr.length; i++) {
                    LongSparseArray longSparseArray = longSparseArrayArr[i];
                    if (!(longSparseArray instanceof C07020cT)) {
                        longSparseArrayArr[i] = new C07020cT(longSparseArray, this, resources);
                    }
                }
            }
        } catch (Exception e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, this.A01)).softReport("DrawableLoader", e.getMessage(), e.getCause());
        }
        try {
            Field declaredField2 = AssetManager.class.getDeclaredField("mThemeCookies");
            declaredField2.setAccessible(true);
            ((ArrayList) declaredField2.get(resources.getAssets())).clear();
        } catch (Exception unused2) {
        }
    }

    private Paint A00() {
        if (this.A08 == TriState.UNSET) {
            this.A08 = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A01)).Aeq(AnonymousClass0jG.A04);
        }
        if (this.A08 == TriState.YES && this.A06 == null) {
            Paint paint = new Paint();
            this.A06 = paint;
            paint.setColor(-2013200640);
        }
        return this.A06;
    }

    private Paint A01() {
        if (this.A09 == TriState.UNSET) {
            this.A09 = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A01)).Aeq(AnonymousClass0jG.A07);
        }
        if (this.A09 == TriState.YES && this.A07 == null) {
            Paint paint = new Paint();
            this.A07 = paint;
            paint.setColor(-1996553985);
        }
        return this.A07;
    }

    public static final C05170Xx A02(AnonymousClass1XY r4) {
        if (A0C == null) {
            synchronized (C05170Xx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r4);
                if (A002 != null) {
                    try {
                        A0C = new C05170Xx(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    private AnonymousClass3AV A03(int i, Paint paint) {
        return new AnonymousClass3AV((AnonymousClass1MZ) AnonymousClass1XX.A03(AnonymousClass1Y3.AF0, this.A01), (Executor) AnonymousClass1XX.A03(AnonymousClass1Y3.BU0, this.A01), i, paint, ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A01)).Aem(2306124617334390920L));
    }

    public Drawable A04(int i, Resources resources) {
        Object obj;
        long j;
        try {
            CountDownLatch countDownLatch = this.A0A;
            if (this.A03) {
                j = 86400000;
            } else if (!this.A02) {
                j = this.A00;
            } else {
                j = 0;
            }
            if (!countDownLatch.await(j, TimeUnit.MILLISECONDS)) {
                this.A02 = true;
            }
        } catch (InterruptedException e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, this.A01)).softReport("DrawableLoader", "Interrupted waiting on strings latch", e);
        }
        C07540dj r3 = A0B;
        synchronized (r3) {
            obj = r3.A00.get(i, null);
        }
        WeakReference weakReference = (WeakReference) obj;
        if (weakReference != null) {
            Drawable.ConstantState constantState = (Drawable.ConstantState) weakReference.get();
            if (constantState != null) {
                return constantState.newDrawable(resources);
            }
            r3.A00(i);
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ALPHA_8;
        BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, i, options));
        WeakReference weakReference2 = new WeakReference(bitmapDrawable.getConstantState());
        synchronized (r3) {
            r3.A00.put(i, weakReference2);
        }
        return bitmapDrawable;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v21, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v23, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v56, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v57, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v63, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v66, resolved type: android.graphics.drawable.Drawable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: X.3AV} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v71, resolved type: android.graphics.drawable.Drawable} */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01c2, code lost:
        if (r3.equals("app-kf-network") == false) goto L_0x01c4;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x026f A[Catch:{ IOException | XmlPullParserException -> 0x027b }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x016b A[Catch:{ IOException | XmlPullParserException -> 0x027b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable A05(int r14, android.content.res.Resources r15) {
        /*
            r13 = this;
            java.lang.String r3 = "DrawableLoader"
            r5 = 2
            r4 = 1
            java.util.concurrent.CountDownLatch r6 = r13.A0A     // Catch:{ InterruptedException -> 0x0022 }
            boolean r0 = r13.A03     // Catch:{ InterruptedException -> 0x0022 }
            if (r0 == 0) goto L_0x000e
            r0 = 86400000(0x5265c00, double:4.2687272E-316)
            goto L_0x0014
        L_0x000e:
            boolean r0 = r13.A02     // Catch:{ InterruptedException -> 0x0022 }
            if (r0 != 0) goto L_0x001f
            long r0 = r13.A00     // Catch:{ InterruptedException -> 0x0022 }
        L_0x0014:
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x0022 }
            boolean r0 = r6.await(r0, r2)     // Catch:{ InterruptedException -> 0x0022 }
            if (r0 != 0) goto L_0x0032
            r13.A02 = r4     // Catch:{ InterruptedException -> 0x0022 }
            goto L_0x0032
        L_0x001f:
            r0 = 0
            goto L_0x0014
        L_0x0022:
            r2 = move-exception
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r13.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.09P r1 = (X.AnonymousClass09P) r1
            java.lang.String r0 = "Interrupted waiting on strings latch"
            r1.softReport(r3, r0, r2)
        L_0x0032:
            X.0dj r2 = X.C05170Xx.A0B
            r1 = 0
            monitor-enter(r2)
            android.util.SparseArray r0 = r2.A00     // Catch:{ all -> 0x028c }
            java.lang.Object r0 = r0.get(r14, r1)     // Catch:{ all -> 0x028c }
            monitor-exit(r2)
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0
            r6 = r15
            if (r0 == 0) goto L_0x0068
            java.lang.Object r0 = r0.get()
            android.graphics.drawable.Drawable$ConstantState r0 = (android.graphics.drawable.Drawable.ConstantState) r0
            if (r0 == 0) goto L_0x0065
            android.graphics.drawable.Drawable r0 = r0.newDrawable(r15)     // Catch:{ NullPointerException -> 0x004f }
            return r0
        L_0x004f:
            r2 = move-exception
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r13.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.09P r1 = (X.AnonymousClass09P) r1
            java.lang.String r0 = "Unable to create new drawable from cached state"
            r1.softReport(r3, r0, r2)
            X.0dj r0 = X.C05170Xx.A0B
            r0.A00(r14)
            goto L_0x0068
        L_0x0065:
            r2.A00(r14)
        L_0x0068:
            java.lang.String r3 = r15.getString(r14)
            int r2 = r3.length()
            if (r2 <= r4) goto L_0x0110
            char r1 = r3.charAt(r4)
            r0 = 124(0x7c, float:1.74E-43)
            if (r1 != r0) goto L_0x0110
            r4 = 0
            int r0 = r3.codePointAt(r4)
            char r1 = (char) r0
            r0 = 78
            if (r1 == r0) goto L_0x00e4
            r0 = 82
            if (r1 != r0) goto L_0x0239
            android.graphics.Paint r0 = r13.A01()
            X.3AV r0 = r13.A03(r14, r0)
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r13.A01
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.1Yd r4 = (X.C25051Yd) r4
            r1 = 2306124617334390920(0x2001001f00000088, double:1.5849415049136788E-154)
            boolean r5 = r4.Aem(r1)
            java.lang.String r1 = "\\|"
            java.lang.String[] r4 = r3.split(r1)
            int r2 = r4.length
            r1 = 6
            if (r2 != r1) goto L_0x022d
            r1 = 1
            r3 = r4[r1]
            r1 = 2
            r2 = r4[r1]
            r1 = 1
            java.lang.String r7 = X.AnonymousClass24H.A01(r3, r2, r5, r1)
            X.C000300h.A01(r7)
            r1 = 3
            r1 = r4[r1]
            int r8 = java.lang.Integer.parseInt(r1)
            r1 = 4
            r1 = r4[r1]
            int r9 = java.lang.Integer.parseInt(r1)
            r1 = 5
        L_0x00ca:
            r1 = r4[r1]
            int r10 = java.lang.Integer.parseInt(r1)
            r11 = 0
            r12 = 0
            r5 = r0
            r5.A03(r6, r7, r8, r9, r10, r11, r12)
        L_0x00d6:
            X.0dj r3 = X.C05170Xx.A0B
            java.lang.ref.WeakReference r2 = new java.lang.ref.WeakReference
            android.graphics.drawable.Drawable$ConstantState r1 = r0.getConstantState()
            r2.<init>(r1)
            monitor-enter(r3)
            goto L_0x0223
        L_0x00e4:
            android.graphics.Paint r0 = r13.A00()
            X.3AV r0 = r13.A03(r14, r0)
            java.lang.String r1 = "\\|"
            java.lang.String[] r4 = r3.split(r1)
            int r2 = r4.length
            r1 = 5
            if (r2 != r1) goto L_0x0245
            java.lang.String r3 = "https://lookaside.facebook.com/assets/"
            r1 = 1
            r2 = r4[r1]
            java.lang.String r1 = "/"
            java.lang.String r7 = X.AnonymousClass08S.A0P(r3, r2, r1)
            r1 = r4[r5]
            int r8 = java.lang.Integer.parseInt(r1)
            r1 = 3
            r1 = r4[r1]
            int r9 = java.lang.Integer.parseInt(r1)
            r1 = 4
            goto L_0x00ca
        L_0x0110:
            r0 = 17
            if (r2 != r0) goto L_0x015a
            r0 = 0
            char r0 = r3.charAt(r0)
            r1 = 70
            if (r0 != r1) goto L_0x015a
            r0 = 17
            if (r2 != r0) goto L_0x0251
            r0 = 0
            char r0 = r3.charAt(r0)
            if (r0 != r1) goto L_0x0251
            r0 = 9
            java.lang.String r1 = r3.substring(r4, r0)
            r0 = 16
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1, r0)
            int r2 = r0.intValue()
            r1 = 9
            r0 = 17
            java.lang.String r1 = r3.substring(r1, r0)
            r0 = 16
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1, r0)
            int r3 = r0.intValue()
            X.3DN r0 = new X.3DN
            r1 = 0
            android.graphics.drawable.Drawable r2 = X.AnonymousClass3DO.A01(r15, r2, r1)
            android.graphics.drawable.Drawable r1 = X.AnonymousClass3DO.A01(r15, r3, r1)
            r0.<init>(r2, r1)
            goto L_0x00d6
        L_0x015a:
            android.content.res.XmlResourceParser r5 = r15.getXml(r14)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
        L_0x015e:
            int r2 = r5.next()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r1 = 2
            if (r2 == r1) goto L_0x0169
            r0 = 1
            if (r2 == r0) goto L_0x0169
            goto L_0x015e
        L_0x0169:
            if (r2 != r1) goto L_0x026f
            java.lang.String r3 = r5.getName()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            int r0 = r3.hashCode()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            switch(r0) {
                case -1900499203: goto L_0x0177;
                case -651164030: goto L_0x0185;
                case -530250655: goto L_0x018f;
                case -183984096: goto L_0x0199;
                case 1121658663: goto L_0x01a3;
                case 1191572447: goto L_0x01ad;
                case 1454686952: goto L_0x01bb;
                default: goto L_0x0176;
            }     // Catch:{ IOException | XmlPullParserException -> 0x027b }
        L_0x0176:
            goto L_0x01c4
        L_0x0177:
            r0 = 818(0x332, float:1.146E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            boolean r0 = r3.equals(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r2 = 4
            if (r0 != 0) goto L_0x01c5
            goto L_0x01c4
        L_0x0185:
            java.lang.String r0 = "app-network"
            boolean r0 = r3.equals(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r2 = 2
            if (r0 != 0) goto L_0x01c5
            goto L_0x01c4
        L_0x018f:
            java.lang.String r0 = "app-fbicon"
            boolean r0 = r3.equals(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r2 = 5
            if (r0 != 0) goto L_0x01c5
            goto L_0x01c4
        L_0x0199:
            java.lang.String r0 = "app-region"
            boolean r0 = r3.equals(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r2 = 0
            if (r0 != 0) goto L_0x01c5
            goto L_0x01c4
        L_0x01a3:
            java.lang.String r0 = "app-tint"
            boolean r0 = r3.equals(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r2 = 1
            if (r0 != 0) goto L_0x01c5
            goto L_0x01c4
        L_0x01ad:
            r0 = 42
            java.lang.String r0 = X.C22298Ase.$const$string(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            boolean r0 = r3.equals(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r2 = 6
            if (r0 != 0) goto L_0x01c5
            goto L_0x01c4
        L_0x01bb:
            java.lang.String r0 = "app-kf-network"
            boolean r0 = r3.equals(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r2 = 3
            if (r0 != 0) goto L_0x01c5
        L_0x01c4:
            r2 = -1
        L_0x01c5:
            switch(r2) {
                case 0: goto L_0x01d3;
                case 1: goto L_0x01d9;
                case 2: goto L_0x01ca;
                case 3: goto L_0x01df;
                case 4: goto L_0x0216;
                case 5: goto L_0x020a;
                case 6: goto L_0x0210;
                default: goto L_0x01c8;
            }     // Catch:{ IOException | XmlPullParserException -> 0x027b }
        L_0x01c8:
            goto L_0x025f
        L_0x01ca:
            android.graphics.Paint r0 = r13.A00()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.3AV r0 = r13.A03(r14, r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            goto L_0x021e
        L_0x01d3:
            X.Dhp r0 = new X.Dhp     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r0.<init>()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            goto L_0x021e
        L_0x01d9:
            X.DHR r0 = new X.DHR     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r0.<init>()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            goto L_0x021e
        L_0x01df:
            int r1 = X.AnonymousClass1Y3.AF0     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.0UN r0 = r13.A01     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.1MZ r4 = (X.AnonymousClass1MZ) r4     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            int r1 = X.AnonymousClass1Y3.Ar2     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.0UN r0 = r13.A01     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.9rk r3 = (X.C208189rk) r3     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            int r1 = X.AnonymousClass1Y3.BU0     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.0UN r0 = r13.A01     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            java.util.concurrent.Executor r2 = (java.util.concurrent.Executor) r2     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            int r1 = X.AnonymousClass1Y3.BCt     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.0UN r0 = r13.A01     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.Bf5 r0 = new X.Bf5     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r0.<init>(r4, r2, r14, r3)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            goto L_0x021e
        L_0x020a:
            X.3DN r0 = new X.3DN     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r0.<init>()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            goto L_0x021e
        L_0x0210:
            android.graphics.drawable.StateListDrawable r0 = new android.graphics.drawable.StateListDrawable     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r0.<init>()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            goto L_0x021e
        L_0x0216:
            android.graphics.Paint r0 = r13.A01()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            X.3AV r0 = r13.A03(r14, r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
        L_0x021e:
            r0.inflate(r15, r5, r5)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            goto L_0x00d6
        L_0x0223:
            android.util.SparseArray r1 = r3.A00     // Catch:{ all -> 0x022a }
            r1.put(r14, r2)     // Catch:{ all -> 0x022a }
            monitor-exit(r3)
            return r0
        L_0x022a:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x022d:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Unexpected inline redrawable format: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r1.<init>(r0)
            throw r1
        L_0x0239:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Unknown custom drawable type: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r1.<init>(r0)
            throw r1
        L_0x0245:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Unexpected inline network drawable format: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r1.<init>(r0)
            throw r1
        L_0x0251:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Cannot parse inline value '"
            java.lang.String r0 = "'"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r3, r0)
            r2.<init>(r0)
            throw r2
        L_0x025f:
            org.xmlpull.v1.XmlPullParserException r2 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            java.lang.String r1 = r5.getPositionDescription()     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            java.lang.String r0 = ": invalid drawable tag "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r3)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r2.<init>(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            throw r2     // Catch:{ IOException | XmlPullParserException -> 0x027b }
        L_0x026f:
            org.xmlpull.v1.XmlPullParserException r1 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r0 = 150(0x96, float:2.1E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            r1.<init>(r0)     // Catch:{ IOException | XmlPullParserException -> 0x027b }
            throw r1     // Catch:{ IOException | XmlPullParserException -> 0x027b }
        L_0x027b:
            r3 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r1 = "Failed to load XML for resource: "
            java.lang.String r0 = r15.getResourceEntryName(r14)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0, r3)
            throw r2
        L_0x028c:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05170Xx.A05(int, android.content.res.Resources):android.graphics.drawable.Drawable");
    }

    private C05170Xx(AnonymousClass1XY r5) {
        int[] iArr;
        int[] iArr2;
        TriState triState = TriState.UNSET;
        this.A08 = triState;
        this.A09 = triState;
        this.A01 = new AnonymousClass0UN(4, r5);
        Class<?> cls = Class.forName("com.facebook.R");
        try {
            Field declaredField = cls.getDeclaredField("custom_drawables");
            declaredField.setAccessible(true);
            iArr = (int[]) declaredField.get(null);
            Arrays.sort(iArr);
        } catch (Exception unused) {
            iArr = null;
        }
        try {
            this.A04 = iArr;
            try {
                Field declaredField2 = cls.getDeclaredField("grayscale_images");
                declaredField2.setAccessible(true);
                iArr2 = (int[]) declaredField2.get(null);
                Arrays.sort(iArr2);
            } catch (Exception unused2) {
                iArr2 = null;
            }
            this.A05 = iArr2;
        } catch (Exception unused3) {
            this.A04 = null;
            this.A05 = null;
        }
    }
}
