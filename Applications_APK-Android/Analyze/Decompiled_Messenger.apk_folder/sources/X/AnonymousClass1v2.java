package X;

import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.send.PendingSendQueueKey;
import com.google.common.base.Objects;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: X.1v2  reason: invalid class name */
public final class AnonymousClass1v2 {
    public int A00 = 0;
    public long A01;
    public String A02;
    public final long A03;
    public final AnonymousClass069 A04;
    public final PendingSendQueueKey A05;
    public final LinkedList A06 = new LinkedList();
    public volatile long A07;
    public volatile boolean A08;

    public static synchronized void A00(AnonymousClass1v2 r2) {
        synchronized (r2) {
            r2.A02 = null;
            r2.A00 = 0;
            r2.A01 = -1;
        }
    }

    public synchronized void A01(Message message) {
        this.A06.add(0, message);
    }

    public synchronized boolean A02() {
        return this.A06.isEmpty();
    }

    public synchronized boolean A03(String str) {
        Iterator it = this.A06.iterator();
        while (it.hasNext()) {
            if (Objects.equal(((Message) it.next()).A0w, str)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    public AnonymousClass1v2(AnonymousClass069 r3, PendingSendQueueKey pendingSendQueueKey) {
        this.A04 = r3;
        this.A05 = pendingSendQueueKey;
        this.A03 = r3.now();
    }
}
