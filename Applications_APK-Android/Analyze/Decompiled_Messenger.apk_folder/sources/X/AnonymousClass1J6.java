package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1J6  reason: invalid class name */
public final class AnonymousClass1J6 extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public InboxUnitThreadItem A01;
    @Comparable(type = 13)
    public AnonymousClass1BN A02;
    @Comparable(type = 13)
    public MigColorScheme A03;

    public AnonymousClass1J6(Context context) {
        super("M4ThreadItemCallToActionComponent");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
