package X;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0Vg  reason: invalid class name and case insensitive filesystem */
public final class C04570Vg implements ThreadFactory {
    public final int A00;
    private final String A01;
    private final AtomicInteger A02 = new AtomicInteger(1);

    public Thread newThread(Runnable runnable) {
        String str = this.A01;
        if (!AnonymousClass0YA.A00) {
            return new AnonymousClass0YD(new AnonymousClass0YB(this, runnable), AnonymousClass08S.A09(str, this.A02.getAndIncrement()));
        }
        throw new IllegalStateException(AnonymousClass08S.A0J("You're trying to create a background thread in a test, add a @Rule ExecutorsCleanup to ensure it's properly cleaned up after the test\nName prefix is: ", str));
    }

    public C04570Vg(String str, int i) {
        this.A01 = str;
        this.A00 = i;
    }
}
