package X;

import java.util.Arrays;

/* renamed from: X.1J0  reason: invalid class name */
public final class AnonymousClass1J0 {
    public static final AnonymousClass1J0 A09 = new AnonymousClass1J0(56, 38, 12, 2.5f, 44.0f, 24, 18, 3.0f, 3.0f);
    public static final AnonymousClass1J0 A0A = new AnonymousClass1J0(40, 26, 10, 2.25f, 30.0f, 20, 15, 3.0f, 3.0f);
    public static final AnonymousClass1J0 A0B = new AnonymousClass1J0(36, 24, 9, 2.0f, 28.0f, 16, 12, 3.0f, 3.0f);
    public static final AnonymousClass1J0 A0C = new AnonymousClass1J0(100, 66, 18, 4.0f, 80.0f, 36, 28, 3.0f, 6.0f);
    public final float A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1J0 r5 = (AnonymousClass1J0) obj;
            if (!(this.A05 == r5.A05 && this.A08 == r5.A08 && this.A04 == r5.A04 && Float.compare(r5.A03, this.A03) == 0 && Float.compare(r5.A02, this.A02) == 0 && this.A07 == r5.A07 && this.A06 == r5.A06 && this.A00 == r5.A00 && this.A01 == r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A05), Integer.valueOf(this.A08), Integer.valueOf(this.A04), Float.valueOf(this.A03), Float.valueOf(this.A02), Integer.valueOf(this.A07), Integer.valueOf(this.A06), Float.valueOf(this.A00), Float.valueOf(this.A01)});
    }

    private AnonymousClass1J0(int i, int i2, int i3, float f, float f2, int i4, int i5, float f3, float f4) {
        this.A05 = i;
        this.A08 = i2;
        this.A04 = i3;
        this.A03 = f;
        this.A02 = f2;
        this.A07 = i4;
        this.A06 = i5;
        this.A00 = f3;
        this.A01 = f4;
    }

    public AnonymousClass1J0(C73983h5 r2) {
        this.A05 = r2.A05;
        this.A08 = r2.A08;
        this.A04 = r2.A04;
        this.A03 = r2.A03;
        this.A02 = r2.A02;
        this.A07 = r2.A07;
        this.A06 = r2.A06;
        this.A00 = r2.A00;
        this.A01 = r2.A01;
    }
}
