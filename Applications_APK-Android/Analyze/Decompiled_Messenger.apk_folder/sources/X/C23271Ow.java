package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.1Ow  reason: invalid class name and case insensitive filesystem */
public interface C23271Ow {
    public static final long A00 = TimeUnit.MINUTES.toMillis(5);

    long Ago();

    double AvD();
}
