package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;
import org.apache.http.message.BasicHeader;

@Singleton
/* renamed from: X.0xx  reason: invalid class name and case insensitive filesystem */
public final class C16900xx {
    private static volatile C16900xx A05;
    public final AnonymousClass069 A00;
    public final C001500z A01;
    public final C25051Yd A02;
    public final FbSharedPreferences A03;
    private final AnonymousClass1YI A04;

    public static final C16900xx A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C16900xx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C16900xx(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public String A02() {
        C001500z r1 = this.A01;
        if (r1 == C001500z.A07 || r1 == C001500z.A08) {
            return this.A02.B4E(845567391498396L, null);
        }
        return null;
    }

    private C16900xx(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass0WT.A00(r2);
        this.A01 = AnonymousClass0UU.A05(r2);
        this.A03 = FbSharedPreferencesModule.A00(r2);
        this.A04 = AnonymousClass0WA.A00(r2);
        this.A00 = AnonymousClass067.A03(r2);
    }

    public ImmutableList A01() {
        String A022 = A02();
        if (!this.A04.AbO(255, false) || C06850cB.A0A(A022)) {
            return null;
        }
        return ImmutableList.of(new BasicHeader(C99084oO.$const$string(42), A022));
    }
}
