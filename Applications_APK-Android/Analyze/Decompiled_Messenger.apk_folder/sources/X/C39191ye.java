package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1ye  reason: invalid class name and case insensitive filesystem */
public final class C39191ye {
    public final ImmutableList A00;

    public C39191ye(ImmutableList immutableList) {
        Preconditions.checkNotNull(immutableList);
        this.A00 = immutableList;
    }
}
