package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1kF  reason: invalid class name and case insensitive filesystem */
public final class C31721kF implements C16810xo {
    private static volatile C31721kF A05;
    private long A00;
    private long A01;
    private AnonymousClass123 A02;
    private C16810xo A03;
    private AnonymousClass126 A04;

    public int AfY() {
        return 2;
    }

    public synchronized AnonymousClass123 AvW() {
        C16810xo r5 = this.A03;
        if (r5 == null) {
            return null;
        }
        long nanoTime = (System.nanoTime() - this.A00) / 1000000000;
        if (this.A02 == null || nanoTime >= 2) {
            this.A02 = r5.AvW();
            this.A00 = System.nanoTime();
        }
        return this.A02;
    }

    public synchronized AnonymousClass126 B6E() {
        C16810xo r5 = this.A03;
        if (r5 == null) {
            return null;
        }
        long nanoTime = (System.nanoTime() - this.A01) / 1000000000;
        if (this.A04 == null || nanoTime >= 2) {
            this.A04 = r5.B6E();
            this.A01 = System.nanoTime();
        }
        return this.A04;
    }

    public int B9x() {
        return 12;
    }

    public boolean isEnabled() {
        return true;
    }

    public static final C31721kF A00(AnonymousClass1XY r3) {
        if (A05 == null) {
            synchronized (C31721kF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A05 = new C31721kF();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public void C7c(C16810xo r1) {
        this.A03 = r1;
    }
}
