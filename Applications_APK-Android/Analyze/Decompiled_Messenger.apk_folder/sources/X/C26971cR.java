package X;

/* renamed from: X.1cR  reason: invalid class name and case insensitive filesystem */
public enum C26971cR {
    NO_DATA,
    STALE_DATA_NEEDS_FULL_SERVER_FETCH,
    STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH,
    UP_TO_DATE
}
