package X;

import android.content.Context;
import android.os.Looper;
import com.facebook.common.build.BuildConstants;
import com.google.common.base.Preconditions;
import java.util.Map;

/* renamed from: X.1Xb  reason: invalid class name and case insensitive filesystem */
public final class C24781Xb extends C24791Xc implements AnonymousClass1XY {
    private static final Thread A09;
    private Map A00;
    private Map A01;
    private Map A02;
    private boolean A03;
    public final Context A04;
    private final C24821Xf A05;
    private final C24811Xe A06;
    private final ThreadLocal A07 = new C24801Xd(this);
    private final boolean A08;

    public C24781Xb getScopeUnawareInjector() {
        return this;
    }

    private void A05() {
        if (!this.A03) {
            throw new RuntimeException("Called injector during binding");
        }
    }

    public AnonymousClass1XY getApplicationInjector() {
        return this.A05;
    }

    public C24811Xe getInjectorThreadStack() {
        Thread thread = A09;
        C000300h.A01(thread);
        if (Thread.currentThread() == thread) {
            return this.A06;
        }
        return (C24811Xe) this.A07.get();
    }

    public C24891Xn getScope(Class cls) {
        Object obj = this.A02.get(cls);
        C000300h.A01(obj);
        return (C24891Xn) obj;
    }

    static {
        Thread thread;
        if (Looper.getMainLooper() == null) {
            thread = null;
        } else {
            thread = Looper.getMainLooper().getThread();
        }
        A09 = thread;
    }

    public C24781Xb(Context context) {
        boolean z = false;
        C005505z.A03("FbInjectorImpl.init", 1732786518);
        try {
            this.A04 = context;
            this.A08 = BuildConstants.isInternalBuild();
            this.A06 = new C24811Xe(context);
            this.A05 = new C24821Xf(this, context);
            Preconditions.checkArgument(context == context.getApplicationContext() ? true : z);
            AnonymousClass1Y2 A012 = new C24871Xk(this).A01();
            this.A00 = A012.A00;
            this.A02 = A012.A03;
            this.A01 = A012.A01;
            this.A03 = true;
        } finally {
            C005505z.A00(-162128466);
        }
    }

    private C24851Xi A04() {
        A05();
        return getInjectorThreadStack().A01();
    }

    public AnonymousClass0US getLazy(C22916BKm bKm) {
        return EIG.A00(getProvider(bKm), getScopeAwareInjector());
    }

    public C04310Tq getProvider(C22916BKm bKm) {
        A05();
        if (this.A08) {
            BM4.A01(C38511xY.PROVIDER_GET, bKm);
        }
        try {
            B3Y b3y = (B3Y) this.A00.get(Integer.valueOf(AnonymousClass1Y3.A00(bKm)));
            if (b3y != null) {
                return b3y.A00();
            }
            throw new C38471xU("No provider bound for " + bKm);
        } finally {
            if (this.A08) {
                BM4.A00();
            }
        }
    }

    public C24851Xi getScopeAwareInjector() {
        C24851Xi A042 = A04();
        if (A042 != null) {
            return A042;
        }
        throw new IllegalStateException("Should never call getScopeAwareInjector without an active ThreadStack");
    }

    public Object getInstance(int i) {
        A05();
        Map map = this.A00;
        Integer valueOf = Integer.valueOf(i);
        if (map.containsKey(valueOf)) {
            Object obj = this.A00.get(valueOf);
            C000300h.A01(obj);
            return ((B3Y) obj).A00().get();
        }
        throw new C38471xU(AnonymousClass08S.A0B("No provider bound for :", i, " Map has # bindings: ", this.A00.size()));
    }

    public Object getInstance(C22916BKm bKm) {
        return getProvider(bKm).get();
    }
}
