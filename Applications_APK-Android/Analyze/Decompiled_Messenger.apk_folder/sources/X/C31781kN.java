package X;

import android.app.Activity;
import android.util.SparseArray;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1kN  reason: invalid class name and case insensitive filesystem */
public final class C31781kN {
    private static volatile C31781kN A02;
    public final SparseArray A00 = new SparseArray();
    public final AnonymousClass09P A01;

    public synchronized boolean A01(Activity activity, Object obj) {
        List list = (List) this.A00.get(activity.getTaskId());
        if (list == null) {
            return false;
        }
        return list.contains(obj);
    }

    public static final C31781kN A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C31781kN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C31781kN(C04750Wa.A01(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C31781kN(AnonymousClass09P r2) {
        this.A01 = r2;
    }
}
