package X;

/* renamed from: X.1iu  reason: invalid class name and case insensitive filesystem */
public final class C30951iu {
    public long A00;
    public long A01;
    public long A02;
    public boolean A03;

    public C30961iv A00() {
        long j = this.A00;
        if (j != 0) {
            if (this.A01 == 0) {
                this.A01 = j;
            }
            if (this.A02 == 0) {
                this.A02 = this.A01;
            }
            return new C30961iv(j, this.A01, this.A02, this.A03);
        }
        throw new IllegalArgumentException("Should set at least maxSizeBytes");
    }
}
