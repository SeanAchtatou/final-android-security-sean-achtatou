package X;

/* renamed from: X.0OV  reason: invalid class name */
public final class AnonymousClass0OV {
    public static final Class A00 = AnonymousClass0OV.class;

    public static void A00(StringBuilder sb, int i, String str, String str2) {
        sb.append("Thread trace:(");
        sb.append(i);
        sb.append(")");
        if (i < 10) {
            sb.append("    ");
        } else if (i < 100) {
            sb.append("   ");
        } else if (i < 1000) {
            sb.append("  ");
        }
        sb.append("                 ");
        A01(sb, str, str2);
        sb.append("\n");
        sb.append(" .                   TOTAL   THREAD  ");
        A01(sb, str, str2);
        sb.append("\n");
    }

    private static void A01(StringBuilder sb, String str, String str2) {
        char c;
        char c2;
        if (str != null && str2 != null) {
            int length = str.length();
            int length2 = str2.length();
            int max = Math.max(length, length2);
            int i = 0;
            char c3 = 0;
            char c4 = 0;
            while (i < max) {
                if (i < length) {
                    c = str.charAt(i);
                } else {
                    c = 0;
                }
                if (i < length2) {
                    c2 = str2.charAt(i);
                } else {
                    c2 = 0;
                }
                if (c3 == 0 && c == '|') {
                    c3 = 1;
                } else if (c3 == 1 && !Character.isWhitespace(c) && c != '|') {
                    c3 = 2;
                }
                if (c4 == 0 && c2 == '|') {
                    c4 = 1;
                } else if (c4 == 1 && !Character.isWhitespace(c2) && c2 != '|') {
                    c4 = 2;
                }
                if (c3 != 2 || c4 != 2) {
                    if (c3 == 1) {
                        sb.append(c);
                    } else if (c4 == 1) {
                        sb.append(c2);
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }
}
