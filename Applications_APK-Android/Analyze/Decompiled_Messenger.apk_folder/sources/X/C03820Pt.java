package X;

import android.os.Process;
import java.util.concurrent.ThreadFactory;

/* renamed from: X.0Pt  reason: invalid class name and case insensitive filesystem */
public final class C03820Pt implements ThreadFactory {
    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setName("Prflo:Upload");
        Process.setThreadPriority(10);
        return thread;
    }
}
