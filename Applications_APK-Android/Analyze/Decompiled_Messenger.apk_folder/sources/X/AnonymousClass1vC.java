package X;

import android.content.Context;
import android.view.View;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1vC  reason: invalid class name */
public final class AnonymousClass1vC extends C17770zR {
    public static final CallerContext A08 = CallerContext.A09("StoryViewerReactionStickerComponentSpec");
    @Comparable(type = 3)
    public int A00;
    @Comparable(type = 13)
    public View.OnTouchListener A01;
    public AnonymousClass303 A02;
    @Comparable(type = 13)
    public GSTModelShape1S0000000 A03;
    public AnonymousClass0UN A04;
    @Comparable(type = 13)
    public AnonymousClass44o A05;
    public Integer A06;
    public Integer A07;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1vC(Context context) {
        super("StoryViewerReactionStickerComponent");
        this.A04 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }

    public C17770zR A16() {
        AnonymousClass1vC r1 = (AnonymousClass1vC) super.A16();
        r1.A02 = null;
        r1.A06 = null;
        r1.A07 = null;
        return r1;
    }
}
