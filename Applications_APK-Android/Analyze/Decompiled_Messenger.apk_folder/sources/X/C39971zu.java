package X;

import android.content.Context;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1zu  reason: invalid class name and case insensitive filesystem */
public final class C39971zu implements C32821mO, C21791Is {
    public static final InterstitialTrigger A01 = new InterstitialTrigger(InterstitialTrigger.Action.A12);
    private final C86834Cf A00;

    public String Aqc() {
        return "6544";
    }

    public long Auk() {
        return 86400000;
    }

    public void Bwu(Context context, InterstitialTrigger interstitialTrigger, Object obj) {
    }

    public void C7z(long j) {
    }

    public static final C39971zu A00(AnonymousClass1XY r1) {
        return new C39971zu(r1);
    }

    public C70753bE B3h(InterstitialTrigger interstitialTrigger) {
        if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00.A00)).Aem(284636072710603L)) {
            return C70753bE.ELIGIBLE;
        }
        return C70753bE.INELIGIBLE;
    }

    public ImmutableList B7C() {
        return ImmutableList.of(A01);
    }

    private C39971zu(AnonymousClass1XY r2) {
        AnonymousClass1F1.A02(r2);
        this.A00 = new C86834Cf(r2);
    }
}
