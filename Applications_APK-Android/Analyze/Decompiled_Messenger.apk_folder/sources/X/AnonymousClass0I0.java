package X;

/* renamed from: X.0I0  reason: invalid class name */
public final class AnonymousClass0I0 {
    public static volatile boolean A00;

    public static long A00(long j) {
        if (C001000r.A02(1) && !A00) {
            A00 = true;
            AnonymousClass08Z.A03(1, "fburl.com/fbsystrace", AnonymousClass1Y3.A87);
            AnonymousClass08Z.A03(1, "USE fbsystrace", AnonymousClass1Y3.A87);
            AnonymousClass08Z.A03(1, "DO NOT USE systrace", AnonymousClass1Y3.A87);
        } else if (A00 && !C001000r.A02(1)) {
            A00 = false;
        }
        return j - System.nanoTime();
    }
}
