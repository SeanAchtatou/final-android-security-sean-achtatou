package X;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1d9  reason: invalid class name and case insensitive filesystem */
public final class C27411d9 {
    public final ThreadLocal A00 = new ThreadLocal();
    private final AtomicInteger A01 = new AtomicInteger();

    private void A00(AnonymousClass1XV r4, AnonymousClass8t3 r5) {
        boolean compareAndSet;
        C04260Tf r0 = r4.A04.A02;
        if (r0 != null) {
            AtomicInteger atomicInteger = r0.A03;
            boolean z = false;
            if (atomicInteger != null) {
                z = true;
            }
            if (z) {
                if (atomicInteger == null) {
                    compareAndSet = false;
                } else {
                    compareAndSet = atomicInteger.compareAndSet(1, 3);
                }
                if (!compareAndSet) {
                    return;
                }
            }
        }
        boolean z2 = false;
        if (r4.A04.A02 != null) {
            z2 = true;
        }
        if (!z2 && r5 != null) {
            r5.onChainActivate(r4);
        }
        ArrayList arrayList = (ArrayList) this.A00.get();
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.A00.set(arrayList);
        }
        arrayList.add(r4);
        AnonymousClass1XT r02 = C27401d8.A02;
        if (r02 != null) {
            r02.BN6(r4);
        }
    }

    public AnonymousClass1XV A01() {
        ArrayList arrayList = (ArrayList) this.A00.get();
        if (arrayList == null || arrayList.isEmpty()) {
            return null;
        }
        return (AnonymousClass1XV) arrayList.get(arrayList.size() - 1);
    }

    public AnonymousClass1XV A03(String str, int i) {
        C30234Es3 es3;
        C30233Es2 es2;
        int trackOnChainDeactivate;
        AnonymousClass1XV A012 = A01();
        String str2 = str;
        int i2 = i;
        if (A012 != null) {
            return A02(A012, str2, 3, i2);
        }
        AnonymousClass8t3 r1 = C27401d8.A01;
        C30231Erz erz = C27401d8.A03;
        if (erz == null || !erz.shouldFillReqChainProps(3, i2)) {
            es3 = Es1.A00;
        } else {
            es3 = new C30230Ery();
            erz.fillReqChainProps(es3, 3, i2);
        }
        AtomicInteger atomicInteger = null;
        if (erz == null || !erz.shouldFillReqContextProps(null, 3, i2)) {
            es2 = C30232Es0.A00;
        } else {
            es2 = new C30229Erx();
            erz.fillReqContextProps(es2, null, 3, i2);
        }
        if (r1 == null) {
            trackOnChainDeactivate = 0;
        } else {
            trackOnChainDeactivate = r1.trackOnChainDeactivate();
        }
        if (trackOnChainDeactivate == 1) {
            atomicInteger = new AtomicInteger(1);
        }
        AnonymousClass1XV r4 = new AnonymousClass1XV(str2, null, Thread.currentThread().getId(), this.A01.getAndIncrement(), 3, es3, es2, atomicInteger, i2, this);
        A00(r4, r1);
        return r4;
    }

    public AnonymousClass1XV A02(AnonymousClass1XV r18, String str, int i, int i2) {
        C30233Es2 es2;
        int i3;
        long id = Thread.currentThread().getId();
        int andIncrement = this.A01.getAndIncrement();
        C30231Erz erz = C27401d8.A03;
        AnonymousClass1XV r3 = r18;
        C30234Es3 es3 = r3.A05;
        C30231Erz erz2 = erz;
        int i4 = i2;
        int i5 = i;
        if (erz == null || !erz.shouldFillReqContextProps(r3, i5, i4)) {
            es2 = C30232Es0.A00;
        } else {
            es2 = new C30229Erx();
            erz2.fillReqContextProps(es2, r3, i5, i4);
        }
        AtomicInteger atomicInteger = r3.A07;
        if (atomicInteger == null || atomicInteger.get() <= 0) {
            atomicInteger = null;
        }
        AnonymousClass1XV r5 = new AnonymousClass1XV(str, r3.A04, id, andIncrement, i5, es3, es2, atomicInteger, i4, this);
        AtomicInteger atomicInteger2 = r5.A07;
        if (atomicInteger2 != null) {
            boolean z = false;
            if (r3.A04.A02 != null) {
                z = true;
            }
            if (!z || !r3.A00(2)) {
                do {
                    i3 = atomicInteger2.get();
                    if (i3 <= 0) {
                        break;
                    }
                } while (!atomicInteger2.compareAndSet(i3, i3 + 1));
            }
        }
        A00(r5, null);
        return r5;
    }
}
