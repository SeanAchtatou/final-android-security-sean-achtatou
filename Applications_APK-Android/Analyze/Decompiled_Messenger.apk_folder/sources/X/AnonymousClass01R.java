package X;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.util.TypedValue;
import java.io.File;

/* renamed from: X.01R  reason: invalid class name */
public class AnonymousClass01R {
    private static TypedValue A00;
    private static final Object A01 = new Object();

    public static int A00(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return context.getColor(i);
        }
        return context.getResources().getColor(i);
    }

    public static int A01(Context context, String str) {
        if (str != null) {
            return context.checkPermission(str, Process.myPid(), Process.myUid());
        }
        throw new IllegalArgumentException("permission is null");
    }

    public static ColorStateList A02(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        return context.getResources().getColorStateList(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    public static Drawable A03(Context context, int i) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21) {
            return context.getDrawable(i);
        }
        if (i2 < 16) {
            synchronized (A01) {
                if (A00 == null) {
                    A00 = new TypedValue();
                }
                context.getResources().getValue(i, A00, true);
                i = A00.resourceId;
            }
        }
        return context.getResources().getDrawable(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0040, code lost:
        return r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File A04(android.content.Context r4) {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x000b
            java.io.File r0 = r4.getNoBackupFilesDir()
            return r0
        L_0x000b:
            android.content.pm.ApplicationInfo r0 = r4.getApplicationInfo()
            java.io.File r4 = new java.io.File
            java.lang.String r1 = r0.dataDir
            java.lang.String r0 = "no_backup"
            r4.<init>(r1, r0)
            java.lang.Class<X.01R> r3 = X.AnonymousClass01R.class
            monitor-enter(r3)
            boolean r0 = r4.exists()     // Catch:{ all -> 0x0041 }
            if (r0 != 0) goto L_0x003f
            boolean r0 = r4.mkdirs()     // Catch:{ all -> 0x0041 }
            if (r0 != 0) goto L_0x003f
            boolean r0 = r4.exists()     // Catch:{ all -> 0x0041 }
            if (r0 != 0) goto L_0x003f
            java.lang.String r2 = "ContextCompat"
            java.lang.String r1 = "Unable to create files subdir "
            java.lang.String r0 = r4.getPath()     // Catch:{ all -> 0x0041 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0041 }
            android.util.Log.w(r2, r0)     // Catch:{ all -> 0x0041 }
            r4 = 0
            monitor-exit(r3)
            return r4
        L_0x003f:
            monitor-exit(r3)
            return r4
        L_0x0041:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01R.A04(android.content.Context):java.io.File");
    }

    public static void A05(Context context, Intent intent, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            context.startActivity(intent, bundle);
        } else {
            context.startActivity(intent);
        }
    }

    public static boolean A06(Context context) {
        if (Build.VERSION.SDK_INT >= 24) {
            return context.isDeviceProtectedStorage();
        }
        return false;
    }

    public static File[] A07(Context context) {
        if (Build.VERSION.SDK_INT >= 19) {
            return context.getExternalCacheDirs();
        }
        return new File[]{context.getExternalCacheDir()};
    }

    public static File[] A08(Context context, String str) {
        if (Build.VERSION.SDK_INT >= 19) {
            return context.getExternalFilesDirs(str);
        }
        return new File[]{context.getExternalFilesDir(str)};
    }
}
