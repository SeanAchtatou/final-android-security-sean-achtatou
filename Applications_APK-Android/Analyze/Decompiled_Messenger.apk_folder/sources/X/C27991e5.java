package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/* renamed from: X.1e5  reason: invalid class name and case insensitive filesystem */
public final class C27991e5 implements Application.ActivityLifecycleCallbacks {
    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onActivityPostCreated(Activity activity, Bundle bundle) {
        C14400tF.A01(activity, C14820uB.ON_CREATE);
    }

    public void onActivityPostResumed(Activity activity) {
        C14400tF.A01(activity, C14820uB.ON_RESUME);
    }

    public void onActivityPostStarted(Activity activity) {
        C14400tF.A01(activity, C14820uB.ON_START);
    }

    public void onActivityPreDestroyed(Activity activity) {
        C14400tF.A01(activity, C14820uB.ON_DESTROY);
    }

    public void onActivityPrePaused(Activity activity) {
        C14400tF.A01(activity, C14820uB.ON_PAUSE);
    }

    public void onActivityPreStopped(Activity activity) {
        C14400tF.A01(activity, C14820uB.ON_STOP);
    }
}
