package X;

import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1BC  reason: invalid class name */
public interface AnonymousClass1BC extends AnonymousClass1BD, AnonymousClass1BE, AnonymousClass1BF, AnonymousClass1BG, AnonymousClass1BH, AnonymousClass1BI, AnonymousClass1BJ, AnonymousClass1BK, AnonymousClass1BL, AnonymousClass1BM, AnonymousClass1BN, AnonymousClass1BO {
    void BC7();

    void BXM(ImmutableList immutableList);

    void Bai(InboxUnitItem inboxUnitItem);

    void Bbw(InboxUnitItem inboxUnitItem);

    boolean Bc6(InboxUnitItem inboxUnitItem);

    void BnL();

    void Bng(InboxUnitItem inboxUnitItem);
}
