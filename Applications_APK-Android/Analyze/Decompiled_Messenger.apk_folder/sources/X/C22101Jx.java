package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1Jx  reason: invalid class name and case insensitive filesystem */
public final class C22101Jx extends C17770zR {
    @Comparable(type = 13)
    public MigColorScheme A00;
    @Comparable(type = 13)
    public AnonymousClass10J A01;
    @Comparable(type = 13)
    public String A02;

    public C22101Jx() {
        super("M4ThreadItemTimestampContentComponent");
    }
}
