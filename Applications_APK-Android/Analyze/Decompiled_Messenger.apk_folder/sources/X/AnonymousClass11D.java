package X;

import java.util.List;

/* renamed from: X.11D  reason: invalid class name */
public final class AnonymousClass11D extends C17770zR {
    public C14940uO A00;
    public C14940uO A01;
    public C14950uP A02;
    public AnonymousClass6I0 A03;
    public List A04;
    public boolean A05;

    public static C37951we A02(AnonymousClass0p4 r3, String str) {
        C37951we r1 = new C37951we();
        r1.A3B(r3, 0, 0, new AnonymousClass11D(str));
        return r1;
    }

    public boolean A1K() {
        return true;
    }

    public static C37951we A00(AnonymousClass0p4 r4) {
        C37951we r1 = new C37951we();
        r1.A3B(r4, 0, 0, new AnonymousClass11D("Column"));
        return r1;
    }

    public static C37951we A01(AnonymousClass0p4 r3, int i, int i2) {
        C37951we r1 = new C37951we();
        r1.A3B(r3, i, i2, new AnonymousClass11D("Column"));
        return r1;
    }

    private AnonymousClass11D(String str) {
        super(str);
    }
}
