package X;

import java.io.File;

/* renamed from: X.0Yc  reason: invalid class name and case insensitive filesystem */
public final class C05220Yc implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.dialtone.DialtoneAsyncSignalFile$1";
    public final /* synthetic */ AnonymousClass0Z9 A00;

    public C05220Yc(AnonymousClass0Z9 r1) {
        this.A00 = r1;
    }

    public void run() {
        File[] listFiles;
        AnonymousClass0Z9 r6 = this.A00;
        boolean A03 = AnonymousClass0Z9.A03(r6);
        File[] listFiles2 = r6.A02.listFiles();
        if (listFiles2 != null) {
            for (File delete : listFiles2) {
                delete.delete();
            }
        }
        if (A03 && (listFiles = r6.A03.listFiles()) != null) {
            for (File delete2 : listFiles) {
                delete2.delete();
            }
        }
        C05230Yd.A00(r6);
    }
}
