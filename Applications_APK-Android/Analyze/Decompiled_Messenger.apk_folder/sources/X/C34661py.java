package X;

import io.card.payment.BuildConfig;
import org.codeaurora.Performance;
import org.json.JSONObject;

/* renamed from: X.1py  reason: invalid class name and case insensitive filesystem */
public final class C34661py implements C26381bM {
    public static boolean A00() {
        Class<Performance> cls = Performance.class;
        try {
            cls.toString();
            if (C34061oa.A01(cls, "perfLockAcquire", Integer.TYPE, int[].class)) {
                Class<Performance> cls2 = Performance.class;
                if (C34061oa.A01(cls, "perfLockRelease", new Class[0])) {
                    return true;
                }
            }
            return false;
        } catch (Error | Exception unused) {
            return false;
        }
    }

    public int AyN() {
        return 2;
    }

    public int AyO() {
        return 1;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", "qualcomm");
            jSONObject.put("framework", "Codeaurora");
            jSONObject.put("extra", BuildConfig.FLAVOR);
            return jSONObject.toString();
        } catch (Exception unused) {
            return BuildConfig.FLAVOR;
        }
    }

    public AnonymousClass0f7 AUy(C26401bO r5, C26501bY r6) {
        int[] Aet = r5.Aet(r6);
        if (Aet == null || Aet.length == 0) {
            return null;
        }
        return new C34681q0(new Performance(), r6.A01, Aet);
    }
}
