package X;

import java.lang.ref.ReferenceQueue;
import java.util.HashSet;

/* renamed from: X.0Yk  reason: invalid class name and case insensitive filesystem */
public final class C05300Yk {
    public boolean A00;
    public final int A01;
    public final ReferenceQueue A02;
    public final HashSet A03 = new HashSet();

    public C05300Yk(ReferenceQueue referenceQueue, int i) {
        this.A02 = referenceQueue;
        this.A01 = i;
    }
}
