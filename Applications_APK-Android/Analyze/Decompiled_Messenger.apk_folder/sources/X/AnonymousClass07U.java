package X;

import android.content.Context;
import com.facebook.xzdecoder.XzInputStream;
import java.io.InputStream;

/* renamed from: X.07U  reason: invalid class name */
public final class AnonymousClass07U extends AnonymousClass07Y {
    public InputStream A00(Context context) {
        return new XzInputStream(super.A00(context));
    }

    public AnonymousClass07U(String str, String str2) {
        super(str, str2);
    }
}
