package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0rd  reason: invalid class name and case insensitive filesystem */
public abstract class C13550rd extends C13140qm {
    public void A04(View view) {
        if (view != null) {
            ((ViewGroup) this.A00).addView(view);
        }
    }

    public void A06(C13140qm r3) {
        ((ViewGroup) this.A00).addView(r3.A00);
    }

    public /* bridge */ /* synthetic */ C13140qm A00(int i, int i2) {
        super.A00(i, i2);
        return this;
    }

    public final void A03(int i, int i2) {
        super.A00(i, i2);
    }

    public C13550rd(ViewGroup viewGroup) {
        super(viewGroup);
    }

    public /* bridge */ /* synthetic */ C13140qm A01(ViewGroup.LayoutParams layoutParams) {
        super.A01(layoutParams);
        return this;
    }

    public final void A05(ViewGroup.LayoutParams layoutParams) {
        super.A01(layoutParams);
    }
}
