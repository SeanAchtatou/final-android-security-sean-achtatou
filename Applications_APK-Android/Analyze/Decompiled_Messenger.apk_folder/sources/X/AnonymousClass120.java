package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.facebook.litho.ComponentHost;
import com.facebook.litho.LithoView;
import java.util.List;

/* renamed from: X.120  reason: invalid class name */
public final class AnonymousClass120 implements AnonymousClass1M8 {
    public void C3Y(Object obj) {
    }

    public void C5f(Object obj, float f) {
        Object obj2 = obj;
        if (obj instanceof ComponentHost) {
            ComponentHost componentHost = (ComponentHost) obj2;
            if (componentHost instanceof LithoView) {
                LithoView lithoView = (LithoView) componentHost;
                lithoView.A00 = (int) f;
                lithoView.requestLayout();
            } else {
                int top = componentHost.getTop();
                AnonymousClass1LA.A01(componentHost, componentHost.getLeft(), top, componentHost.getRight(), (int) (((float) top) + f), false);
            }
            List A0E = componentHost.A0E();
            if (A0E != null) {
                int width = componentHost.getWidth();
                int i = (int) f;
                for (int i2 = 0; i2 < A0E.size(); i2++) {
                    AnonymousClass1LA.A00((Drawable) A0E.get(i2), width, i);
                }
            }
        } else if (obj instanceof View) {
            View view = (View) obj2;
            int top2 = view.getTop();
            AnonymousClass1LA.A01(view, view.getLeft(), top2, view.getRight(), (int) (((float) top2) + f), false);
        } else if (obj instanceof Drawable) {
            Drawable drawable = (Drawable) obj2;
            AnonymousClass1LA.A00(drawable, drawable.getBounds().width(), (int) f);
        } else {
            throw new UnsupportedOperationException("Setting height on unsupported mount content: " + obj);
        }
    }

    public String getName() {
        return "height";
    }

    public float Ab1(C17730zN r2) {
        return (float) r2.A08.height();
    }

    public float Ab2(Object obj) {
        int height;
        if (obj instanceof View) {
            height = ((View) obj).getHeight();
        } else if (obj instanceof Drawable) {
            height = ((Drawable) obj).getBounds().height();
        } else {
            throw new UnsupportedOperationException("Getting height from unsupported mount content: " + obj);
        }
        return (float) height;
    }
}
