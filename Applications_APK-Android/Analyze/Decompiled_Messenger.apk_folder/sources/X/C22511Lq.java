package X;

import com.facebook.common.dextricks.DexLibLoader;
import com.facebook.common.dextricks.DexStore;
import com.facebook.common.dextricks.verifier.Verifier;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Lq  reason: invalid class name and case insensitive filesystem */
public final class C22511Lq extends C28791fN {
    public static final String __redex_internal_original_name = "com.facebook.debug.fps.FrameRateLogger$ScrollPerfRunnable";
    public final /* synthetic */ AnonymousClass1T0 A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C22511Lq(AnonymousClass1T0 r3) {
        super(C22511Lq.class.getSimpleName(), "ScrollPerfRunnable");
        this.A00 = r3;
    }

    private double A00(AnonymousClass184 r7) {
        int i;
        AnonymousClass15Q r2 = this.A00.A0G;
        int i2 = r7.A00;
        int i3 = 1;
        AnonymousClass15Q.A02(r2);
        if (1 != 0) {
            i3 = r2.A00;
        }
        double d = (((double) i2) / ((double) i3)) + 0.0d;
        AnonymousClass15Q r22 = this.A00.A0G;
        int i4 = r7.A01;
        AnonymousClass15Q.A02(r22);
        if (0 != 0) {
            i = r22.A00;
        } else {
            i = 1;
        }
        return d + (((double) i4) / ((double) i));
    }

    private double A01(AnonymousClass184 r6) {
        return this.A00.A0G.A03(r6.A00, true) + 0.0d + this.A00.A0G.A03(r6.A01, false);
    }

    private JSONObject A02(Map map) {
        JSONObject jSONObject = new JSONObject();
        for (C30442EwX ewX : map.keySet()) {
            double A002 = A00((AnonymousClass184) map.get(ewX));
            if (A002 > 0.0d) {
                jSONObject.put("gc", A002);
            }
        }
        return jSONObject;
    }

    private JSONObject A03(Map map, AnonymousClass184 r11) {
        JSONObject jSONObject = new JSONObject();
        double A002 = A00(r11);
        for (C30442EwX ewX : map.keySet()) {
            double A003 = A00((AnonymousClass184) map.get(ewX));
            if (A003 > 0.0d) {
                jSONObject.put("gc", A003);
                A002 -= A003;
            }
        }
        if (A002 > 0.0d) {
            jSONObject.put("unknown", A002);
        }
        return jSONObject;
    }

    private static void A04(C22521Lr r3, String str) {
        r3.A03(AnonymousClass08S.A0J(str, "sum"), 0);
        r3.A03(AnonymousClass08S.A0J(str, "sum_squared"), 0);
        r3.A03(AnonymousClass08S.A0J(str, "max"), 0);
        r3.A03(AnonymousClass08S.A0J(str, "count"), 0);
    }

    private static void A05(C22521Lr r5, String str) {
        String str2;
        long A002 = AnonymousClass00I.A00(str, Long.MIN_VALUE);
        if (A002 != Long.MIN_VALUE) {
            str2 = Long.toString(A002);
        } else {
            str2 = BuildConfig.FLAVOR;
        }
        r5.A04(str, str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short, long, java.util.concurrent.TimeUnit):void
     arg types: [int, int, int, long, java.util.concurrent.TimeUnit]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short, long, com.facebook.common.util.TriState):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short, long, java.util.concurrent.TimeUnit):void */
    public void run() {
        int i;
        List<String> list;
        boolean z;
        boolean z2;
        C73443g5 r0;
        String name;
        C22521Lr r3 = this.A00.A0K;
        r3.A00();
        r3.A03("time_since_startup", ((AnonymousClass4Q0) this.A00.A0R.get()).A01());
        r3.A01("1_frame_drop", A00(this.A00.A0M.A05));
        r3.A01("4_frame_drop", A01(this.A00.A0M.A04));
        AnonymousClass184 r02 = this.A00.A0M.A08;
        r3.A02("total_time_spent", r02.A00 + r02.A01);
        Preconditions.checkNotNull(r3.A02);
        Preconditions.checkState(r3.A04);
        r3.A02.BK9();
        r3.A02 = null;
        r3.A04 = false;
        r3.A03.markerEnd(r3.A00, r3.A01, (short) 2, this.A00.A02, TimeUnit.MILLISECONDS);
        C22521Lr r32 = this.A00.A0J;
        r32.A00();
        AnonymousClass184 r03 = this.A00.A0M.A08;
        r32.A02("total_time_spent", r03.A00 + r03.A01);
        AnonymousClass184 r04 = this.A00.A0N.A08;
        r32.A02("total_time_spent_uncapped", r04.A00 + r04.A01);
        r32.A03("time_since_startup", ((AnonymousClass4Q0) this.A00.A0R.get()).A01());
        AnonymousClass0Ud r6 = (AnonymousClass0Ud) this.A00.A0Q.get();
        r32.A03("time_since_resume", ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, r6.A02)).now() - r6.A0L);
        r32.A01("total_frames", A00(this.A00.A0M.A07));
        r32.A01("total_frames_uncapped", A00(this.A00.A0N.A07));
        r32.A02("scroll_action", 0);
        r32.A01("total_skipped_frames", A00(this.A00.A0M.A06));
        r32.A01("1_frame_drop", A00(this.A00.A0M.A05));
        r32.A01("4_frame_drop", A01(this.A00.A0M.A04));
        r32.A01("total_skipped_frames_uncapped", A00(this.A00.A0N.A06));
        r32.A01("1_frame_drop_uncapped", A00(this.A00.A0N.A05));
        r32.A01("4_frame_drop_uncapped", A01(this.A00.A0N.A04));
        A04(r32, "large_frame_drop_input_time_");
        A04(r32, "large_frame_drop_animation_time_");
        A04(r32, "large_frame_drop_traversal_time_");
        A04(r32, "large_frame_drop_commit_to_input_time_");
        AnonymousClass1T0 r1 = this.A00;
        C09390hE r4 = r1.A0F;
        AnonymousClass184 r9 = r1.A0M.A06;
        AnonymousClass15Q r62 = r1.A0G;
        int i2 = r9.A00;
        AnonymousClass15Q.A02(r62);
        int i3 = 1;
        if (1 != 0) {
            i3 = r62.A00;
        }
        double d = (((double) i2) / ((double) i3)) + 0.0d;
        AnonymousClass15Q r05 = this.A00.A0G;
        int i4 = r9.A01;
        AnonymousClass15Q.A02(r05);
        if (0 != 0) {
            i = r05.A00;
        } else {
            i = 1;
        }
        int ceil = (int) Math.ceil(d + (((double) i4) / ((double) i)));
        AnonymousClass184 r06 = this.A00.A0M.A08;
        long j = (long) (r06.A00 + r06.A01);
        if (C09390hE.A01(r4) != null) {
            r4.A06.A01("dropped_frames", "dropped: %d, total_time_spent: %d", Integer.valueOf(ceil), Long.valueOf(j));
        }
        AnonymousClass1T3 r07 = this.A00.A0P;
        AnonymousClass1T3.A01(r07);
        r32.A02("display_refresh_rate", (int) Math.floor((double) r07.A00));
        AnonymousClass1T3 r08 = this.A00.A0P;
        AnonymousClass1T3.A01(r08);
        r32.A02("sanitized_display_refresh_rate", (int) Math.floor((double) r08.A01));
        r32.A02("approximate_vsync_rate", (int) Math.floor((double) this.A00.A0P.A02()));
        try {
            AnonymousClass1T0 r09 = this.A00;
            JSONObject A03 = A03(r09.A0A, r09.A0M.A06);
            AnonymousClass1T0 r010 = this.A00;
            JSONObject A032 = A03(r010.A09, r010.A0M.A04);
            r32.A04("frame_drop_by_autoblame", A03.toString());
            r32.A04("large_frame_drop_by_autoblame", A032.toString());
            r32.A04("frame_drop_by_autoblame_overlapped", A02(this.A00.A0B).toString());
            r32.A04("large_frame_drop_by_autoblame_overlapped", A02(this.A00.A08).toString());
            r32.A04("total_frames_with_marker", A02(this.A00.A0C).toString());
        } catch (JSONException unused) {
        }
        A05(r32, "ro.hwui.texture_cache_size");
        this.A00.A0J.A04("ro.hwui.texture_cache_flushrate", AnonymousClass00I.A02("ro.hwui.texture_cache_flushrate"));
        A05(r32, "ro.hwui.layer_cache_size");
        A05(r32, "ro.hwui.r_buffer_cache_size");
        A05(r32, "ro.hwui.gradient_cache_size");
        A05(r32, "ro.hwui.path_cache_size");
        A05(r32, "ro.hwui.vertex_cache_size");
        A05(r32, "ro.hwui.patch_cache_size");
        A05(r32, "ro.hwui.drop_shadow_cache_size");
        A05(r32, "ro.hwui.fbo_cache_size");
        String errorMessage = ((AnonymousClass0F1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B79, this.A00.A05)).getErrorMessage();
        if (errorMessage != null && errorMessage.length() > 0) {
            r32.A04("JIT_ERRMSG", errorMessage);
        }
        C26322Cw1 cw1 = this.A00.A06;
        if (cw1 != null) {
            C26316Cvu cvu = cw1.A00.A02;
            if (cvu != null) {
                r0 = cvu.A00.A0N;
            } else {
                r0 = null;
            }
            if (r0 == null) {
                name = BuildConfig.FLAVOR;
            } else {
                name = r0.name();
            }
            list = Collections.singletonList(name);
        } else {
            list = null;
        }
        StringBuilder sb = new StringBuilder();
        AnonymousClass1T3 r011 = this.A00.A0P;
        AnonymousClass1T3.A01(r011);
        int i5 = (r011.A00 > r011.A01 ? 1 : (r011.A00 == r011.A01 ? 0 : -1));
        boolean z3 = false;
        if (i5 != 0) {
            z3 = true;
        }
        if (z3) {
            A06(sb, "fps_guessed");
        }
        if (DexLibLoader.deoptTaint) {
            A06(sb, "dex_unopt");
        }
        int i6 = DexLibLoader.getMainDexStoreLoadInformation().loadResult;
        if ((i6 & 128) != 0) {
            A06(sb, "oatmeal");
            if ((i6 & DexStore.LOAD_RESULT_OATMEAL_QUICKENED) != 0) {
                A06(sb, "quick");
            } else if ((i6 & 512) != 0) {
                A06(sb, "dex2oat_quick");
            }
            if ((i6 & 1024) != 0) {
                A06(sb, "mixed");
            }
            if ((i6 & 2048) != 0) {
                A06(sb, "quick_attempted");
            } else if ((i6 & DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED) != 0) {
                A06(sb, "dex2oat_quick_attempted");
            }
            if ((i6 & DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED) != 0) {
                A06(sb, "mixed_attempted");
            }
            if ((i6 & DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET) != 0) {
                A06(sb, "dex2oat_classpath");
            }
        }
        if ((i6 & DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP) != 0) {
            A06(sb, "periodic_pgo_profile_set");
        }
        Class<Verifier> cls = Verifier.class;
        synchronized (cls) {
            z = Verifier.sTriedDisableRuntimeVerification;
        }
        if (z) {
            synchronized (cls) {
                z2 = Verifier.sDisabledRuntimeVerification;
            }
            if (z2) {
                A06(sb, "disabled_rt_verifier");
            } else {
                A06(sb, "failed_disable_rt_verifier");
            }
        }
        int i7 = AnonymousClass1Y3.B79;
        AnonymousClass1T0 r42 = this.A00;
        if (((AnonymousClass0F1) AnonymousClass1XX.A02(3, i7, r42.A05)).BFX()) {
            A06(sb, "jit_enabled");
        } else {
            A06(sb, "jit_disabled");
        }
        C22531Ls r43 = r42.A04;
        if (r43.A02) {
            A06(sb, "dalvik.vm.usejit.enabled");
        } else {
            A06(sb, "dalvik.vm.usejit.disabled");
        }
        if (r43.A03) {
            A06(sb, "dalvik.vm.usejitprofiles.enabled");
        } else {
            A06(sb, "dalvik.vm.usejitprofiles.disabled");
        }
        A06(sb, AnonymousClass08S.A0J("pm.dexopt.bg-dexopt.", r43.A00));
        A06(sb, AnonymousClass08S.A0X("VmSafeFlagMode.", r43.A01));
        if (list != null) {
            for (String A06 : list) {
                A06(sb, A06);
            }
        }
        r32.A04("trace_tags", sb.toString());
        C30440EwU ewU = this.A00.A03;
        if (ewU != null) {
            boolean z4 = ewU.A07;
            Preconditions.checkNotNull(r32.A02);
            Preconditions.checkState(r32.A04);
            r32.A02.A0B("FRL_HOOK_ADDED", z4);
            boolean z5 = ewU.A06;
            Preconditions.checkNotNull(r32.A02);
            Preconditions.checkState(r32.A04);
            r32.A02.A0B("FRL_FAST_HOOK_COMPLETED", z5);
            r32.A02("FRL_DYNAMIC_FPS", ewU.A00);
            r32.A02("FRL_NUM_TIMES_FPS_CHANGED", ewU.A02);
            r32.A02("FRL_IDX_NUM_TIMES_FPS_WAS_DYNAMICALLY_REDUCED", ewU.A03);
            r32.A02("FRL_IDX_NUM_TIMES_FPS_WAS_DYNAMICALLY_RESTORE", ewU.A04);
            r32.A02("FRL_IDX_NUM_OF_SEPARATE_IDS_SEEN", ewU.A01);
            boolean z6 = ewU.A08;
            Preconditions.checkNotNull(r32.A02);
            Preconditions.checkState(r32.A04);
            r32.A02.A0B("FRL_IDX_USE_SET_VSYNC_RATE", z6);
            r32.A02("FRL_IDX_SET_VSYNC_RATE_WRONG", ewU.A05);
        }
        Preconditions.checkNotNull(r32.A02);
        Preconditions.checkState(r32.A04);
        r32.A02.BK9();
        r32.A02 = null;
        r32.A04 = false;
        r32.A03.markerEnd(r32.A00, r32.A01, (short) 2, this.A00.A02, TimeUnit.MILLISECONDS);
        A07(this.A00.A0A);
        A07(this.A00.A09);
        A07(this.A00.A0B);
        A07(this.A00.A08);
        A07(this.A00.A0C);
        AnonymousClass1T0 r12 = this.A00;
        r12.A0N.A00();
        r12.A0M.A00();
        r12.A03 = null;
    }

    private static void A06(StringBuilder sb, String str) {
        if (sb.length() != 0) {
            sb.append(",");
        }
        sb.append(str);
    }

    private static void A07(Map map) {
        for (AnonymousClass184 r1 : map.values()) {
            r1.A00 = 0;
            r1.A01 = 0;
        }
    }
}
