package X;

/* renamed from: X.0gx  reason: invalid class name and case insensitive filesystem */
public enum C09290gx {
    FULLY_CACHED(true, true, true),
    CACHE_ONLY(true, false, false),
    NETWORK_ONLY(false, true, false),
    FETCH_AND_FILL(false, true, true),
    PREFETCH_TO_DB(false, true, true);
    
    public final boolean fillDB;
    public final boolean readDB;
    public final boolean readNetwork;

    private C09290gx(boolean z, boolean z2, boolean z3) {
        this.readDB = z;
        this.readNetwork = z2;
        this.fillDB = z3;
    }

    public static C09290gx A00(C09510hU r0) {
        switch (r0.ordinal()) {
            case 0:
            case 3:
                return FULLY_CACHED;
            case 1:
                return FETCH_AND_FILL;
            case 2:
                return CACHE_ONLY;
            default:
                return NETWORK_ONLY;
        }
    }
}
