package X;

import com.facebook.messaging.model.threads.ThreadSummary;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Iq  reason: invalid class name and case insensitive filesystem */
public final class C21771Iq {
    private static volatile C21771Iq A01;
    private final AnonymousClass1YI A00;

    public static final C21771Iq A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C21771Iq.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C21771Iq(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static boolean A01(ThreadSummary threadSummary) {
        if (threadSummary == null || !threadSummary.A0S.A0L() || !AnonymousClass6YD.CHAT.equals(threadSummary.A0V.A00())) {
            return false;
        }
        return true;
    }

    public boolean A02() {
        return this.A00.AbO(AnonymousClass1Y3.A1d, false);
    }

    private C21771Iq(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WA.A00(r2);
    }
}
