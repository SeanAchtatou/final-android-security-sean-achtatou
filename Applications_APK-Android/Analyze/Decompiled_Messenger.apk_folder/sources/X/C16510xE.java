package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0xE  reason: invalid class name and case insensitive filesystem */
public final class C16510xE {
    private static volatile C16510xE A01;
    public AnonymousClass0UN A00;

    public static final C16510xE A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C16510xE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C16510xE(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public boolean A0B() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501482743332L);
    }

    public boolean A0C() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501481629204L);
    }

    public boolean A0O() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501488510580L);
    }

    private C16510xE(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public long A01() {
        if (!A0B() || !A0L()) {
            return 1;
        }
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At0(563976462008990L);
    }

    public boolean A02() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501488379507L)) {
            return false;
        }
        return true;
    }

    public boolean A03() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501485954641L)) {
            return false;
        }
        return true;
    }

    public boolean A04() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501485037124L)) {
            return false;
        }
        return true;
    }

    public boolean A05() {
        if (!A0B() || !A0P() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501483398700L)) {
            return false;
        }
        return true;
    }

    public boolean A06() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501487789675L)) {
            return false;
        }
        return true;
    }

    public boolean A07() {
        if (!A0B() || !A08() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501486347863L)) {
            return false;
        }
        return true;
    }

    public boolean A08() {
        if (!A0B() || A0P() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501484054069L)) {
            return false;
        }
        return true;
    }

    public boolean A09() {
        if (!A0B() || !A08() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501486282326L)) {
            return false;
        }
        return true;
    }

    public boolean A0A() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501486151252L)) {
            return false;
        }
        return true;
    }

    public boolean A0D() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501483791922L)) {
            return false;
        }
        return true;
    }

    public boolean A0E() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501482808869L)) {
            return false;
        }
        return true;
    }

    public boolean A0F() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501485364808L)) {
            return false;
        }
        return true;
    }

    public boolean A0G() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501487986286L)) {
            return false;
        }
        return true;
    }

    public boolean A0H() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501485102661L)) {
            return false;
        }
        return true;
    }

    public boolean A0I() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501484185143L)) {
            return false;
        }
        return true;
    }

    public boolean A0J() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501483857459L)) {
            return false;
        }
        return true;
    }

    public boolean A0K() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501485168198L)) {
            return false;
        }
        return true;
    }

    public boolean A0L() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501485233735L)) {
            return false;
        }
        return true;
    }

    public boolean A0M() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501477762544L)) {
            return false;
        }
        return true;
    }

    public boolean A0N() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501482874406L)) {
            return false;
        }
        return true;
    }

    public boolean A0P() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501483071017L)) {
            return false;
        }
        return true;
    }

    public boolean A0Q() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501488117360L)) {
            return false;
        }
        return true;
    }

    public boolean A0R() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501487658601L)) {
            return false;
        }
        return true;
    }

    public boolean A0S() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501485561419L)) {
            return false;
        }
        return true;
    }

    public boolean A0T() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501483136554L)) {
            return false;
        }
        return true;
    }

    public boolean A0U() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501483005480L)) {
            return false;
        }
        return true;
    }

    public boolean A0V() {
        if (!A0B() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501486610009L)) {
            return false;
        }
        return true;
    }
}
