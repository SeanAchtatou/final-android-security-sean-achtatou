package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0Et  reason: invalid class name and case insensitive filesystem */
public final class C02430Et {
    public static List A00(Collection collection) {
        ArrayList arrayList = new ArrayList();
        if (collection != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                arrayList.add(((SubscribeTopic) it.next()).A01);
            }
        }
        return arrayList;
    }
}
