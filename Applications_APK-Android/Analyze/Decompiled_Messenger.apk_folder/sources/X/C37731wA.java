package X;

/* renamed from: X.1wA  reason: invalid class name and case insensitive filesystem */
public final class C37731wA extends Exception {
    public final boolean isNetworkError;

    public C37731wA(Exception exc, boolean z) {
        super("Network failure while fetching thread/messages.", exc);
        this.isNetworkError = z;
    }
}
