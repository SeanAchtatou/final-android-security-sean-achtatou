package X;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1Sm  reason: invalid class name and case insensitive filesystem */
public final class C24151Sm extends C20831Dz implements C24161Sn {
    public List A00;
    public List A01;
    private boolean A02;
    public final C24161Sn A03;
    private final C15730vo A04 = new C21691Ii(this);

    public void A0D(C33781o8 r3) {
        C33781o8 r0;
        C87304Eg r32 = (C87304Eg) r3;
        C24161Sn r1 = this.A03;
        if ((r1 instanceof C20831Dz) && (r0 = r32.A00) != null) {
            ((C20831Dz) r1).A0D(r0);
        }
    }

    public int ArU() {
        return this.A01.size() + this.A03.ArU() + this.A00.size();
    }

    public void BOc(RecyclerView recyclerView) {
        this.A03.BOc(recyclerView);
    }

    public Object getItem(int i) {
        int size = this.A01.size();
        if (i < size || i >= this.A03.ArU() + size) {
            return null;
        }
        return this.A03.getItem(i - size);
    }

    public C24151Sm(C24161Sn r2) {
        this.A03 = r2;
        this.A01 = Collections.emptyList();
        this.A00 = Collections.emptyList();
        A09(this.A03.hasStableIds());
    }

    public void C0O(C15730vo r3) {
        super.C0O(r3);
        if (!this.A02) {
            this.A03.C0O(this.A04);
            this.A02 = true;
        }
    }

    public void CJm(C15730vo r3) {
        super.CJm(r3);
        if (this.A02 && !this.A01.A05()) {
            this.A03.CJm(this.A04);
            this.A02 = false;
        }
    }
}
