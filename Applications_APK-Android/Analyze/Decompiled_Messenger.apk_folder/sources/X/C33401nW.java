package X;

import android.content.ComponentName;
import android.os.IBinder;
import com.google.common.base.Preconditions;
import java.util.Set;

/* renamed from: X.1nW  reason: invalid class name and case insensitive filesystem */
public final class C33401nW {
    public IBinder A00;
    public boolean A01;
    public final int A02;
    public final C36091sJ A03;
    public final Set A04 = C25011Xz.A03();

    public C33401nW(ComponentName componentName, C36091sJ r3, int i) {
        Preconditions.checkNotNull(componentName);
        Preconditions.checkNotNull(r3);
        this.A03 = r3;
        this.A02 = i;
    }
}
