package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1Jp  reason: invalid class name and case insensitive filesystem */
public final class C22021Jp extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public ThreadSummary A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 13)
    public Integer A03;

    public C22021Jp(Context context) {
        super("M4ThreadItemThreadStatusComponent");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }
}
