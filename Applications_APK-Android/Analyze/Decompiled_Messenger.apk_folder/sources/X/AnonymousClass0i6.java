package X;

import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0i6  reason: invalid class name */
public final class AnonymousClass0i6 {
    private static volatile AnonymousClass0i6 A03;
    public AnonymousClass0UN A00;
    public final AtomicReference A01 = new AtomicReference();
    public final AtomicReference A02 = new AtomicReference();

    public static final AnonymousClass0i6 A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass0i6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass0i6(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static void A01(AnonymousClass0i6 r2) {
        int i;
        Thread thread = (Thread) r2.A02.get();
        if (thread != null) {
            thread.setPriority(10);
            C005505z.A03("ThreadJoin", 1056713425);
            try {
                thread.join();
                i = -134211919;
            } catch (InterruptedException e) {
                C010708t.A0T("FrscLanguagePackLoader", e, "Loading thread interrupted");
                i = 1913264577;
            } catch (Throwable th) {
                C005505z.A00(1478019254);
                throw th;
            }
            C005505z.A00(i);
        }
    }

    private AnonymousClass0i6(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
