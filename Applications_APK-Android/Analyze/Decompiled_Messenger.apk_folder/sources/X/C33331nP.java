package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1nP  reason: invalid class name and case insensitive filesystem */
public final class C33331nP implements C05460Za {
    public AnonymousClass0UN A00;
    public UserKey A01 = null;
    public final C04310Tq A02;
    private final C04310Tq A03;

    public static boolean A03(C33331nP r3, ThreadSummary threadSummary, boolean z) {
        C04310Tq r0;
        if (z) {
            r0 = ((C21511Hh) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AH9, r3.A00)).A02;
        } else {
            r0 = ((C21511Hh) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AH9, r3.A00)).A01;
        }
        if (!((Boolean) r0.get()).booleanValue() || !threadSummary.A0S.A0L() || r3.A08(threadSummary, null)) {
            return false;
        }
        return true;
    }

    public void clearUserData() {
        this.A01 = null;
    }

    public static final C33331nP A00(AnonymousClass1XY r1) {
        return new C33331nP(r1);
    }

    public boolean A07(ThreadSummary threadSummary, User user) {
        ThreadKey threadKey = threadSummary.A0S;
        if (ThreadKey.A09(threadKey) || ThreadKey.A0E(threadKey) || A08(threadSummary, user)) {
            return false;
        }
        return true;
    }

    private C33331nP(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(10, r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.Ayu, r3);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.B0T, r3);
    }

    private static boolean A01(ThreadSummary threadSummary) {
        boolean z;
        Preconditions.checkNotNull(threadSummary);
        if (ThreadKey.A09(threadSummary.A0S)) {
            Preconditions.checkNotNull(threadSummary);
            ImmutableList immutableList = threadSummary.A0m;
            if (immutableList != null) {
                C24971Xv it = immutableList.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
                    if (threadParticipant.A00() != null && threadParticipant.A00().A08()) {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A02(C33331nP r4, ThreadKey threadKey) {
        User A032;
        UserKey A07 = ThreadKey.A07(threadKey);
        if (A07 == null || (A032 = ((C14300t0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxE, r4.A00)).A03(A07)) == null) {
            return false;
        }
        return A032.A0F();
    }

    public boolean A04(ThreadKey threadKey) {
        User A032;
        UserKey A07 = ThreadKey.A07(threadKey);
        if (A07 == null || (A032 = ((C14300t0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxE, this.A00)).A03(A07)) == null) {
            return false;
        }
        return A032.A19;
    }

    public boolean A05(ThreadKey threadKey, ThreadSummary threadSummary) {
        Preconditions.checkNotNull(threadSummary);
        if (!ThreadKey.A0E(threadKey) || A01(threadSummary) || ((C52422j4) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ALq, this.A00)).A07(threadSummary) == null) {
            return false;
        }
        return true;
    }

    public boolean A06(ThreadSummary threadSummary, User user) {
        if (A08(threadSummary, user) || !((Boolean) this.A02.get()).booleanValue()) {
            return false;
        }
        ThreadKey threadKey = threadSummary.A0S;
        if (!ThreadKey.A0E(threadKey) && A02(this, threadKey) && !A04(threadSummary.A0S)) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0040, code lost:
        if (r0 != false) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0050, code lost:
        if (r9.A0H != com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason.A02) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005f, code lost:
        if (r3 != false) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009e, code lost:
        if (com.google.common.base.Objects.equal(r2, r8.A01) != false) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a2, code lost:
        if (r0 == false) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0027, code lost:
        if (com.google.common.base.Objects.equal(r2, r8.A01) == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A08(com.facebook.messaging.model.threads.ThreadSummary r9, com.facebook.user.model.User r10) {
        /*
            r8 = this;
            com.google.common.base.Preconditions.checkNotNull(r9)
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r9.A0S
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0B(r1)
            r7 = 1
            if (r0 == 0) goto L_0x0029
            com.facebook.user.model.UserKey r2 = com.facebook.messaging.model.threadkey.ThreadKey.A07(r1)
            com.facebook.user.model.UserKey r0 = r8.A01
            if (r0 != 0) goto L_0x0020
            int r1 = X.AnonymousClass1Y3.B7T
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            r8.A01 = r0
        L_0x0020:
            com.facebook.user.model.UserKey r0 = r8.A01
            boolean r0 = com.google.common.base.Objects.equal(r2, r0)
            r6 = 1
            if (r0 != 0) goto L_0x002a
        L_0x0029:
            r6 = 0
        L_0x002a:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0S
            boolean r3 = r8.A04(r0)
            if (r3 != 0) goto L_0x0042
            if (r9 == 0) goto L_0x0038
            boolean r0 = r9.A0y
            if (r0 != 0) goto L_0x003e
        L_0x0038:
            if (r10 == 0) goto L_0x0124
            boolean r0 = r10.A1D
            if (r0 == 0) goto L_0x0124
        L_0x003e:
            r0 = 1
        L_0x003f:
            r5 = 1
            if (r0 == 0) goto L_0x0043
        L_0x0042:
            r5 = 0
        L_0x0043:
            if (r5 == 0) goto L_0x0056
            if (r9 == 0) goto L_0x0052
            boolean r0 = r9.A0y
            if (r0 != 0) goto L_0x0052
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r2 = r9.A0H
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r1 = com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason.A02
            r0 = 1
            if (r2 == r1) goto L_0x0053
        L_0x0052:
            r0 = 0
        L_0x0053:
            if (r0 == 0) goto L_0x0056
            r5 = 0
        L_0x0056:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0S
            boolean r0 = A02(r8, r0)
            if (r0 == 0) goto L_0x0061
            r4 = 1
            if (r3 == 0) goto L_0x0062
        L_0x0061:
            r4 = 0
        L_0x0062:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0S
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r0)
            if (r0 == 0) goto L_0x00a4
            com.google.common.base.Preconditions.checkNotNull(r9)
            com.facebook.user.model.UserKey r0 = r9.A05()
            if (r0 == 0) goto L_0x00a0
            com.facebook.messaging.model.threads.ThreadParticipant r0 = r9.A03()
            if (r0 == 0) goto L_0x00a0
            com.facebook.messaging.model.threads.ThreadParticipant r0 = r9.A03()
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r0.A04
            boolean r0 = r0.A06
            if (r0 != 0) goto L_0x00a0
            com.facebook.user.model.UserKey r2 = r9.A05()
            com.facebook.user.model.UserKey r0 = r8.A01
            if (r0 != 0) goto L_0x0097
            int r1 = X.AnonymousClass1Y3.B7T
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            r8.A01 = r0
        L_0x0097:
            com.facebook.user.model.UserKey r0 = r8.A01
            boolean r1 = com.google.common.base.Objects.equal(r2, r0)
            r0 = 0
            if (r1 == 0) goto L_0x00a1
        L_0x00a0:
            r0 = 1
        L_0x00a1:
            r3 = 1
            if (r0 != 0) goto L_0x00a5
        L_0x00a4:
            r3 = 0
        L_0x00a5:
            r2 = 7
            int r1 = X.AnonymousClass1Y3.AuG
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1He r0 = (X.C21481He) r0
            boolean r2 = r0.A01()
            if (r6 != 0) goto L_0x0121
            if (r5 != 0) goto L_0x0121
            if (r4 != 0) goto L_0x0121
            if (r3 != 0) goto L_0x0121
            boolean r0 = X.AnonymousClass1H9.A01(r9)
            if (r0 != 0) goto L_0x0121
            com.google.common.base.Preconditions.checkNotNull(r9)
            com.facebook.messaging.model.threads.MarketplaceThreadData r1 = r9.A0W
            r0 = 0
            if (r1 == 0) goto L_0x00cb
            r0 = 1
        L_0x00cb:
            if (r0 != 0) goto L_0x0121
            com.google.common.base.Preconditions.checkNotNull(r9)
            com.google.common.collect.ImmutableList r0 = r9.A0m
            if (r0 == 0) goto L_0x0122
            X.1Xv r1 = r0.iterator()
        L_0x00d8:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0122
            java.lang.Object r0 = r1.next()
            com.facebook.messaging.model.threads.ThreadParticipant r0 = (com.facebook.messaging.model.threads.ThreadParticipant) r0
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r0.A04
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x00d8
            r0 = 1
        L_0x00eb:
            if (r0 != 0) goto L_0x0121
            X.0Tq r0 = r8.A03
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0121
            boolean r0 = A01(r9)
            if (r0 != 0) goto L_0x0121
            if (r2 != 0) goto L_0x0121
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0S
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r0)
            if (r0 == 0) goto L_0x0127
            r2 = 6
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r8.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 283051229710500(0x1016f000008a4, double:1.39845888613077E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 != 0) goto L_0x0127
        L_0x0121:
            return r7
        L_0x0122:
            r0 = 0
            goto L_0x00eb
        L_0x0124:
            r0 = 0
            goto L_0x003f
        L_0x0127:
            r7 = 0
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33331nP.A08(com.facebook.messaging.model.threads.ThreadSummary, com.facebook.user.model.User):boolean");
    }
}
