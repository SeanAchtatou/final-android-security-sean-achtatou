package X;

import java.util.ConcurrentModificationException;

/* renamed from: X.04b  reason: invalid class name */
public class AnonymousClass04b {
    public static int A03;
    public static int A04;
    public static Object[] A05;
    public static Object[] A06;
    public int A00;
    public int[] A01;
    public Object[] A02;

    /* JADX WARNING: Unknown top exception splitter block from list: {B:7:0x0015=Splitter:B:7:0x0015, B:27:0x004d=Splitter:B:27:0x004d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            r5 = 1
            if (r6 == r7) goto L_0x0076
            boolean r0 = r7 instanceof X.AnonymousClass04b
            r4 = 0
            if (r0 == 0) goto L_0x003c
            X.04b r7 = (X.AnonymousClass04b) r7
            int r1 = r6.size()
            int r0 = r7.size()
            if (r1 != r0) goto L_0x0074
            r3 = 0
        L_0x0015:
            int r0 = r6.A00     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            if (r3 >= r0) goto L_0x0076
            java.lang.Object r2 = r6.A07(r3)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            java.lang.Object r1 = r6.A09(r3)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            java.lang.Object r0 = r7.get(r2)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            if (r1 != 0) goto L_0x0030
            if (r0 != 0) goto L_0x0074
            boolean r0 = r7.containsKey(r2)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            if (r0 != 0) goto L_0x0037
            goto L_0x003a
        L_0x0030:
            boolean r0 = r1.equals(r0)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            if (r0 != 0) goto L_0x0037
            goto L_0x003b
        L_0x0037:
            int r3 = r3 + 1
            goto L_0x0015
        L_0x003a:
            return r4
        L_0x003b:
            return r4
        L_0x003c:
            boolean r0 = r7 instanceof java.util.Map
            if (r0 == 0) goto L_0x0075
            java.util.Map r7 = (java.util.Map) r7
            int r1 = r6.size()
            int r0 = r7.size()
            if (r1 != r0) goto L_0x0074
            r3 = 0
        L_0x004d:
            int r0 = r6.A00     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            if (r3 >= r0) goto L_0x0076
            java.lang.Object r2 = r6.A07(r3)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            java.lang.Object r1 = r6.A09(r3)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            java.lang.Object r0 = r7.get(r2)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            if (r1 != 0) goto L_0x0068
            if (r0 != 0) goto L_0x0074
            boolean r0 = r7.containsKey(r2)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            if (r0 != 0) goto L_0x006f
            goto L_0x0072
        L_0x0068:
            boolean r0 = r1.equals(r0)     // Catch:{ ClassCastException | NullPointerException -> 0x0075 }
            if (r0 != 0) goto L_0x006f
            goto L_0x0073
        L_0x006f:
            int r3 = r3 + 1
            goto L_0x004d
        L_0x0072:
            return r4
        L_0x0073:
            return r4
        L_0x0074:
            return r4
        L_0x0075:
            return r4
        L_0x0076:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04b.equals(java.lang.Object):boolean");
    }

    public Object get(Object obj) {
        return getOrDefault(obj, null);
    }

    private int A01() {
        int i = this.A00;
        if (i == 0) {
            return -1;
        }
        int[] iArr = this.A01;
        try {
            int A022 = AnonymousClass04d.A02(iArr, i, 0);
            if (A022 >= 0) {
                Object[] objArr = this.A02;
                if (objArr[A022 << 1] != null) {
                    int i2 = A022 + 1;
                    while (i2 < i && iArr[i2] == 0) {
                        if (objArr[i2 << 1] == null) {
                            return i2;
                        }
                        i2++;
                    }
                    do {
                        A022--;
                        if (A022 < 0 || iArr[A022] != 0) {
                            return i2 ^ -1;
                        }
                    } while (objArr[A022 << 1] != null);
                    return A022;
                }
            }
            return A022;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    private int A02(Object obj, int i) {
        int i2 = this.A00;
        if (i2 == 0) {
            return -1;
        }
        try {
            int A022 = AnonymousClass04d.A02(this.A01, i2, i);
            if (A022 < 0 || obj.equals(this.A02[A022 << 1])) {
                return A022;
            }
            int i3 = A022 + 1;
            while (i3 < i2 && this.A01[i3] == i) {
                if (obj.equals(this.A02[i3 << 1])) {
                    return i3;
                }
                i3++;
            }
            do {
                A022--;
                if (A022 < 0 || this.A01[A022] != i) {
                    return i3 ^ -1;
                }
            } while (!obj.equals(this.A02[A022 << 1]));
            return A022;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    private void A03(int i) {
        Class<AnonymousClass04b> cls = AnonymousClass04b.class;
        if (i == 8) {
            synchronized (cls) {
                try {
                    Object[] objArr = A06;
                    if (objArr != null) {
                        this.A02 = objArr;
                        A06 = (Object[]) objArr[0];
                        this.A01 = (int[]) objArr[1];
                        objArr[1] = null;
                        objArr[0] = null;
                        A04--;
                        return;
                    }
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
        } else if (i == 4) {
            synchronized (cls) {
                try {
                    Object[] objArr2 = A05;
                    if (objArr2 != null) {
                        this.A02 = objArr2;
                        A05 = (Object[]) objArr2[0];
                        this.A01 = (int[]) objArr2[1];
                        objArr2[1] = null;
                        objArr2[0] = null;
                        A03--;
                        return;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        this.A01 = new int[i];
        this.A02 = new Object[(i << 1)];
    }

    private static void A04(int[] iArr, Object[] objArr, int i) {
        Class<AnonymousClass04b> cls = AnonymousClass04b.class;
        int length = iArr.length;
        if (length == 8) {
            synchronized (cls) {
                try {
                    int i2 = A04;
                    if (i2 < 10) {
                        objArr[0] = A06;
                        objArr[1] = iArr;
                        for (int i3 = (i << 1) - 1; i3 >= 2; i3--) {
                            objArr[i3] = null;
                        }
                        A06 = objArr;
                        A04 = i2 + 1;
                    }
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
        } else if (length == 4) {
            synchronized (cls) {
                try {
                    int i4 = A03;
                    if (i4 < 10) {
                        objArr[0] = A05;
                        objArr[1] = iArr;
                        for (int i5 = (i << 1) - 1; i5 >= 2; i5--) {
                            objArr[i5] = null;
                        }
                        A05 = objArr;
                        A03 = i4 + 1;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public int A05(Object obj) {
        int i = this.A00 << 1;
        Object[] objArr = this.A02;
        if (obj == null) {
            for (int i2 = 1; i2 < i; i2 += 2) {
                if (objArr[i2] == null) {
                    return i2 >> 1;
                }
            }
            return -1;
        }
        for (int i3 = 1; i3 < i; i3 += 2) {
            if (obj.equals(objArr[i3])) {
                return i3 >> 1;
            }
        }
        return -1;
    }

    public int A06(Object obj) {
        if (obj == null) {
            return A01();
        }
        return A02(obj, obj.hashCode());
    }

    public Object A07(int i) {
        return this.A02[i << 1];
    }

    public Object A08(int i) {
        int i2;
        Object[] objArr = this.A02;
        int i3 = i << 1;
        Object obj = objArr[i3 + 1];
        int i4 = this.A00;
        if (i4 <= 1) {
            A04(this.A01, objArr, i4);
            this.A01 = AnonymousClass04d.A00;
            this.A02 = AnonymousClass04d.A02;
            i2 = 0;
        } else {
            i2 = i4 - 1;
            int[] iArr = this.A01;
            int length = iArr.length;
            int i5 = 8;
            if (length <= 8 || i4 >= length / 3) {
                if (i < i2) {
                    int i6 = i + 1;
                    int i7 = i2 - i;
                    System.arraycopy(iArr, i6, iArr, i, i7);
                    Object[] objArr2 = this.A02;
                    System.arraycopy(objArr2, i6 << 1, objArr2, i3, i7 << 1);
                }
                Object[] objArr3 = this.A02;
                int i8 = i2 << 1;
                objArr3[i8] = null;
                objArr3[i8 + 1] = null;
            } else {
                if (i4 > 8) {
                    i5 = i4 + (i4 >> 1);
                }
                A03(i5);
                if (i4 == this.A00) {
                    if (i > 0) {
                        System.arraycopy(iArr, 0, this.A01, 0, i);
                        System.arraycopy(objArr, 0, this.A02, 0, i3);
                    }
                    if (i < i2) {
                        int i9 = i + 1;
                        int i10 = i2 - i;
                        System.arraycopy(iArr, i9, this.A01, i, i10);
                        System.arraycopy(objArr, i9 << 1, this.A02, i3, i10 << 1);
                    }
                }
                throw new ConcurrentModificationException();
            }
        }
        if (i4 == this.A00) {
            this.A00 = i2;
            return obj;
        }
        throw new ConcurrentModificationException();
    }

    public Object A09(int i) {
        return this.A02[(i << 1) + 1];
    }

    public void A0A(int i) {
        int i2 = this.A00;
        int[] iArr = this.A01;
        if (iArr.length < i) {
            Object[] objArr = this.A02;
            A03(i);
            if (this.A00 > 0) {
                System.arraycopy(iArr, 0, this.A01, 0, i2);
                System.arraycopy(objArr, 0, this.A02, 0, i2 << 1);
            }
            A04(iArr, objArr, i2);
        }
        if (this.A00 != i2) {
            throw new ConcurrentModificationException();
        }
    }

    public void A0B(AnonymousClass04b r6) {
        int i = r6.A00;
        A0A(this.A00 + i);
        if (this.A00 != 0) {
            for (int i2 = 0; i2 < i; i2++) {
                put(r6.A07(i2), r6.A09(i2));
            }
        } else if (i > 0) {
            System.arraycopy(r6.A01, 0, this.A01, 0, i);
            System.arraycopy(r6.A02, 0, this.A02, 0, i << 1);
            this.A00 = i;
        }
    }

    public void clear() {
        int i = this.A00;
        if (i > 0) {
            int[] iArr = this.A01;
            Object[] objArr = this.A02;
            this.A01 = AnonymousClass04d.A00;
            this.A02 = AnonymousClass04d.A02;
            this.A00 = 0;
            A04(iArr, objArr, i);
        }
        if (this.A00 > 0) {
            throw new ConcurrentModificationException();
        }
    }

    public int hashCode() {
        int hashCode;
        int[] iArr = this.A01;
        Object[] objArr = this.A02;
        int i = this.A00;
        int i2 = 0;
        int i3 = 0;
        int i4 = 1;
        while (i2 < i) {
            Object obj = objArr[i4];
            int i5 = iArr[i2];
            if (obj == null) {
                hashCode = 0;
            } else {
                hashCode = obj.hashCode();
            }
            i3 += hashCode ^ i5;
            i2++;
            i4 += 2;
        }
        return i3;
    }

    public boolean isEmpty() {
        if (this.A00 <= 0) {
            return true;
        }
        return false;
    }

    public Object put(Object obj, Object obj2) {
        int hashCode;
        int A022;
        int i = this.A00;
        if (obj == null) {
            A022 = A01();
            hashCode = 0;
        } else {
            hashCode = obj.hashCode();
            A022 = A02(obj, hashCode);
        }
        if (A022 >= 0) {
            int i2 = (A022 << 1) + 1;
            Object[] objArr = this.A02;
            Object obj3 = objArr[i2];
            objArr[i2] = obj2;
            return obj3;
        }
        int i3 = A022 ^ -1;
        int[] iArr = this.A01;
        if (i >= iArr.length) {
            int i4 = 4;
            if (i >= 8) {
                i4 = (i >> 1) + i;
            } else if (i >= 4) {
                i4 = 8;
            }
            Object[] objArr2 = this.A02;
            A03(i4);
            if (i == this.A00) {
                int[] iArr2 = this.A01;
                if (iArr2.length > 0) {
                    System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                    System.arraycopy(objArr2, 0, this.A02, 0, objArr2.length);
                }
                A04(iArr, objArr2, i);
            }
            throw new ConcurrentModificationException();
        }
        if (i3 < i) {
            int[] iArr3 = this.A01;
            int i5 = i3 + 1;
            System.arraycopy(iArr3, i3, iArr3, i5, i - i3);
            Object[] objArr3 = this.A02;
            System.arraycopy(objArr3, i3 << 1, objArr3, i5 << 1, (this.A00 - i3) << 1);
        }
        int i6 = this.A00;
        if (i == i6) {
            int[] iArr4 = this.A01;
            if (i3 < iArr4.length) {
                iArr4[i3] = hashCode;
                Object[] objArr4 = this.A02;
                int i7 = i3 << 1;
                objArr4[i7] = obj;
                objArr4[i7 + 1] = obj2;
                this.A00 = i6 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }

    public boolean containsKey(Object obj) {
        if (A06(obj) >= 0) {
            return true;
        }
        return false;
    }

    public boolean containsValue(Object obj) {
        if (A05(obj) >= 0) {
            return true;
        }
        return false;
    }

    public Object getOrDefault(Object obj, Object obj2) {
        int A062 = A06(obj);
        if (A062 >= 0) {
            return this.A02[(A062 << 1) + 1];
        }
        return obj2;
    }

    public Object putIfAbsent(Object obj, Object obj2) {
        Object obj3 = get(obj);
        if (obj3 == null) {
            return put(obj, obj2);
        }
        return obj3;
    }

    public int size() {
        return this.A00;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.A00 * 28);
        sb.append('{');
        for (int i = 0; i < this.A00; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            Object A07 = A07(i);
            if (A07 != this) {
                sb.append(A07);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            Object A09 = A09(i);
            if (A09 != this) {
                sb.append(A09);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public AnonymousClass04b() {
        this.A01 = AnonymousClass04d.A00;
        this.A02 = AnonymousClass04d.A02;
        this.A00 = 0;
    }

    public AnonymousClass04b(int i) {
        if (i == 0) {
            this.A01 = AnonymousClass04d.A00;
            this.A02 = AnonymousClass04d.A02;
        } else {
            A03(i);
        }
        this.A00 = 0;
    }

    public Object remove(Object obj) {
        int A062 = A06(obj);
        if (A062 >= 0) {
            return A08(A062);
        }
        return null;
    }

    public boolean remove(Object obj, Object obj2) {
        int A062 = A06(obj);
        if (A062 < 0) {
            return false;
        }
        Object A09 = A09(A062);
        if (obj2 != A09 && (obj2 == null || !obj2.equals(A09))) {
            return false;
        }
        A08(A062);
        return true;
    }

    public Object replace(Object obj, Object obj2) {
        int A062 = A06(obj);
        if (A062 < 0) {
            return null;
        }
        int i = (A062 << 1) + 1;
        Object[] objArr = this.A02;
        Object obj3 = objArr[i];
        objArr[i] = obj2;
        return obj3;
    }

    public boolean replace(Object obj, Object obj2, Object obj3) {
        int A062 = A06(obj);
        if (A062 < 0) {
            return false;
        }
        Object A09 = A09(A062);
        if (A09 != obj2 && (obj2 == null || !obj2.equals(A09))) {
            return false;
        }
        this.A02[(A062 << 1) + 1] = obj3;
        return true;
    }
}
