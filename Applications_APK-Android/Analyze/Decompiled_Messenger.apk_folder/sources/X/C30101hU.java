package X;

import com.google.common.base.Platform;

/* renamed from: X.1hU  reason: invalid class name and case insensitive filesystem */
public enum C30101hU {
    NORMAL("NORMAL"),
    LARGE_BUTTON("LARGE_BUTTON");
    
    public final String name;

    private C30101hU(String str) {
        this.name = str;
    }

    public static C30101hU A00(String str) {
        if (!Platform.stringIsNullOrEmpty(str)) {
            if (str.equalsIgnoreCase("NORMAL")) {
                return NORMAL;
            }
            if (str.equalsIgnoreCase("LARGE_BUTTON")) {
                return LARGE_BUTTON;
            }
        }
        return null;
    }
}
