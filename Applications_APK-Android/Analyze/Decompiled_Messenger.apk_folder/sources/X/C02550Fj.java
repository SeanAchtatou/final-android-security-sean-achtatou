package X;

import android.os.Process;
import java.nio.CharBuffer;
import java.util.Arrays;

/* renamed from: X.0Fj  reason: invalid class name and case insensitive filesystem */
public final class C02550Fj extends C02540Fi {
    private static final long A02 = ((long) Process.myUid());
    private static final CharBuffer A03 = CharBuffer.wrap("dummy0");
    private static final CharBuffer A04 = CharBuffer.wrap("lo");
    private static final CharBuffer A05 = CharBuffer.wrap("wlan0");
    private C02560Fk A00;
    private final CharBuffer A01 = CharBuffer.allocate(128);

    public boolean A02() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(long[], long):void}
     arg types: [long[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(long[], long):void} */
    public boolean A03(long[] jArr) {
        try {
            if (this.A00 == null) {
                this.A00 = new C02560Fk("/proc/net/xt_qtaguid/stats", 512);
            }
            this.A00.A04();
            C02560Fk r1 = this.A00;
            if (!r1.A01 || !r1.A08()) {
                return false;
            }
            Arrays.fill(jArr, 0L);
            this.A00.A05();
            while (true) {
                boolean z = true;
                if (!this.A00.A08()) {
                    return true;
                }
                this.A00.A06();
                this.A00.A07(this.A01);
                this.A00.A06();
                this.A00.A06();
                long A032 = this.A00.A03();
                this.A00.A06();
                boolean z2 = false;
                if (A05.compareTo(this.A01) == 0) {
                    z2 = true;
                }
                if (z2 || A03.compareTo(this.A01) == 0 || A04.compareTo(this.A01) == 0) {
                    z = false;
                }
                if (A032 == A02) {
                    if (z2 || z) {
                        long A033 = this.A00.A03();
                        this.A00.A06();
                        char c = 2;
                        if (z2) {
                            c = 0;
                        }
                        char c2 = c | 0;
                        char c3 = 0;
                        if (A033 == 0) {
                            c3 = 4;
                        }
                        char c4 = c2 | c3;
                        char c5 = c4 | 0;
                        jArr[c5] = jArr[c5] + this.A00.A03();
                        this.A00.A06();
                        this.A00.A06();
                        char c6 = c4 | 1;
                        jArr[c6] = jArr[c6] + this.A00.A03();
                        this.A00.A05();
                    }
                }
                this.A00.A05();
            }
        } catch (AnonymousClass0KX e) {
            AnonymousClass0KZ.A00("QTagUidNetworkBytesCollector", "Unable to parse file", e);
            return false;
        }
    }
}
