package X;

import android.os.Build;
import com.facebook.common.dextricks.DexStore;
import io.card.payment.BuildConfig;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.1N0  reason: invalid class name */
public class AnonymousClass1N0 {
    public C22921Ni A00;
    public C22921Ni A01;
    public C22921Ni A02;
    private C23011Nw A03;
    private C37391vb A04;
    private C22841Na A05;
    private C22861Nc A06;
    private C30661iR A07;
    public final AnonymousClass1NA A08;

    private C22921Ni A00(int i) {
        if (i == 0) {
            if (this.A02 == null) {
                try {
                    Constructor<?> constructor = Class.forName("com.facebook.imagepipeline.memory.NativeMemoryChunkPool").getConstructor(C14320t5.class, AnonymousClass1N7.class, AnonymousClass1N4.class);
                    AnonymousClass1NA r3 = this.A08;
                    this.A02 = (C22921Ni) constructor.newInstance(r3.A02, r3.A05, r3.A08);
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                    AnonymousClass02w.A0D("PoolFactory", BuildConfig.FLAVOR, e);
                    this.A02 = null;
                }
            }
            return this.A02;
        } else if (i == 1) {
            if (this.A01 == null) {
                try {
                    Constructor<?> constructor2 = Class.forName("com.facebook.imagepipeline.memory.BufferMemoryChunkPool").getConstructor(C14320t5.class, AnonymousClass1N7.class, AnonymousClass1N4.class);
                    AnonymousClass1NA r32 = this.A08;
                    this.A01 = (C22921Ni) constructor2.newInstance(r32.A02, r32.A05, r32.A08);
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException unused) {
                    this.A01 = null;
                }
            }
            return this.A01;
        } else if (i == 2) {
            if (this.A00 == null) {
                try {
                    Constructor<?> constructor3 = Class.forName("com.facebook.imagepipeline.memory.AshmemMemoryChunkPool").getConstructor(C14320t5.class, AnonymousClass1N7.class, AnonymousClass1N4.class);
                    AnonymousClass1NA r0 = this.A08;
                    this.A00 = (C22921Ni) constructor3.newInstance(r0.A02, r0.A05, r0.A08);
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException unused2) {
                    this.A00 = null;
                }
            }
            return this.A00;
        } else {
            throw new IllegalArgumentException("Invalid MemoryChunkType");
        }
    }

    public C23011Nw A01() {
        if (this.A03 == null) {
            this.A03 = new C23011Nw(A04(), DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET);
        }
        return this.A03;
    }

    public C37391vb A02() {
        C14320t5 r0;
        if (this.A04 == null) {
            AnonymousClass1NA r1 = this.A08;
            String str = r1.A0A;
            char c = 65535;
            switch (str.hashCode()) {
                case -1868884870:
                    if (str.equals("legacy_default_params")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1106578487:
                    if (str.equals("legacy")) {
                        c = 4;
                        break;
                    }
                    break;
                case -404562712:
                    if (str.equals("experimental")) {
                        c = 2;
                        break;
                    }
                    break;
                case -402149703:
                    if (str.equals("dummy_with_tracking")) {
                        c = 1;
                        break;
                    }
                    break;
                case 95945896:
                    if (str.equals("dummy")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c != 0) {
                if (c == 1) {
                    this.A04 = new AnonymousClass8XK();
                } else if (c == 2) {
                    int i = r1.A01;
                    int i2 = r1.A00;
                    C22901Ng A002 = C22901Ng.A00();
                    AnonymousClass1NA r12 = this.A08;
                    if (r12.A0B) {
                        r0 = r12.A02;
                    } else {
                        r0 = null;
                    }
                    this.A04 = new C23461Bfe(i, i2, A002, r0);
                } else if (c == 3) {
                    this.A04 = new C37381va(r1.A02, C30571iI.A00(), this.A08.A07, false);
                } else if (Build.VERSION.SDK_INT >= 21) {
                    this.A04 = new C37381va(r1.A02, r1.A03, r1.A07, false);
                }
            }
            this.A04 = new AnonymousClass8XJ();
        }
        return this.A04;
    }

    public C22841Na A03() {
        if (this.A05 == null) {
            AnonymousClass1NA r0 = this.A08;
            this.A05 = new C22841Na(r0.A02, r0.A04);
        }
        return this.A05;
    }

    public C22861Nc A04() {
        if (this.A06 == null) {
            AnonymousClass1NA r0 = this.A08;
            this.A06 = new C22861Nc(r0.A02, r0.A06, r0.A09);
        }
        return this.A06;
    }

    public C30661iR A05(int i) {
        if (this.A07 == null) {
            C05520Zg.A03(A00(i), AnonymousClass08S.A09("failed to get pool for chunk type: ", i));
            this.A07 = new C30661iR(A00(i), A01());
        }
        return this.A07;
    }

    public AnonymousClass1N0(AnonymousClass1NA r1) {
        C05520Zg.A02(r1);
        this.A08 = r1;
    }
}
