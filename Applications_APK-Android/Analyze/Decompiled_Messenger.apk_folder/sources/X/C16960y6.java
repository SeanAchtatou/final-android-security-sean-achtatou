package X;

import com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo;
import com.google.common.collect.ImmutableList;
import java.util.List;

/* renamed from: X.0y6  reason: invalid class name and case insensitive filesystem */
public class C16960y6 {
    public int A03() {
        return 0;
    }

    public void A04() {
    }

    public void A05() {
    }

    public void A06() {
    }

    public void A07() {
    }

    public void A08() {
    }

    public void A09() {
        if (this instanceof C34911qN) {
            C34901qM.A01(((C34911qN) this).A00);
        }
    }

    public void A0A() {
    }

    public void A0B() {
    }

    public void A0C() {
    }

    public void A0D() {
    }

    public void A0E() {
    }

    public void A0F() {
        if (this instanceof C34911qN) {
            C34901qM.A01(((C34911qN) this).A00);
        }
    }

    public void A0G() {
        if (this instanceof C34911qN) {
            C34901qM.A01(((C34911qN) this).A00);
        }
    }

    public void A0H(int i) {
    }

    public void A0I(int i, long j, boolean z, String str, boolean z2) {
    }

    public void A0J(FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo) {
    }

    public void A0K(ImmutableList immutableList) {
    }

    public void A0L(List list, List list2, List list3, List list4) {
    }

    public void A0M(boolean z) {
    }

    public void A0N(boolean z) {
    }

    public void A0O(String str, byte[] bArr) {
    }

    public void A0P(boolean z, boolean z2) {
    }

    public void A0Q(ImmutableList immutableList) {
    }

    public void A0R(boolean z, C160127bP r2) {
    }

    public boolean A0S(int i, String str, long j, boolean z, String str2, boolean z2, boolean z3) {
        if (!(this instanceof C34911qN)) {
            return false;
        }
        C34901qM.A01(((C34911qN) this).A00);
        return false;
    }
}
