package X;

import android.os.Handler;
import android.os.Looper;
import android.util.Printer;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

/* renamed from: X.0iO  reason: invalid class name and case insensitive filesystem */
public final class C09730iO {
    private static final WeakHashMap A04 = new WeakHashMap();
    public final Handler A00;
    public final Printer A01 = new AnonymousClass0iS(this);
    public final List A02 = new ArrayList();
    private final Runnable A03 = new AnonymousClass0iX(this);

    public static synchronized C09730iO A00(Looper looper) {
        C09730iO r1;
        synchronized (C09730iO.class) {
            r1 = (C09730iO) A04.get(looper);
            if (r1 == null) {
                r1 = new C09730iO(new Handler(looper));
                A04.put(looper, r1);
            }
        }
        return r1;
    }

    public void A01(AnonymousClass0jI r4) {
        boolean isEmpty;
        if (r4 != null) {
            synchronized (this.A02) {
                isEmpty = this.A02.isEmpty();
                this.A02.add(r4);
            }
            if (isEmpty) {
                AnonymousClass00S.A04(this.A00, this.A03, 1933236563);
            }
        }
    }

    public void A02(AnonymousClass0jI r4) {
        boolean isEmpty;
        if (r4 != null) {
            synchronized (this.A02) {
                this.A02.remove(r4);
                isEmpty = this.A02.isEmpty();
            }
            if (isEmpty) {
                AnonymousClass00S.A04(this.A00, this.A03, 1329964366);
            }
        }
    }

    private C09730iO(Handler handler) {
        this.A00 = handler;
    }
}
