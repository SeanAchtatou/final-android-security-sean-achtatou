package X;

import java.util.Arrays;

/* renamed from: X.06F  reason: invalid class name */
public final class AnonymousClass06F {
    public final int A00;
    public final String A01;
    public final String A02;
    public final Throwable A03;
    public final boolean A04;
    public final boolean A05 = false;

    public boolean equals(Object obj) {
        boolean z;
        boolean z2;
        boolean z3;
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                AnonymousClass06F r5 = (AnonymousClass06F) obj;
                if (this.A04 == r5.A04 && this.A05 == r5.A05 && this.A00 == r5.A00) {
                    String str = this.A01;
                    String str2 = r5.A01;
                    if (str == str2 || (str != null && str.equals(str2))) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (z) {
                        Throwable th = this.A03;
                        Throwable th2 = r5.A03;
                        if (th == th2 || (th != null && th.equals(th2))) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        if (z2) {
                            String str3 = this.A02;
                            String str4 = r5.A02;
                            if (str3 == str4 || (str3 != null && str3.equals(str4))) {
                                z3 = true;
                            } else {
                                z3 = false;
                            }
                            if (!z3 || 0 != 0) {
                                return false;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public static AnonymousClass06F A00(String str, String str2) {
        AnonymousClass06G r0 = new AnonymousClass06G();
        r0.A01 = str;
        r0.A02 = str2;
        return r0.A00();
    }

    public static AnonymousClass06F A01(String str, String str2, int i) {
        AnonymousClass06G r0 = new AnonymousClass06G();
        r0.A01 = str;
        r0.A02 = str2;
        r0.A00 = i;
        return r0.A00();
    }

    public static AnonymousClass06G A02(String str, String str2) {
        AnonymousClass06G r0 = new AnonymousClass06G();
        r0.A01 = str;
        r0.A02 = str2;
        return r0;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, this.A02, Boolean.valueOf(this.A04), Integer.valueOf(this.A00), false});
    }

    public AnonymousClass06F(AnonymousClass06G r2) {
        this.A01 = r2.A01;
        this.A02 = r2.A02;
        this.A03 = r2.A03;
        this.A04 = r2.A04;
        this.A00 = r2.A00;
    }
}
