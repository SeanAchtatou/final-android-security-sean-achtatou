package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.01x  reason: invalid class name and case insensitive filesystem */
public final class C003101x extends C003201y {
    public final int A00;

    public C003101x(Context context, File file, String str, int i) {
        super(context, str, file, "^lib/([^/]+)/([^/]+\\.so)$");
        this.A00 = i;
    }

    public AnonymousClass0EL A0D() {
        return new AnonymousClass0EK(this, this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a A[Catch:{ all -> 0x008d }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0045 A[SYNTHETIC, Splitter:B:14:0x0045] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] A0E() {
        /*
            r5 = this;
            java.io.File r0 = r5.A00
            java.io.File r1 = r0.getCanonicalFile()
            android.os.Parcel r2 = android.os.Parcel.obtain()
            r4 = 2
            r2.writeByte(r4)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r1.getPath()     // Catch:{ all -> 0x008d }
            r2.writeString(r0)     // Catch:{ all -> 0x008d }
            long r0 = r1.lastModified()     // Catch:{ all -> 0x008d }
            r2.writeLong(r0)     // Catch:{ all -> 0x008d }
            android.content.Context r0 = r5.A03     // Catch:{ all -> 0x008d }
            android.content.pm.PackageManager r3 = r0.getPackageManager()     // Catch:{ all -> 0x008d }
            r1 = 0
            if (r3 == 0) goto L_0x0030
            java.lang.String r0 = r0.getPackageName()     // Catch:{ NameNotFoundException | RuntimeException -> 0x0030 }
            android.content.pm.PackageInfo r0 = r3.getPackageInfo(r0, r1)     // Catch:{ NameNotFoundException | RuntimeException -> 0x0030 }
            int r0 = r0.versionCode     // Catch:{ NameNotFoundException | RuntimeException -> 0x0030 }
            goto L_0x0031
        L_0x0030:
            r0 = 0
        L_0x0031:
            r2.writeInt(r0)     // Catch:{ all -> 0x008d }
            int r0 = r5.A00     // Catch:{ all -> 0x008d }
            r3 = 1
            r0 = r0 & r3
            if (r0 != 0) goto L_0x0045
            r2.writeByte(r1)     // Catch:{ all -> 0x008d }
            byte[] r0 = r2.marshall()     // Catch:{ all -> 0x008d }
            r2.recycle()
            return r0
        L_0x0045:
            android.content.Context r0 = r5.A03     // Catch:{ all -> 0x008d }
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()     // Catch:{ all -> 0x008d }
            java.lang.String r1 = r0.nativeLibraryDir     // Catch:{ all -> 0x008d }
            if (r1 != 0) goto L_0x005a
            r2.writeByte(r3)     // Catch:{ all -> 0x008d }
            byte[] r0 = r2.marshall()     // Catch:{ all -> 0x008d }
            r2.recycle()
            return r0
        L_0x005a:
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x008d }
            r0.<init>(r1)     // Catch:{ all -> 0x008d }
            java.io.File r1 = r0.getCanonicalFile()     // Catch:{ all -> 0x008d }
            boolean r0 = r1.exists()     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0074
            r2.writeByte(r3)     // Catch:{ all -> 0x008d }
            byte[] r0 = r2.marshall()     // Catch:{ all -> 0x008d }
            r2.recycle()
            return r0
        L_0x0074:
            r2.writeByte(r4)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r1.getPath()     // Catch:{ all -> 0x008d }
            r2.writeString(r0)     // Catch:{ all -> 0x008d }
            long r0 = r1.lastModified()     // Catch:{ all -> 0x008d }
            r2.writeLong(r0)     // Catch:{ all -> 0x008d }
            byte[] r0 = r2.marshall()     // Catch:{ all -> 0x008d }
            r2.recycle()
            return r0
        L_0x008d:
            r0 = move-exception
            r2.recycle()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C003101x.A0E():byte[]");
    }
}
