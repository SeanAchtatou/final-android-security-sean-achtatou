package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.app.INotificationSideChannel;
import android.support.v4.app.INotificationSideChannel$Stub$Proxy;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0uy  reason: invalid class name and case insensitive filesystem */
public final class C15250uy implements ServiceConnection, Handler.Callback {
    public Set A00 = new HashSet();
    public final Context A01;
    public final Handler A02;
    public final Map A03 = new HashMap();
    private final HandlerThread A04;

    private void A00(AnonymousClass8TG r6) {
        boolean z;
        if (Log.isLoggable("NotifManCompat", 3)) {
            r6.A02.size();
        }
        if (!r6.A02.isEmpty()) {
            if (r6.A03) {
                z = true;
            } else {
                boolean A022 = C006406k.A02(this.A01, new Intent("android.support.BIND_NOTIFICATION_SIDE_CHANNEL").setComponent(r6.A04), this, 33, -35058578);
                r6.A03 = A022;
                if (A022) {
                    r6.A00 = 0;
                } else {
                    Log.w("NotifManCompat", "Unable to bind to listener " + r6.A04);
                    C006406k.A01(this.A01, this, -174721456);
                }
                z = r6.A03;
            }
            if (!z || r6.A01 == null) {
                A01(r6);
                return;
            }
            while (true) {
                C32461ln r1 = (C32461ln) r6.A02.peek();
                if (r1 == null) {
                    break;
                }
                try {
                    r1.C4w(r6.A01);
                    r6.A02.remove();
                } catch (DeadObjectException unused) {
                } catch (RemoteException e) {
                    Log.w("NotifManCompat", "RemoteException communicating with " + r6.A04, e);
                }
            }
            if (!r6.A02.isEmpty()) {
                A01(r6);
            }
        }
    }

    private void A01(AnonymousClass8TG r7) {
        if (!this.A02.hasMessages(3, r7.A04)) {
            int i = r7.A00 + 1;
            r7.A00 = i;
            if (i > 6) {
                Log.w("NotifManCompat", "Giving up on delivering " + r7.A02.size() + " tasks to " + r7.A04 + " after " + r7.A00 + " retries");
                r7.A02.clear();
                return;
            }
            int i2 = (1 << (i - 1)) * AnonymousClass1Y3.A87;
            this.A02.sendMessageDelayed(this.A02.obtainMessage(3, r7.A04), (long) i2);
        }
    }

    public boolean handleMessage(Message message) {
        Set set;
        INotificationSideChannel iNotificationSideChannel$Stub$Proxy;
        int i = message.what;
        if (i == 0) {
            C32461ln r9 = (C32461ln) message.obj;
            String string = Settings.Secure.getString(this.A01.getContentResolver(), "enabled_notification_listeners");
            synchronized (C15240ux.A05) {
                if (string != null) {
                    if (!string.equals(C15240ux.A02)) {
                        HashSet hashSet = new HashSet(r2);
                        for (String unflattenFromString : string.split(":", -1)) {
                            ComponentName unflattenFromString2 = ComponentName.unflattenFromString(unflattenFromString);
                            if (unflattenFromString2 != null) {
                                hashSet.add(unflattenFromString2.getPackageName());
                            }
                        }
                        C15240ux.A03 = hashSet;
                        C15240ux.A02 = string;
                    }
                }
                set = C15240ux.A03;
            }
            if (!set.equals(this.A00)) {
                this.A00 = set;
                List<ResolveInfo> queryIntentServices = this.A01.getPackageManager().queryIntentServices(new Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 0);
                HashSet<ComponentName> hashSet2 = new HashSet<>();
                for (ResolveInfo next : queryIntentServices) {
                    if (set.contains(next.serviceInfo.packageName)) {
                        ServiceInfo serviceInfo = next.serviceInfo;
                        ComponentName componentName = new ComponentName(serviceInfo.packageName, serviceInfo.name);
                        if (next.serviceInfo.permission != null) {
                            Log.w("NotifManCompat", "Permission present on component " + componentName + ", not adding listener record.");
                        } else {
                            hashSet2.add(componentName);
                        }
                    }
                }
                for (ComponentName componentName2 : hashSet2) {
                    if (!this.A03.containsKey(componentName2)) {
                        this.A03.put(componentName2, new AnonymousClass8TG(componentName2));
                    }
                }
                Iterator it = this.A03.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    if (!hashSet2.contains(entry.getKey())) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            entry.getKey();
                        }
                        AnonymousClass8TG r3 = (AnonymousClass8TG) entry.getValue();
                        if (r3.A03) {
                            C006406k.A01(this.A01, this, 1130711122);
                            r3.A03 = false;
                        }
                        r3.A01 = null;
                        it.remove();
                    }
                }
            }
            for (AnonymousClass8TG r1 : this.A03.values()) {
                r1.A02.add(r9);
                A00(r1);
            }
        } else if (i == 1) {
            AnonymousClass4ZI r0 = (AnonymousClass4ZI) message.obj;
            ComponentName componentName3 = r0.A00;
            IBinder iBinder = r0.A01;
            AnonymousClass8TG r2 = (AnonymousClass8TG) this.A03.get(componentName3);
            if (r2 != null) {
                if (iBinder == null) {
                    iNotificationSideChannel$Stub$Proxy = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface(AnonymousClass80H.$const$string(AnonymousClass1Y3.A33));
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof INotificationSideChannel)) {
                        iNotificationSideChannel$Stub$Proxy = new INotificationSideChannel$Stub$Proxy(iBinder);
                    } else {
                        iNotificationSideChannel$Stub$Proxy = (INotificationSideChannel) queryLocalInterface;
                    }
                }
                r2.A01 = iNotificationSideChannel$Stub$Proxy;
                r2.A00 = 0;
                A00(r2);
                return true;
            }
        } else if (i == 2) {
            AnonymousClass8TG r22 = (AnonymousClass8TG) this.A03.get((ComponentName) message.obj);
            if (r22 != null) {
                if (r22.A03) {
                    C006406k.A01(this.A01, this, 1130711122);
                    r22.A03 = false;
                }
                r22.A01 = null;
                return true;
            }
        } else if (i != 3) {
            return false;
        } else {
            AnonymousClass8TG r02 = (AnonymousClass8TG) this.A03.get((ComponentName) message.obj);
            if (r02 != null) {
                A00(r02);
            }
        }
        return true;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.A02.obtainMessage(1, new AnonymousClass4ZI(componentName, iBinder)).sendToTarget();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.A02.obtainMessage(2, componentName).sendToTarget();
    }

    public C15250uy(Context context) {
        this.A01 = context;
        HandlerThread handlerThread = new HandlerThread("NotificationManagerCompat");
        this.A04 = handlerThread;
        handlerThread.start();
        this.A02 = new Handler(this.A04.getLooper(), this);
    }
}
