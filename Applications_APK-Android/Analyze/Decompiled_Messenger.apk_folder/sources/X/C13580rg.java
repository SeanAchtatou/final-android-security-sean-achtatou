package X;

import android.view.View;

/* renamed from: X.0rg  reason: invalid class name and case insensitive filesystem */
public interface C13580rg {
    void detachRecyclableViewFromParent(View view);

    void removeRecyclableViewFromParent(View view, boolean z);
}
