package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0qv  reason: invalid class name and case insensitive filesystem */
public final class C13220qv extends Enum {
    private static final /* synthetic */ C13220qv[] A00;
    public static final C13220qv A01;
    public static final C13220qv A02;
    public static final C13220qv A03;
    public static final C13220qv A04;
    public static final C13220qv A05;
    public static final C13220qv A06;
    public static final C13220qv A07;
    public static final C13220qv A08;
    public static final C13220qv A09;
    public static final C13220qv A0A;
    public static final C13220qv A0B;
    public static final C13220qv A0C;
    public static final C13220qv A0D;
    public static final C13220qv A0E;
    public static final C13220qv A0F;
    public static final C13220qv A0G;
    public static final C13220qv A0H;
    public static final C13220qv A0I;
    public static final C13220qv A0J;
    public static final C13220qv A0K;
    public static final C13220qv A0L;
    public static final C13220qv A0M;
    public static final C13220qv A0N;

    static {
        C13220qv r0 = new C13220qv(AnonymousClass24B.$const$string(49), 0);
        A0M = r0;
        C13220qv r02 = new C13220qv("MONTAGE_VIEWER", 1);
        A0E = r02;
        C13220qv r03 = new C13220qv("MONTAGE_CHATHEAD", 2);
        A0C = r03;
        C13220qv r04 = new C13220qv("OMNI_PICKER", 3);
        A0H = r04;
        C13220qv r05 = new C13220qv("FAB", 4);
        C13220qv r2 = new C13220qv("ROOM_WITH_SHARE_SHEET", 5);
        A0J = r2;
        C13220qv r06 = new C13220qv("INBOX_AD", 6);
        C13220qv r07 = new C13220qv("SPONSORED_MESSAGE", 7);
        C13220qv r11 = new C13220qv("DISCOVER", 8);
        A08 = r11;
        C13220qv r08 = new C13220qv("ACTIVE", 9);
        C13220qv r12 = new C13220qv("ACTIVE_NOW_CHAT_HEAD", 10);
        A01 = r12;
        C13220qv r09 = new C13220qv("BYMM", 11);
        C13220qv r010 = new C13220qv("CYMK", 12);
        C13220qv r011 = new C13220qv("THREAD_SUMMARY", 13);
        A0N = r011;
        C13220qv r3 = new C13220qv("CHAT_HEAD", 14);
        A05 = r3;
        C13220qv r32 = new C13220qv("SEARCH", 15);
        A0K = r32;
        C13220qv r33 = new C13220qv("SEARCH_IN_CHAT", 16);
        A0L = r33;
        C13220qv r15 = new C13220qv("MESSAGE_SEARCH", 17);
        A0B = r15;
        C13220qv r34 = new C13220qv("PEOPLE_TAB", 18);
        C13220qv r35 = new C13220qv("PINNED_GROUPS", 19);
        C13220qv r14 = new C13220qv("OTHER", 20);
        A0I = r14;
        C13220qv r13 = new C13220qv(AnonymousClass24B.$const$string(146), 21);
        A0G = r13;
        C13220qv r36 = new C13220qv("CONNECTIONS_TAB", 22);
        C13220qv r37 = new C13220qv("ACTIVITY_RECREATION", 23);
        A03 = r37;
        C13220qv r4 = new C13220qv("BYPASS_DIRECT_BUTTON", 24);
        C13220qv r42 = new C13220qv("FEED", 25);
        C13220qv r1 = new C13220qv("BIRTHDAY_XMA", 26);
        A04 = r1;
        C13220qv r10 = new C13220qv(AnonymousClass24B.$const$string(115), 27);
        A06 = r10;
        C13220qv r5 = new C13220qv("HIGH_SCHOOL_NETWORKS_TAB", 28);
        C13220qv r52 = new C13220qv("CONTACT_CARD", 29);
        C13220qv r9 = new C13220qv("M_ASSISTANT", 30);
        A0F = r9;
        C13220qv r8 = new C13220qv("GROUP_CHAT_FROM_FB_GROUP", 31);
        A09 = r8;
        C13220qv r53 = new C13220qv("COWATCH_INBOX_CTA", 32);
        C13220qv r54 = new C13220qv("EVENT_CONFIRMED_GOING_CTA", 33);
        C13220qv r55 = new C13220qv("FACEBOOK_LIVE_SHARE_SHEET", 34);
        C13220qv r7 = new C13220qv("CONVERSATION_STARTER", 35);
        A07 = r7;
        C13220qv r6 = new C13220qv("HORIZONTAL_TILE_INBOX", 36);
        A0A = r6;
        C13220qv r56 = new C13220qv("ACTIVE_NOW_INBOX", 37);
        A02 = r56;
        C13220qv r41 = new C13220qv("MONTAGE_INBOX", 38);
        A0D = r41;
        C13220qv[] r43 = new C13220qv[39];
        System.arraycopy(new C13220qv[]{r0, r02, r03, r04, r05, r2, r06, r07, r11, r08, r12, r09, r010, r011, r3, r32, r33, r15, r34, r35, r14, r13, r36, r37, r4, r42, r1}, 0, r43, 0, 27);
        System.arraycopy(new C13220qv[]{r10, r5, r52, r9, r8, r53, r54, r55, r7, r6, r56, r41}, 0, r43, 27, 12);
        A00 = r43;
    }

    public static C13220qv valueOf(String str) {
        return (C13220qv) Enum.valueOf(C13220qv.class, str);
    }

    public static C13220qv[] values() {
        return (C13220qv[]) A00.clone();
    }

    private C13220qv(String str, int i) {
    }
}
