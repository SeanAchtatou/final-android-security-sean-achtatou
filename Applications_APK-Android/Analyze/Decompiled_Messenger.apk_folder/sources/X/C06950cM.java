package X;

import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: X.0cM  reason: invalid class name and case insensitive filesystem */
public final class C06950cM {
    public static int A00(long j) {
        int i = (int) j;
        boolean z = false;
        if (((long) i) == j) {
            z = true;
        }
        Preconditions.checkArgument(z, "Out of range: %s", j);
        return i;
    }

    public static int A01(long j) {
        if (j > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (j < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int) j;
    }

    public static int[] A02(Collection collection) {
        if (collection instanceof C22707B8v) {
            C22707B8v b8v = (C22707B8v) collection;
            return Arrays.copyOfRange(b8v.array, b8v.start, b8v.end);
        }
        Object[] array = collection.toArray();
        int length = array.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            Object obj = array[i];
            Preconditions.checkNotNull(obj);
            iArr[i] = ((Number) obj).intValue();
        }
        return iArr;
    }
}
