package X;

import android.view.animation.Interpolator;

/* renamed from: X.0SP  reason: invalid class name */
public final class AnonymousClass0SP extends C03550Om {
    public final /* synthetic */ AnonymousClass01E A00;

    public void A00(float f) {
        float f2 = ((1.0f - 0.0f) * f) + 0.0f;
        this.A00.A03.setRotation((float) AnonymousClass0O6.A02(0, AnonymousClass1Y3.A2r, f));
        this.A00.A03.setScaleX(f2);
        this.A00.A03.setScaleY(f2);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0SP(AnonymousClass01E r1, Interpolator interpolator, long j) {
        super(interpolator, j);
        this.A00 = r1;
    }
}
