package X;

import com.google.common.base.Preconditions;
import java.util.HashMap;

/* renamed from: X.1uW  reason: invalid class name and case insensitive filesystem */
public final class C37041uW {
    public C94794fE A00;
    private C52942jw A01 = new C52942jw();
    private HashMap A02 = new HashMap();
    private boolean A03;
    private final int A04;
    private final int A05;
    private final C17440yu A06;

    public void A01(Boolean bool) {
        synchronized (this) {
            HashMap A002 = A00(this, true);
            if (A002 != null) {
                C94794fE r0 = this.A00;
                Preconditions.checkNotNull(r0);
                r0.A01(A002, bool);
            }
        }
    }

    public void A02(Integer num, String str, String str2, Integer num2, AnonymousClass0VS r12, Integer num3, Boolean bool, long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9) {
        synchronized (this) {
            C52942jw r0 = this.A01;
            r0.A0C = num;
            r0.A0G = str;
            r0.A0F = str2;
            r0.A0D = num2;
            r0.A0A = r12;
            r0.A0E = num3;
            r0.A0B = bool;
            boolean z = false;
            if (!this.A06.A01) {
                z = true;
            }
            r0.A0H = z;
            C52942jw r2 = (C52942jw) this.A02.get(this.A01);
            if (r2 == null) {
                r2 = this.A01;
                this.A02.put(r2, r2);
                this.A01 = new C52942jw();
            }
            r2.A07 += j;
            r2.A01 += j2;
            r2.A09 += j3;
            r2.A00 += j4;
            r2.A04 += j5;
            r2.A03 += j6;
            r2.A02 += j7;
            r2.A06 += j8;
            r2.A05 += j9;
            long j10 = r2.A08 + 1;
            r2.A08 = j10;
            if (j10 >= ((long) this.A04)) {
                this.A03 = true;
            }
            HashMap A002 = A00(this, false);
            if (A002 != null) {
                C94794fE r1 = this.A00;
                Preconditions.checkNotNull(r1);
                r1.A01(A002, null);
            }
        }
    }

    public static HashMap A00(C37041uW r3, boolean z) {
        if (r3.A00 == null || r3.A02.isEmpty() || (!z && r3.A02.size() < r3.A05 && !r3.A03)) {
            return null;
        }
        HashMap hashMap = r3.A02;
        r3.A02 = new HashMap();
        r3.A03 = false;
        return hashMap;
    }

    public C37041uW(C17440yu r2, int i, int i2) {
        this.A06 = r2;
        this.A05 = i;
        this.A04 = i2;
    }
}
