package X;

/* renamed from: X.1O8  reason: invalid class name */
public final class AnonymousClass1O8 implements C22761Ms {
    private AnonymousClass0UN A00;

    public static final AnonymousClass1O8 A00(AnonymousClass1XY r1) {
        return new AnonymousClass1O8(r1);
    }

    public AnonymousClass1S9 AWg(AnonymousClass1NY r7, int i, C33271nJ r9, C23541Px r10) {
        AnonymousClass97B r5 = new AnonymousClass97B(AnonymousClass1PS.A00(r7.A0A), 24, i, (AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, this.A00));
        AnonymousClass1NY r4 = new AnonymousClass1NY(AnonymousClass1PS.A01(r5));
        if (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(283893043301993L)) {
            return new C33281nK(((C22771Mt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AEG, this.A00)).AWg(r4, r5.size(), r9, r10));
        }
        return ((C22771Mt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AEG, this.A00)).AWg(r4, r5.size(), r9, r10);
    }

    public AnonymousClass1O8(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
