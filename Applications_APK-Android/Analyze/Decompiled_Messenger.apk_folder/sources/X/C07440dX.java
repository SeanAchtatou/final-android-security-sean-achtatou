package X;

import com.facebook.acra.ErrorReporter;
import com.facebook.auth.viewercontext.ViewerContext;
import com.google.common.base.Preconditions;
import java.util.ArrayList;

/* renamed from: X.0dX  reason: invalid class name and case insensitive filesystem */
public abstract class C07440dX implements C26941cO {
    public long A00 = ErrorReporter.MAX_REPORT_AGE;
    public AnonymousClass04a A01;
    public C09290gx A02 = C09290gx.NETWORK_ONLY;
    public ArrayList A03 = new ArrayList();
    public boolean A04 = false;
    public boolean A05 = false;
    public boolean A06 = false;
    public boolean A07 = false;
    public String[] A08 = new String[0];
    private ViewerContext A09;
    public final AnonymousClass04a A0A = new AnonymousClass04a();
    public final C10880l0 A0B;

    public C07440dX A04(String str) {
        this.A03.add(str);
        return this;
    }

    public ViewerContext B9R() {
        return this.A09;
    }

    public C07440dX(C10880l0 r3) {
        Preconditions.checkNotNull(r3);
        this.A0B = r3;
    }

    public C07440dX A03(C09290gx r1) {
        Preconditions.checkNotNull(r1);
        this.A02 = r1;
        return this;
    }

    public C07440dX A02(ViewerContext viewerContext) {
        this.A09 = viewerContext;
        return this;
    }

    public C07440dX A05(String str) {
        return this;
    }

    public C07440dX A06(boolean z) {
        this.A04 = z;
        return this;
    }

    public C07440dX A07(boolean z) {
        this.A05 = z;
        return this;
    }

    public C07440dX A08(boolean z) {
        this.A06 = z;
        return this;
    }
}
