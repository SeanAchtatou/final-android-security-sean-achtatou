package X;

import android.view.Window;
import androidx.fragment.app.Fragment;
import com.facebook.messaging.threadview.params.ThreadViewParams;
import com.google.common.base.Preconditions;

/* renamed from: X.0w1  reason: invalid class name and case insensitive filesystem */
public final class C15810w1 extends C15850w5 {
    public final /* synthetic */ C32301lX A00;

    public void ATE() {
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C15810w1(C32301lX r1, C32291lW r2) {
        super(r2);
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0010, code lost:
        if (r1 != false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ATF() {
        /*
            r2 = this;
            X.1lX r0 = r2.A00
            r1 = 2131298121(0x7f090749, float:1.8214206E38)
            X.0qW r0 = r0.A01
            androidx.fragment.app.Fragment r0 = r0.A0O(r1)
            if (r0 == 0) goto L_0x0012
            boolean r1 = r0.A0b
            r0 = 1
            if (r1 == 0) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            if (r0 == 0) goto L_0x001a
            X.1lX r0 = r2.A00
            r0.A05()
        L_0x001a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15810w1.ATF():void");
    }

    public void ATI() {
        if (C32301lX.A03(this.A00)) {
            C13150qn A002 = C32301lX.A00(this.A00);
            Fragment fragment = A002.A03;
            if (fragment != null) {
                C15830w3.A01(fragment);
                A002.A05.A0I(A002.A03);
            }
            A002.A01();
            return;
        }
        this.A00.A06();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002d, code lost:
        if (r1 != false) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ATL() {
        /*
            r5 = this;
            X.1lX r4 = r5.A00
            X.0g6 r3 = r4.A02
            int r2 = X.AnonymousClass1Y3.AQl
            X.0UN r1 = r3.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.14b r2 = (X.C185414b) r2
            X.0gA r1 = r3.A05
            java.lang.String r0 = "exit_thread"
            r2.AOI(r1, r0)
            X.C32301lX.A01(r4)
            X.0qn r4 = X.C32301lX.A00(r4)
            r4.A03()
            androidx.fragment.app.Fragment r3 = r4.A03
            if (r3 == 0) goto L_0x0066
            X.0wo r2 = r4.A05
            androidx.fragment.app.Fragment r0 = r4.A04
            if (r0 == 0) goto L_0x002f
            boolean r1 = r0.A0b
            r0 = 1
            if (r1 == 0) goto L_0x0030
        L_0x002f:
            r0 = 0
        L_0x0030:
            r1 = 2130771980(0x7f01000c, float:1.7147065E38)
            if (r0 == 0) goto L_0x0038
            r1 = 2130771978(0x7f01000a, float:1.7147061E38)
        L_0x0038:
            r0 = 0
            r2.A07(r1, r0)
            r2.A0J(r3)
        L_0x003f:
            r4.A01()
            X.1lX r0 = r5.A00
            X.1q6 r0 = r0.A08
            X.0qf r2 = r0.A00
            int r1 = X.AnonymousClass1Y3.Anh
            X.0UN r0 = r2.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = (com.facebook.mig.scheme.interfaces.MigColorScheme) r1
            X.1q6 r0 = r2.A0B
            int r1 = r1.B9m()
            X.0qf r0 = r0.A00
            android.app.Activity r0 = r0.A00
            android.view.Window r0 = r0.getWindow()
            if (r0 == 0) goto L_0x0065
            X.C15990wJ.A01(r0, r1, r1)
        L_0x0065:
            return
        L_0x0066:
            r4.A04()
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15810w1.ATL():void");
    }

    public void Bve(C64663Ct r5) {
        C13150qn A002 = C32301lX.A00(this.A00);
        if (A002.A06 != null) {
            C16290wo r2 = A002.A05;
            r2.A07(2130771980, 0);
            Fragment fragment = A002.A00;
            if (fragment == null) {
                Fragment A003 = C55632oP.A00(r5);
                A002.A05.A08(2131298121, A003);
                A002.A00 = A003;
            } else {
                r2.A0J(fragment);
            }
        }
        A002.A02();
        A002.A03();
        A002.A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001e, code lost:
        if (r1 != false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Bvx() {
        /*
            r6 = this;
            X.1lX r4 = r6.A00
            int r1 = X.AnonymousClass1Y3.A7R
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.6cn r1 = (X.C138236cn) r1
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.A03(r0)
            X.0qn r5 = X.C32301lX.A00(r4)
            X.0wo r2 = r5.A05
            androidx.fragment.app.Fragment r0 = r5.A04
            if (r0 == 0) goto L_0x0020
            boolean r1 = r0.A0b
            r0 = 1
            if (r1 == 0) goto L_0x0021
        L_0x0020:
            r0 = 0
        L_0x0021:
            r1 = 2130771980(0x7f01000c, float:1.7147065E38)
            if (r0 == 0) goto L_0x0029
            r1 = 2130771978(0x7f01000a, float:1.7147061E38)
        L_0x0029:
            r0 = 0
            r2.A07(r1, r0)
            r1 = 2131300410(0x7f09103a, float:1.8218849E38)
            X.0wL r0 = new X.0wL
            r0.<init>()
            r2.A08(r1, r0)
            r5.A02()
            r5.A03()
            androidx.fragment.app.Fragment r3 = r5.A00
            if (r3 == 0) goto L_0x0052
            boolean r0 = r3.A0b
            if (r0 != 0) goto L_0x0052
            X.0wo r2 = r5.A05
            r1 = 0
            r0 = 2130771979(0x7f01000b, float:1.7147063E38)
            r2.A07(r1, r0)
            r2.A0H(r3)
        L_0x0052:
            r5.A01()
            int r2 = X.AnonymousClass1Y3.A7C
            X.0UN r1 = r4.A00
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0rN r1 = (X.C13400rN) r1
            java.lang.String r0 = "search"
            X.C13400rN.A01(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15810w1.Bvx():void");
    }

    public void CKx(int i) {
        Window window = this.A00.A08.A00.A00.getWindow();
        if (window != null) {
            C15990wJ.A01(window, i, i);
        }
    }

    public void BwC(ThreadViewParams threadViewParams) {
        Preconditions.checkNotNull(threadViewParams);
        this.A00.A07(threadViewParams);
    }
}
