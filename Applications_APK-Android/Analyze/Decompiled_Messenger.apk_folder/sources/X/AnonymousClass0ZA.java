package X;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.0ZA  reason: invalid class name */
public final class AnonymousClass0ZA {
    public static void A01(InputStream inputStream) {
        try {
            A00(inputStream, true);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static void A00(Closeable closeable, boolean z) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                if (!z) {
                    throw e;
                }
            }
        }
    }
}
