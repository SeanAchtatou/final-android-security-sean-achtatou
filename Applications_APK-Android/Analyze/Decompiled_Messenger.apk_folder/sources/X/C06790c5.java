package X;

import android.content.IntentFilter;
import android.os.Handler;
import java.util.Map;

/* renamed from: X.0c5  reason: invalid class name and case insensitive filesystem */
public final class C06790c5 {
    private boolean A00 = false;
    public final C06870cD A01;
    private final IntentFilter A02;
    private final Handler A03;
    public final /* synthetic */ C04450Us A04;

    public synchronized void A00() {
        boolean z;
        if (this.A00) {
            C010708t.A0I(this.A04.getClass().getSimpleName(), "Called registerBroadcastReceiver twice.");
        } else {
            IntentFilter intentFilter = this.A02;
            if (intentFilter == null) {
                intentFilter = this.A01.A09();
            }
            if (intentFilter != null) {
                int i = 0;
                while (true) {
                    if (i >= intentFilter.countActions()) {
                        break;
                    } else if (intentFilter.getAction(i).contains("com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP")) {
                        z = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
            z = false;
            if (!z || this.A03 != null) {
                this.A04.A02(this.A01, intentFilter, this.A03);
            } else {
                C04450Us r2 = this.A04;
                r2.A02(this.A01, intentFilter, r2.A00);
            }
            this.A00 = true;
        }
    }

    public synchronized void A01() {
        if (this.A00) {
            this.A04.A01(this.A01);
            this.A00 = false;
        }
    }

    public synchronized boolean A02() {
        return this.A00;
    }

    public C06790c5(C04450Us r4, Map map, IntentFilter intentFilter, Handler handler) {
        this.A04 = r4;
        this.A01 = new C25421Zo(this, map.entrySet().iterator(), r4.getClass());
        this.A02 = intentFilter;
        this.A03 = handler;
    }
}
