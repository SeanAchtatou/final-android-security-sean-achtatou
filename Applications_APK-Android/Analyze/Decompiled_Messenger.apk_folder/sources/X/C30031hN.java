package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.common.calltoaction.model.CTAInformationIdentify;

/* renamed from: X.1hN  reason: invalid class name and case insensitive filesystem */
public final class C30031hN implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new CTAInformationIdentify(parcel);
    }

    public Object[] newArray(int i) {
        return new CTAInformationIdentify[i];
    }
}
