package X;

import android.content.Context;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.google.common.base.Preconditions;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1nm  reason: invalid class name and case insensitive filesystem */
public final class C33561nm {
    private static final String[] A03 = {"gps", "network"};
    public final Context A00;
    public final LocationManager A01;
    public final boolean A02;

    public static Integer A00(C33561nm r8, Integer num, Set set, Set set2) {
        Integer num2;
        LocationProvider locationProvider;
        Integer num3 = null;
        for (String str : A03) {
            try {
                Preconditions.checkNotNull(str);
                Preconditions.checkNotNull(r8.A01);
                try {
                    locationProvider = r8.A01.getProvider(str);
                } catch (IllegalArgumentException | IllegalStateException | NullPointerException unused) {
                    locationProvider = null;
                }
                if (locationProvider == null) {
                    num2 = AnonymousClass07B.A01;
                } else if (locationProvider.getPowerRequirement() == 3 && num != AnonymousClass07B.A0C) {
                    num2 = AnonymousClass07B.A01;
                } else if (locationProvider.hasMonetaryCost() && !r8.A02) {
                    num2 = AnonymousClass07B.A01;
                } else if (!r8.A01.isProviderEnabled(str)) {
                    num2 = AnonymousClass07B.A0C;
                } else {
                    num2 = AnonymousClass07B.A0N;
                }
            } catch (SecurityException unused2) {
                num2 = AnonymousClass07B.A00;
            }
            if (num2 == AnonymousClass07B.A0N) {
                if (set != null) {
                    set.add(str);
                }
            } else if (num2 == AnonymousClass07B.A0C && set2 != null) {
                set2.add(str);
            }
            if (num3 == null || (num2 != null && num3.compareTo(num2) < 0)) {
                num3 = num2;
            }
        }
        return num3;
    }

    private boolean A01() {
        boolean z = false;
        try {
            if (this.A00.checkCallingOrSelfPermission(TurboLoader.Locator.$const$string(10)) == 0) {
                z = true;
            }
        } catch (Throwable unused) {
        }
        if (!z) {
            boolean z2 = false;
            try {
                if (this.A00.checkCallingOrSelfPermission(TurboLoader.Locator.$const$string(11)) == 0) {
                    z2 = true;
                }
            } catch (Throwable unused2) {
            }
            if (z2) {
                return true;
            }
            return false;
        }
        return true;
    }

    public AnonymousClass4W9 A02() {
        return A03(AnonymousClass07B.A0C);
    }

    public Integer A04() {
        boolean z;
        boolean z2 = false;
        if (Build.VERSION.SDK_INT >= 29) {
            z2 = true;
        }
        if (!A01()) {
            return AnonymousClass07B.A00;
        }
        if (!z2) {
            return AnonymousClass07B.A01;
        }
        if (z2) {
            z = false;
            try {
                if (this.A00.checkCallingOrSelfPermission(C22298Ase.$const$string(64)) == 0) {
                    z = true;
                }
            } catch (Throwable unused) {
            }
        } else {
            z = false;
        }
        if (z) {
            return AnonymousClass07B.A0N;
        }
        return AnonymousClass07B.A0C;
    }

    public Integer A05() {
        return A00(this, AnonymousClass07B.A0C, null, null);
    }

    public C33561nm(Context context, LocationManager locationManager, boolean z) {
        this.A00 = context;
        this.A01 = locationManager;
        this.A02 = z;
    }

    public AnonymousClass4W9 A03(Integer num) {
        boolean z;
        Integer num2;
        if (!A01()) {
            Integer num3 = AnonymousClass07B.A00;
            return new AnonymousClass4W9(num3, num3, new HashSet(), new HashSet());
        }
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        Integer A002 = A00(this, num, hashSet, hashSet2);
        if (A002 != AnonymousClass07B.A0N) {
            num2 = AnonymousClass07B.A00;
        } else {
            int i = Build.VERSION.SDK_INT;
            boolean z2 = false;
            if (i >= 29) {
                z2 = true;
            }
            if (!z2) {
                num2 = AnonymousClass07B.A01;
            } else {
                boolean z3 = false;
                if (i >= 29) {
                    z3 = true;
                }
                if (z3) {
                    z = false;
                    try {
                        if (this.A00.checkCallingOrSelfPermission(C22298Ase.$const$string(64)) == 0) {
                            z = true;
                        }
                    } catch (Throwable unused) {
                    }
                } else {
                    z = false;
                }
                if (z) {
                    num2 = AnonymousClass07B.A0N;
                } else {
                    num2 = AnonymousClass07B.A0C;
                }
            }
        }
        return new AnonymousClass4W9(A002, num2, hashSet, hashSet2);
    }
}
