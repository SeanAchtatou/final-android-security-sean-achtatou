package X;

import com.facebook.common.util.TriState;
import com.facebook.jni.NativeSoftErrorReporterProxy;
import java.util.List;

/* renamed from: X.0Ok  reason: invalid class name and case insensitive filesystem */
public final class C03530Ok implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.jni.NativeSoftErrorReporterProxy$1";
    public final /* synthetic */ AnonymousClass09P A00;
    public final /* synthetic */ List A01;

    public C03530Ok(List list, AnonymousClass09P r2) {
        this.A01 = list;
        this.A00 = r2;
    }

    public void run() {
        TriState CEb;
        AnonymousClass09Q r0 = NativeSoftErrorReporterProxy.sErrorReportingGkReader;
        if (r0 == null) {
            CEb = TriState.UNSET;
        } else {
            CEb = r0.CEb();
        }
        if (CEb == TriState.YES) {
            for (AnonymousClass06F CGQ : this.A01) {
                this.A00.CGQ(CGQ);
            }
        }
    }
}
