package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.quicklog.QuickPerformanceLoggerProvider;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1xv  reason: invalid class name and case insensitive filesystem */
public final class C38741xv {
    private static volatile C38741xv A02;
    public boolean A00;
    public final QuickPerformanceLogger A01 = QuickPerformanceLoggerProvider.getQPLInstance();

    public static final C38741xv A00(AnonymousClass1XY r3) {
        if (A02 == null) {
            synchronized (C38741xv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A02 = new C38741xv();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }
}
