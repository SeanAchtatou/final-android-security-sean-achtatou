package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1LT  reason: invalid class name */
public final class AnonymousClass1LT {
    private static volatile AnonymousClass1LT A05;
    public int A00;
    public int A01;
    public long A02;
    public C200739cf A03;
    public final AnonymousClass06B A04 = AnonymousClass067.A02();

    public static final AnonymousClass1LT A00(AnonymousClass1XY r3) {
        if (A05 == null) {
            synchronized (AnonymousClass1LT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A05 = new AnonymousClass1LT();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static void A01(AnonymousClass1LT r7) {
        String str;
        C200739cf r3 = r7.A03;
        if (r3 != null && r7.A00 != 0) {
            switch (r7.A01) {
                case 5505026:
                    str = "warm";
                    break;
                case 5505027:
                    str = "cold";
                    break;
                case 5505028:
                    str = "lukewarm";
                    break;
                default:
                    throw new UnsupportedOperationException("Unknown startup type");
            }
            r3.A02("app_lifecycle", (int) (r7.A02 / 1000), new C1063556z(r7, str));
            r7.A03.A02(AnonymousClass80H.$const$string(143), (int) (r7.A02 / 1000), new AnonymousClass2II(r7, str));
            r7.A01 = 0;
            r7.A00 = 0;
            r7.A02 = 0;
        }
    }

    private AnonymousClass1LT() {
    }
}
