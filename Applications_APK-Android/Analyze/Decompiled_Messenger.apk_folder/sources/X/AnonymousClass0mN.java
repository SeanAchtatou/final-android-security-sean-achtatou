package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.0mN  reason: invalid class name */
public final class AnonymousClass0mN {
    public final AnonymousClass04b A00 = new AnonymousClass04b();
    public final AnonymousClass04b A01 = new AnonymousClass04b();
    public final AnonymousClass0mL A02;
    public final AnonymousClass0m9 A03;

    public C33971oR A00(ThreadKey threadKey) {
        this.A02.A01();
        if (threadKey == null) {
            C010708t.A0K("ThreadSummariesCache", "ensuring null threadId ThreadLocalState");
        }
        C33971oR r1 = (C33971oR) this.A00.get(threadKey);
        if (r1 != null) {
            return r1;
        }
        C33971oR r12 = new C33971oR();
        this.A00.put(threadKey, r12);
        return r12;
    }

    public ThreadSummary A01(ThreadKey threadKey) {
        this.A02.A01();
        return (ThreadSummary) this.A01.get(threadKey);
    }

    public ThreadSummary A02(ThreadKey threadKey, String str) {
        this.A02.A01();
        ThreadSummary threadSummary = (ThreadSummary) this.A01.remove(threadKey);
        AnonymousClass0m9 r2 = this.A03;
        synchronized (r2) {
            if (r2.A0F() && AnonymousClass0m9.A05(threadKey)) {
                C71183bx A012 = AnonymousClass0m9.A01(r2, threadKey, null, AnonymousClass08S.A0J("removeThreadFromCache-", str), null);
                r2.A01.put(A012, A012);
            }
        }
        return threadSummary;
    }

    public void A03(ThreadSummary threadSummary, String str) {
        this.A02.A01();
        this.A01.put(threadSummary.A0S, threadSummary);
        AnonymousClass0m9 r2 = this.A03;
        synchronized (r2) {
            if (r2.A0F()) {
                ThreadKey threadKey = threadSummary.A0S;
                if (AnonymousClass0m9.A05(threadKey)) {
                    C71183bx A022 = AnonymousClass0m9.A02(r2, threadKey, null, AnonymousClass08S.A0J("updateThreadInCache-", str), ThreadSummary.A01(threadSummary), true);
                    r2.A01.put(A022, A022);
                }
            }
        }
    }

    public AnonymousClass0mN(AnonymousClass0mL r2, AnonymousClass0m9 r3) {
        this.A02 = r2;
        this.A03 = r3;
    }
}
