package X;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.util.Log;

/* renamed from: X.09t  reason: invalid class name and case insensitive filesystem */
public final class C013209t {
    public final Context A00;

    public String A00(String str, String str2) {
        Bundle bundle;
        Object obj;
        try {
            ApplicationInfo applicationInfo = this.A00.getPackageManager().getApplicationInfo(str2, 128);
            if (applicationInfo == null || (bundle = applicationInfo.metaData) == null || (obj = bundle.get(str)) == null) {
                return null;
            }
            return obj.toString();
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(C013209t.class.getName(), "Error reading <meta-data> from AndroidManifest.xml.", e);
            return null;
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof DeadObjectException) {
                Log.e(C013209t.class.getName(), "Error reading <meta-data> from AndroidManifest.xml.", e2);
                return null;
            }
            throw e2;
        }
    }

    public C013209t(Context context) {
        this.A00 = context;
    }
}
