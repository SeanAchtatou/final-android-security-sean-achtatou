package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.105  reason: invalid class name */
public final class AnonymousClass105 extends Enum {
    public static final AnonymousClass105[] A00 = values();
    private static final /* synthetic */ AnonymousClass105[] A01;
    public static final AnonymousClass105 A02;
    public static final AnonymousClass105 A03;
    public static final AnonymousClass105 A04;
    public final int dbValue;

    static {
        AnonymousClass105 r5 = new AnonymousClass105("PENDING", 0, 0);
        A03 = r5;
        AnonymousClass105 r4 = new AnonymousClass105("PENDING_RETRY", 1, 1);
        A04 = r4;
        AnonymousClass105 r3 = new AnonymousClass105("FAILED", 2, 2);
        A02 = r3;
        A01 = new AnonymousClass105[]{r5, r4, r3, new AnonymousClass105("DRAFT", 3, 3)};
    }

    public static AnonymousClass105 valueOf(String str) {
        return (AnonymousClass105) Enum.valueOf(AnonymousClass105.class, str);
    }

    public static AnonymousClass105[] values() {
        return (AnonymousClass105[]) A01.clone();
    }

    private AnonymousClass105(String str, int i, int i2) {
        this.dbValue = i2;
    }
}
