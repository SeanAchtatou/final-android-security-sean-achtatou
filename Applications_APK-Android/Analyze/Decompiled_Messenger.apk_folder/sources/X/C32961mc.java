package X;

import com.facebook.inject.InjectorModule;
import java.util.EnumSet;

@InjectorModule
/* renamed from: X.1mc  reason: invalid class name and case insensitive filesystem */
public final class C32961mc extends AnonymousClass0UV {
    public static final Long A00(AnonymousClass1XY r2) {
        AnonymousClass0X5<C33931oN> r1 = new AnonymousClass0X5<>(r2, AnonymousClass0X6.A1p);
        EnumSet noneOf = EnumSet.noneOf(C33911oL.class);
        for (C33931oN Aga : r1) {
            noneOf.addAll(Aga.Aga());
        }
        return Long.valueOf(AnonymousClass08P.A00(noneOf));
    }
}
