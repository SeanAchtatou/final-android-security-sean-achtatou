package X;

import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.ImmutableSet;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0jF  reason: invalid class name */
public final class AnonymousClass0jF extends AnonymousClass0Wl {
    private static final AnonymousClass0jI A04 = new AnonymousClass0jH();
    private static final Set A05 = ImmutableSet.A05(AnonymousClass0jG.A02, AnonymousClass0jG.A00);
    private static volatile AnonymousClass0jF A06;
    private AnonymousClass0UN A00;
    private boolean A01;
    private final FbSharedPreferences A02;
    private final C04310Tq A03;

    public String getSimpleName() {
        return "LogController";
    }

    public static final AnonymousClass0jF A00(AnonymousClass1XY r5) {
        if (A06 == null) {
            synchronized (AnonymousClass0jF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A06 = new AnonymousClass0jF(applicationInjector, FbSharedPreferencesModule.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public void A01() {
        int i;
        try {
            i = Integer.parseInt(this.A02.B4F(AnonymousClass0jG.A02, ErrorReportingConstants.ANR_DEFAULT_RECOVERY_DELAY_VAL));
        } catch (NumberFormatException unused) {
            i = -1;
        }
        if (i != -1) {
            C010708t.A01(i);
        } else if (((C001300x) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B3a, this.A00)).A01.ordinal() != 2) {
            i = 5;
            if (((Boolean) this.A03.get()).booleanValue()) {
                i = 4;
            }
        } else {
            i = 3;
        }
        C010708t.A01(i);
        if (C010708t.A0X(2) && !this.A01) {
            ((C09730iO) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AIC, this.A00)).A01(A04);
            this.A01 = true;
        } else if (!C010708t.A0X(2) && this.A01) {
            ((C09730iO) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AIC, this.A00)).A02(A04);
            this.A01 = false;
        }
    }

    private AnonymousClass0jF(AnonymousClass1XY r3, FbSharedPreferences fbSharedPreferences) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.Ax2, r3);
        this.A02 = fbSharedPreferences;
        fbSharedPreferences.C0h(A05, new C10000jM(this));
    }

    public void init() {
        int A032 = C000700l.A03(-1171408176);
        A01();
        C000700l.A09(256194341, A032);
    }
}
