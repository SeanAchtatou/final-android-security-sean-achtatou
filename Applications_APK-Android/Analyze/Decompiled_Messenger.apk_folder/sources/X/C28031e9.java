package X;

import android.database.Cursor;
import com.google.common.collect.ImmutableMap;

/* renamed from: X.1e9  reason: invalid class name and case insensitive filesystem */
public final class C28031e9 {
    public static final String[] A17;
    public AnonymousClass0UN A00;
    public ImmutableMap A01;
    public AnonymousClass0V0 A02;
    private AnonymousClass0V0 A03;
    private AnonymousClass0V0 A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final int A0A;
    public final int A0B;
    public final int A0C;
    public final int A0D;
    public final int A0E;
    public final int A0F;
    public final int A0G;
    public final int A0H;
    public final int A0I;
    public final int A0J;
    public final int A0K;
    public final int A0L;
    public final int A0M;
    public final int A0N;
    public final int A0O;
    public final int A0P;
    public final int A0Q;
    public final int A0R;
    public final int A0S;
    public final int A0T;
    public final int A0U;
    public final int A0V;
    public final int A0W;
    public final int A0X;
    public final int A0Y;
    public final int A0Z;
    public final int A0a;
    public final Boolean A0b;
    public final C04310Tq A0c;
    private final int A0d;
    private final int A0e;
    private final int A0f;
    private final int A0g;
    private final int A0h;
    private final int A0i;
    private final int A0j;
    private final int A0k;
    private final int A0l;
    private final int A0m;
    private final int A0n;
    private final int A0o;
    private final int A0p;
    private final int A0q;
    private final int A0r;
    private final int A0s;
    private final int A0t;
    private final int A0u;
    private final int A0v;
    private final int A0w;
    private final int A0x;
    private final int A0y;
    private final int A0z;
    private final int A10;
    private final int A11;
    private final int A12;
    private final int A13;
    private final int A14;
    private final C13700rt A15;
    private final AnonymousClass0jC A16;

    static {
        String[] strArr = new String[61];
        System.arraycopy(new String[]{"thread_key", "legacy_thread_id", "sequence_id", "senders", "snippet", "snippet_sender", "admin_snippet", "timestamp_ms", "last_read_timestamp_ms", "approx_total_message_count", "unread_message_count", "can_reply_to", "cannot_reply_reason", "is_subscribed", "folder", "draft", "last_fetch_time_ms", "mute_until", "initial_fetch_complete", "is_joinable", "requires_approval", "group_description", "is_discoverable", "room_privacy_mode", "room_associated_fb_group_id", "room_associated_fb_group_name", "room_associated_photo_uri"}, 0, strArr, 0, 27);
        System.arraycopy(new String[]{"has_work_multi_company_associated_group", "approval_toggleable", "video_room_mode", "invite_uri", "room_creation_time", "group_thread_category", "are_admins_supported", "group_thread_add_mode", "group_thread_offline_threading_id", "personal_group_invite_link", "optimistic_group_state", "use_existing_group", "thread_associated_object_type", "can_participants_claim_admin", "group_approval_mode", "unopened_montage_directs", "synced_fb_group_id", "synced_fb_group_status", "synced_fb_group_is_work_multi_company_group", "video_chat_link", "is_fuss_red_page", "is_thread_pinned", "theme_id", "theme_fallback_color", "theme_gradient_colors", "theme_accessibility_label", "thread_pin_timestamp"}, 0, strArr, 27, 27);
        System.arraycopy(new String[]{"group_associated_fb_group_visibility", "thread_connectivity_data", "unsendability_status", "group_thread_subtype", "last_message_timestamp_ms", "job_application_time", "has_non_admin_message"}, 0, strArr, 54, 7);
        A17 = strArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a9, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A00(X.C13680rr r13) {
        /*
            r12 = this;
            X.0V0 r0 = r12.A04
            if (r0 == 0) goto L_0x000d
            X.0V0 r0 = r12.A03
            if (r0 == 0) goto L_0x000d
            X.0V0 r0 = r12.A02
            if (r0 == 0) goto L_0x000d
            return
        L_0x000d:
            com.google.common.collect.ArrayListMultimap r0 = new com.google.common.collect.ArrayListMultimap
            r0.<init>()
            r12.A04 = r0
            com.google.common.collect.ArrayListMultimap r0 = new com.google.common.collect.ArrayListMultimap
            r0.<init>()
            r12.A03 = r0
            com.google.common.collect.ArrayListMultimap r0 = new com.google.common.collect.ArrayListMultimap
            r0.<init>()
            r12.A02 = r0
            X.0Tq r0 = r12.A0c
            java.lang.Object r0 = r0.get()
            X.1aN r0 = (X.C25771aN) r0
            android.database.sqlite.SQLiteDatabase r2 = r0.A06()
            android.database.sqlite.SQLiteQueryBuilder r1 = new android.database.sqlite.SQLiteQueryBuilder
            r1.<init>()
            com.google.common.collect.ImmutableList r0 = r13.A00()
            java.lang.String r0 = X.C28721fG.A01(r0)
            r1.setTables(r0)
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8)
            X.1fH r3 = new X.1fH
            r3.<init>(r0)
            java.util.Iterator r5 = r3.iterator()     // Catch:{ all -> 0x00a3 }
        L_0x0051:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x009f
            java.lang.Object r4 = r5.next()     // Catch:{ all -> 0x00a3 }
            X.115 r4 = (X.AnonymousClass115) r4     // Catch:{ all -> 0x00a3 }
            java.lang.Integer r0 = r4.A06     // Catch:{ all -> 0x00a3 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x00a3 }
            switch(r0) {
                case 0: goto L_0x006e;
                case 1: goto L_0x0078;
                case 2: goto L_0x0082;
                default: goto L_0x0066;
            }     // Catch:{ all -> 0x00a3 }
        L_0x0066:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00a3 }
            java.lang.String r0 = "Improper Thread Participant Type"
            r1.<init>(r0)     // Catch:{ all -> 0x00a3 }
            throw r1     // Catch:{ all -> 0x00a3 }
        L_0x006e:
            X.0V0 r2 = r12.A04     // Catch:{ all -> 0x00a3 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A01     // Catch:{ all -> 0x00a3 }
            com.facebook.messaging.model.threads.ThreadParticipant r0 = r4.A02     // Catch:{ all -> 0x00a3 }
            r2.Byx(r1, r0)     // Catch:{ all -> 0x00a3 }
            goto L_0x0051
        L_0x0078:
            X.0V0 r2 = r12.A03     // Catch:{ all -> 0x00a3 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A01     // Catch:{ all -> 0x00a3 }
            com.facebook.messaging.model.threads.ThreadParticipant r0 = r4.A02     // Catch:{ all -> 0x00a3 }
            r2.Byx(r1, r0)     // Catch:{ all -> 0x00a3 }
            goto L_0x0051
        L_0x0082:
            X.0V0 r2 = r12.A02     // Catch:{ all -> 0x00a3 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A01     // Catch:{ all -> 0x00a3 }
            com.facebook.messaging.model.threads.ThreadJoinRequest r6 = new com.facebook.messaging.model.threads.ThreadJoinRequest     // Catch:{ all -> 0x00a3 }
            com.facebook.user.model.UserKey r7 = r4.A04     // Catch:{ all -> 0x00a3 }
            long r8 = r4.A00     // Catch:{ all -> 0x00a3 }
            com.facebook.user.model.UserKey r10 = r4.A03     // Catch:{ all -> 0x00a3 }
            java.lang.Integer r0 = r4.A05     // Catch:{ all -> 0x00a3 }
            if (r0 != 0) goto L_0x0094
            r11 = -1
            goto L_0x0098
        L_0x0094:
            int r11 = r0.intValue()     // Catch:{ all -> 0x00a3 }
        L_0x0098:
            r6.<init>(r7, r8, r10, r11)     // Catch:{ all -> 0x00a3 }
            r2.Byx(r1, r6)     // Catch:{ all -> 0x00a3 }
            goto L_0x0051
        L_0x009f:
            r3.close()
            return
        L_0x00a3:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a5 }
        L_0x00a5:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x00a9 }
        L_0x00a9:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28031e9.A00(X.0rr):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00ab, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00af, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.C17920zh r14, X.C13680rr r15) {
        /*
            r13 = this;
            android.database.Cursor r2 = r15.A00
            r13.A00(r15)
            com.google.common.collect.ImmutableMap r0 = r13.A01
            if (r0 != 0) goto L_0x00b9
            int r1 = X.AnonymousClass1Y3.Auq
            X.0UN r0 = r13.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r1, r0)
            X.1fX r4 = (X.C28891fX) r4
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r4.A00
            r0 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r0 = 282716222654295(0x1012100060757, double:1.39680373135492E-309)
            boolean r0 = r3.Aem(r0)
            if (r0 == 0) goto L_0x008b
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r4.A00
            r0 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r0 = 282716222261074(0x1012100000752, double:1.39680372941215E-309)
            boolean r0 = r3.Aem(r0)
            if (r0 != 0) goto L_0x005b
            boolean r0 = r4.A01()
            if (r0 != 0) goto L_0x005b
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r4.A00
            r0 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r0 = 282716222523222(0x1012100040756, double:1.396803730707333E-309)
            boolean r0 = r3.Aem(r0)
            if (r0 == 0) goto L_0x008b
        L_0x005b:
            r0 = 1
        L_0x005c:
            if (r0 == 0) goto L_0x00b9
            com.google.common.collect.ImmutableMap$Builder r3 = com.google.common.collect.ImmutableMap.builder()
            X.0Tq r0 = r13.A0c
            java.lang.Object r0 = r0.get()
            X.1aN r0 = (X.C25771aN) r0
            android.database.sqlite.SQLiteDatabase r4 = r0.A06()
            java.lang.String r5 = "thread_themes"
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r5 = r4.query(r5, r6, r7, r8, r9, r10, r11)
            X.1fY r6 = new X.1fY
            r4 = 1
            int r1 = X.AnonymousClass1Y3.AeH
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.12W r0 = (X.AnonymousClass12W) r0
            r6.<init>(r5, r0)
            goto L_0x008d
        L_0x008b:
            r0 = 0
            goto L_0x005c
        L_0x008d:
            java.util.Iterator r5 = r6.iterator()     // Catch:{ all -> 0x00a9 }
        L_0x0091:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x00a9 }
            if (r0 == 0) goto L_0x00b0
            java.lang.Object r4 = r5.next()     // Catch:{ all -> 0x00a9 }
            com.facebook.messaging.customthreads.model.ThreadThemeInfo r4 = (com.facebook.messaging.customthreads.model.ThreadThemeInfo) r4     // Catch:{ all -> 0x00a9 }
            long r0 = r4.B5o()     // Catch:{ all -> 0x00a9 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x00a9 }
            r3.put(r0, r4)     // Catch:{ all -> 0x00a9 }
            goto L_0x0091
        L_0x00a9:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ab }
        L_0x00ab:
            r0 = move-exception
            r6.close()     // Catch:{ all -> 0x00af }
        L_0x00af:
            throw r0
        L_0x00b0:
            r6.close()
            com.google.common.collect.ImmutableMap r0 = r3.build()
            r13.A01 = r0
        L_0x00b9:
            int r0 = r13.A0Y
            java.lang.String r0 = r2.getString(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r3 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)
            r14.A0R = r3
            int r0 = r13.A0u
            long r0 = r2.getLong(r0)
            r14.A08 = r0
            X.0V0 r0 = r13.A04
            java.util.Collection r0 = r0.AbK(r3)
            if (r0 == 0) goto L_0x03fb
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
        L_0x00d9:
            r14.A03(r0)
            X.0V0 r0 = r13.A03
            java.util.Collection r0 = r0.AbK(r3)
            if (r0 == 0) goto L_0x03f7
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
        L_0x00e8:
            X.C05520Zg.A02(r0)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
            r14.A0j = r0
            X.0jC r1 = r13.A16
            int r0 = r13.A0t
            java.lang.String r0 = r2.getString(r0)
            com.google.common.collect.ImmutableList r0 = r1.A04(r0)
            r14.A0v = r0
            X.0jC r1 = r13.A16
            int r0 = r13.A0w
            java.lang.String r0 = r2.getString(r0)
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r1.A03(r0)
            r14.A0P = r0
            int r0 = r13.A0v
            java.lang.String r0 = r2.getString(r0)
            r14.A0s = r0
            int r0 = r13.A0d
            java.lang.String r0 = r2.getString(r0)
            r14.A0l = r0
            int r0 = r13.A0y
            long r0 = r2.getLong(r0)
            r14.A0A = r0
            int r0 = r13.A0p
            long r0 = r2.getLong(r0)
            r14.A06 = r0
            int r3 = X.AnonymousClass1Y3.Ae2
            X.0UN r1 = r13.A00
            r0 = 4
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.1fh r0 = (X.C28991fh) r0
            boolean r0 = r0.A01()
            if (r0 == 0) goto L_0x0146
            int r0 = r13.A0o
            long r0 = r2.getLong(r0)
            r14.A05 = r0
        L_0x0146:
            int r0 = r13.A0e
            long r0 = r2.getLong(r0)
            r14.A02 = r0
            int r0 = r13.A10
            long r0 = r2.getLong(r0)
            r14.A0B = r0
            int r0 = r13.A0f
            int r1 = r2.getInt(r0)
            r4 = 1
            r3 = 0
            r0 = 0
            if (r1 == 0) goto L_0x0162
            r0 = 1
        L_0x0162:
            r14.A0x = r0
            int r0 = r13.A0g
            java.lang.String r1 = r2.getString(r0)
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r0 = com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason.A07
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r1, r0)
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r0 = (com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason) r0
            r14.A0G = r0
            int r0 = r13.A0m
            int r1 = r2.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x017e
            r0 = 1
        L_0x017e:
            r14.A15 = r0
            int r0 = r13.A0i
            java.lang.String r0 = r2.getString(r0)
            X.0l8 r0 = X.C10950l8.A00(r0)
            r14.A0N = r0
            X.0rt r1 = r13.A15
            int r0 = r13.A0h
            java.lang.String r0 = r2.getString(r0)
            com.facebook.messaging.model.messages.MessageDraft r0 = r1.A01(r0)
            r14.A0O = r0
            int r0 = r13.A0q
            long r0 = r2.getLong(r0)
            com.facebook.messaging.model.threads.NotificationSetting r0 = com.facebook.messaging.model.threads.NotificationSetting.A00(r0)
            r14.A0X = r0
            int r0 = r13.A0k
            int r1 = r2.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x01b0
            r0 = 1
        L_0x01b0:
            r14.A13 = r0
            android.database.Cursor r7 = r15.A00
            X.103 r8 = new X.103
            r8.<init>()
            int r0 = r13.A0O
            int r1 = r7.getInt(r0)
            r6 = 1
            r0 = 0
            if (r1 == 0) goto L_0x01c4
            r0 = 1
        L_0x01c4:
            r8.A02 = r0
            int r0 = r13.A05
            int r1 = r7.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x01d0
            r0 = 1
        L_0x01d0:
            r8.A01 = r0
            r13.A00(r15)
            android.database.Cursor r1 = r15.A00
            int r0 = r13.A0Y
            java.lang.String r0 = r1.getString(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r1 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)
            X.0V0 r0 = r13.A02
            java.util.Collection r0 = r0.AbK(r1)
            if (r0 == 0) goto L_0x03f3
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
        L_0x01ed:
            com.google.common.base.Preconditions.checkNotNull(r0)
            r8.A00 = r0
            com.facebook.messaging.model.threads.GroupApprovalInfo r5 = new com.facebook.messaging.model.threads.GroupApprovalInfo
            r5.<init>(r8)
            X.1fC r8 = new X.1fC
            r8.<init>()
            int r0 = r13.A0K
            int r1 = r7.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0206
            r0 = 1
        L_0x0206:
            r8.A06 = r0
            int r0 = r13.A0J
            int r1 = r7.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0212
            r0 = 1
        L_0x0212:
            r8.A05 = r0
            int r0 = r13.A0G
            int r0 = r7.getInt(r0)
            X.0zt r0 = X.C17990zt.A00(r0)
            com.google.common.base.Preconditions.checkNotNull(r0)
            r8.A03 = r0
            com.google.common.base.Preconditions.checkNotNull(r5)
            r8.A02 = r5
            int r0 = r13.A0M
            java.lang.String r0 = r7.getString(r0)
            r8.A04 = r0
            int r0 = r13.A0I
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x0244
            int r0 = r13.A0I
            java.lang.String r0 = r7.getString(r0)
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r8.A00 = r0
        L_0x0244:
            int r0 = r13.A0N
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x0258
            int r0 = r13.A0N
            java.lang.String r0 = r7.getString(r0)
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r8.A01 = r0
        L_0x0258:
            X.0zq r5 = new X.0zq
            r5.<init>()
            int r0 = r13.A0Z
            int r1 = r7.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0267
            r0 = 1
        L_0x0267:
            r5.A09 = r0
            com.facebook.messaging.model.threads.JoinableInfo r0 = new com.facebook.messaging.model.threads.JoinableInfo
            r0.<init>(r8)
            com.google.common.base.Preconditions.checkNotNull(r0)
            r5.A04 = r0
            int r0 = r13.A08
            long r0 = r7.getLong(r0)
            r5.A00 = r0
            int r0 = r13.A06
            int r1 = r7.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0285
            r0 = 1
        L_0x0285:
            r5.A0A = r0
            int r0 = r13.A0X
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x0297
            int r0 = r13.A0X
            java.lang.String r0 = r7.getString(r0)
            r5.A08 = r0
        L_0x0297:
            int r0 = r13.A0F
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x02a7
            int r0 = r13.A0F
            java.lang.String r0 = r7.getString(r0)
            r5.A07 = r0
        L_0x02a7:
            int r0 = r13.A0H
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x02b7
            int r0 = r13.A0H
            long r0 = r7.getLong(r0)
            r5.A01 = r0
        L_0x02b7:
            int r0 = r13.A0A
            boolean r1 = r7.isNull(r0)
            r0 = 0
            if (r1 != 0) goto L_0x0340
            int r1 = r13.A0W
            java.lang.String r1 = r7.getString(r1)
            java.lang.Integer r1 = X.AnonymousClass378.A00(r1)
            int r8 = r1.intValue()
            r1 = 0
            if (r8 != r1) goto L_0x0340
            int r0 = r13.A0A
            long r0 = r7.getLong(r0)
            X.3AR r8 = new X.3AR
            r8.<init>(r0)
            int r0 = r13.A0B
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x02ec
            int r0 = r13.A0B
            java.lang.String r0 = r7.getString(r0)
            r8.A03 = r0
        L_0x02ec:
            int r0 = r13.A0E
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x02fc
            int r0 = r13.A0E
            java.lang.String r0 = r7.getString(r0)
            r8.A04 = r0
        L_0x02fc:
            int r0 = r13.A0C
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x0310
            int r0 = r13.A0C
            java.lang.String r0 = r7.getString(r0)
            X.3AS r0 = X.AnonymousClass3AS.A00(r0)
            r8.A02 = r0
        L_0x0310:
            int r0 = r13.A0D
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x0324
            int r0 = r13.A0D
            int r1 = r7.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0322
            r0 = 1
        L_0x0322:
            r8.A05 = r0
        L_0x0324:
            com.facebook.messaging.model.threads.GroupThreadAssociatedFbGroup r10 = new com.facebook.messaging.model.threads.GroupThreadAssociatedFbGroup
            r10.<init>(r8)
            long r0 = r10.A03
            java.lang.Integer r8 = X.AnonymousClass07B.A00
            java.lang.String r9 = X.AnonymousClass378.A01(r8)
            X.379 r8 = new X.379
            r8.<init>(r0, r9)
            com.google.common.base.Preconditions.checkNotNull(r10)
            r8.A00 = r10
            com.facebook.messaging.model.threads.GroupThreadAssociatedObject r0 = new com.facebook.messaging.model.threads.GroupThreadAssociatedObject
            r0.<init>(r8)
        L_0x0340:
            r5.A02 = r0
            int r0 = r13.A07
            int r0 = r7.getInt(r0)
            if (r0 != 0) goto L_0x034b
            r6 = 0
        L_0x034b:
            r5.A0B = r6
            int r0 = r13.A09
            int r0 = r7.getInt(r0)
            if (r0 != 0) goto L_0x03ef
            X.104 r0 = X.AnonymousClass104.NONE
        L_0x0357:
            r5.A03 = r0
            int r0 = r13.A0P
            boolean r0 = r7.isNull(r0)
            if (r0 == 0) goto L_0x03b0
            int r0 = r13.A0R
            boolean r0 = r7.isNull(r0)
            if (r0 == 0) goto L_0x03b0
            int r0 = r13.A0Q
            boolean r0 = r7.isNull(r0)
            if (r0 == 0) goto L_0x03b0
            r0 = 0
        L_0x0372:
            r5.A05 = r0
            java.lang.Boolean r0 = r13.A0b
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x041a
            int r1 = r13.A0a
            r8 = 0
            r0 = -1
            if (r1 == r0) goto L_0x0418
            int r0 = r13.A0P
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x0418
            int r0 = r13.A0P
            long r11 = r7.getLong(r0)
            r9 = 0
            int r0 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r0 == 0) goto L_0x0418
            int r0 = r13.A0a
            java.lang.String r7 = r7.getString(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r7)
            if (r0 != 0) goto L_0x0418
            r6 = 3
            int r1 = X.AnonymousClass1Y3.BNX
            X.0UN r0 = r13.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.2Oq r6 = (X.C46072Oq) r6
            if (r7 == 0) goto L_0x0418
            goto L_0x03ff
        L_0x03b0:
            X.0xt r6 = new X.0xt
            r6.<init>()
            int r0 = r13.A0P
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x03c5
            int r0 = r13.A0P
            long r0 = r7.getLong(r0)
            r6.A00 = r0
        L_0x03c5:
            int r0 = r13.A0R
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x03d5
            int r0 = r13.A0R
            java.lang.String r0 = r7.getString(r0)
            r6.A01 = r0
        L_0x03d5:
            int r0 = r13.A0Q
            boolean r0 = r7.isNull(r0)
            if (r0 != 0) goto L_0x03e9
            int r0 = r13.A0Q
            int r1 = r7.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x03e7
            r0 = 1
        L_0x03e7:
            r6.A02 = r0
        L_0x03e9:
            com.facebook.messaging.model.threads.SyncedGroupData r0 = new com.facebook.messaging.model.threads.SyncedGroupData
            r0.<init>(r6)
            goto L_0x0372
        L_0x03ef:
            X.104 r0 = X.AnonymousClass104.NEEDS_ADMIN_APPROVAL
            goto L_0x0357
        L_0x03f3:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            goto L_0x01ed
        L_0x03f7:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            goto L_0x00e8
        L_0x03fb:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            goto L_0x00d9
        L_0x03ff:
            X.0jE r1 = r6.A01     // Catch:{ IOException -> 0x040d }
            X.37B r0 = new X.37B     // Catch:{ IOException -> 0x040d }
            r0.<init>(r6)     // Catch:{ IOException -> 0x040d }
            java.lang.Object r0 = r1.readValue(r7, r0)     // Catch:{ IOException -> 0x040d }
            com.facebook.workshared.syncedgroups.model.WorkSyncGroupModelData r0 = (com.facebook.workshared.syncedgroups.model.WorkSyncGroupModelData) r0     // Catch:{ IOException -> 0x040d }
            goto L_0x0417
        L_0x040d:
            X.09P r6 = r6.A00
            java.lang.String r1 = "WorkSyncGroupModelDataParser"
            java.lang.String r0 = "Failed to deserialize work sync group model data"
            r6.CGS(r1, r0)
            r0 = r8
        L_0x0417:
            r8 = r0
        L_0x0418:
            r5.A06 = r8
        L_0x041a:
            com.facebook.messaging.model.threads.GroupThreadData r0 = new com.facebook.messaging.model.threads.GroupThreadData
            r0.<init>(r5)
            r14.A01(r0)
            int r0 = r13.A13
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x0436
            int r0 = r13.A13
            java.lang.String r0 = r2.getString(r0)
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r14.A0C = r0
        L_0x0436:
            int r0 = r13.A0r
            int r8 = r2.getInt(r0)
            X.105[] r7 = X.AnonymousClass105.A00
            int r6 = r7.length
            r5 = 0
        L_0x0440:
            if (r5 >= r6) goto L_0x044b
            r1 = r7[r5]
            int r0 = r1.dbValue
            if (r0 == r8) goto L_0x044d
            int r5 = r5 + 1
            goto L_0x0440
        L_0x044b:
            X.105 r1 = X.AnonymousClass105.A03
        L_0x044d:
            r14.A0Y = r1
            int r0 = r13.A12
            int r1 = r2.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0459
            r0 = 1
        L_0x0459:
            r14.A17 = r0
            int r1 = X.AnonymousClass1Y3.B0B
            X.0UN r0 = r13.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0xu r6 = (X.C16870xu) r6
            int r0 = r13.A0z
            java.lang.String r1 = r2.getString(r0)
            if (r1 == 0) goto L_0x04e5
            java.lang.String r0 = "[]"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x04e5
            X.0jD r0 = r6.A00
            com.fasterxml.jackson.databind.JsonNode r0 = r0.A02(r1)
            com.google.common.collect.ImmutableList$Builder r5 = com.google.common.collect.ImmutableList.builder()
            java.util.Iterator r9 = r0.iterator()
        L_0x0483:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x04e8
            java.lang.Object r1 = r9.next()
            com.fasterxml.jackson.databind.JsonNode r1 = (com.fasterxml.jackson.databind.JsonNode) r1
            X.1TG r7 = com.facebook.messaging.model.messages.Message.A00()
            java.lang.String r0 = "msg_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.path(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r7.A06(r0)
            X.2Qw r8 = com.facebook.ui.media.attachments.model.MediaResource.A00()
            android.net.Uri r0 = android.net.Uri.EMPTY
            r8.A0D = r0
            java.lang.String r0 = "media_type"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.path(r0)
            java.lang.String r1 = com.facebook.common.util.JSONUtil.A0N(r0)
            boolean r0 = X.C06850cB.A0B(r1)
            if (r0 == 0) goto L_0x04cf
            X.28p r0 = X.C421828p.PHOTO
        L_0x04ba:
            r8.A0L = r0
            com.facebook.ui.media.attachments.model.MediaResource r0 = r8.A00()
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r0)
            r7.A09(r0)
            com.facebook.messaging.model.messages.Message r0 = r7.A00()
            r5.add(r0)
            goto L_0x0483
        L_0x04cf:
            X.28p r0 = X.C421828p.A00(r1)     // Catch:{ IllegalArgumentException -> 0x04d4 }
            goto L_0x04ba
        L_0x04d4:
            r3 = move-exception
            java.lang.Class r0 = r6.getClass()
            java.lang.String r1 = r0.getSimpleName()
            java.lang.String r0 = "Unexpected media type."
            X.C010708t.A0L(r1, r0, r3)
            X.28p r0 = X.C421828p.PHOTO
            goto L_0x04ba
        L_0x04e5:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            goto L_0x04ec
        L_0x04e8:
            com.google.common.collect.ImmutableList r0 = r5.build()
        L_0x04ec:
            r14.A0w = r0
            int r0 = r13.A0l
            int r1 = r2.getInt(r0)
            r0 = 0
            if (r1 == 0) goto L_0x04f8
            r0 = 1
        L_0x04f8:
            r14.A12 = r0
            int r0 = r13.A0n
            int r0 = r2.getInt(r0)
            if (r0 != 0) goto L_0x0503
            r4 = 0
        L_0x0503:
            r14.A16 = r4
            int r0 = r13.A0V
            long r3 = r2.getLong(r0)
            com.google.common.collect.ImmutableMap r1 = r13.A01
            if (r1 == 0) goto L_0x057e
            java.lang.Long r0 = java.lang.Long.valueOf(r3)
            java.lang.Object r1 = r1.get(r0)
            com.facebook.messaging.customthreads.model.ThreadThemeInfo r1 = (com.facebook.messaging.customthreads.model.ThreadThemeInfo) r1
        L_0x0519:
            if (r1 == 0) goto L_0x0577
            X.12U r0 = new X.12U
            r0.<init>(r1)
        L_0x0520:
            int r1 = r13.A0T
            int r1 = r2.getInt(r1)
            r0.A00 = r1
            r4 = 1
            int r3 = X.AnonymousClass1Y3.AeH
            X.0UN r1 = r13.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r1)
            X.12W r3 = (X.AnonymousClass12W) r3
            int r1 = r13.A0U
            java.lang.String r1 = r2.getString(r1)
            com.google.common.collect.ImmutableList r3 = r3.A01(r1)
            r0.A06 = r3
            java.lang.String r1 = "gradientColors"
            X.C28931fb.A06(r3, r1)
            int r1 = r13.A0S
            java.lang.String r1 = r2.getString(r1)
            java.lang.String r1 = com.google.common.base.Strings.nullToEmpty(r1)
            r0.A01(r1)
            com.facebook.messaging.customthreads.model.ThreadThemeInfo r0 = r0.A00()
            X.C05520Zg.A02(r0)
            r14.A0L = r0
            int r0 = r13.A0s
            long r0 = r2.getLong(r0)
            r14.A09 = r0
            r3 = 2
            int r1 = X.AnonymousClass1Y3.A79
            X.0UN r0 = r13.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.12Y r6 = (X.AnonymousClass12Y) r6
            int r0 = r13.A0x
            java.lang.String r3 = r2.getString(r0)
            r5 = 0
            if (r3 == 0) goto L_0x059a
            goto L_0x0580
        L_0x0577:
            X.12U r0 = com.facebook.messaging.customthreads.model.ThreadThemeInfo.A00()
            r0.A01 = r3
            goto L_0x0520
        L_0x057e:
            r1 = 0
            goto L_0x0519
        L_0x0580:
            X.0jE r1 = r6.A01     // Catch:{ IOException -> 0x058e }
            X.12Z r0 = new X.12Z     // Catch:{ IOException -> 0x058e }
            r0.<init>(r6)     // Catch:{ IOException -> 0x058e }
            java.lang.Object r0 = r1.readValue(r3, r0)     // Catch:{ IOException -> 0x058e }
            com.facebook.messaging.model.threads.ThreadConnectivityData r0 = (com.facebook.messaging.model.threads.ThreadConnectivityData) r0     // Catch:{ IOException -> 0x058e }
            goto L_0x0599
        L_0x058e:
            r4 = move-exception
            X.09P r3 = r6.A00
            java.lang.String r1 = "DbThreadConnectivityDataSerialization"
            java.lang.String r0 = "Error trying to de-serialize ThreadConnectivityData"
            r3.softReport(r1, r0, r4)
            r0 = r5
        L_0x0599:
            r5 = r0
        L_0x059a:
            r14.A0c = r5
            int r0 = r13.A14
            long r0 = r2.getLong(r0)
            r14.A03 = r0
            int r0 = r13.A11
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x05b4
            int r0 = r13.A11
            java.lang.String r0 = r2.getString(r0)
            r14.A0t = r0
        L_0x05b4:
            int r0 = r13.A0j
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x05cc
            int r0 = r13.A0j
            java.lang.String r1 = r2.getString(r0)
            com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType r0 = com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType.A05
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r1, r0)
            com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType r0 = (com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType) r0
            r14.A0H = r0
        L_0x05cc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28031e9.A01(X.0zh, X.0rr):void");
    }

    public C28031e9(AnonymousClass1XY r3, Cursor cursor, boolean z) {
        String str;
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A16 = new AnonymousClass0jC(r3);
        this.A15 = new C13700rt(r3);
        this.A0c = C25771aN.A02(r3);
        this.A0b = AnonymousClass0UU.A08(r3);
        this.A0Y = cursor.getColumnIndexOrThrow("thread_key");
        this.A0u = cursor.getColumnIndexOrThrow("sequence_id");
        this.A0t = cursor.getColumnIndexOrThrow("senders");
        this.A0v = cursor.getColumnIndexOrThrow("snippet");
        this.A0w = cursor.getColumnIndexOrThrow("snippet_sender");
        this.A0d = cursor.getColumnIndexOrThrow("admin_snippet");
        if (z) {
            str = "timestamp_in_folder_ms";
        } else {
            str = "timestamp_ms";
        }
        this.A0y = cursor.getColumnIndexOrThrow(str);
        this.A0p = cursor.getColumnIndexOrThrow("last_read_timestamp_ms");
        this.A0e = cursor.getColumnIndexOrThrow("approx_total_message_count");
        this.A10 = cursor.getColumnIndexOrThrow("unread_message_count");
        this.A0f = cursor.getColumnIndexOrThrow("can_reply_to");
        this.A0g = cursor.getColumnIndexOrThrow("cannot_reply_reason");
        this.A0m = cursor.getColumnIndexOrThrow("is_subscribed");
        this.A0i = cursor.getColumnIndexOrThrow("folder");
        this.A0h = cursor.getColumnIndexOrThrow("draft");
        this.A0q = cursor.getColumnIndexOrThrow("mute_until");
        this.A0k = cursor.getColumnIndexOrThrow("initial_fetch_complete");
        this.A0L = cursor.getColumnIndexOrThrow("last_fetch_time_ms");
        this.A0F = cursor.getColumnIndexOrThrow("group_description");
        this.A0J = cursor.getColumnIndexOrThrow("is_discoverable");
        this.A0I = cursor.getColumnIndexOrThrow("invite_uri");
        this.A0K = cursor.getColumnIndexOrThrow("is_joinable");
        this.A0O = cursor.getColumnIndexOrThrow("requires_approval");
        this.A05 = cursor.getColumnIndexOrThrow("approval_toggleable");
        this.A0Z = cursor.getColumnIndexOrThrow("video_room_mode");
        this.A0G = cursor.getColumnIndexOrThrow("room_privacy_mode");
        this.A0W = cursor.getColumnIndexOrThrow("thread_associated_object_type");
        this.A0A = cursor.getColumnIndexOrThrow("room_associated_fb_group_id");
        this.A0B = cursor.getColumnIndexOrThrow("room_associated_fb_group_name");
        this.A0E = cursor.getColumnIndexOrThrow("room_associated_photo_uri");
        this.A0D = cursor.getColumnIndexOrThrow("has_work_multi_company_associated_group");
        this.A08 = cursor.getColumnIndexOrThrow("room_creation_time");
        this.A0X = cursor.getColumnIndexOrThrow("group_thread_category");
        this.A06 = cursor.getColumnIndexOrThrow("are_admins_supported");
        this.A0M = cursor.getColumnIndexOrThrow("group_thread_add_mode");
        this.A0H = cursor.getColumnIndexOrThrow("group_thread_offline_threading_id");
        this.A0N = cursor.getColumnIndexOrThrow("personal_group_invite_link");
        this.A0r = cursor.getColumnIndexOrThrow("optimistic_group_state");
        this.A12 = cursor.getColumnIndexOrThrow("use_existing_group");
        this.A07 = cursor.getColumnIndexOrThrow("can_participants_claim_admin");
        this.A09 = cursor.getColumnIndexOrThrow("group_approval_mode");
        this.A0z = cursor.getColumnIndexOrThrow("unopened_montage_directs");
        this.A0P = cursor.getColumnIndexOrThrow("synced_fb_group_id");
        this.A0R = cursor.getColumnIndexOrThrow("synced_fb_group_status");
        this.A0Q = cursor.getColumnIndexOrThrow("synced_fb_group_is_work_multi_company_group");
        this.A13 = cursor.getColumnIndexOrThrow("video_chat_link");
        this.A0l = cursor.getColumnIndexOrThrow("is_fuss_red_page");
        this.A0n = cursor.getColumnIndexOrThrow("is_thread_pinned");
        this.A0V = cursor.getColumnIndexOrThrow("theme_id");
        this.A0T = cursor.getColumnIndexOrThrow("theme_fallback_color");
        this.A0U = cursor.getColumnIndexOrThrow("theme_gradient_colors");
        this.A0S = cursor.getColumnIndexOrThrow("theme_accessibility_label");
        this.A0s = cursor.getColumnIndexOrThrow("thread_pin_timestamp");
        this.A0C = cursor.getColumnIndexOrThrow("group_associated_fb_group_visibility");
        this.A0x = cursor.getColumnIndexOrThrow("thread_connectivity_data");
        this.A11 = cursor.getColumnIndexOrThrow("unsendability_status");
        this.A0j = cursor.getColumnIndexOrThrow("group_thread_subtype");
        this.A0o = cursor.getColumnIndexOrThrow("last_message_timestamp_ms");
        this.A14 = cursor.getColumnIndexOrThrow("job_application_time");
        if (this.A0b.booleanValue()) {
            this.A0a = cursor.getColumnIndexOrThrow("work_sync_group_data");
        } else {
            this.A0a = -1;
        }
    }
}
