package X;

import com.facebook.profilo.logger.Logger;

/* renamed from: X.05T  reason: invalid class name */
public final class AnonymousClass05T implements AnonymousClass05U {
    public final /* synthetic */ AnonymousClass04q A00;

    public AnonymousClass05T(AnonymousClass04q r1) {
        this.A00 = r1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public void BZp(long j) {
        long j2 = j;
        if (j > 0 && this.A00.A01.get() && this.A00.A0A != -1) {
            if (AnonymousClass0OU.A04 == null) {
                AnonymousClass0OU.A04 = new AnonymousClass0OU();
            }
            AnonymousClass0OU.A04.A00(this.A00.A00);
            long max = Math.max(1L, j2);
            long round = Math.round((((double) max) * 1.0d) / ((double) this.A00.A0A)) - 1;
            AnonymousClass04q r5 = this.A00;
            r5.A08 = (int) (((long) r5.A08) + round);
            double d = (double) round;
            if (d >= 4.0d) {
                this.A00.A07 += d / 4.0d;
            }
            this.A00.A09 += max;
            if (round >= 1) {
                Logger.writeStandardEntry(AnonymousClass04q.A0D, 6, 44, 0, 0, 8126498, 0, round);
                AnonymousClass04q.A04("ScrollPerf.FrameDropped");
                if (d >= 4.0d) {
                    AnonymousClass04q.A04("ScrollPerf.LargeFrameDropped");
                }
            }
            AnonymousClass04q.A04("ScrollPerf.FrameStarted");
        }
    }
}
