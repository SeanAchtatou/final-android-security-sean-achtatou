package X;

/* renamed from: X.0hD  reason: invalid class name and case insensitive filesystem */
public final class C09380hD {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;

    static {
        AnonymousClass1Y7 r1 = C04350Ue.A07;
        A00 = (AnonymousClass1Y7) r1.A09("device_id");
        A01 = (AnonymousClass1Y7) r1.A09("device_id_generate_timestamp");
        A02 = (AnonymousClass1Y7) r1.A09("phone_id_last_sync_timestamp");
        A03 = (AnonymousClass1Y7) r1.A09("sfdid");
        A04 = (AnonymousClass1Y7) r1.A09("sfdid_generated_timestamp");
        A07 = (AnonymousClass1Y7) r1.A09("sfdid_last_sync_timestamp");
        A06 = (AnonymousClass1Y7) r1.A09("sfdid_generator_pkg");
        A05 = (AnonymousClass1Y7) r1.A09("sfdid_generator_action");
    }
}
