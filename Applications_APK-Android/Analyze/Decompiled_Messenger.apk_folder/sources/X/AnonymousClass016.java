package X;

import android.os.Process;
import android.util.Log;
import com.facebook.common.dextricks.DexStore;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.016  reason: invalid class name */
public final class AnonymousClass016 implements Thread.UncaughtExceptionHandler {
    public static AnonymousClass016 A04;
    public static Runnable A05;
    public final boolean A00;
    private final Object A01 = new Object();
    private final Thread.UncaughtExceptionHandler A02;
    public volatile List A03 = Collections.unmodifiableList(new ArrayList());
    private byte[] mOomReservation = null;

    public static synchronized AnonymousClass016 A00() {
        AnonymousClass016 r0;
        synchronized (AnonymousClass016.class) {
            r0 = A04;
            if (r0 == null) {
                synchronized (AnonymousClass016.class) {
                    if (A04 == null) {
                        AnonymousClass016 r02 = new AnonymousClass016(Thread.getDefaultUncaughtExceptionHandler(), true);
                        A04 = r02;
                        Thread.setDefaultUncaughtExceptionHandler(r02);
                        r0 = A04;
                    } else {
                        throw new IllegalStateException("Already initialized!");
                    }
                }
            }
        }
        return r0;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Failed compute block dominance frontier
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.computeDominanceFrontier(BlockProcessor.java:301)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:78)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.visit(BlockProcessor.java:46)
        Caused by: java.lang.IndexOutOfBoundsException: Index 12 out of bounds for length 12
        	at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        	at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        	at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        	at java.base/java.util.Objects.checkIndex(Objects.java:372)
        	at java.base/java.util.ArrayList.get(ArrayList.java:458)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.computeBlockDF(BlockProcessor.java:326)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.computeDominanceFrontier(BlockProcessor.java:299)
        	... 2 more
        */
    private static void A01() {
        /*
            java.lang.Runnable r0 = X.AnonymousClass016.A05     // Catch:{ all -> 0x000f }
            if (r0 == 0) goto L_0x0007
            r0.run()     // Catch:{ all -> 0x000f }
        L_0x0007:
            int r0 = android.os.Process.myPid()     // Catch:{ all -> 0x000f }
            android.os.Process.killProcess(r0)     // Catch:{ all -> 0x000f }
            goto L_0x0017
        L_0x000f:
            r2 = move-exception
            java.lang.String r1 = "ExceptionHandlerManager"
            java.lang.String r0 = "Error during exception handling"
            android.util.Log.e(r1, r0, r2)
        L_0x0017:
            r0 = 10
            java.lang.System.exit(r0)     // Catch:{ all -> 0x001d }
            goto L_0x0025
        L_0x001d:
            r2 = move-exception
            java.lang.String r1 = "ExceptionHandlerManager"
            java.lang.String r0 = "Error during exception handling"
            android.util.Log.e(r1, r0, r2)
        L_0x0025:
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass016.A01():void");
    }

    public static synchronized void A02(AnonymousClass015 r4, int i) {
        synchronized (AnonymousClass016.class) {
            AnonymousClass016 A002 = A00();
            synchronized (A002) {
                ArrayList arrayList = new ArrayList(A002.A03);
                AnonymousClass017 r0 = new AnonymousClass017();
                r0.A01 = r4;
                r0.A00 = i;
                arrayList.add(r0);
                if (A002.A00) {
                    Collections.sort(arrayList);
                }
                A002.A03 = Collections.unmodifiableList(arrayList);
            }
        }
    }

    public static synchronized void A03(Thread thread, Throwable th) {
        AnonymousClass016 r0;
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler;
        synchronized (AnonymousClass016.class) {
            synchronized (AnonymousClass016.class) {
                r0 = A04;
            }
            if (r0 != null) {
                uncaughtExceptionHandler = r0.A02;
            } else {
                uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            }
            uncaughtExceptionHandler.uncaughtException(thread, th);
            A01();
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        synchronized (this.A01) {
            try {
                Process.setThreadPriority(-10);
            } catch (Exception unused) {
            }
            this.mOomReservation = null;
            List list = this.A03;
            try {
                for (int size = list.size() - 1; size >= 0; size--) {
                    ((AnonymousClass017) list.get(size)).A01.handleUncaughtException(thread, th, null);
                }
                try {
                    if (th instanceof AnonymousClass0MZ) {
                        Log.w("ExceptionHandlerManager", AnonymousClass08S.A0J("Exit: ", th.getMessage()));
                    } else {
                        this.A02.uncaughtException(thread, th);
                    }
                } catch (Throwable th2) {
                    A01();
                    throw th2;
                }
                A01();
            } catch (Throwable th3) {
                try {
                    if (th instanceof AnonymousClass0MZ) {
                        Log.w("ExceptionHandlerManager", AnonymousClass08S.A0J("Exit: ", th.getMessage()));
                    } else {
                        this.A02.uncaughtException(thread, th);
                    }
                } catch (Throwable th4) {
                    Log.e("ExceptionHandlerManager", "Error during exception handling", th4);
                }
                A01();
                throw th3;
            }
        }
    }

    private AnonymousClass016(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, boolean z) {
        this.A00 = z;
        this.A02 = uncaughtExceptionHandler;
        this.mOomReservation = new byte[DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED];
    }
}
