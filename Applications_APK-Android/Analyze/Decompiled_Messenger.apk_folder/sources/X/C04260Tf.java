package X;

import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0Tf  reason: invalid class name and case insensitive filesystem */
public final class C04260Tf {
    public final int A00;
    public final long A01;
    public final C04260Tf A02;
    public final AtomicInteger A03;

    public C04260Tf(long j, int i, C04260Tf r5, int i2) {
        AtomicInteger atomicInteger;
        this.A02 = r5;
        this.A01 = j;
        this.A00 = i;
        if (i2 == 0) {
            atomicInteger = null;
        } else {
            atomicInteger = new AtomicInteger(i2);
        }
        this.A03 = atomicInteger;
    }
}
