package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.facebook.rti.push.service.FbnsService;

/* renamed from: X.0G7  reason: invalid class name */
public final class AnonymousClass0G7 {
    public final Context A00;
    public final AnonymousClass0G6 A01;

    public void A00() {
        String A07 = this.A01.A00.A00.A07();
        if (A07 != null) {
            Context context = this.A00;
            String A03 = FbnsService.A03(A07);
            if (A07 == null) {
                A07 = context.getPackageName();
            }
            if (A03 == null) {
                A03 = FbnsService.A03(A07);
            }
            Intent intent = new Intent("com.facebook.rti.fbns.intent.UNREGISTER");
            intent.setComponent(new ComponentName(A07, A03));
            intent.putExtra("pkg_name", context.getPackageName());
            new C012309k(context, null).A02(intent);
        }
    }

    public void A01(String str) {
        String A07 = this.A01.A00.A00.A07();
        if (A07 != null) {
            Context context = this.A00;
            if (A07 == null) {
                A07 = context.getPackageName();
            }
            AnonymousClass0H3.A02(context, str, A07, FbnsService.A03(A07), false, new C012309k(context, null));
        }
    }

    public AnonymousClass0G7(Context context, AnonymousClass0G6 r2) {
        this.A00 = context;
        this.A01 = r2;
    }
}
