package X;

/* renamed from: X.1rU  reason: invalid class name and case insensitive filesystem */
public final class C35601rU {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ AnonymousClass1Ri A03;

    public C35601rU(AnonymousClass1Ri r1, int i, int i2, int i3) {
        this.A03 = r1;
        this.A01 = i;
        this.A00 = i2;
        this.A02 = i3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0031, code lost:
        if (r8 >= r6) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        if (r4.A03().BGx() == false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        if (r8 > r5) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0043, code lost:
        if (r4.A0A(r3, r2) != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
        r4.A06(r1.A0U, r3, r2, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
        if (X.C191216w.A00() == false) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0053, code lost:
        X.AnonymousClass1Ri.A0C(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0057, code lost:
        X.AnonymousClass00S.A04(r1.A0R, new X.AnonymousClass25B(r4), 329298550);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00(int r8) {
        /*
            r7 = this;
            X.1Ri r1 = r7.A03
            int r6 = r7.A01
            int r5 = r7.A00
            int r2 = r7.A02
            monitor-enter(r1)
            java.util.List r0 = r1.A0h     // Catch:{ all -> 0x0065 }
            int r0 = r0.size()     // Catch:{ all -> 0x0065 }
            if (r2 == r0) goto L_0x0014
            monitor-exit(r1)     // Catch:{ all -> 0x0065 }
            r0 = 0
            return r0
        L_0x0014:
            java.util.List r0 = r1.A0h     // Catch:{ all -> 0x0065 }
            java.lang.Object r4 = r0.get(r8)     // Catch:{ all -> 0x0065 }
            X.1IK r4 = (X.AnonymousClass1IK) r4     // Catch:{ all -> 0x0065 }
            X.1Ih r0 = r4.A03()     // Catch:{ all -> 0x0065 }
            boolean r0 = r0.C2K()     // Catch:{ all -> 0x0065 }
            if (r0 == 0) goto L_0x0028
            monitor-exit(r1)     // Catch:{ all -> 0x0065 }
            goto L_0x004b
        L_0x0028:
            int r3 = X.AnonymousClass1Ri.A01(r1, r4)     // Catch:{ all -> 0x0065 }
            int r2 = X.AnonymousClass1Ri.A00(r1, r4)     // Catch:{ all -> 0x0065 }
            monitor-exit(r1)     // Catch:{ all -> 0x0065 }
            if (r8 >= r6) goto L_0x003d
            X.1Ih r0 = r4.A03()
            boolean r0 = r0.BGx()
            if (r0 == 0) goto L_0x004d
        L_0x003d:
            if (r8 > r5) goto L_0x004d
            boolean r0 = r4.A0A(r3, r2)
            if (r0 != 0) goto L_0x004b
            X.0p4 r1 = r1.A0U
            r0 = 0
            r4.A06(r1, r3, r2, r0)
        L_0x004b:
            r0 = 1
            return r0
        L_0x004d:
            boolean r0 = X.C191216w.A00()
            if (r0 == 0) goto L_0x0057
            X.AnonymousClass1Ri.A0C(r4)
            goto L_0x004b
        L_0x0057:
            android.os.Handler r2 = r1.A0R
            X.25B r1 = new X.25B
            r1.<init>(r4)
            r0 = 329298550(0x13a0b276, float:4.0565654E-27)
            X.AnonymousClass00S.A04(r2, r1, r0)
            goto L_0x004b
        L_0x0065:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0065 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35601rU.A00(int):boolean");
    }
}
