package X;

import com.fasterxml.jackson.databind.JsonSerializer;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.IdentityHashMap;

/* renamed from: X.0mT  reason: invalid class name and case insensitive filesystem */
public abstract class C11250mT extends C11260mU implements Serializable {
    private static final long serialVersionUID = 1;
    public transient ArrayList _objectIdGenerators;
    public transient IdentityHashMap _seenObjectIds;

    public abstract C11250mT createInstance(C10450k8 r1, C26861cG r2);

    public JsonSerializer serializerInstance(C10080jW r5, Object obj) {
        JsonSerializer jsonSerializer;
        JsonSerializer jsonSerializer2 = null;
        if (obj != null) {
            if (obj instanceof JsonSerializer) {
                jsonSerializer = (JsonSerializer) obj;
            } else if (obj instanceof Class) {
                Class<C422228t> cls = (Class) obj;
                if (!(cls == JsonSerializer.None.class || cls == C422228t.class)) {
                    if (JsonSerializer.class.isAssignableFrom(cls)) {
                        C10450k8 r1 = this._config;
                        CXG cxg = r1._base._handlerInstantiator;
                        if (cxg != null) {
                            jsonSerializer2 = cxg.serializerInstance(r1, r5, cls);
                        }
                        if (jsonSerializer2 == null) {
                            jsonSerializer = (JsonSerializer) C29081fq.createInstance(cls, r1.canOverrideAccessModifiers());
                        } else {
                            jsonSerializer = jsonSerializer2;
                        }
                    } else {
                        throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned Class ", cls.getName(), "; expected Class<JsonSerializer>"));
                    }
                }
            } else {
                throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned serializer definition of type ", obj.getClass().getName(), "; expected type JsonSerializer or Class<JsonSerializer> instead"));
            }
            if (jsonSerializer instanceof AnonymousClass2HD) {
                ((AnonymousClass2HD) jsonSerializer).resolve(this);
            }
            return jsonSerializer;
        }
        return null;
    }

    public CZm findObjectId(Object obj, C25128CaN caN) {
        IdentityHashMap identityHashMap = this._seenObjectIds;
        if (identityHashMap == null) {
            this._seenObjectIds = new IdentityHashMap();
        } else {
            CZm cZm = (CZm) identityHashMap.get(obj);
            if (cZm != null) {
                return cZm;
            }
        }
        C25128CaN caN2 = null;
        ArrayList arrayList = this._objectIdGenerators;
        if (arrayList != null) {
            int i = 0;
            int size = arrayList.size();
            while (true) {
                if (i >= size) {
                    break;
                }
                C25128CaN caN3 = (C25128CaN) this._objectIdGenerators.get(i);
                if (caN3.canUseFor(caN)) {
                    caN2 = caN3;
                    break;
                }
                i++;
            }
        } else {
            this._objectIdGenerators = new ArrayList(8);
        }
        if (caN2 == null) {
            caN.newForSerialization(this);
            caN2 = caN;
            this._objectIdGenerators.add(caN);
        }
        CZm cZm2 = new CZm(caN2);
        this._seenObjectIds.put(obj, cZm2);
        return cZm2;
    }

    public C11250mT() {
    }

    public C11250mT(C11260mU r1, C10450k8 r2, C26861cG r3) {
        super(r1, r2, r3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0mU.findTypedValueSerializer(java.lang.Class, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer
     arg types: [java.lang.Class<?>, int, ?[OBJECT, ARRAY]]
     candidates:
      X.0mU.findTypedValueSerializer(X.0jR, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer
      X.0mU.findTypedValueSerializer(java.lang.Class, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer */
    public void serializeValue(C11710np r7, Object obj) {
        JsonSerializer jsonSerializer;
        boolean z = false;
        if (obj == null) {
            jsonSerializer = this._nullValueSerializer;
        } else {
            Class<?> cls = obj.getClass();
            jsonSerializer = findTypedValueSerializer((Class) cls, true, (CX9) null);
            C10450k8 r2 = this._config;
            String str = r2._rootName;
            if (str == null) {
                z = r2.isEnabled(C10480kE.WRAP_ROOT_VALUE);
                if (z) {
                    r7.writeStartObject();
                    r7.writeFieldName(this._rootNames.findRootName(cls, this._config));
                }
            } else if (str.length() != 0) {
                r7.writeStartObject();
                r7.writeFieldName(str);
                z = true;
            }
        }
        try {
            jsonSerializer.serialize(obj, r7, this);
            if (z) {
                r7.writeEndObject();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            String message = e2.getMessage();
            if (message == null) {
                message = AnonymousClass08S.A0P("[no message for ", e2.getClass().getName(), "]");
            }
            throw new C37701w6(message, e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0mU.findTypedValueSerializer(X.0jR, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer
     arg types: [X.0jR, int, ?[OBJECT, ARRAY]]
     candidates:
      X.0mU.findTypedValueSerializer(java.lang.Class, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer
      X.0mU.findTypedValueSerializer(X.0jR, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer */
    public void serializeValue(C11710np r5, Object obj, C10030jR r7, JsonSerializer jsonSerializer) {
        boolean z;
        if (obj == null) {
            jsonSerializer = this._nullValueSerializer;
            z = false;
        } else {
            if (r7 != null) {
                Class cls = r7._class;
                Class<?> cls2 = obj.getClass();
                if (!cls.isAssignableFrom(cls2) && (!r7._class.isPrimitive() || !C29081fq.wrapperType(r7._class).isAssignableFrom(cls2))) {
                    throw new C37701w6("Incompatible types: declared root type (" + r7 + ") vs " + cls2.getName());
                }
            }
            if (jsonSerializer == null) {
                jsonSerializer = findTypedValueSerializer(r7, true, (CX9) null);
            }
            z = this._config.isEnabled(C10480kE.WRAP_ROOT_VALUE);
            if (z) {
                r5.writeStartObject();
                r5.writeFieldName(this._rootNames.findRootName(r7._class, this._config));
            }
        }
        try {
            jsonSerializer.serialize(obj, r5, this);
            if (z) {
                r5.writeEndObject();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            String message = e2.getMessage();
            if (message == null) {
                message = AnonymousClass08S.A0P("[no message for ", e2.getClass().getName(), "]");
            }
            throw new C37701w6(message, e2);
        }
    }
}
