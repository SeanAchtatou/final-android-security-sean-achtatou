package X;

import com.facebook.payments.paymentmethods.cardform.CardFormParams;
import com.facebook.payments.paymentmethods.model.FbPaymentCard;

/* renamed from: X.1D8  reason: invalid class name */
public final class AnonymousClass1D8 extends C06020ai {
    public final /* synthetic */ C54862n9 A00;
    public final /* synthetic */ CardFormParams A01;
    public final /* synthetic */ FbPaymentCard A02;

    public AnonymousClass1D8(C54862n9 r1, CardFormParams cardFormParams, FbPaymentCard fbPaymentCard) {
        this.A00 = r1;
        this.A01 = cardFormParams;
        this.A02 = fbPaymentCard;
    }
}
