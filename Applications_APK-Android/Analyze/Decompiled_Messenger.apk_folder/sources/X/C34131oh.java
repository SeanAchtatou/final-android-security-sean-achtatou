package X;

import android.os.Process;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1oh  reason: invalid class name and case insensitive filesystem */
public final class C34131oh {
    public static final AtomicInteger A00 = new AtomicInteger(0);
    private static final AtomicBoolean A01 = new AtomicBoolean(false);
    public static volatile Integer A02;
    private static volatile int A03;

    public static void A00() {
        if (A02 != null) {
            boolean z = false;
            if (A02.intValue() >= 0) {
                z = true;
            }
            if (z) {
                Process.setThreadPriority(A02.intValue(), A03);
                A01.set(false);
            }
        }
    }

    public static void A01() {
        if (A00.get() != 0) {
            int decrementAndGet = A00.decrementAndGet();
            if (decrementAndGet == 0) {
                A00();
            } else if (decrementAndGet < 0) {
                A00.compareAndSet(decrementAndGet, 0);
            }
        }
    }

    public static void A02(int i) {
        if (A02 == null) {
            A02 = Integer.valueOf(C70903bT.A00("(RenderThread)"));
        }
        int intValue = A02.intValue();
        boolean z = false;
        if (intValue >= 0) {
            z = true;
        }
        if (z) {
            if (A01.compareAndSet(false, true)) {
                A03 = Process.getThreadPriority(intValue);
            }
            Process.setThreadPriority(intValue, i);
            A00.incrementAndGet();
        }
    }
}
