package X;

import android.content.Context;
import com.facebook.messaging.dialog.MenuDialogFragment;
import com.facebook.messaging.dialog.MenuDialogParams;
import com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams;
import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.messaging.montage.viewer.MontageViewerFragment;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.facebook.messaging.threadview.params.MessageDeepLinkInfo;
import com.facebook.messaging.threadview.params.ThreadViewParams;
import com.facebook.orca.threadlist.ThreadListFragment;
import com.facebook.user.model.UserKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.util.HashMap;

/* renamed from: X.1B9  reason: invalid class name */
public final class AnonymousClass1B9 implements AnonymousClass1B6 {
    public final /* synthetic */ AnonymousClass1BX A00;
    public final /* synthetic */ ThreadListFragment A01;

    public void BWP(DiscoverTabAttachmentItem discoverTabAttachmentItem) {
    }

    public AnonymousClass1B9(ThreadListFragment threadListFragment, AnonymousClass1BX r2) {
        this.A01 = threadListFragment;
        this.A00 = r2;
    }

    public void BN9() {
        this.A01.A1B.A04.A00(C16040wO.CONTACTS);
    }

    public void BZY(C64663Ct r3) {
        if (C29244ERs.A00[r3.ordinal()] == 1) {
            C186414n r0 = this.A01.A1B;
            r0.A04.Bve(C64663Ct.A01);
        }
    }

    public void Bc3(InboxUnitItem inboxUnitItem) {
        this.A01.A0b.A02(inboxUnitItem);
    }

    public void Bd9() {
        this.A01.A14.A0A();
    }

    public void Bf5() {
        C186414n r0 = this.A01.A1B;
        r0.A04.Bvo(r0.A01);
    }

    public void BfQ(long j) {
        C186414n r3 = this.A01.A1B;
        if (r3.A02.A0Q(C99084oO.$const$string(195)) == null) {
            MontageViewerFragment A07 = MontageViewerFragment.A07(j, C88984Nc.A05);
            A07.A0L = new AnonymousClass5BS(r3, null);
            A07.A2G(r3.A02);
        }
    }

    public void BfU(InboxMontageItem inboxMontageItem) {
        C186414n r1 = this.A01.A1B;
        C73443g5 r7 = C73443g5.A08;
        r1.A04.BvS(r1.A01, NavigationTrigger.A00(C99084oO.$const$string(606)), MontageComposerFragmentParams.A01(r7, C73453g6.A00(r7), (C73473g8) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ASj, r1.A00)));
    }

    public void Bfa(ImmutableList immutableList) {
        MontageViewerFragment.A0A(immutableList, C89054Nl.A01).A2G(this.A01.A1B.A02);
    }

    public void Bfc(ImmutableList immutableList) {
        C186414n r2 = this.A01.A1B;
        if (C16000wK.A01(r2.A02)) {
            C111045Qu r9 = new C111045Qu((C110945Qk) AnonymousClass1XX.A03(AnonymousClass1Y3.AHM, r2.A00), r2.A01.getResources());
            AnonymousClass4YL r3 = new AnonymousClass4YL(r2);
            C13060qW r5 = r2.A02;
            Context context = r2.A01;
            String string = r9.A01.getString(2131828462);
            String string2 = r9.A01.getString(2131828461);
            if (!r9.A02.Aem(282432754419087L)) {
                C133186Lf r22 = new C133186Lf();
                r22.A02 = BuildConfig.FLAVOR;
                C1054852u r1 = new C1054852u();
                r1.A02 = 1;
                r1.A05 = string;
                r22.A00(r1.A00());
                C1054852u r12 = new C1054852u();
                r12.A02 = 2;
                r12.A05 = string2;
                r22.A00(r12.A00());
                r22.A04 = true;
                MenuDialogFragment A002 = MenuDialogFragment.A00(new MenuDialogParams(r22));
                A002.A00 = new AnonymousClass4YJ(r3, immutableList);
                A002.A25(r5, "nux_montage_context_menu");
            } else if (context != null) {
                AnonymousClass404 r4 = (AnonymousClass404) AnonymousClass1XX.A03(AnonymousClass1Y3.Ar0, r9.A00);
                AnonymousClass53H A02 = r4.A02(context);
                A02.add(string).A02 = new AnonymousClass4YI(r3, immutableList);
                A02.add(string2).A02 = new AnonymousClass4YK(r3);
                A02.A03 = false;
                r4.A01(context, A02).show();
            }
        }
    }

    public void Bh6(C149266wB r3, ThreadKey threadKey) {
        C186414n r0 = this.A01.A1B;
        r0.A04.Bvd(r0.A01, threadKey, r3);
    }

    public void Bh7() {
        this.A01.A1B.A04.Bvx();
    }

    public void BhA(ThreadKey threadKey, NavigationTrigger navigationTrigger, C13220qv r7) {
        ThreadListFragment threadListFragment = this.A01;
        threadListFragment.A0V.A0E(threadKey, ((C33891oJ) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AoV, threadListFragment.A0O)).A02(threadKey), r7.name(), -1);
        C186414n r2 = this.A01.A1B;
        C13290rA r0 = new C13290rA();
        r0.A01(threadKey);
        r0.A03 = navigationTrigger;
        r0.A02(r7);
        ThreadViewParams A002 = r0.A00();
        Preconditions.checkNotNull(A002);
        r2.A04.BwC(A002);
    }

    public void BhB(ThreadSummary threadSummary, int i, NavigationTrigger navigationTrigger, C13220qv r11) {
        MessageDeepLinkInfo messageDeepLinkInfo;
        C10950l8 r3 = threadSummary.A0O;
        if (r3.A01()) {
            ((C101694sz) AnonymousClass1XX.A02(3, AnonymousClass1Y3.APx, this.A01.A0O)).A04(threadSummary.A0S, r3, "inbox");
        }
        ThreadListFragment threadListFragment = this.A01;
        C24361Ti r32 = threadListFragment.A0U;
        int childCount = i / threadListFragment.A1F.getChildCount();
        HashMap A04 = AnonymousClass0TG.A04();
        ThreadParticipant A042 = C17270yd.A04(r32.A03, threadSummary, C28711fF.ONE_TO_ONE);
        if (A042 != null) {
            A04.put("contact_id", A042.A00().id);
        }
        A04.put("thread_key", threadSummary.A0S.toString());
        A04.put("row_index", Integer.toString(i));
        A04.put("page_index", Integer.toString(childCount));
        A04.put("unread", Boolean.toString(r32.A04.A01(threadSummary)));
        A04.put("last_msg_ts", Long.toString(threadSummary.A0B));
        boolean z = false;
        if (threadSummary.A0S.A05 == C28711fF.ONE_TO_ONE) {
            z = true;
        }
        A04.put(AnonymousClass80H.$const$string(52), Boolean.toString(z));
        r32.A09("thread_list", "thread", A04);
        AnonymousClass26K A002 = AnonymousClass26K.A00();
        A002.A01("position", i);
        ((EEY) AnonymousClass1XX.A02(15, AnonymousClass1Y3.Auy, this.A01.A0O)).A00.AOO(EEY.A01, "opened_thread", null, A002);
        ThreadListFragment threadListFragment2 = this.A01;
        C08770fv r4 = threadListFragment2.A0V;
        ThreadKey threadKey = threadSummary.A0S;
        r4.A0E(threadKey, ((C33891oJ) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AoV, threadListFragment2.A0O)).A02(threadKey), r11.name(), i);
        C186414n r42 = this.A01.A1B;
        C13290rA r33 = new C13290rA();
        r33.A01(threadSummary.A0S);
        r33.A03 = navigationTrigger;
        r33.A02(r11);
        if (!threadSummary.A0B() || threadSummary.A07 <= 0 || !this.A00.A01()) {
            messageDeepLinkInfo = null;
        } else {
            AnonymousClass6CI r2 = new AnonymousClass6CI();
            r2.A00 = threadSummary.A07;
            messageDeepLinkInfo = new MessageDeepLinkInfo(r2);
        }
        r33.A04 = messageDeepLinkInfo;
        ThreadViewParams A003 = r33.A00();
        Preconditions.checkNotNull(A003);
        r42.A04.BwC(A003);
    }

    public void BhP(UserKey userKey, long j, InboxMontageItem inboxMontageItem) {
        String str;
        C122645qG r5;
        boolean z;
        BasicMontageThreadInfo basicMontageThreadInfo;
        C186414n r0 = this.A01.A1B;
        InboxMontageItem inboxMontageItem2 = inboxMontageItem;
        if (inboxMontageItem == null || (basicMontageThreadInfo = inboxMontageItem2.A02) == null) {
            str = null;
        } else {
            str = basicMontageThreadInfo.A06;
        }
        if (inboxMontageItem == null) {
            r5 = new C122645qG(false, true, false, null, null);
        } else {
            boolean z2 = inboxMontageItem2.A06;
            BasicMontageThreadInfo basicMontageThreadInfo2 = inboxMontageItem2.A02;
            if (basicMontageThreadInfo2 == null) {
                z = false;
            } else {
                z = basicMontageThreadInfo2.A07;
            }
            r5 = new C122645qG(z2, true, z, inboxMontageItem2.A01, null);
        }
        new C111045Qu((C110945Qk) AnonymousClass1XX.A03(AnonymousClass1Y3.AHM, r0.A00), r0.A01.getResources()).A00(userKey, j, true, new AnonymousClass5BU(r0, inboxMontageItem2, str, r5), r0.A02, C99084oO.$const$string(658), r0.A01);
    }

    public void Bpe(ImmutableList immutableList, long j, String str, C114765cl r16) {
        String str2;
        C186414n r2 = this.A01.A1B;
        C857544z A012 = ((C857444y) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A8V, r2.A00)).A01(immutableList, j, true, false);
        if (!A012.A01.isEmpty() && r2.A02.A0Q(C99084oO.$const$string(195)) == null) {
            ImmutableList immutableList2 = A012.A01;
            int i = A012.A00;
            C88984Nc r5 = C88984Nc.A06;
            if (str == null) {
                str2 = BuildConfig.FLAVOR;
            } else {
                str2 = str;
            }
            MontageViewerFragment A09 = MontageViewerFragment.A09(immutableList2, i, r5, null, str2, C114765cl.A00, C114765cl.A02, C114765cl.A01);
            A09.A0L = new AnonymousClass5BS(r2, null);
            A09.A0b = str;
            A09.A2G(r2.A02);
        }
    }
}
