package X;

import com.facebook.xzdecoder.XzInputStream;
import java.io.InputStream;

/* renamed from: X.0NW  reason: invalid class name */
public final class AnonymousClass0NW extends AnonymousClass0EM {
    private int A00;
    public final InputStream A01;
    public final /* synthetic */ AnonymousClass0NV A02;

    public C03930Qq A00() {
        C03930Qq r1;
        while (true) {
            r1 = null;
            int i = this.A00;
            AnonymousClass0Na[] r2 = this.A02.A02;
            if (i >= r2.length) {
                break;
            }
            this.A00 = i + 1;
            AnonymousClass0Na r3 = r2[i];
            AnonymousClass0Qp r22 = new AnonymousClass0Qp(this, r3.A02);
            try {
                r1 = new C03930Qq(r3, r22);
                if (r3.A00) {
                    break;
                }
                r1.close();
            } catch (Throwable th) {
                r22.close();
                throw th;
            }
        }
        return r1;
    }

    public AnonymousClass0NW(AnonymousClass0NV r7) {
        Throwable th;
        String str;
        InputStream inputStream;
        this.A02 = r7;
        InputStream inputStream2 = r7.A01.getInputStream(r7.A00);
        try {
            Integer num = r7.A03.A00;
            if (num == AnonymousClass07B.A00) {
                inputStream = new XzInputStream(inputStream2);
            } else if (num == AnonymousClass07B.A01) {
                try {
                    inputStream = (InputStream) Class.forName("com.facebook.zstd.ZstdInputStream").getConstructor(InputStream.class).newInstance(inputStream2);
                } catch (Exception e) {
                    th = new IllegalArgumentException(e);
                }
            } else {
                Integer num2 = AnonymousClass07B.A0C;
                if (num == num2 || num == AnonymousClass07B.A0N) {
                    if (num == num2) {
                        str = "xz";
                    } else {
                        str = "zst";
                    }
                    inputStream = (InputStream) Class.forName("com.facebook.superpack.SuperpackFileInputStream").getMethod("createFromSingletonArchiveInputStream", InputStream.class, String.class).invoke(null, inputStream2, str);
                } else {
                    th = new IllegalStateException("Unknown compression algorithm");
                    throw th;
                }
            }
            this.A01 = inputStream;
            if (!A01()) {
                close();
            }
        } catch (Exception e2) {
            th = new RuntimeException("Could not access Superpack archive", e2);
        } catch (Throwable th2) {
            if (inputStream2 != null) {
                inputStream2.close();
            }
            throw th2;
        }
    }

    public boolean A01() {
        int length;
        int i = this.A00;
        while (true) {
            AnonymousClass0Na[] r0 = this.A02.A02;
            length = r0.length;
            if (i < length && !r0[i].A00) {
                i++;
            }
        }
        if (i < length) {
            return true;
        }
        return false;
    }

    public void close() {
        this.A01.close();
    }
}
