package X;

import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1NF  reason: invalid class name */
public class AnonymousClass1NF {
    public final C21361Gm A00;
    private final InboxUnitThreadItem A01;
    private final AnonymousClass1NH A02;

    public void A00() {
        this.A02.Bri(this.A01, this.A00);
    }

    public AnonymousClass1NF(AnonymousClass1NH r1, InboxUnitThreadItem inboxUnitThreadItem, C21361Gm r3) {
        this.A02 = r1;
        this.A01 = inboxUnitThreadItem;
        this.A00 = r3;
    }
}
