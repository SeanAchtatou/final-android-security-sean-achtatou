package X;

/* renamed from: X.0GN  reason: invalid class name */
public final class AnonymousClass0GN {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "BUILT_IN";
            case 2:
                return "DOWNLOADABLE";
            case 3:
                return "BUILT_IN_AND_DOWNLOADABLE";
            default:
                return "UNDEFINED";
        }
    }

    public static boolean A01(Integer num) {
        if (num == AnonymousClass07B.A01 || num == AnonymousClass07B.A0N) {
            return true;
        }
        return false;
    }
}
