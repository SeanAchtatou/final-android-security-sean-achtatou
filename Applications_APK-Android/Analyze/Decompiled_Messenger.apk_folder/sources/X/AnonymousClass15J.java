package X;

import io.card.payment.BuildConfig;

/* renamed from: X.15J  reason: invalid class name */
public final class AnonymousClass15J {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "new";
            case 2:
                return "patched";
            case 3:
                return "failed";
            default:
                return BuildConfig.FLAVOR;
        }
    }
}
