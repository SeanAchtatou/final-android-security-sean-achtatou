package X;

import com.facebook.graphql.calls.GraphQlCallInput;
import com.facebook.graphql.query.GraphQlQueryParamSet;
import com.facebook.graphql.query.JsonPathValue;
import com.google.common.base.Throwables;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/* renamed from: X.0pD  reason: invalid class name and case insensitive filesystem */
public final class C12360pD implements C09300gy {
    /* JADX WARN: Type inference failed for: r1v0, types: [X.0dX, java.lang.Object, java.lang.Integer] */
    public String Azr(C26931cN r11, AnonymousClass0m4 r12) {
        ArrayList arrayList;
        String str;
        String[] strArr = r11.A08;
        if (strArr == null || (r2 = strArr.length) <= 0) {
            arrayList = null;
        } else {
            arrayList = C04300To.A04(strArr);
            for (String add : strArr) {
                arrayList.add(add);
            }
        }
        C10880l0 r4 = r11.A0B;
        GraphQlQueryParamSet graphQlQueryParamSet = r4.A00;
        GraphQlQueryParamSet graphQlQueryParamSet2 = graphQlQueryParamSet;
        if (graphQlQueryParamSet == null) {
            return r12.A01(r4, null);
        }
        try {
            Integer num = AnonymousClass07B.A00;
            C26961cQ r5 = new C26961cQ();
            for (Map.Entry entry : graphQlQueryParamSet.A00().entrySet()) {
                if (arrayList == null || ((!arrayList.contains(entry.getKey()) && num == num) || (arrayList.contains(entry.getKey()) && num == AnonymousClass07B.A01))) {
                    r5.A00(entry.getKey());
                    Object value = entry.getValue();
                    if (value instanceof List) {
                        r5.A00((List) value);
                    } else if (value instanceof Array) {
                        r5.A00 = (r5.A00 * 31) + Arrays.deepHashCode((Object[]) value);
                    } else if (value instanceof String) {
                        r5.A00((String) value);
                    } else if (value instanceof Number) {
                        r5.A00((Number) value);
                    } else if (value instanceof JsonPathValue) {
                        r5.A00(value.toString());
                    } else if (value instanceof Boolean) {
                        r5.A00((Boolean) value);
                    } else if (value instanceof Enum) {
                        r5.A00((Enum) value);
                    } else if (value instanceof GraphQlCallInput) {
                        r5.A00(((AnonymousClass0jJ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AmL, r12.A00)).writeValueAsString(value));
                    } else if (value instanceof C10040jS) {
                        r5.A00(((AnonymousClass0jJ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AmL, r12.A00)).writeValueAsString(value));
                    } else {
                        r5.A00(value);
                    }
                }
            }
            for (Map.Entry entry2 : graphQlQueryParamSet2.A01.entrySet()) {
                r5.A00(entry2.getKey());
                entry2.getValue();
                ? r1 = 0;
                String str2 = null;
                if (r1 == 0) {
                    str2 = r1.A0B.A06;
                }
                r5.A00(str2);
                entry2.getValue();
                r5.A00(r1);
                entry2.getValue();
                switch (r1.intValue()) {
                    case 1:
                        str = "ALL";
                        break;
                    case 2:
                        str = "FIRST";
                        break;
                    case 3:
                        str = "LAST";
                        break;
                    case 4:
                        str = "NO_FAN_OUT";
                        break;
                    default:
                        str = "EACH";
                        break;
                }
                r5.A00(str);
            }
            return r12.A01(r4, Integer.toString(r5.hashCode()));
        } catch (C37571vt e) {
            throw Throwables.propagate(e);
        }
    }
}
