package X;

import com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager;
import com.facebook.profilo.blackbox.breakpad.BreakpadTraceListener;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.07E  reason: invalid class name */
public final class AnonymousClass07E {
    private static volatile AnonymousClass07E A01;
    private AnonymousClass0UN A00;

    public static final AnonymousClass07E A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass07E.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass07E(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public AnonymousClass0Qc A01() {
        return (AnonymousClass0Qc) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCu, this.A00);
    }

    public AnonymousClass09H[] A02() {
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AcD, this.A00)).AbO(667, false)) {
            int i = AnonymousClass1Y3.AJk;
            AnonymousClass0UN r4 = this.A00;
            return new AnonymousClass09H[]{(AnonymousClass09W) AnonymousClass1XX.A02(1, i, r4), (AnonymousClass09X) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AKx, r4), (BreakpadTraceListener) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BGN, r4), (BlackBoxAppStateAwareManager) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Avi, r4)};
        }
        int i2 = AnonymousClass1Y3.AJk;
        AnonymousClass0UN r3 = this.A00;
        return new AnonymousClass09H[]{(AnonymousClass09W) AnonymousClass1XX.A02(1, i2, r3), (AnonymousClass09X) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AKx, r3), (BlackBoxAppStateAwareManager) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Avi, r3)};
    }

    private AnonymousClass07E(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
    }
}
