package X;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.Semaphore;

/* renamed from: X.1YW  reason: invalid class name */
public final class AnonymousClass1YW {
    public AnonymousClass0UN A00;
    public Map A01 = Collections.synchronizedMap(new HashMap());
    public Set A02 = Collections.synchronizedSet(new HashSet());
    public LinkedBlockingDeque A03 = new LinkedBlockingDeque();
    public Semaphore A04 = new Semaphore(3);
    public final AnonymousClass1YX A05 = new AnonymousClass1YX(this);
    private final boolean A06;

    private ListenableFuture A00() {
        if (this.A04.tryAcquire()) {
            return ((C25311Zd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ARL, this.A00)).A01(A01(), this.A06);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0071 A[LOOP:0: B:1:0x0002->B:22:0x0071, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0084 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Runnable A01() {
        /*
            r8 = this;
            X.1Ze r3 = new X.1Ze
        L_0x0002:
            java.util.concurrent.LinkedBlockingDeque r0 = r8.A03
            java.lang.Object r4 = r0.poll()
            X.1ZX r4 = (X.AnonymousClass1ZX) r4
            if (r4 == 0) goto L_0x007d
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BTd
            X.0UN r0 = r8.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1YY r6 = (X.AnonymousClass1YY) r6
            int r0 = r4.A02
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
            java.util.concurrent.ConcurrentHashMap r0 = r6.A01
            java.lang.Object r7 = r0.get(r5)
            java.util.concurrent.ConcurrentSkipListSet r7 = (java.util.concurrent.ConcurrentSkipListSet) r7
            if (r7 == 0) goto L_0x0061
            boolean r0 = r7.isEmpty()
            if (r0 != 0) goto L_0x0061
            java.util.Iterator r2 = r7.iterator()
        L_0x0031:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0068
            java.lang.Object r1 = r2.next()
            java.lang.Integer r1 = (java.lang.Integer) r1
            java.util.concurrent.ConcurrentSkipListSet r0 = r6.A03
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x0031
            r2.remove()
            java.util.concurrent.ConcurrentHashMap r0 = r6.A02
            java.lang.Object r0 = r0.get(r1)
            java.util.concurrent.ConcurrentSkipListSet r0 = (java.util.concurrent.ConcurrentSkipListSet) r0
            if (r0 == 0) goto L_0x0031
            r0.remove(r5)
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0031
            java.util.concurrent.ConcurrentHashMap r0 = r6.A02
            r0.remove(r1)
            goto L_0x0031
        L_0x0061:
            java.util.concurrent.ConcurrentHashMap r0 = r6.A01
            r0.remove(r5)
            r0 = 0
            goto L_0x006f
        L_0x0068:
            boolean r0 = r7.isEmpty()
            if (r0 != 0) goto L_0x0061
            r0 = 1
        L_0x006f:
            if (r0 == 0) goto L_0x0084
            java.util.Map r1 = r8.A01
            int r0 = r4.A02
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.put(r0, r4)
            goto L_0x0002
        L_0x007d:
            java.util.concurrent.Semaphore r0 = r8.A04
            r0.release()
            r4 = 0
            goto L_0x008f
        L_0x0084:
            java.util.Set r1 = r8.A02
            int r0 = r4.A02
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.add(r0)
        L_0x008f:
            r3.<init>(r8, r4)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1YW.A01():java.lang.Runnable");
    }

    public C25291Zb A02(AnonymousClass1ZX r6) {
        this.A03.addLast(r6);
        int i = AnonymousClass1Y3.BRs;
        AnonymousClass1ZZ.A00((AnonymousClass1ZZ) AnonymousClass1XX.A02(2, i, this.A00), r6, true);
        AnonymousClass1ZZ r3 = (AnonymousClass1ZZ) AnonymousClass1XX.A02(2, i, this.A00);
        C25281Za A002 = AnonymousClass1ZZ.A00(r3, r6, false);
        if (A002 != null) {
            A002.A01 = ((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, r3.A00)).now();
        }
        C25291Zb r32 = new C25291Zb(r6);
        r32.A01 = this.A05;
        C05350Yp.A07(r32, new C25301Zc(r6));
        ListenableFuture A003 = A00();
        if (A003 != null) {
            Preconditions.checkNotNull(A003);
            r32.A05 = A003;
            if (r32.A02) {
                AnonymousClass09V.A00(A003);
                r32.A05.cancel(true);
            } else {
                AnonymousClass09V.A00(A003);
                r32.A05.addListener(new C25341Zg(r32), C25141Ym.INSTANCE);
                return r32;
            }
        }
        return r32;
    }

    public void A03(Integer num) {
        this.A02.remove(num);
        AnonymousClass1YY r5 = (AnonymousClass1YY) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BTd, this.A00);
        Integer valueOf = Integer.valueOf(num.intValue());
        r5.A03.add(valueOf);
        ConcurrentSkipListSet concurrentSkipListSet = (ConcurrentSkipListSet) r5.A02.remove(valueOf);
        if (concurrentSkipListSet != null && !concurrentSkipListSet.isEmpty()) {
            Iterator it = concurrentSkipListSet.iterator();
            while (it.hasNext()) {
                Integer num2 = (Integer) it.next();
                ConcurrentSkipListSet concurrentSkipListSet2 = (ConcurrentSkipListSet) r5.A01.get(num2);
                if (concurrentSkipListSet2 != null) {
                    concurrentSkipListSet2.remove(valueOf);
                    if (concurrentSkipListSet2.isEmpty()) {
                        r5.A01.remove(num2);
                        AnonymousClass1YU r4 = r5.A00;
                        int intValue = num2.intValue();
                        AnonymousClass1YW r2 = r4.A00.A02;
                        Map map = r2.A01;
                        Integer valueOf2 = Integer.valueOf(intValue);
                        AnonymousClass1ZX r1 = (AnonymousClass1ZX) map.remove(valueOf2);
                        if (r1 != null) {
                            r2.A03.addFirst(r1);
                            r2.A00();
                        }
                        AnonymousClass1YW r22 = r4.A00.A01;
                        AnonymousClass1ZX r12 = (AnonymousClass1ZX) r22.A01.remove(valueOf2);
                        if (r12 != null) {
                            r22.A03.addFirst(r12);
                            r22.A00();
                        }
                    }
                }
            }
        }
        if (this.A03.peek() != null) {
            ((C25311Zd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ARL, this.A00)).A01(A01(), this.A06);
        } else {
            this.A04.release();
        }
    }

    public AnonymousClass1YW(AnonymousClass1XY r3, boolean z) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A06 = z;
    }
}
