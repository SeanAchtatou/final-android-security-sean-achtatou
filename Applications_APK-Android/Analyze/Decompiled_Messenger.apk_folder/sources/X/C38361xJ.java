package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.integrity.frx.model.AdditionalAction;

/* renamed from: X.1xJ  reason: invalid class name and case insensitive filesystem */
public final class C38361xJ implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new AdditionalAction(parcel);
    }

    public Object[] newArray(int i) {
        return new AdditionalAction[i];
    }
}
