package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.0jG  reason: invalid class name */
public final class AnonymousClass0jG {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) A03.A09(TurboLoader.Locator.$const$string(164)));
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06 = ((AnonymousClass1Y7) C04350Ue.A06.A09("perfmarker_to_logcat"));
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A06.A09("logging/");
        A03 = r1;
        A00 = (AnonymousClass1Y7) r1.A09("debug_logs");
        AnonymousClass1Y7 r12 = A03;
        r12.A09("thread_tracking");
        A05 = (AnonymousClass1Y7) r12.A09("nonemployee_mode");
        r12.A09("display_full_fclient_value_model");
        r12.A09("display_key_client_value_model");
        A01 = (AnonymousClass1Y7) r12.A09("fresco_overlay");
        A04 = (AnonymousClass1Y7) r12.A09("network_drawable_overlay");
        A07 = (AnonymousClass1Y7) r12.A09("redrawable_overlay");
        r12.A09("cameracore_fps_overlay");
        r12.A09("spherical_360_parallax");
        A08 = (AnonymousClass1Y7) r12.A09("video_viewability_logging_debug");
    }
}
