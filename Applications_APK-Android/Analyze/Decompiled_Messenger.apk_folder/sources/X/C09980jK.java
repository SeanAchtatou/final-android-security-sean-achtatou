package X;

import java.util.Iterator;

/* renamed from: X.0jK  reason: invalid class name and case insensitive filesystem */
public abstract class C09980jK {
    public abstract C10370jz getJsonFactory();

    public abstract C10010jO readTree(C28271eX r1);

    public abstract Object readValue(C28271eX r1, C28231eT r2);

    public abstract Object readValue(C28271eX r1, Class cls);

    public abstract Iterator readValues(C28271eX r1, Class cls);

    public abstract void writeValue(C11710np r1, Object obj);

    public C10370jz getFactory() {
        return getJsonFactory();
    }
}
