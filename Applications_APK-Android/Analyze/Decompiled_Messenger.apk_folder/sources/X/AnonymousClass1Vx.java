package X;

import com.facebook.common.connectionstatus.FbDataConnectionManager;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1Vx  reason: invalid class name */
public final class AnonymousClass1Vx implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.connectionstatus.FbDataConnectionManager$ConnectionQualityResetRunnable";
    public final /* synthetic */ FbDataConnectionManager A00;

    public AnonymousClass1Vx(FbDataConnectionManager fbDataConnectionManager) {
        this.A00 = fbDataConnectionManager;
    }

    public void run() {
        AnonymousClass1Vy r3;
        if (((AnonymousClass0Ud) AnonymousClass1XX.A02(9, AnonymousClass1Y3.B5j, this.A00.A00)).A0G()) {
            FbDataConnectionManager fbDataConnectionManager = this.A00;
            AtomicReference atomicReference = fbDataConnectionManager.A03;
            AnonymousClass1Vy r1 = AnonymousClass1Vy.UNKNOWN;
            atomicReference.set(r1);
            fbDataConnectionManager.A04.set(r1);
            C31121jB r12 = (C31121jB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BH9, fbDataConnectionManager.A00);
            synchronized (r12) {
                C48552ac r0 = r12.A01;
                if (r0 != null) {
                    r0.reset();
                }
                AtomicReference atomicReference2 = r12.A02;
                r3 = AnonymousClass1Vy.UNKNOWN;
                atomicReference2.set(r3);
            }
            AnonymousClass1W1 r13 = (AnonymousClass1W1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B1i, fbDataConnectionManager.A00);
            C48552ac r02 = r13.A01;
            if (r02 != null) {
                r02.reset();
            }
            r13.A03.set(r3);
            FbDataConnectionManager.A01(this.A00);
        }
    }
}
