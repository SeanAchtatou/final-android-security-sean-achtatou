package X;

import com.facebook.imagepipeline.nativecode.WebpTranscoderImpl;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1Sv  reason: invalid class name and case insensitive filesystem */
public abstract class C24241Sv implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.executors.StatefulRunnable";
    public final AtomicInteger A00 = new AtomicInteger(0);

    public Object A00() {
        AnonymousClass1PS A01;
        if (!(this instanceof AnonymousClass1R2)) {
            return null;
        }
        AnonymousClass1R2 r5 = (AnonymousClass1R2) this;
        AnonymousClass1S3 A012 = r5.A01.A00.A01();
        try {
            AnonymousClass1NY r4 = r5.A00;
            InputStream A09 = r4.A09();
            AnonymousClass1O3 A013 = AnonymousClass1OG.A01(A09);
            if (A013 == AnonymousClass1SI.A0C || A013 == AnonymousClass1SI.A09) {
                AnonymousClass9K8.A00();
                C05520Zg.A02(A09);
                C05520Zg.A02(A012);
                WebpTranscoderImpl.nativeTranscodeWebpToJpeg(A09, A012, 80);
                r4.A07 = AnonymousClass1SI.A06;
            } else if (A013 == AnonymousClass1SI.A0B || A013 == AnonymousClass1SI.A0A) {
                AnonymousClass9K8.A00();
                C05520Zg.A02(A09);
                C05520Zg.A02(A012);
                WebpTranscoderImpl.nativeTranscodeWebpToPng(A09, A012);
                r4.A07 = AnonymousClass1SI.A07;
            } else {
                throw new IllegalArgumentException("Wrong image format");
            }
            A01 = AnonymousClass1PS.A01(A012.A01());
            AnonymousClass1NY r1 = new AnonymousClass1NY(A01);
            r1.A0B(r5.A00);
            AnonymousClass1PS.A05(A01);
            A012.close();
            return r1;
        } catch (Throwable th) {
            A012.close();
            throw th;
        }
    }

    public void A02() {
    }

    public void A03(Exception exc) {
    }

    public void A04(Object obj) {
    }

    public void A05(Object obj) {
    }

    public void A01() {
        if (this.A00.compareAndSet(0, 2)) {
            A02();
        }
    }

    public final void run() {
        if (this.A00.compareAndSet(0, 1)) {
            try {
                Object A002 = A00();
                this.A00.set(3);
                try {
                    A05(A002);
                } finally {
                    A04(A002);
                }
            } catch (Exception e) {
                this.A00.set(4);
                A03(e);
            }
        }
    }
}
