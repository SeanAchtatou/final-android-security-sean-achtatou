package X;

import java.io.File;
import java.util.Comparator;

/* renamed from: X.05p  reason: invalid class name and case insensitive filesystem */
public final class C004805p implements Comparator {
    public int compare(Object obj, Object obj2) {
        return ((File) obj).getName().compareTo(((File) obj2).getName());
    }
}
