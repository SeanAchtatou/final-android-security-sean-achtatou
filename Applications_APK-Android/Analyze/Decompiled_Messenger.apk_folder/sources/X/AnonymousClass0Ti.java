package X;

import java.util.Arrays;

/* renamed from: X.0Ti  reason: invalid class name */
public final class AnonymousClass0Ti {
    public static final AnonymousClass0Ti A03 = new AnonymousClass0Ti(-1);
    public static final AnonymousClass0Ti A04 = new AnonymousClass0Ti(new int[0]);
    public final int[] A00;
    public final int[] A01;
    public final int[] A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass0Ti r5 = (AnonymousClass0Ti) obj;
            if (!Arrays.equals(this.A01, r5.A01) || !Arrays.equals(this.A02, r5.A02) || !Arrays.equals(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public static AnonymousClass0Ti A00(int... iArr) {
        return new AnonymousClass0Ti(iArr, null, null);
    }

    public int hashCode() {
        return (((Arrays.hashCode(this.A01) * 31) + Arrays.hashCode(this.A02)) * 31) + Arrays.hashCode(this.A00);
    }

    public String toString() {
        return AnonymousClass08S.A0V("{normalMarkers: ", Arrays.toString(this.A01), ", quickMarkers: ", Arrays.toString(this.A02), ", metadataMarkers: ", Arrays.toString(this.A00), "}");
    }

    private AnonymousClass0Ti(int... iArr) {
        this.A01 = iArr;
        this.A02 = iArr;
        this.A00 = null;
    }

    public AnonymousClass0Ti(int[] iArr, int[] iArr2, int[] iArr3) {
        this.A01 = iArr;
        this.A02 = iArr2;
        this.A00 = iArr3;
    }
}
