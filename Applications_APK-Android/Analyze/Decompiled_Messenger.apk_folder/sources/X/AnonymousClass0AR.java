package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: X.0AR  reason: invalid class name */
public abstract class AnonymousClass0AR {
    public BroadcastReceiver A00;
    public boolean A01 = false;
    public AnonymousClass0Ct A02;
    public final Context A03;
    public final AnonymousClass0AS A04;
    public final C012309k A05;
    public final Integer A06;
    public final String A07;

    public void A06(String str, Intent intent) {
    }

    public void A07(String str, String str2, AnonymousClass0SR r3) {
    }

    public abstract boolean A08(AnonymousClass0SE r1);

    public int A02() {
        Intent intent;
        ArrayList<AnonymousClass0SE> arrayList = new ArrayList<>();
        ArrayList<AnonymousClass0SE> arrayList2 = new ArrayList<>();
        AnonymousClass0Ct A032 = A03();
        synchronized (A032) {
            arrayList.clear();
            arrayList2.clear();
            Map<String, ?> all = A032.A01.getAll();
            SharedPreferences.Editor edit = A032.A01.edit();
            boolean z = false;
            for (Map.Entry next : all.entrySet()) {
                AnonymousClass0PW A002 = AnonymousClass0PW.A00(next.getValue());
                if (A002 != null) {
                    long j = A002.A01;
                    if (j + 86400000 >= System.currentTimeMillis() && j <= System.currentTimeMillis()) {
                        if (A002.A00 + A032.A00.get() < System.currentTimeMillis()) {
                            A002.A00 = System.currentTimeMillis();
                            arrayList.add(A002);
                            edit.putString(A002.A01, A002.A01());
                            z = true;
                        }
                    }
                }
                edit.remove((String) next.getKey());
                if (A002 != null) {
                    arrayList2.add(A002);
                }
                z = true;
            }
            if (z) {
                AnonymousClass0Ct.A00(A032, edit);
            }
        }
        for (AnonymousClass0SE r1 : arrayList2) {
            if (!(r1 == null || (intent = r1.A00) == null)) {
                A07(r1.A01, intent.getPackage(), AnonymousClass0SR.A03);
            }
        }
        int i = 0;
        for (AnonymousClass0SE r2 : arrayList) {
            A06(r2.A01, r2.A00);
            if (A08(r2)) {
                i++;
            }
        }
        return i;
    }

    public AnonymousClass0Ct A03() {
        String str;
        if (this.A02 == null) {
            Context context = this.A03;
            String str2 = this.A07;
            if (1 - this.A06.intValue() != 0) {
                str = "FBNS_LITE";
            } else {
                str = "FBNS";
            }
            this.A02 = new AnonymousClass0Ct(context, AnonymousClass08S.A07(str2, '_', str), this.A01);
        }
        return this.A02;
    }

    public void A04(Handler handler) {
        if (this.A00 == null) {
            C01790Bq r3 = new C01790Bq(this);
            this.A00 = r3;
            C009207y.A01.A08(this.A03, r3, new IntentFilter("com.facebook.rti.intent.ACTION_NOTIFICATION_ACK"), null, handler);
        }
    }

    public AnonymousClass0AR(Context context, C012309k r3, AnonymousClass0AS r4, String str, Integer num) {
        this.A03 = context;
        this.A05 = r3;
        this.A04 = r4;
        this.A07 = str;
        this.A06 = num;
    }

    public static void A00(Context context, String str, String str2, boolean z) {
        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str)) {
            Intent intent = new Intent("com.facebook.rti.intent.ACTION_NOTIFICATION_ACK");
            intent.setPackage(str);
            intent.putExtra("extra_notification_id", str2);
            intent.putExtra("extra_processor_completed", z);
            new C012309k(context, null).A03(intent, str);
        }
    }

    public long A05(String str, String str2, boolean z) {
        return A03().A01(str);
    }
}
