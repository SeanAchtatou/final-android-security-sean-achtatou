package X;

import android.os.SystemClock;
import com.facebook.acra.config.StartupBlockingConfig;

/* renamed from: X.0BA  reason: invalid class name */
public final class AnonymousClass0BA {
    public long A00 = -1;
    public long A01 = -1;
    public long A02 = 0;
    public final AnonymousClass0AW A03;
    public final AnonymousClass0B6 A04;
    private final boolean A05;

    public synchronized void A00() {
        if (this.A05) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            long j = this.A00;
            if (j < 0) {
                this.A00 = elapsedRealtime;
                this.A01 = elapsedRealtime;
            } else {
                long j2 = elapsedRealtime - j;
                this.A00 = elapsedRealtime;
                if (j2 > StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS) {
                    this.A02 += StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS;
                } else {
                    this.A02 += j2;
                }
                if (elapsedRealtime - this.A01 > 20000) {
                    this.A02 += this.A03.AbP(AnonymousClass07B.A02).getLong("total_wake_ms", 0);
                    AnonymousClass0DD AY8 = this.A03.AbP(AnonymousClass07B.A02).AY8();
                    AY8.BzB("total_wake_ms", this.A02);
                    AY8.commit();
                    this.A02 = 0;
                    this.A01 = elapsedRealtime;
                }
                if (elapsedRealtime - this.A03.AbP(AnonymousClass07B.A02).getLong("last_log_ms", elapsedRealtime) > 3600000) {
                    this.A04.A07("mqtt_radio_active_time", C01740Bl.A00("total_wake_ms", Long.toString(this.A03.AbP(AnonymousClass07B.A02).getLong("total_wake_ms", 0))));
                    AnonymousClass0DD AY82 = this.A03.AbP(AnonymousClass07B.A02).AY8();
                    AY82.ASR();
                    AY82.commit();
                    AnonymousClass0DD AY83 = this.A03.AbP(AnonymousClass07B.A02).AY8();
                    AY83.BzB("last_log_ms", elapsedRealtime);
                    AY83.commit();
                }
            }
        }
    }

    public AnonymousClass0BA(AnonymousClass0B6 r7, boolean z, AnonymousClass0AW r9) {
        this.A04 = r7;
        this.A05 = z;
        this.A03 = r9;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (r9.AbP(AnonymousClass07B.A02).getLong("last_log_ms", elapsedRealtime) >= elapsedRealtime) {
            AnonymousClass0DD AY8 = this.A03.AbP(AnonymousClass07B.A02).AY8();
            AY8.BzB("last_log_ms", elapsedRealtime);
            AY8.commit();
        }
    }
}
