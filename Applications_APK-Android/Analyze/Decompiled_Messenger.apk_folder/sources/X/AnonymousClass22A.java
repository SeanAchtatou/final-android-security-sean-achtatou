package X;

import android.content.Context;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.user.model.User;
import java.util.BitSet;

/* renamed from: X.22A  reason: invalid class name */
public final class AnonymousClass22A extends C16070wR {
    @Comparable(type = 13)
    public ThreadKey A00;
    @Comparable(type = 13)
    public C109065Iq A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 13)
    public User A03;
    @Comparable(type = 13)
    public String A04;
    @Comparable(type = 13)
    public String A05;
    @Comparable(type = 3)
    public boolean A06;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1.equals(r5.A02) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0U(X.C16070wR r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0082
            r2 = 0
            if (r5 == 0) goto L_0x001e
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x001e
            X.22A r5 = (X.AnonymousClass22A) r5
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r4.A02
            if (r1 == 0) goto L_0x001f
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r5.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0024
        L_0x001e:
            return r2
        L_0x001f:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r5.A02
            if (r0 == 0) goto L_0x0024
            return r2
        L_0x0024:
            X.5Iq r1 = r4.A01
            if (r1 == 0) goto L_0x0031
            X.5Iq r0 = r5.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0036
            return r2
        L_0x0031:
            X.5Iq r0 = r5.A01
            if (r0 == 0) goto L_0x0036
            return r2
        L_0x0036:
            java.lang.String r1 = r4.A04
            if (r1 == 0) goto L_0x0043
            java.lang.String r0 = r5.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0048
            return r2
        L_0x0043:
            java.lang.String r0 = r5.A04
            if (r0 == 0) goto L_0x0048
            return r2
        L_0x0048:
            java.lang.String r1 = r4.A05
            if (r1 == 0) goto L_0x0055
            java.lang.String r0 = r5.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x005a
            return r2
        L_0x0055:
            java.lang.String r0 = r5.A05
            if (r0 == 0) goto L_0x005a
            return r2
        L_0x005a:
            boolean r1 = r4.A06
            boolean r0 = r5.A06
            if (r1 != r0) goto L_0x001e
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A00
            if (r1 == 0) goto L_0x006d
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0072
            return r2
        L_0x006d:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A00
            if (r0 == 0) goto L_0x0072
            return r2
        L_0x0072:
            com.facebook.user.model.User r1 = r4.A03
            com.facebook.user.model.User r0 = r5.A03
            if (r1 == 0) goto L_0x007f
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0082
            return r2
        L_0x007f:
            if (r0 == 0) goto L_0x0082
            return r2
        L_0x0082:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass22A.A0U(X.0wR):boolean");
    }

    public AnonymousClass22A(Context context) {
        super("ProfileHeaderUnitSection");
        new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }

    public C20301Bw A0R(AnonymousClass1GA r8) {
        String str = this.A05;
        C20311Bx A002 = C20301Bw.A00();
        String[] strArr = {C99084oO.$const$string(692)};
        BitSet bitSet = new BitSet(1);
        C20271Bt r2 = new C20271Bt(r8.A09);
        bitSet.clear();
        r2.A00 = 86400;
        r2.A05 = new AnonymousClass5IM(str);
        bitSet.set(0);
        r2.A04 = C16080wS.A02(r8, 1935729872, new Object[]{r8});
        AnonymousClass1CY.A00(1, bitSet, strArr);
        A002.A01(r2);
        return A002.A00;
    }

    public Object AXa(AnonymousClass10N r26, Object obj) {
        C20311Bx r4;
        C16070wR r0;
        Object obj2 = obj;
        AnonymousClass10N r5 = r26;
        int i = r5.A01;
        if (i == 1046174140) {
            ((AnonymousClass22A) r5.A00).A01.BWX(C99084oO.$const$string(AnonymousClass1Y3.A3Y));
            return null;
        } else if (i != 1935729872) {
            return null;
        } else {
            C1290663b r42 = (C1290663b) obj2;
            C17810zV r2 = r5.A00;
            AnonymousClass1GA r8 = (AnonymousClass1GA) r5.A02[0];
            C156877Ne r1 = r42.A01;
            Object obj3 = r42.A02;
            AnonymousClass22A r22 = (AnonymousClass22A) r2;
            String str = r22.A05;
            ThreadKey threadKey = r22.A00;
            User user = r22.A03;
            C109065Iq r6 = r22.A01;
            boolean z = r22.A06;
            String str2 = r22.A04;
            MigColorScheme migColorScheme = r22.A02;
            C156877Ne r3 = r1;
            if (r1 != null) {
                switch (r1.ordinal()) {
                    case 0:
                        break;
                    case 1:
                        r4 = C20301Bw.A00();
                        C33981oS A062 = AnonymousClass1GV.A06();
                        C37951we A002 = AnonymousClass11D.A00(r8);
                        A002.A2Z(AnonymousClass10G.TOP, 56.0f);
                        A002.A2Z(AnonymousClass10G.BOTTOM, 12.0f);
                        C37941wd A003 = C16980y8.A00(r8);
                        A003.A3E(C14950uP.CENTER);
                        A003.A2H(AnonymousClass1M2.A05(migColorScheme.B2D(), (float) r8.A03().getDimensionPixelSize(2132148235)));
                        A003.A2Z(AnonymousClass10G.A09, 56.0f);
                        AnonymousClass5JB r12 = new AnonymousClass5JB();
                        C17770zR r02 = r8.A04;
                        if (r02 != null) {
                            r12.A07 = r02.A06;
                        }
                        A003.A3A(r12);
                        A002.A39(A003);
                        A062.A05(A002);
                        r0 = A062.A04();
                        r4.A01(r0);
                        return r4.A00;
                    case 2:
                        GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) obj3;
                        r4 = C20301Bw.A00();
                        C33981oS A063 = AnonymousClass1GV.A06();
                        ComponentBuilderCBuilderShape0_0S0300000 A004 = C21641Id.A00(r8);
                        A004.A3F(0);
                        A004.A20(100.0f);
                        A004.A1p(56.0f);
                        A004.A2y(C16080wS.A02(r8, 1046174140, new Object[]{r8}));
                        A063.A06(A004.A34());
                        r4.A00(A063);
                        if (gSTModelShape1S0000000 == null) {
                            r0 = A06(r8, user, migColorScheme, r6);
                            r4.A01(r0);
                            return r4.A00;
                        }
                        C33981oS A064 = AnonymousClass1GV.A06();
                        String[] strArr = {"colorScheme", C99084oO.$const$string(AnonymousClass1Y3.A1B), "entryPoint", "profileId", "queryModel", "threadKey", "user"};
                        BitSet bitSet = new BitSet(7);
                        AnonymousClass5I7 r14 = new AnonymousClass5I7(r8.A09);
                        C17540z4 r18 = r8.A0B;
                        C17770zR r03 = r8.A04;
                        if (r03 != null) {
                            r14.A07 = r03.A06;
                        }
                        bitSet.clear();
                        r14.A07 = str;
                        bitSet.set(3);
                        float f = 0.0f;
                        if (z) {
                            f = 460.0f;
                        }
                        r14.A14().BL8(r18.A00(f));
                        r14.A02 = threadKey;
                        bitSet.set(5);
                        r14.A05 = user;
                        bitSet.set(6);
                        r14.A00 = gSTModelShape1S0000000;
                        bitSet.set(4);
                        r14.A04 = migColorScheme;
                        bitSet.set(0);
                        r14.A14().ANz(C14940uO.FLEX_END);
                        r14.A06 = str2;
                        bitSet.set(2);
                        r14.A03 = r6;
                        bitSet.set(1);
                        AnonymousClass11F.A0C(7, bitSet, strArr);
                        A064.A06(r14);
                        r4.A00(A064);
                        C33981oS A065 = AnonymousClass1GV.A06();
                        ComponentBuilderCBuilderShape0_0S0300000 A005 = C21641Id.A00(r8);
                        A005.A3F(0);
                        A005.A20(100.0f);
                        A005.A1p(8.0f);
                        A065.A06(A005.A34());
                        r4.A00(A065);
                        return r4.A00;
                    case 3:
                        C20311Bx A006 = C20301Bw.A00();
                        A006.A01(A06(r8, user, migColorScheme, r6));
                        return A006.A00;
                    default:
                        throw new IllegalArgumentException(C99084oO.$const$string(347) + r3);
                }
            }
            return C20301Bw.A00().A00;
        }
    }

    public /* bridge */ /* synthetic */ boolean BEe(Object obj) {
        return A0U((C16070wR) obj);
    }

    public static C16070wR A06(AnonymousClass1GA r8, User user, MigColorScheme migColorScheme, C109065Iq r11) {
        C33981oS A062 = AnonymousClass1GV.A06();
        String[] strArr = {"colorScheme", C99084oO.$const$string(AnonymousClass1Y3.A1B), "user"};
        BitSet bitSet = new BitSet(3);
        AnonymousClass5ID r2 = new AnonymousClass5ID(r8.A09);
        C17540z4 r6 = r8.A0B;
        C17770zR r1 = r8.A04;
        if (r1 != null) {
            r2.A07 = r1.A06;
        }
        bitSet.clear();
        r2.A02 = user;
        bitSet.set(2);
        r2.A01 = migColorScheme;
        bitSet.set(0);
        r2.A00 = r11;
        bitSet.set(1);
        r2.A14().BL8(r6.A00(512.0f));
        AnonymousClass11F.A0C(3, bitSet, strArr);
        A062.A06(r2);
        return A062.A04();
    }
}
