package X;

/* renamed from: X.00R  reason: invalid class name */
public final class AnonymousClass00R extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.base.app.SplashScreenApplication$3";
    public final /* synthetic */ AnonymousClass00A A00;
    public final /* synthetic */ AnonymousClass001 A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass00R(AnonymousClass001 r1, String str, AnonymousClass00A r3) {
        super(str);
        this.A01 = r1;
        this.A00 = r3;
    }

    public void run() {
        AnonymousClass001 r0 = this.A01;
        r0.A07();
        r0.A00.A02();
        this.A00.A05();
    }
}
