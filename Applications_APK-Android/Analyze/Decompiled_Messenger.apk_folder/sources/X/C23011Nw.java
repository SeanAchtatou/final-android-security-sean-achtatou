package X;

import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: X.1Nw  reason: invalid class name and case insensitive filesystem */
public final class C23011Nw {
    private final int A00;
    private final C22861Nc A01;

    public void A00(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = (byte[]) this.A01.get(this.A00);
        while (true) {
            try {
                int read = inputStream.read(bArr, 0, this.A00);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            } finally {
                this.A01.C0t(bArr);
            }
        }
    }

    public C23011Nw(C22861Nc r2, int i) {
        C05520Zg.A04(i > 0);
        this.A00 = i;
        this.A01 = r2;
    }
}
