package X;

import android.content.Context;
import com.facebook.common.file.FileModule;
import io.card.payment.BuildConfig;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0iK  reason: invalid class name and case insensitive filesystem */
public final class C09700iK {
    public static final String A00 = AnonymousClass08S.A0J("i18n", C09700iK.class.getName());
    private static volatile C09700iK A01;

    public static C10560kP A00(C09700iK r5, Context context, ArrayList arrayList) {
        if (arrayList == null || arrayList.isEmpty()) {
            return null;
        }
        ArrayList arrayList2 = new ArrayList();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            arrayList2.add(new C10800ks(r5, context, (C10560kP) arrayList.get(i)));
        }
        return ((C10800ks) Collections.min(arrayList2, new C12110oY(AnonymousClass07B.A01))).A01;
    }

    public static final C09700iK A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C09700iK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        FileModule.A00(r4.getApplicationInjector());
                        A01 = new C09700iK();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static File A02(C09700iK r3, Context context, String str, long j, String str2, Integer num) {
        String A012 = C10560kP.A01(str, j, str2, AnonymousClass07B.A0C);
        if (num != AnonymousClass07B.A00) {
            A012 = AnonymousClass08S.A0P(A012, ".", AnonymousClass15J.A00(num));
        }
        return new File(r3.A04(context), A012);
    }

    public C10560kP A03(C09770if r9) {
        Context context = r9.A02;
        ArrayList arrayList = new ArrayList();
        ArrayList A06 = A06(context);
        int size = A06.size();
        for (int i = 0; i < size; i++) {
            C10560kP r2 = (C10560kP) A06.get(i);
            if (r2.A04() == C09640iE.LANGPACK && r2.A08() == AnonymousClass07B.A00 && r2.A03() == r9.A03.A01() && r2.A02.group(1).equals(r9.A07.toString())) {
                arrayList.add(r2);
            }
        }
        C10560kP A002 = A00(this, context, arrayList);
        if (A002 != null) {
            return A002;
        }
        r9.A03.A01();
        r9.A07.toString();
        return A002;
    }

    public File A04(Context context) {
        File file = new File(context.getFilesDir(), "strings");
        if (file.exists() || file.mkdir()) {
            return file;
        }
        C010708t.A0O(A00, "Cannot create language dir: %s", file.getAbsolutePath());
        throw new RuntimeException("Error creating directory for strings file");
    }

    public File A05(Context context, C10560kP r5) {
        return new File(A04(context), r5.A01);
    }

    public void A07(Context context) {
        boolean z;
        HashMap hashMap = new HashMap();
        ArrayList A06 = A06(context);
        int size = A06.size();
        for (int i = 0; i < size; i++) {
            C10560kP r2 = (C10560kP) A06.get(i);
            if (r2.A04() == C09640iE.LANGPACK) {
                String A09 = AnonymousClass08S.A09(r2.A02.group(1), r2.A03());
                if (!hashMap.containsKey(A09)) {
                    hashMap.put(A09, new ArrayList());
                }
                ((ArrayList) hashMap.get(A09)).add(r2);
            }
        }
        for (ArrayList arrayList : hashMap.values()) {
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            int size2 = arrayList.size();
            for (int i2 = 0; i2 < size2; i2++) {
                C10560kP r1 = (C10560kP) arrayList.get(i2);
                switch (r1.A08().intValue()) {
                    case 0:
                        arrayList2.add(r1);
                        break;
                    case 1:
                        arrayList3.add(r1);
                        break;
                }
            }
            C10560kP A002 = A00(this, context, arrayList3);
            if (A002 != null) {
                arrayList3.remove(A002);
            }
            if (A002 != null) {
                File file = new File(A04(context), A002.A01);
                if (file.exists()) {
                    try {
                        z = C03380Nn.A03(AnonymousClass0q2.A08(file), "MD5").equals(A002.A09());
                    } catch (IOException e) {
                        C010708t.A0V(A00, e, "failed to read language pack %s", file.getName());
                        z = false;
                    }
                    if (!z) {
                        C010708t.A0P(A00, "Deleting invalid language file %s with file size %d", file.getName(), Long.valueOf(file.length()));
                        file.delete();
                    } else {
                        File file2 = new File(A04(context), A002.A01.replaceAll(AnonymousClass08S.A0P("\\.", C10570kQ.A00(AnonymousClass07B.A01), ".*$"), BuildConfig.FLAVOR).replaceAll(AnonymousClass08S.A0P("\\.", C10570kQ.A00(AnonymousClass07B.A0C), ".*$"), BuildConfig.FLAVOR));
                        if (file2.exists()) {
                            arrayList2.add(A002);
                        } else {
                            file.renameTo(file2);
                        }
                        arrayList2.addAll(arrayList3);
                        int size3 = arrayList2.size();
                        for (int i3 = 0; i3 < size3; i3++) {
                            C10560kP r22 = (C10560kP) arrayList2.get(i3);
                            if (!r22.A01.equals(file2.getName())) {
                                A05(context, r22).delete();
                            }
                        }
                    }
                }
            }
        }
    }

    public ArrayList A06(Context context) {
        File A04 = A04(context);
        ArrayList arrayList = new ArrayList();
        File[] listFiles = A04.listFiles();
        if (listFiles != null) {
            for (File name : listFiles) {
                C10560kP r1 = new C10560kP(name.getName());
                if (r1.A02.matches()) {
                    arrayList.add(r1);
                }
            }
        }
        return arrayList;
    }

    private C09700iK() {
    }
}
