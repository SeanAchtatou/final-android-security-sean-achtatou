package X;

import java.util.Arrays;

/* renamed from: X.1IF  reason: invalid class name */
public final class AnonymousClass1IF {
    public static final int A03 = AnonymousClass10G.values().length;
    private static final int A04 = AnonymousClass10G.ALL.mIntValue;
    private static final int A05 = AnonymousClass10G.A04.mIntValue;
    private static final int A06 = AnonymousClass10G.A09.mIntValue;
    public long A00 = -1;
    public float[] A01;
    private boolean A02;

    public float A00(int i) {
        byte b = (byte) ((int) ((this.A00 >> (i << 2)) & 15));
        if (b == 15) {
            return Float.NaN;
        }
        return this.A01[b];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r10 == X.AnonymousClass10G.A03) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float A01(X.AnonymousClass10G r10) {
        /*
            r9 = this;
            X.10G r0 = X.AnonymousClass10G.START
            if (r10 == r0) goto L_0x0009
            X.10G r0 = X.AnonymousClass10G.END
            r8 = 0
            if (r10 != r0) goto L_0x000b
        L_0x0009:
            r8 = 2143289344(0x7fc00000, float:NaN)
        L_0x000b:
            long r1 = r9.A00
            r3 = -1
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x0050
            int r0 = r10.mIntValue
            int r0 = r0 << 2
            long r3 = r1 >> r0
            r6 = 15
            long r3 = r3 & r6
            int r0 = (int) r3
            byte r3 = (byte) r0
            r5 = 15
            if (r3 != r5) goto L_0x004b
            boolean r0 = r9.A02
            if (r0 == 0) goto L_0x0050
            X.10G r0 = X.AnonymousClass10G.TOP
            if (r10 == r0) goto L_0x0048
            X.10G r0 = X.AnonymousClass10G.BOTTOM
            if (r10 == r0) goto L_0x0048
            int r0 = X.AnonymousClass1IF.A05
        L_0x0030:
            int r0 = r0 << 2
            long r3 = r1 >> r0
            long r3 = r3 & r6
            int r0 = (int) r3
            byte r3 = (byte) r0
            if (r3 != r5) goto L_0x004b
            int r0 = X.AnonymousClass1IF.A04
            int r0 = r0 << 2
            long r1 = r1 >> r0
            long r1 = r1 & r6
            int r0 = (int) r1
            byte r1 = (byte) r0
            if (r1 == r5) goto L_0x0050
            float[] r0 = r9.A01
            r0 = r0[r1]
            return r0
        L_0x0048:
            int r0 = X.AnonymousClass1IF.A06
            goto L_0x0030
        L_0x004b:
            float[] r0 = r9.A01
            r0 = r0[r3]
            return r0
        L_0x0050:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IF.A01(X.10G):float");
    }

    public float A02(AnonymousClass10G r5) {
        byte b = (byte) ((int) ((this.A00 >> (r5.mIntValue << 2)) & 15));
        if (b == 15) {
            return Float.NaN;
        }
        return this.A01[b];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], int, int, float):void}
     arg types: [float[], int, int, int]
     candidates:
      ClspMth{java.util.Arrays.fill(java.lang.Object[], int, int, java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int, int, int):void}
      ClspMth{java.util.Arrays.fill(char[], int, int, char):void}
      ClspMth{java.util.Arrays.fill(boolean[], int, int, boolean):void}
      ClspMth{java.util.Arrays.fill(byte[], int, int, byte):void}
      ClspMth{java.util.Arrays.fill(long[], int, int, long):void}
      ClspMth{java.util.Arrays.fill(double[], int, int, double):void}
      ClspMth{java.util.Arrays.fill(short[], int, int, short):void}
      ClspMth{java.util.Arrays.fill(float[], int, int, float):void} */
    public void A03(AnonymousClass10G r11, float f) {
        int i = r11.mIntValue;
        float A002 = A00(i);
        boolean isNaN = Float.isNaN(A002);
        boolean z = true;
        if (isNaN || Float.isNaN(f) ? !isNaN || !Float.isNaN(f) : Math.abs(f - A002) >= 1.0E-5f) {
            z = false;
        }
        boolean z2 = false;
        if (!z) {
            long j = this.A00;
            int i2 = i << 2;
            byte b = (byte) ((int) ((j >> i2) & 15));
            if (AnonymousClass1K2.A00(f)) {
                this.A00 = (15 << i2) | j;
                this.A01[b] = Float.NaN;
            } else if (b == 15) {
                float[] fArr = this.A01;
                byte b2 = 0;
                if (fArr != null) {
                    int i3 = 0;
                    while (true) {
                        int length = fArr.length;
                        if (i3 >= length) {
                            float[] fArr2 = new float[Math.min(length << 1, A03)];
                            this.A01 = fArr2;
                            int length2 = fArr.length;
                            System.arraycopy(fArr, 0, fArr2, 0, length2);
                            float[] fArr3 = this.A01;
                            Arrays.fill(fArr3, length2, fArr3.length, Float.NaN);
                            b2 = (byte) length2;
                            break;
                        } else if (AnonymousClass1K2.A00(fArr[i3])) {
                            b2 = (byte) i3;
                            break;
                        } else {
                            i3++;
                        }
                    }
                } else {
                    this.A01 = new float[]{Float.NaN, Float.NaN};
                }
                if (b2 < A03) {
                    int i4 = i << 2;
                    long j2 = ((15 << i4) ^ -1) & this.A00;
                    this.A00 = j2;
                    this.A00 = j2 | (((long) b2) << i4);
                    this.A01[b2] = f;
                } else {
                    throw new IllegalStateException("The newIndex for the array cannot be bigger than the amount of Yoga Edges.");
                }
            } else {
                this.A01[b] = f;
            }
            if (((((int) (this.A00 >> 24)) ^ -1) & AnonymousClass1Y3.AXq) != 0) {
                z2 = true;
            }
            this.A02 = z2;
        }
    }
}
