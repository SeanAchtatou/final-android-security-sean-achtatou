package X;

import java.io.IOException;
import java.io.OutputStream;

/* renamed from: X.1S3  reason: invalid class name */
public abstract class AnonymousClass1S3 extends OutputStream {
    public int A00() {
        return ((AnonymousClass1S2) this).A00;
    }

    public AnonymousClass1SS A01() {
        return ((AnonymousClass1S2) this).A02();
    }

    public void close() {
        try {
            super.close();
        } catch (IOException e) {
            AnonymousClass95E.A00(e);
        }
    }
}
