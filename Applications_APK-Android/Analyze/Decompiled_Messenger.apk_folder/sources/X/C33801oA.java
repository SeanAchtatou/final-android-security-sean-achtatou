package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Preconditions;

/* renamed from: X.1oA  reason: invalid class name and case insensitive filesystem */
public final class C33801oA {
    private final C04310Tq A00;

    public static final C33801oA A01(AnonymousClass1XY r1) {
        return new C33801oA(r1);
    }

    public C33801oA(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WY.A02(r2);
    }

    public static ThreadKey A00(C33801oA r2, AnonymousClass1T9 r3) {
        String A08;
        Preconditions.checkNotNull(r3);
        C36851ty A07 = r3.A07();
        Preconditions.checkNotNull(A07);
        C36861tz A072 = A07.A07();
        Preconditions.checkNotNull(A072);
        String A06 = A072.A06();
        Preconditions.checkNotNull(A06);
        if (A06.equals(r2.A00.get())) {
            Preconditions.checkNotNull(r3.A09());
            A08 = r3.A09();
        } else {
            Preconditions.checkNotNull(r3.A08());
            A08 = r3.A08();
        }
        return ThreadKey.A01(Long.parseLong(A08));
    }
}
