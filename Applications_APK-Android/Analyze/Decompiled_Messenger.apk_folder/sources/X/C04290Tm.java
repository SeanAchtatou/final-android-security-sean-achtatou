package X;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

/* renamed from: X.0Tm  reason: invalid class name and case insensitive filesystem */
public final class C04290Tm {
    public final WeakHashMap A00;
    private final C24771Xa A01;

    public Object A00(Object obj) {
        Object obj2;
        WeakReference weakReference = (WeakReference) this.A00.get(obj);
        if (weakReference != null && (obj2 = weakReference.get()) != null) {
            return obj2;
        }
        Object BI3 = this.A01.BI3(obj);
        this.A00.put(obj, new WeakReference(BI3));
        return BI3;
    }

    public C04290Tm(C24771Xa r2, int i) {
        this.A01 = r2;
        this.A00 = new WeakHashMap(i);
    }
}
