package X;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.0E1  reason: invalid class name */
public abstract class AnonymousClass0E1 extends Service {
    public static final boolean DEBUG = false;
    public static final String TAG = "JobIntentService";
    public static final HashMap sClassWorkEnqueuer = new HashMap();
    public static final Object sLock = new Object();
    public final ArrayList mCompatQueue;
    public AnonymousClass0E3 mCompatWorkEnqueuer;
    public AnonymousClass0E5 mCurProcessor;
    public boolean mDestroyed = false;
    public boolean mInterruptIfStopped = false;
    public C03000Hm mJobImpl;
    public boolean mStopped = false;

    public abstract void onHandleWork(Intent intent);

    public boolean onStopCurrentWork() {
        return true;
    }

    public static AnonymousClass0E3 getWorkEnqueuer(Context context, ComponentName componentName, boolean z, int i) {
        AnonymousClass0E3 r1 = (AnonymousClass0E3) sClassWorkEnqueuer.get(componentName);
        if (r1 == null) {
            if (Build.VERSION.SDK_INT < 26) {
                r1 = new AnonymousClass0E4(context, componentName);
            } else if (z) {
                r1 = new AnonymousClass0E2(context, componentName, i);
            } else {
                throw new IllegalArgumentException("Can't be here without a job id");
            }
            sClassWorkEnqueuer.put(componentName, r1);
        }
        return r1;
    }

    public AnonymousClass0E7 dequeueWork() {
        C03000Hm r0 = this.mJobImpl;
        if (r0 != null) {
            return r0.AX2();
        }
        synchronized (this.mCompatQueue) {
            if (this.mCompatQueue.size() <= 0) {
                return null;
            }
            AnonymousClass0E7 r02 = (AnonymousClass0E7) this.mCompatQueue.remove(0);
            return r02;
        }
    }

    public boolean doStopCurrentWork() {
        AnonymousClass0E5 r1 = this.mCurProcessor;
        if (r1 != null) {
            r1.cancel(this.mInterruptIfStopped);
        }
        this.mStopped = true;
        return true;
    }

    public void ensureProcessorRunningLocked(boolean z) {
        if (this.mCurProcessor == null) {
            this.mCurProcessor = new AnonymousClass0E5(this);
            AnonymousClass0E3 r0 = this.mCompatWorkEnqueuer;
            if (r0 != null && z) {
                r0.A01();
            }
            this.mCurProcessor.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    public IBinder onBind(Intent intent) {
        C03000Hm r0 = this.mJobImpl;
        if (r0 != null) {
            return r0.ATY();
        }
        return null;
    }

    public void processorFinished() {
        ArrayList arrayList = this.mCompatQueue;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.mCurProcessor = null;
                ArrayList arrayList2 = this.mCompatQueue;
                if (arrayList2 != null && arrayList2.size() > 0) {
                    ensureProcessorRunningLocked(false);
                } else if (!this.mDestroyed) {
                    this.mCompatWorkEnqueuer.A00();
                }
            }
        }
    }

    public AnonymousClass0E1() {
        if (Build.VERSION.SDK_INT >= 26) {
            this.mCompatQueue = null;
        } else {
            this.mCompatQueue = new ArrayList();
        }
    }

    public boolean isStopped() {
        return this.mStopped;
    }

    public void onCreate() {
        int A00 = AnonymousClass02C.A00(this, -1748091824);
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            this.mJobImpl = new C02990Hl(this);
            this.mCompatWorkEnqueuer = null;
        } else {
            this.mJobImpl = null;
            this.mCompatWorkEnqueuer = getWorkEnqueuer(this, new ComponentName(this, getClass()), false, 0);
        }
        AnonymousClass02C.A02(-53271393, A00);
    }

    public void onDestroy() {
        int A04 = C000700l.A04(773526438);
        super.onDestroy();
        ArrayList arrayList = this.mCompatQueue;
        if (arrayList != null) {
            synchronized (arrayList) {
                try {
                    this.mDestroyed = true;
                    this.mCompatWorkEnqueuer.A00();
                } finally {
                    C000700l.A0A(426055373, A04);
                }
            }
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        int A01 = AnonymousClass02C.A01(this, 1529133293);
        if (this.mCompatQueue != null) {
            this.mCompatWorkEnqueuer.A02();
            synchronized (this.mCompatQueue) {
                try {
                    ArrayList arrayList = this.mCompatQueue;
                    if (intent == null) {
                        intent = new Intent();
                    }
                    arrayList.add(new AnonymousClass0E6(this, intent, i2));
                    ensureProcessorRunningLocked(true);
                } catch (Throwable th) {
                    while (true) {
                        AnonymousClass02C.A02(1622148265, A01);
                        throw th;
                    }
                }
            }
            AnonymousClass02C.A02(1991930718, A01);
            return 3;
        }
        AnonymousClass02C.A02(1390013199, A01);
        return 2;
    }

    public void setInterruptIfStopped(boolean z) {
        this.mInterruptIfStopped = z;
    }

    public static void enqueueWork(Context context, ComponentName componentName, int i, Intent intent) {
        if (intent != null) {
            synchronized (sLock) {
                AnonymousClass0E3 workEnqueuer = getWorkEnqueuer(context, componentName, true, i);
                workEnqueuer.A03(i);
                workEnqueuer.A04(intent);
            }
            return;
        }
        throw new IllegalArgumentException("work must not be null");
    }

    public static void enqueueWork(Context context, Class cls, int i, Intent intent) {
        enqueueWork(context, new ComponentName(context, cls), i, intent);
    }
}
