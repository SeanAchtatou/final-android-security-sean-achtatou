package X;

import com.facebook.common.dextricks.DalvikInternals;

/* renamed from: X.1OJ  reason: invalid class name */
public final class AnonymousClass1OJ implements AnonymousClass1O4 {
    public static final int A01 = A03.length;
    public static final byte[] A02;
    public static final byte[] A03 = {73, 73, 42, 0};
    public static final byte[] A04 = {77, 77, 0, 42};
    public static final byte[] A05 = AnonymousClass1O5.A01("GIF87a");
    public static final byte[] A06 = AnonymousClass1O5.A01("GIF89a");
    public static final byte[] A07 = AnonymousClass1O5.A01("ftyp");
    public static final byte[] A08 = {0, 0, 1, 0};
    public static final byte[] A09 = {-1, -40, -1};
    public static final byte[] A0A = {-119, 80, 78, 71, DalvikInternals.IOPRIO_CLASS_SHIFT, 10, 26, 10};
    public static final byte[][] A0B = {AnonymousClass1O5.A01("heic"), AnonymousClass1O5.A01("heix"), AnonymousClass1O5.A01("hevc"), AnonymousClass1O5.A01("hevx"), AnonymousClass1O5.A01("mif1"), AnonymousClass1O5.A01("msf1")};
    private static final int A0C;
    private static final int A0D = 4;
    private static final int A0E = 3;
    private static final int A0F = 8;
    public final int A00;

    static {
        byte[] A012 = AnonymousClass1O5.A01("BM");
        A02 = A012;
        A0C = A012.length;
    }

    public int Aoc() {
        return this.A00;
    }

    public AnonymousClass1OJ() {
        int[] iArr = {21, 20, A0E, A0F, 6, A0C, A0D, 12};
        C05520Zg.A04(8 > 0);
        int i = iArr[0];
        for (int i2 = 1; i2 < 8; i2++) {
            int i3 = iArr[i2];
            if (i3 > i) {
                i = i3;
            }
        }
        this.A00 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0035, code lost:
        if (X.AnonymousClass1Q4.A02(r7, 12, X.AnonymousClass1Q4.A05) == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0085, code lost:
        if (X.AnonymousClass1O5.A00(r7, r1, 0) == false) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0098, code lost:
        if (X.AnonymousClass1O5.A00(r7, r1, 0) == false) goto L_0x009a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x010b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass1O3 AXH(byte[] r7, int r8) {
        /*
            r6 = this;
            X.C05520Zg.A02(r7)
            r0 = 0
            boolean r0 = X.AnonymousClass1Q4.A01(r7, r0, r8)
            if (r0 == 0) goto L_0x007a
            r0 = 0
            boolean r0 = X.AnonymousClass1Q4.A01(r7, r0, r8)
            X.C05520Zg.A04(r0)
            r1 = 12
            byte[] r0 = X.AnonymousClass1Q4.A06
            boolean r0 = X.AnonymousClass1Q4.A02(r7, r1, r0)
            if (r0 == 0) goto L_0x001f
            X.1O3 r0 = X.AnonymousClass1SI.A0C
            return r0
        L_0x001f:
            byte[] r0 = X.AnonymousClass1Q4.A04
            boolean r0 = X.AnonymousClass1Q4.A02(r7, r1, r0)
            if (r0 == 0) goto L_0x002a
            X.1O3 r0 = X.AnonymousClass1SI.A0B
            return r0
        L_0x002a:
            r0 = 21
            if (r8 < r0) goto L_0x0037
            byte[] r0 = X.AnonymousClass1Q4.A05
            boolean r1 = X.AnonymousClass1Q4.A02(r7, r1, r0)
            r0 = 1
            if (r1 != 0) goto L_0x0038
        L_0x0037:
            r0 = 0
        L_0x0038:
            if (r0 == 0) goto L_0x0077
            r1 = 12
            byte[] r0 = X.AnonymousClass1Q4.A05
            boolean r5 = X.AnonymousClass1Q4.A02(r7, r1, r0)
            r0 = 20
            byte r4 = r7[r0]
            r3 = 2
            r2 = r4 & r3
            r1 = 1
            r0 = 0
            if (r2 != r3) goto L_0x004e
            r0 = 1
        L_0x004e:
            if (r5 == 0) goto L_0x0057
            if (r0 == 0) goto L_0x0057
        L_0x0052:
            if (r1 == 0) goto L_0x0059
            X.1O3 r0 = X.AnonymousClass1SI.A08
            return r0
        L_0x0057:
            r1 = 0
            goto L_0x0052
        L_0x0059:
            r1 = 12
            byte[] r0 = X.AnonymousClass1Q4.A05
            boolean r3 = X.AnonymousClass1Q4.A02(r7, r1, r0)
            r2 = 16
            r4 = r4 & r2
            r1 = 1
            r0 = 0
            if (r4 != r2) goto L_0x0069
            r0 = 1
        L_0x0069:
            if (r3 == 0) goto L_0x0072
            if (r0 == 0) goto L_0x0072
        L_0x006d:
            if (r1 == 0) goto L_0x0074
            X.1O3 r0 = X.AnonymousClass1SI.A0A
            return r0
        L_0x0072:
            r1 = 0
            goto L_0x006d
        L_0x0074:
            X.1O3 r0 = X.AnonymousClass1SI.A09
            return r0
        L_0x0077:
            X.1O3 r0 = X.AnonymousClass1O3.A02
            return r0
        L_0x007a:
            byte[] r1 = X.AnonymousClass1OJ.A09
            int r0 = r1.length
            if (r8 < r0) goto L_0x0087
            r0 = 0
            boolean r1 = X.AnonymousClass1O5.A00(r7, r1, r0)
            r0 = 1
            if (r1 != 0) goto L_0x0088
        L_0x0087:
            r0 = 0
        L_0x0088:
            if (r0 == 0) goto L_0x008d
            X.1O3 r0 = X.AnonymousClass1SI.A06
            return r0
        L_0x008d:
            byte[] r1 = X.AnonymousClass1OJ.A0A
            int r0 = r1.length
            if (r8 < r0) goto L_0x009a
            r0 = 0
            boolean r1 = X.AnonymousClass1O5.A00(r7, r1, r0)
            r0 = 1
            if (r1 != 0) goto L_0x009b
        L_0x009a:
            r0 = 0
        L_0x009b:
            if (r0 == 0) goto L_0x00a0
            X.1O3 r0 = X.AnonymousClass1SI.A07
            return r0
        L_0x00a0:
            r2 = 0
            r0 = 6
            if (r8 < r0) goto L_0x00b7
            byte[] r1 = X.AnonymousClass1OJ.A05
            r0 = 0
            boolean r0 = X.AnonymousClass1O5.A00(r7, r1, r0)
            if (r0 != 0) goto L_0x00b6
            byte[] r1 = X.AnonymousClass1OJ.A06
            r0 = 0
            boolean r0 = X.AnonymousClass1O5.A00(r7, r1, r0)
            if (r0 == 0) goto L_0x00b7
        L_0x00b6:
            r2 = 1
        L_0x00b7:
            if (r2 == 0) goto L_0x00bc
            X.1O3 r0 = X.AnonymousClass1SI.A03
            return r0
        L_0x00bc:
            byte[] r1 = X.AnonymousClass1OJ.A02
            int r0 = r1.length
            if (r8 >= r0) goto L_0x00c7
            r0 = 0
        L_0x00c2:
            if (r0 == 0) goto L_0x00cd
            X.1O3 r0 = X.AnonymousClass1SI.A01
            return r0
        L_0x00c7:
            r0 = 0
            boolean r0 = X.AnonymousClass1O5.A00(r7, r1, r0)
            goto L_0x00c2
        L_0x00cd:
            byte[] r1 = X.AnonymousClass1OJ.A08
            int r0 = r1.length
            if (r8 >= r0) goto L_0x00d8
            r0 = 0
        L_0x00d3:
            if (r0 == 0) goto L_0x00de
            X.1O3 r0 = X.AnonymousClass1SI.A05
            return r0
        L_0x00d8:
            r0 = 0
            boolean r0 = X.AnonymousClass1O5.A00(r7, r1, r0)
            goto L_0x00d3
        L_0x00de:
            r0 = 12
            if (r8 < r0) goto L_0x0109
            r0 = 3
            byte r0 = r7[r0]
            r4 = 8
            if (r0 < r4) goto L_0x0109
            byte[] r1 = X.AnonymousClass1OJ.A07
            r0 = 4
            boolean r0 = X.AnonymousClass1O5.A00(r7, r1, r0)
            if (r0 == 0) goto L_0x0109
            byte[][] r3 = X.AnonymousClass1OJ.A0B
            int r2 = r3.length
            r1 = 0
        L_0x00f6:
            if (r1 >= r2) goto L_0x0109
            r0 = r3[r1]
            boolean r0 = X.AnonymousClass1O5.A00(r7, r0, r4)
            if (r0 == 0) goto L_0x0106
            r0 = 1
        L_0x0101:
            if (r0 == 0) goto L_0x010b
            X.1O3 r0 = X.AnonymousClass1SI.A04
            return r0
        L_0x0106:
            int r1 = r1 + 1
            goto L_0x00f6
        L_0x0109:
            r0 = 0
            goto L_0x0101
        L_0x010b:
            int r0 = X.AnonymousClass1OJ.A01
            if (r8 < r0) goto L_0x0127
            byte[] r1 = X.AnonymousClass1OJ.A03
            r0 = 0
            boolean r0 = X.AnonymousClass1O5.A00(r7, r1, r0)
            if (r0 != 0) goto L_0x0121
            byte[] r1 = X.AnonymousClass1OJ.A04
            r0 = 0
            boolean r0 = X.AnonymousClass1O5.A00(r7, r1, r0)
            if (r0 == 0) goto L_0x0127
        L_0x0121:
            r0 = 1
        L_0x0122:
            if (r0 == 0) goto L_0x0129
            X.1O3 r0 = X.AnonymousClass1SI.A02
            return r0
        L_0x0127:
            r0 = 0
            goto L_0x0122
        L_0x0129:
            X.1O3 r0 = X.AnonymousClass1O3.A02
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OJ.AXH(byte[], int):X.1O3");
    }
}
