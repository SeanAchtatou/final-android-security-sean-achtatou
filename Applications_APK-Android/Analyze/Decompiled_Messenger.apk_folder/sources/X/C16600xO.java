package X;

import java.util.Comparator;

/* renamed from: X.0xO  reason: invalid class name and case insensitive filesystem */
public final class C16600xO implements Comparator {
    public final /* synthetic */ C16580xM A00;

    public C16600xO(C16580xM r1) {
        this.A00 = r1;
    }

    public int compare(Object obj, Object obj2) {
        return this.A00.A03.AeD(((C32511ls) obj2).getClass()) - this.A00.A03.AeD(((C32511ls) obj).getClass());
    }
}
