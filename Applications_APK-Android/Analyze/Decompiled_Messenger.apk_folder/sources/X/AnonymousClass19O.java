package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.19O  reason: invalid class name */
public final class AnonymousClass19O extends AnonymousClass0W4 {
    public static final ImmutableList A00 = ImmutableList.of(AnonymousClass19Q.A03, AnonymousClass19Q.A00, AnonymousClass19Q.A01, AnonymousClass19Q.A02);
    private static final C06060am A01 = new C06050al(ImmutableList.of(AnonymousClass19Q.A03));

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public AnonymousClass19O() {
        super("identity_keys", A00, A01);
    }
}
