package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.util.StateSet;
import java.util.Arrays;

/* renamed from: X.1M2  reason: invalid class name */
public final class AnonymousClass1M2 {
    public static final int[] A00 = {-16842910};
    public static final int[] A01;
    public static final int[] A02 = StateSet.WILD_CARD;
    public static final int[][] A03;
    private static final Rect A04 = new Rect(0, 0, 0, 0);
    private static final RectF A05 = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
    private static final int[] A06 = {16842919};

    static {
        int[] iArr = {16842910};
        A01 = iArr;
        A03 = new int[][]{iArr};
    }

    public static Drawable A00(float f, int i, int i2) {
        if (Build.VERSION.SDK_INT >= 21) {
            return A07(null, i2, null);
        }
        return A08(f, i, i2, i);
    }

    public static Drawable A02(float f, int i, int i2, int i3) {
        if (Build.VERSION.SDK_INT < 21) {
            return A08(f, i, i2, i3);
        }
        C14960uQ r3 = new C14960uQ();
        Drawable A052 = A05(i, f);
        r3.A01(A00, Integer.valueOf(i3), A052);
        int[] iArr = A01;
        Integer valueOf = Integer.valueOf(i);
        r3.A01(iArr, valueOf, A052);
        r3.A01(A02, valueOf, A052);
        return A07(r3, i2, A05(-1, f));
    }

    public static Drawable A03(float f, int i, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 21) {
            return A07(A05(i, f), i3, A05(-1, f));
        }
        return A08(f, i, i2, i);
    }

    public static Drawable A05(int i, float f) {
        float[] fArr = new float[8];
        Arrays.fill(fArr, f);
        return A06(i, fArr);
    }

    public static Drawable A06(int i, float[] fArr) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(fArr, A05, null));
        shapeDrawable.setPadding(A04);
        shapeDrawable.mutate();
        shapeDrawable.setColorFilter(i, PorterDuff.Mode.SRC_IN);
        return shapeDrawable;
    }

    public static Drawable A07(Drawable drawable, int i, Drawable drawable2) {
        return new RippleDrawable(new ColorStateList(A03, new int[]{i}), drawable, drawable2);
    }

    private static StateListDrawable A08(float f, int i, int i2, int i3) {
        C14960uQ r3 = new C14960uQ();
        Drawable A052 = A05(i, f);
        r3.A01(A00, Integer.valueOf(i3), A052);
        r3.A01(A06, Integer.valueOf(i2), A052);
        int[] iArr = A01;
        Integer valueOf = Integer.valueOf(i);
        r3.A01(iArr, valueOf, A052);
        r3.A01(A02, valueOf, A052);
        return r3;
    }

    public static Drawable A01(float f, int i, int i2) {
        return A03(f, i, i2, i2);
    }

    public static Drawable A04(float f, ColorStateList colorStateList) {
        return A01(f, AnonymousClass1KA.A00(colorStateList), AnonymousClass1KA.A01(colorStateList));
    }
}
