package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.inspiration.model.movableoverlay.music.InspirationMusicStickerInfo;

/* renamed from: X.20k  reason: invalid class name and case insensitive filesystem */
public final class C401320k implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InspirationMusicStickerInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new InspirationMusicStickerInfo[i];
    }
}
