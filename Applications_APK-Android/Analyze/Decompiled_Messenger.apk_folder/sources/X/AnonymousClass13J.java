package X;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.os.Handler;
import com.facebook.messaging.model.messages.Message;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.13J  reason: invalid class name */
public final class AnonymousClass13J {
    private static volatile AnonymousClass13J A0C;
    public ContentObserver A00;
    public AnonymousClass0UN A01;
    public AnonymousClass0ZM A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public final ContentResolver A07;
    public final Handler A08;
    public final FbSharedPreferences A09;
    public final Object A0A = new Object();
    private final C10960l9 A0B;

    public static final AnonymousClass13J A00(AnonymousClass1XY r4) {
        if (A0C == null) {
            synchronized (AnonymousClass13J.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r4);
                if (A002 != null) {
                    try {
                        A0C = new AnonymousClass13J(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    public static boolean A01(AnonymousClass13J r4) {
        if (!((C185814g) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AT9, r4.A01)).A02()) {
            return false;
        }
        if (r4.A0B.A0A()) {
            return true;
        }
        if (!r4.A0B.A08() || !((C25051Yd) AnonymousClass1XX.A03(AnonymousClass1Y3.AOJ, r4.A01)).Aem(285812893685641L)) {
            return false;
        }
        return true;
    }

    private AnonymousClass13J(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(3, r3);
        this.A08 = AnonymousClass0UX.A01(r3);
        this.A07 = C04490Ux.A0E(r3);
        this.A09 = FbSharedPreferencesModule.A00(r3);
        this.A0B = C10960l9.A00(r3);
    }

    public void A02(Message message) {
        String str;
        long parseLong;
        if (A01(this)) {
            AnonymousClass9OR r5 = (AnonymousClass9OR) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ac9, this.A01);
            synchronized (r5) {
                if (message.A0y.equals("sms") && (str = message.A0q) != null) {
                    if (str.startsWith("smsid:")) {
                        parseLong = Long.parseLong(str.substring(6));
                    } else if (str.startsWith("mmsid:")) {
                        parseLong = Long.parseLong(str.substring(6));
                    } else {
                        throw new IllegalArgumentException(AnonymousClass08S.A0J("Not sms/mms message id: ", str));
                    }
                    String str2 = message.A0q;
                    if (str2.startsWith("smsid:")) {
                        long j = r5.A01;
                        if (j != -1) {
                            r5.A01 = Math.max(j, parseLong);
                        }
                    }
                    if (str2.startsWith("mmsid:")) {
                        long j2 = r5.A00;
                        if (j2 != -1) {
                            r5.A00 = Math.max(j2, parseLong);
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (r0 != false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(boolean r5) {
        /*
            r4 = this;
            boolean r0 = A01(r4)
            if (r0 == 0) goto L_0x0043
            r3 = 0
            if (r5 != 0) goto L_0x0025
            r2 = 2
            int r1 = X.AnonymousClass1Y3.Akw
            X.0UN r0 = r4.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1El r0 = (X.C20951El) r0
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x0044
            java.lang.Integer r2 = X.AnonymousClass07B.A01
        L_0x001c:
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            r0 = 0
            if (r2 != r1) goto L_0x0022
            r0 = 1
        L_0x0022:
            r1 = 0
            if (r0 == 0) goto L_0x0026
        L_0x0025:
            r1 = 1
        L_0x0026:
            boolean r0 = r4.A05
            if (r0 == r1) goto L_0x0043
            r0 = r0 ^ 1
            r4.A05 = r0
            if (r0 == 0) goto L_0x0043
            boolean r0 = r4.A04
            if (r0 == 0) goto L_0x0043
            r4.A04 = r3
            android.os.Handler r2 = r4.A08
            X.9OV r1 = new X.9OV
            r1.<init>(r4)
            r0 = 105149256(0x6447348, float:3.6948198E-35)
            X.AnonymousClass00S.A04(r2, r1, r0)
        L_0x0043:
            return
        L_0x0044:
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13J.A03(boolean):void");
    }
}
