package X;

import android.app.Service;
import android.content.Intent;
import com.facebook.common.stringformat.StringFormatUtil;
import java.util.Locale;

/* renamed from: X.0lP  reason: invalid class name */
public abstract class AnonymousClass0lP extends Service {
    public C006606m A00 = C006606m.A00;
    private boolean A01 = false;
    private boolean A02 = false;
    private boolean A03 = false;
    public final AnonymousClass04e A04 = new AnonymousClass1XR();

    public void A0i(String str, Object... objArr) {
        if (!(this instanceof AnonymousClass0lO)) {
            this.A04.C2T("SecureService", String.format(Locale.US, str, objArr), new Throwable());
        } else {
            ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, ((AnonymousClass0lO) this).A00)).CGZ("FbService", StringFormatUtil.formatStrLocaleSafe(str, objArr), new Throwable());
        }
    }

    public int A0f(Intent intent, int i, int i2) {
        return super.onStartCommand(intent, i, i2);
    }

    public final void onCreate() {
        int A002 = AnonymousClass02C.A00(this, -275473941);
        if (this.A01) {
            A0i("Class %s called onCreate twice. This may be due to calling super.onCreate instead of super.onFbCreate", getClass().getName());
            super.onCreate();
            AnonymousClass02C.A02(467267907, A002);
            return;
        }
        try {
            this.A01 = true;
            A0g();
        } finally {
            this.A01 = false;
            AnonymousClass02C.A02(681544989, A002);
        }
    }

    public final void onDestroy() {
        int A042 = C000700l.A04(-507926049);
        if (this.A02) {
            A0i("Class %s called onDestroy twice. This may be due to calling super.onDestroy instead of super.onFbDestroy", getClass().getName());
            super.onDestroy();
            C000700l.A0A(-1000071399, A042);
            return;
        }
        try {
            this.A02 = true;
            A0h();
        } finally {
            this.A02 = false;
            C000700l.A0A(-837348667, A042);
        }
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        boolean A002;
        int A012 = AnonymousClass02C.A01(this, -615596155);
        if (this.A03) {
            A0i("Class %s called onStartCommand twice. This may be due to calling super.onStartCommand instead of super.onFbStartCommand", getClass().getName());
            int onStartCommand = super.onStartCommand(intent, i, i2);
            AnonymousClass02C.A02(-278022792, A012);
            return onStartCommand;
        } else if (!C006506l.A01().A02(this, this, intent)) {
            int onStartCommand2 = super.onStartCommand(intent, i, i2);
            AnonymousClass02C.A02(2052582066, A012);
            return onStartCommand2;
        } else {
            synchronized (this) {
                A002 = this.A00.A00(this, this, intent, this.A04);
            }
            if (!A002) {
                int onStartCommand3 = super.onStartCommand(intent, i, i2);
                AnonymousClass02C.A02(-1265383984, A012);
                return onStartCommand3;
            }
            try {
                this.A03 = true;
                return A0f(intent, i, i2);
            } finally {
                this.A03 = false;
                AnonymousClass02C.A02(1499818339, A012);
            }
        }
    }

    public void A0g() {
        super.onCreate();
    }

    public void A0h() {
        super.onDestroy();
    }
}
