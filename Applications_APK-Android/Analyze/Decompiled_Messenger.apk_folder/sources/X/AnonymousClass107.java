package X;

import com.facebook.graphservice.nativeutil.NativeList;
import com.facebook.graphservice.nativeutil.NativeMap;
import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Map;

/* renamed from: X.107  reason: invalid class name */
public final class AnonymousClass107 {
    public static Object A00(Object obj) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Float) || (obj instanceof Long) || (obj instanceof Byte) || (obj instanceof Short)) {
            return Double.valueOf(((Number) obj).doubleValue());
        }
        if (obj instanceof Map) {
            return new NativeMap((Map) obj);
        }
        if (obj.getClass().isArray()) {
            return new NativeList(ImmutableList.of((Object[]) obj));
        }
        if (obj instanceof List) {
            return new NativeList((List) obj);
        }
        return obj;
    }
}
