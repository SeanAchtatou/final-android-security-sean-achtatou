package X;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;

/* renamed from: X.1zC  reason: invalid class name and case insensitive filesystem */
public final class C39531zC {
    public double A00 = -1.0d;
    public double A01 = -1.0d;
    public double A02 = -1.0d;
    public LatLng A03 = null;

    public CameraPosition A00() {
        return new CameraPosition(this.A03, this.A02, this.A01, this.A00);
    }

    public void A01(double d) {
        while (d >= 360.0d) {
            d -= 360.0d;
        }
        while (d < 0.0d) {
            d += 360.0d;
        }
        this.A00 = d;
    }

    public C39531zC() {
    }

    public C39531zC(C28191DqJ dqJ) {
        if (dqJ != null) {
            this.A00 = dqJ.A00;
            this.A03 = dqJ.A03;
            this.A01 = dqJ.A01;
            this.A02 = dqJ.A02;
        }
    }

    public C39531zC(CameraPosition cameraPosition) {
        if (cameraPosition != null) {
            this.A00 = cameraPosition.bearing;
            this.A03 = cameraPosition.target;
            this.A01 = cameraPosition.tilt;
            this.A02 = cameraPosition.zoom;
        }
    }
}
