package X;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import com.facebook.common.asyncview.SpriteView;

/* renamed from: X.01S  reason: invalid class name */
public final class AnonymousClass01S extends SpriteView {
    public static final Keyframe[] A01 = {Keyframe.ofFloat(0.0f, 1.0f), Keyframe.ofFloat(0.067f, 1.15f), Keyframe.ofFloat(0.14f, 1.08f), Keyframe.ofFloat(0.2167f, 1.1f), Keyframe.ofFloat(0.291f, 1.09f), Keyframe.ofFloat(0.383f, 1.1f), Keyframe.ofFloat(0.5f, 1.095f), Keyframe.ofFloat(0.99f, 1.0f)};
    private Bitmap A00;

    public void onMeasure(int i, int i2) {
        setMeasuredDimension((int) (((float) this.A00.getWidth()) * 1.5f), (int) (((float) this.A00.getHeight()) * 1.5f));
    }

    public AnonymousClass01S(Context context, Bitmap bitmap) {
        super(context);
        this.A00 = bitmap;
    }

    public void A0L() {
        super.A0L();
        Bitmap bitmap = this.A00;
        SpriteView.Sprite sprite = new SpriteView.Sprite(bitmap, (float) bitmap.getWidth(), (float) this.A00.getHeight());
        A0R(sprite);
        Keyframe[] keyframeArr = A01;
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(sprite, PropertyValuesHolder.ofKeyframe("scaleX", keyframeArr), PropertyValuesHolder.ofKeyframe("scaleY", keyframeArr));
        ofPropertyValuesHolder.setDuration(2000L);
        ofPropertyValuesHolder.setRepeatCount(-1);
        ofPropertyValuesHolder.setRepeatMode(1);
        ofPropertyValuesHolder.start();
    }
}
