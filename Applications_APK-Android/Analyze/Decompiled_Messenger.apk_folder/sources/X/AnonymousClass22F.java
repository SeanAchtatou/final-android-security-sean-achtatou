package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import java.util.BitSet;
import java.util.List;

/* renamed from: X.22F  reason: invalid class name */
public final class AnonymousClass22F extends C16070wR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C15480vM A01;
    @Comparable(type = 13)
    public C131096By A02;
    @Comparable(type = 14)
    public AnonymousClass5CD A03 = new AnonymousClass5CD();
    @Comparable(type = 13)
    public MigColorScheme A04;
    @Comparable(type = 13)
    public C07410dQ A05;
    @Comparable(type = 13)
    public String A06;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1.equals(r5.A04) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0U(X.C16070wR r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0080
            r2 = 0
            if (r5 == 0) goto L_0x001e
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x001e
            X.22F r5 = (X.AnonymousClass22F) r5
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r4.A04
            if (r1 == 0) goto L_0x001f
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r5.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0024
        L_0x001e:
            return r2
        L_0x001f:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r5.A04
            if (r0 == 0) goto L_0x0024
            return r2
        L_0x0024:
            X.6By r1 = r4.A02
            if (r1 == 0) goto L_0x0031
            X.6By r0 = r5.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0036
            return r2
        L_0x0031:
            X.6By r0 = r5.A02
            if (r0 == 0) goto L_0x0036
            return r2
        L_0x0036:
            X.0vM r1 = r4.A01
            if (r1 == 0) goto L_0x0043
            X.0vM r0 = r5.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0048
            return r2
        L_0x0043:
            X.0vM r0 = r5.A01
            if (r0 == 0) goto L_0x0048
            return r2
        L_0x0048:
            X.0dQ r1 = r4.A05
            if (r1 == 0) goto L_0x0055
            X.0dQ r0 = r5.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x005a
            return r2
        L_0x0055:
            X.0dQ r0 = r5.A05
            if (r0 == 0) goto L_0x005a
            return r2
        L_0x005a:
            java.lang.String r1 = r4.A06
            if (r1 == 0) goto L_0x0067
            java.lang.String r0 = r5.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x006c
            return r2
        L_0x0067:
            java.lang.String r0 = r5.A06
            if (r0 == 0) goto L_0x006c
            return r2
        L_0x006c:
            X.5CD r0 = r4.A03
            java.lang.Boolean r1 = r0.isInitialRequest
            X.5CD r0 = r5.A03
            java.lang.Boolean r0 = r0.isInitialRequest
            if (r1 == 0) goto L_0x007d
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0080
            return r2
        L_0x007d:
            if (r0 == 0) goto L_0x0080
            return r2
        L_0x0080:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass22F.A0U(X.0wR):boolean");
    }

    public AnonymousClass22F(Context context) {
        super("MessageSearchThreadListSection");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }

    public void A0M(AnonymousClass11I r2, AnonymousClass11I r3) {
        ((AnonymousClass5CD) r3).isInitialRequest = ((AnonymousClass5CD) r2).isInitialRequest;
    }

    public void A0N(AnonymousClass1GA r4) {
        C23871Rg r2 = new C23871Rg();
        r2.A00(true);
        this.A03.isInitialRequest = (Boolean) r2.A00;
    }

    public C20301Bw A0R(AnonymousClass1GA r11) {
        String str = this.A06;
        C131096By r3 = this.A02;
        MigColorScheme migColorScheme = this.A04;
        C83713y4 r1 = (C83713y4) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A1b, this.A00);
        if (C06850cB.A0B(str)) {
            return C20301Bw.A00().A00;
        }
        AnonymousClass6D7 r9 = new AnonymousClass6D7(r1, str, r3);
        C20311Bx A002 = C20301Bw.A00();
        String[] strArr = {C99084oO.$const$string(442), C99084oO.$const$string(553), C99084oO.$const$string(702), "sameItemEventHandler"};
        BitSet bitSet = new BitSet(4);
        C1290162w r2 = new C1290162w(r11.A09);
        bitSet.clear();
        r2.A03 = r9;
        bitSet.set(0);
        r2.A0A = true;
        r2.A09 = AnonymousClass08S.A0L("MessageSearchThreadListSection", str, migColorScheme.hashCode());
        bitSet.set(1);
        r2.A02 = 0;
        r2.A00 = 10;
        r2.A01 = 7;
        AnonymousClass5JB r7 = new AnonymousClass5JB();
        C17770zR r12 = r11.A04;
        if (r12 != null) {
            r7.A07 = r12.A06;
        }
        r2.A05 = r7.A16();
        r2.A06 = C16080wS.A02(r11, -1694024595, new Object[]{r11});
        bitSet.set(2);
        r2.A07 = C16080wS.A02(r11, 947264300, new Object[]{r11});
        bitSet.set(3);
        r2.A01 = C16080wS.A02(r11, -939748803, new Object[]{r11});
        List list = A002.A00.A00;
        AnonymousClass1CY.A00(4, bitSet, strArr);
        list.add(r2);
        return A002.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0037, code lost:
        if (r2 != r1) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object AXa(X.AnonymousClass10N r22, java.lang.Object r23) {
        /*
            r21 = this;
            r4 = r23
            r3 = r22
            int r1 = r3.A01
            r0 = -1694024595(0xffffffff9b073c6d, float:-1.1186463E-22)
            r2 = 0
            if (r1 == r0) goto L_0x0095
            r0 = -939748803(0xffffffffc7fc923d, float:-129316.48)
            r10 = 0
            if (r1 == r0) goto L_0x003f
            r0 = 947264300(0x38761b2c, float:5.8676276E-5)
            if (r1 == r0) goto L_0x0018
            return r10
        L_0x0018:
            X.1Jg r4 = (X.C21931Jg) r4
            java.lang.Object r1 = r4.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            java.lang.Object r0 = r4.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r1.A1x()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A1x()
            if (r1 == 0) goto L_0x0039
            if (r0 == 0) goto L_0x0039
            int r2 = r1.hashCode()
            int r1 = r0.hashCode()
            r0 = 1
            if (r2 == r1) goto L_0x003a
        L_0x0039:
            r0 = 0
        L_0x003a:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        L_0x003f:
            X.64k r4 = (X.C1293464k) r4
            X.0zV r1 = r3.A00
            java.lang.Object[] r0 = r3.A02
            r3 = r0[r2]
            X.1GA r3 = (X.AnonymousClass1GA) r3
            boolean r6 = r4.A02
            java.lang.Integer r5 = r4.A00
            java.lang.Throwable r4 = r4.A01
            X.22F r1 = (X.AnonymousClass22F) r1
            java.lang.String r9 = r1.A06
            X.6By r8 = r1.A02
            X.5CD r0 = r1.A03
            java.lang.Boolean r7 = r0.isInitialRequest
            int r0 = r5.intValue()
            r1 = 1
            switch(r0) {
                case 1: goto L_0x0065;
                case 2: goto L_0x0061;
                case 3: goto L_0x008c;
                default: goto L_0x0061;
            }
        L_0x0061:
            X.C16080wS.A05(r3, r6, r5, r4)
            return r10
        L_0x0065:
            boolean r0 = r7.booleanValue()
            if (r0 == 0) goto L_0x0088
            r8.A04(r9, r1)
            r1 = 0
            X.0wR r0 = r3.A0K()
            if (r0 == 0) goto L_0x0061
            X.2yh r2 = new X.2yh
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            r2.<init>(r1, r0)
            java.lang.String r0 = "updateState:MessageSearchThreadListSection.updateIsInitialRequest"
            r3.A0G(r2, r0)
            goto L_0x0061
        L_0x0088:
            r8.A04(r9, r2)
            goto L_0x0061
        L_0x008c:
            r1 = -1
            boolean r0 = r7.booleanValue()
            r8.A02(r9, r1, r2, r0)
            goto L_0x0061
        L_0x0095:
            X.1IX r4 = (X.AnonymousClass1IX) r4
            X.0zV r1 = r3.A00
            java.lang.Object[] r0 = r3.A02
            r5 = r0[r2]
            X.1GA r5 = (X.AnonymousClass1GA) r5
            java.lang.Object r9 = r4.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r9
            int r7 = r4.A00
            X.22F r1 = (X.AnonymousClass22F) r1
            java.lang.String r6 = r1.A06
            X.6By r4 = r1.A02
            X.0vM r3 = r1.A01
            X.0dQ r2 = r1.A05
            int r8 = X.AnonymousClass1Y3.AWZ
            r0 = r21
            X.0UN r1 = r0.A00
            r0 = 2
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r0, r8, r1)
            X.78t r12 = (X.C1536978t) r12
            java.lang.String r0 = "MessageSearchComponentHelper is null!"
            com.google.common.base.Preconditions.checkNotNull(r4, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r11 = r9.A1x()
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r8 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -1205067842(0xffffffffb82c1fbe, float:-4.1037558E-5)
            r0 = 1771146212(0x69918be4, float:2.1994357E25)
            com.facebook.graphservice.tree.TreeJNI r0 = r9.A0J(r1, r8, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            r10 = r0
            if (r11 == 0) goto L_0x0246
            if (r0 == 0) goto L_0x0246
            r0 = 1934276778(0x734ab8aa, float:1.606124E31)
            int r9 = r10.getIntValue(r0)
            r0 = 1
            if (r9 != r0) goto L_0x0240
            r1 = -1636527011(0xffffffff9e74945d, float:-1.2947933E-20)
            r0 = 1310242805(0x4e18b7f5, float:6.4054816E8)
            com.facebook.graphservice.tree.TreeJNI r10 = r10.A0J(r1, r8, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r10 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r10
            if (r10 == 0) goto L_0x0240
            r1 = 954925063(0x38eb0007, float:1.1205678E-4)
            r0 = 1812652419(0x6c0ae183, float:6.7158685E26)
            com.facebook.graphservice.tree.TreeJNI r1 = r10.A0J(r1, r8, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r9 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r8 = -494781692(0xffffffffe2823b04, float:-1.2011646E21)
            r0 = 166477432(0x9ec3e78, float:5.687368E-33)
            com.facebook.graphservice.tree.TreeJNI r10 = r10.A0J(r8, r9, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r10 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r10
            r13 = 0
            if (r1 == 0) goto L_0x0146
            if (r10 == 0) goto L_0x0146
            com.facebook.messaging.model.threads.ThreadSummary r14 = X.C1536978t.A01(r12, r11)
            X.739 r15 = X.C1536978t.A02(r12, r11)
            if (r14 == 0) goto L_0x0146
            if (r15 == 0) goto L_0x0146
            X.6Cv r13 = new X.6Cv
            java.lang.String r16 = r1.A3w()
            r8 = 1443265168(0x56067a90, float:3.696524E13)
            r0 = 1861910286(0x6efa7f0e, float:3.8762426E28)
            com.google.common.collect.ImmutableList r8 = r10.A0M(r8, r9, r0)
            X.6D6 r0 = new X.6D6
            r0.<init>()
            java.util.List r0 = X.C04300To.A07(r8, r0)
            com.google.common.collect.ImmutableList r17 = com.google.common.collect.ImmutableList.copyOf(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r18 = r1.A1d()
            r0 = 55126294(0x3492916, float:5.9115755E-37)
            long r19 = r1.getTimeValue(r0)
            r13.<init>(r14, r15, r16, r17, r18, r19)
        L_0x0146:
            if (r13 == 0) goto L_0x0249
            int r0 = r13.A00
            r1 = 1
            if (r0 != r1) goto L_0x023d
            java.lang.String r0 = r13.A06
            boolean r0 = X.C06850cB.A0A(r0)
            if (r0 != 0) goto L_0x023d
            com.google.common.collect.ImmutableList r0 = r13.A05
            if (r0 == 0) goto L_0x023d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r13.A02
            if (r0 == 0) goto L_0x023d
        L_0x015d:
            X.6BC r9 = r4.A04
            if (r1 == 0) goto L_0x01fd
            int r1 = X.AnonymousClass1Y3.Anh
            X.0UN r0 = r9.A00
            java.lang.Object r11 = X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r11 = (com.facebook.mig.scheme.interfaces.MigColorScheme) r11
            int r1 = X.AnonymousClass1Y3.Aaw
            X.0UN r0 = r9.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r1, r0)
            X.1S0 r8 = (X.AnonymousClass1S0) r8
            android.content.Context r10 = r9.A01
            java.lang.String r1 = r13.A06
            com.google.common.collect.ImmutableList r0 = r13.A05
            java.lang.CharSequence r10 = X.AnonymousClass6BC.A02(r10, r1, r0, r11)
            com.facebook.messaging.model.threads.ThreadSummary r0 = r13.A03
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r0.A0S
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r13.A02
            java.lang.String r0 = X.AnonymousClass6BC.A04(r9, r1, r0)
            java.lang.CharSequence r10 = X.AnonymousClass6BC.A03(r9, r10, r0)
            long r0 = r13.A01
            r11 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r11
            java.lang.String r11 = r8.A05(r0)
            X.5q6 r8 = X.AnonymousClass6BC.A00(r9)
            com.facebook.messaging.model.threads.ThreadSummary r12 = r13.A03
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r12.A0S
            long r0 = r0.A0G()
            r8.A00 = r0
            X.6BB r1 = r9.A02
            X.1J0 r0 = X.AnonymousClass1J0.A09
            X.2s4 r0 = r1.A0B(r12, r0)
            com.google.common.base.Preconditions.checkNotNull(r0)
            r8.A04 = r0
            X.6BB r1 = r9.A02
            com.facebook.messaging.model.threads.ThreadSummary r0 = r13.A03
            X.2s0 r0 = r1.A08(r0)
            com.google.common.base.Preconditions.checkNotNull(r0)
            r8.A01 = r0
            r0 = 1
            X.2w8 r0 = X.C59922w8.A00(r10, r11, r0)
            r8.A03 = r0
        L_0x01c5:
            X.6CP r0 = new X.6CP
            r0.<init>(r4, r6, r13, r7)
            r8.A01(r0)
            X.2rz r4 = r8.A00()
            X.6CU r1 = X.AnonymousClass6CU.A00
            X.2uO r0 = new X.2uO
            r0.<init>(r4, r13, r1)
            X.2rz r1 = X.C57482s6.A00(r4, r0)
            X.2sI r0 = X.C58942uX.A00(r3, r0)
            X.2rz r1 = X.C57482s6.A00(r1, r0)
            r2.A01(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape1_0S0300000 r0 = X.C61332yi.A00(r5)
            r0.A3A(r1)
            X.2yi r1 = r0.A34()
            X.1my r0 = X.C21621Ib.A00()
            r0.A00 = r1
            X.1Ib r0 = r0.A03()
            return r0
        L_0x01fd:
            com.facebook.messaging.model.threads.ThreadSummary r11 = r13.A03
            int r10 = r13.A00
            X.5q6 r8 = X.AnonymousClass6BC.A00(r9)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r11.A0S
            long r0 = r0.A0G()
            r8.A00 = r0
            X.6BB r1 = r9.A02
            X.1J0 r0 = X.AnonymousClass1J0.A09
            X.2s4 r0 = r1.A0B(r11, r0)
            com.google.common.base.Preconditions.checkNotNull(r0)
            r8.A04 = r0
            X.6BB r0 = r9.A02
            X.2s0 r0 = r0.A08(r11)
            com.google.common.base.Preconditions.checkNotNull(r0)
            r8.A01 = r0
            android.content.Context r9 = r9.A01
            r1 = 2131827426(0x7f111ae2, float:1.9287764E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            java.lang.String r0 = r9.getString(r1, r0)
            X.2s1 r0 = X.C57452s1.A00(r0)
            r8.A03 = r0
            goto L_0x01c5
        L_0x023d:
            r1 = 0
            goto L_0x015d
        L_0x0240:
            X.6Cv r13 = r12.A06(r11, r9)
            goto L_0x0146
        L_0x0246:
            r13 = 0
            goto L_0x0146
        L_0x0249:
            X.1Ih r0 = X.C21621Ib.A01()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass22F.AXa(X.10N, java.lang.Object):java.lang.Object");
    }

    public /* bridge */ /* synthetic */ boolean BEe(Object obj) {
        return A0U((C16070wR) obj);
    }

    public AnonymousClass11I A0S() {
        return this.A03;
    }

    public C16070wR A0T(boolean z) {
        AnonymousClass22F r1 = (AnonymousClass22F) super.A0T(z);
        if (!z) {
            r1.A03 = new AnonymousClass5CD();
        }
        return r1;
    }
}
