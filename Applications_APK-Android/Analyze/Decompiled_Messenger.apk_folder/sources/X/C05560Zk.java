package X;

import android.content.Context;
import com.facebook.inject.ContextScoped;

/* renamed from: X.0Zk  reason: invalid class name and case insensitive filesystem */
public final class C05560Zk implements AnonymousClass062 {
    public C05570Zl A00 = new C05570Zl();
    public AnonymousClass06B A01;
    public C24911Xp A02;
    public AnonymousClass0US A03;

    public C05560Zk(Context context) {
        AnonymousClass1XX r1 = AnonymousClass1XX.get(context);
        this.A02 = (C24911Xp) r1.getScope(ContextScoped.class);
        this.A01 = AnonymousClass067.A02();
        this.A03 = C04750Wa.A03(r1);
    }
}
