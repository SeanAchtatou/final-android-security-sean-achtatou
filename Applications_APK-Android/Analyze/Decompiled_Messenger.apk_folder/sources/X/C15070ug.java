package X;

import com.facebook.litho.TextContent;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0ug  reason: invalid class name and case insensitive filesystem */
public final class C15070ug {
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0007, code lost:
        if (r4.A04(r2) == null) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(int r2, X.C07870eJ r3, X.C07870eJ r4) {
        /*
            if (r4 == 0) goto L_0x0009
            java.lang.Object r1 = r4.A04(r2)
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x0010
            r4.A07(r2)
            return
        L_0x0010:
            r3.A07(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15070ug.A01(int, X.0eJ, X.0eJ):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        if (r1 != false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(android.view.View r2, android.graphics.drawable.Drawable r3, int r4, X.C31401jd r5) {
        /*
            if (r5 == 0) goto L_0x0008
            boolean r0 = r5.A01()
            if (r0 != 0) goto L_0x0010
        L_0x0008:
            r1 = 1
            r4 = r4 & r1
            if (r4 == r1) goto L_0x000d
            r1 = 0
        L_0x000d:
            r0 = 0
            if (r1 == 0) goto L_0x0011
        L_0x0010:
            r0 = 1
        L_0x0011:
            if (r0 == 0) goto L_0x0020
            boolean r0 = r3.isStateful()
            if (r0 == 0) goto L_0x0020
            int[] r0 = r2.getDrawableState()
            r3.setState(r0)
        L_0x0020:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15070ug.A02(android.view.View, android.graphics.drawable.Drawable, int, X.1jd):void");
    }

    public static TextContent A00(List list) {
        int size = list.size();
        if (size == 1) {
            Object obj = list.get(0);
            if (obj instanceof TextContent) {
                return (TextContent) obj;
            }
            return TextContent.A00;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size; i++) {
            Object obj2 = list.get(i);
            if (obj2 instanceof TextContent) {
                arrayList.addAll(((TextContent) obj2).getTextItems());
            }
        }
        return new C123165r9(arrayList);
    }
}
