package X;

/* renamed from: X.1nD  reason: invalid class name and case insensitive filesystem */
public final class C33211nD {
    private static int A04;
    private static C33211nD A05;
    private static final Object A06 = new Object();
    public long A00;
    public Integer A01;
    public String A02;
    private C33211nD A03;

    public static C33211nD A00() {
        synchronized (A06) {
            C33211nD r1 = A05;
            if (r1 == null) {
                return new C33211nD();
            }
            A05 = r1.A03;
            r1.A03 = null;
            A04--;
            return r1;
        }
    }

    public void A01() {
        synchronized (A06) {
            int i = A04;
            if (i < 5) {
                this.A02 = null;
                this.A00 = 0;
                this.A01 = null;
                A04 = i + 1;
                C33211nD r0 = A05;
                if (r0 != null) {
                    this.A03 = r0;
                }
                A05 = this;
            }
        }
    }

    private C33211nD() {
    }
}
