package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.profilepic.ProfilePicUriWithFilePath;

/* renamed from: X.0ef  reason: invalid class name and case insensitive filesystem */
public final class C08090ef implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ProfilePicUriWithFilePath(parcel);
    }

    public Object[] newArray(int i) {
        return new ProfilePicUriWithFilePath[i];
    }
}
