package X;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.util.Map;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0RS  reason: invalid class name */
public final class AnonymousClass0RS {
    public final long A00 = SystemClock.elapsedRealtime();
    public final C01570At A01;
    private final C01400Ab A02;
    private final String A03;

    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "REQUEST_SENT_SUCCESS";
            case 2:
                return "REQUEST_SENT_FAIL";
            case 3:
                return "RESPONSE_RECEIVED";
            case 4:
                return "FAILURE_CACHE_UPDATE";
            case 5:
                return "FAILURE_SERVICE_NOT_STARTED";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                return "FAILURE_MQTT_NOT_CONNECTED";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "FAILURE_UNKNOWN_CLIENT_ERROR";
            case 8:
                return "FAILURE_SERVER_RESPOND_WITH_ERROR";
            case Process.SIGKILL /*9*/:
                return "FAILURE_SERVER_RESPOND_WITH_INVALID_PACKAGE_NAME";
            case AnonymousClass1Y3.A01 /*10*/:
                return "FAILURE_SERVER_RESPOND_WITH_INVALID_TOKEN";
            case AnonymousClass1Y3.A02 /*11*/:
                return "FAILURE_PACKAGE_DOES_NOT_MATCH_INTENT";
            case AnonymousClass1Y3.A03 /*12*/:
                return "FAILURE_EMPTY_PACKAGE_NAME";
            case 13:
                return "UNREGISTER_CALLED";
            case 14:
                return "AUTHFAIL_AUTO_REGISTER";
            case 15:
                return "REGISTER";
            case 16:
                return "UNREGISTER_FAILURE_MQTT_NOT_CONNECTED";
            case 17:
                return "UNREGISTER_REQUEST_SENT_SUCCESS";
            case Process.SIGCONT /*18*/:
                return "UNREGISTER_REQUEST_SENT_FAIL";
            case Process.SIGSTOP /*19*/:
                return "CREDENTIALS_UPDATED";
            default:
                return "CACHE_HIT";
        }
    }

    public static void A01(AnonymousClass0RS r2, String str, Map map) {
        C01750Bm r1 = new C01750Bm(str, r2.A03);
        r1.A01(map);
        r2.A02.C2i(r1);
    }

    public void A02(Integer num, String str) {
        Map A002 = C01740Bl.A00("event_type", A00(num));
        if (!TextUtils.isEmpty(str)) {
            A002.put("event_extra_info", str);
        }
        A01(this, "fbns_registration_event", A002);
    }

    public void A03(Integer num, String str) {
        String str2;
        if (1 - num.intValue() != 0) {
            str2 = "JSON_PARSE_ERROR";
        } else {
            str2 = "UNEXPECTED_TOPIC";
        }
        Map A002 = C01740Bl.A00("event_type", str2);
        if (!TextUtils.isEmpty(str)) {
            A002.put("event_extra_info", str);
        }
        A01(this, "fbns_service_event", A002);
    }

    public void A04(Integer num, String str, String str2) {
        Map A002 = C01740Bl.A00("event_type", AnonymousClass0RT.A00(num));
        if (!TextUtils.isEmpty(str)) {
            A002.put("event_extra_info", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            A002.put("dpn", str2);
        }
        A01(this, "fbns_message_event", A002);
    }

    public void A05(Integer num, String str, String str2, String str3) {
        Map A002 = C01740Bl.A00("event_type", A00(num));
        if (!TextUtils.isEmpty(str)) {
            A002.put("event_extra_info", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            A002.put("spn", str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            A002.put("dpn", str3);
        }
        A01(this, "fbns_registration_event", A002);
    }

    public void A06(String str) {
        Map A002 = C01740Bl.A00("event_type", "verify_sender_failed");
        if (!TextUtils.isEmpty(str)) {
            A002.put("event_extra_info", str);
        }
        A01(this, "fbns_auth_intent_event", A002);
    }

    public AnonymousClass0RS(Context context, C01570At r4, C01400Ab r5) {
        this.A03 = context.getPackageName();
        this.A01 = r4;
        this.A02 = r5;
    }
}
