package X;

import com.facebook.runtimepermissions.RequestPermissionsConfig;

/* renamed from: X.0T0  reason: invalid class name */
public interface AnonymousClass0T0 {
    public static final RequestPermissionsConfig A00;

    void ATu(String str, RequestPermissionsConfig requestPermissionsConfig, C73233fj r3);

    void ATv(String str, C73233fj r2);

    void ATw(String[] strArr, RequestPermissionsConfig requestPermissionsConfig, C73233fj r3);

    void ATx(String[] strArr, C73233fj r2);

    boolean BBd(String str);

    boolean BBf(String[] strArr);

    static {
        C64513Cd r1 = new C64513Cd();
        r1.A00(2);
        A00 = new RequestPermissionsConfig(r1);
    }
}
