package X;

/* renamed from: X.1zy  reason: invalid class name and case insensitive filesystem */
public final class C40011zy {
    public static Integer A00(String str) {
        if (str.equals("UNSET")) {
            return AnonymousClass07B.A00;
        }
        if (str.equals("FRIENDS")) {
            return AnonymousClass07B.A01;
        }
        if (str.equals(C05360Yq.$const$string(301))) {
            return AnonymousClass07B.A0C;
        }
        if (str.equals(C05360Yq.$const$string(293))) {
            return AnonymousClass07B.A0N;
        }
        throw new IllegalArgumentException(str);
    }
}
