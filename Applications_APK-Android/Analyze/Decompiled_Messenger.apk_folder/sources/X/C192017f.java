package X;

import android.content.Context;
import android.view.Choreographer;
import java.util.concurrent.TimeUnit;

/* renamed from: X.17f  reason: invalid class name and case insensitive filesystem */
public final class C192017f implements C192117g {
    public static final long A07 = TimeUnit.MINUTES.toNanos(1);
    public double A00;
    public double A01;
    public long A02;
    private boolean A03 = false;
    public final double A04;
    public final C192317i A05;
    private final AnonymousClass05V A06;

    public void disable() {
        if (this.A03) {
            this.A03 = false;
            this.A06.disable();
            this.A05.Bln(new C73873gu(Math.min(this.A01, 3600.0d), Math.min(this.A00, 1000.0d), TimeUnit.NANOSECONDS.toMillis(Math.min(this.A02, A07))));
            this.A05.BWM();
            this.A01 = 0.0d;
            this.A00 = 0.0d;
            this.A02 = 0;
        }
    }

    public void enable() {
        if (!this.A03) {
            this.A03 = true;
            this.A06.enable();
            this.A05.BXb();
        }
    }

    public C192017f(C192317i r5, Context context, boolean z) {
        if (r5 != null) {
            this.A05 = r5;
            C192717m r2 = new C192717m(this);
            if (z) {
                this.A06 = new C04250Te(Choreographer.getInstance(), r2);
            } else {
                this.A06 = new AnonymousClass088(Choreographer.getInstance(), r2);
            }
            if (AnonymousClass05W.A01 == null) {
                AnonymousClass05W.A01 = new AnonymousClass05W();
            }
            this.A04 = (double) AnonymousClass05W.A01.A00(context);
            this.A01 = 0.0d;
            this.A00 = 0.0d;
            this.A02 = 0;
            return;
        }
        throw new IllegalArgumentException("FPSReporter should not be null");
    }
}
