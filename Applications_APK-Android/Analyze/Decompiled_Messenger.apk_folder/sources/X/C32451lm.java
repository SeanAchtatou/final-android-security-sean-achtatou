package X;

import com.facebook.auth.userscope.UserScoped;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

@UserScoped
/* renamed from: X.1lm  reason: invalid class name and case insensitive filesystem */
public final class C32451lm {
    private static C05540Zi A03;
    public final C17350yl A00;
    public final C24321Td A01;
    private final AnonymousClass09P A02;

    public static final C32451lm A00(AnonymousClass1XY r4) {
        C32451lm r0;
        synchronized (C32451lm.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C32451lm((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (C32451lm) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0024, code lost:
        if (r1 != null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0029, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C36841tx A01(X.C32451lm r0, java.lang.String r1, java.lang.Integer r2) {
        /*
            X.1Td r0 = r0.A01
            com.facebook.omnistore.Collection r0 = r0.A01(r2)
            com.facebook.omnistore.Cursor r1 = r0.getObject(r1)
            boolean r0 = r1.step()     // Catch:{ all -> 0x0021 }
            if (r0 == 0) goto L_0x001c
            java.nio.ByteBuffer r0 = r1.getBlob()     // Catch:{ all -> 0x0021 }
            X.1tx r0 = X.C36841tx.A00(r0)     // Catch:{ all -> 0x0021 }
            r1.close()
            return r0
        L_0x001c:
            r0 = 0
            r1.close()
            return r0
        L_0x0021:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x0023:
            r0 = move-exception
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ all -> 0x0029 }
        L_0x0029:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32451lm.A01(X.1lm, java.lang.String, java.lang.Integer):X.1tx");
    }

    public static C33531nj A02(ImmutableList immutableList) {
        ImmutableList immutableList2 = immutableList;
        C24971Xv it = immutableList2.iterator();
        Float f = null;
        long j = 0;
        long j2 = 0;
        AnonymousClass1T9 r7 = null;
        String str = null;
        float f2 = Float.MAX_VALUE;
        while (it.hasNext()) {
            C36841tx r13 = (C36841tx) it.next();
            AnonymousClass1T9 A0A = r13.A0A();
            Preconditions.checkNotNull(A0A);
            if (r7 == null) {
                r7 = A0A;
            } else {
                String A08 = A0A.A08();
                Preconditions.checkNotNull(A08);
                Preconditions.checkState(A08.equals(r7.A08()));
            }
            long longValue = Long.valueOf(Long.parseLong(r13.A0E())).longValue();
            if (longValue > j2) {
                j2 = longValue;
            }
            int A022 = r13.A02(22);
            boolean z = false;
            if (!(A022 == 0 || r13.A01.get(A022 + r13.A00) == 0)) {
                z = true;
            }
            if (!z && longValue > j) {
                j = longValue;
            }
            if (A0A.A06() != 0.0f && A0A.A06() < f2) {
                f2 = A0A.A06();
                int A023 = A0A.A02(16);
                if (A023 != 0) {
                    str = A0A.A05(A023 + A0A.A00);
                } else {
                    str = null;
                }
            }
        }
        C24311Tc r1 = new C24311Tc();
        if (f2 != Float.MAX_VALUE) {
            f = Float.valueOf(f2);
        }
        r1.A04 = f;
        r1.A05 = str;
        r1.A00 = j;
        r1.A01 = j2;
        r1.A03 = immutableList2;
        C28931fb.A06(immutableList2, "messages");
        Preconditions.checkNotNull(r7);
        AnonymousClass1T9 r72 = r7;
        r1.A02 = r72;
        C28931fb.A06(r72, "fBMMontageThreadBriefSummary");
        return new C33531nj(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0052, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0053, code lost:
        if (r5 != null) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0058 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A03(java.lang.Integer r13) {
        /*
            r12 = this;
            java.lang.String r4 = "MontageLoader"
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            X.1Td r0 = r12.A01     // Catch:{ OmnistoreIOException -> 0x0059, Exception -> 0x0062 }
            com.facebook.omnistore.Collection r5 = r0.A01(r13)     // Catch:{ OmnistoreIOException -> 0x0059, Exception -> 0x0062 }
            r0 = 0
            java.lang.String[] r7 = new java.lang.String[r0]     // Catch:{ OmnistoreIOException -> 0x0059, Exception -> 0x0062 }
            java.lang.String r6 = "select primary_key from {0}"
            java.lang.String r8 = "timestamp_precise"
            r10 = 1
            r9 = 1
            r11 = 1000(0x3e8, float:1.401E-42)
            com.facebook.omnistore.Cursor r5 = r5.queryWithIndexSorted(r6, r7, r8, r9, r10, r11)     // Catch:{ OmnistoreIOException -> 0x0059, Exception -> 0x0062 }
        L_0x001c:
            boolean r0 = r5.step()     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x004c
            java.nio.ByteBuffer r0 = r5.getBlob()     // Catch:{ all -> 0x0050 }
            X.1tx r3 = X.C36841tx.A00(r0)     // Catch:{ all -> 0x0050 }
            X.1T9 r0 = r3.A0A()     // Catch:{ all -> 0x0050 }
            X.AnonymousClass0qX.A00(r0)     // Catch:{ all -> 0x0050 }
            java.lang.String r1 = r0.A08()     // Catch:{ all -> 0x0050 }
            X.AnonymousClass0qX.A00(r1)     // Catch:{ all -> 0x0050 }
            java.lang.Object r0 = r2.get(r1)     // Catch:{ all -> 0x0050 }
            com.google.common.collect.ImmutableList$Builder r0 = (com.google.common.collect.ImmutableList.Builder) r0     // Catch:{ all -> 0x0050 }
            if (r0 != 0) goto L_0x0048
            com.google.common.collect.ImmutableList$Builder r0 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ all -> 0x0050 }
            r0.<init>()     // Catch:{ all -> 0x0050 }
            r2.put(r1, r0)     // Catch:{ all -> 0x0050 }
        L_0x0048:
            r0.add(r3)     // Catch:{ all -> 0x0050 }
            goto L_0x001c
        L_0x004c:
            r5.close()     // Catch:{ OmnistoreIOException -> 0x0059, Exception -> 0x0062 }
            goto L_0x006a
        L_0x0050:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0052 }
        L_0x0052:
            r0 = move-exception
            if (r5 == 0) goto L_0x0058
            r5.close()     // Catch:{ all -> 0x0058 }
        L_0x0058:
            throw r0     // Catch:{ OmnistoreIOException -> 0x0059, Exception -> 0x0062 }
        L_0x0059:
            r3 = move-exception
            X.09P r1 = r12.A02
            java.lang.String r0 = "IO error loading montage threads"
            r1.softReport(r4, r0, r3)
            goto L_0x006a
        L_0x0062:
            r3 = move-exception
            X.09P r1 = r12.A02
            java.lang.String r0 = "loadFBMMontageThreadInfo"
            r1.softReport(r4, r0, r3)
        L_0x006a:
            com.google.common.collect.ImmutableList$Builder r3 = com.google.common.collect.ImmutableList.builder()
            java.util.Set r0 = r2.keySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x0076:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0094
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r2.get(r0)
            com.google.common.collect.ImmutableList$Builder r0 = (com.google.common.collect.ImmutableList.Builder) r0
            com.google.common.collect.ImmutableList r0 = r0.build()
            X.1nj r0 = A02(r0)
            r3.add(r0)
            goto L_0x0076
        L_0x0094:
            com.google.common.collect.ImmutableList r0 = r3.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32451lm.A03(java.lang.Integer):com.google.common.collect.ImmutableList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005b, code lost:
        if (r2 != null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0060 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A04(java.util.Collection r13, java.lang.Integer r14) {
        /*
            r12 = this;
            java.lang.String r4 = "MontageLoader"
            com.google.common.collect.ImmutableList$Builder r3 = com.google.common.collect.ImmutableList.builder()
            java.util.Iterator r5 = r13.iterator()
        L_0x000a:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0073
            java.lang.Object r2 = r5.next()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r1 = "thread_key"
            r0 = 3
            com.facebook.omnistore.IndexQuery r6 = com.facebook.omnistore.IndexQuery.predicate(r1, r0, r2)     // Catch:{ OmnistoreIOException -> 0x006a, Exception -> 0x0061 }
            X.1Td r0 = r12.A01     // Catch:{ OmnistoreIOException -> 0x006a, Exception -> 0x0061 }
            com.facebook.omnistore.Collection r7 = r0.A01(r14)     // Catch:{ OmnistoreIOException -> 0x006a, Exception -> 0x0061 }
            java.lang.String r8 = "timestamp_precise"
            r10 = 1
            r9 = 1
            r11 = 1000(0x3e8, float:1.401E-42)
            com.facebook.omnistore.Cursor r2 = r6.queryWithIndexSorted(r7, r8, r9, r10, r11)     // Catch:{ OmnistoreIOException -> 0x006a, Exception -> 0x0061 }
            com.google.common.collect.ImmutableList$Builder r1 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x0058 }
        L_0x0031:
            boolean r0 = r2.step()     // Catch:{ all -> 0x0058 }
            if (r0 == 0) goto L_0x0043
            java.nio.ByteBuffer r0 = r2.getBlob()     // Catch:{ all -> 0x0058 }
            X.1tx r0 = X.C36841tx.A00(r0)     // Catch:{ all -> 0x0058 }
            r1.add(r0)     // Catch:{ all -> 0x0058 }
            goto L_0x0031
        L_0x0043:
            com.google.common.collect.ImmutableList r1 = r1.build()     // Catch:{ all -> 0x0058 }
            boolean r0 = r1.isEmpty()     // Catch:{ all -> 0x0058 }
            if (r0 != 0) goto L_0x0054
            X.1nj r0 = A02(r1)     // Catch:{ all -> 0x0058 }
            r3.add(r0)     // Catch:{ all -> 0x0058 }
        L_0x0054:
            r2.close()     // Catch:{ OmnistoreIOException -> 0x006a, Exception -> 0x0061 }
            goto L_0x000a
        L_0x0058:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005a }
        L_0x005a:
            r0 = move-exception
            if (r2 == 0) goto L_0x0060
            r2.close()     // Catch:{ all -> 0x0060 }
        L_0x0060:
            throw r0     // Catch:{ OmnistoreIOException -> 0x006a, Exception -> 0x0061 }
        L_0x0061:
            r2 = move-exception
            X.09P r1 = r12.A02
            java.lang.String r0 = "loadFBMMontageThreadInfo"
            r1.softReport(r4, r0, r2)
            goto L_0x000a
        L_0x006a:
            r2 = move-exception
            X.09P r1 = r12.A02
            java.lang.String r0 = "IO error loading montage thread %s"
            r1.softReport(r4, r0, r2)
            goto L_0x000a
        L_0x0073:
            com.google.common.collect.ImmutableList r0 = r3.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32451lm.A04(java.util.Collection, java.lang.Integer):com.google.common.collect.ImmutableList");
    }

    private C32451lm(AnonymousClass1XY r2) {
        this.A01 = C24321Td.A00(r2);
        this.A00 = C17350yl.A00(r2);
        this.A02 = C04750Wa.A01(r2);
    }
}
