package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.1WO  reason: invalid class name */
public final class AnonymousClass1WO implements Executor {
    private static final Handler A00 = new Handler(Looper.getMainLooper());

    public void execute(Runnable runnable) {
        AnonymousClass00S.A04(A00, runnable, 522294301);
    }
}
