package X;

import android.content.Context;
import com.facebook.compactdisk.current.DiskCacheEvents;
import com.facebook.compactdisk.current.Factory;
import com.facebook.compactdisk.current.FileCacheConfig;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1yz  reason: invalid class name and case insensitive filesystem */
public final class C39401yz implements Factory {
    public final /* synthetic */ long A00;
    public final /* synthetic */ long A01;
    public final /* synthetic */ long A02;
    public final /* synthetic */ Context A03;
    public final /* synthetic */ DiskCacheEvents A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ C04310Tq A06;
    public final /* synthetic */ boolean A07;

    public C39401yz(Context context, long j, String str, long j2, C04310Tq r7, boolean z, long j3, DiskCacheEvents diskCacheEvents) {
        this.A03 = context;
        this.A02 = j;
        this.A05 = str;
        this.A01 = j2;
        this.A06 = r7;
        this.A07 = z;
        this.A00 = j3;
        this.A04 = diskCacheEvents;
    }

    public Object create() {
        try {
            String canonicalPath = this.A03.getDir(C05360Yq.$const$string(AnonymousClass1Y3.A3T), 0).getCanonicalPath();
            String valueOf = String.valueOf(this.A02);
            FileCacheConfig.Builder versionID = new FileCacheConfig.Builder().setName(this.A05).setParentDirectory(canonicalPath).setMaxSize(this.A01).setScope(((C30541iE) this.A06.get()).A00()).setKeepDataBetweenSessions(this.A07).setStaleAge(TimeUnit.DAYS.toSeconds(this.A00)).setKeyStatsSamplingRate(AnonymousClass299.A01).enableExtraSupport().setUseNestedDirStructure(true).setStoreInCacheDirectory(false).setVersionID(valueOf);
            AnonymousClass29a r1 = new AnonymousClass29a();
            r1.A01 = valueOf;
            r1.A02 = this.A07;
            r1.A00 = canonicalPath;
            FileCacheConfig.Builder migrationConfig = versionID.setMigrationConfig(r1.A00());
            DiskCacheEvents diskCacheEvents = this.A04;
            if (diskCacheEvents != null) {
                migrationConfig.setEvents(diskCacheEvents, 127);
            }
            return migrationConfig.build();
        } catch (IOException e) {
            C010708t.A0L("CameraCacheConfigsV2", "Error create fileCacheConfig factory", e);
            return null;
        }
    }
}
