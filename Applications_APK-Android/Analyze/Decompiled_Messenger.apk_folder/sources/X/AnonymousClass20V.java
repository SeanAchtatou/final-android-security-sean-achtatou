package X;

import android.util.Log;
import java.io.Closeable;
import java.io.IOException;

/* renamed from: X.20V  reason: invalid class name */
public final class AnonymousClass20V {
    public static void A00(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Log.e("Spectrum", "Could not close stream", e);
            }
        }
    }
}
