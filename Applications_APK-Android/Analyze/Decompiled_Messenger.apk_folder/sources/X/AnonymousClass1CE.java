package X;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import java.util.Collection;
import java.util.Collections;

/* renamed from: X.1CE  reason: invalid class name */
public final class AnonymousClass1CE extends C06140av {
    public final String A00;
    public final Collection A01;
    public final Collection A02;
    public final boolean A03;

    public static String A00(Iterable iterable) {
        return AnonymousClass08S.A0P("(", new Joiner(String.valueOf(',')).join(AnonymousClass0j4.A00(iterable, new C50622eK())), ")");
    }

    public AnonymousClass1CE(String str, Collection collection, boolean z) {
        Collection emptyList;
        Preconditions.checkNotNull(str);
        this.A00 = str;
        Preconditions.checkNotNull(collection);
        if (collection.size() < 450) {
            emptyList = collection;
        } else {
            emptyList = Collections.emptyList();
        }
        this.A02 = emptyList;
        this.A01 = collection.size() < 450 ? Collections.emptyList() : collection;
        this.A03 = z;
    }
}
