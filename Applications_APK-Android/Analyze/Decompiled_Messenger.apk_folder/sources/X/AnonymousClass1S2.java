package X;

/* renamed from: X.1S2  reason: invalid class name */
public final class AnonymousClass1S2 extends AnonymousClass1S3 {
    public int A00;
    public AnonymousClass1PS A01;
    public final C22921Ni A02;

    public AnonymousClass1SE A02() {
        if (AnonymousClass1PS.A07(this.A01)) {
            return new AnonymousClass1SE(this.A01, this.A00);
        }
        throw new AnonymousClass4X2();
    }

    public void close() {
        AnonymousClass1PS.A05(this.A01);
        this.A01 = null;
        this.A00 = -1;
        super.close();
    }

    public AnonymousClass1S2(C22921Ni r3, int i) {
        C05520Zg.A04(i > 0);
        C05520Zg.A02(r3);
        this.A02 = r3;
        this.A00 = 0;
        this.A01 = AnonymousClass1PS.A02(r3.get(i), this.A02);
    }

    public void write(int i) {
        write(new byte[]{(byte) i});
    }

    public void write(byte[] bArr, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            throw new ArrayIndexOutOfBoundsException(AnonymousClass08S.A0D("length=", bArr.length, "; regionStart=", i3, "; regionLength=", i4));
        }
        if (AnonymousClass1PS.A07(this.A01)) {
            int i5 = this.A00 + i2;
            if (AnonymousClass1PS.A07(this.A01)) {
                if (i5 > ((AnonymousClass1NZ) this.A01.A0A()).getSize()) {
                    AnonymousClass1NZ r3 = (AnonymousClass1NZ) this.A02.get(i5);
                    ((AnonymousClass1NZ) this.A01.A0A()).copy(0, r3, 0, this.A00);
                    this.A01.close();
                    this.A01 = AnonymousClass1PS.A02(r3, this.A02);
                }
                ((AnonymousClass1NZ) this.A01.A0A()).write(this.A00, bArr, i, i2);
                this.A00 += i2;
                return;
            }
        }
        throw new AnonymousClass4X2();
    }
}
