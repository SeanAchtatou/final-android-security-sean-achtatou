package X;

import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

/* renamed from: X.0jR  reason: invalid class name and case insensitive filesystem */
public abstract class C10030jR extends C09570i2 implements Serializable, Type {
    private static final long serialVersionUID = 6774285981275451126L;
    public final boolean _asStatic;
    public final Class _class;
    public final int _hashCode;
    public final Object _typeHandler;
    public final Object _valueHandler;

    public abstract C10030jR _narrow(Class cls);

    public C10030jR containedType(int i) {
        return null;
    }

    public int containedTypeCount() {
        return 0;
    }

    public String containedTypeName(int i) {
        return null;
    }

    public abstract boolean equals(Object obj);

    public C10030jR getContentType() {
        return null;
    }

    public C10030jR getKeyType() {
        return null;
    }

    public boolean isArrayType() {
        return false;
    }

    public boolean isCollectionLikeType() {
        return false;
    }

    public abstract boolean isContainerType();

    public boolean isMapLikeType() {
        return false;
    }

    public abstract C10030jR narrowContentsBy(Class cls);

    public abstract String toString();

    public abstract C10030jR widenContentsBy(Class cls);

    public abstract C10030jR withContentTypeHandler(Object obj);

    public abstract C10030jR withContentValueHandler(Object obj);

    public abstract C10030jR withStaticTyping();

    public abstract C10030jR withTypeHandler(Object obj);

    public abstract C10030jR withValueHandler(Object obj);

    private void _assertSubclass(Class cls, Class cls2) {
        if (!this._class.isAssignableFrom(cls)) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Class ", cls.getName(), " is not assignable to ", this._class.getName()));
        }
    }

    public Object getTypeHandler() {
        return this._typeHandler;
    }

    public Object getValueHandler() {
        return this._valueHandler;
    }

    public final int hashCode() {
        return this._hashCode;
    }

    public boolean isAbstract() {
        return Modifier.isAbstract(this._class.getModifiers());
    }

    public boolean isConcrete() {
        if ((this._class.getModifiers() & 1536) == 0 || this._class.isPrimitive()) {
            return true;
        }
        return false;
    }

    public C10030jR narrowBy(Class cls) {
        Class cls2 = this._class;
        if (cls == cls2) {
            return this;
        }
        _assertSubclass(cls, cls2);
        C10030jR _narrow = _narrow(cls);
        Object obj = this._valueHandler;
        if (obj != _narrow.getValueHandler()) {
            _narrow = _narrow.withValueHandler(obj);
        }
        Object obj2 = this._typeHandler;
        if (obj2 != _narrow.getTypeHandler()) {
            return _narrow.withTypeHandler(obj2);
        }
        return _narrow;
    }

    public C10030jR widenBy(Class cls) {
        Class cls2 = this._class;
        if (cls == cls2) {
            return this;
        }
        _assertSubclass(cls2, cls);
        return _narrow(cls);
    }

    public C10030jR(Class cls, int i, Object obj, Object obj2, boolean z) {
        this._class = cls;
        this._hashCode = cls.getName().hashCode() + i;
        this._valueHandler = obj;
        this._typeHandler = obj2;
        this._asStatic = z;
    }

    public boolean hasGenericTypes() {
        if (containedTypeCount() > 0) {
            return true;
        }
        return false;
    }
}
