package X;

import android.content.res.Resources;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

/* renamed from: X.1Sc  reason: invalid class name and case insensitive filesystem */
public final class C24071Sc {
    private final Resources A00;
    private final C04310Tq A01;

    public static final C24071Sc A00(AnonymousClass1XY r3) {
        return new C24071Sc(AnonymousClass0VG.A00(AnonymousClass1Y3.BQw, r3), C04490Ux.A0L(r3));
    }

    public static final C24071Sc A01(AnonymousClass1XY r3) {
        return new C24071Sc(AnonymousClass0VG.A00(AnonymousClass1Y3.BQw, r3), C04490Ux.A0L(r3));
    }

    public char A03() {
        if (C49202bp.A01.contains(((Locale) this.A01.get()).getLanguage())) {
            return 12289;
        }
        return ',';
    }

    public String A04() {
        String language = ((Locale) this.A01.get()).getLanguage();
        char A03 = A03();
        if (C49202bp.A02.contains(language)) {
            return Character.toString(A03);
        }
        return AnonymousClass08S.A00(A03, " ");
    }

    private C24071Sc(C04310Tq r1, Resources resources) {
        this.A01 = r1;
        this.A00 = resources;
    }

    public static String A02(C24071Sc r11, List list) {
        int size = list.size();
        if (size != 0) {
            if (size == 1) {
                return (String) list.get(0);
            }
            if (size == 2) {
                return r11.A00.getString(2131825736, list.get(0), list.get(1));
            }
            Object obj = list.get(0);
            String string = r11.A00.getString(2131825736);
            C49182bn r6 = new C49182bn(list.size() * 5);
            Formatter formatter = new Formatter(r6, r11.A00.getConfiguration().locale);
            for (int i = 1; i < size; i++) {
                formatter.format(string, obj, list.get(i));
                List list2 = r6.A02;
                obj = new C49192bo(list2, r6.A01, list2.size(), r6.A00);
                r6.A01 = r6.A02.size();
                r6.A00 = 0;
            }
            return obj.toString();
        }
        throw new IllegalArgumentException();
    }

    public String A05(List list) {
        int size = list.size();
        if (size == 0) {
            throw new IllegalArgumentException();
        } else if (size == 1) {
            return (String) list.get(0);
        } else {
            int i = size - 1;
            return this.A00.getString(2131825737, A02(this, list.subList(0, i)), list.get(i));
        }
    }
}
