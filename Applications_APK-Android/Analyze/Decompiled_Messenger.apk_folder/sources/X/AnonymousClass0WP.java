package X;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0WP  reason: invalid class name */
public interface AnonymousClass0WP {
    void ANq(Object obj);

    boolean BDj();

    boolean BFP();

    boolean BHL();

    void C25(Object obj);

    AnonymousClass0XX CIB(String str, Runnable runnable, AnonymousClass0XV r3, ExecutorService executorService);

    AnonymousClass0XX CIG(String str, Runnable runnable, AnonymousClass0XV r3, Integer num);

    AnonymousClass0XX CIH(String str, Callable callable, AnonymousClass0XV r3, Integer num);
}
