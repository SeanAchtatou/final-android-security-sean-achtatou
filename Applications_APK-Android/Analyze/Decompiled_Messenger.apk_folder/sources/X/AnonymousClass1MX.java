package X;

import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1MX  reason: invalid class name */
public final class AnonymousClass1MX {
    private static final C07870eJ A01 = new C07870eJ();
    private static final C29761gw A02 = new C29761gw(50);
    private static volatile AnonymousClass1MX A03;
    private final Resources A00;

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005f, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.graphics.drawable.Drawable A04(int r7, int r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            if (r7 != 0) goto L_0x0006
            r0 = 0
            monitor-exit(r6)
            return r0
        L_0x0006:
            X.0ZG r0 = X.AnonymousClass27Y.A02     // Catch:{ all -> 0x0060 }
            java.lang.Object r4 = r0.ALa()     // Catch:{ all -> 0x0060 }
            X.27Y r4 = (X.AnonymousClass27Y) r4     // Catch:{ all -> 0x0060 }
            if (r4 != 0) goto L_0x0015
            X.27Y r4 = new X.27Y     // Catch:{ all -> 0x0060 }
            r4.<init>()     // Catch:{ all -> 0x0060 }
        L_0x0015:
            r4.A01 = r7     // Catch:{ all -> 0x0060 }
            r4.A00 = r8     // Catch:{ all -> 0x0060 }
            X.1gw r0 = X.AnonymousClass1MX.A02     // Catch:{ all -> 0x0060 }
            java.lang.Object r1 = r0.A03(r4)     // Catch:{ all -> 0x0060 }
            android.graphics.drawable.Drawable$ConstantState r1 = (android.graphics.drawable.Drawable.ConstantState) r1     // Catch:{ all -> 0x0060 }
            r5 = 1
            if (r1 != 0) goto L_0x004f
            android.content.res.Resources r0 = r6.A00     // Catch:{ all -> 0x0060 }
            android.graphics.drawable.Drawable r0 = r0.getDrawable(r7)     // Catch:{ all -> 0x0060 }
            android.graphics.drawable.Drawable r3 = r0.mutate()     // Catch:{ all -> 0x0060 }
            android.graphics.ColorFilter r0 = A00(r8)     // Catch:{ all -> 0x0060 }
            r3.setColorFilter(r0)     // Catch:{ all -> 0x0060 }
            android.graphics.drawable.Drawable$ConstantState r2 = r3.getConstantState()     // Catch:{ all -> 0x0060 }
            if (r2 == 0) goto L_0x0057
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0060 }
            r0 = 21
            if (r1 >= r0) goto L_0x0049
            boolean r0 = r3 instanceof android.graphics.drawable.DrawableContainer     // Catch:{ all -> 0x0060 }
            if (r0 != 0) goto L_0x0057
            boolean r0 = r3 instanceof android.graphics.drawable.RotateDrawable     // Catch:{ all -> 0x0060 }
            if (r0 != 0) goto L_0x0057
        L_0x0049:
            X.1gw r0 = X.AnonymousClass1MX.A02     // Catch:{ all -> 0x0060 }
            r0.A05(r4, r2)     // Catch:{ all -> 0x0060 }
            goto L_0x0056
        L_0x004f:
            android.content.res.Resources r0 = r6.A00     // Catch:{ all -> 0x0060 }
            android.graphics.drawable.Drawable r3 = r1.newDrawable(r0)     // Catch:{ all -> 0x0060 }
            goto L_0x0057
        L_0x0056:
            r5 = 0
        L_0x0057:
            if (r5 == 0) goto L_0x005e
            X.0ZG r0 = X.AnonymousClass27Y.A02     // Catch:{ all -> 0x0060 }
            r0.C0w(r4)     // Catch:{ all -> 0x0060 }
        L_0x005e:
            monitor-exit(r6)
            return r3
        L_0x0060:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MX.A04(int, int):android.graphics.drawable.Drawable");
    }

    public static ColorFilter A00(int i) {
        ColorFilter colorFilter;
        if (i == 0) {
            return null;
        }
        C07870eJ r2 = A01;
        synchronized (r2) {
            colorFilter = (ColorFilter) r2.A04(i);
            if (colorFilter == null) {
                colorFilter = new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_IN);
                r2.A09(i, colorFilter);
            }
        }
        return colorFilter;
    }

    public static Drawable A01(Resources resources, Drawable drawable, int i) {
        if (drawable == null) {
            return null;
        }
        int level = drawable.getLevel();
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState != null) {
            drawable = constantState.newDrawable(resources).mutate();
        }
        drawable.setColorFilter(A00(i));
        if (drawable instanceof LevelListDrawable) {
            drawable.setLevel(level);
        }
        return drawable;
    }

    public static final AnonymousClass1MX A03(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass1MX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass1MX(C04490Ux.A0L(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public AnonymousClass1MX(Resources resources) {
        this.A00 = resources;
    }

    public static final AnonymousClass1MX A02(AnonymousClass1XY r0) {
        return A03(r0);
    }
}
