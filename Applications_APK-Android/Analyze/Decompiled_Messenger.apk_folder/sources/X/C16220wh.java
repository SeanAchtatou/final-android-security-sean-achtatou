package X;

import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0wh  reason: invalid class name and case insensitive filesystem */
public final class C16220wh {
    public final C16250wk A00 = new C16250wk();
    public final C16240wj A01;
    public final List A02 = new ArrayList();

    public static int A00(C16220wh r5, int i) {
        if (i >= 0) {
            int Ah4 = r5.A01.Ah4();
            int i2 = i;
            while (i2 < Ah4) {
                C16250wk r1 = r5.A00;
                int A012 = i - (i2 - r1.A01(i2));
                if (A012 != 0) {
                    i2 += A012;
                } else {
                    while (r1.A06(i2)) {
                        i2++;
                    }
                    return i2;
                }
            }
        }
        return -1;
    }

    public static void A01(C16220wh r1, View view) {
        if (r1.A02.remove(view)) {
            r1.A01.Bca(view);
        }
    }

    public int A02() {
        return this.A01.Ah4() - this.A02.size();
    }

    public void A04(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int A002;
        if (i < 0) {
            A002 = this.A01.Ah4();
        } else {
            A002 = A00(this, i);
        }
        this.A00.A05(A002, z);
        if (z) {
            this.A02.add(view);
            this.A01.BXh(view);
        }
        this.A01.AP8(view, A002, layoutParams);
    }

    public void A05(View view, int i, boolean z) {
        int A002;
        if (i < 0) {
            A002 = this.A01.Ah4();
        } else {
            A002 = A00(this, i);
        }
        this.A00.A05(A002, z);
        if (z) {
            this.A02.add(view);
            this.A01.BXh(view);
        }
        this.A01.ANr(view, A002);
    }

    public String toString() {
        return AnonymousClass08S.A0L(this.A00.toString(), ", hidden list:", this.A02.size());
    }

    public C16220wh(C16240wj r2) {
        this.A01 = r2;
    }

    public View A03(int i) {
        return this.A01.Ah1(A00(this, i));
    }
}
