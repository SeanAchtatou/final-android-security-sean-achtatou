package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1L0  reason: invalid class name */
public final class AnonymousClass1L0 extends AnonymousClass0UV {
    private static volatile AnonymousClass1L1 A00;

    public static final AnonymousClass1L1 A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass1L1.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C17420ys();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
