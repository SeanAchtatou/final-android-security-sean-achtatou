package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.GroupApprovalInfo;

/* renamed from: X.0zz  reason: invalid class name and case insensitive filesystem */
public final class C18050zz implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new GroupApprovalInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new GroupApprovalInfo[i];
    }
}
