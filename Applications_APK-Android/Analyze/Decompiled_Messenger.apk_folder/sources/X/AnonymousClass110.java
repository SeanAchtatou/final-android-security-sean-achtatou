package X;

import com.facebook.graphql.enums.GraphQLMessengerDiscoverTabItemMetalineContentType;
import com.facebook.graphql.enums.GraphQLMessengerDiscoverTabItemType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.google.common.collect.ImmutableList;

/* renamed from: X.110  reason: invalid class name */
public interface AnonymousClass110 extends C12200oo {
    GSTModelShape1S0000000 AdK();

    C35961s6 Afi();

    String Agm();

    C35961s6 Ajq();

    GSTModelShape1S0000000 AmG();

    C29331EVh Amx();

    String Amy();

    GSTModelShape1S0000000 ApL();

    boolean Aqo();

    boolean Aqs();

    GraphQLMessengerDiscoverTabItemType Ard();

    GSTModelShape1S0000000 AzR();

    String B27();

    GraphQLMessengerDiscoverTabItemMetalineContentType B28();

    ImmutableList B4W();

    GSTModelShape1S0000000 B5L();

    String B6a();
}
