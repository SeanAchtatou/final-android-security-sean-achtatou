package X;

import android.os.Build;
import android.view.View;
import android.view.WindowManager;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1do  reason: invalid class name and case insensitive filesystem */
public final class C27821do {
    public Object A00;
    public Field A01;
    public Field A02;
    public boolean A03;

    public List A00() {
        Field field;
        List list;
        List list2;
        String str;
        String str2;
        if (!this.A03) {
            this.A03 = true;
            int i = Build.VERSION.SDK_INT;
            if (i > 16) {
                str = "android.view.WindowManagerGlobal";
            } else {
                str = "android.view.WindowManagerImpl";
            }
            if (i > 16) {
                str2 = "getInstance";
            } else {
                str2 = "getDefault";
            }
            try {
                Class<?> cls = Class.forName(str);
                this.A00 = cls.getMethod(str2, new Class[0]).invoke(null, new Object[0]);
                Field declaredField = cls.getDeclaredField("mViews");
                this.A02 = declaredField;
                declaredField.setAccessible(true);
                Field declaredField2 = cls.getDeclaredField("mParams");
                this.A01 = declaredField2;
                declaredField2.setAccessible(true);
            } catch (InvocationTargetException e) {
                e.getCause();
            } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | RuntimeException unused) {
            }
        }
        Object obj = this.A00;
        if (obj == null || (field = this.A02) == null || this.A01 == null) {
            return null;
        }
        try {
            if (Build.VERSION.SDK_INT < 19) {
                list = Arrays.asList((View[]) field.get(obj));
                list2 = Arrays.asList((WindowManager.LayoutParams[]) this.A01.get(this.A00));
            } else {
                list = (List) field.get(obj);
                list2 = (List) this.A01.get(this.A00);
            }
            ArrayList arrayList = new ArrayList();
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                list2.get(i2);
                arrayList.add(new C13070qb((View) list.get(i2)));
            }
            return arrayList;
        } catch (IllegalAccessException | RuntimeException unused2) {
            return null;
        }
    }
}
