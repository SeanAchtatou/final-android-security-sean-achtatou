package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.payments.checkout.configuration.model.PriceSelectorFixedAmountModel;

/* renamed from: X.20C  reason: invalid class name */
public final class AnonymousClass20C implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new PriceSelectorFixedAmountModel(parcel);
    }

    public Object[] newArray(int i) {
        return new PriceSelectorFixedAmountModel[i];
    }
}
