package X;

import com.facebook.stash.core.Stash;
import java.io.File;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: X.1j3  reason: invalid class name and case insensitive filesystem */
public final class C31041j3 implements Stash {
    private final C31051j4 A00;
    private final File A01;

    public Set getAllKeys() {
        int length;
        String[] list = this.A01.list();
        if (list == null || (length = list.length) == 0) {
            return Collections.emptySet();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet(length);
        for (String A002 : list) {
            linkedHashSet.add(C35681rc.A00(A002));
        }
        return linkedHashSet;
    }

    public File getResourcePath(String str) {
        File file = this.A01;
        char[] charArray = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : charArray) {
            if (c == '%' || C35681rc.A00.contains(Character.valueOf(c))) {
                sb.append('%');
                sb.append(Integer.toHexString(c));
            } else {
                sb.append(c);
            }
        }
        return new File(file, sb.toString());
    }

    public long getSizeBytes() {
        return C181778cE.A01(this.A01).A00;
    }

    public boolean remove(String str) {
        return this.A00.A02(getResourcePath(str));
    }

    public boolean removeAll() {
        if (!this.A00.A02(this.A01)) {
            return false;
        }
        this.A01.mkdirs();
        return true;
    }

    public C31041j3(File file, C31051j4 r2) {
        this.A01 = file;
        this.A00 = r2;
    }

    public File getResource(String str) {
        File resourcePath = getResourcePath(str);
        resourcePath.setLastModified(System.currentTimeMillis());
        return resourcePath;
    }

    public boolean hasKey(String str) {
        return getResourcePath(str).exists();
    }

    public File insert(String str) {
        File resourcePath = getResourcePath(str);
        resourcePath.setLastModified(System.currentTimeMillis());
        return resourcePath;
    }
}
