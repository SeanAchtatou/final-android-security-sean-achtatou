package X;

import com.facebook.messaging.model.threads.AdsConversionsQPData;
import io.card.payment.BuildConfig;

/* renamed from: X.1gc  reason: invalid class name and case insensitive filesystem */
public final class C29561gc {
    public double A00;
    public long A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public boolean A07;

    public C29561gc() {
        this.A02 = "NONE";
        this.A03 = BuildConfig.FLAVOR;
    }

    public C29561gc(AdsConversionsQPData adsConversionsQPData) {
        C28931fb.A05(adsConversionsQPData);
        if (adsConversionsQPData instanceof AdsConversionsQPData) {
            this.A02 = adsConversionsQPData.A02;
            this.A00 = adsConversionsQPData.A00;
            this.A03 = adsConversionsQPData.A03;
        } else {
            String str = adsConversionsQPData.A02;
            this.A02 = str;
            C28931fb.A06(str, "conversionType");
            this.A00 = adsConversionsQPData.A00;
            String str2 = adsConversionsQPData.A03;
            this.A03 = str2;
            C28931fb.A06(str2, "currencyCode");
        }
        this.A04 = adsConversionsQPData.A04;
        this.A05 = adsConversionsQPData.A05;
        this.A07 = adsConversionsQPData.A07;
        this.A06 = adsConversionsQPData.A06;
        this.A01 = adsConversionsQPData.A01;
    }
}
