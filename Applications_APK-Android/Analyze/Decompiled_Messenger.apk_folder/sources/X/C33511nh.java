package X;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;

/* renamed from: X.1nh  reason: invalid class name and case insensitive filesystem */
public final class C33511nh extends ShapeDrawable.ShaderFactory {
    private AnonymousClass5XY A00;
    private final int A01;
    private final int A02;
    private final BitmapShader A03;
    private final Matrix A04 = new Matrix();
    private final RectF A05 = new RectF();

    public Shader resize(int i, int i2) {
        float f;
        float f2;
        float f3;
        AnonymousClass5XY r1 = this.A00;
        if (r1 != null) {
            r1.AxT(this.A05);
            f = this.A05.width();
            f2 = this.A05.height();
        } else {
            f = (float) i;
            f2 = (float) i2;
        }
        float f4 = (float) this.A02;
        float f5 = (float) this.A01;
        if (f4 / f5 >= f / f2) {
            f3 = f2 / f5;
        } else {
            f3 = f / f5;
        }
        this.A04.reset();
        this.A04.postTranslate((float) ((-this.A02) >> 1), (float) ((-this.A01) >> 1));
        this.A04.postScale(f3, f3);
        Matrix matrix = this.A04;
        RectF rectF = this.A05;
        matrix.postTranslate(rectF.left + (f / 2.0f), rectF.top + (f2 / 2.0f));
        this.A03.setLocalMatrix(this.A04);
        return this.A03;
    }

    public C33511nh(AnonymousClass5XY r3, Bitmap bitmap) {
        this.A00 = r3;
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        this.A03 = new BitmapShader(bitmap, tileMode, tileMode);
        this.A02 = bitmap.getWidth();
        this.A01 = bitmap.getHeight();
    }
}
