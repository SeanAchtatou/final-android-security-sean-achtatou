package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.08n  reason: invalid class name and case insensitive filesystem */
public final class C010108n {
    public static final long[] A00 = new long[AnonymousClass07B.A00(13).length];
    public static final String[] A01 = new String[AnonymousClass07B.A00(13).length];

    static {
        String str;
        Integer[] A002 = AnonymousClass07B.A00(13);
        int length = A002.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            Integer num = A002[i];
            String[] strArr = A01;
            int i3 = i2 + 1;
            switch (num.intValue()) {
                case 1:
                    str = "MemFree:";
                    break;
                case 2:
                    str = "Buffers:";
                    break;
                case 3:
                    str = "Cached:";
                    break;
                case 4:
                    str = "Dirty:";
                    break;
                case 5:
                    str = "Writeback:";
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                    str = "AnonPages:";
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    str = "Shmem:";
                    break;
                case 8:
                    str = "Slab:";
                    break;
                case Process.SIGKILL /*9*/:
                    str = "KernelStack:";
                    break;
                case AnonymousClass1Y3.A01 /*10*/:
                    str = "PageTables:";
                    break;
                case AnonymousClass1Y3.A02 /*11*/:
                    str = "Bounce:";
                    break;
                case AnonymousClass1Y3.A03 /*12*/:
                    str = "WritebackTmp:";
                    break;
                default:
                    str = "MemTotal:";
                    break;
            }
            strArr[i2] = str;
            i++;
            i2 = i3;
        }
    }
}
