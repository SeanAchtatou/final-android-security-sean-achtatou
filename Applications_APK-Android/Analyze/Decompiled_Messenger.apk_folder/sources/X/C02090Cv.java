package X;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.FragmentState;

/* renamed from: X.0Cv  reason: invalid class name and case insensitive filesystem */
public final class C02090Cv implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new FragmentState(parcel);
    }

    public Object[] newArray(int i) {
        return new FragmentState[i];
    }
}
