package X;

import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;

/* renamed from: X.06h  reason: invalid class name and case insensitive filesystem */
public final class C006206h {
    public static final Uri A00 = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "display_photo");

    public static Uri A00(int i) {
        return new Uri.Builder().scheme("res").path(String.valueOf(i)).build();
    }

    public static Uri A01(String str) {
        if (str != null) {
            return Uri.parse(str);
        }
        return null;
    }

    public static boolean A03(Uri uri) {
        String scheme;
        if (uri == null) {
            scheme = null;
        } else {
            scheme = uri.getScheme();
        }
        return "content".equals(scheme);
    }

    public static boolean A04(Uri uri) {
        String scheme;
        if (uri == null) {
            scheme = null;
        } else {
            scheme = uri.getScheme();
        }
        return "file".equals(scheme);
    }

    public static boolean A05(Uri uri) {
        String scheme;
        if (uri == null) {
            scheme = null;
        } else {
            scheme = uri.getScheme();
        }
        return "res".equals(scheme);
    }

    public static boolean A06(Uri uri) {
        String scheme;
        if (uri == null) {
            scheme = null;
        } else {
            scheme = uri.getScheme();
        }
        if ("https".equals(scheme) || "http".equals(scheme)) {
            return true;
        }
        return false;
    }

    public static boolean A02(Uri uri) {
        String uri2 = uri.toString();
        if (uri2.startsWith(MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString()) || uri2.startsWith(MediaStore.Images.Media.INTERNAL_CONTENT_URI.toString())) {
            return true;
        }
        return false;
    }
}
