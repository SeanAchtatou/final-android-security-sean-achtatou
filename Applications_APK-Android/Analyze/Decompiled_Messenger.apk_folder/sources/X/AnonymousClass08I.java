package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONObject;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.08I  reason: invalid class name */
public final class AnonymousClass08I extends C01860Bx {
    public synchronized JSONObject A01(boolean z, boolean z2) {
        JSONObject jSONObject;
        jSONObject = new JSONObject();
        long j = 0;
        for (Map.Entry entry : this.A01.entrySet()) {
            switch (((AnonymousClass08J) entry.getKey()).ordinal()) {
                case 0:
                case 1:
                    j += ((AtomicLong) entry.getValue()).longValue();
                    break;
                case 5:
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case 8:
                case Process.SIGKILL /*9*/:
                case AnonymousClass1Y3.A01 /*10*/:
                case AnonymousClass1Y3.A02 /*11*/:
                case AnonymousClass1Y3.A03 /*12*/:
                case 13:
                    if (z) {
                        jSONObject.putOpt(((AnonymousClass0C4) entry.getKey()).Arl(), Long.valueOf(((AtomicLong) entry.getValue()).getAndSet(0)));
                        continue;
                    } else {
                        jSONObject.putOpt(((AnonymousClass0C4) entry.getKey()).Arl(), entry.getValue());
                    }
            }
            jSONObject.putOpt(((AnonymousClass0C4) entry.getKey()).Arl(), entry.getValue());
        }
        jSONObject.putOpt(AnonymousClass08J.A0A.Arl(), Long.valueOf(j));
        return jSONObject;
    }

    public AnonymousClass08I() {
        super("lc");
    }
}
