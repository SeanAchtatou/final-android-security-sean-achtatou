package X;

import java.util.Comparator;

/* renamed from: X.226  reason: invalid class name */
public final class AnonymousClass226 implements Comparator {
    public int compare(Object obj, Object obj2) {
        return ((C403921k) obj).A0C() - ((C403921k) obj2).A0C();
    }
}
