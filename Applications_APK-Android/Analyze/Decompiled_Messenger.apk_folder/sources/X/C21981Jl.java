package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1Jl  reason: invalid class name and case insensitive filesystem */
public final class C21981Jl extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public MigColorScheme A01;
    @Comparable(type = 13)
    public Integer A02;

    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "SENT";
            case 2:
                return "DELIVERED";
            case 3:
                return "ERROR";
            default:
                return "SENDING";
        }
    }

    public C21981Jl(Context context) {
        super("M4ThreadItemSendStateComponent");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
