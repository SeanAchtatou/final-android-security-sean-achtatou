package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.1j5  reason: invalid class name and case insensitive filesystem */
public final class C31061j5 {
    public File A00 = new File(this.A01, "cache");
    public File A01;
    public File A02 = new File(this.A01, "files");
    public File A03 = new File(this.A00, "attributed_store");
    public File A04;

    public static void A00(File file, File file2) {
        if (file2 != null && file2.exists()) {
            if (file.exists()) {
                AnonymousClass9AZ.A02(file2);
                return;
            }
            File parentFile = file.getParentFile();
            if (!parentFile.isDirectory()) {
                parentFile.mkdirs();
            }
            file2.renameTo(file);
        }
    }

    public C31061j5(Context context) {
        File file = new File(context.getApplicationInfo().dataDir);
        this.A01 = file;
        this.A04 = new File(file, "attributed_store");
    }
}
