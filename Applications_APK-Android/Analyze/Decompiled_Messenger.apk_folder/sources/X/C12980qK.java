package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

/* renamed from: X.0qK  reason: invalid class name and case insensitive filesystem */
public class C12980qK implements C11450nB {
    public Activity A00;
    public C13080qc A01;

    public void A07() {
        this.A01.onStop();
    }

    public void A08(Bundle bundle) {
        this.A01.BNB(bundle);
    }

    public void A09() {
        this.A01.onBackPressed();
    }

    public void A0A() {
        this.A01.onActivityDestroy();
    }

    public void A0B() {
        this.A01.onPause();
    }

    public void A0C() {
        this.A01.onResume();
    }

    public void A0D() {
        this.A01.onStart();
    }

    public void A0E(Intent intent) {
        this.A01.BNF(intent);
    }

    public void A0F(Bundle bundle) {
        this.A01.Bmx(bundle);
    }

    public void A0G(Fragment fragment) {
        this.A01.BOX(fragment);
    }

    public C13060qW B4m() {
        return this.A01.B4m();
    }

    public void CIW() {
        this.A01.CIW();
    }
}
