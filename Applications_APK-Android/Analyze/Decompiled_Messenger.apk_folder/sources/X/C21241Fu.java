package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableEntry;
import java.util.Map;

/* renamed from: X.1Fu  reason: invalid class name and case insensitive filesystem */
public final class C21241Fu extends C21251Fv {
    public final /* synthetic */ AnonymousClass0r1 A00;
    public final /* synthetic */ Map.Entry A01;

    public C21241Fu(AnonymousClass0r1 r1, Map.Entry entry) {
        this.A00 = r1;
        this.A01 = entry;
    }

    public Object setValue(Object obj) {
        Preconditions.checkArgument(this.A00.A00.A00.A00.apply(new ImmutableEntry(getKey(), obj)));
        return super.setValue(obj);
    }
}
