package X;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.FragmentManagerState;

/* renamed from: X.0Cx  reason: invalid class name and case insensitive filesystem */
public final class C02100Cx implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new FragmentManagerState(parcel);
    }

    public Object[] newArray(int i) {
        return new FragmentManagerState[i];
    }
}
