package X;

import android.content.res.Resources;
import com.facebook.auth.userscope.UserScoped;
import java.util.regex.Pattern;

@UserScoped
/* renamed from: X.0tz  reason: invalid class name and case insensitive filesystem */
public final class C14770tz {
    private static C05540Zi A07;
    public static final Pattern A08 = Pattern.compile("\\n+");
    public AnonymousClass0UN A00;
    public final Resources A01;
    public final C29761gw A02 = new C29761gw(20);
    public final C26681bq A03;
    public final C14780u1 A04;
    public final C22351Kz A05;
    public final C04310Tq A06;

    public static final C14770tz A00(AnonymousClass1XY r4) {
        C14770tz r0;
        synchronized (C14770tz.class) {
            C05540Zi A002 = C05540Zi.A00(A07);
            A07 = A002;
            try {
                if (A002.A03(r4)) {
                    A07.A00 = new C14770tz((AnonymousClass1XY) A07.A01());
                }
                C05540Zi r1 = A07;
                r0 = (C14770tz) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A07.A02();
                throw th;
            }
        }
        return r0;
    }

    private C14770tz(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A03 = C26681bq.A00(r3);
        this.A05 = C22341Ky.A00(r3);
        this.A06 = AnonymousClass0XJ.A0I(r3);
        this.A04 = C14780u1.A00(r3);
        this.A01 = C04490Ux.A0L(r3);
        AnonymousClass1MX.A02(r3);
        AnonymousClass0UU.A08(r3);
    }
}
