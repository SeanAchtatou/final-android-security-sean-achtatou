package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1Jy  reason: invalid class name and case insensitive filesystem */
public final class C22111Jy extends Enum {
    public static final C22111Jy[] A00 = values();
    private static final /* synthetic */ C22111Jy[] A01;
    public static final C22111Jy A02;
    public static final C22111Jy A03;
    public static final C22111Jy A04;
    public static final C22111Jy A05;
    public static final C22111Jy A06;
    public final C21381Gs tileBadge;

    static {
        C22111Jy r3 = new C22111Jy("ACTIVE_NOW", 0, C21381Gs.A01);
        A02 = r3;
        C22111Jy r4 = new C22111Jy("SMS", 1, C21381Gs.A0R);
        A06 = r4;
        C22111Jy r5 = new C22111Jy("TINCAN", 2, C21381Gs.A0T);
        C22111Jy r6 = new C22111Jy("RECENTLY_ACTIVE", 3, C21381Gs.A0Q);
        A05 = r6;
        C22111Jy r7 = new C22111Jy("KOALA_MODE", 4, C21381Gs.A0G);
        C22111Jy r8 = new C22111Jy("ALOHA_HOME", 5, C21381Gs.A02);
        A03 = r8;
        C22111Jy r9 = new C22111Jy("WORK_MCC_EXTERNAL_USER", 6, C21381Gs.A0Y);
        C22111Jy r10 = new C22111Jy("WORK_DND_STATUS", 7, C21381Gs.A0X);
        C22111Jy r11 = new C22111Jy("NONE", 8, C21381Gs.A0L);
        A04 = r11;
        A01 = new C22111Jy[]{r3, r4, r5, r6, r7, r8, r9, r10, r11};
    }

    public static C22111Jy[] values() {
        return (C22111Jy[]) A01.clone();
    }

    private C22111Jy(String str, int i, C21381Gs r3) {
        this.tileBadge = r3;
    }
}
