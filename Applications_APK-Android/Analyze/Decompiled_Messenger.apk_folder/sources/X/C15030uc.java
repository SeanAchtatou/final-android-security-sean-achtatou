package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/* renamed from: X.0uc  reason: invalid class name and case insensitive filesystem */
public final class C15030uc implements Application.ActivityLifecycleCallbacks {
    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        C15040ud.A01(activity);
    }

    public void onActivityDestroyed(Activity activity) {
        C15040ud.A02(activity);
    }
}
