package X;

import android.content.Context;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0vn  reason: invalid class name and case insensitive filesystem */
public final class C15720vn extends C15300v3 implements C15690vh {
    private final int A00;
    private final AtomicInteger A01 = new AtomicInteger(0);

    public final Object A00() {
        throw new UnsupportedOperationException("Call acquire(ComponentContext, ComponentLifecycle)");
    }

    public void BKc(Context context, C17780zS r5) {
        boolean z = false;
        if (this.A00 >= this.A01) {
            z = true;
        }
        if (!z && this.A01.getAndIncrement() < this.A00) {
            C0t(r5.A0V(context));
        }
    }

    public C15720vn(int i, boolean z) {
        super(i, z);
        this.A00 = i;
    }

    public Object ALb(Context context, C17780zS r3) {
        Object A002 = super.A00();
        if (A002 != null) {
            return A002;
        }
        this.A01.incrementAndGet();
        return r3.A0V(context);
    }
}
