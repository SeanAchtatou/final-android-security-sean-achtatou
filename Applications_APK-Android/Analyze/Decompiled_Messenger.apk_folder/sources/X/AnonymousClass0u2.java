package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.Set;

@UserScoped
/* renamed from: X.0u2  reason: invalid class name */
public final class AnonymousClass0u2 {
    private static C05540Zi A01;
    public final Set A00;

    public static final AnonymousClass0u2 A00(AnonymousClass1XY r4) {
        AnonymousClass0u2 r0;
        synchronized (AnonymousClass0u2.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new AnonymousClass0u2((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass0u2) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass0u2(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0X5(r3, AnonymousClass0X6.A1X);
    }
}
