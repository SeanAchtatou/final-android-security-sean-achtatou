package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1sk  reason: invalid class name and case insensitive filesystem */
public final class C36241sk {
    public final byte A00;
    public final String A01;
    public final short A02;

    public String toString() {
        return "<TField name:'" + this.A01 + "' type:" + ((int) this.A00) + " field-id:" + ((int) this.A02) + ">";
    }

    public C36241sk() {
        this(BuildConfig.FLAVOR, (byte) 0, 0);
    }

    public C36241sk(String str, byte b, short s) {
        this.A01 = str;
        this.A00 = b;
        this.A02 = s;
    }
}
