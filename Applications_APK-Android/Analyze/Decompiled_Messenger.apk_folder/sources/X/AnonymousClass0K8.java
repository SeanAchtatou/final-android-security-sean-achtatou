package X;

import java.io.InputStream;

/* renamed from: X.0K8  reason: invalid class name */
public final class AnonymousClass0K8 extends InputStream {
    private boolean A00 = false;
    private final InputStream A01;

    public void close() {
        this.A01.close();
    }

    public AnonymousClass0K8(InputStream inputStream) {
        this.A01 = inputStream;
    }

    public int read() {
        if (this.A00) {
            return -1;
        }
        int read = this.A01.read();
        if (read != 0) {
            return read;
        }
        this.A00 = true;
        return -1;
    }

    public int read(byte[] bArr, int i, int i2) {
        if (!this.A00) {
            int read = this.A01.read(bArr, i, i2);
            if (read == -1) {
                this.A00 = true;
                return -1;
            }
            int i3 = 0;
            while (i3 < read) {
                if (bArr[i + i3] == 0) {
                    this.A00 = true;
                    if (i3 != 0) {
                        return i3;
                    }
                } else {
                    i3++;
                }
            }
            return read;
        }
        return -1;
    }
}
