package X;

import android.content.Intent;
import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.graphservice.interfaces.Summary;
import com.facebook.messaging.inbox2.items.InboxTrackableItem;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0wS  reason: invalid class name and case insensitive filesystem */
public abstract class C16080wS implements C17790zT, C17800zU {
    public Object A0B(C16070wR r2) {
        if (this instanceof C20271Bt) {
            return ((C20271Bt) r2).A07;
        }
        if (!(this instanceof C20291Bv)) {
            return null;
        }
        return ((C20291Bv) r2).A0A;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
        if (r0 == false) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0C(X.AnonymousClass1GA r26) {
        /*
            r25 = this;
            r1 = r25
            boolean r0 = r1 instanceof X.C20271Bt
            r13 = r26
            if (r0 != 0) goto L_0x018c
            boolean r0 = r1 instanceof X.C20291Bv
            if (r0 == 0) goto L_0x01c3
            X.1Bv r1 = (X.C20291Bv) r1
            X.3iN r12 = r1.A0A
            X.AmJ r0 = r1.A05
            r24 = r0
            java.lang.String r11 = r1.A0E
            java.lang.Object r10 = r1.A0D
            long r3 = r1.A03
            X.Alj r9 = r1.A06
            X.3ik r0 = r1.A09
            X.3iM r8 = r0.connectionHandler
            X.EfB r7 = r0.serviceListener
            X.3iZ r2 = r0.connectionData
            X.7Ne r6 = r0.fetchState
            java.lang.Throwable r5 = r0.fetchError
            java.lang.String r1 = r0.lastLocalCacheScopeUsed
            if (r12 == 0) goto L_0x01c3
            boolean r0 = r11.equals(r1)
            r17 = r0 ^ 1
            if (r17 == 0) goto L_0x003c
            r0 = 0
            if (r1 == 0) goto L_0x0038
            r0 = 1
        L_0x0038:
            r16 = 1
            if (r0 != 0) goto L_0x003e
        L_0x003c:
            r16 = 0
        L_0x003e:
            if (r16 == 0) goto L_0x0084
            monitor-enter(r12)
            X.3hz r15 = X.C74723iN.A00(r12)     // Catch:{ all -> 0x007d }
            if (r15 != 0) goto L_0x0058
            X.0US r0 = r12.A0B     // Catch:{ all -> 0x007d }
            java.lang.Object r14 = r0.get()     // Catch:{ all -> 0x007d }
            X.09P r14 = (X.AnonymousClass09P) r14     // Catch:{ all -> 0x007d }
            java.lang.String r1 = "GraphQLConnectionService"
            java.lang.String r0 = "You must invoke connect() first!"
            r14.CGS(r1, r0)     // Catch:{ all -> 0x007d }
            monitor-exit(r12)     // Catch:{ all -> 0x007d }
            goto L_0x0084
        L_0x0058:
            monitor-exit(r12)     // Catch:{ all -> 0x007d }
            X.AmF r14 = r12.A0A
            java.lang.Object r1 = r15.A0P
            monitor-enter(r1)
            X.AmC r0 = r15.A0K     // Catch:{ all -> 0x0079 }
            if (r14 == 0) goto L_0x0073
            X.0bK r0 = r0.A00     // Catch:{ all -> 0x0079 }
            r0.A04(r14)     // Catch:{ all -> 0x0079 }
            java.util.Set r0 = r15.A0T     // Catch:{ all -> 0x0079 }
            r0.remove(r14)     // Catch:{ all -> 0x0079 }
            java.util.Set r0 = r15.A0S     // Catch:{ all -> 0x0079 }
            r0.add(r14)     // Catch:{ all -> 0x0079 }
            monitor-exit(r1)     // Catch:{ all -> 0x0079 }
            goto L_0x0081
        L_0x0073:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ all -> 0x0079 }
            r0.<init>()     // Catch:{ all -> 0x0079 }
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x0079:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0079 }
            goto L_0x0185
        L_0x007d:
            r0 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x007d }
            goto L_0x0185
        L_0x0081:
            r15.A06()
        L_0x0084:
            if (r16 == 0) goto L_0x008a
            if (r9 != 0) goto L_0x009e
            X.3iZ r2 = X.C74833iZ.A06
        L_0x008a:
            if (r16 == 0) goto L_0x008e
            X.7Ne r6 = X.C156877Ne.INITIAL_STATE
        L_0x008e:
            if (r16 == 0) goto L_0x0091
            r5 = 0
        L_0x0091:
            java.lang.Object[] r1 = new java.lang.Object[]{r13, r2, r6, r5}
            r0 = 6759691(0x67250b, float:9.472345E-39)
            X.10N r0 = A02(r13, r0, r1)
            monitor-enter(r7)
            goto L_0x00b9
        L_0x009e:
            X.3iZ r2 = new X.3iZ
            com.google.common.collect.ImmutableList r14 = r9.A00
            boolean r1 = r9.A04
            boolean r0 = r9.A03
            X.3hy r23 = new X.3hy
            r23.<init>()
            r20 = 0
            r18 = r2
            r19 = r14
            r21 = r1
            r22 = r0
            r18.<init>(r19, r20, r21, r22, r23)
            goto L_0x008a
        L_0x00b9:
            r7.A00 = r0     // Catch:{ all -> 0x0189 }
            monitor-exit(r7)
            monitor-enter(r12)
            r12.A01 = r7     // Catch:{ all -> 0x0186 }
            monitor-exit(r12)
            if (r17 == 0) goto L_0x0160
            X.0wR r0 = r13.A0K()
            if (r0 == 0) goto L_0x00d6
            X.2yh r2 = new X.2yh
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            java.lang.Object[] r0 = new java.lang.Object[]{r11}
            r2.<init>(r1, r0)
            r13.A0E(r2)
        L_0x00d6:
            X.3x4 r1 = r12.A09
            X.Am5 r5 = new X.Am5
            int r0 = X.AnonymousClass1Y3.AQj
            X.0UQ r0 = X.AnonymousClass0UQ.A00(r0, r1)
            r13 = r5
            r14 = r11
            r15 = r24
            r16 = r0
            r13.<init>(r14, r15, r16)
            r5.A03 = r3
            r5.A04 = r9
            X.0US r0 = r5.A06
            java.lang.Object r4 = r0.get()
            X.3hz r4 = (X.C74513hz) r4
            java.lang.Object r6 = r4.A0R
            monitor-enter(r6)
            boolean r0 = r4.A0C     // Catch:{ all -> 0x0154 }
            if (r0 != 0) goto L_0x0122
            X.AmK r0 = r4.A0J     // Catch:{ all -> 0x0154 }
            boolean r0 = r0.A02()     // Catch:{ all -> 0x0154 }
            if (r0 != 0) goto L_0x0122
            r0 = 1
            r4.A0C = r0     // Catch:{ all -> 0x0154 }
            com.facebook.quicklog.QuickPerformanceLogger r1 = r4.A0O     // Catch:{ all -> 0x0154 }
            int r0 = r4.A0F     // Catch:{ all -> 0x0154 }
            r3 = 8716323(0x850023, float:1.221417E-38)
            r1.markerStart(r3, r0)     // Catch:{ all -> 0x0154 }
            com.facebook.quicklog.QuickPerformanceLogger r2 = r4.A0O     // Catch:{ all -> 0x0154 }
            int r1 = r4.A0F     // Catch:{ all -> 0x0154 }
            java.lang.String r0 = r5.A07     // Catch:{ all -> 0x0154 }
            r2.markerTag(r3, r1, r0)     // Catch:{ all -> 0x0154 }
            X.Am4 r0 = new X.Am4     // Catch:{ all -> 0x0154 }
            r0.<init>(r4, r5)     // Catch:{ all -> 0x0154 }
            X.C74513hz.A04(r4, r0)     // Catch:{ all -> 0x0154 }
        L_0x0122:
            monitor-exit(r6)     // Catch:{ all -> 0x0154 }
            monitor-enter(r12)
            java.util.concurrent.atomic.AtomicBoolean r1 = r12.A03     // Catch:{ all -> 0x0186 }
            r0 = 0
            r1.set(r0)     // Catch:{ all -> 0x0186 }
            r12.A00 = r4     // Catch:{ all -> 0x0186 }
            X.AmF r3 = r12.A0A     // Catch:{ all -> 0x0186 }
            java.lang.Object r1 = r4.A0P     // Catch:{ all -> 0x0186 }
            monitor-enter(r1)     // Catch:{ all -> 0x0186 }
            java.util.Set r0 = r4.A0S     // Catch:{ all -> 0x0151 }
            r0.remove(r3)     // Catch:{ all -> 0x0151 }
            monitor-exit(r1)     // Catch:{ all -> 0x0151 }
            X.AmK r0 = r4.A0J     // Catch:{ all -> 0x0186 }
            boolean r0 = r0.A02()     // Catch:{ all -> 0x0186 }
            if (r0 != 0) goto L_0x0157
            java.lang.Object r2 = r4.A0Q     // Catch:{ all -> 0x0186 }
            monitor-enter(r2)     // Catch:{ all -> 0x0186 }
            X.3hy r1 = r4.A05     // Catch:{ all -> 0x014e }
            X.AmE r0 = new X.AmE     // Catch:{ all -> 0x014e }
            r0.<init>(r4, r3, r1)     // Catch:{ all -> 0x014e }
            X.C74513hz.A05(r4, r0)     // Catch:{ all -> 0x014e }
            monitor-exit(r2)     // Catch:{ all -> 0x014e }
            goto L_0x0157
        L_0x014e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x014e }
            goto L_0x0153
        L_0x0151:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0151 }
        L_0x0153:
            throw r0     // Catch:{ all -> 0x0186 }
        L_0x0154:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0154 }
            goto L_0x0185
        L_0x0157:
            monitor-exit(r12)
            X.EfD r0 = new X.EfD
            r0.<init>(r12, r10)
            X.C74723iN.A03(r12, r0)
        L_0x0160:
            monitor-enter(r12)
            X.EfB r0 = r12.A01     // Catch:{ all -> 0x0183 }
            if (r0 == 0) goto L_0x016f
            java.util.List r0 = r12.A0C     // Catch:{ all -> 0x0183 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0183 }
            if (r0 != 0) goto L_0x016f
            monitor-exit(r12)     // Catch:{ all -> 0x0183 }
            goto L_0x0171
        L_0x016f:
            monitor-exit(r12)     // Catch:{ all -> 0x0183 }
            goto L_0x0179
        L_0x0171:
            X.Ef9 r0 = new X.Ef9
            r0.<init>(r12)
            X.C74723iN.A03(r12, r0)
        L_0x0179:
            if (r8 == 0) goto L_0x01c3
            monitor-enter(r8)
            r8.A00 = r12     // Catch:{ all -> 0x0180 }
            monitor-exit(r8)
            return
        L_0x0180:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x0183:
            r0 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x0183 }
        L_0x0185:
            throw r0
        L_0x0186:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x0189:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x018c:
            r2 = r1
            X.1Bt r2 = (X.C20271Bt) r2
            X.7NE r3 = r2.A07
            X.6RZ r0 = r2.A06
            X.7ND r1 = r0.serviceListener
            X.BiW r0 = r2.A01
            if (r3 == 0) goto L_0x01c3
            monitor-enter(r1)
            r1.A01 = r13     // Catch:{ all -> 0x01c0 }
            monitor-exit(r1)
            monitor-enter(r1)
            r1.A00 = r0     // Catch:{ all -> 0x01c0 }
            monitor-exit(r1)
            r3.A02(r1)
            monitor-enter(r3)
            boolean r0 = r3.A00     // Catch:{ all -> 0x01bd }
            r0 = r0 ^ 1
            monitor-exit(r3)
            if (r0 == 0) goto L_0x01c3
            r2 = 0
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 0
            A05(r13, r2, r1, r0)
            X.0gx r0 = r3.A03
            if (r0 != 0) goto L_0x01b9
            X.0gx r0 = X.C09290gx.FULLY_CACHED
        L_0x01b9:
            X.AnonymousClass7NE.A00(r3, r0)
            return
        L_0x01bd:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x01c0:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x01c3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16080wS.A0C(X.1GA):void");
    }

    public void A0D(AnonymousClass1GA r16) {
        if (this instanceof C20271Bt) {
            C20271Bt r5 = (C20271Bt) this;
            AnonymousClass653 r8 = r5.A05;
            C09290gx r10 = r5.A02;
            long j = r5.A00;
            C04310Tq r4 = r5.A08;
            C04310Tq r3 = r5.A09;
            AnonymousClass7NO r7 = (AnonymousClass7NO) r4.get();
            r3.get();
            r5.A07 = new AnonymousClass7NE(r7, r8, (QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r5.A03), r10, j, null, null);
        } else if (this instanceof C20291Bv) {
            C20291Bv r32 = (C20291Bv) this;
            int i = r32.A00;
            int i2 = r32.A02;
            C83103x5 r1 = (C83103x5) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APF, r32.A07);
            if (i == -1) {
                i = i2;
            }
            r32.A0A = new C74723iN(i, i2, AnonymousClass0Uc.A00(), AnonymousClass0UX.A07(r1), new C83093x4(r1), AnonymousClass0UQ.A00(AnonymousClass1Y3.Amr, r1));
        }
    }

    public void A0E(AnonymousClass1GA r8) {
        if (this instanceof C20271Bt) {
            C20271Bt r0 = (C20271Bt) this;
            AnonymousClass7NE r2 = r0.A07;
            AnonymousClass7ND r1 = r0.A06.serviceListener;
            if (r2 != null) {
                synchronized (r1) {
                    r1.A01 = null;
                }
                synchronized (r1) {
                    r1.A00 = null;
                }
                r2.A02(null);
            }
        } else if (this instanceof C20291Bv) {
            C20291Bv r12 = (C20291Bv) this;
            C74723iN r6 = r12.A0A;
            C74913ik r02 = r12.A09;
            C29652EfB efB = r02.serviceListener;
            C74713iM r4 = r02.connectionHandler;
            AnonymousClass09P r3 = (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r12.A07);
            if (r6 == null) {
                AnonymousClass06G A02 = AnonymousClass06F.A02("BaseGraphQLConnectionSection", "null service param in onUnbindService()");
                A02.A03 = new IllegalArgumentException(C99084oO.$const$string(AnonymousClass1Y3.A5u));
                r3.CGQ(A02.A00());
                return;
            }
            synchronized (efB) {
                efB.A00 = null;
            }
            synchronized (r6) {
                r6.A01 = null;
            }
            if (r4 != null) {
                synchronized (r4) {
                    r4.A00 = null;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:72:0x013f A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(X.AnonymousClass1GA r19, X.AnonymousClass1CW r20, X.C16070wR r21, X.C16070wR r22) {
        /*
            r18 = this;
            r0 = r18
            r4 = r21
            r6 = r22
            boolean r0 = r0 instanceof X.AnonymousClass1GV
            if (r0 == 0) goto L_0x0057
            X.1GV r4 = (X.AnonymousClass1GV) r4
            X.1GV r6 = (X.AnonymousClass1GV) r6
            X.1mx r11 = new X.1mx
            r5 = 0
            if (r4 != 0) goto L_0x01ac
            r1 = r5
        L_0x0014:
            if (r6 != 0) goto L_0x01a8
            r0 = r5
        L_0x0017:
            r11.<init>(r1, r0)
            X.1mx r3 = new X.1mx
            if (r4 != 0) goto L_0x01a4
            r1 = r5
        L_0x001f:
            if (r6 != 0) goto L_0x01a0
            r0 = r5
        L_0x0022:
            r3.<init>(r1, r0)
            X.1mx r2 = new X.1mx
            if (r4 != 0) goto L_0x019c
            r1 = r5
        L_0x002a:
            if (r6 != 0) goto L_0x0198
            r0 = r5
        L_0x002d:
            r2.<init>(r1, r0)
            X.1mx r1 = new X.1mx
            if (r4 != 0) goto L_0x0194
            r4 = r5
        L_0x0035:
            if (r6 != 0) goto L_0x0190
            r0 = r5
        L_0x0038:
            r1.<init>(r4, r0)
            X.1mx r13 = new X.1mx
            r13.<init>(r5, r5)
            X.1mx r4 = new X.1mx
            r4.<init>(r5, r5)
            java.lang.Object r0 = r4.A01
            r16 = r0
            java.lang.Object r9 = r4.A00
            java.lang.Object r14 = r11.A01
            X.0zR r14 = (X.C17770zR) r14
            java.lang.Object r12 = r11.A00
            X.0zR r12 = (X.C17770zR) r12
            if (r14 != 0) goto L_0x0058
            if (r12 != 0) goto L_0x0058
        L_0x0057:
            return
        L_0x0058:
            r8 = 0
            r10 = r20
            if (r14 == 0) goto L_0x006c
            if (r12 != 0) goto L_0x006c
            X.1Ih r3 = X.C21621Ib.A01()
            r2 = 3
            X.1IH r0 = X.AnonymousClass1IH.A00(r2, r8, r3, r0, r5)
            r10.A04(r0)
            return
        L_0x006c:
            java.lang.Object r0 = r3.A00
            if (r0 == 0) goto L_0x00de
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r7 = r0.booleanValue()
        L_0x0076:
            r6 = 1
            java.lang.Object r0 = r2.A00
            if (r0 == 0) goto L_0x00dc
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r5 = r0.intValue()
        L_0x0081:
            java.lang.Object r0 = r1.A00
            if (r0 == 0) goto L_0x00da
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r4 = r0.booleanValue()
        L_0x008b:
            r17 = r19
            if (r14 != 0) goto L_0x00e0
            if (r12 == 0) goto L_0x00e0
            X.1my r2 = X.C21621Ib.A00()
            java.lang.Object r1 = r13.A00
            java.util.Map r1 = (java.util.Map) r1
            r0 = r17
            X.AnonymousClass1GV.A07(r2, r1, r0, r11)
            r2.A00 = r12
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r7)
            java.lang.String r0 = "is_sticky"
            r2.A01(r0, r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r5)
            java.lang.String r0 = "span_size"
            r2.A01(r0, r1)
            java.lang.Object r0 = r2.A00(r4)
            X.1my r0 = (X.C33121my) r0
            X.1Ib r4 = r0.A03()
            X.1KE r3 = r17.A08()
            X.0wR r0 = r10.A02
            if (r0 == 0) goto L_0x00cb
            java.lang.String r1 = r0.A04
            java.lang.String r0 = "section_global_key"
            r4.AMk(r0, r1)
        L_0x00cb:
            X.1In r2 = new X.1In
            r2.<init>(r4, r3)
            r1 = 1
            r0 = 0
            X.1IH r0 = X.AnonymousClass1IH.A00(r1, r8, r2, r0, r9)
            r10.A04(r0)
            return
        L_0x00da:
            r4 = 0
            goto L_0x008b
        L_0x00dc:
            r5 = 1
            goto L_0x0081
        L_0x00de:
            r7 = 0
            goto L_0x0076
        L_0x00e0:
            java.lang.Object r0 = r3.A01
            if (r0 == 0) goto L_0x018d
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r3 = r0.booleanValue()
        L_0x00ea:
            java.lang.Object r0 = r2.A01
            if (r0 == 0) goto L_0x00f4
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r6 = r0.intValue()
        L_0x00f4:
            java.lang.Object r0 = r1.A01
            if (r0 == 0) goto L_0x00fe
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r8 = r0.booleanValue()
        L_0x00fe:
            java.lang.Object r15 = r13.A01
            java.util.Map r15 = (java.util.Map) r15
            java.lang.Object r2 = r13.A00
            java.util.Map r2 = (java.util.Map) r2
            if (r15 == r2) goto L_0x018b
            if (r15 == 0) goto L_0x013c
            if (r2 == 0) goto L_0x013c
            int r1 = r15.size()
            int r0 = r2.size()
            if (r1 != r0) goto L_0x013c
            java.util.Set r0 = r15.entrySet()
            java.util.Iterator r15 = r0.iterator()
        L_0x011e:
            boolean r0 = r15.hasNext()
            if (r0 == 0) goto L_0x018b
            java.lang.Object r0 = r15.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getValue()
            java.lang.Object r0 = r0.getKey()
            java.lang.Object r0 = r2.get(r0)
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 != 0) goto L_0x011e
        L_0x013c:
            r1 = 0
        L_0x013d:
            if (r3 != r7) goto L_0x014b
            if (r6 != r5) goto L_0x014b
            if (r8 != r4) goto L_0x014b
            boolean r0 = r14.A1L(r12)
            if (r0 == 0) goto L_0x014b
            if (r1 != 0) goto L_0x0057
        L_0x014b:
            X.1my r2 = X.C21621Ib.A00()
            java.lang.Object r1 = r13.A00
            java.util.Map r1 = (java.util.Map) r1
            r0 = r17
            X.AnonymousClass1GV.A07(r2, r1, r0, r11)
            r2.A00 = r12
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r7)
            java.lang.String r0 = "is_sticky"
            r2.A01(r0, r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r5)
            java.lang.String r0 = "span_size"
            r2.A01(r0, r1)
            java.lang.Object r0 = r2.A00(r4)
            X.1my r0 = (X.C33121my) r0
            X.1Ib r1 = r0.A03()
            X.1KE r0 = r17.A08()
            X.1In r3 = new X.1In
            r3.<init>(r1, r0)
            r2 = 0
            r1 = 2
            r0 = r16
            X.1IH r0 = X.AnonymousClass1IH.A00(r1, r2, r3, r0, r9)
            r10.A04(r0)
            return
        L_0x018b:
            r1 = 1
            goto L_0x013d
        L_0x018d:
            r3 = 0
            goto L_0x00ea
        L_0x0190:
            java.lang.Boolean r0 = r6.A01
            goto L_0x0038
        L_0x0194:
            java.lang.Boolean r4 = r4.A01
            goto L_0x0035
        L_0x0198:
            java.lang.Integer r0 = r6.A03
            goto L_0x002d
        L_0x019c:
            java.lang.Integer r1 = r4.A03
            goto L_0x002a
        L_0x01a0:
            java.lang.Boolean r0 = r6.A02
            goto L_0x0022
        L_0x01a4:
            java.lang.Boolean r1 = r4.A02
            goto L_0x001f
        L_0x01a8:
            X.0zR r0 = r6.A00
            goto L_0x0017
        L_0x01ac:
            X.0zR r1 = r4.A00
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16080wS.A0F(X.1GA, X.1CW, X.0wR, X.0wR):void");
    }

    public void A0G(AnonymousClass1GA r2, C16070wR r3, C16070wR r4) {
        if (this instanceof C20271Bt) {
            ((C20271Bt) r4).A07 = ((C20271Bt) r3).A07;
        } else if (this instanceof C20291Bv) {
            ((C20291Bv) r4).A0A = ((C20291Bv) r3).A0A;
        }
    }

    public boolean A0H() {
        return (this instanceof AnonymousClass1GV) || (this instanceof AnonymousClass1AM);
    }

    public void A0I(AnonymousClass1KE r3) {
        if (this instanceof AnonymousClass1AO) {
            AnonymousClass1AO r1 = (AnonymousClass1AO) this;
            if (r3 != null) {
                r1.A03 = (C35301r0) r3.A01(C35301r0.class);
                r1.A05 = (C12260oy) r3.A01(C12260oy.class);
                r1.A00 = (C23610BiW) r3.A01(C23610BiW.class);
            }
        } else if (this instanceof C20271Bt) {
            C20271Bt r12 = (C20271Bt) this;
            if (r3 != null) {
                r12.A01 = (C23610BiW) r3.A01(C23610BiW.class);
            }
        } else if (this instanceof C20291Bv) {
            C20291Bv r13 = (C20291Bv) this;
            if (r3 != null) {
                r13.A04 = (C23610BiW) r3.A01(C23610BiW.class);
            }
        }
    }

    public void A0J(AnonymousClass1GA r12, int i, int i2, int i3, int i4, int i5) {
        C74723iN r3;
        C21967Alq A03;
        if (this instanceof C16060wQ) {
            C16060wQ r0 = (C16060wQ) this;
            List list = r0.A09;
            C191917d r4 = r0.A05;
            C04480Uv r7 = r0.A00;
            Integer num = r0.A08;
            C15340v8 r9 = r0.A02;
            C15460vK r32 = r0.A03;
            if (r9 != null) {
                synchronized (r9) {
                    int size = list.size();
                    for (int i6 = 0; i6 < size; i6++) {
                        InboxTrackableItem A00 = C15340v8.A00(r9, (InboxUnitItem) list.get(i6));
                        if (i6 < i || i6 > i2) {
                            r9.A00.A03(A00);
                        } else {
                            r9.A00.A02(A00);
                        }
                    }
                }
            }
            if (r7 != null) {
                ArrayList arrayList = new ArrayList();
                while (i < i2) {
                    InboxUnitItem inboxUnitItem = (InboxUnitItem) list.get(i);
                    if (inboxUnitItem instanceof InboxUnitThreadItem) {
                        arrayList.add(((InboxUnitThreadItem) inboxUnitItem).A01.A0S);
                    }
                    i++;
                }
                r7.A04(new Intent(C06680bu.A0u).putExtra("threads", new ArrayList(arrayList)));
            }
            if (list != null && list.size() != 0) {
                int AqL = r32 == null ? 0 : r32.A00.AqL(567309345294210L, 0);
                InboxUnitItem inboxUnitItem2 = (InboxUnitItem) list.get(list.size() - 1);
                if (r4 != null && i2 >= (i3 - 1) - AqL && (inboxUnitItem2 instanceof InboxLoadMorePlaceholderItem) && num == AnonymousClass07B.A01) {
                    r4.Boe(inboxUnitItem2.A02.A0Q());
                    return;
                }
                return;
            }
            return;
        } else if (this instanceof C20291Bv) {
            C20291Bv r5 = (C20291Bv) this;
            C74913ik r1 = r5.A09;
            C74713iM r42 = r1.connectionHandler;
            C74833iZ r02 = r1.connectionData;
            AtomicBoolean atomicBoolean = r1.isWaitingForOnDataBound;
            boolean z = r5.A0F;
            Object obj = r5.A0D;
            int i7 = r5.A01;
            if (r02.A03 && !atomicBoolean.get() && i2 != -1 && i2 + 1 >= i3 - i7 && z) {
                synchronized (r42) {
                    r3 = r42.A00;
                }
                if (r3 != null && r3.A03.get()) {
                    C74503hy A01 = C74723iN.A01(r3);
                    if (C74503hy.A00(A01, A01.A03())) {
                        int i8 = r3.A08;
                        synchronized (r3) {
                            try {
                                r3.A05 = true;
                                C74513hz A002 = C74723iN.A00(r3);
                                if (A002 == null) {
                                    ((AnonymousClass09P) r3.A0B.get()).CGS("GraphQLConnectionService", "You must invoke connect() first!");
                                    return;
                                }
                                synchronized (A002.A0Q) {
                                    try {
                                        A03 = A002.A05.A03();
                                    } catch (Throwable th) {
                                        while (true) {
                                            th = th;
                                            break;
                                        }
                                    }
                                }
                                C74513hz.A02(A002, A03, C73863gt.FIRST, i8, obj, null);
                                return;
                            } catch (Throwable th2) {
                                while (true) {
                                    th = th2;
                                }
                            }
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } else {
            return;
        }
        throw th;
    }

    public AnonymousClass1KE A0L(AnonymousClass1GA r5, AnonymousClass1KE r6) {
        if (!(this instanceof AnonymousClass1AO)) {
            return r6;
        }
        AnonymousClass1AO r3 = (AnonymousClass1AO) this;
        AnonymousClass1KE A00 = AnonymousClass1KE.A00(r6);
        A00.A02(C23610BiW.class, (C23610BiW) r3.A07.ttrcTraceRef.get());
        A00.A02(C22863BIh.class, r3.A06.A02);
        return A00;
    }

    public void A0M(AnonymousClass11I r2, AnonymousClass11I r3) {
        if (this instanceof AnonymousClass1AO) {
            AnonymousClass5CT r22 = (AnonymousClass5CT) r2;
            AnonymousClass5CT r32 = (AnonymousClass5CT) r3;
            r32.ttrcRenderTracker = r22.ttrcRenderTracker;
            r32.ttrcTraceRef = r22.ttrcTraceRef;
        } else if (this instanceof C20271Bt) {
            AnonymousClass6RZ r23 = (AnonymousClass6RZ) r2;
            AnonymousClass6RZ r33 = (AnonymousClass6RZ) r3;
            r33.cachedModel = r23.cachedModel;
            r33.dataSource = r23.dataSource;
            r33.fetchError = r23.fetchError;
            r33.fetchState = r23.fetchState;
            r33.responseModel = r23.responseModel;
            r33.serviceListener = r23.serviceListener;
            r33.summary = r23.summary;
            r33.ttrcTraceHelper = r23.ttrcTraceHelper;
        } else if (this instanceof C20291Bv) {
            C74913ik r24 = (C74913ik) r2;
            C74913ik r34 = (C74913ik) r3;
            r34.connectionData = r24.connectionData;
            r34.connectionHandler = r24.connectionHandler;
            r34.dataSource = r24.dataSource;
            r34.fetchError = r24.fetchError;
            r34.fetchState = r24.fetchState;
            r34.isWaitingForOnDataBound = r24.isWaitingForOnDataBound;
            r34.lastLocalCacheScopeUsed = r24.lastLocalCacheScopeUsed;
            r34.serviceListener = r24.serviceListener;
            r34.ttrcTraceHelper = r24.ttrcTraceHelper;
        }
    }

    public void A0N(AnonymousClass1GA r19) {
        C74833iZ r12;
        if (this instanceof AnonymousClass1AO) {
            AnonymousClass1AO r7 = (AnonymousClass1AO) this;
            C23871Rg r6 = new C23871Rg();
            C23871Rg r5 = new C23871Rg();
            BOS bos = (BOS) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aj8, r7.A01);
            C23610BiW biW = r7.A00;
            C12260oy r2 = r7.A05;
            r5.A00(new C93344cY(r19.A09));
            if (biW == null && r2 != null) {
                biW = bos.A02(r2);
            }
            r6.A00(new AtomicReference(biW));
            Object obj = r6.A00;
            if (obj != null) {
                r7.A07.ttrcTraceRef = (AtomicReference) obj;
            }
            Object obj2 = r5.A00;
            if (obj2 != null) {
                r7.A07.ttrcRenderTracker = (C93344cY) obj2;
            }
        } else if (this instanceof C20271Bt) {
            C20271Bt r22 = (C20271Bt) this;
            C23871Rg r10 = new C23871Rg();
            C23871Rg r9 = new C23871Rg();
            C23871Rg r8 = new C23871Rg();
            C23871Rg r72 = new C23871Rg();
            C23871Rg r62 = new C23871Rg();
            C23871Rg r52 = new C23871Rg();
            C23871Rg r4 = new C23871Rg();
            C23871Rg r3 = new C23871Rg();
            r10.A00(C156877Ne.INITIAL_STATE);
            r9.A00(null);
            r8.A00(null);
            r72.A00(null);
            r62.A00(C156837Na.UNSET);
            r52.A00(null);
            r4.A00(new AnonymousClass7ND());
            r3.A00(new AnonymousClass23T());
            Object obj3 = r10.A00;
            if (obj3 != null) {
                r22.A06.fetchState = (C156877Ne) obj3;
            }
            Object obj4 = r9.A00;
            if (obj4 != null) {
                r22.A06.responseModel = obj4;
            }
            Object obj5 = r8.A00;
            if (obj5 != null) {
                r22.A06.cachedModel = obj5;
            }
            Object obj6 = r72.A00;
            if (obj6 != null) {
                r22.A06.fetchError = (Throwable) obj6;
            }
            Object obj7 = r62.A00;
            if (obj7 != null) {
                r22.A06.dataSource = (C156837Na) obj7;
            }
            Object obj8 = r52.A00;
            if (obj8 != null) {
                r22.A06.summary = (Summary) obj8;
            }
            Object obj9 = r4.A00;
            if (obj9 != null) {
                r22.A06.serviceListener = (AnonymousClass7ND) obj9;
            }
            Object obj10 = r3.A00;
            if (obj10 != null) {
                r22.A06.ttrcTraceHelper = (AnonymousClass23T) obj10;
            }
        } else if (this instanceof C20291Bv) {
            C20291Bv r23 = (C20291Bv) this;
            C23871Rg r1 = new C23871Rg();
            C23871Rg r102 = new C23871Rg();
            C23871Rg r92 = new C23871Rg();
            C23871Rg r82 = new C23871Rg();
            C23871Rg r73 = new C23871Rg();
            C23871Rg r63 = new C23871Rg();
            C23871Rg r53 = new C23871Rg();
            C23871Rg r42 = new C23871Rg();
            C23871Rg r32 = new C23871Rg();
            C21960Alj alj = r23.A06;
            r1.A00(new C74713iM());
            r102.A00(new C29652EfB());
            if (alj != null) {
                r12 = new C74833iZ(alj.A00, false, alj.A04, alj.A03, new C74503hy());
            } else {
                r12 = C74833iZ.A06;
            }
            r92.A00(r12);
            r82.A00(C156877Ne.INITIAL_STATE);
            r73.A00(null);
            r63.A00(C156837Na.UNSET);
            r53.A00(null);
            r42.A00(new AtomicBoolean(false));
            r32.A00(new AnonymousClass23T());
            Object obj11 = r1.A00;
            if (obj11 != null) {
                r23.A09.connectionHandler = (C74713iM) obj11;
            }
            Object obj12 = r102.A00;
            if (obj12 != null) {
                r23.A09.serviceListener = (C29652EfB) obj12;
            }
            Object obj13 = r92.A00;
            if (obj13 != null) {
                r23.A09.connectionData = (C74833iZ) obj13;
            }
            Object obj14 = r82.A00;
            if (obj14 != null) {
                r23.A09.fetchState = (C156877Ne) obj14;
            }
            Object obj15 = r73.A00;
            if (obj15 != null) {
                r23.A09.fetchError = (Throwable) obj15;
            }
            Object obj16 = r63.A00;
            if (obj16 != null) {
                r23.A09.dataSource = (C156837Na) obj16;
            }
            Object obj17 = r53.A00;
            if (obj17 != null) {
                r23.A09.lastLocalCacheScopeUsed = (String) obj17;
            }
            Object obj18 = r42.A00;
            if (obj18 != null) {
                r23.A09.isWaitingForOnDataBound = (AtomicBoolean) obj18;
            }
            Object obj19 = r32.A00;
            if (obj19 != null) {
                r23.A09.ttrcTraceHelper = (AnonymousClass23T) obj19;
            }
        }
    }

    public void A0O(AnonymousClass1GA r8) {
        C74723iN r3;
        if (this instanceof C20271Bt) {
            AnonymousClass7NE r1 = ((C20271Bt) this).A07;
            if (r1 != null) {
                synchronized (r1) {
                    try {
                        r1.A00 = false;
                    } catch (Throwable th) {
                        while (true) {
                            th = th;
                        }
                    }
                }
                C09290gx r0 = r1.A03;
                if (r0 == null) {
                    r0 = C09290gx.FETCH_AND_FILL;
                }
                AnonymousClass7NE.A00(r1, r0);
                return;
            }
            return;
        } else if (this instanceof C20291Bv) {
            C20291Bv r2 = (C20291Bv) this;
            C74713iM r12 = r2.A09.connectionHandler;
            Object obj = r2.A0D;
            synchronized (r12) {
                try {
                    r3 = r12.A00;
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
            if (r3 != null && r3.A03.get()) {
                int i = r3.A07;
                synchronized (r3) {
                    try {
                        r3.A05 = true;
                        r3.A04 = true;
                        C74513hz A00 = C74723iN.A00(r3);
                        if (A00 == null) {
                            ((AnonymousClass09P) r3.A0B.get()).CGS("GraphQLConnectionService", "You must invoke connect() first!");
                            return;
                        } else {
                            C74513hz.A02(A00, C21967Alq.A05, C73863gt.FIRST, i, obj, null);
                            return;
                        }
                    } catch (Throwable th3) {
                        while (true) {
                            th = th3;
                            break;
                        }
                    }
                }
            } else {
                return;
            }
        } else {
            return;
        }
        throw th;
    }

    public void A0P(AnonymousClass1GA r27, boolean z, boolean z2, long j, int i, int i2, AnonymousClass1CX r34, int i3) {
        Summary summary;
        C156837Na r20;
        C23610BiW A02;
        boolean z3 = z;
        long j2 = j;
        boolean z4 = z2;
        if (this instanceof AnonymousClass1AO) {
            AnonymousClass1AO r0 = (AnonymousClass1AO) this;
            AnonymousClass248 r2 = r0.A06;
            AnonymousClass654 r4 = r0.A04;
            AnonymousClass5CT r1 = r0.A07;
            AtomicReference atomicReference = r1.ttrcTraceRef;
            C93344cY r7 = r1.ttrcRenderTracker;
            C30290EtU etU = new C30290EtU(j2);
            etU.A03 = z3;
            etU.A04 = z4;
            etU.A02 = r4;
            etU.A01 = (C23610BiW) atomicReference.get();
            C30289EtT etT = new C30289EtT(etU);
            int i4 = AnonymousClass1Y3.AJR;
            AnonymousClass0UN r42 = r7.A00;
            ((BOW) AnonymousClass1XX.A02(1, i4, r42)).A01((BOT) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AtC, r42));
            C22863BIh bIh = r2.A02;
            C23610BiW biW = etT.A01;
            AnonymousClass654 r14 = etT.A02;
            if (!(biW == null || bIh == null)) {
                ((BOT) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AtC, r7.A00)).A01();
            }
            if (r14 == null && bIh != null) {
                r14 = new AnonymousClass654(bIh.A07.A00());
            }
            BOS bos = (BOS) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aj8, r7.A00);
            if (biW == null && bIh != null) {
                biW = bos.A02(bIh.A06);
            }
            if (biW != null) {
                biW.CHi("UI_INITIAL_LOAD");
            }
            if (r14 != null && biW != null) {
                GraphQLResult graphQLResult = r2.A01;
                if (graphQLResult == null || (summary = graphQLResult.A02) == null) {
                    if (graphQLResult != null) {
                        AnonymousClass23T.A00(r7.A03, biW, r14, etT.A03, etT.A04, C156837Na.FROM_LOCAL_CACHE, null, 0, etT.A00);
                        return;
                    }
                    Throwable th = r2.A03;
                    if (th != null) {
                        biW.AZS(th.getMessage());
                    }
                } else if (bIh == null || r2.A03 != null) {
                    r7.A03.A02(biW, r14, etT.A03, etT.A04, summary, etT.A00);
                } else {
                    boolean z5 = false;
                    if (3 == bIh.A00) {
                        z5 = true;
                    }
                    if (!z5 || r7.A01) {
                        AnonymousClass23T r15 = r7.A03;
                        boolean z6 = etT.A03;
                        boolean z7 = etT.A04;
                        long j3 = etT.A00;
                        boolean z8 = true;
                        boolean z9 = false;
                        if (bIh.A01 > 0) {
                            z9 = true;
                        }
                        if (bIh.A03 <= 0) {
                            z8 = false;
                        }
                        if (!z9 || z8) {
                            r20 = z8 ? C156837Na.FROM_NETWORK : C156837Na.UNSET;
                        } else {
                            Summary summary2 = bIh.A05;
                            r20 = (summary2 == null || summary2.isNetworkComplete) ? C156837Na.FROM_LOCAL_CACHE : C156837Na.FROM_LOCAL_STALE_CACHE;
                        }
                        AnonymousClass23T.A00(r15, biW, r14, z6, z7, r20, summary, 0, j3);
                        if (bIh.A04 == C09290gx.FULLY_CACHED) {
                            boolean z10 = false;
                            if (bIh.A01 != -1) {
                                z10 = true;
                            }
                            if (z10) {
                                biW.BzL(r14.A00);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    r7.A01 = !bIh.A01();
                    BOS bos2 = (BOS) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aj8, r7.A00);
                    long j4 = graphQLResult.A00;
                    long j5 = r7.A02;
                    C12260oy r5 = bIh.A06;
                    if (!(r5 == null || (A02 = bos2.A02(r5)) == null)) {
                        A02.CHi("hot");
                    }
                    biW.Bxi(r14.A00, bIh.A05, bIh.A01(), j4, j5);
                }
            }
        } else if (this instanceof C16060wQ) {
            ((C20921Ei) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BMw, ((C16060wQ) this).A01)).A0A(false);
        } else if (!(this instanceof C20271Bt) && (this instanceof C20291Bv)) {
            C20291Bv r02 = (C20291Bv) this;
            AnonymousClass654 r43 = r02.A0B;
            C23610BiW biW2 = r02.A04;
            C74913ik r03 = r02.A09;
            C156837Na r72 = r03.dataSource;
            AnonymousClass23T r22 = r03.ttrcTraceHelper;
            if (r43 != null && biW2 != null) {
                AnonymousClass23T.A00(r22, biW2, r43, z3, z4, r72, null, 0, j2);
            }
        }
    }

    public void A0Q(AnonymousClass1GA r3) {
        if (this instanceof C20291Bv) {
            ((C20291Bv) this).A09.isWaitingForOnDataBound.set(false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00e0, code lost:
        if (r4 != 3) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01a4, code lost:
        if (r3 == (r5 - 1)) goto L_0x01a6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C20301Bw A0R(X.AnonymousClass1GA r15) {
        /*
            r14 = this;
            boolean r0 = r14 instanceof X.AnonymousClass1AO
            if (r0 != 0) goto L_0x026b
            boolean r0 = r14 instanceof X.C35291qz
            if (r0 != 0) goto L_0x01c3
            boolean r0 = r14 instanceof X.C16060wQ
            if (r0 != 0) goto L_0x0098
            boolean r0 = r14 instanceof X.AnonymousClass1FF
            if (r0 != 0) goto L_0x0056
            boolean r0 = r14 instanceof X.C20271Bt
            if (r0 != 0) goto L_0x001a
            boolean r0 = r14 instanceof X.C20291Bv
            if (r0 != 0) goto L_0x035e
            r0 = 0
            return r0
        L_0x001a:
            r0 = r14
            X.1Bt r0 = (X.C20271Bt) r0
            X.6RZ r0 = r0.A06
            X.7Ne r6 = r0.fetchState
            java.lang.Object r5 = r0.responseModel
            X.7Na r4 = r0.dataSource
            java.lang.Throwable r3 = r0.fetchError
            X.7Ne r0 = X.C156877Ne.DOWNLOAD_ERROR
            if (r6 != r0) goto L_0x002f
            if (r5 != 0) goto L_0x0054
            if (r3 == 0) goto L_0x0054
        L_0x002f:
            r1 = 1
        L_0x0030:
            java.lang.String r0 = "Fetch state is DOWNLOAD_ERROR but response model/fetch error are not properly set."
            X.AnonymousClass064.A05(r1, r0)
            X.0wR r0 = r15.A0K()
            if (r0 != 0) goto L_0x004b
            r2 = 0
        L_0x003c:
            X.63b r1 = new X.63b
            r1.<init>()
            r1.A02 = r5
            r1.A01 = r6
            r1.A03 = r3
            r1.A00 = r4
            goto L_0x038e
        L_0x004b:
            X.0wR r0 = r15.A0K()
            X.1Bt r0 = (X.C20271Bt) r0
            X.10N r2 = r0.A04
            goto L_0x003c
        L_0x0054:
            r1 = 0
            goto L_0x0030
        L_0x0056:
            X.1Bx r6 = X.C20301Bw.A00()
            X.1oS r5 = X.AnonymousClass1GV.A06()
            r1 = 1
            java.lang.String r0 = "orientation"
            java.lang.String[] r7 = new java.lang.String[]{r0}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r1)
            X.1Ga r3 = new X.1Ga
            r3.<init>()
            X.0zR r1 = r15.A04
            if (r1 == 0) goto L_0x0077
            java.lang.String r0 = r1.A06
            r3.A07 = r0
        L_0x0077:
            r4.clear()
            r0 = 0
            r3.A01 = r0
            r4.set(r0)
            r3.A00 = r0
            r0 = 1
            r3.A02 = r0
            X.1GV r1 = r5.A00
            X.AnonymousClass11F.A0C(r0, r4, r7)
            r1.A00 = r3
            java.util.BitSet r1 = r5.A01
            r0 = 0
            r1.set(r0)
            r6.A00(r5)
            X.1Bw r0 = r6.A00
            return r0
        L_0x0098:
            r0 = r14
            X.0wQ r0 = (X.C16060wQ) r0
            java.util.List r8 = r0.A09
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r0.A04
            java.lang.Integer r1 = r0.A08
            boolean r0 = r8.isEmpty()
            if (r0 == 0) goto L_0x00ae
            X.1Bx r0 = X.C20301Bw.A00()
            X.1Bw r0 = r0.A00
            return r0
        L_0x00ae:
            r6 = 0
            int r0 = r8.size()
            int r3 = r0 + -1
            java.lang.Object r0 = r8.get(r3)
            com.facebook.messaging.inbox2.items.InboxUnitItem r0 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r0
            boolean r0 = r0 instanceof com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem
            if (r0 == 0) goto L_0x00e7
            java.lang.String r0 = "A LoadMoreState must be provided to render an InboxLoadMorePlaceholderItem"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            java.lang.Object r9 = r8.get(r3)
            com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem r9 = (com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem) r9
            int r5 = r8.size()
            int[] r0 = X.C33301nM.A00
            int r2 = r1.intValue()
            r4 = r0[r2]
            r1 = 1
            r0 = 0
            if (r2 == r0) goto L_0x01b1
            r0 = 2
            java.lang.String r2 = ""
            if (r4 == r0) goto L_0x01a3
            r0 = 3
            if (r4 == r0) goto L_0x01a6
        L_0x00e2:
            r0 = 0
            java.util.List r8 = r8.subList(r0, r3)
        L_0x00e7:
            X.1Bx r5 = X.C20301Bw.A00()
            r0 = 1
            java.lang.String r9 = "data"
            java.lang.String[] r4 = new java.lang.String[]{r9}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r0)
            X.1AM r2 = new X.1AM
            r2.<init>()
            r3.clear()
            X.0ds r0 = new X.0ds
            r0.<init>(r7)
            java.util.List r0 = X.C04300To.A07(r8, r0)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
            r2.A04 = r0
            r0 = 0
            r3.set(r0)
            java.lang.String r0 = "items"
            r2.A05 = r0
            java.lang.Object[] r1 = new java.lang.Object[]{r15}
            r0 = 239257522(0xe42c7b2, float:2.4008496E-30)
            X.10N r0 = A02(r15, r0, r1)
            r2.A02 = r0
            java.lang.Object[] r1 = new java.lang.Object[]{r15}
            r0 = 1171108835(0x45cdb3e3, float:6582.486)
            X.10N r0 = A02(r15, r0, r1)
            r2.A01 = r0
            java.lang.Object[] r1 = new java.lang.Object[]{r15}
            r0 = -680621655(0xffffffffd76e89a9, float:-2.62275013E14)
            X.10N r0 = A02(r15, r0, r1)
            r2.A00 = r0
            X.1Bw r0 = r5.A00
            java.util.List r1 = r0.A00
            r0 = 1
            X.AnonymousClass1CY.A00(r0, r3, r4)
            r1.add(r2)
            if (r6 == 0) goto L_0x01a0
            java.lang.String[] r4 = new java.lang.String[]{r9}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r0)
            X.1AM r2 = new X.1AM
            r2.<init>()
            r3.clear()
            X.1Hr r0 = new X.1Hr
            r0.<init>(r6, r7)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r0)
            r2.A04 = r0
            r0 = 0
            r3.set(r0)
            java.lang.String r0 = "loadmore"
            r2.A05 = r0
            java.lang.Object[] r1 = new java.lang.Object[]{r15}
            r0 = 239257522(0xe42c7b2, float:2.4008496E-30)
            X.10N r0 = A02(r15, r0, r1)
            r2.A02 = r0
            java.lang.Object[] r1 = new java.lang.Object[]{r15}
            r0 = 2120202712(0x7e5fb9d8, float:7.43457E37)
            X.10N r0 = A02(r15, r0, r1)
            r2.A01 = r0
            java.lang.Object[] r1 = new java.lang.Object[]{r15}
            r0 = -680621655(0xffffffffd76e89a9, float:-2.62275013E14)
            X.10N r0 = A02(r15, r0, r1)
            r2.A00 = r0
            X.1Bw r0 = r5.A00
            java.util.List r1 = r0.A00
            r0 = 1
            X.AnonymousClass1CY.A00(r0, r3, r4)
            r1.add(r2)
        L_0x01a0:
            X.1Bw r0 = r5.A00
            return r0
        L_0x01a3:
            int r5 = r5 - r1
            if (r3 != r5) goto L_0x01b1
        L_0x01a6:
            com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem r6 = new com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem
            X.1ck r1 = r9.A02
            X.17k r0 = X.C192517k.LOADING
            r6.<init>(r1, r0, r2)
            goto L_0x00e2
        L_0x01b1:
            com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem r6 = new com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem
            X.1ck r2 = r9.A02
            X.17k r1 = X.C192517k.LOAD_MORE
            r0 = 2131833824(0x7f1133e0, float:1.930074E38)
            java.lang.String r0 = r15.A0C(r0)
            r6.<init>(r2, r1, r0)
            goto L_0x00e2
        L_0x01c3:
            r0 = r14
            X.1qz r0 = (X.C35291qz) r0
            com.google.common.collect.ImmutableList r13 = r0.A08
            java.lang.Integer r12 = r0.A09
            X.17d r11 = r0.A05
            X.0v8 r10 = r0.A01
            X.0vK r9 = r0.A03
            X.0x5 r8 = r0.A06
            X.0vU r1 = r0.A07
            X.0ki r2 = r0.A02
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r0.A04
            int r3 = X.AnonymousClass1Y3.BPt
            X.0UN r4 = r0.A00
            r0 = 0
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r0, r3, r4)
            X.0Uv r6 = (X.C04480Uv) r6
            int r3 = X.AnonymousClass1Y3.A2Y
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r4)
            X.1BB r0 = (X.AnonymousClass1BB) r0
            X.1Bx r5 = X.C20301Bw.A00()
            boolean r3 = r13.isEmpty()
            if (r3 != 0) goto L_0x0223
            X.1FF r4 = new X.1FF
            r4.<init>()
            X.1Bw r3 = r5.A00
            java.util.List r3 = r3.A00
            r3.add(r4)
            X.1oS r3 = X.AnonymousClass1GV.A06()
            X.1rI r4 = new X.1rI
            r4.<init>(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C15560vU.A00(r1, r15, r2, r7)
            java.lang.Object r0 = r2.A00
            X.1GN r0 = (X.AnonymousClass1GN) r0
            r0.A04 = r4
            X.1GN r0 = r2.A36()
            r3.A06(r0)
            X.1GV r0 = r3.A04()
            r5.A01(r0)
        L_0x0223:
            r2 = 2
            r0 = 146(0x92, float:2.05E-43)
            java.lang.String r3 = X.C99084oO.$const$string(r0)
            java.lang.String r0 = "inboxUnitItems"
            java.lang.String[] r4 = new java.lang.String[]{r3, r0}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.0wQ r2 = new X.0wQ
            android.content.Context r0 = r15.A09
            r2.<init>(r0)
            r3.clear()
            r2.A07 = r1
            r1 = 0
            r3.set(r1)
            r2.A06 = r8
            r2.A09 = r13
            r1 = 1
            r3.set(r1)
            r2.A04 = r7
            r2.A08 = r12
            r2.A05 = r11
            r2.A02 = r10
            r2.A03 = r9
            r2.A00 = r6
            java.lang.String r1 = "inboxUnitItems"
            r2.A05 = r1
            X.1Bw r0 = r5.A00
            java.util.List r1 = r0.A00
            r0 = 2
            X.AnonymousClass1CY.A00(r0, r3, r4)
            r1.add(r2)
            X.1Bw r0 = r5.A00
            return r0
        L_0x026b:
            r7 = r14
            X.1AO r7 = (X.AnonymousClass1AO) r7
            X.248 r6 = r7.A06
            X.654 r4 = r7.A04
            int r2 = X.AnonymousClass1Y3.Aj8
            X.0UN r1 = r7.A01
            r0 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.BOS r5 = (X.BOS) r5
            X.BiW r1 = r7.A00
            X.1r0 r3 = r7.A03
            X.5CT r0 = r7.A07
            java.util.concurrent.atomic.AtomicReference r2 = r0.ttrcTraceRef
            X.4cY r7 = r0.ttrcRenderTracker
            X.BIh r0 = r6.A02
            if (r1 != 0) goto L_0x0293
            if (r0 == 0) goto L_0x0293
            X.0oy r0 = r0.A06
            X.BiW r1 = r5.A02(r0)
        L_0x0293:
            if (r1 == 0) goto L_0x0299
            r0 = 0
            r2.compareAndSet(r0, r1)
        L_0x0299:
            java.lang.Object r5 = r2.get()
            X.BiW r5 = (X.C23610BiW) r5
            X.BIh r8 = r6.A02
            int r2 = X.AnonymousClass1Y3.Aj8
            X.0UN r1 = r7.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.BOS r1 = (X.BOS) r1
            if (r5 != 0) goto L_0x02b6
            if (r8 == 0) goto L_0x02b6
            X.0oy r0 = r8.A06
            X.BiW r5 = r1.A02(r0)
        L_0x02b6:
            X.BIh r1 = r6.A02
            r2 = r4
            if (r4 != 0) goto L_0x02c8
            if (r1 == 0) goto L_0x02c8
            X.654 r2 = new X.654
            X.BIt r0 = r1.A07
            java.lang.String r0 = r0.A00()
            r2.<init>(r0)
        L_0x02c8:
            if (r5 == 0) goto L_0x034f
            java.lang.Throwable r1 = r6.A03
            if (r1 == 0) goto L_0x034f
            com.facebook.graphql.executor.GraphQLResult r0 = r6.A01
            if (r0 != 0) goto L_0x034f
            java.lang.String r0 = r1.getMessage()
            r5.AZS(r0)
        L_0x02d9:
            X.7Na r2 = X.AnonymousClass1AO.A06(r6)
            boolean r0 = X.C35301r0.A01(r3)
            if (r0 == 0) goto L_0x030f
            X.7Na r0 = X.C156837Na.UNSET
            if (r2 == r0) goto L_0x030f
            X.BIh r1 = r6.A02
            if (r4 != 0) goto L_0x02f8
            if (r1 == 0) goto L_0x02f8
            X.654 r4 = new X.654
            X.BIt r0 = r1.A07
            java.lang.String r0 = r0.A00()
            r4.<init>(r0)
        L_0x02f8:
            if (r4 != 0) goto L_0x0346
            java.lang.String r1 = ""
        L_0x02fc:
            X.7Na r0 = X.C156837Na.FROM_NETWORK
            if (r2 != r0) goto L_0x0343
            java.lang.String r0 = "nc"
        L_0x0302:
            java.lang.String r2 = X.AnonymousClass08S.A0J(r1, r0)
            r3.A00 = r2
            java.lang.String r1 = "_changeset"
            java.lang.String r0 = "_start"
            r3.A03(r1, r0, r2)
        L_0x030f:
            X.0wR r0 = r15.A0K()
            if (r0 != 0) goto L_0x033a
            r5 = 0
        L_0x0316:
            java.lang.Object r4 = r6.A02
            X.7Ne r3 = X.AnonymousClass1AO.A07(r6)
            java.lang.Throwable r2 = r6.A03
            X.7Na r0 = X.AnonymousClass1AO.A06(r6)
            X.63b r1 = new X.63b
            r1.<init>()
            r1.A02 = r4
            r1.A01 = r3
            r1.A03 = r2
            r1.A00 = r0
            X.0zV r0 = r5.A00
            X.0zT r0 = r0.Alq()
            java.lang.Object r0 = r0.AXa(r5, r1)
            goto L_0x0398
        L_0x033a:
            X.0wR r0 = r15.A0K()
            X.1AO r0 = (X.AnonymousClass1AO) r0
            X.10N r5 = r0.A02
            goto L_0x0316
        L_0x0343:
            java.lang.String r0 = "cc"
            goto L_0x0302
        L_0x0346:
            java.lang.String r1 = r4.A00
            java.lang.String r0 = "_"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            goto L_0x02fc
        L_0x034f:
            if (r5 == 0) goto L_0x02d9
            if (r2 == 0) goto L_0x02d9
            X.23T r1 = r7.A03
            X.7Na r0 = X.AnonymousClass1AO.A06(r6)
            r1.A01(r5, r2, r0)
            goto L_0x02d9
        L_0x035e:
            r0 = r14
            X.1Bv r0 = (X.C20291Bv) r0
            X.654 r2 = r0.A0B
            X.BiW r1 = r0.A04
            X.3ik r0 = r0.A09
            X.3iZ r6 = r0.connectionData
            X.7Ne r5 = r0.fetchState
            java.lang.Throwable r4 = r0.fetchError
            X.7Na r3 = r0.dataSource
            X.23T r0 = r0.ttrcTraceHelper
            if (r0 == 0) goto L_0x037a
            if (r2 == 0) goto L_0x037a
            if (r1 == 0) goto L_0x037a
            r0.A01(r1, r2, r3)
        L_0x037a:
            X.0wR r0 = r15.A0K()
            if (r0 != 0) goto L_0x039b
            r2 = 0
        L_0x0381:
            X.63b r1 = new X.63b
            r1.<init>()
            r1.A02 = r6
            r1.A01 = r5
            r1.A03 = r4
            r1.A00 = r3
        L_0x038e:
            X.0zV r0 = r2.A00
            X.0zT r0 = r0.Alq()
            java.lang.Object r0 = r0.AXa(r2, r1)
        L_0x0398:
            X.1Bw r0 = (X.C20301Bw) r0
            return r0
        L_0x039b:
            X.0wR r0 = r15.A0K()
            X.1Bv r0 = (X.C20291Bv) r0
            X.10N r2 = r0.A08
            goto L_0x0381
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16080wS.A0R(X.1GA):X.1Bw");
    }

    public Object ALY(C61302yf r2, Object obj, Object[] objArr) {
        return null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0056, code lost:
        if (r9.A01.isEmpty() != false) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c0, code lost:
        if (r4 == false) goto L_0x00c2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object AXa(X.AnonymousClass10N r24, java.lang.Object r25) {
        /*
            r23 = this;
            r3 = r25
            r0 = r23
            boolean r0 = r0 instanceof X.C20291Bv
            if (r0 != 0) goto L_0x000a
            r0 = 0
            return r0
        L_0x000a:
            r2 = r24
            int r1 = r2.A01
            r16 = 0
            r0 = 6759691(0x67250b, float:9.472345E-39)
            if (r1 != r0) goto L_0x0134
            X.EfC r3 = (X.C29653EfC) r3
            X.0zV r12 = r2.A00
            java.lang.Object[] r2 = r2.A02
            r15 = 0
            r10 = r2[r15]
            X.1GA r10 = (X.AnonymousClass1GA) r10
            X.7Ne r0 = r3.A01
            java.lang.Object r9 = r3.A03
            java.lang.Throwable r8 = r3.A04
            X.7Na r7 = r3.A00
            java.lang.Integer r11 = r3.A02
            r1 = 1
            r6 = r2[r1]
            X.3iZ r6 = (X.C74833iZ) r6
            r1 = 2
            r5 = r2[r1]
            X.7Ne r5 = (X.C156877Ne) r5
            r1 = 3
            r4 = r2[r1]
            java.lang.Throwable r4 = (java.lang.Throwable) r4
            X.1Bv r12 = (X.C20291Bv) r12
            X.BiW r3 = r12.A04
            X.3ik r1 = r12.A09
            java.util.concurrent.atomic.AtomicBoolean r2 = r1.isWaitingForOnDataBound
            X.3iZ r9 = (X.C74833iZ) r9
            int r1 = r0.ordinal()
            r12 = 1
            switch(r1) {
                case 0: goto L_0x0090;
                case 1: goto L_0x0074;
                case 2: goto L_0x004b;
                case 3: goto L_0x0094;
                default: goto L_0x004b;
            }
        L_0x004b:
            r14 = 0
            r13 = 1
            if (r9 == 0) goto L_0x0058
            com.google.common.collect.ImmutableList r1 = r9.A01
            boolean r1 = r1.isEmpty()
            r12 = 0
            if (r1 == 0) goto L_0x0059
        L_0x0058:
            r12 = 1
        L_0x0059:
            int r1 = r0.ordinal()
            switch(r1) {
                case 0: goto L_0x00d1;
                case 1: goto L_0x00f2;
                case 2: goto L_0x00b7;
                case 3: goto L_0x00d5;
                default: goto L_0x0060;
            }
        L_0x0060:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r1 = "Wrong FetchState is Returned: "
            r2.<init>(r1)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r3.<init>(r0)
            throw r3
        L_0x0074:
            if (r9 != 0) goto L_0x004b
            X.3iZ r9 = new X.3iZ
            com.google.common.collect.ImmutableList r14 = r6.A01
            r19 = 0
            boolean r13 = r6.A04
            boolean r12 = r6.A03
            X.3hy r1 = r6.A00
            r17 = r9
            r18 = r14
            r20 = r13
            r21 = r12
            r22 = r1
            r17.<init>(r18, r19, r20, r21, r22)
            goto L_0x004b
        L_0x0090:
            if (r9 != 0) goto L_0x0094
            r9 = r6
            goto L_0x004b
        L_0x0094:
            if (r9 == 0) goto L_0x0097
            r12 = 0
        L_0x0097:
            X.3iZ r17 = new X.3iZ
            if (r12 == 0) goto L_0x00b4
            com.google.common.collect.ImmutableList r1 = r6.A01
        L_0x009d:
            r19 = 0
            r20 = 0
            r21 = 0
            if (r12 == 0) goto L_0x00b1
            X.3hy r9 = r6.A00
        L_0x00a7:
            r18 = r1
            r22 = r9
            r17.<init>(r18, r19, r20, r21, r22)
            r9 = r17
            goto L_0x004b
        L_0x00b1:
            X.3hy r9 = r9.A00
            goto L_0x00a7
        L_0x00b4:
            com.google.common.collect.ImmutableList r1 = r9.A01
            goto L_0x009d
        L_0x00b7:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            if (r5 != r0) goto L_0x00c2
            r4 = 1
            if (r6 != 0) goto L_0x00c4
            if (r9 != 0) goto L_0x00c4
        L_0x00c0:
            if (r4 != 0) goto L_0x0101
        L_0x00c2:
            r14 = 1
            goto L_0x0101
        L_0x00c4:
            if (r6 == 0) goto L_0x00cf
            if (r9 == 0) goto L_0x00cf
            boolean r3 = r6.equals(r9)
            if (r3 == 0) goto L_0x00cf
            goto L_0x00c0
        L_0x00cf:
            r4 = 0
            goto L_0x00c0
        L_0x00d1:
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r14 = 1
            goto L_0x0101
        L_0x00d5:
            if (r12 == 0) goto L_0x00ea
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            if (r5 != r0) goto L_0x00dd
            if (r8 == r4) goto L_0x00de
        L_0x00dd:
            r14 = 1
        L_0x00de:
            X.7Ne r0 = X.C156877Ne.DOWNLOAD_ERROR
            if (r3 == 0) goto L_0x0101
            java.lang.String r4 = r8.getMessage()
            r3.AZS(r4)
            goto L_0x0101
        L_0x00ea:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            X.7Na r7 = X.C156837Na.FROM_LOCAL_CACHE
            X.7Ne r0 = X.C156877Ne.IDLE_STATE
            r14 = 1
            goto L_0x0101
        L_0x00f2:
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            if (r5 != r0) goto L_0x00fa
            java.lang.Integer r3 = X.AnonymousClass07B.A00
            if (r11 != r3) goto L_0x00fb
        L_0x00fa:
            r14 = 1
        L_0x00fb:
            X.7Ne r3 = X.C156877Ne.INITIAL_STATE
            if (r5 == r3) goto L_0x0100
            r5 = r0
        L_0x0100:
            r0 = r5
        L_0x0101:
            if (r14 == 0) goto L_0x0134
            r2.set(r13)
            A05(r10, r12, r1, r8)
            if (r15 != 0) goto L_0x0120
            X.0wR r1 = r10.A0K()
            if (r1 == 0) goto L_0x0134
            X.2yh r2 = new X.2yh
            java.lang.Object[] r0 = new java.lang.Object[]{r9, r0, r8, r7}
            r2.<init>(r15, r0)
            java.lang.String r0 = "updateState:BaseGraphQLConnectionSection.updateState"
            r10.A0F(r2, r0)
            return r16
        L_0x0120:
            X.0wR r1 = r10.A0K()
            if (r1 == 0) goto L_0x0134
            X.2yh r2 = new X.2yh
            java.lang.Object[] r0 = new java.lang.Object[]{r9, r0, r8, r7}
            r2.<init>(r15, r0)
            java.lang.String r0 = "updateState:BaseGraphQLConnectionSection.updateState"
            r10.A0G(r2, r0)
        L_0x0134:
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16080wS.AXa(X.10N, java.lang.Object):java.lang.Object");
    }

    public boolean A0K(C16070wR r2, C16070wR r3) {
        if ((this instanceof AnonymousClass1AO) || (this instanceof C16060wQ) || (this instanceof C20271Bt) || (this instanceof C20291Bv)) {
            return true;
        }
        if (r2 == r3) {
            return false;
        }
        if (r2 == null || !r2.A0U(r3)) {
            return true;
        }
        return false;
    }

    public static AnonymousClass10N A02(AnonymousClass1GA r1, int i, Object[] objArr) {
        AnonymousClass10N A06 = r1.A06(i, objArr);
        C16070wR A0K = r1.A0K();
        A0K.A03.A02.A09.A02(A0K.A04, A06);
        return A06;
    }

    public static void A03(AnonymousClass1GA r3, int i) {
        C16070wR A0K = r3.A0K();
        AnonymousClass1AC r32 = r3.A02;
        if (A0K != null && r32 != null) {
            AnonymousClass1AC.A05(r32.A0B, new C30426Ew1(r32, A0K.A04, i));
        }
    }

    public static void A04(AnonymousClass1GA r3, int i, int i2) {
        C16070wR A0K = r3.A0K();
        AnonymousClass1AC r32 = r3.A02;
        if (A0K != null && r32 != null) {
            AnonymousClass1AC.A05(r32.A0B, new C30425Ew0(r32, A0K.A04, i, i2));
        }
    }

    public static void A05(AnonymousClass1GA r2, boolean z, Integer num, Throwable th) {
        AnonymousClass10N r1;
        C16070wR A0K = r2.A0K();
        if (A0K != null) {
            while (true) {
                C16070wR r0 = A0K.A02;
                if (r0 == null) {
                    r1 = r2.A00;
                    break;
                }
                r1 = A0K.A01;
                if (r1 != null) {
                    break;
                }
                A0K = r0;
            }
        } else {
            r1 = null;
        }
        if (r1 != null) {
            C1293464k r02 = new C1293464k();
            r02.A02 = z;
            r02.A00 = num;
            r02.A01 = th;
            r1.A00(r02);
        }
    }
}
