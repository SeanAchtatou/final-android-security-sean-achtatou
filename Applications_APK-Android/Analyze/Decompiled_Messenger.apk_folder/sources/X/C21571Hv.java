package X;

import com.google.common.base.Function;

/* renamed from: X.1Hv  reason: invalid class name and case insensitive filesystem */
public final class C21571Hv implements Function {
    public final /* synthetic */ AnonymousClass17S A00;
    public final /* synthetic */ C33461nc A01;

    public C21571Hv(AnonymousClass17S r1, C33461nc r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public Object apply(Object obj) {
        AnonymousClass101 r4 = (AnonymousClass101) obj;
        if (r4 == null) {
            if (this.A01.A00 == C09510hU.CHECK_SERVER_FOR_NEW_DATA) {
                return AnonymousClass1EO.A00(null);
            }
            return AnonymousClass1EO.A01(null);
        } else if (AnonymousClass17S.A01(this.A00, r4)) {
            return AnonymousClass1EO.A01(r4);
        } else {
            return AnonymousClass1EO.A00(r4);
        }
    }
}
