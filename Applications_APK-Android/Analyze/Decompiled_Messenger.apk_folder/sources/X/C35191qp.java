package X;

import android.os.PowerManager;
import com.facebook.acra.ACRA;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.time.RealtimeSinceBootClock;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Locale;

/* renamed from: X.1qp  reason: invalid class name and case insensitive filesystem */
public final class C35191qp implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.NavigationLogger$4";
    public final /* synthetic */ AnonymousClass146 A00;

    public C35191qp(AnonymousClass146 r1) {
        this.A00 = r1;
    }

    public void run() {
        HashMap hashMap;
        EVR[] evrArr;
        AnonymousClass146 r3 = this.A00;
        synchronized (r3) {
            if (!r3.A08) {
                r3.A08 = true;
                r3.A05 = null;
                RealtimeSinceBootClock.A00.now();
                C09390hE r2 = (C09390hE) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BP0, r3.A03);
                synchronized (r2) {
                    if (r2.A06 != null) {
                        r2.A06.A00("app_backgrounded");
                        C89364Ot r9 = (C89364Ot) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APg, r2.A02);
                        long j = r2.A01;
                        C22361La r6 = r2.A05;
                        EVQ evq = r2.A06;
                        r6.A02("version", 3);
                        r6.A03("session_start_time", j);
                        r6.A03("collection_start_time", evq.A01);
                        r6.A03("serialization_time", r9.A00.now());
                        r6.A03("session_duration", r9.A00.now() - j);
                        r6.A06(ACRA.SESSION_ID_KEY, evq.A02);
                        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
                        synchronized (evq) {
                            ArrayDeque arrayDeque = evq.A00;
                            evrArr = (EVR[]) arrayDeque.toArray(new EVR[arrayDeque.size()]);
                        }
                        for (EVR evr : evrArr) {
                            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
                            objectNode.put("event_name", evr.A01);
                            objectNode.put("event_time_ms", evr.A00);
                            String str = evr.A02;
                            if (str != null) {
                                try {
                                    objectNode.put("event_extra", String.format(Locale.US, str, evr.A03));
                                } catch (Exception e) {
                                    objectNode.put("error", e.getClass().getSimpleName());
                                } catch (Throwable th) {
                                    th = th;
                                    throw th;
                                }
                            }
                            arrayNode.add(objectNode);
                        }
                        r6.A04("events", arrayNode);
                        r6.A0A();
                        r2.A06 = null;
                        r2.A05 = null;
                        r2.A01 = 0;
                    }
                }
                Integer num = AnonymousClass07B.A0i;
                if (!((PowerManager) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A8E, r3.A03)).isScreenOn()) {
                    num = AnonymousClass07B.A0N;
                }
                USLEBaseShape0S0000000 A01 = AnonymousClass146.A01(r3, num);
                AnonymousClass13b r62 = (AnonymousClass13b) AnonymousClass1XX.A02(12, AnonymousClass1Y3.A7w, r3.A03);
                synchronized (r62) {
                    try {
                        AnonymousClass13b.A03(r62);
                        hashMap = new HashMap();
                        int i = AnonymousClass1Y3.AhD;
                        hashMap.putAll(((AnonymousClass2ZS) AnonymousClass1XX.A02(1, i, r62.A04)).A00);
                        ((AnonymousClass2ZS) AnonymousClass1XX.A02(1, i, r62.A04)).A03();
                    } catch (Throwable th2) {
                        th = th2;
                        throw th;
                    }
                }
                if (A01 != null) {
                    A01.A0F("analytic_counters", hashMap);
                }
                C31201jJ r0 = r3.A01;
                if (!(r0 == null || A01 == null)) {
                    A01.A0D("click_point", (String) r0.A00());
                    r3.A0D(null);
                }
                String str2 = r3.A06;
                if (!(str2 == null || A01 == null)) {
                    A01.A0D("custom_session_id", str2);
                    r3.A06 = null;
                }
                AnonymousClass146.A04(r3, A01);
                if (!((AnonymousClass0Ud) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B5j, r3.A03)).A0J()) {
                    AnonymousClass14J r02 = (AnonymousClass14J) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Asq, r3.A03);
                    synchronized (r02.A02) {
                        try {
                            r02.A01.clear();
                        } catch (Throwable th3) {
                            th = th3;
                            throw th;
                        }
                    }
                }
            }
        }
    }
}
