package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.0qS  reason: invalid class name */
public final class AnonymousClass0qS implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.init.Initializer$1";
    public final /* synthetic */ C13480rV A00;
    public final /* synthetic */ C13510rY A01;

    public AnonymousClass0qS(C13510rY r1, C13480rV r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void run() {
        try {
            C13510rY r4 = this.A01;
            C13480rV r3 = this.A00;
            if (r4.A02.compareAndSet(false, true)) {
                try {
                    C005505z.A05("Initializer for %s", r4.A01(), -388918079);
                    r4.A02(r3);
                    C005505z.A00(1684802488);
                    r4.A01.countDown();
                } catch (Throwable th) {
                    r4.A01.countDown();
                    throw th;
                }
            }
        } catch (Throwable th2) {
            AnonymousClass00S.A03(new Handler(Looper.getMainLooper()), new AnonymousClass7X8(th2), -302439933);
        }
    }
}
