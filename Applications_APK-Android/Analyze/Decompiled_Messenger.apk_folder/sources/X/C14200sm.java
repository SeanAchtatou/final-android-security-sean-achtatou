package X;

import com.facebook.omnistore.OmnistoreCustomLogger;

/* renamed from: X.0sm  reason: invalid class name and case insensitive filesystem */
public final class C14200sm implements OmnistoreCustomLogger {
    public int getAnalyticsEventBuilderId(String str, String str2) {
        return 0;
    }

    public void logAnalyticsEvent(int i, String str, String str2, String str3) {
    }

    public void logCounter(String str, int i) {
    }

    public void logDebug(String str, String str2) {
    }

    public void logError(String str, String str2) {
        C010708t.A0I(str, str2);
    }

    public void logInfo(String str, String str2) {
    }

    public void logWarning(String str, String str2) {
        C010708t.A0J(str, str2);
    }
}
