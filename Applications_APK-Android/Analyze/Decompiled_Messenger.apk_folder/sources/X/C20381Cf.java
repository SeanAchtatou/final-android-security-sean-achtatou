package X;

import com.facebook.stickers.model.StickerPack;

/* renamed from: X.1Cf  reason: invalid class name and case insensitive filesystem */
public final class C20381Cf extends C06020ai {
    public final /* synthetic */ C55162nd A00;
    public final /* synthetic */ StickerPack A01;

    public C20381Cf(C55162nd r1, StickerPack stickerPack) {
        this.A00 = r1;
        this.A01 = stickerPack;
    }

    public void dispose() {
        super.dispose();
        C010708t.A0B(C55162nd.A07, "Image download for pack %s cancelled.", this.A01.A0B);
        C55162nd.A01(this.A00, false, this.A01);
    }
}
