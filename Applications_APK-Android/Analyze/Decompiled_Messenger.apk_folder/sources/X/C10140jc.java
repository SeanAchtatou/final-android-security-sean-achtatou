package X;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;

/* renamed from: X.0jc  reason: invalid class name and case insensitive filesystem */
public abstract class C10140jc implements AnonymousClass0jN, Serializable {
    public C10160je findAutoDetectVisibility(C10070jV r1, C10160je r2) {
        return r2;
    }

    public Object findContentDeserializer(C10080jW r2) {
        return null;
    }

    public Object findContentSerializer(C10080jW r2) {
        return null;
    }

    public Object findDeserializationContentConverter(C183512m r2) {
        return null;
    }

    public Class findDeserializationContentType(C10080jW r2, C10030jR r3) {
        return null;
    }

    public Object findDeserializationConverter(C10080jW r2) {
        return null;
    }

    public Class findDeserializationKeyType(C10080jW r2, C10030jR r3) {
        return null;
    }

    public String findDeserializationName(AnonymousClass137 r2) {
        return null;
    }

    public String findDeserializationName(C29091fr r2) {
        return null;
    }

    public String findDeserializationName(C29141fw r2) {
        return null;
    }

    public Class findDeserializationType(C10080jW r2, C10030jR r3) {
        return null;
    }

    public Object findDeserializer(C10080jW r2) {
        return null;
    }

    public Object findFilterId(C10070jV r2) {
        return null;
    }

    public CX6 findFormat(C183512m r2) {
        return null;
    }

    public Boolean findIgnoreUnknownProperties(C10070jV r2) {
        return null;
    }

    public Object findInjectableValueId(C183512m r2) {
        return null;
    }

    public Object findKeyDeserializer(C10080jW r2) {
        return null;
    }

    public Object findKeySerializer(C10080jW r2) {
        return null;
    }

    public Object findNamingStrategy(C10070jV r2) {
        return null;
    }

    public CZj findObjectIdInfo(C10080jW r2) {
        return null;
    }

    public CZj findObjectReferenceInfo(C10080jW r1, CZj cZj) {
        return cZj;
    }

    public Class findPOJOBuilder(C10070jV r2) {
        return null;
    }

    public BM3 findPOJOBuilderConfig(C10070jV r2) {
        return null;
    }

    public String[] findPropertiesToIgnore(C10080jW r2) {
        return null;
    }

    public CXQ findPropertyContentTypeResolver(C10470kA r2, C183512m r3, C10030jR r4) {
        return null;
    }

    public CXQ findPropertyTypeResolver(C10470kA r2, C183512m r3, C10030jR r4) {
        return null;
    }

    public C860046h findReferenceType(C183512m r2) {
        return null;
    }

    public C87974Hw findRootName(C10070jV r2) {
        return null;
    }

    public Object findSerializationContentConverter(C183512m r2) {
        return null;
    }

    public Class findSerializationContentType(C10080jW r2, C10030jR r3) {
        return null;
    }

    public Object findSerializationConverter(C10080jW r2) {
        return null;
    }

    public AnonymousClass0oC findSerializationInclusion(C10080jW r1, AnonymousClass0oC r2) {
        return r2;
    }

    public Class findSerializationKeyType(C10080jW r2, C10030jR r3) {
        return null;
    }

    public String findSerializationName(C29091fr r2) {
        return null;
    }

    public String findSerializationName(C29141fw r2) {
        return null;
    }

    public String[] findSerializationPropertyOrder(C10070jV r2) {
        return null;
    }

    public Boolean findSerializationSortAlphabetically(C10070jV r2) {
        return null;
    }

    public Class findSerializationType(C10080jW r2) {
        return null;
    }

    public AnonymousClass290 findSerializationTyping(C10080jW r2) {
        return null;
    }

    public Object findSerializer(C10080jW r2) {
        return null;
    }

    public List findSubtypes(C10080jW r2) {
        return null;
    }

    public String findTypeName(C10070jV r2) {
        return null;
    }

    public CXQ findTypeResolver(C10470kA r2, C10070jV r3, C10030jR r4) {
        return null;
    }

    public C25133CaS findUnwrappingNameTransformer(C183512m r2) {
        return null;
    }

    public Object findValueInstantiator(C10070jV r2) {
        return null;
    }

    public Class[] findViews(C10080jW r2) {
        return null;
    }

    public boolean hasAnyGetterAnnotation(C29141fw r2) {
        return false;
    }

    public boolean hasAnySetterAnnotation(C29141fw r2) {
        return false;
    }

    public boolean hasAsValueAnnotation(C29141fw r2) {
        return false;
    }

    public boolean hasCreatorAnnotation(C10080jW r2) {
        return false;
    }

    public boolean hasIgnoreMarker(C183512m r2) {
        return false;
    }

    public Boolean hasRequiredMarker(C183512m r2) {
        return null;
    }

    public boolean isAnnotationBundle(Annotation annotation) {
        return false;
    }

    public Boolean isIgnorableType(C10070jV r2) {
        return null;
    }

    public Boolean isTypeId(C183512m r2) {
        return null;
    }

    public abstract C11780nw version();

    public C87974Hw findNameForDeserialization(C10080jW r4) {
        String str;
        if (r4 instanceof C29091fr) {
            str = findDeserializationName((C29091fr) r4);
        } else if (r4 instanceof C29141fw) {
            str = findDeserializationName((C29141fw) r4);
        } else if (r4 instanceof AnonymousClass137) {
            str = findDeserializationName((AnonymousClass137) r4);
        } else {
            str = null;
        }
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return C87974Hw.USE_DEFAULT;
        }
        return new C87974Hw(str, null);
    }

    public C87974Hw findNameForSerialization(C10080jW r4) {
        String str;
        if (r4 instanceof C29091fr) {
            str = findSerializationName((C29091fr) r4);
        } else if (r4 instanceof C29141fw) {
            str = findSerializationName((C29141fw) r4);
        } else {
            str = null;
        }
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return C87974Hw.USE_DEFAULT;
        }
        return new C87974Hw(str, null);
    }

    public CX6 findFormat(C10080jW r2) {
        if (r2 instanceof C183512m) {
            return findFormat((C183512m) r2);
        }
        return null;
    }
}
