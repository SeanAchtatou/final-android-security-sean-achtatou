package X;

import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.User;
import com.google.common.base.Objects;

/* renamed from: X.0mO  reason: invalid class name and case insensitive filesystem */
public final class C11210mO {
    public final AnonymousClass04b A00 = new AnonymousClass04b();
    public final AnonymousClass04b A01 = new AnonymousClass04b();
    public final AnonymousClass0mL A02;
    public final AnonymousClass0m9 A03;

    public Message A00(ThreadKey threadKey, String str) {
        this.A02.A01();
        MessagesCollection messagesCollection = (MessagesCollection) this.A01.get(threadKey);
        if (messagesCollection != null) {
            C24971Xv it = messagesCollection.A01.iterator();
            while (it.hasNext()) {
                Message message = (Message) it.next();
                if (Objects.equal(message.A0w, str)) {
                    return message;
                }
            }
        }
        return null;
    }

    public Message A01(String str) {
        this.A02.A01();
        return (Message) this.A00.get(str);
    }

    public MessagesCollection A02(ThreadKey threadKey) {
        this.A02.A01();
        return (MessagesCollection) this.A01.get(threadKey);
    }

    public void A03() {
        this.A02.A01();
        this.A01.clear();
        this.A00.clear();
        AnonymousClass0m9 r2 = this.A03;
        synchronized (r2) {
            if (r2.A0F()) {
                r2.A03.clear();
                C71183bx A012 = AnonymousClass0m9.A01(r2, null, null, "clearAllMessagesFromCache", null);
                r2.A01.put(A012, A012);
            }
        }
    }

    public void A04(MessagesCollection messagesCollection, User user, boolean z, boolean z2) {
        String bool;
        String str;
        MessagesCollection A022;
        this.A02.A01();
        ThreadKey threadKey = messagesCollection.A00;
        if (threadKey == null) {
            C010708t.A0K("MessagesCache", "Null thread key");
        } else {
            threadKey.toString();
        }
        if (((user != null && user.A14) || C010708t.A0X(3)) && !z && !z2 && threadKey != null && (A022 = A02(threadKey)) != null && !A022.A08()) {
            C196249Kq A002 = AnonymousClass0m9.A00(A022.A01, false);
            C196249Kq A003 = AnonymousClass0m9.A00(messagesCollection.A01, false);
            if (C196249Kq.A02(A002, A003)) {
                String format = String.format("newLatest=%s, newSecondLatest=%s, oldLatest=%s, oldSecondLatest=%s", Message.A01(A003.A00), Message.A01(A003.A01), Message.A01(A002.A00), Message.A01(A002.A01));
                AnonymousClass0m9 r3 = this.A03;
                synchronized (r3) {
                    C71183bx A012 = AnonymousClass0m9.A01(r3, threadKey, null, "wrongMessagesCollectionUpdate", format);
                    r3.A01.put(A012, A012);
                }
                C010708t.A0N("wrongMessagesCollectionUpdate", format, new RuntimeException("Wrong MessagesCollection update"));
            }
        }
        A05(threadKey);
        this.A02.A01();
        C24971Xv it = messagesCollection.A01.iterator();
        while (it.hasNext()) {
            Message message = (Message) it.next();
            this.A00.put(message.A0q, message);
        }
        this.A01.put(messagesCollection.A00, messagesCollection);
        AnonymousClass0m9 r1 = this.A03;
        synchronized (r1) {
            AnonymousClass0m9.A04(r1, messagesCollection, "putMessagesIntoCache");
        }
        if ((user != null && user.A14) || C010708t.A0X(3)) {
            boolean z3 = false;
            if (MessagesCollection.A03(messagesCollection.A01) == null) {
                z3 = true;
            }
            if (!z3) {
                if (threadKey == null) {
                    bool = "Unknown";
                } else {
                    boolean z4 = false;
                    if (threadKey.A05 == C28711fF.ONE_TO_ONE) {
                        z4 = true;
                    }
                    bool = Boolean.toString(z4);
                }
                if (messagesCollection.A04() > 100) {
                    str = "Thread messages is not in order in cache";
                } else {
                    str = "Thread messages is not in order in cache, isCanonicalThread=" + bool + ", messagesCollection=" + messagesCollection;
                }
                C010708t.A0K("MessagesOutOfOrderInCache", str);
            }
        }
    }

    public void A05(ThreadKey threadKey) {
        C196249Kq r1;
        this.A02.A01();
        if (threadKey != null && this.A03.A0F()) {
            threadKey.toString();
            AnonymousClass0m9 r4 = this.A03;
            MessagesCollection A022 = A02(threadKey);
            synchronized (r4) {
                if (r4.A0F() && AnonymousClass0m9.A05(threadKey)) {
                    r4.A03.remove(threadKey);
                    if (A022 == null) {
                        r1 = null;
                    } else {
                        r1 = AnonymousClass0m9.A00(A022.A01, true);
                    }
                    C71183bx A012 = AnonymousClass0m9.A01(r4, threadKey, r1, "removeMessagesFromCache", null);
                    r4.A01.put(A012, A012);
                }
            }
        }
        MessagesCollection messagesCollection = (MessagesCollection) this.A01.remove(threadKey);
        if (messagesCollection != null) {
            C24971Xv it = messagesCollection.A01.iterator();
            while (it.hasNext()) {
                this.A00.remove(((Message) it.next()).A0q);
            }
        }
    }

    public C11210mO(AnonymousClass0mL r2, AnonymousClass0m9 r3) {
        this.A02 = r2;
        this.A03 = r3;
    }
}
