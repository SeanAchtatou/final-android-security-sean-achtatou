package X;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.12c  reason: invalid class name */
public final class AnonymousClass12c {
    private static volatile AnonymousClass12c A01;
    public List A00 = new ArrayList();

    public static final AnonymousClass12c A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass12c.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new AnonymousClass12c();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A01() {
        for (AnonymousClass12d r0 : this.A00) {
            C28191eP r3 = r0.A00;
            if (C28191eP.A0G(r3) && !((C47852Yi) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AHR, r3.A02)).A03().equals("none")) {
                int i = r3.A00 + 1;
                r3.A00 = i;
                int i2 = r3.A01;
                if (i >= i2) {
                    r3.A00 = 0;
                    r3.A01 = i2 * 3;
                    C28191eP.A08(r3, (AnonymousClass157) r3.A06.get(), "header_error_force_fetch");
                }
            }
        }
    }
}
