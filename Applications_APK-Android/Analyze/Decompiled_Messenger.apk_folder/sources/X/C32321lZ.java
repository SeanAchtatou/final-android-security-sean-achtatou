package X;

import android.content.Context;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.facebook.messaging.threadview.params.ThreadViewParams;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1lZ  reason: invalid class name and case insensitive filesystem */
public class C32321lZ implements C32331la {
    public C32331la A00;

    public void ATE() {
        this.A00.ATE();
    }

    public void ATF() {
        this.A00.ATF();
    }

    public void ATI() {
        this.A00.ATI();
    }

    public void ATL() {
        this.A00.ATL();
    }

    public void BvP(Context context) {
        this.A00.BvP(context);
    }

    public void BvS(Context context, NavigationTrigger navigationTrigger, MontageComposerFragmentParams montageComposerFragmentParams) {
        this.A00.BvS(context, navigationTrigger, montageComposerFragmentParams);
    }

    public void BvV(Context context, ImmutableList immutableList) {
        this.A00.BvV(context, immutableList);
    }

    public void Bvd(Context context, ThreadKey threadKey, C149266wB r4) {
        this.A00.Bvd(context, threadKey, r4);
    }

    public void Bve(C64663Ct r2) {
        this.A00.Bve(r2);
    }

    public void Bvn(Context context, C13060qW r3, String str) {
        this.A00.Bvn(context, r3, str);
    }

    public void Bvo(Context context) {
        this.A00.Bvo(context);
    }

    public void Bvw(Context context) {
        this.A00.Bvw(context);
    }

    public void Bvx() {
        this.A00.Bvx();
    }

    public void Bw0(Context context) {
        this.A00.Bw0(context);
    }

    public void BwC(ThreadViewParams threadViewParams) {
        this.A00.BwC(threadViewParams);
    }

    public void CKx(int i) {
        this.A00.CKx(i);
    }
}
