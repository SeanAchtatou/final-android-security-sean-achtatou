package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0PC  reason: invalid class name */
public final class AnonymousClass0PC implements FilenameFilter {
    public boolean accept(File file, String str) {
        return str.endsWith("_tmp");
    }
}
