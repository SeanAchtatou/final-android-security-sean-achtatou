package X;

import android.content.Context;
import com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleParamIdentifier;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.messaging.model.threads.ThreadConnectivityContextParam;
import com.facebook.messaging.model.threads.ThreadConnectivityData;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1Io  reason: invalid class name and case insensitive filesystem */
public final class C21751Io extends C17770zR {
    public static final int A05 = AnonymousClass1JQ.LARGE.B3A();
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public InboxUnitThreadItem A01;
    @Comparable(type = 13)
    public AnonymousClass1BN A02;
    @Comparable(type = 13)
    public AnonymousClass1BO A03;
    @Comparable(type = 13)
    public MigColorScheme A04;

    public C21751Io(Context context) {
        super("M4ThreadItemContentComponent");
        this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }

    public static String A00(ThreadConnectivityData threadConnectivityData, GraphQLThreadConnectivityStatusSubtitleParamIdentifier graphQLThreadConnectivityStatusSubtitleParamIdentifier) {
        C24971Xv it = threadConnectivityData.A02.iterator();
        while (it.hasNext()) {
            ThreadConnectivityContextParam threadConnectivityContextParam = (ThreadConnectivityContextParam) it.next();
            if (threadConnectivityContextParam.A01().equals(graphQLThreadConnectivityStatusSubtitleParamIdentifier)) {
                return threadConnectivityContextParam.A00;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00fa, code lost:
        if (r13.A00.Aem(282497179059643L) == false) goto L_0x00fc;
     */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0342  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C17770zR A0O(X.AnonymousClass0p4 r21) {
        /*
            r20 = this;
            r2 = r20
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r6 = r2.A01
            X.1BN r0 = r2.A02
            r19 = r0
            X.1BO r10 = r2.A03
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r2.A04
            int r1 = X.AnonymousClass1Y3.APM
            X.0UN r0 = r2.A00
            r9 = 1
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.0uK r11 = (X.C14900uK) r11
            r3 = r21
            X.1wd r2 = X.C16980y8.A00(r3)
            r12 = 3
            java.lang.String r4 = "colorScheme"
            java.lang.String r1 = "montageListener"
            java.lang.String r0 = "threadTileViewData"
            java.lang.String[] r8 = new java.lang.String[]{r4, r1, r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r12)
            X.1JF r4 = new X.1JF
            android.content.Context r0 = r3.A09
            r4.<init>(r0)
            X.0z4 r12 = r3.A0B
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x003e
            java.lang.String r1 = r0.A06
            r4.A07 = r1
        L_0x003e:
            r5.clear()
            r1 = 4
            X.11G r0 = r4.A14()
            r0.A0A(r1)
            X.1H4 r0 = r6.A05
            r4.A05 = r0
            r0 = 2
            r5.set(r0)
            r4.A04 = r7
            r0 = 0
            r5.set(r0)
            r4.A02 = r10
            r5.set(r9)
            X.10G r10 = X.AnonymousClass10G.A09
            X.1JQ r0 = X.AnonymousClass1JQ.SMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            int r1 = r12.A00(r0)
            X.11G r0 = r4.A14()
            r0.BJx(r10, r1)
            X.10G r10 = X.AnonymousClass10G.END
            int r0 = X.C21751Io.A05
            float r0 = (float) r0
            int r1 = r12.A00(r0)
            X.11G r0 = r4.A14()
            r0.BJx(r10, r1)
            r1 = 0
            X.11G r0 = r4.A14()
            r0.AaD(r1)
            r0 = 3
            X.AnonymousClass11F.A0C(r0, r5, r8)
            r2.A3A(r4)
            X.1we r1 = X.AnonymousClass11D.A00(r3)
            X.1Jr r17 = new X.1Jr
            r17.<init>()
            X.0z4 r0 = r3.A0B
            r18 = r0
            r4 = r17
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x00a6
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x00a6:
            X.1we r8 = X.AnonymousClass11D.A00(r3)
            com.facebook.messaging.model.threads.ThreadSummary r13 = r6.A01
            int r5 = X.AnonymousClass1Y3.B7T
            X.0UN r4 = r11.A00
            r0 = 6
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r5, r4)
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            java.lang.String r12 = ""
            if (r0 == 0) goto L_0x0117
            int r0 = X.AnonymousClass1Y3.AIh
            X.AnonymousClass1XX.A03(r0, r4)
            com.facebook.messaging.model.threads.ThreadConnectivityData r10 = r13.A0d
            boolean r0 = X.AnonymousClass1H9.A01(r13)
            if (r0 == 0) goto L_0x0117
            if (r10 == 0) goto L_0x0117
            com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleType r4 = r10.A01()
            com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleType r0 = com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleType.A04
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0117
            r5 = 7
            int r4 = X.AnonymousClass1Y3.AG3
            X.0UN r0 = r11.A00
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.9Zr r13 = (X.C199289Zr) r13
            X.1Yd r0 = r13.A00
            r4 = 282497178928570(0x100ee000005ba, double:1.39572151155674E-309)
            boolean r0 = r0.Aem(r4)
            if (r0 == 0) goto L_0x00fc
            X.1Yd r0 = r13.A00
            r4 = 282497179059643(0x100ee000205bb, double:1.39572151220433E-309)
            boolean r4 = r0.Aem(r4)
            r0 = 1
            if (r4 != 0) goto L_0x00fd
        L_0x00fc:
            r0 = 0
        L_0x00fd:
            if (r0 == 0) goto L_0x0117
            r5 = 8
            int r4 = X.AnonymousClass1Y3.Atm
            X.0UN r0 = r11.A00
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.3zA r11 = (X.C84283zA) r11
            com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleType r0 = r10.A01()
            int r0 = r0.ordinal()
            r5 = 0
            switch(r0) {
                case 2: goto L_0x03c7;
                case 3: goto L_0x041a;
                case 4: goto L_0x0117;
                case 5: goto L_0x0117;
                case 6: goto L_0x044c;
                case 7: goto L_0x0117;
                case 8: goto L_0x0407;
                case 9: goto L_0x03d5;
                default: goto L_0x0117;
            }
        L_0x0117:
            boolean r0 = X.C06850cB.A0B(r12)
            if (r0 != 0) goto L_0x03c4
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r4 = X.C17030yD.A00(r3)
            r4.A3l(r12)
            X.10J r0 = X.AnonymousClass10J.A0C
            r4.A3f(r0)
            r4.A3c(r7)
            X.0yD r0 = r4.A39()
        L_0x0130:
            r8.A3A(r0)
            X.1wd r10 = X.C16980y8.A00(r3)
            java.lang.String r5 = "colorScheme"
            java.lang.String[] r12 = new java.lang.String[]{r5}
            java.util.BitSet r11 = new java.util.BitSet
            r11.<init>(r9)
            X.1RG r4 = new X.1RG
            android.content.Context r0 = r3.A09
            r4.<init>(r0)
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x0151
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x0151:
            r11.clear()
            r4.A03 = r7
            r0 = 0
            r11.set(r0)
            X.AnonymousClass11F.A0C(r9, r11, r12)
            r10.A3A(r4)
            com.facebook.messaging.model.threads.ThreadSummary r0 = r6.A01
            if (r0 == 0) goto L_0x03c1
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r0.A0W
            if (r0 == 0) goto L_0x03c1
            com.facebook.messaging.model.threads.ProfileSellingInvoice r0 = r0.A02
            if (r0 == 0) goto L_0x03c1
            com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum r0 = r0.A00
            if (r0 == 0) goto L_0x03c1
        L_0x0170:
            r16 = 0
            if (r0 == 0) goto L_0x017b
            int r0 = r0.ordinal()
            switch(r0) {
                case 4: goto L_0x031f;
                case 5: goto L_0x017b;
                case 6: goto L_0x017b;
                case 7: goto L_0x0317;
                default: goto L_0x017b;
            }
        L_0x017b:
            r0 = r16
            r10.A39(r0)
            X.0uO r0 = X.C14940uO.CENTER
            r10.A3D(r0)
            X.0y8 r0 = r10.A00
            r8.A3A(r0)
            X.1wd r9 = X.C16980y8.A00(r3)
            r12 = 0
            r0 = 1
            java.lang.String[] r11 = new java.lang.String[]{r5}
            java.util.BitSet r10 = new java.util.BitSet
            r10.<init>(r0)
            X.1RH r4 = new X.1RH
            android.content.Context r0 = r3.A09
            r4.<init>(r0)
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x01a8
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x01a8:
            r10.clear()
            r4.A03 = r7
            r10.set(r12)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r10, r11)
            r9.A3A(r4)
            java.lang.String[] r12 = new java.lang.String[]{r5}
            java.util.BitSet r11 = new java.util.BitSet
            r11.<init>(r0)
            X.1ks r10 = new X.1ks
            android.content.Context r0 = r3.A09
            r10.<init>(r0)
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x01cf
            java.lang.String r4 = r0.A06
            r10.A07 = r4
        L_0x01cf:
            r11.clear()
            r10.A03 = r7
            r0 = 0
            r11.set(r0)
            r4 = 0
            X.11G r0 = r10.A14()
            r0.AaD(r4)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r11, r12)
            r9.A3A(r10)
            X.10G r4 = X.AnonymousClass10G.TOP
            X.1JQ r0 = X.AnonymousClass1JQ.XXSMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            r9.A2X(r4, r0)
            X.0y8 r0 = r9.A00
            r8.A3A(r0)
            r0 = 1065353216(0x3f800000, float:1.0)
            r8.A1n(r0)
            X.11D r9 = r8.A00
            r8 = 3
            java.lang.String r4 = "item"
            java.lang.String r0 = "listener"
            java.lang.String[] r11 = new java.lang.String[]{r5, r4, r0}
            java.util.BitSet r10 = new java.util.BitSet
            r10.<init>(r8)
            X.1J6 r8 = new X.1J6
            android.content.Context r0 = r3.A09
            r8.<init>(r0)
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x021c
            java.lang.String r4 = r0.A06
            r8.A07 = r4
        L_0x021c:
            r10.clear()
            r8.A01 = r6
            r0 = 1
            r10.set(r0)
            r8.A03 = r7
            r0 = 0
            r10.set(r0)
            r0 = r19
            r8.A02 = r0
            r0 = 2
            r10.set(r0)
            r4 = 0
            X.11G r0 = r8.A14()
            r0.AaD(r4)
            r0 = 3
            X.AnonymousClass11F.A0C(r0, r10, r11)
            r4 = 2
            java.lang.String r0 = "item"
            java.lang.String[] r11 = new java.lang.String[]{r5, r0}
            java.util.BitSet r10 = new java.util.BitSet
            r10.<init>(r4)
            X.1RJ r5 = new X.1RJ
            android.content.Context r0 = r3.A09
            r5.<init>(r0)
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x025a
            java.lang.String r4 = r0.A06
            r5.A07 = r4
        L_0x025a:
            r10.clear()
            r5.A01 = r6
            r0 = 1
            r10.set(r0)
            r5.A03 = r7
            r0 = 0
            r10.set(r0)
            r4 = 0
            X.11G r0 = r5.A14()
            r0.AaD(r4)
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r10, r11)
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r9, r8, r5)
            if (r4 == 0) goto L_0x0289
            r0 = r17
            java.util.List r0 = r0.A01
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x030e
            r0 = r17
            r0.A01 = r4
        L_0x0289:
            int r0 = X.C21751Io.A05
            float r4 = (float) r0
            r0 = r18
            int r4 = r0.A00(r4)
            r0 = r17
            r0.A00 = r4
            r1.A3A(r0)
            X.10G r4 = X.AnonymousClass10G.TOP
            X.1JQ r0 = X.AnonymousClass1JQ.SMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            r1.A2X(r4, r0)
            X.10G r4 = X.AnonymousClass10G.BOTTOM
            X.1JQ r0 = X.AnonymousClass1JQ.SMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            r1.A2X(r4, r0)
            r4 = 1065353216(0x3f800000, float:1.0)
            r1.A1n(r4)
            r2.A39(r1)
            X.10G r1 = X.AnonymousClass10G.A04
            X.1JQ r0 = X.AnonymousClass1JQ.XLARGE
            int r0 = r0.B3A()
            float r0 = (float) r0
            r2.A2Z(r1, r0)
            X.0uO r0 = X.C14940uO.CENTER
            r2.A3D(r0)
            r2.A1n(r4)
            java.lang.String r0 = "thread_list_item"
            r2.A2p(r0)
            X.0y8 r6 = r2.A00
            r1 = 1
            java.lang.String r0 = "content"
            java.lang.String[] r5 = new java.lang.String[]{r0}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r1)
            X.1J1 r2 = new X.1J1
            android.content.Context r0 = r3.A09
            r2.<init>(r0)
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x02ef
            java.lang.String r1 = r0.A06
            r2.A07 = r1
        L_0x02ef:
            r4.clear()
            if (r6 != 0) goto L_0x0309
            r0 = 0
        L_0x02f5:
            r2.A01 = r0
            r0 = 0
            r4.set(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            X.11G r0 = r2.A14()
            r0.AaB(r1)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r2
        L_0x0309:
            X.0zR r0 = r6.A16()
            goto L_0x02f5
        L_0x030e:
            r0 = r17
            java.util.List r0 = r0.A01
            r0.addAll(r4)
            goto L_0x0289
        L_0x0317:
            r0 = 2131827023(0x7f11194f, float:1.9286947E38)
            java.lang.String r15 = r3.A0C(r0)
            goto L_0x0326
        L_0x031f:
            r0 = 2131827020(0x7f11194c, float:1.928694E38)
            java.lang.String r15 = r3.A0C(r0)
        L_0x0326:
            X.1wd r16 = X.C16980y8.A00(r3)
            java.lang.String r0 = "text"
            java.lang.String[] r13 = new java.lang.String[]{r0}
            java.util.BitSet r12 = new java.util.BitSet
            r12.<init>(r9)
            X.1Kx r11 = new X.1Kx
            android.content.Context r0 = r3.A09
            r11.<init>(r0)
            X.0z4 r4 = r3.A0B
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x0346
            java.lang.String r14 = r0.A06
            r11.A07 = r14
        L_0x0346:
            r12.clear()
            r11.A0S = r15
            r0 = 0
            r12.set(r0)
            X.10M r14 = X.AnonymousClass10M.ROBOTO_BOLD
            android.content.Context r0 = r3.A09
            android.graphics.Typeface r0 = r14.A00(r0)
            r11.A0L = r0
            r0 = 1093664768(0x41300000, float:11.0)
            int r0 = r4.A01(r0)
            r11.A0I = r0
            X.0y3 r0 = r7.Aqj()
            int r0 = r0.Aha()
            int r0 = r4.A02(r0)
            r11.A0H = r0
            r11.A0B = r9
            X.36k r14 = new X.36k
            android.content.Context r9 = r3.A09
            r0 = 1082130432(0x40800000, float:4.0)
            int r0 = X.C007106r.A00(r9, r0)
            float r9 = (float) r0
            int r0 = r7.B2X()
            r14.<init>(r0)
            r14.CB6(r9)
            X.11G r0 = r11.A14()
            r0.A0C(r14)
            X.10G r14 = X.AnonymousClass10G.A04
            r0 = 1082130432(0x40800000, float:4.0)
            int r9 = r4.A00(r0)
            X.11G r0 = r11.A14()
            r0.BwM(r14, r9)
            X.10G r9 = X.AnonymousClass10G.A09
            r0 = 1065353216(0x3f800000, float:1.0)
            int r4 = r4.A00(r0)
            X.11G r0 = r11.A14()
            r0.BwM(r9, r4)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r12, r13)
            r0 = r16
            r0.A3A(r11)
            X.10G r9 = X.AnonymousClass10G.START
            r4 = 1086324736(0x40c00000, float:6.0)
            r0.A2X(r9, r4)
            r4 = 0
            r0.A1o(r4)
            goto L_0x017b
        L_0x03c1:
            r0 = 0
            goto L_0x0170
        L_0x03c4:
            r0 = 0
            goto L_0x0130
        L_0x03c7:
            int r4 = X.AnonymousClass1Y3.AZY
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0aH r0 = (X.C05760aH) r0
            r4 = 2131823143(0x7f110a27, float:1.9279077E38)
            goto L_0x0414
        L_0x03d5:
            com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleParamIdentifier r0 = com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleParamIdentifier.NAME
            java.lang.String r10 = A00(r10, r0)
            if (r10 == 0) goto L_0x03f4
            int r4 = X.AnonymousClass1Y3.AZY
            X.0UN r0 = r11.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0aH r5 = (X.C05760aH) r5
            r4 = 2131823147(0x7f110a2b, float:1.9279085E38)
            java.lang.Object[] r0 = new java.lang.Object[]{r10}
            java.lang.String r12 = r5.getString(r4, r0)
            goto L_0x0117
        L_0x03f4:
            int r4 = X.AnonymousClass1Y3.AZY
            X.0UN r0 = r11.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0aH r4 = (X.C05760aH) r4
            r0 = 2131823148(0x7f110a2c, float:1.9279087E38)
            java.lang.String r12 = r4.getString(r0)
            goto L_0x0117
        L_0x0407:
            int r4 = X.AnonymousClass1Y3.AZY
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0aH r0 = (X.C05760aH) r0
            r4 = 2131823149(0x7f110a2d, float:1.927909E38)
        L_0x0414:
            java.lang.String r12 = r0.getString(r4)
            goto L_0x0117
        L_0x041a:
            com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleParamIdentifier r0 = com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleParamIdentifier.NAME
            java.lang.String r10 = A00(r10, r0)
            if (r10 == 0) goto L_0x0439
            int r4 = X.AnonymousClass1Y3.AZY
            X.0UN r0 = r11.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0aH r5 = (X.C05760aH) r5
            r4 = 2131823150(0x7f110a2e, float:1.9279092E38)
            java.lang.Object[] r0 = new java.lang.Object[]{r10}
            java.lang.String r12 = r5.getString(r4, r0)
            goto L_0x0117
        L_0x0439:
            int r4 = X.AnonymousClass1Y3.AZY
            X.0UN r0 = r11.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0aH r4 = (X.C05760aH) r4
            r0 = 2131823151(0x7f110a2f, float:1.9279094E38)
            java.lang.String r12 = r4.getString(r0)
            goto L_0x0117
        L_0x044c:
            com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleParamIdentifier r0 = com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleParamIdentifier.COUNT
            java.lang.String r12 = A00(r10, r0)
            if (r12 == 0) goto L_0x0477
            int r4 = X.AnonymousClass1Y3.AZY
            X.0UN r0 = r11.A00
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0aH r10 = (X.C05760aH) r10
            r5 = 2131689500(0x7f0f001c, float:1.9008017E38)
            int r4 = java.lang.Integer.parseInt(r12)
            int r0 = java.lang.Integer.parseInt(r12)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            java.lang.String r12 = r10.getQuantityString(r5, r4, r0)
            goto L_0x0117
        L_0x0477:
            int r4 = X.AnonymousClass1Y3.AZY
            X.0UN r0 = r11.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0aH r4 = (X.C05760aH) r4
            r0 = 2131823152(0x7f110a30, float:1.9279096E38)
            java.lang.String r12 = r4.getString(r0)
            goto L_0x0117
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21751Io.A0O(X.0p4):X.0zR");
    }
}
