package X;

/* renamed from: X.1zZ  reason: invalid class name and case insensitive filesystem */
public final class C39761zZ implements C28501Dvy {
    public final /* synthetic */ C39751zY A00;

    public C39761zZ(C39751zY r1) {
        this.A00 = r1;
    }

    public void BgR(Object obj) {
        C39751zY r3 = this.A00;
        float floatValue = ((Float) obj).floatValue();
        if (!r3.A04) {
            r3.A0F.A04(r3.A0E, new C28191DqJ(-1.0d, null, (double) floatValue, -1.0d), null);
            r3.A07.A00.A0F.BQw();
        }
    }
}
