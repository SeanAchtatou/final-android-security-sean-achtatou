package X;

import android.content.Context;

/* renamed from: X.1hz  reason: invalid class name and case insensitive filesystem */
public final class C30401hz {
    public AnonymousClass0UN A00;
    private Object A01;
    private boolean A02 = false;
    public final C27321d0 A03 = C27321d0.A00;
    private final Context A04;
    private final C72463eK A05 = null;

    public static synchronized void A00(C30401hz r3) {
        synchronized (r3) {
            if (!r3.A02) {
                Context context = r3.A04;
                if (context != null) {
                    r3.A00 = new AnonymousClass0UN(7, AnonymousClass1XX.get(context));
                    r3.A02 = true;
                } else {
                    throw new NullPointerException("If you use @PluginInjectProp, you must put a non-null context in MessengerSecureMessageInterface.create() as the second parameter");
                }
            }
        }
    }

    public static boolean A01(C30401hz r4) {
        Boolean bool;
        boolean z;
        if (r4.A01 == null) {
            C27271cv.A02.getAndIncrement();
            r4.A03.A06(C22298Ase.$const$string(30), C22298Ase.$const$string(31));
            try {
                C72463eK r1 = r4.A05;
                if (r1 == null) {
                    bool = null;
                } else {
                    bool = r1.isNeeded("com.facebook.messenger.msys.plugins.messengermsys.MessengerMsysKillSwitch");
                }
                if (bool != null) {
                    z = bool.booleanValue();
                } else {
                    Boolean bool2 = C27351d3.A00;
                    if (bool2 != null) {
                        z = bool2.booleanValue();
                    } else {
                        z = C27351d3.A00(r4.A03);
                    }
                }
                if (z) {
                    r4.A01 = C27271cv.A00;
                } else {
                    r4.A01 = C27271cv.A01;
                }
                r4.A03.A02();
            } catch (Exception e) {
                r4.A01 = C27271cv.A01;
                throw e;
            } catch (Throwable th) {
                r4.A03.A02();
                throw th;
            }
        }
        if (r4.A01 == C27271cv.A01) {
            return false;
        }
        return true;
    }

    public C30401hz(Context context) {
        this.A04 = context;
    }
}
