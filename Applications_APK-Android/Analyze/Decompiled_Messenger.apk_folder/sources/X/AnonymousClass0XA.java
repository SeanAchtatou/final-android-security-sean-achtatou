package X;

import com.facebook.acra.CrashReportData;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.acra.constants.ReportField;
import com.facebook.acra.info.ExternalProcessInfo;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.io.Writer;

/* renamed from: X.0XA  reason: invalid class name */
public final class AnonymousClass0XA implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.errorreporting.FbErrorReporterImpl$1";
    public final /* synthetic */ ExternalProcessInfo A00;
    public final /* synthetic */ AnonymousClass06D A01;
    public final /* synthetic */ Integer A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ Throwable A05;

    public AnonymousClass0XA(AnonymousClass06D r1, String str, String str2, Integer num, ExternalProcessInfo externalProcessInfo, Throwable th) {
        this.A01 = r1;
        this.A04 = str;
        this.A03 = str2;
        this.A02 = num;
        this.A00 = externalProcessInfo;
        this.A05 = th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.acra.CrashReportData.merge(java.util.Map, boolean, java.io.Writer):void
     arg types: [java.util.Map, int, ?[OBJECT, ARRAY]]
     candidates:
      ClspMth{java.util.HashMap.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      com.facebook.acra.CrashReportData.merge(java.util.Map, boolean, java.io.Writer):void */
    public void run() {
        try {
            CrashReportData crashReportData = new CrashReportData();
            crashReportData.put(ErrorReportingConstants.SOFT_ERROR_CATEGORY, this.A04);
            crashReportData.put(TurboLoader.Locator.$const$string(87), this.A03);
            crashReportData.put(ErrorReportingConstants.SOFT_ERROR_OCCURRENCE_COUNT, Integer.toString(this.A02.intValue()));
            ErrorReporter errorReporter = (ErrorReporter) this.A01.A05.get();
            if (this.A00 != null) {
                String eventsLog = errorReporter.getEventsLog();
                if (eventsLog != null) {
                    crashReportData.put(ReportField.EVENTSLOG, eventsLog);
                }
                crashReportData.put(ReportField.LOGCAT, errorReporter.getLogcatOutputIfPidFound(true, null));
                crashReportData.merge(this.A00.getAcraFields(), true, (Writer) null);
            }
            errorReporter.handleException(this.A05, crashReportData);
        } catch (Throwable th) {
            if (!this.A01.A06) {
                return;
            }
            if (th instanceof Error) {
                throw ((Error) th);
            } else if (th instanceof RuntimeException) {
                throw th;
            } else {
                throw new RuntimeException(th);
            }
        }
    }
}
