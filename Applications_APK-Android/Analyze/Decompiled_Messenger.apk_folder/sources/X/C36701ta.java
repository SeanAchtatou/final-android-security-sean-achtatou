package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1ta  reason: invalid class name and case insensitive filesystem */
public final class C36701ta extends C36711tb {
    public String A00 = BuildConfig.FLAVOR;

    public C36701ta() {
    }

    public C36701ta(long j, String str) {
        this.A07 = j;
        this.A00 = str;
    }

    public C36701ta(String str) {
        this.A00 = str;
    }
}
