package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

@UserScoped
/* renamed from: X.113  reason: invalid class name */
public final class AnonymousClass113 implements CallerContextable {
    private static C05540Zi A02 = null;
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.blocking.MontageHiddenUserHelper";
    public AnonymousClass0UN A00;
    public final AnonymousClass128 A01;

    public static final AnonymousClass113 A00(AnonymousClass1XY r4) {
        AnonymousClass113 r0;
        synchronized (AnonymousClass113.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass113((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass113) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public void A01(UserKey userKey) {
        ((C189216c) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B7s, this.A00)).A0N(C99084oO.$const$string(AnonymousClass1Y3.A2a));
        User A09 = ((AnonymousClass0XN) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AXA, this.A00)).A09();
        if (A09 != null) {
            ThreadKey A04 = ThreadKey.A04(Long.parseLong(userKey.id), Long.parseLong(A09.A0j));
            if (((C26681bq) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AuB, this.A00)).A08(A04) != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(A04);
                ((C189216c) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B7s, this.A00)).A0P(null, arrayList, getClass().getName());
            }
        }
    }

    public void A02(UserKey userKey) {
        this.A01.A02(userKey.id);
        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aoc, this.A00), new AnonymousClass7E1(this, userKey), -1276376418);
        A01(userKey);
    }

    private AnonymousClass113(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(7, r3);
        this.A01 = AnonymousClass127.A00(r3);
    }

    public boolean A03(long j) {
        return this.A01.A04(UserKey.A01(Long.toString(j)).id);
    }
}
