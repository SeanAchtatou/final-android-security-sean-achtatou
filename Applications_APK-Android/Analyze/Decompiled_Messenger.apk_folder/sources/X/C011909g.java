package X;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.09g  reason: invalid class name and case insensitive filesystem */
public final class C011909g implements ThreadFactory {
    private final String A00;
    private final ThreadGroup A01;
    private final AtomicInteger A02 = new AtomicInteger(1);

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(this.A01, runnable, AnonymousClass08S.A09(this.A00, this.A02.getAndIncrement()), 0);
        if (thread.isDaemon()) {
            thread.setDaemon(false);
        }
        if (thread.getPriority() != 5) {
            thread.setPriority(5);
        }
        return thread;
    }

    public C011909g(String str) {
        ThreadGroup threadGroup;
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            threadGroup = securityManager.getThreadGroup();
        } else {
            threadGroup = Thread.currentThread().getThreadGroup();
        }
        this.A01 = threadGroup;
        this.A00 = AnonymousClass08S.A0J(str, "-");
    }
}
