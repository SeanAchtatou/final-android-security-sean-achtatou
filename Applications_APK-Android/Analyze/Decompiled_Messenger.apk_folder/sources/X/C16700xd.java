package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0xd  reason: invalid class name and case insensitive filesystem */
public final class C16700xd extends AnonymousClass0UV {
    private static C05540Zi A00;

    public static final C33701o0 A00(AnonymousClass1XY r3) {
        C33701o0 r0;
        synchronized (C33701o0.class) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r3)) {
                    A00.A00 = C33711o1.A02((AnonymousClass1XY) A00.A01()).A01;
                }
                C05540Zi r1 = A00;
                r0 = (C33701o0) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C16660xW A01(AnonymousClass1XY r3) {
        return new C16660xW(new C16710xe(r3), AnonymousClass07B.A0C);
    }

    public static final C16660xW A02(AnonymousClass1XY r3) {
        return new C16660xW(new C16710xe(r3), AnonymousClass07B.A01);
    }

    public static final C16660xW A03(AnonymousClass1XY r3) {
        return new C16660xW(new C16710xe(r3), AnonymousClass07B.A0i);
    }

    public static final C16660xW A04(AnonymousClass1XY r3) {
        return new C16660xW(new C16710xe(r3), AnonymousClass07B.A00);
    }

    public static final Boolean A05(AnonymousClass1XY r2) {
        boolean z = false;
        if (AnonymousClass0UU.A02(r2).A02 == C001500z.A07) {
            z = true;
        }
        return Boolean.valueOf(z);
    }
}
