package X;

/* renamed from: X.1z3  reason: invalid class name and case insensitive filesystem */
public final class C39441z3 implements Runnable {
    public static final String __redex_internal_original_name = "com.google.android.play.core.tasks.b";
    private final /* synthetic */ C51412ga A00;
    private final /* synthetic */ AnonymousClass90S A01;

    public C39441z3(AnonymousClass90S r1, C51412ga r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public final void run() {
        synchronized (this.A01.A01) {
            AnonymousClass90Q r1 = this.A01.A00;
            if (r1 != null) {
                r1.BT0(this.A00);
            }
        }
    }
}
