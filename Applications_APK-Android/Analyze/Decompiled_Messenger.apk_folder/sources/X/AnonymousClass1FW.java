package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1FW  reason: invalid class name */
public final class AnonymousClass1FW extends AnonymousClass0UV {
    public static final Boolean A00(AnonymousClass1XY r4) {
        C001500z A05 = AnonymousClass0UU.A05(r4);
        Boolean valueOf = Boolean.valueOf(AnonymousClass0WA.A00(r4).AbO(AnonymousClass1Y3.A69, false));
        if (A05 != C001500z.A07) {
            return false;
        }
        return valueOf;
    }
}
