package X;

import com.mapbox.mapboxsdk.geometry.LatLng;

/* renamed from: X.1zN  reason: invalid class name and case insensitive filesystem */
public final class C39641zN implements C28385Du1 {
    public final /* synthetic */ C39731zW A00;

    public C39641zN(C39731zW r1) {
        this.A00 = r1;
    }

    public boolean BeA(LatLng latLng) {
        C28342DtC dtC = this.A00.A00.A01;
        if (dtC == null) {
            return false;
        }
        dtC.C5B("gesture_single_tap");
        return false;
    }
}
