package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0pk  reason: invalid class name and case insensitive filesystem */
public final class C12650pk extends AnonymousClass0Wl {
    private static volatile C12650pk A01;
    private final C26221b6 A00;

    public String getSimpleName() {
        return "LanguageSwitcherCommonExInit";
    }

    public static final C12650pk A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C12650pk.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C12650pk(C26221b6.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C12650pk(C26221b6 r1) {
        this.A00 = r1;
    }

    public void init() {
        int A03 = C000700l.A03(-450836847);
        this.A00.A04();
        C000700l.A09(-776640521, A03);
    }
}
