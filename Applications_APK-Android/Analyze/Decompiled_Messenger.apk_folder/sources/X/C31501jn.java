package X;

import android.view.View;

/* renamed from: X.1jn  reason: invalid class name and case insensitive filesystem */
public final class C31501jn implements AnonymousClass119 {
    public static int A00(float f, Integer num) {
        int i;
        int i2;
        String str;
        int intValue = num.intValue();
        switch (intValue) {
            case 0:
                return View.MeasureSpec.makeMeasureSpec(0, 0);
            case 1:
                i = AnonymousClass1K3.A00(f);
                i2 = 1073741824;
                break;
            case 2:
                i = AnonymousClass1K3.A00(f);
                i2 = Integer.MIN_VALUE;
                break;
            default:
                if (num != null) {
                    switch (intValue) {
                        case 1:
                            str = "EXACTLY";
                            break;
                        case 2:
                            str = "AT_MOST";
                            break;
                        default:
                            str = "UNDEFINED";
                            break;
                    }
                } else {
                    str = "null";
                }
                throw new IllegalArgumentException(AnonymousClass08S.A0J("Unexpected YogaMeasureMode: ", str));
        }
        return View.MeasureSpec.makeMeasureSpec(i, i2);
    }

    public long BKx(AnonymousClass10W r13, float f, Integer num, float f2, Integer num2) {
        C22571Lz r5;
        int width;
        int height;
        C17470yx r8 = (C17470yx) r13.getData();
        C17770zR B5K = r8.B5K();
        AnonymousClass0p4 r7 = B5K.A03;
        if (r7 != null && r7.A0J()) {
            return 0;
        }
        C17770zR r2 = null;
        if (r8.AOl()) {
            r5 = r8.AkV();
        } else {
            r5 = null;
        }
        boolean A02 = C27041cY.A02();
        int A00 = A00(f, num);
        int A002 = A00(f2, num2);
        if (A02) {
            C09040gQ APo = C27041cY.A00.APo(AnonymousClass08S.A0J("measure:", B5K.A1A()));
            APo.AOo("widthSpec", View.MeasureSpec.toString(A00));
            APo.AOo("heightSpec", View.MeasureSpec.toString(A002));
            APo.AOn("componentId", B5K.A00);
            APo.flush();
        }
        r8.C8r(A00);
        r8.C8l(A002);
        AnonymousClass0p4 AiS = r8.AiS();
        if (C17770zR.A0B(AiS, B5K) || r8.BBY()) {
            C17770zR AoX = r8.AoX();
            if (B5K != AoX) {
                r2 = AoX;
            } else if (r8.AxD() != null) {
                r2 = r8.AxD().B5K();
            }
            if (r2 != null) {
                AiS = r2.A03;
            }
            C17470yx A03 = C31951kr.A03(AiS, r8, A00, A002);
            width = A03.getWidth();
            height = A03.getHeight();
        } else if (r5 == null || r5.AsD() != A00 || r5.Ary() != A002 || B5K.A11()) {
            AnonymousClass10L r11 = new AnonymousClass10L(Integer.MIN_VALUE, Integer.MIN_VALUE);
            B5K.A0f(r7, r8, A00, A002, r11);
            width = r11.A01;
            if (width < 0 || (height = r11.A00) < 0) {
                throw new IllegalStateException("MeasureOutput not set, ComponentLifecycle is: " + B5K);
            } else if (r8.AkV() != null) {
                r8.AkV().C8r(A00);
                r8.AkV().C8l(A002);
                r8.AkV().C8n((float) width);
                r8.AkV().C8m((float) height);
            }
        } else {
            width = (int) r5.As2();
            height = (int) r5.As1();
        }
        r8.C8n((float) width);
        r8.C8m((float) height);
        if (A02) {
            C27041cY.A00();
        }
        return AnonymousClass1L5.A00(width, height);
    }
}
