package X;

import com.fasterxml.jackson.databind.JsonNode;
import io.card.payment.BuildConfig;

/* renamed from: X.1gl  reason: invalid class name and case insensitive filesystem */
public final class C29651gl extends C14610tg {
    public static final C29651gl instance = new C29651gl();

    public String asText() {
        return BuildConfig.FLAVOR;
    }

    public JsonNode deepCopy() {
        return this;
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public String toString() {
        return BuildConfig.FLAVOR;
    }

    public C182811d asToken() {
        return C182811d.NOT_AVAILABLE;
    }

    public C11980oL getNodeType() {
        return C11980oL.MISSING;
    }

    public final void serialize(C11710np r1, C11260mU r2) {
        r1.writeNull();
    }

    private C29651gl() {
    }

    public void serializeWithType(C11710np r1, C11260mU r2, CY1 cy1) {
        r1.writeNull();
    }
}
