package X;

/* renamed from: X.1fV  reason: invalid class name and case insensitive filesystem */
public abstract class C28871fV extends C28141eK {
    public C28871fV(C008807t r2, AnonymousClass04e r3, C28151eL r4) {
        super(r2, r3, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0088  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0J(android.content.Context r8, android.content.pm.ComponentInfo r9) {
        /*
            r7 = this;
            boolean r0 = r7 instanceof X.C28881fW
            if (r0 != 0) goto L_0x003c
            r6 = r7
            X.1fU r6 = (X.C28861fU) r6
            android.content.pm.ApplicationInfo r5 = r8.getApplicationInfo()
            android.content.pm.ApplicationInfo r3 = r9.applicationInfo
            java.lang.String r4 = "ExternalIntentScope"
            if (r5 == 0) goto L_0x001e
            if (r3 == 0) goto L_0x001e
            int r1 = r5.uid     // Catch:{ SecurityException -> 0x0071 }
            int r0 = r3.uid     // Catch:{ SecurityException -> 0x0071 }
            boolean r0 = X.C28061eC.A04(r8, r1, r0)     // Catch:{ SecurityException -> 0x0071 }
            r0 = r0 ^ 1
            return r0
        L_0x001e:
            X.04e r2 = r6.A00
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Null app info, current app: "
            r1.<init>(r0)
            r1.append(r5)
            java.lang.String r0 = ", target app: "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0 = 0
            r2.C2T(r4, r1, r0)
            r0 = 0
            return r0
        L_0x003c:
            r5 = r7
            X.1fW r5 = (X.C28881fW) r5
            android.content.pm.ApplicationInfo r0 = r9.applicationInfo
            java.lang.String r4 = "ThirdPartyIntentScope"
            if (r0 != 0) goto L_0x004f
            X.04e r2 = r5.A00
            r1 = 0
            java.lang.String r0 = "Null application info."
            r2.C2T(r4, r0, r1)
            r0 = 0
            return r0
        L_0x004f:
            X.0TW r1 = r5.A00     // Catch:{ SecurityException -> 0x005e }
            int r0 = r0.uid     // Catch:{ SecurityException -> 0x005e }
            X.0bo r0 = X.AnonymousClass0TW.A00(r0, r8)     // Catch:{ SecurityException -> 0x005e }
            boolean r0 = X.AnonymousClass0TW.A04(r1, r0, r8)     // Catch:{ SecurityException -> 0x005e }
            r0 = r0 ^ 1
            return r0
        L_0x005e:
            r3 = move-exception
            X.04e r2 = r5.A00
            java.lang.String r1 = "Unexpected exception in checking trusted app for "
            java.lang.String r0 = r9.packageName
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.C2T(r4, r0, r3)
            java.lang.Integer r2 = X.C28141eK.A04(r5)
            goto L_0x0083
        L_0x0071:
            r3 = move-exception
            X.04e r2 = r6.A00
            java.lang.String r1 = "Unexpected exception in verifying signature for: "
            java.lang.String r0 = r9.packageName
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.C2T(r4, r0, r3)
            java.lang.Integer r2 = X.C28141eK.A04(r6)
        L_0x0083:
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            r0 = 0
            if (r2 != r1) goto L_0x0089
            r0 = 1
        L_0x0089:
            r0 = r0 ^ 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28871fV.A0J(android.content.Context, android.content.pm.ComponentInfo):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004b, code lost:
        if (r1 == false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005d, code lost:
        if (X.C28141eK.A04(r8) == X.AnonymousClass07B.A01) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.Intent A00(X.C28871fV r8, android.content.Intent r9, android.content.Context r10, java.util.List r11) {
        /*
            java.util.ArrayList r2 = new java.util.ArrayList
            int r0 = r11.size()
            r2.<init>(r0)
            java.util.Iterator r7 = r11.iterator()
        L_0x000d:
            boolean r0 = r7.hasNext()
            java.lang.String r4 = "DifferentKeyIntentScope"
            r3 = 0
            if (r0 == 0) goto L_0x006f
            java.lang.Object r6 = r7.next()
            android.content.pm.ComponentInfo r6 = (android.content.pm.ComponentInfo) r6
            boolean r0 = r8.A0J(r10, r6)
            if (r0 != 0) goto L_0x003e
            boolean r0 = r8.A0H()
            if (r0 == 0) goto L_0x003b
            r2.add(r6)
            java.lang.String r1 = "Non-external/third-party component detected, but allowing because of fail-open: "
        L_0x002d:
            java.lang.String r0 = X.C28141eK.A05(r9)
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            X.04e r0 = r8.A00
            r0.C2T(r4, r1, r3)
            goto L_0x000d
        L_0x003b:
            java.lang.String r1 = "Removed non-external/third-party component: "
            goto L_0x002d
        L_0x003e:
            android.content.pm.ApplicationInfo r0 = r6.applicationInfo
            if (r0 == 0) goto L_0x004d
            java.lang.String r1 = r0.className
            java.lang.String r0 = "com.android.internal.app.ResolverActivity"
            boolean r1 = r0.equals(r1)
            r0 = 1
            if (r1 != 0) goto L_0x004e
        L_0x004d:
            r0 = 0
        L_0x004e:
            if (r0 == 0) goto L_0x006b
            boolean r0 = r8.A0H()
            if (r0 != 0) goto L_0x005f
            java.lang.Integer r5 = X.C28141eK.A04(r8)
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            r0 = 0
            if (r5 != r1) goto L_0x0060
        L_0x005f:
            r0 = 1
        L_0x0060:
            if (r0 == 0) goto L_0x0068
            r2.add(r6)
            java.lang.String r1 = "Found potentially dangerous resolver but not removing: "
            goto L_0x002d
        L_0x0068:
            java.lang.String r1 = "Removed potentially dangerous resolver: "
            goto L_0x002d
        L_0x006b:
            r2.add(r6)
            goto L_0x000d
        L_0x006f:
            boolean r0 = r2.isEmpty()
            if (r0 == 0) goto L_0x0085
            X.04e r2 = r8.A00
            java.lang.String r1 = "No matching different-signature components for: "
            java.lang.String r0 = X.C28141eK.A05(r9)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.C2T(r4, r0, r3)
            return r3
        L_0x0085:
            int r1 = r2.size()
            int r0 = r11.size()
            if (r1 == r0) goto L_0x009e
            int r1 = r2.size()
            r0 = 1
            if (r1 <= r0) goto L_0x00a8
            java.util.List r0 = X.C28141eK.A08(r2, r9)
            android.content.Intent r9 = X.C28141eK.A03(r0)
        L_0x009e:
            X.04e r1 = r8.A00
            boolean r0 = r8.A0H()
            X.C90684Up.A00(r9, r1, r0)
            return r9
        L_0x00a8:
            r0 = 0
            java.lang.Object r0 = r2.get(r0)
            android.content.pm.ComponentInfo r0 = (android.content.pm.ComponentInfo) r0
            android.content.ComponentName r2 = new android.content.ComponentName
            java.lang.String r1 = r0.packageName
            java.lang.String r0 = r0.name
            r2.<init>(r1, r0)
            r9.setComponent(r2)
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28871fV.A00(X.1fV, android.content.Intent, android.content.Context, java.util.List):android.content.Intent");
    }
}
