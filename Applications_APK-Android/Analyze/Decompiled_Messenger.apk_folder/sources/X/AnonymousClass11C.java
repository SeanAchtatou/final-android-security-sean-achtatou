package X;

import android.view.accessibility.AccessibilityManager;

/* renamed from: X.11C  reason: invalid class name */
public final class AnonymousClass11C implements AccessibilityManager.AccessibilityStateChangeListener {
    public C17590z9 A00;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass11C)) {
            return false;
        }
        return this.A00.equals(((AnonymousClass11C) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public void onAccessibilityStateChanged(boolean z) {
        this.A00.onAccessibilityStateChanged(z);
    }

    public AnonymousClass11C(C17590z9 r1) {
        this.A00 = r1;
    }
}
