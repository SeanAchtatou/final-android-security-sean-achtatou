package X;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0iB  reason: invalid class name and case insensitive filesystem */
public final class C09610iB implements AnonymousClass1YQ, AnonymousClass0jI {
    private static volatile C09610iB A0K;
    public long A00;
    public String A01;
    public BlockingQueue A02;
    public boolean A03;
    private boolean A04;
    private boolean A05;
    public final long A06;
    public final long A07;
    public final long A08;
    public final AnonymousClass1ZE A09;
    public final AnonymousClass0iZ A0A;
    public final List A0B = new ArrayList();
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    private final C09730iO A0F;
    private final C25051Yd A0G;
    private final Random A0H;
    private final Executor A0I;
    private final boolean A0J;

    public String getSimpleName() {
        return "LooperProfiler";
    }

    public static final C09610iB A00(AnonymousClass1XY r4) {
        if (A0K == null) {
            synchronized (C09610iB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0K, r4);
                if (A002 != null) {
                    try {
                        A0K = new C09610iB(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0K;
    }

    public void Bew(String str) {
        this.A02.offer(str);
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass07A.A04(this.A0I, new C189948sK(this), 1005826941);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0051, code lost:
        if (r6.A0H.nextInt(100) < r3) goto L_0x0053;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C09610iB(X.AnonymousClass1XY r7) {
        /*
            r6 = this;
            r6.<init>()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r6.A0B = r0
            r5 = 0
            r6.A05 = r5
            r6.A04 = r5
            X.1Yd r0 = X.AnonymousClass0WT.A00(r7)
            r6.A0G = r0
            X.0iO r0 = X.AnonymousClass0iN.A00(r7)
            r6.A0F = r0
            X.0iZ r0 = X.AnonymousClass0iZ.A00(r7)
            r6.A0A = r0
            X.1ZE r0 = X.AnonymousClass1ZD.A00(r7)
            r6.A09 = r0
            java.util.Random r0 = X.AnonymousClass0W9.A01()
            r6.A0H = r0
            X.1Yd r2 = r6.A0G
            r0 = 563650033877521(0x200a3000c0211, double:2.78480118016138E-309)
            long r0 = r2.At0(r0)
            int r3 = (int) r0
            X.1Yd r2 = r6.A0G
            r0 = 282175056381094(0x100a3000004a6, double:1.394130014712154E-309)
            boolean r2 = r2.Aem(r0)
            r4 = 1
            r0 = -1
            if (r3 == r0) goto L_0x0053
            java.util.Random r1 = r6.A0H
            r0 = 100
            int r1 = r1.nextInt(r0)
            r0 = 0
            if (r1 >= r3) goto L_0x0054
        L_0x0053:
            r0 = 1
        L_0x0054:
            if (r2 == 0) goto L_0x0059
            if (r0 == 0) goto L_0x0059
            r5 = 1
        L_0x0059:
            r6.A0C = r5
            X.1Yd r2 = r6.A0G
            r0 = 282175057101994(0x100a3000b04aa, double:1.394130018273873E-309)
            boolean r0 = r2.Aem(r0)
            r6.A0D = r0
            X.1Yd r2 = r6.A0G
            r0 = 563650033222154(0x200a30002020a, double:2.784801176923436E-309)
            long r0 = r2.At0(r0)
            r6.A07 = r0
            X.1Yd r2 = r6.A0G
            r0 = 563650033615376(0x200a300080210, double:2.78480117886621E-309)
            r2.At0(r0)
            X.1Yd r2 = r6.A0G
            r0 = 563650033287691(0x200a30003020b, double:2.78480117724723E-309)
            long r0 = r2.At0(r0)
            r6.A06 = r0
            X.1Yd r2 = r6.A0G
            r0 = 2306125184270664872(0x200100a3000904a8, double:1.5851292837685447E-154)
            boolean r0 = r2.Aem(r0)
            r6.A0E = r0
            X.1Yd r2 = r6.A0G
            r0 = 2306125184270730409(0x200100a3000a04a9, double:1.5851292837902516E-154)
            boolean r0 = r2.Aem(r0)
            r6.A0J = r0
            X.1Yd r2 = r6.A0G
            r0 = 282175056446631(0x100a3000104a7, double:1.39413001503595E-309)
            boolean r0 = r2.Aem(r0)
            r6.A03 = r0
            X.1Yd r2 = r6.A0G
            r0 = 563650033418765(0x200a30005020d, double:2.784801177894823E-309)
            long r0 = r2.At0(r0)
            r6.A08 = r0
            X.1Yd r2 = r6.A0G
            r0 = 563650033549839(0x200a30007020f, double:2.784801178542415E-309)
            long r2 = r2.At0(r0)
            int r1 = (int) r2
            if (r1 >= r4) goto L_0x00e3
            java.util.concurrent.LinkedBlockingQueue r0 = new java.util.concurrent.LinkedBlockingQueue
            r0.<init>()
            r6.A02 = r0
        L_0x00d5:
            java.lang.String r1 = "LooperProfiler"
            X.09g r0 = new X.09g
            r0.<init>(r1)
            java.util.concurrent.ExecutorService r0 = java.util.concurrent.Executors.newSingleThreadExecutor(r0)
            r6.A0I = r0
            return
        L_0x00e3:
            java.util.concurrent.ArrayBlockingQueue r0 = new java.util.concurrent.ArrayBlockingQueue
            r0.<init>(r1)
            r6.A02 = r0
            goto L_0x00d5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09610iB.<init>(X.1XY):void");
    }

    public void init() {
        int A032 = C000700l.A03(-474885101);
        if (!this.A0C || this.A04) {
            C000700l.A09(1916390142, A032);
            return;
        }
        this.A04 = true;
        this.A0F.A01(this);
        if (this.A0J) {
            this.A0B.add(new C189958sL(this));
        }
        C000700l.A09(-139642575, A032);
    }
}
