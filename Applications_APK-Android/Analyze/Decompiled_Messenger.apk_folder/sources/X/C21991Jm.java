package X;

/* renamed from: X.1Jm  reason: invalid class name and case insensitive filesystem */
public class C21991Jm extends C26701bs {
    private static final long serialVersionUID = 416067702302823522L;
    public final C10030jR _keyType;
    public final C10030jR _valueType;

    public int containedTypeCount() {
        return 2;
    }

    public String containedTypeName(int i) {
        if (i == 0) {
            return "K";
        }
        if (i == 1) {
            return "V";
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj == null || obj.getClass() != getClass()) {
                return false;
            }
            C21991Jm r5 = (C21991Jm) obj;
            if (this._class != r5._class || !this._keyType.equals(r5._keyType) || !this._valueType.equals(r5._valueType)) {
                return false;
            }
        }
        return true;
    }

    public boolean isContainerType() {
        return true;
    }

    public boolean isMapLikeType() {
        return true;
    }

    public C10030jR _narrow(Class cls) {
        return new C21991Jm(cls, this._keyType, this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    public String buildCanonicalName() {
        StringBuilder sb = new StringBuilder();
        sb.append(this._class.getName());
        C10030jR r1 = this._keyType;
        if (r1 != null) {
            sb.append('<');
            sb.append(r1.toCanonical());
            sb.append(',');
            sb.append(this._valueType.toCanonical());
            sb.append('>');
        }
        return sb.toString();
    }

    public C10030jR containedType(int i) {
        if (i == 0) {
            return this._keyType;
        }
        if (i == 1) {
            return this._valueType;
        }
        return null;
    }

    public C10030jR getContentType() {
        return this._valueType;
    }

    public C10030jR getKeyType() {
        return this._keyType;
    }

    public C10030jR narrowContentsBy(Class cls) {
        C10030jR r1 = this._valueType;
        if (cls == r1._class) {
            return this;
        }
        return new C21991Jm(this._class, this._keyType, r1.narrowBy(cls), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public C10030jR narrowKey(Class cls) {
        C10030jR r1 = this._keyType;
        if (cls == r1._class) {
            return this;
        }
        return new C21991Jm(this._class, r1.narrowBy(cls), this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    public String toString() {
        return "[map-like type; class " + this._class.getName() + ", " + this._keyType + " -> " + this._valueType + "]";
    }

    public C10030jR widenContentsBy(Class cls) {
        C10030jR r1 = this._valueType;
        if (cls == r1._class) {
            return this;
        }
        return new C21991Jm(this._class, this._keyType, r1.widenBy(cls), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public C21991Jm withKeyValueHandler(Object obj) {
        return new C21991Jm(this._class, this._keyType.withValueHandler(obj), this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    public C21991Jm(Class cls, C10030jR r8, C10030jR r9, Object obj, Object obj2, boolean z) {
        super(cls, r8.hashCode() ^ r9.hashCode(), obj, obj2, z);
        this._keyType = r8;
        this._valueType = r9;
    }

    public C21991Jm withContentTypeHandler(Object obj) {
        return new C21991Jm(this._class, this._keyType, this._valueType.withTypeHandler(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public C21991Jm withContentValueHandler(Object obj) {
        return new C21991Jm(this._class, this._keyType, this._valueType.withValueHandler(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public C21991Jm withStaticTyping() {
        if (this._asStatic) {
            return this;
        }
        return new C21991Jm(this._class, this._keyType, this._valueType.withStaticTyping(), this._valueHandler, this._typeHandler, true);
    }

    public C21991Jm withTypeHandler(Object obj) {
        return new C21991Jm(this._class, this._keyType, this._valueType, this._valueHandler, obj, this._asStatic);
    }

    public C21991Jm withValueHandler(Object obj) {
        return new C21991Jm(this._class, this._keyType, this._valueType, obj, this._typeHandler, this._asStatic);
    }
}
