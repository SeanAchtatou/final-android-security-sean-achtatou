package X;

import android.content.Context;
import android.os.Build;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;

/* renamed from: X.0Pb  reason: invalid class name and case insensitive filesystem */
public final class C03690Pb extends PreferenceCategory {
    public void onAttachedToHierarchy(PreferenceManager preferenceManager) {
        super.onAttachedToHierarchy(preferenceManager);
        setTitle("Profilo");
        C73593gN r2 = new C73593gN(getContext());
        r2.A03(C005706b.A01);
        r2.setTitle("Enable manual tracing");
        if (Build.VERSION.SDK_INT >= 14) {
            r2.setSummaryOff("Tap to enable manual controls (see notification)");
            r2.setSummaryOn("Tap to disable manual controls");
        }
        r2.setDefaultValue(false);
        addPreference(r2);
    }

    public C03690Pb(Context context) {
        super(context);
    }
}
