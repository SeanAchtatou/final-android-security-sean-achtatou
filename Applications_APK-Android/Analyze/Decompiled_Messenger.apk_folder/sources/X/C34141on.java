package X;

import android.content.Context;
import com.facebook.compactdisk.common.PrivacyGuard;
import com.facebook.compactdisk.current.DiskCacheConfig;
import com.facebook.stash.core.Stash;
import java.io.File;

/* renamed from: X.1on  reason: invalid class name and case insensitive filesystem */
public final class C34141on {
    public AnonymousClass0UN A00;
    private Stash A01;

    public static final C34141on A00(AnonymousClass1XY r1) {
        return new C34141on(r1);
    }

    public static String A01(AnonymousClass15S r5) {
        C31071j6 r4 = new C31071j6("graph_store_cache");
        r4.A00 = 3;
        AnonymousClass13z r2 = AnonymousClass13z.A01;
        r4.A00(r2);
        C31071j6 r1 = new C31071j6("graph_store_cache");
        r1.A00 = 1;
        r1.A00(r2);
        r4.A01 = r5.A03(r1);
        return r5.A02(r4).getAbsolutePath();
    }

    public static void A02(String str) {
        File file = new File(str);
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    for (File delete : file2.listFiles()) {
                        delete.delete();
                    }
                }
                file2.delete();
            }
        }
        file.delete();
    }

    public DiskCacheConfig A03(Context context, C30541iE r7) {
        DiskCacheConfig.Builder maxSize = new DiskCacheConfig.Builder().setVersionID("0").setName("graph_service_cache").setParentDirectory(context.getApplicationContext().getCacheDir().getPath()).setStoreInCacheDirectory(false).setScope(r7.A00()).setStaleAge((long) 604800).setKeyStatsSamplingRate(AnonymousClass299.A03).setMaxSize((long) 10485760);
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AcD, this.A00)).AbO(643, false)) {
            maxSize.setEvents(new AnonymousClass2SB(this), 72);
        }
        return maxSize.build();
    }

    public Stash A04() {
        if (!((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C34241p8) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A4o, this.A00)).A00)).Aem(285142879114640L)) {
            return null;
        }
        if (this.A01 == null) {
            File file = new File(C35651rZ.A00((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.A6S, this.A00), false, "graph_service_cache", "0", ((PrivacyGuard) AnonymousClass1XX.A03(AnonymousClass1Y3.Ajd, this.A00)).getUUID()));
            file.mkdirs();
            C30941it r5 = new C30941it();
            r5.A03 = "graph_service_cache";
            r5.A00 = C30961iv.A00((long) 10485760);
            r5.A01 = new AnonymousClass0ZZ(((long) 7) * 86400);
            r5.A02 = AnonymousClass13z.A01;
            this.A01 = ((C30931is) AnonymousClass1XX.A03(AnonymousClass1Y3.Auo, this.A00)).A01(file, r5.A00());
        }
        return this.A01;
    }

    public C34141on(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
