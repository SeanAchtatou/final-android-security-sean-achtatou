package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0fh  reason: invalid class name and case insensitive filesystem */
public final class C08640fh extends C08650fi {
    private int A00 = 0;
    public final C08670fk A01;
    public final Map A02 = Collections.synchronizedMap(new HashMap());
    public final Map A03 = Collections.synchronizedMap(new HashMap());
    public final Map A04 = Collections.synchronizedMap(new HashMap());
    public final boolean A05;

    public static void A00(C08640fh r1, int i) {
        if (r1.A00 != i) {
            r1.A04.clear();
            r1.A03.clear();
            r1.A02.clear();
            r1.A00 = i;
        }
    }

    public C08640fh(C08670fk r2, boolean z) {
        this.A01 = r2;
        this.A05 = z;
    }
}
