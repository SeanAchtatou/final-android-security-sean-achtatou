package X;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.1mp  reason: invalid class name and case insensitive filesystem */
public interface C33091mp {
    int ArU();

    void BOc(RecyclerView recyclerView);

    void BPy(C33781o8 r1, int i);

    C33781o8 BV1(ViewGroup viewGroup, int i);

    void BW8(RecyclerView recyclerView);

    void C0O(C15730vo r1);

    void CJm(C15730vo r1);

    long getItemId(int i);

    int getItemViewType(int i);

    boolean hasStableIds();
}
