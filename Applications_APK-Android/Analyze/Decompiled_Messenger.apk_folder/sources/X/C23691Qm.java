package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1Qm  reason: invalid class name and case insensitive filesystem */
public final class C23691Qm extends Enum {
    public static final C23691Qm A00;
    public static final C23691Qm A01 = new C23691Qm("GAMES", 2, C34891qL.A0d, C29631gj.A13);
    public static final C23691Qm A02;
    public static final C23691Qm A03;
    public static final C23691Qm A04;
    public static final C23691Qm A05;
    public final C34891qL mM4ButtonGlyph;
    public final C29631gj mM4IconGlyph;

    static {
        C34891qL r6 = C34891qL.A0x;
        C29631gj r5 = C29631gj.A1a;
        A00 = new C23691Qm("CALL", 0, r6, r5);
        A04 = new C23691Qm("MISSED_CALL", 1, r6, r5);
        C34891qL r4 = C34891qL.A0A;
        C29631gj r3 = C29631gj.A0F;
        A05 = new C23691Qm("VIDEO", 3, r4, r3);
        A03 = new C23691Qm("GROUP_JOIN_VIDEO", 4, r4, r3);
        A02 = new C23691Qm("GROUP_JOIN_AUDIO", 5, r6, r5);
        new C23691Qm("JOIN_COWATCH", 6, C34891qL.A0X, C29631gj.A0r);
    }

    private C23691Qm(String str, int i, C34891qL r3, C29631gj r4) {
        this.mM4ButtonGlyph = r3;
        this.mM4IconGlyph = r4;
    }
}
