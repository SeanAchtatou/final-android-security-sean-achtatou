package X;

import com.facebook.stash.core.Stash;

/* renamed from: X.1KG  reason: invalid class name */
public final class AnonymousClass1KG implements C30981ix {
    public final /* synthetic */ AnonymousClass1OL A00;

    public void AP6(Stash stash) {
    }

    public AnonymousClass1KG(AnonymousClass1OL r1) {
        this.A00 = r1;
    }

    public void Bla(Object obj, int i) {
        Integer num;
        String str = (String) obj;
        if (i == 0) {
            num = AnonymousClass07B.A0C;
        } else if (i == 2) {
            num = AnonymousClass07B.A01;
        } else if (i == 3) {
            num = AnonymousClass07B.A00;
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown remove reason: ", i));
        }
        C33211nD A002 = C33211nD.A00();
        A002.A02 = str;
        A002.A01 = num;
        this.A00.BY8(A002);
    }

    public void BaB(Object obj, Object obj2) {
    }

    public void BbN(Object obj, Object obj2) {
    }
}
