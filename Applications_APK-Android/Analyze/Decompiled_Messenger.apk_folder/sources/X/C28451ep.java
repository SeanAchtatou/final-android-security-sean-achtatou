package X;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ep  reason: invalid class name and case insensitive filesystem */
public final class C28451ep {
    private static final Class A06 = C28451ep.class;
    private static volatile C28451ep A07;
    public AnonymousClass0UN A00;
    public Class A01;
    public Object A02;
    public Future A03;
    public final AnonymousClass1Y6 A04;
    public final ScheduledExecutorService A05;

    public static final C28451ep A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C28451ep.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C28451ep(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static void A01(C28451ep r4) {
        boolean z = true;
        boolean z2 = false;
        if (r4.A01 == null) {
            z2 = true;
        }
        boolean z3 = false;
        if (r4.A02 == null) {
            z3 = true;
        }
        if (z2 != z3) {
            z = false;
        }
        Preconditions.checkState(z);
    }

    public void A02(Class cls) {
        this.A04.AOz();
        A01(this);
        Future future = this.A03;
        if (future != null) {
            future.cancel(false);
            this.A03 = null;
        }
        Object obj = this.A02;
        if (obj != null) {
            ((C28461eq) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aq4, this.A00)).A06(obj);
            this.A02 = null;
            if (!Objects.equal(this.A01, cls)) {
                C010708t.A0C(A06, "stopTrackingActivityLaunch called for %s while tracking %s.", cls, this.A01);
            }
            this.A01 = null;
        }
    }

    private C28451ep(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A04 = AnonymousClass0UX.A07(r3);
        this.A05 = AnonymousClass0UX.A0e(r3);
    }
}
