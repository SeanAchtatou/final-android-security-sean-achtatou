package X;

/* renamed from: X.0lB  reason: invalid class name and case insensitive filesystem */
public final class C10980lB {
    public static final AnonymousClass1Y7 A00 = ((AnonymousClass1Y7) C04350Ue.A05.A09("orca_accounts/"));
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y8 A08 = C04350Ue.A0A.A09("orca_accounts/");
    public static final AnonymousClass1Y8 A09;
    public static final AnonymousClass1Y8 A0A;
    public static final AnonymousClass1Y8 A0B;

    static {
        AnonymousClass1Y7 r1 = A00;
        A04 = (AnonymousClass1Y7) r1.A09("override_gating");
        A03 = (AnonymousClass1Y7) r1.A09("accountswich_visited");
        A05 = (AnonymousClass1Y7) r1.A09("saved_account/");
        A06 = (AnonymousClass1Y7) r1.A09("unseen_for_tab");
        A07 = (AnonymousClass1Y7) r1.A09("unseen_threads/");
        AnonymousClass1Y8 r12 = A08;
        A0A = r12.A09("account_switch_nux_flow_type");
        A0B = r12.A09("short_nux_needed");
        A09 = r12.A09("short_nux_after_reg_needed");
        AnonymousClass1Y7 r13 = A00;
        A02 = (AnonymousClass1Y7) r13.A09("get_dbl_nonce");
        A01 = (AnonymousClass1Y7) r13.A09("entering_source");
    }
}
