package X;

import android.graphics.Rect;

/* renamed from: X.1jk  reason: invalid class name and case insensitive filesystem */
public final class C31471jk implements C31481jl {
    public final AnonymousClass117 A00;

    public Rect Af0() {
        return this.A00.A0A;
    }

    public float AlS() {
        AnonymousClass117 r1 = this.A00;
        Rect rect = r1.A0A;
        int i = rect.bottom;
        return ((float) i) - (r1.A00 * ((float) (i - rect.top)));
    }

    public float AlT() {
        AnonymousClass117 r1 = this.A00;
        Rect rect = r1.A0A;
        int i = rect.top;
        return ((float) i) + (r1.A00 * ((float) (rect.bottom - i)));
    }

    public void BXg() {
        AnonymousClass10N r0 = this.A00.A04;
        if (r0 != null) {
            C33181nA.A00(r0);
        }
    }

    public void BYG() {
        AnonymousClass10N r0 = this.A00.A07;
        if (r0 != null) {
            C33181nA.A03(r0);
        }
    }

    public String getId() {
        return AnonymousClass08S.A0J("f_", this.A00.A00());
    }

    public C31471jk(AnonymousClass117 r1) {
        this.A00 = r1;
    }
}
