package X;

/* renamed from: X.1ww  reason: invalid class name and case insensitive filesystem */
public final class C38131ww extends Exception {
    public static final long serialVersionUID = 1;

    public C38131ww(Exception exc) {
        super(exc);
    }

    public C38131ww(String str) {
        super(str);
    }
}
