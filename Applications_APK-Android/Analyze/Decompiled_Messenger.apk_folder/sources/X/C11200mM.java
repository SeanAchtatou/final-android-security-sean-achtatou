package X;

import java.io.Closeable;

/* renamed from: X.0mM  reason: invalid class name and case insensitive filesystem */
public final class C11200mM implements Closeable {
    public final /* synthetic */ AnonymousClass0mL A00;

    public C11200mM(AnonymousClass0mL r1) {
        this.A00 = r1;
    }

    public void close() {
        this.A00.A00.writeLock().unlock();
    }
}
