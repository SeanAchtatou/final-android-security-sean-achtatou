package X;

import android.os.Looper;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0iN  reason: invalid class name */
public final class AnonymousClass0iN extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile C09730iO A01;

    public static final C09730iO A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = C09730iO.A00(Looper.getMainLooper());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
