package X;

import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0tV  reason: invalid class name and case insensitive filesystem */
public final class C14520tV {
    private AnonymousClass0UN A00;

    public static final C14520tV A00(AnonymousClass1XY r1) {
        return new C14520tV(r1);
    }

    public void A01(C32481lp r6, AnonymousClass12V r7, ImmutableList immutableList, C14570tc r9, Predicate predicate) {
        if (((Boolean) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BAo, this.A00)).booleanValue()) {
            ImmutableList copyOf = ImmutableList.copyOf(AnonymousClass0TH.A01(immutableList, predicate));
            int size = copyOf.size();
            for (int i = 0; i < size; i++) {
                r6.A01(r9.A05(r7.A01, (ThreadSummary) copyOf.get(i)));
            }
        }
    }

    public C14520tV(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
