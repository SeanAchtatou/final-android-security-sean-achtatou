package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.0sD  reason: invalid class name and case insensitive filesystem */
public final class C13870sD extends C13900sG {
    private static C04470Uu A01;
    public AnonymousClass0UN A00;

    public static final C13870sD A00(AnonymousClass1XY r4) {
        C13870sD r0;
        synchronized (C13870sD.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C13870sD((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C13870sD) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C13870sD(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
