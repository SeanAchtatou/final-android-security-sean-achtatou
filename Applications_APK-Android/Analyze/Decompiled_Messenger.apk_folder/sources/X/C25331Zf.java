package X;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Zf  reason: invalid class name and case insensitive filesystem */
public final class C25331Zf extends AnonymousClass0XX {
    private final C05190Xz A00 = new C05190Xz(this);

    public void done() {
        super.done();
        this.A00.A01(this);
    }

    public C25331Zf(Runnable runnable, Object obj) {
        super(runnable, obj);
    }

    public C25331Zf(Callable callable) {
        super(callable);
    }

    public Object get() {
        this.A00.A00();
        return super.get();
    }

    public Object get(long j, TimeUnit timeUnit) {
        this.A00.A00();
        return super.get(j, timeUnit);
    }
}
