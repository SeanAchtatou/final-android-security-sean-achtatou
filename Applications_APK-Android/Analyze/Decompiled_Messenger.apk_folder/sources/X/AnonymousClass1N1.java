package X;

import com.facebook.common.util.TriState;

/* renamed from: X.1N1  reason: invalid class name */
public final class AnonymousClass1N1 {
    public long A00;
    public TriState A01 = TriState.UNSET;
    public final AnonymousClass06B A02;
    private final C04980Xe A03;

    public synchronized TriState A00() {
        boolean z;
        TriState triState;
        synchronized (this) {
            z = true;
            if (this.A01 != TriState.UNSET && this.A02.now() - this.A00 < 3600000) {
                z = false;
            }
        }
        if (z) {
            if (this.A03.A03("image_pipeline_counters")) {
                triState = TriState.YES;
            } else {
                triState = TriState.NO;
            }
            this.A01 = triState;
            this.A00 = this.A02.now();
        }
        return this.A01;
    }

    public AnonymousClass1N1(C04980Xe r2, AnonymousClass06B r3) {
        this.A03 = r2;
        this.A02 = r3;
    }
}
