package X;

/* renamed from: X.1kn  reason: invalid class name and case insensitive filesystem */
public final class C31921kn {
    /* JADX WARNING: Can't wrap try/catch for region: R(2:36|37) */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r11 = com.facebook.device.resourcemonitor.DataUsageBytes.A02;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:36:0x00bf */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0102 A[SYNTHETIC, Splitter:B:63:0x0102] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.device.resourcemonitor.DataUsageBytes A00(int r17, int r18, int r19) {
        /*
            android.os.StrictMode$ThreadPolicy r16 = android.os.StrictMode.allowThreadDiskReads()
            r2 = 0
            java.io.File r1 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00f8 }
            java.lang.String r0 = "/proc/net/xt_qtaguid/stats"
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00f8 }
            java.io.DataInputStream r9 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x00f8 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00f8 }
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00f8 }
            r9.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00f8 }
            com.facebook.device.resourcemonitor.DataUsageBytes r13 = com.facebook.device.resourcemonitor.DataUsageBytes.A02     // Catch:{ FileNotFoundException -> 0x00f5, all -> 0x00f2 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00f5, all -> 0x00f2 }
            r10.<init>()     // Catch:{ FileNotFoundException -> 0x00f5, all -> 0x00f2 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r8 = new byte[r0]     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            int r7 = r9.read(r8)     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            r15 = 1
        L_0x0026:
            if (r7 <= 0) goto L_0x00dd
            r14 = 0
            r6 = 0
        L_0x002a:
            if (r6 >= r7) goto L_0x00d7
            byte r0 = r8[r6]     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            char r1 = (char) r0     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            r0 = 10
            if (r1 != r0) goto L_0x003b
            java.lang.String r0 = r10.toString()     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            r10.setLength(r14)     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            goto L_0x003f
        L_0x003b:
            r10.append(r1)     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            goto L_0x0042
        L_0x003f:
            if (r15 == 0) goto L_0x0045
            r15 = 0
        L_0x0042:
            int r6 = r6 + 1
            goto L_0x002a
        L_0x0045:
            java.util.StringTokenizer r1 = new java.util.StringTokenizer     // Catch:{ NoSuchElementException -> 0x00bf }
            r1.<init>(r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            r1.nextToken()     // Catch:{ NoSuchElementException -> 0x00bf }
            java.lang.String r2 = r1.nextToken()     // Catch:{ NoSuchElementException -> 0x00bf }
            java.lang.String r0 = "lo"
            boolean r0 = r2.equalsIgnoreCase(r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            if (r0 == 0) goto L_0x005c
            com.facebook.device.resourcemonitor.DataUsageBytes r11 = com.facebook.device.resourcemonitor.DataUsageBytes.A02     // Catch:{ NoSuchElementException -> 0x00bf }
            goto L_0x00c1
        L_0x005c:
            java.lang.String r4 = r1.nextToken()     // Catch:{ NoSuchElementException -> 0x00bf }
            java.lang.String r0 = "0x0"
            boolean r0 = r4.equals(r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            if (r0 == 0) goto L_0x0069
            goto L_0x0080
        L_0x0069:
            java.math.BigInteger r3 = new java.math.BigInteger     // Catch:{ NoSuchElementException -> 0x00bf }
            r2 = 2
            int r0 = r4.length()     // Catch:{ NoSuchElementException -> 0x00bf }
            int r0 = r0 + -8
            java.lang.String r2 = r4.substring(r2, r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            r0 = 16
            r3.<init>(r2, r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            int r0 = r3.intValue()     // Catch:{ NoSuchElementException -> 0x00bf }
            goto L_0x0081
        L_0x0080:
            r0 = 0
        L_0x0081:
            r2 = r18
            if (r0 == r2) goto L_0x0088
            com.facebook.device.resourcemonitor.DataUsageBytes r11 = com.facebook.device.resourcemonitor.DataUsageBytes.A02     // Catch:{ NoSuchElementException -> 0x00bf }
            goto L_0x00c1
        L_0x0088:
            java.lang.String r0 = r1.nextToken()     // Catch:{ NoSuchElementException -> 0x00bf }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            r2 = r17
            if (r0 == r2) goto L_0x0097
            com.facebook.device.resourcemonitor.DataUsageBytes r11 = com.facebook.device.resourcemonitor.DataUsageBytes.A02     // Catch:{ NoSuchElementException -> 0x00bf }
            goto L_0x00c1
        L_0x0097:
            java.lang.String r0 = r1.nextToken()     // Catch:{ NoSuchElementException -> 0x00bf }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            r2 = r19
            if (r0 == r2) goto L_0x00a6
            com.facebook.device.resourcemonitor.DataUsageBytes r11 = com.facebook.device.resourcemonitor.DataUsageBytes.A02     // Catch:{ NoSuchElementException -> 0x00bf }
            goto L_0x00c1
        L_0x00a6:
            java.lang.String r0 = r1.nextToken()     // Catch:{ NoSuchElementException -> 0x00bf }
            long r2 = java.lang.Long.parseLong(r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            r1.nextToken()     // Catch:{ NoSuchElementException -> 0x00bf }
            java.lang.String r0 = r1.nextToken()     // Catch:{ NoSuchElementException -> 0x00bf }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            com.facebook.device.resourcemonitor.DataUsageBytes r11 = new com.facebook.device.resourcemonitor.DataUsageBytes     // Catch:{ NoSuchElementException -> 0x00bf }
            r11.<init>(r2, r0)     // Catch:{ NoSuchElementException -> 0x00bf }
            goto L_0x00c1
        L_0x00bf:
            com.facebook.device.resourcemonitor.DataUsageBytes r11 = com.facebook.device.resourcemonitor.DataUsageBytes.A02     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
        L_0x00c1:
            com.facebook.device.resourcemonitor.DataUsageBytes r0 = com.facebook.device.resourcemonitor.DataUsageBytes.A02     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            if (r11 == r0) goto L_0x0042
            com.facebook.device.resourcemonitor.DataUsageBytes r12 = new com.facebook.device.resourcemonitor.DataUsageBytes     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            long r4 = r13.A00     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            long r0 = r11.A00     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            long r4 = r4 + r0
            long r2 = r13.A01     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            long r0 = r11.A01     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            long r2 = r2 + r0
            r12.<init>(r4, r2)     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            r13 = r12
            goto L_0x0042
        L_0x00d7:
            int r7 = r9.read(r8)     // Catch:{ IOException -> 0x00eb, NumberFormatException -> 0x00e4 }
            goto L_0x0026
        L_0x00dd:
            r9.close()     // Catch:{ IOException -> 0x00e0 }
        L_0x00e0:
            android.os.StrictMode.setThreadPolicy(r16)
            return r13
        L_0x00e4:
            r0 = move-exception
            X.1vu r1 = new X.1vu     // Catch:{ FileNotFoundException -> 0x00f5, all -> 0x00f2 }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00f5, all -> 0x00f2 }
            goto L_0x00f1
        L_0x00eb:
            r0 = move-exception
            X.1vu r1 = new X.1vu     // Catch:{ FileNotFoundException -> 0x00f5, all -> 0x00f2 }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00f5, all -> 0x00f2 }
        L_0x00f1:
            throw r1     // Catch:{ FileNotFoundException -> 0x00f5, all -> 0x00f2 }
        L_0x00f2:
            r0 = move-exception
            r2 = r9
            goto L_0x0100
        L_0x00f5:
            r1 = move-exception
            r2 = r9
            goto L_0x00f9
        L_0x00f8:
            r1 = move-exception
        L_0x00f9:
            X.1vu r0 = new X.1vu     // Catch:{ all -> 0x00ff }
            r0.<init>(r1)     // Catch:{ all -> 0x00ff }
            throw r0     // Catch:{ all -> 0x00ff }
        L_0x00ff:
            r0 = move-exception
        L_0x0100:
            if (r2 == 0) goto L_0x0105
            r2.close()     // Catch:{ IOException -> 0x0105 }
        L_0x0105:
            android.os.StrictMode.setThreadPolicy(r16)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31921kn.A00(int, int, int):com.facebook.device.resourcemonitor.DataUsageBytes");
    }
}
