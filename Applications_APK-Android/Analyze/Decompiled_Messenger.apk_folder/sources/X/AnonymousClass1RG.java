package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1RG  reason: invalid class name */
public final class AnonymousClass1RG extends C17770zR {
    @Comparable(type = 13)
    public InboxUnitThreadItem A00;
    @Comparable(type = 13)
    public ThreadSummary A01;
    @Comparable(type = 13)
    public C16400x0 A02;
    @Comparable(type = 13)
    public MigColorScheme A03;

    public AnonymousClass1RG(Context context) {
        super("M4ThreadItemThreadTitleComponent");
        new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
