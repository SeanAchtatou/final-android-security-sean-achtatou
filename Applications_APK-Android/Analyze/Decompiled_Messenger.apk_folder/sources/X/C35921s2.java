package X;

import android.os.DVFSHelper;
import io.card.payment.BuildConfig;
import org.json.JSONObject;

/* renamed from: X.1s2  reason: invalid class name and case insensitive filesystem */
public final class C35921s2 implements C26381bM {
    private static boolean A00;

    public static boolean A00() {
        Class<DVFSHelper> cls = DVFSHelper.class;
        try {
            cls.toString();
            Class cls2 = Boolean.TYPE;
            if (!C34061oa.A01(cls, "onScrollEvent", cls2)) {
                return false;
            }
            A00 = C34061oa.A01(cls, "onSmoothScrollEvent", cls2);
            return true;
        } catch (Error | Exception unused) {
            return false;
        }
    }

    public AnonymousClass0f7 AUy(C26401bO r4, C26501bY r5) {
        int[] Aet = r4.Aet(r5);
        if (Aet == null || Aet.length == 0) {
            return null;
        }
        boolean z = false;
        if (Aet[0] > 50 && A00) {
            z = true;
        }
        return new AnonymousClass38D(r5.A01, z);
    }

    public int AyN() {
        return 4;
    }

    public int AyO() {
        return 2;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", "samsung");
            jSONObject.put("framework", "DVFSHelper");
            jSONObject.put("extra", A00 ? "partial" : BuildConfig.FLAVOR);
            return jSONObject.toString();
        } catch (Exception unused) {
            return BuildConfig.FLAVOR;
        }
    }
}
