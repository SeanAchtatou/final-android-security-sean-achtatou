package X;

import android.os.Bundle;
import com.facebook.messaging.inbox2.items.InboxTrackableItem;

/* renamed from: X.1Af  reason: invalid class name and case insensitive filesystem */
public final class C19921Af implements AnonymousClass1AZ {
    private AnonymousClass0UN A00;

    public void BMG(C24201Sr r1) {
    }

    public void BMH(C24201Sr r1) {
    }

    public void C8h(boolean z) {
    }

    public boolean CEL() {
        return false;
    }

    public static final C19921Af A00(AnonymousClass1XY r1) {
        return new C19921Af(r1);
    }

    public C33901oK Apl() {
        return C33901oK.A0b;
    }

    public void CKW(C24201Sr r9, boolean z) {
        Bundle bundle;
        if (z && (bundle = ((InboxTrackableItem) r9.A05).A03) != null) {
            ((AnonymousClass2KI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APZ, this.A00)).A02(bundle.getLong("thread_summary"), bundle.getString("message_id"), bundle.getLong("timestamp"));
        }
    }

    private C19921Af(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
