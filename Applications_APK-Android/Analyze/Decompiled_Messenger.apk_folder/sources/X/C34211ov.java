package X;

import java.io.File;
import java.io.FileNotFoundException;

/* renamed from: X.1ov  reason: invalid class name and case insensitive filesystem */
public final class C34211ov {
    public static void A00(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                return;
            }
            if (!file.delete()) {
                throw new C182098ck(file.getAbsolutePath(), new AnonymousClass51U(file.getAbsolutePath()));
            }
        }
        if (!file.mkdirs() && !file.isDirectory()) {
            throw new C182098ck(file.getAbsolutePath());
        }
    }

    public static void A01(File file, File file2) {
        C05520Zg.A02(file);
        C05520Zg.A02(file2);
        file2.delete();
        if (!file.renameTo(file2)) {
            Throwable th = null;
            if (file2.exists()) {
                th = new AnonymousClass51U(file2.getAbsolutePath());
            } else if (!file.getParentFile().exists()) {
                th = new C182818dx(file.getAbsolutePath());
            } else if (!file.exists()) {
                th = new FileNotFoundException(file.getAbsolutePath());
            }
            throw new AnonymousClass51S(AnonymousClass08S.A0S("Unknown error renaming ", file.getAbsolutePath(), " to ", file2.getAbsolutePath()), th);
        }
    }
}
