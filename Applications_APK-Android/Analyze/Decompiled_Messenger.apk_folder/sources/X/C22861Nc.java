package X;

import android.util.SparseIntArray;

/* renamed from: X.1Nc  reason: invalid class name and case insensitive filesystem */
public class C22861Nc extends C22871Nd implements C22881Ne {
    public final int[] A00;

    public C22861Nc(C14320t5 r5, AnonymousClass1N7 r6, AnonymousClass1N4 r7) {
        super(r5, r6, r7);
        SparseIntArray sparseIntArray = r6.A04;
        this.A00 = new int[sparseIntArray.size()];
        for (int i = 0; i < sparseIntArray.size(); i++) {
            this.A00[i] = sparseIntArray.keyAt(i);
        }
        this.A04.C0d(this);
        this.A08.C6L(this);
    }
}
