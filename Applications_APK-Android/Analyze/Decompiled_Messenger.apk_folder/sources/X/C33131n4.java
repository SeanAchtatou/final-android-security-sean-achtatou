package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps$EntryFunction;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1n4  reason: invalid class name and case insensitive filesystem */
public class C33131n4<K, V> extends C35361r6<K> {
    public final Map A00;

    public void clear() {
        this.A00.clear();
    }

    public boolean contains(Object obj) {
        return this.A00.containsKey(obj);
    }

    public boolean isEmpty() {
        return this.A00.isEmpty();
    }

    public Iterator iterator() {
        Iterator it = this.A00.entrySet().iterator();
        Maps$EntryFunction maps$EntryFunction = Maps$EntryFunction.KEY;
        Preconditions.checkNotNull(maps$EntryFunction);
        return new C26991cT(it, maps$EntryFunction);
    }

    public int size() {
        return this.A00.size();
    }

    public C33131n4(Map map) {
        Preconditions.checkNotNull(map);
        this.A00 = map;
    }

    public boolean remove(Object obj) {
        if (!contains(obj)) {
            return false;
        }
        this.A00.remove(obj);
        return true;
    }
}
