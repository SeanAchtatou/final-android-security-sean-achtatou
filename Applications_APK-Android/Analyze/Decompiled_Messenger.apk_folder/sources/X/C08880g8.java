package X;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0g8  reason: invalid class name and case insensitive filesystem */
public abstract class C08880g8 extends C08890g9 implements Future {
    public Future A02() {
        return !(this instanceof AnonymousClass14X) ? !(this instanceof AnonymousClass14Y) ? ((C08960gG) this).A00 : ((AnonymousClass14Y) this).A00 : ((AnonymousClass14X) this).A03();
    }

    public boolean cancel(boolean z) {
        return A02().cancel(z);
    }

    public boolean isCancelled() {
        return A02().isCancelled();
    }

    public boolean isDone() {
        return A02().isDone();
    }

    public Object get() {
        return A02().get();
    }

    public Object get(long j, TimeUnit timeUnit) {
        return A02().get(j, timeUnit);
    }
}
