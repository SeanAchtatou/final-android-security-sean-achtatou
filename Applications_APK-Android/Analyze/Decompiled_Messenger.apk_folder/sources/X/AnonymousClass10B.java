package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.10B  reason: invalid class name */
public final class AnonymousClass10B {
    private static volatile AnonymousClass10B A00;

    public static final AnonymousClass10B A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (AnonymousClass10B.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new AnonymousClass10B(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private AnonymousClass10B(AnonymousClass1XY r3) {
        new AnonymousClass0UN(1, r3);
    }
}
