package X;

import android.view.Choreographer;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.dalvikdistract.DalvikDistract;

/* renamed from: X.0gZ  reason: invalid class name and case insensitive filesystem */
public final class C09120gZ implements AnonymousClass03a {
    private static boolean A00;

    public void Bse() {
        synchronized (C09120gZ.class) {
            if (AnonymousClass08Z.A05(262144) && !A00) {
                String property = System.getProperty(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1H));
                if (property == null || property.startsWith("0.") || property.startsWith("1.")) {
                    Class<AnonymousClass8UR> cls = AnonymousClass8UR.class;
                    Class<Choreographer> cls2 = Choreographer.class;
                    try {
                        Class<?> cls3 = Class.forName(AnonymousClass08S.A0J(cls2.getName(), "$CallbackRecord"));
                        DalvikDistract dalvikDistract = new DalvikDistract();
                        dalvikDistract.hook(cls2.getDeclaredMethod("scheduleVsyncLocked", new Class[0]), cls.getDeclaredMethod("hookScheduleVsyncLocked", cls2), cls.getDeclaredMethod("originalScheduleVsyncLocked", cls2));
                        Class cls4 = Long.TYPE;
                        dalvikDistract.hook(cls2.getDeclaredMethod("doFrame", cls4, Integer.TYPE), cls.getDeclaredMethod("hookDoFrame", cls2, cls4, Integer.TYPE), cls.getDeclaredMethod("originalDoFrame", cls2, cls4, Integer.TYPE));
                        DalvikDistract.Unsafe unsafe = DalvikDistract.unsafe(cls3.getDeclaredMethod("run", Long.TYPE));
                        Class<Object> cls5 = Object.class;
                        Class cls6 = Long.TYPE;
                        dalvikDistract.hook(unsafe, cls.getDeclaredMethod("hookRun", cls5, cls6), cls.getDeclaredMethod("originalRun", cls5, cls6));
                        dalvikDistract.commit();
                        A00 = true;
                    } catch (ClassNotFoundException | NoSuchMethodException e) {
                        C010708t.A08(cls, "Failed to hook", e);
                        throw new RuntimeException(e);
                    }
                } else {
                    throw new RuntimeException("ART not supported.");
                }
            }
        }
    }

    public void Bsf() {
        synchronized (C09120gZ.class) {
            if (A00) {
                Class<Choreographer> cls = Choreographer.class;
                try {
                    Class<?> cls2 = Class.forName(AnonymousClass08S.A0J(cls.getName(), "$CallbackRecord"));
                    DalvikDistract dalvikDistract = new DalvikDistract();
                    dalvikDistract.unhook(cls.getDeclaredMethod("scheduleVsyncLocked", new Class[0]));
                    dalvikDistract.unhook(cls.getDeclaredMethod("doFrame", Long.TYPE, Integer.TYPE));
                    dalvikDistract.unhook(cls2.getDeclaredMethod("run", Long.TYPE));
                    dalvikDistract.commit();
                    A00 = false;
                } catch (ClassNotFoundException | NoSuchMethodException e) {
                    C010708t.A08(AnonymousClass8UR.class, "Failed to unhook", e);
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
