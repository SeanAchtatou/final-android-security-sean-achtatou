package X;

import java.util.List;
import java.util.Set;

/* renamed from: X.0UH  reason: invalid class name */
public interface AnonymousClass0UH {
    Object getInstance(int i);

    Object getInstance(C22916BKm bKm);

    Object getInstance(Class cls);

    Object getInstance(Class cls, Class cls2);

    AnonymousClass0US getLazy(C22916BKm bKm);

    AnonymousClass0US getLazyList(C22916BKm bKm);

    AnonymousClass0US getLazySet(C22916BKm bKm);

    List getList(C22916BKm bKm);

    C04310Tq getListProvider(C22916BKm bKm);

    C04310Tq getProvider(C22916BKm bKm);

    C24891Xn getScope(Class cls);

    Set getSet(C22916BKm bKm);

    C04310Tq getSetProvider(C22916BKm bKm);
}
