package X;

import android.view.animation.Interpolator;

/* renamed from: X.0On  reason: invalid class name and case insensitive filesystem */
public class C03560On implements Interpolator {
    private final Interpolator[] A00;

    public float getInterpolation(float f) {
        for (int length = this.A00.length - 1; length >= 0; length--) {
            f = this.A00[length].getInterpolation(f);
        }
        return f;
    }

    public C03560On(Interpolator... interpolatorArr) {
        this.A00 = interpolatorArr;
    }
}
