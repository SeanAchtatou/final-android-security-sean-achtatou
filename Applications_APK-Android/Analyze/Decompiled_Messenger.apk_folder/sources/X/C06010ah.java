package X;

import android.app.PendingIntent;
import android.os.Bundle;
import androidx.core.graphics.drawable.IconCompat;
import io.card.payment.BuildConfig;

/* renamed from: X.0ah  reason: invalid class name and case insensitive filesystem */
public class C06010ah {
    public int A00;
    public PendingIntent A01;
    public CharSequence A02;
    public boolean A03;
    public boolean A04;
    private IconCompat A05;
    public final int A06;
    public final Bundle A07;
    public final boolean A08;
    public final C623030w[] A09;
    public final C623030w[] A0A;

    public IconCompat A00() {
        int i;
        if (this.A05 == null && (i = this.A00) != 0) {
            this.A05 = IconCompat.A00(null, BuildConfig.FLAVOR, i);
        }
        return this.A05;
    }

    public CharSequence A01() {
        return this.A02;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public C06010ah(int i, CharSequence charSequence, PendingIntent pendingIntent) {
        this(i != 0 ? IconCompat.A00(null, BuildConfig.FLAVOR, i) : null, charSequence, pendingIntent, new Bundle(), null, null, true, 0, true, false);
    }

    public C06010ah(IconCompat iconCompat, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, C623030w[] r7, C623030w[] r8, boolean z, int i, boolean z2, boolean z3) {
        this.A04 = true;
        this.A05 = iconCompat;
        if (iconCompat != null && iconCompat.A03() == 2) {
            this.A00 = iconCompat.A02();
        }
        this.A02 = AnonymousClass0ZN.A00(charSequence);
        this.A01 = pendingIntent;
        this.A07 = bundle == null ? new Bundle() : bundle;
        this.A0A = r7;
        this.A09 = r8;
        this.A03 = z;
        this.A06 = i;
        this.A04 = z2;
        this.A08 = z3;
    }
}
