package X;

import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.fbservice.service.ServiceException;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.orca.threadlist.ThreadListFragment;
import com.facebook.proxygen.LigerSamplePolicy;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;
import io.card.payment.BuildConfig;

/* renamed from: X.17Q  reason: invalid class name */
public final class AnonymousClass17Q implements C20021Ap {
    public final /* synthetic */ AnonymousClass16R A00;

    public AnonymousClass17Q(AnonymousClass16R r1) {
        this.A00 = r1;
    }

    public void Bd1(Object obj, Object obj2) {
        C55452o6 r8 = (C55452o6) obj2;
        ThreadListFragment threadListFragment = this.A00.A0G.A00;
        C35381r8 r1 = ((C32971md) obj).A02;
        if (r1 == C35381r8.A02) {
            threadListFragment.A1M.clear();
            threadListFragment.A0n = r8;
            C196118y r0 = threadListFragment.A0w;
            if (r0 != null) {
                r0.A02.A0C(false);
            }
            threadListFragment.A1Y = true;
            AnonymousClass16N r2 = threadListFragment.A11;
            Integer num = AnonymousClass07B.A01;
            C13880sE r02 = threadListFragment.A14.A0H;
            if (r02 == null) {
                r02 = C13880sE.A07;
            }
            r2.A03(num, r02);
            if (r8.A01 && threadListFragment.A1a() && (!((Boolean) threadListFragment.A1R.get()).booleanValue() || threadListFragment.A0Z.isConnected())) {
                ServiceException serviceException = r8.A00;
                if (threadListFragment.A0r == null) {
                    threadListFragment.A0r = (C67023Mq) threadListFragment.A1Q.get();
                }
                C67023Mq r4 = threadListFragment.A0r;
                r4.A00 = 80;
                r4.A03 = LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT;
                r4.A08 = true;
                String A01 = ((C55972p3) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ANl, threadListFragment.A0O)).A01(serviceException, false, true);
                if (C06850cB.A0B(A01)) {
                    A01 = BuildConfig.FLAVOR;
                }
                threadListFragment.A0r.A02(threadListFragment.A2K(2131297849), A01);
            }
            threadListFragment.A0U.A06("thread_list");
        } else if (r1 == C35381r8.MORE_THREADS) {
            C13500rX r22 = new C13500rX(threadListFragment.A1j());
            r22.A09(2131832814);
            r22.A08(2131832812);
            r22.A02(17039370, new C120495mc());
            r22.A07();
            threadListFragment.A1U = true;
            ThreadListFragment.A0J(threadListFragment, threadListFragment.A10);
        }
        AnonymousClass16R.A01(this.A00);
    }

    public void BdJ(Object obj, Object obj2) {
        C196118y r0;
        C32971md r13 = (C32971md) obj;
        ThreadListFragment threadListFragment = this.A00.A0G.A00;
        DataFetchDisposition dataFetchDisposition = ((C13890sF) obj2).A01;
        boolean z = false;
        if (dataFetchDisposition.A07 != AnonymousClass0u3.SMS) {
            threadListFragment.A1c = false;
        }
        if (!dataFetchDisposition.A05.asBoolean(false)) {
            if (!(threadListFragment.A0I == null || r13.A02 != C35381r8.A02 || (r0 = threadListFragment.A0w) == null)) {
                r0.A02.A0C(false);
            }
            C33891oJ r3 = (C33891oJ) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AoV, threadListFragment.A0O);
            if (!r3.A01) {
                r3.A01 = true;
                ThreadsCollection A09 = ((C26681bq) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AuB, r3.A00)).A09(C10950l8.A05, C10700ki.NON_SMS, RegularImmutableSet.A05);
                C34011oV r9 = r3.A04;
                int At0 = (int) ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C35221qs) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AP0, r3.A00)).A00)).At0(564058054984367L);
                int A01 = ((C35221qs) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AP0, r3.A00)).A01();
                ImmutableList.Builder builder = ImmutableList.builder();
                C24971Xv it = A09.A00.iterator();
                int i = 0;
                while (it.hasNext()) {
                    ThreadSummary threadSummary = (ThreadSummary) it.next();
                    int i2 = i + 1;
                    if (i == At0) {
                        break;
                    }
                    ThreadKey threadKey = threadSummary.A0S;
                    if (!C26681bq.A02((C26681bq) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AuB, r9.A01), threadKey).A0k(threadKey, A01)) {
                        builder.add((Object) threadSummary.A0S);
                    }
                    i = i2;
                }
                ImmutableList build = builder.build();
                int i3 = AnonymousClass1Y3.AP0;
                ((C35221qs) AnonymousClass1XX.A02(3, i3, r3.A00)).A01();
                if (!build.isEmpty()) {
                    r3.A05 = ImmutableSet.A0A(build);
                    C33891oJ.A01(r3, build, ((C35221qs) AnonymousClass1XX.A02(3, i3, r3.A00)).A01(), "TopThreads");
                }
            }
        } else if (!((Boolean) threadListFragment.A1S.get()).booleanValue()) {
            threadListFragment.A14.A0C(AnonymousClass1FD.AUTOMATIC_REFRESH, "sync disabled", "ThreadListFragment");
        } else {
            if (((C10960l9) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BOk, threadListFragment.A0O)).A0B() && !threadListFragment.A1c) {
                z = true;
            }
            AnonymousClass0lL A012 = AnonymousClass07W.A01(threadListFragment.A0N, "ensure_sync", new Bundle(), CallerContext.A09("ThreadListFragment"), -1057627449);
            A012.C81(!z);
            C27211cp A02 = ((C32271lU) threadListFragment.A1T.get()).A02(A012);
            if (z) {
                C05350Yp.A08(A02, new C409823r(threadListFragment), threadListFragment.A1N);
            }
        }
        AnonymousClass16R.A01(this.A00);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0042, code lost:
        if (r1 == false) goto L_0x0044;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BdU(java.lang.Object r6, com.google.common.util.concurrent.ListenableFuture r7) {
        /*
            r5 = this;
            X.1md r6 = (X.C32971md) r6
            int r1 = X.AnonymousClass1Y3.Aq4
            X.16R r0 = r5.A00
            X.0UN r0 = r0.A08
            r4 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1eq r0 = (X.C28461eq) r0
            r0.A05(r7)
            X.16R r0 = r5.A00
            X.15b r0 = r0.A0G
            com.facebook.orca.threadlist.ThreadListFragment r3 = r0.A00
            X.3Mq r0 = r3.A0r
            if (r0 == 0) goto L_0x001f
            r0.A01()
        L_0x001f:
            X.1r8 r1 = r6.A02
            X.1r8 r0 = X.C35381r8.A02
            if (r1 != r0) goto L_0x0054
            r0 = 0
            r3.A0n = r0
            X.16N r2 = r3.A11
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            X.16R r0 = r3.A14
            X.0sE r0 = r0.A0H
            if (r0 != 0) goto L_0x0034
            X.0sE r0 = X.C13880sE.A07
        L_0x0034:
            r2.A03(r1, r0)
            X.16R r0 = r3.A14
            X.1ja r0 = r0.A0F
            X.1md r0 = r0.A06
            if (r0 == 0) goto L_0x0044
            boolean r1 = r0.A04
            r0 = 1
            if (r1 != 0) goto L_0x0045
        L_0x0044:
            r0 = 0
        L_0x0045:
            if (r0 == 0) goto L_0x0053
            X.18y r2 = r3.A0w
            if (r2 == 0) goto L_0x0053
            X.1Ed r1 = r2.A02
            r0 = 1
            r1.A0C(r0)
            r2.A01 = r4
        L_0x0053:
            return
        L_0x0054:
            X.1r8 r0 = X.C35381r8.MORE_THREADS
            if (r1 != r0) goto L_0x0053
            java.lang.Integer r2 = X.AnonymousClass07B.A0C
            r3.A1J = r2
            X.0sE r1 = r3.A10
            r0 = 1
            com.facebook.orca.threadlist.ThreadListFragment.A0K(r3, r1, r2, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass17Q.BdU(java.lang.Object, com.google.common.util.concurrent.ListenableFuture):void");
    }

    public void Bgh(Object obj, Object obj2) {
        C32971md r4 = (C32971md) obj;
        C13890sF r5 = (C13890sF) obj2;
        this.A00.A0X.put(r4.A01, r5);
        this.A00.A0G.A01(r4, r5);
        AnonymousClass16R.A04(this.A00, AnonymousClass07B.A01, "TL onNewResult");
    }
}
