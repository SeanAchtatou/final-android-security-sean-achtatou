package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ML  reason: invalid class name */
public final class AnonymousClass1ML implements AnonymousClass1OH {
    private static volatile AnonymousClass1ML A01;
    private AnonymousClass0UN A00;

    public static final AnonymousClass1ML A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1ML.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1ML(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0061, code lost:
        if (r8 != X.C413024x.class) goto L_0x0063;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BJ8(java.lang.Integer r7, java.lang.Class r8, java.lang.String r9, java.lang.Throwable r10) {
        /*
            r6 = this;
            java.lang.String r1 = "MEDIACACHE_ERROR_"
            int r0 = r7.intValue()
            switch(r0) {
                case 1: goto L_0x007e;
                case 2: goto L_0x0081;
                case 3: goto L_0x0084;
                case 4: goto L_0x0087;
                case 5: goto L_0x008a;
                case 6: goto L_0x008e;
                case 7: goto L_0x0092;
                case 8: goto L_0x0096;
                case 9: goto L_0x009a;
                case 10: goto L_0x009e;
                case 11: goto L_0x00a2;
                case 12: goto L_0x00a6;
                case 13: goto L_0x00aa;
                case 14: goto L_0x00ae;
                case 15: goto L_0x00b2;
                case 16: goto L_0x00b6;
                default: goto L_0x0009;
            }
        L_0x0009:
            java.lang.String r0 = "READ_DECODE"
        L_0x000b:
            java.lang.String r2 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.String r1 = r8.getName()
            java.lang.String r0 = ":"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r9)
            X.06G r0 = X.AnonymousClass06F.A02(r2, r0)
            if (r10 == 0) goto L_0x0021
            r0.A03 = r10
        L_0x0021:
            X.06F r5 = r0.A00()
            int r2 = X.AnonymousClass1Y3.Amr
            X.0UN r1 = r6.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.09P r0 = (X.AnonymousClass09P) r0
            r0.CGQ(r5)
            int r2 = X.AnonymousClass1Y3.Ayr
            X.0UN r1 = r6.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.8Rj r0 = (X.C179568Rj) r0
            X.8Rk r4 = r0.A00
            java.lang.String r3 = r5.A01
            java.lang.Throwable r2 = r5.A03
            java.lang.String r1 = r5.A02
            java.lang.String r0 = "%s %s %s"
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r3, r2, r1)
            r4.A00(r0)
            int r2 = X.AnonymousClass1Y3.At2
            X.0UN r1 = r6.A00
            r0 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4WT r2 = (X.AnonymousClass4WT) r2
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            if (r7 != r0) goto L_0x0063
            java.lang.Class<X.24x> r1 = X.C413024x.class
            r0 = 1
            if (r8 == r1) goto L_0x0064
        L_0x0063:
            r0 = 0
        L_0x0064:
            if (r0 == 0) goto L_0x007d
            com.facebook.analytics.DeprecatedAnalyticsLogger r2 = r2.A00
            java.lang.String r1 = "disk_storage_cache_read_invalid_entry_event"
            r0 = 0
            X.1La r1 = r2.A04(r1, r0)
            boolean r0 = r1.A0B()
            if (r0 == 0) goto L_0x007d
            java.lang.String r0 = "message"
            r1.A06(r0, r9)
            r1.A0A()
        L_0x007d:
            return
        L_0x007e:
            java.lang.String r0 = "READ_FILE"
            goto L_0x000b
        L_0x0081:
            java.lang.String r0 = "READ_FILE_NOT_FOUND"
            goto L_0x000b
        L_0x0084:
            java.lang.String r0 = "READ_INVALID_ENTRY"
            goto L_0x000b
        L_0x0087:
            java.lang.String r0 = "WRITE_ENCODE"
            goto L_0x000b
        L_0x008a:
            java.lang.String r0 = "WRITE_CREATE_TEMPFILE"
            goto L_0x000b
        L_0x008e:
            java.lang.String r0 = "WRITE_UPDATE_FILE_NOT_FOUND"
            goto L_0x000b
        L_0x0092:
            java.lang.String r0 = "WRITE_RENAME_FILE_TEMPFILE_NOT_FOUND"
            goto L_0x000b
        L_0x0096:
            java.lang.String r0 = "WRITE_RENAME_FILE_TEMPFILE_PARENT_NOT_FOUND"
            goto L_0x000b
        L_0x009a:
            java.lang.String r0 = "WRITE_RENAME_FILE_OTHER"
            goto L_0x000b
        L_0x009e:
            java.lang.String r0 = "WRITE_CREATE_DIR"
            goto L_0x000b
        L_0x00a2:
            java.lang.String r0 = "WRITE_CALLBACK_ERROR"
            goto L_0x000b
        L_0x00a6:
            java.lang.String r0 = "WRITE_INVALID_ENTRY"
            goto L_0x000b
        L_0x00aa:
            java.lang.String r0 = "DELETE_FILE"
            goto L_0x000b
        L_0x00ae:
            java.lang.String r0 = "EVICTION"
            goto L_0x000b
        L_0x00b2:
            java.lang.String r0 = "GENERIC_IO"
            goto L_0x000b
        L_0x00b6:
            java.lang.String r0 = "OTHER"
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ML.BJ8(java.lang.Integer, java.lang.Class, java.lang.String, java.lang.Throwable):void");
    }

    private AnonymousClass1ML(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
