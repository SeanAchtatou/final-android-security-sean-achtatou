package X;

import android.content.Context;
import android.content.Intent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.16d  reason: invalid class name and case insensitive filesystem */
public class C189316d {
    private static volatile C189316d A01;
    public final String A00;

    public static final C189316d A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C189316d.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C189316d(AnonymousClass1YA.A00(r4.getApplicationInjector()).getPackageName());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A02(Intent intent, Context context) {
        intent.setPackage(this.A00);
        context.sendBroadcast(intent);
    }

    public C189316d(String str) {
        C05520Zg.A02(str);
        this.A00 = str;
    }
}
