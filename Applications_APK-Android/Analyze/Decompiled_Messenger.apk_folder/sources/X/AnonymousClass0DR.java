package X;

/* renamed from: X.0DR  reason: invalid class name */
public final class AnonymousClass0DR {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final boolean A09;

    public AnonymousClass0DR(String str, int i, int i2, String str2, String str3, int i3, int i4, String str4, String str5, boolean z) {
        this.A08 = str;
        this.A02 = i;
        this.A03 = i2;
        this.A06 = str3;
        this.A01 = i3;
        this.A00 = i4;
        this.A04 = str4;
        this.A07 = str2;
        this.A05 = str5;
        this.A09 = z;
    }
}
