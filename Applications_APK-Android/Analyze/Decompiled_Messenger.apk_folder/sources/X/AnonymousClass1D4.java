package X;

import com.facebook.payments.contactinfo.picker.ContactInfoPickerRunTimeData;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1D4  reason: invalid class name */
public final class AnonymousClass1D4 extends C06020ai {
    public final /* synthetic */ ContactInfoPickerRunTimeData A00;
    public final /* synthetic */ C54252m7 A01;
    public final /* synthetic */ C23694Bka A02;
    public final /* synthetic */ ImmutableList.Builder A03;

    public AnonymousClass1D4(C54252m7 r1, ContactInfoPickerRunTimeData contactInfoPickerRunTimeData, ImmutableList.Builder builder, C23694Bka bka) {
        this.A01 = r1;
        this.A00 = contactInfoPickerRunTimeData;
        this.A03 = builder;
        this.A02 = bka;
    }
}
