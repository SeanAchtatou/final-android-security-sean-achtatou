package X;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1dD  reason: invalid class name and case insensitive filesystem */
public final class C27451dD {
    private static volatile C27451dD A09;
    public final C11420n7 A00;
    public final LinkedList A01 = new LinkedList();
    public final Lock A02 = new ReentrantLock();
    public final List A03 = new CopyOnWriteArrayList();
    public final List A04 = new CopyOnWriteArrayList();
    public volatile long A05 = 0;
    public volatile long A06 = 0;
    public volatile long A07 = 0;
    public volatile long A08;

    public static final C27451dD A00(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (C27451dD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new C27451dD(C11420n7.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    private C27451dD(C11420n7 r3) {
        this.A00 = r3;
    }
}
