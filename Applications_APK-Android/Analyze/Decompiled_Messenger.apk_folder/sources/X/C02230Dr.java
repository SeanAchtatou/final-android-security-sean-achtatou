package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.0Dr  reason: invalid class name and case insensitive filesystem */
public final class C02230Dr implements AnonymousClass06U {
    public final /* synthetic */ C02220Dq A00;
    public final /* synthetic */ Runnable A01;

    public C02230Dr(C02220Dq r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r7) {
        int A002 = AnonymousClass09Y.A00(-1908167240);
        String dataString = intent.getDataString();
        C012309k r0 = this.A00.A00;
        if (AnonymousClass0AB.A01(r0.A00, dataString, r0.A01)) {
            this.A01.run();
        }
        AnonymousClass09Y.A01(324140484, A002);
    }
}
