package X;

import android.os.Build;
import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1fj  reason: invalid class name and case insensitive filesystem */
public final class C29011fj {
    private static C05540Zi A01;
    private AnonymousClass0UN A00;

    public static final C29011fj A00(AnonymousClass1XY r4) {
        C29011fj r0;
        synchronized (C29011fj.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C29011fj((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C29011fj) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public boolean A01() {
        if (Build.VERSION.SDK_INT <= 28 || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282351150040420L)) {
            return false;
        }
        return true;
    }

    private C29011fj(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
