package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.1nj  reason: invalid class name and case insensitive filesystem */
public final class C33531nj {
    public final long A00;
    public final AnonymousClass1T9 A01;
    public final ImmutableList A02;
    private final long A03;
    private final Float A04;
    private final String A05;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C33531nj) {
                C33531nj r8 = (C33531nj) obj;
                if (!C28931fb.A07(this.A01, r8.A01) || this.A03 != r8.A03 || this.A00 != r8.A00 || !C28931fb.A07(this.A02, r8.A02) || !C28931fb.A07(this.A04, r8.A04) || !C28931fb.A07(this.A05, r8.A05)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A02(C28931fb.A02(C28931fb.A03(1, this.A01), this.A03), this.A00), this.A02), this.A04), this.A05);
    }

    public C33531nj(C24311Tc r3) {
        AnonymousClass1T9 r1 = r3.A02;
        C28931fb.A06(r1, "fBMMontageThreadBriefSummary");
        this.A01 = r1;
        this.A03 = r3.A00;
        this.A00 = r3.A01;
        ImmutableList immutableList = r3.A03;
        C28931fb.A06(immutableList, "messages");
        this.A02 = immutableList;
        this.A04 = r3.A04;
        this.A05 = r3.A05;
    }
}
