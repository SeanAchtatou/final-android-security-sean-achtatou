package X;

import java.lang.reflect.Array;

/* renamed from: X.07J  reason: invalid class name */
public final class AnonymousClass07J {
    public static final Class A0A = AnonymousClass07J.class;
    public int A00;
    public long A01;
    public Object[] A02;
    public final int A03;
    public final int A04;
    public final long A05;
    public final AnonymousClass069 A06;
    private final int A07;
    private final C005906e A08;
    private final Class A09;

    public synchronized Object A01() {
        Object create;
        int i = this.A00;
        if (i > 0) {
            int i2 = i - 1;
            this.A00 = i2;
            Object[] objArr = this.A02;
            create = objArr[i2];
            objArr[i2] = null;
        } else {
            create = this.A08.create();
        }
        this.A08.BO1(create);
        return create;
    }

    public synchronized void A02(Object obj) {
        synchronized (this) {
            long now = this.A06.now();
            if (this.A00 < (this.A03 << 1)) {
                this.A01 = now;
            }
            if (now - this.A01 > this.A05) {
                synchronized (this) {
                    int length = this.A02.length;
                    int max = Math.max(length - this.A03, this.A04);
                    if (max != length) {
                        A00(max);
                    }
                }
            }
        }
        this.A08.BlQ(obj);
        int i = this.A00;
        int i2 = this.A07;
        if (i < i2) {
            int i3 = i + 1;
            int length2 = this.A02.length;
            if (i3 > length2) {
                A00(Math.min(i2, length2 + this.A03));
            }
            Object[] objArr = this.A02;
            int i4 = this.A00;
            this.A00 = i4 + 1;
            objArr[i4] = obj;
        }
    }

    private void A00(int i) {
        Object[] objArr = (Object[]) Array.newInstance(this.A09, i);
        Object[] objArr2 = this.A02;
        System.arraycopy(objArr2, 0, objArr, 0, Math.min(objArr2.length, i));
        this.A02 = objArr;
        this.A00 = Math.min(this.A00, i);
    }

    public AnonymousClass07J(Class cls, int i, int i2, int i3, long j, C005906e r9, AnonymousClass069 r10) {
        this.A09 = cls;
        int max = Math.max(i, 0);
        this.A04 = max;
        this.A07 = Math.max(max, i2);
        this.A03 = Math.max(i3, 1);
        this.A05 = j;
        this.A08 = r9;
        this.A06 = r10;
        this.A02 = (Object[]) Array.newInstance(cls, max);
    }
}
