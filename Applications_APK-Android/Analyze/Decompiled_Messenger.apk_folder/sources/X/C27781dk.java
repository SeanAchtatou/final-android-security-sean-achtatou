package X;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1dk  reason: invalid class name and case insensitive filesystem */
public final class C27781dk {
    public final CopyOnWriteArrayList A00 = new CopyOnWriteArrayList();
    private final C13060qW A01;

    public void A00(Fragment fragment, Context context, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A00(fragment, context, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A01(Fragment fragment, Context context, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A01(fragment, context, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A02(Fragment fragment, Bundle bundle, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A02(fragment, bundle, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A03(Fragment fragment, Bundle bundle, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A03(fragment, bundle, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A04(Fragment fragment, Bundle bundle, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A04(fragment, bundle, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A05(Fragment fragment, Bundle bundle, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A05(fragment, bundle, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A06(Fragment fragment, View view, Bundle bundle, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A06(fragment, view, bundle, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            D6H d6h = (D6H) it.next();
            if (!z || d6h.A00) {
                d6h.A01.A00(this.A01, fragment, view, bundle);
            }
        }
    }

    public void A07(Fragment fragment, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A07(fragment, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A08(Fragment fragment, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A08(fragment, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A09(Fragment fragment, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A09(fragment, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A0A(Fragment fragment, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A0A(fragment, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A0B(Fragment fragment, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A0B(fragment, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A0C(Fragment fragment, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A0C(fragment, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A0D(Fragment fragment, boolean z) {
        Fragment fragment2 = this.A01.A03;
        if (fragment2 != null) {
            fragment2.A18().A0P.A0D(fragment, true);
        }
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public C27781dk(C13060qW r2) {
        this.A01 = r2;
    }
}
