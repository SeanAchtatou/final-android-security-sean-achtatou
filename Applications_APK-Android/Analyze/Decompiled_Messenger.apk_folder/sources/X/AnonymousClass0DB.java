package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.0DB  reason: invalid class name */
public final class AnonymousClass0DB implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0GX A00;

    public AnonymousClass0DB(AnonymousClass0GX r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r11) {
        int A002 = AnonymousClass09Y.A00(1269750396);
        if (intent.getAction().equals("com.facebook.common.appstate.AppStateManager.USER_LEFT_APP")) {
            AnonymousClass0GX r1 = this.A00;
            if (AnonymousClass0GX.A02(r1)) {
                System.runFinalization();
            }
            AnonymousClass0A0 r7 = AnonymousClass08D.A00;
            if (r7 != null && AnonymousClass0GX.A03(r1)) {
                String str = r1.A00;
                char c = 65535;
                int hashCode = str.hashCode();
                if (hashCode != -1739549278) {
                    if (hashCode != -232334244) {
                        if (hashCode == 97332324 && str.equals("CONCURRENT_GC")) {
                            c = 1;
                        }
                    } else if (str.equals("GC_FOR_ALLOC")) {
                        c = 0;
                    }
                } else if (str.equals("COMPLETE_GC")) {
                    c = 2;
                }
                if (c == 0) {
                    r7.manualGcForAlloc();
                } else if (c == 1) {
                    r7.manualGcConcurrent();
                } else if (c == 2) {
                    r7.manualGcComplete();
                }
            }
        }
        AnonymousClass09Y.A01(-766144168, A002);
    }
}
