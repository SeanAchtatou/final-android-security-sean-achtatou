package X;

import java.io.Writer;

/* renamed from: X.01L  reason: invalid class name */
public final class AnonymousClass01L implements AnonymousClass01J {
    private static final long A00;
    private static final long A01;
    private static final long A02;

    static {
        AnonymousClass01M r2 = new AnonymousClass01M();
        A02 = r2.A02;
        A01 = r2.A01;
        A00 = r2.A00;
    }

    public boolean ANg(Writer writer, AnonymousClass0HM r5) {
        writer.append((CharSequence) "\"watermarkMin\":");
        writer.append((CharSequence) Long.toString(A02));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"watermarkLow\":");
        writer.append((CharSequence) Long.toString(A01));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"watermarkHigh\":");
        writer.append((CharSequence) Long.toString(A00));
        return true;
    }
}
