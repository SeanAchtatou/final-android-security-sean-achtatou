package X;

import com.google.common.collect.AbstractMapBasedMultimap;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1Fj  reason: invalid class name and case insensitive filesystem */
public class C21141Fj extends C33131n4<K, Collection<V>> {
    public final /* synthetic */ AbstractMapBasedMultimap A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C21141Fj(AbstractMapBasedMultimap abstractMapBasedMultimap, Map map) {
        super(map);
        this.A00 = abstractMapBasedMultimap;
    }

    public boolean containsAll(Collection collection) {
        return this.A00.keySet().containsAll(collection);
    }

    public boolean equals(Object obj) {
        if (this == obj || this.A00.keySet().equals(obj)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.A00.keySet().hashCode();
    }

    public Iterator iterator() {
        return new C50392du(this, this.A00.entrySet().iterator());
    }

    public boolean remove(Object obj) {
        int i;
        Collection collection = (Collection) this.A00.remove(obj);
        if (collection != null) {
            i = collection.size();
            collection.clear();
            this.A00.A00 -= i;
        } else {
            i = 0;
        }
        if (i > 0) {
            return true;
        }
        return false;
    }

    public void clear() {
        C24931Xr.A08(iterator());
    }
}
