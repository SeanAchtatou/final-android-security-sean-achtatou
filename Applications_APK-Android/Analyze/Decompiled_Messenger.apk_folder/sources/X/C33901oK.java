package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1oK  reason: invalid class name and case insensitive filesystem */
public final class C33901oK extends Enum {
    private static final /* synthetic */ C33901oK[] A00;
    public static final C33901oK A01;
    public static final C33901oK A02;
    public static final C33901oK A03;
    public static final C33901oK A04;
    public static final C33901oK A05;
    public static final C33901oK A06;
    public static final C33901oK A07;
    public static final C33901oK A08;
    public static final C33901oK A09;
    public static final C33901oK A0A;
    public static final C33901oK A0B;
    public static final C33901oK A0C;
    public static final C33901oK A0D;
    public static final C33901oK A0E;
    public static final C33901oK A0F;
    public static final C33901oK A0G;
    public static final C33901oK A0H;
    public static final C33901oK A0I;
    public static final C33901oK A0J;
    public static final C33901oK A0K;
    public static final C33901oK A0L;
    public static final C33901oK A0M;
    public static final C33901oK A0N;
    public static final C33901oK A0O;
    public static final C33901oK A0P;
    public static final C33901oK A0Q;
    public static final C33901oK A0R;
    public static final C33901oK A0S;
    public static final C33901oK A0T;
    public static final C33901oK A0U;
    public static final C33901oK A0V;
    public static final C33901oK A0W;
    public static final C33901oK A0X;
    public static final C33901oK A0Y;
    public static final C33901oK A0Z;
    public static final C33901oK A0a;
    public static final C33901oK A0b;

    static {
        C33901oK r0 = new C33901oK("THREAD", 0);
        A0b = r0;
        C33901oK r02 = new C33901oK("CONVERSATION_STARTER", 1);
        A05 = r02;
        C33901oK r03 = new C33901oK("SECTION_HEADER", 2);
        A0Z = r03;
        C33901oK r04 = new C33901oK("MESSAGE_REQUEST_DISCLAIMER", 3);
        A0K = r04;
        C33901oK r05 = new C33901oK("MESSAGE_REQUEST_HEADER", 4);
        A0L = r05;
        C33901oK r06 = new C33901oK("MESSAGE_REQUEST_THREAD", 5);
        C33901oK r07 = new C33901oK("AGGREGATED_MESSAGE_REQUEST_THREADS", 6);
        C33901oK r12 = new C33901oK("ACTIVE_NOW_PRESENCE_DISABLED_UPSELL", 7);
        A01 = r12;
        C33901oK r3 = new C33901oK("LOAD_MORE_THREADS_PLACEHOLDER", 8);
        A0I = r3;
        C33901oK r2 = new C33901oK("MONTAGE_AND_ACTIVE_NOW", 9);
        A0Q = r2;
        C33901oK r08 = new C33901oK("MONTAGE_COMPOSER_HEADER", 10);
        C33901oK r13 = new C33901oK("MONTAGE_COMPOSER_HEADER_ITEM", 11);
        A0R = r13;
        C33901oK r09 = new C33901oK("MONTAGE_NUX_ITEM", 12);
        C33901oK r010 = new C33901oK("MONTAGE_SNIPPET", 13);
        C33901oK r4 = new C33901oK("BYMM", 14);
        A03 = r4;
        C33901oK r42 = new C33901oK("BYMM_VERTICAL", 15);
        A04 = r42;
        C33901oK r43 = new C33901oK("BUSINESS_VCARD", 16);
        A02 = r43;
        C33901oK r44 = new C33901oK("DISCOVER_VERTICAL_UNIT", 17);
        A0E = r44;
        C33901oK r45 = new C33901oK("DISCOVER_VERTICAL_COMPACT_ITEM", 18);
        A0D = r45;
        C33901oK r46 = new C33901oK("DISCOVER_GRID_UNIT", 19);
        A0B = r46;
        C33901oK r47 = new C33901oK("DISCOVER_GENERIC_ITEM", 20);
        A0A = r47;
        C33901oK r48 = new C33901oK("DISCOVER_GAME_VERTICAL_ITEM_WITH_SUBATTACHMENTS", 21);
        A09 = r48;
        C33901oK r49 = new C33901oK("DISCOVER_GAME_NUX_FOOTER", 22);
        A08 = r49;
        C33901oK r410 = new C33901oK("DISCOVER_GAME_MEDIA_CARD", 23);
        A06 = r410;
        C33901oK r15 = new C33901oK("DISCOVER_GAME_MEDIA_CARD_WITH_TOS", 24);
        A07 = r15;
        C33901oK r14 = new C33901oK("DISCOVER_HORIZONTAL_CARD", 25);
        A0C = r14;
        C33901oK r1 = new C33901oK("MORE_FOOTER", 26);
        A0S = r1;
        C33901oK r5 = new C33901oK("SEE_ALL_FOOTER", 27);
        A0a = r5;
        C33901oK r52 = new C33901oK("CONTACTS_YOU_MAY_KNOW", 28);
        C33901oK r53 = new C33901oK("CONTACTS_YOU_MAY_KNOW_ITEM", 29);
        C33901oK r54 = new C33901oK("HORIZONTAL_TILE_ITEM", 30);
        A0G = r54;
        C33901oK r55 = new C33901oK("HORIZONTAL_TILES_UNIT_ITEM", 31);
        A0F = r55;
        C33901oK r56 = new C33901oK("ROOM_SUGGESTION", 32);
        A0V = r56;
        C33901oK r57 = new C33901oK("ROOM_SUGGESTION_ITEM", 33);
        A0X = r57;
        C33901oK r58 = new C33901oK("ROOM_SUGGESTION_CREATE_ROOM", 34);
        A0W = r58;
        C33901oK r59 = new C33901oK("ROOM_SUGGESTION_SEE_MORE", 35);
        A0Y = r59;
        C33901oK r510 = new C33901oK("GAME_SUGGESTION", 36);
        C33901oK r511 = new C33901oK("MESSENGER_ADS_SINGLE_LARGE_UNIT", 37);
        C33901oK r11 = new C33901oK("MESSENGER_ADS_ITEM", 38);
        A0M = r11;
        C33901oK r512 = new C33901oK("DIRECT_M", 39);
        C33901oK r10 = new C33901oK("MESSENGER_DISCOVERY_CATEGORY", 40);
        A0N = r10;
        C33901oK r513 = new C33901oK("MESSENGER_DISCOVERY_BANNER", 41);
        C33901oK r9 = new C33901oK("MESSENGER_DISCOVERY_LOCATION_UPSELL", 42);
        A0O = r9;
        C33901oK r8 = new C33901oK("MESSENGER_EXTENSION_ITEM", 43);
        A0P = r8;
        C33901oK r7 = new C33901oK("INVITE_COWORKERS", 44);
        A0H = r7;
        C33901oK r16 = new C33901oK("CHAT_SUGGESTION", 45);
        C33901oK r162 = new C33901oK("GROUP_FOR_CHAT_SUGGESTION", 46);
        C33901oK r6 = new C33901oK("NEEDY_USER_SUGGESTED_ITEM", 47);
        A0T = r6;
        C33901oK r514 = new C33901oK("MARKETPLACE_FOLDER", 48);
        A0J = r514;
        C33901oK r542 = new C33901oK("BUSINESS_FOLDER", 49);
        C33901oK r543 = new C33901oK("DIVIDER", 50);
        C33901oK r544 = new C33901oK("PINNED_THREADS_HEADER", 51);
        A0U = r544;
        C33901oK[] r411 = new C33901oK[52];
        System.arraycopy(new C33901oK[]{r0, r02, r03, r04, r05, r06, r07, r12, r3, r2, r08, r13, r09, r010, r4, r42, r43, r44, r45, r46, r47, r48, r49, r410, r15, r14, r1}, 0, r411, 0, 27);
        System.arraycopy(new C33901oK[]{r5, r52, r53, r54, r55, r56, r57, r58, r59, r510, r511, r11, r512, r10, r513, r9, r8, r7, r16, r162, r6, r514, r542, r543, r544}, 0, r411, 27, 25);
        A00 = r411;
    }

    public static C33901oK valueOf(String str) {
        return (C33901oK) Enum.valueOf(C33901oK.class, str);
    }

    public static C33901oK[] values() {
        return (C33901oK[]) A00.clone();
    }

    private C33901oK(String str, int i) {
    }
}
