package X;

import android.content.Context;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0WM  reason: invalid class name */
public final class AnonymousClass0WM extends AnonymousClass0UV {
    public static final AnonymousClass0WP A00(AnonymousClass1XY r5) {
        Object obj;
        Context A00 = AnonymousClass1YA.A00(r5);
        C001500z A05 = AnonymousClass0UU.A05(r5);
        AnonymousClass0VB A002 = AnonymousClass0VB.A00(AnonymousClass1Y3.BKQ, r5);
        AnonymousClass0VB A003 = AnonymousClass0VB.A00(AnonymousClass1Y3.BMa, r5);
        if (AnonymousClass0Y5.A00(A00, A05).A02) {
            obj = A003.get();
        } else {
            obj = A002.get();
        }
        return (AnonymousClass0WP) obj;
    }

    public static final AnonymousClass0YZ A01(AnonymousClass1XY r5) {
        Object obj;
        Context A00 = AnonymousClass1YA.A00(r5);
        C001500z A05 = AnonymousClass0UU.A05(r5);
        AnonymousClass0VB A002 = AnonymousClass0VB.A00(AnonymousClass1Y3.BKQ, r5);
        AnonymousClass0VB A003 = AnonymousClass0VB.A00(AnonymousClass1Y3.BMa, r5);
        if (AnonymousClass0Y5.A00(A00, A05).A02) {
            obj = A003.get();
        } else {
            obj = A002.get();
        }
        return (AnonymousClass0YZ) ((AnonymousClass0WP) obj);
    }
}
