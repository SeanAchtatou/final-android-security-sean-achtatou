package X;

import android.content.Context;
import android.text.Layout;
import android.text.TextUtils;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1L2  reason: invalid class name */
public final class AnonymousClass1L2 extends C17770zR {
    public static final Layout.Alignment A0G = Layout.Alignment.ALIGN_NORMAL;
    public static final TextUtils.TruncateAt A0H = TextUtils.TruncateAt.END;
    public static final C22311Kv A0I = C22181Kf.A04;
    public static final C14920uM A0J = C14920uM.TOP;
    @Comparable(type = 0)
    public float A00;
    @Comparable(type = 0)
    public float A01 = 1.0f;
    @Comparable(type = 3)
    public int A02;
    @Comparable(type = 3)
    public int A03 = Integer.MAX_VALUE;
    @Comparable(type = 13)
    public Layout.Alignment A04 = A0G;
    @Comparable(type = 13)
    public TextUtils.TruncateAt A05 = A0H;
    @Comparable(type = 13)
    public C22311Kv A06 = A0I;
    public AnonymousClass0UN A07;
    @Comparable(type = 13)
    public C14920uM A08 = A0J;
    @Comparable(type = 13)
    public C16930y3 A09;
    @Comparable(type = 13)
    public AnonymousClass1JL A0A;
    @Comparable(type = 13)
    public AnonymousClass10M A0B;
    @Comparable(type = 13)
    public CharSequence A0C;
    @Comparable(type = 3)
    public boolean A0D;
    @Comparable(type = 3)
    public boolean A0E;
    @Comparable(type = 3)
    public boolean A0F;

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r4) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(9);
        ComponentBuilderCBuilderShape0_0S0300000.A09(componentBuilderCBuilderShape0_0S0300000, r4, 0, 0, new AnonymousClass1L2(r4.A09));
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public AnonymousClass1L2(Context context) {
        super("MigConfigurableText");
        this.A07 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
