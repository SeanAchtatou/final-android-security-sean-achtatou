package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphql.executor.GraphQLResult;

/* renamed from: X.0mI  reason: invalid class name */
public final class AnonymousClass0mI implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new GraphQLResult(parcel);
    }

    public Object[] newArray(int i) {
        return new GraphQLResult[i];
    }
}
