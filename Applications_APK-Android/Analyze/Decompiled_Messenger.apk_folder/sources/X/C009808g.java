package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.08g  reason: invalid class name and case insensitive filesystem */
public final class C009808g {
    public final Map A00;

    public C009808g() {
        this.A00 = Collections.synchronizedMap(new HashMap());
    }

    public C009808g(C009808g r3) {
        Map synchronizedMap = Collections.synchronizedMap(new HashMap());
        this.A00 = synchronizedMap;
        synchronizedMap.putAll(r3.A00);
    }
}
