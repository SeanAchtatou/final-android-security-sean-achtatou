package X;

import android.view.animation.LinearInterpolator;
import com.facebook.common.asyncview.SpriteView;

/* renamed from: X.0SQ  reason: invalid class name */
public final class AnonymousClass0SQ extends C03550Om {
    public final /* synthetic */ float A00;
    public final /* synthetic */ AnonymousClass01E A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0SQ(AnonymousClass01E r2, long j, float f) {
        super(new LinearInterpolator(), j);
        this.A01 = r2;
        this.A00 = f;
    }

    public void A00(float f) {
        SpriteView.Sprite sprite = this.A01.A04;
        float f2 = -this.A00;
        sprite.setTranslationX(((0.0f - f2) * f) + f2);
        SpriteView.Sprite sprite2 = this.A01.A06;
        float f3 = this.A00;
        sprite2.setTranslationX(((0.0f - f3) * f) + f3);
    }
}
