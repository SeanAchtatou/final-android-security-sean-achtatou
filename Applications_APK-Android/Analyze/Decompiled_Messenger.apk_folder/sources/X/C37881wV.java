package X;

import com.facebook.resources.impl.DrawableCounterLogger;
import java.util.concurrent.Callable;

/* renamed from: X.1wV  reason: invalid class name and case insensitive filesystem */
public final class C37881wV implements Callable {
    public final /* synthetic */ DrawableCounterLogger A00;
    public final /* synthetic */ int[] A01;
    public final /* synthetic */ int[] A02;
    public final /* synthetic */ AnonymousClass15L[] A03;

    public C37881wV(DrawableCounterLogger drawableCounterLogger, int[] iArr, AnonymousClass15L[] r3, int[] iArr2) {
        this.A00 = drawableCounterLogger;
        this.A01 = iArr;
        this.A03 = r3;
        this.A02 = iArr2;
    }

    public Object call() {
        String r0;
        int[] iArr = this.A01;
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        while (true) {
            AnonymousClass15L r12 = null;
            if (i >= length) {
                return null;
            }
            int i3 = iArr[i];
            AnonymousClass15L[] r1 = this.A03;
            if (i2 < r1.length) {
                r12 = r1[i2];
                int[] iArr2 = this.A02;
                int i4 = iArr2[i2] - 1;
                iArr2[i2] = i4;
                if (i4 <= 0) {
                    i2++;
                }
            }
            int i5 = AnonymousClass1Y3.BQ3;
            DrawableCounterLogger drawableCounterLogger = this.A00;
            AnonymousClass0UN r13 = drawableCounterLogger.A00;
            C07380dK r3 = (C07380dK) AnonymousClass1XX.A02(2, i5, r13);
            StringBuilder sb = new StringBuilder("android_drawable.");
            sb.append(((C05760aH) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZY, r13)).getResourcePackageName(i3));
            sb.append('(');
            if (r12 == null) {
                r0 = "null";
            } else {
                r0 = r12.toString();
            }
            sb.append(r0);
            sb.append(')');
            sb.append(':');
            int i6 = AnonymousClass1Y3.AZY;
            sb.append(((C05760aH) AnonymousClass1XX.A02(3, i6, drawableCounterLogger.A00)).getResourceTypeName(i3));
            sb.append(':');
            sb.append(((C05760aH) AnonymousClass1XX.A02(3, i6, drawableCounterLogger.A00)).getResourceEntryName(i3));
            r3.A03(sb.toString());
            i++;
        }
    }
}
