package X;

import android.net.Uri;
import com.facebook.common.callercontext.CallerContext;
import java.io.IOException;

/* renamed from: X.0ii  reason: invalid class name and case insensitive filesystem */
public abstract class C09800ii {
    public final AnonymousClass0iW A00;
    public volatile AnonymousClass0j7 A01;

    public static C22371Lb A01(C09800ii r4, AnonymousClass1LI r5, boolean z) {
        if (!z && AnonymousClass1LJ.A00.contains(r5)) {
            return null;
        }
        boolean z2 = false;
        if (r4.A00.A09()) {
            z2 = true;
        }
        if (!z2) {
            r4.A00.A0A(-2147483640);
            return null;
        } else if (z) {
            return A00(r4, r5);
        } else {
            AnonymousClass1LJ.A00.add(r5);
            AnonymousClass07A.A04(r4.A03(), new C25823Cmo(r4, r5), 1353336186);
            return null;
        }
    }

    public C22371Lb A02(Uri uri, AnonymousClass1LI r7) {
        C09790ih r4 = (C09790ih) this;
        return (C22371Lb) ((AId) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AqU, r4.A00)).A02(new C191208vC(uri, new C25821Cmm(r4, r7), CallerContext.A04(C09790ih.class)));
    }

    public AnonymousClass0VL A03() {
        return (AnonymousClass0VL) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AZx, ((C09790ih) this).A00);
    }

    public static C22371Lb A00(C09800ii r1, AnonymousClass1LI r2) {
        try {
            return r1.A02(Uri.parse(r2.A02), r2);
        } catch (IOException unused) {
            return null;
        }
    }

    public C09800ii(AnonymousClass0iW r2) {
        this.A00 = r2;
        r2.A0A(0);
    }
}
