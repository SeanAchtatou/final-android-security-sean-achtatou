package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0sx  reason: invalid class name and case insensitive filesystem */
public final class C14280sx extends AnonymousClass0UV {
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0037, code lost:
        if ("enabled".equals(r1) == false) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A00(X.AnonymousClass1XY r3) {
        /*
            X.1YI r1 = X.AnonymousClass0WA.A00(r3)
            com.facebook.prefs.shared.FbSharedPreferences r2 = com.facebook.prefs.shared.FbSharedPreferencesModule.A00(r3)
            X.155 r3 = X.AnonymousClass155.A01(r3)
            r0 = 582(0x246, float:8.16E-43)
            com.facebook.common.util.TriState r1 = r1.Ab9(r0)
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            if (r1 != r0) goto L_0x003f
            boolean r0 = r2.BFQ()
            if (r0 == 0) goto L_0x003f
            java.lang.String r2 = r3.A08()
            X.157 r1 = X.AnonymousClass155.A02(r3)
            java.lang.String r0 = ""
            java.lang.String r1 = r3.A0H(r1, r0)
            boolean r0 = com.facebook.zero.common.ZeroToken.A00(r2)
            if (r0 == 0) goto L_0x0039
            java.lang.String r0 = "enabled"
            boolean r1 = r0.equals(r1)
            r0 = 1
            if (r1 != 0) goto L_0x003a
        L_0x0039:
            r0 = 0
        L_0x003a:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        L_0x003f:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14280sx.A00(X.1XY):java.lang.Boolean");
    }
}
