package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eL  reason: invalid class name and case insensitive filesystem */
public final class C07890eL extends C07880eK {
    private static volatile C07890eL A00;

    public static final C07890eL A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (C07890eL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new C07890eL(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private C07890eL(AnonymousClass1XY r5) {
        AnonymousClass0UN r3 = new AnonymousClass0UN(2, r5);
        A06((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r3), (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r3));
    }
}
