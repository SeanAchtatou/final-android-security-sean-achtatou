package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.1mM  reason: invalid class name and case insensitive filesystem */
public final class C32801mM implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public C32801mM(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(1848696172);
        C33741o4 r3 = this.A00;
        C193617v r0 = r3.A03;
        if (r0 != null) {
            r0.Btb(false, false, "inbox2 changed");
            r3.A03.BtU("inbox2 changed");
        }
        AnonymousClass09Y.A01(-1228806406, A002);
    }
}
