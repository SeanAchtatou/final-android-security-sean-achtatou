package X;

import com.facebook.common.perftest.PerfTestConfig;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fW  reason: invalid class name and case insensitive filesystem */
public final class C08540fW extends C08550fX {
    private static volatile C08540fW A01;
    public final ExecutorService A00;

    public static final C08540fW A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C08540fW.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C08540fW(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C08540fW(AnonymousClass1XY r2) {
        PerfTestConfig.A01(r2);
        this.A00 = AnonymousClass0UX.A0a(r2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000a, code lost:
        if (X.AnonymousClass0iY.A00 != false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
            r2 = this;
            super.A02()
            boolean r0 = r2.A03
            if (r0 != 0) goto L_0x000c
            boolean r1 = X.AnonymousClass0iY.A00
            r0 = 0
            if (r1 == 0) goto L_0x000d
        L_0x000c:
            r0 = 1
        L_0x000d:
            r2.A03 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08540fW.A02():void");
    }
}
