package X;

import com.google.common.base.Optional;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1qS  reason: invalid class name and case insensitive filesystem */
public final class C34961qS extends C34971qT {
    private static volatile C34961qS A03;
    public AnonymousClass0UN A00;
    public Optional A01;
    public final C25051Yd A02;

    public String getName() {
        return "RtcAppLogger";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    private C34961qS(AnonymousClass1XY r3) {
        super("rtc_app_log.txt");
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A02 = AnonymousClass0WT.A00(r3);
    }

    public static final C34961qS A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C34961qS.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C34961qS(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }
}
