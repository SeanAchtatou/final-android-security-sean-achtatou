package X;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.common.base.Preconditions;
import com.google.common.collect.RegularImmutableList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.0eP  reason: invalid class name and case insensitive filesystem */
public final class C07930eP implements AnonymousClass1YQ, C05460Za {
    public Intent A00;
    public Handler A01;
    private Messenger A02;
    public final C07380dK A03;
    public final C06890cF A04;
    public final C04460Ut A05;
    public final AnonymousClass09P A06;
    public final C06130au A07;
    public final Runnable A08 = new C06780c4(this);
    public final String A09;
    public final ConcurrentMap A0A = AnonymousClass0TG.A07();
    public final ConcurrentMap A0B = AnonymousClass0TG.A07();
    public final ConcurrentMap A0C = AnonymousClass0TG.A07();
    public final C04310Tq A0D;
    public final boolean A0E;
    private final HandlerThread A0F;
    private final C04910Wu A0G;
    public volatile C414625o A0H;

    public String getSimpleName() {
        return "PeerProcessManagerImpl";
    }

    public static C06130au A00(C07930eP r8, Message message) {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo;
        AnonymousClass00M r0;
        int i = message.arg1;
        C06130au r3 = (C06130au) r8.A0B.get(Integer.valueOf(i));
        if (r3 == null && r8.A0E) {
            Integer valueOf = Integer.valueOf(i);
            ConcurrentMap concurrentMap = r8.A0B;
            String str = r8.A09;
            List runningAppProcesses = r8.A0G.A00.getRunningAppProcesses();
            if (runningAppProcesses == null) {
                runningAppProcesses = RegularImmutableList.A02;
            }
            Iterator it = runningAppProcesses.iterator();
            while (true) {
                if (!it.hasNext()) {
                    runningAppProcessInfo = null;
                    break;
                }
                runningAppProcessInfo = (ActivityManager.RunningAppProcessInfo) it.next();
                if (runningAppProcessInfo.pid == i) {
                    break;
                }
            }
            if (runningAppProcessInfo == null || TextUtils.isEmpty(runningAppProcessInfo.processName)) {
                r0 = new AnonymousClass00M();
            } else {
                r0 = AnonymousClass00M.A01(runningAppProcessInfo.processName);
            }
            C010708t.A0P("PeerProcessManagerImpl", "Message from unknown process: %d, probably the message's arg1 is not set to the pid of source process. Message details: %s, peer infos: %s, ActionName: %s, ProcessName: %s", valueOf, message, concurrentMap, str, r0.toString());
        }
        return r3;
    }

    public static String A01(C07930eP r6) {
        String str = (String) r6.A0D.get();
        if (str != null) {
            r6.A00.putExtra("__KEY_LOGGED_USER_ID__", str);
            A02(r6);
            AnonymousClass00S.A05(r6.A01, r6.A08, 1000, -737153025);
        }
        return str;
    }

    public static void A02(C07930eP r5) {
        try {
            r5.A05.C4x(r5.A00);
        } catch (Exception e) {
            AnonymousClass09P r3 = r5.A06;
            r3.softReport("PeerProcessManagerImpl", "Exception occurred when sending peer init intent; peer info: " + r5.A07 + "; intent: " + r5.A00, e);
        }
    }

    public static void A03(C07930eP r2, C06130au r3) {
        if (r2.A0B.remove(Integer.valueOf(r3.A01)) != null) {
            for (C07420dS BiG : r2.A0C.keySet()) {
                BiG.BiG(r3);
            }
        }
    }

    public static void A04(C07930eP r3, C06130au r4, Integer num) {
        boolean z;
        r3.A0B.put(Integer.valueOf(r4.A01), r4);
        for (C07420dS BiF : r3.A0C.keySet()) {
            BiF.BiF(r4, num);
        }
        try {
            Preconditions.checkNotNull(r4);
            r4.A00.getBinder().linkToDeath(new C10600kW(r3, r4), 0);
            z = true;
        } catch (RemoteException unused) {
            z = false;
        }
        if (!z) {
            A03(r3, r4);
        }
    }

    public void A06(Message message) {
        if (!this.A0B.isEmpty()) {
            message.arg1 = this.A07.A01;
            AnonymousClass00S.A04(this.A01, new AnonymousClass188(this, message), -390361240);
        }
    }

    public void clearUserData() {
        if (this.A0E) {
            A06(Message.obtain((Handler) null, 1));
            this.A0B.clear();
            AnonymousClass00S.A04(this.A01, new C07490de(this), 65254181);
        }
    }

    public C07930eP(String str, C04460Ut r6, C04310Tq r7, C04910Wu r8, AnonymousClass09P r9, C06890cF r10, HandlerThread handlerThread, C04310Tq r12, boolean z, C07380dK r14) {
        this.A09 = str;
        this.A05 = r6;
        this.A0G = r8;
        this.A06 = r9;
        this.A04 = r10;
        this.A0F = handlerThread;
        this.A0D = r12;
        this.A0E = z;
        this.A03 = r14;
        this.A07 = new C06130au(null, ((Integer) r7.get()).intValue(), AnonymousClass00M.A00());
    }

    public void A05(int i, C06150aw r5) {
        Preconditions.checkNotNull(r5);
        ConcurrentMap concurrentMap = this.A0A;
        Integer valueOf = Integer.valueOf(i);
        if (!concurrentMap.containsKey(valueOf)) {
            this.A0A.put(valueOf, r5);
            return;
        }
        throw new IllegalStateException(AnonymousClass08S.A0A("The listener for message type ", i, " has already registered"));
    }

    public void init() {
        int A032 = C000700l.A03(-883894635);
        this.A02 = new Messenger(new C07060cZ(this, this.A0F.getLooper()));
        this.A01 = new Handler(this.A0F.getLooper());
        this.A07.A00 = this.A02;
        C06600bl BMm = this.A05.BMm();
        BMm.A02(this.A09, new C07460db(this));
        BMm.A01(this.A01);
        BMm.A00().A00();
        Intent intent = new Intent(this.A09);
        this.A00 = intent;
        intent.putExtra("peer_info", this.A07.A01());
        AnonymousClass00S.A04(this.A01, new C07490de(this), 65254181);
        C000700l.A09(1868955547, A032);
    }
}
