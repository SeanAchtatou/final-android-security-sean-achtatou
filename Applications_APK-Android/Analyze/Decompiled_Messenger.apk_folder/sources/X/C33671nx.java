package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1nx  reason: invalid class name and case insensitive filesystem */
public final class C33671nx {
    private static volatile C33671nx A01;
    public final C25051Yd A00;

    public static final C33671nx A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C33671nx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C33671nx(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C33671nx(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
