package X;

import android.content.Context;
import android.telephony.SubscriptionManager;

/* renamed from: X.1k1  reason: invalid class name and case insensitive filesystem */
public final class C31641k1 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.hardware.CellDiagnosticsSerializer$1";
    public final /* synthetic */ C31631k0 A00;

    public C31641k1(C31631k0 r1) {
        this.A00 = r1;
    }

    public void run() {
        C31631k0 r3 = this.A00;
        r3.A00 = SubscriptionManager.from((Context) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BCt, r3.A02));
        SubscriptionManager subscriptionManager = this.A00.A00;
        if (subscriptionManager != null) {
            subscriptionManager.addOnSubscriptionsChangedListener(new A83(this));
            this.A00.A08 = true;
        }
    }
}
