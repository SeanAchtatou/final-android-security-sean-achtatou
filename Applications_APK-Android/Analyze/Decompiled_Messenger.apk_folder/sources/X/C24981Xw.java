package X;

import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: X.1Xw  reason: invalid class name and case insensitive filesystem */
public abstract class C24981Xw extends C24991Xx {
    public int A00 = 0;
    public boolean A01;
    public Object[] A02;

    private void A00(int i) {
        Object[] objArr = this.A02;
        int length = objArr.length;
        if (length < i) {
            this.A02 = Arrays.copyOf(objArr, C24991Xx.A01(length, i));
            this.A01 = false;
        } else if (this.A01) {
            this.A02 = (Object[]) objArr.clone();
            this.A01 = false;
        }
    }

    public C24991Xx addAll(Iterable iterable) {
        if (iterable instanceof Collection) {
            A00(this.A00 + ((Collection) iterable).size());
        }
        super.addAll(iterable);
        return this;
    }

    public C24981Xw(int i) {
        C25001Xy.A00(i, "initialCapacity");
        this.A02 = new Object[i];
    }

    public C24981Xw add(Object obj) {
        Preconditions.checkNotNull(obj);
        A00(this.A00 + 1);
        Object[] objArr = this.A02;
        int i = this.A00;
        this.A00 = i + 1;
        objArr[i] = obj;
        return this;
    }

    public C24991Xx add(Object... objArr) {
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            AnonymousClass0U8.A00(objArr[i], i);
        }
        A00(this.A00 + length);
        System.arraycopy(objArr, 0, this.A02, this.A00, length);
        this.A00 += length;
        return this;
    }
}
