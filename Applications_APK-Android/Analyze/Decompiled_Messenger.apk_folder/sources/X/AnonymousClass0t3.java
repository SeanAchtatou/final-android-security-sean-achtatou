package X;

import com.google.common.base.MoreObjects;

/* renamed from: X.0t3  reason: invalid class name */
public final class AnonymousClass0t3 {
    public int A00 = -1;
    public int A01 = -1;
    public long A02 = -1;
    public long A03 = -1;
    public long A04 = -1;
    public C13430rQ A05 = C13430rQ.TP_DISABLED;

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("lastUpdateTimestampMs", this.A04);
        stringHelper.add("lastFullUpdateTimestampMs", this.A02);
        stringHelper.add("lastFullUpdateSize", this.A00);
        stringHelper.add("lastMqttDisconnect", this.A03);
        stringHelper.add("lastPresenceFullListDownloadState", this.A05);
        stringHelper.add("numUsersOnline", this.A01);
        return stringHelper.toString();
    }
}
