package X;

import android.content.Context;
import android.content.DialogInterface;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.0rL  reason: invalid class name and case insensitive filesystem */
public final class C13380rL implements DialogInterface.OnClickListener {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C13210qu A01;

    public C13380rL(C13210qu r1, Context context) {
        this.A01 = r1;
        this.A00 = context;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        ((AnonymousClass3N2) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BAD, this.A01.A00)).A05(this.A00, TurboLoader.Locator.$const$string(17));
    }
}
