package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.auth.viewercontext.ViewerContext;

@UserScoped
/* renamed from: X.1Eb  reason: invalid class name and case insensitive filesystem */
public final class C20851Eb extends C189316d {
    private static C05540Zi A02;
    public final AnonymousClass1Y6 A00;
    public final C04310Tq A01;

    public static final C20851Eb A01(AnonymousClass1XY r5) {
        C20851Eb r0;
        synchronized (C20851Eb.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r3 = (AnonymousClass1XY) A02.A01();
                    A02.A00 = new C20851Eb(r3, AnonymousClass1YA.A00(r3));
                }
                C05540Zi r1 = A02;
                r0 = (C20851Eb) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public void A02(Intent intent, Context context) {
        ViewerContext B9S = ((C05920aY) this.A01.get()).B9S();
        if (B9S != null) {
            if (!this.A00.BHM()) {
                intent = new Intent(intent);
            }
            intent.putExtra("overridden_viewer_context", B9S);
        }
        super.A02(intent, context);
    }

    private C20851Eb(AnonymousClass1XY r2, Context context) {
        super(context.getPackageName());
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.BJQ, r2);
        this.A00 = AnonymousClass0UX.A07(r2);
    }
}
