package X;

/* renamed from: X.0YM  reason: invalid class name */
public final class AnonymousClass0YM implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.init.impl.FbAppInitializerInternal$6";
    public final /* synthetic */ C04830Wi A00;
    public final /* synthetic */ AnonymousClass0Y7 A01;
    public final /* synthetic */ String A02;

    public AnonymousClass0YM(AnonymousClass0Y7 r1, C04830Wi r2, String str) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = str;
    }

    public void run() {
        AnonymousClass1Z5 BLq = this.A00.BLq();
        if (BLq != null) {
            this.A01.A08(BLq, this.A02);
        }
    }
}
