package X;

import com.facebook.quicklog.PerformanceLoggingEvent;

/* renamed from: X.0fA  reason: invalid class name and case insensitive filesystem */
public final class C08320fA extends AnonymousClass0f8 {
    private static boolean A00 = true;
    public static final String[] A01 = {"Rss:", "Pss:", "Shared_Clean:", "Shared_Dirty:", "Private_Clean:", "Private_Dirty:", "Anonymous:", "Swap:", "SwapPss:"};
    public static final String[] A02 = {"VmRSS:", "RssAnon:", "VmSwap:"};

    public String Azg() {
        return "detailed_memory_stats";
    }

    public static final C08320fA A00() {
        return new C08320fA();
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        C30401Evc evc = (C30401Evc) obj;
        C30401Evc evc2 = (C30401Evc) obj2;
        if (performanceLoggingEvent.A0F == null) {
            if (evc != null) {
                performanceLoggingEvent.A06("pss_at_start", evc.A03);
                performanceLoggingEvent.A06("rss_at_start", evc.A04);
                performanceLoggingEvent.A06("anonymous_rss_at_start", evc.A00);
                performanceLoggingEvent.A06("dirty_pss_at_start", evc.A01);
                performanceLoggingEvent.A06("private_dirty_at_start", evc.A02);
            }
            if (evc2 != null) {
                performanceLoggingEvent.A06("pss_used", evc2.A03);
                performanceLoggingEvent.A06("rss_used", evc2.A04);
                performanceLoggingEvent.A06("anonymous_rss_used", evc2.A00);
                performanceLoggingEvent.A06("dirty_pss_used", evc2.A01);
                performanceLoggingEvent.A06("private_dirty_used", evc2.A02);
            }
        }
    }

    public long Azh() {
        return C08350fD.A03;
    }

    public Class B3O() {
        return C30401Evc.class;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0042, code lost:
        if (r0 != false) goto L_0x006c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object CGO() {
        /*
            r12 = this;
            X.Evc r10 = new X.Evc
            r10.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "/proc/"
            r1.<init>(r0)
            int r0 = android.os.Process.myPid()
            r1.append(r0)
            r0 = 47
            r1.append(r0)
            java.lang.String r11 = r1.toString()
            boolean r0 = X.C08320fA.A00
            if (r0 == 0) goto L_0x0044
            java.lang.String r0 = "smaps_rollup"
            java.lang.String r2 = X.AnonymousClass08S.A0J(r11, r0)
            java.lang.String[] r1 = X.C08320fA.A01
            int r0 = r1.length
            long[] r5 = new long[r0]
            boolean r0 = X.AnonymousClass00V.A02(r2, r1, r5)
            r6 = 0
            if (r0 == 0) goto L_0x0041
            r0 = 5
            r8 = r5[r0]
            r3 = 0
            int r0 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x006d
            r1 = r5[r6]
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x006d
        L_0x0041:
            r0 = 0
        L_0x0042:
            if (r0 != 0) goto L_0x006c
        L_0x0044:
            r4 = 0
            X.C08320fA.A00 = r4
            java.lang.String r0 = "status"
            java.lang.String r2 = X.AnonymousClass08S.A0J(r11, r0)
            java.lang.String[] r1 = X.C08320fA.A02
            int r0 = r1.length
            long[] r3 = new long[r0]
            boolean r0 = X.AnonymousClass00V.A02(r2, r1, r3)
            if (r0 == 0) goto L_0x006c
            r1 = r3[r4]
            r0 = 2
            r5 = r3[r0]
            long r1 = r1 + r5
            r10.A04 = r1
            r0 = 1
            r3 = r3[r0]
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x006c
            long r3 = r3 + r5
            r10.A00 = r3
        L_0x006c:
            return r10
        L_0x006d:
            r0 = r5[r6]
            r2 = 7
            r6 = r5[r2]
            long r0 = r0 + r6
            r10.A04 = r0
            r0 = 1
            r0 = r5[r0]
            r2 = 8
            r2 = r5[r2]
            long r0 = r0 + r2
            r10.A03 = r0
            r2 = 6
            r2 = r5[r2]
            long r2 = r2 + r6
            r10.A00 = r2
            r2 = 2
            r2 = r5[r2]
            r4 = 3
            r6 = r5[r4]
            long r2 = r2 + r6
            r4 = 4
            r4 = r5[r4]
            long r4 = r4 + r8
            long r0 = r0 - r4
            double r4 = (double) r0
            double r0 = (double) r2
            double r4 = r4 / r0
            double r2 = (double) r8
            double r0 = (double) r6
            double r0 = r0 * r4
            double r2 = r2 + r0
            long r0 = (long) r2
            r10.A01 = r0
            r10.A02 = r8
            r0 = 1
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08320fA.CGO():java.lang.Object");
    }

    public boolean BEZ(C08360fE r2) {
        return true;
    }
}
