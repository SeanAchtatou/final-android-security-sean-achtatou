package X;

import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.14O  reason: invalid class name */
public final class AnonymousClass14O extends C14680to {
    public static final AnonymousClass14O[] CANONICALS;
    public final int _value;

    public boolean canConvertToInt() {
        return true;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            return obj != null && obj.getClass() == getClass() && ((AnonymousClass14O) obj)._value == this._value;
        }
        return true;
    }

    public boolean isInt() {
        return true;
    }

    static {
        AnonymousClass14O[] r3 = new AnonymousClass14O[12];
        CANONICALS = r3;
        for (int i = 0; i < 12; i++) {
            r3[i] = new AnonymousClass14O(i - 1);
        }
    }

    public boolean asBoolean(boolean z) {
        if (this._value != 0) {
            return true;
        }
        return false;
    }

    public String asText() {
        return C48502aX.toString(this._value);
    }

    public C182811d asToken() {
        return C182811d.VALUE_NUMBER_INT;
    }

    public BigInteger bigIntegerValue() {
        return BigInteger.valueOf((long) this._value);
    }

    public BigDecimal decimalValue() {
        return BigDecimal.valueOf((long) this._value);
    }

    public double doubleValue() {
        return (double) this._value;
    }

    public float floatValue() {
        return (float) this._value;
    }

    public int hashCode() {
        return this._value;
    }

    public int intValue() {
        return this._value;
    }

    public long longValue() {
        return (long) this._value;
    }

    public C29501gW numberType() {
        return C29501gW.INT;
    }

    public Number numberValue() {
        return Integer.valueOf(this._value);
    }

    public final void serialize(C11710np r2, C11260mU r3) {
        r2.writeNumber(this._value);
    }

    public AnonymousClass14O(int i) {
        this._value = i;
    }
}
