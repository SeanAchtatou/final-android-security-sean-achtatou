package X;

import android.widget.BaseAdapter;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.0QL  reason: invalid class name */
public final class AnonymousClass0QL {
    public static void A00(BaseAdapter baseAdapter, int i) {
        Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 20, 0, 0, i, 0, 0);
        baseAdapter.notifyDataSetChanged();
    }

    public static void A01(BaseAdapter baseAdapter, int i) {
        Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 20, 0, 0, i, 0, 0);
        baseAdapter.notifyDataSetInvalidated();
    }
}
