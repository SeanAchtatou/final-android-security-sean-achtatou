package X;

import java.util.Locale;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1mG  reason: invalid class name and case insensitive filesystem */
public final class C32741mG extends Enum {
    public static final C32741mG A00 = new C32741mG("ARCHIVED", 8);
    public static final C32741mG A01 = new C32741mG("HIGHLIGHT", 4);
    public static final C32741mG A02 = new C32741mG("INSTAGRAM", 9);
    public static final C32741mG A03 = new C32741mG("NEWSFEED", 5);
    public static final C32741mG A04 = new C32741mG("USER", 7);

    static {
        new C32741mG("BIRTHDAY", 0);
        new C32741mG("EVENT", 1);
        new C32741mG("GOODWILL", 2);
        new C32741mG("GROUP", 3);
        new C32741mG("PAGE", 6);
    }

    public static boolean A00(String str) {
        if (str == null || !str.equalsIgnoreCase(A00.name())) {
            return false;
        }
        return true;
    }

    public static boolean A01(String str) {
        if (str == null || !str.equalsIgnoreCase(A03.name())) {
            return false;
        }
        return true;
    }

    public static boolean A02(String str) {
        if (str == null || str.equalsIgnoreCase(A04.name()) || str.equalsIgnoreCase(A03.name()) || str.equalsIgnoreCase(A00.name())) {
            return true;
        }
        return false;
    }

    private C32741mG(String str, int i) {
    }

    public String toString() {
        return super.toString().toLowerCase(Locale.US);
    }
}
