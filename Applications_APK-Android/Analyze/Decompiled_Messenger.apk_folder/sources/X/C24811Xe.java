package X;

import android.content.Context;
import com.google.common.base.Preconditions;
import java.util.List;

/* renamed from: X.1Xe  reason: invalid class name and case insensitive filesystem */
public final class C24811Xe {
    public final Context A00;
    public final List A01 = C04300To.A00();
    public final List A02 = C04300To.A00();

    public Context A00() {
        if (this.A01.isEmpty()) {
            return this.A00;
        }
        List list = this.A01;
        return (Context) list.get(list.size() - 1);
    }

    public C24851Xi A01() {
        if (this.A02.isEmpty()) {
            return null;
        }
        List list = this.A02;
        return (C24851Xi) list.get(list.size() - 1);
    }

    public void A02() {
        Preconditions.checkState(!this.A01.isEmpty());
        List list = this.A01;
        list.remove(list.size() - 1);
    }

    public void A03() {
        Preconditions.checkState(!this.A02.isEmpty());
        List list = this.A02;
        list.remove(list.size() - 1);
    }

    public C24811Xe(Context context) {
        this.A00 = context;
    }
}
