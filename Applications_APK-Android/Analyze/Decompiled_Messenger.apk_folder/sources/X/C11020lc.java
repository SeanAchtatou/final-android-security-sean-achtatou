package X;

import com.facebook.fbservice.service.OperationResult;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.0lc  reason: invalid class name and case insensitive filesystem */
public final class C11020lc {
    public long A00;
    public long A01;
    public C11030ld A02;
    public OperationResult A03;
    public ImmutableList A04;
    public ListenableFuture A05;
    public List A06 = C04300To.A00();
    public boolean A07;
    public final long A08;
    public final C11010lb A09;
    public final C16890xw A0A;

    public C11020lc(C11010lb r6, C16890xw r7, long j, LinkedList linkedList) {
        this.A09 = r6;
        this.A0A = r7;
        this.A08 = j;
        this.A07 = false;
        ImmutableList.Builder builder = new ImmutableList.Builder();
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            C11010lb r2 = (C11010lb) it.next();
            builder.add((Object) (r2.A04 + "-" + r2.A02));
        }
        this.A04 = builder.build();
    }
}
