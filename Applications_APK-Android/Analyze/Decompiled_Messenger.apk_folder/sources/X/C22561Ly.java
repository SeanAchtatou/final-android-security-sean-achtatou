package X;

import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;

/* renamed from: X.1Ly  reason: invalid class name and case insensitive filesystem */
public final class C22561Ly {
    public static int A00(Layout layout) {
        int i = 0;
        if (layout == null) {
            return 0;
        }
        if (Build.VERSION.SDK_INT < 20 && (layout instanceof StaticLayout)) {
            int max = Math.max(0, layout.getLineCount() - 1);
            float lineDescent = (float) (layout.getLineDescent(max) - layout.getLineAscent(max));
            float spacingAdd = lineDescent - ((lineDescent - layout.getSpacingAdd()) / layout.getSpacingMultiplier());
            if (spacingAdd >= 0.0f) {
                i = (int) (((double) spacingAdd) + 0.5d);
            } else {
                i = -((int) (((double) (-spacingAdd)) + 0.5d));
            }
        }
        return layout.getHeight() - i;
    }

    public static int A01(Layout layout) {
        if (layout == null) {
            return 0;
        }
        int lineCount = layout.getLineCount();
        int i = 0;
        for (int i2 = 0; i2 < lineCount; i2++) {
            i = Math.max(i, (int) layout.getLineRight(i2));
        }
        return i;
    }
}
