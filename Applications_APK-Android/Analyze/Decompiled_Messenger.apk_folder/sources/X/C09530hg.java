package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0hg  reason: invalid class name and case insensitive filesystem */
public final class C09530hg extends AnonymousClass0W1 {
    public C09530hg() {
        super("units", 9, ImmutableList.of(new C09540hi(), new C06040ak()));
    }

    public static final C09530hg A00() {
        return new C09530hg();
    }

    public void A08(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        while (i < i2) {
            String A00 = AnonymousClass0W4.A00("properties");
            C007406x.A00(-1889933480);
            sQLiteDatabase.execSQL(A00);
            C007406x.A00(-758415459);
            String A002 = AnonymousClass0W4.A00("units");
            C007406x.A00(439872622);
            sQLiteDatabase.execSQL(A002);
            C007406x.A00(-1525660733);
            A04(sQLiteDatabase);
            i = i2;
        }
    }
}
