package X;

import com.facebook.profilo.logger.Logger;
import com.facebook.profilo.logger.api.ProfiloLogger;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0vI  reason: invalid class name and case insensitive filesystem */
public final class C15440vI implements C08470fP {
    private double A00;
    private int A01;
    private long A02 = 0;
    private long A03 = 0;
    private boolean A04;
    private boolean A05;
    private boolean A06;
    private boolean A07;
    private final int A08;
    private final AnonymousClass15Q A09;
    private final C09020gN A0A;
    private final AnonymousClass1T3 A0B;
    private final C32781mK A0C;
    private final AnonymousClass0US A0D;
    private final QuickPerformanceLogger A0E;
    private final Runnable A0F = new C33791o9();
    private final List A0G = new ArrayList();

    private boolean A00() {
        for (Integer intValue : this.A0G) {
            if (C004004z.A02(intValue.intValue())) {
                return true;
            }
        }
        return false;
    }

    public void A01() {
        if (this.A05) {
            this.A05 = false;
            if (this.A06) {
                this.A0E.markerAnnotate(this.A08, "vsync_time", String.valueOf(this.A0B.A02()));
                this.A0E.markerAnnotate(this.A08, "total_skipped_frames_uncapped", String.valueOf(this.A01));
                this.A0E.markerAnnotate(this.A08, "total_time_spent_uncapped", String.valueOf(this.A02));
                this.A0E.markerAnnotate(this.A08, "time_since_startup", String.valueOf(((AnonymousClass4Q0) this.A0D.get()).A01()));
                this.A0E.markerAnnotate(this.A08, "4_frame_drop_uncapped", String.valueOf(this.A00));
                this.A0A.A01();
                this.A06 = false;
            }
            if (this.A04) {
                this.A0E.markerAnnotate(this.A08, "touch_event_latency", String.valueOf(this.A03));
                this.A04 = false;
            }
            if (this.A07 && ((int) this.A00) == 0) {
                this.A0E.markerCancel(this.A08);
                return;
            }
        } else if ((!C004004z.A02(this.A08) && !A00()) || !C004004z.A03("frames")) {
            return;
        }
        this.A0E.markerEnd(this.A08, 2);
    }

    public void A02() {
        C192917o r0;
        if (!this.A05) {
            this.A05 = true;
            this.A0E.markerStart(this.A08);
            if ((C004004z.A02(this.A08) || A00()) && !C004004z.A03("frames")) {
                this.A0A.A02();
                this.A01 = 0;
                this.A00 = 0.0d;
                this.A02 = 0;
                this.A03 = 0;
                this.A06 = true;
                C32781mK r1 = this.A0C;
                if (r1.A00 == null) {
                    r0 = null;
                } else {
                    r0 = r1.A04;
                }
                if (r0 != null) {
                    this.A03 = r0.A03 - r0.A01;
                    this.A04 = true;
                }
            }
        }
    }

    public void onFrameRendered(int i) {
        int i2;
        int i3;
        double d;
        int i4;
        if (this.A05) {
            int i5 = i;
            if (i != 0) {
                if (AnonymousClass0OU.A04 == null) {
                    AnonymousClass0OU.A04 = new AnonymousClass0OU();
                }
                AnonymousClass0OU.A04.A00(this.A0F);
                int max = Math.max(1, i5);
                int A022 = this.A0B.A02();
                int max2 = Math.max(Math.round(((float) max) / ((float) A022)) - 1, 0);
                this.A01 += max2;
                double d2 = this.A00;
                AnonymousClass15Q r0 = this.A09;
                long j = (long) max2;
                AnonymousClass15Q.A02(r0);
                if (0 != 0) {
                    i2 = r0.A01;
                } else {
                    i2 = 4;
                }
                if (0 != 0) {
                    i3 = r0.A01;
                } else {
                    i3 = 4;
                }
                int i6 = (j > ((long) i3) ? 1 : (j == ((long) i3) ? 0 : -1));
                boolean z = false;
                if (i6 >= 0) {
                    z = true;
                }
                if (z) {
                    d = ((double) Math.round((((double) j) * 100.0d) / ((double) i2))) / 100.0d;
                } else {
                    d = 0.0d;
                }
                this.A00 = d2 + d;
                this.A02 += (long) ((max2 + 1) * A022);
                if (max2 >= 1) {
                    if (ProfiloLogger.sHasProfilo) {
                        Logger.writeStandardEntry(AnonymousClass00n.A09, 6, 44, 0, 0, 8126498, 0, j);
                    }
                    C005505z.A03("ScrollPerf.FrameDropped", 1909412155);
                    C005505z.A00(-199444953);
                    AnonymousClass15Q r02 = this.A09;
                    AnonymousClass15Q.A02(r02);
                    if (0 != 0) {
                        i4 = r02.A01;
                    } else {
                        i4 = 4;
                    }
                    int i7 = (j > ((long) i4) ? 1 : (j == ((long) i4) ? 0 : -1));
                    boolean z2 = false;
                    if (i7 >= 0) {
                        z2 = true;
                    }
                    if (z2) {
                        C005505z.A03("ScrollPerf.LargeFrameDropped", 1981816652);
                        C005505z.A00(1575040301);
                    }
                }
                C005505z.A03("ScrollPerf.FrameStarted", -1349115737);
                C005505z.A00(-175442571);
            }
        }
    }

    public C15440vI(QuickPerformanceLogger quickPerformanceLogger, C27031cX r4, AnonymousClass1T3 r5, C32781mK r6, AnonymousClass0US r7, AnonymousClass15Q r8, int i, AnonymousClass1YI r10) {
        this.A0E = quickPerformanceLogger;
        C09020gN A002 = r4.A00(true);
        this.A0A = A002;
        A002.A01 = this;
        this.A0B = r5;
        this.A0C = r6;
        this.A0D = r7;
        this.A09 = r8;
        this.A08 = i;
        this.A07 = r10.AbO(AnonymousClass1Y3.A2s, false);
    }
}
