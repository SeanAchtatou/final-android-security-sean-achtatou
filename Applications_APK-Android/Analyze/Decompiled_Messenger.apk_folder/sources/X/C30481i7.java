package X;

import com.facebook.acra.ACRA;
import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import java.math.RoundingMode;
import java.util.Arrays;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1i7  reason: invalid class name and case insensitive filesystem */
public final class C30481i7 extends CharMatcher {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final String A04;
    public final char[] A05;
    public final boolean[] A06;
    private final byte[] A07;

    public int A00(char c) {
        Object valueOf;
        byte b;
        if (c <= 127 && (b = this.A07[c]) != -1) {
            return b;
        }
        StringBuilder sb = new StringBuilder("Unrecognized character: ");
        if (CharMatcher.Invisible.INSTANCE.matches(c)) {
            valueOf = AnonymousClass08S.A0J("0x", Integer.toHexString(c));
        } else {
            valueOf = Character.valueOf(c);
        }
        sb.append(valueOf);
        throw new C37831wK(sb.toString());
    }

    public boolean equals(Object obj) {
        if (obj instanceof C30481i7) {
            return Arrays.equals(this.A05, ((C30481i7) obj).A05);
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(this.A05);
    }

    public boolean matches(char c) {
        if (!CharMatcher.Ascii.INSTANCE.matches(c) || this.A07[c] == -1) {
            return false;
        }
        return true;
    }

    public String toString() {
        return this.A04;
    }

    public C30481i7(String str, char[] cArr) {
        int i;
        Preconditions.checkNotNull(str);
        this.A04 = str;
        Preconditions.checkNotNull(cArr);
        this.A05 = cArr;
        try {
            int length = cArr.length;
            RoundingMode roundingMode = RoundingMode.UNNECESSARY;
            if (length > 0) {
                switch (AnonymousClass18J.A00[roundingMode.ordinal()]) {
                    case 1:
                        if (!(length > 0) || !(((length + -1) & length) == 0)) {
                            throw new ArithmeticException("mode was UNNECESSARY, but rounding was necessary");
                        }
                    case 2:
                    case 3:
                        i = 31 - Integer.numberOfLeadingZeros(length);
                        break;
                    case 4:
                    case 5:
                        i = 32 - Integer.numberOfLeadingZeros(length - 1);
                        break;
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    case 8:
                        int numberOfLeadingZeros = Integer.numberOfLeadingZeros(length);
                        i = (31 - numberOfLeadingZeros) + (((((-1257966797 >>> numberOfLeadingZeros) - length) ^ -1) ^ -1) >>> 31);
                        break;
                    default:
                        throw new AssertionError();
                }
                this.A01 = i;
                int min = Math.min(8, Integer.lowestOneBit(i));
                try {
                    this.A03 = 8 / min;
                    this.A02 = i / min;
                    this.A00 = length - 1;
                    byte[] bArr = new byte[128];
                    Arrays.fill(bArr, (byte) -1);
                    int i2 = 0;
                    while (i2 < length) {
                        char c = cArr[i2];
                        String str2 = "Non-ASCII character: %s";
                        if (CharMatcher.Ascii.INSTANCE.matches(c)) {
                            str2 = "Duplicate character: %s";
                            if (bArr[c] == -1) {
                                bArr[c] = (byte) i2;
                                i2++;
                            }
                        }
                        throw new IllegalArgumentException(Preconditions.format(str2, Character.valueOf(c)));
                    }
                    this.A07 = bArr;
                    boolean[] zArr = new boolean[this.A03];
                    for (int i3 = 0; i3 < this.A02; i3++) {
                        zArr[AnonymousClass18K.A00(i3 << 3, this.A01, RoundingMode.CEILING)] = true;
                    }
                    this.A06 = zArr;
                } catch (ArithmeticException e) {
                    throw new IllegalArgumentException(AnonymousClass08S.A0J("Illegal alphabet ", new String(cArr)), e);
                }
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A0M("x", " (", length, ") must be > 0"));
            }
        } catch (ArithmeticException e2) {
            throw new IllegalArgumentException(AnonymousClass08S.A09("Illegal alphabet length ", cArr.length), e2);
        }
    }
}
