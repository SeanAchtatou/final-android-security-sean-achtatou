package X;

/* renamed from: X.1kR  reason: invalid class name and case insensitive filesystem */
public final class C31821kR {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A;
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C;
    public static final AnonymousClass1Y7 A0D;
    public static final AnonymousClass1Y7 A0E;
    public static final AnonymousClass1Y7 A0F;
    public static final AnonymousClass1Y7 A0G;
    public static final AnonymousClass1Y7 A0H;
    public static final AnonymousClass1Y7 A0I;
    public static final AnonymousClass1Y7 A0J;
    public static final AnonymousClass1Y7 A0K;
    public static final AnonymousClass1Y7 A0L;
    public static final AnonymousClass1Y7 A0M;
    public static final AnonymousClass1Y7 A0N;
    public static final AnonymousClass1Y7 A0O;
    public static final AnonymousClass1Y7 A0P = ((AnonymousClass1Y7) A0O.A09("release_info"));
    public static final AnonymousClass1Y7 A0Q;
    public static final AnonymousClass1Y7 A0R;
    private static final AnonymousClass1Y7 A0S;
    private static final AnonymousClass1Y7 A0T;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A05.A09("new_selfupdate/");
        A0O = r1;
        A0M = (AnonymousClass1Y7) r1.A09("last_fetched_ts");
        AnonymousClass1Y7 r12 = A0O;
        A04 = (AnonymousClass1Y7) r12.A09("flow_timeout_ts");
        A02 = (AnonymousClass1Y7) r12.A09("download_prompt_count");
        A03 = (AnonymousClass1Y7) r12.A09("download_prompt_ts");
        A0G = (AnonymousClass1Y7) r12.A09("install_prompt_count");
        A0H = (AnonymousClass1Y7) r12.A09("install_prompt_ts");
        A05 = (AnonymousClass1Y7) r12.A09("install_notification_count");
        A06 = (AnonymousClass1Y7) r12.A09("install_notification_ts");
        AnonymousClass1Y7 r0 = (AnonymousClass1Y7) r12.A09("install_pending_state/");
        A0S = r0;
        A0D = (AnonymousClass1Y7) r0.A09("release_info");
        AnonymousClass1Y7 r13 = A0S;
        A08 = (AnonymousClass1Y7) r13.A09(AnonymousClass80H.$const$string(145));
        A09 = (AnonymousClass1Y7) r13.A09(AnonymousClass24B.$const$string(1113));
        A0B = (AnonymousClass1Y7) r13.A09(AnonymousClass24B.$const$string(299));
        A0C = (AnonymousClass1Y7) r13.A09(AnonymousClass24B.$const$string(300));
        A0E = (AnonymousClass1Y7) r13.A09(AnonymousClass80H.$const$string(AnonymousClass1Y3.A1a));
        A0F = (AnonymousClass1Y7) r13.A09(AnonymousClass24B.$const$string(AnonymousClass1Y3.ABw));
        A07 = (AnonymousClass1Y7) r13.A09("background_mode");
        A0A = (AnonymousClass1Y7) r13.A09("is_mobile_data_only");
        AnonymousClass1Y7 r14 = (AnonymousClass1Y7) A0O.A09("internal_settings/");
        A0T = r14;
        A0I = (AnonymousClass1Y7) r14.A09("package_name_override");
        AnonymousClass1Y7 r15 = A0T;
        A0J = (AnonymousClass1Y7) r15.A09("version_code_override");
        A0K = (AnonymousClass1Y7) r15.A09("version_name_override");
        AnonymousClass1Y7 r16 = A0O;
        A0Q = (AnonymousClass1Y7) r16.A09("unknown_sources_installation_checked");
        A0R = (AnonymousClass1Y7) r16.A09("unknown_sources_installation_ts");
        A00 = (AnonymousClass1Y7) r16.A09("additional_download_prompt");
        A01 = (AnonymousClass1Y7) r16.A09("disable_prompts_until_ts");
        A0L = (AnonymousClass1Y7) r16.A09("last_apk_scan");
        A0N = (AnonymousClass1Y7) r16.A09("mobile_data_upgrade_count");
    }
}
