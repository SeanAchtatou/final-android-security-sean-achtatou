package X;

import android.content.Context;
import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.1zs  reason: invalid class name and case insensitive filesystem */
public final class C39951zs {
    private static C04470Uu A01;
    public final Context A00;

    public static final C39951zs A00(AnonymousClass1XY r4) {
        C39951zs r0;
        synchronized (C39951zs.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C39951zs((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C39951zs) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C39951zs(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1YA.A00(r2);
    }
}
