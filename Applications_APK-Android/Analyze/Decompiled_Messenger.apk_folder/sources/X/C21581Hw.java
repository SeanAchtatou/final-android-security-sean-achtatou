package X;

import android.database.sqlite.SQLiteDatabase;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.graphql.query.GQSQStringShape0S0000000_I0;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;

@UserScoped
/* renamed from: X.1Hw  reason: invalid class name and case insensitive filesystem */
public final class C21581Hw {
    private static C05540Zi A03;
    private static final ImmutableList A04 = ImmutableList.of("MESSENGER_ADS");
    public AnonymousClass0UN A00;
    public final C05660a7 A01 = new C05660a7("MESSENGER_INBOX2_ADS", BuildConfig.FLAVOR, BuildConfig.FLAVOR);
    public final C25581a4 A02;

    public static C26931cN A00(C21581Hw r3) {
        GQSQStringShape0S0000000_I0 A022 = ((InboxUnitGraphQLQueryExecutorHelper) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AGk, r3.A00)).A02("MESSENGER_INBOX2");
        A022.A09("serviceType", "MESSENGER_INBOX2");
        A022.A09("fetch_type", "REGULAR");
        A022.A08("specific_units", A04);
        A022.A05("separate_ads_fetch_enabled", true);
        C26931cN A002 = C26931cN.A00(A022);
        A002.A0B(C09290gx.NETWORK_ONLY);
        return A002;
    }

    public static final C21581Hw A01(AnonymousClass1XY r6) {
        C21581Hw r0;
        synchronized (C21581Hw.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r6)) {
                    AnonymousClass1XY r4 = (AnonymousClass1XY) A03.A01();
                    C05540Zi r3 = A03;
                    C25521Zy r1 = new C25521Zy(r4);
                    new C05670a8(r4);
                    r3.A00 = new C21581Hw(r4, r1);
                }
                C05540Zi r12 = A03;
                r0 = (C21581Hw) r12.A00;
                r12.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static AnonymousClass101 A03(C21581Hw r6, AnonymousClass102 r7, GSTModelShape1S0000000 gSTModelShape1S0000000) {
        C005505z.A03("convertResultToInboxUnitSnapshot", 402797895);
        try {
            return new AnonymousClass101(r7, C25611a7.ALL, ((C06080ao) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ab0, r6.A00)).A02(new C11500nH(C05660a7.A00(r6.A01, "last_successful_fetch_ms")), 0), ((C07900eM) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A0D, r6.A00)).A07(gSTModelShape1S0000000));
        } finally {
            C005505z.A00(2137501188);
        }
    }

    private C21581Hw(AnonymousClass1XY r4, C25521Zy r5) {
        this.A00 = new AnonymousClass0UN(10, r4);
        this.A02 = r5.A00("MESSENGER_INBOX2_ADS", BuildConfig.FLAVOR, BuildConfig.FLAVOR);
    }

    public static AnonymousClass101 A02(C21581Hw r10) {
        ImmutableList A0M;
        SQLiteDatabase A06;
        C26931cN A002 = A00(r10);
        C005505z.A03("makeRequest", -1305526996);
        try {
            GraphQLResult graphQLResult = (GraphQLResult) C46802Rw.A01(((C11170mD) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASp, r10.A00)).A04(A002), C50232dd.class);
            C005505z.A00(1213256531);
            C005505z.A03("writeResultToDatabase", -556414380);
            try {
                GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) graphQLResult.A03;
                String $const$string = ECX.$const$string(79);
                if (gSTModelShape1S0000000 != null) {
                    GSTModelShape1S0000000 A1n = gSTModelShape1S0000000.A1n();
                    if (!(A1n == null || (A0M = A1n.A0M(104993457, C27161ck.class, 738428414)) == null)) {
                        A06 = ((C09250go) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A82, r10.A00)).A06();
                        C007406x.A01(A06, 1495253828);
                        r10.A02.A02();
                        C24971Xv it = A0M.iterator();
                        int i = 0;
                        while (it.hasNext()) {
                            r10.A02.A03((C27161ck) it.next(), i, false);
                            i++;
                        }
                        ((C06080ao) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ab0, r10.A00)).A05(new C11500nH(C05660a7.A00(r10.A01, "last_successful_fetch_ms")), graphQLResult.A00);
                        ((C06080ao) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ab0, r10.A00)).A06(new C11500nH(C05660a7.A00(r10.A01, "last_successful_cache_key")), A002.A00.Azr(A002, (AnonymousClass0m4) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZ6, r10.A00)));
                        ((C06080ao) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ab0, r10.A00)).A07(r10.A01.A01(), false);
                        A06.setTransactionSuccessful();
                        C007406x.A02(A06, 725869350);
                        C005505z.A00(-2032546776);
                        return A03(r10, AnonymousClass102.FROM_SERVER, (GSTModelShape1S0000000) graphQLResult.A03);
                    }
                }
                ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r10.A00)).CGS($const$string, "Null result from server");
                C005505z.A00(-1376336673);
                return A03(r10, AnonymousClass102.FROM_SERVER, (GSTModelShape1S0000000) graphQLResult.A03);
            } catch (Throwable th) {
                C005505z.A00(572893961);
                throw th;
            }
        } catch (Throwable th2) {
            C005505z.A00(16550783);
            throw th2;
        }
    }
}
