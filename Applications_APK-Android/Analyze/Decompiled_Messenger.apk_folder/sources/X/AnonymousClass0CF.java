package X;

/* renamed from: X.0CF  reason: invalid class name */
public final class AnonymousClass0CF implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.protocol.MqttClient$8";
    public final /* synthetic */ AnonymousClass0CE A00;
    public final /* synthetic */ AnonymousClass0C8 A01;
    public final /* synthetic */ AnonymousClass0CG A02;
    public final /* synthetic */ Throwable A03;

    public AnonymousClass0CF(AnonymousClass0C8 r1, AnonymousClass0CE r2, AnonymousClass0CG r3, Throwable th) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = r3;
        this.A03 = th;
    }

    public void run() {
        AnonymousClass0C8.A03(this.A01, this.A00, this.A02, this.A03);
    }
}
