package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.model.UserKey;

/* renamed from: X.16Q  reason: invalid class name */
public final class AnonymousClass16Q implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new UserKey(C25651aB.valueOf(parcel.readString()), parcel.readString());
    }

    public Object[] newArray(int i) {
        return new UserKey[i];
    }
}
