package X;

import android.net.Uri;

/* renamed from: X.13U  reason: invalid class name */
public abstract class AnonymousClass13U implements AnonymousClass13Q {
    private final AnonymousClass13Q A00;

    public String Ad9() {
        return this.A00.Ad9();
    }

    public Uri.Builder AdB() {
        return this.A00.AdB();
    }

    public String AdC() {
        if (!(this instanceof AnonymousClass13S)) {
            return this.A00.AdC();
        }
        AnonymousClass13S r1 = (AnonymousClass13S) this;
        if (r1.A00.A0G()) {
            return r1.A02;
        }
        return r1.A01;
    }

    public Uri.Builder AoB() {
        return this.A00.AoB();
    }

    public Uri.Builder AoC() {
        return this.A00.AoC();
    }

    public Uri.Builder AoJ() {
        return this.A00.AoJ();
    }

    public Uri.Builder Auq() {
        return this.A00.Auq();
    }

    public Uri.Builder B2G() {
        return this.A00.B2G();
    }

    public Uri.Builder B2H() {
        return this.A00.B2H();
    }

    public String getDomain() {
        return this.A00.getDomain();
    }

    public AnonymousClass13U(AnonymousClass13Q r1) {
        this.A00 = r1;
    }
}
