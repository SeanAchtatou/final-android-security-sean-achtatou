package X;

/* renamed from: X.0qX  reason: invalid class name */
public final class AnonymousClass0qX {
    public static void A00(Object obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
    }

    public static void A01(Object obj, Object obj2) {
        if (obj == null) {
            throw new NullPointerException(String.valueOf(obj2));
        }
    }

    public static void A02(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }
}
