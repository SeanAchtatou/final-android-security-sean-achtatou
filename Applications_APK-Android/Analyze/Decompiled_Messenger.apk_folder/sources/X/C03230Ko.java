package X;

import android.content.Context;
import java.io.FilenameFilter;

/* renamed from: X.0Ko  reason: invalid class name and case insensitive filesystem */
public final class C03230Ko {
    public final Context A00;
    public final FilenameFilter A01 = new C03220Kn();

    public C03230Ko(Context context) {
        this.A00 = context;
    }
}
