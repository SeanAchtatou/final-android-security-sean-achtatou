package X;

import java.util.concurrent.Callable;

/* renamed from: X.108  reason: invalid class name */
public final class AnonymousClass108 {
    public AnonymousClass0VL A00;
    public Runnable A01;
    public String A02;
    public String A03 = "Other";
    public String A04;
    public Callable A05;
    private AnonymousClass0UN A06;

    public static final AnonymousClass108 A00(AnonymousClass1XY r1) {
        return new AnonymousClass108(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        if (0 != 0) goto L_0x001a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00ec A[LOOP:0: B:29:0x007a->B:59:0x00ec, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00f6 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1TL A01() {
        /*
            r16 = this;
            r3 = r16
            java.lang.String r1 = r3.A02
            java.lang.String r0 = "No taskname was given. Use setTaskName(String) to resolve."
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            java.lang.Runnable r1 = r3.A01
            if (r1 != 0) goto L_0x0011
            java.util.concurrent.Callable r0 = r3.A05
            if (r0 != 0) goto L_0x001a
        L_0x0011:
            if (r1 == 0) goto L_0x0017
            java.util.concurrent.Callable r0 = r3.A05
            if (r0 == 0) goto L_0x001a
        L_0x0017:
            r1 = 0
            if (r1 == 0) goto L_0x001b
        L_0x001a:
            r1 = 1
        L_0x001b:
            java.lang.String r0 = "Either a Runnable or Callable should be given."
            com.google.common.base.Preconditions.checkArgument(r1, r0)
            r2 = 0
            if (r2 != 0) goto L_0x002d
            java.lang.Runnable r1 = r3.A01
            if (r1 == 0) goto L_0x0046
            r0 = 0
            X.0XX r2 = new X.0XX
            r2.<init>(r1, r0)
        L_0x002d:
            r4 = 5
            int r1 = X.AnonymousClass1Y3.B7t
            X.0UN r0 = r3.A06
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.10A r4 = (X.AnonymousClass10A) r4
            java.lang.String r5 = r3.A02
            X.0cV r0 = r4.A02
            X.0kK r0 = r0.A00
            X.10B r0 = r0.Aio()
            if (r0 == 0) goto L_0x00fb
            monitor-enter(r4)
            goto L_0x004e
        L_0x0046:
            java.util.concurrent.Callable r0 = r3.A05
            X.0XX r2 = new X.0XX
            r2.<init>(r0)
            goto L_0x002d
        L_0x004e:
            java.util.Map r0 = r4.A01     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x00f6
            com.facebook.common.util.TriState r0 = r4.A00     // Catch:{ all -> 0x00f8 }
            r12 = 0
            boolean r0 = r0.asBoolean(r12)     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x00f6
            java.util.Map r0 = r4.A01     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x00f6
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x00f8 }
            r0.<init>()     // Catch:{ all -> 0x00f8 }
            r4.A01 = r0     // Catch:{ all -> 0x00f8 }
            X.0cV r0 = r4.A02     // Catch:{ all -> 0x00f8 }
            X.0kK r1 = r0.A00     // Catch:{ all -> 0x00f8 }
            X.10B r0 = r1.Aio()     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x00f6
            X.0gn r0 = r1.Aik()     // Catch:{ all -> 0x00f8 }
            int[] r11 = r0.Aip()     // Catch:{ all -> 0x00f8 }
            int r10 = r11.length     // Catch:{ all -> 0x00f8 }
            r9 = 0
        L_0x007a:
            if (r9 >= r10) goto L_0x00f2
            r14 = r11[r9]     // Catch:{ all -> 0x00f8 }
            java.lang.String r1 = "BusinessThreadQuery,ThreadQuery,UsersQuery,InstantGamesListQuery,MessagingContactsRankingQuery,FetchMyMontageThreadFbidQuery,MessagingSearchCacheQuery,FetchCmsQuery"
            switch(r14) {
                case 5505026: goto L_0x0085;
                case 5505027: goto L_0x0085;
                case 5505028: goto L_0x0085;
                case 5505081: goto L_0x0088;
                case 5505105: goto L_0x0088;
                default: goto L_0x0083;
            }     // Catch:{ all -> 0x00f8 }
        L_0x0083:
            monitor-enter(r4)     // Catch:{ all -> 0x00f8 }
            goto L_0x008b
        L_0x0085:
            java.lang.String r1 = "BusinessThreadQuery,ThreadQuery,UsersQuery,InstantGamesListQuery,MessagingContactsRankingQuery,FetchMyMontageThreadFbidQuery,MessagingSearchCacheQuery,FetchCmsQueryFetchThreadList,ThreadsQuery,MessagesQuery"
            goto L_0x0083
        L_0x0088:
            java.lang.String r1 = "BusinessThreadQuery,ThreadQuery,UsersQuery,InstantGamesListQuery,MessagingContactsRankingQuery,FetchMyMontageThreadFbidQuery,MessagingSearchCacheQuery,FetchCmsQueryFetchStickerCoordinator"
            goto L_0x0083
        L_0x008b:
            java.util.Locale r0 = java.util.Locale.US     // Catch:{ all -> 0x00ef }
            java.lang.String r1 = r1.toLowerCase(r0)     // Catch:{ all -> 0x00ef }
            java.lang.String r0 = ","
            java.lang.String[] r13 = r1.split(r0)     // Catch:{ all -> 0x00ef }
            int r8 = r13.length     // Catch:{ all -> 0x00ef }
            r7 = 0
            if (r8 == 0) goto L_0x00ab
            r6 = 0
            r0 = 1
            if (r8 != r0) goto L_0x00a7
            r0 = r13[r12]     // Catch:{ all -> 0x00ef }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00ef }
            if (r0 != 0) goto L_0x00ab
        L_0x00a7:
            java.util.Map r0 = r4.A01     // Catch:{ all -> 0x00ef }
            if (r0 != 0) goto L_0x00b2
        L_0x00ab:
            r4.A01 = r7     // Catch:{ all -> 0x00ef }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x00ef }
            r4.A00 = r0     // Catch:{ all -> 0x00ef }
            goto L_0x00e3
        L_0x00b2:
            if (r6 >= r8) goto L_0x00e3
            r15 = r13[r6]     // Catch:{ all -> 0x00ef }
            java.util.Map r0 = r4.A01     // Catch:{ all -> 0x00ef }
            boolean r0 = r0.containsKey(r15)     // Catch:{ all -> 0x00ef }
            if (r0 == 0) goto L_0x00d6
            java.util.Map r0 = r4.A01     // Catch:{ all -> 0x00ef }
            java.lang.Object r0 = r0.get(r15)     // Catch:{ all -> 0x00ef }
            X.1lw r0 = (X.C32551lw) r0     // Catch:{ all -> 0x00ef }
            if (r0 != 0) goto L_0x00cf
            r4.A01 = r7     // Catch:{ all -> 0x00ef }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x00ef }
            r4.A00 = r0     // Catch:{ all -> 0x00ef }
            goto L_0x00e3
        L_0x00cf:
            android.util.SparseBooleanArray r1 = r0.A00     // Catch:{ all -> 0x00ef }
            r0 = 1
            r1.append(r14, r0)     // Catch:{ all -> 0x00ef }
            goto L_0x00e0
        L_0x00d6:
            java.util.Map r1 = r4.A01     // Catch:{ all -> 0x00ef }
            X.1lw r0 = new X.1lw     // Catch:{ all -> 0x00ef }
            r0.<init>(r14)     // Catch:{ all -> 0x00ef }
            r1.put(r15, r0)     // Catch:{ all -> 0x00ef }
        L_0x00e0:
            int r6 = r6 + 1
            goto L_0x00b2
        L_0x00e3:
            monitor-exit(r4)     // Catch:{ all -> 0x00f8 }
            com.facebook.common.util.TriState r0 = r4.A00     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.asBoolean(r12)     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x00f6
            int r9 = r9 + 1
            goto L_0x007a
        L_0x00ef:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00f8 }
            throw r0     // Catch:{ all -> 0x00f8 }
        L_0x00f2:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO     // Catch:{ all -> 0x00f8 }
            r4.A00 = r0     // Catch:{ all -> 0x00f8 }
        L_0x00f6:
            monitor-exit(r4)
            goto L_0x0107
        L_0x00f8:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x00fb:
            com.facebook.common.util.TriState r0 = r4.A00
            boolean r0 = r0.isSet()
            if (r0 != 0) goto L_0x0107
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            r4.A00 = r0
        L_0x0107:
            com.facebook.common.util.TriState r1 = r4.A00
            r0 = 0
            boolean r0 = r1.asBoolean(r0)
            if (r0 == 0) goto L_0x011b
            X.3cO r0 = new X.3cO
            r0.<init>()
        L_0x0115:
            X.1TL r1 = new X.1TL
            r1.<init>(r2, r0, r3)
            return r1
        L_0x011b:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r5.toLowerCase(r0)
            java.util.Map r0 = r4.A01
            if (r0 == 0) goto L_0x0134
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x0134
            java.util.Map r0 = r4.A01
            java.lang.Object r0 = r0.get(r1)
            X.1GL r0 = (X.AnonymousClass1GL) r0
            goto L_0x0115
        L_0x0134:
            X.1GM r0 = new X.1GM
            r0.<init>()
            goto L_0x0115
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass108.A01():X.1TL");
    }

    public void A03(String str) {
        int i;
        int i2;
        if (str.equals("ForUiThread")) {
            i = 0;
            i2 = AnonymousClass1Y3.AKg;
        } else if (str.equals("Foreground")) {
            i = 1;
            i2 = AnonymousClass1Y3.Am5;
        } else if (str.equals("Default")) {
            i = 2;
            i2 = AnonymousClass1Y3.AZx;
        } else if (str.equals("Background")) {
            i = 3;
            i2 = AnonymousClass1Y3.ASK;
        } else if (str.equals("ForNonUiThread")) {
            i = 4;
            i2 = AnonymousClass1Y3.ALA;
        } else {
            return;
        }
        this.A00 = (AnonymousClass0VL) AnonymousClass1XX.A02(i, i2, this.A06);
    }

    private AnonymousClass108(AnonymousClass1XY r4) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(6, r4);
        this.A06 = r2;
        this.A00 = (AnonymousClass0VL) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ASK, r2);
    }

    public void A02(Runnable runnable) {
        this.A01 = runnable;
    }
}
