package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.0W3  reason: invalid class name */
public final class AnonymousClass0W3 extends AnonymousClass0W4 {
    private static final ImmutableList A00 = ImmutableList.of(AnonymousClass0W5.A00, AnonymousClass0W5.A01, AnonymousClass0W5.A02);

    public AnonymousClass0W3() {
        super("preferences", A00);
    }
}
