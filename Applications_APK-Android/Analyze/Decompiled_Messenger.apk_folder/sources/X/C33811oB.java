package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.1oB  reason: invalid class name and case insensitive filesystem */
public final class C33811oB implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass1TD A00;

    public C33811oB(AnonymousClass1TD r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r7) {
        int A002 = AnonymousClass09Y.A00(1783815724);
        AnonymousClass1TD r2 = this.A00;
        synchronized (r2) {
            AnonymousClass1G0 r1 = r2.A00;
            if (r1 != null) {
                r1.A01(false);
                r2.A00 = null;
            }
        }
        AnonymousClass09Y.A01(1245064177, A002);
    }
}
