package X;

import java.io.File;
import java.util.Comparator;

/* renamed from: X.0YS  reason: invalid class name */
public final class AnonymousClass0YS implements Comparator {
    public int compare(Object obj, Object obj2) {
        long lastModified = ((File) obj).lastModified();
        long lastModified2 = ((File) obj2).lastModified();
        if (lastModified < lastModified2) {
            return -1;
        }
        if (lastModified == lastModified2) {
            return 0;
        }
        return 1;
    }
}
