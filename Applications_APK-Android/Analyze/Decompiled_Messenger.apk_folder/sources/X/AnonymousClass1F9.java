package X;

import java.util.List;

/* renamed from: X.1F9  reason: invalid class name */
public final class AnonymousClass1F9 {
    public float A00 = 2.0f;
    public int A01 = -1;
    public AnonymousClass0p4 A02;
    public C35301r0 A03;
    public BTL A04;
    public C27799DjA A05;
    public C32691mA A06;
    public AnonymousClass19N A07;
    public AnonymousClass19L A08 = AnonymousClass1Ri.A19;
    public AnonymousClass6QN A09;
    public C193217r A0A;
    public List A0B;
    public boolean A0C = AnonymousClass07c.canInterruptAndMoveLayoutsBetweenThreads;
    public boolean A0D = false;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G = false;
    public boolean A0H = true;
    public boolean A0I;
    public boolean A0J = AnonymousClass07c.isLayoutDiffingEnabled;
    public boolean A0K = AnonymousClass07c.isReconciliationEnabled;
    public boolean A0L = AnonymousClass07c.incrementalVisibilityHandling;
    public boolean A0M = AnonymousClass07c.useCancelableLayoutFutures;
    public boolean A0N;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
        if (r0.A0d != false) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0027, code lost:
        if (r1 == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1Ri A00(X.AnonymousClass0p4 r7) {
        /*
            r6 = this;
            X.0p4 r0 = new X.0p4
            android.content.Context r1 = r7.A09
            java.lang.String r2 = r7.A0B()
            X.38i r3 = r7.A05()
            X.1KE r5 = r7.A08()
            r4 = 0
            r0.<init>(r1, r2, r3, r4, r5)
            r6.A02 = r0
            boolean r0 = r6.A0H
            r3 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0029
            com.facebook.litho.ComponentTree r0 = r7.A05
            if (r0 == 0) goto L_0x0025
            boolean r0 = r0.A0d
            r1 = 0
            if (r0 == 0) goto L_0x0026
        L_0x0025:
            r1 = 1
        L_0x0026:
            r0 = 1
            if (r1 != 0) goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            r6.A0H = r0
            X.19N r0 = r6.A07
            if (r0 != 0) goto L_0x0037
            X.19M r0 = new X.19M
            r0.<init>(r3, r2)
            r6.A07 = r0
        L_0x0037:
            X.1Ri r0 = new X.1Ri
            r0.<init>(r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1F9.A00(X.0p4):X.1Ri");
    }
}
