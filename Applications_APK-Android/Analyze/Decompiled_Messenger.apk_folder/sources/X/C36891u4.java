package X;

import com.google.common.base.Objects;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1u4  reason: invalid class name and case insensitive filesystem */
public final class C36891u4 extends Enum {
    private static final /* synthetic */ C36891u4[] A00;
    public static final C36891u4 A01;
    public static final C36891u4 A02;
    public static final C36891u4 A03;
    public static final C36891u4 A04;
    public static final C36891u4 A05;
    public static final C36891u4 A06;
    public static final C36891u4 A07;
    public static final C36891u4 A08;
    public static final C36891u4 A09;
    public static final C36891u4 A0A;
    public static final C36891u4 A0B;
    public static final C36891u4 A0C;
    public static final C36891u4 A0D;
    public static final C36891u4 A0E;
    public static final C36891u4 A0F;
    public static final C36891u4 A0G;
    public static final C36891u4 A0H;
    public final String serializedString;
    public final boolean shouldNotBeRetried;

    static {
        C36891u4 r0 = new C36891u4("NONE", 0, "none", false);
        A07 = r0;
        C36891u4 r02 = new C36891u4("EARLIER_MESSAGE_FROM_THREAD_FAILED", 1, "earlier_message_from_thread_failed", false);
        A01 = r02;
        C36891u4 r13 = new C36891u4("PENDING_SEND_ON_STARTUP", 2, "pending_send_on_startup", false);
        A0B = r13;
        C36891u4 r12 = new C36891u4("MEDIA_UPLOAD_FAILED", 3, "media_upload_failed", false);
        A05 = r12;
        C36891u4 r03 = new C36891u4("MEDIA_UPLOAD_FAILED_NONRETRYABLE", 4, "media_upload_failed_nonretryable", true);
        A06 = r03;
        C36891u4 r04 = new C36891u4("PERMANENT_FAILURE", 5, "permanent_failure", true);
        A0C = r04;
        C36891u4 r15 = new C36891u4("RETRYABLE_FAILURE", 6, "retryable_failure", false);
        A0D = r15;
        C36891u4 r14 = new C36891u4("P2P_PAYMENT_FAILURE", 7, "p2p_payment_failure", true);
        A09 = r14;
        C36891u4 r10 = new C36891u4("P2P_PAYMENT_RISK_FAILURE", 8, "p2p_payment_risk_failure", true);
        A0A = r10;
        C36891u4 r9 = new C36891u4("HTTP_4XX_ERROR", 9, "http_4xx", false);
        A02 = r9;
        C36891u4 r8 = new C36891u4("HTTP_5XX_ERROR", 10, "http_5xx", false);
        A03 = r8;
        C36891u4 r23 = new C36891u4("API_EXCEPTION", 11, "api_exception", false);
        C36891u4 r7 = new C36891u4("IO_EXCEPTION", 12, "io_exception", false);
        A04 = r7;
        C36891u4 r6 = new C36891u4("SMS_SEND_FAILED", 13, "sms_send_failed", false);
        A0E = r6;
        C36891u4 r5 = new C36891u4("TINCAN_RETRYABLE", 14, "tincan_send_failed_retryable", false);
        A0G = r5;
        C36891u4 r4 = new C36891u4("TINCAN_NONRETRYABLE", 15, "tincan_send_failed_nonretriable", true);
        A0F = r4;
        C36891u4 r3 = new C36891u4("TINCAN_THREAD_PARTICIPANTS_CHANGED", 16, "tincan_send_failed_thread_participants_changed", false);
        A0H = r3;
        C36891u4 r232 = new C36891u4("MQTT_PRICING_RESET", 17, "mqtt_pricing_reset", false);
        C36891u4 r233 = new C36891u4("OTHER", 18, "other", false);
        A08 = r233;
        C36891u4 r36 = r232;
        C36891u4 r37 = r233;
        C36891u4 r19 = r0;
        C36891u4 r20 = r02;
        C36891u4 r21 = r13;
        C36891u4 r22 = r12;
        A00 = new C36891u4[]{r19, r20, r21, r22, r03, r04, r15, r14, r10, r9, r8, r23, r7, r6, r5, r4, r3, r36, r37};
    }

    public static C36891u4 A00(String str) {
        if (str == null) {
            return A07;
        }
        for (C36891u4 r1 : values()) {
            if (Objects.equal(str, r1.serializedString)) {
                return r1;
            }
        }
        return A08;
    }

    public static C36891u4 valueOf(String str) {
        return (C36891u4) Enum.valueOf(C36891u4.class, str);
    }

    public static C36891u4[] values() {
        return (C36891u4[]) A00.clone();
    }

    private C36891u4(String str, int i, String str2, boolean z) {
        this.serializedString = str2;
        this.shouldNotBeRetried = z;
    }
}
