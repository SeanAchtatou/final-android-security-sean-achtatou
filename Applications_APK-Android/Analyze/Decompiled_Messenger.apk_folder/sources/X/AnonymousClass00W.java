package X;

import android.os.Process;
import android.util.Log;
import java.lang.reflect.Method;

/* renamed from: X.00W  reason: invalid class name */
public final class AnonymousClass00W implements AnonymousClass00X {
    private static volatile Method A00;
    private static volatile Method A01;
    private static volatile Method A02;
    private static volatile boolean A03;
    private static volatile boolean A04;
    private static volatile boolean A05;

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (A04() == null) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass00W A00() {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 25
            if (r1 > r0) goto L_0x0022
            java.lang.reflect.Method r0 = A02()
            if (r0 == 0) goto L_0x0019
            java.lang.reflect.Method r0 = A03()
            if (r0 == 0) goto L_0x0019
            java.lang.reflect.Method r1 = A04()
            r0 = 1
            if (r1 != 0) goto L_0x001a
        L_0x0019:
            r0 = 0
        L_0x001a:
            if (r0 == 0) goto L_0x0022
            X.00W r0 = new X.00W
            r0.<init>()
            return r0
        L_0x0022:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00W.A00():X.00W");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: java.lang.reflect.InvocationTargetException} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.lang.IllegalAccessException} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: java.lang.reflect.InvocationTargetException} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.lang.reflect.InvocationTargetException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object A01(java.lang.reflect.Method r5, java.lang.Object... r6) {
        /*
            java.lang.String r4 = ")"
            if (r5 == 0) goto L_0x0031
            r1 = 0
            r0 = 6
            java.lang.Object r0 = r5.invoke(r1, r6)     // Catch:{ IllegalAccessException -> 0x000b, InvocationTargetException -> 0x001b }
            return r0
        L_0x000b:
            r3 = move-exception
            java.lang.String r2 = "OldProcReader"
            boolean r0 = android.util.Log.isLoggable(r2, r0)
            if (r0 == 0) goto L_0x0031
            java.lang.String r1 = "Error (IllegalAccessException - "
            java.lang.String r0 = r3.getLocalizedMessage()
            goto L_0x002a
        L_0x001b:
            r3 = move-exception
            java.lang.String r2 = "OldProcReader"
            boolean r0 = android.util.Log.isLoggable(r2, r0)
            if (r0 == 0) goto L_0x0031
            java.lang.String r1 = "Error (InvocationTargetException - "
            java.lang.String r0 = r3.getLocalizedMessage()
        L_0x002a:
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r4)
            android.util.Log.e(r2, r0, r3)
        L_0x0031:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00W.A01(java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
    }

    private static Method A02() {
        if (!A03) {
            Class cls = Integer.TYPE;
            A00 = A05("parseProcLine", byte[].class, cls, cls, int[].class, String[].class, long[].class, float[].class);
            A03 = true;
        }
        return A00;
    }

    private static Method A03() {
        if (!A04) {
            A01 = A05("readProcFile", String.class, int[].class, String[].class, long[].class, float[].class);
            A04 = true;
        }
        return A01;
    }

    private static Method A04() {
        if (!A05) {
            A02 = A05("readProcLines", String.class, String[].class, long[].class);
            A05 = true;
        }
        return A02;
    }

    private static Method A05(String str, Class... clsArr) {
        try {
            return Process.class.getMethod(str, clsArr);
        } catch (NoSuchMethodException e) {
            if (!Log.isLoggable("OldProcReader", 5)) {
                return null;
            }
            Log.w("OldProcReader", AnonymousClass08S.A0J("Warning! Could not get access to JNI method - ", str), e);
            return null;
        }
    }

    public boolean BwU(byte[] bArr, int i, int i2, int[] iArr, String[] strArr, long[] jArr, float[] fArr) {
        Object A012 = A01(A02(), bArr, Integer.valueOf(i), Integer.valueOf(i2), iArr, strArr, jArr, fArr);
        if (A012 == null) {
            return false;
        }
        return ((Boolean) A012).booleanValue();
    }

    public boolean Bzf(String str, int[] iArr, String[] strArr, long[] jArr, float[] fArr) {
        Object A012 = A01(A03(), str, iArr, strArr, jArr, fArr);
        if (A012 == null) {
            return false;
        }
        return ((Boolean) A012).booleanValue();
    }

    public boolean Bzg(String str, String[] strArr, long[] jArr) {
        return true ^ Boolean.FALSE.equals(A01(A04(), str, strArr, jArr));
    }
}
