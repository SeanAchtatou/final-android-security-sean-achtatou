package X;

import android.content.DialogInterface;
import java.util.concurrent.ExecutorService;

/* renamed from: X.20p  reason: invalid class name and case insensitive filesystem */
public final class C401820p implements DialogInterface.OnClickListener {
    public final /* synthetic */ AnonymousClass6W4 A00;

    public C401820p(AnonymousClass6W4 r1) {
        this.A00 = r1;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        AnonymousClass6WG.A01((AnonymousClass6WG) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ACL, this.A00.A02), "revoke_link_confirmed");
        AnonymousClass6W4 r5 = this.A00;
        ((C94194e7) AnonymousClass1XX.A03(AnonymousClass1Y3.AGZ, r5.A02)).A01(r5.A05, (ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADs, r5.A02), new AnonymousClass6W7(r5));
    }
}
