package X;

import com.facebook.crypto.keychain.KeyChain;

/* renamed from: X.193  reason: invalid class name */
public final class AnonymousClass193 {
    public final KeyChain A00;
    public final AnonymousClass18C A01;
    public final Integer A02;

    public AnonymousClass193(AnonymousClass18C r2, Integer num, KeyChain keyChain) {
        this.A01 = r2;
        this.A02 = num;
        this.A00 = new AnonymousClass196(keyChain, num);
    }
}
