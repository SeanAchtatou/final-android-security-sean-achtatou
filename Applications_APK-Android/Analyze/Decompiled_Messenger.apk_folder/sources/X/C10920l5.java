package X;

import android.view.ViewGroup;

/* renamed from: X.0l5  reason: invalid class name and case insensitive filesystem */
public final class C10920l5 {
    public final int A00;
    public final int A01;
    public final ViewGroup.LayoutParams A02;
    public final C13830s9 A03;
    public final String A04;

    public C10920l5(int i, int i2, C13830s9 r3, ViewGroup.LayoutParams layoutParams, String str) {
        this.A00 = i;
        this.A01 = i2;
        this.A03 = r3;
        this.A02 = layoutParams;
        this.A04 = str;
    }

    public C10920l5(int i, int i2, C13830s9 r4, String str) {
        this.A00 = i;
        this.A01 = i2;
        this.A03 = r4;
        this.A04 = str;
        this.A02 = null;
    }
}
