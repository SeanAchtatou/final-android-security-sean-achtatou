package X;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0f6  reason: invalid class name */
public abstract class AnonymousClass0f6 {
    public Map A01() {
        if (!(this instanceof C26411bP)) {
            return null;
        }
        C26411bP r6 = (C26411bP) this;
        HashMap hashMap = new HashMap();
        boolean z = r6.A00.A05;
        String $const$string = AnonymousClass80H.$const$string(AnonymousClass1Y3.A4F);
        if (z) {
            hashMap.put($const$string, String.valueOf(r6.A00.A02));
            hashMap.put("platform", r6.A00.A01.toString());
            hashMap.put("model", r6.A00.A00.toString());
            return hashMap;
        }
        hashMap.put($const$string, "false");
        hashMap.put("platform", "uninitialized");
        hashMap.put("model", "uninitialized");
        return hashMap;
    }

    public void A02(Context context) {
        C26361bK r0;
        if (this instanceof C26421bQ) {
            r0 = ((C26421bQ) this).A00;
        } else if (this instanceof C26411bP) {
            r0 = ((C26411bP) this).A00;
        } else {
            return;
        }
        C26361bK.A00(r0, context);
    }

    public AnonymousClass0f7 A00(int i, C26501bY r8) {
        C26361bK r2;
        AnonymousClass0f7 AUy;
        Integer num;
        int i2;
        if (this instanceof C26331bH) {
            C26331bH r5 = (C26331bH) this;
            if (!r5.A00 || r8 == null) {
                return C26331bH.A04;
            }
            int i3 = r8.A03;
            boolean z = false;
            boolean z2 = false;
            if ((i3 & 1) == 1) {
                z2 = true;
            }
            if ((i3 & 2) == 2) {
                z = true;
            }
            AnonymousClass8TR r3 = new AnonymousClass8TR(r5.A02, z2, z);
            C26331bH.A03.put(i, r3);
            return r3;
        } else if (this instanceof C26441bS) {
            return new AnonymousClass8TQ();
        } else {
            if (this instanceof C26431bR) {
                return new AnonymousClass2RJ(((C26431bR) this).A00, r8);
            }
            if (this instanceof C26421bQ) {
                C26421bQ r0 = (C26421bQ) this;
                if (r8 != null) {
                    r2 = r0.A00;
                }
                return C26361bK.A06;
            } else if (this instanceof C26411bP) {
                C26411bP r02 = (C26411bP) this;
                if (r8 != null) {
                    r2 = r02.A00;
                }
                return C26361bK.A06;
            } else if (this instanceof C26351bJ) {
                C08270ey r1 = ((C26351bJ) this).A00;
                if (r1 == null) {
                    return C08240eu.A00;
                }
                return new C26581bg(r1);
            } else if (!(this instanceof AnonymousClass0f5)) {
                if (!(this instanceof AnonymousClass0f4)) {
                    if (!(this instanceof C08810fz)) {
                        return C08240eu.A00;
                    }
                    if (r8 != null) {
                        return new AnonymousClass2SV(r8.A03);
                    }
                } else if (r8 != null) {
                    return new C46632Qu(r8.A03);
                }
                return C08240eu.A00;
            } else {
                AnonymousClass0f5 r52 = (AnonymousClass0f5) this;
                Integer num2 = r52.A01;
                C73263fn r22 = r52.A00;
                if (r8 == null || (i2 = r8.A03) <= -1) {
                    num = r52.A02;
                } else {
                    num = Integer.valueOf(i2);
                }
                return new C49962d9(num2, r22, num);
            }
            if (r2.A02 && (AUy = r2.A01.AUy(r2.A00, r8)) != null) {
                return AUy;
            }
            return C26361bK.A06;
        }
    }
}
