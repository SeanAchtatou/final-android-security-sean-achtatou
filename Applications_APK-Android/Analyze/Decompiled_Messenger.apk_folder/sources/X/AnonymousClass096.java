package X;

import com.facebook.common.build.BuildConstants;

/* renamed from: X.096  reason: invalid class name */
public final class AnonymousClass096 {
    public static final String A00 = AnonymousClass08S.A09("disable.", BuildConstants.A00());

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.facebook.superpack.ditto.DittoPatch} */
    /* JADX WARN: Type inference failed for: r0v0, types: [X.0DK] */
    /* JADX WARN: Type inference failed for: r0v4 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    public static synchronized com.facebook.superpack.ditto.DittoPatch A00(android.content.Context r2) {
        /*
            java.lang.Class<X.096> r1 = X.AnonymousClass096.class
            monitor-enter(r1)
            r0 = 0
            if (r0 == 0) goto L_0x000e
            com.facebook.superpack.ditto.DittoPatch r0 = r0.A00(r2)     // Catch:{ all -> 0x000b }
            goto L_0x000e
        L_0x000b:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x000e:
            monitor-exit(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass096.A00(android.content.Context):com.facebook.superpack.ditto.DittoPatch");
    }

    public static synchronized boolean A01() {
        boolean z;
        synchronized (AnonymousClass096.class) {
            z = false;
            if (0 != 0) {
                z = true;
            }
        }
        return z;
    }
}
