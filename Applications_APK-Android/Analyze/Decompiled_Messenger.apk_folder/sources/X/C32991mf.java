package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1mf  reason: invalid class name and case insensitive filesystem */
public final class C32991mf implements AnonymousClass1FI {
    private static volatile C32991mf A00;

    public static final C32991mf A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C32991mf.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C32991mf();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public ImmutableMap get() {
        return ImmutableMap.of(new SubscribeTopic(TurboLoader.Locator.$const$string(98), 0), AnonymousClass1FP.ALWAYS);
    }
}
