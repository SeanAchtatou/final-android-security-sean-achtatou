package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.101  reason: invalid class name */
public final class AnonymousClass101 {
    public final long A00;
    public final AnonymousClass102 A01;
    public final C25611a7 A02;
    public final ImmutableList A03;

    public AnonymousClass101(AnonymousClass102 r1, C25611a7 r2, long j, ImmutableList immutableList) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = j;
        this.A03 = immutableList;
    }
}
