package X;

/* renamed from: X.1fA  reason: invalid class name and case insensitive filesystem */
public final class C28661fA implements C04310Tq {
    public final /* synthetic */ C28611f5 A00;

    public C28661fA(C28611f5 r1) {
        this.A00 = r1;
    }

    public Object get() {
        C35091qf r2 = (C35091qf) AnonymousClass1XX.A02(15, AnonymousClass1Y3.AQO, this.A00.A00);
        if (C35091qf.A01 == null) {
            C35101qg r1 = new C35101qg();
            r1.A01 = new C35111qh(r2);
            r1.A00 = new C35121qi(r2);
            r1.A02 = new C35131qj(r2);
            C35091qf.A01 = new C35141qk(r1);
        }
        return C35091qf.A01;
    }
}
