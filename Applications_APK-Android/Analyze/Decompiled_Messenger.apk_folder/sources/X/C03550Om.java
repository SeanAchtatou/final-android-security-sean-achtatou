package X;

import android.view.animation.Interpolator;

/* renamed from: X.0Om  reason: invalid class name and case insensitive filesystem */
public abstract class C03550Om {
    public final long A00;
    public final Interpolator A01;

    public abstract void A00(float f);

    public C03550Om(Interpolator interpolator, long j) {
        this.A01 = interpolator;
        this.A00 = j;
    }
}
