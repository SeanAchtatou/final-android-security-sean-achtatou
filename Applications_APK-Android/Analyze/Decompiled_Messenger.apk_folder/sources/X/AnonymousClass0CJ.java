package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0CJ  reason: invalid class name */
public final class AnonymousClass0CJ {
    public final int A00;
    public final C36191sf A01;
    public final Boolean A02;
    public final Boolean A03;
    public final Boolean A04;
    public final Byte A05;
    public final Integer A06;
    public final Integer A07;
    public final Long A08;
    public final Long A09;
    public final Long A0A;
    public final Long A0B;
    public final Long A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final String A0I;
    public final String A0J;
    public final List A0K;
    public final Map A0L;

    public static AnonymousClass0CJ A00(String str) {
        Integer num;
        int i;
        Byte b;
        Integer num2;
        C36191sf r27;
        try {
            JSONObject jSONObject = new JSONObject(str);
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray = jSONObject.optJSONArray(A03(AnonymousClass07B.A06));
            int i2 = 0;
            while (optJSONArray != null && i2 < optJSONArray.length()) {
                arrayList.add(optJSONArray.getString(i2));
                i2++;
            }
            HashMap hashMap = new HashMap();
            JSONObject optJSONObject = jSONObject.optJSONObject(A03(AnonymousClass07B.A0J));
            if (optJSONObject != null) {
                Iterator<String> keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    hashMap.put(next, optJSONObject.getString(next));
                }
            }
            Long A022 = A02(jSONObject, AnonymousClass07B.A00);
            String A042 = A04(jSONObject, AnonymousClass07B.A0C);
            Long A023 = A02(jSONObject, AnonymousClass07B.A0N);
            Long A024 = A02(jSONObject, AnonymousClass07B.A05);
            Integer valueOf = Integer.valueOf(jSONObject.optInt(A03(AnonymousClass07B.A03), -1));
            Integer num3 = AnonymousClass07B.A04;
            if (!jSONObject.has(A03(num3))) {
                num = null;
            } else {
                num = Integer.valueOf(jSONObject.optInt(A03(num3)));
            }
            Boolean A012 = A01(jSONObject, AnonymousClass07B.A0o);
            Boolean A013 = A01(jSONObject, AnonymousClass07B.A0n);
            String A043 = A04(jSONObject, AnonymousClass07B.A0q);
            String A044 = A04(jSONObject, AnonymousClass07B.A02);
            Boolean A014 = A01(jSONObject, AnonymousClass07B.A0p);
            Long A025 = A02(jSONObject, AnonymousClass07B.A0Y);
            String A045 = A04(jSONObject, AnonymousClass07B.A0i);
            if ("jz".equals(A045)) {
                i = 1;
            } else {
                i = 0;
                if ("jzo".equals(A045)) {
                    i = 2;
                }
            }
            String A046 = A04(jSONObject, AnonymousClass07B.A07);
            String A047 = A04(jSONObject, AnonymousClass07B.A08);
            String A048 = A04(jSONObject, AnonymousClass07B.A0B);
            String A049 = A04(jSONObject, AnonymousClass07B.A0A);
            try {
                b = Byte.valueOf(Byte.parseByte(jSONObject.optString(A03(AnonymousClass07B.A0I))));
            } catch (Exception unused) {
                b = null;
            }
            Long A026 = A02(jSONObject, AnonymousClass07B.A0H);
            Integer num4 = AnonymousClass07B.A0K;
            if (!jSONObject.has(A03(num4))) {
                num2 = null;
            } else {
                num2 = Integer.valueOf(jSONObject.optInt(A03(num4)));
            }
            switch (num2.intValue()) {
                case 1:
                    r27 = C36191sf.DEFAULT;
                    break;
                case 2:
                    r27 = C36191sf.G2;
                    break;
                case 3:
                    r27 = C36191sf.G3;
                    break;
                case 4:
                    r27 = C36191sf.G4;
                    break;
                case 5:
                    r27 = C36191sf.G5;
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                    r27 = C36191sf.LTE;
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    r27 = C36191sf.WIFI;
                    break;
                default:
                    r27 = null;
                    break;
            }
            return new AnonymousClass0CJ(A022, A042, A023, A024, valueOf, num, A012, A013, A043, A044, A014, A025, i, A046, A047, arrayList, A048, A049, b, hashMap, A026, r27);
        } catch (JSONException unused2) {
            return null;
        }
    }

    public String toString() {
        return "ConnectPayloadUserName {" + "user_id = <redacted>, " + "user_agent = " + this.A0J + ", " + "capabilities = " + this.A08 + ", " + "mqtt_session_id = " + this.A0B + ", " + "network_type = " + this.A07 + ", " + "network_subtype = " + this.A07 + ", " + "chat_on = " + this.A02 + ", " + "no_auto_fg = " + this.A04 + ", " + "device_client_id = <redacted>" + ", " + "device_client_secret = <redacted>" + ", " + "fg_keepalive = " + this.A03 + ", " + "client_type = " + this.A0E + ", " + "app_id = " + this.A0D + ", " + "connect_payload_hash = " + this.A0F + "}";
    }

    private static Boolean A01(JSONObject jSONObject, Integer num) {
        if (!jSONObject.has(A03(num))) {
            return null;
        }
        return Boolean.valueOf(jSONObject.optBoolean(A03(num)));
    }

    private static Long A02(JSONObject jSONObject, Integer num) {
        if (!jSONObject.has(A03(num))) {
            return null;
        }
        return Long.valueOf(jSONObject.optLong(A03(num)));
    }

    public static String A03(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "s";
            case 2:
                return "a";
            case 3:
                return "cp";
            case 4:
                return "ecp";
            case 5:
                return "pf";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                return "no_auto_fg";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "chat_on";
            case 8:
                return "fg";
            case Process.SIGKILL /*9*/:
                return "d";
            case AnonymousClass1Y3.A01 /*10*/:
                return "ds";
            case AnonymousClass1Y3.A02 /*11*/:
                return "nwt";
            case AnonymousClass1Y3.A03 /*12*/:
                return "nwst";
            case 13:
                return "mqtt_sid";
            case 14:
                return "st";
            case 15:
                return "ct";
            case 16:
                return "aid";
            case 17:
                return "log";
            case Process.SIGCONT /*18*/:
                return "dc";
            case Process.SIGSTOP /*19*/:
                return "h";
            case 20:
                return "fbnsck";
            case AnonymousClass1Y3.A05 /*21*/:
                return "fbnscs";
            case AnonymousClass1Y3.A06 /*22*/:
                return "fbnsdi";
            case 23:
                return "fbnsds";
            case AnonymousClass1Y3.A07 /*24*/:
                return "luid";
            case 25:
                return "clientStack";
            case AnonymousClass1Y3.A08 /*26*/:
                return "app_specific_info";
            case AnonymousClass1Y3.A09 /*27*/:
                return "nwti";
            default:
                return "u";
        }
    }

    private static String A04(JSONObject jSONObject, Integer num) {
        if (!jSONObject.has(A03(num))) {
            return null;
        }
        return jSONObject.optString(A03(num));
    }

    public AnonymousClass0CJ(Long l, String str, Long l2, Long l3, Integer num, Integer num2, Boolean bool, Boolean bool2, String str2, String str3, Boolean bool3, Long l4, int i, String str4, String str5, List list, String str6, String str7, Byte b, Map map, Long l5, C36191sf r23) {
        this.A0C = l;
        this.A0J = str;
        this.A08 = l2;
        this.A0B = l3;
        this.A07 = num;
        this.A06 = num2;
        this.A02 = bool;
        this.A04 = bool2;
        this.A0H = str2;
        this.A0I = str3;
        this.A03 = bool3;
        this.A0A = l4;
        this.A00 = i;
        this.A0E = str4;
        this.A0D = str5;
        this.A0K = list;
        this.A0F = str6;
        this.A0G = str7;
        this.A05 = b;
        this.A0L = map;
        this.A09 = l5;
        this.A01 = r23;
    }
}
