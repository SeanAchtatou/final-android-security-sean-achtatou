package X;

import android.content.Intent;

/* renamed from: X.0AK  reason: invalid class name */
public final class AnonymousClass0AK implements AnonymousClass0AL {
    public final /* synthetic */ AnonymousClass0AH A00;

    public AnonymousClass0AK(AnonymousClass0AH r1) {
        this.A00 = r1;
    }

    public void BgN(Intent intent) {
        if (AnonymousClass0A2.A00(intent.getAction(), "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED")) {
            AnonymousClass0AH.A03(this.A00, intent);
        }
    }
}
