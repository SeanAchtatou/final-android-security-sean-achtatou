package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.fbservice.service.ServiceException;
import com.facebook.http.protocol.ApiErrorResult;
import com.facebook.proxygen.TraceFieldType;

/* renamed from: X.20s  reason: invalid class name and case insensitive filesystem */
public final class C402120s {
    public OperationResult A00;
    public ServiceException A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    private String A05;
    private boolean A06;
    public final C402320u A07;
    public final String A08;
    private final C17410yr A09 = new AnonymousClass2SI(this);
    private final C13460rT A0A;
    private final AnonymousClass2Wj A0B;
    private final AnonymousClass2XE A0C;
    private final C47652Wu A0D;
    private final String A0E;
    private final String A0F;

    public static void A00(C402120s r4, OperationResult operationResult) {
        String str;
        AnonymousClass2Wj r3 = r4.A0B;
        if (!(r3 == null || (str = r4.A05) == null)) {
            if (!str.endsWith("_success")) {
                str = AnonymousClass08S.A0J(str, "_success");
            }
            AnonymousClass2Wj.A02(r3, str, null);
            r4.A05 = null;
        }
        C47652Wu r2 = r4.A0D;
        if (r2 != null) {
            r2.Bqk(r4.A0E, (Parcelable) operationResult.A08());
        }
        r4.A0A.A2O(r4.A09);
    }

    public static void A01(C402120s r6, ServiceException serviceException) {
        String str;
        ApiErrorResult apiErrorResult;
        String str2;
        AnonymousClass2Wj r4 = r6.A0B;
        if (!(r4 == null || (str = r6.A05) == null)) {
            if (!str.endsWith("_failure")) {
                str = AnonymousClass08S.A0J(str, "_failure");
            }
            AnonymousClass26K A002 = AnonymousClass26K.A00();
            if (serviceException != null) {
                C14880uI r5 = serviceException.errorCode;
                A002.A04(TraceFieldType.ErrorCode, r5.toString());
                r5.toString();
                if (r5 == C14880uI.A01 && (apiErrorResult = (ApiErrorResult) serviceException.result.A08()) != null) {
                    int A022 = apiErrorResult.A02();
                    if (A022 == 406) {
                        str2 = "_two_fac_required";
                    } else {
                        if (A022 == 405) {
                            str2 = "_checkpoint_required";
                        }
                        A002.A01("api_error_code", A022);
                    }
                    str = AnonymousClass08S.A0J(str, str2);
                    A002.A01("api_error_code", A022);
                }
            }
            AnonymousClass2Wj.A02(r4, str, A002);
            r6.A05 = null;
        }
        AnonymousClass2XE r3 = r6.A0C;
        if (r3 != null) {
            AnonymousClass00S.A04((Handler) AnonymousClass1XX.A03(AnonymousClass1Y3.Amu, r3.A00), new AnonymousClass7PP(r3, serviceException), -1824733581);
        }
        C47652Wu r1 = r6.A0D;
        if (r1 != null) {
            r1.BYf(r6.A0E, serviceException);
        }
        r6.A0A.A2O(r6.A09);
    }

    public boolean A02(Bundle bundle, int i, String str) {
        C156107Jr r1;
        if (this.A07.A2M() || this.A0F.isEmpty()) {
            return false;
        }
        this.A0A.A2N(this.A09);
        this.A02 = this.A0A.A1Z();
        if (!(this.A0B == null || str == null || TextUtils.isEmpty(str))) {
            this.A05 = str;
            AnonymousClass2Wj r2 = this.A0B;
            if (!str.endsWith("_request")) {
                str = AnonymousClass08S.A0J(str, "_request");
            }
            AnonymousClass2Wj.A02(r2, str, null);
        }
        if (i != 0) {
            if (this.A06) {
                r1 = new AnonymousClass5t1(this.A0A.A1j(), i);
            } else {
                r1 = new C38961yH(this.A0A.A1j(), i);
            }
            this.A07.A2J(r1);
        }
        this.A07.A2K(this.A0F, bundle);
        return true;
    }

    public boolean A03(Parcelable parcelable, int i, String str) {
        Bundle bundle = new Bundle();
        String str2 = this.A08;
        if (str2 != null && !str2.isEmpty()) {
            bundle.putParcelable(str2, parcelable);
        }
        return A02(bundle, i, str);
    }

    public C402120s(C13460rT r2, String str, String str2, String str3, AnonymousClass2XE r6, C47652Wu r7, AnonymousClass2Wj r8, boolean z) {
        this.A0A = r2;
        this.A0F = str;
        this.A0E = str2;
        this.A08 = str3;
        this.A0C = r6;
        this.A0D = r7;
        this.A07 = C402320u.A00(r2, str2);
        this.A06 = z;
        this.A0B = r8;
    }
}
