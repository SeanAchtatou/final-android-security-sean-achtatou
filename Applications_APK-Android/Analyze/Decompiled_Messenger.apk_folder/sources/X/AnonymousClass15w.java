package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.DeadObjectException;
import java.util.List;

/* renamed from: X.15w  reason: invalid class name */
public final class AnonymousClass15w extends C28131eJ {
    public String A00 = null;
    public final C28141eK A01;
    private final List A02;

    public static Intent A01(AnonymousClass15w r4, Intent intent, Context context) {
        if (context == null || r4.A02.isEmpty()) {
            return intent;
        }
        for (C83873yM r2 : r4.A02) {
            if (r2.getApplicableScopeType$REDEX$eeEbB34xg12() == r4.A01.A0E() && r2.isEligible(intent, context) && (intent = r2.apply(intent, context)) == null) {
                return null;
            }
        }
        return intent;
    }

    public ComponentName A08(Intent intent, Context context) {
        Intent A0D = this.A01.A0D(intent, context, this.A00);
        this.A00 = null;
        if (A0D == null) {
            return null;
        }
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 26) {
            z = true;
        }
        if (z) {
            return A00(context, A0D);
        }
        return context.startService(A0D);
    }

    public void A0C(Intent intent, Context context) {
        List<Intent> A0G = this.A01.A0G(intent, context, this.A00);
        this.A00 = null;
        if (!A0G.isEmpty()) {
            for (Intent sendBroadcast : A0G) {
                try {
                    context.sendBroadcast(sendBroadcast);
                } catch (RuntimeException e) {
                    if (!(e.getCause() instanceof DeadObjectException)) {
                        throw e;
                    }
                }
            }
        }
    }

    public void A0D(Intent intent, Bundle bundle, Context context) {
        Intent A0B = this.A01.A0B(intent, context, this.A00);
        this.A00 = null;
        if (A0B != null) {
            boolean z = false;
            if (Build.VERSION.SDK_INT >= 16) {
                z = true;
            }
            if (!z && bundle != null) {
                this.A01.A00.C2S("Warning: launching intents with a bundle on API < 16 will crash the app.");
            }
            Intent A012 = A01(this, A0B, context);
            if (A012 != null) {
                AnonymousClass01R.A05(context, A012, bundle);
            }
        }
    }

    public AnonymousClass15w(C28141eK r2, List list) {
        this.A01 = r2;
        this.A02 = list;
    }

    public static ComponentName A00(Context context, Intent intent) {
        ComponentName startForegroundService = context.startForegroundService(intent);
        C011409b r0 = C013309u.A00;
        if (r0 != null) {
            r0.CH0(startForegroundService, intent);
        }
        return startForegroundService;
    }
}
