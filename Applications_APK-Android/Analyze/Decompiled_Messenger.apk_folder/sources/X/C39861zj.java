package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1zj  reason: invalid class name and case insensitive filesystem */
public final class C39861zj {
    public final C08500fS A00;
    public final C08490fR A01;
    public final ThreadLocal A02 = new ThreadLocal();
    private final AnonymousClass06B A03;
    private final AnonymousClass09O A04;
    private final C25401Zm A05;
    private final C08510fT A06;
    private final Random A07;
    private final C04310Tq A08;
    private final C08230et[] A09;

    private PerformanceLoggingEvent A00(int i, long j, TimeUnit timeUnit) {
        int BzU;
        int i2 = i;
        int B1q = this.A05.B1q(i);
        if (B1q == -1 || (BzU = this.A05.BzU(B1q)) == Integer.MAX_VALUE) {
            return null;
        }
        return A01(i2, j, timeUnit, BzU);
    }

    private PerformanceLoggingEvent A01(int i, long j, TimeUnit timeUnit, int i2) {
        PerformanceLoggingEvent performanceLoggingEvent = (PerformanceLoggingEvent) PerformanceLoggingEvent.A0c.A01();
        performanceLoggingEvent.A08 = timeUnit.toNanos(j);
        performanceLoggingEvent.A0R = 2;
        performanceLoggingEvent.A0C = this.A03.now();
        performanceLoggingEvent.A0B = this.A04.nowNanos();
        performanceLoggingEvent.A05 = i2;
        performanceLoggingEvent.A02 = this.A07.nextInt();
        performanceLoggingEvent.A06 = i;
        performanceLoggingEvent.A0Y = false;
        performanceLoggingEvent.A0X = false;
        performanceLoggingEvent.A0L = (AnonymousClass1J8) this.A08.get();
        return performanceLoggingEvent;
    }

    public static PerformanceLoggingEvent A02(C39861zj r8, String str, Ed3 ed3, long j) {
        PerformanceLoggingEvent A012 = r8.A01(27787270, j, TimeUnit.MICROSECONDS, ed3.A02);
        A012.A0A("type", str);
        A012.A05("real_marker_id", (long) ed3.A00);
        A012.A05("nanoseconds_value", j);
        return A012;
    }

    public static void A03(C39861zj r5, Ed3 ed3, PerformanceLoggingEvent performanceLoggingEvent) {
        if (r5.A09 != null) {
            boolean z = false;
            if (ed3.A0A.size() > 0) {
                z = true;
            }
            performanceLoggingEvent.A0C("metadata_was_used", z);
            for (int i = 0; i < ed3.A09.size(); i++) {
                int intValue = ((Integer) ed3.A09.get(i)).intValue();
                performanceLoggingEvent.A05(AnonymousClass08S.A0J("metadata_", r5.A09[intValue].Azg()), ((Long) ed3.A0A.get(i)).longValue());
            }
        }
    }

    public static boolean A04(C39861zj r2) {
        Boolean bool = (Boolean) r2.A02.get();
        if (bool != null && bool.booleanValue()) {
            return false;
        }
        r2.A02.set(true);
        return true;
    }

    public boolean A06(int i, String str, int i2) {
        PerformanceLoggingEvent A002 = A00(i, 0, TimeUnit.NANOSECONDS);
        if (A002 == null) {
            return false;
        }
        A002.A05(str, (long) i2);
        this.A01.A00(A002);
        return true;
    }

    public C39861zj(C08490fR r2, C04310Tq r3, C25401Zm r4, AnonymousClass06B r5, AnonymousClass09O r6, Random random, C08500fS r8, C08230et[] r9) {
        this.A01 = r2;
        this.A08 = r3;
        this.A05 = r4;
        this.A03 = r5;
        this.A09 = r9;
        this.A06 = new C08510fT();
        this.A04 = r6;
        this.A07 = random;
        this.A00 = r8;
    }

    public void A05(int i, long j, TimeUnit timeUnit, String[] strArr, int[] iArr, int[] iArr2) {
        PerformanceLoggingEvent A002 = A00(i, j, timeUnit);
        if (A002 != null) {
            int i2 = 0;
            while (true) {
                boolean z = true;
                if (i2 < strArr.length) {
                    int i3 = iArr2[i2];
                    if (i3 == 2) {
                        A002.A05(strArr[i2], (long) iArr[i2]);
                    } else if (i3 == 7) {
                        String str = strArr[i2];
                        if (iArr[i2] == 0) {
                            z = false;
                        }
                        A002.A0C(str, z);
                    } else {
                        throw new UnsupportedOperationException(AnonymousClass08S.A0A("Annotation type ", i3, " is not supported yet. Add support on your own."));
                    }
                    i2++;
                } else {
                    this.A01.A00(A002);
                    return;
                }
            }
        }
    }
}
