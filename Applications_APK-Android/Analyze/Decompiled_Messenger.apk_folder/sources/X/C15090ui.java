package X;

import android.os.Build;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ui  reason: invalid class name and case insensitive filesystem */
public final class C15090ui {
    private static volatile C15090ui A02;
    public final C001500z A00;
    public final C25051Yd A01;

    public static final C15090ui A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C15090ui.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C15090ui(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public boolean A01() {
        if (this.A00 != C001500z.A07 || Build.VERSION.SDK_INT < 26) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (android.os.Build.VERSION.SDK_INT < 26) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03() {
        /*
            r3 = this;
            X.00z r1 = r3.A00
            X.00z r0 = X.C001500z.A07
            if (r1 != r0) goto L_0x000d
            int r2 = android.os.Build.VERSION.SDK_INT
            r1 = 26
            r0 = 1
            if (r2 >= r1) goto L_0x000e
        L_0x000d:
            r0 = 0
        L_0x000e:
            if (r0 != 0) goto L_0x0017
            boolean r1 = r3.A04()
            r0 = 0
            if (r1 == 0) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15090ui.A03():boolean");
    }

    public boolean A04() {
        if (this.A00 != C001500z.A0C || Build.VERSION.SDK_INT < 26) {
            return false;
        }
        return true;
    }

    private C15090ui(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0WT.A00(r2);
        this.A00 = AnonymousClass0UU.A05(r2);
        AnonymousClass1YA.A02(r2);
    }

    public boolean A02() {
        if (!A03() || !this.A01.Aem(282793531672439L)) {
            return false;
        }
        return true;
    }

    public boolean A05() {
        if (!A03() || !this.A01.Aem(2306125639537067719L)) {
            return false;
        }
        return true;
    }

    public boolean A06() {
        if (!A03() || !this.A01.Aem(2306125639536608962L)) {
            return false;
        }
        return true;
    }
}
