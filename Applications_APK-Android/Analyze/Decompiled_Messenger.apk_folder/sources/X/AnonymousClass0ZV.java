package X;

import android.app.Notification;
import android.os.Build;

/* renamed from: X.0ZV  reason: invalid class name */
public final class AnonymousClass0ZV extends C06570bi {
    private CharSequence A00;

    public void A00(C71303cA r3) {
        if (Build.VERSION.SDK_INT >= 16) {
            Notification.BigTextStyle bigText = new Notification.BigTextStyle(r3.AfO()).setBigContentTitle(this.A01).bigText(this.A00);
            if (this.A03) {
                bigText.setSummaryText(this.A02);
            }
        }
    }

    public void A01(CharSequence charSequence) {
        this.A00 = AnonymousClass0ZN.A00(charSequence);
    }

    public AnonymousClass0ZV() {
    }

    public AnonymousClass0ZV(AnonymousClass0ZN r2) {
        if (this.A00 != r2) {
            this.A00 = r2;
            if (r2 != null) {
                r2.A0A(this);
            }
        }
    }
}
