package X;

/* renamed from: X.0uF  reason: invalid class name and case insensitive filesystem */
public final class C14850uF implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.presence.DefaultPresenceManager$1";
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C14850uF(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    public void run() {
        AnonymousClass0r6 r1 = this.A00;
        r1.A06 = null;
        if (AnonymousClass0r6.A0I(r1, false)) {
            AnonymousClass0r6 r2 = this.A00;
            r2.A00 = -1;
            AnonymousClass0r6.A09(r2, C13430rQ.TP_DISABLED);
            AnonymousClass0r6.A0B(r2, "/t_p");
        }
    }
}
