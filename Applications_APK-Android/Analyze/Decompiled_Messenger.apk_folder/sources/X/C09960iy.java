package X;

import android.os.Build;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.0iy  reason: invalid class name and case insensitive filesystem */
public final class C09960iy {
    private static volatile C09960iy A02;
    public AnonymousClass0UN A00;
    private Boolean A01 = null;

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "SMILEYS_AND_PEOPLE";
            case 2:
                return "ANIMALS_AND_NATURE";
            case 3:
                return "FOOD_AND_DRINK";
            case 4:
                return "TRAVEL_AND_PLACES";
            case 5:
                return "ACTIVITIES";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return "OBJECTS";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "SYMBOLS";
            case 8:
                return "FLAGS";
            case Process.SIGKILL:
                return "OTHER";
            default:
                return C99084oO.$const$string(AnonymousClass1Y3.A0v);
        }
    }

    public static final C09960iy A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C09960iy.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C09960iy(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0009  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C25824Cmp A02(X.C25818Cmj r6) {
        /*
            r5 = this;
            java.lang.Integer r4 = r6.A00
            int r0 = r4.intValue()
            switch(r0) {
                case 0: goto L_0x0039;
                case 1: goto L_0x0035;
                case 2: goto L_0x0031;
                case 3: goto L_0x002d;
                case 4: goto L_0x0029;
                case 5: goto L_0x0025;
                case 6: goto L_0x0021;
                case 7: goto L_0x001d;
                case 8: goto L_0x0019;
                default: goto L_0x0009;
            }
        L_0x0009:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r1 = "Unknown emoji category: "
            java.lang.String r0 = A01(r4)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x0019:
            r3 = 2132279309(0x7f18000d, float:2.0204292E38)
            goto L_0x004c
        L_0x001d:
            r3 = 2132279315(0x7f180013, float:2.0204304E38)
            goto L_0x004c
        L_0x0021:
            r3 = 2132279312(0x7f180010, float:2.0204298E38)
            goto L_0x004c
        L_0x0025:
            r3 = 2132279306(0x7f18000a, float:2.0204286E38)
            goto L_0x004c
        L_0x0029:
            r3 = 2132279316(0x7f180014, float:2.0204306E38)
            goto L_0x004c
        L_0x002d:
            r3 = 2132279310(0x7f18000e, float:2.0204294E38)
            goto L_0x004c
        L_0x0031:
            r3 = 2132279311(0x7f18000f, float:2.0204296E38)
            goto L_0x004c
        L_0x0035:
            r3 = 2132279308(0x7f18000c, float:2.020429E38)
            goto L_0x004c
        L_0x0039:
            X.0UN r2 = r5.A00
            r1 = 2
            int r0 = X.AnonymousClass1Y3.AJa
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.1MT r2 = (X.AnonymousClass1MT) r2
            X.1gj r1 = X.C29631gj.A0P
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            int r3 = r2.A03(r1, r0)
        L_0x004c:
            int r0 = r4.intValue()
            switch(r0) {
                case 0: goto L_0x0085;
                case 1: goto L_0x007e;
                case 2: goto L_0x0077;
                case 3: goto L_0x0070;
                case 4: goto L_0x0069;
                case 5: goto L_0x0062;
                case 6: goto L_0x005b;
                case 7: goto L_0x0054;
                case 8: goto L_0x008c;
                default: goto L_0x0053;
            }
        L_0x0053:
            goto L_0x0009
        L_0x0054:
            r2 = 2131824224(0x7f110e60, float:1.928127E38)
            r1 = 2131824225(0x7f110e61, float:1.9281272E38)
            goto L_0x0092
        L_0x005b:
            r2 = 2131824211(0x7f110e53, float:1.9281243E38)
            r1 = 2131824212(0x7f110e54, float:1.9281246E38)
            goto L_0x0092
        L_0x0062:
            r2 = 2131824194(0x7f110e42, float:1.928121E38)
            r1 = 2131824195(0x7f110e43, float:1.9281211E38)
            goto L_0x0092
        L_0x0069:
            r2 = 2131824226(0x7f110e62, float:1.9281274E38)
            r1 = 2131824227(0x7f110e63, float:1.9281276E38)
            goto L_0x0092
        L_0x0070:
            r2 = 2131824206(0x7f110e4e, float:1.9281233E38)
            r1 = 2131824207(0x7f110e4f, float:1.9281235E38)
            goto L_0x0092
        L_0x0077:
            r2 = 2131824208(0x7f110e50, float:1.9281237E38)
            r1 = 2131824209(0x7f110e51, float:1.928124E38)
            goto L_0x0092
        L_0x007e:
            r2 = 2131824201(0x7f110e49, float:1.9281223E38)
            r1 = 2131824202(0x7f110e4a, float:1.9281225E38)
            goto L_0x0092
        L_0x0085:
            r2 = 2131824213(0x7f110e55, float:1.9281248E38)
            r1 = 2131824214(0x7f110e56, float:1.928125E38)
            goto L_0x0092
        L_0x008c:
            r2 = 2131824204(0x7f110e4c, float:1.928123E38)
            r1 = 2131824205(0x7f110e4d, float:1.9281231E38)
        L_0x0092:
            X.Cmp r0 = new X.Cmp
            r0.<init>(r6, r3, r2, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09960iy.A02(X.Cmj):X.Cmp");
    }

    public boolean A03() {
        Boolean bool;
        if (this.A01 == null) {
            if (Build.VERSION.SDK_INT >= 19) {
                int i = AnonymousClass1Y3.AdI;
                ((AnonymousClass0iR) AnonymousClass1XX.A02(0, i, this.A00)).A01.A03();
                if (((AnonymousClass0iR) AnonymousClass1XX.A02(0, i, this.A00)).A01() != null) {
                    bool = Boolean.TRUE;
                    this.A01 = bool;
                }
            }
            bool = Boolean.FALSE;
            this.A01 = bool;
        }
        return this.A01.booleanValue();
    }

    public boolean A04() {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return false;
    }

    private C09960iy(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
