package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1vJ  reason: invalid class name */
public final class AnonymousClass1vJ extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public DGF A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 5)
    public ImmutableList A03;
    public C04310Tq A04;
    @Comparable(type = 3)
    public boolean A05;

    public AnonymousClass1vJ(Context context) {
        super("M4MessengerAvailabilitySettingLayout");
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A00 = new AnonymousClass0UN(2, r2);
        this.A04 = AnonymousClass0XJ.A0H(r2);
    }
}
