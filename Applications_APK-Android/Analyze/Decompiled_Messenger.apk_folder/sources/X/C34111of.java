package X;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1of  reason: invalid class name and case insensitive filesystem */
public final class C34111of {
    private static volatile C34111of A02;
    private AnonymousClass0UN A00;
    public final Map A01 = new HashMap();

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        r1 = X.AnonymousClass1Y3.BBd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0039, code lost:
        if (((com.facebook.quicklog.QuickPerformanceLogger) X.AnonymousClass1XX.A02(0, r1, r4.A00)).isMarkerOn(3997707) == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003b, code lost:
        r1 = (com.facebook.quicklog.QuickPerformanceLogger) X.AnonymousClass1XX.A02(0, r1, r4.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0047, code lost:
        if (com.google.common.base.Platform.stringIsNullOrEmpty(r5) == false) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0049, code lost:
        r5 = "UnknownSurface";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004b, code lost:
        if (r6 == false) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004d, code lost:
        r0 = ":Enter";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        r1.markerPoint(3997707, X.AnonymousClass08S.A0J(r5, r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0057, code lost:
        r0 = ":Exit";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (((java.lang.Boolean) r4.A01.get(r5)).booleanValue() == false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C34111of r4, java.lang.String r5, boolean r6) {
        /*
            monitor-enter(r4)
            java.util.Map r0 = r4.A01     // Catch:{ all -> 0x005a }
            boolean r0 = r0.containsKey(r5)     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0018
            java.util.Map r0 = r4.A01     // Catch:{ all -> 0x005a }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x005a }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x005a }
            boolean r1 = r0.booleanValue()     // Catch:{ all -> 0x005a }
            r0 = 1
            if (r1 != 0) goto L_0x0019
        L_0x0018:
            r0 = 0
        L_0x0019:
            if (r0 != r6) goto L_0x001d
            monitor-exit(r4)     // Catch:{ all -> 0x005a }
            return
        L_0x001d:
            java.util.Map r1 = r4.A01     // Catch:{ all -> 0x005a }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x005a }
            r1.put(r5, r0)     // Catch:{ all -> 0x005a }
            monitor-exit(r4)     // Catch:{ all -> 0x005a }
            int r1 = X.AnonymousClass1Y3.BBd
            X.0UN r0 = r4.A00
            r3 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0
            r2 = 3997707(0x3d000b, float:5.60198E-39)
            boolean r0 = r0.isMarkerOn(r2)
            if (r0 == 0) goto L_0x0056
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 == 0) goto L_0x004b
            java.lang.String r5 = "UnknownSurface"
        L_0x004b:
            if (r6 == 0) goto L_0x0057
            java.lang.String r0 = ":Enter"
        L_0x004f:
            java.lang.String r0 = X.AnonymousClass08S.A0J(r5, r0)
            r1.markerPoint(r2, r0)
        L_0x0056:
            return
        L_0x0057:
            java.lang.String r0 = ":Exit"
            goto L_0x004f
        L_0x005a:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x005a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34111of.A01(X.1of, java.lang.String, boolean):void");
    }

    public static final C34111of A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C34111of.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C34111of(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C34111of(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
