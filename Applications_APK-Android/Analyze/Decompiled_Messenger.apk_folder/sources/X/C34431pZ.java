package X;

import com.facebook.common.stringformat.StringFormatUtil;

/* renamed from: X.1pZ  reason: invalid class name and case insensitive filesystem */
public final class C34431pZ implements C13330rF {
    private final long A00;
    private final String A01;
    private final Object[] A02;

    public String Aae() {
        return StringFormatUtil.formatStrLocaleSafe(this.A01, this.A02);
    }

    public long getStartTime() {
        return this.A00;
    }

    public C34431pZ(long j, String str, Object... objArr) {
        this.A00 = j;
        this.A01 = str;
        this.A02 = objArr;
    }
}
