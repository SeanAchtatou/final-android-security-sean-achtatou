package X;

import android.content.Context;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.ipc.stories.model.StoryBucket;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;
import com.facebook.resources.ui.FbFrameLayout;

/* renamed from: X.1vD  reason: invalid class name */
public final class AnonymousClass1vD extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C73513gF A01;
    @Comparable(type = 13)
    public StoryBucket A02;
    @Comparable(type = 13)
    public StoryCard A03;
    @Comparable(type = 13)
    public C88334Jw A04;
    @Comparable(type = 13)
    public C90124Rw A05;
    @Comparable(type = 13)
    public AnonymousClass4O4 A06;
    @Comparable(type = 13)
    public AnonymousClass46A A07;
    @Comparable(type = 13)
    public String A08;
    @LoggedInUser
    public C04310Tq A09;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1vD(Context context) {
        super("StoryViewerPollStickerComponent");
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A00 = new AnonymousClass0UN(4, r2);
        this.A09 = AnonymousClass0WY.A01(r2);
    }

    public static void A00(FbFrameLayout fbFrameLayout, C95464gT r3) {
        boolean z = false;
        if (r3.A04 != null) {
            z = true;
        }
        if (z) {
            fbFrameLayout.findViewById(2131298005);
        }
    }
}
