package X;

import java.util.Arrays;
import java.util.Collection;

/* renamed from: X.0ax  reason: invalid class name and case insensitive filesystem */
public final class C06160ax {
    public static C25601a6 A00() {
        return new AnonymousClass0j1();
    }

    public static C25601a6 A01(C06140av... r4) {
        AnonymousClass0j1 r3 = new AnonymousClass0j1();
        for (C06140av A05 : r4) {
            r3.A05(A05);
        }
        return r3;
    }

    public static C25601a6 A02(C06140av... r4) {
        C12890qA r3 = new C12890qA();
        for (C06140av A05 : r4) {
            r3.A05(A05);
        }
        return r3;
    }

    public static C06140av A03(String str, String str2) {
        return new AnonymousClass0XK(str, str2);
    }

    public static C06140av A04(String str, Collection collection) {
        return new AnonymousClass1CE(str, collection, false);
    }

    public static C06140av A05(String str, String... strArr) {
        return new AnonymousClass1CE(str, Arrays.asList(strArr), true);
    }
}
