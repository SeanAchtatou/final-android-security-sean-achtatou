package X;

import android.os.Handler;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;

/* renamed from: X.0Jl  reason: invalid class name */
public final class AnonymousClass0Jl extends AnonymousClass0KC implements RunnableFuture {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.common.executors.HandlerExecutorServiceImpl$ListenableScheduledRunnableFuture";

    public AnonymousClass0Jl(Handler handler, Runnable runnable, Object obj) {
        super(handler, runnable, obj);
    }

    public AnonymousClass0Jl(Handler handler, Callable callable) {
        super(handler, callable);
    }
}
