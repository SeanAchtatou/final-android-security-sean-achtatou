package X;

import android.content.Intent;
import android.net.Uri;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.messaging.media.download.DownloadedMedia;
import com.facebook.messaging.media.viewer.MediaViewFragment;

/* renamed from: X.20i  reason: invalid class name and case insensitive filesystem */
public final class C401120i implements C04810Wg {
    public final /* synthetic */ MediaViewFragment A00;

    public C401120i(MediaViewFragment mediaViewFragment) {
        this.A00 = mediaViewFragment;
    }

    public void BYh(Throwable th) {
        this.A00.A02.softReport("MediaViewFragment", "Could not save photo to storage for sharing", th);
        ((C67553Ph) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AI4, this.A00.A06)).A04(new C51092fR(2131824293));
    }

    public void Bqf(Object obj) {
        DownloadedMedia downloadedMedia = (DownloadedMedia) obj;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Ap7, this.A00.A06)).A01(C99084oO.$const$string(AnonymousClass1Y3.A3E)), 21);
        if (downloadedMedia != null) {
            C103194vp r1 = downloadedMedia.A01;
            if (!r1.equals(C103194vp.A02)) {
                if (!r1.equals(C103194vp.NO_PERMISSION)) {
                    MediaViewFragment mediaViewFragment = this.A00;
                    if (mediaViewFragment.A00 != null) {
                        Uri A04 = MediaViewFragment.A04(mediaViewFragment, downloadedMedia);
                        if (A04 != null) {
                            Intent intent = new Intent(C99084oO.$const$string(388));
                            intent.setType("image/jpeg");
                            intent.putExtra(AnonymousClass80H.$const$string(0), A04);
                            MediaViewFragment.A09(uSLEBaseShape0S0000000, C417626w.A08(MediaViewFragment.A00(this.A00, intent), this.A00.A00), "image/jpeg");
                            return;
                        }
                        ((C67553Ph) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AI4, this.A00.A06)).A04(new C51092fR(2131824293));
                    }
                }
                MediaViewFragment.A09(uSLEBaseShape0S0000000, false, "image/jpeg");
            }
        }
        this.A00.A02.CGS("MediaViewFragment", "Could not save photo to storage for sharing");
        ((C67553Ph) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AI4, this.A00.A06)).A04(new C51092fR(2131824293));
        MediaViewFragment.A09(uSLEBaseShape0S0000000, false, "image/jpeg");
    }
}
