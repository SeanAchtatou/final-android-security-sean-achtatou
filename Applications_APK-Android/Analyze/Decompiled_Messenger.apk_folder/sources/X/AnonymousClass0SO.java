package X;

import android.view.animation.LinearInterpolator;

/* renamed from: X.0SO  reason: invalid class name */
public final class AnonymousClass0SO extends C03550Om {
    public final /* synthetic */ AnonymousClass01E A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0SO(AnonymousClass01E r2, long j) {
        super(new LinearInterpolator(), j);
        this.A00 = r2;
    }

    public void A00(float f) {
        this.A00.A04.setScaleX(f);
        this.A00.A04.setScaleY(f);
        this.A00.A05.setScaleX(f);
        this.A00.A05.setScaleY(f);
        this.A00.A06.setScaleX(f);
        this.A00.A06.setScaleY(f);
    }
}
