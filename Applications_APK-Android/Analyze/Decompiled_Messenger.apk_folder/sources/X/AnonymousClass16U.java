package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.16U  reason: invalid class name */
public final class AnonymousClass16U implements AnonymousClass0ZM {
    public final /* synthetic */ AnonymousClass16S A00;

    public AnonymousClass16U(AnonymousClass16S r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r3) {
        AnonymousClass16S.A01(this.A00);
    }
}
