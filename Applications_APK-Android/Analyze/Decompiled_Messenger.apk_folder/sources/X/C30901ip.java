package X;

/* renamed from: X.1ip  reason: invalid class name and case insensitive filesystem */
public final class C30901ip implements C30831ii {
    public double B7D(C133746Nn r6) {
        switch (r6.ordinal()) {
            case 0:
                return 0.0d;
            case 1:
            case 2:
            case 3:
            case 4:
                return 1.0d;
            default:
                AnonymousClass02w.A0F("NativeMemoryCacheTrimStrategy", "unknown trim type: %s", r6);
                return 0.0d;
        }
    }
}
