package X;

/* renamed from: X.0OY  reason: invalid class name */
public final class AnonymousClass0OY implements C05700aB {
    public static final AnonymousClass1Y7 A00 = ((AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("http/")).A09("check_certs"));
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) ((AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("sandbox/")).A09("web/")).A09("sandbox"));

    static {
        C04350Ue.A05.A09("feed/demo_ad_invalidation");
        ((AnonymousClass1Y7) C04350Ue.A06.A09("analytics")).A09("batch_size");
        AnonymousClass1Y7 r1 = C04350Ue.A06;
        r1.A09("autoplay_eligibility");
        r1.A09("autoplay_has_user_touched_setting");
        r1.A09("video_broadcast_is_live_scribe_tool_tip_shown");
        AnonymousClass1Y7 r12 = C04350Ue.A05;
        r12.A09("feed/enable_place_save_nux_history");
        r12.A09("timeline/show_tooltips");
        r12.A09("groups/nux/login_flow");
        r12.A09("groups/nux/pog_reordering");
        r12.A09("groups/nux/create_flow");
        r12.A09("groups/nux/edit_favorites");
        r12.A09("react/dev_support");
        r12.A09("react/latest_app_key");
        r12.A09("privacy/default_privacy_enabled");
        AnonymousClass1Y7 r13 = (AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("sms_integration/")).A09("defaultapp/");
        A01 = r13;
        r13.A09("sms_in_readonly_mode");
        AnonymousClass1Y7 r14 = A01;
        r14.A09("sms_internal_no_readonly_notification");
        r14.A09("sms_readonly_set_time");
        r14.A09("sms_internal_suppress_nux");
        AnonymousClass1Y7 r15 = C04350Ue.A02;
        r15.A09("video_force_autoplay");
        r15.A09("video_logging_level");
        r15.A09("video_player_debug");
        ((AnonymousClass1Y7) C04350Ue.A06.A09("logging/")).A09("logging_level");
        AnonymousClass1Y7 r16 = C04350Ue.A05;
        r16.A09("jewels/videohome/count");
        r16.A09("jewels/watchlist/count");
    }
}
