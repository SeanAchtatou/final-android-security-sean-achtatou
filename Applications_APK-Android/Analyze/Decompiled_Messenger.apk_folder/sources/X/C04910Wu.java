package X;

import android.app.ActivityManager;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Wu  reason: invalid class name and case insensitive filesystem */
public final class C04910Wu {
    public static final Class A01 = C04910Wu.class;
    private static volatile C04910Wu A02;
    public final ActivityManager A00;

    public static final C04910Wu A00(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (C04910Wu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass1YA.A00(applicationInjector);
                        ActivityManager A04 = C04490Ux.A04(applicationInjector);
                        AnonymousClass0VG.A00(AnonymousClass1Y3.A0W, applicationInjector);
                        A02 = new C04910Wu(A04);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C04910Wu(ActivityManager activityManager) {
        this.A00 = activityManager;
    }
}
