package X;

import com.facebook.profilo.logger.Logger;
import java.io.OutputStream;

/* renamed from: X.0EO  reason: invalid class name */
public final class AnonymousClass0EO extends OutputStream {
    private int A00;
    private OutputStream A01;

    public void close() {
        this.A01.close();
    }

    public void flush() {
        this.A01.flush();
    }

    public String toString() {
        return "OutpuStreamWrapper for " + this.A01;
    }

    public AnonymousClass0EO(OutputStream outputStream, int i) {
        this.A01 = outputStream;
        this.A00 = i;
    }

    public void write(int i) {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 12, 0, 0, this.A00, 0, 0);
        try {
            this.A01.write(i);
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
        } catch (Throwable th) {
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
            throw th;
        }
    }

    public void write(byte[] bArr) {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 12, 0, 0, this.A00, 0, 0);
        try {
            this.A01.write(bArr);
            Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
        } catch (Throwable th) {
            Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
            throw th;
        }
    }

    public void write(byte[] bArr, int i, int i2) {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 12, 0, 0, this.A00, 0, 0);
        try {
            this.A01.write(bArr, i, i2);
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
        } catch (Throwable th) {
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
            throw th;
        }
    }
}
