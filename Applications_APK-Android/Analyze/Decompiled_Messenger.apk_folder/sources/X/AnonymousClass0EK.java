package X;

import java.io.File;
import java.util.zip.ZipEntry;

/* renamed from: X.0EK  reason: invalid class name */
public final class AnonymousClass0EK extends AnonymousClass0ES {
    private File A00;
    private final int A01;
    public final /* synthetic */ C003101x A02;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0EK(C003101x r3, C003201y r4) {
        super(r3, r4);
        this.A02 = r3;
        this.A00 = new File(r3.A03.getApplicationInfo().nativeLibraryDir);
        this.A01 = r3.A00;
    }

    public boolean A02(ZipEntry zipEntry, String str) {
        zipEntry.getName();
        C003101x r1 = this.A02;
        if (str.equals(r1.A00)) {
            r1.A00 = null;
        } else if ((this.A01 & 1) != 0) {
            File file = new File(this.A00, str);
            if (!file.isFile() || file.length() != zipEntry.getSize()) {
                return true;
            }
            return false;
        }
        return true;
    }
}
