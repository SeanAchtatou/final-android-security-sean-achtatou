package X;

import org.codeaurora.Performance;

/* renamed from: X.1q0  reason: invalid class name and case insensitive filesystem */
public final class C34681q0 extends C34631pv {
    private final Performance A00;

    public void A03() {
        this.A00.perfLockRelease();
    }

    public final boolean A06() {
        if (this.A00.perfLockAcquire(this.A03, this.A00) >= 0) {
            return true;
        }
        return false;
    }

    public C34681q0(Performance performance, int i, int[] iArr) {
        super(i, iArr);
        this.A00 = performance;
    }
}
