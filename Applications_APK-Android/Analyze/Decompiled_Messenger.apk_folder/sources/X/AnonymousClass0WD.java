package X;

import java.util.ArrayDeque;
import javax.inject.Singleton;

/* renamed from: X.0WD  reason: invalid class name */
public final class AnonymousClass0WD {
    private static final ArrayDeque A03 = new ArrayDeque(32);
    private C24811Xe A00;
    private AnonymousClass0UO A01;
    private Byte A02;

    public static AnonymousClass0WD A00(Object obj, AnonymousClass1XY r4) {
        AnonymousClass0WD r3;
        if (obj != null || r4 == null) {
            return null;
        }
        ArrayDeque arrayDeque = A03;
        synchronized (arrayDeque) {
            r3 = (AnonymousClass0WD) arrayDeque.pollFirst();
        }
        if (r3 == null) {
            r3 = new AnonymousClass0WD();
        }
        AnonymousClass0UO A002 = AnonymousClass0UO.A00();
        r3.A01 = A002;
        byte b = A002.A00;
        A002.A00 = (byte) (1 | b);
        r3.A02 = Byte.valueOf(b);
        r3.A00 = ((AnonymousClass0U1) r4.getScope(Singleton.class)).A00();
        return r3;
    }

    public final void A01() {
        C24811Xe r0 = this.A00;
        if (r0 != null) {
            r0.A02();
            r0.A03();
            this.A00 = null;
        }
        Byte b = this.A02;
        if (b != null) {
            this.A01.A00 = b.byteValue();
            this.A02 = null;
        }
        this.A01 = null;
        ArrayDeque arrayDeque = A03;
        synchronized (arrayDeque) {
            arrayDeque.addFirst(this);
        }
    }
}
