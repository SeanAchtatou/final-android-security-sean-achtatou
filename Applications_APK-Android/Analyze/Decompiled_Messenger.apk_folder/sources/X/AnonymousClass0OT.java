package X;

import com.facebook.rti.push.service.FbnsService;

/* renamed from: X.0OT  reason: invalid class name */
public final class AnonymousClass0OT extends AnonymousClass0AX {
    public C012309k A00;
    public AnonymousClass0RS A01;
    public AnonymousClass0RK A02;
    public AnonymousClass0RF A03;

    public void A02(C012309k r7, C01410Ac r8) {
        this.A00 = r7;
        super.A01(r8, FbnsService.A0B);
        this.A03 = new AnonymousClass0RF(this.A06, r8.A0B);
        this.A02 = new AnonymousClass0RK(r8.A05, this.A0V, r7, this.A07, r8.A0B);
        this.A01 = new AnonymousClass0RS(r8.A05, this.A0H, this.A01);
    }
}
