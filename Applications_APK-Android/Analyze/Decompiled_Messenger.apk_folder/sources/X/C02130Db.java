package X;

import android.system.ErrnoException;
import android.system.OsConstants;

/* renamed from: X.0Db  reason: invalid class name and case insensitive filesystem */
public final class C02130Db {
    public static int A00(Throwable th) {
        if (th instanceof ErrnoException) {
            return ((ErrnoException) th).errno;
        }
        return -1;
    }

    public static String A01(int i) {
        return OsConstants.errnoName(i);
    }
}
