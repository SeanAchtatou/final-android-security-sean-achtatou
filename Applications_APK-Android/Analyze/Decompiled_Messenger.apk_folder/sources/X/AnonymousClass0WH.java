package X;

import android.text.TextUtils;
import com.facebook.common.util.TriState;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Executors;

/* renamed from: X.0WH  reason: invalid class name */
public final class AnonymousClass0WH {
    public String A00;
    public boolean A01;
    private String A02;
    public final AnonymousClass0WI A03;
    public final AnonymousClass0WI A04;
    public final AnonymousClass0WC A05;
    private final AnonymousClass0X2 A06;
    private final File A07;

    private synchronized String A00() {
        if (this.A02 == null) {
            this.A02 = this.A05.Anv();
        }
        return this.A02;
    }

    private static boolean A03(int i, int i2) {
        if (i == i2) {
            return true;
        }
        C010708t.A0O("GatekeeperRepository", "The number of gatekeepers in files doesn't match: %s and %s", Integer.valueOf(i), Integer.valueOf(i2));
        return false;
    }

    public static synchronized boolean A04(AnonymousClass0WH r2) {
        boolean z;
        synchronized (r2) {
            z = !TextUtils.equals(r2.A00(), r2.A00);
        }
        return z;
    }

    public synchronized void A05(AnonymousClass0X4 r9) {
        TriState triState;
        TriState triState2;
        int i;
        int i2;
        C05410Yv r0;
        if (A02()) {
            A00();
            if (this.A06.A02()) {
                try {
                    if (!this.A01 && (r0 = (C05410Yv) this.A04.A00()) != null) {
                        this.A00 = r0.A00;
                    }
                    if (A04(this)) {
                        AnonymousClass0WC r02 = this.A05;
                        if (this.A03.A01(new C83853yK(r02.Anv(), r02.Anu()))) {
                            this.A00 = A00();
                        }
                        this.A06.A01();
                    }
                    String A002 = A00();
                    int length = r9.A02.length;
                    byte[] bArr = new byte[length];
                    for (int i3 = 0; i3 < length; i3++) {
                        synchronized (r9) {
                            triState = r9.A02[i3];
                        }
                        synchronized (r9) {
                            triState2 = r9.A01[i3];
                        }
                        switch (triState.ordinal()) {
                            case 0:
                                i = 2;
                                break;
                            case 1:
                                i = 1;
                                break;
                            case 2:
                                i = 0;
                                break;
                            default:
                                th = new IllegalArgumentException();
                                throw th;
                        }
                        switch (triState2.ordinal()) {
                            case 0:
                                i2 = 8;
                                break;
                            case 1:
                                i2 = 4;
                                break;
                            case 2:
                                i2 = 0;
                                break;
                            default:
                                th = new IllegalArgumentException();
                                throw th;
                        }
                        bArr[i3] = (byte) (i | i2);
                    }
                    this.A04.A01(new C05410Yv(A002, bArr));
                    this.A01 = true;
                    this.A06.A01();
                } catch (Throwable th) {
                    this.A06.A01();
                    throw th;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public synchronized boolean A06(AnonymousClass0X4 r10) {
        boolean z;
        z = false;
        if (!this.A01) {
            this.A01 = true;
            if (A02() && this.A06.A02()) {
                try {
                    C05410Yv r3 = (C05410Yv) this.A04.A00();
                    if (r3 != null) {
                        if (!TextUtils.equals(r3.A00, A00())) {
                            C83853yK r2 = (C83853yK) this.A03.A00();
                            if (r2 != null) {
                                String str = r3.A00;
                                String str2 = r2.A00;
                                if (!str.equals(str2)) {
                                    C010708t.A0O("GatekeeperRepository", "The hash of gatekeeper names in files doesn't match: %s and %s", str, str2);
                                } else if (A03(r3.A01.length, r2.A01.size())) {
                                    ArrayList arrayList = r2.A01;
                                    byte[] bArr = r3.A01;
                                    A63 a63 = new A63(this.A05);
                                    int size = arrayList.size();
                                    for (int i = 0; i < size; i++) {
                                        Integer num = (Integer) a63.A00.get((String) arrayList.get(i));
                                        if (num != null) {
                                            A01(r10, num.intValue(), bArr[i]);
                                        }
                                    }
                                    AnonymousClass07A.A04(Executors.newSingleThreadExecutor(), new C83843yJ(this, r10), -1188138996);
                                }
                            }
                        } else if (A03(r3.A01.length, this.A05.AwA())) {
                            byte[] bArr2 = r3.A01;
                            for (int i2 = 0; i2 < this.A05.AwA(); i2++) {
                                A01(r10, i2, bArr2[i2]);
                            }
                        }
                        this.A00 = r3.A00;
                        z = true;
                        this.A06.A01();
                    }
                    z = false;
                    this.A06.A01();
                } catch (Throwable th) {
                    this.A06.A01();
                    throw th;
                }
            }
        }
        return z;
    }

    private static void A01(AnonymousClass0X4 r2, int i, byte b) {
        boolean z = false;
        if ((b & 3) != 0) {
            z = true;
        }
        if (z) {
            boolean z2 = false;
            if ((b & 2) != 0) {
                z2 = true;
            }
            r2.A04(i, z2);
        } else {
            r2.A03(i);
        }
        boolean z3 = false;
        if ((b & 12) != 0) {
            z3 = true;
        }
        if (z3) {
            boolean z4 = false;
            if ((b & 8) != 0) {
                z4 = true;
            }
            r2.A05(i, z4);
            return;
        }
        r2.A02(i);
    }

    private boolean A02() {
        if (this.A07.exists() || this.A07.mkdirs()) {
            return true;
        }
        C010708t.A0P("GatekeeperRepository", "Cannot create working directory: %s", this.A07.getAbsoluteFile());
        return false;
    }

    private AnonymousClass0WH(AnonymousClass0WC r4, AnonymousClass0WI r5, AnonymousClass0WI r6, File file) {
        this.A05 = r4;
        this.A04 = r5;
        this.A03 = r6;
        this.A07 = file;
        this.A06 = new AnonymousClass0X2(new File(file, "file_lock"));
    }

    public AnonymousClass0WH(AnonymousClass0WC r5, File file) {
        this(r5, new AnonymousClass0WI(new C04950Wz(), file, "gk_state"), new AnonymousClass0WI(new AnonymousClass0X1(), file, "gk_names"), file);
    }
}
