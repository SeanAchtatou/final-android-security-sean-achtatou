package X;

import android.content.Context;
import android.content.IntentFilter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.06W  reason: invalid class name */
public class AnonymousClass06W extends AnonymousClass06X {
    public IntentFilter A00;
    public Collection A01;
    public final AnonymousClass04b A02;

    public final synchronized AnonymousClass06U A02(Context context, String str) {
        AnonymousClass06U r0;
        r0 = null;
        if (str != null) {
            r0 = (AnonymousClass06U) this.A02.get(str);
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r1 == false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean A08(java.lang.String r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.util.Collection r0 = r2.A01     // Catch:{ all -> 0x000f }
            if (r0 == 0) goto L_0x000c
            boolean r1 = r0.contains(r3)     // Catch:{ all -> 0x000f }
            r0 = 1
            if (r1 != 0) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            monitor-exit(r2)
            return r0
        L_0x000f:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass06W.A08(java.lang.String):boolean");
    }

    public final synchronized IntentFilter A09() {
        if (this.A00 == null) {
            this.A00 = new IntentFilter();
            int size = this.A02.size();
            for (int i = 0; i < size; i++) {
                this.A00.addAction((String) this.A02.A07(i));
            }
        }
        return this.A00;
    }

    public AnonymousClass06W(String str, AnonymousClass06U r4) {
        AnonymousClass04b r1 = new AnonymousClass04b(1);
        this.A02 = r1;
        AnonymousClass06X.A01(str);
        AnonymousClass06X.A01(r4);
        r1.put(str, r4);
    }

    public AnonymousClass06W(String str, AnonymousClass06U r4, String str2, AnonymousClass06U r6) {
        AnonymousClass04b r1 = new AnonymousClass04b(2);
        this.A02 = r1;
        AnonymousClass06X.A01(str);
        AnonymousClass06X.A01(r4);
        r1.put(str, r4);
        AnonymousClass04b r0 = this.A02;
        AnonymousClass06X.A01(str2);
        AnonymousClass06X.A01(r6);
        r0.put(str2, r6);
    }

    public AnonymousClass06W(String str, AnonymousClass06U r4, String str2, AnonymousClass06U r6, String str3, AnonymousClass06U r8) {
        AnonymousClass04b r1 = new AnonymousClass04b(3);
        this.A02 = r1;
        AnonymousClass06X.A01(str);
        AnonymousClass06X.A01(r4);
        r1.put(str, r4);
        AnonymousClass04b r0 = this.A02;
        AnonymousClass06X.A01(str2);
        AnonymousClass06X.A01(r6);
        r0.put(str2, r6);
        AnonymousClass04b r02 = this.A02;
        AnonymousClass06X.A01(str3);
        AnonymousClass06X.A01(r8);
        r02.put(str3, r8);
    }

    public AnonymousClass06W(String str, AnonymousClass06U r4, String str2, AnonymousClass06U r6, String str3, AnonymousClass06U r8, String str4, AnonymousClass06U r10, String str5, AnonymousClass06U r12) {
        AnonymousClass04b r1 = new AnonymousClass04b(5);
        this.A02 = r1;
        AnonymousClass06X.A01(str);
        AnonymousClass06X.A01(r4);
        r1.put(str, r4);
        AnonymousClass04b r0 = this.A02;
        AnonymousClass06X.A01(str2);
        AnonymousClass06X.A01(r6);
        r0.put(str2, r6);
        AnonymousClass04b r02 = this.A02;
        AnonymousClass06X.A01(str3);
        AnonymousClass06X.A01(r8);
        r02.put(str3, r8);
        AnonymousClass04b r03 = this.A02;
        AnonymousClass06X.A01(str4);
        AnonymousClass06X.A01(r10);
        r03.put(str4, r10);
        AnonymousClass04b r04 = this.A02;
        AnonymousClass06X.A01(str5);
        AnonymousClass06X.A01(r12);
        r04.put(str5, r12);
    }

    public AnonymousClass06W(Iterator it) {
        AnonymousClass06X.A01(it);
        this.A02 = new AnonymousClass04b();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (this.A02.put(entry.getKey(), entry.getValue()) != null) {
                throw new IllegalArgumentException(String.format("action '%s' found more than once in action map", entry.getKey()));
            }
        }
        if (this.A02.isEmpty()) {
            throw new IllegalArgumentException("Must include an entry for at least one action");
        }
    }
}
