package X;

import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.messaging.extensions.common.ExtensionParams;
import com.facebook.messaging.typingattribution.TypingAttributionData;
import com.facebook.proxygen.LigerSamplePolicy;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.21Q  reason: invalid class name */
public final class AnonymousClass21Q implements C54002li {
    public ExtensionParams A00;
    public C69533Xq A01;
    public ScheduledFuture A02;
    public final C69513Xo A03;
    @LoggedInUser
    public final C04310Tq A04;
    private final C67053Mt A05;
    private final Runnable A06 = new C26921DHr(this);
    private final ScheduledExecutorService A07;

    public boolean BPB() {
        return false;
    }

    public void BtN() {
    }

    public static final AnonymousClass21Q A00(AnonymousClass1XY r1) {
        return new AnonymousClass21Q(r1);
    }

    public void A01() {
        ScheduledFuture scheduledFuture = this.A02;
        if (scheduledFuture == null || scheduledFuture.isDone()) {
            this.A02 = this.A07.scheduleAtFixedRate(this.A06, 0, LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT, TimeUnit.MILLISECONDS);
        }
    }

    public void BNn() {
        ExtensionParams extensionParams = this.A00;
        if (extensionParams != null) {
            this.A05.A03(extensionParams.A04.getName(), extensionParams.A05);
        }
        ScheduledFuture scheduledFuture = this.A02;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
        }
        C69533Xq r1 = this.A01;
        if (r1 != null) {
            r1.A03(TypingAttributionData.A04);
        }
    }

    private AnonymousClass21Q(AnonymousClass1XY r2) {
        this.A03 = C69513Xo.A00(r2);
        this.A07 = AnonymousClass0UX.A0j(r2);
        this.A04 = AnonymousClass0WY.A01(r2);
        new AnonymousClass1FX(r2);
        this.A05 = new C67053Mt(r2);
    }

    public void BNo() {
        A01();
    }
}
