package X;

import android.os.Parcelable;
import java.util.List;

/* renamed from: X.0qZ  reason: invalid class name */
public final class AnonymousClass0qZ {
    public final C13020qR A00;

    public void A00() {
        C13060qW r1 = this.A00.A03;
        r1.A0G = false;
        r1.A0H = false;
        C13060qW.A0A(r1, 2);
    }

    public void A01() {
        C13060qW r1 = this.A00.A03;
        r1.A0G = false;
        r1.A0H = false;
        C13060qW.A0A(r1, 1);
    }

    public void A02() {
        C13060qW r1 = this.A00.A03;
        r1.A0G = false;
        r1.A0H = false;
        C13060qW.A0A(r1, 4);
    }

    public void A03() {
        C13060qW r1 = this.A00.A03;
        r1.A0H = true;
        C13060qW.A0A(r1, 2);
    }

    public void A04(Parcelable parcelable, List list) {
        C13060qW r3 = this.A00.A03;
        C82933wm r2 = new C82933wm(list, null, null);
        if (r3.A06 instanceof AnonymousClass0n8) {
            C13060qW.A0D(r3, new IllegalStateException("You must use restoreSaveState when your FragmentHostCallback implements ViewModelStoreOwner"));
        }
        r3.A07.A03(r2);
        r3.A0e(parcelable);
    }

    public AnonymousClass0qZ(C13020qR r1) {
        this.A00 = r1;
    }
}
