package X;

import com.facebook.tigon.TigonBodyProvider;
import com.facebook.tigon.TigonCallbacks;
import com.facebook.tigon.TigonRequestToken;
import com.facebook.tigon.iface.TigonRequest;
import java.nio.ByteBuffer;
import java.util.concurrent.Executor;

/* renamed from: X.1jp  reason: invalid class name and case insensitive filesystem */
public interface C31521jp extends AnonymousClass1VE {
    boolean isAvailable();

    TigonRequestToken sendRequest(TigonRequest tigonRequest, TigonBodyProvider tigonBodyProvider, TigonCallbacks tigonCallbacks, Executor executor);

    TigonRequestToken sendRequest(TigonRequest tigonRequest, ByteBuffer[] byteBufferArr, int i, TigonCallbacks tigonCallbacks, Executor executor);
}
