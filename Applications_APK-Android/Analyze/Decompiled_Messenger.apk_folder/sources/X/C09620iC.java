package X;

import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0iC  reason: invalid class name and case insensitive filesystem */
public final class C09620iC {
    private static volatile C09620iC A02;
    public AnonymousClass0UN A00;
    public final AtomicReference A01 = new AtomicReference(null);

    public static final C09620iC A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C09620iC.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C09620iC(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C09620iC(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
