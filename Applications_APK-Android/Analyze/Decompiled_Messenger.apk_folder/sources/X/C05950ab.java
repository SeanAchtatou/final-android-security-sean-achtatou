package X;

import android.net.Uri;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ab  reason: invalid class name and case insensitive filesystem */
public final class C05950ab {
    private static volatile C05950ab A00;

    public static final C05950ab A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C05950ab.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C05950ab();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public C06750c1 A01(Uri uri, int i) {
        if (C06980cP.A03.equals(uri)) {
            return new C06730bz(i);
        }
        if (C06980cP.A09.equals(uri)) {
            return new C25411Zn(i);
        }
        if (C06980cP.A07.equals(uri)) {
            return new C04320Ts(i);
        }
        if (C06980cP.A04.equals(uri)) {
            return new C06760c2(i);
        }
        return null;
    }
}
