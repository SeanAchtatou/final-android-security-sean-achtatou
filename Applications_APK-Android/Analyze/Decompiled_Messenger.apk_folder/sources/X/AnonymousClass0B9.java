package X;

import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0B9  reason: invalid class name */
public final class AnonymousClass0B9 {
    public final AtomicLong A00 = new AtomicLong();
    public final AtomicLong A01 = new AtomicLong();
    public final AtomicLong A02 = new AtomicLong();
    public final AtomicLong A03 = new AtomicLong();
    public final AtomicLong A04 = new AtomicLong();
    public volatile String A05;

    public C04130Sb A00(boolean z) {
        C04130Sb r3 = new C04130Sb(this.A05, this.A03.get() - this.A04.get(), this.A01.get() - this.A00.get(), this.A01.get() - this.A02.get());
        if (z) {
            this.A00.set(0);
            this.A01.set(0);
        }
        return r3;
    }
}
