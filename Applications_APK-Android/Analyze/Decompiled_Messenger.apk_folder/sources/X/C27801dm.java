package X;

import androidx.fragment.app.Fragment;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.1dm  reason: invalid class name and case insensitive filesystem */
public class C27801dm {
    public static final AnonymousClass04b A00 = new AnonymousClass04b();

    public static Class A00(ClassLoader classLoader, String str) {
        try {
            Class cls = (Class) A00.get(str);
            if (cls != null) {
                return cls;
            }
            Class<?> cls2 = Class.forName(str, false, classLoader);
            A00.put(str, cls2);
            return cls2;
        } catch (ClassNotFoundException e) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": make sure class name exists"), e);
        } catch (ClassCastException e2) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": make sure class is a valid subclass of Fragment"), e2);
        }
    }

    public Fragment A01(ClassLoader classLoader, String str) {
        if (this instanceof C27791dl) {
            return Fragment.A02(((C27791dl) this).A00.A06.A01, str, null);
        }
        try {
            return (Fragment) A00(classLoader, str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (InstantiationException e) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": make sure class name exists, is public, and has an empty constructor that is public"), e);
        } catch (IllegalAccessException e2) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": make sure class name exists, is public, and has an empty constructor that is public"), e2);
        } catch (NoSuchMethodException e3) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": could not find Fragment constructor"), e3);
        } catch (InvocationTargetException e4) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": calling Fragment constructor caused an exception"), e4);
        }
    }
}
