package X;

import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.base.Objects;
import java.util.Arrays;

/* renamed from: X.1Rw  reason: invalid class name and case insensitive filesystem */
public final class C24021Rw {
    private final int A00;
    private final ParticipantInfo A01;
    private final ThreadKey A02;
    private final String A03;
    private final String A04;
    private final boolean A05;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C24021Rw)) {
            return false;
        }
        C24021Rw r4 = (C24021Rw) obj;
        return this.A00 == r4.A00 && Objects.equal(this.A01, r4.A01) && Objects.equal(this.A02, r4.A02) && Objects.equal(this.A03, r4.A03) && Objects.equal(this.A04, r4.A04) && this.A05 == r4.A05;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A02, this.A01, this.A03, this.A04, Integer.valueOf(this.A00), Boolean.valueOf(this.A05)});
    }

    public C24021Rw(ThreadSummary threadSummary, int i, boolean z) {
        this.A02 = threadSummary.A0S;
        this.A01 = threadSummary.A0Q;
        this.A03 = threadSummary.A0p;
        this.A04 = threadSummary.A0w;
        this.A00 = i;
        this.A05 = z;
    }
}
