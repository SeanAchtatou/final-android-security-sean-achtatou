package X;

import com.google.common.base.Ascii;
import com.google.common.base.Equivalence;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.MapMakerInternalMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.0eQ  reason: invalid class name and case insensitive filesystem */
public final class C07940eQ {
    public MapMakerInternalMap.Strength A00;
    public MapMakerInternalMap.Strength A01;
    public boolean A02;
    public int A03 = -1;
    public int A04 = -1;
    public Equivalence A05;

    public MapMakerInternalMap.Strength A00() {
        return (MapMakerInternalMap.Strength) MoreObjects.firstNonNull(this.A00, MapMakerInternalMap.Strength.STRONG);
    }

    public MapMakerInternalMap.Strength A01() {
        return (MapMakerInternalMap.Strength) MoreObjects.firstNonNull(this.A01, MapMakerInternalMap.Strength.STRONG);
    }

    public ConcurrentMap A02() {
        if (!this.A02) {
            int i = this.A04;
            if (i == -1) {
                i = 16;
            }
            int i2 = this.A03;
            if (i2 == -1) {
                i2 = 4;
            }
            return new ConcurrentHashMap(i, 0.75f, i2);
        }
        MapMakerInternalMap.Strength A002 = A00();
        MapMakerInternalMap.Strength strength = MapMakerInternalMap.Strength.STRONG;
        if (A002 == strength && A01() == strength) {
            return new MapMakerInternalMap(this, C30180Er5.A00);
        }
        if (A00() == strength && A01() == MapMakerInternalMap.Strength.WEAK) {
            return new MapMakerInternalMap(this, AnonymousClass29q.A00);
        }
        if (A00() == MapMakerInternalMap.Strength.WEAK && A01() == MapMakerInternalMap.Strength.STRONG) {
            return new MapMakerInternalMap(this, C25561a2.A00);
        }
        MapMakerInternalMap.Strength A003 = A00();
        MapMakerInternalMap.Strength strength2 = MapMakerInternalMap.Strength.WEAK;
        if (A003 == strength2 && A01() == strength2) {
            return new MapMakerInternalMap(this, C30178Er3.A00);
        }
        throw new AssertionError();
    }

    public void A03(int i) {
        int i2 = this.A03;
        boolean z = true;
        boolean z2 = false;
        if (i2 == -1) {
            z2 = true;
        }
        Preconditions.checkState(z2, "concurrency level was already set to %s", i2);
        if (i <= 0) {
            z = false;
        }
        Preconditions.checkArgument(z);
        this.A03 = i;
    }

    public void A04(int i) {
        int i2 = this.A04;
        boolean z = true;
        boolean z2 = false;
        if (i2 == -1) {
            z2 = true;
        }
        Preconditions.checkState(z2, "initial capacity was already set to %s", i2);
        if (i < 0) {
            z = false;
        }
        Preconditions.checkArgument(z);
        this.A04 = i;
    }

    public void A05(Equivalence equivalence) {
        Equivalence equivalence2 = this.A05;
        boolean z = false;
        if (equivalence2 == null) {
            z = true;
        }
        Preconditions.checkState(z, "key equivalence was already set to %s", equivalence2);
        Preconditions.checkNotNull(equivalence);
        this.A05 = equivalence;
        this.A02 = true;
    }

    public void A06(MapMakerInternalMap.Strength strength) {
        MapMakerInternalMap.Strength strength2 = this.A00;
        boolean z = false;
        if (strength2 == null) {
            z = true;
        }
        Preconditions.checkState(z, "Key strength was already set to %s", strength2);
        Preconditions.checkNotNull(strength);
        this.A00 = strength;
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.A02 = true;
        }
    }

    public void A07(MapMakerInternalMap.Strength strength) {
        MapMakerInternalMap.Strength strength2 = this.A01;
        boolean z = false;
        if (strength2 == null) {
            z = true;
        }
        Preconditions.checkState(z, "Value strength was already set to %s", strength2);
        Preconditions.checkNotNull(strength);
        this.A01 = strength;
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.A02 = true;
        }
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        int i = this.A04;
        if (i != -1) {
            stringHelper.add("initialCapacity", i);
        }
        int i2 = this.A03;
        if (i2 != -1) {
            stringHelper.add("concurrencyLevel", i2);
        }
        MapMakerInternalMap.Strength strength = this.A00;
        if (strength != null) {
            stringHelper.add(C22298Ase.$const$string(83), Ascii.toLowerCase(strength.toString()));
        }
        MapMakerInternalMap.Strength strength2 = this.A01;
        if (strength2 != null) {
            stringHelper.add(C22298Ase.$const$string(AnonymousClass1Y3.A0j), Ascii.toLowerCase(strength2.toString()));
        }
        if (this.A05 != null) {
            stringHelper.addValue("keyEquivalence");
        }
        return stringHelper.toString();
    }
}
