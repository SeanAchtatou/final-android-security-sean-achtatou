package X;

import android.graphics.Rect;
import android.graphics.RectF;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.fragment.app.Fragment;
import androidx.transition.FragmentTransitionSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.0wt  reason: invalid class name and case insensitive filesystem */
public abstract class C16340wt {
    public Object A04(Object obj) {
        if (!(this instanceof FragmentTransitionSupport)) {
            if (obj != null) {
                return ((Transition) obj).clone();
            }
            return null;
        } else if (obj != null) {
            return ((C14030sU) obj).clone();
        } else {
            return null;
        }
    }

    public Object A05(Object obj) {
        if (!(this instanceof FragmentTransitionSupport)) {
            if (obj == null) {
                return null;
            }
            TransitionSet transitionSet = new TransitionSet();
            transitionSet.addTransition((Transition) obj);
            return transitionSet;
        } else if (obj == null) {
            return null;
        } else {
            C16360wv r0 = new C16360wv();
            r0.A0h((C14030sU) obj);
            return r0;
        }
    }

    public Object A06(Object obj, Object obj2, Object obj3) {
        if (!(this instanceof FragmentTransitionSupport)) {
            TransitionSet transitionSet = new TransitionSet();
            if (obj != null) {
                transitionSet.addTransition((Transition) obj);
            }
            if (obj2 != null) {
                transitionSet.addTransition((Transition) obj2);
            }
            if (obj3 != null) {
                transitionSet.addTransition((Transition) obj3);
            }
            return transitionSet;
        }
        C16360wv r0 = new C16360wv();
        if (obj != null) {
            r0.A0h((C14030sU) obj);
        }
        if (obj2 != null) {
            r0.A0h((C14030sU) obj2);
        }
        if (obj3 != null) {
            r0.A0h((C14030sU) obj3);
        }
        return r0;
    }

    public void A07(ViewGroup viewGroup, Object obj) {
        if (!(this instanceof FragmentTransitionSupport)) {
            TransitionManager.beginDelayedTransition(viewGroup, (Transition) obj);
        } else {
            AnonymousClass914.A02(viewGroup, (C14030sU) obj);
        }
    }

    public void A09(Object obj, Rect rect) {
        if (!(this instanceof FragmentTransitionSupport)) {
            if (obj != null) {
                ((Transition) obj).setEpicenterCallback(new C27904Dl1(rect));
            }
        } else if (obj != null) {
            ((C14030sU) obj).A0T(new C29861h6(rect));
        }
    }

    public void A0A(Object obj, View view) {
        if (!(this instanceof FragmentTransitionSupport)) {
            if (obj != null) {
                ((Transition) obj).addTarget(view);
            }
        } else if (obj != null) {
            ((C14030sU) obj).A0B(view);
        }
    }

    public void A0B(Object obj, View view) {
        if (!(this instanceof FragmentTransitionSupport)) {
            if (obj != null) {
                ((Transition) obj).removeTarget(view);
            }
        } else if (obj != null) {
            ((C14030sU) obj).A0C(view);
        }
    }

    public void A0C(Object obj, View view) {
        if (!(this instanceof FragmentTransitionSupport)) {
            if (view != null) {
                Rect rect = new Rect();
                A01(view, rect);
                ((Transition) obj).setEpicenterCallback(new C27905Dl2(rect));
            }
        } else if (view != null) {
            Rect rect2 = new Rect();
            A01(view, rect2);
            ((C14030sU) obj).A0T(new C17140yO(rect2));
        }
    }

    public void A0D(Object obj, View view, ArrayList arrayList) {
        if (!(this instanceof FragmentTransitionSupport)) {
            ((Transition) obj).addListener(new EX0(view, arrayList));
        } else {
            ((C14030sU) obj).A0E(new C17150yP(view, arrayList));
        }
    }

    public void A0E(Object obj, View view, ArrayList arrayList) {
        if (!(this instanceof FragmentTransitionSupport)) {
            C37111ui r4 = (C37111ui) this;
            TransitionSet transitionSet = (TransitionSet) obj;
            List<View> targets = transitionSet.getTargets();
            targets.clear();
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                A02(targets, (View) arrayList.get(i));
            }
            targets.add(view);
            arrayList.add(view);
            r4.A0G(transitionSet, arrayList);
            return;
        }
        FragmentTransitionSupport fragmentTransitionSupport = (FragmentTransitionSupport) this;
        C16360wv r6 = (C16360wv) obj;
        ArrayList arrayList2 = r6.A0E;
        arrayList2.clear();
        int size2 = arrayList.size();
        for (int i2 = 0; i2 < size2; i2++) {
            A02(arrayList2, (View) arrayList.get(i2));
        }
        arrayList2.add(view);
        arrayList.add(view);
        fragmentTransitionSupport.A0G(r6, arrayList);
    }

    public void A0F(Object obj, Object obj2, ArrayList arrayList, Object obj3, ArrayList arrayList2, Object obj4, ArrayList arrayList3) {
        ArrayList arrayList4 = arrayList;
        Object obj5 = obj2;
        Object obj6 = obj3;
        ArrayList arrayList5 = arrayList2;
        ArrayList arrayList6 = arrayList3;
        Object obj7 = obj4;
        if (!(this instanceof FragmentTransitionSupport)) {
            ((Transition) obj).addListener(new EX1((C37111ui) this, obj5, arrayList4, obj6, arrayList5, obj7, arrayList6));
        } else {
            ((C14030sU) obj).A0E(new C17370yn((FragmentTransitionSupport) this, obj5, arrayList4, obj6, arrayList5, obj7, arrayList6));
        }
    }

    public void A0G(Object obj, ArrayList arrayList) {
        if (!(this instanceof FragmentTransitionSupport)) {
            C37111ui r3 = (C37111ui) this;
            Transition transition = (Transition) obj;
            if (transition != null) {
                int i = 0;
                if (transition instanceof TransitionSet) {
                    TransitionSet transitionSet = (TransitionSet) transition;
                    int transitionCount = transitionSet.getTransitionCount();
                    while (i < transitionCount) {
                        r3.A0G(transitionSet.getTransitionAt(i), arrayList);
                        i++;
                    }
                } else if (!C37111ui.A00(transition) && A03(transition.getTargets())) {
                    int size = arrayList.size();
                    while (i < size) {
                        transition.addTarget((View) arrayList.get(i));
                        i++;
                    }
                }
            }
        } else {
            FragmentTransitionSupport fragmentTransitionSupport = (FragmentTransitionSupport) this;
            C14030sU r5 = (C14030sU) obj;
            if (r5 != null) {
                int i2 = 0;
                if (r5 instanceof C16360wv) {
                    C16360wv r52 = (C16360wv) r5;
                    int size2 = r52.A01.size();
                    while (i2 < size2) {
                        fragmentTransitionSupport.A0G((i2 < 0 || i2 >= r52.A01.size()) ? null : (C14030sU) r52.A01.get(i2), arrayList);
                        i2++;
                    }
                } else if (!FragmentTransitionSupport.A00(r5) && A03(r5.A0E)) {
                    int size3 = arrayList.size();
                    while (i2 < size3) {
                        r5.A0B((View) arrayList.get(i2));
                        i2++;
                    }
                }
            }
        }
    }

    public void A0H(Object obj, ArrayList arrayList, ArrayList arrayList2) {
        List<View> targets;
        if (!(this instanceof FragmentTransitionSupport)) {
            C37111ui r2 = (C37111ui) this;
            Transition transition = (Transition) obj;
            int i = 0;
            if (transition instanceof TransitionSet) {
                TransitionSet transitionSet = (TransitionSet) transition;
                int transitionCount = transitionSet.getTransitionCount();
                while (i < transitionCount) {
                    r2.A0H(transitionSet.getTransitionAt(i), arrayList, arrayList2);
                    i++;
                }
            } else if (!C37111ui.A00(transition) && (targets = transition.getTargets()) != null && targets.size() == arrayList.size() && targets.containsAll(arrayList)) {
                int size = arrayList2 == null ? 0 : arrayList2.size();
                while (i < size) {
                    transition.addTarget((View) arrayList2.get(i));
                    i++;
                }
                for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                    transition.removeTarget((View) arrayList.get(size2));
                }
            }
        } else {
            FragmentTransitionSupport fragmentTransitionSupport = (FragmentTransitionSupport) this;
            C14030sU r5 = (C14030sU) obj;
            int i2 = 0;
            if (r5 instanceof C16360wv) {
                C16360wv r52 = (C16360wv) r5;
                int size3 = r52.A01.size();
                while (i2 < size3) {
                    fragmentTransitionSupport.A0H((i2 < 0 || i2 >= r52.A01.size()) ? null : (C14030sU) r52.A01.get(i2), arrayList, arrayList2);
                    i2++;
                }
            } else if (!FragmentTransitionSupport.A00(r5)) {
                ArrayList arrayList3 = r5.A0E;
                if (arrayList3.size() == arrayList.size() && arrayList3.containsAll(arrayList)) {
                    int size4 = arrayList2 == null ? 0 : arrayList2.size();
                    while (i2 < size4) {
                        r5.A0B((View) arrayList2.get(i2));
                        i2++;
                    }
                    for (int size5 = arrayList.size() - 1; size5 >= 0; size5--) {
                        r5.A0C((View) arrayList.get(size5));
                    }
                }
            }
        }
    }

    public void A0I(Object obj, ArrayList arrayList, ArrayList arrayList2) {
        if (!(this instanceof FragmentTransitionSupport)) {
            C37111ui r1 = (C37111ui) this;
            TransitionSet transitionSet = (TransitionSet) obj;
            if (transitionSet != null) {
                transitionSet.getTargets().clear();
                transitionSet.getTargets().addAll(arrayList2);
                r1.A0H(transitionSet, arrayList, arrayList2);
                return;
            }
            return;
        }
        FragmentTransitionSupport fragmentTransitionSupport = (FragmentTransitionSupport) this;
        C16360wv r3 = (C16360wv) obj;
        if (r3 != null) {
            r3.A0E.clear();
            r3.A0E.addAll(arrayList2);
            fragmentTransitionSupport.A0H(r3, arrayList, arrayList2);
        }
    }

    public boolean A0L(Object obj) {
        return !(this instanceof FragmentTransitionSupport) ? obj instanceof Transition : obj instanceof C14030sU;
    }

    public static boolean A03(List list) {
        if (list == null || list.isEmpty()) {
            return true;
        }
        return false;
    }

    public void A08(Fragment fragment, Object obj, C29702EgP egP, Runnable runnable) {
        if (!(this instanceof C37111ui)) {
            runnable.run();
        } else {
            ((Transition) obj).addListener(new EX2(runnable));
        }
    }

    public static void A01(View view, Rect rect) {
        if (C15320v6.isAttachedToWindow(view)) {
            RectF rectF = new RectF();
            rectF.set(0.0f, 0.0f, (float) view.getWidth(), (float) view.getHeight());
            view.getMatrix().mapRect(rectF);
            rectF.offset((float) view.getLeft(), (float) view.getTop());
            ViewParent parent = view.getParent();
            while (parent instanceof View) {
                View view2 = (View) parent;
                rectF.offset((float) (-view2.getScrollX()), (float) (-view2.getScrollY()));
                view2.getMatrix().mapRect(rectF);
                rectF.offset((float) view2.getLeft(), (float) view2.getTop());
                parent = view2.getParent();
            }
            int[] iArr = new int[2];
            view.getRootView().getLocationOnScreen(iArr);
            rectF.offset((float) iArr[0], (float) iArr[1]);
            rect.set(Math.round(rectF.left), Math.round(rectF.top), Math.round(rectF.right), Math.round(rectF.bottom));
        }
    }

    public static void A02(List list, View view) {
        boolean z;
        boolean z2;
        int size = list.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                z = false;
                break;
            } else if (list.get(i) == view) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            list.add(view);
            for (int i2 = size; i2 < list.size(); i2++) {
                View view2 = (View) list.get(i2);
                if (view2 instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view2;
                    int childCount = viewGroup.getChildCount();
                    for (int i3 = 0; i3 < childCount; i3++) {
                        View childAt = viewGroup.getChildAt(i3);
                        int i4 = 0;
                        while (true) {
                            if (i4 >= size) {
                                z2 = false;
                                break;
                            } else if (list.get(i4) == childAt) {
                                z2 = true;
                                break;
                            } else {
                                i4++;
                            }
                        }
                        if (!z2) {
                            list.add(childAt);
                        }
                    }
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:7:0x0016 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: android.view.ViewGroup} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: android.view.ViewGroup} */
    /* JADX WARN: Type inference failed for: r5v1, types: [java.lang.Object] */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0045, code lost:
        if (X.C15320v6.getTransitionName(r5) != null) goto L_0x0047;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0J(java.util.ArrayList r4, android.view.View r5) {
        /*
            r3 = this;
            int r0 = r5.getVisibility()
            if (r0 != 0) goto L_0x004c
            boolean r0 = r5 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x0049
            android.view.ViewGroup r5 = (android.view.ViewGroup) r5
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0029
            boolean r1 = r5.isTransitionGroup()
        L_0x0016:
            if (r1 != 0) goto L_0x0049
            int r2 = r5.getChildCount()
            r1 = 0
        L_0x001d:
            if (r1 >= r2) goto L_0x004c
            android.view.View r0 = r5.getChildAt(r1)
            r3.A0J(r4, r0)
            int r1 = r1 + 1
            goto L_0x001d
        L_0x0029:
            r0 = 2131300905(0x7f091229, float:1.8219853E38)
            java.lang.Object r0 = r5.getTag(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 == 0) goto L_0x003a
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0047
        L_0x003a:
            android.graphics.drawable.Drawable r0 = r5.getBackground()
            if (r0 != 0) goto L_0x0047
            java.lang.String r0 = X.C15320v6.getTransitionName(r5)
            r1 = 0
            if (r0 == 0) goto L_0x0016
        L_0x0047:
            r1 = 1
            goto L_0x0016
        L_0x0049:
            r4.add(r5)
        L_0x004c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16340wt.A0J(java.util.ArrayList, android.view.View):void");
    }

    public void A0K(Map map, View view) {
        if (view.getVisibility() == 0) {
            String transitionName = C15320v6.getTransitionName(view);
            if (transitionName != null) {
                map.put(transitionName, view);
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int childCount = viewGroup.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    A0K(map, viewGroup.getChildAt(i));
                }
            }
        }
    }
}
