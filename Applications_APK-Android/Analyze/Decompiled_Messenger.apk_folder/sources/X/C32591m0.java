package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.1m0  reason: invalid class name and case insensitive filesystem */
public final class C32591m0 {
    private AnonymousClass0UN A00;

    public static final C32591m0 A00(AnonymousClass1XY r1) {
        return new C32591m0(r1);
    }

    public C32591m0(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public ImmutableList A01() {
        ImmutableList.Builder builder = ImmutableList.builder();
        if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C34871qJ) AnonymousClass1XX.A03(AnonymousClass1Y3.AQQ, this.A00)).A00)).Aem(287397736684974L)) {
            builder.add((Object) new AnonymousClass3AK((C53542kx) AnonymousClass1XX.A03(AnonymousClass1Y3.AOw, this.A00), C99084oO.$const$string(202)));
        }
        builder.add((Object) ((AnonymousClass12K) AnonymousClass1XX.A03(AnonymousClass1Y3.AAo, this.A00)));
        builder.add((Object) ((AnonymousClass12L) AnonymousClass1XX.A03(AnonymousClass1Y3.AVz, this.A00)));
        C16510xE r2 = (C16510xE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADT, this.A00);
        boolean z = false;
        if (r2.A0O() && ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r2.A00)).Aem(282501489034874L)) {
            z = true;
        }
        if (z) {
            builder.add((Object) ((AnonymousClass53F) AnonymousClass1XX.A03(AnonymousClass1Y3.AfB, this.A00)));
        }
        return builder.build();
    }
}
