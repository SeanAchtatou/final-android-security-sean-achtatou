package X;

import android.net.Uri;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.ContextChain;
import com.facebook.http.interfaces.RequestPriority;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.net.URI;
import java.util.Map;
import java.util.Random;
import javax.inject.Singleton;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;

@Singleton
/* renamed from: X.1Ov  reason: invalid class name and case insensitive filesystem */
public final class C23261Ov extends C23281Ox {
    private static volatile C23261Ov A03;
    public AnonymousClass0UN A00;
    public Random A01;
    private final AnonymousClass0US A02;

    public static C48142Zr A00(C23261Ov r14, Uri uri, CallerContext callerContext, RequestPriority requestPriority, C196579Mo r18, C50142dU r19, boolean z, C50162dW r21) {
        ContextChain contextChain;
        Map map;
        C23261Ov r2 = r14;
        CallerContext callerContext2 = callerContext;
        C50172dX r4 = new C50172dX(((C001300x) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B3a, r14.A00)).A04, ImmutableList.of(callerContext2.A0F()));
        Uri uri2 = uri;
        HttpGet httpGet = new HttpGet(URI.create(uri.toString()));
        httpGet.addHeader("Referer", r4.A00().toString());
        httpGet.addHeader(AnonymousClass24B.$const$string(173), "unknown");
        C196579Mo r0 = r18;
        if (r18 != null) {
            int i = r0.A00;
            if (i > 0) {
                httpGet.addHeader("Range", new C196579Mo(i - 1, r0.A01).A00());
            } else {
                httpGet.addHeader("Range", r0.A00());
            }
        }
        C50142dU r5 = r19;
        if (r5.A03.A09 instanceof C50182dY) {
            ImmutableMap immutableMap = null;
            C24971Xv it = immutableMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                httpGet.addHeader((String) entry.getKey(), (String) entry.getValue());
            }
        }
        HttpClientParams.setRedirecting(httpGet.getParams(), true);
        ((AnonymousClass29g) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BAf, r14.A00)).A07(uri.toString());
        int i2 = AnonymousClass1Y3.BAf;
        AnonymousClass0UN r8 = r14.A00;
        AnonymousClass29g r12 = (AnonymousClass29g) AnonymousClass1XX.A02(4, i2, r8);
        C50192dZ r1 = new C50192dZ(uri, r12);
        boolean booleanValue = ((Boolean) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BKO, r8)).booleanValue();
        C25051Yd r82 = (C25051Yd) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AOJ, r8);
        C50222dc r9 = new C50222dc(uri2, new C50202da(r14, r5, z, r21), r12, r14.A02, (AnonymousClass29o) AnonymousClass1XX.A02(5, AnonymousClass1Y3.APS, r8), booleanValue, r82);
        AnonymousClass2Y1 r42 = new AnonymousClass2Y1();
        r42.A0C = "image";
        r42.A05 = callerContext2;
        r42.A0A = "MediaDownloader";
        r42.A0I = httpGet;
        r42.A0G = r1;
        r42.A0H = r9;
        r42.A07 = requestPriority;
        r42.A01 = 2;
        r42.A0J = true;
        r42.A0D = AnonymousClass2Y0.A00(AnonymousClass07B.A00);
        if (r82.Aem(282063388410952L) && (contextChain = callerContext2.A00) != null && (map = contextChain.A00) != null && map.containsKey("expected_response_size")) {
            r42.A00 = ((Integer) contextChain.A00.get("expected_response_size")).intValue();
        }
        if (((C25051Yd) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AOJ, r2.A00)).Aem(283897338269295L)) {
            String A0P = AnonymousClass08S.A0P(AnonymousClass01P.A05(), ":", r5.A03.A0B);
            r42.A01("endToEndTraceUseCaseId", "images-infra-android");
            r42.A01("endToEndTraceLoggingId", A0P);
        }
        String str = r5.A03.A0B;
        String str2 = str;
        if (str != null) {
            r42.A01("drawee_request_id", str2);
        }
        return ((AnonymousClass0Z4) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AAv, r2.A00)).A04(r42.A00());
    }

    public static final C23261Ov A02(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C23261Ov.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C23261Ov(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C23261Ov(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(13, r3);
        this.A02 = AnonymousClass0VB.A00(AnonymousClass1Y3.BCX, r3);
    }

    public static RequestPriority A01(C23561Pz r3) {
        switch (r3.ordinal()) {
            case 0:
                return RequestPriority.A03;
            case 1:
                return RequestPriority.A05;
            case 2:
                return RequestPriority.A04;
            default:
                throw new UnsupportedOperationException("Unrecognized priority: " + r3);
        }
    }
}
