package X;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

/* renamed from: X.0up  reason: invalid class name and case insensitive filesystem */
public final class C15160up extends Drawable implements Animatable {
    public static final Interpolator A06 = new LinearInterpolator();
    private static final Interpolator A07 = new C15170uq();
    private static final int[] A08 = {C15320v6.MEASURED_STATE_MASK};
    public float A00;
    public boolean A01;
    public float A02;
    public Animator A03;
    private Resources A04;
    public final C15630vb A05;

    public int getOpacity() {
        return -3;
    }

    private void A00(float f, float f2, float f3, float f4) {
        C15630vb r3 = this.A05;
        float f5 = this.A04.getDisplayMetrics().density;
        float f6 = f2 * f5;
        r3.A08 = f6;
        r3.A0J.setStrokeWidth(f6);
        r3.A02 = f * f5;
        r3.A0C = 0;
        r3.A0D = r3.A0G[0];
        r3.A0B = (int) (f3 * f5);
        r3.A0A = (int) (f4 * f5);
    }

    public void A01(float f, C15630vb r11) {
        if (f > 0.75f) {
            float f2 = (f - 0.75f) / 0.25f;
            int[] iArr = r11.A0G;
            int i = r11.A0C;
            int i2 = iArr[i];
            int i3 = iArr[(i + 1) % iArr.length];
            int i4 = (i2 >> 24) & 255;
            int i5 = (i2 >> 16) & 255;
            int i6 = (i2 >> 8) & 255;
            int i7 = i2 & 255;
            r11.A0D = ((i4 + ((int) (((float) (((i3 >> 24) & 255) - i4)) * f2))) << 24) | ((i5 + ((int) (((float) (((i3 >> 16) & 255) - i5)) * f2))) << 16) | ((i6 + ((int) (((float) (((i3 >> 8) & 255) - i6)) * f2))) << 8) | (i7 + ((int) (f2 * ((float) ((i3 & 255) - i7)))));
            return;
        }
        r11.A0D = r11.A0G[r11.A0C];
    }

    public void A02(float f, C15630vb r9, boolean z) {
        float f2;
        float interpolation;
        if (this.A01) {
            A01(f, r9);
            float f3 = r9.A06;
            float f4 = r9.A07;
            float f5 = r9.A05;
            r9.A04 = f4 + (((f5 - 0.01f) - f4) * f);
            r9.A01 = f5;
            r9.A03 = f3 + ((((float) (Math.floor((double) (f3 / 0.8f)) + 1.0d)) - f3) * f);
        } else if (f != 1.0f || z) {
            float f6 = r9.A06;
            if (f < 0.5f) {
                interpolation = r9.A07;
                f2 = (A07.getInterpolation(f / 0.5f) * 0.79f) + 0.01f + interpolation;
            } else {
                f2 = r9.A07 + 0.79f;
                interpolation = f2 - (((1.0f - A07.getInterpolation((f - 0.5f) / 0.5f)) * 0.79f) + 0.01f);
            }
            r9.A04 = interpolation;
            r9.A01 = f2;
            r9.A03 = f6 + (0.20999998f * f);
            this.A02 = (f + this.A00) * 216.0f;
        }
    }

    public void A03(int i) {
        if (i == 0) {
            A00(11.0f, 3.0f, 12.0f, 6.0f);
        } else {
            A00(7.5f, 2.5f, 10.0f, 5.0f);
        }
        invalidateSelf();
    }

    public int getAlpha() {
        return this.A05.A09;
    }

    public boolean isRunning() {
        return this.A03.isRunning();
    }

    public void setAlpha(int i) {
        this.A05.A09 = i;
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.A05.A0J.setColorFilter(colorFilter);
        invalidateSelf();
    }

    public void start() {
        Animator animator;
        long j;
        this.A03.cancel();
        C15630vb r3 = this.A05;
        float f = r3.A04;
        r3.A07 = f;
        float f2 = r3.A01;
        r3.A05 = f2;
        r3.A06 = r3.A03;
        if (f2 != f) {
            this.A01 = true;
            animator = this.A03;
            j = 666;
        } else {
            r3.A0C = 0;
            r3.A0D = r3.A0G[0];
            r3.A07 = 0.0f;
            r3.A05 = 0.0f;
            r3.A06 = 0.0f;
            r3.A04 = 0.0f;
            r3.A01 = 0.0f;
            r3.A03 = 0.0f;
            animator = this.A03;
            j = 1332;
        }
        animator.setDuration(j);
        this.A03.start();
    }

    public void stop() {
        this.A03.cancel();
        this.A02 = 0.0f;
        C15630vb r2 = this.A05;
        if (r2.A0F) {
            r2.A0F = false;
        }
        r2.A0C = 0;
        r2.A0D = r2.A0G[0];
        r2.A07 = 0.0f;
        r2.A05 = 0.0f;
        r2.A06 = 0.0f;
        r2.A04 = 0.0f;
        r2.A01 = 0.0f;
        r2.A03 = 0.0f;
        invalidateSelf();
    }

    public C15160up(Context context) {
        AnonymousClass0qX.A00(context);
        this.A04 = context.getResources();
        C15630vb r2 = new C15630vb();
        this.A05 = r2;
        int[] iArr = A08;
        r2.A0G = iArr;
        r2.A0C = 0;
        r2.A0D = iArr[0];
        r2.A08 = 2.5f;
        r2.A0J.setStrokeWidth(2.5f);
        invalidateSelf();
        C15630vb r22 = this.A05;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.addUpdateListener(new C15640vc(this, r22));
        ofFloat.setRepeatCount(-1);
        ofFloat.setRepeatMode(1);
        ofFloat.setInterpolator(A06);
        ofFloat.addListener(new C15650vd(this, r22));
        this.A03 = ofFloat;
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        Canvas canvas2 = canvas;
        canvas.save();
        canvas.rotate(this.A02, bounds.exactCenterX(), bounds.exactCenterY());
        C15630vb r2 = this.A05;
        RectF rectF = r2.A0K;
        float f = r2.A02;
        float f2 = (r2.A08 / 2.0f) + f;
        if (f <= 0.0f) {
            f2 = (((float) Math.min(bounds.width(), bounds.height())) / 2.0f) - Math.max((((float) r2.A0B) * r2.A00) / 2.0f, r2.A08 / 2.0f);
        }
        rectF.set(((float) bounds.centerX()) - f2, ((float) bounds.centerY()) - f2, ((float) bounds.centerX()) + f2, ((float) bounds.centerY()) + f2);
        float f3 = r2.A04;
        float f4 = r2.A03;
        float f5 = (f3 + f4) * 360.0f;
        float f6 = ((r2.A01 + f4) * 360.0f) - f5;
        r2.A0J.setColor(r2.A0D);
        r2.A0J.setAlpha(r2.A09);
        float f7 = r2.A08 / 2.0f;
        rectF.inset(f7, f7);
        canvas.drawCircle(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, r2.A0I);
        float f8 = -f7;
        rectF.inset(f8, f8);
        canvas2.drawArc(rectF, f5, f6, false, r2.A0J);
        if (r2.A0F) {
            Path path = r2.A0E;
            if (path == null) {
                Path path2 = new Path();
                r2.A0E = path2;
                path2.setFillType(Path.FillType.EVEN_ODD);
            } else {
                path.reset();
            }
            r2.A0E.moveTo(0.0f, 0.0f);
            r2.A0E.lineTo(((float) r2.A0B) * r2.A00, 0.0f);
            Path path3 = r2.A0E;
            float f9 = r2.A00;
            path3.lineTo((((float) r2.A0B) * f9) / 2.0f, ((float) r2.A0A) * f9);
            r2.A0E.offset(((Math.min(rectF.width(), rectF.height()) / 2.0f) + rectF.centerX()) - ((((float) r2.A0B) * r2.A00) / 2.0f), rectF.centerY() + (r2.A08 / 2.0f));
            r2.A0E.close();
            r2.A0H.setColor(r2.A0D);
            r2.A0H.setAlpha(r2.A09);
            canvas.save();
            canvas.rotate(f5 + f6, rectF.centerX(), rectF.centerY());
            canvas.drawPath(r2.A0E, r2.A0H);
            canvas.restore();
        }
        canvas.restore();
    }
}
