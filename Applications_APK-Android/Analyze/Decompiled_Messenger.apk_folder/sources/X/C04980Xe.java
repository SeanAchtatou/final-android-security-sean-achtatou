package X;

import com.facebook.auth.annotations.LoggedInUser;
import java.util.Collections;

/* renamed from: X.0Xe  reason: invalid class name and case insensitive filesystem */
public abstract class C04980Xe {
    public C35701re A00;
    private final int A01;
    private final C05030Xj A02;
    private final AnonymousClass1YI A03;

    public static void A01(C04980Xe r3) {
        if (r3.A03.AbO(r3.A01, false)) {
            r3.A00 = C35701re.A02;
        } else {
            r3.A00 = C35701re.A01;
        }
    }

    public boolean A03(String str) {
        return !this.A02.A01.A03("__fs_policy_blacklisted_events__", Collections.emptySet()).contains(str);
    }

    public C04980Xe(@LoggedInUser AnonymousClass1YI r2, C05000Xg r3, int i, C05030Xj r5) {
        this.A03 = r2;
        this.A01 = i;
        this.A02 = r5;
        r3.A00(new AnonymousClass0YF(this), i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (X.AnonymousClass0YO.A00.contains(r3) == false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.flexiblesampling.SamplingResult A02(java.lang.String r3, boolean r4) {
        /*
            r2 = this;
            boolean r0 = X.C006006f.A01()
            if (r0 != 0) goto L_0x001e
            boolean r0 = X.AnonymousClass0YO.A01
            if (r0 == 0) goto L_0x0013
            java.util.List r0 = X.AnonymousClass0YO.A00
            boolean r1 = r0.contains(r3)
            r0 = 1
            if (r1 != 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            if (r0 != 0) goto L_0x001e
            X.0Xj r1 = r2.A02
            r0 = 0
            com.facebook.flexiblesampling.SamplingResult r0 = r1.A03(r3, r0, r0, r4)
            return r0
        L_0x001e:
            com.facebook.flexiblesampling.SamplingResult r0 = com.facebook.flexiblesampling.SamplingResult.A00()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04980Xe.A02(java.lang.String, boolean):com.facebook.flexiblesampling.SamplingResult");
    }
}
