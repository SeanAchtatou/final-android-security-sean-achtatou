package X;

import android.content.Context;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.mig.scheme.schemes.DarkColorScheme;
import com.facebook.mig.scheme.schemes.LightColorScheme;

@UserScoped
/* renamed from: X.0se  reason: invalid class name and case insensitive filesystem */
public final class C14120se {
    private static C05540Zi A02;
    private final C14140sg A00;
    private final C14170sj A01;

    public static final C14120se A00(AnonymousClass1XY r4) {
        C14120se r0;
        synchronized (C14120se.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new C14120se((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (C14120se) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public MigColorScheme A01() {
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, this.A00.A00)).AbO(AnonymousClass1Y3.A1P, true)) {
            return C17190yT.A00();
        }
        C14170sj r3 = this.A01;
        if (!((C29011fj) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Alg, r3.A00)).A01() ? C14170sj.A01(r3).intValue() == 1 : (((Context) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A6S, r3.A00)).getResources().getConfiguration().uiMode & 48) == 32) {
            return DarkColorScheme.A00();
        }
        return LightColorScheme.A00();
    }

    private C14120se(AnonymousClass1XY r2) {
        this.A00 = C14140sg.A00(r2);
        this.A01 = C14170sj.A00(r2);
    }
}
