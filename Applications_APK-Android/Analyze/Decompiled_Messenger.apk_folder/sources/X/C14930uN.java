package X;

import android.animation.StateListAnimator;
import android.graphics.drawable.Drawable;

/* renamed from: X.0uN  reason: invalid class name and case insensitive filesystem */
public final class C14930uN {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public StateListAnimator A04;
    public Drawable A05;
    public AnonymousClass38W A06;
    public AnonymousClass1IF A07;
    public AnonymousClass10N A08;
    public AnonymousClass10N A09;
    public AnonymousClass10N A0A;
    public AnonymousClass10N A0B;
    public AnonymousClass10N A0C;
    public AnonymousClass10N A0D;
    public Integer A0E;
    public String A0F;
    public String A0G;
    public boolean A0H;
}
