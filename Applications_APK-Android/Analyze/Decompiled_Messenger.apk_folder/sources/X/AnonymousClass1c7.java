package X;

import com.fasterxml.jackson.databind.JsonDeserializer;

/* renamed from: X.1c7  reason: invalid class name */
public interface AnonymousClass1c7 {
    JsonDeserializer findArrayDeserializer(BMA bma, C10490kF r2, C10120ja r3, C64433Bp r4, JsonDeserializer jsonDeserializer);

    JsonDeserializer findBeanDeserializer(C10030jR r1, C10490kF r2, C10120ja r3);

    JsonDeserializer findCollectionDeserializer(C28331ed r1, C10490kF r2, C10120ja r3, C64433Bp r4, JsonDeserializer jsonDeserializer);

    JsonDeserializer findCollectionLikeDeserializer(AnonymousClass1CA r1, C10490kF r2, C10120ja r3, C64433Bp r4, JsonDeserializer jsonDeserializer);

    JsonDeserializer findEnumDeserializer(Class cls, C10490kF r2, C10120ja r3);

    JsonDeserializer findMapDeserializer(C180610a r1, C10490kF r2, C10120ja r3, C422628y r4, C64433Bp r5, JsonDeserializer jsonDeserializer);

    JsonDeserializer findMapLikeDeserializer(C21991Jm r1, C10490kF r2, C10120ja r3, C422628y r4, C64433Bp r5, JsonDeserializer jsonDeserializer);

    JsonDeserializer findTreeNodeDeserializer(Class cls, C10490kF r2, C10120ja r3);
}
