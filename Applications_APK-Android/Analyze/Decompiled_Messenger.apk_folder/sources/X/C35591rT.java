package X;

import java.util.HashMap;

/* renamed from: X.1rT  reason: invalid class name and case insensitive filesystem */
public final class C35591rT implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.litho.sections.fb.common.IdleExecutorHandler$CancellableRunnable";
    public Runnable A00;
    private final HashMap A01;

    public void run() {
        Runnable runnable = this.A00;
        if (runnable != null) {
            runnable.run();
            this.A01.remove(this.A00);
            this.A00 = null;
        }
    }

    public C35591rT(Runnable runnable, HashMap hashMap) {
        this.A00 = runnable;
        this.A01 = hashMap;
    }
}
