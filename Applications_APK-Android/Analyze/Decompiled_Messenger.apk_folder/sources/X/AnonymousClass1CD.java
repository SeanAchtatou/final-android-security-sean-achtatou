package X;

import java.util.Iterator;

/* renamed from: X.1CD  reason: invalid class name */
public final class AnonymousClass1CD extends AnonymousClass1CJ {
    public final /* synthetic */ Iterable A00;

    public AnonymousClass1CD(Iterable iterable) {
        this.A00 = iterable;
    }

    public Iterator iterator() {
        return new AnonymousClass0UK(AnonymousClass0j4.A00(this.A00, new AnonymousClass1CC()).iterator());
    }
}
