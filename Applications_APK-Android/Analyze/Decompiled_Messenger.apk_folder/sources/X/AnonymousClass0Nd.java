package X;

import java.util.concurrent.Executor;

/* renamed from: X.0Nd  reason: invalid class name */
public final class AnonymousClass0Nd {
    public final AnonymousClass09P A00;
    public final Executor A01;

    public AnonymousClass0Nd(AnonymousClass09P r1, Executor executor) {
        this.A00 = r1;
        this.A01 = executor;
    }
}
