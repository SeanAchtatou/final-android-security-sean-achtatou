package X;

import com.facebook.auth.viewercontext.ViewerContext;

/* renamed from: X.1yB  reason: invalid class name and case insensitive filesystem */
public final class C38901yB {
    public final ViewerContext A00;
    public final C11170mD A01;

    public static final C38901yB A00(AnonymousClass1XY r1) {
        return new C38901yB(r1);
    }

    public C38901yB(AnonymousClass1XY r2) {
        this.A00 = C10580kT.A00(r2);
        this.A01 = C11170mD.A00(r2);
    }
}
