package X;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/* renamed from: X.0Wz  reason: invalid class name and case insensitive filesystem */
public final class C04950Wz implements AnonymousClass0X0 {
    public Object BzW(File file) {
        DataInputStream dataInputStream;
        String str;
        Object[] objArr;
        boolean z = false;
        try {
            dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
            try {
                String readUTF = dataInputStream.readUTF();
                if (!"GK_STATE".equals(readUTF)) {
                    str = "Cannot read gatekeepers state, invalid signature: %s";
                    objArr = new Object[]{readUTF};
                } else {
                    int readInt = dataInputStream.readInt();
                    if (readInt != 1) {
                        str = "Cannot read gatekeepers state, invalid version: %s";
                        objArr = new Object[]{Integer.valueOf(readInt)};
                    } else {
                        String readUTF2 = dataInputStream.readUTF();
                        byte[] bArr = new byte[dataInputStream.readInt()];
                        dataInputStream.readFully(bArr);
                        try {
                            C05410Yv r0 = new C05410Yv(readUTF2, bArr);
                            AnonymousClass0ZA.A00(dataInputStream, false);
                            return r0;
                        } catch (Throwable th) {
                            th = th;
                            z = true;
                            AnonymousClass0ZA.A00(dataInputStream, !z);
                            throw th;
                        }
                    }
                }
                C010708t.A0Q("StateFileSerializer", str, objArr);
                AnonymousClass0ZA.A00(dataInputStream, true);
                return null;
            } catch (Throwable th2) {
                th = th2;
                AnonymousClass0ZA.A00(dataInputStream, !z);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            dataInputStream = null;
            AnonymousClass0ZA.A00(dataInputStream, !z);
            throw th;
        }
    }

    public void CNY(File file, Object obj) {
        C05410Yv r6 = (C05410Yv) obj;
        DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file), 1024));
        boolean z = true;
        try {
            dataOutputStream.writeUTF("GK_STATE");
            dataOutputStream.writeInt(z ? 1 : 0);
            dataOutputStream.writeUTF(r6.A00);
            dataOutputStream.writeInt(r6.A01.length);
            dataOutputStream.write(r6.A01);
            z = false;
        } finally {
            AnonymousClass0ZA.A00(dataOutputStream, z);
        }
    }
}
