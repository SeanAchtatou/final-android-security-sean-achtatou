package X;

/* renamed from: X.1kx  reason: invalid class name and case insensitive filesystem */
public final class C32001kx implements C22221Kj {
    public static final C32001kx A01 = new C32001kx(true);
    private final boolean A00;

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0014, code lost:
        if (r2 == 2) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int ASF(java.lang.CharSequence r6, int r7, int r8) {
        /*
            r5 = this;
            int r8 = r8 + r7
            r4 = 0
            r3 = 0
        L_0x0003:
            if (r7 >= r8) goto L_0x002a
            char r0 = r6.charAt(r7)
            byte r2 = java.lang.Character.getDirectionality(r0)
            r0 = 1
            if (r2 == 0) goto L_0x0017
            if (r2 == r0) goto L_0x0016
            r1 = 2
            r0 = 2
            if (r2 != r1) goto L_0x0017
        L_0x0016:
            r0 = 0
        L_0x0017:
            r1 = 1
            if (r0 == 0) goto L_0x0021
            if (r0 != r1) goto L_0x0027
            boolean r0 = r5.A00
            if (r0 != 0) goto L_0x0026
            return r1
        L_0x0021:
            boolean r0 = r5.A00
            if (r0 == 0) goto L_0x0026
            return r4
        L_0x0026:
            r3 = 1
        L_0x0027:
            int r7 = r7 + 1
            goto L_0x0003
        L_0x002a:
            if (r3 == 0) goto L_0x002f
            boolean r0 = r5.A00
            return r0
        L_0x002f:
            r0 = 2
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32001kx.ASF(java.lang.CharSequence, int, int):int");
    }

    private C32001kx(boolean z) {
        this.A00 = z;
    }
}
