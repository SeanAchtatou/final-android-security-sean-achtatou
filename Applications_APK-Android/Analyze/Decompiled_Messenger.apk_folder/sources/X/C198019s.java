package X;

import android.content.Context;
import android.util.AttributeSet;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.19s  reason: invalid class name and case insensitive filesystem */
public class C198019s extends RecyclerView {
    public C198019s(Context context) {
        super(context);
        new C15920wC(this);
    }

    public C198019s(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new C15920wC(this);
    }

    public C198019s(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new C15920wC(this);
    }
}
