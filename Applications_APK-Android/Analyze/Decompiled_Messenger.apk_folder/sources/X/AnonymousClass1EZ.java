package X;

import android.app.Activity;

/* renamed from: X.1EZ  reason: invalid class name */
public final class AnonymousClass1EZ implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.AnalyticsActivityListener$1";
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ AnonymousClass1EY A01;

    public AnonymousClass1EZ(AnonymousClass1EY r1, Activity activity) {
        this.A01 = r1;
        this.A00 = activity;
    }

    public void run() {
        AnonymousClass1EY.A02(this.A01, this.A00);
    }
}
