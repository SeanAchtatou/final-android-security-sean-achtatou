package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem;
import com.facebook.user.model.LastActive;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1HA  reason: invalid class name */
public final class AnonymousClass1HA {
    public AnonymousClass0UN A00;
    public final AnonymousClass1HB A01;
    public final AnonymousClass0r6 A02;
    public final C21521Hi A03;
    private final AnonymousClass1H6 A04;

    public static final AnonymousClass1HA A00(AnonymousClass1XY r1) {
        return new AnonymousClass1HA(r1);
    }

    public static Long A01(AnonymousClass1HA r2, ThreadSummary threadSummary) {
        UserKey A002;
        ThreadKey threadKey = threadSummary.A0S;
        if (threadKey.A0L()) {
            ImmutableList A012 = r2.A04.A01(threadSummary, true);
            if (A012.isEmpty()) {
                return null;
            }
            A002 = (UserKey) A012.get(0);
        } else if (!threadKey.A0N()) {
            return null;
        } else {
            A002 = UserKey.A00(Long.valueOf(threadKey.A01));
        }
        return A02(r2, A002);
    }

    public static Long A02(AnonymousClass1HA r3, UserKey userKey) {
        Long l;
        LastActive A0M = r3.A02.A0M(userKey);
        if (A0M != null) {
            l = Long.valueOf(A0M.A00);
        } else {
            l = null;
        }
        if (l == null) {
            return null;
        }
        return Long.valueOf((((AnonymousClass06B) AnonymousClass1XX.A03(AnonymousClass1Y3.AgK, r3.A00)).now() - l.longValue()) / 1000);
    }

    public static String A03(AnonymousClass1HA r1, ThreadSummary threadSummary) {
        ImmutableList A012 = r1.A01.A01(threadSummary);
        if (A012.isEmpty()) {
            return null;
        }
        return r1.A03.A01(((User) A012.get(0)).A0Q).toString();
    }

    public UnifiedPresenceViewLoggerItem A04(ThreadSummary threadSummary) {
        ImmutableMap build;
        C06610bn r4 = new C06610bn();
        r4.A03 = this.A01.A02(threadSummary);
        r4.A01 = A01(this, threadSummary);
        ImmutableList A012 = this.A01.A01(threadSummary);
        if (A012.isEmpty()) {
            build = RegularImmutableMap.A03;
        } else {
            ObjectNode A013 = this.A03.A01(((User) A012.get(0)).A0Q);
            ImmutableMap.Builder builder = ImmutableMap.builder();
            try {
                Iterator fields = A013.fields();
                while (fields.hasNext()) {
                    Map.Entry entry = (Map.Entry) fields.next();
                    builder.put(entry.getKey(), Integer.valueOf(((JsonNode) entry.getValue()).intValue()));
                }
            } catch (Exception e) {
                C010708t.A0L("UserPresenceLoggingMapCreator", "Could not deserialize JSON", e);
            }
            build = builder.build();
        }
        r4.A00 = build;
        return new UnifiedPresenceViewLoggerItem(r4);
    }

    public UnifiedPresenceViewLoggerItem A05(User user) {
        C06610bn r2 = new C06610bn();
        r2.A01 = A02(this, user.A0Q);
        r2.A02 = this.A03.A01(user.A0Q).toString();
        return new UnifiedPresenceViewLoggerItem(r2);
    }

    public AnonymousClass1HA(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A04 = AnonymousClass1H6.A00(r3);
        this.A01 = AnonymousClass1HB.A00(r3);
        this.A03 = new C21521Hi(r3);
        this.A02 = C14890uJ.A00(r3);
    }
}
