package X;

import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import java.util.Map;
import java.util.Random;

/* renamed from: X.1Ob  reason: invalid class name and case insensitive filesystem */
public final class C23061Ob implements C23051Oa {
    private static final C23081Od A02 = new C23071Oc();
    private final DeprecatedAnalyticsLogger A00;
    private final Random A01 = new Random();

    public void C07(C30781id r12, C23191Oo[] r13, String[] strArr, C23191Oo[] r15, C23191Oo[] r16, int i) {
        int i2 = i;
        C23191Oo[] r9 = r16;
        C412024n.A02(r12, r13, strArr, r15, r9, i2);
        if (this.A01.nextInt(i2) == 0) {
            C22361La A04 = this.A00.A04("contextual_config_exposure", false);
            if (A04.A0B()) {
                A00(r12, i2, A04);
                StringBuilder sb = new StringBuilder();
                if (A02(sb, r12.AiU())) {
                    A04.A06("context", sb.toString());
                    sb.setLength(0);
                }
                if (A02(sb, r13)) {
                    A04.A06("context_value", sb.toString());
                    sb.setLength(0);
                }
                if (A02(sb, strArr)) {
                    A04.A06("bucket", sb.toString());
                    sb.setLength(0);
                }
                if (A02(sb, r12.Auv())) {
                    A04.A06("monitor", sb.toString());
                    sb.setLength(0);
                }
                if (A02(sb, r15)) {
                    A04.A06("monitor_value", sb.toString());
                    sb.setLength(0);
                }
                if (A02(sb, r9)) {
                    A04.A06("result", sb.toString());
                    sb.setLength(0);
                } else {
                    A04.A06("result", "INVALID");
                }
                A01(r12, A04);
                A04.A0A();
            }
        }
    }

    private static boolean A02(StringBuilder sb, Object[] objArr) {
        if (objArr == null) {
            return false;
        }
        C06850cB.A09(sb, ", ", A02, objArr);
        return true;
    }

    public C23061Ob(DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        this.A00 = deprecatedAnalyticsLogger;
    }

    private static void A00(C30781id r3, int i, C22361La r5) {
        Map map = r3.B00().A00;
        if (!map.isEmpty()) {
            for (Map.Entry entry : map.entrySet()) {
                r5.A06((String) entry.getKey(), (String) entry.getValue());
            }
        }
        r5.A02(ErrorReportingConstants.SOFT_ERROR_OCCURRENCE_COUNT, i * r5.A00());
        r5.A06("policy_id", r3.Ayb());
        r5.A06("config_name", r3.getName());
        r5.A06("version", r3.B8L());
        r5.A03("cfg_ver_timestamp", r3.B3V());
    }

    private void A01(C30781id r3, C22361La r4) {
        if (!r3.B00().A01 && this.A01.nextInt(10) == 0) {
            r4.A06("json", r3.B00().A02);
            r3.B00().A01 = true;
        }
    }

    public void C08(C30781id r4, String str, int i) {
        C412024n.A00(r4, i);
        if (this.A01.nextInt(i) == 0) {
            C22361La A04 = this.A00.A04("contextual_config_exposure", false);
            if (A04.A0B()) {
                A00(r4, i, A04);
                A04.A06("exception", str);
                A01(r4, A04);
                A04.A0A();
            }
        }
    }
}
