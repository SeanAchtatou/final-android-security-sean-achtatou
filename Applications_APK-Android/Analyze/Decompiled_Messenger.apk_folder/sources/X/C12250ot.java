package X;

import io.card.payment.BuildConfig;
import java.io.Writer;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0ot  reason: invalid class name and case insensitive filesystem */
public abstract class C12250ot {
    public C12250ot A00;
    public C06390bQ A01;
    public AnonymousClass0oB A02;
    public C73403g1 A03;
    public AtomicInteger A04 = new AtomicInteger(0);
    public boolean A05;
    private AnonymousClass20I A06;
    private boolean A07 = false;

    public void A03() {
        if (!(this instanceof C12240os)) {
            ((C16910xz) this).A00.clear();
            return;
        }
        C12240os r1 = (C12240os) this;
        r1.A01.clear();
        r1.A00 = 0;
    }

    public void A04() {
        if (!(this instanceof C12240os)) {
            C16910xz r1 = (C16910xz) this;
            r1.A01.A01.C0w(r1);
            return;
        }
        C12240os r12 = (C12240os) this;
        r12.A01.A02.C0w(r12);
    }

    public void A05() {
        if (!(this instanceof C12240os)) {
            C16910xz r4 = (C16910xz) this;
            int size = r4.A00.size();
            for (int i = 0; i < size; i++) {
                Object obj = r4.A00.get(i);
                if (obj instanceof C12250ot) {
                    ((C12250ot) obj).A08();
                }
            }
            return;
        }
        C12240os r3 = (C12240os) this;
        for (int i2 = 0; i2 < r3.A00; i2++) {
            Object A0G = r3.A0G(i2);
            if (A0G instanceof C12250ot) {
                ((C12250ot) A0G).A08();
            }
        }
    }

    public void A09(int i) {
        ArrayList arrayList;
        if (!(this instanceof C12240os)) {
            C16910xz r3 = (C16910xz) this;
            int size = r3.A00.size() - i;
            while (true) {
                int i2 = size - 1;
                if (size <= 0) {
                    break;
                }
                ArrayList arrayList2 = r3.A00;
                arrayList2.remove(arrayList2.size() - 1);
                size = i2;
            }
            arrayList = r3.A00;
        } else {
            C12240os r32 = (C12240os) this;
            int i3 = r32.A00 - i;
            while (true) {
                int i4 = i3 - 1;
                if (i3 <= 0) {
                    break;
                }
                ArrayList arrayList3 = r32.A01;
                arrayList3.remove(arrayList3.size() - 1);
                ArrayList arrayList4 = r32.A01;
                arrayList4.remove(arrayList4.size() - 1);
                i3 = i4;
            }
            arrayList = r32.A01;
        }
        arrayList.trimToSize();
    }

    private void A02() {
        if (this.A04.get() == 0) {
            A05();
            C06390bQ r0 = this.A01;
            if (r0 != null) {
                A09(r0.A00);
            }
            A03();
            this.A05 = false;
            this.A07 = false;
            this.A02 = null;
            this.A00 = null;
            this.A03 = null;
            if (this.A01 != null) {
                AnonymousClass20I r1 = this.A06;
                if (r1 != null) {
                    r1.A00 = null;
                }
                A04();
                return;
            }
            return;
        }
        throw new IllegalStateException("Releasing object with non-zero refCount.");
    }

    public void A06() {
        int decrementAndGet = this.A04.decrementAndGet();
        if (decrementAndGet != 1) {
            if (decrementAndGet >= 0) {
                C12250ot r3 = this.A00;
                if (r3 == null) {
                    A02();
                    return;
                }
                throw new IllegalStateException("Trying to release, when added to " + r3);
            }
            throw new IllegalStateException("release() has been called with refCount == 0");
        }
    }

    public void A07() {
        if (!this.A07) {
            C12250ot r3 = this.A00;
            if (r3 != null) {
                throw new IllegalStateException("Already added to " + r3);
            }
            return;
        }
        throw new IllegalStateException("Attempting to re-attach a detached ParamsCollection");
    }

    public void A08() {
        int decrementAndGet = this.A04.decrementAndGet();
        if (decrementAndGet == 1) {
            this.A07 = true;
            this.A00 = null;
            this.A03 = null;
        } else if (decrementAndGet >= 0) {
            A02();
        } else {
            throw new IllegalStateException("releaseFromParent() has been called with refCount == 0");
        }
    }

    public void A0A(C06390bQ r5) {
        int incrementAndGet = this.A04.incrementAndGet();
        if (incrementAndGet == 1) {
            this.A01 = r5;
            AnonymousClass20I r3 = this.A06;
            if (BuildConfig.BUILD_TYPE != 0) {
                if (0 == 0) {
                    r3 = null;
                } else {
                    if (r3 == null) {
                        r3 = new AnonymousClass20I();
                    } else if (r3.A00 != null) {
                        throw new IllegalArgumentException("closeGuard was never released before calling open.", r3.A00);
                    }
                    r3.A00 = new Throwable(AnonymousClass08S.A0P("Explicit termination method '", BuildConfig.BUILD_TYPE, "' not called"));
                }
                this.A06 = r3;
                if (!this.A05) {
                    this.A05 = true;
                    return;
                }
                throw new IllegalStateException("Internal bug, expected object to be immutable");
            }
            throw new NullPointerException("closer == null");
        }
        throw new IllegalStateException(AnonymousClass08S.A09("Acquired object with non-zero initial refCount current = ", incrementAndGet));
    }

    public void A0B(AnonymousClass0oB r2) {
        C000300h.A02(r2, "encoder cannot be null!");
        this.A02 = r2;
    }

    public void A0C(Writer writer) {
        C000300h.A02(writer, "Writer is null!");
        C000300h.A02(this.A02, "No encoder set, please call setEncoder() first!");
        this.A02.AYP(writer, this);
    }

    public void A0D(Writer writer, AnonymousClass0oB r3) {
        C000300h.A02(writer, "Writer is null!");
        AnonymousClass0oB r0 = this.A02;
        if (r0 != null) {
            r3 = r0;
        }
        C000300h.A02(r3, "No encoder available");
        r3.AYP(writer, this);
    }
}
