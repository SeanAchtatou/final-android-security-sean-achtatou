package X;

import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import com.facebook.ui.emoji.model.Emoji;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Qj  reason: invalid class name and case insensitive filesystem */
public final class C23661Qj extends C20831Dz {
    private static final C30502Exn A0A = new C30502Exn(false, false);
    private static final int[] A0B = {16842919};
    private static final int[] A0C = {16842910, -16842919, -16842913};
    private static final int[] A0D = {16842913};
    public int A00;
    public int A01;
    public int A02;
    public C30502Exn A03 = A0A;
    public C26797DBx A04;
    public C14960uQ A05;
    public Emoji A06;
    public final Resources A07;
    public final C46552Qm A08;
    public final List A09 = new ArrayList();

    public static final C23661Qj A00(AnonymousClass1XY r2) {
        return new C23661Qj(r2, C04490Ux.A0L(r2));
    }

    public static void A01(C23661Qj r4, int i, int i2, int i3) {
        r4.A00 = i2;
        C14960uQ r3 = new C14960uQ();
        r4.A05 = r3;
        int[] iArr = A0C;
        GradientDrawable gradientDrawable = (GradientDrawable) r4.A07.getDrawable(2132214372);
        ((GradientDrawable) gradientDrawable.mutate()).setColor(i);
        r3.addState(iArr, gradientDrawable);
        C14960uQ r32 = r4.A05;
        int[] iArr2 = A0B;
        GradientDrawable gradientDrawable2 = (GradientDrawable) r4.A07.getDrawable(2132214373);
        ((GradientDrawable) gradientDrawable2.mutate()).setColor(i2);
        r32.addState(iArr2, gradientDrawable2);
        C14960uQ r33 = r4.A05;
        int[] iArr3 = A0D;
        GradientDrawable gradientDrawable3 = (GradientDrawable) r4.A07.getDrawable(2132214373);
        ((GradientDrawable) gradientDrawable3.mutate()).setColor(i3);
        r33.addState(iArr3, gradientDrawable3);
        r4.A04();
    }

    public int ArU() {
        boolean z = this.A03.A00;
        return (z ? 1 : 0) + this.A09.size() + (this.A03.A01 ? 1 : 0);
    }

    public C23661Qj(AnonymousClass1XY r4, Resources resources) {
        this.A08 = C26564D2a.A00(r4);
        C22341Ky.A00(r4);
        this.A07 = resources;
        int dimensionPixelSize = resources.getDimensionPixelSize(2132148253);
        this.A02 = dimensionPixelSize;
        this.A01 = dimensionPixelSize;
        int color = this.A07.getColor(2132082725);
        A01(this, 0, color, C012509m.A00(color, 0.3f));
        A09(true);
    }
}
