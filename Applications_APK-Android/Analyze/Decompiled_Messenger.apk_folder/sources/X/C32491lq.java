package X;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1lq  reason: invalid class name and case insensitive filesystem */
public class C32491lq extends C191817c {
    private static TimeInterpolator A0B;
    public ArrayList A00 = new ArrayList();
    public ArrayList A01 = new ArrayList();
    public ArrayList A02 = new ArrayList();
    public ArrayList A03 = new ArrayList();
    public ArrayList A04 = new ArrayList();
    public ArrayList A05 = new ArrayList();
    public ArrayList A06 = new ArrayList();
    public ArrayList A07 = new ArrayList();
    public ArrayList A08 = new ArrayList();
    public ArrayList A09 = new ArrayList();
    public ArrayList A0A = new ArrayList();

    public boolean A0R(C33781o8 r9, int i, int i2, int i3, int i4) {
        C33781o8 r3 = r9;
        View view = r9.A0H;
        int translationX = i + ((int) view.getTranslationX());
        int translationY = i2 + ((int) r9.A0H.getTranslationY());
        A02(r9);
        int i5 = i3;
        int i6 = i3 - translationX;
        int i7 = i4;
        int i8 = i4 - translationY;
        if (i6 == 0 && i8 == 0) {
            A0L(r9);
            A0B(r9);
            return false;
        }
        if (i6 != 0) {
            view.setTranslationX((float) (-i6));
        }
        if (i8 != 0) {
            view.setTranslationY((float) (-i8));
        }
        this.A08.add(new C29695EgC(r3, translationX, translationY, i5, i7));
        return true;
    }

    private void A02(C33781o8 r3) {
        if (A0B == null) {
            A0B = new ValueAnimator().getInterpolator();
        }
        r3.A0H.animate().setInterpolator(A0B);
        A0A(r3);
    }

    public static boolean A04(C32491lq r4, C29686Eg3 eg3, C33781o8 r6) {
        if (eg3.A04 == r6) {
            eg3.A04 = null;
        } else if (eg3.A05 != r6) {
            return false;
        } else {
            eg3.A05 = null;
        }
        r6.A0H.setAlpha(1.0f);
        r6.A0H.setTranslationX(0.0f);
        r6.A0H.setTranslationY(0.0f);
        r4.A0B(r6);
        return true;
    }

    public boolean A0S(C33781o8 r20, C33781o8 r21, int i, int i2, int i3, int i4) {
        C33781o8 r14 = r21;
        C33781o8 r7 = r20;
        int i5 = i;
        int i6 = i2;
        int i7 = i4;
        int i8 = i3;
        if (r7 == r14) {
            return A0R(r7, i5, i6, i8, i7);
        }
        float translationX = r7.A0H.getTranslationX();
        float translationY = r7.A0H.getTranslationY();
        float alpha = r7.A0H.getAlpha();
        A02(r7);
        int i9 = (int) (((float) (i3 - i)) - translationX);
        int i10 = (int) (((float) (i4 - i2)) - translationY);
        r7.A0H.setTranslationX(translationX);
        r7.A0H.setTranslationY(translationY);
        r7.A0H.setAlpha(alpha);
        if (r21 != null) {
            A02(r14);
            r14.A0H.setTranslationX((float) (-i9));
            r14.A0H.setTranslationY((float) (-i10));
            r14.A0H.setAlpha(0.0f);
        }
        this.A07.add(new C29686Eg3(r7, r14, i5, i6, i8, i7));
        return true;
    }

    public static void A01(C32491lq r3, List list, C33781o8 r5) {
        for (int size = list.size() - 1; size >= 0; size--) {
            C29686Eg3 eg3 = (C29686Eg3) list.get(size);
            if (A04(r3, eg3, r5) && eg3.A05 == null && eg3.A04 == null) {
                list.remove(eg3);
            }
        }
    }

    public static void A03(List list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            ((C33781o8) list.get(size)).A0H.animate().cancel();
        }
    }

    public boolean A0H(C33781o8 r3, List list) {
        if (!list.isEmpty() || super.A0H(r3, list)) {
            return true;
        }
        return false;
    }

    public boolean A0P(C33781o8 r3) {
        A02(r3);
        r3.A0H.setAlpha(0.0f);
        this.A06.add(r3);
        return true;
    }

    public boolean A0Q(C33781o8 r2) {
        A02(r2);
        this.A09.add(r2);
        return true;
    }

    public void A0T() {
        if (!A0C()) {
            A09();
        }
    }
}
