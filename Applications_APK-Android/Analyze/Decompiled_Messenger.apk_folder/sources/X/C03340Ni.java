package X;

import com.google.common.base.Throwables;
import java.lang.reflect.Field;
import java.util.Map;

/* renamed from: X.0Ni  reason: invalid class name and case insensitive filesystem */
public final class C03340Ni implements Map.Entry {
    public final /* synthetic */ C03350Nj A00;
    public final /* synthetic */ Field A01;

    public static Object A00(Object obj, Field field) {
        field.setAccessible(true);
        try {
            return field.get(obj);
        } catch (IllegalAccessException e) {
            throw Throwables.propagate(e);
        }
    }

    public static void A01(Object obj, Field field, Object obj2) {
        field.setAccessible(true);
        try {
            field.set(obj, obj2);
        } catch (IllegalAccessException e) {
            throw Throwables.propagate(e);
        }
    }

    public C03340Ni(C03350Nj r1, Field field) {
        this.A00 = r1;
        this.A01 = field;
    }

    public Object getKey() {
        return this.A01.getName();
    }

    public Object getValue() {
        return A00(this.A00.A00, this.A01);
    }

    public Object setValue(Object obj) {
        Object value = getValue();
        A01(this.A00.A00, this.A01, obj);
        return value;
    }
}
