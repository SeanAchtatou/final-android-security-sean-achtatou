package X;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.1R7  reason: invalid class name */
public class AnonymousClass1R7 extends ViewGroup.MarginLayoutParams {
    public boolean A00 = false;
    public boolean A01 = true;
    public final Rect A02 = new Rect();
    public C33781o8 mViewHolder;

    public boolean A00() {
        if ((this.mViewHolder.A00 & 2) != 0) {
            return true;
        }
        return false;
    }

    public AnonymousClass1R7(int i, int i2) {
        super(i, i2);
    }

    public AnonymousClass1R7(AnonymousClass1R7 r2) {
        super((ViewGroup.LayoutParams) r2);
    }

    public AnonymousClass1R7(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnonymousClass1R7(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public AnonymousClass1R7(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
