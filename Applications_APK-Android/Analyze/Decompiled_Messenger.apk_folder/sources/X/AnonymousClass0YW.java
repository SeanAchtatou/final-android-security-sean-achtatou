package X;

import android.os.Looper;
import android.os.MessageQueue;

/* renamed from: X.0YW  reason: invalid class name */
public final class AnonymousClass0YW implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appchoreographer.DefaultAppChoreographer$2";
    public final /* synthetic */ AnonymousClass0Y6 A00;

    public AnonymousClass0YW(AnonymousClass0Y6 r1) {
        this.A00 = r1;
    }

    public void run() {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, this.A00.A05)).AOz();
        MessageQueue myQueue = Looper.myQueue();
        if (myQueue != null) {
            myQueue.addIdleHandler(new C05150Xv(this, "DefaultAppChoreographer"));
        }
    }
}
