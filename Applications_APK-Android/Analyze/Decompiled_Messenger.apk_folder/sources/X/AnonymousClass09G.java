package X;

import android.content.Context;
import android.util.Log;
import java.io.File;

/* renamed from: X.09G  reason: invalid class name */
public final class AnonymousClass09G implements AnonymousClass04t {
    public AnonymousClass05d A00;
    public C03950Rc A01;
    public File A02;
    public Integer A03;
    public String A04;

    public static String A01(Integer num) {
        if (num == null) {
            return "null";
        }
        switch (num.intValue()) {
            case 1:
                return "FILE";
            case 2:
                return "STRING";
            default:
                return "INIT";
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0029 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String A00(java.io.File r4) {
        /*
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x002a }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x002a }
            r0.<init>(r4)     // Catch:{ IOException -> 0x002a }
            r3.<init>(r0)     // Catch:{ IOException -> 0x002a }
            long r1 = r4.length()     // Catch:{ all -> 0x0023 }
            int r0 = (int) r1     // Catch:{ all -> 0x0023 }
            byte[] r2 = new byte[r0]     // Catch:{ all -> 0x0023 }
            r3.read(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = new java.lang.String     // Catch:{ all -> 0x0023 }
            java.lang.String r0 = "UTF-8"
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r0)     // Catch:{ all -> 0x0023 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0023 }
            r3.close()     // Catch:{ IOException -> 0x002a }
            return r1
        L_0x0023:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0025 }
        L_0x0025:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x0029 }
        L_0x0029:
            throw r0     // Catch:{ IOException -> 0x002a }
        L_0x002a:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass09G.A00(java.io.File):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0087, code lost:
        if (r2 == false) goto L_0x00d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x011c, code lost:
        if (r0 != null) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0137, code lost:
        if (r0 == null) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0139, code lost:
        r0.A06(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x017a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:88:0x017e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass05d Anp() {
        /*
            r7 = this;
            X.05d r0 = r7.A00
            if (r0 == 0) goto L_0x0005
            return r0
        L_0x0005:
            java.lang.Integer r4 = r7.A03
            int r0 = r4.intValue()
            r3 = 1
            switch(r0) {
                case 0: goto L_0x0025;
                case 1: goto L_0x001f;
                case 2: goto L_0x0033;
                default: goto L_0x000f;
            }
        L_0x000f:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = "ProfiloConfigProvider.getConfigString(): invalid mode: "
            java.lang.String r0 = A01(r4)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x001f:
            r0 = 0
            java.lang.String r4 = A00(r0)
            goto L_0x0035
        L_0x0025:
            java.io.File r2 = new java.io.File
            java.io.File r1 = r7.A02
            java.lang.String r0 = "ProfiloInitFileConfig.json"
            r2.<init>(r1, r0)
            java.lang.String r4 = A00(r2)
            goto L_0x0035
        L_0x0033:
            java.lang.String r4 = r7.A04
        L_0x0035:
            X.0Rc r0 = r7.A01
            if (r0 == 0) goto L_0x003f
            if (r4 != 0) goto L_0x003c
            r3 = 0
        L_0x003c:
            r0.A05(r3)
        L_0x003f:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Integer r3 = r7.A03
            int r0 = r3.intValue()
            r2 = 0
            r6 = 1
            switch(r0) {
                case 0: goto L_0x005f;
                case 1: goto L_0x008e;
                case 2: goto L_0x008e;
                default: goto L_0x004f;
            }
        L_0x004f:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = "ProfiloConfigProvider.deleteOldConfigs(): invalid mode: "
            java.lang.String r0 = A01(r3)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x005f:
            java.io.File r3 = new java.io.File
            java.io.File r1 = r7.A02
            java.lang.String r0 = "ProfiloInitFileConfig.json"
            r3.<init>(r1, r0)
            java.io.File r2 = new java.io.File
            java.io.File r1 = r7.A02
            java.lang.String r0 = "ProfiloInitFileConfig.json.bak"
            r2.<init>(r1, r0)
            boolean r0 = r3.renameTo(r2)
            if (r0 != 0) goto L_0x008c
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x008a
            boolean r0 = r3.delete()
            if (r0 != 0) goto L_0x008a
            r3.deleteOnExit()
            r2 = 0
        L_0x0087:
            if (r2 != 0) goto L_0x00d8
            goto L_0x00d1
        L_0x008a:
            r2 = 1
            goto L_0x0087
        L_0x008c:
            r2 = 1
            goto L_0x00d8
        L_0x008e:
            java.io.File r3 = new java.io.File
            java.io.File r1 = r7.A02
            java.lang.String r0 = "ProfiloInitFileConfig.json"
            r3.<init>(r1, r0)
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x0141
            boolean r0 = r3.delete()
            if (r0 != 0) goto L_0x0141
            r3.deleteOnExit()
            r0 = 0
        L_0x00a7:
            if (r0 != 0) goto L_0x00b1
            java.lang.String r0 = r3.getAbsolutePath()
            r5.append(r0)
            r6 = 0
        L_0x00b1:
            java.io.File r3 = new java.io.File
            java.io.File r1 = r7.A02
            java.lang.String r0 = "ProfiloInitFileConfig.json.bak"
            r3.<init>(r1, r0)
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x013f
            boolean r0 = r3.delete()
            if (r0 != 0) goto L_0x013f
            r3.deleteOnExit()
            r0 = 0
        L_0x00ca:
            if (r0 != 0) goto L_0x013d
            java.lang.String r0 = " "
            r5.append(r0)
        L_0x00d1:
            java.lang.String r0 = r3.getAbsolutePath()
            r5.append(r0)
        L_0x00d8:
            X.0Rc r0 = r7.A01
            if (r0 == 0) goto L_0x00e2
            r5.toString()
            r0.A04(r2)
        L_0x00e2:
            if (r4 != 0) goto L_0x0101
            X.05d r1 = X.C004105a.A01
        L_0x00e6:
            r7.A00 = r1
            X.05d r0 = X.C004105a.A01
            if (r1 == r0) goto L_0x018f
            int[] r1 = X.AnonymousClass09I.A00
            java.lang.Integer r3 = r7.A03
            int r0 = r3.intValue()
            r1 = r1[r0]
            r0 = 1
            if (r1 == r0) goto L_0x01b7
            r0 = 2
            r4 = 0
            if (r1 == r0) goto L_0x01a2
            r0 = 3
            if (r1 != r0) goto L_0x017f
            goto L_0x0144
        L_0x0101:
            r5 = 0
            com.facebook.profilo.config.v2.ConfigParser r0 = new com.facebook.profilo.config.v2.ConfigParser     // Catch:{ ConfigException -> 0x011f }
            r0.<init>(r4)     // Catch:{ ConfigException -> 0x011f }
            com.facebook.profilo.config.v2.Config r2 = r0.parseConfig()     // Catch:{ ConfigException -> 0x011f }
            boolean r0 = r2.isDisablingConfig()     // Catch:{ ConfigException -> 0x011f }
            if (r0 != 0) goto L_0x0117
            X.09J r1 = new X.09J     // Catch:{ ConfigException -> 0x011f }
            r1.<init>(r2)     // Catch:{ ConfigException -> 0x011f }
            goto L_0x0119
        L_0x0117:
            X.05d r1 = X.C004105a.A01     // Catch:{ ConfigException -> 0x011f }
        L_0x0119:
            r5 = 1
            X.0Rc r0 = r7.A01
            if (r0 == 0) goto L_0x00e6
            goto L_0x0139
        L_0x011f:
            r4 = move-exception
            java.lang.String r3 = "Profilo/ProfiloConfigProvider"
            java.lang.String r2 = "Failed to parse config. Mode = "
            java.lang.Integer r0 = r7.A03     // Catch:{ all -> 0x01ba }
            java.lang.String r1 = A01(r0)     // Catch:{ all -> 0x01ba }
            java.lang.String r0 = "."
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x01ba }
            android.util.Log.w(r3, r0, r4)     // Catch:{ all -> 0x01ba }
            X.05d r1 = X.C004105a.A01     // Catch:{ all -> 0x01ba }
            X.0Rc r0 = r7.A01
            if (r0 == 0) goto L_0x00e6
        L_0x0139:
            r0.A06(r5)
            goto L_0x00e6
        L_0x013d:
            r2 = r6
            goto L_0x00d8
        L_0x013f:
            r0 = 1
            goto L_0x00ca
        L_0x0141:
            r0 = 1
            goto L_0x00a7
        L_0x0144:
            java.io.File r2 = r7.A02     // Catch:{ IOException -> 0x01b0 }
            java.lang.String r1 = "ProfiloInitFileConfig.json"
            java.lang.String r0 = ".tmp"
            java.io.File r3 = java.io.File.createTempFile(r1, r0, r2)     // Catch:{ IOException -> 0x01b0 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x01b0 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x01b0 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x01b0 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x01b0 }
            java.lang.String r1 = r7.A04     // Catch:{ all -> 0x0178 }
            java.lang.String r0 = "UTF-8"
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r0)     // Catch:{ all -> 0x0178 }
            byte[] r0 = r1.getBytes(r0)     // Catch:{ all -> 0x0178 }
            r2.write(r0)     // Catch:{ all -> 0x0178 }
            r2.close()     // Catch:{ IOException -> 0x01b0 }
            java.io.File r2 = new java.io.File
            java.io.File r1 = r7.A02
            java.lang.String r0 = "ProfiloInitFileConfig.json"
            r2.<init>(r1, r0)
            boolean r4 = r3.renameTo(r2)
            goto L_0x01b0
        L_0x0178:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x017a }
        L_0x017a:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x017e }
        L_0x017e:
            throw r0     // Catch:{ IOException -> 0x01b0 }
        L_0x017f:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = "ProfiloConfigProvider.writeConfig(): invalid mode: "
            java.lang.String r0 = A01(r3)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x018f:
            java.lang.Integer r1 = r7.A03
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x01b7
            java.io.File r2 = new java.io.File
            java.io.File r1 = r7.A02
            java.lang.String r0 = "ProfiloInitFileConfig.json.bak"
            r2.<init>(r1, r0)
            r2.delete()
            goto L_0x01b7
        L_0x01a2:
            r3 = 0
            java.io.File r2 = new java.io.File
            java.io.File r1 = r7.A02
            java.lang.String r0 = "ProfiloInitFileConfig.json"
            r2.<init>(r1, r0)
            boolean r4 = r3.renameTo(r2)
        L_0x01b0:
            X.0Rc r0 = r7.A01
            if (r0 == 0) goto L_0x01b7
            r0.A07(r4)
        L_0x01b7:
            X.05d r0 = r7.A00
            return r0
        L_0x01ba:
            r1 = move-exception
            X.0Rc r0 = r7.A01
            if (r0 == 0) goto L_0x01c2
            r0.A06(r5)
        L_0x01c2:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass09G.Anp():X.05d");
    }

    public AnonymousClass09G(Context context) {
        this.A03 = AnonymousClass07B.A00;
        File file = new File(context.getCacheDir(), "profilo");
        this.A02 = file;
        if (!file.exists() && !this.A02.mkdir()) {
            this.A00 = C004105a.A01;
            Log.w("Profilo/ProfiloConfigProvider", AnonymousClass08S.A0J("Failed to mkdir ", this.A02.getName()));
        }
    }

    public AnonymousClass09G(Context context, String str, C03950Rc r4) {
        this(context);
        this.A04 = str;
        this.A03 = AnonymousClass07B.A0C;
        this.A01 = r4;
    }
}
