package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.http.interfaces.RequestPriority;

/* renamed from: X.0l7  reason: invalid class name and case insensitive filesystem */
public final class C10940l7 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        try {
            return RequestPriority.valueOf(readString);
        } catch (IllegalArgumentException unused) {
            C010708t.A0B(RequestPriority.A01, "Do not recognize priority %s. Defaulting to %s.", readString, RequestPriority.A00.name());
            return RequestPriority.A00;
        }
    }

    public Object[] newArray(int i) {
        return new RequestPriority[i];
    }
}
