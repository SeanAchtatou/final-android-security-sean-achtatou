package X;

import android.content.Context;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1NO  reason: invalid class name */
public final class AnonymousClass1NO extends C17770zR {
    public static final MigColorScheme A06 = C17190yT.A00();
    @Comparable(type = 13)
    public C34891qL A00;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public MigColorScheme A02 = A06;
    @Comparable(type = 13)
    public Integer A03;
    @Comparable(type = 13)
    public String A04;
    @Comparable(type = 3)
    public boolean A05 = true;

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r4) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(6);
        ComponentBuilderCBuilderShape0_0S0300000.A06(componentBuilderCBuilderShape0_0S0300000, r4, 0, 0, new AnonymousClass1NO(r4.A09));
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public AnonymousClass1NO(Context context) {
        super("M4MigIconButton");
        this.A01 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
