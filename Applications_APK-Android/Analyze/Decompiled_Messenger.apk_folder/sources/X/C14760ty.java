package X;

import android.net.Uri;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.List;

@UserScoped
/* renamed from: X.0ty  reason: invalid class name and case insensitive filesystem */
public final class C14760ty {
    private static C05540Zi A02;
    private static final C32411li A03 = C32411li.A00("thread_tile_data_factory", "thread_no_participants");
    public AnonymousClass0UN A00;
    private final C04310Tq A01;

    public AnonymousClass1H4 A0D(ThreadSummary threadSummary) {
        return A0G(threadSummary, false, 0, true, false);
    }

    public AnonymousClass1H4 A0F(ThreadSummary threadSummary, boolean z) {
        return A0G(threadSummary, false, 0, true, z);
    }

    public static final C14760ty A01(AnonymousClass1XY r4) {
        C14760ty r0;
        synchronized (C14760ty.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new C14760ty((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (C14760ty) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARN: Type inference failed for: r1v6, types: [java.util.List, java.util.ArrayList] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.common.collect.ImmutableList A02(com.google.common.collect.ImmutableList r6) {
        /*
            r5 = this;
            int r2 = X.AnonymousClass1Y3.AUz
            X.0UN r1 = r5.A00
            r0 = 14
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Hx r0 = (X.AnonymousClass1Hx) r0
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 285696929568580(0x103d700001744, double:1.411530380221616E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0039
            r2 = 15
            int r1 = X.AnonymousClass1Y3.AjN
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Hy r0 = (X.C21591Hy) r0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r6)
            X.1IB r0 = r0.A00
            java.util.Collections.sort(r1, r0)
            r6 = r1
        L_0x0039:
            com.google.common.collect.ImmutableList$Builder r4 = com.google.common.collect.ImmutableList.builder()
            int r0 = r6.size()
            r3 = 3
            int r2 = java.lang.Math.min(r3, r0)
            r1 = 0
        L_0x0047:
            if (r1 >= r2) goto L_0x0057
            java.lang.Object r0 = r6.get(r1)
            com.facebook.messaging.model.messages.ParticipantInfo r0 = (com.facebook.messaging.model.messages.ParticipantInfo) r0
            com.facebook.user.model.UserKey r0 = r0.A01
            r4.add(r0)
            int r1 = r1 + 1
            goto L_0x0047
        L_0x0057:
            if (r2 >= r3) goto L_0x0062
            X.0Tq r0 = r5.A01
            java.lang.Object r0 = r0.get()
            r4.add(r0)
        L_0x0062:
            com.google.common.collect.ImmutableList r0 = r4.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14760ty.A02(com.google.common.collect.ImmutableList):com.google.common.collect.ImmutableList");
    }

    private void A04(ThreadSummary threadSummary, ImmutableList immutableList) {
        AnonymousClass0ZF A032 = ((C06230bA) AnonymousClass1XX.A03(AnonymousClass1Y3.BTX, this.A00)).A03(A03);
        if (A032.A0G()) {
            A032.A0A("thread_type", threadSummary.A0S.A05.name());
            A032.A08("is_self_participant", Boolean.valueOf(((C17270yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A3x, this.A00)).A0D(threadSummary)));
            A032.A09("raw_participant_count", Integer.valueOf(threadSummary.A0m.size()));
            A032.A09("participant_count", Integer.valueOf(immutableList.size()));
            A032.A0E();
        }
    }

    private void A05(C21051Ev r5, ThreadSummary threadSummary, String str) {
        boolean A022;
        int i = AnonymousClass1Y3.ADw;
        AnonymousClass1F8 r0 = (AnonymousClass1F8) AnonymousClass1XX.A02(10, i, this.A00);
        AnonymousClass064.A01(r0, "Badges experiment helper cannot be null");
        if (r0.A01(str)) {
            AnonymousClass064.A01((AnonymousClass1F8) AnonymousClass1XX.A02(10, i, this.A00), "Badges experiment helper cannot be null");
            if (threadSummary.A0m.size() > 1000) {
                A022 = false;
            } else {
                A022 = ((AnonymousClass1HB) AnonymousClass1XX.A03(AnonymousClass1Y3.AGy, this.A00)).A02(threadSummary);
            }
            if (A022) {
                r5.A04 = C21381Gs.A01;
            }
        }
    }

    private void A06(C21051Ev r5, ThreadSummary threadSummary, String str) {
        boolean Aer;
        if (!((AnonymousClass1H6) AnonymousClass1XX.A02(7, AnonymousClass1Y3.A8q, this.A00)).A01(threadSummary, true).isEmpty()) {
            if ("bottom_sharesheet_suggestions".equals(str)) {
                Aer = ((AnonymousClass4QY) AnonymousClass1XX.A02(16, AnonymousClass1Y3.BU6, this.A00)).A00.Aem(283003985660033L);
            } else {
                Aer = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass1FZ) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BTP, this.A00)).A00)).Aer(285791418849156L, AnonymousClass0XE.A07);
            }
            if (Aer) {
                r5.A04 = C21381Gs.A0Q;
            }
        }
    }

    private void A07(C21051Ev r7, ImmutableList immutableList) {
        int i = AnonymousClass1Y3.AUZ;
        AnonymousClass0UN r2 = this.A00;
        AnonymousClass064.A01((C21411Gv) AnonymousClass1XX.A02(3, i, r2), "Tile factory cannot be null");
        Boolean bool = (Boolean) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BAo, r2);
        AnonymousClass064.A01(bool, "Environment constant cannot be null");
        if (bool.booleanValue()) {
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                C21381Gs A07 = ((C21411Gv) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AUZ, this.A00)).A07(((ParticipantInfo) it.next()).A01, false, 0, true);
                if (A07 == C21381Gs.A0Y) {
                    r7.A04 = A07;
                    return;
                }
            }
        }
    }

    public Uri A08(ThreadKey threadKey, String str) {
        ((C21071Ex) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AYM, this.A00)).A00.get(threadKey);
        Uri.Builder A032 = ((C417026q) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ALp, this.A00)).A03(true);
        A032.appendQueryParameter("tid", AnonymousClass08S.A0G("t_", threadKey.A0G()));
        if (str != null) {
            A032.appendQueryParameter("hash", str);
        }
        A032.appendQueryParameter("format", "binary");
        return A032.build();
    }

    public Uri A09(ThreadSummary threadSummary) {
        Uri A08;
        C21071Ex r3 = (C21071Ex) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AYM, this.A00);
        AnonymousClass064.A01(r3, "Photo cache cannot be null");
        boolean z = false;
        if (threadSummary.A0u != null) {
            z = true;
        }
        if (!z) {
            boolean z2 = false;
            if (threadSummary.A0E != null) {
                z2 = true;
            }
            if (!z2) {
                r3.A00.get(threadSummary.A0S);
                return null;
            }
        }
        AnonymousClass064.A01(r3, "Photo cache cannot be null");
        Uri uri = threadSummary.A0E;
        boolean z3 = false;
        if (uri != null) {
            z3 = true;
        }
        if (!z3 || C006306i.A02(uri)) {
            A08 = A08(threadSummary.A0S, threadSummary.A0u);
        } else {
            A08 = threadSummary.A0E;
        }
        if (A08.isAbsolute()) {
            return A08;
        }
        return null;
    }

    public AnonymousClass1H4 A0A() {
        C21051Ev r3 = new C21051Ev();
        r3.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, this.A00);
        r3.A07 = true;
        return r3.A00();
    }

    public AnonymousClass1H4 A0B(Uri uri, C21381Gs r6) {
        C21051Ev r3 = new C21051Ev();
        r3.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, this.A00);
        r3.A01 = uri;
        r3.A04 = r6;
        return r3.A00();
    }

    public AnonymousClass1H4 A0C(Uri uri, List list) {
        C21051Ev r3 = new C21051Ev();
        r3.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, this.A00);
        if (uri != null) {
            r3.A01 = uri;
        } else {
            r3.A06 = A03(list);
        }
        return r3.A00();
    }

    public AnonymousClass1H4 A0E(ThreadSummary threadSummary, C21381Gs r8) {
        int i = AnonymousClass1Y3.A3x;
        AnonymousClass0UN r5 = this.A00;
        C17270yd r2 = (C17270yd) AnonymousClass1XX.A02(1, i, r5);
        AnonymousClass064.A01(r2, "Participant utils cannot be null");
        C21051Ev r3 = new C21051Ev();
        r3.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, r5);
        ImmutableList A0A = r2.A0A(threadSummary);
        if (A0A.size() < 2) {
            A04(threadSummary, A0A);
            r3.A07 = true;
            r3.A04 = r8;
        } else {
            r3.A06 = A02(A0A);
            r3.A04 = r8;
        }
        return r3.A00();
    }

    public AnonymousClass1H4 A0G(ThreadSummary threadSummary, boolean z, int i, boolean z2, boolean z3) {
        ImmutableList immutableList;
        ImmutableList of;
        C195218m A0L;
        ImmutableList of2;
        int i2 = AnonymousClass1Y3.AYM;
        AnonymousClass0UN r2 = this.A00;
        AnonymousClass064.A01((C21071Ex) AnonymousClass1XX.A02(11, i2, r2), "Photo cache cannot be null");
        AnonymousClass064.A01((C21771Iq) AnonymousClass1XX.A02(6, AnonymousClass1Y3.A6d, r2), "Gating util cannot be null");
        C21051Ev r3 = new C21051Ev();
        r3.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, r2);
        int size = threadSummary.A0m.size();
        ThreadParticipant A06 = ((C17270yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A3x, this.A00)).A06(threadSummary);
        Uri A09 = A09(threadSummary);
        if (A09 != null) {
            if (z2) {
                A05(r3, threadSummary, "unknown");
            }
            if (z) {
                A06(r3, threadSummary, "unknown");
            }
            if (threadSummary.A0W == null || A06 == null || size != 2) {
                r3.A01 = A09;
                ((C21071Ex) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AYM, this.A00)).A00.get(threadSummary.A0S);
            } else {
                r3.A06 = ImmutableList.of(A06.A00());
            }
        } else if (size == 1) {
            ParticipantInfo participantInfo = ((ThreadParticipant) threadSummary.A0m.get(0)).A04;
            if (((C21441Gz) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BDi, this.A00)).A01(Long.toString(threadSummary.A0S.A01))) {
                r3.A04 = C21381Gs.A0X;
            }
            r3.A06 = ImmutableList.of(participantInfo.A01);
            String str = participantInfo.A03;
            if (str == null) {
                of2 = RegularImmutableList.A02;
            } else {
                of2 = ImmutableList.of(str);
            }
            r3.A05 = of2;
            return r3.A00();
        } else if (size != 2 || !((C17270yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A3x, this.A00)).A0D(threadSummary)) {
            ImmutableList A0A = ((C17270yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A3x, this.A00)).A0A(threadSummary);
            if (threadSummary.A0S.A05 == C28711fF.SMS) {
                ImmutableList.Builder builder = ImmutableList.builder();
                C24971Xv it = A0A.iterator();
                while (it.hasNext()) {
                    builder.add((Object) Strings.nullToEmpty(((ParticipantInfo) it.next()).A03));
                }
                ImmutableList build = builder.build();
                Preconditions.checkNotNull(build);
                r3.A05 = build;
                r3.A04 = C21381Gs.A0R;
                r3.A01 = C006206h.A00(2131231176);
            } else if (z2) {
                A05(r3, threadSummary, "unknown");
            }
            if (z) {
                A06(r3, threadSummary, "unknown");
            }
            A07(r3, A0A);
            if (A0A.size() < 2) {
                A04(threadSummary, A0A);
                r3.A07 = true;
                return r3.A00();
            }
            if (z3) {
                ImmutableList.Builder builder2 = ImmutableList.builder();
                C24971Xv it2 = A0A.iterator();
                while (it2.hasNext()) {
                    builder2.add((Object) ((ParticipantInfo) it2.next()).A01);
                }
                immutableList = builder2.build();
            } else {
                immutableList = A02(A0A);
            }
            r3.A06 = immutableList;
            return r3.A00();
        } else {
            int i3 = AnonymousClass1Y3.AUZ;
            AnonymousClass0UN r5 = this.A00;
            AnonymousClass064.A01((C21411Gv) AnonymousClass1XX.A02(3, i3, r5), "Tile factory cannot be null");
            int i4 = AnonymousClass1Y3.A3x;
            C17270yd r4 = (C17270yd) AnonymousClass1XX.A02(1, i4, r5);
            AnonymousClass064.A01(r4, "Participant utils cannot be null");
            ThreadParticipant A062 = r4.A06(threadSummary);
            if (A062 != null) {
                if (!AnonymousClass1Gw.A00(threadSummary.A0S)) {
                    r3.A06 = ImmutableList.of(A062.A00());
                }
                String str2 = A062.A04.A03;
                if (str2 == null) {
                    of = RegularImmutableList.A02;
                } else {
                    of = ImmutableList.of(str2);
                }
                r3.A05 = of;
                ThreadKey threadKey = threadSummary.A0S;
                if (ThreadKey.A09(threadKey)) {
                    A07(r3, ((C17270yd) AnonymousClass1XX.A02(1, i4, this.A00)).A0A(threadSummary));
                } else if (((C21441Gz) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BDi, this.A00)).A01(Long.toString(threadKey.A01)) && ThreadKey.A0B(threadKey)) {
                    r3.A04 = C21381Gs.A0X;
                } else if (ThreadKey.A0F(threadKey)) {
                    r3.A04 = C21381Gs.A0T;
                } else {
                    if (ThreadKey.A0E(threadKey)) {
                        C50452e3 r6 = null;
                        if (threadKey != null && ((int) threadKey.A0G()) == -102) {
                            r6 = C50452e3.BUSINESS;
                        }
                        if (r6 != null) {
                            r3.A04 = C21381Gs.A0L;
                            AnonymousClass1XX.A03(AnonymousClass1Y3.A6v, this.A00);
                            MigColorScheme migColorScheme = (MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, this.A00);
                            if (r6.ordinal() != 0) {
                                throw new IllegalArgumentException();
                            }
                            int i5 = 2132346699;
                            if (C012609n.A02(migColorScheme.B9m())) {
                                i5 = 2132346700;
                            }
                            r3.A01 = Uri.parse(AnonymousClass08S.A0J("res:///", String.valueOf(i5)));
                            r3.A00 = ((C21401Gu) AnonymousClass1XX.A02(9, AnonymousClass1Y3.B4t, this.A00)).A01();
                        } else {
                            r3.A04 = C21381Gs.A0R;
                            String str3 = A062.A04.A05;
                            if (str3 != null) {
                                r3.A06 = ImmutableList.of(UserKey.A01(str3));
                            }
                            int i6 = AnonymousClass1Y3.B4t;
                            AnonymousClass0UN r1 = this.A00;
                            C21401Gu r42 = (C21401Gu) AnonymousClass1XX.A02(9, i6, r1);
                            AnonymousClass064.A01(r42, "Sms pref helper cannot be null");
                            if (((C52752jd) AnonymousClass1XX.A02(18, AnonymousClass1Y3.B44, r1)).A05(threadSummary) != r42.A01()) {
                                r3.A00 = ((C52752jd) AnonymousClass1XX.A02(18, AnonymousClass1Y3.B44, this.A00)).A05(threadSummary);
                            }
                        }
                    }
                    C21381Gs A07 = ((C21411Gv) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AUZ, this.A00)).A07(A062.A00(), z, i, z2);
                    User A032 = ((C14300t0) AnonymousClass1XX.A02(19, AnonymousClass1Y3.AxE, this.A00)).A03(A062.A00());
                    if (A032 != null && !A032.A0F() && (A07 == C21381Gs.A01 || A07 == C21381Gs.A0Q)) {
                        boolean Aem = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C35461rG) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ABB, this.A00)).A00)).Aem(282437049517457L);
                        int i7 = AnonymousClass1Y3.ATb;
                        if (Aem) {
                            A0L = ((AnonymousClass0r6) AnonymousClass1XX.A02(17, i7, this.A00)).A0L(A062.A00(), i);
                        } else {
                            A0L = ((AnonymousClass0r6) AnonymousClass1XX.A02(17, i7, this.A00)).A0L(A062.A00(), -1);
                        }
                        if (A0L.A07 == AnonymousClass07B.A01) {
                            A07 = C21381Gs.A0L;
                        }
                    }
                    r3.A04 = A07;
                }
            }
        }
        return r3.A00();
    }

    public AnonymousClass1H4 A0H(User user) {
        C21411Gv r2 = (C21411Gv) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AUZ, this.A00);
        AnonymousClass064.A01(r2, "Tile factory cannot be null");
        return A0I(user, C21411Gv.A02(r2, user, false, 0, true));
    }

    public AnonymousClass1H4 A0I(User user, C21381Gs r8) {
        ImmutableList of;
        User user2;
        int i = AnonymousClass1Y3.AUZ;
        AnonymousClass0UN r5 = this.A00;
        C21411Gv r4 = (C21411Gv) AnonymousClass1XX.A02(3, i, r5);
        AnonymousClass064.A01(r4, "Tile factory cannot be null");
        UserKey userKey = user.A0Q;
        if (userKey.A08() && (user2 = user.A0N) != null) {
            userKey = user2.A0Q;
        }
        C21051Ev r2 = new C21051Ev();
        r2.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, r5);
        r2.A04 = C21411Gv.A02(r4, user, false, 0, true);
        r2.A02 = user.A04();
        r2.A06 = ImmutableList.of(userKey);
        String A09 = user.A09();
        if (A09 == null) {
            of = RegularImmutableList.A02;
        } else {
            of = ImmutableList.of(A09);
        }
        r2.A05 = of;
        r2.A04 = r8;
        return r2.A00();
    }

    public AnonymousClass1H4 A0J(UserKey userKey) {
        C21411Gv r2 = (C21411Gv) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AUZ, this.A00);
        AnonymousClass064.A01(r2, "Tile factory cannot be null");
        return A0K(userKey, r2.A07(userKey, false, 0, true));
    }

    public AnonymousClass1H4 A0K(UserKey userKey, C21381Gs r6) {
        C21051Ev r3 = new C21051Ev();
        r3.A06 = ImmutableList.of(userKey);
        r3.A04 = r6;
        r3.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, this.A00);
        return r3.A00();
    }

    public AnonymousClass1H4 A0L(ImmutableList immutableList, C21381Gs r6) {
        C21051Ev r3 = new C21051Ev();
        r3.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, this.A00);
        r3.A06 = A03(immutableList);
        r3.A04 = r6;
        return r3.A00();
    }

    public AnonymousClass1H4 A0M(ImmutableList immutableList, boolean z) {
        C21051Ev r3 = new C21051Ev();
        r3.A03 = (C21391Gt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBV, this.A00);
        if (!z) {
            immutableList = A03(immutableList);
        }
        r3.A06 = immutableList;
        return r3.A00();
    }

    private C14760ty(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(20, r3);
        this.A01 = AnonymousClass0XJ.A0J(r3);
    }

    public static final C14760ty A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    private static ImmutableList A03(List list) {
        ImmutableList.Builder builder = ImmutableList.builder();
        int min = Math.min(list.size(), 3);
        for (int i = 0; i < min; i++) {
            builder.add(list.get(i));
        }
        return builder.build();
    }
}
