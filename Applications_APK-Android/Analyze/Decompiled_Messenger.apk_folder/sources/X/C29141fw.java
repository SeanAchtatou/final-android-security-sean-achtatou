package X;

import java.io.Serializable;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

/* renamed from: X.1fw  reason: invalid class name and case insensitive filesystem */
public final class C29141fw extends C184112y implements Serializable {
    private static final long serialVersionUID = 1;
    public final transient Method _method;
    public Class[] _paramClasses;
    public B3U _serialization;

    public final Object call1(Object obj) {
        return this._method.invoke(null, obj);
    }

    public /* bridge */ /* synthetic */ AnnotatedElement getAnnotated() {
        return this._method;
    }

    public Class getDeclaringClass() {
        return this._method.getDeclaringClass();
    }

    public String getFullName() {
        return getDeclaringClass().getName() + "#" + getName() + "(" + getParameterCount() + " params)";
    }

    public Type getGenericParameterType(int i) {
        Type[] genericParameterTypes = this._method.getGenericParameterTypes();
        if (i >= genericParameterTypes.length) {
            return null;
        }
        return genericParameterTypes[i];
    }

    public Type getGenericType() {
        return this._method.getGenericReturnType();
    }

    public /* bridge */ /* synthetic */ Member getMember() {
        return this._method;
    }

    public String getName() {
        return this._method.getName();
    }

    public int getParameterCount() {
        if (this._paramClasses == null) {
            this._paramClasses = this._method.getParameterTypes();
        }
        return this._paramClasses.length;
    }

    public Class getRawParameterType(int i) {
        if (this._paramClasses == null) {
            this._paramClasses = this._method.getParameterTypes();
        }
        Class[] clsArr = this._paramClasses;
        if (i >= clsArr.length) {
            return null;
        }
        return clsArr[i];
    }

    public Class getRawType() {
        return this._method.getReturnType();
    }

    public C10030jR getType(C28311eb r2) {
        return getType(r2, this._method.getTypeParameters());
    }

    public Object getValue(Object obj) {
        try {
            return this._method.invoke(obj, new Object[0]);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Failed to getValue() with method ", getFullName(), ": ", e.getMessage()), e);
        } catch (InvocationTargetException e2) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Failed to getValue() with method ", getFullName(), ": ", e2.getMessage()), e2);
        }
    }

    public Object readResolve() {
        B3U b3u = this._serialization;
        Class cls = b3u.clazz;
        try {
            Method declaredMethod = cls.getDeclaredMethod(b3u.name, b3u.args);
            if (!declaredMethod.isAccessible()) {
                C29081fq.checkAndFixAccess(declaredMethod);
            }
            return new C29141fw(declaredMethod, null, null);
        } catch (Exception unused) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Could not find method '", this._serialization.name, "' from Class '", cls.getName()));
        }
    }

    public void setValue(Object obj, Object obj2) {
        try {
            this._method.invoke(obj, obj2);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Failed to setValue() with method ", getFullName(), ": ", e.getMessage()), e);
        } catch (InvocationTargetException e2) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Failed to setValue() with method ", getFullName(), ": ", e2.getMessage()), e2);
        }
    }

    public String toString() {
        return AnonymousClass08S.A0P("[method ", getFullName(), "]");
    }

    public Object writeReplace() {
        return new C29141fw(new B3U(this._method));
    }

    private C29141fw(B3U b3u) {
        super(null, null);
        this._method = null;
        this._serialization = b3u;
    }

    public C29141fw(Method method, C10090jX r4, C10090jX[] r5) {
        super(r4, r5);
        if (method != null) {
            this._method = method;
            return;
        }
        throw new IllegalArgumentException("Can not construct AnnotatedMethod with null Method");
    }

    public final Object call() {
        return this._method.invoke(null, new Object[0]);
    }

    public final Object call(Object[] objArr) {
        return this._method.invoke(null, objArr);
    }
}
