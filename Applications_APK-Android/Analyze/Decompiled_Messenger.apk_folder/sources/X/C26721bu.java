package X;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1bu  reason: invalid class name and case insensitive filesystem */
public final class C26721bu {
    public int _collCount;
    public int _collEnd;
    public C56002p6[] _collList;
    public boolean _collListShared;
    public int _count;
    public final int _hashSeed;
    public final boolean _intern;
    public int _longestCollisionList;
    public int[] _mainHash;
    public int _mainHashMask;
    public boolean _mainHashShared;
    public C07580dn[] _mainNames;
    public boolean _mainNamesShared;
    public transient boolean _needRehash;
    public final C26721bu _parent;
    public final AtomicReference _tableInfo;

    public static int calcHash(C26721bu r3, int[] iArr, int i) {
        if (i >= 3) {
            int i2 = iArr[0] ^ r3._hashSeed;
            int i3 = (((i2 + (i2 >>> 9)) * 33) + iArr[1]) * 65599;
            int i4 = (i3 + (i3 >>> 15)) ^ iArr[2];
            int i5 = i4 + (i4 >>> 17);
            for (int i6 = 3; i6 < i; i6++) {
                int i7 = (i5 * 31) ^ iArr[i6];
                int i8 = i7 + (i7 >>> 3);
                i5 = i8 ^ (i8 << 7);
            }
            int i9 = i5 + (i5 >>> 15);
            return (i9 << 9) ^ i9;
        }
        throw new IllegalArgumentException();
    }

    public static int findBestBucket(C26721bu r7) {
        C56002p6[] r6 = r7._collList;
        int i = r7._collEnd;
        int i2 = Integer.MAX_VALUE;
        int i3 = -1;
        for (int i4 = 0; i4 < i; i4++) {
            int i5 = r6[i4]._length;
            if (i5 < i2) {
                if (i5 == 1) {
                    return i4;
                }
                i3 = i4;
                i2 = i5;
            }
        }
        return i3;
    }

    public static C07570dm initTableInfo(int i) {
        return new C07570dm(0, i - 1, new int[i], new C07580dn[i], null, 0, 0, 0);
    }

    public C26721bu(int i, boolean z, int i2) {
        this._parent = null;
        this._hashSeed = i2;
        this._intern = z;
        int i3 = 16;
        if (i >= 16) {
            if (((i - 1) & i) == 0) {
                i3 = i;
            } else {
                while (i3 < i) {
                    i3 += i3;
                }
            }
        }
        this._tableInfo = new AtomicReference(initTableInfo(i3));
    }

    public C26721bu(C26721bu r2, boolean z, int i, C07570dm r5) {
        this._parent = r2;
        this._hashSeed = i;
        this._intern = z;
        this._tableInfo = null;
        this._count = r5.count;
        this._mainHashMask = r5.mainHashMask;
        this._mainHash = r5.mainHash;
        this._mainNames = r5.mainNames;
        this._collList = r5.collList;
        this._collCount = r5.collCount;
        this._collEnd = r5.collEnd;
        this._longestCollisionList = r5.longestCollisionList;
        this._needRehash = false;
        this._mainHashShared = true;
        this._mainNamesShared = true;
        this._collListShared = true;
    }

    public C07580dn findName(int i, int i2) {
        int i3;
        C56002p6 r0;
        if (i2 == 0) {
            int i4 = i ^ this._hashSeed;
            int i5 = i4 + (i4 >>> 15);
            i3 = i5 ^ (i5 >>> 9);
        } else {
            int i6 = ((i ^ (i >>> 15)) + (i2 * 33)) ^ this._hashSeed;
            i3 = i6 + (i6 >>> 7);
        }
        int i7 = this._mainHashMask & i3;
        int i8 = this._mainHash[i7];
        if ((((i8 >> 8) ^ i3) << 8) == 0) {
            C07580dn r2 = this._mainNames[i7];
            if (r2 != null) {
                if (r2.equals(i, i2)) {
                    return r2;
                }
            }
            return null;
        } else if (i8 == 0) {
            return null;
        }
        int i9 = i8 & 255;
        if (i9 > 0 && (r0 = this._collList[i9 - 1]) != null) {
            return r0.find(i3, i, i2);
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        if (r2 == 0) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C07580dn findName(int[] r6, int r7) {
        /*
            r5 = this;
            r0 = 3
            if (r7 >= r0) goto L_0x0011
            r2 = 0
            r1 = r6[r2]
            r0 = 2
            if (r7 < r0) goto L_0x000c
            r0 = 1
            r2 = r6[r0]
        L_0x000c:
            X.0dn r0 = r5.findName(r1, r2)
            return r0
        L_0x0011:
            int r3 = calcHash(r5, r6, r7)
            int r1 = r5._mainHashMask
            r1 = r1 & r3
            int[] r0 = r5._mainHash
            r2 = r0[r1]
            int r0 = r2 >> 8
            r0 = r0 ^ r3
            int r0 = r0 << 8
            r4 = 0
            if (r0 != 0) goto L_0x0031
            X.0dn[] r0 = r5._mainNames
            r1 = r0[r1]
            if (r1 == 0) goto L_0x0030
            boolean r0 = r1.equals(r6, r7)
            if (r0 == 0) goto L_0x0034
        L_0x0030:
            return r1
        L_0x0031:
            if (r2 != 0) goto L_0x0034
        L_0x0033:
            return r4
        L_0x0034:
            r0 = r2 & 255(0xff, float:3.57E-43)
            if (r0 <= 0) goto L_0x0033
            int r1 = r0 + -1
            X.2p6[] r0 = r5._collList
            r1 = r0[r1]
            if (r1 == 0) goto L_0x0033
        L_0x0040:
            X.0dn r2 = r1._name
            int r0 = r2.hashCode()
            if (r0 != r3) goto L_0x004f
            boolean r0 = r2.equals(r6, r7)
            if (r0 == 0) goto L_0x004f
            return r2
        L_0x004f:
            X.2p6 r1 = r1._next
            if (r1 != 0) goto L_0x0040
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26721bu.findName(int[], int):X.0dn");
    }
}
