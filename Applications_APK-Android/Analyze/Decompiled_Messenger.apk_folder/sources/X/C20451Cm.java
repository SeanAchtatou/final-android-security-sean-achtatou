package X;

import com.facebook.payments.logging.PaymentsFlowStep;
import com.facebook.payments.logging.PaymentsLoggingSessionData;
import com.facebook.payments.model.PaymentItemType;
import com.facebook.payments.shipping.model.ShippingAddressFormInput;

/* renamed from: X.1Cm  reason: invalid class name and case insensitive filesystem */
public final class C20451Cm extends C06020ai {
    public final /* synthetic */ PaymentsFlowStep A00;
    public final /* synthetic */ PaymentsLoggingSessionData A01;
    public final /* synthetic */ PaymentItemType A02;
    public final /* synthetic */ C55122nZ A03;
    public final /* synthetic */ ShippingAddressFormInput A04;

    public C20451Cm(C55122nZ r1, PaymentsLoggingSessionData paymentsLoggingSessionData, ShippingAddressFormInput shippingAddressFormInput, PaymentsFlowStep paymentsFlowStep, PaymentItemType paymentItemType) {
        this.A03 = r1;
        this.A01 = paymentsLoggingSessionData;
        this.A04 = shippingAddressFormInput;
        this.A00 = paymentsFlowStep;
        this.A02 = paymentItemType;
    }
}
