package X;

/* renamed from: X.1gL  reason: invalid class name and case insensitive filesystem */
public final class C29391gL implements AnonymousClass06U {
    private final C29371gJ A00;

    public C29391gL(C29371gJ r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0029, code lost:
        if (((X.C72013dW) r7.A01.peek()).A00 > r7.A00.now()) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Bl1(android.content.Context r10, android.content.Intent r11, X.AnonymousClass06Y r12) {
        /*
            r9 = this;
            r0 = -1558756800(0xffffffffa3174240, float:-8.199755E-18)
            int r4 = X.AnonymousClass09Y.A00(r0)
            X.1gJ r7 = r9.A00
            monitor-enter(r7)
            com.google.common.collect.ImmutableList$Builder r8 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x0062 }
        L_0x000e:
            java.util.PriorityQueue r0 = r7.A01     // Catch:{ all -> 0x0062 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0062 }
            if (r0 != 0) goto L_0x002b
            java.util.PriorityQueue r0 = r7.A01     // Catch:{ all -> 0x0062 }
            java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x0062 }
            X.3dW r0 = (X.C72013dW) r0     // Catch:{ all -> 0x0062 }
            long r5 = r0.A00     // Catch:{ all -> 0x0062 }
            X.069 r0 = r7.A00     // Catch:{ all -> 0x0062 }
            long r2 = r0.now()     // Catch:{ all -> 0x0062 }
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x002c
        L_0x002b:
            r0 = 1
        L_0x002c:
            if (r0 != 0) goto L_0x003c
            java.util.PriorityQueue r0 = r7.A01     // Catch:{ all -> 0x0062 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0062 }
            X.3dW r0 = (X.C72013dW) r0     // Catch:{ all -> 0x0062 }
            X.14Y r0 = r0.A01     // Catch:{ all -> 0x0062 }
            r8.add(r0)     // Catch:{ all -> 0x0062 }
            goto L_0x000e
        L_0x003c:
            com.google.common.collect.ImmutableList r0 = r8.build()     // Catch:{ all -> 0x0062 }
            X.C29371gJ.A02(r7)     // Catch:{ all -> 0x0062 }
            monitor-exit(r7)     // Catch:{ all -> 0x0062 }
            r0.size()
            X.1Xv r1 = r0.iterator()
        L_0x004b:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x005b
            java.lang.Object r0 = r1.next()
            X.14Y r0 = (X.AnonymousClass14Y) r0
            r0.run()
            goto L_0x004b
        L_0x005b:
            r0 = 559592209(0x215ab311, float:7.4098265E-19)
            X.AnonymousClass09Y.A01(r0, r4)
            return
        L_0x0062:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0062 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29391gL.Bl1(android.content.Context, android.content.Intent, X.06Y):void");
    }
}
