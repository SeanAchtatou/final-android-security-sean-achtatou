package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.inboxads.common.InboxAdsItem;

/* renamed from: X.1sH  reason: invalid class name and case insensitive filesystem */
public final class C36071sH implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxAdsItem(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxAdsItem[i];
    }
}
