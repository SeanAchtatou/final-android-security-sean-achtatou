package X;

/* renamed from: X.0FP  reason: invalid class name */
public final class AnonymousClass0FP extends AnonymousClass0FM {
    public long cameraOpenTimeMs;
    public long cameraPreviewTimeMs;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass0FP r7 = (AnonymousClass0FP) obj;
            if (!(this.cameraPreviewTimeMs == r7.cameraPreviewTimeMs && this.cameraOpenTimeMs == r7.cameraOpenTimeMs)) {
                return false;
            }
        }
        return true;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r3) {
        AnonymousClass0FP r32 = (AnonymousClass0FP) r3;
        this.cameraPreviewTimeMs = r32.cameraPreviewTimeMs;
        this.cameraOpenTimeMs = r32.cameraOpenTimeMs;
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        AnonymousClass0FP r52 = (AnonymousClass0FP) r5;
        AnonymousClass0FP r62 = (AnonymousClass0FP) r6;
        if (r62 == null) {
            r62 = new AnonymousClass0FP();
        }
        if (r52 == null) {
            r62.cameraPreviewTimeMs = this.cameraPreviewTimeMs;
            r62.cameraOpenTimeMs = this.cameraOpenTimeMs;
            return r62;
        }
        r62.cameraPreviewTimeMs = this.cameraPreviewTimeMs - r52.cameraPreviewTimeMs;
        r62.cameraOpenTimeMs = this.cameraOpenTimeMs - r52.cameraOpenTimeMs;
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        AnonymousClass0FP r52 = (AnonymousClass0FP) r5;
        AnonymousClass0FP r62 = (AnonymousClass0FP) r6;
        if (r62 == null) {
            r62 = new AnonymousClass0FP();
        }
        if (r52 == null) {
            r62.cameraPreviewTimeMs = this.cameraPreviewTimeMs;
            r62.cameraOpenTimeMs = this.cameraOpenTimeMs;
            return r62;
        }
        r62.cameraPreviewTimeMs = this.cameraPreviewTimeMs + r52.cameraPreviewTimeMs;
        r62.cameraOpenTimeMs = this.cameraOpenTimeMs + r52.cameraOpenTimeMs;
        return r62;
    }

    public int hashCode() {
        long j = this.cameraPreviewTimeMs;
        long j2 = this.cameraOpenTimeMs;
        return (((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "CameraMetrics{cameraPreviewTimeMs=" + this.cameraPreviewTimeMs + ", cameraOpenTimeMs=" + this.cameraOpenTimeMs + '}';
    }
}
