package X;

import com.google.common.base.Preconditions;
import java.util.AbstractSet;
import java.util.Collection;

/* renamed from: X.1r6  reason: invalid class name and case insensitive filesystem */
public abstract class C35361r6<E> extends AbstractSet<E> {
    public boolean removeAll(Collection collection) {
        return C25011Xz.A0B(this, collection);
    }

    public boolean retainAll(Collection collection) {
        Preconditions.checkNotNull(collection);
        return super.retainAll(collection);
    }
}
