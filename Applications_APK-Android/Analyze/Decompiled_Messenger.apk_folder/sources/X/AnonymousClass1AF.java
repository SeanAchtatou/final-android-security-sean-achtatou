package X;

/* renamed from: X.1AF  reason: invalid class name */
public final class AnonymousClass1AF {
    public C30428Ew3 A00;
    public Integer A01;
    public boolean A02;
    private final C15590vX A03;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r2 == X.AnonymousClass07B.A0C) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A00() {
        /*
            r3 = this;
            java.lang.Integer r2 = r3.A01
            if (r2 == 0) goto L_0x000d
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            if (r2 == r0) goto L_0x000d
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r0 = 0
            if (r2 != r1) goto L_0x000e
        L_0x000d:
            r0 = 1
        L_0x000e:
            if (r0 == 0) goto L_0x0015
            boolean r1 = r3.A02
            r0 = 1
            if (r1 == 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AF.A00():boolean");
    }

    public void A01() {
        C30428Ew3 ew3 = this.A00;
        if (ew3 != null && A00()) {
            this.A03.C3C(ew3.A00, ew3.A01);
            this.A00 = null;
        }
    }

    public AnonymousClass1AF(C15590vX r1) {
        this.A03 = r1;
    }

    public void A02(int i, int i2) {
        if (A00()) {
            this.A03.C3C(i, i2);
        } else {
            this.A00 = new C30428Ew3(i, i2);
        }
    }
}
