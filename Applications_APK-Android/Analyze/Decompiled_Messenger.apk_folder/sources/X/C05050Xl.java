package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Xl  reason: invalid class name and case insensitive filesystem */
public final class C05050Xl {
    private static volatile C05050Xl A01;
    public final FbSharedPreferences A00;

    public static final C05050Xl A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C05050Xl.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C05050Xl(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C05050Xl(AnonymousClass1XY r2) {
        this.A00 = FbSharedPreferencesModule.A00(r2);
    }
}
