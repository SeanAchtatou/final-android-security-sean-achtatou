package X;

import android.content.pm.PackageManager;

/* renamed from: X.0hC  reason: invalid class name and case insensitive filesystem */
public final class C09370hC {
    public final PackageManager A00;
    public final String A01;

    public static final C09370hC A00(AnonymousClass1XY r3) {
        return new C09370hC(C04490Ux.A0J(r3), C04490Ux.A0t(r3));
    }

    public C09370hC(PackageManager packageManager, String str) {
        this.A00 = packageManager;
        this.A01 = str;
    }
}
