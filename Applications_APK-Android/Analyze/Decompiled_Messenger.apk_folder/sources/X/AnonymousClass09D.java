package X;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.os.DeadObjectException;
import com.facebook.resources.compat.RedexResourcesCompat;

/* renamed from: X.09D  reason: invalid class name */
public final class AnonymousClass09D {
    public static C04170Ss A00(Context context) {
        String A01 = A01(context, "google_app_id");
        String A012 = A01(context, "google_api_key");
        if (A01 == null || A012 == null) {
            return null;
        }
        AnonymousClass04Y r1 = new AnonymousClass04Y();
        r1.A02 = A01(context, "firebase_database_url");
        r1.A03 = A01(context, "gcm_defaultSenderId");
        r1.A04 = A01(context, "project_id");
        AnonymousClass09E.A05(A01, "ApplicationId must be set.");
        r1.A01 = A01;
        AnonymousClass09E.A05(A012, "ApiKey must be set.");
        r1.A00 = A012;
        return r1.A00();
    }

    private static String A01(Context context, String str) {
        String packageName = context.getPackageName();
        Resources resources = context.getResources();
        int identifier = RedexResourcesCompat.getIdentifier(resources, str, "string", packageName);
        if (identifier == 0) {
            return null;
        }
        return resources.getString(identifier);
    }

    public static boolean A02(Context context) {
        AnonymousClass1WN r1;
        C04170Ss A00 = A00(context);
        if (A00 == null) {
            C010708t.A0I("FirebaseInitHelper", "FirebaseApp custom init failure: options is null.");
            return false;
        }
        try {
            if (context.getApplicationContext() instanceof Application) {
                Application application = (Application) context.getApplicationContext();
                if (AnonymousClass1WK.A00.get() == null) {
                    AnonymousClass1WK r2 = new AnonymousClass1WK();
                    if (AnonymousClass1WK.A00.compareAndSet(null, r2)) {
                        AnonymousClass1WM.A00(application);
                        AnonymousClass1WM.A04.A02(r2);
                    }
                }
            }
            String trim = "[DEFAULT]".trim();
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            synchronized (AnonymousClass1WN.A0A) {
                boolean z = false;
                if (!AnonymousClass1WN.A0B.containsKey(trim)) {
                    z = true;
                }
                AnonymousClass09E.A09(z, AnonymousClass08S.A0P("FirebaseApp name ", trim, " already exists!"));
                AnonymousClass09E.A02(context, "Application context cannot be null.");
                r1 = new AnonymousClass1WN(context, trim, A00);
                AnonymousClass1WN.A0B.put(trim, r1);
            }
            AnonymousClass1WN.A03(r1);
            return true;
        } catch (RuntimeException e) {
            if (e.getCause() instanceof DeadObjectException) {
                C010708t.A0L("FirebaseInitHelper", "DeadObjectException thrown during Firebase initialization.", e);
                return false;
            }
            throw e;
        }
    }
}
