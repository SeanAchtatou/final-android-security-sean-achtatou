package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.12L  reason: invalid class name */
public final class AnonymousClass12L implements C15520vQ {
    private static volatile AnonymousClass12L A02;
    private AnonymousClass0UN A00;
    private final AnonymousClass1MT A01;

    public String B5H() {
        return "AddContactsButton";
    }

    public static final AnonymousClass12L A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass12L.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass12L(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public C15390vD Ae6() {
        return (C15390vD) AnonymousClass1XX.A03(AnonymousClass1Y3.A9f, this.A00);
    }

    public AnonymousClass10Z Ap6(Context context) {
        return new AnonymousClass10Z(AnonymousClass01R.A03(context, this.A01.A02(C34891qL.A0c, AnonymousClass07B.A01)), 21, -4, 27, 0);
    }

    private AnonymousClass12L(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = AnonymousClass1MT.A00(r3);
    }

    public String AiM(Context context) {
        return context.getString(2131821216);
    }

    public void BSD(Context context, C33691nz r2, C13060qW r3) {
        r2.BvP(context);
    }
}
