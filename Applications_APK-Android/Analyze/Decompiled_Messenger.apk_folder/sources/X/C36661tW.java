package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.1tW  reason: invalid class name and case insensitive filesystem */
public final class C36661tW extends C36671tX {
    public static final C36661tW A00(AnonymousClass1XY r4) {
        return new C36661tW(AnonymousClass067.A02(), FbSharedPreferencesModule.A00(r4), C06920cI.A00(r4));
    }

    public C36661tW(AnonymousClass06B r1, FbSharedPreferences fbSharedPreferences, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        super(r1, fbSharedPreferences, deprecatedAnalyticsLogger);
    }
}
