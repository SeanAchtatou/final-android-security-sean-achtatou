package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.stringformat.StringFormatUtil;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@UserScoped
/* renamed from: X.21Y  reason: invalid class name */
public final class AnonymousClass21Y {
    private static C05540Zi A04;
    public final C09330h1 A00 = new C09330h1(500);
    public final C16920y2 A01;
    private final AnonymousClass06B A02;
    private final SimpleDateFormat A03 = new SimpleDateFormat(TurboLoader.Locator.$const$string(116), Locale.US);

    public static final AnonymousClass21Y A00(AnonymousClass1XY r4) {
        AnonymousClass21Y r0;
        synchronized (AnonymousClass21Y.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new AnonymousClass21Y((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (AnonymousClass21Y) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A01(AnonymousClass21Y r4, char c, String str, String str2, Object... objArr) {
        if (r4.A01.A00.Aem(282256660759817L)) {
            if (objArr.length > 0) {
                str2 = StringFormatUtil.formatStrLocaleSafe(str2, objArr);
            }
            r4.A00.A04(StringFormatUtil.formatStrLocaleSafe("%s %c/%s: %s", r4.A03.format(new Date(r4.A02.now())), Character.valueOf(c), str, str2));
        }
    }

    public void A02(String str, String str2, Object... objArr) {
        A01(this, 'D', str, str2, objArr);
    }

    private AnonymousClass21Y(AnonymousClass1XY r4) {
        this.A01 = new C16920y2(r4);
        this.A02 = AnonymousClass067.A02();
    }

    public void A03(String str, String str2, Object... objArr) {
        C010708t.A0O(str, str2, objArr);
        A01(this, 'E', str, str2, objArr);
    }

    public void A04(String str, Throwable th, String str2, Object... objArr) {
        C010708t.A0U(str, th, str2, objArr);
        if (this.A01.A00.Aem(282256660759817L)) {
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            A01(this, 'E', str, StringFormatUtil.formatStrLocaleSafe("%s\n%s", str2, stringWriter.toString()), objArr);
        }
    }
}
