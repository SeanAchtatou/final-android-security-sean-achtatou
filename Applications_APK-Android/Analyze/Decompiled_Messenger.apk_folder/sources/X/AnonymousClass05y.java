package X;

import android.os.Build;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.05y  reason: invalid class name */
public final class AnonymousClass05y implements AnonymousClass03a {
    public void Bse() {
        AnonymousClass05x.A00();
        if (C001000r.A02(1)) {
            StringBuilder sb = new StringBuilder(127);
            sb.append("Android trace tags: ");
            sb.append(AnonymousClass00I.A00("debug.atrace.tags.enableflags", 0));
            sb.append(", Facebook trace tags: ");
            sb.append(C001000r.A01);
            AnonymousClass08Z.A04(64, "process_labels", sb.toString(), 0);
        }
        if (C001000r.A02(64)) {
            AnonymousClass08Z.A04(64, "process_name", AnonymousClass00H.A00(), 0);
            String A02 = AnonymousClass00I.A02("dalvik.vm.heapgrowthlimit");
            String A022 = AnonymousClass00I.A02("dalvik.vm.heapmaxfree");
            String A023 = AnonymousClass00I.A02("dalvik.vm.heapminfree");
            AnonymousClass08Z.A04(64, "process_labels", String.format("device=%s,heapgrowthlimit=%s,heapstartsize=%s,heapminfree=%s,heapmaxfree=%s,heaptargetutilization=%s", Build.MODEL, A02, AnonymousClass00I.A02("dalvik.vm.heapstartsize"), A022, A023, AnonymousClass00I.A02("dalvik.vm.heaptargetutilization")), 0);
        }
        boolean z = false;
        if (0 != 0) {
            z = true;
        }
        if (z && C001000r.A02(64)) {
            AnonymousClass00C.A01(64, "TraceExistingThreadsMetadata", 1638420212);
            WeakHashMap weakHashMap = null;
            try {
                for (Map.Entry entry : weakHashMap.entrySet()) {
                    AnonymousClass08Z.A04(64, "thread_name", ((Thread) entry.getKey()).getName(), ((Integer) entry.getValue()).intValue());
                }
            } finally {
                AnonymousClass00C.A00(64, 1299111541);
            }
        }
    }

    public void Bsf() {
    }
}
