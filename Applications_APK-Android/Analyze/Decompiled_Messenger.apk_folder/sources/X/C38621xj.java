package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.1xj  reason: invalid class name and case insensitive filesystem */
public final class C38621xj implements Iterator {
    private int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ Iterator A02;

    public C38621xj(int i, Iterator it) {
        this.A01 = i;
        this.A02 = it;
    }

    public boolean hasNext() {
        if (this.A00 >= this.A01 || !this.A02.hasNext()) {
            return false;
        }
        return true;
    }

    public void remove() {
        this.A02.remove();
    }

    public Object next() {
        if (hasNext()) {
            this.A00++;
            return this.A02.next();
        }
        throw new NoSuchElementException();
    }
}
