package X;

import androidx.fragment.app.Fragment;
import java.util.HashSet;

/* renamed from: X.1di  reason: invalid class name and case insensitive filesystem */
public final class C27761di implements C27771dj {
    public final /* synthetic */ C13060qW A00;

    public void BSw(Fragment fragment, C29702EgP egP) {
        boolean z;
        int i;
        synchronized (egP) {
            z = egP.A02;
        }
        if (!z) {
            C13060qW r2 = this.A00;
            HashSet hashSet = (HashSet) r2.A0C.get(fragment);
            if (hashSet != null && hashSet.remove(egP) && hashSet.isEmpty()) {
                r2.A0C.remove(fragment);
                if (fragment.A0F < 3) {
                    C13060qW.A0B(r2, fragment);
                    AnonymousClass2NQ r0 = fragment.A0K;
                    if (r0 == null) {
                        i = 0;
                    } else {
                        i = r0.A03;
                    }
                    r2.A0s(fragment, i);
                }
            }
        }
    }

    public C27761di(C13060qW r1) {
        this.A00 = r1;
    }

    public void BpR(Fragment fragment, C29702EgP egP) {
        C13060qW r2 = this.A00;
        if (r2.A0C.get(fragment) == null) {
            r2.A0C.put(fragment, new HashSet());
        }
        ((HashSet) r2.A0C.get(fragment)).add(egP);
    }
}
