package X;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;

/* renamed from: X.0cL  reason: invalid class name and case insensitive filesystem */
public abstract class C06940cL {
    public final Supplier A00 = Suppliers.memoize(new C06700bw(this));
    public final Supplier A01 = Suppliers.memoize(new C06710bx(this));
    public final Supplier A02 = Suppliers.memoize(new C06970cO(this));

    public ImmutableSet A01() {
        return RegularImmutableSet.A05;
    }

    public ImmutableSet A02() {
        C06820c8 r9 = (C06820c8) this;
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B50, r9.A00)).AbO(5, true)) {
            return RegularImmutableSet.A05;
        }
        C07410dQ A012 = ImmutableSet.A01();
        A012.A00(C26271bB.A01);
        ImmutableMap.Builder builder = ImmutableMap.builder();
        builder.put("fb", Integer.valueOf((int) AnonymousClass1Y3.A5P));
        builder.put("ro", 672);
        ImmutableMap build = builder.build();
        ImmutableMap.Builder builder2 = ImmutableMap.builder();
        builder2.put("fb", Integer.valueOf((int) AnonymousClass1Y3.A0k));
        builder2.put("ro", Integer.valueOf((int) AnonymousClass1Y3.A0l));
        ImmutableMap build2 = builder2.build();
        C24971Xv it = C26271bB.A00.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            Integer num = null;
            Integer num2 = build2 == null ? null : (Integer) build2.get(str);
            if (build != null) {
                num = (Integer) build.get(str);
            }
            if (num2 == null || num == null) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Amr, r9.A00)).CGY("AndroidMessengerSupportedLanguages", AnonymousClass08S.A0J("Missing GK for locale ", str));
            } else if (((AnonymousClass1YI) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B50, r9.A00)).AbO(num2.intValue(), false) || ((AnonymousClass1YI) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AcD, r9.A00)).AbO(num.intValue(), false)) {
                A012.A01(str);
            }
        }
        return A012.build();
    }

    public ImmutableSet A03() {
        C07410dQ A012 = ImmutableSet.A01();
        A012.A00(((C06820c8) this).A02());
        A012.A01("en");
        return A012.build();
    }
}
