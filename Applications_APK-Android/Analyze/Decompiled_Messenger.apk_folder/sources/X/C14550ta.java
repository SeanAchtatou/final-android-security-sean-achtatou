package X;

import com.facebook.common.json.FbJsonDeserializer;
import com.facebook.common.json.ImmutableListDeserializer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Collection;

/* renamed from: X.0ta  reason: invalid class name and case insensitive filesystem */
public final class C14550ta {
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0028, code lost:
        if (((X.C48762b0) r7).mHumanReadableFormatEnabled == false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A03(X.C11710np r6, X.C11260mU r7, java.lang.Object r8) {
        /*
            if (r8 == 0) goto L_0x0036
            java.lang.Class r1 = r8.getClass()
            java.lang.Class<X.1fn> r0 = X.C29051fn.class
            boolean r0 = r0.isAssignableFrom(r1)
            r3 = 2
            if (r0 == 0) goto L_0x0072
            boolean r0 = r8 instanceof com.facebook.graphservice.interfaces.Tree
            com.google.common.base.Preconditions.checkArgument(r0)
            r4 = r8
            com.facebook.graphservice.interfaces.Tree r4 = (com.facebook.graphservice.interfaces.Tree) r4
            boolean r2 = r4.isValid()
            int r5 = r4.getTypeTag()
            boolean r0 = r7 instanceof X.C48762b0
            if (r0 == 0) goto L_0x002a
            X.2b0 r7 = (X.C48762b0) r7
            boolean r1 = r7.mHumanReadableFormatEnabled
            r0 = 1
            if (r1 != 0) goto L_0x002b
        L_0x002a:
            r0 = 0
        L_0x002b:
            if (r0 == 0) goto L_0x0037
            if (r2 == 0) goto L_0x0037
            java.lang.String r0 = r4.toExpensiveHumanReadableDebugString()
            r6.writeRawValue(r0)
        L_0x0036:
            return
        L_0x0037:
            if (r2 == 0) goto L_0x0062
            com.facebook.graphservice.interfaces.TreeSerializer r0 = X.C05850aR.A03()
            java.nio.ByteBuffer r1 = r0.serializeTreeToByteBuffer(r4)
            int r0 = r1.limit()
            byte[] r4 = new byte[r0]
            r1.get(r4)
            java.lang.String r2 = "tree:"
        L_0x004c:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r5)
            java.lang.String r0 = "type_tag:%08x;"
            java.lang.String r1 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1)
            java.lang.String r0 = android.util.Base64.encodeToString(r4, r3)
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r6.writeString(r0)
            return
        L_0x0062:
            java.lang.String r1 = "AutoGenJsonHelper"
            java.lang.String r0 = "Use of deprecated flatbuffer infra"
            X.C010708t.A0K(r1, r0)
            com.facebook.flatbuffers.Flattenable r8 = (com.facebook.flatbuffers.Flattenable) r8
            byte[] r4 = X.C21647Aek.A06(r8)
            java.lang.String r2 = "fltb:"
            goto L_0x004c
        L_0x0072:
            java.lang.Class<X.0oo> r0 = X.C12200oo.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x009d
            java.lang.Class<X.3b1> r0 = X.C70653b1.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 != 0) goto L_0x009d
            com.facebook.graphservice.interfaces.TreeSerializer r0 = X.C05850aR.A03()
            com.facebook.graphservice.interfaces.Tree r8 = (com.facebook.graphservice.interfaces.Tree) r8
            java.nio.ByteBuffer r1 = r0.serializeTreeToByteBuffer(r8)
            int r0 = r1.limit()
            byte[] r0 = new byte[r0]
            r1.get(r0)
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r3)
            r6.writeString(r0)
            return
        L_0x009d:
            java.lang.Class<X.0jS> r0 = X.C10040jS.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x00ab
            X.0jS r8 = (X.C10040jS) r8
            r8.serialize(r6, r7)
            return
        L_0x00ab:
            boolean r0 = r1.isEnum()
            if (r0 == 0) goto L_0x00bb
            java.lang.Enum r8 = (java.lang.Enum) r8
            java.lang.String r0 = r8.name()
            r6.writeString(r0)
            return
        L_0x00bb:
            java.lang.Class<java.util.Collection> r0 = java.util.Collection.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x00c9
            java.util.Collection r8 = (java.util.Collection) r8
            A06(r6, r7, r8)
            return
        L_0x00c9:
            r6.writeObject(r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14550ta.A03(X.0np, X.0mU, java.lang.Object):void");
    }

    public static void A04(C11710np r0, C11260mU r1, String str, Object obj) {
        if (obj != null) {
            r0.writeFieldName(str);
            A03(r0, r1, obj);
        }
    }

    public static void A05(C11710np r0, C11260mU r1, String str, Collection collection) {
        if (collection != null) {
            r0.writeFieldName(str);
            A06(r0, r1, collection);
        }
    }

    public static void A06(C11710np r2, C11260mU r3, Collection collection) {
        if (collection != null) {
            r2.writeStartArray();
            for (Object A03 : collection) {
                A03(r2, r3, A03);
            }
            r2.writeEndArray();
        }
    }

    public static void A09(C11710np r1, String str, Boolean bool) {
        if (bool != null) {
            r1.writeFieldName(str);
            r1.writeBoolean(bool.booleanValue());
        }
    }

    public static void A0A(C11710np r1, String str, Integer num) {
        if (num != null) {
            r1.writeFieldName(str);
            r1.writeNumber(num.intValue());
        }
    }

    public static void A0B(C11710np r0, String str, String str2) {
        if (str2 != null) {
            r0.writeFieldName(str);
            r0.writeString(str2);
        }
    }

    public static ImmutableList A00(C28271eX r2, C26791c3 r3, Class cls, C28231eT r5) {
        ImmutableListDeserializer immutableListDeserializer;
        if (r2.getCurrentToken() == C182811d.VALUE_NULL) {
            return RegularImmutableList.A02;
        }
        if (cls != null) {
            immutableListDeserializer = new ImmutableListDeserializer(cls);
        } else if (r5 != null) {
            immutableListDeserializer = new ImmutableListDeserializer(((AnonymousClass0jE) r2.getCodec()).findDeserializer(r3, r5._type));
        } else {
            throw new IllegalArgumentException("Need to set simple or generic inner list type!");
        }
        return (ImmutableList) immutableListDeserializer.deserialize(r2, r3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object A01(java.lang.reflect.Type r6, X.C28271eX r7, X.C26791c3 r8) {
        /*
            X.11d r0 = r7.getCurrentToken()
            X.11d r4 = X.C182811d.VALUE_NULL
            if (r0 != r4) goto L_0x000d
            r7.skipChildren()
            r0 = 0
            return r0
        L_0x000d:
            boolean r0 = r6 instanceof java.lang.reflect.ParameterizedType
            if (r0 == 0) goto L_0x0047
            r0 = r6
            java.lang.reflect.ParameterizedType r0 = (java.lang.reflect.ParameterizedType) r0
            java.lang.reflect.Type r5 = r0.getRawType()
            java.lang.Class r5 = (java.lang.Class) r5
            java.lang.reflect.Type[] r3 = r0.getActualTypeArguments()
            java.lang.Class<com.google.common.collect.ImmutableList> r0 = com.google.common.collect.ImmutableList.class
            boolean r0 = r0.isAssignableFrom(r5)
            if (r0 == 0) goto L_0x0047
            int r2 = r3.length
            r1 = 0
            r0 = 1
            if (r2 == r0) goto L_0x002c
            r0 = 0
        L_0x002c:
            com.google.common.base.Preconditions.checkState(r0)
            X.0js r2 = X.C10300js.instance
            r1 = r3[r1]
            r0 = 0
            X.0jR r0 = r2._constructType(r1, r0)
            X.1ed r1 = X.C28331ed.construct(r5, r0)
        L_0x003c:
            X.11d r0 = r7.getCurrentToken()
            if (r0 != r4) goto L_0x004f
            r7.skipChildren()
            r0 = 0
            return r0
        L_0x0047:
            X.0js r1 = X.C10300js.instance
            r0 = 0
            X.0jR r1 = r1._constructType(r6, r0)
            goto L_0x003c
        L_0x004f:
            X.0jK r0 = r7.getCodec()
            X.0jE r0 = (X.AnonymousClass0jE) r0
            com.fasterxml.jackson.databind.JsonDeserializer r0 = r0.findDeserializer(r8, r1)
            java.lang.Object r0 = r0.deserialize(r7, r8)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14550ta.A01(java.lang.reflect.Type, X.1eX, X.1c3):java.lang.Object");
    }

    public static String A02(C28271eX r3) {
        if (r3.getCurrentToken() == C182811d.VALUE_NULL) {
            r3.skipChildren();
            return null;
        }
        String text = r3.getText();
        if (text != null) {
            return text;
        }
        throw new C37561vs("Failed to read text from Json stream", r3.getCurrentLocation());
    }

    public static void A07(C11710np r0, String str, float f) {
        r0.writeFieldName(str);
        r0.writeNumber(f);
    }

    public static void A08(C11710np r0, String str, int i) {
        r0.writeFieldName(str);
        r0.writeNumber(i);
    }

    public static void A0C(C11710np r0, String str, boolean z) {
        r0.writeFieldName(str);
        r0.writeBoolean(z);
    }

    public static void A0D(Class cls, C28271eX r6, Exception exc) {
        String str;
        try {
            str = FbJsonDeserializer.getJsonParserText(r6);
        } catch (Exception unused) {
            str = "failed to get parser text";
        }
        throw new C37561vs(AnonymousClass08S.A0S("Failed to deserialize to instance ", cls.getSimpleName(), "\n", str), r6.getCurrentLocation(), exc);
    }
}
