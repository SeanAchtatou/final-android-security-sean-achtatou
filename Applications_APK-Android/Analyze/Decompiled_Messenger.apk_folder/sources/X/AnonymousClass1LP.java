package X;

/* renamed from: X.1LP  reason: invalid class name */
public abstract class AnonymousClass1LP extends AnonymousClass1LQ {
    public int A04(int[] iArr, int i, int i2, int i3) {
        if (i2 - i < 22) {
            while (i < i2) {
                if (i3 == iArr[i]) {
                    return iArr[i + 1];
                }
                i += 2;
            }
            return -2;
        }
        int i4 = i2 - 1;
        while (i <= i4) {
            int i5 = ((i + i4) >>> 2) << 1;
            int i6 = iArr[i5];
            if (i6 < i3) {
                i = i5 + 2;
            } else if (i6 <= i3) {
                return iArr[i5 + 1];
            } else {
                i4 = i5 - 2;
            }
        }
        return -2;
    }

    public AnonymousClass1LP(int[] iArr, int i, int i2) {
        super(iArr, i, i2);
    }
}
