package X;

import com.facebook.omnistore.OmnistoreIOException;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import com.facebook.omnistore.util.DeviceIdUtil;

/* renamed from: X.1Tf  reason: invalid class name and case insensitive filesystem */
public final class C24341Tf {
    public final C29271g9 A00;
    private final C001300x A01;
    private final C04310Tq A02;

    public static final C24341Tf A00(AnonymousClass1XY r1) {
        return new C24341Tf(r1);
    }

    public static final C24341Tf A01(AnonymousClass1XY r1) {
        return new C24341Tf(r1);
    }

    public void A02(OmnistoreComponent omnistoreComponent) {
        A03(omnistoreComponent, new StringBuilder());
    }

    public void A03(OmnistoreComponent omnistoreComponent, StringBuilder sb) {
        if (!DeviceIdUtil.isSupportedApp(this.A01.A04)) {
            sb.append("Attempted to open an Omnistore Component on unsupported app: ");
            sb.append(omnistoreComponent.getClass().toString());
            C010708t.A0P(C08520fU.A0K, "Attempted to open an Omnistore Component on unsupported app: %s", omnistoreComponent);
            return;
        }
        AnonymousClass1Uz A012 = ((C29431gP) this.A02.get()).A01(omnistoreComponent);
        synchronized (this.A00) {
            try {
                A012.Bgx(this.A00);
            } catch (AnonymousClass3LY | AnonymousClass3LZ e) {
                sb.append("Failed to open omnistore while trying to get collection for component: ");
                sb.append(omnistoreComponent.getClass().toString());
                sb.append(e.getStackTrace());
                C010708t.A0V(C08520fU.A0K, e, "Failed to open omnistore while trying to get collection for component: %s", omnistoreComponent);
            } catch (OmnistoreIOException e2) {
                sb.append("Omnistore IO Error while opening collection ");
                sb.append(omnistoreComponent.getClass().toString());
                sb.append(e2.getStackTrace());
                C010708t.A0U(C08520fU.A0K, e2, "Omnistore IO Error while opening collection %s", omnistoreComponent);
            }
        }
    }

    public void A04(OmnistoreStoredProcedureComponent omnistoreStoredProcedureComponent) {
        if (!DeviceIdUtil.isSupportedApp(this.A01.A04)) {
            C010708t.A0P(C08520fU.A0K, "Attempted to open an Omnistore Stored Procedure Component on unsupported app: %s", omnistoreStoredProcedureComponent);
            return;
        }
        C36481tC A022 = ((C29431gP) this.A02.get()).A02(omnistoreStoredProcedureComponent);
        synchronized (this.A00) {
            try {
                A022.Bgx(this.A00);
            } catch (AnonymousClass3LY | AnonymousClass3LZ e) {
                C010708t.A0V(C08520fU.A0K, e, "Failed to open omnistore while trying to initialize stored procedure component: %s", omnistoreStoredProcedureComponent);
            } catch (OmnistoreIOException e2) {
                C010708t.A0U(C08520fU.A0K, e2, "Omnistore IO Error while initializing stored procedure component %s", omnistoreStoredProcedureComponent);
            }
        }
    }

    private C24341Tf(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass0VB.A00(AnonymousClass1Y3.AVV, r2);
        this.A00 = C29271g9.A01(r2);
        this.A01 = AnonymousClass0UU.A02(r2);
    }
}
