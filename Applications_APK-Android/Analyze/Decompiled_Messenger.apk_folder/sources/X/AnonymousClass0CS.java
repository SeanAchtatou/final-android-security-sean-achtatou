package X;

/* renamed from: X.0CS  reason: invalid class name */
public final class AnonymousClass0CS implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$CallbackHandler$2";
    public final /* synthetic */ AnonymousClass0CC A00;
    public final /* synthetic */ AnonymousClass0CR A01;

    public AnonymousClass0CS(AnonymousClass0CC r1, AnonymousClass0CR r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0172  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r17 = this;
            r2 = r17
            X.0CC r1 = r2.A00
            boolean r0 = r1.A01
            if (r0 != 0) goto L_0x000b
            X.AnonymousClass0CC.A00(r1)
        L_0x000b:
            X.0CC r0 = r2.A00
            X.0AH r0 = r0.A02
            X.0C8 r3 = r0.A0n
            X.0CC r1 = r2.A00
            X.0C8 r0 = r1.A00
            if (r3 != r0) goto L_0x0193
            X.0CR r0 = r2.A01
            X.0Aq r4 = r0.A02
            boolean r0 = r4.A02()
            r3 = 0
            if (r0 == 0) goto L_0x0142
            X.0AH r0 = r1.A02
            X.0AQ r1 = r0.A0E
            java.lang.Object r0 = r4.A01()
            X.0Bu r0 = (X.C01830Bu) r0
            boolean r5 = r1.CKA(r0)
            r5 = r5 | r3
        L_0x0031:
            X.0CC r0 = r2.A00
            X.0AH r1 = r0.A02
            java.lang.String r0 = r1.A0Q
            boolean r0 = X.AnonymousClass0A9.A01(r0)
            if (r0 != 0) goto L_0x0054
            X.0CR r0 = r2.A01
            X.0Aq r4 = r0.A03
            boolean r0 = r4.A02()
            if (r0 == 0) goto L_0x0054
            X.0AU r1 = r1.A0K
            java.lang.Object r0 = r4.A01()
            X.0C5 r0 = (X.AnonymousClass0C5) r0
            boolean r0 = r1.CKB(r0)
            r5 = r5 | r0
        L_0x0054:
            if (r5 == 0) goto L_0x005f
            X.0CC r0 = r2.A00
            X.0AH r0 = r0.A02
            X.0AG r0 = r0.A0I
            r0.BV6()
        L_0x005f:
            X.0CC r1 = r2.A00
            boolean r0 = r1.A01
            r8 = 0
            if (r0 != 0) goto L_0x006b
            X.0AH r0 = r1.A02
            r0.A0O(r8, r8)
        L_0x006b:
            X.0CC r0 = r2.A00
            X.0AH r0 = r0.A02
            r0.A0E()
            X.0CC r0 = r2.A00
            X.0AH r0 = r0.A02
            X.0B7 r6 = r0.A0A
            long r4 = android.os.SystemClock.elapsedRealtime()
            java.util.concurrent.atomic.AtomicLong r0 = r0.A0i
            long r0 = r0.get()
            long r4 = r4 - r0
            java.lang.Class<X.0Bw> r0 = X.C01850Bw.class
            X.0By r1 = r6.A07(r0)
            X.0Bw r1 = (X.C01850Bw) r1
            X.0C3 r0 = X.AnonymousClass0C3.A03
            java.lang.Object r0 = r1.A00(r0)
            java.util.concurrent.atomic.AtomicLong r0 = (java.util.concurrent.atomic.AtomicLong) r0
            r0.incrementAndGet()
            X.0C3 r0 = X.AnonymousClass0C3.A01
            java.lang.Object r0 = r1.A00(r0)
            java.util.concurrent.atomic.AtomicLong r0 = (java.util.concurrent.atomic.AtomicLong) r0
            r0.set(r4)
            X.0B9 r0 = r6.A00
            java.util.concurrent.atomic.AtomicLong r4 = r0.A01
            long r0 = android.os.SystemClock.elapsedRealtime()
            r4.set(r0)
            X.0CC r0 = r2.A00
            X.0AH r4 = r0.A02
            boolean r0 = r4.A0Y
            if (r0 == 0) goto L_0x00e7
            X.0B7 r6 = r4.A0A
            X.0Sd r1 = new X.0Sd
            X.0Mo r7 = X.AnonymousClass0B7.A00(r6)
            java.lang.Class<X.0Bw> r0 = X.C01850Bw.class
            X.0By r9 = r6.A07(r0)
            X.0Bw r9 = (X.C01850Bw) r9
            X.0B9 r5 = r6.A00
            r0 = 1
            X.0Sb r11 = r5.A00(r0)
            java.lang.Class<X.0Bz> r0 = X.C01880Bz.class
            X.0By r12 = r6.A07(r0)
            X.0Bz r12 = (X.C01880Bz) r12
            java.lang.Class<X.0C1> r0 = X.AnonymousClass0C1.class
            X.0By r13 = r6.A07(r0)
            X.0C1 r13 = (X.AnonymousClass0C1) r13
            r6 = r1
            r14 = 0
            r16 = 0
            r10 = r8
            r15 = 0
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)
            X.AnonymousClass0AH.A04(r4, r1, r3)
        L_0x00e7:
            X.0CC r0 = r2.A00
            X.0AH r0 = r0.A02
            X.0B7 r1 = r0.A0A
            java.lang.Class<X.0Bw> r0 = X.C01850Bw.class
            X.0By r1 = r1.A07(r0)
            X.0Bw r1 = (X.C01850Bw) r1
            X.0C3 r0 = X.AnonymousClass0C3.A04
            r1.A02(r0, r8)
            X.0CC r1 = r2.A00
            boolean r0 = r1.A01
            if (r0 != 0) goto L_0x0107
            X.0AH r0 = r1.A02
            X.0AG r0 = r0.A0I
            r0.BTc()
        L_0x0107:
            X.0CC r0 = r2.A00
            X.0AH r5 = r0.A02
            long r7 = android.os.SystemClock.elapsedRealtime()
            X.0BL r4 = r5.A0P
            long r0 = r4.A01
            long r7 = r7 - r0
            X.0B6 r6 = r5.A09
            int r0 = r4.A00
            java.lang.String r5 = "retry_count"
            java.lang.String r4 = java.lang.String.valueOf(r0)
            java.lang.String r1 = "retry_duration_ms"
            java.lang.String r0 = java.lang.String.valueOf(r7)
            java.lang.String[] r0 = new java.lang.String[]{r5, r4, r1, r0}
            java.util.Map r4 = X.C01740Bl.A00(r0)
            java.lang.String r1 = "mqtt_connection_retries"
            r6.A07(r1, r4)
            X.07g r0 = r6.A00
            if (r0 == 0) goto L_0x0138
            r0.BIq(r1, r4)
        L_0x0138:
            X.0CC r0 = r2.A00
            X.0AH r8 = r0.A02
            r8.A0Z = r3
            X.0C8 r7 = r0.A00
            monitor-enter(r7)
            goto L_0x0145
        L_0x0142:
            r5 = 0
            goto L_0x0031
        L_0x0145:
            java.util.List r6 = r7.A00     // Catch:{ all -> 0x0177 }
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A0H     // Catch:{ all -> 0x0177 }
            int r1 = r0.incrementAndGet()     // Catch:{ all -> 0x0177 }
            r0 = 1
            if (r1 <= r0) goto L_0x0162
            X.0CC r5 = r7.A0Y     // Catch:{ all -> 0x0177 }
            java.lang.String r4 = "Mqtt Unknown Exception"
            java.lang.String r1 = "getAndResetConnectMessage being called twice"
            java.lang.Throwable r0 = new java.lang.Throwable     // Catch:{ all -> 0x0177 }
            r0.<init>()     // Catch:{ all -> 0x0177 }
        L_0x015b:
            r5.A02(r4, r1, r0)     // Catch:{ all -> 0x0177 }
        L_0x015e:
            r0 = 0
            r7.A00 = r0     // Catch:{ all -> 0x0177 }
            goto L_0x0170
        L_0x0162:
            if (r6 != 0) goto L_0x015e
            X.0CC r5 = r7.A0Y     // Catch:{ all -> 0x0177 }
            java.lang.String r4 = "Mqtt Unknown Exception"
            java.lang.String r1 = "connectMessage is null"
            java.lang.Throwable r0 = new java.lang.Throwable     // Catch:{ all -> 0x0177 }
            r0.<init>()     // Catch:{ all -> 0x0177 }
            goto L_0x015b
        L_0x0170:
            if (r6 != 0) goto L_0x017a
            java.util.List r6 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0177 }
            goto L_0x017a
        L_0x0177:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x017a:
            monitor-exit(r7)
            r8.A0a(r6)
            X.0CC r0 = r2.A00
            X.0AH r1 = r0.A02
            java.util.List r0 = java.util.Collections.EMPTY_LIST
            r1.A0Z(r0)
            X.0CC r0 = r2.A00
            X.0AH r2 = r0.A02
            long r0 = android.os.SystemClock.elapsedRealtime()
            r2.A0e = r0
            r2.A0V = r3
        L_0x0193:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0CS.run():void");
    }
}
