package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.model.UserFbidIdentifier;

/* renamed from: X.1aG  reason: invalid class name and case insensitive filesystem */
public final class C25701aG implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new UserFbidIdentifier(parcel);
    }

    public Object[] newArray(int i) {
        return new UserFbidIdentifier[i];
    }
}
