package X;

import java.util.Set;

/* renamed from: X.1zI  reason: invalid class name and case insensitive filesystem */
public final class C39591zI implements C53182kL {
    public final /* synthetic */ C204459kV A00;

    public C39591zI(C204459kV r1) {
        this.A00 = r1;
    }

    public void BYd(Exception exc) {
        Set<C203539io> set = this.A00.A05;
        if (set != null) {
            for (C203539io r5 : set) {
                AnonymousClass26K A002 = AnonymousClass26K.A00();
                if (exc != null) {
                    A002.A04("retriever_setup_failure", exc.getMessage());
                }
                C185414b r2 = r5.A00;
                C08900gA r1 = C08870g7.A2b;
                r2.AOO(r1, "retriever_setup_failure", null, A002);
                r5.A00.AYS(r1);
            }
        }
    }
}
