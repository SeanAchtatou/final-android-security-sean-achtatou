package X;

/* renamed from: X.063  reason: invalid class name */
public abstract class AnonymousClass063 implements Comparable {
    public int A00;
    public String A01;
    public final AnonymousClass063 A02;
    public final String A03;

    public abstract AnonymousClass063 A04(AnonymousClass063 r1, String str);

    public String A05() {
        String str;
        if (this.A01 == null) {
            AnonymousClass063 r2 = this.A02;
            String str2 = this.A03;
            if (r2 == null || r2.A05() == null) {
                str = str2;
            } else {
                str = r2.A05();
                if (str2 != null) {
                    str = str.concat(str2);
                }
            }
            this.A01 = str;
        }
        return this.A01;
    }

    public boolean A07(AnonymousClass063 r3) {
        String str;
        String str2 = this.A01;
        if (str2 != null && (str = r3.A01) != null) {
            return str2.startsWith(str);
        }
        AnonymousClass063 r0 = this.A02;
        if ((r0 == null || !r0.A07(r3)) && !A05().startsWith(r3.A05())) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        if (r4.A03 == null) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A08(X.AnonymousClass063 r4) {
        /*
            r3 = this;
            java.lang.String r0 = r3.A01
            r2 = 1
            if (r0 != 0) goto L_0x002c
            java.lang.String r0 = r4.A01
            if (r0 != 0) goto L_0x002c
            X.063 r1 = r3.A02
            if (r1 != 0) goto L_0x0023
            X.063 r0 = r4.A02
            if (r0 != 0) goto L_0x002c
        L_0x0011:
            java.lang.String r1 = r3.A03
            if (r1 != 0) goto L_0x001a
            java.lang.String r0 = r4.A03
            if (r0 != 0) goto L_0x002c
        L_0x0019:
            return r2
        L_0x001a:
            java.lang.String r0 = r4.A03
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x002c
            return r2
        L_0x0023:
            X.063 r0 = r4.A02
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x002c
            goto L_0x0011
        L_0x002c:
            java.lang.String r0 = r3.A05()
            if (r0 != 0) goto L_0x003a
            java.lang.String r0 = r4.A05()
            if (r0 == 0) goto L_0x0019
            r2 = 0
            return r2
        L_0x003a:
            java.lang.String r1 = r3.A05()
            java.lang.String r0 = r4.A05()
            boolean r2 = r1.equals(r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass063.A08(X.063):boolean");
    }

    public int compareTo(Object obj) {
        return A05().compareTo(((AnonymousClass063) obj).A05());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return A08((AnonymousClass063) obj);
    }

    public int hashCode() {
        int i = this.A00;
        if (i == 0) {
            String str = this.A01;
            if (str == null) {
                AnonymousClass063 r0 = this.A02;
                if (r0 != null) {
                    i = r0.hashCode();
                } else {
                    i = 0;
                }
                str = this.A03;
            }
            if (str != null) {
                int length = str.length();
                for (int i2 = 0; i2 < length; i2++) {
                    i = (i * 31) + str.charAt(i2);
                }
            }
            this.A00 = i;
        }
        return i;
    }

    public String A06(AnonymousClass063 r3) {
        AnonymousClass064.A03(A05().startsWith(r3.A05()));
        return A05().substring(r3.A05().length());
    }

    public AnonymousClass063 A09(String str) {
        return A04(this, str);
    }

    public String toString() {
        return A05();
    }

    public AnonymousClass063(AnonymousClass063 r3, String str) {
        boolean z = true;
        AnonymousClass064.A03(r3 != null);
        AnonymousClass064.A03(str == null ? false : z);
        this.A02 = r3;
        this.A03 = str;
    }

    public AnonymousClass063(String str) {
        AnonymousClass064.A03(str != null);
        this.A02 = null;
        this.A03 = str;
        this.A01 = str;
    }
}
