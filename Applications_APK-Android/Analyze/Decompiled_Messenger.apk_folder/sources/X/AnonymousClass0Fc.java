package X;

/* renamed from: X.0Fc  reason: invalid class name */
public final class AnonymousClass0Fc extends AnonymousClass0FM {
    public long blkIoTicks;
    public long cancelledWriteBytes;
    public long majorFaults;
    public long rcharBytes;
    public long readBytes;
    public long syscrCount;
    public long syscwCount;
    public long wcharBytes;
    public long writeBytes;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass0Fc r7 = (AnonymousClass0Fc) obj;
            if (!(r7.rcharBytes == this.rcharBytes && r7.wcharBytes == this.wcharBytes && r7.syscrCount == this.syscrCount && r7.syscwCount == this.syscwCount && r7.readBytes == this.readBytes && r7.writeBytes == this.writeBytes && r7.cancelledWriteBytes == this.cancelledWriteBytes && r7.majorFaults == this.majorFaults && r7.blkIoTicks == this.blkIoTicks)) {
                return false;
            }
        }
        return true;
    }

    private void A00(AnonymousClass0Fc r3) {
        this.rcharBytes = r3.rcharBytes;
        this.wcharBytes = r3.wcharBytes;
        this.syscrCount = r3.syscrCount;
        this.syscwCount = r3.syscwCount;
        this.readBytes = r3.readBytes;
        this.writeBytes = r3.writeBytes;
        this.cancelledWriteBytes = r3.cancelledWriteBytes;
        this.majorFaults = r3.majorFaults;
        this.blkIoTicks = r3.blkIoTicks;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A00((AnonymousClass0Fc) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        AnonymousClass0Fc r52 = (AnonymousClass0Fc) r5;
        AnonymousClass0Fc r62 = (AnonymousClass0Fc) r6;
        if (r62 == null) {
            r62 = new AnonymousClass0Fc();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.rcharBytes = this.rcharBytes - r52.rcharBytes;
        r62.wcharBytes = this.wcharBytes - r52.wcharBytes;
        r62.syscrCount = this.syscrCount - r52.syscrCount;
        r62.syscwCount = this.syscwCount - r52.syscwCount;
        r62.readBytes = this.readBytes - r52.readBytes;
        r62.writeBytes = this.writeBytes - r52.writeBytes;
        r62.cancelledWriteBytes = this.cancelledWriteBytes - r52.cancelledWriteBytes;
        r62.majorFaults = this.majorFaults - r52.majorFaults;
        r62.blkIoTicks = this.blkIoTicks - r52.blkIoTicks;
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        AnonymousClass0Fc r52 = (AnonymousClass0Fc) r5;
        AnonymousClass0Fc r62 = (AnonymousClass0Fc) r6;
        if (r62 == null) {
            r62 = new AnonymousClass0Fc();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.rcharBytes = this.rcharBytes + r52.rcharBytes;
        r62.wcharBytes = this.wcharBytes + r52.wcharBytes;
        r62.syscrCount = this.syscrCount + r52.syscrCount;
        r62.syscwCount = this.syscwCount + r52.syscwCount;
        r62.readBytes = this.readBytes + r52.readBytes;
        r62.writeBytes = this.writeBytes + r52.writeBytes;
        r62.cancelledWriteBytes = this.cancelledWriteBytes + r52.cancelledWriteBytes;
        r62.majorFaults = this.majorFaults + r52.majorFaults;
        r62.blkIoTicks = this.blkIoTicks + r52.blkIoTicks;
        return r62;
    }

    public int hashCode() {
        long j = this.rcharBytes;
        long j2 = this.wcharBytes;
        long j3 = this.syscrCount;
        long j4 = this.syscwCount;
        long j5 = this.readBytes;
        long j6 = this.writeBytes;
        long j7 = this.cancelledWriteBytes;
        long j8 = this.majorFaults;
        long j9 = this.blkIoTicks;
        return (((((((((((((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) (j8 ^ (j8 >>> 32)))) * 31) + ((int) (j9 ^ (j9 >>> 32)));
    }

    public String toString() {
        return "DiskMetrics{rcharBytes=" + this.rcharBytes + ", wcharBytes=" + this.wcharBytes + ", syscrCount=" + this.syscrCount + ", syscwCount=" + this.syscwCount + ", readBytes=" + this.readBytes + ", writeBytes=" + this.writeBytes + ", cancelledWriteBytes=" + this.cancelledWriteBytes + ", majorFaults=" + this.majorFaults + ", blkIoTicks=" + this.blkIoTicks + "}";
    }
}
