package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0bB  reason: invalid class name and case insensitive filesystem */
public final class C06240bB {
    private static volatile C06240bB A0C;
    public AnonymousClass0UN A00;
    public AnonymousClass99G A01;
    public Long A02 = null;
    private AnonymousClass2SP A03;
    private Boolean A04 = null;
    private Boolean A05 = null;
    private Boolean A06 = null;
    public final C05770aI A07;
    public final Object A08 = new Object();
    private final Object A09 = new Object();
    private final Object A0A = new Object();
    private final C04310Tq A0B;

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        if (A09() == false) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0038, code lost:
        monitor-enter(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r0 = ((X.C25921ac) X.AnonymousClass1XX.A02(2, X.AnonymousClass1Y3.BEf, r8.A00)).B6J();
        r6 = r0.A01;
        r1 = r0.A00;
        r3 = ((android.content.Context) X.AnonymousClass1XX.A02(4, X.AnonymousClass1Y3.BCt, r8.A00)).getPackageName();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0065, code lost:
        if (X.EYZ.A00().containsKey(r3) == false) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0067, code lost:
        r3 = (java.lang.String) X.EYZ.A00().get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0071, code lost:
        A06(new X.AnonymousClass2SP(r6, r1, r3));
        r0 = X.AnonymousClass1Y3.B9S;
        r1 = r8.A00;
        ((X.AnonymousClass99C) X.AnonymousClass1XX.A02(1, r0, r1)).Btc(null, r8.A03, ((android.content.Context) X.AnonymousClass1XX.A02(4, X.AnonymousClass1Y3.BCt, r1)).getPackageName(), X.AnonymousClass07B.A00);
        r0 = r8.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0097, code lost:
        monitor-exit(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0098, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0099, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x009c, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009f, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass2SP A02() {
        /*
            r8 = this;
            monitor-enter(r8)
            X.2SP r0 = r8.A03     // Catch:{ all -> 0x009d }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r8)     // Catch:{ all -> 0x009d }
            return r0
        L_0x0007:
            X.0aI r1 = r8.A07     // Catch:{ all -> 0x009d }
            java.lang.String r0 = "phone_id"
            r4 = 0
            java.lang.String r7 = r1.A07(r0, r4)     // Catch:{ all -> 0x009d }
            X.0aI r1 = r8.A07     // Catch:{ all -> 0x009d }
            java.lang.String r0 = "phone_id_ts"
            r5 = 0
            long r1 = r1.A05(r0, r5)     // Catch:{ all -> 0x009d }
            X.0aI r3 = r8.A07     // Catch:{ all -> 0x009d }
            java.lang.String r0 = "origin"
            java.lang.String r3 = r3.A07(r0, r4)     // Catch:{ all -> 0x009d }
            if (r7 == 0) goto L_0x0031
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0031
            X.2SP r4 = new X.2SP     // Catch:{ all -> 0x009d }
            r4.<init>(r7, r1, r3)     // Catch:{ all -> 0x009d }
            r8.A03 = r4     // Catch:{ all -> 0x009d }
            monitor-exit(r8)     // Catch:{ all -> 0x009d }
            return r4
        L_0x0031:
            monitor-exit(r8)     // Catch:{ all -> 0x009d }
            boolean r0 = r8.A09()
            if (r0 == 0) goto L_0x009c
            monitor-enter(r8)
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BEf     // Catch:{ all -> 0x0099 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x0099 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0099 }
            X.1ac r0 = (X.C25921ac) r0     // Catch:{ all -> 0x0099 }
            X.0hL r0 = r0.B6J()     // Catch:{ all -> 0x0099 }
            X.2SP r7 = new X.2SP     // Catch:{ all -> 0x0099 }
            java.lang.String r6 = r0.A01     // Catch:{ all -> 0x0099 }
            long r1 = r0.A00     // Catch:{ all -> 0x0099 }
            int r3 = X.AnonymousClass1Y3.BCt     // Catch:{ all -> 0x0099 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x0099 }
            r5 = 4
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r3, r0)     // Catch:{ all -> 0x0099 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = r0.getPackageName()     // Catch:{ all -> 0x0099 }
            java.util.Map r0 = X.EYZ.A00()     // Catch:{ all -> 0x0099 }
            boolean r0 = r0.containsKey(r3)     // Catch:{ all -> 0x0099 }
            if (r0 == 0) goto L_0x0071
            java.util.Map r0 = X.EYZ.A00()     // Catch:{ all -> 0x0099 }
            java.lang.Object r3 = r0.get(r3)     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0099 }
        L_0x0071:
            r7.<init>(r6, r1, r3)     // Catch:{ all -> 0x0099 }
            r8.A06(r7)     // Catch:{ all -> 0x0099 }
            r2 = 1
            int r0 = X.AnonymousClass1Y3.B9S     // Catch:{ all -> 0x0099 }
            X.0UN r1 = r8.A00     // Catch:{ all -> 0x0099 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r0, r1)     // Catch:{ all -> 0x0099 }
            X.99C r3 = (X.AnonymousClass99C) r3     // Catch:{ all -> 0x0099 }
            X.2SP r2 = r8.A03     // Catch:{ all -> 0x0099 }
            int r0 = X.AnonymousClass1Y3.BCt     // Catch:{ all -> 0x0099 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r0, r1)     // Catch:{ all -> 0x0099 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x0099 }
            java.lang.String r1 = r0.getPackageName()     // Catch:{ all -> 0x0099 }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0099 }
            r3.Btc(r4, r2, r1, r0)     // Catch:{ all -> 0x0099 }
            X.2SP r0 = r8.A03     // Catch:{ all -> 0x0099 }
            monitor-exit(r8)     // Catch:{ all -> 0x0099 }
            return r0
        L_0x0099:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0099 }
            goto L_0x009f
        L_0x009c:
            return r4
        L_0x009d:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x009d }
        L_0x009f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06240bB.A02():X.2SP");
    }

    public synchronized AnonymousClass99G A04() {
        AnonymousClass99G r0;
        r0 = this.A01;
        if (r0 == null) {
            r0 = A03();
            this.A01 = r0;
        }
        return r0;
    }

    public synchronized void A06(AnonymousClass2SP r5) {
        this.A03 = r5;
        AnonymousClass16O A062 = this.A07.A06();
        A062.A0B("phone_id", this.A03.A01);
        A062.A0A("phone_id_ts", this.A03.A00);
        A062.A0B("origin", this.A03.A02);
        A062.A06();
    }

    public static final C06240bB A01(AnonymousClass1XY r5) {
        if (A0C == null) {
            synchronized (C06240bB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0C = new C06240bB(applicationInjector, AnonymousClass0XP.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    public void A07(AnonymousClass99G r5) {
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A00)).edit();
        edit.BzA(C09380hD.A04, r5.A00);
        edit.BzC(C09380hD.A03, r5.A03);
        edit.BzC(C09380hD.A06, r5.A02);
        edit.BzC(C09380hD.A05, r5.A01);
        edit.commit();
    }

    public void A08(boolean z) {
        synchronized (this.A08) {
            this.A04 = Boolean.valueOf(z);
            AnonymousClass16O A062 = this.A07.A06();
            A062.A0D("phone_id_synced", this.A04.booleanValue());
            A062.A06();
        }
    }

    public boolean A09() {
        boolean booleanValue;
        synchronized (this.A08) {
            if (this.A04 == null) {
                this.A04 = Boolean.valueOf(this.A07.A0B("phone_id_synced", false));
            }
            booleanValue = this.A04.booleanValue();
        }
        return booleanValue;
    }

    public boolean A0A() {
        boolean booleanValue;
        boolean z;
        synchronized (this.A09) {
            if (this.A05 == null) {
                boolean z2 = true;
                boolean Aeo = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aeo(2306124823492821744L, true);
                if (this.A0B.get() != null) {
                    z = ((Boolean) this.A0B.get()).booleanValue();
                } else {
                    z = true;
                }
                if (!Aeo || !z) {
                    z2 = false;
                }
                this.A05 = Boolean.valueOf(z2);
            }
            booleanValue = this.A05.booleanValue();
        }
        return booleanValue;
    }

    public boolean A0B() {
        boolean booleanValue;
        synchronized (this.A0A) {
            if (this.A06 == null) {
                this.A06 = Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aeo(2306124823492887281L, true));
            }
            booleanValue = this.A06.booleanValue();
        }
        return booleanValue;
    }

    private C06240bB(AnonymousClass1XY r3, AnonymousClass0XQ r4) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A0B = AnonymousClass0VG.A00(AnonymousClass1Y3.B5l, r3);
        this.A07 = r4.A00("default_phone_id");
    }

    public static final C06240bB A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public AnonymousClass99G A03() {
        if (!A0B()) {
            return null;
        }
        String B4F = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A00)).B4F(C09380hD.A03, null);
        Long valueOf = Long.valueOf(((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A00)).At2(C09380hD.A04, Long.MAX_VALUE));
        String B4F2 = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A00)).B4F(C09380hD.A06, null);
        String B4F3 = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A00)).B4F(C09380hD.A05, null);
        if (C06850cB.A0B(B4F) || C06850cB.A0B(B4F2) || C06850cB.A0B(B4F3)) {
            return null;
        }
        long longValue = valueOf.longValue();
        if (longValue != Long.MAX_VALUE) {
            return new AnonymousClass99G(B4F, longValue, B4F2, B4F3);
        }
        return null;
    }

    public String A05() {
        AnonymousClass2SP A022 = A02();
        if (A022 != null) {
            return A022.A01;
        }
        return null;
    }
}
