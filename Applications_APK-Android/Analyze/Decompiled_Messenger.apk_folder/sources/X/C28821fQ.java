package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.messages.ParticipantInfo;

/* renamed from: X.1fQ  reason: invalid class name and case insensitive filesystem */
public final class C28821fQ implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ParticipantInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new ParticipantInfo[i];
    }
}
