package X;

import com.facebook.inject.InjectorModule;
import com.facebook.messaging.tincan.messenger.TincanPreKeyManager;

@InjectorModule
/* renamed from: X.1tN  reason: invalid class name and case insensitive filesystem */
public final class C36591tN extends AnonymousClass0UV {
    private static volatile TincanPreKeyManager A00;

    public static final TincanPreKeyManager A01(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (TincanPreKeyManager.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        A00 = TincanPreKeyManager.A00(r3.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final AnonymousClass281 A00(AnonymousClass1XY r0) {
        return AnonymousClass281.A00(r0);
    }

    public static final C99484p9 A02(AnonymousClass1XY r0) {
        return C99484p9.A00(r0);
    }
}
