package X;

import android.os.Handler;

/* renamed from: X.1MO  reason: invalid class name */
public final class AnonymousClass1MO extends AnonymousClass0VH {
    public void execute(Runnable runnable) {
        if (this.A00.getLooper().getThread() == Thread.currentThread()) {
            A01(runnable).run();
        } else {
            super.execute(runnable);
        }
    }

    public AnonymousClass1MO(Handler handler, AnonymousClass0VE r2) {
        super(handler, r2);
    }
}
