package X;

import android.content.Context;
import androidx.fragment.app.Fragment;
import com.google.common.collect.ImmutableList;

/* renamed from: X.13r  reason: invalid class name */
public interface AnonymousClass13r {
    void AP7(Context context, Fragment fragment, C33691nz r3);

    Fragment AV6();

    C15390vD Ae6();

    Class AiR();

    AnonymousClass10Z B5D(Context context);

    String B5E(Context context);

    String B6U(Context context);

    ImmutableList B6e();

    boolean BGe(Fragment fragment);

    void BST(boolean z);

    void C4d(Fragment fragment);

    void CBh(Fragment fragment, C33761o6 r2);
}
