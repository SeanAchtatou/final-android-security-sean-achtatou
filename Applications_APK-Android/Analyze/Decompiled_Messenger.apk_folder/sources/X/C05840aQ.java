package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0aQ  reason: invalid class name and case insensitive filesystem */
public final class C05840aQ extends AnonymousClass0Wl {
    private static volatile C05840aQ A02;
    private final Context A00;
    private final C25051Yd A01;

    public String getSimpleName() {
        return "StaticGraphServiceFactoryInitializer";
    }

    public synchronized void init() {
        int A03 = C000700l.A03(-1185055544);
        if (this.A01.Aem(2306125033946219528L)) {
            try {
                this.A00.getAssets();
                C05850aR.A00(this.A00);
            } catch (RuntimeException unused) {
            }
        }
        C000700l.A09(-1228924220, A03);
    }

    public static final C05840aQ A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C05840aQ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r4.getApplicationInjector();
                        AnonymousClass1YA.A02(applicationInjector);
                        A02 = new C05840aQ(applicationInjector);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C05840aQ(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1YA.A00(r2);
        this.A01 = AnonymousClass0WT.A00(r2);
    }
}
