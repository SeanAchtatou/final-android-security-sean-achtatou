package X;

import android.database.Cursor;

/* renamed from: X.0cX  reason: invalid class name and case insensitive filesystem */
public final class C07040cX extends AnonymousClass0UT {
    public C12170og A00(Cursor cursor, boolean z) {
        C13660rp r3 = new C13660rp(this);
        C13670rq r2 = new C13670rq(this);
        new C13690rs(this);
        return new C12170og(r3, r2, cursor, z);
    }

    public C07040cX(AnonymousClass1XY r1) {
        super(r1);
    }
}
