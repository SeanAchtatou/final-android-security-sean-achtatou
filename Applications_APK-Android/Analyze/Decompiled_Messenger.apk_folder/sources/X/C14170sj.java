package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.prefs.shared.FbSharedPreferences;

@UserScoped
/* renamed from: X.0sj  reason: invalid class name and case insensitive filesystem */
public final class C14170sj {
    private static C05540Zi A01;
    public AnonymousClass0UN A00;

    public static final C14170sj A00(AnonymousClass1XY r4) {
        C14170sj r0;
        synchronized (C14170sj.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C14170sj((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C14170sj) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public static Integer A01(C14170sj r6) {
        String B4F = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r6.A00)).B4F(C14090sb.A00, null);
        for (Integer num : C14190sl.A00) {
            if (C14190sl.A01(num).equals(B4F)) {
                return num;
            }
        }
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, ((C14140sg) AnonymousClass1XX.A03(AnonymousClass1Y3.BMG, r6.A00)).A00)).AbO(AnonymousClass1Y3.A1O, false)) {
            return AnonymousClass07B.A01;
        }
        return AnonymousClass07B.A00;
    }

    private C14170sj(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
