package X;

import android.content.res.Resources;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.messaging.montage.model.art.ArtItem;
import com.google.common.base.Preconditions;

/* renamed from: X.1Jb  reason: invalid class name and case insensitive filesystem */
public final class C21881Jb extends C20831Dz implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.composer.art.bottomsheetpicker.BottomSheetStickerAdapter";
    public C26639D5f A00;
    public C26512Czy A01;
    public final Resources A02;
    public final C61832zZ A03;
    public final AnonymousClass1MT A04;
    public final C73473g8 A05;

    public static final C21881Jb A00(AnonymousClass1XY r1) {
        return new C21881Jb(r1);
    }

    public static ArtItem A01(C21881Jb r1, int i) {
        C26512Czy czy = r1.A01;
        if (czy == null || i < 0) {
            return null;
        }
        Preconditions.checkArgument(czy.A01.get(i) instanceof ArtItem);
        return (ArtItem) r1.A01.A01.get(i);
    }

    public int ArU() {
        C26512Czy czy = this.A01;
        if (czy == null) {
            return 0;
        }
        return czy.A01.size();
    }

    public C21881Jb(AnonymousClass1XY r2) {
        this.A03 = C61832zZ.A00(r2);
        this.A02 = C04490Ux.A0L(r2);
        this.A04 = AnonymousClass1MT.A00(r2);
        this.A05 = C73473g8.A00(r2);
    }
}
