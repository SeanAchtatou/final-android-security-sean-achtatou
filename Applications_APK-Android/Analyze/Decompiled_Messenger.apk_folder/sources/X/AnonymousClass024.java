package X;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import java.io.FileDescriptor;

/* renamed from: X.024  reason: invalid class name */
public final class AnonymousClass024 {
    public static boolean A00 = true;

    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x0083 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00() {
        /*
            boolean r0 = X.AnonymousClass024.A00
            r7 = -1
            if (r0 != 0) goto L_0x0006
            return r7
        L_0x0006:
            int r0 = android.os.Build.VERSION.SDK_INT
            java.lang.String r3 = "/dev/zero"
            r4 = 21
            r2 = 0
            if (r0 < r4) goto L_0x006c
            java.io.FileDescriptor r3 = A01(r3)
        L_0x0013:
            if (r3 == 0) goto L_0x00bb
            boolean r0 = r3.valid()
            if (r0 == 0) goto L_0x00bb
            r11 = 12
            r8 = 32
        L_0x001f:
            int r0 = r11 + 1
            if (r0 >= r8) goto L_0x00a7
            int r0 = r11 + r8
            int r10 = r0 / 2
            r0 = 1
            long r0 = r0 << r10
            int r5 = android.os.Build.VERSION.SDK_INT     // Catch:{ IllegalStateException -> 0x008b }
            if (r5 < r4) goto L_0x0033
            boolean r0 = A05(r3, r0)     // Catch:{ IllegalStateException -> 0x008b }
            goto L_0x0066
        L_0x0033:
            boolean r5 = X.AnonymousClass024.A00     // Catch:{ IllegalStateException -> 0x008b }
            if (r5 == 0) goto L_0x0065
            libcore.io.Os r12 = libcore.io.Libcore.os     // Catch:{ Exception -> 0x0052, NoClassDefFoundError | NoSuchMethodError -> 0x0083 }
            r13 = 0
            int r17 = libcore.io.OsConstants.PROT_READ     // Catch:{ Exception -> 0x0052, NoClassDefFoundError | NoSuchMethodError -> 0x0083 }
            int r5 = libcore.io.OsConstants.PROT_WRITE     // Catch:{ Exception -> 0x0052, NoClassDefFoundError | NoSuchMethodError -> 0x0083 }
            r17 = r17 | r5
            r18 = 0
            r20 = 0
            r15 = r0
            r19 = r3
            long r5 = r12.mmap(r13, r15, r17, r18, r19, r20)     // Catch:{ Exception -> 0x0052, NoClassDefFoundError | NoSuchMethodError -> 0x0083 }
            libcore.io.Os r9 = libcore.io.Libcore.os     // Catch:{ Exception -> 0x0052, NoClassDefFoundError | NoSuchMethodError -> 0x0083 }
            r9.munmap(r5, r0)     // Catch:{ Exception -> 0x0052, NoClassDefFoundError | NoSuchMethodError -> 0x0083 }
            goto L_0x0085
        L_0x0052:
            r1 = move-exception
            boolean r0 = r1 instanceof libcore.io.ErrnoException     // Catch:{ IllegalStateException -> 0x008b }
            if (r0 == 0) goto L_0x0085
            libcore.io.ErrnoException r1 = (libcore.io.ErrnoException) r1     // Catch:{ IllegalStateException -> 0x008b }
            int r1 = r1.errno     // Catch:{ IllegalStateException -> 0x008b }
            int r0 = libcore.io.OsConstants.EINVAL     // Catch:{ IllegalStateException -> 0x008b }
            if (r1 != r0) goto L_0x0061
            r0 = 1
            goto L_0x0066
        L_0x0061:
            int r0 = libcore.io.OsConstants.ENOMEM     // Catch:{ IllegalStateException -> 0x008b }
            if (r1 != r0) goto L_0x0085
        L_0x0065:
            r0 = 0
        L_0x0066:
            if (r0 == 0) goto L_0x006a
            r11 = r10
            goto L_0x001f
        L_0x006a:
            r8 = r10
            goto L_0x001f
        L_0x006c:
            libcore.io.Os r1 = libcore.io.Libcore.os     // Catch:{ Exception -> 0x007d, NoClassDefFoundError | NoSuchMethodError -> 0x0075 }
            int r0 = libcore.io.OsConstants.O_RDWR     // Catch:{ Exception -> 0x007d, NoClassDefFoundError | NoSuchMethodError -> 0x0075 }
            java.io.FileDescriptor r3 = r1.open(r3, r0, r2)     // Catch:{ Exception -> 0x007d, NoClassDefFoundError | NoSuchMethodError -> 0x0075 }
            goto L_0x0013
        L_0x0075:
            java.io.FileDescriptor r3 = new java.io.FileDescriptor
            r3.<init>()
            X.AnonymousClass024.A00 = r2
            goto L_0x0013
        L_0x007d:
            java.io.FileDescriptor r3 = new java.io.FileDescriptor
            r3.<init>()
            goto L_0x0013
        L_0x0083:
            X.AnonymousClass024.A00 = r2     // Catch:{ IllegalStateException -> 0x008b }
        L_0x0085:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ IllegalStateException -> 0x008b }
            r0.<init>()     // Catch:{ IllegalStateException -> 0x008b }
            throw r0     // Catch:{ IllegalStateException -> 0x008b }
        L_0x008b:
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 >= r4) goto L_0x00b8
            libcore.io.Os r0 = libcore.io.Libcore.os     // Catch:{ Exception -> 0x00bb, NoClassDefFoundError | NoSuchMethodError -> 0x00b5 }
            r0.close(r3)     // Catch:{ Exception -> 0x00bb, NoClassDefFoundError | NoSuchMethodError -> 0x00b5 }
            return r7
        L_0x0095:
            r1 = move-exception
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r4) goto L_0x009e
            A03(r3)
        L_0x009d:
            throw r1
        L_0x009e:
            libcore.io.Os r0 = libcore.io.Libcore.os     // Catch:{ Exception -> 0x009d, NoClassDefFoundError | NoSuchMethodError -> 0x00a4 }
            r0.close(r3)     // Catch:{ Exception -> 0x009d, NoClassDefFoundError | NoSuchMethodError -> 0x00a4 }
            goto L_0x009d
        L_0x00a4:
            X.AnonymousClass024.A00 = r2
            goto L_0x009d
        L_0x00a7:
            int r0 = r11 + -10
            r7 = 1
            int r7 = r7 << r0
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 >= r4) goto L_0x00b8
            libcore.io.Os r0 = libcore.io.Libcore.os     // Catch:{ Exception -> 0x00bb, NoClassDefFoundError | NoSuchMethodError -> 0x00b5 }
            r0.close(r3)     // Catch:{ Exception -> 0x00bb, NoClassDefFoundError | NoSuchMethodError -> 0x00b5 }
            return r7
        L_0x00b5:
            X.AnonymousClass024.A00 = r2
            return r7
        L_0x00b8:
            A03(r3)
        L_0x00bb:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass024.A00():int");
    }

    public static final FileDescriptor A01(String str) {
        try {
            return Os.open(str, OsConstants.O_RDWR, 0);
        } catch (ErrnoException unused) {
            return new FileDescriptor();
        }
    }

    public static final boolean A05(FileDescriptor fileDescriptor, long j) {
        try {
            Os.munmap(Os.mmap(0, j, OsConstants.PROT_READ | OsConstants.PROT_WRITE, 0, fileDescriptor, 0), j);
        } catch (ErrnoException e) {
            int i = e.errno;
            if (i == OsConstants.EINVAL) {
                return true;
            }
            if (i == OsConstants.ENOMEM) {
                return false;
            }
        }
        throw new IllegalStateException();
    }

    public static final void A03(FileDescriptor fileDescriptor) {
        if (fileDescriptor.valid()) {
            try {
                Os.close(fileDescriptor);
            } catch (ErrnoException unused) {
            }
        }
    }
}
