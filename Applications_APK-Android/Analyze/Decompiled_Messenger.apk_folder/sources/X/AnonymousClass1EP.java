package X;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;

/* renamed from: X.1EP  reason: invalid class name */
public abstract class AnonymousClass1EP implements AnonymousClass1EM {
    public static final AnonymousClass1EO A03 = new AnonymousClass1EO(null, AnonymousClass07B.A00);
    public AnonymousClass1G0 A00;
    public C20021Ap A01 = new AnonymousClass17U();
    public final Executor A02;

    public void A08() {
        if (this instanceof AnonymousClass17O) {
            AnonymousClass17O r2 = (AnonymousClass17O) this;
            r2.A00 = r2.A01.now();
        }
    }

    public void A09() {
        if (this instanceof AnonymousClass17O) {
            AnonymousClass17O r2 = (AnonymousClass17O) this;
            r2.A00 = r2.A01.now();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0086, code lost:
        if (r4.A02 == X.C25611a7.TOP) goto L_0x0088;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1EO A0A(java.lang.Object r9) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.AnonymousClass1TC
            if (r0 != 0) goto L_0x0098
            boolean r0 = r8 instanceof X.AnonymousClass17O
            if (r0 != 0) goto L_0x0038
            r2 = r8
            X.17S r2 = (X.AnonymousClass17S) r2
            X.1nc r9 = (X.C33461nc) r9
            monitor-enter(r2)
            X.0hU r1 = r9.A00     // Catch:{ all -> 0x0035 }
            X.0hU r0 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA     // Catch:{ all -> 0x0035 }
            if (r1 != r0) goto L_0x0017
            X.1EO r0 = X.AnonymousClass1EP.A03     // Catch:{ all -> 0x0035 }
            goto L_0x001d
        L_0x0017:
            X.101 r0 = r2.A02     // Catch:{ all -> 0x0035 }
            if (r0 != 0) goto L_0x001f
            X.1EO r0 = X.AnonymousClass1EP.A03     // Catch:{ all -> 0x0035 }
        L_0x001d:
            monitor-exit(r2)
            return r0
        L_0x001f:
            boolean r0 = X.AnonymousClass17S.A01(r2, r0)     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x002d
            X.101 r0 = r2.A02     // Catch:{ all -> 0x0035 }
            X.1EO r0 = X.AnonymousClass1EO.A01(r0)     // Catch:{ all -> 0x0035 }
            monitor-exit(r2)
            return r0
        L_0x002d:
            X.101 r0 = r2.A02     // Catch:{ all -> 0x0035 }
            X.1EO r0 = X.AnonymousClass1EO.A00(r0)     // Catch:{ all -> 0x0035 }
            monitor-exit(r2)
            return r0
        L_0x0035:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0038:
            r7 = r8
            X.17O r7 = (X.AnonymousClass17O) r7
            X.1G5 r9 = (X.AnonymousClass1G5) r9
            java.lang.Integer r1 = r9.A00
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 == r0) goto L_0x00a8
            X.0Zh r3 = r7.A03
            monitor-enter(r3)
            X.100 r2 = r3.A01     // Catch:{ all -> 0x0095 }
            if (r2 != 0) goto L_0x004b
            goto L_0x0054
        L_0x004b:
            X.1G6 r1 = new X.1G6     // Catch:{ all -> 0x0095 }
            boolean r0 = r3.A03     // Catch:{ all -> 0x0095 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0095 }
            monitor-exit(r3)
            goto L_0x0056
        L_0x0054:
            r1 = 0
            monitor-exit(r3)
        L_0x0056:
            if (r1 == 0) goto L_0x00a8
            X.100 r0 = r1.A00
            X.101 r4 = r0.A01
            boolean r0 = r1.A01
            if (r0 != 0) goto L_0x0088
            X.06B r0 = r7.A01
            long r5 = r0.now()
            long r0 = r4.A00
            long r5 = r5 - r0
            X.0hQ r0 = r7.A02
            long r2 = r0.AtO()
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x0075
            r0 = 1
        L_0x0075:
            if (r0 != 0) goto L_0x0088
            X.102 r1 = r4.A01
            X.102 r0 = X.AnonymousClass102.FROM_CACHE_STALE
            if (r1 == r0) goto L_0x0088
            X.102 r0 = X.AnonymousClass102.NO_DATA
            if (r1 == r0) goto L_0x0088
            X.1a7 r2 = r4.A02
            X.1a7 r1 = X.C25611a7.TOP
            r0 = 1
            if (r2 != r1) goto L_0x0089
        L_0x0088:
            r0 = 0
        L_0x0089:
            if (r0 == 0) goto L_0x0090
            X.1EO r0 = X.AnonymousClass1EO.A00(r4)
            return r0
        L_0x0090:
            X.1EO r0 = X.AnonymousClass1EO.A01(r4)
            return r0
        L_0x0095:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0098:
            r0 = r8
            X.1TC r0 = (X.AnonymousClass1TC) r0
            X.1TD r0 = r0.A00
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r0 = r0.A01()
            if (r0 == 0) goto L_0x00a8
            X.1EO r0 = X.AnonymousClass1EO.A00(r0)
            return r0
        L_0x00a8:
            X.1EO r0 = X.AnonymousClass1EP.A03
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1EP.A0A(java.lang.Object):X.1EO");
    }

    public void A0C() {
        if (this instanceof AnonymousClass17S) {
            AnonymousClass17S r1 = (AnonymousClass17S) this;
            synchronized (r1) {
                r1.A01 = null;
            }
        }
    }

    public ListenableFuture A07(Object obj, AnonymousClass1EO r3) {
        if (this instanceof AnonymousClass1TC) {
            return ((AnonymousClass1TC) this).A00.A02();
        }
        throw new IllegalStateException();
    }

    public ListenableFuture A0B(Object obj, AnonymousClass1EO r7) {
        C09510hU r3;
        C25611a7 r2;
        ListenableFuture A002;
        if (this instanceof AnonymousClass17O) {
            AnonymousClass17O r4 = (AnonymousClass17O) this;
            AnonymousClass1G5 r6 = (AnonymousClass1G5) obj;
            Integer num = r6.A00;
            if (num == AnonymousClass07B.A00) {
                r3 = C09510hU.CHECK_SERVER_FOR_NEW_DATA;
                r2 = C25611a7.ALL;
            } else if (r7 != null && r7.A00 == AnonymousClass07B.A01) {
                r3 = C09510hU.PREFER_CACHE_IF_UP_TO_DATE;
                r2 = C25611a7.ALL;
            } else if (num == AnonymousClass07B.A0C) {
                r3 = C09510hU.DO_NOT_CHECK_SERVER;
                r2 = C25611a7.TOP;
            } else if (num == AnonymousClass07B.A0N) {
                r3 = C09510hU.DO_NOT_CHECK_SERVER;
                r2 = C25611a7.ALL;
            } else {
                r3 = C09510hU.STALE_DATA_OKAY;
                r2 = C25611a7.ALL;
            }
            return AnonymousClass169.A00(r4.A03.A03(new C09500hT(r3, r2, r6.A01)), new AnonymousClass1GK(r3, r2));
        } else if (!(this instanceof AnonymousClass17S)) {
            return AnonymousClass169.A00(A07(obj, r7), new C22301Ku());
        } else {
            AnonymousClass17S r42 = (AnonymousClass17S) this;
            C33461nc r62 = (C33461nc) obj;
            synchronized (r42) {
                if (r7.A00 == AnonymousClass07B.A01) {
                    r62 = new C33461nc(C09510hU.CHECK_SERVER_FOR_NEW_DATA);
                }
                r42.A01 = r62;
                A002 = AnonymousClass169.A00(((C05160Xw) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJD, r42.A00)).CIF(new C21561Hu(r42, r62)), new C21571Hv(r42, r62));
            }
            return A002;
        }
    }

    public void ARp() {
        AnonymousClass1G0 r1 = this.A00;
        if (r1 != null) {
            this.A00 = null;
            r1.A01(false);
        }
    }

    public void C6Z(C20021Ap r2) {
        if (r2 == null) {
            this.A01 = new AnonymousClass17U();
        } else {
            this.A01 = r2;
        }
    }

    public AnonymousClass1EP(Executor executor) {
        this.A02 = executor;
    }

    public static void A02(AnonymousClass1EP r4, Object obj, AnonymousClass1EO r6, int i) {
        ListenableFuture A0B = r4.A0B(obj, r6);
        r4.A09();
        AnonymousClass07A.A04(r4.A02, new AnonymousClass1GP(r4, obj, A0B), -936649627);
        AnonymousClass1EQ r1 = new AnonymousClass1EQ(r4, obj, i);
        r4.A00 = AnonymousClass1G0.A00(A0B, r1);
        C05350Yp.A08(A0B, r1, r4.A02);
    }

    public void CHB(Object obj) {
        ARp();
        A08();
        AnonymousClass1EO A0A = A0A(obj);
        Preconditions.checkNotNull(A0A);
        Integer num = A0A.A00;
        boolean z = false;
        if (num != AnonymousClass07B.A00) {
            z = true;
        }
        if (z) {
            Object obj2 = A0A.A01;
            boolean z2 = false;
            if (num == AnonymousClass07B.A0C) {
                z2 = true;
            }
            AnonymousClass07A.A04(this.A02, new AnonymousClass1G7(this, obj, obj2, z2), -282398520);
            if (z2) {
                return;
            }
        }
        A02(this, obj, A0A, 1);
    }
}
