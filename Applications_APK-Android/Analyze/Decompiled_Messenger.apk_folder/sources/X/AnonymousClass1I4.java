package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1I4  reason: invalid class name */
public final class AnonymousClass1I4 {
    private static volatile AnonymousClass1I4 A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass1I4 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1I4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1I4(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass1I4(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
