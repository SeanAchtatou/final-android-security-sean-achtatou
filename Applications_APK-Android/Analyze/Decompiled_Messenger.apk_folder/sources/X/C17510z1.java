package X;

import android.content.res.Configuration;

/* renamed from: X.0z1  reason: invalid class name and case insensitive filesystem */
public abstract class C17510z1 {
    public static C17510z1 latest;
    public final Configuration A00;

    public Object A00(int i) {
        return ((C17520z2) this).A00.A03(Integer.valueOf(i));
    }

    public void A01(int i, Object obj) {
        ((C17520z2) this).A00.A04(Integer.valueOf(i), obj);
    }

    public C17510z1(Configuration configuration) {
        this.A00 = configuration;
    }
}
