package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.0p0  reason: invalid class name and case insensitive filesystem */
public final class C12280p0 {
    public static final C21321Gd A00 = new C21321Gd();
    public static final C12290p1 A01 = C12290p1.A08;
    public static final Object A02 = new Object();
    public static volatile C50422dy A03 = new EF3();

    public static Bundle A00(Intent intent) {
        if (intent != null) {
            Bundle bundleExtra = intent.getBundleExtra("surface_bundle");
            if (bundleExtra != null) {
                return bundleExtra;
            }
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey("surface_bundle")) {
                return extras.getBundle("surface_bundle");
            }
        }
        return null;
    }

    public static C27581dQ A01(Context context, Bundle bundle) {
        Object newInstance;
        Context baseContext;
        Class cls = (Class) bundle.getSerializable("surface_prop_class");
        if (cls != null) {
            Bundle bundle2 = bundle.getBundle("surface_props_bundle");
            try {
                Constructor<?>[] declaredConstructors = cls.getDeclaredConstructors();
                if (declaredConstructors.length != 0) {
                    Constructor<?> constructor = declaredConstructors[0];
                    constructor.setAccessible(true);
                    if (constructor.getParameterTypes().length == 0) {
                        newInstance = constructor.newInstance(new Object[0]);
                    } else {
                        newInstance = constructor.newInstance(context);
                    }
                    C27581dQ A05 = ((C27581dQ) newInstance).A05(new C131486Ds(context), bundle2);
                    Activity A012 = C73193fe.A01(context);
                    int i = 0;
                    if (!(A012 == null || A012.isFinishing() || (baseContext = A012.getBaseContext()) == null || baseContext.getApplicationContext() == null)) {
                        i = C73193fe.A00(A012);
                    }
                    A05.A00 = bundle.getInt("surface_height_offset", i);
                    return A05;
                }
                throw new IllegalStateException("More than one private constructors found");
            } catch (InstantiationException unused) {
                throw new IllegalStateException("SurfaceProps cannot be created from a private constructor with no parameter");
            } catch (InvocationTargetException e) {
                throw new IllegalStateException(e.getCause());
            } catch (IllegalAccessException e2) {
                throw new IllegalStateException(e2.getMessage());
            }
        } else {
            throw new C37761wD("Must use getIntentForActivityWithProps() or getBundleWithProps() to navigate");
        }
    }

    public static boolean A02(C12260oy r2) {
        boolean contains;
        synchronized (A02) {
            contains = A00.contains(r2);
        }
        return contains;
    }
}
