package X;

import android.os.Looper;

/* renamed from: X.0lZ  reason: invalid class name */
public final class AnonymousClass0lZ implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.analytics.perf.MessagingPerformanceLogger$1";
    public final /* synthetic */ C08770fv A00;

    public AnonymousClass0lZ(C08770fv r1) {
        this.A00 = r1;
    }

    public void run() {
        this.A00.A0I.AOz();
        C08770fv r0 = this.A00;
        Runnable runnable = r0.A06;
        if (runnable != null) {
            r0.A0I.C1d(runnable);
        }
        Looper.myQueue().addIdleHandler(this.A00.A0D);
        this.A00.A0A = true;
    }
}
