package X;

import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

@InjectorModule
/* renamed from: X.1Mq  reason: invalid class name and case insensitive filesystem */
public final class C22741Mq extends AnonymousClass0UV {
    private static volatile C188458oz A00;
    private static volatile C22751Mr A01;

    public static final C188458oz A00(AnonymousClass1XY r5) {
        C188458oz r0;
        if (A00 == null) {
            synchronized (C188458oz.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        C04910Wu.A00(applicationInjector);
                        FbSharedPreferences A003 = FbSharedPreferencesModule.A00(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.BPH, applicationInjector);
                        if (AnonymousClass00M.A00().A05()) {
                            r0 = new C188458oz(A003, A004);
                        } else {
                            r0 = null;
                        }
                        A00 = r0;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C22751Mr A01(AnonymousClass1XY r9) {
        C22751Mr r4;
        if (A01 == null) {
            synchronized (C22751Mr.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r9);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r9.getApplicationInjector();
                        C04910Wu.A00(applicationInjector);
                        FbSharedPreferences A003 = FbSharedPreferencesModule.A00(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.BPH, applicationInjector);
                        AnonymousClass06B A02 = AnonymousClass067.A02();
                        AnonymousClass0VG A005 = AnonymousClass0VG.A00(AnonymousClass1Y3.A7s, applicationInjector);
                        AnonymousClass00M A006 = AnonymousClass00M.A00();
                        if (A006.A05()) {
                            r4 = null;
                        } else {
                            r4 = new C22751Mr(A003, A006.A04(), A004, A02, A005);
                        }
                        A01 = r4;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
