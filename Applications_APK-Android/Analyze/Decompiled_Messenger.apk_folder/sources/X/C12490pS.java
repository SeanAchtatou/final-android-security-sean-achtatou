package X;

/* renamed from: X.0pS  reason: invalid class name and case insensitive filesystem */
public final class C12490pS implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.debug.debugoverlay.DebugOverlayController$1";
    public final /* synthetic */ C12450pN A00;

    public C12490pS(C12450pN r1) {
        this.A00 = r1;
    }

    public void run() {
        if (this.A00.A00.get() != null) {
            AHI ahi = (AHI) this.A00.A00.get();
            ahi.A00.clear();
            AHI.A00(ahi);
            ahi.setVisibility(8);
        }
    }
}
