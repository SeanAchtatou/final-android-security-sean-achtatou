package X;

import com.facebook.fbservice.service.BlueServiceLogic;

/* renamed from: X.1q4  reason: invalid class name and case insensitive filesystem */
public final class C34721q4 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.fbservice.service.BlueServiceRunning$1";
    public final /* synthetic */ C34711q3 A00;

    public C34721q4(C34711q3 r1) {
        this.A00 = r1;
    }

    public void run() {
        C34711q3 r3 = this.A00;
        synchronized (r3) {
            r3.A02 = false;
            if (r3.A00 == 0) {
                ((BlueServiceLogic) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AVT, r3.A01)).A03();
            }
        }
    }
}
