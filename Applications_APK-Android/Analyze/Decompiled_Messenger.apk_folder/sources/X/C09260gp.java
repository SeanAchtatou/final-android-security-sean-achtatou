package X;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0gp  reason: invalid class name and case insensitive filesystem */
public abstract class C09260gp extends AnonymousClass1YH {
    public final C09280gs A00;

    public static synchronized SQLiteDatabase A01(C09260gp r1) {
        SQLiteDatabase A06;
        synchronized (r1) {
            A06 = super.A06();
        }
        return A06;
    }

    public synchronized SQLiteDatabase A06() {
        SQLiteDatabase A01;
        A01 = A01(this);
        synchronized (this) {
            try {
                this.A00.A02(A01.getPath());
            } catch (C37671w3 e) {
                throw new IllegalStateException(AnonymousClass08S.A0J("Cannot store uid, initial exception: ", e.getMessage()), e);
            } catch (C37671w3 e2) {
                A07();
                if (AnonymousClass1YH.A05) {
                    A01 = A01(this);
                }
                this.A00.A02(A01.getPath());
            }
        }
        return A01;
    }

    public void A09() {
        String str;
        synchronized (this) {
            SQLiteDatabase sQLiteDatabase = this.A00;
            if (sQLiteDatabase != null) {
                str = sQLiteDatabase.getPath();
            } else {
                str = null;
            }
        }
        if (str != null) {
            this.A00.A01(str);
        }
        super.A09();
    }

    public C09260gp(Context context, C04740Vz r2, C09280gs r3, ImmutableList immutableList, String str) {
        super(context, r2, immutableList, str);
        this.A00 = r3;
    }
}
