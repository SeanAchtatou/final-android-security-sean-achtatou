package X;

import android.os.DeadObjectException;
import android.os.Message;
import android.os.RemoteException;
import com.google.common.base.Joiner;
import java.util.ArrayList;

/* renamed from: X.188  reason: invalid class name */
public final class AnonymousClass188 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.multiprocess.peer.PeerProcessManagerImpl$5";
    public final /* synthetic */ Message A00;
    public final /* synthetic */ C07930eP A01;

    public AnonymousClass188(C07930eP r1, Message message) {
        this.A01 = r1;
        this.A00 = message;
    }

    public void run() {
        ArrayList<C06130au> A002 = C04300To.A00();
        for (C06130au r7 : this.A01.A0B.values()) {
            try {
                r7.A00.send(this.A00);
            } catch (RemoteException e) {
                if (e instanceof DeadObjectException) {
                    A002.add(r7);
                } else {
                    AnonymousClass09P r3 = this.A01.A06;
                    r3.softReport("PeerProcessManagerImpl", "RemoteException occurred when sending the message to peer " + r7.A02 + "; message: " + this.A00 + "; data keys: " + Joiner.on(", ").join(this.A00.getData().keySet()) + "; peer info: " + this.A01.A07, e);
                }
            }
        }
        for (C06130au A03 : A002) {
            C07930eP.A03(this.A01, A03);
        }
    }
}
