package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.analytics.InboxSourceLoggingData;

/* renamed from: X.1mS  reason: invalid class name and case insensitive filesystem */
public final class C32861mS implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxSourceLoggingData(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxSourceLoggingData[i];
    }
}
