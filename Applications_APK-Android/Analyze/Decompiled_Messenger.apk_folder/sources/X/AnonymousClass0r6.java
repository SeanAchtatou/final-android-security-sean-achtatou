package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.user.model.LastActive;
import com.facebook.user.model.UserKey;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0r6  reason: invalid class name */
public final class AnonymousClass0r6 implements AnonymousClass1YQ, C13270r7 {
    public static final Class A0X = AnonymousClass0r6.class;
    private static volatile AnonymousClass0r6 A0Y;
    public long A00 = -1;
    public long A01 = -1;
    public AnonymousClass0UN A02;
    public C13430rQ A03;
    public C13430rQ A04;
    public AnonymousClass1F5 A05;
    public ScheduledFuture A06;
    public boolean A07;
    private boolean A08;
    private boolean A09;
    private boolean A0A;
    private boolean A0B;
    private boolean A0C;
    public final C05180Xy A0D;
    public final C29761gw A0E;
    public final C29761gw A0F;
    public final C29761gw A0G;
    public final C29761gw A0H;
    public final AnonymousClass0t3 A0I;
    public final AnonymousClass0V0 A0J;
    public final Runnable A0K = new C14850uF(this);
    public final Map A0L;
    public final ConcurrentMap A0M;
    public final ConcurrentMap A0N;
    public final C04310Tq A0O;
    public final C04310Tq A0P;
    public final C04310Tq A0Q;
    private final C06790c5 A0R;
    private final AnonymousClass0ZM A0S;
    private final AnonymousClass0V0 A0T;
    private final Set A0U = C008907u.A00();
    private final C04310Tq A0V;
    public volatile boolean A0W;

    public static synchronized void A06(AnonymousClass0r6 r2) {
        synchronized (r2) {
            r2.A0B = false;
            if (r2.A0C) {
                r2.A0C = false;
                r2.A0Q();
            }
        }
    }

    public static synchronized void A07(AnonymousClass0r6 r2) {
        synchronized (r2) {
            r2.A0A = false;
            if (r2.A09) {
                r2.A09 = false;
                r2.A0R();
            }
        }
    }

    public static void A0A(AnonymousClass0r6 r5, UserKey userKey) {
        C05180Xy r0;
        C195218m A0L2 = r5.A0L(userKey, -1);
        synchronized (r5.A0T) {
            Collection AbK = r5.A0T.AbK(userKey);
            if (AbK == null) {
                r0 = null;
            } else {
                r0 = new C05180Xy(AbK);
            }
        }
        A08(r5, r0, userKey, A0L2, null);
    }

    public static synchronized boolean A0G(AnonymousClass0r6 r1) {
        synchronized (r1) {
            AnonymousClass1F5 r0 = r1.A05;
            if (r0 == null) {
                return false;
            }
            boolean z = r0.A00;
            return z;
        }
    }

    public static synchronized boolean A0I(AnonymousClass0r6 r4, boolean z) {
        synchronized (r4) {
            if (r4.A05 == null) {
                AnonymousClass1F5 r3 = new AnonymousClass1F5("/t_p", 0, AnonymousClass1FP.APP_USE);
                r4.A05 = r3;
                ((C32981me) AnonymousClass1XX.A02(13, AnonymousClass1Y3.Al3, r4.A02)).A00.add(r3);
            }
            AnonymousClass1F5 r1 = r4.A05;
            if (r1.A00 == z) {
                return false;
            }
            if (z) {
                r1.A00 = true;
            } else {
                r1.A00 = false;
            }
            ((C32471lo) AnonymousClass1XX.A02(11, AnonymousClass1Y3.ArN, r4.A02)).A00.A02();
            return true;
        }
    }

    private boolean A0J(UserKey userKey) {
        if (userKey != null) {
            int i = AnonymousClass1Y3.BAo;
            AnonymousClass0UN r2 = this.A02;
            if ((!((Boolean) AnonymousClass1XX.A02(22, i, r2)).booleanValue() || !((C21441Gz) AnonymousClass1XX.A02(20, AnonymousClass1Y3.BDi, r2)).A01(userKey.id)) && A0Y()) {
                return true;
            }
            return false;
        }
        return false;
    }

    public synchronized void A0Q() {
        if (this.A0B) {
            this.A0C = true;
        } else {
            this.A0B = true;
            C05350Yp.A08(((AnonymousClass0VL) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZx, this.A02)).CIF(new AnonymousClass25M(this)), new AnonymousClass21N(this), (Executor) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A6Z, this.A02));
        }
    }

    public synchronized void A0R() {
        long j;
        AnonymousClass0VL r1;
        if (((C193517u) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B1g, this.A02)).A06()) {
            if (this.A0A) {
                this.A09 = true;
            } else {
                this.A0A = true;
                C24521Ty r2 = (C24521Ty) AnonymousClass1XX.A02(16, AnonymousClass1Y3.BHU, this.A02);
                if (((C001500z) AnonymousClass1XX.A03(AnonymousClass1Y3.BH4, r2.A00)).ordinal() != 0) {
                    j = 0;
                } else {
                    j = ((C25051Yd) r2.A01.get()).At0(563976455914133L);
                }
                if (j == 0) {
                    r1 = (AnonymousClass0VL) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZx, this.A02);
                } else {
                    C24521Ty r3 = (C24521Ty) AnonymousClass1XX.A02(16, AnonymousClass1Y3.BHU, this.A02);
                    if (j == 1) {
                        r1 = (AnonymousClass0VL) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Am5, r3.A00);
                    } else if (j == 4) {
                        r1 = (AnonymousClass0VL) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A4I, r3.A00);
                    } else if (j == 8) {
                        r1 = (AnonymousClass0VL) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AVD, r3.A00);
                    } else {
                        r1 = (AnonymousClass0VL) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZx, r3.A00);
                    }
                }
                C05350Yp.A07(r1.CIF(new C36971uP(this)), new C36981uQ(this));
            }
        }
    }

    public boolean A0Z(UserKey userKey) {
        C37291v0 r0;
        return userKey != null && A0J(userKey) && (r0 = (C37291v0) this.A0N.get(userKey)) != null && r0.A0A;
    }

    public String getSimpleName() {
        return "DefaultPresenceManager";
    }

    public void onDeviceActive() {
    }

    public void onDeviceStopped() {
    }

    public static final AnonymousClass0r6 A00(AnonymousClass1XY r4) {
        if (A0Y == null) {
            synchronized (AnonymousClass0r6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0Y, r4);
                if (A002 != null) {
                    try {
                        A0Y = new AnonymousClass0r6(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0Y;
    }

    public static C37291v0 A01(AnonymousClass0r6 r2, UserKey userKey) {
        C37291v0 r1 = (C37291v0) r2.A0N.get(userKey);
        if (r1 == null) {
            r1 = new C37291v0();
            r1.A09 = userKey;
            C37291v0 r0 = (C37291v0) r2.A0N.putIfAbsent(userKey, r1);
            if (r0 != null) {
                return r0;
            }
        }
        return r1;
    }

    public static Collection A02(AnonymousClass0r6 r10, int i, int i2) {
        try {
            C005505z.A03("PresenceManager:getOnlineUsersInternal", 1112732635);
            boolean z = false;
            if (r10.A0Y()) {
                z = true;
            }
            if (!z) {
                return Collections.emptyList();
            }
            ArrayList A002 = C04300To.A00();
            int i3 = 0;
            UserKey userKey = (UserKey) r10.A0O.get();
            for (Map.Entry entry : r10.A0N.entrySet()) {
                if (!((UserKey) entry.getKey()).equals(userKey)) {
                    if (i >= 0 && i3 >= i) {
                        break;
                    }
                    C37291v0 r8 = (C37291v0) entry.getValue();
                    if (i2 != -1) {
                        long j = (long) i2;
                        int i4 = ((r8.A01 & j) > j ? 1 : ((r8.A01 & j) == j ? 0 : -1));
                        boolean z2 = false;
                        if (i4 == 0) {
                            z2 = true;
                        }
                        if (!z2) {
                        }
                    }
                    if (r8.A0A) {
                        boolean z3 = false;
                        if ((r8.A01 & 262144) == 0) {
                            z3 = true;
                        }
                        if (z3) {
                            A002.add(entry.getKey());
                            i3++;
                        }
                    }
                }
            }
            C005505z.A00(-56480124);
            return A002;
        } finally {
            C005505z.A00(784735574);
        }
    }

    public static void A03(AnonymousClass0r6 r2) {
        for (C37291v0 A0D2 : r2.A0N.values()) {
            r2.A0D(A0D2);
        }
    }

    public static void A04(AnonymousClass0r6 r5) {
        C29761gw.A00(r5.A0E, -1);
        C29761gw.A00(r5.A0H, -1);
        synchronized (r5.A0D) {
            for (C37291v0 r2 : r5.A0N.values()) {
                if (!r5.A0D.contains(r2.A09)) {
                    r5.A0D(r2);
                }
            }
        }
    }

    public static void A05(AnonymousClass0r6 r6) {
        if (r6.A0U.isEmpty() && r6.A08) {
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, r6.A02)).AOz();
            if (r6.A01 != -1) {
                r6.A0E("android_generic_presence_interval_test", "android_generic_presence_interval_control", ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r6.A02)).now() - r6.A01);
                r6.A01 = -1;
            }
            r6.A0E("android_generic_presence_active_count_test", "android_generic_presence_active_count_control", (long) A02(r6, -1, -1).size());
            ScheduledFuture scheduledFuture = r6.A06;
            if (scheduledFuture != null) {
                scheduledFuture.cancel(false);
                r6.A06 = null;
            }
            AnonymousClass1FR r1 = (AnonymousClass1FR) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BL6, r6.A02);
            if (r1.A01 || r1.A00) {
                r6.A06 = ((ScheduledExecutorService) AnonymousClass1XX.A02(14, AnonymousClass1Y3.BKX, r6.A02)).schedule(r6.A0K, ((C25051Yd) r6.A0P.get()).At1(564075234853566L, 300), TimeUnit.SECONDS);
            }
            r6.A08 = false;
        } else if (!r6.A0U.isEmpty() && !r6.A08) {
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, r6.A02)).AOz();
            ScheduledFuture scheduledFuture2 = r6.A06;
            if (scheduledFuture2 != null) {
                scheduledFuture2.cancel(false);
                r6.A06 = null;
            }
            ScheduledFuture scheduledFuture3 = null;
            if (scheduledFuture3 != null) {
                scheduledFuture3.cancel(true);
            }
            r6.A01 = ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r6.A02)).now();
            AnonymousClass1FR r12 = (AnonymousClass1FR) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BL6, r6.A02);
            if ((r12.A01 || r12.A00) && A0I(r6, true)) {
                r6.A00 = ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r6.A02)).now();
                A09(r6, C13430rQ.TP_WAITING_FOR_FULL_LIST);
                r6.A0R();
            }
            r6.A08 = true;
        }
    }

    public static void A08(AnonymousClass0r6 r7, C05180Xy r8, UserKey userKey, C195218m r10, C82963wp r11) {
        UserKey userKey2;
        C195218m r5;
        AnonymousClass0r6 r3 = r7;
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, r7.A02)).AOz();
        Iterator it = ((Set) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AxN, r7.A02)).iterator();
        while (true) {
            userKey2 = userKey;
            r5 = r10;
            if (!it.hasNext()) {
                break;
            }
            ((AnonymousClass4F1) it.next()).onStateChanged(userKey, r10);
        }
        if (((C75903kd) AnonymousClass1XX.A02(15, AnonymousClass1Y3.BA9, r7.A02)).A01()) {
            AnonymousClass07A.A04((AnonymousClass0VL) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZx, r3.A02), new C76013ko(r3, userKey2, r5, ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r7.A02)).now()), 233226875);
        }
        if (r8 != null) {
            boolean z = false;
            for (int size = r8.size() - 1; size >= 0; size--) {
                if (!((AnonymousClass23L) r8.A00.A07(size)).A00(userKey, r10)) {
                    r8.A00.A08(size);
                    z = true;
                }
            }
            if (!z) {
                return;
            }
            if (r11 == null) {
                synchronized (r3.A0T) {
                    try {
                        r3.A0T.C2O(userKey, r8);
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
                return;
            }
            synchronized (r3.A0J) {
                try {
                    r3.A0J.C2O(r11.A00, r8);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public static void A09(AnonymousClass0r6 r1, C13430rQ r2) {
        r1.A0I.A05 = r2;
        r1.A04 = r2;
    }

    public static void A0B(AnonymousClass0r6 r3, String str) {
        if (!Objects.equal(str, "/t_p") || !((AnonymousClass1FR) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BL6, r3.A02)).A01) {
            A03(r3);
        } else {
            A04(r3);
        }
        A0C(r3, true);
    }

    public static void A0C(AnonymousClass0r6 r4, boolean z) {
        C05180Xy r3;
        try {
            C005505z.A03("PresenceManager:notifyListeners", 2136170711);
            if (z) {
                synchronized (r4.A0T) {
                    r3 = new C05180Xy(r4.A0T.BHh());
                }
                int size = r3.size();
                for (int i = 0; i < size; i++) {
                    A0A(r4, (UserKey) r3.A00.A07(i));
                }
            }
            for (AnonymousClass12A A012 : r4.A0M.keySet()) {
                A012.A01();
            }
            C005505z.A00(-688765812);
        } catch (Throwable th) {
            C005505z.A00(838891238);
            throw th;
        }
    }

    private void A0D(C37291v0 r6) {
        boolean z = r6.A0A;
        r6.A0A = false;
        r6.A0C = false;
        r6.A00 = 0;
        r6.A03 = -1;
        r6.A05 = 0;
        if (z) {
            for (AnonymousClass4F1 onStateChanged : (Set) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AxN, this.A02)) {
                onStateChanged.onStateChanged(r6.A09, new C195218m(new AnonymousClass1H2()));
            }
        }
    }

    private void A0E(String str, String str2, long j) {
        int i = AnonymousClass1Y3.BL6;
        AnonymousClass0UN r3 = this.A02;
        AnonymousClass1FR r2 = (AnonymousClass1FR) AnonymousClass1XX.A02(12, i, r3);
        if (r2.A01 || r2.A00) {
            ((C07380dK) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BQ3, r3)).A04(str, j, "counters");
        } else {
            ((C07380dK) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BQ3, r3)).A04(str2, j, "counters");
        }
    }

    public static boolean A0F(AnonymousClass0r6 r3) {
        long j;
        C25051Yd r0;
        int i = AnonymousClass1Y3.BAo;
        AnonymousClass0UN r2 = r3.A02;
        if (!((Boolean) AnonymousClass1XX.A02(22, i, r2)).booleanValue()) {
            switch (((C001500z) AnonymousClass1XX.A03(AnonymousClass1Y3.BH4, r2)).ordinal()) {
                case 0:
                    r0 = (C25051Yd) r3.A0P.get();
                    j = 285855843620764L;
                    return r0.Aem(j);
                case 1:
                    r0 = (C25051Yd) r3.A0P.get();
                    j = 285855843555227L;
                    return r0.Aem(j);
            }
        }
        return false;
    }

    public static boolean A0H(AnonymousClass0r6 r2, UserKey userKey, Set set, UserKey userKey2) {
        if (!((C25051Yd) r2.A0P.get()).Aem(284167921340327L)) {
            return A0K(userKey, set, userKey2);
        }
        if (userKey.type != C25651aB.A03 || set.contains(userKey.id)) {
            return false;
        }
        return true;
    }

    public static boolean A0K(UserKey userKey, Set set, UserKey userKey2) {
        if (userKey.type != C25651aB.A03 || set.contains(userKey.id) || userKey.equals(userKey2)) {
            return false;
        }
        return true;
    }

    public C195218m A0L(UserKey userKey, int i) {
        Integer num;
        int i2;
        if (userKey == null) {
            C010708t.A0K("DefaultPresenceManager", "userKey is null");
            return C195218m.A0A;
        }
        C37291v0 r3 = (C37291v0) this.A0N.get(userKey);
        if (r3 == null) {
            return C195218m.A0A;
        }
        if (!A0J(userKey) || (!r3.A0A && (i <= 0 || !A0b(userKey, i)))) {
            num = AnonymousClass07B.A01;
            i2 = 0;
        } else {
            num = AnonymousClass07B.A00;
            i2 = r3.A00;
        }
        AnonymousClass1H2 r2 = new AnonymousClass1H2();
        r2.A07 = num;
        r2.A08 = r3.A0B;
        r2.A04 = r3.A06;
        r2.A09 = r3.A0C;
        r2.A06 = r3.A08;
        r2.A00 = i2;
        r2.A03 = r3.A05;
        r2.A01 = r3.A01;
        r2.A02 = r3.A02;
        r2.A05 = r3.A07;
        return new C195218m(r2);
    }

    public LastActive A0M(UserKey userKey) {
        AnonymousClass09P r7;
        String formatStrLocaleSafe;
        C37291v0 r0 = (C37291v0) this.A0N.get(userKey);
        if (r0 != null) {
            long j = r0.A03;
            if (j > 0) {
                if (j > 9223372036854775L) {
                    r7 = (AnonymousClass09P) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Amr, this.A02);
                    formatStrLocaleSafe = StringFormatUtil.formatStrLocaleSafe("getLastActiveForUser invalid last active (overflow): %d seconds for user %s", Long.valueOf(j), userKey.id);
                } else {
                    long j2 = 1000 * j;
                    if (Math.abs(j2 - ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, this.A02)).now()) <= 15552000000L) {
                        return new LastActive(j2);
                    }
                    int i = AnonymousClass1Y3.Amr;
                    AnonymousClass0UN r1 = this.A02;
                    r7 = (AnonymousClass09P) AnonymousClass1XX.A02(8, i, r1);
                    formatStrLocaleSafe = StringFormatUtil.formatStrLocaleSafe("getLastActiveForUser stale last active: %d seconds for user %s, now(): %d", Long.valueOf(j), userKey.id, Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r1)).now()));
                }
                r7.CGS("PresenceManagerError", formatStrLocaleSafe);
                return null;
            }
        }
        return null;
    }

    public String A0N() {
        if (this.A07) {
            this.A0I.A01 = A02(this, -1, -1).size();
        }
        return this.A0I.toString();
    }

    public String A0O(LastActive lastActive) {
        return ((C74053hC) AnonymousClass1XX.A02(19, AnonymousClass1Y3.ALV, this.A02)).A0D(AnonymousClass07B.A0J, lastActive.A00);
    }

    public void A0S(AnonymousClass12A r3) {
        this.A0M.put(r3, true);
    }

    public void A0T(AnonymousClass12A r2) {
        this.A0M.remove(r2);
    }

    public void A0V(UserKey userKey, AnonymousClass23L r4) {
        synchronized (this.A0T) {
            this.A0T.remove(userKey, r4);
        }
    }

    public void A0W(Object obj) {
        AnonymousClass1Y6 r1 = (AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, this.A02);
        if (!r1.BHM()) {
            r1.C4C(new C404321o(this, obj));
            return;
        }
        this.A0U.add(obj);
        A05(this);
    }

    public void A0X(Object obj) {
        AnonymousClass1Y6 r1 = (AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, this.A02);
        if (!r1.BHM()) {
            r1.C4C(new AnonymousClass2OH(this, obj));
            return;
        }
        this.A0U.remove(obj);
        A05(this);
    }

    public boolean A0Y() {
        return ((Boolean) this.A0V.get()).booleanValue();
    }

    public boolean A0a(UserKey userKey) {
        C195218m A0L2;
        if (!((C25051Yd) this.A0P.get()).Aem(281496451547143L) || (A0L2 = A0L(userKey, -1)) == C195218m.A0A || A0L2.A07 != AnonymousClass07B.A00 || (A0L2.A00 & ((long) (1 << C33911oL.A04.ordinal()))) == 0) {
            return false;
        }
        return true;
    }

    public void onAppActive() {
        AnonymousClass07A.A04((ScheduledExecutorService) AnonymousClass1XX.A02(14, AnonymousClass1Y3.BKX, this.A02), new DQC(this), -1135946972);
    }

    public void onAppPaused() {
        AnonymousClass07A.A04((ScheduledExecutorService) AnonymousClass1XX.A02(14, AnonymousClass1Y3.BKX, this.A02), new DQB(this), 180799122);
    }

    public void onAppStopped() {
        AnonymousClass07A.A04((ScheduledExecutorService) AnonymousClass1XX.A02(14, AnonymousClass1Y3.BKX, this.A02), new DQD(this), -1460431133);
    }

    private AnonymousClass0r6(AnonymousClass1XY r6) {
        this.A02 = new AnonymousClass0UN(24, r6);
        this.A0Q = AnonymousClass0VB.A00(AnonymousClass1Y3.BPb, r6);
        this.A0V = AnonymousClass0VG.A00(AnonymousClass1Y3.B66, r6);
        this.A0P = AnonymousClass0VG.A00(AnonymousClass1Y3.AOJ, r6);
        AnonymousClass0VG.A00(AnonymousClass1Y3.AcD, r6);
        this.A0O = AnonymousClass0XJ.A0I(r6);
        this.A0T = new HashMultimap();
        this.A0J = new HashMultimap();
        this.A0M = AnonymousClass0TG.A07();
        this.A0N = AnonymousClass0TG.A07();
        this.A0L = Collections.synchronizedMap(new HashMap());
        this.A0I = new AnonymousClass0t3();
        this.A0G = new C29761gw(10);
        this.A0F = new C29761gw(((C25051Yd) this.A0P.get()).AqL(563912026423918L, 10));
        this.A0E = new C29761gw(20);
        this.A0H = new C29761gw(25);
        this.A0D = new C05180Xy();
        A09(this, C13430rQ.TP_DISABLED);
        C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AKb, this.A02)).BMm();
        BMm.A02(AnonymousClass80H.$const$string(AnonymousClass1Y3.A12), new C17220yW(this));
        BMm.A02(C99084oO.$const$string(433), new C17230yX(this));
        BMm.A02(C99084oO.$const$string(47), new C30211hg(this));
        BMm.A02(TurboLoader.Locator.$const$string(2), new C17240yZ(this));
        BMm.A02("com.facebook.presence.ACTION_PUSH_RECEIVED", new C30221hh(this));
        BMm.A02("com.facebook.presence.ACTION_OTHER_USER_TYPING_CHANGED", new C17250yb(this));
        BMm.A02("com.facebook.presence.ACTION_PRESENCE_RECEIVED", new C17260yc(this));
        BMm.A02("com.facebook.presence.ACTION_PUSH_STATE_RECEIVED", new C14860uG(this));
        this.A0R = BMm.A00();
        this.A0S = new C17210yV(this);
        this.A03 = C13430rQ.MQTT_DISCONNECTED;
        this.A07 = ((C25051Yd) this.A0P.get()).Aem(286439958911803L);
        int i = AnonymousClass1Y3.BAo;
        AnonymousClass0UN r2 = this.A02;
        if (((Boolean) AnonymousClass1XX.A02(22, i, r2)).booleanValue()) {
            ((C72243dw) AnonymousClass1XX.A02(21, AnonymousClass1Y3.AL1, r2)).subscribe(new AnonymousClass25I());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0079, code lost:
        if (A0L(r4, -1).A07 == X.AnonymousClass07B.A00) goto L_0x007b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Collection A0P(int r13) {
        /*
            r12 = this;
            boolean r1 = r12.A0Y()
            r0 = 0
            if (r1 == 0) goto L_0x0008
            r0 = 1
        L_0x0008:
            if (r0 != 0) goto L_0x000f
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        L_0x000f:
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            X.0Tq r0 = r12.A0O
            java.lang.Object r5 = r0.get()
            com.facebook.user.model.UserKey r5 = (com.facebook.user.model.UserKey) r5
            java.util.concurrent.ConcurrentMap r0 = r12.A0N
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r11 = r0.iterator()
        L_0x0026:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x0082
            java.lang.Object r1 = r11.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r0 = r1.getKey()
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x0026
            java.lang.Object r4 = r1.getKey()
            com.facebook.user.model.UserKey r4 = (com.facebook.user.model.UserKey) r4
            com.facebook.user.model.LastActive r3 = r12.A0M(r4)
            if (r3 == 0) goto L_0x0026
            r2 = 7
            int r1 = X.AnonymousClass1Y3.AgK
            X.0UN r0 = r12.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.06B r0 = (X.AnonymousClass06B) r0
            long r9 = r0.now()
            long r0 = r3.A00
            long r9 = r9 - r0
            r0 = 1000(0x3e8, double:4.94E-321)
            long r7 = r9 / r0
            r1 = 60
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x007b
            long r2 = (long) r13
            r0 = 60000(0xea60, double:2.9644E-319)
            long r2 = r2 * r0
            int r0 = (r9 > r2 ? 1 : (r9 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x007b
            r0 = -1
            X.18m r0 = r12.A0L(r4, r0)
            java.lang.Integer r2 = r0.A07
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 1
            if (r2 != r1) goto L_0x007c
        L_0x007b:
            r0 = 0
        L_0x007c:
            if (r0 == 0) goto L_0x0026
            r6.add(r4)
            goto L_0x0026
        L_0x0082:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0r6.A0P(int):java.util.Collection");
    }

    public void A0U(UserKey userKey, AnonymousClass23L r4) {
        Preconditions.checkNotNull(userKey);
        Preconditions.checkNotNull(r4);
        synchronized (this.A0T) {
            this.A0T.Byx(userKey, r4);
        }
    }

    public boolean A0b(UserKey userKey, int i) {
        if (!A0J(userKey)) {
            return false;
        }
        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, this.A02)).now() - TimeUnit.MINUTES.toMillis((long) i);
        LastActive A0M2 = A0M(userKey);
        if (A0M2 == null || A0M2.A00 < now) {
            return false;
        }
        return true;
    }

    public boolean A0c(Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            if (A0Z((UserKey) it.next())) {
                return true;
            }
        }
        return false;
    }

    public void init() {
        int A032 = C000700l.A03(2125571469);
        try {
            C005505z.A03("PresenceManager:init", 574549625);
            this.A0R.A00();
            if (((C25051Yd) this.A0P.get()).Aem(287333312109977L)) {
                ((FbSharedPreferences) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B6q, this.A02)).C0h(ImmutableSet.A04(C34341pQ.A01), this.A0S);
            }
            ((FbSharedPreferences) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B6q, this.A02)).C0h(ImmutableSet.A04(C34341pQ.A00), this.A0S);
        } finally {
            C005505z.A00(-175099839);
            C000700l.A09(-1522944607, A032);
        }
    }
}
