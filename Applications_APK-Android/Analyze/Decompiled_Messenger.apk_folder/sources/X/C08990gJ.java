package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.0gJ  reason: invalid class name and case insensitive filesystem */
public final class C08990gJ extends Handler {
    private final QuickPerformanceLogger A00;
    private final C27021cW A01;

    public void handleMessage(Message message) {
        boolean A02;
        int i = message.what;
        if (i == 0) {
            this.A00.markerStart(5505205);
            C27021cW r1 = this.A01;
            if (r1.A07 && (!C004004z.A02(5505205) || !C004004z.A03("frames"))) {
                r1.A02 = true;
                synchronized (r1) {
                    A02 = C004004z.A02(5505205);
                }
                if (A02) {
                    r1.A01 = false;
                    if (!r1.A03) {
                        r1.A06.A02();
                        r1.A03 = true;
                        if (r1.A00 == null) {
                            C30210Erb erb = new C30210Erb(r1, 5505205);
                            r1.A00 = erb;
                            r1.A05.BxS(erb, (long) r1.A04);
                        }
                    }
                }
            }
            this.A00.markerAnnotate(5505205, "used_draw_listener", message.arg1);
            this.A00.markerAnnotate(5505205, "startup_kind", message.arg2);
            sendMessageDelayed(obtainMessage(1), 30000);
        } else if (i == 1) {
            this.A01.A02(5505205);
            this.A00.markerEnd(5505205, 2);
        }
    }

    public C08990gJ(QuickPerformanceLogger quickPerformanceLogger, C27021cW r3) {
        super(Looper.getMainLooper());
        this.A00 = quickPerformanceLogger;
        this.A01 = r3;
    }
}
