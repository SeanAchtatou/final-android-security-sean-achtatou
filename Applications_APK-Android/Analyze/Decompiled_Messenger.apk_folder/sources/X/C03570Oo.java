package X;

import android.view.animation.Interpolator;

/* renamed from: X.0Oo  reason: invalid class name and case insensitive filesystem */
public final class C03570Oo implements Interpolator {
    public float getInterpolation(float f) {
        return ((double) f) <= 0.5d ? f * 2.0f : (1.0f - f) * 2.0f;
    }
}
