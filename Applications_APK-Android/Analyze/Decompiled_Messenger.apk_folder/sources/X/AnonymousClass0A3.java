package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0A3  reason: invalid class name */
public final class AnonymousClass0A3 {
    public static boolean A00(Comparable comparable, Comparable comparable2) {
        Preconditions.checkNotNull(comparable);
        Preconditions.checkNotNull(comparable2);
        if (comparable.compareTo(comparable2) >= 0) {
            return true;
        }
        return false;
    }
}
