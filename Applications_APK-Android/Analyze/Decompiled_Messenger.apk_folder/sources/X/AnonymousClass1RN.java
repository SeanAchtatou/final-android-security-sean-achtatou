package X;

/* renamed from: X.1RN  reason: invalid class name */
public final class AnonymousClass1RN implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.datasource.AbstractDataSource$1";
    public final /* synthetic */ AnonymousClass1QN A00;
    public final /* synthetic */ C23511Pu A01;
    public final /* synthetic */ boolean A02;
    public final /* synthetic */ boolean A03;

    public AnonymousClass1RN(AnonymousClass1QN r1, boolean z, C23511Pu r3, boolean z2) {
        this.A00 = r1;
        this.A03 = z;
        this.A01 = r3;
        this.A02 = z2;
    }

    public void run() {
        if (this.A03) {
            this.A01.BYZ(this.A00);
        } else if (this.A02) {
            this.A01.BRF(this.A00);
        } else {
            this.A01.Bga(this.A00);
        }
    }
}
