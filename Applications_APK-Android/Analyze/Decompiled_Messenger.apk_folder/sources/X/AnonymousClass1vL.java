package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1vL  reason: invalid class name */
public final class AnonymousClass1vL extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public MigColorScheme A01;

    public AnonymousClass1vL(Context context) {
        super("M4SwipeNotificationsOffButton");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
