package X;

import java.util.Comparator;

/* renamed from: X.0c0  reason: invalid class name and case insensitive filesystem */
public final class C06740c0 implements Comparator {
    public int compare(Object obj, Object obj2) {
        C06750c1 r3 = (C06750c1) obj;
        C06750c1 r4 = (C06750c1) obj2;
        int i = r3.A00;
        int i2 = r4.A00;
        if (i > i2) {
            return -1;
        }
        if (i < i2) {
            return 1;
        }
        return Integer.valueOf(r3.hashCode()).compareTo(Integer.valueOf(r4.hashCode()));
    }
}
