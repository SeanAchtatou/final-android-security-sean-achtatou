package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.16k  reason: invalid class name and case insensitive filesystem */
public final class C190016k {
    private static volatile C190016k A01;
    public AnonymousClass1YI A00;

    public static final C190016k A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C190016k.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C190016k(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C190016k(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WA.A00(r2);
    }
}
