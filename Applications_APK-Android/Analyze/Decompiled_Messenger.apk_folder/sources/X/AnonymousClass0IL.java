package X;

import org.json.JSONException;

/* renamed from: X.0IL  reason: invalid class name */
public final class AnonymousClass0IL implements C02780Gi {
    public void C2x(AnonymousClass0FM r4, C02910Gx r5) {
        try {
            r5.AMW("healthstats", ((AnonymousClass0KI) r4).A09().toString());
        } catch (JSONException e) {
            AnonymousClass0KZ.A00("HealthStatsMetricsReporter", "Couldn't log healthstats metrics", e);
        }
    }
}
