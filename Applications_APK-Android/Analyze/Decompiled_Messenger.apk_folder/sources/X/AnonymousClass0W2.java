package X;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.google.common.base.Preconditions;

/* renamed from: X.0W2  reason: invalid class name */
public abstract class AnonymousClass0W2 {
    public final int A00;
    public final String A01;

    public void A04(SQLiteDatabase sQLiteDatabase) {
        if (this instanceof AnonymousClass0W1) {
            AnonymousClass0W1 r3 = (AnonymousClass0W1) this;
            if (!(r3 instanceof C11310mh)) {
                C24971Xv it = r3.A00.iterator();
                while (it.hasNext()) {
                    ((AnonymousClass0W4) it.next()).A09(sQLiteDatabase);
                }
                return;
            }
            AnonymousClass0r8.A05(sQLiteDatabase, "sqliteproc_metadata", BJJ.A00, BJJ.A01);
            AnonymousClass0r8.A05(sQLiteDatabase, "sqliteproc_schema", BKB.A00, BKB.A01);
            C11310mh.A03((C11310mh) r3, sQLiteDatabase, true);
        } else if (!(this instanceof AnonymousClass17L)) {
            C007406x.A00(-1344629641);
            sQLiteDatabase.execSQL("CREATE TABLE contacts_upload_snapshot (local_contact_id INTEGER PRIMARY KEY, contact_hash TEXT, contact_extra_fields_hash TEXT);");
            C007406x.A00(-211676562);
        } else {
            C007406x.A00(1065247146);
            sQLiteDatabase.execSQL("CREATE TABLE phone_address_book_snapshot (local_contact_id INTEGER PRIMARY KEY, contact_hash TEXT)");
            C007406x.A00(1736731148);
        }
    }

    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.String, java.lang.String[]] */
    /* JADX WARN: Type inference failed for: r1v5 */
    public void A05(SQLiteDatabase sQLiteDatabase) {
        ? r1;
        int i;
        if (this instanceof AnonymousClass17L) {
            r1 = 0;
            i = 567;
        } else if (!(this instanceof AnonymousClass0W1)) {
            r1 = 0;
            i = AnonymousClass1Y3.A16;
        } else {
            AnonymousClass0W1 r12 = (AnonymousClass0W1) this;
            if (!(r12 instanceof AnonymousClass19C) && !(r12 instanceof C09530hg) && !(r12 instanceof C11310mh) && !(r12 instanceof C190216m)) {
                C24971Xv it = r12.A00.iterator();
                while (it.hasNext()) {
                    sQLiteDatabase.delete(((AnonymousClass0W4) it.next()).A00, null, null);
                }
                return;
            }
            return;
        }
        sQLiteDatabase.delete(AnonymousClass80H.$const$string(i), r1, r1);
    }

    public void A06(SQLiteDatabase sQLiteDatabase, Context context) {
        if (this instanceof AnonymousClass0W1) {
            C24971Xv it = ((AnonymousClass0W1) this).A00.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }

    public void A07(SQLiteDatabase sQLiteDatabase) {
    }

    public abstract void A08(SQLiteDatabase sQLiteDatabase, int i, int i2);

    public AnonymousClass0W2(String str, int i) {
        Preconditions.checkArgument(i < 1 ? false : true, "Version must be positive");
        this.A01 = str;
        this.A00 = i;
    }
}
