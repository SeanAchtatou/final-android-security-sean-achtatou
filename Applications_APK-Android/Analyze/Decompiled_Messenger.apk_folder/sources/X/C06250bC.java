package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0bC  reason: invalid class name and case insensitive filesystem */
public final class C06250bC extends C06260bD {
    private static volatile C06250bC A02;
    public AnonymousClass0UN A00;
    public final C04310Tq A01;

    public static final C06250bC A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C06250bC.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C06250bC(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C06250bC(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = AnonymousClass0WY.A02(r3);
        C06280bF.A00(r3);
    }
}
