package X;

/* renamed from: X.0vM  reason: invalid class name and case insensitive filesystem */
public final class C15480vM {
    private AnonymousClass0UN A00;
    private boolean A01;
    private boolean A02;
    public final C17610zB A03 = new C17610zB();
    public final AnonymousClass069 A04;
    public final C19881Ab A05;
    private final AnonymousClass1AT A06;
    private final C20001An A07;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r8.A02 == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A00() {
        /*
            r8 = this;
            boolean r0 = r8.A01
            if (r0 == 0) goto L_0x0009
            boolean r0 = r8.A02
            r7 = 1
            if (r0 != 0) goto L_0x000a
        L_0x0009:
            r7 = 0
        L_0x000a:
            if (r7 == 0) goto L_0x0030
            X.069 r0 = r8.A04
            long r3 = r0.now()
            X.0zB r6 = r8.A03
            int r5 = r6.A01()
            r2 = 0
        L_0x0019:
            if (r2 >= r5) goto L_0x0033
            long r0 = r6.A04(r2)
            java.lang.Object r1 = r6.A07(r0)
            X.1Sr r1 = (X.C24201Sr) r1
            boolean r0 = r1.A08
            if (r0 == 0) goto L_0x002d
            r1.A04 = r3
            r1.A03 = r3
        L_0x002d:
            int r2 = r2 + 1
            goto L_0x0019
        L_0x0030:
            A01(r8)
        L_0x0033:
            X.1AT r1 = r8.A06
            if (r1 == 0) goto L_0x003c
            X.0zB r0 = r8.A03
            r1.CKX(r0, r7)
        L_0x003c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15480vM.A00():void");
    }

    public static void A01(C15480vM r11) {
        long now = r11.A04.now();
        C17610zB r112 = r11.A03;
        int A012 = r112.A01();
        for (int i = 0; i < A012; i++) {
            C24201Sr r8 = (C24201Sr) r112.A06(i);
            if (r8.A08) {
                r8.A02 += now - r8.A04;
                r8.A04 = now;
            }
        }
    }

    public void A02(C33941oO r7) {
        long now = this.A04.now();
        long ArZ = r7.ArZ();
        C24201Sr r3 = (C24201Sr) this.A03.A07(ArZ);
        if (r3 == null) {
            r3 = (C24201Sr) C24191Sq.A00.A01();
            this.A03.A0D(ArZ, r3);
        }
        r3.A05 = r7;
        if (!r3.A08) {
            r3.A08 = true;
            r3.A04 = now;
        }
        r3.A03 = now;
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A5f, false)) {
            r3.A07.add(Long.valueOf(AnonymousClass06A.A00.now()));
        }
        AnonymousClass1AT r0 = this.A06;
        if (r0 != null) {
            r0.BMG(r3);
        }
        this.A07.A01();
    }

    public void A03(C33941oO r11) {
        C24201Sr r3 = (C24201Sr) this.A03.A07(r11.ArZ());
        if (r3 != null) {
            r3.A05 = r11;
            long now = this.A04.now();
            if (r3.A08) {
                r3.A08 = false;
                r3.A02 += now - r3.A04;
            }
            r3.A03 = now;
            if (((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A5f, false)) {
                r3.A06.add(Long.valueOf(AnonymousClass06A.A00.now()));
            }
            AnonymousClass1AT r0 = this.A06;
            if (r0 != null) {
                r0.BMH(r3);
            }
            this.A07.A01();
        }
    }

    public void A04(boolean z) {
        this.A01 = z;
        A00();
        C20001An r0 = this.A07;
        r0.A02 = z;
        r0.A01();
    }

    public void A05(boolean z) {
        this.A02 = z;
        AnonymousClass1AT r0 = this.A06;
        if (r0 != null) {
            r0.C8h(z);
        }
        A00();
    }

    public C15480vM(AnonymousClass1XY r3, C19991Am r4, AnonymousClass1AT r5, C19881Ab r6) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A04 = AnonymousClass067.A03(r3);
        this.A05 = r6;
        this.A06 = r5;
        this.A07 = new C20001An(r4, new C19981Al(this));
    }
}
