package X;

import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType;
import com.facebook.graphql.enums.GraphQLMessengerRetailItemMediaTag;
import com.facebook.graphql.enums.GraphQLMontageDirectState;
import com.facebook.graphql.enums.GraphQLStoryAttachmentStyle;
import com.facebook.graphql.enums.GraphQLXMALayoutType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.model.attachment.Attachment;
import com.facebook.messaging.model.attachment.VideoData;
import com.facebook.messaging.model.messagemetadata.MessagePersonaPlatformMetadata;
import com.facebook.messaging.model.messages.GenericAdminMessageExtensibleData;
import com.facebook.messaging.model.messages.GenericAdminMessageInfo;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessengerCallLogProperties;
import com.facebook.messaging.model.messages.MessengerPageThreadActionSystemAddDetailsProperty;
import com.facebook.messaging.model.messages.MontageBrandedCameraAttributionData;
import com.facebook.messaging.model.share.SentShareAttachment;
import com.facebook.messaging.model.share.Share;
import com.facebook.messaging.model.share.ShareMedia;
import com.facebook.messaging.model.share.brandedcamera.SentBrandedCameraShare;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.facebook.ui.media.attachments.model.MediaUploadConfig;
import com.facebook.user.model.UserKey;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableList;
import java.util.Collections;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1fS  reason: invalid class name and case insensitive filesystem */
public final class C28841fS {
    public static final ImmutableList A05 = ImmutableList.of("PAYMENT_RECEIVED", AnonymousClass24B.$const$string(385), AnonymousClass24B.$const$string(650));
    public static final ImmutableSet A06 = ImmutableSet.A06(AnonymousClass1V7.A0K, AnonymousClass1V7.A0J, AnonymousClass1V7.A0A);
    private static volatile C28841fS A07;
    public AnonymousClass0UN A00;
    public final AnonymousClass1YI A01;
    public final C13750s0 A02;
    private final C04310Tq A03;
    private final C04310Tq A04;

    public static boolean A0E(Message message) {
        MediaUploadConfig mediaUploadConfig;
        return message != null && !message.A0b.isEmpty() && (mediaUploadConfig = ((MediaResource) message.A0b.get(0)).A0N) != null && "animated_sticker".equals(mediaUploadConfig.A06);
    }

    public static boolean A0H(Message message) {
        return message != null && !message.A0b.isEmpty() && ((MediaResource) message.A0b.get(0)).A04();
    }

    public static boolean A0u(Message message) {
        return message != null && C06850cB.A0D(message.A0y, new String[]{AnonymousClass24B.$const$string(83), AnonymousClass24B.$const$string(82), AnonymousClass24B.$const$string(283)});
    }

    public static boolean A0v(Message message) {
        C61192yU r0;
        C100354qi B46;
        ImmutableList B4T;
        return (message == null || (r0 = message.A06) == null || (B46 = r0.B46()) == null || (B4T = B46.B4T()) == null || B4T.isEmpty() || !GraphQLStoryAttachmentStyle.A49.equals(B4T.get(0))) ? false : true;
    }

    public static boolean A0w(Message message) {
        return message != null && C013509w.A01(message.A0c) && C013509w.A01(((Share) message.A0c.get(0)).A02) && ShareMedia.Type.LINK.equals(((ShareMedia) ((Share) message.A0c.get(0)).A02.get(0)).A00) && !C06850cB.A0B(message.A10);
    }

    public static boolean A0x(Message message) {
        C61192yU r0;
        C100354qi B46;
        ImmutableList B4T;
        return (message == null || (r0 = message.A06) == null || (B46 = r0.B46()) == null || (B4T = B46.B4T()) == null || B4T.isEmpty() || !GraphQLStoryAttachmentStyle.A4B.equals(B4T.get(0))) ? false : true;
    }

    public static boolean A1C(Message message) {
        if (message != null && C013509w.A01(message.A0b)) {
            C24971Xv it = message.A0b.iterator();
            while (it.hasNext()) {
                if (((MediaResource) it.next()).A07 > 20000) {
                    return false;
                }
            }
        }
        if (message == null || !C013509w.A01(message.A0X)) {
            return true;
        }
        C24971Xv it2 = message.A0X.iterator();
        while (it2.hasNext()) {
            VideoData videoData = ((Attachment) it2.next()).A04;
            if (videoData != null && ((long) videoData.A00) > 20) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A1F(com.facebook.messaging.model.messages.MessagesCollection r5, java.lang.String r6, long r7) {
        /*
            r4 = 0
            if (r6 != 0) goto L_0x000a
            r1 = -1
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x000a
        L_0x0009:
            return r4
        L_0x000a:
            com.google.common.collect.ImmutableList r0 = r5.A01
            X.1Xv r3 = r0.iterator()
        L_0x0010:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0009
            java.lang.Object r1 = r3.next()
            com.facebook.messaging.model.messages.Message r1 = (com.facebook.messaging.model.messages.Message) r1
            if (r6 == 0) goto L_0x0026
            java.lang.String r0 = r1.A0q
            boolean r0 = r6.equals(r0)
            if (r0 != 0) goto L_0x002c
        L_0x0026:
            long r1 = r1.A03
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0010
        L_0x002c:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28841fS.A1F(com.facebook.messaging.model.messages.MessagesCollection, java.lang.String, long):boolean");
    }

    public static long A00(Message message) {
        long j = message.A02;
        boolean z = false;
        if (j != 0) {
            z = true;
        }
        if (!z || j >= message.A03) {
            return message.A03;
        }
        return j;
    }

    public static GraphQLMontageDirectState A01(Message message) {
        C61192yU r0;
        C100354qi B46;
        C77163mm B5M;
        GraphQLMontageDirectState graphQLMontageDirectState;
        if (message == null || (r0 = message.A06) == null || (B46 = r0.B46()) == null || (B5M = B46.B5M()) == null || (graphQLMontageDirectState = (GraphQLMontageDirectState) B5M.A0O(-1214396839, GraphQLMontageDirectState.UNSET_OR_UNRECOGNIZED_ENUM_VALUE)) == null) {
            return GraphQLMontageDirectState.OPENED;
        }
        return graphQLMontageDirectState;
    }

    public static Message A03(Message message) {
        SentBrandedCameraShare sentBrandedCameraShare = message.A0T.A02;
        if (sentBrandedCameraShare == null) {
            return message;
        }
        AnonymousClass1TG A002 = Message.A00();
        A002.A03(message);
        if (message.A0E == null) {
            AnonymousClass49H r1 = new AnonymousClass49H();
            r1.A05 = sentBrandedCameraShare.A02;
            r1.A02 = sentBrandedCameraShare.A01;
            r1.A00 = sentBrandedCameraShare.A04;
            r1.A03 = sentBrandedCameraShare.A05;
            r1.A01 = sentBrandedCameraShare.A03;
            A002.A0D = new MontageBrandedCameraAttributionData(r1);
        }
        if (!A0F(message) && !A0I(message)) {
            A002.A09(Collections.singletonList(sentBrandedCameraShare.A00));
        }
        A002.A0S = null;
        return A002.A00();
    }

    public static final C28841fS A05(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C28841fS.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C28841fS(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static ImmutableList A08(Message message) {
        if (message == null) {
            return RegularImmutableList.A02;
        }
        ImmutableList A042 = message.A04();
        if (!C013509w.A01(A042)) {
            return RegularImmutableList.A02;
        }
        return A042;
    }

    public static String A0A(Message message) {
        ImmutableMap immutableMap;
        if (message == null || (immutableMap = message.A0i) == null) {
            return null;
        }
        C69333Wq r1 = C69333Wq.PERSONA;
        if (immutableMap.get(r1) == null || ((MessagePersonaPlatformMetadata) message.A0i.get(r1)).A00 == null) {
            return null;
        }
        return ((MessagePersonaPlatformMetadata) message.A0i.get(r1)).A00.A00;
    }

    public static boolean A0D(Message message) {
        C100354qi B46;
        C77163mm B5M;
        GSTModelShape1S0000000 AfR;
        ImmutableList A3O;
        GraphQLMessengerRetailItemMediaTag Ato;
        C61192yU r0 = message.A06;
        if (r0 == null || (B46 = r0.B46()) == null || (B5M = B46.B5M()) == null || B5M.AfR() == null || (AfR = B5M.AfR()) == null || (A3O = AfR.A3O()) == null || A3O.isEmpty() || (Ato = ((C77183mo) A3O.get(0)).Ato()) == null || !Ato.equals(GraphQLMessengerRetailItemMediaTag.AR)) {
            return false;
        }
        return true;
    }

    public static boolean A0F(Message message) {
        ImmutableList immutableList = message.A0X;
        if (immutableList == null || immutableList.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean A0G(Message message) {
        ImmutableMap immutableMap;
        if (message == null || (immutableMap = message.A0h) == null || immutableMap.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean A0I(Message message) {
        ImmutableList immutableList = message.A0b;
        if (immutableList == null || immutableList.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean A0J(Message message) {
        return !C06850cB.A0A(message.A10);
    }

    public static boolean A0K(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A1S) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0L(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A02) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0M(Message message) {
        return !A06.contains(message.A0D);
    }

    public static boolean A0N(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (!(message == null || (genericAdminMessageInfo = message.A09) == null)) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0U) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0O(Message message) {
        SentBrandedCameraShare sentBrandedCameraShare;
        MediaResource mediaResource;
        SentShareAttachment sentShareAttachment = message.A0T;
        if (!(sentShareAttachment == null || !C22652B5y.BRANDED_CAMERA.equals(sentShareAttachment.A00) || (sentBrandedCameraShare = sentShareAttachment.A02) == null || (mediaResource = sentBrandedCameraShare.A00) == null)) {
            boolean z = false;
            if (mediaResource.A0R.A00 == C64563Cj.A01) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0P(Message message) {
        C61192yU r0;
        C100354qi B46;
        ImmutableList B4T;
        if (message == null || (r0 = message.A06) == null || (B46 = r0.B46()) == null || (B4T = B46.B4T()) == null || !B4T.contains(GraphQLStoryAttachmentStyle.A4A)) {
            return false;
        }
        return true;
    }

    public static boolean A0Q(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (message.A0D == AnonymousClass1V7.A04 && (genericAdminMessageInfo = message.A09) != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A08) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0R(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (message.A0D == AnonymousClass1V7.A04 && (genericAdminMessageInfo = message.A09) != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0J) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0023, code lost:
        if (r0 != false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0S(com.facebook.messaging.model.messages.Message r2) {
        /*
            X.1V7 r1 = r2.A0D
            X.1V7 r0 = X.AnonymousClass1V7.A04
            if (r1 != r0) goto L_0x0029
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r0 = r2.A09
            if (r0 == 0) goto L_0x0029
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r2 = r0.A0C
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A09
            r0 = 0
            if (r2 != r1) goto L_0x0012
            r0 = 1
        L_0x0012:
            if (r0 != 0) goto L_0x0025
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A07
            r0 = 0
            if (r2 != r1) goto L_0x001a
            r0 = 1
        L_0x001a:
            if (r0 != 0) goto L_0x0025
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A08
            r0 = 0
            if (r2 != r1) goto L_0x0022
            r0 = 1
        L_0x0022:
            r1 = 0
            if (r0 == 0) goto L_0x0026
        L_0x0025:
            r1 = 1
        L_0x0026:
            r0 = 1
            if (r1 != 0) goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28841fS.A0S(com.facebook.messaging.model.messages.Message):boolean");
    }

    public static boolean A0T(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A09) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0U(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo == null) {
            return false;
        }
        GraphQLExtensibleMessageAdminTextType graphQLExtensibleMessageAdminTextType = genericAdminMessageInfo.A0C;
        boolean z = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A0r) {
            z = true;
        }
        if (z || genericAdminMessageInfo.A02()) {
            return true;
        }
        boolean z2 = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A0s) {
            z2 = true;
        }
        if (z2) {
            return true;
        }
        boolean z3 = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A0y) {
            z3 = true;
        }
        if (z3) {
            return true;
        }
        boolean z4 = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A0z) {
            z4 = true;
        }
        if (z4) {
            return true;
        }
        boolean z5 = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A0x) {
            z5 = true;
        }
        if (z5) {
            return true;
        }
        boolean z6 = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A0v) {
            z6 = true;
        }
        if (z6) {
            return true;
        }
        boolean z7 = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A0t) {
            z7 = true;
        }
        if (z7) {
            return true;
        }
        boolean z8 = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A0u) {
            z8 = true;
        }
        if (z8) {
            return true;
        }
        return false;
    }

    public static boolean A0V(Message message) {
        if (message.A0D == AnonymousClass1V7.A0A) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        if (r2 == X.AnonymousClass1V7.A0M) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0W(com.facebook.messaging.model.messages.Message r3) {
        /*
            X.1V7 r2 = r3.A0D
            X.1V7 r0 = X.AnonymousClass1V7.A0N
            if (r2 == r0) goto L_0x0021
            X.1V7 r0 = X.AnonymousClass1V7.A0L
            if (r2 == r0) goto L_0x0021
            X.1V7 r0 = X.AnonymousClass1V7.A0O
            if (r2 == r0) goto L_0x0021
            X.1V7 r0 = X.AnonymousClass1V7.A03
            if (r2 == r0) goto L_0x0017
            X.1V7 r1 = X.AnonymousClass1V7.A0M
            r0 = 0
            if (r2 != r1) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            if (r0 != 0) goto L_0x0021
            boolean r1 = A0Z(r3)
            r0 = 0
            if (r1 == 0) goto L_0x0022
        L_0x0021:
            r0 = 1
        L_0x0022:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28841fS.A0W(com.facebook.messaging.model.messages.Message):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0X(com.facebook.messaging.model.messages.Message r3) {
        /*
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r3 = r3.A09
            if (r3 == 0) goto L_0x0047
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r2 = r3.A0C
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A1Y
            r0 = 0
            if (r2 != r1) goto L_0x000c
            r0 = 1
        L_0x000c:
            if (r0 == 0) goto L_0x0047
            com.facebook.messaging.model.messages.GenericAdminMessageExtensibleData r0 = r3.A01()
            com.facebook.messaging.model.messages.MessengerCallLogProperties r0 = (com.facebook.messaging.model.messages.MessengerCallLogProperties) r0
            if (r0 == 0) goto L_0x0047
            java.lang.String r1 = r0.A05
            java.lang.String r0 = "group_call_started"
            boolean r0 = X.C06850cB.A0C(r1, r0)
        L_0x001e:
            if (r0 != 0) goto L_0x0043
            if (r3 == 0) goto L_0x0045
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r2 = r3.A0C
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A1Y
            r0 = 0
            if (r2 != r1) goto L_0x002a
            r0 = 1
        L_0x002a:
            if (r0 == 0) goto L_0x0045
            com.facebook.messaging.model.messages.GenericAdminMessageExtensibleData r0 = r3.A01()
            com.facebook.messaging.model.messages.MessengerCallLogProperties r0 = (com.facebook.messaging.model.messages.MessengerCallLogProperties) r0
            if (r0 == 0) goto L_0x0045
            java.lang.String r1 = r0.A05
            r0 = 491(0x1eb, float:6.88E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            boolean r0 = X.C06850cB.A0C(r1, r0)
        L_0x0040:
            r1 = 0
            if (r0 == 0) goto L_0x0044
        L_0x0043:
            r1 = 1
        L_0x0044:
            return r1
        L_0x0045:
            r0 = 0
            goto L_0x0040
        L_0x0047:
            r0 = 0
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28841fS.A0X(com.facebook.messaging.model.messages.Message):boolean");
    }

    public static boolean A0Y(Message message) {
        boolean z;
        GenericAdminMessageExtensibleData genericAdminMessageExtensibleData;
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            GraphQLExtensibleMessageAdminTextType graphQLExtensibleMessageAdminTextType = genericAdminMessageInfo.A0C;
            if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A3l || graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A36 || (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A1Y && (genericAdminMessageExtensibleData = genericAdminMessageInfo.A0D) != null && (genericAdminMessageExtensibleData instanceof MessengerCallLogProperties) && C06850cB.A0C(((MessengerCallLogProperties) genericAdminMessageExtensibleData).A05, "group_call_started"))) {
                z = true;
            } else {
                z = false;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0Z(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (message.A0D == AnonymousClass1V7.A04 && (genericAdminMessageInfo = message.A09) != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0Z) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0a(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (!(message == null || (genericAdminMessageInfo = message.A09) == null)) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A1h) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0b(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0W) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0c(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0e) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0d(Message message) {
        C100354qi B46;
        ImmutableList B4T;
        C61192yU r0 = message.A06;
        if (r0 == null || (B46 = r0.B46()) == null || (B4T = B46.B4T()) == null || !B4T.contains(GraphQLStoryAttachmentStyle.A3C)) {
            return false;
        }
        return true;
    }

    public static boolean A0e(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0r) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0f(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0s) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0g(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0v) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0h(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A10) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0i(Message message) {
        if (message == null || !message.A0g.containsKey("is_mentorship") || message.A10 == null) {
            return false;
        }
        return true;
    }

    public static boolean A0j(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A1P) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0k(Message message) {
        Integer num = message.A0l;
        if (num == null || num.equals(Integer.valueOf(C69503Xn.A0E.getValue()))) {
            return false;
        }
        return true;
    }

    public static boolean A0l(Message message) {
        String str = message.A0z;
        if (!C72043dZ.A03(str) || Objects.equal(str, "227878347358915")) {
            return false;
        }
        return true;
    }

    public static boolean A0m(Message message) {
        if (message.A0z != null) {
            return true;
        }
        return false;
    }

    public static boolean A0n(Message message) {
        C100354qi B46;
        ImmutableList B4T;
        C61192yU r0 = message.A06;
        if (r0 == null || (B46 = r0.B46()) == null || (B4T = B46.B4T()) == null || !B4T.contains(GraphQLStoryAttachmentStyle.A2i)) {
            return false;
        }
        return true;
    }

    public static boolean A0o(Message message) {
        C100354qi B46;
        ImmutableList B4T;
        C61192yU r0 = message.A06;
        if (r0 == null || (B46 = r0.B46()) == null || (B4T = B46.B4T()) == null) {
            return false;
        }
        if (B4T.contains(GraphQLStoryAttachmentStyle.A3g) || B4T.contains(GraphQLStoryAttachmentStyle.A3f)) {
            return true;
        }
        return false;
    }

    public static boolean A0p(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A1n) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0q(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A23) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0r(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A24) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0s(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A22) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0t(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A25) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0y(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (message.A0D == AnonymousClass1V7.A04 && (genericAdminMessageInfo = message.A09) != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A3A) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A0z(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (!(message == null || (genericAdminMessageInfo = message.A09) == null)) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A2m) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A10(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (!(message == null || (genericAdminMessageInfo = message.A09) == null)) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A2h) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A11(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (!(message == null || (genericAdminMessageInfo = message.A09) == null)) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A31) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A12(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A2R) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A13(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A2T) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A14(Message message) {
        if (message.A0D == AnonymousClass1V7.A0J) {
            return true;
        }
        return false;
    }

    public static boolean A15(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A0W) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A16(Message message) {
        AnonymousClass1V7 r2 = message.A0D;
        if (r2 == AnonymousClass1V7.A0F || r2 == AnonymousClass1V7.A0C || r2 == AnonymousClass1V7.A0D || r2 == AnonymousClass1V7.A0T || r2 == AnonymousClass1V7.A0E || r2 == AnonymousClass1V7.A07) {
            return true;
        }
        return false;
    }

    public static boolean A17(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (message.A0D == AnonymousClass1V7.A04 && (genericAdminMessageInfo = message.A09) != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A3M) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0008, code lost:
        if (r1 == null) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A18(com.facebook.messaging.model.messages.Message r3) {
        /*
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r3.A0K
            r2 = 1
            if (r0 == 0) goto L_0x000a
            com.facebook.user.model.UserKey r1 = r0.A01
            r0 = 1
            if (r1 != 0) goto L_0x000b
        L_0x000a:
            r0 = 0
        L_0x000b:
            if (r0 == 0) goto L_0x0016
            java.lang.String r0 = r3.A0s
            boolean r0 = X.C06850cB.A0B(r0)
            if (r0 != 0) goto L_0x0016
            return r2
        L_0x0016:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28841fS.A18(com.facebook.messaging.model.messages.Message):boolean");
    }

    public static boolean A19(Message message) {
        if (message == null || !message.A0g.containsKey("message") || !((String) message.A0g.get("message")).equals("save")) {
            return false;
        }
        return true;
    }

    public static boolean A1A(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A3d) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A1B(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo = message.A09;
        if (genericAdminMessageInfo != null) {
            boolean z = false;
            if (genericAdminMessageInfo.A0C == GraphQLExtensibleMessageAdminTextType.A3e) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A1D(Message message, Message message2) {
        String str = message.A0q;
        if (str != null && str.equals(message2.A0q)) {
            return true;
        }
        String str2 = message.A0w;
        if (str2 == null || !str2.equals(message2.A0w)) {
            return false;
        }
        return true;
    }

    public static boolean A1E(Message message, Message message2) {
        if (!Objects.equal(message.A0q, message2.A0q) || !A0k(message) || !A0k(message2) || !message.A14 || message2.A14) {
            return false;
        }
        return true;
    }

    public UserKey A1G() {
        return (UserKey) this.A03.get();
    }

    public boolean A1H(Message message) {
        if (message.A0K == null || this.A04.get() == null || !Objects.equal(message.A0K.A01.id, ((ViewerContext) this.A04.get()).mUserId)) {
            return false;
        }
        return true;
    }

    public boolean A1I(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (!this.A01.AbO(765, false) || message == null || (genericAdminMessageInfo = message.A09) == null) {
            return false;
        }
        GraphQLExtensibleMessageAdminTextType graphQLExtensibleMessageAdminTextType = genericAdminMessageInfo.A0C;
        boolean z = false;
        if (graphQLExtensibleMessageAdminTextType == GraphQLExtensibleMessageAdminTextType.A2l) {
            z = true;
        }
        if (z) {
            return true;
        }
        return false;
    }

    public boolean A1K(Message message) {
        C100354qi B46;
        C77163mm B5M;
        ImmutableList Apt;
        C61192yU r0 = message.A06;
        if (r0 == null || (B46 = r0.B46()) == null) {
            return false;
        }
        boolean z = false;
        if (this.A01.AbO(759, false) && B46.B4T().contains(GraphQLStoryAttachmentStyle.A4u) && (B5M = B46.B5M()) != null && (Apt = B5M.Apt()) != null && !Apt.isEmpty()) {
            z = true;
        }
        if (z || B46.B4T().contains(GraphQLStoryAttachmentStyle.A4t) || A0C(B46)) {
            return true;
        }
        return false;
    }

    private C28841fS(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A03 = AnonymousClass0XJ.A0I(r3);
        this.A01 = AnonymousClass0WA.A00(r3);
        this.A02 = C13750s0.A00(r3);
        this.A04 = C10580kT.A03(r3);
    }

    public static GraphQLXMALayoutType A02(Message message) {
        C100354qi B46;
        GSTModelShape1S0000000 gSTModelShape1S0000000;
        C61192yU BAB = message.BAB();
        if (BAB == null || (B46 = BAB.B46()) == null || (gSTModelShape1S0000000 = (GSTModelShape1S0000000) B46.A0J(-54785808, GSTModelShape1S0000000.class, -1874146071)) == null) {
            return null;
        }
        return (GraphQLXMALayoutType) gSTModelShape1S0000000.A0O(2011608879, GraphQLXMALayoutType.A03);
    }

    public static final C28841fS A04(AnonymousClass1XY r0) {
        return A05(r0);
    }

    public static Share A06(C36871u1 r2) {
        ImmutableList B2v = r2.B2v();
        if (!B2v.isEmpty()) {
            return (Share) B2v.get(0);
        }
        SentShareAttachment B2Y = r2.B2Y();
        if (B2Y != null) {
            return B2Y.A01;
        }
        return null;
    }

    public static MediaResource A07(Message message) {
        if (A0O(message)) {
            return message.A0T.A02.A00;
        }
        return null;
    }

    public static String A09(Message message) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        if (!A11(message) || message == null || (genericAdminMessageInfo = message.A09) == null || !(genericAdminMessageInfo.A01() instanceof MessengerPageThreadActionSystemAddDetailsProperty) || ((MessengerPageThreadActionSystemAddDetailsProperty) genericAdminMessageInfo.A01()).A01 == null) {
            return null;
        }
        return ((MessengerPageThreadActionSystemAddDetailsProperty) genericAdminMessageInfo.A01()).A01;
    }

    public static String A0B(String str) {
        int length = str.length();
        if (length <= 2) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer(length);
        stringBuffer.append(str.substring(0, 1));
        stringBuffer.append("(");
        stringBuffer.append(length - 2);
        stringBuffer.append(")");
        stringBuffer.append(str.substring(length - 1, length));
        return stringBuffer.toString();
    }

    public static boolean A0C(C100354qi r1) {
        ImmutableList B4T = r1.B4T();
        if (B4T.contains(GraphQLStoryAttachmentStyle.A4w) || B4T.contains(GraphQLStoryAttachmentStyle.A5H)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002f, code lost:
        if (r0 == false) goto L_0x0031;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A1J(com.facebook.messaging.model.messages.Message r5) {
        /*
            r4 = this;
            boolean r0 = r4.A1K(r5)
            if (r0 != 0) goto L_0x0035
            X.1YI r1 = r4.A01
            r0 = 759(0x2f7, float:1.064E-42)
            r3 = 0
            boolean r0 = r1.AbO(r0, r3)
            if (r0 == 0) goto L_0x0020
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r0 = r5.A09
            if (r0 == 0) goto L_0x0020
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r2 = r0.A0C
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A0V
            r0 = 0
            if (r2 != r1) goto L_0x001d
            r0 = 1
        L_0x001d:
            if (r0 == 0) goto L_0x0020
            r3 = 1
        L_0x0020:
            if (r3 != 0) goto L_0x0035
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r0 = r5.A09
            if (r0 == 0) goto L_0x0031
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r2 = r0.A0C
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A2d
            r0 = 0
            if (r2 != r1) goto L_0x002e
            r0 = 1
        L_0x002e:
            r1 = 1
            if (r0 != 0) goto L_0x0032
        L_0x0031:
            r1 = 0
        L_0x0032:
            r0 = 0
            if (r1 == 0) goto L_0x0036
        L_0x0035:
            r0 = 1
        L_0x0036:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28841fS.A1J(com.facebook.messaging.model.messages.Message):boolean");
    }
}
