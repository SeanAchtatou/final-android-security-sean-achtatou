package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.facebook.litho.LithoView;

/* renamed from: X.0zK  reason: invalid class name and case insensitive filesystem */
public final class C17700zK {
    public static final AnonymousClass1M8 A00 = new AnonymousClass121();
    public static final AnonymousClass1M8 A01;
    public static final AnonymousClass1M8 A02 = new AnonymousClass1MQ();
    public static final AnonymousClass1M8 A03 = new C15280v1();
    public static final AnonymousClass1M8 A04;
    public static final AnonymousClass1M8 A05;
    public static final AnonymousClass1M8 A06;
    public static final AnonymousClass1M8[] A07;

    public static float A00(View view, boolean z) {
        float y;
        float f = 0.0f;
        while (view != null) {
            if (view instanceof LithoView) {
                return f;
            }
            if (z) {
                y = view.getX();
            } else {
                y = view.getY();
            }
            f += y;
            if (view.getParent() instanceof View) {
                view = (View) view.getParent();
            } else {
                throw new RuntimeException("Expected parent to be View, was " + view.getParent());
            }
        }
        throw new RuntimeException("Got unexpected null parent");
    }

    static {
        AnonymousClass1M7 r4 = new AnonymousClass1M7();
        A05 = r4;
        C180910d r3 = new C180910d();
        A06 = r3;
        C181910r r2 = new C181910r();
        A04 = r2;
        AnonymousClass120 r1 = new AnonymousClass120();
        A01 = r1;
        A07 = new AnonymousClass1M8[]{r4, r3, r2, r1};
    }

    public static View A02(Object obj, AnonymousClass1M8 r4) {
        if (obj instanceof View) {
            return (View) obj;
        }
        throw new RuntimeException("Animating '" + r4.getName() + "' is only supported on Views (got " + obj + ")");
    }

    public static View A01(Drawable drawable) {
        Drawable.Callback callback;
        while (true) {
            callback = drawable.getCallback();
            if (!(callback instanceof Drawable)) {
                break;
            }
            drawable = (Drawable) callback;
        }
        if (callback instanceof View) {
            return (View) callback;
        }
        return null;
    }
}
