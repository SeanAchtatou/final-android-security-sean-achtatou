package X;

import android.content.Context;
import android.content.IntentFilter;
import com.google.common.base.Preconditions;

/* renamed from: X.1ns  reason: invalid class name and case insensitive filesystem */
public final class C33621ns extends C14370tC {
    public final Context A00;

    public C33621ns(Context context, C15080uh r3, IntentFilter intentFilter) {
        super(r3, intentFilter, null);
        Preconditions.checkNotNull(context);
        this.A00 = context.getApplicationContext();
    }
}
