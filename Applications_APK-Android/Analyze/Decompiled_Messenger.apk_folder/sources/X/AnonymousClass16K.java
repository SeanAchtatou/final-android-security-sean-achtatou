package X;

import io.card.payment.BuildConfig;

/* renamed from: X.16K  reason: invalid class name */
public final class AnonymousClass16K extends AnonymousClass16J {
    public static final String __redex_internal_original_name = "com.facebook.litho.sections.SectionTree$CalculateChangeSetRunnable";
    public int A00 = -1;
    public C157047Nv A01;
    public String A02;
    public boolean A03;
    private final C31551js A04;
    public final /* synthetic */ AnonymousClass1AC A05;

    public synchronized void A01() {
        if (this.A03) {
            this.A03 = false;
            this.A00 = -1;
            this.A02 = null;
            this.A04.C1H(this);
        }
    }

    public synchronized void A02(int i, String str, C157047Nv r5) {
        if (!this.A03) {
            this.A03 = true;
            Throwable th = this.A00;
            if (th != null) {
                th.fillInStackTrace();
            }
            String str2 = BuildConfig.FLAVOR;
            if (this.A04.BHG()) {
                StringBuilder sb = new StringBuilder("SectionTree.CalculateChangeSetRunnable.ensurePosted - ");
                sb.append(this.A05.A0G);
                sb.append(" - ");
                sb.append(i);
                if (str != null) {
                    sb.append(" - ");
                    sb.append(str);
                }
                str2 = sb.toString();
            }
            this.A04.BxL(this, str2);
            this.A00 = i;
            this.A02 = str;
            this.A01 = r5;
        }
    }

    public AnonymousClass16K(AnonymousClass1AC r2, C31551js r3) {
        this.A05 = r2;
        this.A04 = r3;
    }
}
