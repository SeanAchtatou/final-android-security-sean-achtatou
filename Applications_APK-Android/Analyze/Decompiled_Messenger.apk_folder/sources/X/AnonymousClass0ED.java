package X;

import com.facebook.acra.LogCatCollector;
import java.io.UnsupportedEncodingException;

/* renamed from: X.0ED  reason: invalid class name */
public final class AnonymousClass0ED {
    public static byte[] A00(String str) {
        try {
            return str.getBytes(LogCatCollector.UTF_8_ENCODING);
        } catch (UnsupportedEncodingException unused) {
            throw new RuntimeException("UTF-8 not supported");
        }
    }
}
