package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.os.Build;
import com.facebook.common.dextricks.DexStore;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* renamed from: X.1SJ  reason: invalid class name */
public final class AnonymousClass1SJ {
    public static final AnonymousClass0ZG A00 = new AnonymousClass0ZG(12);

    public static int A00(Bitmap.Config config) {
        int i = AnonymousClass1SM.A00[config.ordinal()];
        if (i == 1) {
            return 4;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3 || i == 4) {
            return 2;
        }
        if (i == 5) {
            return 8;
        }
        throw new UnsupportedOperationException("The provided Bitmap.Config is not supported");
    }

    public static int A01(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        int i = Build.VERSION.SDK_INT;
        if (i > 19) {
            try {
                return bitmap.getAllocationByteCount();
            } catch (NullPointerException unused) {
            }
        }
        if (i >= 12) {
            return bitmap.getByteCount();
        }
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    public static AnonymousClass1SL A02(InputStream inputStream) {
        C05520Zg.A02(inputStream);
        ByteBuffer byteBuffer = (ByteBuffer) A00.ALa();
        if (byteBuffer == null) {
            byteBuffer = ByteBuffer.allocate(DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET);
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            options.inTempStorage = byteBuffer.array();
            ColorSpace colorSpace = null;
            BitmapFactory.decodeStream(inputStream, null, options);
            if (Build.VERSION.SDK_INT >= 26) {
                colorSpace = options.outColorSpace;
            }
            return new AnonymousClass1SL(options.outWidth, options.outHeight, colorSpace);
        } finally {
            A00.C0w(byteBuffer);
        }
    }
}
