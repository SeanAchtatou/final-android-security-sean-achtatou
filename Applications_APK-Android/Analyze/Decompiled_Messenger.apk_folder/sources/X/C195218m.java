package X;

import com.facebook.common.util.TriState;
import com.facebook.messaging.model.messagemetadata.MessagePlatformPersona;
import com.facebook.messaging.typingattribution.TypingAttributionData;
import java.util.Arrays;

/* renamed from: X.18m  reason: invalid class name and case insensitive filesystem */
public final class C195218m {
    public static final C195218m A0A = new C195218m(new AnonymousClass1H2());
    public long A00;
    private long A01;
    public final int A02;
    public final long A03;
    public final TriState A04;
    public final MessagePlatformPersona A05;
    public final TypingAttributionData A06;
    public final Integer A07;
    public final boolean A08;
    public final boolean A09;

    public boolean equals(Object obj) {
        TypingAttributionData typingAttributionData;
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                C195218m r8 = (C195218m) obj;
                if (this.A08 == r8.A08 && this.A09 == r8.A09 && this.A07 == r8.A07 && this.A02 == r8.A02 && this.A04 == r8.A04 && this.A03 == r8.A03 && this.A00 == r8.A00 && this.A01 == r8.A01 && ((typingAttributionData = this.A06) == null || typingAttributionData.equals(r8.A06))) {
                    if (typingAttributionData == null && r8.A06 != null) {
                        return false;
                    }
                    MessagePlatformPersona messagePlatformPersona = this.A05;
                    if (messagePlatformPersona == null || messagePlatformPersona.equals(r8.A05)) {
                        return this.A05 != null || r8.A05 == null;
                    }
                    return false;
                }
            }
            return false;
        }
    }

    public boolean A00() {
        if (this.A07 != AnonymousClass07B.A00 || (this.A00 & ((long) (1 << C33911oL.A01.ordinal()))) == 0) {
            return false;
        }
        return true;
    }

    public boolean A01() {
        if (this.A07 != AnonymousClass07B.A00 || (this.A00 & ((long) (1 << C33911oL.A02.ordinal()))) == 0) {
            return false;
        }
        return true;
    }

    public boolean A02() {
        if (this.A07 != AnonymousClass07B.A00 || (this.A00 & ((long) (1 << C33911oL.A03.ordinal()))) == 0) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str;
        if (1 - this.A07.intValue() != 0) {
            str = "AVAILABLE";
        } else {
            str = "NONE";
        }
        return Arrays.hashCode(new Object[]{str, Boolean.valueOf(this.A08), Boolean.valueOf(this.A09), this.A04, Long.valueOf(this.A03), Long.valueOf(this.A00), Long.valueOf(this.A01)});
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("Availability: ");
        Integer num = this.A07;
        if (num == null) {
            str = "null";
        } else if (1 - num.intValue() != 0) {
            str = "AVAILABLE";
        } else {
            str = "NONE";
        }
        sb.append(str);
        sb.append(", HasMobile: ");
        sb.append(this.A08);
        sb.append(", IsTyping: ");
        sb.append(this.A09);
        sb.append(", IsOnMessenger: ");
        sb.append(this.A04);
        sb.append(", VoipCapabilities: ");
        sb.append(this.A03);
        sb.append(", AllCapabilities: ");
        sb.append(this.A00);
        sb.append(", AlohaProxyUserId: ");
        sb.append(this.A01);
        return sb.toString();
    }

    public C195218m(AnonymousClass1H2 r3) {
        this.A07 = r3.A07;
        this.A08 = r3.A08;
        this.A04 = r3.A04;
        this.A09 = r3.A09;
        this.A06 = r3.A06;
        this.A02 = r3.A00;
        this.A03 = r3.A03;
        this.A00 = r3.A01;
        this.A01 = r3.A02;
        this.A05 = r3.A05;
    }
}
