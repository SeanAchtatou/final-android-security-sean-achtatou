package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1aB  reason: invalid class name and case insensitive filesystem */
public final class C25651aB extends Enum {
    private static final /* synthetic */ C25651aB[] A00;
    public static final C25651aB A01;
    public static final C25651aB A02;
    public static final C25651aB A03;
    public static final C25651aB A04;
    public static final C25651aB A05;

    static {
        C25651aB r2 = new C25651aB("FACEBOOK", 0);
        A03 = r2;
        C25651aB r3 = new C25651aB(C99084oO.$const$string(248), 1);
        A01 = r3;
        C25651aB r4 = new C25651aB("PHONE_NUMBER", 2);
        A05 = r4;
        C25651aB r5 = new C25651aB("FACEBOOK_OBJECT", 3);
        C25651aB r6 = new C25651aB("EMAIL", 4);
        A02 = r6;
        C25651aB r7 = new C25651aB("FACEBOOK_CONTACT", 5);
        A04 = r7;
        A00 = new C25651aB[]{r2, r3, r4, r5, r6, r7};
    }

    public static C25651aB valueOf(String str) {
        return (C25651aB) Enum.valueOf(C25651aB.class, str);
    }

    public static C25651aB[] values() {
        return (C25651aB[]) A00.clone();
    }

    private C25651aB(String str, int i) {
    }
}
