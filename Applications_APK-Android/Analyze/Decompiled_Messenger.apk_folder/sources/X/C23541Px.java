package X;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;

/* renamed from: X.1Px  reason: invalid class name and case insensitive filesystem */
public class C23541Px {
    public static final C23541Px A09 = new C23551Py().A00();
    public final int A00 = Integer.MAX_VALUE;
    public final int A01 = 100;
    public final Bitmap.Config A02;
    public final ColorSpace A03;
    public final AnonymousClass8W1 A04;
    public final C22761Ms A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                C23541Px r5 = (C23541Px) obj;
                if (!(this.A01 == r5.A01 && this.A00 == r5.A00 && this.A07 == r5.A07 && 0 == 0 && this.A06 == r5.A06 && this.A08 == r5.A08 && this.A02 == r5.A02 && this.A05 == r5.A05 && this.A04 == r5.A04 && this.A03 == r5.A03)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int ordinal = ((((((((((((this.A01 * 31) + this.A00) * 31) + (this.A07 ? 1 : 0)) * 31) + 0) * 31) + (this.A06 ? 1 : 0)) * 31) + (this.A08 ? 1 : 0)) * 31) + this.A02.ordinal()) * 31;
        C22761Ms r0 = this.A05;
        int i3 = 0;
        if (r0 != null) {
            i = r0.hashCode();
        } else {
            i = 0;
        }
        int i4 = (ordinal + i) * 31;
        AnonymousClass8W1 r02 = this.A04;
        if (r02 != null) {
            i2 = r02.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 31;
        ColorSpace colorSpace = this.A03;
        if (colorSpace != null) {
            i3 = colorSpace.hashCode();
        }
        return i5 + i3;
    }

    public String toString() {
        AnonymousClass217 A002 = C14620th.A00(this);
        AnonymousClass217.A00(A002, "minDecodeIntervalMs", String.valueOf(this.A01));
        AnonymousClass217.A00(A002, "maxDimensionPx", String.valueOf(this.A00));
        A002.A01("decodePreviewFrame", this.A07);
        A002.A01("useLastFrameForPreview", false);
        A002.A01("decodeAllFrames", this.A06);
        A002.A01("forceStaticImage", this.A08);
        AnonymousClass217.A00(A002, "bitmapConfigName", this.A02.name());
        AnonymousClass217.A00(A002, "customImageDecoder", this.A05);
        AnonymousClass217.A00(A002, "bitmapTransformation", this.A04);
        AnonymousClass217.A00(A002, "colorSpace", this.A03);
        return AnonymousClass08S.A0P("ImageDecodeOptions{", A002.toString(), "}");
    }

    public C23541Px(C23551Py r2) {
        this.A07 = r2.A04;
        this.A06 = r2.A03;
        this.A08 = r2.A05;
        this.A02 = r2.A00;
        this.A05 = r2.A02;
        this.A04 = r2.A01;
        this.A03 = null;
    }
}
