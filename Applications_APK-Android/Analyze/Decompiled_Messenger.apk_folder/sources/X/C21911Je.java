package X;

import android.content.Context;
import android.graphics.Path;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Je  reason: invalid class name and case insensitive filesystem */
public final class C21911Je extends C17770zR {
    public static final int A0H = C21901Jd.A0Q;
    @Comparable(type = 0)
    public float A00;
    @Comparable(type = 0)
    public float A01;
    @Comparable(type = 3)
    public int A02;
    @Comparable(type = 3)
    public int A03;
    @Comparable(type = 3)
    public int A04;
    @Comparable(type = 3)
    public int A05 = A0H;
    @Comparable(type = 3)
    public int A06;
    @Comparable(type = 3)
    public int A07;
    @Comparable(type = 13)
    public Path A08;
    @Comparable(type = 13)
    public Typeface A09;
    @Comparable(type = 13)
    public Drawable A0A;
    @Comparable(type = 13)
    public AnonymousClass1JY A0B;
    @Comparable(type = 13)
    public C22131Ka A0C;
    @Comparable(type = 13)
    public AnonymousClass1KU A0D;
    @Comparable(type = 3)
    public boolean A0E;
    @Comparable(type = 3)
    public boolean A0F;
    @Comparable(type = 3)
    public boolean A0G;

    public C21911Je(Context context) {
        super("UserTileComponent");
        new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r3, int i, int i2) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(11);
        ComponentBuilderCBuilderShape0_0S0300000.A0B(componentBuilderCBuilderShape0_0S0300000, r3, i, i2, new C21911Je(r3.A09));
        return componentBuilderCBuilderShape0_0S0300000;
    }
}
