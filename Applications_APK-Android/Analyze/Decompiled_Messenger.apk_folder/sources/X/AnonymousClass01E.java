package X;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import com.facebook.common.asyncview.SpriteView;

/* renamed from: X.01E  reason: invalid class name */
public final class AnonymousClass01E extends SpriteView {
    public Bitmap A00;
    public Bitmap A01;
    public Bitmap A02;
    public SpriteView.Sprite A03;
    public SpriteView.Sprite A04;
    public SpriteView.Sprite A05;
    public SpriteView.Sprite A06;
    public SpriteView.Sprite A07;
    public final float A08 = getResources().getDisplayMetrics().density;

    private SpriteView.Sprite A00() {
        Bitmap bitmap = this.A01;
        return new SpriteView.Sprite(bitmap, (float) bitmap.getWidth(), (float) this.A01.getHeight());
    }

    private C03550Om A01(SpriteView.Sprite sprite, long j) {
        return new AnonymousClass0SK(new AnonymousClass0SJ(j + 400), new AnonymousClass0SU(this, new AnonymousClass0DH(), 500, sprite, this.A08 * 4.0f));
    }

    public AnonymousClass01E(Context context) {
        super(context);
    }

    public void A0K() {
        super.A0K();
        this.A02.recycle();
        this.A01.recycle();
        this.A00.recycle();
    }

    public void A0L() {
        super.A0L();
        this.A02 = BitmapFactory.decodeResource(getResources(), 2132346705);
        int i = (int) (this.A08 * 14.0f);
        this.A01 = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
        shapeDrawable.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
        shapeDrawable.setBounds(0, 0, i, i);
        shapeDrawable.draw(new Canvas(this.A01));
        this.A00 = BitmapFactory.decodeResource(getResources(), 2132346703);
        Bitmap bitmap = this.A02;
        this.A07 = new SpriteView.Sprite(bitmap, ((float) bitmap.getWidth()) * 1.3f, ((float) this.A02.getHeight()) * 1.3f);
        this.A04 = A00();
        this.A05 = A00();
        this.A06 = A00();
        Bitmap bitmap2 = this.A00;
        this.A03 = new SpriteView.Sprite(bitmap2, ((float) bitmap2.getWidth()) * 1.3f, ((float) this.A00.getHeight()) * 1.3f);
        A0R(this.A07);
        A0R(this.A04);
        A0R(this.A05);
        A0R(this.A06);
        A0R(this.A03);
        float f = this.A08 * 4.0f;
        this.A03.setTranslationY(f);
        this.A04.setTranslationY(f);
        this.A05.setTranslationY(f);
        this.A06.setTranslationY(f);
        this.A04.setScaleX(0.0f);
        this.A04.setScaleY(0.0f);
        this.A05.setScaleX(0.0f);
        this.A05.setScaleY(0.0f);
        this.A06.setScaleX(0.0f);
        this.A06.setScaleY(0.0f);
        AnonymousClass0Mq r10 = new AnonymousClass0Mq(new AnonymousClass0SZ(new LinearInterpolator(), A01(this.A04, 0), A01(this.A05, 100), A01(this.A06, 200)), 2);
        AnonymousClass0SZ r11 = new AnonymousClass0SZ(new AnticipateInterpolator(), new AnonymousClass0SQ(this, 200, this.A08 * 24.0f));
        AnonymousClass0SO r2 = new AnonymousClass0SO(this, 700);
        AnonymousClass0SK r3 = new AnonymousClass0SK(new AnonymousClass0SJ(1000), new AnonymousClass0SZ(new LinearInterpolator(), new AnonymousClass0DI(this, new AnticipateInterpolator(0.7f), 700), r2), new AnonymousClass0SN(r11), r10, r11, new AnonymousClass0SZ(new LinearInterpolator(), new AnonymousClass0SP(this, new OvershootInterpolator(0.7f), 700), new AnonymousClass0SN(r2)));
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.addUpdateListener(new C03590Oq(r3));
        ofFloat.setDuration(r3.A00);
        ofFloat.setRepeatCount(-1);
        ofFloat.setInterpolator(new LinearInterpolator());
        ofFloat.start();
    }
}
