package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.checkpoint.CheckpointMetadata;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.messaging.sms.NeueSmsPreferenceActivity;
import com.facebook.zero.LocalZeroTokenManagerReceiverRegistration;
import com.facebook.zero.service.ZeroInterstitialEligibilityManager;
import com.google.common.base.Platform;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1ZS  reason: invalid class name */
public abstract class AnonymousClass1ZS implements AnonymousClass06U {
    private final AnonymousClass0US A00;
    private final AnonymousClass06j A01;

    public void A02(Context context, Intent intent, Object obj) {
        String stringExtra;
        if (this instanceof C32351lc) {
            ((ZeroInterstitialEligibilityManager) obj).A01();
        } else if (this instanceof C32941ma) {
            C32941ma r3 = (C32941ma) this;
            C35251qv r10 = (C35251qv) obj;
            C005505z.A03("ZeroHeaderRequestManager.LocalZeroHeaderRequestManagerReceiverRegistration.onReceive", 1607644724);
            try {
                if ("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED".equals(intent.getAction()) && !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r3.A00)).Aem(285224484607517L)) {
                    C32941ma.A01(r3, r10);
                }
            } finally {
                C005505z.A00(-900987964);
            }
        } else if (this instanceof C36411t1) {
            C36411t1 r32 = (C36411t1) this;
            C35251qv r102 = (C35251qv) obj;
            if ("com.facebook.zero.ACTION_FORCE_ZERO_HEADER_REFRESH".equals(intent.getAction())) {
                int i = AnonymousClass1Y3.BJf;
                if (((AnonymousClass2X9) AnonymousClass1XX.A02(0, i, r32.A00)).CFC()) {
                    ((AnonymousClass2X9) AnonymousClass1XX.A02(0, i, r32.A00)).AaW();
                } else {
                    r102.A05(true, AnonymousClass07B.A01);
                }
            }
        } else if (this instanceof LocalZeroTokenManagerReceiverRegistration) {
            LocalZeroTokenManagerReceiverRegistration localZeroTokenManagerReceiverRegistration = (LocalZeroTokenManagerReceiverRegistration) this;
            C28191eP r103 = (C28191eP) obj;
            String action = intent.getAction();
            if ("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED".equals(action)) {
                if (!((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, localZeroTokenManagerReceiverRegistration.A00)).A0G()) {
                    r103.A0O("Network changed in foreground");
                    return;
                }
                synchronized (localZeroTokenManagerReceiverRegistration) {
                    localZeroTokenManagerReceiverRegistration.A01 = true;
                }
            } else if ("com.facebook.zero.ZERO_HEADER_REFRESH_COMPLETED".equals(action)) {
                r103.A0P("headers");
            }
        } else if (this instanceof C36401t0) {
            C36401t0 r33 = (C36401t0) this;
            C29271g9 r104 = (C29271g9) obj;
            if (AnonymousClass195.CHANNEL_CONNECTED == AnonymousClass195.A00(intent.getIntExtra("event", AnonymousClass195.UNKNOWN.value)) && !r33.A01) {
                C36421t3 r5 = new C36421t3(r33, r104);
                if (!((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, r33.A00)).AbO(AnonymousClass1Y3.A2U, false)) {
                    AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BKH, r33.A00), r5, 340959094);
                } else {
                    AnonymousClass108 r1 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, r33.A00);
                    r1.A02(r5);
                    r1.A02 = "OmnistoreInit";
                    r1.A03("Background");
                    ((AnonymousClass0g4) AnonymousClass1XX.A03(AnonymousClass1Y3.ASI, r33.A00)).A04(r1.A01(), "ReplaceExisting");
                }
                r33.A01 = true;
            }
        } else if (this instanceof C25351Zh) {
            C50532eB r105 = (C50532eB) obj;
            if (intent.hasExtra("address_list")) {
                AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Aoc, r105.A00), new C98164mf(r105, intent.getStringArrayListExtra("address_list")), 796042164);
            }
        } else if (this instanceof C05500Ze) {
            AnonymousClass91M r106 = (AnonymousClass91M) obj;
            if (!intent.getBooleanExtra("default_sms", false) || !((C185814g) r106.A03.get()).A02() || !Platform.stringIsNullOrEmpty(((AnonymousClass9NB) r106.A05.get()).A06(r106.A01))) {
                r106.A02.A02(null, AnonymousClass1Y3.BNQ);
                return;
            }
            AnonymousClass0ZN A012 = ((C71223c2) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANy, r106.A00)).A01(r106.A01, AnonymousClass1Y3.BNQ);
            AnonymousClass0ZV r4 = new AnonymousClass0ZV();
            r4.A01(r106.A01.getString(2131828919));
            PendingIntent A002 = C64633Cq.A00(r106.A01, 0, new Intent(r106.A01, NeueSmsPreferenceActivity.class), 134217728);
            A012.A0C(r106.A01.getString(2131828920));
            A012.A0B(r106.A01.getString(2131828919));
            A012.A05(r106.A06.A02());
            A012.A0C = A002;
            A012.A0E(true);
            A012.A0A(r4);
            r106.A02.A01(AnonymousClass1Y3.BNQ, A012.A02());
            C21451Ha.A05((C21451Ha) r106.A04.get(), C21451Ha.A01("sms_takeover_fallback_notification"));
        } else if (!(this instanceof C06690bv)) {
            C93964dg r107 = (C93964dg) obj;
            if ("com.facebook.http.protocol.CHECKPOINT_API_EXCEPTION".equals(intent.getAction())) {
                r107.A02(new CheckpointMetadata(intent.getStringExtra("checkpoint_flow_id"), intent.getStringExtra("checkpoint_content_id"), intent.getBooleanExtra("show_native_checkpoints", false)));
            } else if (C06680bu.A0K.equals(intent.getAction()) && (stringExtra = intent.getStringExtra(AnonymousClass80H.$const$string(AnonymousClass1Y3.A0z))) != null && stringExtra.equals(C133026Kn.A04.toString())) {
                r107.A01();
            }
        } else {
            AnonymousClass4MI r108 = (AnonymousClass4MI) obj;
            C851341w r12 = (C851341w) r108.A01.get();
            synchronized (r12) {
                r12.A00.clear();
            }
            ((BlueServiceOperationFactory) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B1W, r108.A00)).newInstance("sync_chat_context", Bundle.EMPTY, 1, AnonymousClass4MI.A02).CGe();
        }
    }

    public AnonymousClass1ZS(AnonymousClass06j r1, AnonymousClass0US r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r5) {
        int A002 = AnonymousClass09Y.A00(1061341488);
        if (!this.A01.A02(context, this, intent)) {
            AnonymousClass09Y.A01(-16735296, A002);
            return;
        }
        A02(context, intent, this.A00.get());
        AnonymousClass09Y.A01(1389481980, A002);
    }
}
