package X;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import com.facebook.device_id.DefaultPhoneIdProvider;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.util.ArrayList;

/* renamed from: X.1XF  reason: invalid class name */
public abstract class AnonymousClass1XF extends ContentProvider {
    public C45732Ng A00() {
        return (C45732Ng) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgJ, ((DefaultPhoneIdProvider) this).A00);
    }

    public AnonymousClass2SP A01(Context context) {
        DefaultPhoneIdProvider defaultPhoneIdProvider = (DefaultPhoneIdProvider) this;
        if (!((C25921ac) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BEf, ((C06240bB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AlF, defaultPhoneIdProvider.A00)).A00)).BFQ()) {
            try {
                ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, ((C06240bB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AlF, defaultPhoneIdProvider.A00)).A00)).AQP();
            } catch (InterruptedException unused) {
            }
        }
        return ((C06240bB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AlF, defaultPhoneIdProvider.A00)).A02();
    }

    public AnonymousClass99G A02(Context context) {
        DefaultPhoneIdProvider defaultPhoneIdProvider = (DefaultPhoneIdProvider) this;
        if (!((C25921ac) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BEf, ((C06240bB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AlF, defaultPhoneIdProvider.A00)).A00)).BFQ()) {
            try {
                ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, ((C06240bB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AlF, defaultPhoneIdProvider.A00)).A00)).AQP();
            } catch (InterruptedException unused) {
            }
        }
        return ((C06240bB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AlF, defaultPhoneIdProvider.A00)).A04();
    }

    public void A03() {
        DefaultPhoneIdProvider defaultPhoneIdProvider = (DefaultPhoneIdProvider) this;
        defaultPhoneIdProvider.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(defaultPhoneIdProvider.getContext()));
    }

    public boolean A04(Context context) {
        if (!(this instanceof DefaultPhoneIdProvider)) {
            return true;
        }
        return ((C06240bB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AlF, ((DefaultPhoneIdProvider) this).A00)).A0A();
    }

    public boolean A05(Context context) {
        if (!(this instanceof DefaultPhoneIdProvider)) {
            return true;
        }
        return ((C06240bB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AlF, ((DefaultPhoneIdProvider) this).A00)).A0B();
    }

    public boolean onCreate() {
        return true;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        throw new UnsupportedOperationException();
    }

    public String getType(Uri uri) {
        throw new UnsupportedOperationException();
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException();
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException();
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        boolean z;
        boolean z2;
        SecurityException securityException;
        A03();
        try {
            Context context = getContext();
            AnonymousClass0TW r2 = C1932893t.A01;
            if (context == null) {
                z = false;
            } else {
                z = AnonymousClass0TW.A04(r2, AnonymousClass0TW.A01(context), context);
            }
            if (!z) {
                AnonymousClass0TW r1 = C1932893t.A00;
                if (context == null) {
                    z2 = false;
                } else {
                    z2 = AnonymousClass0TW.A04(r1, AnonymousClass0TW.A01(context), context);
                }
                if (!z2) {
                    if (!AnonymousClass0TW.A04(r2, AnonymousClass0TW.A01(context), context)) {
                        securityException = new SecurityException("Access denied.");
                    } else if (!AnonymousClass0TW.A04(r1, AnonymousClass0TW.A01(context), context)) {
                        securityException = new SecurityException("Access denied.");
                    }
                    throw securityException;
                }
            }
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            AnonymousClass2SP A01 = A01(getContext());
            if (A01 != null && A04(getContext())) {
                arrayList.add("COL_PHONE_ID");
                arrayList.add("COL_TIMESTAMP");
                arrayList.add("COL_ORIGIN");
                arrayList2.add(A01.A01);
                arrayList2.add(Long.toString(A01.A00));
                arrayList2.add(A01.A02);
            }
            AnonymousClass99G A02 = A02(getContext());
            if (A02 != null && A05(getContext())) {
                arrayList.add("COL_SFDID");
                arrayList.add("COL_SFDID_CREATION_TS");
                arrayList.add("COL_SFDID_GP");
                arrayList.add("COL_SFDID_GA");
                arrayList2.add(A02.A03);
                arrayList2.add(Long.toString(A02.A00));
                arrayList2.add(A02.A02);
                arrayList2.add(A02.A01);
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            MatrixCursor matrixCursor = new MatrixCursor((String[]) arrayList.toArray(new String[arrayList.size()]));
            matrixCursor.addRow(arrayList2.toArray(new String[arrayList2.size()]));
            return matrixCursor;
        } catch (Exception e) {
            if (A00() == null) {
                return null;
            }
            A00().A01("AbstractPhoneIdProvider", e.getMessage(), e);
            return null;
        }
    }
}
