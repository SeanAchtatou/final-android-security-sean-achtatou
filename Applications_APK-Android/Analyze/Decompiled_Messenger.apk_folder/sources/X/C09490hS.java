package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.collect.ImmutableSet;

/* renamed from: X.0hS  reason: invalid class name and case insensitive filesystem */
public final class C09490hS implements AnonymousClass06U {
    public final /* synthetic */ C05530Zh A00;

    public C09490hS(C05530Zh r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r7) {
        boolean z;
        int A002 = AnonymousClass09Y.A00(1583115258);
        C05530Zh r2 = this.A00;
        synchronized (r2) {
            if (r2.A01 != null) {
                if (intent.hasExtra("thread_key")) {
                    z = C05530Zh.A02(r2, ImmutableSet.A04((ThreadKey) intent.getParcelableExtra("thread_key")));
                } else if (intent.hasExtra("multiple_thread_keys")) {
                    z = C05530Zh.A02(r2, ImmutableSet.A0A(intent.getParcelableArrayListExtra("multiple_thread_keys")));
                } else {
                    z = true;
                }
                if (z) {
                    C05530Zh.A00(r2);
                }
            }
        }
        AnonymousClass09Y.A01(2011876739, A002);
    }
}
