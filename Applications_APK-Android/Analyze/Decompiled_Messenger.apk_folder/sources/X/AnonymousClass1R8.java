package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Patterns;
import com.facebook.common.util.StringLocaleUtil;
import com.google.common.base.Objects;
import com.google.common.base.Platform;
import java.util.List;

/* renamed from: X.1R8  reason: invalid class name */
public final class AnonymousClass1R8 extends Drawable implements Drawable.Callback {
    public float A00;
    public int A01;
    public int A02;
    public Path A03;
    public AnonymousClass1KU A04;
    public String A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public final Paint A09;
    public final RectF A0A;
    private final Rect A0B;

    public int getOpacity() {
        return -3;
    }

    public static String A00(String str, AnonymousClass1KU r4) {
        if (r4 != AnonymousClass1KU.TWO_LETTER) {
            return C189116b.A04(str);
        }
        if (C06850cB.A0B(str)) {
            return null;
        }
        String trim = str.trim();
        if (C06850cB.A0B(trim) || Patterns.PHONE.matcher(trim).matches()) {
            return null;
        }
        List A082 = C06850cB.A08(trim, ' ');
        int size = A082.size();
        StringBuilder sb = new StringBuilder(2);
        sb.appendCodePoint(C189116b.A00((String) A082.get(0)));
        if (size > 1) {
            sb.appendCodePoint(C189116b.A00((String) A082.get(size - 1)));
        }
        return sb.toString();
    }

    private void A01() {
        String str = this.A05;
        if (Platform.stringIsNullOrEmpty(str)) {
            this.A0B.setEmpty();
        } else {
            this.A09.getTextBounds(str, 0, str.length(), this.A0B);
        }
    }

    public void A02(float f) {
        this.A09.setTextSize(f);
        A01();
    }

    public void A03(int i) {
        this.A09.setColor(i);
    }

    public void A04(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.A09.setColor(context.getColor(i));
        } else {
            this.A09.setColor(AnonymousClass01R.A00(context, i));
        }
    }

    public void A05(Typeface typeface) {
        this.A09.setTypeface(typeface);
        A01();
    }

    public void A06(String str) {
        if (!Objects.equal(str, this.A05)) {
            this.A05 = str;
            A01();
            invalidateSelf();
        }
    }

    public boolean A07(String str) {
        if (!Objects.equal(this.A06, str)) {
            this.A06 = str;
            A06(StringLocaleUtil.toUpperCaseLocaleSafe(A00(str, this.A04)));
        }
        if (this.A05 != null) {
            return true;
        }
        return false;
    }

    public void draw(Canvas canvas) {
        boolean z = !Platform.stringIsNullOrEmpty(this.A05);
        Rect bounds = getBounds();
        canvas.save();
        Path path = this.A03;
        if (path != null) {
            boolean z2 = false;
            if (Build.VERSION.SDK_INT >= 18) {
                z2 = true;
            }
            if (z2) {
                canvas.clipPath(path);
            }
        }
        if (this.A07 && (z || this.A08)) {
            int color = this.A09.getColor();
            this.A09.setColor(this.A01);
            this.A0A.set(bounds);
            canvas.drawOval(this.A0A, this.A09);
            this.A09.setStyle(Paint.Style.STROKE);
            this.A09.setStrokeWidth(this.A00);
            this.A09.setColor(this.A02);
            canvas.drawOval(this.A0A, this.A09);
            this.A09.setStyle(Paint.Style.FILL);
            this.A09.setColor(color);
        }
        if (z) {
            canvas.drawText(this.A05, bounds.exactCenterX(), bounds.exactCenterY() + ((float) (this.A0B.height() >> 1)), this.A09);
        }
        canvas.restore();
    }

    public void setAlpha(int i) {
        this.A09.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.A09.setColorFilter(colorFilter);
    }

    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        A01();
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public AnonymousClass1R8() {
        this.A04 = AnonymousClass1KU.ONE_LETTER;
        Paint paint = new Paint();
        this.A09 = paint;
        paint.setFlags(1);
        this.A09.setTextAlign(Paint.Align.CENTER);
        this.A0B = new Rect();
        this.A0A = new RectF();
    }

    public AnonymousClass1R8(Context context, AttributeSet attributeSet, int i, int i2) {
        this();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C008407o.A2p, i, i2);
        this.A04 = AnonymousClass1KU.values()[obtainStyledAttributes.getInt(2, AnonymousClass1KU.ONE_LETTER.ordinal())];
        int color = obtainStyledAttributes.getColor(1, AnonymousClass01R.A00(context, 2132083163));
        obtainStyledAttributes.recycle();
        A03(color);
        A02((float) obtainStyledAttributes.getDimensionPixelSize(0, C007106r.A04(context.getResources(), 2132148404)));
    }
}
