package X;

import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.proxygen.LigerSamplePolicy;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Zw  reason: invalid class name and case insensitive filesystem */
public final class C25501Zw {
    private static volatile C25501Zw A08;
    public long A00 = 0;
    public long A01 = 0;
    public long A02 = 0;
    public long A03 = 0;
    public AnonymousClass0UN A04;
    private long A05 = 0;
    private long A06 = 0;
    private long A07 = 0;

    public static synchronized void A01(C25501Zw r1, long j) {
        synchronized (r1) {
            if (!r1.A03(j)) {
                r1.notifyAll();
            }
        }
    }

    public synchronized void A06() {
        long j;
        long j2;
        boolean z = false;
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, this.A04)).AOx();
        long now = ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A04)).now();
        if (A03(now)) {
            if (A02(now)) {
                j2 = 15000;
                j = ((AnonymousClass0Ud) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B5j, this.A04)).A0E;
            } else if (A04(now)) {
                j2 = StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS;
                j = this.A00;
            } else {
                j2 = LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT;
                j = this.A01;
            }
            long j3 = j2 - (now - j);
            if (j3 > 0) {
                z = true;
            }
            AnonymousClass064.A06(z, "remaining time < 0: %d", Long.valueOf(j3));
            C21231Ft r4 = (C21231Ft) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A0z, this.A04);
            try {
                wait(j3);
            } catch (InterruptedException e) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r4.A00)).softReport("AppStartupNotifier", e);
            }
        }
        return;
    }

    public synchronized void A07(Integer num) {
        switch (num.intValue()) {
            case 1:
                this.A05 = 0;
                break;
            case 2:
                this.A06 = 0;
                break;
            case 3:
                this.A07 = 0;
                break;
        }
        this.A03 = ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A04)).now();
        ((C17440yu) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BT7, this.A04)).A01 = true;
        notifyAll();
    }

    public synchronized void A08(Integer num) {
        switch (num.intValue()) {
            case 1:
                this.A05 = ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A04)).now();
                break;
            case 2:
                this.A06 = ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A04)).now();
                break;
            case 3:
                this.A07 = ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A04)).now();
                break;
        }
        this.A03 = ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A04)).now();
        ((C17440yu) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BT7, this.A04)).A01 = true;
        notifyAll();
    }

    public static final C25501Zw A00(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (C25501Zw.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new C25501Zw(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    private boolean A02(long j) {
        if (this.A03 != 0 || j - ((AnonymousClass0Ud) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B5j, this.A04)).A0E >= 15000) {
            return false;
        }
        return true;
    }

    private boolean A04(long j) {
        long j2 = this.A00;
        if (j2 <= 0 || j2 <= this.A03 || j2 - this.A02 <= 1000 || j - j2 >= StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS) {
            return false;
        }
        return true;
    }

    public boolean A09() {
        return A03(((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A04)).now());
    }

    private C25501Zw(AnonymousClass1XY r3) {
        this.A04 = new AnonymousClass0UN(5, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (r3 <= r7.A03) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A03(long r8) {
        /*
            r7 = this;
            boolean r0 = r7.A02(r8)
            if (r0 != 0) goto L_0x002a
            boolean r0 = r7.A04(r8)
            if (r0 != 0) goto L_0x002a
            long r3 = r7.A01
            long r0 = r7.A02
            long r5 = r3 - r0
            r1 = 1000(0x3e8, double:4.94E-321)
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0026
            long r8 = r8 - r3
            r1 = 5000(0x1388, double:2.4703E-320)
            int r0 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0026
            long r1 = r7.A03
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r1 = 1
            if (r0 > 0) goto L_0x0027
        L_0x0026:
            r1 = 0
        L_0x0027:
            r0 = 0
            if (r1 == 0) goto L_0x002b
        L_0x002a:
            r0 = 1
        L_0x002b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25501Zw.A03(long):boolean");
    }

    public long A05(Integer num) {
        long j;
        switch (num.intValue()) {
            case 0:
                j = this.A03;
                break;
            case 1:
                j = this.A05;
                break;
            case 2:
                j = this.A06;
                break;
            case 3:
                j = this.A07;
                break;
            default:
                j = 0;
                break;
        }
        if (j != 0) {
            return ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A04)).now() - j;
        }
        return 0;
    }
}
