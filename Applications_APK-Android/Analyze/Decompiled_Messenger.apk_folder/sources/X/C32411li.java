package X;

/* renamed from: X.1li  reason: invalid class name and case insensitive filesystem */
public final class C32411li {
    public final Integer A00;
    public final String A01;
    public final String A02;
    public final boolean A03;
    public final boolean A04;

    public static C32411li A00(String str, String str2) {
        return new C32411li(str, str2, false, AnonymousClass07B.A00, false);
    }

    public C32411li(String str, String str2, boolean z, Integer num, boolean z2) {
        this.A02 = str;
        this.A01 = str2;
        this.A04 = z;
        this.A00 = num;
        this.A03 = z2;
    }
}
