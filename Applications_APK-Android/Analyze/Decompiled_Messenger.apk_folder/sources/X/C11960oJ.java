package X;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.0oJ  reason: invalid class name and case insensitive filesystem */
public final class C11960oJ {
    public static void A02(String str, JsonNode jsonNode, C12240os r4) {
        if (jsonNode == null || jsonNode.isNull()) {
            r4.A0K(str, null);
        } else if (jsonNode.isTextual()) {
            r4.A0K(str, jsonNode.textValue());
        } else if (jsonNode.isNumber()) {
            r4.A0J(str, jsonNode.numberValue());
        } else if (jsonNode.isBoolean()) {
            C12240os.A01(r4, str, Boolean.valueOf(jsonNode.booleanValue()));
        } else if (jsonNode.isObject()) {
            A01((ObjectNode) jsonNode, r4.A0F(str));
        } else if (jsonNode.isArray()) {
            A00((ArrayNode) jsonNode, str, r4.A0E(str));
        } else {
            throw new IllegalArgumentException("Unsupported JSON type: " + jsonNode.getNodeType());
        }
    }

    private static void A00(ArrayNode arrayNode, String str, C16910xz r5) {
        Iterator elements = arrayNode.elements();
        while (elements.hasNext()) {
            JsonNode jsonNode = (JsonNode) elements.next();
            if (jsonNode.isNull()) {
                C16910xz.A00(r5, null);
            } else if (jsonNode.isTextual()) {
                C16910xz.A00(r5, jsonNode.textValue());
            } else if (jsonNode.isNumber()) {
                C16910xz.A00(r5, jsonNode.numberValue());
            } else if (jsonNode.isBoolean()) {
                C16910xz.A00(r5, Boolean.valueOf(jsonNode.booleanValue()));
            } else if (jsonNode.isObject()) {
                A01((ObjectNode) jsonNode, r5.A0F());
            } else if (jsonNode.isArray()) {
                A00((ArrayNode) jsonNode, str, r5.A0E());
            } else {
                throw new IllegalArgumentException("Unsupported JSON type for '" + str + "': " + jsonNode.getNodeType());
            }
        }
    }

    public static void A01(ObjectNode objectNode, C12240os r5) {
        Iterator fields = objectNode.fields();
        while (fields.hasNext()) {
            Map.Entry entry = (Map.Entry) fields.next();
            String str = (String) entry.getKey();
            JsonNode jsonNode = (JsonNode) entry.getValue();
            if (jsonNode.isNull()) {
                r5.A0K(str, null);
            } else if (jsonNode.isTextual()) {
                r5.A0K(str, jsonNode.textValue());
            } else if (jsonNode.isNumber()) {
                r5.A0J(str, jsonNode.numberValue());
            } else if (jsonNode.isBoolean()) {
                C12240os.A01(r5, str, Boolean.valueOf(jsonNode.booleanValue()));
            } else if (jsonNode.isObject()) {
                A01((ObjectNode) jsonNode, r5.A0F(str));
            } else if (jsonNode.isArray()) {
                A00((ArrayNode) jsonNode, str, r5.A0E(str));
            } else {
                throw new IllegalArgumentException("Unsupported JSON type for '" + str + "': " + jsonNode.getNodeType());
            }
        }
    }
}
