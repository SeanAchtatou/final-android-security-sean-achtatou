package X;

import android.os.RemoteException;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/* renamed from: X.0GC  reason: invalid class name */
public final class AnonymousClass0GC implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector$4";
    public final /* synthetic */ AnonymousClass0R9 A00;
    public final /* synthetic */ SkywalkerSubscriptionConnector A01;
    public final /* synthetic */ JsonNode A02;
    public final /* synthetic */ String A03;

    public AnonymousClass0GC(SkywalkerSubscriptionConnector skywalkerSubscriptionConnector, String str, JsonNode jsonNode, AnonymousClass0R9 r4) {
        this.A01 = skywalkerSubscriptionConnector;
        this.A03 = str;
        this.A02 = jsonNode;
        this.A00 = r4;
    }

    /* JADX INFO: finally extract failed */
    public void run() {
        boolean z;
        ObjectNode createObjectNode = this.A01.A02.createObjectNode();
        createObjectNode.put(this.A03, this.A02.toString());
        String str = null;
        ObjectNode A032 = SkywalkerSubscriptionConnector.A02(null, null, createObjectNode);
        C36991uR BvK = this.A01.A01.BvK();
        try {
            z = BvK.A07("/pubsub", A032, LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT);
            BvK.A06();
        } catch (RemoteException e) {
            C010708t.A08(SkywalkerSubscriptionConnector.A08, "Remote exception for publish", e);
            str = e.getMessage();
            BvK.A06();
            z = false;
        } catch (Throwable th) {
            BvK.A06();
            throw th;
        }
        AnonymousClass0R9 r0 = this.A00;
        if (r0 == null) {
            return;
        }
        if (z) {
            r0.BqS();
        } else {
            r0.BqR(str);
        }
    }
}
