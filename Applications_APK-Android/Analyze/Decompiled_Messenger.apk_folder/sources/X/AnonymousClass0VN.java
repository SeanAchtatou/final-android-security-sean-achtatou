package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0VN  reason: invalid class name */
public final class AnonymousClass0VN {
    private static volatile AnonymousClass0VN A03;
    public final int A00;
    public final int A01;
    public final int A02;

    public static final AnonymousClass0VN A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass0VN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass0VN(AnonymousClass0VO.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private AnonymousClass0VN(AnonymousClass0VP r3) {
        int A07 = r3.A07();
        A07 = A07 == -1 ? r3.A06() : A07;
        A07 = A07 < 1 ? 1 : A07;
        this.A00 = A07 + 1;
        int i = A07 << 1;
        this.A01 = i + 1;
        this.A02 = i;
    }
}
