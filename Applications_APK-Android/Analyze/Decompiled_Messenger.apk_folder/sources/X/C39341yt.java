package X;

import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.facebook.interstitial.triggers.InterstitialTriggerContext;
import com.facebook.quickpromotion.model.QuickPromotionDefinition;

/* renamed from: X.1yt  reason: invalid class name and case insensitive filesystem */
public final class C39341yt extends C93444cj {
    public boolean A03(QuickPromotionDefinition quickPromotionDefinition, QuickPromotionDefinition.ContextualFilter contextualFilter, InterstitialTrigger interstitialTrigger) {
        InterstitialTriggerContext interstitialTriggerContext;
        return (interstitialTrigger == null || (interstitialTriggerContext = interstitialTrigger.A00) == null || Boolean.valueOf(interstitialTriggerContext.A00("target_user_is_birthday")).booleanValue() != Boolean.valueOf(contextualFilter.value).booleanValue()) ? false : true;
    }

    public static final C39341yt A00() {
        return new C39341yt();
    }
}
