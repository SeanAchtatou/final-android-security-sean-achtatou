package X;

import com.facebook.payments.settings.PaymentSettingsPickerRunTimeData;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1Cp  reason: invalid class name and case insensitive filesystem */
public final class C20481Cp extends C06020ai {
    public final /* synthetic */ C23694Bka A00;
    public final /* synthetic */ PaymentSettingsPickerRunTimeData A01;
    public final /* synthetic */ C55112nY A02;
    public final /* synthetic */ C55102nX A03;
    public final /* synthetic */ ImmutableList A04;

    public C20481Cp(C55112nY r1, PaymentSettingsPickerRunTimeData paymentSettingsPickerRunTimeData, C55102nX r3, ImmutableList immutableList, C23694Bka bka) {
        this.A02 = r1;
        this.A01 = paymentSettingsPickerRunTimeData;
        this.A03 = r3;
        this.A04 = immutableList;
        this.A00 = bka;
    }
}
