package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Pj  reason: invalid class name and case insensitive filesystem */
public final class C23401Pj extends C23441Pn {
    private static volatile C23401Pj A01;
    public final C22721Mo A00;

    public static final C23401Pj A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (C23401Pj.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        A01 = new C23401Pj(new C22711Mn(r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C23401Pj(C22711Mn r4) {
        this.A00 = new C22721Mo(C22741Mq.A01(r4), "android_image_pipeline_producer_counters");
    }
}
