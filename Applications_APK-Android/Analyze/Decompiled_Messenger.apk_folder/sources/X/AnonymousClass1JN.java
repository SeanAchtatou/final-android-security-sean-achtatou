package X;

import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1JN  reason: invalid class name */
public final class AnonymousClass1JN extends C17770zR {
    @Comparable(type = 13)
    public C25998CqM A00;
    @Comparable(type = 13)
    public C204069ji A01;
    @Comparable(type = 14)
    public AnonymousClass5CJ A02 = new AnonymousClass5CJ();
    @Comparable(type = 13)
    public C203819jI A03;
    @Comparable(type = 13)
    public C121605oW A04;
    @Comparable(type = 13)
    public String A05;
    @Comparable(type = 3)
    public boolean A06;
    @Comparable(type = 3)
    public boolean A07;

    public AnonymousClass1JN() {
        super("AccountLoginRecSearchRootComponent");
    }

    public C17770zR A16() {
        AnonymousClass1JN r1 = (AnonymousClass1JN) super.A16();
        r1.A02 = new AnonymousClass5CJ();
        return r1;
    }
}
