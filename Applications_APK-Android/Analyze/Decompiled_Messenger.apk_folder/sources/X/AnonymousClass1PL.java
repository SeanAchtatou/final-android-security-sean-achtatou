package X;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import com.facebook.webpsupport.WebpBitmapFactoryImpl;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1PL  reason: invalid class name */
public final class AnonymousClass1PL {
    public static AnonymousClass1PM A0Q = new AnonymousClass1PM();
    public final int A00;
    public final Context A01;
    public final Bitmap.Config A02;
    public final C23251Ou A03;
    public final C23251Ou A04;
    public final AnonymousClass1PD A05;
    public final C23111Og A06;
    public final C23111Og A07;
    public final C23111Og A08;
    public final C14320t5 A09;
    public final C22601Mc A0A;
    public final C22701Mm A0B;
    public final C30831ii A0C;
    public final C22641Mg A0D;
    public final AnonymousClass1PF A0E;
    public final AnonymousClass1PU A0F;
    public final C30641iP A0G;
    public final C22761Ms A0H;
    public final C23301Oz A0I;
    public final AnonymousClass1N0 A0J;
    public final C23281Ox A0K;
    public final C23351Pe A0L;
    public final Set A0M;
    public final Set A0N;
    public final boolean A0O;
    public final boolean A0P;

    public AnonymousClass1PL(AnonymousClass1PI r6) {
        int i;
        WebpBitmapFactoryImpl A002;
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("ImagePipelineConfig()");
        }
        this.A0F = new AnonymousClass1PU(r6.A0N);
        C23111Og r2 = r6.A03;
        this.A06 = r2 == null ? new AnonymousClass1P7((ActivityManager) r6.A0M.getSystemService("activity")) : r2;
        C30831ii r0 = r6.A0A;
        this.A0C = r0 == null ? new AnonymousClass1PP() : r0;
        this.A02 = Bitmap.Config.ARGB_8888;
        C22601Mc r02 = r6.A08;
        if (r02 == null) {
            synchronized (C22601Mc.class) {
                if (C22601Mc.A00 == null) {
                    C22601Mc.A00 = new C22601Mc();
                }
                r02 = C22601Mc.A00;
            }
        }
        this.A0A = r02;
        Context context = r6.A0M;
        C05520Zg.A02(context);
        this.A01 = context;
        AnonymousClass1PF r1 = r6.A0C;
        if (r1 == null) {
            new C412524s();
            r1 = new C412424r();
        }
        this.A0E = r1;
        this.A0O = r6.A0K;
        C23111Og r03 = r6.A04;
        this.A07 = r03 == null ? new AnonymousClass1PB() : r03;
        C22701Mm r04 = r6.A09;
        if (r04 == null) {
            synchronized (AnonymousClass8SY.class) {
                if (AnonymousClass8SY.A00 == null) {
                    AnonymousClass8SY.A00 = new AnonymousClass8SY();
                }
                r04 = AnonymousClass8SY.A00;
            }
        }
        this.A0B = r04;
        this.A0H = r6.A0E;
        this.A0L = null;
        C23111Og r05 = r6.A05;
        this.A08 = r05 == null ? new AnonymousClass54Q() : r05;
        C23251Ou r12 = r6.A00;
        if (r12 == null) {
            Context context2 = r6.A0M;
            try {
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A02("DiskCacheConfig.getDefaultMainDiskCacheConfig");
                }
                r12 = new C23231Os(context2).A00();
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A01();
                }
            } catch (Throwable th) {
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A01();
                }
                throw th;
            }
        }
        this.A03 = r12;
        C14320t5 r06 = r6.A06;
        this.A09 = r06 == null ? BTD.A00() : r06;
        long j = this.A0F.A08;
        if (j != 2 || Build.VERSION.SDK_INT < 27) {
            i = 0;
            if (j == 1) {
                i = 1;
            }
        } else {
            i = 2;
        }
        this.A00 = i;
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("ImagePipelineConfig->mNetworkFetcher");
        }
        C23281Ox r13 = r6.A0H;
        this.A0K = r13 == null ? new C196679Mz(30000) : r13;
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
        AnonymousClass1N0 r22 = r6.A0G;
        this.A0J = r22 == null ? new AnonymousClass1N0(new AnonymousClass1NA(new C30551iG())) : r22;
        C23301Oz r07 = r6.A0F;
        this.A0I = r07 == null ? new AnonymousClass1P2() : r07;
        Set set = r6.A0J;
        this.A0N = set == null ? new HashSet() : set;
        Set set2 = r6.A0I;
        this.A0M = set2 == null ? new HashSet() : set2;
        this.A0P = r6.A0L;
        C23251Ou r08 = r6.A01;
        this.A04 = r08 == null ? this.A03 : r08;
        int i2 = this.A0J.A08.A04.A01;
        C22641Mg r09 = r6.A0B;
        this.A0D = r09 == null ? new C192368zf(i2) : r09;
        this.A05 = r6.A02;
        this.A0G = r6.A0D;
        AnonymousClass1PU r010 = this.A0F;
        WebpBitmapFactoryImpl webpBitmapFactoryImpl = r010.A0D;
        if (webpBitmapFactoryImpl != null) {
            C191768xR r23 = new C191768xR(this.A0J);
            AnonymousClass1PU r011 = this.A0F;
            AnonymousClass1Q4.A00 = webpBitmapFactoryImpl;
            C84013yd r012 = r011.A0C;
            if (r012 != null) {
                webpBitmapFactoryImpl.setWebpErrorLogger(r012);
            }
            webpBitmapFactoryImpl.setBitmapCreator(r23);
        } else if (r010.A0K && AnonymousClass1Q4.A03 && (A002 = AnonymousClass1Q4.A00()) != null) {
            C191768xR r14 = new C191768xR(this.A0J);
            AnonymousClass1PU r013 = this.A0F;
            AnonymousClass1Q4.A00 = A002;
            C84013yd r014 = r013.A0C;
            if (r014 != null) {
                A002.setWebpErrorLogger(r014);
            }
            A002.setBitmapCreator(r14);
        }
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
    }
}
