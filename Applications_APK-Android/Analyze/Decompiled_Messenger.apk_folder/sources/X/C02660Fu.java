package X;

import android.util.SparseArray;

/* renamed from: X.0Fu  reason: invalid class name and case insensitive filesystem */
public final class C02660Fu extends AnonymousClass0FM {
    private static final C02700Fy A00 = new C02700Fy();
    public boolean isAttributionEnabled;
    public final SparseArray sensorConsumption;
    public final C02700Fy total;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C02660Fu r5 = (C02660Fu) obj;
            if (this.isAttributionEnabled != r5.isAttributionEnabled || !this.total.equals(r5.total) || !C02740Gd.A01(this.sensorConsumption, r5.sensorConsumption)) {
                return false;
            }
        }
        return true;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A09((C02660Fu) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02660Fu r52 = (C02660Fu) r5;
        C02660Fu r62 = (C02660Fu) r6;
        if (r62 == null) {
            r62 = new C02660Fu(this.isAttributionEnabled);
        }
        if (r52 == null) {
            r62.A09(this);
        } else {
            this.total.A07(r52.total, r62.total);
            if (r62.isAttributionEnabled) {
                A00(-1, this.sensorConsumption, r52.sensorConsumption, r62.sensorConsumption);
                return r62;
            }
        }
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02660Fu r52 = (C02660Fu) r5;
        C02660Fu r62 = (C02660Fu) r6;
        if (r62 == null) {
            r62 = new C02660Fu(this.isAttributionEnabled);
        }
        if (r52 == null) {
            r62.A09(this);
        } else {
            this.total.A08(r52.total, r62.total);
            if (r62.isAttributionEnabled) {
                A00(1, this.sensorConsumption, r52.sensorConsumption, r62.sensorConsumption);
                return r62;
            }
        }
        return r62;
    }

    public void A09(C02660Fu r6) {
        this.total.A0B(r6.total);
        if (this.isAttributionEnabled && r6.isAttributionEnabled) {
            this.sensorConsumption.clear();
            int size = r6.sensorConsumption.size();
            for (int i = 0; i < size; i++) {
                this.sensorConsumption.put(r6.sensorConsumption.keyAt(i), r6.sensorConsumption.valueAt(i));
            }
        }
    }

    public int hashCode() {
        return ((((this.isAttributionEnabled ? 1 : 0) * true) + this.total.hashCode()) * 31) + this.sensorConsumption.hashCode();
    }

    public String toString() {
        return "SensorMetrics{isAttributionEnabled=" + this.isAttributionEnabled + ", total=" + this.total + ", sensorConsumption=" + this.sensorConsumption + '}';
    }

    private static void A00(int i, SparseArray sparseArray, SparseArray sparseArray2, SparseArray sparseArray3) {
        AnonymousClass0FM A07;
        AnonymousClass0FM A072;
        sparseArray3.clear();
        int size = sparseArray.size();
        for (int i2 = 0; i2 < size; i2++) {
            int keyAt = sparseArray.keyAt(i2);
            C02700Fy r2 = (C02700Fy) sparseArray.valueAt(i2);
            AnonymousClass0FM r1 = (AnonymousClass0FM) sparseArray2.get(keyAt, A00);
            if (i > 0) {
                A072 = r2.A08(r1, null);
            } else {
                A072 = r2.A07(r1, null);
            }
            C02700Fy r0 = (C02700Fy) A072;
            if (!A00.equals(r0)) {
                sparseArray3.put(keyAt, r0);
            }
        }
        int size2 = sparseArray2.size();
        for (int i3 = 0; i3 < size2; i3++) {
            int keyAt2 = sparseArray2.keyAt(i3);
            if (sparseArray.get(keyAt2) == null) {
                C02700Fy r22 = A00;
                AnonymousClass0FM r12 = (AnonymousClass0FM) sparseArray2.valueAt(i3);
                if (i > 0) {
                    A07 = r22.A08(r12, null);
                } else {
                    A07 = r22.A07(r12, null);
                }
                C02700Fy r02 = (C02700Fy) A07;
                if (!r22.equals(r02)) {
                    sparseArray3.put(keyAt2, r02);
                }
            }
        }
    }

    public C02660Fu() {
        this(false);
    }

    public C02660Fu(boolean z) {
        this.total = new C02700Fy();
        this.sensorConsumption = new SparseArray();
        this.isAttributionEnabled = z;
    }
}
