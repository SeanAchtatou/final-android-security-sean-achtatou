package X;

import com.facebook.messaging.inbox2.items.InboxTrackableItem;

/* renamed from: X.1Ad  reason: invalid class name and case insensitive filesystem */
public final class C19901Ad implements AnonymousClass1AZ {
    private final C19911Ae A00;

    public void C8h(boolean z) {
    }

    public boolean CEL() {
        return true;
    }

    public static final C19901Ad A00(AnonymousClass1XY r1) {
        return new C19901Ad(r1);
    }

    public C33901oK Apl() {
        return C33901oK.A0b;
    }

    public void BMG(C24201Sr r7) {
        C19911Ae r5 = this.A00;
        InboxTrackableItem inboxTrackableItem = (InboxTrackableItem) r7.A05;
        C33901oK r4 = inboxTrackableItem.A04;
        long ArZ = inboxTrackableItem.ArZ();
        C17610zB r1 = r5.A01;
        if (r1.A02(ArZ) >= 0) {
            r1.A0B(ArZ);
        }
        r5.A00.A0D(ArZ, r4);
    }

    public void BMH(C24201Sr r7) {
        C19911Ae r5 = this.A00;
        InboxTrackableItem inboxTrackableItem = (InboxTrackableItem) r7.A05;
        C33901oK r4 = inboxTrackableItem.A04;
        long ArZ = inboxTrackableItem.ArZ();
        C17610zB r1 = r5.A00;
        if (r1.A02(ArZ) >= 0) {
            r1.A0B(ArZ);
            r5.A01.A0D(ArZ, r4);
        }
    }

    public void CKW(C24201Sr r7, boolean z) {
        if (!z) {
            C19911Ae r5 = this.A00;
            InboxTrackableItem inboxTrackableItem = (InboxTrackableItem) r7.A05;
            C33901oK r4 = inboxTrackableItem.A04;
            long ArZ = inboxTrackableItem.ArZ();
            C17610zB r1 = r5.A00;
            if (r1.A02(ArZ) >= 0) {
                r1.A0B(ArZ);
                r5.A01.A0D(ArZ, r4);
            }
        } else if (r7.A08) {
            C19911Ae r52 = this.A00;
            InboxTrackableItem inboxTrackableItem2 = (InboxTrackableItem) r7.A05;
            C33901oK r42 = inboxTrackableItem2.A04;
            long ArZ2 = inboxTrackableItem2.ArZ();
            C17610zB r12 = r52.A01;
            if (r12.A02(ArZ2) >= 0) {
                r12.A0B(ArZ2);
            }
            r52.A00.A0D(ArZ2, r42);
        }
    }

    private C19901Ad(AnonymousClass1XY r2) {
        this.A00 = C19911Ae.A00(r2);
    }
}
