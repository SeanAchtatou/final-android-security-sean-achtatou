package X;

import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ET  reason: invalid class name */
public final class AnonymousClass1ET {
    private static volatile AnonymousClass1ET A01;
    public AnonymousClass0UN A00;

    public static boolean A01(String str, Set set) {
        if (str != null) {
            Iterator it = set.iterator();
            while (it.hasNext()) {
                if (((Pattern) it.next()).matcher(str).matches()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static final AnonymousClass1ET A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1ET.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1ET(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass1ET(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
