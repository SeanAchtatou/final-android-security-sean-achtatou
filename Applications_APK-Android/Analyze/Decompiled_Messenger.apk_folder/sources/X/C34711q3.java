package X;

import com.facebook.fbservice.service.BlueServiceLogic;
import com.google.common.base.Preconditions;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1q3  reason: invalid class name and case insensitive filesystem */
public final class C34711q3 {
    private static volatile C34711q3 A05;
    public int A00;
    public AnonymousClass0UN A01;
    public boolean A02;
    public final Runnable A03 = new C34721q4(this);
    private final boolean A04;

    public synchronized void A01() {
        int i = this.A00;
        boolean z = false;
        if (i >= 1) {
            z = true;
        }
        Preconditions.checkState(z);
        int i2 = i - 1;
        this.A00 = i2;
        if (i2 == 0) {
            if (!this.A04) {
                ((BlueServiceLogic) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AVT, this.A01)).A03();
            } else if (!this.A02) {
                this.A02 = true;
                int i3 = AnonymousClass1Y3.A8a;
                AnonymousClass0UN r1 = this.A01;
                ((AnonymousClass0WP) AnonymousClass1XX.A02(2, i3, r1)).CIB("BlueServiceRunning.stopQueuesIfIdle", this.A03, AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_LOW_PRIORITY, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aoc, r1));
            }
        }
    }

    public synchronized void A02() {
        this.A00++;
    }

    public static final C34711q3 A00(AnonymousClass1XY r5) {
        if (A05 == null) {
            synchronized (C34711q3.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A05 = new C34711q3(applicationInjector, AnonymousClass0WT.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    private C34711q3(AnonymousClass1XY r3, C25051Yd r4) {
        this.A01 = new AnonymousClass0UN(3, r3);
        this.A04 = r4.Aem(282909497100296L);
    }
}
