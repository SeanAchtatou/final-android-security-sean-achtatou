package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.messaging.business.share.util.PlatformShareExtras;
import com.google.common.base.Platform;

/* renamed from: X.1zp  reason: invalid class name and case insensitive filesystem */
public final class C39921zp implements C189808s5 {
    public Intent AQj(Context context, Bundle bundle) {
        String string = bundle.getString("cta_id");
        if (Platform.stringIsNullOrEmpty(string)) {
            return null;
        }
        Intent intent = new Intent(C60982y9.A03);
        intent.setData(Uri.parse(C52652jT.A0W));
        intent.putExtra("ShareType", "ShareType.platformItem");
        C39931zq r0 = new C39931zq();
        r0.A00 = string;
        intent.putExtra(C22298Ase.$const$string(4), new PlatformShareExtras(r0));
        intent.putExtra("title", context.getString(2131821265));
        return intent;
    }
}
