package X;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import java.util.Arrays;

/* renamed from: X.0wN  reason: invalid class name and case insensitive filesystem */
public final class C16030wN {
    public final ImmutableList A00;
    public final ImmutableList A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C16030wN r5 = (C16030wN) obj;
            if (!Objects.equal(this.A00, r5.A00) || !Objects.equal(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A01});
    }

    public C16030wN(ImmutableList immutableList, ImmutableList immutableList2) {
        this.A00 = immutableList;
        this.A01 = immutableList2;
    }
}
