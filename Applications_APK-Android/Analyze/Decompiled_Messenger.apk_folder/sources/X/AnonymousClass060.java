package X;

import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.systrace.TraceDirect;
import java.util.IllegalFormatException;

/* renamed from: X.060  reason: invalid class name */
public final class AnonymousClass060 {
    static {
        new AnonymousClass061();
    }

    public static void A00() {
        if (!AnonymousClass08Z.A05(32)) {
            return;
        }
        if (TraceDirect.checkNative()) {
            TraceDirect.nativeEndSection();
        } else {
            AnonymousClass0HL.A00("E");
        }
    }

    public static void A01(String str, int i, Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        if (AnonymousClass08Z.A05(32)) {
            if (!(i == -1 && (objArr == null || objArr.length == 0))) {
                if (i == 0) {
                    str = StringFormatUtil.formatStrLocaleSafe(str);
                } else if (i == 1) {
                    str = StringFormatUtil.formatStrLocaleSafe(str, obj);
                } else if (i == 2) {
                    str = StringFormatUtil.formatStrLocaleSafe(str, obj, obj2);
                } else if (i == 3) {
                    str = StringFormatUtil.formatStrLocaleSafe(str, obj, obj2, obj3);
                } else if (i != 4) {
                    try {
                        str = StringFormatUtil.formatStrLocaleSafe(str, objArr);
                    } catch (IllegalFormatException e) {
                        C010708t.A0N("Tracer", "Bad format string", e);
                    }
                } else {
                    str = StringFormatUtil.formatStrLocaleSafe(str, obj, obj2, obj3, obj4);
                }
            }
            AnonymousClass08Z.A00(32, str);
        }
    }
}
