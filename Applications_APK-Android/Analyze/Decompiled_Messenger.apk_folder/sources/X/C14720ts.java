package X;

import com.facebook.common.util.TriState;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0ts  reason: invalid class name and case insensitive filesystem */
public final class C14720ts extends AnonymousClass0UV {
    public static final TriState A00(AnonymousClass1XY r1) {
        return AnonymousClass0WA.A00(r1).Ab9(484);
    }

    public static final Boolean A01(AnonymousClass1XY r0) {
        return Boolean.valueOf(C14730tt.A00(r0).A0M());
    }

    public static final Boolean A02(AnonymousClass1XY r0) {
        return Boolean.valueOf(C14730tt.A00(r0).A0P());
    }

    public static final Boolean A03(AnonymousClass1XY r0) {
        return Boolean.valueOf(C14730tt.A00(r0).A0K());
    }
}
