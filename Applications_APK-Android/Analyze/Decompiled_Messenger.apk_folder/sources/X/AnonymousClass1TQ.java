package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1TQ  reason: invalid class name */
public final class AnonymousClass1TQ {
    private static C05540Zi A01;
    private AnonymousClass0UN A00;

    public static final AnonymousClass1TQ A00(AnonymousClass1XY r4) {
        AnonymousClass1TQ r0;
        synchronized (AnonymousClass1TQ.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new AnonymousClass1TQ((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass1TQ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public AnonymousClass0VL A01(long j) {
        int i;
        int i2;
        int i3;
        int i4;
        if (j == 900) {
            i3 = 5;
            i4 = AnonymousClass1Y3.AJD;
        } else if (j == 300) {
            i3 = 2;
            i4 = AnonymousClass1Y3.AsN;
        } else {
            if (j == 700) {
                i = 3;
                i2 = AnonymousClass1Y3.A4I;
            } else if (j == 100) {
                i = 0;
                i2 = AnonymousClass1Y3.Am5;
            } else if (j == 800) {
                i = 4;
                i2 = AnonymousClass1Y3.AVD;
            } else {
                i = 1;
                i2 = AnonymousClass1Y3.AZx;
            }
            return (AnonymousClass0VL) AnonymousClass1XX.A02(i, i2, this.A00);
        }
        return (C05160Xw) AnonymousClass1XX.A02(i3, i4, this.A00);
    }

    private AnonymousClass1TQ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
    }
}
