package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.ThreadParticipant;

/* renamed from: X.116  reason: invalid class name */
public final class AnonymousClass116 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadParticipant(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadParticipant[i];
    }
}
