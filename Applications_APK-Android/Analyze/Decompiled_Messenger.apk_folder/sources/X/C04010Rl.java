package X;

import com.facebook.acra.LogCatCollector;
import java.io.DataInputStream;

/* renamed from: X.0Rl  reason: invalid class name and case insensitive filesystem */
public class C04010Rl {
    public int A00;
    public C01990Ck A01;

    public C04010Rl(C01990Ck r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    public String A00(DataInputStream dataInputStream) {
        int readUnsignedByte = dataInputStream.readUnsignedByte();
        this.A00 -= 2;
        int readUnsignedByte2 = dataInputStream.readUnsignedByte() | (readUnsignedByte << 8);
        byte[] bArr = new byte[readUnsignedByte2];
        dataInputStream.readFully(bArr);
        this.A00 -= readUnsignedByte2;
        return new String(bArr, LogCatCollector.UTF_8_ENCODING);
    }
}
