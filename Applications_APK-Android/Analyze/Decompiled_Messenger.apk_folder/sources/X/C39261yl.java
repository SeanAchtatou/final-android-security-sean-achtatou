package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import com.facebook.payments.picker.model.CoreClientData;
import com.facebook.payments.picker.model.PickerScreenConfig;
import com.facebook.payments.picker.model.PickerScreenFetcherParams;
import com.facebook.payments.picker.model.SimplePickerRunTimeData;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;

/* renamed from: X.1yl  reason: invalid class name and case insensitive filesystem */
public final class C39261yl extends C23949Bq2 {
    public final /* synthetic */ C23920BpY A00;

    public C39261yl(C23920BpY bpY) {
        this.A00 = bpY;
    }

    public void BvY(C12570pc r4) {
        r4.A27(this.A00.B4m().A0T(), "payments_dialog_fragment", true);
    }

    public void Bwx(C51842hx r12) {
        Intent intent;
        C23920BpY bpY = this.A00;
        switch (r12.A01.intValue()) {
            case 0:
                intent = (Intent) r12.A00("extra_activity_result_data");
                break;
            case 1:
            case 3:
            default:
                return;
            case 2:
                return;
            case 4:
                Parcelable A002 = r12.A00(C22298Ase.$const$string(AnonymousClass1Y3.A1i));
                Preconditions.checkNotNull(A002);
                if (A002 instanceof CoreClientData) {
                    bpY.A07.A03(bpY.A0A, (CoreClientData) A002);
                    return;
                } else if (A002 instanceof PickerScreenFetcherParams) {
                    bpY.A07.A04(bpY.A0A, (PickerScreenFetcherParams) A002);
                    return;
                } else {
                    return;
                }
            case 5:
                Blb blb = bpY.A07;
                SimplePickerRunTimeData simplePickerRunTimeData = bpY.A0A;
                String $const$string = C22298Ase.$const$string(218);
                String string = r12.A00.getString(C99084oO.$const$string(AnonymousClass1Y3.A3y), null);
                Preconditions.checkNotNull(blb.A00);
                C23719Bld bld = blb.A00;
                PickerScreenConfig pickerScreenConfig = simplePickerRunTimeData.A01;
                PickerScreenFetcherParams pickerScreenFetcherParams = simplePickerRunTimeData.A02;
                CoreClientData coreClientData = simplePickerRunTimeData.A00;
                ImmutableMap immutableMap = simplePickerRunTimeData.A03;
                HashMap hashMap = new HashMap();
                hashMap.putAll(immutableMap);
                hashMap.put((C23704Bkl) r12.A00.getSerializable($const$string), string);
                SimplePickerRunTimeData A02 = blb.A02(pickerScreenConfig, pickerScreenFetcherParams, coreClientData, ImmutableMap.copyOf(hashMap));
                C23920BpY bpY2 = bld.A00;
                bpY2.A0A = A02;
                C23920BpY.A00(bpY2);
                intent = bpY.A0A.A00();
                break;
        }
        C23920BpY.A04(bpY, intent);
        Activity activity = (Activity) AnonymousClass065.A00(bpY.A1j(), Activity.class);
        if (activity != null) {
            activity.finish();
        }
    }

    public void CGq(Intent intent) {
        C417626w.A06(intent, this.A00.A1j());
    }

    public void CGr(Intent intent, int i) {
        C417626w.A01(intent, i, this.A00);
    }

    public void CHG(Intent intent) {
        C417626w.A03(intent, this.A00.A1j());
    }

    public void CHI(Intent intent, int i) {
        C02200Dj.A00().A0D().A0A(intent, i, this.A00);
    }
}
