package X;

import android.content.Intent;
import android.view.View;
import com.facebook.sosource.bsod.BSODActivity;

/* renamed from: X.0Qn  reason: invalid class name */
public final class AnonymousClass0Qn implements View.OnClickListener {
    public final /* synthetic */ BSODActivity A00;
    public final /* synthetic */ String A01;

    public AnonymousClass0Qn(BSODActivity bSODActivity, String str) {
        this.A00 = bSODActivity;
        this.A01 = str;
    }

    public void onClick(View view) {
        int A05 = C000700l.A05(323701038);
        this.A00.startActivity(new Intent(this.A01));
        this.A00.finish();
        C000700l.A0B(1286818915, A05);
    }
}
