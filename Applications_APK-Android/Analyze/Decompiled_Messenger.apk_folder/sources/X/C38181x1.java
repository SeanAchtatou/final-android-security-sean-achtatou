package X;

/* renamed from: X.1x1  reason: invalid class name and case insensitive filesystem */
public final class C38181x1 implements Runnable {
    public static final String __redex_internal_original_name = "com.google.android.exoplayer2.source.MediaSourceEventListener$EventDispatcher$3";
    public final /* synthetic */ C21439AEg A00;
    public final /* synthetic */ C21422ADo A01;
    public final /* synthetic */ C38031wm A02;
    public final /* synthetic */ C38211x4 A03;

    public C38181x1(C21439AEg aEg, C38211x4 r2, C21422ADo aDo, C38031wm r4) {
        this.A00 = aEg;
        this.A03 = r2;
        this.A01 = aDo;
        this.A02 = r4;
    }

    public void run() {
        C38211x4 r4 = this.A03;
        C21439AEg aEg = this.A00;
        r4.BdD(aEg.A00, aEg.A01, this.A01, this.A02);
    }
}
