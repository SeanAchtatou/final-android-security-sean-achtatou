package X;

/* renamed from: X.1nw  reason: invalid class name and case insensitive filesystem */
public final class C33661nw {
    private AnonymousClass0UN A00;
    private C55632oP A01;
    private final C33671nx A02;

    public static final C33661nw A00(AnonymousClass1XY r1) {
        return new C33661nw(r1);
    }

    public C55632oP A01() {
        if (this.A01 == null && this.A02.A00.Aem(282479999059383L)) {
            this.A01 = (C55632oP) AnonymousClass1XX.A03(AnonymousClass1Y3.B99, this.A00);
        }
        return this.A01;
    }

    private C33661nw(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A02 = C33671nx.A00(r3);
    }
}
