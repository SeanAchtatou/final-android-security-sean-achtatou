package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Ic  reason: invalid class name and case insensitive filesystem */
public abstract class C21631Ic implements C21681Ih {
    private Map A00;
    private Map A01;

    public C637038i Aht() {
        boolean z = this instanceof C21621Ib;
        return null;
    }

    public String Asm() {
        boolean z = this instanceof C21621Ib;
        return null;
    }

    public boolean BBN() {
        boolean z = this instanceof AnonymousClass1IL;
        return false;
    }

    public boolean C2H() {
        return this instanceof C21621Ib;
    }

    public boolean C2K() {
        return this instanceof AnonymousClass1IL;
    }

    public void AMi(String str, Object obj) {
        if (this.A00 == null) {
            this.A00 = Collections.synchronizedMap(new HashMap());
        }
        this.A00.put(str, obj);
    }

    public void AMk(String str, Object obj) {
        if (this.A01 == null) {
            this.A01 = Collections.synchronizedMap(new HashMap());
        }
        this.A01.put(str, obj);
    }

    public C17770zR Ahm() {
        if (this instanceof C21621Ib) {
            return ((C21621Ib) this).A00;
        }
        throw new UnsupportedOperationException();
    }

    public Object AjM(String str) {
        Map map = this.A00;
        if (map == null) {
            return null;
        }
        return map.get(str);
    }

    public Object Ajj(String str) {
        Map map = this.A01;
        if (map == null) {
            return null;
        }
        return map.get(str);
    }

    public AnonymousClass10N B0i() {
        if (this instanceof C21621Ib) {
            return null;
        }
        throw new UnsupportedOperationException();
    }

    public int B3W() {
        Map map = this.A00;
        if (map == null || !map.containsKey("span_size")) {
            return 1;
        }
        return ((Integer) this.A00.get("span_size")).intValue();
    }

    public C78893ph B96() {
        if (this instanceof AnonymousClass1IL) {
            return ((AnonymousClass1IL) this).A01;
        }
        throw new UnsupportedOperationException();
    }

    public C37481vk B9A() {
        if (this instanceof AnonymousClass1IL) {
            return ((AnonymousClass1IL) this).A02;
        }
        throw new UnsupportedOperationException();
    }

    public int B9K() {
        if (this instanceof AnonymousClass1IL) {
            return ((AnonymousClass1IL) this).A00;
        }
        throw new UnsupportedOperationException();
    }

    public boolean BFA() {
        Map map = this.A00;
        if (map == null || !map.containsKey("is_full_span")) {
            return false;
        }
        return ((Boolean) this.A00.get("is_full_span")).booleanValue();
    }

    public boolean BGx() {
        Map map = this.A00;
        if (map == null || !map.containsKey("is_sticky")) {
            return false;
        }
        return ((Boolean) this.A00.get("is_sticky")).booleanValue();
    }

    public void CDA(int i) {
        if (!(this instanceof AnonymousClass1IL)) {
            throw new UnsupportedOperationException();
        }
        ((AnonymousClass1IL) this).A00 = i;
    }

    public C21631Ic(C21671Ig r2) {
        this.A00 = r2.A00;
        this.A01 = r2.A01;
    }
}
