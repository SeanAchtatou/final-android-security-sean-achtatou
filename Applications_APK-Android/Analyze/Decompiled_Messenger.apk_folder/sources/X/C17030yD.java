package X;

import android.text.Layout;
import android.text.TextUtils;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.0yD  reason: invalid class name and case insensitive filesystem */
public final class C17030yD extends C17770zR {
    public static final Layout.Alignment A0A = Layout.Alignment.ALIGN_NORMAL;
    public static final TextUtils.TruncateAt A0B = TextUtils.TruncateAt.END;
    public static final C22311Kv A0C = C22181Kf.A04;
    @Comparable(type = 0)
    public float A00;
    @Comparable(type = 3)
    public int A01 = Integer.MAX_VALUE;
    @Comparable(type = 13)
    public Layout.Alignment A02 = A0A;
    @Comparable(type = 13)
    public TextUtils.TruncateAt A03 = A0B;
    @Comparable(type = 13)
    public C22311Kv A04 = A0C;
    @Comparable(type = 13)
    public MigColorScheme A05;
    @Comparable(type = 13)
    public AnonymousClass10K A06;
    @Comparable(type = 13)
    public CharSequence A07;
    @Comparable(type = 3)
    public boolean A08;
    @Comparable(type = 3)
    public boolean A09;

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r3) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(10);
        ComponentBuilderCBuilderShape0_0S0300000.A0A(componentBuilderCBuilderShape0_0S0300000, r3, 0, 0, new C17030yD());
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public C17030yD() {
        super("MigText");
    }
}
