package X;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0Gs  reason: invalid class name and case insensitive filesystem */
public final class C02870Gs implements C02780Gi {
    public boolean A00 = true;

    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        C02590Fn r62 = (C02590Fn) r6;
        long j = r62.heldTimeMs;
        if (j != 0) {
            r7.AMV("wakelock_held_time_ms", j);
        }
        long j2 = r62.acquiredCount;
        if (j2 != 0) {
            r7.AMV("wakelock_acquired_count", j2);
        }
        if (this.A00) {
            try {
                JSONObject A09 = r62.A09();
                if (A09 != null) {
                    r7.AMW("wakelock_tag_time_ms", A09.toString());
                }
            } catch (JSONException e) {
                AnonymousClass0KZ.A00("WakeLockMetricsReporter", "Failed to serialize wakelock attribution data", e);
            }
        }
    }
}
