package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1nO  reason: invalid class name and case insensitive filesystem */
public final class C33321nO extends Enum {
    private static final /* synthetic */ C33321nO[] A00;
    public static final C33321nO A01;
    public static final C33321nO A02;

    static {
        C33321nO r4 = new C33321nO("NONE", 0);
        A01 = r4;
        C33321nO r3 = new C33321nO("MEDIA", 1);
        C33321nO r2 = new C33321nO("WAVE", 2);
        A02 = r2;
        A00 = new C33321nO[]{r4, r3, r2};
    }

    public static C33321nO valueOf(String str) {
        return (C33321nO) Enum.valueOf(C33321nO.class, str);
    }

    public static C33321nO[] values() {
        return (C33321nO[]) A00.clone();
    }

    private C33321nO(String str, int i) {
    }
}
