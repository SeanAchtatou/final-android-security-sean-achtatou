package X;

import java.util.BitSet;

/* renamed from: X.1Gb  reason: invalid class name and case insensitive filesystem */
public final class C21311Gb extends AnonymousClass1CY {
    public C1290963f A00;
    public final BitSet A01 = new BitSet(3);
    public final String[] A02 = {"edgeRenderEventHandler", "isSameItemEventHandler", "paginableList"};

    public AnonymousClass1CY A02(String str) {
        super.A02(str);
        return this;
    }
}
