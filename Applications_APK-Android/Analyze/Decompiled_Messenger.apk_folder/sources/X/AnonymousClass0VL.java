package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0VL  reason: invalid class name */
public interface AnonymousClass0VL extends ExecutorService {
    ListenableFuture CIC(Runnable runnable);

    ListenableFuture CIE(Runnable runnable, Object obj);

    ListenableFuture CIF(Callable callable);
}
