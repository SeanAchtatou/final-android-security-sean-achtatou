package X;

import com.facebook.proxygen.AnalyticsLogger;
import java.util.Map;

/* renamed from: X.0xr  reason: invalid class name and case insensitive filesystem */
public final class C16840xr implements AnalyticsLogger {
    private final C06230bA A00;

    public void reportEvent(Map map, String str, String str2) {
        if (map != null && str != null && str2 != null) {
            AnonymousClass0ZF A03 = this.A00.A03(C32411li.A00(str2, str));
            if (A03.A0G()) {
                A03.A0F(map);
                A03.A0E();
            }
        }
    }

    public C16840xr(C06230bA r1) {
        this.A00 = r1;
    }
}
