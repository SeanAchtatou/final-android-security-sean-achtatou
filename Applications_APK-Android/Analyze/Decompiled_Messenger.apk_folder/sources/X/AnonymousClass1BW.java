package X;

/* renamed from: X.1BW  reason: invalid class name */
public final class AnonymousClass1BW {
    public static Integer A00(String str) {
        if (str.equals("COMMERCE_PAGE_TYPE_AGENT")) {
            return AnonymousClass07B.A00;
        }
        if (str.equals("COMMERCE_PAGE_TYPE_BANK")) {
            return AnonymousClass07B.A01;
        }
        if (str.equals("COMMERCE_PAGE_TYPE_BUSINESS")) {
            return AnonymousClass07B.A0C;
        }
        if (str.equals("COMMERCE_PAGE_TYPE_RIDE_SHARE")) {
            return AnonymousClass07B.A0N;
        }
        if (str.equals("COMMERCE_PAGE_TYPE_UNKNOWN")) {
            return AnonymousClass07B.A0Y;
        }
        if (str.equals("COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION")) {
            return AnonymousClass07B.A0i;
        }
        throw new IllegalArgumentException(str);
    }

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "COMMERCE_PAGE_TYPE_BANK";
            case 2:
                return "COMMERCE_PAGE_TYPE_BUSINESS";
            case 3:
                return "COMMERCE_PAGE_TYPE_RIDE_SHARE";
            case 4:
                return "COMMERCE_PAGE_TYPE_UNKNOWN";
            case 5:
                return "COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION";
            default:
                return "COMMERCE_PAGE_TYPE_AGENT";
        }
    }
}
