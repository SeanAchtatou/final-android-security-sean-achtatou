package X;

import com.facebook.common.dextricks.DexStore;
import java.util.Arrays;

/* renamed from: X.11z  reason: invalid class name */
public final class AnonymousClass11z {
    private static final byte[] HEX_BYTES;
    private static final char[] HEX_CHARS;
    public static final int[] sHexValues;
    public static final int[] sInputCodes;
    public static final int[] sInputCodesComment;
    public static final int[] sInputCodesJsNames;
    public static final int[] sInputCodesUtf8;
    public static final int[] sInputCodesUtf8JsNames;
    public static final int[] sOutputEscapes128;

    static {
        char[] charArray = "0123456789ABCDEF".toCharArray();
        HEX_CHARS = charArray;
        int length = charArray.length;
        byte[] bArr = new byte[length];
        HEX_BYTES = bArr;
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) charArray[i];
        }
        int[] iArr = new int[DexStore.LOAD_RESULT_OATMEAL_QUICKENED];
        for (int i2 = 0; i2 < 32; i2++) {
            iArr[i2] = -1;
        }
        iArr[34] = 1;
        iArr[92] = 1;
        sInputCodes = iArr;
        int[] iArr2 = new int[DexStore.LOAD_RESULT_OATMEAL_QUICKENED];
        System.arraycopy(iArr, 0, iArr2, 0, DexStore.LOAD_RESULT_OATMEAL_QUICKENED);
        for (int i3 = 128; i3 < 256; i3++) {
            int i4 = 2;
            if ((i3 & 224) != 192) {
                i4 = 3;
                if ((i3 & AnonymousClass1Y3.A1q) != 224) {
                    i4 = -1;
                    if ((i3 & 248) == 240) {
                        i4 = 4;
                    }
                }
            }
            iArr2[i3] = i4;
        }
        sInputCodesUtf8 = iArr2;
        int[] iArr3 = new int[DexStore.LOAD_RESULT_OATMEAL_QUICKENED];
        Arrays.fill(iArr3, -1);
        for (int i5 = 33; i5 < 256; i5++) {
            if (Character.isJavaIdentifierPart((char) i5)) {
                iArr3[i5] = 0;
            }
        }
        iArr3[64] = 0;
        iArr3[35] = 0;
        iArr3[42] = 0;
        iArr3[45] = 0;
        iArr3[43] = 0;
        sInputCodesJsNames = iArr3;
        int[] iArr4 = new int[DexStore.LOAD_RESULT_OATMEAL_QUICKENED];
        System.arraycopy(iArr3, 0, iArr4, 0, DexStore.LOAD_RESULT_OATMEAL_QUICKENED);
        Arrays.fill(iArr4, 128, 128, 0);
        sInputCodesUtf8JsNames = iArr4;
        int[] iArr5 = new int[DexStore.LOAD_RESULT_OATMEAL_QUICKENED];
        sInputCodesComment = iArr5;
        System.arraycopy(sInputCodesUtf8, 128, iArr5, 128, 128);
        int[] iArr6 = sInputCodesComment;
        Arrays.fill(iArr6, 0, 32, -1);
        iArr6[9] = 0;
        iArr6[10] = 10;
        iArr6[13] = 13;
        iArr6[42] = 42;
        int[] iArr7 = new int[128];
        for (int i6 = 0; i6 < 32; i6++) {
            iArr7[i6] = -1;
        }
        iArr7[34] = 34;
        iArr7[92] = 92;
        iArr7[8] = 98;
        iArr7[9] = 116;
        iArr7[12] = 102;
        iArr7[10] = 110;
        iArr7[13] = 114;
        sOutputEscapes128 = iArr7;
        int[] iArr8 = new int[128];
        sHexValues = iArr8;
        Arrays.fill(iArr8, -1);
        for (int i7 = 0; i7 < 10; i7++) {
            sHexValues[i7 + 48] = i7;
        }
        for (int i8 = 0; i8 < 6; i8++) {
            int[] iArr9 = sHexValues;
            int i9 = i8 + 10;
            iArr9[i8 + 97] = i9;
            iArr9[i8 + 65] = i9;
        }
    }

    public static void appendQuoted(StringBuilder sb, String str) {
        int i;
        int[] iArr = sOutputEscapes128;
        int length = iArr.length;
        int length2 = str.length();
        for (int i2 = 0; i2 < length2; i2++) {
            char charAt = str.charAt(i2);
            if (charAt >= length || (i = iArr[charAt]) == 0) {
                sb.append(charAt);
            } else {
                sb.append('\\');
                if (i < 0) {
                    sb.append('u');
                    sb.append('0');
                    sb.append('0');
                    char[] cArr = HEX_CHARS;
                    sb.append(cArr[charAt >> 4]);
                    sb.append(cArr[charAt & 15]);
                } else {
                    sb.append((char) i);
                }
            }
        }
    }

    public static byte[] copyHexBytes() {
        return (byte[]) HEX_BYTES.clone();
    }

    public static char[] copyHexChars() {
        return (char[]) HEX_CHARS.clone();
    }

    private AnonymousClass11z() {
    }
}
