package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1jZ  reason: invalid class name and case insensitive filesystem */
public final class C31361jZ implements AnonymousClass1CR {
    private static volatile C31361jZ A04;
    private AnonymousClass0UN A00;
    private Integer A01;
    public final AnonymousClass04b A02 = new AnonymousClass04b();
    public final C25051Yd A03;

    public synchronized boolean Aen(long j, String str) {
        boolean z;
        AnonymousClass04b r0 = this.A02;
        Long valueOf = Long.valueOf(j);
        Object obj = r0.get(valueOf);
        if (obj instanceof Boolean) {
            z = ((Boolean) obj).booleanValue();
            Boolean valueOf2 = Boolean.valueOf(z);
            boolean Aem = this.A03.Aem(j);
            if (z != Aem) {
                A01(str, valueOf2, Boolean.valueOf(Aem));
            }
        } else {
            z = this.A03.Aem(j);
            this.A02.put(valueOf, Boolean.valueOf(z));
        }
        return z;
    }

    public synchronized int AqM(long j, String str) {
        long j2;
        synchronized (this) {
            AnonymousClass04b r0 = this.A02;
            Long valueOf = Long.valueOf(j);
            Object obj = r0.get(valueOf);
            if (obj instanceof Long) {
                j2 = ((Long) obj).longValue();
                Long valueOf2 = Long.valueOf(j2);
                long At0 = this.A03.At0(j);
                if (j2 != At0) {
                    A01(str, valueOf2, Long.valueOf(At0));
                }
            } else {
                j2 = this.A03.At0(j);
                this.A02.put(valueOf, Long.valueOf(j2));
            }
        }
        return (int) j2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0021, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String B4E(long r5, java.lang.String r7) {
        /*
            r4 = this;
            monitor-enter(r4)
            X.04b r0 = r4.A02     // Catch:{ all -> 0x002f }
            java.lang.Long r3 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x002f }
            java.lang.Object r2 = r0.get(r3)     // Catch:{ all -> 0x002f }
            boolean r0 = r2 instanceof java.lang.String     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x0022
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x002f }
            X.1Yd r0 = r4.A03     // Catch:{ all -> 0x002f }
            java.lang.String r1 = r0.B4C(r5)     // Catch:{ all -> 0x002f }
            boolean r0 = X.C06850cB.A0C(r2, r1)     // Catch:{ all -> 0x002f }
            if (r0 != 0) goto L_0x0020
            r4.A01(r7, r2, r1)     // Catch:{ all -> 0x002f }
        L_0x0020:
            monitor-exit(r4)
            return r2
        L_0x0022:
            X.1Yd r0 = r4.A03     // Catch:{ all -> 0x002f }
            java.lang.String r1 = r0.B4C(r5)     // Catch:{ all -> 0x002f }
            X.04b r0 = r4.A02     // Catch:{ all -> 0x002f }
            r0.put(r3, r1)     // Catch:{ all -> 0x002f }
            monitor-exit(r4)
            return r1
        L_0x002f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31361jZ.B4E(long, java.lang.String):java.lang.String");
    }

    public static final C31361jZ A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C31361jZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C31361jZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private void A01(String str, Object obj, Object obj2) {
        String A002;
        StringBuilder sb = new StringBuilder("android_messenger_search:critical_config_inconsistency:");
        Integer num = this.A01;
        if (num == null) {
            A002 = "null";
        } else {
            A002 = AnonymousClass1FL.A00(num);
        }
        sb.append(A002);
        sb.append(":");
        sb.append(AnonymousClass1FL.A00(this.A03.Aj4(false)));
        sb.append(":");
        sb.append(str);
        sb.append(":");
        sb.append(obj.toString());
        sb.append(":");
        sb.append(obj2.toString());
        String sb2 = sb.toString();
        C010708t.A0I("CachedMobileConfig", sb2);
        ((C07380dK) AnonymousClass1XX.A03(AnonymousClass1Y3.BQ3, this.A00)).A03(sb2);
    }

    public Integer Aj3() {
        Integer Aj4 = this.A03.Aj4(false);
        this.A01 = Aj4;
        return Aj4;
    }

    private C31361jZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A03 = AnonymousClass0WT.A00(r3);
    }
}
