package X;

import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.1up  reason: invalid class name and case insensitive filesystem */
public final class C37181up implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C29791gz A01;
    public final /* synthetic */ Runnable A02;

    public C37181up(C29791gz r1, View view, Runnable runnable) {
        this.A01 = r1;
        this.A00 = view;
        this.A02 = runnable;
    }

    public void onGlobalLayout() {
        C37531vp.A03(this.A00, this);
        AnonymousClass00S.A03((Handler) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amu, this.A01.A00), this.A02, -561016951);
    }
}
