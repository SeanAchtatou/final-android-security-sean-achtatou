package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.fbservice.results.DataFetchDisposition;

/* renamed from: X.0T1  reason: invalid class name */
public final class AnonymousClass0T1 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new DataFetchDisposition(parcel);
    }

    public Object[] newArray(int i) {
        return new DataFetchDisposition[i];
    }
}
