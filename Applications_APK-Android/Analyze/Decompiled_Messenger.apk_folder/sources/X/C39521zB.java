package X;

/* renamed from: X.1zB  reason: invalid class name and case insensitive filesystem */
public enum C39521zB implements C36201sg {
    NO_WARNING(0),
    SHOULD_SHOW_WARNING(1),
    WARNING_DISMISSED(2),
    REPORTED(3),
    BLOCKED(4),
    REINSTATED(5);
    
    private final int value;

    private C39521zB(int i) {
        this.value = i;
    }

    public int getValue() {
        return this.value;
    }
}
