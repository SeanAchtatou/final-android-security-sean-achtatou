package X;

import android.app.Activity;
import android.os.Build;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.08e  reason: invalid class name and case insensitive filesystem */
public final class C009608e {
    public int A00;
    public boolean A01 = false;
    public final WeakHashMap A02 = new WeakHashMap();

    public synchronized int A01(Object obj, AnonymousClass01Y r4) {
        this.A01 = true;
        if (r4 == AnonymousClass01Y.ACTIVITY_STARTED) {
            this.A00++;
        } else if (r4 == AnonymousClass01Y.ACTIVITY_STOPPED) {
            this.A00--;
        }
        if (r4 == AnonymousClass01Y.IN_BACKGROUND || r4 == AnonymousClass01Y.IN_BACKGROUND_DUE_TO_LOW_IMPORTANCE) {
            this.A02.remove(obj);
        } else {
            this.A02.put(obj, r4);
        }
        return this.A00;
    }

    public boolean A02() {
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        synchronized (this) {
            for (Map.Entry entry : this.A02.entrySet()) {
                if (entry.getValue() == AnonymousClass01Y.ACTIVITY_PAUSED || entry.getValue() == AnonymousClass01Y.ACTIVITY_RESUMED || entry.getValue() == AnonymousClass01Y.ACTIVITY_STARTED) {
                    Object key = entry.getKey();
                    if ((key instanceof Activity) && A00((Activity) key)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public static boolean A00(Activity activity) {
        if (activity.isInMultiWindowMode() || activity.isInPictureInPictureMode()) {
            return true;
        }
        return false;
    }
}
