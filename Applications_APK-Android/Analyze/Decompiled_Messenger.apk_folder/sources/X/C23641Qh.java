package X;

import android.content.Context;
import com.facebook.ipc.stories.model.ViewerInfo;
import com.facebook.messaging.model.messages.MontageFeedbackOverlay;
import com.facebook.messaging.model.messages.MontageFeedbackPoll;
import com.facebook.messaging.model.messages.MontageFeedbackPollOption;
import com.facebook.messaging.model.messages.MontageReactionSticker;
import com.facebook.messaging.model.messages.MontageSliderSticker;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.montage.forked.viewer.overlays.slider.model.FbSliderVoteModel;
import com.facebook.messaging.montage.forked.viewer.overlays.slider.model.FbSliderVotesModel;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1Qh  reason: invalid class name and case insensitive filesystem */
public final class C23641Qh extends C20831Dz {
    public int A00;
    public AnonymousClass0UN A01;
    public AnonymousClass3Vz A02;
    public AnonymousClass4K8 A03;
    public MigColorScheme A04;
    public ImmutableList A05;
    public AnonymousClass0V0 A06;
    public String A07;
    public String A08;
    public String A09;
    public List A0A;
    public Map A0B = Collections.emptyMap();
    public Map A0C = Collections.emptyMap();
    public Map A0D = Collections.emptyMap();
    public boolean A0E;
    public boolean A0F;
    private int A0G;
    private List A0H;
    public final Context A0I;
    public final C22351Kz A0J;
    public final List A0K = new ArrayList();
    public final List A0L;

    private String A00(int i) {
        Context context = this.A0I;
        int i2 = 2131828535;
        if (this.A0E) {
            i2 = 2131828390;
        }
        return context.getString(i2, Integer.valueOf(i));
    }

    private void A01() {
        MontageSliderSticker montageSliderSticker;
        FbSliderVotesModel fbSliderVotesModel;
        this.A0K.clear();
        this.A0K.add(new AnonymousClass4KG(0, null));
        List<MontageFeedbackOverlay> list = this.A0H;
        if (list != null) {
            this.A0B = new HashMap();
            this.A0D = new HashMap();
            this.A0C = new HashMap();
            this.A0A = new ArrayList();
            for (MontageFeedbackOverlay montageFeedbackOverlay : list) {
                MontageFeedbackPoll montageFeedbackPoll = montageFeedbackOverlay.A01;
                if (montageFeedbackPoll == null || montageFeedbackPoll.A06.equals("FIVE_OPTION_STAR_RATING")) {
                    MontageReactionSticker montageReactionSticker = montageFeedbackOverlay.A03;
                    if (montageReactionSticker != null) {
                        Preconditions.checkNotNull(montageReactionSticker);
                        if (!C013509w.A02(montageReactionSticker.A02)) {
                            C24971Xv it = montageReactionSticker.A02.iterator();
                            while (it.hasNext()) {
                                this.A0C.put((String) it.next(), montageReactionSticker.A04);
                            }
                        }
                    } else if (!(this.A0G <= 0 || (montageSliderSticker = montageFeedbackOverlay.A05) == null || (fbSliderVotesModel = montageSliderSticker.A01) == null)) {
                        ImmutableList immutableList = fbSliderVotesModel.A00;
                        if (!immutableList.isEmpty()) {
                            MontageSliderSticker montageSliderSticker2 = montageFeedbackOverlay.A05;
                            this.A08 = montageSliderSticker2.A03;
                            this.A05 = immutableList;
                            this.A07 = montageSliderSticker2.A02;
                            this.A0K.add(new AnonymousClass4KG(4, null));
                            MontageSliderSticker montageSliderSticker3 = montageFeedbackOverlay.A05;
                            Preconditions.checkNotNull(montageSliderSticker3);
                            FbSliderVotesModel fbSliderVotesModel2 = montageSliderSticker3.A01;
                            if (fbSliderVotesModel2 != null) {
                                C24971Xv it2 = fbSliderVotesModel2.A00.iterator();
                                while (it2.hasNext()) {
                                    FbSliderVoteModel fbSliderVoteModel = (FbSliderVoteModel) it2.next();
                                    ViewerInfo viewerInfo = fbSliderVoteModel.A01;
                                    if (viewerInfo != null) {
                                        this.A0D.put(viewerInfo.A00, Integer.valueOf((int) fbSliderVoteModel.A00));
                                    }
                                }
                            }
                        }
                    }
                } else {
                    this.A0K.add(new AnonymousClass4KG(3, null));
                    MontageFeedbackPoll montageFeedbackPoll2 = montageFeedbackOverlay.A01;
                    Preconditions.checkNotNull(montageFeedbackPoll2);
                    this.A0F = montageFeedbackPoll2.A09;
                    C24971Xv it3 = montageFeedbackPoll2.A03.iterator();
                    while (it3.hasNext()) {
                        MontageFeedbackPollOption montageFeedbackPollOption = (MontageFeedbackPollOption) it3.next();
                        if (C013509w.A01(montageFeedbackPollOption.A00)) {
                            C24971Xv it4 = montageFeedbackPollOption.A00.iterator();
                            while (it4.hasNext()) {
                                this.A0B.put((String) it4.next(), montageFeedbackPollOption.A04);
                            }
                        }
                    }
                    this.A0A.addAll(montageFeedbackOverlay.A01.A03);
                }
            }
        }
        for (ThreadParticipant r3 : this.A0L) {
            this.A0K.add(new AnonymousClass4KG(1, r3));
        }
        if (this.A00 > 0) {
            this.A0K.add(new AnonymousClass4KG(2, null));
        }
    }

    public void A0F(List list, int i, boolean z, ImmutableList immutableList, AnonymousClass0V0 r6) {
        this.A0L.clear();
        if (list != null) {
            this.A0L.addAll(list);
            this.A0G = list.size();
        }
        this.A0E = z;
        this.A09 = A00(this.A0L.size());
        this.A00 = i;
        this.A0H = immutableList;
        this.A06 = r6;
        A01();
        A04();
    }

    public int ArU() {
        return this.A0K.size();
    }

    public C23641Qh(AnonymousClass1XY r3, Context context, List list, int i, boolean z, List list2, AnonymousClass0V0 r9) {
        ArrayList arrayList;
        this.A01 = new AnonymousClass0UN(2, r3);
        this.A04 = AnonymousClass27E.A00(r3);
        this.A0J = C22341Ky.A00(r3);
        this.A02 = new AnonymousClass3Vz(r3);
        this.A0I = context;
        if (list != null) {
            arrayList = new ArrayList(list);
        }
        this.A0L = arrayList;
        this.A0G = arrayList.size();
        this.A0E = z;
        this.A09 = A00(this.A0L.size());
        this.A00 = i;
        this.A0H = list2;
        this.A06 = r9;
        this.A08 = "😍";
        A01();
    }
}
