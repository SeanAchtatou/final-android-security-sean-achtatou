package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.google.common.collect.ArrayListMultimap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0Uv  reason: invalid class name and case insensitive filesystem */
public final class C04480Uv {
    private static C04480Uv A06;
    private static final Object A07 = new Object();
    public final Context A00;
    public final AnonymousClass0V0 A01 = new ArrayListMultimap();
    public final Map A02 = AnonymousClass0TG.A04();
    public final Map A03 = AnonymousClass0TG.A04();
    public final Map A04 = AnonymousClass0TG.A04();
    private final AnonymousClass0V2 A05;

    public void A02(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        A03(broadcastReceiver, intentFilter, null);
    }

    public void A03(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter, Looper looper) {
        BroadcastReceiver broadcastReceiver2 = broadcastReceiver;
        IntentFilter intentFilter2 = intentFilter;
        if (looper == null) {
            this.A05.A01(broadcastReceiver, intentFilter);
            return;
        }
        synchronized (this.A04) {
            long id = looper.getThread().getId();
            Map map = this.A03;
            Long valueOf = Long.valueOf(id);
            C06810c7 r11 = (C06810c7) map.get(valueOf);
            if (r11 == null) {
                r11 = new C06810c7(this, looper, id);
                this.A03.put(valueOf, r11);
            }
            r11.A01.getAndIncrement();
            C06930cK r6 = new C06930cK(intentFilter2, broadcastReceiver2, id, r11);
            Object obj = (List) this.A04.get(broadcastReceiver);
            if (obj == null) {
                obj = C04300To.A02(1);
                this.A04.put(broadcastReceiver, obj);
            }
            obj.add(intentFilter);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                String action = intentFilter.getAction(i);
                Object obj2 = (List) this.A02.get(action);
                if (obj2 == null) {
                    obj2 = C04300To.A02(1);
                    this.A02.put(action, obj2);
                }
                obj2.add(r6);
            }
        }
    }

    public static C04480Uv A00(Context context) {
        C04480Uv r0;
        synchronized (A07) {
            if (A06 == null) {
                A06 = new C04480Uv(context.getApplicationContext());
            }
            r0 = A06;
        }
        return r0;
    }

    public void A01(BroadcastReceiver broadcastReceiver) {
        synchronized (this.A04) {
            AnonymousClass0V2 r11 = this.A05;
            synchronized (r11.A04) {
                ArrayList arrayList = (ArrayList) r11.A04.remove(broadcastReceiver);
                if (arrayList != null) {
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        C06580bj r5 = (C06580bj) arrayList.get(size);
                        r5.A01 = true;
                        for (int i = 0; i < r5.A03.countActions(); i++) {
                            String action = r5.A03.getAction(i);
                            ArrayList arrayList2 = (ArrayList) r11.A03.get(action);
                            if (arrayList2 != null) {
                                for (int size2 = arrayList2.size() - 1; size2 >= 0; size2--) {
                                    C06580bj r1 = (C06580bj) arrayList2.get(size2);
                                    if (r1.A02 == broadcastReceiver) {
                                        r1.A01 = true;
                                        arrayList2.remove(size2);
                                    }
                                }
                                if (arrayList2.size() <= 0) {
                                    r11.A03.remove(action);
                                }
                            }
                        }
                    }
                }
            }
            List list = (List) this.A04.remove(broadcastReceiver);
            if (list != null) {
                for (int i2 = 0; i2 < list.size(); i2++) {
                    IntentFilter intentFilter = (IntentFilter) list.get(i2);
                    for (int i3 = 0; i3 < intentFilter.countActions(); i3++) {
                        String action2 = intentFilter.getAction(i3);
                        List list2 = (List) this.A02.get(action2);
                        if (list2 != null) {
                            int i4 = 0;
                            while (i4 < list2.size()) {
                                if (((C06930cK) list2.get(i4)).A03 == broadcastReceiver) {
                                    C06930cK r112 = (C06930cK) list2.remove(i4);
                                    boolean z = false;
                                    if (r112.A01.A01.decrementAndGet() <= 0) {
                                        z = true;
                                    }
                                    if (z) {
                                        this.A03.remove(Long.valueOf(r112.A01.A00));
                                    }
                                    i4--;
                                }
                                i4++;
                            }
                            if (list2.isEmpty()) {
                                this.A02.remove(action2);
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean A04(Intent intent) {
        Intent intent2;
        boolean z;
        HashSet<Long> hashSet;
        boolean z2;
        ArrayListMultimap arrayListMultimap;
        boolean z3;
        boolean z4;
        synchronized (this.A04) {
            AnonymousClass0V2 r8 = this.A05;
            synchronized (r8.A04) {
                intent2 = intent;
                try {
                    String action = intent2.getAction();
                    String resolveTypeIfNeeded = intent2.resolveTypeIfNeeded(r8.A00.getContentResolver());
                    Uri data = intent2.getData();
                    String scheme = intent2.getScheme();
                    Set<String> categories = intent2.getCategories();
                    boolean z5 = false;
                    if ((intent2.getFlags() & 8) != 0) {
                        z5 = true;
                    }
                    ArrayList arrayList = (ArrayList) r8.A03.get(intent2.getAction());
                    if (arrayList != null) {
                        ArrayList arrayList2 = null;
                        for (int i = 0; i < arrayList.size(); i++) {
                            C06580bj r3 = (C06580bj) arrayList.get(i);
                            if (!r3.A00) {
                                int match = r3.A03.match(action, resolveTypeIfNeeded, scheme, data, categories, "LocalBroadcastManager");
                                if (match >= 0) {
                                    if (arrayList2 == null) {
                                        arrayList2 = new ArrayList();
                                    }
                                    arrayList2.add(r3);
                                    r3.A00 = true;
                                } else if (z5 && match != -4) {
                                }
                            }
                        }
                        if (arrayList2 != null) {
                            for (int i2 = 0; i2 < arrayList2.size(); i2++) {
                                ((C06580bj) arrayList2.get(i2)).A00 = false;
                            }
                            r8.A02.add(new C21131Fi(intent2, arrayList2));
                            if (!r8.A01.hasMessages(1)) {
                                r8.A01.sendEmptyMessage(1);
                            }
                            z = true;
                        }
                    }
                    z = false;
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                    throw th;
                }
            }
            synchronized (this.A04) {
                try {
                    AnonymousClass0V0 r9 = this.A01;
                    Intent intent3 = intent2;
                    synchronized (this.A04) {
                        String action2 = intent2.getAction();
                        String resolveTypeIfNeeded2 = intent2.resolveTypeIfNeeded(this.A00.getContentResolver());
                        Uri data2 = intent2.getData();
                        String scheme2 = intent2.getScheme();
                        Set<String> categories2 = intent2.getCategories();
                        boolean z6 = false;
                        if ((intent2.getFlags() & 8) != 0) {
                            z6 = true;
                        }
                        List list = (List) this.A02.get(intent2.getAction());
                        hashSet = null;
                        if (list != null) {
                            arrayListMultimap = null;
                            for (int i3 = 0; i3 < list.size(); i3++) {
                                C06930cK r32 = (C06930cK) list.get(i3);
                                if (!r32.A00) {
                                    if (r32.A04.match(action2, resolveTypeIfNeeded2, scheme2, data2, categories2, "LocalBroadcastManager") >= 0) {
                                        if (arrayListMultimap == null) {
                                            arrayListMultimap = new ArrayListMultimap();
                                        }
                                        arrayListMultimap.Byx(Long.valueOf(r32.A02), r32);
                                        r32.A00 = true;
                                    } else if (z6) {
                                    }
                                }
                            }
                            z2 = false;
                        } else {
                            z2 = false;
                            arrayListMultimap = null;
                        }
                        if (arrayListMultimap != null) {
                            for (C06930cK r33 : arrayListMultimap.values()) {
                                r33.A00 = z2;
                            }
                            for (Long l : arrayListMultimap.keySet()) {
                                r9.Byx(l, new AnonymousClass2JZ(intent3, arrayListMultimap.AbK(l)));
                            }
                            hashSet = C25011Xz.A04(arrayListMultimap.keySet());
                        }
                    }
                    if (hashSet != null) {
                        for (Long l2 : hashSet) {
                            Handler handler = (Handler) this.A03.get(l2);
                            if (handler != null && !handler.hasMessages(1)) {
                                handler.sendEmptyMessage(1);
                            }
                        }
                        z3 = true;
                    } else {
                        z3 = false;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
            z4 = z3 | z;
        }
        return z4;
    }

    private C04480Uv(Context context) {
        this.A00 = context;
        this.A05 = AnonymousClass0V2.A00(context);
    }
}
