package X;

import java.io.IOException;

/* renamed from: X.0SV  reason: invalid class name */
public final class AnonymousClass0SV extends IOException {
    public AnonymousClass0DA mDNSResolveStatus;

    public AnonymousClass0SV(AnonymousClass0DA r3) {
        super("Status: " + r3);
        this.mDNSResolveStatus = r3;
    }
}
