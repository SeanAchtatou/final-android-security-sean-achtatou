package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem;

/* renamed from: X.1HV  reason: invalid class name */
public final class AnonymousClass1HV implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new UnifiedPresenceViewLoggerItem(parcel);
    }

    public Object[] newArray(int i) {
        return new UnifiedPresenceViewLoggerItem[i];
    }
}
