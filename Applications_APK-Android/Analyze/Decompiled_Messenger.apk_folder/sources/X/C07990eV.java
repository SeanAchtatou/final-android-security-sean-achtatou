package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eV  reason: invalid class name and case insensitive filesystem */
public final class C07990eV {
    private static volatile C07990eV A02;
    public AnonymousClass0UN A00;
    public final boolean A01;

    public static final C07990eV A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C07990eV.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C07990eV(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C07990eV(AnonymousClass1XY r4) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(2, r4);
        this.A00 = r2;
        this.A01 = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r2)).Aem(287371966815652L);
    }
}
