package X;

/* renamed from: X.0SZ  reason: invalid class name */
public final class AnonymousClass0SZ extends C03550Om {
    private C03550Om[] A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0SZ(android.view.animation.Interpolator r7, X.C03550Om... r8) {
        /*
            r6 = this;
            int r5 = r8.length
            r2 = 0
            r4 = 0
        L_0x0004:
            if (r4 >= r5) goto L_0x0011
            r0 = r8[r4]
            long r0 = r0.A00
            long r2 = java.lang.Math.max(r2, r0)
            int r4 = r4 + 1
            goto L_0x0004
        L_0x0011:
            r6.<init>(r7, r2)
            r6.A00 = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0SZ.<init>(android.view.animation.Interpolator, X.0Om[]):void");
    }

    public void A00(float f) {
        float f2;
        float f3 = f * ((float) this.A00);
        for (C03550Om r6 : this.A00) {
            long j = r6.A00;
            if (j < this.A00) {
                f2 = Math.min(f3, (float) j);
            } else {
                f2 = f3;
            }
            r6.A00(r6.A01.getInterpolation(f2 / ((float) j)));
        }
    }
}
