package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import androidx.fragment.app.Fragment;

/* renamed from: X.0qc  reason: invalid class name and case insensitive filesystem */
public interface C13080qc extends C11460nC {
    void AZu();

    void AZx(Activity activity);

    Object AqZ(Class cls);

    MenuInflater Atv();

    Object Aze(Object obj);

    Resources B1A();

    C13060qW B4m();

    View B8z(int i);

    Window B9w();

    boolean BB8(Throwable th);

    boolean BBy();

    boolean BLI(boolean z);

    void BNB(Bundle bundle);

    void BNF(Intent intent);

    void BNH(int i, int i2, Intent intent);

    void BOX(Fragment fragment);

    void BPZ(Bundle bundle);

    boolean BUO(MenuItem menuItem);

    Dialog BUk(int i);

    boolean BUt(Menu menu);

    boolean BhL(MenuItem menuItem);

    void BjD(Bundle bundle);

    void BjI();

    void BjR(int i, Dialog dialog);

    boolean BjY(Menu menu);

    void BmS();

    void Bmx(Bundle bundle);

    void Btu();

    void C0V(C27231cr r1);

    void C7B(int i);

    void C7C(View view);

    void C8U(Intent intent);

    void CB0(Object obj, Object obj2);

    void CBQ(int i);

    void CGn(Intent intent);

    void CIW();

    boolean dispatchKeyEvent(KeyEvent keyEvent);

    boolean dispatchTouchEvent(MotionEvent motionEvent);

    Intent getIntent();

    void onActivityDestroy();

    void onAttachedToWindow();

    void onBackPressed();

    void onConfigurationChanged(Configuration configuration);

    void onContentChanged();

    void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo);

    View onCreatePanelView(int i);

    boolean onKeyDown(int i, KeyEvent keyEvent);

    boolean onKeyUp(int i, KeyEvent keyEvent);

    void onLowMemory();

    void onPause();

    void onResume();

    boolean onSearchRequested();

    void onStart();

    void onStop();

    void onTrimMemory(int i);

    void onWindowFocusChanged(boolean z);

    void startActivityForResult(Intent intent, int i);
}
