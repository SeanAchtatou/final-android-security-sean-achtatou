package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.0iU  reason: invalid class name */
public final class AnonymousClass0iU extends C09820ik {
    private static C04470Uu A02;
    public AnonymousClass0UN A00;
    public final C09870ip A01;

    public static final AnonymousClass0iU A00(AnonymousClass1XY r5) {
        AnonymousClass0iU r0;
        synchronized (AnonymousClass0iU.class) {
            C04470Uu A002 = C04470Uu.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r3 = (AnonymousClass1XY) A02.A01();
                    A02.A00 = new AnonymousClass0iU(r3, new C09850in(r3));
                }
                C04470Uu r1 = A02;
                r0 = (AnonymousClass0iU) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass0iU(AnonymousClass1XY r4, C09850in r5) {
        this.A00 = new AnonymousClass0UN(4, r4);
        this.A01 = new C09870ip(r5.A00, "emoji_font/");
    }
}
