package X;

import android.content.Context;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0Z9  reason: invalid class name */
public final class AnonymousClass0Z9 extends C05230Yd {
    private AnonymousClass0UN A00;
    private final Runnable A01 = new C05220Yc(this);
    private final Runnable A02 = new C07860eI(this);

    public void A04() {
        this.A00 = false;
        this.A01 = false;
        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BKH, this.A00), this.A01, 883028998);
    }

    public void A05() {
        this.A00 = true;
        this.A01 = true;
        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BKH, this.A00), this.A02, 254527124);
    }

    public static final AnonymousClass0Z9 A02(AnonymousClass1XY r2) {
        return new AnonymousClass0Z9(r2, AnonymousClass1YA.A00(r2));
    }

    public static boolean A03(AnonymousClass0Z9 r3) {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, r3.A00)).AbO(851, false);
    }

    public AnonymousClass0Z9(AnonymousClass1XY r3, Context context) {
        super(context);
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public boolean A06() {
        Boolean bool;
        boolean A03 = A03(this);
        if (this.A00 == null) {
            C05230Yd.A00(this);
        }
        if (!A03 || this.A00.booleanValue()) {
            bool = this.A00;
        } else {
            C05230Yd.A01(this);
            bool = this.A01;
        }
        return bool.booleanValue();
    }
}
