package X;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.Choreographer;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/* renamed from: X.08b  reason: invalid class name and case insensitive filesystem */
public class C009408b extends SurfaceView implements SurfaceHolder.Callback2 {
    public int A00;
    public int A01;
    public int A02 = 0;
    public int A03;
    public int A04;
    public SurfaceHolder A05;
    public Thread A06;
    private int A07;
    private Handler A08;
    private Choreographer.FrameCallback A09;
    private boolean A0A;

    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|5) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0018 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void A0E(int r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            int r0 = r3.A02     // Catch:{ all -> 0x0020 }
            if (r0 == r4) goto L_0x001e
            android.os.Handler r2 = r3.A08     // Catch:{ all -> 0x0020 }
            r1 = 1
            r0 = 0
            android.os.Message r0 = r2.obtainMessage(r1, r4, r0)     // Catch:{ all -> 0x0020 }
            r0.sendToTarget()     // Catch:{ all -> 0x0020 }
        L_0x0010:
            int r0 = r3.A02     // Catch:{ all -> 0x0020 }
            if (r0 == r4) goto L_0x001e
            r3.wait()     // Catch:{ InterruptedException -> 0x0018 }
            goto L_0x0010
        L_0x0018:
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0020 }
            r0.<init>()     // Catch:{ all -> 0x0020 }
            throw r0     // Catch:{ all -> 0x0020 }
        L_0x001e:
            monitor-exit(r3)
            return
        L_0x0020:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C009408b.A0E(int):void");
    }

    public static void A0H(C009408b r3) {
        synchronized (r3) {
            try {
                Looper.prepare();
                r3.A08 = new AnonymousClass0L2(r3);
                if (Build.VERSION.SDK_INT >= 16) {
                    r3.A09 = A0D(r3);
                }
                r3.A0L();
                r3.notifyAll();
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        Looper.loop();
        synchronized (r3) {
            try {
                r3.A0K();
                r3.A08 = null;
                r3.A09 = null;
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|9|10|11|12|6) */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0016, code lost:
        A0E(0);
        r3.A08.obtainMessage(0).sendToTarget();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r2.join();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0026, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0029, code lost:
        if (r3.A08 != null) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002d, code lost:
        if (r3.A06 == r2) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002f, code lost:
        android.util.Log.e("fb-AsyncView", "thread class member changed unexpectedly");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0036, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0038, code lost:
        r3.A06 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0042, code lost:
        throw new java.lang.AssertionError();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0043, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0046, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x004c, code lost:
        throw new java.lang.AssertionError(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x004f, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x000f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0I() {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.Thread r2 = r3.A06     // Catch:{ all -> 0x004d }
            if (r2 != 0) goto L_0x0007
            monitor-exit(r3)     // Catch:{ all -> 0x004d }
            return
        L_0x0007:
            android.os.Handler r0 = r3.A08     // Catch:{ all -> 0x004d }
            if (r0 != 0) goto L_0x0015
            r3.wait()     // Catch:{ InterruptedException -> 0x000f }
            goto L_0x0007
        L_0x000f:
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x004d }
            r0.<init>()     // Catch:{ all -> 0x004d }
            throw r0     // Catch:{ all -> 0x004d }
        L_0x0015:
            monitor-exit(r3)     // Catch:{ all -> 0x004d }
            r1 = 0
            r3.A0E(r1)
            android.os.Handler r0 = r3.A08
            android.os.Message r0 = r0.obtainMessage(r1)
            r0.sendToTarget()
            r2.join()     // Catch:{ InterruptedException -> 0x0046 }
            monitor-enter(r3)
            android.os.Handler r0 = r3.A08     // Catch:{ all -> 0x0043 }
            if (r0 != 0) goto L_0x003d
            java.lang.Thread r0 = r3.A06     // Catch:{ all -> 0x0043 }
            if (r0 == r2) goto L_0x0038
            java.lang.String r1 = "fb-AsyncView"
            java.lang.String r0 = "thread class member changed unexpectedly"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x0043 }
        L_0x0036:
            monitor-exit(r3)     // Catch:{ all -> 0x0043 }
            goto L_0x003c
        L_0x0038:
            r0 = 0
            r3.A06 = r0     // Catch:{ all -> 0x0043 }
            goto L_0x0036
        L_0x003c:
            return
        L_0x003d:
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0043 }
            r0.<init>()     // Catch:{ all -> 0x0043 }
            throw r0     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0043 }
            goto L_0x004f
        L_0x0046:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>(r1)
            throw r0
        L_0x004d:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x004d }
        L_0x004f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C009408b.A0I():void");
    }

    public void A0Q(SurfaceHolder surfaceHolder, long j) {
    }

    public synchronized void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        A0E(0);
        this.A05 = surfaceHolder;
        this.A04 = i2;
        this.A03 = i3;
        A0E(1);
    }

    public synchronized void surfaceCreated(SurfaceHolder surfaceHolder) {
        synchronized (this) {
            try {
                if (this.A06 == null) {
                    C03290Kz r1 = new C03290Kz(this, "AsyncView");
                    this.A06 = r1;
                    r1.start();
                }
            } catch (Throwable th) {
                th = th;
                throw th;
            }
        }
        while (this.A08 == null) {
            try {
                wait();
            } catch (InterruptedException unused) {
                th = new AssertionError();
                throw th;
            }
        }
    }

    public synchronized void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        A0E(0);
        this.A08.removeMessages(2);
        this.A05 = null;
        this.A04 = 0;
        this.A03 = 0;
    }

    public synchronized void surfaceRedrawNeeded(SurfaceHolder surfaceHolder) {
        this.A08.obtainMessage(2).sendToTarget();
        synchronized (this) {
            this.A01++;
            int i = this.A00;
            while (i == this.A00) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    throw new AssertionError(e);
                }
            }
            this.A01--;
        }
    }

    public static Choreographer.FrameCallback A0D(C009408b r1) {
        return new AnonymousClass0L1(r1);
    }

    public C009408b(Context context) {
        super(context);
        setZOrderOnTop(true);
        SurfaceHolder holder = getHolder();
        holder.setFormat(-3);
        holder.addCallback(this);
    }

    public static void A0F(Choreographer.FrameCallback frameCallback) {
        Choreographer.getInstance().postFrameCallback(frameCallback);
    }

    public static void A0G(Choreographer.FrameCallback frameCallback) {
        Choreographer.getInstance().removeFrameCallback(frameCallback);
    }

    public void A0J() {
        A0M();
        if (this.A0A) {
            if (Build.VERSION.SDK_INT >= 16) {
                A0G(this.A09);
            } else {
                this.A08.removeMessages(3);
            }
            this.A0A = false;
        }
    }

    public void A0K() {
        A0M();
    }

    public void A0L() {
        A0M();
    }

    public final void A0M() {
        if (Thread.currentThread() != this.A06) {
            throw new AssertionError("method called on wrong thread");
        }
    }

    public final void A0N() {
        A0M();
        if (this.A02 == 1 && !this.A0A) {
            if (Build.VERSION.SDK_INT >= 16) {
                A0F(this.A09);
            } else {
                Handler handler = this.A08;
                handler.sendMessageDelayed(handler.obtainMessage(3), (long) this.A07);
            }
            this.A0A = true;
        }
    }

    public void A0O(long j) {
        A0M();
        this.A0A = false;
        if (this.A02 == 1) {
            SurfaceHolder surfaceHolder = this.A05;
            A0M();
            A0Q(surfaceHolder, j);
            if (this.A01 > 0) {
                this.A00++;
                notifyAll();
            }
        }
    }

    public void onAttachedToWindow() {
        int A062 = C000700l.A06(-877390583);
        if (Build.VERSION.SDK_INT < 16 && this.A07 == 0) {
            float refreshRate = ((Activity) getContext()).getWindowManager().getDefaultDisplay().getRefreshRate();
            if (refreshRate >= 60.0f) {
                refreshRate = 60.0f;
            } else if (refreshRate <= 15.0f) {
                refreshRate = 15.0f;
            }
            this.A07 = (int) (1000.0f / refreshRate);
        }
        super.onAttachedToWindow();
        C000700l.A0C(-898155097, A062);
    }

    public void onDetachedFromWindow() {
        int A062 = C000700l.A06(1318101975);
        A0I();
        super.onDetachedFromWindow();
        C000700l.A0C(1632027957, A062);
    }

    public void A0P(SurfaceHolder surfaceHolder, int i, int i2) {
        A0M();
    }
}
