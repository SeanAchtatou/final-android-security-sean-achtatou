package X;

import android.view.View;

/* renamed from: X.19M  reason: invalid class name */
public class AnonymousClass19M implements AnonymousClass19N {
    private final AnonymousClass19S A00;

    public void CBG(AnonymousClass1Ri r1) {
    }

    public int AOk(int i, int i2, int i3, int i4) {
        float f;
        float f2;
        if (this.A00.A01 != 0) {
            f = (float) i4;
            f2 = (float) i2;
        } else {
            f = (float) i3;
            f2 = (float) i;
        }
        int ceil = (int) Math.ceil((double) (f / f2));
        if (ceil < 2) {
            return 2;
        }
        if (ceil > 10) {
            return 10;
        }
        return ceil;
    }

    public C27380Dbe AWH(int i, int i2) {
        return new C27379Dbd(i, i2, B1z());
    }

    public int AZo() {
        return this.A00.A1t();
    }

    public int AZq() {
        return this.A00.A1u();
    }

    public int AZr() {
        return this.A00.A1v();
    }

    public int AZs() {
        return this.A00.AZs();
    }

    public int Ah7(int i, C21681Ih r3) {
        if (this.A00.A01 != 0) {
            return View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        return i;
    }

    public int AhA(int i, C21681Ih r3) {
        if (this.A00.A01 != 0) {
            return i;
        }
        return View.MeasureSpec.makeMeasureSpec(0, 0);
    }

    public int ArU() {
        return this.A00.A0d();
    }

    public AnonymousClass19T AsH() {
        return this.A00;
    }

    public int B1z() {
        return this.A00.A01;
    }

    public AnonymousClass19M(int i, boolean z) {
        AnonymousClass19R r1 = new AnonymousClass19R(i, z);
        this.A00 = r1;
        r1.A0D = false;
    }

    public AnonymousClass19M(AnonymousClass19S r1) {
        this.A00 = r1;
    }
}
