package X;

import com.facebook.messaging.cowatch.launcher.parameters.CoWatchLauncherParams;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.facebook.messaging.threadview.params.MessageDeepLinkInfo;
import com.facebook.messaging.threadview.params.ThreadViewMessagesInitParams;
import com.facebook.messaging.threadview.params.ThreadViewParams;
import com.google.common.base.Preconditions;

/* renamed from: X.0rA  reason: invalid class name and case insensitive filesystem */
public final class C13290rA {
    public int A00;
    public CoWatchLauncherParams A01;
    public ThreadKey A02;
    public NavigationTrigger A03;
    public MessageDeepLinkInfo A04;
    public ThreadViewMessagesInitParams A05;
    public C13220qv A06 = C13220qv.A0I;
    public boolean A07;

    public ThreadViewParams A00() {
        return new ThreadViewParams(this);
    }

    public void A01(ThreadKey threadKey) {
        Preconditions.checkNotNull(threadKey);
        this.A02 = threadKey;
    }

    public void A02(C13220qv r1) {
        Preconditions.checkNotNull(r1);
        this.A06 = r1;
    }
}
