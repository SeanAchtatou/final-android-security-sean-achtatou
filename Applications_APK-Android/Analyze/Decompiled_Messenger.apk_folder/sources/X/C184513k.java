package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.AdsConversionsQPData;

/* renamed from: X.13k  reason: invalid class name and case insensitive filesystem */
public final class C184513k implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new AdsConversionsQPData(parcel);
    }

    public Object[] newArray(int i) {
        return new AdsConversionsQPData[i];
    }
}
