package X;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.1gw  reason: invalid class name and case insensitive filesystem */
public class C29761gw {
    public int A00;
    public int A01;
    private int A02;
    private int A03;
    private int A04;
    private int A05;
    public final int A06;
    public final LinkedHashMap A07;
    private final int A08;

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.C29761gw r5, int r6) {
        /*
            monitor-enter(r5)
            int r0 = r5.A01     // Catch:{ all -> 0x0072 }
            monitor-exit(r5)
            if (r0 <= r6) goto L_0x0071
            r4 = 0
        L_0x0007:
            monitor-enter(r5)
            int r0 = r5.A01     // Catch:{ all -> 0x006e }
            if (r0 < 0) goto L_0x005a
            java.util.LinkedHashMap r0 = r5.A07     // Catch:{ all -> 0x006e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x006e }
            if (r0 == 0) goto L_0x0018
            int r0 = r5.A01     // Catch:{ all -> 0x006e }
            if (r0 != 0) goto L_0x005a
        L_0x0018:
            int r0 = r5.A06     // Catch:{ all -> 0x006e }
            if (r4 < r0) goto L_0x0020
            int r0 = r5.A01     // Catch:{ all -> 0x006e }
            if (r0 <= r6) goto L_0x0058
        L_0x0020:
            java.util.LinkedHashMap r0 = r5.A07     // Catch:{ all -> 0x006e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x006e }
            if (r0 != 0) goto L_0x0058
            java.util.LinkedHashMap r0 = r5.A07     // Catch:{ all -> 0x006e }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x006e }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x006e }
            java.lang.Object r0 = r0.next()     // Catch:{ all -> 0x006e }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x006e }
            java.lang.Object r3 = r0.getKey()     // Catch:{ all -> 0x006e }
            java.lang.Object r2 = r0.getValue()     // Catch:{ all -> 0x006e }
            java.util.LinkedHashMap r0 = r5.A07     // Catch:{ all -> 0x006e }
            r0.remove(r3)     // Catch:{ all -> 0x006e }
            int r0 = r5.A01     // Catch:{ all -> 0x006e }
            r1 = 1
            int r0 = r0 - r1
            r5.A01 = r0     // Catch:{ all -> 0x006e }
            int r0 = r5.A00     // Catch:{ all -> 0x006e }
            int r0 = r0 + r1
            r5.A00 = r0     // Catch:{ all -> 0x006e }
            int r4 = r4 + 1
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            r0 = 0
            r5.A07(r1, r3, r2, r0)
            goto L_0x0007
        L_0x0058:
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            return
        L_0x005a:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x006e }
            java.lang.Class r0 = r5.getClass()     // Catch:{ all -> 0x006e }
            java.lang.String r1 = r0.getName()     // Catch:{ all -> 0x006e }
            java.lang.String r0 = ".sizeOf() is reporting inconsistent results!"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x006e }
            r2.<init>(r0)     // Catch:{ all -> 0x006e }
            throw r2     // Catch:{ all -> 0x006e }
        L_0x006e:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            goto L_0x0074
        L_0x0071:
            return
        L_0x0072:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0072 }
        L_0x0074:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29761gw.A00(X.1gw, int):void");
    }

    public final synchronized int A02() {
        return this.A07.size();
    }

    public final synchronized Map A06() {
        return new LinkedHashMap(this.A07);
    }

    public void A07(boolean z, Object obj, Object obj2, Object obj3) {
    }

    public final synchronized String toString() {
        int i;
        int i2;
        int i3;
        i = this.A02;
        i2 = this.A04;
        int i4 = i + i2;
        if (i4 != 0) {
            i3 = (i * 100) / i4;
        } else {
            i3 = 0;
        }
        return String.format(Locale.US, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", Integer.valueOf(this.A03), Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
    }

    public final Object A03(Object obj) {
        if (obj != null) {
            synchronized (this) {
                Object obj2 = this.A07.get(obj);
                if (obj2 != null) {
                    this.A02++;
                    return obj2;
                }
                this.A04++;
                return null;
            }
        }
        throw new NullPointerException("key == null");
    }

    public final Object A04(Object obj) {
        Object remove;
        if (obj != null) {
            synchronized (this) {
                remove = this.A07.remove(obj);
                if (remove != null) {
                    this.A01--;
                }
            }
            if (remove != null) {
                A07(false, obj, remove, null);
            }
            return remove;
        }
        throw new NullPointerException("key == null");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
        return r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A05(java.lang.Object r10, java.lang.Object r11) {
        /*
            r9 = this;
            if (r10 == 0) goto L_0x0088
            if (r11 == 0) goto L_0x0088
            monitor-enter(r9)
            int r0 = r9.A05     // Catch:{ all -> 0x0085 }
            int r0 = r0 + 1
            r9.A05 = r0     // Catch:{ all -> 0x0085 }
            int r0 = r9.A01     // Catch:{ all -> 0x0085 }
            r8 = 1
            int r0 = r0 + r8
            r9.A01 = r0     // Catch:{ all -> 0x0085 }
            java.util.LinkedHashMap r0 = r9.A07     // Catch:{ all -> 0x0085 }
            java.lang.Object r4 = r0.put(r10, r11)     // Catch:{ all -> 0x0085 }
            if (r4 == 0) goto L_0x001e
            int r0 = r9.A01     // Catch:{ all -> 0x0085 }
            int r0 = r0 - r8
            r9.A01 = r0     // Catch:{ all -> 0x0085 }
        L_0x001e:
            monitor-exit(r9)     // Catch:{ all -> 0x0085 }
            if (r4 == 0) goto L_0x0025
            r0 = 0
            r9.A07(r0, r10, r4, r11)
        L_0x0025:
            int r0 = r9.A03
            int r6 = r9.A08
            A00(r9, r0)
            r7 = r9
            monitor-enter(r7)
            java.util.LinkedHashMap r0 = r9.A07     // Catch:{ all -> 0x0082 }
            int r0 = r0.size()     // Catch:{ all -> 0x0082 }
            monitor-exit(r7)
            if (r0 <= r6) goto L_0x0081
            r5 = 0
        L_0x0038:
            monitor-enter(r7)
            int r0 = r9.A06     // Catch:{ all -> 0x007e }
            if (r5 < r0) goto L_0x0045
            java.util.LinkedHashMap r0 = r9.A07     // Catch:{ all -> 0x007e }
            int r0 = r0.size()     // Catch:{ all -> 0x007e }
            if (r0 <= r6) goto L_0x007c
        L_0x0045:
            java.util.LinkedHashMap r0 = r9.A07     // Catch:{ all -> 0x007e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x007e }
            if (r0 != 0) goto L_0x007c
            java.util.LinkedHashMap r0 = r9.A07     // Catch:{ all -> 0x007e }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x007e }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x007e }
            java.lang.Object r0 = r0.next()     // Catch:{ all -> 0x007e }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x007e }
            java.lang.Object r3 = r0.getKey()     // Catch:{ all -> 0x007e }
            java.lang.Object r2 = r0.getValue()     // Catch:{ all -> 0x007e }
            java.util.LinkedHashMap r0 = r9.A07     // Catch:{ all -> 0x007e }
            r0.remove(r3)     // Catch:{ all -> 0x007e }
            int r0 = r9.A01     // Catch:{ all -> 0x007e }
            int r0 = r0 - r8
            r9.A01 = r0     // Catch:{ all -> 0x007e }
            int r0 = r9.A00     // Catch:{ all -> 0x007e }
            int r0 = r0 + r8
            r9.A00 = r0     // Catch:{ all -> 0x007e }
            int r5 = r5 + 1
            monitor-exit(r7)     // Catch:{ all -> 0x007e }
            r0 = 0
            r9.A07(r8, r3, r2, r0)
            goto L_0x0038
        L_0x007c:
            monitor-exit(r7)     // Catch:{ all -> 0x007e }
            return r4
        L_0x007e:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x007e }
            goto L_0x0087
        L_0x0081:
            return r4
        L_0x0082:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0082 }
            goto L_0x0087
        L_0x0085:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0085 }
        L_0x0087:
            throw r0
        L_0x0088:
            java.lang.NullPointerException r1 = new java.lang.NullPointerException
            java.lang.String r0 = "key == null || value == null"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29761gw.A05(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public C29761gw(int i) {
        this(Integer.MAX_VALUE, i, 0);
    }

    private C29761gw(int i, int i2, int i3) {
        if (i <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i2 > 0) {
            this.A06 = i3;
            this.A03 = i;
            this.A08 = i2;
            this.A07 = new LinkedHashMap(0, 0.75f, true);
        } else {
            throw new IllegalArgumentException("maxEntries <= 0");
        }
    }
}
