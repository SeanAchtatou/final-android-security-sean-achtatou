package X;

import android.content.Context;

/* renamed from: X.0p3  reason: invalid class name and case insensitive filesystem */
public final class C12310p3 extends AnonymousClass0p4 {
    public C12320p7 A00;
    public final int A01;

    public C12310p3(C12310p3 r2) {
        super(r2.A09);
        this.A01 = r2.A01;
    }

    public C12310p3(Context context, int i) {
        super(context);
        this.A01 = i;
    }
}
