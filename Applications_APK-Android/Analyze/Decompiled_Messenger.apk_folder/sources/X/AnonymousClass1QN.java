package X;

import android.util.Pair;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

/* renamed from: X.1QN  reason: invalid class name */
public abstract class AnonymousClass1QN implements AnonymousClass1QO {
    public static volatile C27501dI A06;
    public float A00 = 0.0f;
    public Integer A01 = AnonymousClass07B.A00;
    public Object A02 = null;
    public Throwable A03 = null;
    public boolean A04 = false;
    public final ConcurrentLinkedQueue A05 = new ConcurrentLinkedQueue();

    private synchronized boolean A04() {
        boolean z;
        z = false;
        if (this.A01 == AnonymousClass07B.A0C) {
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (BEz() != false) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean A05() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.isClosed()     // Catch:{ all -> 0x0011 }
            if (r0 == 0) goto L_0x000e
            boolean r1 = r2.BEz()     // Catch:{ all -> 0x0011 }
            r0 = 1
            if (r1 == 0) goto L_0x000f
        L_0x000e:
            r0 = 0
        L_0x000f:
            monitor-exit(r2)
            return r0
        L_0x0011:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QN.A05():boolean");
    }

    public void A06(float f) {
        boolean z;
        synchronized (this) {
            if (this.A04 || this.A01 != AnonymousClass07B.A00 || f < this.A00) {
                z = false;
            } else {
                this.A00 = f;
                z = true;
            }
        }
        if (z) {
            Iterator it = this.A05.iterator();
            while (it.hasNext()) {
                Pair pair = (Pair) it.next();
                AnonymousClass07A.A04((Executor) pair.second, new AnonymousClass1SC(this, (C23511Pu) pair.first), 2029468784);
            }
        }
    }

    public void A07(Object obj) {
        if ((this instanceof C33521ni) || (this instanceof AnonymousClass1QL)) {
            AnonymousClass1PS.A05((AnonymousClass1PS) obj);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0026, code lost:
        if (r5 == null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0028, code lost:
        A07(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002b, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0032, code lost:
        r0 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A08(java.lang.Object r5, boolean r6) {
        /*
            r4 = this;
            r3 = r4
            r2 = 0
            monitor-enter(r3)     // Catch:{ all -> 0x003f }
            boolean r0 = r4.A04     // Catch:{ all -> 0x003c }
            if (r0 != 0) goto L_0x0025
            java.lang.Integer r1 = r4.A01     // Catch:{ all -> 0x003c }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x003c }
            if (r1 != r0) goto L_0x0025
            if (r6 == 0) goto L_0x0017
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x003c }
            r4.A01 = r0     // Catch:{ all -> 0x003c }
            r0 = 1065353216(0x3f800000, float:1.0)
            r4.A00 = r0     // Catch:{ all -> 0x003c }
        L_0x0017:
            java.lang.Object r1 = r4.A02     // Catch:{ all -> 0x003c }
            if (r1 == r5) goto L_0x0022
            r4.A02 = r5     // Catch:{ all -> 0x001f }
            r5 = r1
            goto L_0x0023
        L_0x001f:
            r0 = move-exception
            r2 = r1
            goto L_0x003d
        L_0x0022:
            r5 = r2
        L_0x0023:
            monitor-exit(r3)     // Catch:{ all -> 0x0039 }
            goto L_0x002d
        L_0x0025:
            monitor-exit(r3)     // Catch:{ all -> 0x0039 }
            if (r5 == 0) goto L_0x002b
            r4.A07(r5)
        L_0x002b:
            r0 = 0
            goto L_0x0033
        L_0x002d:
            if (r5 == 0) goto L_0x0032
            r4.A07(r5)
        L_0x0032:
            r0 = 1
        L_0x0033:
            if (r0 == 0) goto L_0x0038
            r4.A02()
        L_0x0038:
            return r0
        L_0x0039:
            r0 = move-exception
            r2 = r5
            goto L_0x003d
        L_0x003c:
            r0 = move-exception
        L_0x003d:
            monitor-exit(r3)     // Catch:{ all -> 0x003c }
            throw r0     // Catch:{ all -> 0x003f }
        L_0x003f:
            r0 = move-exception
            if (r2 == 0) goto L_0x0045
            r4.A07(r2)
        L_0x0045:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QN.A08(java.lang.Object, boolean):boolean");
    }

    public boolean A09(Throwable th) {
        boolean z;
        synchronized (this) {
            if (this.A04 || this.A01 != AnonymousClass07B.A00) {
                z = false;
            } else {
                this.A01 = AnonymousClass07B.A0C;
                this.A03 = th;
                z = true;
            }
        }
        if (z) {
            A02();
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
        A07(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        if (BEz() != false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        A02();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001f, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3.A05.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0025, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0026, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0027, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x002c, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r1 == null) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AT6() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.A04     // Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x0008
            r2 = 0
            monitor-exit(r3)     // Catch:{ all -> 0x002a }
            return r2
        L_0x0008:
            r2 = 1
            r3.A04 = r2     // Catch:{ all -> 0x002a }
            java.lang.Object r1 = r3.A02     // Catch:{ all -> 0x002a }
            r0 = 0
            r3.A02 = r0     // Catch:{ all -> 0x002a }
            monitor-exit(r3)     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x0016
            r3.A07(r1)
        L_0x0016:
            boolean r0 = r3.BEz()
            if (r0 != 0) goto L_0x001f
            r3.A02()
        L_0x001f:
            monitor-enter(r3)
            java.util.concurrent.ConcurrentLinkedQueue r0 = r3.A05     // Catch:{ all -> 0x0027 }
            r0.clear()     // Catch:{ all -> 0x0027 }
            monitor-exit(r3)     // Catch:{ all -> 0x0027 }
            return r2
        L_0x0027:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0027 }
            goto L_0x002c
        L_0x002a:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002a }
        L_0x002c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QN.AT6():boolean");
    }

    public synchronized Throwable AmS() {
        return this.A03;
    }

    public synchronized float AzW() {
        return this.A00;
    }

    public synchronized Object B1I() {
        return this.A02;
    }

    public boolean BBX() {
        return false;
    }

    public synchronized boolean BEz() {
        boolean z;
        z = false;
        if (this.A01 != AnonymousClass07B.A00) {
            z = true;
        }
        return z;
    }

    public synchronized boolean isClosed() {
        return this.A04;
    }

    private void A03(C23511Pu r4, Executor executor, boolean z, boolean z2) {
        int i;
        Runnable r1 = new AnonymousClass1RN(this, z, r4, z2);
        if (A06 != null) {
            AnonymousClass1XV A012 = C27401d8.A00.A01();
            if (A012 == null) {
                i = 0;
            } else {
                i = A012.A03;
            }
            r1 = C25231Yv.A00("AbstractDataSource_notifyDataSubscriber", r1, i);
        }
        AnonymousClass07A.A04(executor, r1, -2050190287);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0027, code lost:
        if (r1 == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0047, code lost:
        if (r1 == false) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x005f, code lost:
        if (r3.A00 != r3.A01.length) goto L_0x0061;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean BBn() {
        /*
            r4 = this;
            boolean r0 = r4 instanceof X.AnonymousClass1R0
            if (r0 != 0) goto L_0x004f
            boolean r0 = r4 instanceof X.C23821Rb
            if (r0 != 0) goto L_0x002f
            boolean r0 = r4 instanceof X.C23841Rd
            if (r0 != 0) goto L_0x0018
            monitor-enter(r4)
            java.lang.Object r1 = r4.A02     // Catch:{ all -> 0x0015 }
            r0 = 0
            if (r1 == 0) goto L_0x0013
            r0 = 1
        L_0x0013:
            monitor-exit(r4)
            return r0
        L_0x0015:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0018:
            r2 = r4
            X.1Rd r2 = (X.C23841Rd) r2
            monitor-enter(r2)
            X.1QO r0 = X.C23841Rd.A00(r2)     // Catch:{ all -> 0x002c }
            if (r0 == 0) goto L_0x0029
            boolean r1 = r0.BBn()     // Catch:{ all -> 0x002c }
            r0 = 1
            if (r1 != 0) goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            monitor-exit(r2)
            return r0
        L_0x002c:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x002f:
            r2 = r4
            X.1Rb r2 = (X.C23821Rb) r2
            monitor-enter(r2)
            X.5SY r0 = r2.A05     // Catch:{ all -> 0x004c }
            boolean r0 = r0.A01     // Catch:{ all -> 0x004c }
            if (r0 == 0) goto L_0x003c
            X.C23821Rb.A03(r2)     // Catch:{ all -> 0x004c }
        L_0x003c:
            X.1QO r0 = X.C23821Rb.A00(r2)     // Catch:{ all -> 0x004c }
            if (r0 == 0) goto L_0x0049
            boolean r1 = r0.BBn()     // Catch:{ all -> 0x004c }
            r0 = 1
            if (r1 != 0) goto L_0x004a
        L_0x0049:
            r0 = 0
        L_0x004a:
            monitor-exit(r2)
            return r0
        L_0x004c:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x004f:
            r3 = r4
            X.1R0 r3 = (X.AnonymousClass1R0) r3
            monitor-enter(r3)
            boolean r0 = r3.isClosed()     // Catch:{ all -> 0x0064 }
            if (r0 != 0) goto L_0x0061
            int r2 = r3.A00     // Catch:{ all -> 0x0064 }
            X.1QO[] r0 = r3.A01     // Catch:{ all -> 0x0064 }
            int r1 = r0.length     // Catch:{ all -> 0x0064 }
            r0 = 1
            if (r2 == r1) goto L_0x0062
        L_0x0061:
            r0 = 0
        L_0x0062:
            monitor-exit(r3)
            return r0
        L_0x0064:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QN.BBn():boolean");
    }

    private void A02() {
        boolean A042 = A04();
        boolean A052 = A05();
        Iterator it = this.A05.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            A03((C23511Pu) pair.first, (Executor) pair.second, A042, A052);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        if (A05() != false) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0031, code lost:
        if (r0 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0033, code lost:
        A03(r3, r4, A04(), A05());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void CIK(X.C23511Pu r3, java.util.concurrent.Executor r4) {
        /*
            r2 = this;
            X.C05520Zg.A02(r3)
            X.C05520Zg.A02(r4)
            monitor-enter(r2)
            boolean r0 = r2.A04     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x000d
            monitor-exit(r2)     // Catch:{ all -> 0x003f }
            return
        L_0x000d:
            java.lang.Integer r1 = r2.A01     // Catch:{ all -> 0x003f }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x003f }
            if (r1 != r0) goto L_0x001c
            java.util.concurrent.ConcurrentLinkedQueue r1 = r2.A05     // Catch:{ all -> 0x003f }
            android.util.Pair r0 = android.util.Pair.create(r3, r4)     // Catch:{ all -> 0x003f }
            r1.add(r0)     // Catch:{ all -> 0x003f }
        L_0x001c:
            boolean r0 = r2.BBn()     // Catch:{ all -> 0x003f }
            if (r0 != 0) goto L_0x002f
            boolean r0 = r2.BEz()     // Catch:{ all -> 0x003f }
            if (r0 != 0) goto L_0x002f
            boolean r1 = r2.A05()     // Catch:{ all -> 0x003f }
            r0 = 0
            if (r1 == 0) goto L_0x0030
        L_0x002f:
            r0 = 1
        L_0x0030:
            monitor-exit(r2)     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x003e
            boolean r1 = r2.A04()
            boolean r0 = r2.A05()
            r2.A03(r3, r4, r1, r0)
        L_0x003e:
            return
        L_0x003f:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QN.CIK(X.1Pu, java.util.concurrent.Executor):void");
    }
}
