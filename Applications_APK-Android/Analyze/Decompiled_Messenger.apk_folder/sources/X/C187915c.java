package X;

import android.content.Context;
import android.view.View;
import com.google.common.collect.RegularImmutableList;

/* renamed from: X.15c  reason: invalid class name and case insensitive filesystem */
public final class C187915c implements View.OnClickListener {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C33691nz A01;

    public C187915c(C33691nz r1, Context context) {
        this.A01 = r1;
        this.A00 = context;
    }

    public void onClick(View view) {
        int A05 = C000700l.A05(849272796);
        this.A01.BvV(this.A00, RegularImmutableList.A02);
        C000700l.A0B(486591301, A05);
    }
}
