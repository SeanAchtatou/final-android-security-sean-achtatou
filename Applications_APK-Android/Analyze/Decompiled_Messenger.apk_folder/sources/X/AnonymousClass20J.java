package X;

import com.google.common.base.Optional;
import com.google.common.collect.HashBasedTable;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.LinkedHashMap;

/* renamed from: X.20J  reason: invalid class name */
public final class AnonymousClass20J {
    public static final AnonymousClass20J A03 = new AnonymousClass20J();
    public int A00;
    public int A01;
    public final AnonymousClass3K9 A02 = new HashBasedTable(new LinkedHashMap(), new HashBasedTable.Factory(0));

    public synchronized boolean A00(AnnotatedElement annotatedElement, Class cls) {
        Annotation annotation;
        boolean z;
        synchronized (this) {
            Optional optional = (Optional) this.A02.A03(annotatedElement, cls);
            if (optional != null) {
                this.A00++;
                annotation = (Annotation) optional.orNull();
            } else {
                annotation = annotatedElement.getAnnotation(cls);
                this.A02.A04(annotatedElement, cls, Optional.fromNullable(annotation));
                this.A01++;
            }
        }
        z = false;
        if (annotation != null) {
            z = true;
        }
        return z;
    }
}
