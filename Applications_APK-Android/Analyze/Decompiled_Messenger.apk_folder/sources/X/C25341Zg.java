package X;

/* renamed from: X.1Zg  reason: invalid class name and case insensitive filesystem */
public final class C25341Zg implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.scheduler.OrchestrationFuture$1";
    public final /* synthetic */ C25291Zb A00;

    public C25341Zg(C25291Zb r1) {
        this.A00 = r1;
    }

    public void run() {
        C30377EvA evA;
        C25291Zb r1 = this.A00;
        r1.A04.countDown();
        if (r1.isCancelled() && (evA = null) != null) {
            evA.onJobCancelled();
        }
        r1.A03.A01();
    }
}
