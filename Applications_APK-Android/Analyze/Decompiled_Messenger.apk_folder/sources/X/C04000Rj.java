package X;

import java.io.DataOutputStream;

/* renamed from: X.0Rj  reason: invalid class name and case insensitive filesystem */
public final class C04000Rj {
    public static int A02(DataOutputStream dataOutputStream, int i) {
        int i2 = 0;
        do {
            int i3 = i % 128;
            i >>= 7;
            if (i > 0) {
                i3 |= 128;
            }
            dataOutputStream.writeByte(i3);
            i2++;
        } while (i > 0);
        return i2;
    }

    public static int A00(AnonymousClass0Rv r2) {
        int i = 0;
        if (r2.A05) {
            i = 128;
        }
        if (r2.A04) {
            i |= 64;
        }
        if (r2.A07) {
            i |= 32;
        }
        int i2 = i | ((r2.A02 & 3) << 3);
        if (r2.A06) {
            i2 |= 4;
        }
        if (r2.A03) {
            return i2 | 2;
        }
        return i2;
    }

    public static int A01(C01990Ck r2) {
        int i = (r2.A03.mValue << 4) | 0;
        if (r2.A04) {
            i |= 8;
        }
        int i2 = i | (r2.A02 << 1);
        if (r2.A01) {
            return i2 | 1;
        }
        return i2;
    }
}
