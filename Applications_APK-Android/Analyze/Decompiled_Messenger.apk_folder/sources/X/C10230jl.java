package X;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: X.0jl  reason: invalid class name and case insensitive filesystem */
public final class C10230jl implements C10240jm, Serializable {
    public transient String _jdkSerializeValue;
    public char[] _quotedChars;
    public byte[] _quotedUTF8Ref;
    public byte[] _unquotedUTF8Ref;
    public final String _value;

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeUTF(this._value);
    }

    public int appendQuotedUTF8(byte[] bArr, int i) {
        byte[] bArr2 = this._quotedUTF8Ref;
        if (bArr2 == null) {
            bArr2 = A20.getInstance().quoteAsUTF8(this._value);
            this._quotedUTF8Ref = bArr2;
        }
        int length = bArr2.length;
        if (i + length > bArr.length) {
            return -1;
        }
        System.arraycopy(bArr2, 0, bArr, i, length);
        return length;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0030, code lost:
        r14 = r2 + 1;
        r13 = r9.charAt(r2);
        r0 = r6[r13];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        if (r0 >= 0) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003a, code lost:
        r2 = r10._quoteBuffer;
        r2[1] = 'u';
        r12 = X.A20.HEX_CHARS;
        r2[4] = r12[r13 >> 4];
        r2[5] = r12[r13 & 15];
        r12 = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0052, code lost:
        r2 = r11 + r12;
        r1 = r7.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0055, code lost:
        if (r2 <= r1) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0057, code lost:
        r1 = r1 - r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0058, code lost:
        if (r1 <= 0) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005a, code lost:
        java.lang.System.arraycopy(r10._quoteBuffer, 0, r7, r11, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005f, code lost:
        r7 = r8.finishCurrentSegment();
        r12 = r12 - r1;
        java.lang.System.arraycopy(r10._quoteBuffer, r1, r7, 0, r12);
        r11 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006a, code lost:
        r2 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006c, code lost:
        java.lang.System.arraycopy(r10._quoteBuffer, 0, r7, r11, r12);
        r11 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0073, code lost:
        r10._quoteBuffer[1] = (char) r0;
        r12 = 2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final char[] asQuotedChars() {
        /*
            r15 = this;
            char[] r0 = r15._quotedChars
            if (r0 != 0) goto L_0x0094
            X.A20 r10 = X.A20.getInstance()
            java.lang.String r9 = r15._value
            X.1eY r8 = r10._textBuffer
            if (r8 != 0) goto L_0x0016
            X.1eY r8 = new X.1eY
            r0 = 0
            r8.<init>(r0)
            r10._textBuffer = r8
        L_0x0016:
            char[] r7 = r8.emptyAndGetCurrentSegment()
            int[] r6 = X.AnonymousClass11z.sOutputEscapes128
            int r5 = r6.length
            int r4 = r9.length()
            r3 = 0
            r2 = 0
            r11 = 0
        L_0x0024:
            if (r2 >= r4) goto L_0x008c
        L_0x0026:
            char r1 = r9.charAt(r2)
            if (r1 >= r5) goto L_0x007b
            r0 = r6[r1]
            if (r0 == 0) goto L_0x007b
            int r14 = r2 + 1
            char r13 = r9.charAt(r2)
            r0 = r6[r13]
            if (r0 >= 0) goto L_0x0073
            char[] r2 = r10._quoteBuffer
            r1 = 1
            r0 = 117(0x75, float:1.64E-43)
            r2[r1] = r0
            char[] r12 = X.A20.HEX_CHARS
            int r0 = r13 >> 4
            char r1 = r12[r0]
            r0 = 4
            r2[r0] = r1
            r0 = r13 & 15
            char r1 = r12[r0]
            r0 = 5
            r2[r0] = r1
            r12 = 6
        L_0x0052:
            int r2 = r11 + r12
            int r1 = r7.length
            if (r2 <= r1) goto L_0x006c
            int r1 = r1 - r11
            if (r1 <= 0) goto L_0x005f
            char[] r0 = r10._quoteBuffer
            java.lang.System.arraycopy(r0, r3, r7, r11, r1)
        L_0x005f:
            char[] r7 = r8.finishCurrentSegment()
            int r12 = r12 - r1
            char[] r0 = r10._quoteBuffer
            java.lang.System.arraycopy(r0, r1, r7, r3, r12)
            r11 = r12
        L_0x006a:
            r2 = r14
            goto L_0x0024
        L_0x006c:
            char[] r0 = r10._quoteBuffer
            java.lang.System.arraycopy(r0, r3, r7, r11, r12)
            r11 = r2
            goto L_0x006a
        L_0x0073:
            char[] r2 = r10._quoteBuffer
            char r1 = (char) r0
            r0 = 1
            r2[r0] = r1
            r12 = 2
            goto L_0x0052
        L_0x007b:
            int r0 = r7.length
            if (r11 < r0) goto L_0x0083
            char[] r7 = r8.finishCurrentSegment()
            r11 = 0
        L_0x0083:
            int r0 = r11 + 1
            r7[r11] = r1
            int r2 = r2 + 1
            r11 = r0
            if (r2 < r4) goto L_0x0026
        L_0x008c:
            r8._currentSize = r11
            char[] r0 = r8.contentsAsArray()
            r15._quotedChars = r0
        L_0x0094:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10230jl.asQuotedChars():char[]");
    }

    public final byte[] asQuotedUTF8() {
        byte[] bArr = this._quotedUTF8Ref;
        if (bArr != null) {
            return bArr;
        }
        byte[] quoteAsUTF8 = A20.getInstance().quoteAsUTF8(this._value);
        this._quotedUTF8Ref = quoteAsUTF8;
        return quoteAsUTF8;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00fa, code lost:
        throw new java.lang.IllegalArgumentException(X.C1935494z.illegalSurrogateDesc(r3));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] asUnquotedUTF8() {
        /*
            r12 = this;
            byte[] r0 = r12._unquotedUTF8Ref
            if (r0 != 0) goto L_0x004b
            X.A20 r8 = X.A20.getInstance()
            java.lang.String r7 = r12._value
            X.A21 r6 = r8._byteBuilder
            if (r6 != 0) goto L_0x0018
            X.A21 r6 = new X.A21
            r1 = 0
            r0 = 500(0x1f4, float:7.0E-43)
            r6.<init>(r1, r0)
            r8._byteBuilder = r6
        L_0x0018:
            int r5 = r7.length()
            r6.reset()
            byte[] r4 = r6._currBlock
            int r2 = r4.length
            r0 = 0
            r1 = 0
        L_0x0024:
            if (r0 >= r5) goto L_0x0041
            int r11 = r0 + 1
            char r3 = r7.charAt(r0)
        L_0x002c:
            r0 = 127(0x7f, float:1.78E-43)
            if (r3 > r0) goto L_0x0055
            if (r1 < r2) goto L_0x0039
            X.A21._allocMore(r6)
            byte[] r4 = r6._currBlock
            int r2 = r4.length
            r1 = 0
        L_0x0039:
            int r9 = r1 + 1
            byte r0 = (byte) r3
            r4[r1] = r0
            if (r11 < r5) goto L_0x004c
            r1 = r9
        L_0x0041:
            X.A21 r0 = r8._byteBuilder
            r0._currBlockPtr = r1
            byte[] r0 = r0.toByteArray()
            r12._unquotedUTF8Ref = r0
        L_0x004b:
            return r0
        L_0x004c:
            int r0 = r11 + 1
            char r3 = r7.charAt(r11)
            r11 = r0
            r1 = r9
            goto L_0x002c
        L_0x0055:
            if (r1 < r2) goto L_0x005e
            X.A21._allocMore(r6)
            byte[] r4 = r6._currBlock
            int r2 = r4.length
            r1 = 0
        L_0x005e:
            r0 = 2048(0x800, float:2.87E-42)
            if (r3 >= r0) goto L_0x007f
            int r9 = r1 + 1
            int r0 = r3 >> 6
            r0 = r0 | 192(0xc0, float:2.69E-43)
            byte r0 = (byte) r0
            r4[r1] = r0
        L_0x006b:
            if (r9 < r2) goto L_0x0074
            X.A21._allocMore(r6)
            byte[] r4 = r6._currBlock
            int r2 = r4.length
            r9 = 0
        L_0x0074:
            int r1 = r9 + 1
            r0 = r3 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            byte r0 = (byte) r0
            r4[r9] = r0
            r0 = r11
            goto L_0x0024
        L_0x007f:
            r0 = 55296(0xd800, float:7.7486E-41)
            if (r3 < r0) goto L_0x00d2
            r0 = 57343(0xdfff, float:8.0355E-41)
            if (r3 > r0) goto L_0x00d2
            r0 = 56319(0xdbff, float:7.892E-41)
            if (r3 > r0) goto L_0x00f1
            if (r11 >= r5) goto L_0x00f1
            int r10 = r11 + 1
            char r0 = r7.charAt(r11)
            int r3 = X.A20._convertSurrogate(r3, r0)
            r0 = 1114111(0x10ffff, float:1.561202E-39)
            if (r3 > r0) goto L_0x00f1
            int r9 = r1 + 1
            int r0 = r3 >> 18
            r0 = r0 | 240(0xf0, float:3.36E-43)
            byte r0 = (byte) r0
            r4[r1] = r0
            if (r9 < r2) goto L_0x00b1
            X.A21._allocMore(r6)
            byte[] r4 = r6._currBlock
            int r2 = r4.length
            r9 = 0
        L_0x00b1:
            int r1 = r9 + 1
            int r0 = r3 >> 12
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            byte r0 = (byte) r0
            r4[r9] = r0
            if (r1 < r2) goto L_0x00c5
            X.A21._allocMore(r6)
            byte[] r4 = r6._currBlock
            int r2 = r4.length
            r1 = 0
        L_0x00c5:
            int r9 = r1 + 1
            int r0 = r3 >> 6
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            byte r0 = (byte) r0
            r4[r1] = r0
            r11 = r10
            goto L_0x006b
        L_0x00d2:
            int r10 = r1 + 1
            int r0 = r3 >> 12
            r0 = r0 | 224(0xe0, float:3.14E-43)
            byte r0 = (byte) r0
            r4[r1] = r0
            if (r10 < r2) goto L_0x00e4
            X.A21._allocMore(r6)
            byte[] r4 = r6._currBlock
            int r2 = r4.length
            r10 = 0
        L_0x00e4:
            int r9 = r10 + 1
            int r0 = r3 >> 6
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            byte r0 = (byte) r0
            r4[r10] = r0
            goto L_0x006b
        L_0x00f1:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = X.C1935494z.illegalSurrogateDesc(r3)
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10230jl.asUnquotedUTF8():byte[]");
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return this._value.equals(((C10230jl) obj)._value);
    }

    public final String getValue() {
        return this._value;
    }

    public final int hashCode() {
        return this._value.hashCode();
    }

    public Object readResolve() {
        return new C10230jl(this._jdkSerializeValue);
    }

    public final String toString() {
        return this._value;
    }

    public C10230jl(String str) {
        if (str != null) {
            this._value = str;
            return;
        }
        throw new IllegalStateException("Null String illegal for SerializedString");
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this._jdkSerializeValue = objectInputStream.readUTF();
    }
}
