package X;

import android.os.Bundle;
import android.view.View;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

/* renamed from: X.0wm  reason: invalid class name and case insensitive filesystem */
public final class C16270wm extends C15220uv {
    public final C16260wl A00;

    public C16270wm(C16260wl r1) {
        this.A00 = r1;
    }

    public boolean A0D(View view, int i, Bundle bundle) {
        if (super.A0D(view, i, bundle)) {
            return true;
        }
        boolean A1B = this.A00.A01.A1B();
        return false;
    }

    public void A0I(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        AnonymousClass19T r0;
        super.A0I(view, accessibilityNodeInfoCompat);
        if (!this.A00.A01.A1B() && (r0 = this.A00.A01.A0L) != null) {
            r0.A1B(view, accessibilityNodeInfoCompat);
        }
    }
}
