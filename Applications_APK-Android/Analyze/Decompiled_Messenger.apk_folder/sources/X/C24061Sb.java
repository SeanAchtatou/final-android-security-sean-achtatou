package X;

import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import com.facebook.inject.ContextScoped;
import com.facebook.messaging.ui.name.ThreadNameViewData;
import io.card.payment.BuildConfig;
import java.util.List;

@ContextScoped
/* renamed from: X.1Sb  reason: invalid class name and case insensitive filesystem */
public final class C24061Sb {
    private static C04470Uu A03;
    public final C24071Sc A00;
    private final Resources A01;
    private final C22351Kz A02;

    public static final C24061Sb A00(AnonymousClass1XY r6) {
        C24061Sb r0;
        synchronized (C24061Sb.class) {
            C04470Uu A002 = C04470Uu.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r6)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A03.A01();
                    A03.A00 = new C24061Sb(C04490Ux.A0L(r02), C24071Sc.A00(r02), C22341Ky.A00(r02));
                }
                C04470Uu r1 = A03;
                r0 = (C24061Sb) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public String A02(int i) {
        return AnonymousClass08S.A0J(this.A00.A04(), this.A01.getString(2131833841, Integer.valueOf(i)));
    }

    public String A03(ThreadNameViewData threadNameViewData) {
        if (threadNameViewData == null) {
            return BuildConfig.FLAVOR;
        }
        if (threadNameViewData.A02) {
            return threadNameViewData.A01;
        }
        return A04(threadNameViewData.A00);
    }

    public C24061Sb(Resources resources, C24071Sc r2, C22351Kz r3) {
        this.A01 = resources;
        this.A00 = r2;
        this.A02 = r3;
    }

    public CharSequence A01(CharSequence charSequence, int i) {
        SpannableStringBuilder valueOf = SpannableStringBuilder.valueOf(charSequence);
        C22351Kz r0 = this.A02;
        if (r0 != null) {
            r0.ANh(valueOf, i);
        }
        return valueOf;
    }

    public String A04(List list) {
        if (!list.isEmpty()) {
            return C24071Sc.A02(this.A00, list);
        }
        return this.A01.getString(2131833843);
    }
}
