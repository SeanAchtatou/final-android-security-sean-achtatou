package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.inbox2.analytics.InboxSourceLoggingData;
import com.facebook.messaging.inbox2.items.InboxTrackableItem;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.1mT  reason: invalid class name and case insensitive filesystem */
public final class C32871mT implements C19881Ab {
    public AnonymousClass0UN A00;
    public final DeprecatedAnalyticsLogger A01;
    public final InboxSourceLoggingData A02;
    private final AnonymousClass0jE A03;

    private static void A00(ObjectNode objectNode, InboxTrackableItem inboxTrackableItem) {
        if (C010708t.A0X(2)) {
            objectNode.put("u", inboxTrackableItem.A0A);
        }
        objectNode.put("id", inboxTrackableItem.A07);
        objectNode.put("p", inboxTrackableItem.A00);
        objectNode.put("rp", inboxTrackableItem.A01);
        objectNode.put("up", inboxTrackableItem.A02);
        if (!C06850cB.A0B(inboxTrackableItem.A09)) {
            objectNode.put("ulg", inboxTrackableItem.A09);
        }
        if (!C06850cB.A0B(inboxTrackableItem.A06)) {
            objectNode.put("ilg", inboxTrackableItem.A06);
        }
        A01(objectNode, inboxTrackableItem);
    }

    private void A03(Object obj) {
        if (C010708t.A0X(2)) {
            try {
                C10200ji r4 = new C10200ji();
                AnonymousClass0jE r3 = this.A03;
                new A24(r3, r3._serializationConfig, null, r4).writeValueAsString(obj);
            } catch (C37571vt unused) {
            }
        }
    }

    private static void A01(ObjectNode objectNode, InboxTrackableItem inboxTrackableItem) {
        ImmutableMap immutableMap = inboxTrackableItem.A05;
        if (immutableMap != null) {
            C24971Xv it = immutableMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                if (objectNode.has((String) entry.getKey())) {
                    throw new IllegalArgumentException(String.format("Item of type %s provides a logging extra for a duplicate param (%s)", inboxTrackableItem.A04, entry.getKey()));
                } else if ("an".equals(entry.getKey())) {
                    objectNode.put((String) entry.getKey(), Integer.valueOf((String) entry.getValue()));
                } else {
                    objectNode.put((String) entry.getKey(), (String) entry.getValue());
                }
            }
        }
    }

    public void A04(InboxUnitItem inboxUnitItem, String str, Map map) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        A00(objectNode, inboxUnitItem.A07());
        if (!C06850cB.A0B(str)) {
            objectNode.put("ct", str);
        }
        InboxSourceLoggingData.A00(objectNode, this.A02);
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                objectNode.put((String) entry.getKey(), (String) entry.getValue());
            }
        }
        A03(objectNode);
        C11670nb r2 = new C11670nb("inbox2_click");
        r2.A0D("pigeon_reserved_keyword_module", "inbox2");
        r2.A0B("i", objectNode);
        this.A01.A09(r2);
    }

    public C32871mT(AnonymousClass1XY r3, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, AnonymousClass0jE r5, InboxSourceLoggingData inboxSourceLoggingData) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = deprecatedAnalyticsLogger;
        this.A03 = r5;
        this.A02 = inboxSourceLoggingData;
    }

    private static void A02(ObjectNode objectNode, String str, List list) {
        ArrayNode putArray = objectNode.putArray(str);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            putArray._children.add(new LongNode(((Long) it.next()).longValue()));
        }
    }

    public void BJd(Collection collection) {
        collection.size();
        if (!collection.isEmpty()) {
            if (C010708t.A0X(2)) {
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    it.next();
                }
            }
            ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
            HashMultimap hashMultimap = new HashMultimap();
            Iterator it2 = collection.iterator();
            while (it2.hasNext()) {
                C24201Sr r4 = (C24201Sr) it2.next();
                InboxTrackableItem inboxTrackableItem = (InboxTrackableItem) r4.A05;
                boolean z = true;
                if (!inboxTrackableItem.A0B && ((inboxTrackableItem.A04 != C33901oK.A0b || !((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A1f, false)) && !((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A5f, false))) {
                    z = false;
                }
                if (z) {
                    ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
                    A00(objectNode, (InboxTrackableItem) r4.A05);
                    objectNode.put("t", StringFormatUtil.formatStrLocaleSafe("%.3f", Float.valueOf(((float) r4.A02) / 1000.0f)));
                    objectNode.put("h", r4.A00);
                    if (((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A5f, false)) {
                        A02(objectNode, "its", r4.A06);
                        A02(objectNode, "vts", r4.A07);
                    }
                    InboxSourceLoggingData.A00(objectNode, this.A02);
                    arrayNode.add(objectNode);
                } else {
                    hashMultimap.Byx(((InboxTrackableItem) r4.A05).A0A, r4);
                }
            }
            for (Map.Entry value : hashMultimap.AOp().entrySet()) {
                Collection<C24201Sr> collection2 = (Collection) value.getValue();
                Preconditions.checkArgument(!collection2.isEmpty());
                int i = Integer.MAX_VALUE;
                long j = 0;
                InboxTrackableItem inboxTrackableItem2 = null;
                for (C24201Sr r1 : collection2) {
                    inboxTrackableItem2 = (InboxTrackableItem) r1.A05;
                    i = Math.min(i, inboxTrackableItem2.A00);
                    j += r1.A02;
                }
                long size = j / ((long) collection2.size());
                Preconditions.checkNotNull(inboxTrackableItem2);
                ObjectNode objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                if (C010708t.A0X(2)) {
                    objectNode2.put("u", inboxTrackableItem2.A0A);
                }
                objectNode2.put("id", inboxTrackableItem2.A08);
                objectNode2.put("p", i);
                objectNode2.put("up", inboxTrackableItem2.A02);
                if (!C06850cB.A0B(inboxTrackableItem2.A09)) {
                    objectNode2.put("ulg", inboxTrackableItem2.A09);
                }
                objectNode2.put("t", StringFormatUtil.formatStrLocaleSafe("%.3f", Float.valueOf(((float) size) / 1000.0f)));
                objectNode2.put("n", collection2.size());
                InboxSourceLoggingData.A00(objectNode2, this.A02);
                A01(objectNode2, inboxTrackableItem2);
                arrayNode.add(objectNode2);
            }
            A03(arrayNode);
            C11670nb r2 = new C11670nb("inbox2_vpv");
            r2.A0D("pigeon_reserved_keyword_module", "inbox2");
            r2.A0B("is", arrayNode);
            this.A01.A09(r2);
        }
    }
}
