package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.0Ky  reason: invalid class name and case insensitive filesystem */
public final class C03280Ky {
    public final File A00;

    public C03280Ky(Context context, String str) {
        this.A00 = new File(context.getFilesDir(), AnonymousClass08S.A0J("mqtt_analytics.", str));
    }
}
