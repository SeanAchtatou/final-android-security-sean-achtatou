package X;

import android.content.Context;

/* renamed from: X.0GK  reason: invalid class name */
public final class AnonymousClass0GK {
    private static C71533cZ A00;

    public static synchronized C71533cZ A00(Context context) {
        C71533cZ r0;
        synchronized (AnonymousClass0GK.class) {
            if (A00 == null) {
                Context applicationContext = context.getApplicationContext();
                A00 = new C71523cY(new C71543ca(applicationContext, applicationContext.getPackageName()), applicationContext, applicationContext.getPackageName());
            }
            r0 = A00;
        }
        return r0;
    }
}
