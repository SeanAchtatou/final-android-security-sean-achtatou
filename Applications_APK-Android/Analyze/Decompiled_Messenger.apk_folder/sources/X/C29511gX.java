package X;

import com.facebook.acra.ACRA;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1gX  reason: invalid class name and case insensitive filesystem */
public final class C29511gX implements C29521gY {
    public static final C29511gX A00 = new C29511gX();
    private static final int A01 = C012509m.A02(872415231, C15320v6.MEASURED_STATE_MASK);

    public int AjS(MigColorScheme migColorScheme, Object obj) {
        C29481gU r5 = (C29481gU) obj;
        switch (r5.ordinal()) {
            case 0:
                return -15096833;
            case 1:
            case 2:
                return -1;
            case 3:
            case 4:
                return -2130706433;
            case 5:
                return 872415231;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return C15320v6.MEASURED_STATE_MASK;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return A01;
            default:
                throw new IllegalArgumentException(AnonymousClass08S.A0J("No dark scheme mapping defined for usage color: ", r5.name()));
        }
    }

    public int AsM(MigColorScheme migColorScheme, Object obj) {
        C29481gU r5 = (C29481gU) obj;
        switch (r5.ordinal()) {
            case 0:
                return -16737793;
            case 1:
                return C15320v6.MEASURED_STATE_MASK;
            case 2:
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return -1;
            case 3:
                return Integer.MIN_VALUE;
            case 4:
                return 1459617792;
            case 5:
                return 520093696;
            default:
                throw new IllegalArgumentException(AnonymousClass08S.A0J("No light scheme mapping defined for usage color: ", r5.name()));
        }
    }

    private C29511gX() {
    }
}
