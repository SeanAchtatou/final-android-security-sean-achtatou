package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import android.os.RemoteException;
import android.os.SystemClock;
import com.facebook.push.fbns.ipc.FbnsAIDLRequest;
import com.facebook.push.fbns.ipc.FbnsAIDLResult;
import com.facebook.push.fbns.ipc.IFbnsAIDLService;
import com.facebook.rti.push.service.FbnsService;
import java.util.Iterator;
import java.util.concurrent.Callable;

/* renamed from: X.0Hy  reason: invalid class name and case insensitive filesystem */
public final class C03090Hy implements Callable {
    public final /* synthetic */ FbnsAIDLRequest A00;
    public final /* synthetic */ C03040Hr A01;

    public C03090Hy(C03040Hr r1, FbnsAIDLRequest fbnsAIDLRequest) {
        this.A01 = r1;
        this.A00 = fbnsAIDLRequest;
    }

    public Object call() {
        RemoteException remoteException;
        IFbnsAIDLService iFbnsAIDLService;
        boolean z;
        boolean z2;
        String packageName;
        try {
            C03040Hr r4 = this.A01;
            synchronized (r4) {
                r4.A00++;
                long j = 200;
                int i = 1;
                while (true) {
                    if (!C03040Hr.A02(r4)) {
                        if (i > 5) {
                            C010708t.A0O("FbnsAIDLClientManager", "Max Try reached for binding to FbnsAIDLService, threadId %d", Long.valueOf(Thread.currentThread().getId()));
                            break;
                        }
                        Thread currentThread = Thread.currentThread();
                        currentThread.getId();
                        SystemClock.elapsedRealtime();
                        synchronized (r4) {
                            try {
                                z = false;
                                if (r4.A02 == AnonymousClass07B.A01) {
                                    z = true;
                                }
                            } catch (Throwable th) {
                                th = th;
                                throw th;
                            }
                        }
                        if (!z) {
                            if (Looper.getMainLooper().getThread() == currentThread) {
                                C010708t.A0I("FbnsAIDLClientManager", "This operation can't be run on UI thread");
                                z2 = true;
                            } else {
                                currentThread.getId();
                                SystemClock.elapsedRealtime();
                                Context context = r4.A03;
                                Iterator it = AnonymousClass0A9.A01.iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        packageName = context.getPackageName();
                                        break;
                                    }
                                    packageName = (String) it.next();
                                    if (AnonymousClass0AB.A01(context, packageName, C009207y.A01)) {
                                        break;
                                    }
                                }
                                ComponentName componentName = new ComponentName(packageName, FbnsService.A03(packageName));
                                Intent intent = new Intent(IFbnsAIDLService.class.getName());
                                intent.setComponent(componentName);
                                new C012309k(r4.A03, null).A01(intent);
                                synchronized (r4) {
                                    z2 = false;
                                    try {
                                        if (C006406k.A02(r4.A03, intent, r4.A04, 1, -958744252)) {
                                            r4.A02 = AnonymousClass07B.A01;
                                        } else {
                                            C010708t.A0I("FbnsAIDLClientManager", "open failed: bindService failure, do unbind to let service shutdown");
                                            C006406k.A01(r4.A03, r4.A04, 727883886);
                                        }
                                    } catch (SecurityException e) {
                                        C010708t.A0L("FbnsAIDLClientManager", "open failed: bindService throw SecurityException", e);
                                        z2 = true;
                                    } catch (Throwable th2) {
                                        th = th2;
                                        throw th;
                                    }
                                }
                            }
                            if (z2) {
                                break;
                            }
                        }
                        r4.wait(j);
                        j *= 2;
                        i++;
                    }
                }
            }
            C03040Hr r5 = this.A01;
            FbnsAIDLRequest fbnsAIDLRequest = this.A00;
            FbnsAIDLResult fbnsAIDLResult = new FbnsAIDLResult(Bundle.EMPTY);
            try {
                synchronized (r5) {
                    if (C03040Hr.A02(r5)) {
                        iFbnsAIDLService = r5.A01;
                        if (iFbnsAIDLService == null) {
                            remoteException = new RemoteException("AIDLService is null");
                        }
                    } else {
                        remoteException = new RemoteException("AIDLService is not bound");
                    }
                    throw remoteException;
                }
                C03080Hx r0 = (C03080Hx) C03080Hx.A00.get(Integer.valueOf(fbnsAIDLRequest.A00));
                if (r0 == null) {
                    r0 = C03080Hx.NOT_EXIST;
                }
                if (r0.mHasReturn) {
                    Bundle bundle = fbnsAIDLRequest.A00;
                    if (bundle == null) {
                        bundle = Bundle.EMPTY;
                    }
                    bundle.toString();
                    fbnsAIDLResult = iFbnsAIDLService.BzG(fbnsAIDLRequest);
                } else {
                    Bundle bundle2 = fbnsAIDLRequest.A00;
                    if (bundle2 == null) {
                        bundle2 = Bundle.EMPTY;
                    }
                    bundle2.toString();
                    iFbnsAIDLService.CK8(fbnsAIDLRequest);
                }
            } catch (DeadObjectException e2) {
                C010708t.A0L("FbnsAIDLClientManager", "Fbns AIDL request got DeadObjectException", e2);
            } catch (RemoteException e3) {
                C010708t.A0L("FbnsAIDLClientManager", "Fbns AIDL request got RemoteException", e3);
            }
            return fbnsAIDLResult;
        } finally {
            C03040Hr.A00(this.A01);
        }
    }
}
