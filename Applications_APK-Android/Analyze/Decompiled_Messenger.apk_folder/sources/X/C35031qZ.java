package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1qZ  reason: invalid class name and case insensitive filesystem */
public final class C35031qZ {
    public String A00;
    public boolean A01;

    public C35031qZ() {
        this.A00 = BuildConfig.FLAVOR;
    }

    public C35031qZ(C35041qa r3) {
        C28931fb.A05(r3);
        if (r3 instanceof C35041qa) {
            this.A01 = r3.A01;
            this.A00 = r3.A00;
            return;
        }
        this.A01 = r3.A01;
        String str = r3.A00;
        this.A00 = str;
        C28931fb.A06(str, "text");
    }
}
