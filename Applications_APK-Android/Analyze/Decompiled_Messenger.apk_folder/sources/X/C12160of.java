package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0of  reason: invalid class name and case insensitive filesystem */
public final class C12160of implements AnonymousClass06B {
    private static volatile C12160of A01;
    private long A00;

    public synchronized void A01(long j) {
        this.A00 = Math.max(j, this.A00);
    }

    public synchronized long now() {
        long currentTimeMillis = System.currentTimeMillis();
        long j = this.A00;
        if (currentTimeMillis > j) {
            return currentTimeMillis;
        }
        long j2 = j + 1;
        this.A00 = j2;
        return j2;
    }

    public static final C12160of A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C12160of.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C12160of();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
