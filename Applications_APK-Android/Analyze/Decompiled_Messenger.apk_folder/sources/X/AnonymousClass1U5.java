package X;

import java.util.Set;

/* renamed from: X.1U5  reason: invalid class name */
public final class AnonymousClass1U5 {
    private final Set A00;

    public static final AnonymousClass1U5 A00(AnonymousClass1XY r3) {
        return new AnonymousClass1U5(new AnonymousClass0X5(r3, AnonymousClass0X6.A10));
    }

    public void A01(C10880l0 r6) {
        C005505z.A03("GraphQLDefaultParameters.applyDefaultParameters", -1986811628);
        try {
            for (AnonymousClass1U7 r3 : this.A00) {
                C24971Xv it = r3.Ax9().iterator();
                while (it.hasNext()) {
                    String str = (String) it.next();
                    if (r6.A0C(str)) {
                        r6.A08(str, r3.AxA(str, r6));
                    }
                }
            }
        } finally {
            C005505z.A00(-1017177530);
        }
    }

    private AnonymousClass1U5(Set set) {
        this.A00 = set;
    }
}
