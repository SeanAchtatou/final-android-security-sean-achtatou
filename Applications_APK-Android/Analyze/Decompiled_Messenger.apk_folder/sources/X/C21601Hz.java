package X;

import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.messaging.inboxfolder.InboxUnitFolderItem;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1Hz  reason: invalid class name and case insensitive filesystem */
public final class C21601Hz {
    public int A00;
    public final List A01 = new ArrayList();

    public ImmutableList A00(ImmutableList immutableList) {
        if (!(!this.A01.isEmpty())) {
            return immutableList;
        }
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            InboxUnitItem inboxUnitItem = (InboxUnitItem) it.next();
            for (AnonymousClass1I0 r1 : this.A01) {
                if (r1.A03(inboxUnitItem) && (inboxUnitItem instanceof InboxUnitThreadItem)) {
                    r1.A00.add((InboxUnitThreadItem) inboxUnitItem);
                }
            }
        }
        ArrayList arrayList = new ArrayList();
        C24971Xv it2 = immutableList.iterator();
        while (it2.hasNext()) {
            InboxUnitItem inboxUnitItem2 = (InboxUnitItem) it2.next();
            boolean z = false;
            Iterator it3 = this.A01.iterator();
            while (true) {
                if (!it3.hasNext()) {
                    break;
                }
                AnonymousClass1I0 r6 = (AnonymousClass1I0) it3.next();
                if (r6.A03(inboxUnitItem2)) {
                    if (r6.A02() && !r6.A01) {
                        ArrayList arrayList2 = new ArrayList();
                        for (InboxUnitThreadItem inboxUnitThreadItem : r6.A00) {
                            ThreadSummary threadSummary = inboxUnitThreadItem.A01;
                            if (threadSummary.A0B()) {
                                arrayList2.add(threadSummary);
                            }
                        }
                        ImmutableList copyOf = ImmutableList.copyOf((Collection) arrayList2);
                        C64653Cs r2 = new C64653Cs(inboxUnitItem2.A02);
                        r2.A01 = r6.A00();
                        r2.A02 = ((InboxUnitThreadItem) r6.A00.get(0)).A01;
                        r2.A03 = copyOf;
                        arrayList.add(new InboxUnitFolderItem(r2));
                        r6.A01 = true;
                    }
                    z = true;
                }
            }
            if (!z) {
                arrayList.add(inboxUnitItem2);
            }
        }
        this.A00 = immutableList.size() - arrayList.size();
        return ImmutableList.copyOf((Collection) arrayList);
    }
}
