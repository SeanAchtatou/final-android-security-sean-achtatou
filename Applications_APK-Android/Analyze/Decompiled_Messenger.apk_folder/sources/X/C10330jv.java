package X;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/* renamed from: X.0jv  reason: invalid class name and case insensitive filesystem */
public final class C10330jv extends DateFormat {
    public static final String[] ALL_FORMATS;
    public static final DateFormat DATE_FORMAT_ISO8601;
    public static final DateFormat DATE_FORMAT_ISO8601_Z;
    public static final DateFormat DATE_FORMAT_PLAIN;
    public static final DateFormat DATE_FORMAT_RFC1123;
    public static final TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("GMT");
    public static final C10330jv instance = new C10330jv();
    public transient DateFormat _formatISO8601;
    public transient DateFormat _formatISO8601_z;
    public transient DateFormat _formatPlain;
    public transient DateFormat _formatRFC1123;
    public transient TimeZone _timezone;

    static {
        String $const$string = AnonymousClass80H.$const$string(AnonymousClass1Y3.A1d);
        ALL_FORMATS = new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM yyyy HH:mm:ss zzz", $const$string};
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        DATE_FORMAT_RFC1123 = simpleDateFormat;
        TimeZone timeZone = DEFAULT_TIMEZONE;
        simpleDateFormat.setTimeZone(timeZone);
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        DATE_FORMAT_ISO8601 = simpleDateFormat2;
        simpleDateFormat2.setTimeZone(timeZone);
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DATE_FORMAT_ISO8601_Z = simpleDateFormat3;
        simpleDateFormat3.setTimeZone(timeZone);
        SimpleDateFormat simpleDateFormat4 = new SimpleDateFormat($const$string);
        DATE_FORMAT_PLAIN = simpleDateFormat4;
        simpleDateFormat4.setTimeZone(timeZone);
    }

    private final DateFormat _cloneFormat(DateFormat dateFormat) {
        TimeZone timeZone = this._timezone;
        DateFormat dateFormat2 = (DateFormat) dateFormat.clone();
        if (timeZone != null) {
            dateFormat2.setTimeZone(timeZone);
        }
        return dateFormat2;
    }

    public /* bridge */ /* synthetic */ Object clone() {
        return new C10330jv();
    }

    public StringBuffer format(Date date, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        if (this._formatISO8601 == null) {
            this._formatISO8601 = _cloneFormat(DATE_FORMAT_ISO8601);
        }
        return this._formatISO8601.format(date, stringBuffer, fieldPosition);
    }

    public void setTimeZone(TimeZone timeZone) {
        if (timeZone != this._timezone) {
            this._formatRFC1123 = null;
            this._formatISO8601 = null;
            this._formatISO8601_z = null;
            this._formatPlain = null;
            this._timezone = timeZone;
        }
    }

    public C10330jv() {
    }

    public C10330jv(TimeZone timeZone) {
        this._timezone = timeZone;
    }

    public Date parse(String str) {
        String trim = str.trim();
        ParsePosition parsePosition = new ParsePosition(0);
        Date parse = parse(trim, parsePosition);
        if (parse != null) {
            return parse;
        }
        StringBuilder sb = new StringBuilder();
        for (String str2 : ALL_FORMATS) {
            if (sb.length() > 0) {
                sb.append("\", \"");
            } else {
                sb.append('\"');
            }
            sb.append(str2);
        }
        sb.append('\"');
        throw new ParseException(String.format("Can not parse date \"%s\": not compatible with any of standard forms (%s)", trim, sb.toString()), parsePosition.getErrorIndex());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0025, code lost:
        if (r10.charAt(4) != '-') goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x014b, code lost:
        if (r1 < 0) goto L_0x014d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0132  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Date parse(java.lang.String r10, java.text.ParsePosition r11) {
        /*
            r9 = this;
            int r6 = r10.length()
            r1 = 0
            r0 = 5
            if (r6 < r0) goto L_0x0027
            char r0 = r10.charAt(r1)
            boolean r0 = java.lang.Character.isDigit(r0)
            if (r0 == 0) goto L_0x0027
            r0 = 3
            char r0 = r10.charAt(r0)
            boolean r0 = java.lang.Character.isDigit(r0)
            if (r0 == 0) goto L_0x0027
            r0 = 4
            char r2 = r10.charAt(r0)
            r1 = 45
            r0 = 1
            if (r2 == r1) goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            if (r0 == 0) goto L_0x0119
            int r2 = r6 + -1
            char r1 = r10.charAt(r2)
            r0 = 10
            if (r6 > r0) goto L_0x004b
            boolean r0 = java.lang.Character.isDigit(r1)
            if (r0 == 0) goto L_0x004b
            java.text.DateFormat r1 = r9._formatPlain
            if (r1 != 0) goto L_0x0046
            java.text.DateFormat r0 = X.C10330jv.DATE_FORMAT_PLAIN
            java.text.DateFormat r1 = r9._cloneFormat(r0)
            r9._formatPlain = r1
        L_0x0046:
            java.util.Date r0 = r1.parse(r10, r11)
            return r0
        L_0x004b:
            r7 = 58
            r5 = 90
            java.lang.String r4 = ".000"
            if (r1 != r5) goto L_0x0074
            java.text.DateFormat r1 = r9._formatISO8601_z
            if (r1 != 0) goto L_0x005f
            java.text.DateFormat r0 = X.C10330jv.DATE_FORMAT_ISO8601_Z
            java.text.DateFormat r1 = r9._cloneFormat(r0)
            r9._formatISO8601_z = r1
        L_0x005f:
            int r0 = r6 + -4
            char r0 = r10.charAt(r0)
            if (r0 != r7) goto L_0x0046
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r10)
            r0.insert(r2, r4)
            java.lang.String r10 = r0.toString()
            goto L_0x0046
        L_0x0074:
            r0 = 6
            if (r6 < r0) goto L_0x00ed
            int r0 = r6 + -6
            char r0 = r10.charAt(r0)
            r2 = 43
            if (r0 == r2) goto L_0x0099
            r1 = 45
            if (r0 == r1) goto L_0x0099
            int r0 = r6 + -5
            char r0 = r10.charAt(r0)
            if (r0 == r2) goto L_0x0099
            if (r0 == r1) goto L_0x0099
            int r0 = r6 + -3
            char r0 = r10.charAt(r0)
            if (r0 == r2) goto L_0x0099
            if (r0 != r1) goto L_0x00ed
        L_0x0099:
            r0 = 1
        L_0x009a:
            if (r0 == 0) goto L_0x00ef
            int r2 = r6 + -3
            char r1 = r10.charAt(r2)
            if (r1 != r7) goto L_0x00de
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r10)
            int r0 = r6 + -2
            r1.delete(r2, r0)
            java.lang.String r10 = r1.toString()
        L_0x00b2:
            int r2 = r10.length()
            int r0 = r2 + -9
            char r0 = r10.charAt(r0)
            boolean r0 = java.lang.Character.isDigit(r0)
            if (r0 == 0) goto L_0x00d0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r10)
            int r0 = r2 + -5
            r1.insert(r0, r4)
            java.lang.String r10 = r1.toString()
        L_0x00d0:
            java.text.DateFormat r1 = r9._formatISO8601
            if (r1 != 0) goto L_0x0046
            java.text.DateFormat r0 = X.C10330jv.DATE_FORMAT_ISO8601
            java.text.DateFormat r1 = r9._cloneFormat(r0)
            r9._formatISO8601 = r1
            goto L_0x0046
        L_0x00de:
            r0 = 43
            if (r1 == r0) goto L_0x00e6
            r0 = 45
            if (r1 != r0) goto L_0x00b2
        L_0x00e6:
            java.lang.String r0 = "00"
            java.lang.String r10 = X.AnonymousClass08S.A0J(r10, r0)
            goto L_0x00b2
        L_0x00ed:
            r0 = 0
            goto L_0x009a
        L_0x00ef:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r10)
            r0 = 84
            int r0 = r10.lastIndexOf(r0)
            int r6 = r6 - r0
            int r1 = r6 + -1
            r0 = 8
            if (r1 > r0) goto L_0x0104
            r2.append(r4)
        L_0x0104:
            r2.append(r5)
            java.lang.String r10 = r2.toString()
            java.text.DateFormat r1 = r9._formatISO8601_z
            if (r1 != 0) goto L_0x0046
            java.text.DateFormat r0 = X.C10330jv.DATE_FORMAT_ISO8601_Z
            java.text.DateFormat r1 = r9._cloneFormat(r0)
            r9._formatISO8601_z = r1
            goto L_0x0046
        L_0x0119:
            r2 = r6
        L_0x011a:
            int r2 = r2 + -1
            if (r2 < 0) goto L_0x0130
            char r1 = r10.charAt(r2)
            r0 = 48
            if (r1 < r0) goto L_0x012a
            r0 = 57
            if (r1 <= r0) goto L_0x011a
        L_0x012a:
            if (r2 > 0) goto L_0x0130
            r0 = 45
            if (r1 == r0) goto L_0x011a
        L_0x0130:
            if (r2 >= 0) goto L_0x015e
            java.lang.String r5 = X.C29491gV.MAX_LONG_STR
            int r4 = r5.length()
            r3 = 1
            if (r6 < r4) goto L_0x014d
            if (r6 > r4) goto L_0x015c
            r2 = 0
        L_0x013e:
            if (r2 >= r4) goto L_0x014d
            char r1 = r10.charAt(r2)
            char r0 = r5.charAt(r2)
            int r1 = r1 - r0
            if (r1 == 0) goto L_0x0159
            if (r1 >= 0) goto L_0x015c
        L_0x014d:
            if (r3 == 0) goto L_0x015e
            java.util.Date r2 = new java.util.Date
            long r0 = java.lang.Long.parseLong(r10)
            r2.<init>(r0)
            return r2
        L_0x0159:
            int r2 = r2 + 1
            goto L_0x013e
        L_0x015c:
            r3 = 0
            goto L_0x014d
        L_0x015e:
            java.text.DateFormat r0 = r9._formatRFC1123
            if (r0 != 0) goto L_0x016a
            java.text.DateFormat r0 = X.C10330jv.DATE_FORMAT_RFC1123
            java.text.DateFormat r0 = r9._cloneFormat(r0)
            r9._formatRFC1123 = r0
        L_0x016a:
            java.text.DateFormat r0 = r9._formatRFC1123
            java.util.Date r0 = r0.parse(r10, r11)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10330jv.parse(java.lang.String, java.text.ParsePosition):java.util.Date");
    }
}
