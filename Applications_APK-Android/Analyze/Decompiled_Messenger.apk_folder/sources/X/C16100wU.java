package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0wU  reason: invalid class name and case insensitive filesystem */
public final class C16100wU implements Runnable {
    public static final String __redex_internal_original_name = "androidx.recyclerview.widget.RecyclerView$2";
    public final /* synthetic */ RecyclerView A00;

    public C16100wU(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    public void run() {
        C20221Bj r0 = this.A00.A0K;
        if (r0 != null) {
            r0.A08();
        }
        this.A00.A08 = false;
    }
}
