package X;

import android.content.Context;
import android.graphics.Bitmap;

/* renamed from: X.1O0  reason: invalid class name */
public final class AnonymousClass1O0 implements C22791Mv {
    public final /* synthetic */ C23021Nx A00;
    public final /* synthetic */ C25051Yd A01;

    public AnonymousClass30A Ad2(Context context) {
        return null;
    }

    public AnonymousClass1O0(C25051Yd r1, C23021Nx r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public C22761Ms Anz(Bitmap.Config config) {
        return new AnonymousClass1LW(this, config);
    }

    public C22761Ms B9o(Bitmap.Config config) {
        return new C30701iV(this, config);
    }
}
