package X;

/* renamed from: X.1Xg  reason: invalid class name and case insensitive filesystem */
public abstract class C24831Xg extends C24841Xh implements C24851Xi, AnonymousClass062 {
    public final AnonymousClass1XX A00;

    public AnonymousClass0US getLazy(C22916BKm bKm) {
        C04310Tq provider = this.A00.getScopeUnawareInjector().getProvider(bKm);
        if (provider instanceof AnonymousClass0US) {
            return (AnonymousClass0US) provider;
        }
        return new EIG(provider, this);
    }

    public C04310Tq getProvider(C22916BKm bKm) {
        return new AnonymousClass48Z(this, this.A00.getScopeUnawareInjector().getProvider(bKm));
    }

    public C24831Xg(AnonymousClass1XX r1) {
        super(r1);
        this.A00 = r1;
    }

    public Object getInstance(int i) {
        Object AYf = AYf();
        try {
            return this.A00.getScopeUnawareInjector().getInstance(i);
        } finally {
            AZH(AYf);
        }
    }

    public Object getInstance(C22916BKm bKm) {
        Object AYf = AYf();
        try {
            return this.A00.getScopeUnawareInjector().getInstance(bKm);
        } finally {
            AZH(AYf);
        }
    }
}
