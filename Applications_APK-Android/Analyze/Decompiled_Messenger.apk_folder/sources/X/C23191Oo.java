package X;

/* renamed from: X.1Oo  reason: invalid class name and case insensitive filesystem */
public final class C23191Oo {
    public String A00;
    public boolean A01;
    private double A02;
    private long A03;
    private String A04;

    public static String A00(String str) {
        if (str == null) {
            return null;
        }
        char c = 65535;
        switch (str.hashCode()) {
            case -1838656495:
                if (str.equals("STRING")) {
                    c = 3;
                    break;
                }
                break;
            case 72655:
                if (str.equals("INT")) {
                    c = 1;
                    break;
                }
                break;
            case 2044650:
                if (str.equals("BOOL")) {
                    c = 0;
                    break;
                }
                break;
            case 66988604:
                if (str.equals("FLOAT")) {
                    c = 2;
                    break;
                }
                break;
        }
        if (c == 0 || c == 1 || c == 2 || c == 3) {
            return str;
        }
        return null;
    }

    public double A01() {
        String str = this.A00;
        if (str.equals("INT")) {
            return (double) this.A03;
        }
        if (str.equals("FLOAT")) {
            return this.A02;
        }
        throw new C37861wT("Invalid value type");
    }

    public long A02() {
        String str = this.A00;
        if (str.equals("INT")) {
            return this.A03;
        }
        if (str.equals("FLOAT")) {
            return (long) this.A02;
        }
        throw new C37861wT("Invalid value type");
    }

    public String toString() {
        String str = this.A04;
        if (str != null) {
            return str;
        }
        String str2 = this.A00;
        char c = 65535;
        int hashCode = str2.hashCode();
        if (hashCode != 72655) {
            if (hashCode != 2044650) {
                if (hashCode == 66988604 && str2.equals("FLOAT")) {
                    c = 2;
                }
            } else if (str2.equals("BOOL")) {
                c = 0;
            }
        } else if (str2.equals("INT")) {
            c = 1;
        }
        if (c == 0) {
            this.A04 = String.valueOf(this.A01);
        } else if (c == 1) {
            this.A04 = String.valueOf(this.A03);
        } else if (c != 2) {
            this.A04 = "n/a";
        } else {
            this.A04 = String.valueOf(this.A02);
        }
        return this.A04;
    }

    public C23191Oo(double d) {
        this.A00 = "FLOAT";
        this.A02 = d;
    }

    public C23191Oo(int i) {
        this.A00 = "INT";
        this.A03 = (long) i;
    }

    public C23191Oo(long j) {
        this.A00 = "INT";
        this.A03 = j;
    }

    public C23191Oo(String str) {
        if (str == null) {
            this.A00 = "NULL";
            this.A04 = "n/a";
            return;
        }
        this.A00 = "STRING";
        this.A04 = str;
    }

    public C23191Oo(String str, String str2) {
        if (str == null) {
            throw new C37861wT("Invalid value type");
        } else if (str2 == null) {
            this.A00 = "NULL";
        } else {
            char c = 65535;
            switch (str.hashCode()) {
                case -1838656495:
                    if (str.equals("STRING")) {
                        c = 3;
                        break;
                    }
                    break;
                case 72655:
                    if (str.equals("INT")) {
                        c = 1;
                        break;
                    }
                    break;
                case 2044650:
                    if (str.equals("BOOL")) {
                        c = 0;
                        break;
                    }
                    break;
                case 66988604:
                    if (str.equals("FLOAT")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            if (c != 0) {
                if (c == 1) {
                    try {
                        this.A03 = Long.parseLong(str2);
                    } catch (NumberFormatException unused) {
                        throw new C37861wT("Invalid integer", str2);
                    }
                } else if (c == 2) {
                    try {
                        this.A02 = Double.parseDouble(str2);
                    } catch (NumberFormatException unused2) {
                        throw new C37861wT("Invalid float", str2);
                    }
                } else if (c == 3) {
                    this.A04 = str2;
                }
            } else if (str2.equals("true") || str2.equals("false")) {
                this.A01 = Boolean.parseBoolean(str2);
            } else {
                throw new C37861wT("Invalid boolean", str2);
            }
            this.A00 = str;
        }
    }

    public C23191Oo(boolean z) {
        this.A00 = "BOOL";
        this.A01 = z;
    }
}
