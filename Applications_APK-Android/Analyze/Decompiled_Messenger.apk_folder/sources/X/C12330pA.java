package X;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.facebook.common.util.StringLocaleUtil;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* renamed from: X.0pA  reason: invalid class name and case insensitive filesystem */
public final class C12330pA extends C12340pB {
    public static final char[] A0L = "0123456789abcdef".toCharArray();
    public int A00;
    public C97964mI A01;
    public C860146i A02;
    public C97954mH A03;
    public Integer A04;
    public Long A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public Map A0D;
    public final int A0E;
    public final Class A0F;
    public final String A0G;
    public final String A0H;
    public final Map A0I;
    public final Map A0J;
    public final boolean A0K;

    public static String A00() {
        byte[] bArr = new byte[3];
        new Random().nextBytes(bArr);
        char[] cArr = new char[(3 << 1)];
        for (int i = 0; i < 3; i++) {
            byte b = bArr[i] & 255;
            int i2 = i << 1;
            char[] cArr2 = A0L;
            cArr[i2] = cArr2[b >>> 4];
            cArr[i2 + 1] = cArr2[b & 15];
        }
        return new String(cArr).substring(0, 5);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        if (r1.equals(r5.A0G) == false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0132
            r2 = 0
            if (r5 == 0) goto L_0x0030
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x0030
            X.0pA r5 = (X.C12330pA) r5
            int r1 = r4.A0E
            int r0 = r5.A0E
            if (r1 != r0) goto L_0x0030
            int r1 = r4.A00
            int r0 = r5.A00
            if (r1 != r0) goto L_0x0030
            boolean r1 = r4.A0K
            boolean r0 = r5.A0K
            if (r1 != r0) goto L_0x0030
            java.lang.String r1 = r4.A0G
            if (r1 == 0) goto L_0x0031
            java.lang.String r0 = r5.A0G
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0036
        L_0x0030:
            return r2
        L_0x0031:
            java.lang.String r0 = r5.A0G
            if (r0 == 0) goto L_0x0036
            return r2
        L_0x0036:
            java.lang.Class r1 = r4.A0F
            java.lang.Class r0 = r5.A0F
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0030
            java.lang.String r1 = r4.A0H
            java.lang.String r0 = r5.A0H
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0030
            java.lang.String r1 = r4.A0B
            java.lang.String r0 = r5.A0B
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0030
            java.lang.String r1 = r4.A06
            if (r1 == 0) goto L_0x0061
            java.lang.String r0 = r5.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0066
            return r2
        L_0x0061:
            java.lang.String r0 = r5.A06
            if (r0 == 0) goto L_0x0066
            return r2
        L_0x0066:
            java.lang.String r1 = r4.A09
            java.lang.String r0 = r5.A09
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0030
            java.lang.String r1 = r4.A0C
            if (r1 == 0) goto L_0x007d
            java.lang.String r0 = r5.A0C
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0082
            return r2
        L_0x007d:
            java.lang.String r0 = r5.A0C
            if (r0 == 0) goto L_0x0082
            return r2
        L_0x0082:
            java.lang.String r1 = r4.A08
            if (r1 == 0) goto L_0x008f
            java.lang.String r0 = r5.A08
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0094
            return r2
        L_0x008f:
            java.lang.String r0 = r5.A08
            if (r0 == 0) goto L_0x0094
            return r2
        L_0x0094:
            java.lang.Integer r1 = r4.A04
            if (r1 == 0) goto L_0x00a1
            java.lang.Integer r0 = r5.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00a6
            return r2
        L_0x00a1:
            java.lang.Integer r0 = r5.A04
            if (r0 == 0) goto L_0x00a6
            return r2
        L_0x00a6:
            java.lang.String r1 = r4.A07
            if (r1 == 0) goto L_0x00b3
            java.lang.String r0 = r5.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00b8
            return r2
        L_0x00b3:
            java.lang.String r0 = r5.A07
            if (r0 == 0) goto L_0x00b8
            return r2
        L_0x00b8:
            X.4mI r1 = r4.A01
            if (r1 == 0) goto L_0x00c5
            X.4mI r0 = r5.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00ca
            return r2
        L_0x00c5:
            X.4mI r0 = r5.A01
            if (r0 == 0) goto L_0x00ca
            return r2
        L_0x00ca:
            X.46i r1 = r4.A02
            if (r1 == 0) goto L_0x00d7
            X.46i r0 = r5.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00dc
            return r2
        L_0x00d7:
            X.46i r0 = r5.A02
            if (r0 == 0) goto L_0x00dc
            return r2
        L_0x00dc:
            java.lang.Long r1 = r4.A05
            if (r1 == 0) goto L_0x00e9
            java.lang.Long r0 = r5.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00ee
            return r2
        L_0x00e9:
            java.lang.Long r0 = r5.A05
            if (r0 == 0) goto L_0x00ee
            return r2
        L_0x00ee:
            java.lang.String r1 = r4.A0A
            if (r1 == 0) goto L_0x00fb
            java.lang.String r0 = r5.A0A
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0100
            return r2
        L_0x00fb:
            java.lang.String r0 = r5.A0A
            if (r0 == 0) goto L_0x0100
            return r2
        L_0x0100:
            java.util.Map r1 = r4.A0J
            if (r1 == 0) goto L_0x010d
            java.util.Map r0 = r5.A0J
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0112
            return r2
        L_0x010d:
            java.util.Map r0 = r5.A0J
            if (r0 == 0) goto L_0x0112
            return r2
        L_0x0112:
            java.util.Map r1 = r4.A0I
            if (r1 == 0) goto L_0x011f
            java.util.Map r0 = r5.A0I
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0124
            return r2
        L_0x011f:
            java.util.Map r0 = r5.A0I
            if (r0 == 0) goto L_0x0124
            return r2
        L_0x0124:
            X.4mH r1 = r4.A03
            X.4mH r0 = r5.A03
            if (r1 == 0) goto L_0x012f
            boolean r3 = r1.equals(r0)
            return r3
        L_0x012f:
            if (r0 == 0) goto L_0x0132
            r3 = 0
        L_0x0132:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12330pA.equals(java.lang.Object):boolean");
    }

    private void A02(Fragment fragment) {
        Object A032;
        C13020qR r0 = fragment.A0N;
        if (r0 == null) {
            A032 = null;
        } else {
            A032 = r0.A03();
        }
        if (A032 != null && fragment.A17() != null && fragment.A17().A0U() != null) {
            for (Fragment fragment2 : fragment.A17().A0U()) {
                if (fragment2 != null) {
                    A0L(fragment2);
                }
            }
        }
    }

    public static void A03(C12330pA r4, C97954mH r5) {
        for (Map.Entry entry : r5.A0H.entrySet()) {
            r4.A0I.remove(entry.getKey());
            A03(r4, (C97954mH) entry.getValue());
            C97954mH r0 = r4.A03;
            if (r0 != null && r0.A0C == ((Integer) entry.getKey()).intValue()) {
                r4.A03 = null;
            }
        }
    }

    public void A0L(Fragment fragment) {
        C97954mH r2;
        C97954mH r0;
        Map map;
        int i;
        if (fragment instanceof C30371Ev4) {
            C97954mH r1 = this.A03;
            if (r1 == null) {
                r2 = new C97954mH(fragment, null);
            } else {
                r2 = new C97954mH(fragment, Integer.valueOf(r1.A0C));
                r0 = this.A03;
                map = r0.A0H;
                i = r2.A0C;
                map.put(Integer.valueOf(i), r2);
                this.A0I.put(Integer.valueOf(fragment.hashCode()), r2);
                A02(fragment);
            }
        } else if (this.A0I.containsKey(Integer.valueOf(fragment.hashCode()))) {
            A02(fragment);
            return;
        } else {
            Fragment fragment2 = fragment.A0L;
            Integer num = null;
            if (fragment2 != null) {
                num = Integer.valueOf(fragment2.hashCode());
            }
            r2 = new C97954mH(fragment, num);
            if (fragment2 != null) {
                if (this.A0I.containsKey(Integer.valueOf(fragment2.hashCode()))) {
                    r0 = (C97954mH) this.A0I.get(Integer.valueOf(fragment2.hashCode()));
                    map = r0.A0H;
                    i = r2.A0C;
                    map.put(Integer.valueOf(i), r2);
                    this.A0I.put(Integer.valueOf(fragment.hashCode()), r2);
                    A02(fragment);
                }
                A0L(fragment2);
                return;
            }
        }
        map = this.A0J;
        i = fragment.hashCode();
        map.put(Integer.valueOf(i), r2);
        this.A0I.put(Integer.valueOf(fragment.hashCode()), r2);
        A02(fragment);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        if (r4.A0I.containsKey(java.lang.Integer.valueOf(r5.hashCode())) == false) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0M(androidx.fragment.app.Fragment r5) {
        /*
            r4 = this;
            if (r5 != 0) goto L_0x0006
            r0 = 0
            r4.A03 = r0
            return
        L_0x0006:
            X.4mH r3 = r4.A03
            if (r3 == 0) goto L_0x0040
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>()
        L_0x000f:
            java.lang.Integer r0 = r3.A0E
            if (r0 == 0) goto L_0x0021
            r2.add(r0)
            java.util.Map r1 = r4.A0I
            java.lang.Integer r0 = r3.A0E
            java.lang.Object r3 = r1.get(r0)
            X.4mH r3 = (X.C97954mH) r3
            goto L_0x000f
        L_0x0021:
            int r0 = r5.hashCode()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r2.contains(r0)
            if (r0 == 0) goto L_0x0030
            return
        L_0x0030:
            java.util.Map r1 = r4.A0I
            int r0 = r5.hashCode()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r1.containsKey(r0)
            if (r0 != 0) goto L_0x0043
        L_0x0040:
            r4.A0L(r5)
        L_0x0043:
            java.util.Map r1 = r4.A0I
            int r0 = r5.hashCode()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r0 = r1.get(r0)
            X.4mH r0 = (X.C97954mH) r0
            r4.A03 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12330pA.A0M(androidx.fragment.app.Fragment):void");
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18 = this.A0E * 31;
        String str = this.A0G;
        int i19 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i20 = (i18 + i) * 31;
        Class cls = this.A0F;
        if (cls != null) {
            i2 = cls.hashCode();
        } else {
            i2 = 0;
        }
        int i21 = (i20 + i2) * 31;
        String str2 = this.A0H;
        if (str2 != null) {
            i3 = str2.hashCode();
        } else {
            i3 = 0;
        }
        int i22 = (((i21 + i3) * 31) + this.A00) * 31;
        String str3 = this.A0B;
        if (str3 != null) {
            i4 = str3.hashCode();
        } else {
            i4 = 0;
        }
        int i23 = (i22 + i4) * 31;
        String str4 = this.A06;
        if (str4 != null) {
            i5 = str4.hashCode();
        } else {
            i5 = 0;
        }
        int i24 = (((i23 + i5) * 31) + (this.A0K ? 1 : 0)) * 31;
        String str5 = this.A09;
        if (str5 != null) {
            i6 = str5.hashCode();
        } else {
            i6 = 0;
        }
        int i25 = (i24 + i6) * 31;
        String str6 = this.A0C;
        if (str6 != null) {
            i7 = str6.hashCode();
        } else {
            i7 = 0;
        }
        int i26 = (i25 + i7) * 31;
        String str7 = this.A08;
        if (str7 != null) {
            i8 = str7.hashCode();
        } else {
            i8 = 0;
        }
        int i27 = (i26 + i8) * 31;
        Integer num = this.A04;
        if (num != null) {
            i9 = num.hashCode();
        } else {
            i9 = 0;
        }
        int i28 = (i27 + i9) * 31;
        String str8 = this.A07;
        if (str8 != null) {
            i10 = str8.hashCode();
        } else {
            i10 = 0;
        }
        int i29 = (i28 + i10) * 31;
        C97964mI r0 = this.A01;
        if (r0 != null) {
            i11 = r0.hashCode();
        } else {
            i11 = 0;
        }
        int i30 = (i29 + i11) * 31;
        C860146i r02 = this.A02;
        if (r02 != null) {
            i12 = r02.hashCode();
        } else {
            i12 = 0;
        }
        int i31 = (i30 + i12) * 31;
        Long l = this.A05;
        if (l != null) {
            i13 = l.hashCode();
        } else {
            i13 = 0;
        }
        int i32 = (i31 + i13) * 31;
        String str9 = this.A0A;
        if (str9 != null) {
            i14 = str9.hashCode();
        } else {
            i14 = 0;
        }
        int i33 = (i32 + i14) * 31;
        Map map = this.A0J;
        if (map != null) {
            i15 = map.hashCode();
        } else {
            i15 = 0;
        }
        int i34 = (i33 + i15) * 31;
        Map map2 = this.A0I;
        if (map2 != null) {
            i16 = map2.hashCode();
        } else {
            i16 = 0;
        }
        int i35 = (i34 + i16) * 31;
        Map map3 = this.A0D;
        if (map3 != null) {
            i17 = map3.hashCode();
        } else {
            i17 = 0;
        }
        int i36 = (i35 + i17) * 31;
        C97954mH r03 = this.A03;
        if (r03 != null) {
            i19 = r03.hashCode();
        }
        return i36 + i19;
    }

    public String toString() {
        return "ActivitySession{mActivityInstanceId=" + this.A0E + "," + "\n" + "mModuleName='" + this.A0G + '\'' + "," + "\n" + "mActivityClass=" + this.A0F + ", mSessionId='" + this.A0H + '\'' + "," + "\n" + "mSubsessionId=" + this.A00 + "," + "\n" + "mSubsessionTimestamp='" + this.A0B + '\'' + "," + "\n" + "keyURI='" + this.A06 + '\'' + "," + "\n" + "mIsFragmentActivity=" + this.A0K + "," + "\n" + "mCurrentSurfaceLinkId='" + this.A09 + '\'' + "," + "\n" + "mSurfaceName='" + this.A0C + '\'' + "," + "\n" + "mBookmarkTypeName='" + this.A08 + '\'' + "," + "\n" + "mBadgeCount=" + this.A04 + "," + "\n" + "mPromoSource='" + this.A01 + '\'' + "," + "\n" + "mPromoType='" + this.A02 + '\'' + "," + "\n" + "mPromoId=" + this.A05 + "," + "\n" + "mBadgeType='" + this.A07 + '\'' + "," + "\n" + ",mOriginalNavigationTapPoint='" + this.A0A + '\'' + "," + "\n" + ",mExtras=" + this.A0D + "," + "\n" + "mFragments=" + this.A0J + "," + "\n" + "mAllFragments=" + this.A0I + "," + "\n" + "mCurrentFragment=" + this.A03 + '}';
    }

    public static String A01(String str, Class cls) {
        return StringLocaleUtil.A00("<%s,%s>", str, AnonymousClass07V.A01(cls));
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C12330pA(android.app.Activity r9, java.lang.String r10) {
        /*
            r8 = this;
            java.lang.Class r2 = r9.getClass()
            int r4 = r9.hashCode()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            android.content.Intent r0 = r9.getIntent()
            if (r0 == 0) goto L_0x0024
            android.content.Intent r1 = r9.getIntent()
            java.lang.String r0 = "key_uri"
            java.lang.String r7 = r1.getStringExtra(r0)
        L_0x001d:
            r6 = 0
            r1 = r8
            r3 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7)
            return
        L_0x0024:
            r7 = 0
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12330pA.<init>(android.app.Activity, java.lang.String):void");
    }

    private C12330pA(Class cls, String str, int i, List list, Fragment fragment, String str2) {
        this.A0J = new HashMap();
        this.A0I = new HashMap();
        this.A0F = cls;
        this.A0G = str;
        this.A0E = i;
        this.A0H = A00();
        this.A0B = C12350pC.A04(AnonymousClass06A.A00.now());
        this.A0K = FragmentActivity.class.isAssignableFrom(this.A0F);
        this.A06 = str2;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            A0L((Fragment) it.next());
        }
        if (fragment != null) {
            A0M(fragment);
        }
    }
}
