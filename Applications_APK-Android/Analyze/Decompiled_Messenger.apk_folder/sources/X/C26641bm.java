package X;

import android.graphics.Typeface;
import java.io.File;

/* renamed from: X.1bm  reason: invalid class name and case insensitive filesystem */
public final class C26641bm implements C09950ix {
    public Object AUP(File file) {
        if (file == null) {
            return null;
        }
        return new C26671bp(file, (Typeface) C09950ix.A01.AUP(file));
    }
}
