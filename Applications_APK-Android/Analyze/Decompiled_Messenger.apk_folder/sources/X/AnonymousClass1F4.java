package X;

/* renamed from: X.1F4  reason: invalid class name */
public final class AnonymousClass1F4 extends C21061Ew implements C31011j0 {
    public static final AnonymousClass1F4 A00 = new AnonymousClass1F4();

    public C31011j0 ANQ(String str, double d) {
        return this;
    }

    public C31011j0 ANR(String str, int i) {
        return this;
    }

    public C31011j0 ANS(String str, long j) {
        return this;
    }

    public C31011j0 ANT(String str, String str2) {
        return this;
    }

    public C31011j0 ANU(String str, boolean z) {
        return this;
    }

    public C31011j0 ANV(String str, double[] dArr) {
        return this;
    }

    public C31011j0 ANW(String str, int[] iArr) {
        return this;
    }

    public C31011j0 ANX(String str, String[] strArr) {
        return this;
    }

    public C31011j0 ANY(String str, boolean[] zArr) {
        return this;
    }

    public C31011j0 Bx9(long j) {
        return this;
    }

    public C21061Ew BxA() {
        return this;
    }

    public C31011j0 BxB(boolean z) {
        return this;
    }
}
