package X;

import com.facebook.common.stringformat.StringFormatUtil;

/* renamed from: X.064  reason: invalid class name */
public final class AnonymousClass064 {
    public static void A00(Object obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
    }

    public static void A01(Object obj, String str) {
        if (obj == null) {
            throw new NullPointerException(str);
        }
    }

    public static void A02(String str) {
        if (str == null) {
            throw new RuntimeException("Expected a non-empty string, but got null");
        } else if (str.isEmpty()) {
            throw new RuntimeException("Expected a non-empty string");
        }
    }

    public static void A03(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    public static void A04(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    public static void A05(boolean z, String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    public static void A06(boolean z, String str, Object obj) {
        if (!z) {
            throw new IllegalStateException(StringFormatUtil.formatStrLocaleSafe(str, obj));
        }
    }
}
