package X;

import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1MC  reason: invalid class name */
public final class AnonymousClass1MC implements AnonymousClass1MD {
    public final /* synthetic */ InboxUnitThreadItem A00;
    public final /* synthetic */ AnonymousClass1BK A01;

    public AnonymousClass1MC(AnonymousClass1BK r1, InboxUnitThreadItem inboxUnitThreadItem) {
        this.A01 = r1;
        this.A00 = inboxUnitThreadItem;
    }

    public void BcX() {
        AnonymousClass1BK r1 = this.A01;
        if (r1 != null) {
            r1.BcY(this.A00);
        }
    }

    public void Bmj() {
        AnonymousClass1BK r1 = this.A01;
        if (r1 != null) {
            r1.Bmk(this.A00);
        }
    }
}
