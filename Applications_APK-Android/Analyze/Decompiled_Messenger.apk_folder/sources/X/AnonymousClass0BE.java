package X;

import java.util.concurrent.ExecutorService;

/* renamed from: X.0BE  reason: invalid class name */
public abstract class AnonymousClass0BE {
    public final AnonymousClass0BG A00;
    public final ExecutorService A01;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.concurrent.Future A00(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.concurrent.ExecutorService r2 = r4.A01     // Catch:{ all -> 0x0037 }
            X.0SD r1 = new X.0SD     // Catch:{ all -> 0x0037 }
            r1.<init>(r4, r5)     // Catch:{ all -> 0x0037 }
            r0 = 391670844(0x17586c3c, float:6.9929975E-25)
            java.util.concurrent.Future r3 = X.AnonymousClass07A.A03(r2, r1, r0)     // Catch:{ all -> 0x0037 }
            X.0BG r0 = r4.A00     // Catch:{ all -> 0x0037 }
            java.util.TreeSet r1 = r0.A01()     // Catch:{ all -> 0x0037 }
            boolean r0 = r1.isEmpty()     // Catch:{ all -> 0x0037 }
            if (r0 != 0) goto L_0x0035
            java.lang.Object r2 = r1.first()     // Catch:{ all -> 0x0037 }
            X.0Ry r2 = (X.AnonymousClass0Ry) r2     // Catch:{ all -> 0x0037 }
            java.lang.String r0 = r2.A02     // Catch:{ all -> 0x0037 }
            boolean r0 = r0.equals(r5)     // Catch:{ all -> 0x0037 }
            if (r0 == 0) goto L_0x0035
            int r1 = r2.A00     // Catch:{ all -> 0x0037 }
            r0 = 3
            if (r1 > r0) goto L_0x0035
            X.0CO r0 = new X.0CO     // Catch:{ all -> 0x0037 }
            r0.<init>(r2)     // Catch:{ all -> 0x0037 }
            monitor-exit(r4)
            return r0
        L_0x0035:
            monitor-exit(r4)
            return r3
        L_0x0037:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BE.A00(java.lang.String):java.util.concurrent.Future");
    }

    public synchronized void A01(AnonymousClass0Ry r8) {
        AnonymousClass0Ry A002 = this.A00.A00(r8);
        if (A002 != null) {
            this.A00.A04(A002, new AnonymousClass0Ry(A002.A02, A002.A00(), A002.A01 - 10, A002.A00 + 1));
            this.A00.A02();
        }
    }

    public AnonymousClass0BE(ExecutorService executorService, AnonymousClass0AW r6) {
        this.A01 = executorService;
        this.A00 = new AnonymousClass0BG(10, r6.AbP(AnonymousClass07B.A00), "/settings_mqtt_address");
    }
}
