package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.RunnableFuture;

/* renamed from: X.0WN  reason: invalid class name */
public abstract class AnonymousClass0WN extends AbstractExecutorService implements AnonymousClass0VL {
    /* renamed from: CIC */
    public ListenableFuture submit(Runnable runnable) {
        return (ListenableFuture) super.submit(runnable);
    }

    /* renamed from: CIE */
    public ListenableFuture submit(Runnable runnable, Object obj) {
        return (ListenableFuture) super.submit(runnable, obj);
    }

    /* renamed from: CIF */
    public ListenableFuture submit(Callable callable) {
        return (ListenableFuture) super.submit(callable);
    }

    public final RunnableFuture newTaskFor(Runnable runnable, Object obj) {
        return new C55522oD(Executors.callable(runnable, obj));
    }

    public final RunnableFuture newTaskFor(Callable callable) {
        return new C55522oD(callable);
    }
}
