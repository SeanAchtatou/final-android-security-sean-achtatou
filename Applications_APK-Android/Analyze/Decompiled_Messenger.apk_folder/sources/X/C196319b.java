package X;

import android.view.View;

/* renamed from: X.19b  reason: invalid class name and case insensitive filesystem */
public final class C196319b implements AnonymousClass19Z {
    public final /* synthetic */ AnonymousClass19T A00;

    public C196319b(AnonymousClass19T r1) {
        this.A00 = r1;
    }

    public View Ah1(int i) {
        return this.A00.A0u(i);
    }

    public int AxE() {
        AnonymousClass19T r0 = this.A00;
        return r0.A01 - r0.A0e();
    }

    public int AxG() {
        return this.A00.A0h();
    }

    public int Ah5(View view) {
        return this.A00.A0i(view) + ((AnonymousClass1R7) view.getLayoutParams()).bottomMargin;
    }

    public int Ah8(View view) {
        return this.A00.A0l(view) - ((AnonymousClass1R7) view.getLayoutParams()).topMargin;
    }
}
