package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo;

/* renamed from: X.1sE  reason: invalid class name and case insensitive filesystem */
public final class C36041sE implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxAdsMediaInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxAdsMediaInfo[i];
    }
}
