package X;

import java.io.Closeable;
import java.io.OutputStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;

/* renamed from: X.1eX  reason: invalid class name and case insensitive filesystem */
public abstract class C28271eX implements Closeable, AnonymousClass0jN {
    public int _features;

    public abstract void clearCurrentToken();

    public abstract void close();

    public abstract BigInteger getBigIntegerValue();

    public abstract byte[] getBinaryValue(C10350jx r1);

    public abstract C09980jK getCodec();

    public abstract C64443Bu getCurrentLocation();

    public abstract String getCurrentName();

    public abstract C182811d getCurrentToken();

    public abstract BigDecimal getDecimalValue();

    public abstract double getDoubleValue();

    public abstract Object getEmbeddedObject();

    public abstract float getFloatValue();

    public Object getInputSource() {
        return null;
    }

    public abstract int getIntValue();

    public abstract long getLongValue();

    public abstract C29501gW getNumberType();

    public abstract Number getNumberValue();

    public abstract String getText();

    public abstract char[] getTextCharacters();

    public abstract int getTextLength();

    public abstract int getTextOffset();

    public abstract C64443Bu getTokenLocation();

    public boolean getValueAsBoolean(boolean z) {
        return z;
    }

    public double getValueAsDouble(double d) {
        return d;
    }

    public int getValueAsInt(int i) {
        return i;
    }

    public long getValueAsLong(long j) {
        return j;
    }

    public abstract String getValueAsString(String str);

    public abstract boolean hasCurrentToken();

    public abstract boolean hasTextCharacters();

    public abstract C182811d nextToken();

    public abstract C182811d nextValue();

    public int releaseBuffered(OutputStream outputStream) {
        return -1;
    }

    public int releaseBuffered(Writer writer) {
        return -1;
    }

    public abstract void setCodec(C09980jK r1);

    public abstract C28271eX skipChildren();

    public abstract C11780nw version();

    public C37561vs _constructError(String str) {
        return new C37561vs(str, getCurrentLocation());
    }

    public boolean isEnabled(C10390k1 r4) {
        if (((1 << r4.ordinal()) & this._features) != 0) {
            return true;
        }
        return false;
    }

    public void setSchema(A25 a25) {
        throw new UnsupportedOperationException(AnonymousClass08S.A0T("Parser of type ", getClass().getName(), " does not support schema of type '", a25.getSchemaType(), "'"));
    }

    public byte getByteValue() {
        int intValue = getIntValue();
        if (intValue >= -128 && intValue <= 255) {
            return (byte) intValue;
        }
        throw _constructError(AnonymousClass08S.A0P("Numeric value (", getText(), ") out of range of Java byte"));
    }

    public short getShortValue() {
        int intValue = getIntValue();
        if (intValue >= -32768 && intValue <= 32767) {
            return (short) intValue;
        }
        throw _constructError(AnonymousClass08S.A0P("Numeric value (", getText(), ") out of range of Java short"));
    }

    public boolean isExpectedStartArrayToken() {
        if (getCurrentToken() == C182811d.START_ARRAY) {
            return true;
        }
        return false;
    }

    public int nextIntValue(int i) {
        if (nextToken() == C182811d.VALUE_NUMBER_INT) {
            return getIntValue();
        }
        return i;
    }

    public String nextTextValue() {
        if (nextToken() == C182811d.VALUE_STRING) {
            return getText();
        }
        return null;
    }

    public C10010jO readValueAsTree() {
        C09980jK codec = getCodec();
        if (codec != null) {
            return codec.readTree(this);
        }
        throw new IllegalStateException("No ObjectCodec defined for the parser, can not deserialize JSON into JsonNode tree");
    }

    public Iterator readValuesAs(Class cls) {
        C09980jK codec = getCodec();
        if (codec != null) {
            return codec.readValues(this, cls);
        }
        throw new IllegalStateException("No ObjectCodec defined for the parser, can not deserialize JSON into Java objects");
    }

    public C28271eX() {
    }

    public C28271eX(int i) {
        this._features = i;
    }

    public boolean getValueAsBoolean() {
        return getValueAsBoolean(false);
    }

    public double getValueAsDouble() {
        return getValueAsDouble(0.0d);
    }

    public int getValueAsInt() {
        return getValueAsInt(0);
    }

    public long getValueAsLong() {
        return getValueAsLong(0);
    }

    public String getValueAsString() {
        return getValueAsString(null);
    }

    public Object readValueAs(C28231eT r3) {
        C09980jK codec = getCodec();
        if (codec != null) {
            return codec.readValue(this, r3);
        }
        throw new IllegalStateException("No ObjectCodec defined for the parser, can not deserialize JSON into Java objects");
    }

    public Object readValueAs(Class cls) {
        C09980jK codec = getCodec();
        if (codec != null) {
            return codec.readValue(this, cls);
        }
        throw new IllegalStateException("No ObjectCodec defined for the parser, can not deserialize JSON into Java objects");
    }
}
