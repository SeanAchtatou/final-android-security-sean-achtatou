package X;

/* renamed from: X.1P5  reason: invalid class name */
public final class AnonymousClass1P5 implements AnonymousClass1P6 {
    public final /* synthetic */ AnonymousClass1P4 A00;

    public AnonymousClass1P5(AnonymousClass1P4 r1) {
        this.A00 = r1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public C23191Oo AiV(String str) {
        if ("java_heap_limit".equals(str)) {
            return new C23191Oo(Math.min(this.A00.A03.getMemoryClass(), Integer.MAX_VALUE));
        }
        if ("total_memory".equals(str)) {
            return new C23191Oo(Math.min(this.A00.A00, 2147483647L));
        }
        return null;
    }
}
