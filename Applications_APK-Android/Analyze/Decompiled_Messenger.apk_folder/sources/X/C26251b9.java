package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.common.util.TriState;
import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.base.Preconditions;
import java.util.Iterator;
import java.util.Locale;

@InjectorModule
/* renamed from: X.1b9  reason: invalid class name and case insensitive filesystem */
public final class C26251b9 extends AnonymousClass0UV {
    private static C04470Uu A00;
    private static final Object A01 = new Object();
    private static volatile AnonymousClass1LZ A02;
    private static volatile C123715sD A03;
    private static volatile AnonymousClass1WD A04;

    public static final Boolean A0B() {
        return true;
    }

    public static final Boolean A0C() {
        return true;
    }

    public static final Boolean A0D() {
        return true;
    }

    public static final C16580xM A02(AnonymousClass1XY r3) {
        C16580xM r2 = new C16580xM(r3);
        r2.A04(new AnonymousClass0X5(r3, AnonymousClass0X6.A0V), C16630xR.A00(r3));
        return r2;
    }

    public static final AnonymousClass1LZ A05(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass1LZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass15N(AnonymousClass0WU.A01(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final C30483ExN A07() {
        return new C30483ExN();
    }

    public static final C123715sD A08(AnonymousClass1XY r5) {
        if (A03 == null) {
            synchronized (C123715sD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A03 = new C123715sD(AnonymousClass1YA.A00(applicationInjector), new AnonymousClass27D(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final AnonymousClass1WD A09(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass1WD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass1WD(AnonymousClass0WU.A01(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final AnonymousClass6OO A0A() {
        return null;
    }

    public static final String A0M() {
        return "Orca-Android";
    }

    public static final String A0N() {
        return "com.facebook.orca.auth.StartScreenActivity";
    }

    public static final String A0O() {
        return "Facebook Messenger/Media";
    }

    public static final String A0P(AnonymousClass1XY r4) {
        String str;
        String str2;
        synchronized (A01) {
            C04470Uu A002 = C04470Uu.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r4)) {
                    C04470Uu r2 = A00;
                    C29211g3 r0 = (C29211g3) AnonymousClass065.A00(AnonymousClass1YA.A00((AnonymousClass1XY) A00.A01()), C29211g3.class);
                    if (r0 != null) {
                        str2 = r0.AdG();
                    } else {
                        str2 = ErrorReportingConstants.APP_NAME_KEY;
                    }
                    r2.A00 = str2;
                }
                C04470Uu r1 = A00;
                str = (String) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return str;
    }

    public static final ComponentName A00(AnonymousClass1XY r0) {
        AnonymousClass1YA.A00(r0);
        return null;
    }

    public static final Intent A01(AnonymousClass1XY r1) {
        return C1932393n.A00(AnonymousClass1YA.A00(r1), "com.facebook.messaging.internalprefs.MessengerInternalPreferenceActivity");
    }

    public static final TriState A03(AnonymousClass1XY r1) {
        return AnonymousClass0WA.A00(r1).Ab9(AnonymousClass1Y3.A5G);
    }

    public static final TriState A04(AnonymousClass1XY r1) {
        return AnonymousClass0WA.A00(r1).Ab9(155);
    }

    public static final C60982y9 A06(AnonymousClass1XY r0) {
        return C60972y8.A01(r0);
    }

    public static final Boolean A0E(AnonymousClass1XY r1) {
        if (AnonymousClass0XJ.A04(r1) == TriState.YES) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static final Boolean A0F(AnonymousClass1XY r4) {
        C186714q r1;
        Context A002 = AnonymousClass1YA.A00(r4);
        Iterator it = AnonymousClass147.A01(r4).A01.iterator();
        while (true) {
            if (!it.hasNext()) {
                r1 = null;
                break;
            }
            r1 = (C186714q) it.next();
            if (r1.BDw(A002)) {
                break;
            }
        }
        boolean z = false;
        if (r1 != null) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r1.booleanValue() != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A0G(X.AnonymousClass1XY r1) {
        /*
            java.lang.Boolean r0 = X.C18020zw.A00(r1)
            java.lang.Boolean r1 = X.C13740rz.A02(r1)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0015
            boolean r1 = r1.booleanValue()
            r0 = 1
            if (r1 == 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26251b9.A0G(X.1XY):java.lang.Boolean");
    }

    public static final Boolean A0H(AnonymousClass1XY r5) {
        FbSharedPreferences A002 = FbSharedPreferencesModule.A00(r5);
        TriState A042 = AnonymousClass0XJ.A04(r5);
        AnonymousClass1YI A003 = AnonymousClass0WA.A00(r5);
        boolean z = false;
        if (A042 == TriState.YES && A003.AbO(AnonymousClass1Y3.A60, false) && A002.Aep(AnonymousClass0jG.A08, false)) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    public static final Boolean A0I(AnonymousClass1XY r2) {
        return Boolean.valueOf(FbSharedPreferencesModule.A00(r2).Aep(AnonymousClass3BZ.A03, false));
    }

    public static final Integer A0J() {
        return 2132476939;
    }

    public static final Integer A0K(AnonymousClass1XY r0) {
        AnonymousClass0WA.A00(r0);
        return 2132411600;
    }

    public static final Long A0L() {
        return 14400000L;
    }

    public static final String A0Q(AnonymousClass1XY r1) {
        Context A002 = AnonymousClass1YA.A00(r1);
        Preconditions.checkNotNull(A002);
        if (((AnonymousClass29p) AnonymousClass1XX.A03(AnonymousClass1Y3.AUv, new AnonymousClass2Xx(A002).A00)) != null) {
            return "unknown";
        }
        return null;
    }

    public static final Locale A0R(AnonymousClass1XY r0) {
        return C26261bA.A00(r0).A01();
    }
}
