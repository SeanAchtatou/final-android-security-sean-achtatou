package X;

import java.util.BitSet;

/* renamed from: X.1oS  reason: invalid class name and case insensitive filesystem */
public final class C33981oS extends AnonymousClass1CY {
    public AnonymousClass1GV A00;
    public final BitSet A01 = new BitSet(1);
    public final String[] A02 = {"component"};

    public AnonymousClass1GV A04() {
        AnonymousClass1CY.A00(1, this.A01, this.A02);
        return this.A00;
    }

    public void A05(AnonymousClass11F r3) {
        C17770zR A31;
        AnonymousClass1GV r1 = this.A00;
        if (r3 == null) {
            A31 = null;
        } else {
            A31 = r3.A31();
        }
        r1.A00 = A31;
        this.A01.set(0);
    }

    public void A06(C17770zR r3) {
        C17770zR A16;
        AnonymousClass1GV r1 = this.A00;
        if (r3 == null) {
            A16 = null;
        } else {
            A16 = r3.A16();
        }
        r1.A00 = A16;
        this.A01.set(0);
    }

    public /* bridge */ /* synthetic */ AnonymousClass1CY A02(String str) {
        super.A02(str);
        return this;
    }

    public void A07(String str) {
        super.A02(str);
    }
}
