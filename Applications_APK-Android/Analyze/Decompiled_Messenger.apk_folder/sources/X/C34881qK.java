package X;

/* renamed from: X.1qK  reason: invalid class name and case insensitive filesystem */
public final class C34881qK {
    public AnonymousClass0UN A00;
    private C57712sU A01;

    public static final C34881qK A00(AnonymousClass1XY r1) {
        return new C34881qK(r1);
    }

    public boolean A01(int i) {
        C57712sU r0;
        if (!((C32581lz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Axn, this.A00)).A01()) {
            return true;
        }
        if (this.A01 == null) {
            C163657hd r3 = (C163657hd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AoL, this.A00);
            if (!((C32581lz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Axn, r3.A00)).A01()) {
                r0 = C57712sU.A01;
            } else {
                boolean z = false;
                if (((Boolean) r3.A01.get()).booleanValue()) {
                    z = true;
                }
                if (!z) {
                    r0 = C163657hd.A04;
                } else if (!z) {
                    C010708t.A0K("AccountCapabilityFactory", "Wrong account type when computing account capabilities.");
                    r0 = C163657hd.A02;
                } else {
                    r0 = C163657hd.A03;
                }
            }
            this.A01 = r0;
        }
        return this.A01.A01(i);
    }

    private C34881qK(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
