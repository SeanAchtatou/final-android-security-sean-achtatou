package X;

/* renamed from: X.1In  reason: invalid class name and case insensitive filesystem */
public final class C21741In implements C21681Ih {
    public final AnonymousClass1KE A00;
    private final C21681Ih A01;

    public void AMi(String str, Object obj) {
        this.A01.AMi(str, obj);
    }

    public void AMk(String str, Object obj) {
        this.A01.AMk(str, obj);
    }

    public C17770zR Ahm() {
        return this.A01.Ahm();
    }

    public C637038i Aht() {
        return this.A01.Aht();
    }

    public Object AjM(String str) {
        return this.A01.AjM(str);
    }

    public Object Ajj(String str) {
        return this.A01.Ajj(str);
    }

    public String Asm() {
        return this.A01.Asm();
    }

    public AnonymousClass10N B0i() {
        return this.A01.B0i();
    }

    public int B3W() {
        return this.A01.B3W();
    }

    public C78893ph B96() {
        return this.A01.B96();
    }

    public C37481vk B9A() {
        return this.A01.B9A();
    }

    public int B9K() {
        return this.A01.B9K();
    }

    public boolean BBN() {
        return this.A01.BBN();
    }

    public boolean BFA() {
        return this.A01.BFA();
    }

    public boolean BGx() {
        return this.A01.BGx();
    }

    public boolean C2H() {
        return this.A01.C2H();
    }

    public boolean C2K() {
        return this.A01.C2K();
    }

    public void CDA(int i) {
        this.A01.CDA(i);
    }

    public String getName() {
        return this.A01.getName();
    }

    public C21741In(C21681Ih r1, AnonymousClass1KE r2) {
        this.A01 = r1 == null ? C21621Ib.A01() : r1;
        this.A00 = r2;
    }
}
