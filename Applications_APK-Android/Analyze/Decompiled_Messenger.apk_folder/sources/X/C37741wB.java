package X;

import com.facebook.http.protocol.ApiErrorResult;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/* renamed from: X.1wB  reason: invalid class name and case insensitive filesystem */
public class C37741wB extends IOException implements C37751wC {
    public String batchOperationName;
    public ApiErrorResult result;

    public C37741wB(ApiErrorResult apiErrorResult) {
        super("[code] " + apiErrorResult.A02() + " [message]: " + apiErrorResult.A05() + " [extra]: " + apiErrorResult.A04());
        this.result = apiErrorResult;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeObject(this.result);
    }

    /* renamed from: A00 */
    public ApiErrorResult AmF() {
        return this.result;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this.result = (ApiErrorResult) objectInputStream.readObject();
    }
}
