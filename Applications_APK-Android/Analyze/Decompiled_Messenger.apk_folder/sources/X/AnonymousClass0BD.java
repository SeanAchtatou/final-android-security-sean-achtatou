package X;

import java.util.concurrent.ExecutorService;

/* renamed from: X.0BD  reason: invalid class name */
public final class AnonymousClass0BD {
    public final AnonymousClass0BC A00;
    public final ExecutorService A01;

    public AnonymousClass0BD(ExecutorService executorService, AnonymousClass0BC r2) {
        this.A01 = executorService;
        this.A00 = r2;
    }
}
