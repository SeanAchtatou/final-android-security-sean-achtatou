package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Ap  reason: invalid class name and case insensitive filesystem */
public final class C01530Ap extends AbstractExecutorService implements C01510An {
    public static final String A09 = AnonymousClass08S.A0J(C01530Ap.class.getCanonicalName(), ".ACTION_ALARM.");
    public final Handler A00;
    public final String A01;
    public final PriorityQueue A02 = new PriorityQueue();
    private final int A03;
    private final AlarmManager A04;
    private final PendingIntent A05;
    private final BroadcastReceiver A06;
    private final Context A07;
    private final C009207y A08;

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001f, code lost:
        if (((X.C01970Ci) r7.A02.peek()).A00 > android.os.SystemClock.elapsedRealtime()) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(X.C01530Ap r7) {
        /*
            monitor-enter(r7)
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x004e }
            r6.<init>()     // Catch:{ all -> 0x004e }
        L_0x0006:
            java.util.PriorityQueue r0 = r7.A02     // Catch:{ all -> 0x004e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x004e }
            if (r0 != 0) goto L_0x0021
            java.util.PriorityQueue r0 = r7.A02     // Catch:{ all -> 0x004e }
            java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x004e }
            X.0Ci r0 = (X.C01970Ci) r0     // Catch:{ all -> 0x004e }
            long r4 = r0.A00     // Catch:{ all -> 0x004e }
            long r2 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x004e }
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x0022
        L_0x0021:
            r0 = 1
        L_0x0022:
            if (r0 != 0) goto L_0x0032
            java.util.PriorityQueue r0 = r7.A02     // Catch:{ all -> 0x004e }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x004e }
            X.0Ci r0 = (X.C01970Ci) r0     // Catch:{ all -> 0x004e }
            X.0Cb r0 = r0.A01     // Catch:{ all -> 0x004e }
            r6.add(r0)     // Catch:{ all -> 0x004e }
            goto L_0x0006
        L_0x0032:
            A03(r7)     // Catch:{ all -> 0x004e }
            monitor-exit(r7)     // Catch:{ all -> 0x004e }
            r6.size()
            java.util.Iterator r1 = r6.iterator()
        L_0x003d:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x004d
            java.lang.Object r0 = r1.next()
            X.0Cb r0 = (X.C01900Cb) r0
            r0.run()
            goto L_0x003d
        L_0x004d:
            return
        L_0x004e:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x004e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C01530Ap.A02(X.0Ap):void");
    }

    /* renamed from: C4X */
    public C01940Cf schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        C01900Cb r4 = new C01900Cb(this, runnable, null);
        A01(r4, SystemClock.elapsedRealtime() + timeUnit.toMillis(j));
        return r4;
    }

    public void execute(Runnable runnable) {
        submit(runnable, null);
    }

    public boolean isShutdown() {
        return false;
    }

    public boolean isTerminated() {
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: A00 */
    public C01940Cf submit(Runnable runnable, Object obj) {
        C01900Cb r3 = new C01900Cb(this, runnable, obj);
        A01(r3, SystemClock.elapsedRealtime());
        AnonymousClass00S.A04(this.A00, new AnonymousClass0SX(this), 1230887862);
        return r3;
    }

    public static void A03(C01530Ap r5) {
        if (r5.A02.isEmpty()) {
            r5.A08.A05(r5.A04, r5.A05);
            return;
        }
        long j = ((C01970Ci) r5.A02.peek()).A00;
        SystemClock.elapsedRealtime();
        int i = r5.A03;
        if (i >= 23) {
            r5.A08.A04(r5.A04, 2, j, r5.A05);
        } else if (i >= 19) {
            r5.A08.A02(r5.A04, 2, j, r5.A05);
        } else {
            r5.A04.set(2, j, r5.A05);
        }
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void shutdown() {
        this.A08.A05(this.A04, this.A05);
        try {
            this.A07.unregisterReceiver(this.A06);
        } catch (IllegalArgumentException e) {
            C010708t.A0S("WakingExecutorService", e, "Failed to unregister broadcast receiver");
        }
    }

    public List shutdownNow() {
        throw new UnsupportedOperationException();
    }

    public C01530Ap(C01420Ad r5, String str, Context context, Handler handler, C009207y r9) {
        StringBuilder sb = new StringBuilder(A09);
        sb.append(str);
        String packageName = context.getPackageName();
        if (!TextUtils.isEmpty(packageName)) {
            sb.append('.');
            sb.append(packageName);
        }
        this.A01 = sb.toString();
        this.A07 = context;
        C01540Aq A002 = r5.A00("alarm", AlarmManager.class);
        if (A002.A02()) {
            this.A04 = (AlarmManager) A002.A01();
            this.A03 = Build.VERSION.SDK_INT;
            this.A00 = handler;
            this.A08 = r9;
            Intent intent = new Intent(this.A01);
            intent.setPackage(this.A07.getPackageName());
            this.A05 = PendingIntent.getBroadcast(this.A07, 0, intent, 134217728);
            C01560As r3 = new C01560As(this);
            this.A06 = r3;
            this.A07.registerReceiver(r3, new IntentFilter(this.A01), null, handler);
            return;
        }
        throw new IllegalArgumentException("Cannot acquire Alarm service");
    }

    private void A01(C01900Cb r3, long j) {
        SystemClock.elapsedRealtime();
        synchronized (this) {
            this.A02.add(new C01970Ci(r3, j));
            A03(this);
        }
    }

    public RunnableFuture newTaskFor(Runnable runnable, Object obj) {
        return new AnonymousClass0IM(this, runnable, obj);
    }

    public RunnableFuture newTaskFor(Callable callable) {
        return new AnonymousClass0IM(this, callable);
    }

    public /* bridge */ /* synthetic */ ScheduledFuture schedule(Callable callable, long j, TimeUnit timeUnit) {
        C01900Cb r4 = new C01900Cb(this, callable);
        A01(r4, SystemClock.elapsedRealtime() + timeUnit.toMillis(j));
        return r4;
    }

    public /* bridge */ /* synthetic */ Future submit(Runnable runnable) {
        return submit(runnable, null);
    }

    public /* bridge */ /* synthetic */ Future submit(Callable callable) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        C01900Cb r4 = new C01900Cb(this, callable);
        A01(r4, SystemClock.elapsedRealtime() + timeUnit.toMillis(0));
        AnonymousClass00S.A04(this.A00, new AnonymousClass0SY(this), -1211634230);
        return r4;
    }
}
