package X;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1gq  reason: invalid class name and case insensitive filesystem */
public final class C29701gq extends View.AccessibilityDelegate {
    public final C15220uv A00;

    public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        return this.A00.A0E(view, accessibilityEvent);
    }

    public AccessibilityNodeProvider getAccessibilityNodeProvider(View view) {
        AnonymousClass38I A0G = this.A00.A0G(view);
        if (A0G != null) {
            return (AccessibilityNodeProvider) A0G.A00;
        }
        return null;
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        this.A00.A0H(view, accessibilityEvent);
    }

    public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = new AccessibilityNodeInfoCompat(accessibilityNodeInfo);
        accessibilityNodeInfoCompat.A0X(C15320v6.isScreenReaderFocusable(view));
        accessibilityNodeInfoCompat.A0V(C15320v6.isAccessibilityHeading(view));
        accessibilityNodeInfoCompat.A0M(C15320v6.getAccessibilityPaneTitle(view));
        this.A00.A0I(view, accessibilityNodeInfoCompat);
        accessibilityNodeInfoCompat.A0O(accessibilityNodeInfo.getText(), view);
        List list = (List) view.getTag(2131300900);
        if (list == null) {
            list = Collections.emptyList();
        }
        for (int i = 0; i < list.size(); i++) {
            accessibilityNodeInfoCompat.A0H((C25973Cpq) list.get(i));
        }
    }

    public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        this.A00.A0B(view, accessibilityEvent);
    }

    public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.A00.A0F(viewGroup, view, accessibilityEvent);
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        return this.A00.A0D(view, i, bundle);
    }

    public void sendAccessibilityEvent(View view, int i) {
        this.A00.A0A(view, i);
    }

    public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
        this.A00.A0C(view, accessibilityEvent);
    }

    public C29701gq(C15220uv r1) {
        this.A00 = r1;
    }
}
