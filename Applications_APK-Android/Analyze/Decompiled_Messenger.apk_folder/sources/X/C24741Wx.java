package X;

import android.os.Build;
import android.os.Bundle;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;

/* renamed from: X.1Wx  reason: invalid class name and case insensitive filesystem */
public final class C24741Wx {
    public final C24701Wt A00;
    public final C24751Wy A01;
    public final Executor A02;
    private final AnonymousClass1WN A03;
    private final C24591We A04;

    public static final C53112kD A00(C24741Wx r5, String str, String str2, String str3, Bundle bundle) {
        String str4;
        Set unmodifiableSet;
        Set unmodifiableSet2;
        String A07;
        bundle.putString("scope", str3);
        bundle.putString("sender", str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        AnonymousClass1WN r0 = r5.A03;
        AnonymousClass1WN.A02(r0);
        bundle.putString("gmp_app_id", r0.A02.A00);
        bundle.putString("gmsv", Integer.toString(r5.A00.A04()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString(TurboLoader.Locator.$const$string(66), r5.A00.A05());
        C24701Wt r2 = r5.A00;
        synchronized (r2) {
            if (r2.A00 == null) {
                C24701Wt.A02(r2);
            }
            str4 = r2.A00;
        }
        bundle.putString("app_ver_name", str4);
        bundle.putString("cliv", "fiid-12451000");
        C24591We r4 = r5.A04;
        C24691Ws r02 = r4.A00;
        synchronized (r02.A00) {
            try {
                unmodifiableSet = Collections.unmodifiableSet(r02.A00);
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (unmodifiableSet.isEmpty()) {
            A07 = r4.A01;
        } else {
            String str5 = r4.A01;
            C24691Ws r03 = r4.A00;
            synchronized (r03.A00) {
                try {
                    unmodifiableSet2 = Collections.unmodifiableSet(r03.A00);
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
            A07 = AnonymousClass08S.A07(str5, ' ', C24591We.A00(unmodifiableSet2));
        }
        bundle.putString("Firebase-Client", A07);
        C53122kE r3 = new C53122kE();
        AnonymousClass07A.A04(r5.A02, new C72773ew(r5, bundle, r3), -1816695846);
        return r3.A00;
        throw th;
    }

    public C24741Wx(AnonymousClass1WN r1, C24701Wt r2, Executor executor, C24751Wy r4, C24591We r5) {
        this.A03 = r1;
        this.A00 = r2;
        this.A01 = r4;
        this.A02 = executor;
        this.A04 = r5;
    }
}
