package X;

/* renamed from: X.0Xc  reason: invalid class name and case insensitive filesystem */
public final class C04970Xc {
    public static final C04970Xc A02 = new C04970Xc(true, AnonymousClass07B.A00);
    public static final C04970Xc A03 = new C04970Xc();
    public static final C04970Xc A04 = new C04970Xc(true, AnonymousClass07B.A01);
    public final Integer A00;
    public final boolean A01;

    private C04970Xc() {
        this.A01 = false;
        this.A00 = AnonymousClass07B.A00;
    }

    private C04970Xc(boolean z, Integer num) {
        this.A01 = z;
        this.A00 = num;
    }
}
