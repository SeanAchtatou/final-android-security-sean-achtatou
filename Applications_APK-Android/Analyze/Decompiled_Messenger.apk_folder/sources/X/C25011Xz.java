package X;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableEnumSet;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.RegularImmutableSet;
import com.google.common.collect.Sets$UnmodifiableNavigableSet;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1Xz  reason: invalid class name and case insensitive filesystem */
public final class C25011Xz {
    public static HashSet A05(Object... objArr) {
        HashSet hashSet = new HashSet(AnonymousClass0TG.A00(objArr.length));
        Collections.addAll(hashSet, objArr);
        return hashSet;
    }

    public static boolean A0A(Set set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                return set.size() == set2.size() && set.containsAll(set2);
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    public static ImmutableSet A01(Iterable iterable) {
        EnumSet of;
        if (iterable instanceof ImmutableEnumSet) {
            return (ImmutableEnumSet) iterable;
        }
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (!collection.isEmpty()) {
                of = EnumSet.copyOf(collection);
            }
            return RegularImmutableSet.A05;
        }
        Iterator it = iterable.iterator();
        if (it.hasNext()) {
            of = EnumSet.of((Enum) it.next());
            C24931Xr.A09(of, it);
        }
        return RegularImmutableSet.A05;
        return ImmutableEnumSet.A0C(of);
    }

    public static AnonymousClass0UB A02(Set set, Set set2) {
        Preconditions.checkNotNull(set, "set1");
        Preconditions.checkNotNull(set2, "set2");
        return new AnonymousClass0UA(set, new Predicates.NotPredicate(Predicates.in(set2)), set2);
    }

    public static HashSet A03() {
        return new HashSet();
    }

    public static HashSet A04(Iterable iterable) {
        if (iterable instanceof Collection) {
            return new HashSet((Collection) iterable);
        }
        Iterator it = iterable.iterator();
        HashSet A03 = A03();
        C24931Xr.A09(A03, it);
        return A03;
    }

    public static LinkedHashSet A06(Iterable iterable) {
        if (iterable instanceof Collection) {
            return new LinkedHashSet((Collection) iterable);
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        AnonymousClass0j4.A0D(linkedHashSet, iterable);
        return linkedHashSet;
    }

    public static NavigableSet A07(NavigableSet navigableSet) {
        if ((navigableSet instanceof ImmutableSortedSet) || (navigableSet instanceof Sets$UnmodifiableNavigableSet)) {
            return navigableSet;
        }
        return new Sets$UnmodifiableNavigableSet(navigableSet);
    }

    public static Set A08() {
        return Collections.newSetFromMap(new ConcurrentHashMap());
    }

    public static Set A09(Set set, Predicate predicate) {
        if (set instanceof SortedSet) {
            SortedSet sortedSet = (SortedSet) set;
            if (sortedSet instanceof AnonymousClass1Fo) {
                AnonymousClass1Fo r3 = (AnonymousClass1Fo) sortedSet;
                return new C29784EiW((SortedSet) r3.A01, Predicates.and(r3.A00, predicate));
            }
            Preconditions.checkNotNull(sortedSet);
            Preconditions.checkNotNull(predicate);
            return new C29784EiW(sortedSet, predicate);
        } else if (set instanceof AnonymousClass1Fo) {
            AnonymousClass1Fo r32 = (AnonymousClass1Fo) set;
            return new AnonymousClass1Fo((Set) r32.A01, Predicates.and(r32.A00, predicate));
        } else {
            Preconditions.checkNotNull(set);
            Preconditions.checkNotNull(predicate);
            return new AnonymousClass1Fo(set, predicate);
        }
    }

    public static int A00(Set set) {
        int i;
        int i2 = 0;
        for (Object next : set) {
            if (next != null) {
                i = next.hashCode();
            } else {
                i = 0;
            }
            i2 = ((i2 + i) ^ -1) ^ -1;
        }
        return i2;
    }

    public static boolean A0B(Set set, Collection<Object> collection) {
        boolean z;
        Preconditions.checkNotNull(collection);
        if (collection instanceof AnonymousClass0UG) {
            collection = ((AnonymousClass0UG) collection).AYC();
        }
        if (!(collection instanceof Set) || collection.size() <= set.size()) {
            z = false;
            for (Object remove : collection) {
                z |= set.remove(remove);
            }
        } else {
            Iterator it = set.iterator();
            Preconditions.checkNotNull(collection);
            z = false;
            while (it.hasNext()) {
                if (collection.contains(it.next())) {
                    it.remove();
                    z = true;
                }
            }
        }
        return z;
    }
}
