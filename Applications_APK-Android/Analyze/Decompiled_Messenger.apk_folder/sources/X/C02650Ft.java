package X;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0Ft  reason: invalid class name and case insensitive filesystem */
public final class C02650Ft {
    public final int A00;
    public final AtomicInteger A01 = new AtomicInteger();
    public final AtomicLong A02 = new AtomicLong();

    public C02650Ft(int i) {
        this.A00 = i;
    }
}
