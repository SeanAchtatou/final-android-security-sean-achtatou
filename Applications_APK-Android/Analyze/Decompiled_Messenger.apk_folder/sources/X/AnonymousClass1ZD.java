package X;

import com.facebook.analytics.AnalyticsClientModule;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1ZD  reason: invalid class name */
public final class AnonymousClass1ZD extends AnonymousClass0UV {
    public static final AnonymousClass1ZE A00(AnonymousClass1XY r0) {
        return AnalyticsClientModule.A04(r0);
    }
}
