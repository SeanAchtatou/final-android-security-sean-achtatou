package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.profilo.ipc.TraceConfigData;

/* renamed from: X.0I7  reason: invalid class name */
public final class AnonymousClass0I7 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new TraceConfigData(parcel);
    }

    public Object[] newArray(int i) {
        return new TraceConfigData[i];
    }
}
