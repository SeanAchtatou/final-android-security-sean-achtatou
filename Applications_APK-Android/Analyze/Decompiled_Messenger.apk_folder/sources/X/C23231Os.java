package X;

import android.content.Context;

/* renamed from: X.1Os  reason: invalid class name and case insensitive filesystem */
public final class C23231Os {
    public int A00 = 1;
    public long A01 = 41943040;
    public long A02 = 10485760;
    public long A03 = 2097152;
    public AnonymousClass1OH A04;
    public AnonymousClass1OL A05;
    public C23241Ot A06 = new C23241Ot();
    public AnonymousClass1OI A07;
    public C23111Og A08;
    public String A09 = "image_cache";
    public final Context A0A;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r2.A0A != null) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C23251Ou A00() {
        /*
            r2 = this;
            X.1Og r0 = r2.A08
            if (r0 != 0) goto L_0x0009
            android.content.Context r0 = r2.A0A
            r1 = 0
            if (r0 == 0) goto L_0x000a
        L_0x0009:
            r1 = 1
        L_0x000a:
            java.lang.String r0 = "Either a non-null context or a base directory path or supplier must be provided."
            X.C05520Zg.A07(r1, r0)
            X.1Og r0 = r2.A08
            if (r0 != 0) goto L_0x001e
            android.content.Context r0 = r2.A0A
            if (r0 == 0) goto L_0x001e
            X.5G5 r0 = new X.5G5
            r0.<init>(r2)
            r2.A08 = r0
        L_0x001e:
            X.1Ou r0 = new X.1Ou
            r0.<init>(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C23231Os.A00():X.1Ou");
    }

    public C23231Os(Context context) {
        this.A0A = context;
    }
}
