package X;

import android.os.Handler;
import android.os.Looper;
import android.view.ViewTreeObserver;
import com.facebook.base.app.SplashScreenActivity;

/* renamed from: X.08c  reason: invalid class name */
public final class AnonymousClass08c implements ViewTreeObserver.OnDrawListener {
    private boolean A00;
    public final /* synthetic */ ViewTreeObserver A01;
    public final /* synthetic */ SplashScreenActivity A02;

    public AnonymousClass08c(SplashScreenActivity splashScreenActivity, ViewTreeObserver viewTreeObserver) {
        this.A02 = splashScreenActivity;
        this.A01 = viewTreeObserver;
    }

    public void onDraw() {
        if (!this.A00) {
            this.A02.A03 = true;
            AnonymousClass00S.A04(new Handler(Looper.getMainLooper()), new C001601d(this, this), -1145899079);
        }
        this.A00 = true;
    }
}
