package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0GX  reason: invalid class name */
public final class AnonymousClass0GX implements AnonymousClass1YQ {
    private static volatile AnonymousClass0GX A04;
    public String A00;
    private C06790c5 A01;
    private final C04460Ut A02;
    private final C25051Yd A03;

    public String getSimpleName() {
        return "BackgroundGcInit";
    }

    public static final AnonymousClass0GX A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass0GX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass0GX(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final AnonymousClass0US A01(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.BEX, r1);
    }

    public static boolean A02(AnonymousClass0GX r2) {
        return r2.A03.Aem(281960308278150L);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r2.isPlatformSupported() == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A03(X.AnonymousClass0GX r3) {
        /*
            X.0A0 r2 = X.AnonymousClass08D.A00
            X.0Gz r1 = X.C02920Gz.A00
            r0 = 0
            if (r2 == r1) goto L_0x0008
            r0 = 1
        L_0x0008:
            if (r0 == 0) goto L_0x0011
            boolean r1 = r2.isPlatformSupported()
            r0 = 1
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 != 0) goto L_0x0016
            r0 = 0
            return r0
        L_0x0016:
            java.lang.String r0 = r3.A00
            java.lang.String r1 = "NONE"
            if (r0 != 0) goto L_0x001e
            r3.A00 = r1
        L_0x001e:
            java.lang.String r0 = r3.A00
            boolean r0 = r1.equals(r0)
            r0 = r0 ^ 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GX.A03(X.0GX):boolean");
    }

    private AnonymousClass0GX(AnonymousClass1XY r2) {
        this.A03 = AnonymousClass0WT.A00(r2);
        this.A02 = C04430Uq.A02(r2);
    }

    public void init() {
        C06790c5 r0;
        int A032 = C000700l.A03(-430798091);
        if ((A03(this) || A02(this)) && ((r0 = this.A01) == null || !r0.A02())) {
            AnonymousClass0DB r2 = new AnonymousClass0DB(this);
            C06600bl BMm = this.A02.BMm();
            BMm.A02("com.facebook.common.appstate.AppStateManager.USER_LEFT_APP", r2);
            C06790c5 A002 = BMm.A00();
            this.A01 = A002;
            A002.A00();
        }
        C000700l.A09(-1794337035, A032);
    }
}
