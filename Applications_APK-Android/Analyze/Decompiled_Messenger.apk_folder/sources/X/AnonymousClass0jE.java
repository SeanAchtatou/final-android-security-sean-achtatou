package X;

import com.facebook.common.json.ArrayListDeserializer;
import com.facebook.common.json.ImmutableListDeserializer;
import com.facebook.common.json.ImmutableMapDeserializer;
import com.facebook.common.json.LinkedHashMapDeserializer;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/* renamed from: X.0jE  reason: invalid class name */
public final class AnonymousClass0jE extends AnonymousClass0jJ {
    private static AnonymousClass0jE objectMapper;
    private boolean mHumanReadableFormatEnabled;
    private final C26731bv mJsonLogger;

    static {
        C10360jy r4 = new C10360jy();
        C10290jr r3 = new C10290jr(r4, AnonymousClass0jJ.DEFAULT_ANNOTATION_INTROSPECTOR, AnonymousClass0jJ.STD_VISIBILITY_CHECKER, null, C10300js.instance, null, C10330jv.instance, null, Locale.getDefault(), TimeZone.getTimeZone("GMT"), C10340jw.MIME_NO_LINEFEEDS);
        Class<AnonymousClass0jJ> cls = AnonymousClass0jJ.class;
        try {
            Field declaredField = cls.getDeclaredField("DEFAULT_INTROSPECTOR");
            declaredField.setAccessible(true);
            declaredField.set(null, r4);
            Field declaredField2 = cls.getDeclaredField("DEFAULT_BASE");
            declaredField2.setAccessible(true);
            declaredField2.set(null, r3);
        } catch (IllegalAccessException | NoSuchFieldException unused) {
        }
    }

    public static synchronized AnonymousClass0jE getInstance() {
        AnonymousClass0jE r0;
        synchronized (AnonymousClass0jE.class) {
            if (objectMapper == null) {
                objectMapper = new AnonymousClass0jE(new C10370jz(), new C07590do(), false);
            }
            r0 = objectMapper;
        }
        return r0;
    }

    public C11250mT _serializerProvider(C10450k8 r7) {
        return new C48762b0(this._serializerProvider, r7, this._serializerFactory, this.mJsonLogger, this.mHumanReadableFormatEnabled);
    }

    public JsonDeserializer _findRootDeserializer(C26791c3 r2, C10030jR r3) {
        return findDeserializer(r2, r3);
    }

    public Object _readMapAndClose(C28271eX r2, C10030jR r3) {
        if (r2.getCodec() == null) {
            r2.setCodec(this);
        }
        return super._readMapAndClose(r2, r3);
    }

    public Object _readValue(C10490kF r2, C28271eX r3, C10030jR r4) {
        if (r3.getCodec() == null) {
            r3.setCodec(this);
        }
        return super._readValue(r2, r3, r4);
    }

    private AnonymousClass0jE(C10370jz r7, C26731bv r8) {
        super(r7, null, null);
        C10490kF r4;
        C10450k8 r3;
        C10490kF r1;
        C10450k8 r0;
        this.mJsonLogger = r8;
        C11750nt r12 = new C11750nt();
        if (r12.getModuleName() == null) {
            throw new IllegalArgumentException("Module without defined name");
        } else if (r12.version() != null) {
            r12.setupModule(new C11790nx(this, this));
            AnonymousClass0o9 r5 = AnonymousClass0o9.ALL;
            C10180jg r32 = C10180jg.NONE;
            C10490kF r2 = this._deserializationConfig;
            C10290jr r13 = r2._base;
            C10290jr withVisibility = r13.withVisibility(r5, r32);
            if (r13 == withVisibility) {
                r4 = r2;
            } else {
                r4 = new C10490kF(r2, withVisibility);
            }
            this._deserializationConfig = r4;
            C10450k8 r22 = this._serializationConfig;
            C10290jr r14 = r22._base;
            C10290jr withVisibility2 = r14.withVisibility(r5, r32);
            if (r14 == withVisibility2) {
                r3 = r22;
            } else {
                r3 = new C10450k8(r22, withVisibility2);
            }
            this._serializationConfig = r3;
            AnonymousClass1c1 r02 = AnonymousClass1c1.FAIL_ON_UNKNOWN_PROPERTIES;
            int i = r4._deserFeatures;
            int mask = (r02.getMask() ^ -1) & i;
            if (mask == i) {
                r1 = r4;
            } else {
                r1 = new C10490kF(r4, r4._mapperFeatures, mask);
            }
            this._deserializationConfig = r1;
            AnonymousClass0oC r15 = AnonymousClass0oC.NON_NULL;
            if (r3._serializationInclusion == r15) {
                r0 = r3;
            } else {
                r0 = new C10450k8(r3, r15);
            }
            this._serializationConfig = r0;
        } else {
            throw new IllegalArgumentException("Module without defined version");
        }
    }

    private AnonymousClass0jE(C10370jz r1, C26731bv r2, boolean z) {
        this(r1, r2);
        this.mHumanReadableFormatEnabled = z;
    }

    public JsonDeserializer findDeserializer(C26791c3 r5, C10030jR r6) {
        JsonDeserializer jsonDeserializer;
        Class<String> cls;
        if (!r6.hasGenericTypes()) {
            jsonDeserializer = C28341ee.A00(r6._class);
        } else {
            jsonDeserializer = null;
        }
        if (jsonDeserializer == null) {
            Class<ImmutableMap> cls2 = r6._class;
            if (cls2 == List.class || cls2 == ArrayList.class) {
                jsonDeserializer = new ArrayListDeserializer(r6);
            } else if (cls2 == ImmutableList.class) {
                jsonDeserializer = new ImmutableListDeserializer(r6);
            } else {
                C10030jR containedType = r6.containedType(0);
                boolean z = false;
                if (containedType != null && ((cls = containedType._class) == String.class || Enum.class.isAssignableFrom(cls))) {
                    z = true;
                }
                if (z) {
                    if (cls2 == Map.class || cls2 == HashMap.class || cls2 == LinkedHashMap.class) {
                        jsonDeserializer = new LinkedHashMapDeserializer(r6);
                    } else if (cls2 == ImmutableMap.class) {
                        jsonDeserializer = new ImmutableMapDeserializer(r6);
                    }
                }
                jsonDeserializer = null;
            }
        }
        if (jsonDeserializer == null) {
            jsonDeserializer = super._findRootDeserializer(r5, r6);
            C26731bv r2 = this.mJsonLogger;
            if (r2 != null) {
                r2.BIr(AnonymousClass07B.A01, r6.toString(), jsonDeserializer);
            }
        }
        return jsonDeserializer;
    }

    public JsonDeserializer findDeserializer(C26791c3 r5, Class cls) {
        JsonDeserializer A00 = C28341ee.A00(cls);
        if (A00 == null) {
            A00 = super._findRootDeserializer(r5, this._typeFactory._constructType(cls, null));
            C26731bv r2 = this.mJsonLogger;
            if (r2 != null) {
                r2.BIr(AnonymousClass07B.A01, cls.toString(), A00);
            }
        }
        return A00;
    }

    public JsonDeserializer findDeserializer(C26791c3 r3, Type type) {
        if (type instanceof Class) {
            return findDeserializer(r3, (Class) type);
        }
        return findDeserializer(r3, this._typeFactory._constructType(type, null));
    }
}
