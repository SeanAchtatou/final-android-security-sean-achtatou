package X;

import com.facebook.graphql.enums.GraphQLMessengerInbox2RecentUnitConfigType;
import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.graphservice.factory.GraphQLServiceFactory;
import com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem;
import com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import java.util.Iterator;

/* renamed from: X.19G  reason: invalid class name */
public final class AnonymousClass19G {
    public static final Predicate A0B = new C14440tL();
    public AnonymousClass0UN A00;
    public final AnonymousClass06B A01;
    public final AnonymousClass0u0 A02;
    public final C14450tM A03;
    public final C14570tc A04;
    public final C14520tV A05;
    private final GraphQLServiceFactory A06;
    private final C14500tS A07;
    private final AnonymousClass0tR A08;
    private final AnonymousClass1BS A09;
    private final C25051Yd A0A;

    private static int A00(AnonymousClass101 r4, GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType) {
        int i = -1;
        for (int i2 = 0; i2 < r4.A03.size(); i2++) {
            if (((AnonymousClass12V) r4.A03.get(i2)).A01.A0Q() == graphQLMessengerInboxUnitType) {
                if (i != -1) {
                    return -1;
                }
                i = i2;
            }
        }
        return i;
    }

    public boolean A0A(AnonymousClass101 r7) {
        if (r7 == null) {
            return false;
        }
        int A002 = A00(r7, GraphQLMessengerInboxUnitType.A0I);
        int A003 = A00(r7, GraphQLMessengerInboxUnitType.A02);
        return r7.A02 == C25611a7.TOP ? A002 >= 0 : (A002 == -1 || A003 == -1 || A003 < A002 || r7.A03.size() == 2) ? false : true;
    }

    public static int A01(AnonymousClass19G r13, Iterable iterable, int i) {
        long now = r13.A01.now();
        Iterator it = iterable.iterator();
        int i2 = 0;
        int i3 = 0;
        while (it.hasNext()) {
            ThreadSummary threadSummary = (ThreadSummary) it.next();
            if (now - threadSummary.A0B > ((long) i) * 3600000) {
                break;
            }
            if (r13.A09.A01(threadSummary)) {
                i2 = i3;
            }
            i3++;
        }
        return i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(int, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(int, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setString(int, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, int]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(int, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(int, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setBoolean(int, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI */
    private AnonymousClass12V A02(AnonymousClass12V r6, MessageRequestsSnippet messageRequestsSnippet) {
        GSMBuilderShape0S0000000 gSMBuilderShape0S0000000;
        Class<GSMBuilderShape0S0000000> cls = GSMBuilderShape0S0000000.class;
        GSMBuilderShape0S0000000 gSMBuilderShape0S00000002 = (GSMBuilderShape0S0000000) this.A06.newTreeBuilder("MessengerInboxItem", cls, -123977991);
        GSMBuilderShape0S0000000 gSMBuilderShape0S00000003 = (GSMBuilderShape0S0000000) this.A06.newTreeBuilder("MessengerInbox2ConversationRequestItemData", cls, 1916299833);
        gSMBuilderShape0S00000003.setInt("unread_count", Integer.valueOf(messageRequestsSnippet.A02));
        gSMBuilderShape0S00000003.setString("snippet", messageRequestsSnippet.A03);
        gSMBuilderShape0S00000002.setTree("messenger_inbox_item_attachment", (C16820xp) gSMBuilderShape0S00000003.getResult(C16820xp.class, 1916299833));
        GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) gSMBuilderShape0S00000002.getResult(GSTModelShape1S0000000.class, -123977991);
        if (r6 != null) {
            gSMBuilderShape0S0000000 = C27161ck.A00(r6.A01, this.A06);
        } else {
            gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) this.A06.newTreeBuilder("MessengerInboxUnit", GSMBuilderShape0S0000000.class, 738428414);
            gSMBuilderShape0S0000000.A01("messenger_inbox_unit_type", GraphQLMessengerInboxUnitType.A07);
            gSMBuilderShape0S0000000.A0I("849346485182969");
            gSMBuilderShape0S0000000.setBoolean("messenger_inbox_unit_should_log_item_impressions", (Boolean) true);
        }
        gSMBuilderShape0S0000000.setTreeList("messenger_inbox_unit_items", ImmutableList.of(gSTModelShape1S0000000));
        return new AnonymousClass12V((C27161ck) gSMBuilderShape0S0000000.getResult(C27161ck.class, 738428414), null);
    }

    public static AnonymousClass12V A03(AnonymousClass101 r5) {
        ImmutableList immutableList = r5.A03;
        int size = immutableList.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass12V r2 = (AnonymousClass12V) immutableList.get(i);
            if (r2.A01.A0Q() == GraphQLMessengerInboxUnitType.A0I) {
                return r2;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, int]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(int, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(int, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setBoolean(int, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI */
    public static AnonymousClass12V A04(AnonymousClass19G r3) {
        GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) r3.A06.newTreeBuilder("MessengerInboxUnit", GSMBuilderShape0S0000000.class, 738428414);
        gSMBuilderShape0S0000000.A01("messenger_inbox_unit_type", GraphQLMessengerInboxUnitType.A0I);
        gSMBuilderShape0S0000000.A0I("1674434246165228");
        gSMBuilderShape0S0000000.setBoolean("messenger_inbox_unit_should_log_item_impressions", (Boolean) false);
        return new AnonymousClass12V((C27161ck) gSMBuilderShape0S0000000.getResult(C27161ck.class, 738428414), null);
    }

    public static void A06(AnonymousClass19G r6, C32481lp r7, AnonymousClass12V r8, ImmutableList immutableList, Predicate predicate) {
        Predicate predicate2;
        if (!((Boolean) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BAo, r6.A00)).booleanValue()) {
            predicate2 = Predicates.ObjectPredicate.ALWAYS_FALSE;
        } else {
            predicate2 = A0B;
        }
        int size = immutableList.size();
        for (int i = 0; i < size; i++) {
            ThreadSummary threadSummary = (ThreadSummary) immutableList.get(i);
            if (predicate.apply(Long.valueOf(threadSummary.A0B)) && !predicate2.apply(threadSummary)) {
                r7.A01(r6.A04.A05(r8.A01, threadSummary));
            }
        }
    }

    public static boolean A07(GSTModelShape1S0000000 gSTModelShape1S0000000) {
        if (gSTModelShape1S0000000 != null) {
            GraphQLMessengerInbox2RecentUnitConfigType graphQLMessengerInbox2RecentUnitConfigType = GraphQLMessengerInbox2RecentUnitConfigType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE;
            GraphQLMessengerInbox2RecentUnitConfigType graphQLMessengerInbox2RecentUnitConfigType2 = (GraphQLMessengerInbox2RecentUnitConfigType) gSTModelShape1S0000000.A0O(17530391, graphQLMessengerInbox2RecentUnitConfigType);
            if (graphQLMessengerInbox2RecentUnitConfigType2 == null || graphQLMessengerInbox2RecentUnitConfigType2 == graphQLMessengerInbox2RecentUnitConfigType) {
                return false;
            }
            return true;
        }
        return false;
    }

    public ImmutableList A08(C13890sF r4, MessageRequestsSnippet messageRequestsSnippet, boolean z) {
        ThreadsCollection threadsCollection = r4.A02;
        ImmutableList immutableList = threadsCollection.A00;
        if (!z) {
            messageRequestsSnippet = null;
        }
        return A09(this.A03.A02(immutableList), messageRequestsSnippet, threadsCollection.A01, false);
    }

    public ImmutableList A09(Iterable iterable, MessageRequestsSnippet messageRequestsSnippet, boolean z, boolean z2) {
        C32481lp r4 = new C32481lp();
        AnonymousClass12V A042 = A04(this);
        C27161ck r3 = A042.A01;
        if (messageRequestsSnippet != null && !this.A02.A00.Aem(284833641403416L)) {
            this.A04.A09(r4, A02(null, messageRequestsSnippet), false);
        }
        if (z2) {
            r4.A01(new MessageRequestsHeaderInboxItem(r3, 2131827398));
        }
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            r4.A01(this.A04.A05(A042.A01, (ThreadSummary) it.next()));
        }
        if (!z) {
            r4.A01(new InboxLoadMorePlaceholderItem(A042.A01));
        }
        return r4.A03.build();
    }

    public AnonymousClass19G(AnonymousClass1XY r3, C14470tO r4, String str) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A09 = AnonymousClass1BS.A00(r3);
        this.A01 = C12160of.A00(r3);
        this.A03 = C14450tM.A00(r3);
        this.A08 = AnonymousClass0tR.A00(r3);
        this.A07 = C14500tS.A00(r3);
        this.A0A = AnonymousClass0WT.A00(r3);
        this.A05 = new C14520tV(r3);
        this.A06 = C27011cV.A03(r3);
        this.A02 = AnonymousClass0u0.A00(r3);
        this.A04 = new C14570tc(r4, str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        if (r1 != false) goto L_0x0047;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A05(X.AnonymousClass19G r13, X.C32481lp r14, X.AnonymousClass12V r15, com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r16, X.C34801qC r17, X.C15820w2 r18, X.AnonymousClass101 r19, java.util.Map r20) {
        /*
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = r15.A00
            int r0 = r0.ordinal()
            switch(r0) {
                case 1: goto L_0x02a8;
                case 12: goto L_0x02a2;
                case 26: goto L_0x02ac;
                case 27: goto L_0x02a5;
                case 46: goto L_0x029e;
                default: goto L_0x0009;
            }
        L_0x0009:
            r3 = 25
        L_0x000b:
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BUW
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1qK r0 = (X.C34881qK) r0
            boolean r0 = r0.A01(r3)
            if (r0 == 0) goto L_0x0027
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = r15.A00
            int r0 = r0.ordinal()
            r2 = r18
            switch(r0) {
                case 1: goto L_0x0028;
                case 9: goto L_0x02b0;
                case 11: goto L_0x0150;
                case 12: goto L_0x0269;
                case 13: goto L_0x015d;
                case 17: goto L_0x0150;
                case 18: goto L_0x0133;
                case 19: goto L_0x02b0;
                case 26: goto L_0x0184;
                case 27: goto L_0x02b6;
                case 46: goto L_0x0287;
                case 49: goto L_0x0123;
                case 50: goto L_0x0168;
                default: goto L_0x0027;
            }
        L_0x0027:
            return
        L_0x0028:
            X.0tc r11 = r13.A04
            boolean r0 = r2.A03
            if (r0 != 0) goto L_0x003a
            r11.A07(r14, r15)
            com.facebook.messaging.inbox2.activenow.InboxActiveNowPresenceDisabledUpsellItem r1 = new com.facebook.messaging.inbox2.activenow.InboxActiveNowPresenceDisabledUpsellItem
            X.1ck r0 = r15.A01
            r1.<init>(r0)
            goto L_0x0298
        L_0x003a:
            X.1qC r0 = r2.A00
            if (r0 == 0) goto L_0x0047
            com.google.common.collect.ImmutableList r0 = r0.A00
            boolean r1 = X.C013509w.A02(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0048
        L_0x0047:
            r0 = 1
        L_0x0048:
            if (r0 != 0) goto L_0x0027
            r11.A07(r14, r15)
            X.1ck r0 = r15.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A0R()
            if (r0 == 0) goto L_0x0113
            X.1ck r0 = r15.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r0.A0R()
            r0 = -929496119(0xffffffffc89903c9, float:-313374.28)
            int r0 = r1.getIntValue(r0)
            if (r0 <= 0) goto L_0x0113
            X.1ck r0 = r15.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r0.A0R()
            r0 = -929496119(0xffffffffc89903c9, float:-313374.28)
            int r10 = r1.getIntValue(r0)
        L_0x0071:
            X.1qC r0 = r2.A00
            com.google.common.collect.ImmutableList r12 = r0.A00
            java.util.LinkedList r9 = new java.util.LinkedList
            r9.<init>()
            int r8 = r12.size()
            r7 = 0
            r6 = 0
        L_0x0080:
            if (r7 >= r8) goto L_0x0116
            java.lang.Object r5 = r12.get(r7)
            com.facebook.user.model.User r5 = (com.facebook.user.model.User) r5
            int r0 = r9.size()
            if (r0 >= r10) goto L_0x0116
            X.1Gs r4 = X.C21381Gs.A01
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r11.A00
            r0 = 4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 2306125746910791524(0x2001012600000764, double:1.5853156396669737E-154)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x00f5
            r2 = 3
            int r1 = X.AnonymousClass1Y3.A1J
            X.0UN r0 = r11.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2F9 r1 = (X.AnonymousClass2F9) r1
            java.lang.String r0 = r5.A0j
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r1.A03(r0)
            if (r2 == 0) goto L_0x00f5
            int r1 = X.AnonymousClass1Y3.AuB
            X.0UN r0 = r11.A00
            r13 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r13, r1, r0)
            X.1bq r0 = (X.C26681bq) r0
            com.facebook.messaging.model.threads.ThreadSummary r1 = r0.A08(r2)
            if (r1 == 0) goto L_0x00f5
            r3 = 2
            int r2 = X.AnonymousClass1Y3.BGy
            X.0UN r0 = r11.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.0uY r2 = (X.C15000uY) r2
            com.facebook.user.model.UserKey r0 = r5.A0Q
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r2.A03(r0)
            int r2 = X.AnonymousClass1Y3.AuB
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r13, r2, r0)
            X.1bq r0 = (X.C26681bq) r0
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A08(r3)
            if (r0 == 0) goto L_0x0110
            long r2 = r0.A0B
        L_0x00ed:
            long r0 = r1.A0B
            int r13 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r13 <= 0) goto L_0x00f5
            X.1Gs r4 = X.C21381Gs.A0T
        L_0x00f5:
            X.1ck r3 = r15.A01
            X.0ty r0 = r11.A03
            X.1H4 r2 = r0.A0I(r5, r4)
            com.facebook.messaging.inbox2.horizontaltiles.HorizontalTileInboxItem r1 = new com.facebook.messaging.inbox2.horizontaltiles.HorizontalTileInboxItem
            r0 = 0
            r1.<init>(r3, r0, r5, r2)
            int r0 = r6 + 1
            r1.A0C(r6)
            r9.add(r1)
            int r7 = r7 + 1
            r6 = r0
            goto L_0x0080
        L_0x0110:
            r2 = 0
            goto L_0x00ed
        L_0x0113:
            r10 = 5
            goto L_0x0071
        L_0x0116:
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.copyOf(r9)
            com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem r1 = new com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem
            X.1ck r0 = r15.A01
            r1.<init>(r0, r2)
            goto L_0x0298
        L_0x0123:
            java.lang.Object r1 = r15.A02
            if (r1 == 0) goto L_0x0027
            r0 = r1
            com.facebook.messaging.discovery.model.DiscoverTabGameNuxFooterItem r0 = (com.facebook.messaging.discovery.model.DiscoverTabGameNuxFooterItem) r0
            java.lang.String r0 = r0.A00
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 == 0) goto L_0x0298
            return
        L_0x0133:
            java.lang.Object r2 = r15.A02
            if (r2 == 0) goto L_0x0027
            r1 = r2
            com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem r1 = (com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem) r1
            java.lang.String r0 = r1.A01
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 != 0) goto L_0x0027
            java.lang.String r0 = r1.A00
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 != 0) goto L_0x0027
            com.facebook.messaging.inbox2.items.InboxUnitItem r2 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r2
            r14.A01(r2)
            return
        L_0x0150:
            X.0tc r1 = r13.A04
            java.lang.Object r0 = r15.A02
            java.util.Collection r0 = (java.util.Collection) r0
            boolean r0 = X.C013509w.A02(r0)
            if (r0 == 0) goto L_0x0163
            return
        L_0x015d:
            X.0tc r1 = r13.A04
            java.lang.Object r0 = r15.A02
            if (r0 == 0) goto L_0x0027
        L_0x0163:
            r1.A07(r14, r15)
            goto L_0x0261
        L_0x0168:
            r1 = r20
            if (r20 == 0) goto L_0x0027
            X.0tc r2 = r13.A04
            java.lang.String r0 = "is_connected_param"
            java.lang.Object r1 = r1.get(r0)
            boolean r0 = r1 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x0027
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r0 = r1.booleanValue()
            if (r0 != 0) goto L_0x0027
            r2.A06(r14, r15)
            return
        L_0x0184:
            X.0tR r0 = r13.A08
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x01da
            java.lang.Object r5 = r15.A02
            if (r5 == 0) goto L_0x0027
            X.0tS r0 = r13.A07
            java.util.List r5 = (java.util.List) r5
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r0.A00
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1ZE r1 = (X.AnonymousClass1ZE) r1
            java.lang.String r0 = "inbox_ad_unit_hidden_for_zero_rating"
            X.0bW r1 = r1.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r4 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 286(0x11e, float:4.01E-43)
            r4.<init>(r1, r0)
            boolean r0 = r4.A0G()
            if (r0 == 0) goto L_0x0027
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            if (r5 == 0) goto L_0x02be
            java.util.Iterator r2 = r5.iterator()
        L_0x01bb:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x02be
            java.lang.Object r1 = r2.next()
            com.facebook.messaging.business.inboxads.common.InboxAdsItem r1 = (com.facebook.messaging.business.inboxads.common.InboxAdsItem) r1
            int r0 = r3.length()
            if (r0 <= 0) goto L_0x01d2
            java.lang.String r0 = ","
            r3.append(r0)
        L_0x01d2:
            com.facebook.messaging.business.inboxads.common.InboxAdsData r0 = r1.A00
            java.lang.String r0 = r0.A0F
            r3.append(r0)
            goto L_0x01bb
        L_0x01da:
            int r4 = r14.A00
            X.1Yd r3 = r13.A0A
            r1 = 563903436751462(0x200de00090266, double:2.786053156707096E-309)
            r0 = 5
            int r0 = r3.AqL(r1, r0)
            if (r4 >= r0) goto L_0x022d
            X.0tS r0 = r13.A07
            int r3 = r14.A00
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r0.A00
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1ZE r1 = (X.AnonymousClass1ZE) r1
            java.lang.String r0 = "inbox_ad_error"
            X.0bW r1 = r1.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 266(0x10a, float:3.73E-43)
            r2.<init>(r1, r0)
            boolean r0 = r2.A0G()
            if (r0 == 0) goto L_0x0027
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            int r0 = X.C111495Ss.A00(r0)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "error_type"
            r2.A0B(r0, r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r3)
            java.lang.String r0 = "p_rows"
            r2.A0B(r0, r1)
            java.lang.String r0 = "messenger_inbox_ads"
            r2.A0O(r0)
            r2.A06()
            return
        L_0x022d:
            X.0tc r3 = r13.A04
            r4 = r19
            if (r19 == 0) goto L_0x025a
            com.google.common.collect.ImmutableList r0 = r4.A03
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x025a
            com.google.common.collect.ImmutableList r0 = r4.A03
            r2 = 0
            java.lang.Object r0 = r0.get(r2)
            if (r0 == 0) goto L_0x025a
            com.google.common.collect.ImmutableList r0 = r4.A03
            java.lang.Object r0 = r0.get(r2)
            X.12V r0 = (X.AnonymousClass12V) r0
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r1 = r0.A00
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A0F
            if (r1 != r0) goto L_0x025a
            com.google.common.collect.ImmutableList r0 = r4.A03
            java.lang.Object r15 = r0.get(r2)
            X.12V r15 = (X.AnonymousClass12V) r15
        L_0x025a:
            java.lang.Object r0 = r15.A02
            if (r0 == 0) goto L_0x0027
            r3.A07(r14, r15)
        L_0x0261:
            java.lang.Object r0 = r15.A02
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            r14.A02(r0)
            return
        L_0x0269:
            r3 = r16
            if (r16 == 0) goto L_0x0027
            X.0u0 r0 = r13.A02
            X.1Yd r2 = r0.A00
            r0 = 284833641403416(0x1030e00041418, double:1.407265169972927E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 != 0) goto L_0x0027
            X.0tc r2 = r13.A04
            X.12V r1 = r13.A02(r15, r3)
            r0 = 1
            r2.A09(r14, r1, r0)
            return
        L_0x0287:
            X.0tc r0 = r13.A04
            java.lang.Boolean r0 = r0.A06
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0027
            com.facebook.workchat.inbox.invites.bridge.WorkChatInviteCoworkersUnitInboxItem r1 = new com.facebook.workchat.inbox.invites.bridge.WorkChatInviteCoworkersUnitInboxItem
            X.1ck r0 = r15.A01
            r1.<init>(r0)
        L_0x0298:
            com.facebook.messaging.inbox2.items.InboxUnitItem r1 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r1
            r14.A01(r1)
            return
        L_0x029e:
            r3 = 24
            goto L_0x000b
        L_0x02a2:
            r3 = 1
            goto L_0x000b
        L_0x02a5:
            r3 = 6
            goto L_0x000b
        L_0x02a8:
            r3 = 23
            goto L_0x000b
        L_0x02ac:
            r3 = 19
            goto L_0x000b
        L_0x02b0:
            X.0tc r0 = r13.A04
            r0.A06(r14, r15)
            return
        L_0x02b6:
            X.0tc r0 = r13.A04
            r1 = r17
            r0.A08(r14, r15, r1, r2)
            return
        L_0x02be:
            java.lang.String r1 = r3.toString()
            java.lang.String r0 = "client_tokens"
            r4.A0D(r0, r1)
            java.lang.String r0 = "messenger_inbox_ads"
            r4.A0O(r0)
            r4.A06()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19G.A05(X.19G, X.1lp, X.12V, com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet, X.1qC, X.0w2, X.101, java.util.Map):void");
    }
}
