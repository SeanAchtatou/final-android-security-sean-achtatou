package X;

import android.content.Context;
import android.util.Log;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;
import com.facebook.profilo.mmapbuf.MmapBufferManager;
import com.facebook.profilo.writer.NativeTraceWriterCallbacks;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.05k  reason: invalid class name and case insensitive filesystem */
public final class C004505k implements C003804w, C004605l, NativeTraceWriterCallbacks, C003904y {
    public static AtomicReference A0F = new AtomicReference(null);
    public AnonymousClass05d A00;
    public AnonymousClass04t A01;
    public AnonymousClass04t A02;
    public AnonymousClass05m A03;
    public MmapBufferManager A04;
    public AnonymousClass06L A05;
    public AnonymousClass0Qc A06;
    public C000900o[] A07;
    public C000900o[] A08;
    public final C004705n A09;
    public final String A0A;
    public final boolean A0B;
    private final Object A0C = new Object();
    private final HashMap A0D;
    private final Random A0E;

    private synchronized AnonymousClass0Qc A01(boolean z) {
        AnonymousClass06L r0;
        AnonymousClass0Qc A022;
        AnonymousClass05d r02;
        if (!(this.A06 != null || (r0 = this.A05) == null || (A022 = r0.A02()) == null)) {
            synchronized (this) {
                if (this.A06 != A022) {
                    this.A06 = A022;
                    if (!(A022 == null || (r02 = this.A00) == null)) {
                        A022.A04(r02.B5B());
                    }
                    if (z) {
                        A08();
                    }
                }
            }
        }
        return this.A06;
    }

    private void A02() {
        synchronized (this) {
            if (this.A02 != null) {
                AnonymousClass054 r0 = AnonymousClass054.A07;
                if (r0 != null) {
                    int i = r0.A02.get();
                    boolean z = false;
                    if (i != 0) {
                        z = true;
                    }
                    if (z) {
                    }
                }
                AnonymousClass04t r1 = this.A02;
                this.A02 = null;
                A03(r1);
            }
        }
    }

    private void A03(AnonymousClass04t r2) {
        synchronized (this) {
            this.A01 = r2;
            A04(this, r2.Anp());
        }
        this.A09.BTJ();
    }

    private void A06(File file, File file2, File file3, int i, long j) {
        AnonymousClass05o r5;
        synchronized (this) {
            boolean z = true;
            if ((i & 1) != 0) {
                z = false;
            }
            AnonymousClass05m r4 = this.A03;
            String name = file2.getName();
            int lastIndexOf = name.lastIndexOf(46);
            if (lastIndexOf != -1) {
                name = name.substring(0, lastIndexOf);
            }
            String A0J = AnonymousClass08S.A0J(name, ".log");
            if (!z) {
                A0J = AnonymousClass08S.A0J("override-", A0J);
            }
            File file4 = r4.A06;
            if (file4.isDirectory() || file4.mkdirs()) {
                if (file2.renameTo(new File(file4, A0J))) {
                    r4.A02.A00++;
                } else {
                    r4.A02.A03++;
                }
                if (!r4.A07) {
                    ArrayList<File> arrayList = new ArrayList<>();
                    File file5 = new File(AnonymousClass05m.A00(r4), "upload");
                    arrayList.addAll(AnonymousClass05m.A01(file5, AnonymousClass05m.A0A));
                    arrayList.addAll(AnonymousClass05m.A01(file5, AnonymousClass05m.A09));
                    if (!arrayList.isEmpty()) {
                        File file6 = r4.A06;
                        for (File file7 : arrayList) {
                            file7.renameTo(new File(file6, file7.getName()));
                        }
                        file5.delete();
                        r4.A07 = true;
                    }
                }
                AnonymousClass05m.A03(r4, file4, r4.A03, r4.A01);
                AnonymousClass05m.A02(r4, r4.A03, r4.A00);
            } else {
                r4.A02.A01++;
            }
            A08();
            AnonymousClass05m r1 = this.A03;
            r5 = r1.A02;
            r1.A02 = new AnonymousClass05o();
        }
        A05(file3);
        this.A09.Bsa(file, j);
        this.A09.Bsb(r5.A02 + r5.A03 + r5.A01 + r5.A04, r5.A06, r5.A05, r5.A00);
    }

    public void A08() {
        AnonymousClass0Qc A012 = A01(false);
        if (A012 != null) {
            AnonymousClass05m r5 = this.A03;
            File file = r5.A06;
            AnonymousClass05m.A03(r5, file, r5.A03, r5.A01);
            List A013 = AnonymousClass05m.A01(file, AnonymousClass05m.A09);
            Collections.sort(A013, new C004805p());
            AnonymousClass0Qc.A02(A012, A013, this, false);
            List A014 = AnonymousClass05m.A01(this.A03.A06, AnonymousClass05m.A0A);
            Collections.sort(A014, new C004905q());
            AnonymousClass0Qc.A02(A012, A014, this, true);
        }
    }

    public void A09(AnonymousClass04t r3) {
        boolean equals;
        synchronized (this) {
            try {
                equals = r3.equals(this.A01);
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (!equals) {
            this.A09.BgU();
            synchronized (this) {
                try {
                    AnonymousClass054 r0 = AnonymousClass054.A07;
                    if (r0 != null) {
                        int i = r0.A02.get();
                        boolean z = false;
                        if (i != 0) {
                            z = true;
                        }
                        if (z) {
                            this.A02 = r3;
                        }
                    }
                    A03(r3);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public void Bth(File file) {
        synchronized (this) {
            AnonymousClass05m r3 = this.A03;
            if (AnonymousClass05m.A04(r3, file, new File(r3.A03, file.getName()))) {
                AnonymousClass05m.A02(r3, r3.A03, r3.A00);
            }
        }
        this.A09.Bth(file);
    }

    public void onTraceStop(TraceContext traceContext) {
        C000900o[] r2;
        C000900o[] r3;
        AnonymousClass05d r0;
        int tracingProviders;
        int tracingProviders2;
        synchronized (this) {
            try {
                r2 = this.A07;
                r3 = this.A08;
                r0 = this.A00;
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (r0 != null) {
            Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126470, 0, r0.Ai2());
        }
        int i = 0;
        for (C000900o r1 : r2) {
            if (r1.A01 == null || r1.A02) {
                tracingProviders2 = r1.getTracingProviders();
            } else {
                tracingProviders2 = 0;
            }
            i |= tracingProviders2;
        }
        for (C000900o r12 : r3) {
            if (r12.A01 == null || r12.A02) {
                tracingProviders = r12.getTracingProviders();
            } else {
                tracingProviders = 0;
            }
            i |= tracingProviders;
        }
        TraceEvents.disableProviders(traceContext.A02);
        synchronized (this.A0C) {
            int i2 = 0;
            while (i2 < r8) {
                try {
                    r3[i2].A05(traceContext, this);
                    i2++;
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
        }
        for (C000900o A052 : r2) {
            A052.A05(traceContext, this);
        }
        this.A09.BkP(i);
        A02();
        this.A09.onTraceStop(traceContext);
        return;
        throw th;
    }

    public static C004505k A00() {
        C004505k r0 = (C004505k) A0F.get();
        if (r0 != null) {
            return r0;
        }
        throw new IllegalStateException("TraceOrchestrator has not been initialized");
    }

    public static void A04(C004505k r3, AnonymousClass05d r4) {
        if (!r4.equals(r3.A00)) {
            r3.A00 = r4;
            AnonymousClass054 r2 = AnonymousClass054.A07;
            if (r2 != null) {
                r2.A03.compareAndSet((AnonymousClass05d) r2.A03.get(), r4);
                AnonymousClass0Qc A012 = r3.A01(true);
                if (A012 != null) {
                    A012.A04(r4.B5B());
                    return;
                }
                return;
            }
            throw new IllegalStateException("Performing config change before TraceControl has been initialized");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x006f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A07(java.io.File r9, java.lang.String r10, java.util.zip.ZipOutputStream r11) {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r9, r10)
            java.io.File r8 = r0.getAbsoluteFile()
            java.net.URI r7 = r9.toURI()
            java.lang.String[] r6 = r8.list()
            int r5 = r6.length
            r4 = 0
        L_0x0013:
            if (r4 >= r5) goto L_0x0075
            r0 = r6[r4]
            java.io.File r2 = new java.io.File
            r2.<init>(r8, r0)
            boolean r0 = r2.exists()
            if (r0 == 0) goto L_0x0066
            java.net.URI r0 = r2.toURI()
            java.net.URI r0 = r7.relativize(r0)
            java.lang.String r1 = r0.getPath()
            boolean r0 = r2.isFile()
            if (r0 == 0) goto L_0x0059
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ all -> 0x0070 }
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0070 }
            r0.<init>(r9, r1)     // Catch:{ all -> 0x0070 }
            r3.<init>(r0)     // Catch:{ all -> 0x0070 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r0]     // Catch:{ all -> 0x0069 }
            java.util.zip.ZipEntry r0 = new java.util.zip.ZipEntry     // Catch:{ all -> 0x0069 }
            r0.<init>(r1)     // Catch:{ all -> 0x0069 }
            r11.putNextEntry(r0)     // Catch:{ all -> 0x0069 }
        L_0x004a:
            int r1 = r3.read(r2)     // Catch:{ all -> 0x0069 }
            if (r1 <= 0) goto L_0x0055
            r0 = 0
            r11.write(r2, r0, r1)     // Catch:{ all -> 0x0069 }
            goto L_0x004a
        L_0x0055:
            r3.close()     // Catch:{ all -> 0x0070 }
            goto L_0x0063
        L_0x0059:
            boolean r0 = r2.isDirectory()
            if (r0 == 0) goto L_0x0066
            A07(r9, r1, r11)
            goto L_0x0066
        L_0x0063:
            r11.closeEntry()
        L_0x0066:
            int r4 = r4 + 1
            goto L_0x0013
        L_0x0069:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x006b }
        L_0x006b:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x006f }
        L_0x006f:
            throw r0     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r0 = move-exception
            r11.closeEntry()
            throw r0
        L_0x0075:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C004505k.A07(java.io.File, java.lang.String, java.util.zip.ZipOutputStream):void");
    }

    public void Bds(Throwable th) {
        this.A09.Bds(th);
    }

    public void Bsc(TraceContext traceContext) {
        C000900o[] r3;
        this.A09.onTraceStart(traceContext);
        synchronized (this) {
            r3 = this.A07;
        }
        for (C000900o A062 : r3) {
            A062.A06(traceContext, this);
        }
        this.A09.BkO();
    }

    public void Bsd(TraceContext traceContext) {
        C000900o[] r4;
        ArrayList<String> arrayList;
        synchronized (TraceEvents.class) {
            if (TraceEvents.sInitialized) {
                AnonymousClass08Y r3 = ProvidersRegistry.A00;
                int A002 = r3.A00(r3.A01);
                if (A002 != TraceEvents.sLastNameRefreshProvidersState) {
                    TraceEvents.sLastNameRefreshProvidersState = A002;
                    synchronized (r3.A01) {
                        arrayList = new ArrayList<>(r3.A01);
                    }
                    int size = arrayList.size();
                    int[] iArr = new int[size];
                    String[] strArr = new String[size];
                    int i = 0;
                    for (String str : arrayList) {
                        strArr[i] = str;
                        iArr[i] = ProvidersRegistry.getBitMaskFor(str);
                        i++;
                    }
                    TraceEvents.nativeRefreshProviderNames(iArr, strArr);
                }
            } else {
                throw new IllegalStateException("Native library is not initialized.");
            }
        }
        int i2 = traceContext.A02;
        synchronized (TraceEvents.class) {
            TraceEvents.sProviders = TraceEvents.nativeEnableProviders(i2);
        }
        synchronized (this) {
            try {
                r4 = this.A08;
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        synchronized (this.A0C) {
            try {
                for (C000900o A062 : r4) {
                    A062.A06(traceContext, this);
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public void Btg(File file) {
        this.A09.Btg(file);
    }

    public void onTraceWriteAbort(long j, int i) {
        long j2;
        C005005r r4;
        AnonymousClass05d r1;
        int B6I;
        synchronized (this.A0D) {
            try {
                HashMap hashMap = this.A0D;
                j2 = j;
                Long valueOf = Long.valueOf(j);
                r4 = (C005005r) hashMap.get(valueOf);
                if (r4 != null) {
                    this.A0D.remove(valueOf);
                } else {
                    throw new IllegalStateException("onTraceWriteAbort can't be called without onTraceWriteStart");
                }
            } catch (Throwable th) {
                th = th;
                throw th;
            }
        }
        this.A09.onTraceWriteAbort(j, i);
        Log.w("Profilo/TraceOrchestrator", AnonymousClass08S.A0J("Trace is aborted with code: ", AnonymousClass0PI.A00(i)));
        AnonymousClass054 r0 = AnonymousClass054.A07;
        if (r0 != null) {
            r0.A09(j, i);
            if (this.A0B) {
                File file = r4.A01;
                if (file.exists()) {
                    File parentFile = file.getParentFile();
                    synchronized (this) {
                        try {
                            r1 = this.A00;
                        } catch (Throwable th2) {
                            while (true) {
                                th = th2;
                                break;
                            }
                        }
                    }
                    boolean z = false;
                    if (r1 != null && i == 4 && (B6I = r1.AiX().B6I()) != 0 && this.A0E.nextInt(B6I) == 0) {
                        z = true;
                    }
                    if (!z && !file.delete()) {
                        Log.e("Profilo/TraceOrchestrator", "Could not delete aborted trace");
                    }
                    if (!z) {
                        A05(parentFile);
                    } else {
                        A06(file, file, parentFile, r4.A00, j2);
                    }
                }
            }
        } else {
            throw new IllegalStateException("No TraceControl when cleaning up aborted trace");
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:43|44|45|46|47) */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ba, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:46:0x00be */
    /* JADX WARNING: Missing exception handler attribute for start block: B:54:0x00c5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onTraceWriteEnd(long r15, int r17) {
        /*
            r14 = this;
            java.util.HashMap r2 = r14.A0D
            monitor-enter(r2)
            java.util.HashMap r0 = r14.A0D     // Catch:{ all -> 0x010c }
            r12 = r15
            java.lang.Long r1 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x010c }
            java.lang.Object r4 = r0.get(r1)     // Catch:{ all -> 0x010c }
            X.05r r4 = (X.C005005r) r4     // Catch:{ all -> 0x010c }
            if (r4 == 0) goto L_0x0104
            java.util.HashMap r0 = r14.A0D     // Catch:{ all -> 0x010c }
            r0.remove(r1)     // Catch:{ all -> 0x010c }
            monitor-exit(r2)     // Catch:{ all -> 0x010c }
            X.05n r0 = r14.A09
            r2 = r17
            r0.onTraceWriteEnd(r12, r2)
            java.io.File r8 = r4.A01
            boolean r0 = r8.exists()
            if (r0 == 0) goto L_0x0103
            java.lang.String r7 = r8.getName()
            r0 = 46
            int r6 = r7.lastIndexOf(r0)
            java.lang.String r1 = "-cs-"
            java.lang.String r0 = java.lang.Integer.toHexString(r2)
            java.lang.String r5 = X.AnonymousClass08S.A0J(r1, r0)
            java.io.File r3 = new java.io.File
            java.lang.String r2 = r8.getParent()
            r1 = r7
            if (r6 <= 0) goto L_0x0049
            r0 = 0
            java.lang.String r1 = r7.substring(r0, r6)
        L_0x0049:
            if (r6 <= 0) goto L_0x0092
            java.lang.String r0 = r7.substring(r6)
        L_0x004f:
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r5, r0)
            r3.<init>(r2, r0)
            boolean r0 = r8.renameTo(r3)
            if (r0 == 0) goto L_0x005d
            r8 = r3
        L_0x005d:
            boolean r0 = r14.A0B
            if (r0 == 0) goto L_0x0103
            java.io.File r10 = r8.getParentFile()
            boolean r0 = r10.isDirectory()
            r1 = 1
            if (r0 == 0) goto L_0x0090
            java.lang.String[] r0 = r10.list()
            int r0 = r0.length
            if (r0 <= r1) goto L_0x0090
        L_0x0073:
            if (r1 == 0) goto L_0x00ca
            java.lang.String r1 = ".zip.tmp"
            boolean r0 = r10.isDirectory()
            r9 = 0
            if (r0 == 0) goto L_0x00cd
            java.lang.String r0 = r10.getName()
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r1)
            java.io.File r3 = new java.io.File
            java.lang.String r0 = r10.getParent()
            r3.<init>(r0, r1)
            goto L_0x0095
        L_0x0090:
            r1 = 0
            goto L_0x0073
        L_0x0092:
            java.lang.String r0 = ""
            goto L_0x004f
        L_0x0095:
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00c6 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00c6 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x00c6 }
            r0 = 262144(0x40000, float:3.67342E-40)
            r2.<init>(r1, r0)     // Catch:{ IOException -> 0x00c6 }
            java.util.zip.ZipOutputStream r1 = new java.util.zip.ZipOutputStream     // Catch:{ all -> 0x00bf }
            r1.<init>(r2)     // Catch:{ all -> 0x00bf }
            java.lang.String r0 = "."
            A07(r10, r0, r1)     // Catch:{ all -> 0x00b8 }
            r1.flush()     // Catch:{ all -> 0x00b8 }
            r1.finish()     // Catch:{ all -> 0x00b8 }
            r1.close()     // Catch:{ all -> 0x00bf }
            r2.close()     // Catch:{ IOException -> 0x00c6 }
            goto L_0x00cc
        L_0x00b8:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ba }
        L_0x00ba:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x00be }
        L_0x00be:
            throw r0     // Catch:{ all -> 0x00bf }
        L_0x00bf:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00c1 }
        L_0x00c1:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x00c5 }
        L_0x00c5:
            throw r0     // Catch:{ IOException -> 0x00c6 }
        L_0x00c6:
            r3.delete()
            goto L_0x00cd
        L_0x00ca:
            r9 = r8
            goto L_0x00fc
        L_0x00cc:
            r9 = r3
        L_0x00cd:
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r0 = "yyyy-MM-dd'T'HH-mm-ss"
            r2.<init>(r0, r1)
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            java.lang.String r5 = r2.format(r0)
            java.io.File r3 = new java.io.File
            java.io.File r2 = r9.getParentFile()
            java.lang.String r1 = "-"
            java.lang.String r0 = r9.getName()
            java.lang.String r0 = X.AnonymousClass08S.A0P(r5, r1, r0)
            r3.<init>(r2, r0)
            boolean r0 = r9.renameTo(r3)
            if (r0 == 0) goto L_0x00f9
            r9 = r3
        L_0x00f9:
            A05(r10)
        L_0x00fc:
            int r11 = r4.A00
            r7 = r14
            r7.A06(r8, r9, r10, r11, r12)
            return
        L_0x0103:
            return
        L_0x0104:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x010c }
            java.lang.String r0 = "onTraceWriteEnd can't be called without onTraceWriteStart"
            r1.<init>(r0)     // Catch:{ all -> 0x010c }
            throw r1     // Catch:{ all -> 0x010c }
        L_0x010c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x010c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C004505k.onTraceWriteEnd(long, int):void");
    }

    public void onTraceWriteStart(long j, int i, String str) {
        Long valueOf;
        C005005r r0;
        synchronized (this.A0D) {
            try {
                HashMap hashMap = this.A0D;
                valueOf = Long.valueOf(j);
                r0 = (C005005r) hashMap.get(valueOf);
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (r0 == null) {
            this.A09.onTraceWriteStart(j, i, str);
            synchronized (this.A0D) {
                try {
                    this.A0D.put(valueOf, new C005005r(i, new File(str)));
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
            return;
        }
        throw new IllegalStateException("Trace already registered on start");
    }

    public C004505k(Context context, AnonymousClass04t r8, C000900o[] r9, String str, boolean z, File file) {
        this.A01 = r8;
        this.A00 = null;
        this.A03 = new AnonymousClass05m(context, file);
        this.A06 = null;
        this.A0E = new Random();
        this.A09 = new C004705n();
        this.A0A = str;
        this.A0B = z;
        this.A0D = new HashMap(2);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (C000900o r1 : r9) {
            if (r1.A08()) {
                arrayList.add(r1);
            } else {
                arrayList2.add(r1);
            }
        }
        this.A07 = (C000900o[]) arrayList2.toArray(new C000900o[arrayList2.size()]);
        this.A08 = (C000900o[]) arrayList.toArray(new C000900o[arrayList.size()]);
    }

    public static void A05(File file) {
        if (file.isDirectory()) {
            try {
                String[] list = file.list();
                if (list != null) {
                    for (String file2 : list) {
                        File file3 = new File(file, file2);
                        if (file3.isDirectory()) {
                            A05(file3);
                        } else {
                            file3.delete();
                        }
                    }
                }
                file.delete();
            } catch (Exception e) {
                Log.e("ZipHelper", "failed to delete directory", e);
            }
        }
    }

    public void onTraceAbort(TraceContext traceContext) {
        C000900o[] r6;
        C000900o[] r5;
        int i;
        A02();
        synchronized (this) {
            try {
                r6 = this.A07;
                r5 = this.A08;
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        this.A09.onTraceAbort(traceContext);
        TraceEvents.disableProviders(traceContext.A02);
        synchronized (this.A0C) {
            try {
                for (C000900o A052 : r5) {
                    A052.A05(traceContext, this);
                }
            } catch (Throwable th2) {
                while (true) {
                    th = th2;
                    break;
                }
            }
        }
        for (C000900o A053 : r6) {
            A053.A05(traceContext, this);
        }
        return;
        throw th;
    }
}
