package X;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.io.IOException;

/* renamed from: X.12W  reason: invalid class name */
public final class AnonymousClass12W {
    private AnonymousClass0UN A00;
    private final AnonymousClass0jE A01 = C05040Xk.A00();

    public static final AnonymousClass12W A00(AnonymousClass1XY r1) {
        return new AnonymousClass12W(r1);
    }

    public AnonymousClass12W(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public ImmutableList A01(String str) {
        if (C06850cB.A0B(str)) {
            return RegularImmutableList.A02;
        }
        try {
            ImmutableList immutableList = (ImmutableList) this.A01.readValue(str, ImmutableList.class);
            if (immutableList == null) {
                return RegularImmutableList.A02;
            }
            return immutableList;
        } catch (IOException e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport("DbThreadThemeSerialization", AnonymousClass08S.A0J("Failed to deserialize theme gradient colors: ", str), e);
            return RegularImmutableList.A02;
        }
    }

    public ImmutableList A02(String str) {
        if (C06850cB.A0B(str)) {
            return RegularImmutableList.A02;
        }
        try {
            ImmutableList immutableList = (ImmutableList) this.A01.readValue(str, ImmutableList.class);
            if (immutableList == null) {
                return RegularImmutableList.A02;
            }
            return immutableList;
        } catch (IOException e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport("DbThreadThemeSerialization", AnonymousClass08S.A0J("Failed to deserialize theme strings list: ", str), e);
            return RegularImmutableList.A02;
        }
    }

    public String A03(ImmutableList immutableList) {
        if (C013509w.A02(immutableList)) {
            return null;
        }
        try {
            return this.A01.writeValueAsString(immutableList);
        } catch (C37571vt e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport("DbThreadThemeSerialization", "Failed to serialize theme gradient colors.", e);
            return null;
        }
    }

    public String A04(ImmutableList immutableList) {
        if (C013509w.A02(immutableList)) {
            return null;
        }
        try {
            return this.A01.writeValueAsString(immutableList);
        } catch (C37571vt e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport("DbThreadThemeSerialization", "Failed to serialize theme strings list.", e);
            return null;
        }
    }
}
