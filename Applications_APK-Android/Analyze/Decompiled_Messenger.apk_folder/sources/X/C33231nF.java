package X;

import java.util.HashMap;

/* renamed from: X.1nF  reason: invalid class name and case insensitive filesystem */
public final class C33231nF implements C23961Rq {
    public final /* synthetic */ AnonymousClass1SP A00;

    public C33231nF(AnonymousClass1SP r1) {
        this.A00 = r1;
    }

    public void C47(AnonymousClass1NY r19, int i) {
        AnonymousClass1NY r3;
        String $const$string;
        long j;
        int i2 = i;
        AnonymousClass1SP r7 = this.A00;
        C23351Pe r2 = r7.A03;
        AnonymousClass1NY r12 = r19;
        AnonymousClass1NY.A05(r12);
        AnonymousClass1T5 createImageTranscoder = r2.createImageTranscoder(r12.A07, this.A00.A04);
        C05520Zg.A02(createImageTranscoder);
        AnonymousClass1QK r1 = r7.A01;
        r1.A07.Bk8(r1, "ResizeAndRotateProducer");
        AnonymousClass1Q0 r32 = r7.A01.A09;
        AnonymousClass1S3 A01 = r7.A05.A00.A01();
        C22665B6t b6t = null;
        try {
            C80293sC transcode = createImageTranscoder.transcode(r12, A01, r32.A07, r32.A06, null, 85);
            if (transcode.A00 != 2) {
                AnonymousClass36w r5 = r32.A06;
                String identifier = createImageTranscoder.getIdentifier();
                AnonymousClass1QK r33 = r7.A01;
                if (r33.A07.C3Q(r33, "ResizeAndRotateProducer")) {
                    AnonymousClass1NY.A05(r12);
                    int i3 = r12.A05;
                    AnonymousClass1NY.A05(r12);
                    String A02 = AnonymousClass08S.A02(i3, "x", r12.A01);
                    if (r5 != null) {
                        $const$string = AnonymousClass08S.A02(r5.A03, "x", r5.A02);
                    } else {
                        $const$string = AnonymousClass24B.$const$string(51);
                    }
                    HashMap hashMap = new HashMap();
                    AnonymousClass1NY.A05(r12);
                    hashMap.put("Image format", String.valueOf(r12.A07));
                    hashMap.put("Original size", A02);
                    hashMap.put("Requested size", $const$string);
                    AnonymousClass1RU r122 = r7.A02;
                    synchronized (r122) {
                        try {
                            j = r122.A01 - r122.A02;
                        } catch (Throwable th) {
                            th = th;
                            throw th;
                        }
                    }
                    hashMap.put("queueTime", String.valueOf(j));
                    hashMap.put("Transcoder id", identifier);
                    hashMap.put("Transcoding result", String.valueOf(transcode));
                    b6t = C22665B6t.A00(hashMap);
                }
                AnonymousClass1PS A012 = AnonymousClass1PS.A01(A01.A01());
                try {
                    r3 = new AnonymousClass1NY(A012);
                    r3.A07 = AnonymousClass1SI.A06;
                    r3.A0A();
                    AnonymousClass1QK r22 = r7.A01;
                    r22.A07.Bk6(r22, "ResizeAndRotateProducer", b6t);
                    if (transcode.A00 != 1) {
                        i2 = i | 16;
                    }
                    r7.A00.Bgg(r3, i2);
                    AnonymousClass1NY.A04(r3);
                    AnonymousClass1PS.A05(A012);
                    A01.close();
                } catch (Throwable th2) {
                    th = th2;
                    AnonymousClass1PS.A05(A012);
                    throw th;
                }
            } else {
                throw new RuntimeException("Error while transcoding the image");
            }
        } catch (Exception e) {
            AnonymousClass1QK r23 = r7.A01;
            r23.A07.Bk4(r23, "ResizeAndRotateProducer", e, b6t);
            if (AnonymousClass1QU.A03(i2)) {
                r7.A00.BYh(e);
            }
        } catch (Throwable th3) {
            A01.close();
            throw th3;
        }
    }
}
