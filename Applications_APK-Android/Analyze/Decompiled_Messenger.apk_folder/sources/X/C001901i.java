package X;

import android.content.Context;
import android.os.health.HealthStats;
import android.os.health.SystemHealthManager;
import android.util.Pair;
import com.facebook.acra.ACRA;
import com.facebook.analytics.appstatelogger.AppState;
import com.facebook.analytics.appstatelogger.AppStateLogFile;
import com.facebook.forker.Process;
import java.io.File;
import java.util.List;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.01i  reason: invalid class name and case insensitive filesystem */
public final class C001901i extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.analytics.appstatelogger.AppStateLoggerThread";
    public int A00 = 0;
    public int A01 = -1;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public long A06;
    public Pair A07;
    public AnonymousClass06N A08;
    public AppStateLogFile A09;
    public C002101k A0A;
    public AnonymousClass04f A0B;
    public Object A0C;
    public Runnable A0D;
    public Throwable A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L = false;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public final Context A0S;
    public final AppState A0T;
    public final C002001j A0U = new C002001j(5);
    public final C001801h A0V;
    public final AnonymousClass08h A0W = new AnonymousClass08h();
    public final AnonymousClass01G A0X;
    public final C009908i A0Y;
    public final C009608e A0Z;
    public final File A0a;
    public final Object A0b = new Object();
    public final Object A0c = new Object();
    public final String A0d;
    public final List A0e;
    public final boolean A0f;

    /* JADX WARNING: Can't wrap try/catch for region: R(7:1|2|3|(2:7|8)|9|10|11) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void A03(X.C001901i r2) {
        /*
            monitor-enter(r2)
            r0 = 1
            r2.A0O = r0     // Catch:{ all -> 0x0017 }
            boolean r0 = r2.A0N     // Catch:{ all -> 0x0017 }
            if (r0 == 0) goto L_0x0012
            int r1 = r2.A01     // Catch:{ all -> 0x0017 }
            r0 = -1
            if (r1 == r0) goto L_0x0012
            int r0 = r2.A00     // Catch:{ IllegalArgumentException | SecurityException -> 0x0012 }
            android.os.Process.setThreadPriority(r1, r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x0012 }
        L_0x0012:
            r2.notify()     // Catch:{ all -> 0x0017 }
            monitor-exit(r2)
            return
        L_0x0017:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C001901i.A03(X.01i):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002b, code lost:
        if (r4 == X.AnonymousClass04f.A04) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A04(X.C001901i r3, X.AnonymousClass04f r4, java.lang.Runnable r5, boolean r6, boolean r7) {
        /*
            monitor-enter(r3)
            X.04f r1 = r3.A0B     // Catch:{ all -> 0x004f }
            if (r4 != r1) goto L_0x0007
            monitor-exit(r3)     // Catch:{ all -> 0x004f }
            return
        L_0x0007:
            r2 = 1
            if (r6 == 0) goto L_0x0015
            if (r7 == 0) goto L_0x0015
            X.04f r0 = X.AnonymousClass04f.NO_ANR_DETECTED     // Catch:{ all -> 0x004f }
            if (r4 == r0) goto L_0x0015
            if (r1 != r0) goto L_0x0015
            r3.A0F = r2     // Catch:{ all -> 0x004f }
            goto L_0x001e
        L_0x0015:
            X.04f r0 = X.AnonymousClass04f.NO_ANR_DETECTED     // Catch:{ all -> 0x004f }
            if (r4 != r0) goto L_0x001e
            if (r1 == r0) goto L_0x001e
            r0 = 0
            r3.A0F = r0     // Catch:{ all -> 0x004f }
        L_0x001e:
            r3.A0B = r4     // Catch:{ all -> 0x004f }
            X.04f r0 = X.AnonymousClass04f.DURING_ANR     // Catch:{ all -> 0x004f }
            if (r4 == r0) goto L_0x002d
            X.04f r0 = X.AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_BLOCKED     // Catch:{ all -> 0x004f }
            if (r4 == r0) goto L_0x002d
            X.04f r1 = X.AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_BLOCKED     // Catch:{ all -> 0x004f }
            r0 = 0
            if (r4 != r1) goto L_0x002e
        L_0x002d:
            r0 = 1
        L_0x002e:
            if (r0 == 0) goto L_0x0035
            int r0 = r3.A03     // Catch:{ all -> 0x004f }
            int r0 = r0 + r2
            r3.A03 = r0     // Catch:{ all -> 0x004f }
        L_0x0035:
            java.lang.Object r1 = r3.A0c     // Catch:{ all -> 0x004f }
            monitor-enter(r1)     // Catch:{ all -> 0x004f }
            r3.A0D = r5     // Catch:{ all -> 0x004c }
            monitor-exit(r1)     // Catch:{ all -> 0x004c }
            boolean r0 = r3.A0R     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x0044
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r3.A09     // Catch:{ all -> 0x004f }
            r3.A01(r0)     // Catch:{ all -> 0x004f }
        L_0x0044:
            A03(r3)     // Catch:{ all -> 0x004f }
            monitor-exit(r3)     // Catch:{ all -> 0x004f }
            A02(r3)
            return
        L_0x004c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x004c }
            throw r0     // Catch:{ all -> 0x004f }
        L_0x004f:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x004f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C001901i.A04(X.01i, X.04f, java.lang.Runnable, boolean, boolean):void");
    }

    public void A06() {
        synchronized (this) {
            A03(this);
        }
        A02(this);
    }

    public static Pair A00(Context context, String str) {
        HealthStats healthStats;
        long j;
        try {
            HealthStats takeMyUidSnapshot = ((SystemHealthManager) context.getSystemService(SystemHealthManager.class)).takeMyUidSnapshot();
            if (!takeMyUidSnapshot.hasStats(10014) || (healthStats = takeMyUidSnapshot.getStats(10014).get(str)) == null) {
                return null;
            }
            long j2 = 0;
            if (healthStats.hasMeasurement(30005)) {
                j = healthStats.getMeasurement(30005);
            } else {
                j = 0;
            }
            if (healthStats.hasMeasurement(30004)) {
                j2 = healthStats.getMeasurement(30004);
            }
            return new Pair(Long.valueOf(j), Long.valueOf(j2));
        } catch (SecurityException e) {
            C010708t.A0L("AppStateLoggerThread", "Unable to retrieve health stats", e);
            return null;
        }
    }

    private void A01(AppStateLogFile appStateLogFile) {
        if (this.A0A == C002101k.A0D) {
            switch (this.A0B.ordinal()) {
                case 1:
                case 4:
                    appStateLogFile.updateStatus(C002101k.A00);
                    return;
                case 2:
                case 5:
                    appStateLogFile.updateStatus(C002101k.A05);
                    return;
                case 3:
                    appStateLogFile.updateStatus(C002101k.A06);
                    return;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                    appStateLogFile.updateStatus(C002101k.A04);
                    return;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    appStateLogFile.updateStatus(C002101k.A03);
                    return;
                case 8:
                    appStateLogFile.updateStatus(C002101k.A01);
                    return;
                case Process.SIGKILL /*9*/:
                    appStateLogFile.updateStatus(C002101k.A02);
                    return;
                default:
                    appStateLogFile.updateStatus(C002101k.A09);
                    return;
            }
        }
    }

    public static void A02(C001901i r2) {
        synchronized (r2.A0C) {
            r2.A0I = true;
            r2.A0C.notify();
        }
    }

    private void A05(String str, Throwable th) {
        C010708t.A0R("AppStateLoggerThread", th, str);
        this.A0U.A00(str, th);
    }

    public void A07(boolean z, boolean z2) {
        if (z2) {
            synchronized (this.A0b) {
                try {
                    this.A0L = true;
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
        }
        synchronized (this) {
            try {
                A03(this);
            } catch (Throwable th2) {
                while (true) {
                    th = th2;
                }
            }
        }
        if (z) {
            A02(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x007c, code lost:
        if (r2.A0Q(r1) != false) goto L_0x007e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C001901i(X.AnonymousClass01G r6, java.io.File r7, X.C001801h r8, com.facebook.analytics.appstatelogger.AppState r9, X.C009608e r10, android.content.Context r11, java.lang.String r12, java.util.List r13, boolean r14, X.C03330Nh r15, int r16) {
        /*
            r5 = this;
            java.lang.String r0 = "AppStateLoggerThread"
            r5.<init>(r0)
            r1 = -1
            r5.A01 = r1
            r2 = 0
            r5.A00 = r2
            X.01j r3 = new X.01j
            r0 = 5
            r3.<init>(r0)
            r5.A0U = r3
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r5.A0c = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r5.A0b = r0
            r5.A0L = r2
            X.08h r0 = new X.08h
            r0.<init>()
            r5.A0W = r0
            r5.A0S = r11
            r5.A0X = r6
            r0 = 1
            r5.A0O = r0
            r5.A0I = r0
            X.01k r0 = X.C002101k.A0D
            r5.A0A = r0
            X.04f r0 = X.AnonymousClass04f.NO_ANR_DETECTED
            r5.A0B = r0
            r5.A0F = r2
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r5.A0C = r0
            r5.A0a = r7
            r5.A0V = r8
            r5.A0G = r2
            r5.A0T = r9
            r5.A0Z = r10
            r5.A0d = r12
            boolean r0 = r6.A0U(r11)
            r5.A0P = r0
            r5.A0f = r14
            r5.A0e = r13
            X.08h r0 = r5.A0W
            android.app.ActivityManager$RunningAppProcessInfo r0 = r0.A01
            r0.importance = r1
            X.08i r2 = new X.08i
            android.content.Context r1 = r5.A0S
            java.lang.String r0 = r5.A0d
            r3 = r16
            r2.<init>(r1, r0, r3, r15)
            r5.A0Y = r2
            X.01G r2 = r5.A0X
            android.content.Context r1 = r5.A0S
            boolean r0 = r2.A0P(r1)
            if (r0 != 0) goto L_0x007e
            boolean r1 = r2.A0Q(r1)
            r0 = 0
            if (r1 == 0) goto L_0x007f
        L_0x007e:
            r0 = 1
        L_0x007f:
            r5.A0H = r0
            java.lang.Object r2 = r5.A0C
            monitor-enter(r2)
            X.01G r1 = r5.A0X     // Catch:{ all -> 0x012d }
            android.content.Context r0 = r5.A0S     // Catch:{ all -> 0x012d }
            int r0 = r1.A05(r0)     // Catch:{ all -> 0x012d }
            long r0 = (long) r0     // Catch:{ all -> 0x012d }
            r5.A06 = r0     // Catch:{ all -> 0x012d }
            monitor-exit(r2)     // Catch:{ all -> 0x012d }
            X.01G r1 = r5.A0X
            android.content.Context r0 = r5.A0S
            int r0 = r1.A04(r0)
            long r0 = (long) r0
            r5.A05 = r0
            X.01G r1 = r5.A0X
            android.content.Context r0 = r5.A0S
            int r0 = r1.A03(r0)
            long r0 = (long) r0
            r5.A04 = r0
            X.01G r1 = r5.A0X
            android.content.Context r0 = r5.A0S
            boolean r0 = r1.A0I(r0)
            r5.A0N = r0
            X.01G r1 = r5.A0X
            android.content.Context r0 = r5.A0S
            boolean r0 = r1.A0G(r0)
            r5.A0K = r0
            X.01G r1 = r5.A0X
            android.content.Context r0 = r5.A0S
            boolean r0 = r1.A0R(r0)
            r5.A0M = r0
            X.01G r1 = r5.A0X
            android.content.Context r0 = r5.A0S
            int r0 = r1.A06(r0)
            r5.A02 = r0
            java.io.File r0 = r5.A0a     // Catch:{ Exception -> 0x0121 }
            java.io.File r2 = r0.getParentFile()     // Catch:{ Exception -> 0x0121 }
            boolean r0 = r2.exists()     // Catch:{ Exception -> 0x0121 }
            if (r0 != 0) goto L_0x00f3
            boolean r0 = r2.mkdirs()     // Catch:{ Exception -> 0x0121 }
            if (r0 != 0) goto L_0x00f3
            java.lang.String r1 = "Unable to create app state log directory: %s"
            java.lang.String r0 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x0121 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = java.lang.String.format(r1, r0)     // Catch:{ Exception -> 0x0121 }
            r0 = 0
            r5.A05(r1, r0)     // Catch:{ Exception -> 0x0121 }
            goto L_0x0129
        L_0x00f3:
            com.facebook.analytics.appstatelogger.AppStateLogFile r2 = new com.facebook.analytics.appstatelogger.AppStateLogFile     // Catch:{ Exception -> 0x0121 }
            java.io.File r1 = r5.A0a     // Catch:{ Exception -> 0x0121 }
            boolean r0 = r5.A0H     // Catch:{ Exception -> 0x0121 }
            r2.<init>(r1, r0)     // Catch:{ Exception -> 0x0121 }
            r5.A09 = r2     // Catch:{ Exception -> 0x0121 }
            X.01k r0 = X.C002101k.A0A     // Catch:{ Exception -> 0x0121 }
            r2.updateStatus(r0)     // Catch:{ Exception -> 0x0121 }
            X.01Y r1 = X.AnonymousClass01Y.INITIAL_STATE     // Catch:{ Exception -> 0x0121 }
            r0 = 32
            r2.updateForegroundEntityInfo(r1, r0)     // Catch:{ Exception -> 0x0121 }
            r3 = 32
            boolean r0 = r2.mIsEnabled     // Catch:{ Exception -> 0x0121 }
            if (r0 == 0) goto L_0x011a
            com.facebook.analytics.appstatelogger.AppStateLogFile.assertIsAscii(r3)     // Catch:{ Exception -> 0x0121 }
            java.nio.MappedByteBuffer r2 = r2.mMappedByteBuffer     // Catch:{ Exception -> 0x0121 }
            r1 = 4
            byte r0 = (byte) r3     // Catch:{ Exception -> 0x0121 }
            r2.put(r1, r0)     // Catch:{ Exception -> 0x0121 }
        L_0x011a:
            java.io.File r0 = r5.A0a     // Catch:{ Exception -> 0x0121 }
            r0.getAbsolutePath()     // Catch:{ Exception -> 0x0121 }
            r0 = 1
            goto L_0x012a
        L_0x0121:
            r1 = move-exception
            java.lang.String r0 = "Error opening app state logging file"
            r5.A05(r0, r1)
            r0 = 0
            goto L_0x012a
        L_0x0129:
            r0 = 0
        L_0x012a:
            r5.A0R = r0
            return
        L_0x012d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x012d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C001901i.<init>(X.01G, java.io.File, X.01h, com.facebook.analytics.appstatelogger.AppState, X.08e, android.content.Context, java.lang.String, java.util.List, boolean, X.0Nh, int):void");
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [X.0HM, java.lang.String, java.lang.Runnable] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Can't wrap try/catch for region: R(19:8|9|10|11|(2:13|(1:15))|16|17|(1:19)|20|(6:21|22|23|24|25|26)|39|(4:41|42|43|(3:44|45|(3:47|(2:51|(2:53|463)(1:462))|459)(1:54)))|94|(80:95|1af|106|107|1cd|(2:152|153)|154|155|(1:157)|158|(1:160)|161|162|163|164|(1:168)|169|170|(2:(1:173)|174)|(1:176)|177|(1:179)|180|2d7|187|188|2f9|194|(4:197|(2:199|473)(1:472)|470|195)|471|200|(1:202)|203|(1:205)|206|(1:208)|209|(1:211)|212|(1:214)|215|(4:218|(2:220|477)(1:476)|474|216)|475|221|(1:225)|226|(1:228)|229|(1:231)|232|(1:234)|235|(1:237)|238|(1:240)|241|(8:243|244|245|(2:248|246)|478|249|252|253)|254|(1:256)|(1:258)|(3:260|(1:262)(1:265)|263)|264|(1:267)|280|281|282|283|284|285|286|287|(1:289)|290|774|302|303|788|(3:311|312|(1:316))|351|(1:(2:396|397))(3:355|(3:357|358|468)(1:467)|464))|(0)|(1:399)|400|401|(3:403|404|410)(1:492)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:342|343|344|345|346) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x009a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:337:0x07cd */
    /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00d8 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:345:0x07d4 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:74:0x018b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:86:0x019b */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a4 A[Catch:{ all -> 0x084e }] */
    /* JADX WARNING: Removed duplicated region for block: B:396:0x0823 A[SYNTHETIC, Splitter:B:396:0x0823] */
    /* JADX WARNING: Removed duplicated region for block: B:399:0x0828 A[Catch:{ all -> 0x084e }] */
    /* JADX WARNING: Removed duplicated region for block: B:403:0x0831 A[SYNTHETIC, Splitter:B:403:0x0831] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ec A[Catch:{ all -> 0x084e }] */
    /* JADX WARNING: Removed duplicated region for block: B:492:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01b0 A[LOOP:3: B:97:0x01b0->B:103:?, LOOP_START, SYNTHETIC, Splitter:B:97:0x01b0] */
    public void run() {
        /*
            r36 = this;
            r5 = r36
            boolean r0 = r5.A0R     // Catch:{ all -> 0x0877 }
            if (r0 == 0) goto L_0x087d
            monitor-enter(r5)     // Catch:{ all -> 0x0877 }
            int r0 = android.os.Process.myTid()     // Catch:{ all -> 0x086d }
            r5.A01 = r0     // Catch:{ all -> 0x086d }
            int r0 = android.os.Process.myTid()     // Catch:{ all -> 0x086d }
            int r0 = android.os.Process.getThreadPriority(r0)     // Catch:{ all -> 0x086d }
            r5.A00 = r0     // Catch:{ all -> 0x086d }
            monitor-exit(r5)     // Catch:{ all -> 0x086d }
            java.util.Properties r4 = new java.util.Properties     // Catch:{ all -> 0x084e }
            r4.<init>()     // Catch:{ all -> 0x084e }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            java.lang.String r1 = r0.A0X     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "processName"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            int r0 = r0.A0U     // Catch:{ all -> 0x084e }
            java.lang.String r1 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "processId"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            java.lang.String r1 = r0.A0V     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "appVersionName"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            int r0 = r0.A0T     // Catch:{ all -> 0x084e }
            java.lang.String r1 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "appVersionCode"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            java.lang.String r1 = r0.A0W     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "installerName"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            long r0 = r0.A04     // Catch:{ all -> 0x084e }
            java.lang.String r1 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "aslCreationTime"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            boolean r0 = r0.A0Y     // Catch:{ all -> 0x084e }
            java.lang.String r1 = java.lang.Boolean.toString(r0)     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "startedInBackground"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            long r0 = r0.A0B     // Catch:{ all -> 0x084e }
            java.lang.String r1 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "timeToAslRegister"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x009a }
            java.lang.String r0 = "/proc/sys/kernel/osrelease"
            r1.<init>(r0)     // Catch:{ IOException -> 0x009a }
            boolean r0 = r1.exists()     // Catch:{ IOException -> 0x009a }
            if (r0 == 0) goto L_0x009a
            java.util.Scanner r0 = new java.util.Scanner     // Catch:{ IOException -> 0x009a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x009a }
            java.lang.String r1 = r0.nextLine()     // Catch:{ IOException -> 0x009a }
            int r0 = r1.length()     // Catch:{ IOException -> 0x009a }
            if (r0 <= 0) goto L_0x009a
            java.lang.String r0 = "kernelVersion"
            r4.setProperty(r0, r1)     // Catch:{ IOException -> 0x009a }
        L_0x009a:
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x084e }
            long r6 = r0.A06     // Catch:{ all -> 0x084e }
            r1 = 0
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00ad
            java.lang.String r1 = java.lang.Long.toString(r6)     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "deviceMemory"
            r4.setProperty(r0, r1)     // Catch:{ all -> 0x084e }
        L_0x00ad:
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x084e }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x084e }
            r1.<init>()     // Catch:{ all -> 0x084e }
            java.io.File r0 = r5.A0a     // Catch:{ all -> 0x084e }
            r1.append(r0)     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "_static"
            r1.append(r0)     // Catch:{ all -> 0x084e }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x084e }
            r2.<init>(r0)     // Catch:{ all -> 0x084e }
            r3 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00d9 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x00d9 }
            r4.store(r1, r3)     // Catch:{ all -> 0x00d2 }
            r1.close()     // Catch:{ IOException -> 0x00d9 }
            goto L_0x00e2
        L_0x00d2:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00d4 }
        L_0x00d4:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x00d8 }
        L_0x00d8:
            throw r0     // Catch:{ IOException -> 0x00d9 }
        L_0x00d9:
            r2 = move-exception
            java.lang.String r1 = "AppStateLoggerThread"
            java.lang.String r0 = "Error saving static properties"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ all -> 0x084e }
            r4 = r3
        L_0x00e2:
            X.01G r1 = r5.A0X     // Catch:{ all -> 0x084e }
            android.content.Context r0 = r5.A0S     // Catch:{ all -> 0x084e }
            boolean r0 = r1.A0L(r0)     // Catch:{ all -> 0x084e }
            if (r0 == 0) goto L_0x01a9
            java.lang.String r2 = "Error reading /proc/self/maps"
            java.util.TreeMap r6 = new java.util.TreeMap     // Catch:{ all -> 0x084e }
            r6.<init>()     // Catch:{ all -> 0x084e }
            java.lang.String r0 = "/proc/self/maps"
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x019c, IOException -> 0x01a3 }
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x019c, IOException -> 0x01a3 }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x019c, IOException -> 0x01a3 }
            r0 = 128(0x80, float:1.794E-43)
            r8.<init>(r1, r0)     // Catch:{ FileNotFoundException -> 0x019c, IOException -> 0x01a3 }
        L_0x0101:
            java.lang.String r7 = r8.readLine()     // Catch:{ all -> 0x0195 }
            if (r7 == 0) goto L_0x0133
            java.lang.String r0 = ".so"
            boolean r0 = r7.endsWith(r0)     // Catch:{ all -> 0x0195 }
            if (r0 == 0) goto L_0x0101
            r0 = 47
            int r1 = r7.indexOf(r0)     // Catch:{ all -> 0x0195 }
            r0 = -1
            if (r1 == r0) goto L_0x0101
            java.lang.String r7 = r7.substring(r1)     // Catch:{ all -> 0x0195 }
            boolean r0 = r6.containsKey(r7)     // Catch:{ all -> 0x0195 }
            if (r0 != 0) goto L_0x0101
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0195 }
            r0.<init>(r7)     // Catch:{ all -> 0x0195 }
            long r0 = r0.length()     // Catch:{ all -> 0x0195 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0195 }
            r6.put(r7, r0)     // Catch:{ all -> 0x0195 }
            goto L_0x0101
        L_0x0133:
            r8.close()     // Catch:{ FileNotFoundException -> 0x019c, IOException -> 0x01a3 }
            int r0 = r6.size()     // Catch:{ all -> 0x084e }
            if (r0 == 0) goto L_0x01a9
            java.io.OutputStreamWriter r7 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x018c }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x018c }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x018c }
            r1.<init>()     // Catch:{ IOException -> 0x018c }
            java.io.File r0 = r5.A0a     // Catch:{ IOException -> 0x018c }
            r1.append(r0)     // Catch:{ IOException -> 0x018c }
            java.lang.String r0 = "_lib"
            r1.append(r0)     // Catch:{ IOException -> 0x018c }
            java.lang.String r0 = r1.toString()     // Catch:{ IOException -> 0x018c }
            r2.<init>(r0)     // Catch:{ IOException -> 0x018c }
            java.lang.String r0 = "ISO-8859-1"
            r7.<init>(r2, r0)     // Catch:{ IOException -> 0x018c }
            java.util.Set r0 = r6.entrySet()     // Catch:{ all -> 0x0185 }
            java.util.Iterator r6 = r0.iterator()     // Catch:{ all -> 0x0185 }
        L_0x0163:
            boolean r0 = r6.hasNext()     // Catch:{ all -> 0x0185 }
            if (r0 == 0) goto L_0x0181
            java.lang.Object r0 = r6.next()     // Catch:{ all -> 0x0185 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x0185 }
            java.lang.String r2 = "%s %d\n"
            java.lang.Object r1 = r0.getKey()     // Catch:{ all -> 0x0185 }
            java.lang.Object r0 = r0.getValue()     // Catch:{ all -> 0x0185 }
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r2, r1, r0)     // Catch:{ all -> 0x0185 }
            r7.write(r0)     // Catch:{ all -> 0x0185 }
            goto L_0x0163
        L_0x0181:
            r7.close()     // Catch:{ IOException -> 0x018c }
            goto L_0x01a9
        L_0x0185:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0187 }
        L_0x0187:
            r0 = move-exception
            r7.close()     // Catch:{ all -> 0x018b }
        L_0x018b:
            throw r0     // Catch:{ IOException -> 0x018c }
        L_0x018c:
            r2 = move-exception
            java.lang.String r1 = "AppStateLoggerThread"
            java.lang.String r0 = "Error writing native library file"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ all -> 0x084e }
            goto L_0x01a9
        L_0x0195:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0197 }
        L_0x0197:
            r0 = move-exception
            r8.close()     // Catch:{ all -> 0x019b }
        L_0x019b:
            throw r0     // Catch:{ FileNotFoundException -> 0x019c, IOException -> 0x01a3 }
        L_0x019c:
            r1 = move-exception
            java.lang.String r0 = "AppStateLoggerThread"
            X.C010708t.A0L(r0, r2, r1)     // Catch:{ all -> 0x084e }
            goto L_0x01a9
        L_0x01a3:
            r1 = move-exception
            java.lang.String r0 = "AppStateLoggerThread"
            X.C010708t.A0L(r0, r2, r1)     // Catch:{ all -> 0x084e }
        L_0x01a9:
            long r6 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x084e }
        L_0x01ad:
            java.lang.Object r9 = r5.A0C     // Catch:{ all -> 0x084e }
            monitor-enter(r9)     // Catch:{ all -> 0x084e }
        L_0x01b0:
            boolean r2 = r5.A0I     // Catch:{ all -> 0x0847 }
            r8 = 0
            r5.A0I = r8     // Catch:{ all -> 0x0847 }
            long r10 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0847 }
            long r10 = r10 - r6
            long r0 = r5.A06     // Catch:{ all -> 0x0847 }
            long r0 = r0 - r10
            r10 = 0
            if (r2 != 0) goto L_0x01cb
            int r2 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r2 <= 0) goto L_0x01cb
            java.lang.Object r2 = r5.A0C     // Catch:{ InterruptedException -> 0x07f0 }
            r2.wait(r0)     // Catch:{ InterruptedException -> 0x07f0 }
            goto L_0x01b0
        L_0x01cb:
            monitor-exit(r9)     // Catch:{ all -> 0x0847 }
            r2 = 0
            monitor-enter(r5)     // Catch:{ all -> 0x0843 }
        L_0x01ce:
            boolean r0 = r5.A0O     // Catch:{ all -> 0x0840 }
            if (r0 != 0) goto L_0x01f4
            X.08e r1 = r5.A0Z     // Catch:{ InterruptedException -> 0x080a }
            monitor-enter(r1)     // Catch:{ InterruptedException -> 0x080a }
            int r0 = r1.A00     // Catch:{ all -> 0x0807 }
            monitor-exit(r1)     // Catch:{ InterruptedException -> 0x080a }
            if (r0 != 0) goto L_0x01e7
            boolean r0 = r5.A0P     // Catch:{ InterruptedException -> 0x080a }
            if (r0 == 0) goto L_0x01e4
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ InterruptedException -> 0x080a }
            boolean r0 = r0.A0S     // Catch:{ InterruptedException -> 0x080a }
            if (r0 != 0) goto L_0x01e7
        L_0x01e4:
            long r0 = r5.A04     // Catch:{ InterruptedException -> 0x080a }
            goto L_0x01e9
        L_0x01e7:
            long r0 = r5.A05     // Catch:{ InterruptedException -> 0x080a }
        L_0x01e9:
            int r6 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r6 != 0) goto L_0x01f1
            r5.wait()     // Catch:{ InterruptedException -> 0x080a }
            goto L_0x01ce
        L_0x01f1:
            r5.wait(r0)     // Catch:{ InterruptedException -> 0x080a }
        L_0x01f4:
            boolean r0 = r5.A0N     // Catch:{ all -> 0x0840 }
            if (r0 == 0) goto L_0x0205
            boolean r0 = r5.A0O     // Catch:{ IllegalArgumentException | SecurityException -> 0x0205 }
            if (r0 == 0) goto L_0x0202
            int r0 = r5.A00     // Catch:{ IllegalArgumentException | SecurityException -> 0x0205 }
        L_0x01fe:
            android.os.Process.setThreadPriority(r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x0205 }
            goto L_0x0205
        L_0x0202:
            r0 = 19
            goto L_0x01fe
        L_0x0205:
            if (r3 == 0) goto L_0x020c
            boolean r0 = r5.A0O     // Catch:{ all -> 0x0840 }
            r3.onWriteLoopStarted(r0)     // Catch:{ all -> 0x0840 }
        L_0x020c:
            com.facebook.analytics.appstatelogger.AppState r6 = new com.facebook.analytics.appstatelogger.AppState     // Catch:{ all -> 0x0840 }
            com.facebook.analytics.appstatelogger.AppState r0 = r5.A0T     // Catch:{ all -> 0x0840 }
            r6.<init>(r0)     // Catch:{ all -> 0x0840 }
            android.content.Context r0 = r5.A0S     // Catch:{ all -> 0x0840 }
            java.lang.String r0 = com.facebook.acra.criticaldata.CriticalAppData.getUserId(r0)     // Catch:{ all -> 0x0840 }
            r6.A0P = r0     // Catch:{ all -> 0x0840 }
            boolean r0 = r5.A0G     // Catch:{ all -> 0x0840 }
            r6.A0R = r0     // Catch:{ all -> 0x0840 }
            java.lang.Throwable r0 = r5.A0E     // Catch:{ all -> 0x0840 }
            r6.A0Q = r0     // Catch:{ all -> 0x0840 }
            r6.A02 = r8     // Catch:{ all -> 0x0840 }
            boolean r0 = r5.A0F     // Catch:{ all -> 0x0840 }
            r6.A0S = r0     // Catch:{ all -> 0x0840 }
            int r0 = r5.A03     // Catch:{ all -> 0x0840 }
            r6.A03 = r0     // Catch:{ all -> 0x0840 }
            X.06N r13 = r5.A08     // Catch:{ all -> 0x0840 }
            X.08e r7 = r5.A0Z     // Catch:{ all -> 0x0840 }
            monitor-enter(r7)     // Catch:{ all -> 0x0840 }
            java.util.WeakHashMap r1 = new java.util.WeakHashMap     // Catch:{ all -> 0x083d }
            java.util.WeakHashMap r0 = r7.A02     // Catch:{ all -> 0x083d }
            r1.<init>(r0)     // Catch:{ all -> 0x083d }
            java.util.Map r29 = java.util.Collections.unmodifiableMap(r1)     // Catch:{ all -> 0x083d }
            monitor-exit(r7)     // Catch:{ all -> 0x0840 }
            r5.A0O = r8     // Catch:{ all -> 0x0840 }
            monitor-exit(r5)     // Catch:{ all -> 0x0840 }
            if (r13 == 0) goto L_0x0267
            long r0 = r13.A01()     // Catch:{ all -> 0x0843 }
            r6.A0A = r0     // Catch:{ all -> 0x0843 }
            java.lang.String r0 = r13.A02()     // Catch:{ all -> 0x0843 }
            r6.A0G = r0     // Catch:{ all -> 0x0843 }
            java.lang.String r0 = r13.A06()     // Catch:{ all -> 0x0843 }
            r6.A0O = r0     // Catch:{ all -> 0x0843 }
            java.lang.String r0 = r13.A04()     // Catch:{ all -> 0x0843 }
            r6.A0L = r0     // Catch:{ all -> 0x0843 }
            java.lang.String r0 = r13.A05()     // Catch:{ all -> 0x0843 }
            r6.A0M = r0     // Catch:{ all -> 0x0843 }
            java.lang.String r0 = r13.A03()     // Catch:{ all -> 0x0843 }
            r6.A0H = r0     // Catch:{ all -> 0x0843 }
        L_0x0267:
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r5.A09     // Catch:{ IOException | JSONException -> 0x07d5 }
            r34 = r0
            r14 = 0
            if (r4 != 0) goto L_0x026f
            r14 = 1
        L_0x026f:
            X.01k r1 = X.C002101k.A0F     // Catch:{ IOException | JSONException -> 0x07d5 }
            r0.updateStatus(r1)     // Catch:{ IOException | JSONException -> 0x07d5 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ IOException | JSONException -> 0x07d5 }
            r0 = 2
            r10.<init>(r0)     // Catch:{ IOException | JSONException -> 0x07d5 }
            boolean r0 = r5.A0J     // Catch:{ IOException | JSONException -> 0x07d5 }
            if (r0 == 0) goto L_0x0286
            X.099 r0 = new X.099     // Catch:{ IOException | JSONException -> 0x07d5 }
            r0.<init>()     // Catch:{ IOException | JSONException -> 0x07d5 }
            r10.add(r0)     // Catch:{ IOException | JSONException -> 0x07d5 }
        L_0x0286:
            java.io.ByteArrayOutputStream r26 = new java.io.ByteArrayOutputStream     // Catch:{ IOException | JSONException -> 0x07d5 }
            r1 = 4096(0x1000, float:5.74E-42)
            r0 = r26
            r0.<init>(r1)     // Catch:{ IOException | JSONException -> 0x07d5 }
            r7 = r0
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x07ce }
            r0 = 24
            if (r1 < r0) goto L_0x02a4
            android.util.Pair r0 = r5.A07     // Catch:{ all -> 0x07ce }
            if (r0 != 0) goto L_0x02a4
            android.content.Context r1 = r5.A0S     // Catch:{ all -> 0x07ce }
            java.lang.String r0 = r5.A0d     // Catch:{ all -> 0x07ce }
            android.util.Pair r0 = A00(r1, r0)     // Catch:{ all -> 0x07ce }
            r5.A07 = r0     // Catch:{ all -> 0x07ce }
        L_0x02a4:
            boolean r0 = r5.A0f     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 == 0) goto L_0x02bb
            if (r3 == 0) goto L_0x02af
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ IllegalStateException -> 0x0746 }
            r3.onCodePoint$REDEX$yYf5PajrCI7(r0)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x02af:
            long r0 = X.AnonymousClass0HN.A01()     // Catch:{ IllegalStateException -> 0x0746 }
            r6.A0D = r0     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = X.AnonymousClass0HN.A00()     // Catch:{ IllegalStateException -> 0x0746 }
            r6.A07 = r0     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x02bb:
            if (r3 == 0) goto L_0x02c2
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ IllegalStateException -> 0x0746 }
            r3.onCodePoint$REDEX$yYf5PajrCI7(r0)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x02c2:
            android.content.Context r1 = r5.A0S     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "activity"
            java.lang.Object r1 = r1.getSystemService(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            android.app.ActivityManager r1 = (android.app.ActivityManager) r1     // Catch:{ IllegalStateException -> 0x0746 }
            if (r1 != 0) goto L_0x02d5
            java.lang.String r8 = "AppStateLoggerThread"
            java.lang.String r0 = "Could not get ActivityManager"
            X.C010708t.A0I(r8, r0)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x02d5:
            X.08h r8 = r5.A0W     // Catch:{ IllegalStateException -> 0x0746 }
            monitor-enter(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            if (r1 == 0) goto L_0x02e1
            X.08h r0 = r5.A0W     // Catch:{ all -> 0x0740 }
            android.app.ActivityManager$MemoryInfo r0 = r0.A00     // Catch:{ all -> 0x0740 }
            r1.getMemoryInfo(r0)     // Catch:{ all -> 0x0740 }
        L_0x02e1:
            X.08i r1 = r5.A0Y     // Catch:{ all -> 0x0740 }
            X.08h r0 = r5.A0W     // Catch:{ all -> 0x0740 }
            android.app.ActivityManager$RunningAppProcessInfo r0 = r0.A01     // Catch:{ all -> 0x0740 }
            r1.A03(r0)     // Catch:{ all -> 0x0740 }
            monitor-exit(r8)     // Catch:{ all -> 0x0740 }
            X.01h r0 = r5.A0V     // Catch:{ IllegalStateException -> 0x0746 }
            long r17 = r0.A01()     // Catch:{ IllegalStateException -> 0x0746 }
            X.01h r0 = r5.A0V     // Catch:{ IllegalStateException -> 0x0746 }
            long r19 = r0.A00()     // Catch:{ IllegalStateException -> 0x0746 }
            X.01h r8 = r5.A0V     // Catch:{ IllegalStateException -> 0x0746 }
            monitor-enter(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r8.A00     // Catch:{ all -> 0x0743 }
            r32 = r0
            monitor-exit(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            android.util.Pair r0 = r5.A07     // Catch:{ IllegalStateException -> 0x0746 }
            r28 = r0
            boolean r0 = r5.A0P     // Catch:{ IllegalStateException -> 0x0746 }
            r27 = r0
            boolean r0 = r5.A0f     // Catch:{ IllegalStateException -> 0x0746 }
            r25 = r0
            X.08h r1 = r5.A0W     // Catch:{ IllegalStateException -> 0x0746 }
            android.app.ActivityManager$MemoryInfo r0 = r1.A00     // Catch:{ IllegalStateException -> 0x0746 }
            r31 = r0
            android.app.ActivityManager$RunningAppProcessInfo r0 = r1.A01     // Catch:{ IllegalStateException -> 0x0746 }
            r30 = r0
            java.util.List r0 = r5.A0e     // Catch:{ IllegalStateException -> 0x0746 }
            java.io.OutputStreamWriter r9 = new java.io.OutputStreamWriter     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r1 = "UTF-8"
            r9.<init>(r7, r1)     // Catch:{ IllegalStateException -> 0x0746 }
            long r15 = android.os.SystemClock.uptimeMillis()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r11 = "{"
            r9.append(r11)     // Catch:{ IllegalStateException -> 0x0746 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x032b:
            boolean r0 = r7.hasNext()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r8 = ","
            if (r0 == 0) goto L_0x0343
            java.lang.Object r1 = r7.next()     // Catch:{ IllegalStateException -> 0x0746 }
            X.01J r1 = (X.AnonymousClass01J) r1     // Catch:{ IllegalStateException -> 0x0746 }
            boolean r0 = r1.ANg(r9, r3)     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 == 0) goto L_0x032b
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            goto L_0x032b
        L_0x0343:
            java.lang.String r7 = "\","
            if (r14 == 0) goto L_0x03c3
            java.lang.String r1 = r6.A0X     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"processName\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"process_id\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            int r0 = r6.A0U     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r1 = r6.A0V     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"appVersionName\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
            int r1 = r6.A0T     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"appVersionCode\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Integer.toString(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r1 = r6.A0W     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"installerName\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A04     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r14 = "\"aslCreationTime\":"
            r9.append(r14)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            boolean r1 = r6.A0Y     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"startedInBackground\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Boolean.toString(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"deviceMemory\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A06     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x03c3:
            java.lang.String r0 = "\"processWallClockUptimeMs\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A05     // Catch:{ IllegalStateException -> 0x0746 }
            long r21 = r15 - r0
            java.lang.String r0 = java.lang.Long.toString(r21)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A0C     // Catch:{ IllegalStateException -> 0x0746 }
            r23 = 0
            int r14 = (r0 > r23 ? 1 : (r0 == r23 ? 0 : -1))
            if (r14 <= 0) goto L_0x03ed
            java.lang.String r14 = "\"timeToFirstActivityTransitionMs\":"
            r9.append(r14)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x03ed:
            long r0 = r6.A09     // Catch:{ IllegalStateException -> 0x0746 }
            int r14 = (r0 > r23 ? 1 : (r0 == r23 ? 0 : -1))
            if (r14 <= 0) goto L_0x040a
            java.lang.String r0 = "\"lastUpdateTimeMs\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r21 = java.lang.System.currentTimeMillis()     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A09     // Catch:{ IllegalStateException -> 0x0746 }
            long r21 = r21 - r0
            java.lang.String r0 = java.lang.Long.toString(r21)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x040a:
            long r0 = r6.A0A     // Catch:{ IllegalStateException -> 0x0746 }
            int r14 = (r0 > r23 ? 1 : (r0 == r23 ? 0 : -1))
            if (r14 <= 0) goto L_0x0421
            java.lang.String r0 = "\"lastLauncherIntentTimeMs\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A0A     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x0421:
            java.lang.String r1 = r6.A0G     // Catch:{ IllegalStateException -> 0x0746 }
            if (r1 == 0) goto L_0x0430
            java.lang.String r0 = "\"analyticsSessionId\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x0430:
            java.lang.String r0 = "\"activities\":["
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.util.Set r0 = r29.entrySet()     // Catch:{ IllegalStateException -> 0x0746 }
            java.util.Iterator r21 = r0.iterator()     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x043d:
            boolean r0 = r21.hasNext()     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 == 0) goto L_0x0481
            java.lang.Object r0 = r21.next()     // Catch:{ IllegalStateException -> 0x0746 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Object r1 = r0.getKey()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Class r1 = r1.getClass()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r14 = r1.getSimpleName()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Object r1 = r0.getValue()     // Catch:{ IllegalStateException -> 0x0746 }
            X.01Y r1 = (X.AnonymousClass01Y) r1     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r11)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"name\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r14)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\",\"state\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r1.toString()     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"}"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            boolean r0 = r21.hasNext()     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 == 0) goto L_0x043d
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            goto L_0x043d
        L_0x0481:
            java.lang.String r0 = "],"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A0P     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 == 0) goto L_0x049d
            boolean r0 = r0.isEmpty()     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 != 0) goto L_0x049d
            java.lang.String r0 = "\"userId\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A0P     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x049d:
            java.lang.String r0 = "\"granularExposures\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A0K     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"navModule\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A0N     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"endpoint\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A00()     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"timeSinceNavigationMs\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A08     // Catch:{ IllegalStateException -> 0x0746 }
            long r21 = r15 - r0
            java.lang.String r0 = java.lang.Long.toString(r21)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            if (r25 == 0) goto L_0x04fd
            java.lang.String r0 = "\"free_internal_disk_space_bytes\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A07     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"total_internal_disk_space_bytes\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r6.A0D     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x04fd:
            java.lang.String r0 = "\"radioType\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A0O     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"mobileconfig_canary\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A0L     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"mobileconfig_fetch_timestamps\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A0M     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r11 = r6.A0H     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r1 = "\""
            if (r11 == 0) goto L_0x053f
            java.lang.String r0 = "\"attribution_id\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\\\""
            java.lang.String r0 = java.util.regex.Matcher.quoteReplacement(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r11.replaceAll(r1, r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x053f:
            java.lang.String r0 = "\"total_fgtm_ms\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r17)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"current_fgtm_ms\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r19)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"total_fg_count\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r17 = r32
            java.lang.String r0 = java.lang.Long.toString(r17)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Boolean r0 = r6.A0F     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 == 0) goto L_0x0583
            java.lang.String r0 = "\"sticky_bit_enabled\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Boolean r0 = r6.A0F     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r0.toString()     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x0583:
            java.lang.String r0 = "\"first_message_code\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            int r0 = r6.A01     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"first_message_str\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r6.A0J     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r7)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"anr_detector_id\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            int r0 = r6.A00     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"device_is_shutting_down\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            boolean r0 = r6.A0R     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Boolean.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Throwable r0 = r6.A0Q     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 == 0) goto L_0x05df
            java.lang.String r0 = "\"last_throwable\":\""
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Throwable r0 = r6.A0Q     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = r0.getName()     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x05df:
            java.lang.String r0 = "\"available_memory\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r0 = r31
            long r0 = r0.availMem     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"lmk_threshold\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r0 = r31
            long r0 = r0.threshold     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Runtime r7 = java.lang.Runtime.getRuntime()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"java_runtime_max_memory_bytes\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r7.maxMemory()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"java_runtime_total_memory_bytes\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r7.totalMemory()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"java_runtime_free_memory_bytes\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r7.freeMemory()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ IllegalStateException -> 0x0746 }
            r0 = 16
            if (r1 < r0) goto L_0x066e
            java.lang.String r0 = "\"lmk_importance\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r0 = r30
            int r0 = r0.importance     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"lmk_last_trim_level\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r0 = r30
            int r0 = r0.lastTrimLevel     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x066e:
            java.lang.String r0 = "\"future_num_activities\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            int r0 = r6.A02     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"cur_uptime\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r15)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            X.08g r0 = r6.A0E     // Catch:{ IllegalStateException -> 0x0746 }
            java.util.Map r0 = r0.A00     // Catch:{ IllegalStateException -> 0x0746 }
            boolean r0 = r0.isEmpty()     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 != 0) goto L_0x06d2
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"custom_app_data\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            X.08g r0 = r6.A0E     // Catch:{ IllegalStateException -> 0x0746 }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ OutOfMemoryError -> 0x06cd }
            r7.<init>()     // Catch:{ OutOfMemoryError -> 0x06cd }
            java.util.Map r0 = r0.A00     // Catch:{ OutOfMemoryError -> 0x06cd }
            java.util.Set r0 = r0.entrySet()     // Catch:{ OutOfMemoryError -> 0x06cd }
            java.util.Iterator r11 = r0.iterator()     // Catch:{ OutOfMemoryError -> 0x06cd }
        L_0x06ae:
            boolean r0 = r11.hasNext()     // Catch:{ OutOfMemoryError -> 0x06cd }
            if (r0 == 0) goto L_0x06c8
            java.lang.Object r0 = r11.next()     // Catch:{ OutOfMemoryError -> 0x06cd }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ OutOfMemoryError -> 0x06cd }
            java.lang.Object r1 = r0.getKey()     // Catch:{ OutOfMemoryError -> 0x06cd }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ OutOfMemoryError -> 0x06cd }
            java.lang.Object r0 = r0.getValue()     // Catch:{ OutOfMemoryError -> 0x06cd }
            r7.put(r1, r0)     // Catch:{ OutOfMemoryError -> 0x06cd }
            goto L_0x06ae
        L_0x06c8:
            java.lang.String r0 = r7.toString()     // Catch:{ OutOfMemoryError -> 0x06cd }
            goto L_0x06cf
        L_0x06cd:
            java.lang.String r0 = "{}"
        L_0x06cf:
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x06d2:
            int r1 = r6.A03     // Catch:{ IllegalStateException -> 0x0746 }
            if (r1 <= 0) goto L_0x06e5
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"acra_anr_count\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Integer.toString(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x06e5:
            if (r28 == 0) goto L_0x0719
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"anr_count\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r0 = r28
            java.lang.Object r0 = r0.first     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r0.longValue()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"crash_count\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r0 = r28
            java.lang.Object r0 = r0.second     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ IllegalStateException -> 0x0746 }
            long r0 = r0.longValue()     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x0719:
            if (r27 == 0) goto L_0x072c
            r9.append(r8)     // Catch:{ IllegalStateException -> 0x0746 }
            java.lang.String r0 = "\"fg_anr\":"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            boolean r0 = r6.A0S     // Catch:{ IllegalStateException -> 0x0746 }
            if (r0 == 0) goto L_0x0735
            java.lang.String r0 = "1"
        L_0x0729:
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x072c:
            java.lang.String r0 = "}"
            r9.append(r0)     // Catch:{ IllegalStateException -> 0x0746 }
            r9.flush()     // Catch:{ IllegalStateException -> 0x0746 }
            goto L_0x0738
        L_0x0735:
            java.lang.String r0 = "0"
            goto L_0x0729
        L_0x0738:
            if (r3 == 0) goto L_0x074c
            java.lang.Integer r1 = X.AnonymousClass07B.A0N     // Catch:{ IllegalStateException -> 0x0746 }
            r3.onCodePoint$REDEX$yYf5PajrCI7(r1)     // Catch:{ IllegalStateException -> 0x0746 }
            goto L_0x074c
        L_0x0740:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0740 }
            goto L_0x0745
        L_0x0743:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x0745:
            throw r0     // Catch:{ IllegalStateException -> 0x0746 }
        L_0x0746:
            r6 = move-exception
            java.lang.String r1 = "Generating malformed JSON"
            r5.A05(r1, r6)     // Catch:{ all -> 0x07ce }
        L_0x074c:
            java.io.OutputStream r0 = r34.createContentOutputStream()     // Catch:{ all -> 0x07ce }
            r10.add(r0)     // Catch:{ all -> 0x07ce }
            X.01p r1 = new X.01p     // Catch:{ all -> 0x07ce }
            r1.<init>(r10)     // Catch:{ all -> 0x07ce }
            r0 = r26
            r0.writeTo(r1)     // Catch:{ all -> 0x07c7 }
            r1.flush()     // Catch:{ all -> 0x07c7 }
            r1.close()     // Catch:{ all -> 0x07ce }
            r26.close()     // Catch:{ IOException | JSONException -> 0x07d5 }
            if (r3 == 0) goto L_0x076d
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y     // Catch:{ IOException | JSONException -> 0x07d5 }
            r3.onCodePoint$REDEX$yYf5PajrCI7(r0)     // Catch:{ IOException | JSONException -> 0x07d5 }
        L_0x076d:
            r0 = r34
            r5.A01(r0)     // Catch:{ IOException | JSONException -> 0x07d5 }
            java.lang.Object r6 = r5.A0b     // Catch:{ IOException | JSONException -> 0x07d5 }
            monitor-enter(r6)     // Catch:{ IOException | JSONException -> 0x07d5 }
            java.lang.Object r1 = r5.A0b     // Catch:{ all -> 0x07c4 }
            monitor-enter(r1)     // Catch:{ all -> 0x07c4 }
            boolean r0 = r5.A0L     // Catch:{ all -> 0x07c1 }
            if (r0 == 0) goto L_0x0784
            r0 = 0
            r5.A0L = r0     // Catch:{ all -> 0x07c1 }
            java.lang.Object r0 = r5.A0b     // Catch:{ all -> 0x07c1 }
            r0.notify()     // Catch:{ all -> 0x07c1 }
        L_0x0784:
            monitor-exit(r1)     // Catch:{ all -> 0x07c1 }
            monitor-exit(r6)     // Catch:{ all -> 0x07c4 }
            java.lang.Object r1 = r5.A0c     // Catch:{ IOException | JSONException -> 0x07d5 }
            monitor-enter(r1)     // Catch:{ IOException | JSONException -> 0x07d5 }
            java.lang.Runnable r0 = r5.A0D     // Catch:{ all -> 0x07be }
            if (r0 == 0) goto L_0x0792
            r0.run()     // Catch:{ all -> 0x07be }
            r5.A0D = r3     // Catch:{ all -> 0x07be }
        L_0x0792:
            monitor-exit(r1)     // Catch:{ all -> 0x07be }
            if (r13 == 0) goto L_0x07db
            boolean r0 = r5.A0M     // Catch:{ IOException | JSONException -> 0x07d5 }
            if (r0 == 0) goto L_0x07db
            X.01k r1 = r5.A0A     // Catch:{ IOException | JSONException -> 0x07d5 }
            X.01k r0 = X.C002101k.A0D     // Catch:{ IOException | JSONException -> 0x07d5 }
            if (r1 != r0) goto L_0x07db
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r5.A09     // Catch:{ IOException | JSONException -> 0x07d5 }
            android.content.Context r7 = r5.A0S     // Catch:{ IOException | JSONException -> 0x07d5 }
            java.lang.String r15 = X.AnonymousClass01P.A05()     // Catch:{ IOException | JSONException -> 0x07d5 }
            int r6 = r5.A02     // Catch:{ IOException | JSONException -> 0x07d5 }
            X.0HO r1 = new X.0HO     // Catch:{ IOException | JSONException -> 0x07d5 }
            r1.<init>(r0)     // Catch:{ IOException | JSONException -> 0x07d5 }
            X.0HP r0 = new X.0HP     // Catch:{ IOException | JSONException -> 0x07d5 }
            r0.<init>(r4)     // Catch:{ IOException | JSONException -> 0x07d5 }
            r14 = r7
            r16 = r6
            r17 = r1
            r18 = r0
            r13.A07(r14, r15, r16, r17, r18)     // Catch:{ IOException | JSONException -> 0x07d5 }
            goto L_0x07db
        L_0x07be:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x07be }
            goto L_0x07c6
        L_0x07c1:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x07c1 }
            throw r0     // Catch:{ all -> 0x07c4 }
        L_0x07c4:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x07c4 }
        L_0x07c6:
            throw r0     // Catch:{ IOException | JSONException -> 0x07d5 }
        L_0x07c7:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x07c9 }
        L_0x07c9:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x07cd }
        L_0x07cd:
            throw r0     // Catch:{ all -> 0x07ce }
        L_0x07ce:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x07d0 }
        L_0x07d0:
            r0 = move-exception
            r26.close()     // Catch:{ all -> 0x07d4 }
        L_0x07d4:
            throw r0     // Catch:{ IOException | JSONException -> 0x07d5 }
        L_0x07d5:
            r2 = move-exception
            java.lang.String r0 = "Error dumping app state to log file"
            r5.A05(r0, r2)     // Catch:{ all -> 0x0843 }
        L_0x07db:
            X.01k r6 = r5.A0A     // Catch:{ all -> 0x0843 }
            X.01k r1 = X.C002101k.A0D     // Catch:{ all -> 0x0843 }
            if (r6 != r1) goto L_0x0821
            boolean r0 = r5.A0Q     // Catch:{ all -> 0x0843 }
            if (r0 != 0) goto L_0x0821
            long r6 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0843 }
            if (r3 == 0) goto L_0x01ad
            r3.onWriteLoopEnded(r2)     // Catch:{ all -> 0x084e }
            goto L_0x01ad
        L_0x07f0:
            r1 = move-exception
            java.lang.String r0 = "Interrupted while sleeping"
            r5.A05(r0, r1)     // Catch:{ all -> 0x0847 }
            monitor-exit(r9)     // Catch:{ all -> 0x0847 }
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r5.A09     // Catch:{ all -> 0x0877 }
            if (r0 == 0) goto L_0x087d
            r0.close()     // Catch:{ IOException -> 0x07ff }
            goto L_0x0806
        L_0x07ff:
            r1 = move-exception
            java.lang.String r0 = "Failed to close log file"
            r5.A05(r0, r1)     // Catch:{ all -> 0x0877 }
            return
        L_0x0806:
            return
        L_0x0807:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ InterruptedException -> 0x080a }
            throw r0     // Catch:{ InterruptedException -> 0x080a }
        L_0x080a:
            r1 = move-exception
            java.lang.String r0 = "Interrupted while waiting for updated app state"
            r5.A05(r0, r1)     // Catch:{ all -> 0x0840 }
            monitor-exit(r5)     // Catch:{ all -> 0x0840 }
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r5.A09     // Catch:{ all -> 0x0877 }
            if (r0 == 0) goto L_0x087d
            r0.close()     // Catch:{ IOException -> 0x0819 }
            goto L_0x0820
        L_0x0819:
            r1 = move-exception
            java.lang.String r0 = "Failed to close log file"
            r5.A05(r0, r1)     // Catch:{ all -> 0x0877 }
            return
        L_0x0820:
            return
        L_0x0821:
            if (r3 == 0) goto L_0x0826
            r3.onWriteLoopEnded(r2)     // Catch:{ all -> 0x084e }
        L_0x0826:
            if (r6 == r1) goto L_0x082d
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r5.A09     // Catch:{ all -> 0x084e }
            r0.updateStatus(r6)     // Catch:{ all -> 0x084e }
        L_0x082d:
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r5.A09     // Catch:{ all -> 0x0877 }
            if (r0 == 0) goto L_0x087d
            r0.close()     // Catch:{ IOException -> 0x0835 }
            goto L_0x083c
        L_0x0835:
            r1 = move-exception
            java.lang.String r0 = "Failed to close log file"
            r5.A05(r0, r1)     // Catch:{ all -> 0x0877 }
            return
        L_0x083c:
            return
        L_0x083d:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0840 }
            throw r0     // Catch:{ all -> 0x0840 }
        L_0x0840:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0840 }
            throw r0     // Catch:{ all -> 0x0843 }
        L_0x0843:
            r0 = move-exception
            if (r3 == 0) goto L_0x084d
            goto L_0x084a
        L_0x0847:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0847 }
            goto L_0x084d
        L_0x084a:
            r3.onWriteLoopEnded(r2)     // Catch:{ all -> 0x084e }
        L_0x084d:
            throw r0     // Catch:{ all -> 0x084e }
        L_0x084e:
            r2 = move-exception
            boolean r0 = r5.A0K     // Catch:{ all -> 0x0864 }
            if (r0 == 0) goto L_0x0863
            com.facebook.analytics.appstatelogger.AppStateLogFile r1 = r5.A09     // Catch:{ all -> 0x0864 }
            if (r1 == 0) goto L_0x0863
            X.01k r0 = X.C002101k.A08     // Catch:{ Exception -> 0x085d }
            r1.updateStatus(r0)     // Catch:{ Exception -> 0x085d }
            goto L_0x0863
        L_0x085d:
            r1 = move-exception
            java.lang.String r0 = "Error while trying to update status"
            r5.A05(r0, r1)     // Catch:{ all -> 0x0864 }
        L_0x0863:
            throw r2     // Catch:{ all -> 0x0864 }
        L_0x0864:
            r2 = move-exception
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r5.A09     // Catch:{ all -> 0x0877 }
            if (r0 == 0) goto L_0x0876
            r0.close()     // Catch:{ IOException -> 0x0870 }
            goto L_0x0876
        L_0x086d:
            r2 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x086d }
            goto L_0x0876
        L_0x0870:
            r1 = move-exception
            java.lang.String r0 = "Failed to close log file"
            r5.A05(r0, r1)     // Catch:{ all -> 0x0877 }
        L_0x0876:
            throw r2     // Catch:{ all -> 0x0877 }
        L_0x0877:
            r1 = move-exception
            java.lang.String r0 = "Unhandled exception in AppStateLoggerThread.run"
            r5.A05(r0, r1)
        L_0x087d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C001901i.run():void");
    }
}
