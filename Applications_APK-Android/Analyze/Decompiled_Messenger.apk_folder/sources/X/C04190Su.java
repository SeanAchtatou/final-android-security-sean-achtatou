package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0Su  reason: invalid class name and case insensitive filesystem */
public final class C04190Su implements Set {
    public final /* synthetic */ C04180St A00;

    public C04190Su(C04180St r1) {
        this.A00 = r1;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        int A02 = this.A00.A02();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            this.A00.A0A(entry.getKey(), entry.getValue());
        }
        if (A02 != this.A00.A02()) {
            return true;
        }
        return false;
    }

    public void clear() {
        this.A00.A08();
    }

    public boolean contains(Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            int A03 = this.A00.A03(entry.getKey());
            if (A03 >= 0) {
                Object A05 = this.A00.A05(A03, 1);
                Object value = entry.getValue();
                if (A05 == value) {
                    return true;
                }
                if (A05 == null || !A05.equals(value)) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode;
        int hashCode2;
        int i = 0;
        for (int A02 = this.A00.A02() - 1; A02 >= 0; A02--) {
            C04180St r0 = this.A00;
            Object A05 = r0.A05(A02, 0);
            Object A052 = r0.A05(A02, 1);
            if (A05 == null) {
                hashCode = 0;
            } else {
                hashCode = A05.hashCode();
            }
            if (A052 == null) {
                hashCode2 = 0;
            } else {
                hashCode2 = A052.hashCode();
            }
            i += hashCode ^ hashCode2;
        }
        return i;
    }

    public boolean isEmpty() {
        if (this.A00.A02() == 0) {
            return true;
        }
        return false;
    }

    public Iterator iterator() {
        return new C26921cM(this.A00);
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public int size() {
        return this.A00.A02();
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object obj) {
        return C04180St.A01(this, obj);
    }

    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public Object[] toArray(Object[] objArr) {
        throw new UnsupportedOperationException();
    }
}
