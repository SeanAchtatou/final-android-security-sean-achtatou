package X;

/* renamed from: X.0V7  reason: invalid class name */
public enum AnonymousClass0V7 {
    REALTIME_DO_NOT_USE(-8),
    BLOCKING_UI(-7),
    URGENT(5),
    IMPORTANT(5),
    FOREGROUND(10),
    NORMAL(14),
    BACKGROUND(19);
    
    public final int mAndroidThreadPriority;

    private AnonymousClass0V7(int i) {
        this.mAndroidThreadPriority = i;
    }

    public static AnonymousClass0V7 A00(int i) {
        AnonymousClass0V7 r6 = null;
        AnonymousClass0V7 r4 = null;
        for (AnonymousClass0V7 r3 : values()) {
            int i2 = r3.mAndroidThreadPriority;
            if (i2 >= i) {
                boolean z = true;
                if (r6 != null && r6.mAndroidThreadPriority <= i2) {
                    z = false;
                }
                if (z) {
                    r6 = r3;
                }
            }
            boolean z2 = true;
            if (r4 != null && i2 <= r4.mAndroidThreadPriority) {
                z2 = false;
            }
            if (z2) {
                r4 = r3;
            }
        }
        if (r6 != null) {
            return r6;
        }
        if (r4 != null) {
            return r4;
        }
        throw new IllegalStateException();
    }
}
