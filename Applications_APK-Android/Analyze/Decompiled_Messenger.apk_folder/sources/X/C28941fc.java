package X;

import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.1fc  reason: invalid class name and case insensitive filesystem */
public final class C28941fc extends C14680to {
    public final float _value;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return Float.compare(this._value, ((C28941fc) obj)._value) == 0;
    }

    public String asText() {
        return Double.toString((double) this._value);
    }

    public C182811d asToken() {
        return C182811d.VALUE_NUMBER_FLOAT;
    }

    public boolean canConvertToInt() {
        float f = this._value;
        if (f < -2.14748365E9f || f > 2.14748365E9f) {
            return false;
        }
        return true;
    }

    public BigDecimal decimalValue() {
        return BigDecimal.valueOf((double) this._value);
    }

    public double doubleValue() {
        return (double) this._value;
    }

    public float floatValue() {
        return this._value;
    }

    public int hashCode() {
        return Float.floatToIntBits(this._value);
    }

    public int intValue() {
        return (int) this._value;
    }

    public long longValue() {
        return (long) this._value;
    }

    public C29501gW numberType() {
        return C29501gW.FLOAT;
    }

    public Number numberValue() {
        return Float.valueOf(this._value);
    }

    public final void serialize(C11710np r2, C11260mU r3) {
        r2.writeNumber(this._value);
    }

    public C28941fc(float f) {
        this._value = f;
    }

    public BigInteger bigIntegerValue() {
        return decimalValue().toBigInteger();
    }
}
