package X;

/* renamed from: X.0th  reason: invalid class name and case insensitive filesystem */
public final class C14620th {
    public static AnonymousClass217 A00(Object obj) {
        String replaceAll = obj.getClass().getName().replaceAll("\\$[0-9]+", "\\$");
        int lastIndexOf = replaceAll.lastIndexOf(36);
        if (lastIndexOf == -1) {
            lastIndexOf = replaceAll.lastIndexOf(46);
        }
        return new AnonymousClass217(replaceAll.substring(lastIndexOf + 1));
    }

    public static boolean A01(Object obj, Object obj2) {
        if (obj == obj2) {
            return true;
        }
        if (obj == null || !obj.equals(obj2)) {
            return false;
        }
        return true;
    }
}
