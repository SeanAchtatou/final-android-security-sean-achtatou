package X;

import android.os.Handler;

/* renamed from: X.1Iy  reason: invalid class name and case insensitive filesystem */
public final class C21851Iy {
    public static volatile C27421dA A00;

    public static C31551js A00(C31551js r3) {
        if (A00 == null || (r3 instanceof C27441dC)) {
            return r3;
        }
        if (r3 instanceof Handler) {
            return new C31561jt((Handler) r3, "Litho_", 1);
        }
        return new C35571rR(r3, "Litho_");
    }
}
