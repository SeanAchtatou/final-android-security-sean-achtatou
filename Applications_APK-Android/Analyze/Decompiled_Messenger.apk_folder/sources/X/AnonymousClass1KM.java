package X;

/* renamed from: X.1KM  reason: invalid class name */
public final class AnonymousClass1KM extends C17770zR {
    public int A00 = -1;
    public C78893ph A01;
    public final C37481vk A02;

    public AnonymousClass1KM(C37481vk r3, String str) {
        super(AnonymousClass08S.A0J("ViewCompatComponent_", str), System.identityHashCode(r3));
        this.A02 = r3;
    }

    public int A0M() {
        int i = this.A00;
        if (i == -1) {
            return super.A0M();
        }
        return i;
    }
}
