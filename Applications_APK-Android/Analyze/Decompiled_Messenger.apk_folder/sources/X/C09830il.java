package X;

import java.io.Closeable;

/* renamed from: X.0il  reason: invalid class name and case insensitive filesystem */
public interface C09830il extends Closeable {
    public static final C09830il A00 = new C09840im();

    void close();
}
