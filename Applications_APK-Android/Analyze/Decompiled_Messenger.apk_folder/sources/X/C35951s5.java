package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1s5  reason: invalid class name and case insensitive filesystem */
public final class C35951s5 extends Enum {
    private static final /* synthetic */ C35951s5[] A00;
    public static final C35951s5 A01;
    public static final C35951s5 A02;
    public static final C35951s5 A03;
    public static final C35951s5 A04;
    public static final C35951s5 A05;

    static {
        C35951s5 r2 = new C35951s5("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A05 = r2;
        C35951s5 r3 = new C35951s5("MESSENGER_DESTINATION", 1);
        A01 = r3;
        C35951s5 r4 = new C35951s5("NEKO", 2);
        C35951s5 r5 = new C35951s5("PHOTO", 3);
        A04 = r5;
        C35951s5 r6 = new C35951s5("MULTI_PHOTO", 4);
        A02 = r6;
        C35951s5 r7 = new C35951s5("NO_CTA", 5);
        A03 = r7;
        A00 = new C35951s5[]{r2, r3, r4, r5, r6, r7};
    }

    public static C35951s5 valueOf(String str) {
        return (C35951s5) Enum.valueOf(C35951s5.class, str);
    }

    public static C35951s5[] values() {
        return (C35951s5[]) A00.clone();
    }

    private C35951s5(String str, int i) {
    }
}
