package X;

import java.lang.reflect.Method;

/* renamed from: X.00g  reason: invalid class name and case insensitive filesystem */
public final class C000200g {
    public final long A00;
    public final Method A01;
    public final Method A02;

    public C000200g(Method method, Method method2, long j) {
        this.A01 = method;
        this.A02 = method2;
        this.A00 = j;
    }
}
