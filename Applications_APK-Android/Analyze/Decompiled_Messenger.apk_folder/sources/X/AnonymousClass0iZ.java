package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0iZ  reason: invalid class name */
public final class AnonymousClass0iZ {
    private static volatile AnonymousClass0iZ A03;
    public AnonymousClass0UN A00;
    public final AnonymousClass0ia A01;
    public volatile boolean A02;

    public static final AnonymousClass0iZ A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass0iZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass0iZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private AnonymousClass0iZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = new AnonymousClass0ia(r3);
    }
}
