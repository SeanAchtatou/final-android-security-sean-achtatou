package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector;

/* renamed from: X.0GB  reason: invalid class name */
public final class AnonymousClass0GB implements AnonymousClass06U {
    public final /* synthetic */ SkywalkerSubscriptionConnector A00;

    public AnonymousClass0GB(SkywalkerSubscriptionConnector skywalkerSubscriptionConnector) {
        this.A00 = skywalkerSubscriptionConnector;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r5) {
        int A002 = AnonymousClass09Y.A00(-823453021);
        SkywalkerSubscriptionConnector.A06(this.A00, intent);
        AnonymousClass09Y.A01(-607999304, A002);
    }
}
