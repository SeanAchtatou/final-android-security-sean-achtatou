package X;

import com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet;
import com.google.common.base.Preconditions;

/* renamed from: X.0sE  reason: invalid class name and case insensitive filesystem */
public final class C13880sE {
    public static final C13880sE A07 = new C13880sE(null, C13890sF.A03, C15820w2.A04, null, C34801qC.A03, null, C13890sF.A03);
    public final C34801qC A00;
    public final C15820w2 A01;
    public final AnonymousClass101 A02;
    public final AnonymousClass101 A03;
    public final MessageRequestsSnippet A04;
    public final C13890sF A05;
    public final C13890sF A06;

    public C13880sE(AnonymousClass101 r1, C13890sF r2, C15820w2 r3, MessageRequestsSnippet messageRequestsSnippet, C34801qC r5, AnonymousClass101 r6, C13890sF r7) {
        this.A03 = r1;
        Preconditions.checkNotNull(r2);
        this.A06 = r2;
        Preconditions.checkNotNull(r3);
        this.A01 = r3;
        this.A04 = messageRequestsSnippet;
        this.A00 = r5 == null ? C34801qC.A03 : r5;
        this.A02 = r6;
        this.A05 = r7;
    }
}
