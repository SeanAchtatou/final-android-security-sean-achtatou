package X;

import com.facebook.messaging.analytics.perf.PostStartupTracker;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.0gH  reason: invalid class name and case insensitive filesystem */
public final class C08970gH implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.analytics.perf.MessagingPerformanceFunnelLogger$1";
    public final /* synthetic */ int A00;
    public final /* synthetic */ C08860g6 A01;

    public C08970gH(C08860g6 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    public void run() {
        PostStartupTracker postStartupTracker = (PostStartupTracker) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BCF, this.A01.A00);
        int i = this.A00;
        if (postStartupTracker.A00 != AnonymousClass07L.A03) {
            postStartupTracker.A02.removeMessages(1);
            C08990gJ r1 = postStartupTracker.A02;
            r1.sendMessageAtFrontOfQueue(r1.obtainMessage(1));
        } else if (((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, postStartupTracker.A01)).isMarkerOn(5505205)) {
            return;
        }
        postStartupTracker.A00 = AnonymousClass07L.A03;
        C08990gJ r12 = postStartupTracker.A02;
        r12.sendMessage(r12.obtainMessage(0, 0, i));
    }
}
