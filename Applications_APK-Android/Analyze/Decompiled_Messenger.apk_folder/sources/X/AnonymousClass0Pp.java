package X;

import java.net.Socket;
import java.security.cert.Certificate;
import java.util.concurrent.ExecutorService;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: X.0Pp  reason: invalid class name */
public final class AnonymousClass0Pp extends AnonymousClass0SS {
    private final AnonymousClass0BC A00;
    private final SSLSocketFactory A01;

    public AnonymousClass0Pp(ExecutorService executorService, SSLSocketFactory sSLSocketFactory, AnonymousClass0BC r3) {
        super(executorService);
        this.A01 = sSLSocketFactory;
        this.A00 = r3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    public Socket A00(Socket socket, String str, int i) {
        String str2;
        int length;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        AnonymousClass0A1.A01(socket.isConnected());
        String str8 = str;
        SSLSocket sSLSocket = (SSLSocket) this.A01.createSocket(socket, str, i, true);
        AnonymousClass0BC r3 = this.A00;
        sSLSocket.getInputStream();
        SSLSession session = sSLSocket.getSession();
        if (session == null) {
            throw new SSLException("SSL Session is null");
        } else if ("SSL_NULL_WITH_NULL_NULL".equals(session.getCipherSuite())) {
            if (sSLSocket.isClosed()) {
                str3 = "closed";
            } else {
                str3 = "open";
            }
            if (sSLSocket.isConnected()) {
                str4 = "connected";
            } else {
                str4 = "disconnected";
            }
            if (sSLSocket.isBound()) {
                str5 = "bound";
            } else {
                str5 = "unbound";
            }
            if (sSLSocket.isInputShutdown()) {
                str6 = "input_shutdown";
            } else {
                str6 = "input_open";
            }
            if (sSLSocket.isOutputShutdown()) {
                str7 = "output_shutdown";
            } else {
                str7 = "output_open";
            }
            throw new SSLException(String.format(null, "SSL handshake returned an invalid session. Socket state (%s, %s, %s, %s, %s, %s, %s)", str3, str4, str5, str6, str7, str8, String.valueOf(sSLSocket.getInetAddress())));
        } else if (r3.A00.verify(str, session)) {
            return sSLSocket;
        } else {
            String inetAddress = sSLSocket.getInetAddress().toString();
            try {
                Certificate[] peerCertificates = session.getPeerCertificates();
                if (peerCertificates == null || (length = peerCertificates.length) <= 0) {
                    str2 = "No certificates";
                } else {
                    str2 = String.format(null, "num: %d, %s", Integer.valueOf(length), peerCertificates[0].toString());
                }
            } catch (SSLException e) {
                str2 = AnonymousClass08S.A0J("Exception getting certificates ", e.toString());
            }
            throw new SSLException(String.format(null, "could not verify hostname for (%s, %s). (%s)", str, inetAddress, str2));
        }
    }
}
