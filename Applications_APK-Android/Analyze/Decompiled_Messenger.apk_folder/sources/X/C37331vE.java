package X;

import android.content.Context;
import com.facebook.ipc.stories.model.AudienceControlData;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1vE  reason: invalid class name and case insensitive filesystem */
public final class C37331vE extends C17770zR {
    public AnonymousClass303 A00;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public C73513gF A02;
    @Comparable(type = 13)
    public AudienceControlData A03;
    @Comparable(type = 13)
    public StoryCard A04;
    @Comparable(type = 13)
    public AnonymousClass4O9 A05;
    @Comparable(type = 13)
    public String A06;

    public int A0M() {
        return 3;
    }

    public C37331vE(Context context) {
        super("StoryViewerContributionStickerComponent");
        this.A01 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }

    public C17770zR A16() {
        C37331vE r1 = (C37331vE) super.A16();
        r1.A00 = null;
        return r1;
    }
}
