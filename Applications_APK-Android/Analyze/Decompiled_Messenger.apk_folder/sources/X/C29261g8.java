package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.ThreadRtcCallInfoData;

/* renamed from: X.1g8  reason: invalid class name and case insensitive filesystem */
public final class C29261g8 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadRtcCallInfoData(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadRtcCallInfoData[i];
    }
}
