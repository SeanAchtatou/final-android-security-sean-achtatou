package X;

import android.content.Context;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.ContextChain;
import com.facebook.graphql.enums.GraphQLObjectionableContentCategory;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.montage.forked.model.hcontroller.ControllerParams;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.mig.scheme.schemes.DarkColorScheme;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1vP  reason: invalid class name */
public final class AnonymousClass1vP extends C17770zR {
    public static final CallerContext A03 = CallerContext.A01(CallerContext.A0B("MontageStoryViewerWarningScreenController", AnonymousClass24B.$const$string(286)), new ContextChain("i", "montage_story_viewer_warning_screen_controller", AnonymousClass403.A02));
    public static final MigColorScheme A04 = DarkColorScheme.A00();
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public ControllerParams A01;
    @Comparable(type = 13)
    public MigColorScheme A02 = A04;

    public AnonymousClass1vP(Context context) {
        super("MontageStoryViewerWarningScreenComponent");
        this.A00 = new AnonymousClass0UN(4, AnonymousClass1XX.get(context));
    }

    public static String A00(StoryCard storyCard) {
        ImmutableList A0N;
        AnonymousClass4BB A0Q = storyCard.A0Q();
        if (A0Q == null || (A0N = A0Q.A0N(1296516636, GraphQLObjectionableContentCategory.A01)) == null || A0N.isEmpty()) {
            return null;
        }
        return ((GraphQLObjectionableContentCategory) A0N.get(0)).name();
    }
}
