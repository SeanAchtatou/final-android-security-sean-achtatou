package X;

/* renamed from: X.0bd  reason: invalid class name and case insensitive filesystem */
public final class C06520bd implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.quicklog.module.QPLConfigManager$1";
    public final /* synthetic */ C06510bc A00;

    public C06520bd(C06510bc r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x00e8 */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r9 = this;
            X.0bc r0 = r9.A00
            java.util.concurrent.atomic.AtomicReference r1 = r0.A03
            X.0US r0 = r0.A00
            java.lang.Object r5 = r0.get()
            X.0YL r5 = (X.AnonymousClass0YL) r5
            X.0bc r0 = r9.A00
            X.0dw r0 = r0.A01
            X.0US r0 = r0.A00
            java.lang.Object r2 = r0.get()
            X.0XN r2 = (X.AnonymousClass0XN) r2
            r0 = 0
            if (r2 == 0) goto L_0x0023
            com.facebook.auth.viewercontext.ViewerContext r2 = r2.A08()
            if (r2 == 0) goto L_0x0023
            java.lang.String r0 = r2.mUserId
        L_0x0023:
            java.lang.String r4 = "QPLConfig"
            if (r0 != 0) goto L_0x002a
            r8 = 0
        L_0x0028:
            r3 = 0
            goto L_0x003f
        L_0x002a:
            X.0cj r2 = X.C07810eC.A00
            java.nio.charset.Charset r3 = com.google.common.base.Charsets.UTF_8
            X.0co r2 = r2.A01()
            X.0co r0 = r2.A05(r0, r3)
            X.0cq r0 = r0.A09()
            java.lang.String r8 = r0.toString()
            goto L_0x0028
        L_0x003f:
            android.content.Context r5 = r5.A00     // Catch:{ IOException -> 0x00e9 }
            java.lang.String r2 = "qpl"
            r0 = 0
            java.io.File r7 = r5.getDir(r2, r0)     // Catch:{ IOException -> 0x00e9 }
            java.io.File r6 = new java.io.File     // Catch:{ IOException -> 0x00e9 }
            java.util.Locale r5 = java.util.Locale.US     // Catch:{ IOException -> 0x00e9 }
            java.lang.Object[] r2 = new java.lang.Object[]{r8}     // Catch:{ IOException -> 0x00e9 }
            java.lang.String r0 = "qpl_sampling_config_v2.%s"
            java.lang.String r0 = java.lang.String.format(r5, r0, r2)     // Catch:{ IOException -> 0x00e9 }
            r6.<init>(r7, r0)     // Catch:{ IOException -> 0x00e9 }
            boolean r0 = r6.exists()     // Catch:{ IOException -> 0x00e9 }
            if (r0 != 0) goto L_0x006a
            java.io.File r6 = new java.io.File     // Catch:{ IOException -> 0x00e9 }
            r0 = 581(0x245, float:8.14E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ IOException -> 0x00e9 }
            r6.<init>(r7, r0)     // Catch:{ IOException -> 0x00e9 }
        L_0x006a:
            java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x00e9 }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00e9 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00e9 }
            r0.<init>(r6)     // Catch:{ IOException -> 0x00e9 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00e9 }
            r5.<init>(r2)     // Catch:{ IOException -> 0x00e9 }
            java.lang.Object r0 = r1.get()     // Catch:{ all -> 0x00e2 }
            if (r0 != 0) goto L_0x00dd
            int r2 = r5.readInt()     // Catch:{ all -> 0x00e2 }
            r0 = 1
            if (r2 == r0) goto L_0x0094
            java.lang.String r1 = "unsupported config version %d"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x00e2 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00e2 }
            X.C010708t.A0O(r4, r1, r0)     // Catch:{ all -> 0x00e2 }
            goto L_0x00dd
        L_0x0094:
            java.lang.String r7 = r5.readUTF()     // Catch:{ all -> 0x00e2 }
            java.lang.Object r0 = r1.get()     // Catch:{ all -> 0x00e2 }
            if (r0 != 0) goto L_0x00dd
            X.1Zr r6 = X.C25451Zr.A01(r5, r1)     // Catch:{ all -> 0x00e2 }
            if (r6 == 0) goto L_0x00dd
            java.lang.Object r0 = r1.get()     // Catch:{ all -> 0x00e2 }
            if (r0 != 0) goto L_0x00dd
            byte r2 = r5.readByte()     // Catch:{ all -> 0x00e2 }
            r0 = 1
            if (r2 != r0) goto L_0x00bd
            X.1Zr r0 = X.C25451Zr.A01(r5, r1)     // Catch:{ all -> 0x00e2 }
            if (r0 == 0) goto L_0x00cb
            X.0cs r2 = new X.0cs     // Catch:{ all -> 0x00e2 }
            r2.<init>(r0)     // Catch:{ all -> 0x00e2 }
            goto L_0x00cc
        L_0x00bd:
            if (r2 == 0) goto L_0x00cb
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = "We do not support metadata type "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r2)     // Catch:{ all -> 0x00e2 }
            r1.<init>(r0)     // Catch:{ all -> 0x00e2 }
            throw r1     // Catch:{ all -> 0x00e2 }
        L_0x00cb:
            r2 = r3
        L_0x00cc:
            if (r2 == 0) goto L_0x00dd
            java.lang.Object r0 = r1.get()     // Catch:{ all -> 0x00e2 }
            if (r0 != 0) goto L_0x00dd
            X.0YP r1 = new X.0YP     // Catch:{ all -> 0x00e2 }
            r1.<init>(r8, r6, r2, r7)     // Catch:{ all -> 0x00e2 }
            r5.close()     // Catch:{ IOException -> 0x00e9 }
            goto L_0x00f0
        L_0x00dd:
            r5.close()     // Catch:{ IOException -> 0x00e9 }
            r1 = r3
            goto L_0x00f0
        L_0x00e2:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00e4 }
        L_0x00e4:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x00e8 }
        L_0x00e8:
            throw r0     // Catch:{ IOException -> 0x00e9 }
        L_0x00e9:
            r1 = move-exception
            java.lang.String r0 = "failed to load config"
            X.C010708t.A0M(r4, r0, r1)
            r1 = r3
        L_0x00f0:
            if (r1 == 0) goto L_0x00f9
            X.0bc r0 = r9.A00
            java.util.concurrent.atomic.AtomicReference r0 = r0.A03
            r0.compareAndSet(r3, r1)
        L_0x00f9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06520bd.run():void");
    }
}
