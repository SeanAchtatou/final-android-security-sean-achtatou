package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.facebook.fbservice.service.IBlueService;
import com.facebook.fbservice.service.OperationResult;

/* renamed from: X.1co  reason: invalid class name and case insensitive filesystem */
public final class C27201co implements ServiceConnection {
    public final /* synthetic */ C27171cl A00;

    public C27201co(C27171cl r1) {
        this.A00 = r1;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        C27171cl r2 = this.A00;
        IBlueService A02 = IBlueService.Stub.A02(iBinder);
        if (!r2.BEO()) {
            r2.A07 = A02;
            C27171cl.A01(r2);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        C27171cl r2 = this.A00;
        r2.A07 = null;
        if (r2.A04 == C27181cm.OPERATION_QUEUED) {
            r2.A05(OperationResult.A02(C14880uI.A0C, "BlueService disconnected"));
        }
    }
}
