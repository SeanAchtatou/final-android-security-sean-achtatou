package X;

import android.content.Context;
import android.text.Layout;
import android.text.TextUtils;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import io.card.payment.BuildConfig;
import java.util.BitSet;

/* renamed from: X.11H  reason: invalid class name */
public final class AnonymousClass11H extends C17770zR {
    @Comparable(type = 13)
    public C25998CqM A00;
    @Comparable(type = 13)
    public C134406Qm A01;
    public AnonymousClass0UN A02;
    @Comparable(type = 13)
    public C203459id A03;
    @Comparable(type = 14)
    public AnonymousClass5CK A04 = new AnonymousClass5CK();
    @Comparable(type = 13)
    public C203729j9 A05;
    @Comparable(type = 13)
    public C121605oW A06;
    @Comparable(type = 13)
    public MigColorScheme A07;
    @Comparable(type = 13)
    public String A08;
    @Comparable(type = 3)
    public boolean A09;
    @Comparable(type = 3)
    public boolean A0A;
    @Comparable(type = 3)
    public boolean A0B;

    public static C17770zR A03(AnonymousClass0p4 r9, C203949jW r10, float f, float f2) {
        String str;
        String[] strArr = {"stateContainer"};
        BitSet bitSet = new BitSet(1);
        AnonymousClass10I r4 = new AnonymousClass10I(r9.A09);
        C17540z4 r7 = r9.A0B;
        C17770zR r2 = r9.A04;
        if (r2 != null) {
            r4.A07 = r2.A06;
        }
        bitSet.clear();
        r4.A05 = r10.A03;
        r4.A04 = r10.A01.A01;
        bitSet.set(0);
        r4.A08 = true;
        String str2 = "phone_number_email_field";
        if ("phone_number_email_field" == 0) {
            if (r2 != null) {
                str = r2.A1A();
            } else {
                str = "unknown component";
            }
            C09070gU.A01(AnonymousClass07B.A01, "Component:NullKeySet", AnonymousClass08S.A0P("Setting a null key from ", str, C99084oO.$const$string(0)));
            str2 = "null";
        }
        r4.A1I(str2);
        r4.A14().A0S("phone_number_email_field");
        r4.A14().A0X(r10.A09);
        r4.A00 = 5;
        r4.A06 = r7.A09(2131826662);
        r4.A14().BJx(AnonymousClass10G.TOP, r7.A00(f));
        r4.A14().BJx(AnonymousClass10G.BOTTOM, r7.A00(f2));
        r4.A02 = C17780zS.A0E(AnonymousClass11H.class, r9, 1874929519, new Object[]{r9});
        AnonymousClass11F.A0C(1, bitSet, strArr);
        return r4;
    }

    public static C17770zR A04(AnonymousClass0p4 r9, C203949jW r10, float f, float f2) {
        String str;
        String[] strArr = {"stateContainer"};
        BitSet bitSet = new BitSet(1);
        C17020yC r8 = new C17020yC();
        C17540z4 r6 = r9.A0B;
        C17770zR r2 = r9.A04;
        if (r2 != null) {
            r8.A07 = r2.A06;
        }
        bitSet.clear();
        r8.A04 = r10.A03;
        r8.A03 = r10.A01.A02;
        bitSet.set(0);
        String str2 = "password_field";
        if ("password_field" == 0) {
            if (r2 != null) {
                str = r2.A1A();
            } else {
                str = "unknown component";
            }
            C09070gU.A01(AnonymousClass07B.A01, "Component:NullKeySet", AnonymousClass08S.A0P("Setting a null key from ", str, C99084oO.$const$string(0)));
            str2 = "null";
        }
        r8.A1I(str2);
        r8.A14().A0S("password_field");
        r8.A05 = r6.A09(2131830050);
        r8.A00 = 6;
        r8.A14().BJx(AnonymousClass10G.TOP, r6.A00(f));
        r8.A14().BJx(AnonymousClass10G.BOTTOM, r6.A00(f2));
        Class<AnonymousClass11H> cls = AnonymousClass11H.class;
        r8.A02 = C17780zS.A0E(cls, r9, 1196116736, new Object[]{r9});
        r8.A01 = C17780zS.A0E(cls, r9, 96515278, new Object[]{r9});
        AnonymousClass11F.A0C(1, bitSet, strArr);
        return r8;
    }

    public AnonymousClass11H(Context context) {
        super("AccountLoginRootComponent");
        this.A02 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }

    public static C17770zR A00(AnonymousClass0p4 r1, C203949jW r2, float f, float f2) {
        if (TextUtils.isEmpty(r2.A04)) {
            return AnonymousClass11D.A00(r1).A00;
        }
        ComponentBuilderCBuilderShape0_0S0300000 A002 = C17030yD.A00(r1);
        A002.A2n(BuildConfig.FLAVOR);
        A002.A3c(r2.A03);
        A002.A3l(r2.A04);
        A002.A2p("error_field");
        A002.A2X(AnonymousClass10G.TOP, f);
        A002.A2X(AnonymousClass10G.BOTTOM, f2);
        A002.A3f(AnonymousClass10J.A0T);
        A002.A3V(Layout.Alignment.ALIGN_NORMAL);
        return A002.A39();
    }

    public static C17770zR A01(AnonymousClass0p4 r6, C203949jW r7, float f, float f2) {
        AnonymousClass5X7 r4 = new AnonymousClass5X7(r6.A09);
        C17540z4 r5 = r6.A0B;
        C17770zR r2 = r6.A04;
        if (r2 != null) {
            r4.A07 = r2.A06;
        }
        r4.A02 = r7.A03;
        r4.A05 = AnonymousClass07B.A0n;
        r4.A03 = r5.A09(2131821048);
        r4.A14().A0S("forgot_password_button");
        r4.A14().BJx(AnonymousClass10G.TOP, r5.A00(f));
        r4.A14().BJx(AnonymousClass10G.BOTTOM, r5.A00(f2));
        r4.A14().A0G(C17780zS.A0E(AnonymousClass11H.class, r6, 1415173789, new Object[]{r6}));
        return r4;
    }

    public static C17770zR A02(AnonymousClass0p4 r3, C203949jW r4, float f, float f2) {
        C16930y3 B5d = AnonymousClass10J.A03.B5d(r4.A03);
        C37941wd A002 = C16980y8.A00(r3);
        A002.A27(1);
        A002.A2A(1);
        A002.A1n(1.0f);
        A002.A23(B5d.AhV());
        A002.A2X(AnonymousClass10G.TOP, f);
        A002.A2X(AnonymousClass10G.BOTTOM, f2);
        return A002.A00;
    }

    public static C17770zR A0D(AnonymousClass0p4 r6, C203949jW r7, float f, float f2) {
        if (!r7.A0A) {
            return AnonymousClass11D.A00(r6).A00;
        }
        C37941wd A002 = C16980y8.A00(r6);
        C126715xM r4 = new C126715xM();
        C17540z4 r5 = r6.A0B;
        C17770zR r1 = r6.A04;
        if (r1 != null) {
            r4.A07 = r1.A06;
        }
        r4.A02 = r7.A03;
        r4.A14().A0S("save_password_checkbox");
        r4.A14().A0G(C17780zS.A0E(AnonymousClass11H.class, r6, -952092468, new Object[]{r6}));
        r4.A03 = !r7.A01.A00;
        r4.A14().AaD(0.0f);
        r4.A14().BJx(AnonymousClass10G.RIGHT, r5.A00((float) AnonymousClass1JQ.SMALL.B3A()));
        A002.A3A(r4);
        ComponentBuilderCBuilderShape0_0S0300000 A003 = C17030yD.A00(r6);
        A003.A3c(r7.A03);
        A003.A3P(2131821111);
        A003.A3f(r7.A02.A0E());
        A002.A3A(A003.A39());
        A002.A2X(AnonymousClass10G.TOP, f);
        A002.A2X(AnonymousClass10G.BOTTOM, f2);
        A002.A3D(C14940uO.CENTER);
        return A002.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0047, code lost:
        if (r8.A05 != false) goto L_0x0049;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C17770zR A0I(X.AnonymousClass0p4 r7, X.C203949jW r8, java.lang.Integer r9, float r10, float r11) {
        /*
            X.5X7 r4 = new X.5X7
            android.content.Context r0 = r7.A09
            r4.<init>(r0)
            X.0z4 r5 = r7.A0B
            X.0zR r2 = r7.A04
            if (r2 == 0) goto L_0x0011
            java.lang.String r1 = r2.A06
            r4.A07 = r1
        L_0x0011:
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r8.A03
            r4.A02 = r1
            r4.A05 = r9
            r1 = 2131821050(0x7f1101fa, float:1.9274832E38)
            java.lang.String r1 = r5.A09(r1)
            r4.A03 = r1
            X.9j9 r1 = r8.A01
            X.2Nf r1 = r1.A02
            java.lang.String r1 = r1.A00
            java.lang.String r1 = r1.trim()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x0049
            X.9j9 r1 = r8.A01
            X.9gP r1 = r1.A01
            java.lang.String r1 = r1.A00
            java.lang.String r1 = r1.trim()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x0049
            boolean r1 = r8.A06
            if (r1 != 0) goto L_0x0049
            boolean r2 = r8.A05
            r1 = 1
            if (r2 == 0) goto L_0x004a
        L_0x0049:
            r1 = 0
        L_0x004a:
            r4.A06 = r1
            java.lang.String r6 = "login_button"
            r1 = r6
            if (r6 != 0) goto L_0x006d
            X.0zR r1 = r7.A04
            if (r1 == 0) goto L_0x00a6
            java.lang.String r3 = r1.A1A()
        L_0x0059:
            java.lang.String r2 = "Setting a null key from "
            r1 = 0
            java.lang.String r1 = X.C99084oO.$const$string(r1)
            java.lang.String r3 = X.AnonymousClass08S.A0P(r2, r3, r1)
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            java.lang.String r1 = "Component:NullKeySet"
            X.C09070gU.A01(r2, r1, r3)
            java.lang.String r1 = "null"
        L_0x006d:
            r4.A1I(r1)
            X.11G r1 = r4.A14()
            r1.A0S(r6)
            X.10G r3 = X.AnonymousClass10G.TOP
            int r2 = r5.A00(r10)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            X.10G r3 = X.AnonymousClass10G.BOTTOM
            int r2 = r5.A00(r11)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            java.lang.Class<X.11H> r3 = X.AnonymousClass11H.class
            java.lang.Object[] r2 = new java.lang.Object[]{r7}
            r1 = -855949748(0xffffffffccfb3e4c, float:-1.31723872E8)
            X.10N r1 = X.C17780zS.A0E(r3, r7, r1, r2)
            X.11G r0 = r4.A14()
            r0.A0G(r1)
            return r4
        L_0x00a6:
            java.lang.String r3 = "unknown component"
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass11H.A0I(X.0p4, X.9jW, java.lang.Integer, float, float):X.0zR");
    }

    public static C17770zR A0J(AnonymousClass0p4 r6, C203949jW r7, Integer num, float f, float f2, boolean z) {
        AnonymousClass5X7 r4 = new AnonymousClass5X7(r6.A09);
        C17540z4 r5 = r6.A0B;
        C17770zR r2 = r6.A04;
        if (r2 != null) {
            r4.A07 = r2.A06;
        }
        r4.A02 = r7.A03;
        r4.A05 = num;
        int i = 2131821045;
        if (z) {
            i = 2131821047;
        }
        r4.A03 = r5.A09(i);
        r4.A14().A0S("create_account_button");
        r4.A14().BJx(AnonymousClass10G.TOP, r5.A00(f));
        r4.A14().BJx(AnonymousClass10G.BOTTOM, r5.A00(f2));
        r4.A14().A0G(C17780zS.A0E(AnonymousClass11H.class, r6, 1782726174, new Object[]{r6}));
        return r4;
    }

    public static void A0K(AnonymousClass0p4 r3) {
        if (r3.A04 != null) {
            r3.A0G(new C61322yh(0, new Object[0]), "updateState:AccountLoginRootComponent.increaseUiVersion");
        }
    }

    public C17770zR A16() {
        AnonymousClass11H r1 = (AnonymousClass11H) super.A16();
        r1.A04 = new AnonymousClass5CK();
        return r1;
    }
}
