package X;

import android.util.SparseIntArray;

/* renamed from: X.1N7  reason: invalid class name */
public final class AnonymousClass1N7 {
    public boolean A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final SparseIntArray A04;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
        if (r3 < r2) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1N7(int r2, int r3, android.util.SparseIntArray r4, int r5) {
        /*
            r1 = this;
            r1.<init>()
            if (r2 < 0) goto L_0x0008
            r0 = 1
            if (r3 >= r2) goto L_0x0009
        L_0x0008:
            r0 = 0
        L_0x0009:
            X.C05520Zg.A05(r0)
            r1.A03 = r2
            r1.A02 = r3
            r1.A04 = r4
            r1.A01 = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1N7.<init>(int, int, android.util.SparseIntArray, int):void");
    }
}
