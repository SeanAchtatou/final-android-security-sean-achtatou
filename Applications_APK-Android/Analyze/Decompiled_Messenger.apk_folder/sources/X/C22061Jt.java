package X;

import java.util.AbstractCollection;
import java.util.Iterator;

/* renamed from: X.1Jt  reason: invalid class name and case insensitive filesystem */
public final class C22061Jt extends AbstractCollection<V> {
    public final /* synthetic */ C22141Kb A00;

    public C22061Jt(C22141Kb r1) {
        this.A00 = r1;
    }

    public void clear() {
        this.A00.clear();
    }

    public boolean contains(Object obj) {
        return this.A00.containsValue(obj);
    }

    public boolean isEmpty() {
        return this.A00.isEmpty();
    }

    public Iterator iterator() {
        return new C32791mL(this.A00);
    }

    public int size() {
        return this.A00.size();
    }
}
