package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0WT  reason: invalid class name */
public final class AnonymousClass0WT extends AnonymousClass0UV {
    public static final C25051Yd A00(AnonymousClass1XY r0) {
        return AnonymousClass0WU.A01(r0);
    }

    public static final C25051Yd A01(AnonymousClass1XY r0) {
        return AnonymousClass0WU.A02(r0);
    }
}
