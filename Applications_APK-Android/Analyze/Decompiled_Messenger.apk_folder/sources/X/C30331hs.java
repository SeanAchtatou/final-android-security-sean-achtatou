package X;

import com.facebook.messaging.notify.MessageReactionNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1hs  reason: invalid class name and case insensitive filesystem */
public final class C30331hs extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$25";
    public final /* synthetic */ MessageReactionNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C30331hs(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, MessageReactionNotification messageReactionNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = messageReactionNotification;
    }
}
