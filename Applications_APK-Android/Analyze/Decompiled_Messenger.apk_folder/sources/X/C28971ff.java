package X;

import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.1ff  reason: invalid class name and case insensitive filesystem */
public final class C28971ff extends C14680to {
    private static final BigInteger MAX_INTEGER = BigInteger.valueOf(2147483647L);
    private static final BigInteger MAX_LONG = BigInteger.valueOf(Long.MAX_VALUE);
    private static final BigInteger MIN_INTEGER = BigInteger.valueOf(-2147483648L);
    private static final BigInteger MIN_LONG = BigInteger.valueOf(Long.MIN_VALUE);
    public final BigInteger _value;

    public boolean asBoolean(boolean z) {
        return !BigInteger.ZERO.equals(this._value);
    }

    public String asText() {
        return this._value.toString();
    }

    public C182811d asToken() {
        return C182811d.VALUE_NUMBER_INT;
    }

    public BigInteger bigIntegerValue() {
        return this._value;
    }

    public boolean canConvertToInt() {
        if (this._value.compareTo(MIN_INTEGER) < 0 || this._value.compareTo(MAX_INTEGER) > 0) {
            return false;
        }
        return true;
    }

    public BigDecimal decimalValue() {
        return new BigDecimal(this._value);
    }

    public double doubleValue() {
        return this._value.doubleValue();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return ((C28971ff) obj)._value.equals(this._value);
    }

    public float floatValue() {
        return this._value.floatValue();
    }

    public int hashCode() {
        return this._value.hashCode();
    }

    public int intValue() {
        return this._value.intValue();
    }

    public long longValue() {
        return this._value.longValue();
    }

    public C29501gW numberType() {
        return C29501gW.BIG_INTEGER;
    }

    public Number numberValue() {
        return this._value;
    }

    public final void serialize(C11710np r2, C11260mU r3) {
        r2.writeNumber(this._value);
    }

    public C28971ff(BigInteger bigInteger) {
        this._value = bigInteger;
    }
}
