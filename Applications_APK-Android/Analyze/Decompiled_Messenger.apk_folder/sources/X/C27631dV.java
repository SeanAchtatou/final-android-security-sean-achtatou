package X;

import android.os.Bundle;

/* renamed from: X.1dV  reason: invalid class name and case insensitive filesystem */
public final class C27631dV {
    public boolean A00 = true;
    public Bundle A01;
    public AnonymousClass1XK A02 = new AnonymousClass1XK();
    public boolean A03;
    private E46 A04;

    public void A00(Class cls) {
        if (this.A00) {
            if (this.A04 == null) {
                this.A04 = new E46(this);
            }
            try {
                cls.getDeclaredConstructor(new Class[0]);
                E46 e46 = this.A04;
                e46.A00.add(cls.getName());
            } catch (NoSuchMethodException e) {
                throw new IllegalArgumentException(AnonymousClass08S.A0P("Class", cls.getSimpleName(), " must have default constructor in order to be automatically recreated"), e);
            }
        } else {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }
}
