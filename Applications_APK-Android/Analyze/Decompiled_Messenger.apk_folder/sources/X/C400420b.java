package X;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.connectionstatus.FbDataConnectionManager;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.common.util.TriState;
import com.facebook.graphql.enums.GraphQLVideoBroadcastStatus;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.video.engine.api.VideoDataSource;
import com.facebook.video.engine.api.VideoPlayerParams;
import com.facebook.video.heroplayer.ipc.ParcelableFormat;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.base.Optional;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import io.card.payment.BuildConfig;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import javax.inject.Singleton;
import org.json.JSONArray;

@Singleton
/* renamed from: X.20b  reason: invalid class name and case insensitive filesystem */
public final class C400420b {
    public static Set A0N;
    public static final Set A0O = ImmutableSet.A09(C178548Ls.A00(AnonymousClass07B.A0D), C178548Ls.A00(AnonymousClass07B.A01), C178548Ls.A00(AnonymousClass07B.A0C), C178548Ls.A00(AnonymousClass07B.A0N), C178548Ls.A00(AnonymousClass07B.A0i), C178548Ls.A00(AnonymousClass07B.A0F), C178548Ls.A00(AnonymousClass07B.A0G), C178548Ls.A00(AnonymousClass07B.A0Y), C178548Ls.A00(AnonymousClass07B.A0o), C178548Ls.A00(AnonymousClass07B.A07), C178548Ls.A00(AnonymousClass07B.A0V), C178548Ls.A00(AnonymousClass07B.A0U));
    public static final Set A0P;
    private static volatile C400420b A0Q;
    public AnonymousClass0UN A00;
    public Integer A01;
    public boolean A02 = false;
    public final int A03;
    public final int A04;
    public final AnonymousClass09P A05;
    public final C29761gw A06;
    public final C29761gw A07;
    public final C29761gw A08;
    public final C29761gw A09;
    public final AnonymousClass1YI A0A;
    public final AnonymousClass0US A0B;
    public final Set A0C;
    public final AtomicInteger A0D = new AtomicInteger(0);
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;
    public final boolean A0I;
    public final boolean A0J;
    public final boolean A0K;
    private final C29761gw A0L;
    private final boolean A0M;

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006c, code lost:
        if (r4 >= r14.A03) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d5, code lost:
        if (((X.C25051Yd) X.AnonymousClass1XX.A02(1, X.AnonymousClass1Y3.AOJ, r5.A03.A00)).Aem(283626763783460L) == false) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f2, code lost:
        if (r5.A05.get(r4) == null) goto L_0x00f4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x020b  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x02c4  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x02e1  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0314  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x037d  */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x0419  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0456  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0499 A[Catch:{ Exception -> 0x04b7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:209:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:210:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:211:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0071  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0G(X.C400420b r14, X.C11670nb r15, boolean r16) {
        /*
            r0 = r15
            com.google.common.base.Preconditions.checkNotNull(r15)
            java.lang.String r1 = "video_id"
            java.lang.String r7 = r15.A07(r1)
            java.lang.String r1 = "player_origin"
            java.lang.String r8 = r15.A07(r1)
            java.lang.String r1 = "player_suborigin"
            java.lang.String r3 = r15.A07(r1)
            java.lang.String r1 = "debug_reason"
            java.lang.String r6 = r15.A07(r1)
            java.lang.String r5 = r15.A05
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r7)
            r1 = r14
            if (r2 != 0) goto L_0x004a
            X.1gw r2 = r14.A09
            java.lang.Object r2 = r2.A03(r7)
            java.util.concurrent.atomic.AtomicInteger r2 = (java.util.concurrent.atomic.AtomicInteger) r2
            if (r2 == 0) goto L_0x004a
            int r4 = r2.intValue()
            r2 = 200(0xc8, float:2.8E-43)
            if (r4 >= r2) goto L_0x006e
            boolean r2 = A0L(r14, r8, r3)
            if (r2 == 0) goto L_0x004a
            java.util.Set r3 = r14.A0C
            monitor-enter(r3)
            java.util.Set r2 = r14.A0C     // Catch:{ all -> 0x0047 }
            r2.add(r7)     // Catch:{ all -> 0x0047 }
            monitor-exit(r3)     // Catch:{ all -> 0x0047 }
            goto L_0x004c
        L_0x0047:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0047 }
            throw r0
        L_0x004a:
            r2 = 1
            goto L_0x006f
        L_0x004c:
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r6)
            if (r2 != 0) goto L_0x005c
            X.2lJ r2 = X.C53752lJ.A05
            java.lang.String r2 = r2.value
            boolean r2 = r2.equals(r6)
            if (r2 == 0) goto L_0x004a
        L_0x005c:
            java.lang.Integer r2 = X.AnonymousClass07B.A0o
            java.lang.String r2 = X.C178548Ls.A00(r2)
            boolean r2 = r2.equals(r5)
            if (r2 == 0) goto L_0x006a
            int r4 = r4 + -1
        L_0x006a:
            int r2 = r14.A03
            if (r4 < r2) goto L_0x004a
        L_0x006e:
            r2 = 0
        L_0x006f:
            if (r2 == 0) goto L_0x04f9
            int r4 = X.AnonymousClass1Y3.AGz
            X.0UN r3 = r14.A00
            r2 = 23
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x019e
            X.8E4 r3 = X.AnonymousClass8E4.TABLET
        L_0x0085:
            java.lang.String r2 = "device_type"
            r15.A0C(r2, r3)
            java.lang.String r3 = "pigeon_reserved_keyword_module"
            java.lang.String r2 = "video"
            r15.A0D(r3, r2)
            r4 = 4
            int r3 = X.AnonymousClass1Y3.A6x
            X.0UN r2 = r14.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.8Dp r5 = (X.C176618Dp) r5
            java.lang.String r2 = "video_id"
            java.lang.String r4 = r15.A07(r2)
            if (r4 == 0) goto L_0x0122
            java.util.Set r3 = X.C176618Dp.A07
            java.lang.String r2 = r15.A05
            boolean r2 = r3.contains(r2)
            if (r2 != 0) goto L_0x0122
            X.2lx r2 = X.C54152lx.A05
            java.lang.String r3 = r2.A01
            java.lang.String r2 = "player_suborigin"
            java.lang.String r2 = r15.A07(r2)
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x00d7
            X.8Ga r2 = r5.A03
            int r6 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r2.A00
            r2 = 1
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r6, r3)
            X.1Yd r6 = (X.C25051Yd) r6
            r2 = 283626763783460(0x101f500810d24, double:1.40130240226538E-309)
            boolean r3 = r6.Aem(r2)
            r2 = 1
            if (r3 != 0) goto L_0x00d8
        L_0x00d7:
            r2 = 0
        L_0x00d8:
            if (r2 == 0) goto L_0x0198
            java.util.Set r3 = X.C176618Dp.A08
            java.lang.String r2 = r15.A05
        L_0x00de:
            boolean r2 = r3.contains(r2)
            if (r2 == 0) goto L_0x00e7
            X.C176618Dp.A02(r5, r4, r15)
        L_0x00e7:
            java.lang.String r2 = r5.A01
            if (r2 == 0) goto L_0x00f4
            java.util.Map r2 = r5.A05
            java.lang.Object r3 = r2.get(r4)
            r2 = 1
            if (r3 != 0) goto L_0x00f5
        L_0x00f4:
            r2 = 0
        L_0x00f5:
            if (r2 == 0) goto L_0x0113
            java.lang.String r3 = r5.A01
            java.lang.String r2 = "video_chaining_session_id"
            r15.A0D(r2, r3)
            java.util.Set r3 = X.C176618Dp.A06
            java.lang.String r2 = r15.A05
            boolean r2 = r3.contains(r2)
            if (r2 != 0) goto L_0x0113
            java.util.Map r2 = r5.A05
            java.lang.Object r3 = r2.get(r4)
            java.lang.String r2 = "video_chaining_depth_level"
            r15.A0C(r2, r3)
        L_0x0113:
            java.util.Map r2 = r5.A04
            java.lang.Object r3 = r2.get(r4)
            java.lang.String r3 = (java.lang.String) r3
            if (r3 == 0) goto L_0x0122
            java.lang.String r2 = "adaptive_parent_video_id"
            r15.A0D(r2, r3)
        L_0x0122:
            r4 = 31
            int r3 = X.AnonymousClass1Y3.AKJ
            X.0UN r2 = r14.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.7w8 r5 = (X.C171927w8) r5
            java.lang.String r2 = "video_id"
            java.lang.String r4 = r15.A07(r2)
            int r2 = r5.A02
            r6 = 0
            if (r2 == 0) goto L_0x0196
            java.util.Map r2 = r5.A04
            if (r2 == 0) goto L_0x0196
            boolean r2 = r2.containsKey(r4)
            if (r2 == 0) goto L_0x0168
            java.util.Map r2 = r5.A04
            java.lang.Object r2 = r2.get(r4)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            if (r2 == 0) goto L_0x0151
            boolean r6 = r2.booleanValue()
        L_0x0151:
            if (r6 == 0) goto L_0x02da
            int r6 = X.AnonymousClass1Y3.B5j
            X.0UN r3 = r5.A00
            r2 = 4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r6, r3)
            X.0Ud r2 = (X.AnonymousClass0Ud) r2
            if (r2 == 0) goto L_0x02da
            boolean r2 = r2.A0G()
            if (r2 != 0) goto L_0x02da
            r7 = 0
            goto L_0x01a2
        L_0x0168:
            java.util.Map r2 = r5.A04
            int r3 = r2.size()
            r2 = 1000(0x3e8, float:1.401E-42)
            if (r3 <= r2) goto L_0x0177
            java.util.Map r2 = r5.A04
            r2.clear()
        L_0x0177:
            java.util.Random r3 = r5.A05
            int r2 = r5.A02
            int r2 = r3.nextInt(r2)
            if (r2 != 0) goto L_0x018d
            java.util.Map r3 = r5.A04
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r3.put(r4, r2)
            r6 = 1
            goto L_0x0151
        L_0x018d:
            java.util.Map r3 = r5.A04
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r6)
            r3.put(r4, r2)
        L_0x0196:
            r6 = 0
            goto L_0x0151
        L_0x0198:
            java.util.Set r3 = X.C176618Dp.A09
            java.lang.String r2 = r15.A05
            goto L_0x00de
        L_0x019e:
            X.8E4 r3 = X.AnonymousClass8E4.MOBILE
            goto L_0x0085
        L_0x01a2:
            int r3 = X.AnonymousClass1Y3.BPU     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            X.0UN r2 = r5.A00     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            r6 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            X.25O r2 = (X.AnonymousClass25O) r2     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            android.location.LocationManager r3 = r2.A01     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            java.lang.String r2 = "network"
            boolean r2 = r3.isProviderEnabled(r2)     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            if (r2 != 0) goto L_0x01cb
            int r3 = X.AnonymousClass1Y3.BPU     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            X.0UN r2 = r5.A00     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            X.25O r2 = (X.AnonymousClass25O) r2     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            android.location.LocationManager r3 = r2.A01     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            java.lang.String r2 = "gps"
            boolean r2 = r3.isProviderEnabled(r2)     // Catch:{ IllegalArgumentException | SecurityException -> 0x01cc }
            if (r2 == 0) goto L_0x01cc
        L_0x01cb:
            r7 = 1
        L_0x01cc:
            if (r7 == 0) goto L_0x02da
            java.util.Set r3 = X.C171927w8.A07
            java.lang.String r2 = r15.A05
            boolean r2 = r3.contains(r2)
            if (r2 == 0) goto L_0x02da
            if (r4 == 0) goto L_0x0285
            boolean r2 = r4.isEmpty()
            if (r2 != 0) goto L_0x0285
            java.util.Map r2 = r5.A03
            if (r2 == 0) goto L_0x0285
            boolean r2 = r2.containsKey(r4)
            if (r2 == 0) goto L_0x0267
            java.util.Map r2 = r5.A03
            java.lang.Object r3 = r2.get(r4)
            java.lang.String r3 = (java.lang.String) r3
        L_0x01f2:
            java.lang.String r2 = "random_session_id"
            r15.A0D(r2, r3)
            int r3 = r5.A02
            java.lang.String r2 = "video_insight_sample_weight"
            r15.A09(r2, r3)
            int r4 = X.AnonymousClass1Y3.AeN
            X.0UN r3 = r5.A00
            r2 = 3
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.0h3 r2 = (X.C09340h3) r2
            if (r2 == 0) goto L_0x0214
            java.lang.String r3 = r2.A0K()
            java.lang.String r2 = "connection_type"
            r15.A0D(r2, r3)
        L_0x0214:
            java.util.HashMap r6 = new java.util.HashMap
            r6.<init>()
            boolean r2 = r5.A01
            if (r2 == 0) goto L_0x029e
            X.0UN r3 = r5.A00
            r2 = 3
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.0h3 r2 = (X.C09340h3) r2
            java.lang.String r3 = r2.A0K()
            java.lang.String r2 = "wifi"
            boolean r2 = r2.equalsIgnoreCase(r3)
            if (r2 == 0) goto L_0x029e
            int r4 = X.AnonymousClass1Y3.BB5
            X.0UN r3 = r5.A00
            r2 = 0
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.A4J r7 = (X.A4J) r7
            int r3 = X.AnonymousClass1Y3.AWJ
            X.0UN r2 = r7.A00
            r4 = 1
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.0pP r3 = (X.C12460pP) r3
            if (r3 == 0) goto L_0x0253
            java.lang.String r2 = "android.permission.ACCESS_WIFI_STATE"
            boolean r2 = r3.A08(r2)
            if (r2 == 0) goto L_0x0253
            r4 = 0
        L_0x0253:
            if (r4 != 0) goto L_0x029e
            r4 = 0
            int r3 = X.AnonymousClass1Y3.AzH
            X.0UN r2 = r7.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)
            android.net.wifi.WifiManager r2 = (android.net.wifi.WifiManager) r2
            boolean r2 = r2.isWifiEnabled()
            if (r2 == 0) goto L_0x029e
            goto L_0x0289
        L_0x0267:
            java.util.Map r2 = r5.A03
            int r3 = r2.size()
            r2 = 1000(0x3e8, float:1.401E-42)
            if (r3 <= r2) goto L_0x0276
            java.util.Map r2 = r5.A03
            r2.clear()
        L_0x0276:
            java.util.UUID r2 = X.C188215g.A00()
            java.lang.String r3 = r2.toString()
            java.util.Map r2 = r5.A03
            r2.put(r4, r3)
            goto L_0x01f2
        L_0x0285:
            java.lang.String r3 = ""
            goto L_0x01f2
        L_0x0289:
            X.2p8 r3 = r7.A01     // Catch:{ NullPointerException | SecurityException -> 0x029e }
            r2 = 365(0x16d, float:5.11E-43)
            java.lang.String r2 = X.AnonymousClass80H.$const$string(r2)     // Catch:{ NullPointerException | SecurityException -> 0x029e }
            android.net.wifi.WifiInfo r2 = r3.A02(r2)     // Catch:{ NullPointerException | SecurityException -> 0x029e }
            java.lang.String r3 = "hardware_address"
            java.lang.String r2 = r2.getBSSID()     // Catch:{ NullPointerException | SecurityException -> 0x029e }
            r6.put(r3, r2)     // Catch:{ NullPointerException | SecurityException -> 0x029e }
        L_0x029e:
            int r4 = X.AnonymousClass1Y3.AO4
            X.0UN r3 = r5.A00
            r2 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.1k0 r2 = (X.C31631k0) r2
            r2.A0B(r6)
            r2 = 0
            X.C11670nb.A02(r15, r6, r2)
            int r4 = X.AnonymousClass1Y3.BPU
            X.0UN r3 = r5.A00
            r2 = 2
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.25O r4 = (X.AnonymousClass25O) r4
            r2 = 600000(0x927c0, double:2.964394E-318)
            X.8pw r5 = r4.A02(r2)
            if (r5 == 0) goto L_0x02da
            android.location.Location r2 = r5.A00
            double r3 = r2.getLatitude()
            java.lang.String r2 = "device_lat"
            r15.A08(r2, r3)
            android.location.Location r2 = r5.A00
            double r3 = r2.getLongitude()
            java.lang.String r2 = "device_long"
            r15.A08(r2, r3)
        L_0x02da:
            r3 = 0
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "watch_and_go_session_id"
            r15.A0D(r2, r3)
        L_0x02e6:
            java.lang.String r2 = "external_log_id"
            java.lang.String r2 = r15.A07(r2)
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            java.lang.String r2 = "video_time_position"
            r15.A07(r2)
            java.lang.String r2 = "seq_num"
            java.lang.String r2 = r15.A07(r2)
            r5 = 0
            int r4 = java.lang.Integer.parseInt(r2)     // Catch:{ NumberFormatException -> 0x0301 }
            goto L_0x0302
        L_0x0301:
            r4 = 0
        L_0x0302:
            java.lang.String r2 = "video_id"
            java.lang.String r2 = r15.A07(r2)
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r4 <= r3) goto L_0x0329
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r2 != 0) goto L_0x0329
            if (r4 > r3) goto L_0x04f9
            X.09P r4 = r14.A05
            java.lang.String r2 = r15.A05()
            java.lang.Object[] r3 = new java.lang.Object[]{r2}
            java.lang.String r2 = "Too many events for same video: (%s)"
            java.lang.String r3 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r2, r3)
            java.lang.String r2 = "too_many_events"
            r4.CGS(r2, r3)
        L_0x0329:
            X.1YI r3 = r14.A0A
            r2 = 500(0x1f4, float:7.0E-43)
            boolean r2 = r3.AbO(r2, r5)
            if (r2 == 0) goto L_0x033c
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.String r4 = "elapsed_realtime_ms"
            r15.A0A(r4, r2)
        L_0x033c:
            r4 = 2
            int r3 = X.AnonymousClass1Y3.A7w
            X.0UN r2 = r14.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.13b r2 = (X.AnonymousClass13b) r2
            r2.A05(r15)
            java.lang.String r2 = "playback_is_live_streaming"
            java.lang.String r2 = r15.A07(r2)
            boolean r2 = java.lang.Boolean.parseBoolean(r2)
            if (r2 == 0) goto L_0x03cd
            if (r16 == 0) goto L_0x03af
            r2 = 647(0x287, float:9.07E-43)
            java.lang.String r4 = X.C05360Yq.$const$string(r2)
            java.lang.String r3 = "true"
            r2 = 1
            java.util.Map r2 = X.C11670nb.A00(r15, r2)
            r2.put(r4, r3)
        L_0x0368:
            int r3 = X.AnonymousClass1Y3.B5y
            X.0UN r2 = r14.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r3, r2)
            X.EYf r2 = (X.C29381EYf) r2
            X.EYc r2 = X.C29378EYc.A00(r2)
            r2.A01(r15)
        L_0x0379:
            boolean r2 = r14.A0H
            if (r2 == 0) goto L_0x03ed
            int r4 = X.AnonymousClass1Y3.AzA
            X.0UN r2 = r14.A00
            r3 = 17
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r4, r2)
            java.util.Set r2 = (java.util.Set) r2
            if (r2 == 0) goto L_0x03ed
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto L_0x03ed
            X.0UN r2 = r14.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r4, r2)
            java.util.Set r2 = (java.util.Set) r2
            java.util.Iterator r3 = r2.iterator()
        L_0x039d:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x03ed
            java.lang.Object r2 = r3.next()
            X.4WH r2 = (X.AnonymousClass4WH) r2
            if (r2 == 0) goto L_0x039d
            r2.onLogEvent(r15)
            goto L_0x039d
        L_0x03af:
            int r3 = X.AnonymousClass1Y3.B5y
            X.0UN r2 = r14.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r3, r2)
            X.EYf r2 = (X.C29381EYf) r2
            X.EYc r2 = X.C29378EYc.A00(r2)
            X.EYe r2 = r2.A00
            int r4 = X.AnonymousClass1Y3.AR0
            X.0UN r3 = r2.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r4, r3)
            com.facebook.analytics.DeprecatedAnalyticsLogger r2 = (com.facebook.analytics.DeprecatedAnalyticsLogger) r2
            r2.A06(r15)
            goto L_0x0379
        L_0x03cd:
            if (r16 != 0) goto L_0x0368
            int r3 = X.AnonymousClass1Y3.B5y
            X.0UN r2 = r14.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r3, r2)
            X.EYf r2 = (X.C29381EYf) r2
            X.EYc r2 = X.C29378EYc.A00(r2)
            X.EYe r2 = r2.A00
            int r4 = X.AnonymousClass1Y3.AR0
            X.0UN r3 = r2.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r4, r3)
            com.facebook.analytics.DeprecatedAnalyticsLogger r2 = (com.facebook.analytics.DeprecatedAnalyticsLogger) r2
            r2.A05(r15)
            goto L_0x0379
        L_0x03ed:
            java.lang.String r3 = r15.A05
            java.lang.Integer r2 = X.AnonymousClass07B.A0Y
            java.lang.String r2 = X.C178548Ls.A00(r2)
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x046e
            java.lang.Integer r2 = X.AnonymousClass07B.A0o
            java.lang.String r2 = X.C178548Ls.A00(r2)
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x046e
        L_0x0407:
            java.lang.String r2 = "video_time_position"
            java.lang.String r9 = r15.A07(r2)
            boolean r2 = r14.A0G
            java.lang.String r3 = "player_origin"
            java.lang.String r5 = "player"
            java.lang.String r8 = "video_id"
            java.lang.String r7 = "VideoLoggingUtils"
            if (r2 == 0) goto L_0x0450
            java.lang.String r2 = "autoplay_failure_reasons"
            java.lang.String r6 = r15.A07(r2)
            java.lang.String r10 = r15.A07(r8)
            java.lang.String r11 = r15.A05
            java.lang.String r12 = r15.A07(r5)
            java.lang.String r13 = r15.A07(r3)
            java.lang.String r2 = "debug_reason"
            java.lang.String r14 = r15.A07(r2)
            java.lang.String r16 = ""
            if (r9 != 0) goto L_0x0465
            r15 = r16
        L_0x0439:
            java.lang.String r2 = "[]"
            boolean r2 = r2.equals(r6)
            if (r2 != 0) goto L_0x0447
            java.lang.String r2 = " ap="
            java.lang.String r16 = X.AnonymousClass08S.A0J(r2, r6)
        L_0x0447:
            java.lang.Object[] r4 = new java.lang.Object[]{r10, r11, r12, r13, r14, r15, r16}
            java.lang.String r2 = "VideoEvent: videoId[%s] Event[%s] player[%s] origin[%s] reason[%s] %s%s"
            X.C010708t.A0P(r7, r2, r4)
        L_0x0450:
            java.lang.Integer r2 = r1.A01
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            if (r2 == r1) goto L_0x04f9
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            if (r2 != r1) goto L_0x04c1
            java.util.Set r2 = X.C400420b.A0O
            java.lang.String r1 = r0.A05
            boolean r1 = r2.contains(r1)
            if (r1 != 0) goto L_0x04c1
            return
        L_0x0465:
            java.lang.String r4 = " pos=["
            java.lang.String r2 = "]"
            java.lang.String r15 = X.AnonymousClass08S.A0P(r4, r9, r2)
            goto L_0x0439
        L_0x046e:
            java.lang.String r2 = "video_last_start_time_position"
            java.lang.String r2 = r15.A07(r2)     // Catch:{ Exception -> 0x04b7 }
            double r3 = java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x04b7 }
            java.lang.String r2 = "video_time_position"
            java.lang.String r2 = r15.A07(r2)     // Catch:{ Exception -> 0x04b7 }
            double r5 = java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x04b7 }
            double r5 = r5 - r3
            r4 = 20
            int r3 = X.AnonymousClass1Y3.A8Q     // Catch:{ Exception -> 0x04b7 }
            X.0UN r2 = r14.A00     // Catch:{ Exception -> 0x04b7 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ Exception -> 0x04b7 }
            java.util.Set r2 = (java.util.Set) r2     // Catch:{ Exception -> 0x04b7 }
            java.util.Iterator r3 = r2.iterator()     // Catch:{ Exception -> 0x04b7 }
        L_0x0493:
            boolean r2 = r3.hasNext()     // Catch:{ Exception -> 0x04b7 }
            if (r2 == 0) goto L_0x0407
            java.lang.Object r4 = r3.next()     // Catch:{ Exception -> 0x04b7 }
            X.8Kl r4 = (X.C178238Kl) r4     // Catch:{ Exception -> 0x04b7 }
            boolean r2 = r4.shouldNotifyLoggerAboutVideoPlayEvent()     // Catch:{ Exception -> 0x04b7 }
            if (r2 == 0) goto L_0x0493
            boolean r7 = r15.A08     // Catch:{ Exception -> 0x04b7 }
            java.lang.String r2 = "player_origin"
            java.lang.String r8 = r15.A07(r2)     // Catch:{ Exception -> 0x04b7 }
            java.lang.String r2 = "player"
            java.lang.String r9 = r15.A07(r2)     // Catch:{ Exception -> 0x04b7 }
            r4.reportVideoPlayEvent(r5, r7, r8, r9)     // Catch:{ Exception -> 0x04b7 }
            goto L_0x0493
        L_0x04b7:
            r4 = move-exception
            java.lang.String r3 = "VideoLoggingUtils"
            java.lang.String r2 = "Failed to report UIH video play event"
            X.C010708t.A0N(r3, r2, r4)
            goto L_0x0407
        L_0x04c1:
            r0.A07(r8)
            java.lang.String r1 = "video_last_start_time_position"
            r0.A07(r1)
            java.lang.String r1 = "video_seek_source_time_position"
            r0.A07(r1)
            java.lang.String r1 = "video_play_reason"
            r0.A07(r1)
            r0.A07(r5)
            java.lang.String r1 = "video_chaining_session_id"
            r0.A07(r1)
            java.lang.String r1 = "video_chaining_depth_level"
            r0.A07(r1)
            r0.A07(r3)
            java.lang.String r1 = "player_suborigin"
            r0.A07(r1)
            r1 = 273(0x111, float:3.83E-43)
            java.lang.String r1 = X.C05360Yq.$const$string(r1)
            r0.A07(r1)
            java.lang.String r1 = "attribution_id"
            r0.A07(r1)
            r0.A05()
        L_0x04f9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.A0G(X.20b, X.0nb, boolean):void");
    }

    private static boolean A0N(String str) {
        if (str != null) {
            return C53752lJ.A05.value.equals(str) || C53752lJ.A0w.value.equals(str);
        }
        return false;
    }

    static {
        String A002 = C178548Ls.A00(AnonymousClass07B.A0C);
        String A003 = C178548Ls.A00(AnonymousClass07B.A0V);
        String A004 = C178548Ls.A00(AnonymousClass07B.A01);
        String A005 = C178548Ls.A00(AnonymousClass07B.A0F);
        String A006 = C178548Ls.A00(AnonymousClass07B.A0G);
        String A007 = C178548Ls.A00(AnonymousClass07B.A0o);
        String[] strArr = new String[29];
        System.arraycopy(new String[]{C178548Ls.A00(AnonymousClass07B.A0M), C178548Ls.A00(AnonymousClass07B.A0O), C178548Ls.A00(AnonymousClass07B.A0q), C178548Ls.A00(AnonymousClass07B.A02), C178548Ls.A00(AnonymousClass07B.A0B), C178548Ls.A00(AnonymousClass07B.A03), C178548Ls.A00(AnonymousClass07B.A0N), C178548Ls.A00(AnonymousClass07B.A04), C178548Ls.A00(AnonymousClass07B.A03), C178548Ls.A00(AnonymousClass07B.A0Y), C178548Ls.A00(AnonymousClass07B.A0i), C178548Ls.A00(AnonymousClass07B.A0C), C178548Ls.A00(AnonymousClass07B.A05), C178548Ls.A00(AnonymousClass07B.A06), C05360Yq.$const$string(2444), C05360Yq.$const$string(AnonymousClass1Y3.AKA), C05360Yq.$const$string(AnonymousClass1Y3.ADJ), C178548Ls.A00(AnonymousClass07B.A0K), C178548Ls.A00(AnonymousClass07B.A0L), C178548Ls.A00(AnonymousClass07B.A0Q), C178548Ls.A00(AnonymousClass07B.A0J), C05360Yq.$const$string(1320), C05360Yq.$const$string(AnonymousClass1Y3.AAr), C178548Ls.A00(AnonymousClass07B.A0c), C178548Ls.A00(AnonymousClass07B.A0I), C178548Ls.A00(AnonymousClass07B.A0Z), C178548Ls.A00(AnonymousClass07B.A0X)}, 0, strArr, 0, 27);
        System.arraycopy(new String[]{C178548Ls.A00(AnonymousClass07B.A07), C178548Ls.A00(AnonymousClass07B.A0o)}, 0, strArr, 27, 2);
        A0P = ImmutableSet.A09(A002, A003, A004, A005, A006, A007, strArr);
    }

    public static int A00(C400420b r5) {
        int i = AnonymousClass1Y3.AAT;
        int streamVolume = ((AudioManager) AnonymousClass1XX.A02(13, i, r5.A00)).getStreamVolume(3);
        int streamMaxVolume = ((AudioManager) AnonymousClass1XX.A02(13, i, r5.A00)).getStreamMaxVolume(3);
        if (streamMaxVolume == 0) {
            return 0;
        }
        return (streamVolume * 100) / streamMaxVolume;
    }

    public static final C400420b A01(AnonymousClass1XY r10) {
        if (A0Q == null) {
            synchronized (C400420b.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0Q, r10);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r10.getApplicationInjector();
                        AnonymousClass09P A012 = C04750Wa.A01(applicationInjector);
                        C25051Yd A003 = AnonymousClass0WT.A00(applicationInjector);
                        AnonymousClass0VG A004 = AnonymousClass0VG.A00(AnonymousClass1Y3.ARl, applicationInjector);
                        C04310Tq A052 = C04750Wa.A05(applicationInjector);
                        FbSharedPreferencesModule.A00(applicationInjector);
                        A0Q = new C400420b(applicationInjector, A012, A003, A004, A052, AnonymousClass4W5.A00(AnonymousClass07B.A00), AnonymousClass0WA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0Q;
    }

    public static String A02(C400420b r3) {
        int i = ((Context) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BCt, r3.A00)).getResources().getConfiguration().orientation;
        if (i == 1) {
            return C22298Ase.$const$string(90);
        }
        if (i == 2) {
            return "landscape";
        }
        return "unknown";
    }

    public static void A04(C11670nb r4, C96004i0 r5) {
        Intent registerReceiver;
        Integer num;
        String str;
        if (r5.A01) {
            registerReceiver = r5.A00;
        } else {
            registerReceiver = r5.A02.registerReceiver(new C06870cD(AnonymousClass80H.$const$string(14), new C95994hz(r5)), C96004i0.A03);
            r5.A00 = registerReceiver;
            r5.A01 = true;
        }
        if (registerReceiver == null) {
            num = AnonymousClass07B.A00;
        } else if (registerReceiver.getIntExtra("state", 0) == 0) {
            num = AnonymousClass07B.A01;
        } else {
            num = AnonymousClass07B.A0C;
        }
        switch (num.intValue()) {
            case 1:
                str = TurboLoader.Locator.$const$string(24);
                break;
            case 2:
                str = "connected";
                break;
            default:
                str = "unknown";
                break;
        }
        r4.A0D("headset_state", str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A06(X.C11670nb r8, X.AnonymousClass8SI r9, X.AnonymousClass8ST r10) {
        /*
            java.lang.String r3 = "stall_time"
            java.lang.String r5 = "stall_count_200_ms"
            r0 = 224(0xe0, float:3.14E-43)
            java.lang.String r6 = X.C99084oO.$const$string(r0)
            if (r9 != 0) goto L_0x002e
            if (r10 == 0) goto L_0x002d
            int r0 = r10.A00
            r8.A09(r6, r0)
            int r0 = r10.A01
            r8.A09(r5, r0)
            int r0 = r10.A02
            float r1 = (float) r0
            r0 = 1148846080(0x447a0000, float:1000.0)
            float r1 = r1 / r0
            double r0 = (double) r1
            r8.A08(r3, r0)
            int r0 = r10.A00
            if (r0 <= 0) goto L_0x002d
            long r1 = r10.A03
            java.lang.String r0 = "first_stall_start_position"
            r8.A0A(r0, r1)
        L_0x002d:
            return
        L_0x002e:
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            java.lang.String r2 = "stall_record_time"
            long r0 = r9.A06     // Catch:{ JSONException -> 0x0120 }
            r8.A0A(r2, r0)     // Catch:{ JSONException -> 0x0120 }
            float r0 = r9.A02     // Catch:{ JSONException -> 0x0120 }
            double r0 = (double) r0     // Catch:{ JSONException -> 0x0120 }
            r8.A08(r3, r0)     // Catch:{ JSONException -> 0x0120 }
            int r1 = r9.A04     // Catch:{ JSONException -> 0x0120 }
            int r0 = r9.A05     // Catch:{ JSONException -> 0x0120 }
            r8.A09(r6, r1)     // Catch:{ JSONException -> 0x0120 }
            r8.A09(r5, r0)     // Catch:{ JSONException -> 0x0120 }
            if (r1 <= 0) goto L_0x010a
            int r0 = r9.A04     // Catch:{ JSONException -> 0x0120 }
            if (r0 != 0) goto L_0x0052
            r1 = 0
            goto L_0x0056
        L_0x0052:
            float r1 = r9.A02     // Catch:{ JSONException -> 0x0120 }
            float r0 = (float) r0     // Catch:{ JSONException -> 0x0120 }
            float r1 = r1 / r0
        L_0x0056:
            double r1 = (double) r1     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "average_stall_time"
            r8.A08(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            X.8SR r0 = r9.A07     // Catch:{ JSONException -> 0x0120 }
            long r1 = r0.A02     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "first_stall_start_position"
            r8.A0A(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            X.8SR r0 = r9.A07     // Catch:{ JSONException -> 0x0120 }
            long r1 = r0.A01     // Catch:{ JSONException -> 0x0120 }
            float r0 = (float) r1     // Catch:{ JSONException -> 0x0120 }
            r3 = 1148846080(0x447a0000, float:1000.0)
            float r0 = r0 / r3
            double r1 = (double) r0     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "first_stall_time"
            r8.A08(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            X.8SR r0 = r9.A08     // Catch:{ JSONException -> 0x0120 }
            long r1 = r0.A02     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "last_stall_start_position"
            r8.A0A(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            X.8SR r0 = r9.A08     // Catch:{ JSONException -> 0x0120 }
            long r1 = r0.A01     // Catch:{ JSONException -> 0x0120 }
            float r0 = (float) r1     // Catch:{ JSONException -> 0x0120 }
            float r0 = r0 / r3
            double r1 = (double) r0     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "last_stall_time"
            r8.A08(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            X.8SR r0 = r9.A09     // Catch:{ JSONException -> 0x0120 }
            long r1 = r0.A02     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "max_stall_start_position"
            r8.A0A(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            X.8SR r0 = r9.A09     // Catch:{ JSONException -> 0x0120 }
            long r1 = r0.A01     // Catch:{ JSONException -> 0x0120 }
            float r0 = (float) r1     // Catch:{ JSONException -> 0x0120 }
            float r0 = r0 / r3
            double r1 = (double) r0     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "max_stall_time"
            r8.A08(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            com.google.common.collect.ImmutableList r0 = r9.A0A     // Catch:{ JSONException -> 0x0120 }
            int r1 = r0.size()     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "recent_stalls_total_count"
            r8.A09(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            float r0 = r9.A00     // Catch:{ JSONException -> 0x0120 }
            double r1 = (double) r0     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "recent_stalls_total_duration"
            r8.A08(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            float r0 = r9.A01     // Catch:{ JSONException -> 0x0120 }
            double r1 = (double) r0     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "recent_stalls_total_duration_trimmed"
            r8.A08(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            boolean r1 = r9.A0B     // Catch:{ JSONException -> 0x0120 }
            java.lang.String r0 = "is_stalling"
            r8.A0E(r0, r1)     // Catch:{ JSONException -> 0x0120 }
            org.json.JSONArray r6 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0120 }
            r6.<init>()     // Catch:{ JSONException -> 0x0120 }
            com.google.common.collect.ImmutableList r0 = r9.A0A     // Catch:{ JSONException -> 0x0120 }
            X.1Xv r7 = r0.iterator()     // Catch:{ JSONException -> 0x0120 }
        L_0x00ca:
            boolean r0 = r7.hasNext()     // Catch:{ JSONException -> 0x0120 }
            if (r0 == 0) goto L_0x0105
            java.lang.Object r5 = r7.next()     // Catch:{ JSONException -> 0x0120 }
            X.8SR r5 = (X.AnonymousClass8SR) r5     // Catch:{ JSONException -> 0x0120 }
            monitor-enter(r5)     // Catch:{ JSONException -> 0x0120 }
            org.json.JSONObject r0 = r5.A00     // Catch:{ all -> 0x0102 }
            if (r0 != 0) goto L_0x00fb
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x0102 }
            r3.<init>()     // Catch:{ all -> 0x0102 }
            r5.A00 = r3     // Catch:{ all -> 0x0102 }
            java.lang.String r2 = "position_ms"
            long r0 = r5.A02     // Catch:{ all -> 0x0102 }
            r3.put(r2, r0)     // Catch:{ all -> 0x0102 }
            org.json.JSONObject r3 = r5.A00     // Catch:{ all -> 0x0102 }
            java.lang.String r2 = "time_ms"
            long r0 = r5.A03     // Catch:{ all -> 0x0102 }
            r3.put(r2, r0)     // Catch:{ all -> 0x0102 }
            org.json.JSONObject r3 = r5.A00     // Catch:{ all -> 0x0102 }
            java.lang.String r2 = "duration_ms"
            long r0 = r5.A01     // Catch:{ all -> 0x0102 }
            r3.put(r2, r0)     // Catch:{ all -> 0x0102 }
        L_0x00fb:
            org.json.JSONObject r0 = r5.A00     // Catch:{ all -> 0x0102 }
            monitor-exit(r5)     // Catch:{ JSONException -> 0x0120 }
            r6.put(r0)     // Catch:{ JSONException -> 0x0120 }
            goto L_0x00ca
        L_0x0102:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ JSONException -> 0x0120 }
            throw r0     // Catch:{ JSONException -> 0x0120 }
        L_0x0105:
            java.lang.String r0 = "recent_stalls_detail"
            r4.put(r0, r6)     // Catch:{ JSONException -> 0x0120 }
        L_0x010a:
            float r1 = r9.A03     // Catch:{ JSONException -> 0x0120 }
            r0 = 0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0117
            java.lang.String r2 = "time_until_interrupt"
            double r0 = (double) r1     // Catch:{ JSONException -> 0x0120 }
            r8.A08(r2, r0)     // Catch:{ JSONException -> 0x0120 }
        L_0x0117:
            java.lang.String r1 = "stall_info_json"
            java.lang.String r0 = r4.toString()     // Catch:{ JSONException -> 0x0120 }
            r8.A0D(r1, r0)     // Catch:{ JSONException -> 0x0120 }
        L_0x0120:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.A06(X.0nb, X.8SI, X.8ST):void");
    }

    public static void A07(C11670nb r3, VideoPlayerParams videoPlayerParams, String str) {
        GraphQLVideoBroadcastStatus graphQLVideoBroadcastStatus;
        if (videoPlayerParams != null) {
            if (str != null && videoPlayerParams.A0a) {
                A0N.add(str);
            }
            r3.A0E("playback_is_live_streaming", videoPlayerParams.A0a);
            r3.A0E("playback_is_broadcast", videoPlayerParams.A0V);
            r3.A0E("is_servable_via_fbms", videoPlayerParams.A0e);
            if (videoPlayerParams.A0V && (graphQLVideoBroadcastStatus = videoPlayerParams.A0G) != null) {
                r3.A0D("playback_broadcast_status", graphQLVideoBroadcastStatus.toString());
            }
            if (videoPlayerParams.A00() != null) {
                r3.A0D("projection", videoPlayerParams.A00().toString());
            }
            if (videoPlayerParams.A01() != null) {
                r3.A0D("audio_ch_conf", videoPlayerParams.A01().toString());
            }
            int i = videoPlayerParams.A0B;
            if (i != -1) {
                r3.A09("story_position", i);
            }
            r3.A0E("is_spherical_fallback", videoPlayerParams.A0f);
            if (A0N.contains(str)) {
                r3.A0E("has_been_live", true);
            }
            ImmutableMap immutableMap = videoPlayerParams.A0L;
            if (immutableMap != null) {
                C24971Xv it = immutableMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    r3.A0D((String) entry.getKey(), (String) entry.getValue());
                }
            }
        }
    }

    public static void A08(C11670nb r3, ParcelableFormat parcelableFormat, ParcelableFormat parcelableFormat2, int i, int i2) {
        r3.A09("available_qualities", i);
        boolean z = true;
        if (i <= 1) {
            z = false;
        }
        r3.A0E("is_abr_enabled", z);
        r3.A09("available_audio_qualities", i2);
        if (parcelableFormat != null) {
            r3.A0D("representation_id", parcelableFormat.id);
            r3.A09("video_width", parcelableFormat.width);
            r3.A09("video_height", parcelableFormat.height);
            r3.A09(TraceFieldType.Bitrate, parcelableFormat.bitrate);
            r3.A0D("quality_label", parcelableFormat.fbQualityLabel);
            r3.A09("video_height", parcelableFormat.height);
            r3.A0D("mime", parcelableFormat.mimeType);
        }
        if (parcelableFormat2 != null) {
            r3.A0D("audio_representation_id", parcelableFormat2.id);
            r3.A09("audio_bitrate", parcelableFormat2.bitrate);
        }
    }

    public static void A09(C11670nb r3, AnonymousClass8EV r4) {
        if (r4 != null) {
            r3.A09("current_viewability_percentage", r4.A02);
            r3.A08("actual_viewability_value", (double) r4.A00);
            int i = r4.A01;
            if (i != 0) {
                r3.A09("viewability_debug_reason", i);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0046, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0047, code lost:
        if (r6 != null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        r6.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004c, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004d, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[ExcHandler: IOException | NumberFormatException (unused java.lang.Throwable), PHI: r6 
      PHI: (r6v2 java.lang.Process) = (r6v0 java.lang.Process), (r6v4 java.lang.Process), (r6v4 java.lang.Process), (r6v4 java.lang.Process), (r6v4 java.lang.Process) binds: [B:2:0x0003, B:6:0x0031, B:13:0x0042, B:14:?, B:7:?] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:2:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0A(X.C11670nb r7, boolean r8) {
        /*
            if (r8 == 0) goto L_0x0052
            r6 = 0
            java.lang.ProcessBuilder r2 = new java.lang.ProcessBuilder     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            java.lang.String r1 = "cat"
            java.lang.String r0 = "/sys/class/thermal/thermal_zone0/temp"
            java.lang.String[] r0 = new java.lang.String[]{r1, r0}     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            r2.<init>(r0)     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            java.lang.Process r6 = r2.start()     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            java.io.OutputStream r0 = r6.getOutputStream()     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            r0.close()     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            java.io.InputStream r0 = r6.getInputStream()     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            r1.<init>(r0)     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            r2.<init>(r1)     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            if (r0 == 0) goto L_0x004f
            java.lang.String r5 = "cpu_temperature"
            double r3 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0036, IOException | NumberFormatException -> 0x004d }
            goto L_0x0038
        L_0x0036:
            r3 = 0
        L_0x0038:
            r1 = 4652007308841189376(0x408f400000000000, double:1000.0)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0042
            double r3 = r3 / r1
        L_0x0042:
            r7.A08(r5, r3)     // Catch:{ IOException | NumberFormatException -> 0x004d, all -> 0x0046 }
            goto L_0x004f
        L_0x0046:
            r0 = move-exception
            if (r6 == 0) goto L_0x004c
            r6.destroy()
        L_0x004c:
            throw r0
        L_0x004d:
            if (r6 == 0) goto L_0x0052
        L_0x004f:
            r6.destroy()
        L_0x0052:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.A0A(X.0nb, boolean):void");
    }

    public static void A0B(C400420b r6, C11670nb r7) {
        r7.A0C("data_connection_quality", ((FbDataConnectionManager) AnonymousClass1XX.A02(7, AnonymousClass1Y3.B3e, r6.A00)).A08());
        r7.A0A("fb_bandwidth", (long) (((FbDataConnectionManager) AnonymousClass1XX.A02(7, AnonymousClass1Y3.B3e, r6.A00)).A03() * 1000.0d));
        r7.A0C("data_latency_quality", ((FbDataConnectionManager) AnonymousClass1XX.A02(7, AnonymousClass1Y3.B3e, r6.A00)).A07());
        r7.A0A("fb_latency", (long) ((FbDataConnectionManager) AnonymousClass1XX.A02(7, AnonymousClass1Y3.B3e, r6.A00)).A04());
        r7.A0A("video_bandwidth", ((AnonymousClass222) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMC, r6.A00)).A0E.A01());
    }

    public static void A0C(C400420b r3, C11670nb r4, String str) {
        C80783t5 r32;
        if (str != null && (r32 = (C80783t5) r3.A07.A03(str)) != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("autoplay_fb_bandwidth", Long.toString(r32.A01));
            hashMap.put("autoplay_video_bandwidth", Long.toString(r32.A02));
            hashMap.put("autoplay_time_since_last_fb_bandwidth_ms", Long.toString(r32.A04));
            hashMap.put("autoplay_time_since_last_connectivity_change_ms", Long.toString(r32.A03));
            String str2 = r32.A07;
            if (str2 != null && !str2.isEmpty()) {
                hashMap.put("autoplay_connection_oracle_model_desc", str2);
                hashMap.put("autoplay_connection_oracle_bw_est", Integer.toString(r32.A00));
            }
            C11670nb.A02(r4, hashMap, false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002e, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002f, code lost:
        r4.A05.softReport("ExtraParamsFromPropertyBag", com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe("VideoPropertyBag_AIOBBE: videoId=%s, event=%s", r6, r5.A05()), r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0D(X.C400420b r4, X.C11670nb r5, java.lang.String r6) {
        /*
            if (r6 == 0) goto L_0x0040
            r2 = 8
            int r1 = X.AnonymousClass1Y3.AtX     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
            X.0UN r0 = r4.A00     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
            X.820 r2 = (X.AnonymousClass820) r2     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
            monitor-enter(r2)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
            com.google.common.base.Preconditions.checkNotNull(r6)     // Catch:{ all -> 0x002b }
            X.1gw r0 = r2.A00     // Catch:{ all -> 0x002b }
            java.lang.Object r0 = r0.A03(r6)     // Catch:{ all -> 0x002b }
            java.util.HashMap r0 = (java.util.HashMap) r0     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x001f
            r1 = 0
            monitor-exit(r2)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
            goto L_0x0024
        L_0x001f:
            com.google.common.collect.ImmutableMap r1 = com.google.common.collect.ImmutableMap.copyOf(r0)     // Catch:{ all -> 0x002b }
            monitor-exit(r2)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
        L_0x0024:
            if (r1 == 0) goto L_0x0040
            r0 = 0
            X.C11670nb.A02(r5, r1, r0)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
            return
        L_0x002b:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
            throw r0     // Catch:{ ArrayIndexOutOfBoundsException -> 0x002e }
        L_0x002e:
            r3 = move-exception
            java.lang.String r1 = r5.A05()
            java.lang.String r0 = "VideoPropertyBag_AIOBBE: videoId=%s, event=%s"
            java.lang.String r2 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r6, r1)
            X.09P r1 = r4.A05
            java.lang.String r0 = "ExtraParamsFromPropertyBag"
            r1.softReport(r0, r2, r3)
        L_0x0040:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.A0D(X.20b, X.0nb, java.lang.String):void");
    }

    public static void A0F(C400420b r0, C11670nb r1, String str, String str2, long j) {
        if (r0.A0M) {
            r1.A0D("surface_state", str);
            r1.A0D("surface_id", str2);
            r1.A0A("surface_last_update_time", j);
        }
    }

    public static void A0H(C400420b r0, String str, C11670nb r2) {
        C80783t5 r02;
        TriState triState;
        if (str != null && (r02 = (C80783t5) r0.A07.A03(str)) != null && (triState = r02.A05) != TriState.UNSET) {
            r2.A0C("vr_cast_button_displayed", triState);
        }
    }

    public static void A0J(String str, C11670nb r4, C194749Cr r5) {
        String str2;
        Map map;
        Object obj;
        if (str != null && r5 != null) {
            String A002 = r5.A00(str);
            if (str == null || (map = (Map) r5.A00.A03(str)) == null || (obj = map.get(TraceFieldType.StreamType)) == null) {
                str2 = null;
            } else {
                str2 = Integer.toString(((Integer) obj).intValue());
            }
            if (A002 != null) {
                r4.A0D("live_trace_stream_id", A002);
            }
            if (str2 != null) {
                r4.A0D("live_trace_stream_type", str2);
            }
            r4.A0D("live_trace_source_id", A9W.A09.A02);
        }
    }

    public static boolean A0L(C400420b r3, String str, String str2) {
        C54152lx r0;
        String str3;
        if (!r3.A0A.AbO(212, false) || r3.A03 <= 0 || (str3 = (r0 = C54152lx.A08).A01) == null || !r0.A00.equals(str) || !str3.equals(str2)) {
            return false;
        }
        return true;
    }

    public static boolean A0M(String str) {
        if (str == null || !str.startsWith("GIF:")) {
            return true;
        }
        return false;
    }

    public void A0P(VideoPlayerParams videoPlayerParams, AnonymousClass8Q6 r12, AnonymousClass8EV r13, int i, C54152lx r15) {
        C11670nb r3 = new C11670nb(C178548Ls.A00(AnonymousClass07B.A0X));
        r3.A08("video_time_position", (double) (((float) i) / 1000.0f));
        r3.A09("current_viewability_percentage", r13.A02);
        r3.A08("actual_viewability_value", (double) r13.A00);
        int i2 = r13.A01;
        if (i2 != 0) {
            r3.A09("viewability_debug_reason", i2);
        }
        A07(r3, videoPlayerParams, videoPlayerParams.A0Q);
        A0E(this, r3, videoPlayerParams.A0Q, videoPlayerParams.A0K, videoPlayerParams.A0g, r15, r12, false);
    }

    public void A0Q(VideoPlayerParams videoPlayerParams, JsonNode jsonNode, AnonymousClass8Q6 r20, String str, int i, int i2, String str2, C54152lx r25, boolean z, boolean z2, boolean z3) {
        String str3 = str2;
        int i3 = i2;
        int i4 = i;
        AnonymousClass8Q6 r7 = r20;
        C54152lx r6 = r25;
        ((C76053ks) AnonymousClass1XX.A02(24, AnonymousClass1Y3.A4M, this.A00)).A0A(str3, r6, r7, i4, i3, true);
        C11670nb r10 = new C11670nb(C178548Ls.A00(AnonymousClass07B.A03));
        r10.A08("video_seek_source_time_position", (double) (((float) i4) / 1000.0f));
        r10.A08("video_time_position", (double) (((float) i3) / 1000.0f));
        r10.A0D("debug_reason", str);
        r10.A0D("video_play_reason", C53752lJ.A0w.value);
        r10.A0E("playback_is_live_streaming", z2);
        r10.A0E(AnonymousClass80H.$const$string(20), z3);
        A07(r10, videoPlayerParams, str3);
        String str4 = str3;
        A0E(this, r10, str4, jsonNode, z, r6, r7, false);
    }

    public void A0R(VideoPlayerParams videoPlayerParams, String str, C54152lx r6, AnonymousClass8Q6 r7, int i, String str2) {
        String str3;
        VideoDataSource videoDataSource;
        Uri uri;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(25, AnonymousClass1Y3.Ap7, this.A00)).A01("replica_switch_failed"), AnonymousClass1Y3.A4L);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D(TraceFieldType.VideoId, videoPlayerParams.A0Q);
            uSLEBaseShape0S0000000.A0D("reason", str2);
            if (videoPlayerParams == null || (videoDataSource = videoPlayerParams.A0I) == null || (uri = videoDataSource.A03) == null) {
                str3 = null;
            } else {
                str3 = uri.toString();
            }
            uSLEBaseShape0S0000000.A0D("url", str3);
            uSLEBaseShape0S0000000.A0C("event_creation_time", Long.valueOf(System.currentTimeMillis()));
            uSLEBaseShape0S0000000.A0D("player_origin", r6.A00);
            uSLEBaseShape0S0000000.A0D("player_suborigin", r6.A01);
            uSLEBaseShape0S0000000.A0D("player_type", r7.value);
            uSLEBaseShape0S0000000.A0B("video_time_position", Integer.valueOf(i));
            if (str != null) {
                uSLEBaseShape0S0000000.A0C("event_id", Long.valueOf((long) this.A0D.getAndIncrement()));
                uSLEBaseShape0S0000000.A0L("REPLICA_SWITCH_FAILED");
                uSLEBaseShape0S0000000.A0C("event_creation_time", Long.valueOf(System.currentTimeMillis()));
                uSLEBaseShape0S0000000.A0D("event_severity", "INFO");
                uSLEBaseShape0S0000000.A0D("stream_id", str);
                uSLEBaseShape0S0000000.A0Q(A9W.A09.A02);
            }
            uSLEBaseShape0S0000000.A0O("video");
            uSLEBaseShape0S0000000.A06();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x002b, code lost:
        if (r0.A0g == false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0T(com.fasterxml.jackson.databind.JsonNode r11, X.AnonymousClass8Q6 r12, java.lang.String r13, int r14, int r15, java.lang.String r16, X.C54152lx r17, com.facebook.video.engine.api.VideoPlayerParams r18) {
        /*
            r10 = this;
            X.0nb r3 = new X.0nb
            java.lang.Integer r0 = X.AnonymousClass07B.A06
            java.lang.String r0 = X.C178548Ls.A00(r0)
            r3.<init>(r0)
            java.lang.String r0 = "debug_reason"
            r3.A0D(r0, r13)
            float r1 = (float) r14
            r0 = 1148846080(0x447a0000, float:1000.0)
            float r1 = r1 / r0
            double r1 = (double) r1
            java.lang.String r0 = "video_time_position"
            r3.A08(r0, r1)
            java.lang.String r0 = "current_volume"
            r3.A09(r0, r15)
            r0 = r18
            r4 = r16
            A07(r3, r0, r4)
            if (r18 == 0) goto L_0x002d
            boolean r0 = r0.A0g
            r6 = 1
            if (r0 != 0) goto L_0x002e
        L_0x002d:
            r6 = 0
        L_0x002e:
            r9 = 0
            r2 = r10
            r8 = r12
            r5 = r11
            r7 = r17
            A0E(r2, r3, r4, r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.A0T(com.fasterxml.jackson.databind.JsonNode, X.8Q6, java.lang.String, int, int, java.lang.String, X.2lx, com.facebook.video.engine.api.VideoPlayerParams):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x002b, code lost:
        if (r0.A0g == false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0U(com.fasterxml.jackson.databind.JsonNode r11, X.AnonymousClass8Q6 r12, java.lang.String r13, int r14, int r15, java.lang.String r16, X.C54152lx r17, com.facebook.video.engine.api.VideoPlayerParams r18) {
        /*
            r10 = this;
            X.0nb r3 = new X.0nb
            java.lang.Integer r0 = X.AnonymousClass07B.A05
            java.lang.String r0 = X.C178548Ls.A00(r0)
            r3.<init>(r0)
            java.lang.String r0 = "debug_reason"
            r3.A0D(r0, r13)
            float r1 = (float) r14
            r0 = 1148846080(0x447a0000, float:1000.0)
            float r1 = r1 / r0
            double r1 = (double) r1
            java.lang.String r0 = "video_time_position"
            r3.A08(r0, r1)
            java.lang.String r0 = "current_volume"
            r3.A09(r0, r15)
            r0 = r18
            r4 = r16
            A07(r3, r0, r4)
            if (r18 == 0) goto L_0x002d
            boolean r0 = r0.A0g
            r6 = 1
            if (r0 != 0) goto L_0x002e
        L_0x002d:
            r6 = 0
        L_0x002e:
            r9 = 0
            r2 = r10
            r8 = r12
            r5 = r11
            r7 = r17
            A0E(r2, r3, r4, r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.A0U(com.fasterxml.jackson.databind.JsonNode, X.8Q6, java.lang.String, int, int, java.lang.String, X.2lx, com.facebook.video.engine.api.VideoPlayerParams):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x005c, code lost:
        if (r2.A0g == false) goto L_0x005e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0V(com.fasterxml.jackson.databind.JsonNode r12, X.AnonymousClass8Q6 r13, java.lang.String r14, java.lang.String r15, int r16, int r17, java.lang.String r18, X.C54152lx r19, java.lang.String r20, java.lang.String r21, com.facebook.video.engine.api.VideoPlayerParams r22) {
        /*
            r11 = this;
            java.lang.String r2 = "logVideoPlayedForThreeSeconds"
            r0 = 0
            r3 = r20
            java.lang.String r1 = A03(r11, r0, r3, r2)
            X.0nb r4 = new X.0nb
            java.lang.Integer r0 = X.AnonymousClass07B.A0J
            java.lang.String r0 = X.C178548Ls.A00(r0)
            r4.<init>(r0)
            java.lang.String r0 = "streaming_format"
            r4.A0D(r0, r14)
            java.lang.String r0 = "fbvp_session_id"
            r4.A0D(r0, r15)
            java.lang.String r0 = "player_version"
            r2 = r21
            r4.A0D(r0, r2)
            java.lang.String r0 = "video_play_reason"
            r4.A0D(r0, r1)
            r0 = r16
            float r0 = (float) r0
            r3 = 1148846080(0x447a0000, float:1000.0)
            float r0 = r0 / r3
            double r1 = (double) r0
            java.lang.String r0 = "video_time_position"
            r4.A08(r0, r1)
            r0 = r17
            float r0 = (float) r0
            float r0 = r0 / r3
            double r1 = (double) r0
            java.lang.String r0 = "video_last_start_time_position"
            r4.A08(r0, r1)
            r5 = r18
            r2 = r22
            A07(r4, r2, r5)
            A0H(r11, r5, r4)
            A0B(r11, r4)
            java.lang.Integer r0 = X.AnonymousClass07B.A0J
            java.lang.String r1 = X.C178548Ls.A00(r0)
            r0 = 0
            A0I(r11, r5, r1, r0)
            if (r22 == 0) goto L_0x005e
            boolean r0 = r2.A0g
            r7 = 1
            if (r0 != 0) goto L_0x005f
        L_0x005e:
            r7 = 0
        L_0x005f:
            r10 = 0
            r3 = r11
            r9 = r13
            r6 = r12
            r8 = r19
            A0E(r3, r4, r5, r6, r7, r8, r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.A0V(com.fasterxml.jackson.databind.JsonNode, X.8Q6, java.lang.String, java.lang.String, int, int, java.lang.String, X.2lx, java.lang.String, java.lang.String, com.facebook.video.engine.api.VideoPlayerParams):void");
    }

    public void A0W(String str) {
        Integer num;
        try {
            if (str.equals("NONE")) {
                num = AnonymousClass07B.A00;
            } else if (str.equals("ONLY_CORE_ANALYTICS_EVENTS")) {
                num = AnonymousClass07B.A01;
            } else if (str.equals("ALL")) {
                num = AnonymousClass07B.A0C;
            } else {
                throw new IllegalArgumentException(str);
            }
            this.A01 = num;
        } catch (IllegalArgumentException unused) {
            this.A01 = AnonymousClass07B.A00;
        }
    }

    public void A0Y(String str, C179148Oo r8, C179128Om r9, String str2) {
        AnonymousClass8E4 r0;
        String name = r8.name();
        ((C76053ks) AnonymousClass1XX.A02(24, AnonymousClass1Y3.A4M, this.A00)).A0B(str, name, str2);
        C80563sg r5 = (C80563sg) AnonymousClass1XX.A02(28, AnonymousClass1Y3.Ayb, this.A00);
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ap7, r5.A00)).A01(AnonymousClass80H.$const$string(AnonymousClass1Y3.A1O)), AnonymousClass1Y3.A4B);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D(TraceFieldType.VideoId, str);
            r8.name();
            uSLEBaseShape0S0000000.A0D(TraceFieldType.ErrorDomain, name);
            uSLEBaseShape0S0000000.A0D("reliability_label", r9.reliabilityLabel.A00());
            uSLEBaseShape0S0000000.A0D("debug_reason", str2);
            uSLEBaseShape0S0000000.A0D("connection", C80563sg.A01(r5));
            if (((Boolean) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGz, r5.A00)).booleanValue()) {
                r0 = AnonymousClass8E4.TABLET;
            } else {
                r0 = AnonymousClass8E4.MOBILE;
            }
            uSLEBaseShape0S0000000.A0D("device_type", r0.value);
            if (((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, r5.A00)).AbO(500, false)) {
                uSLEBaseShape0S0000000.A0C("elapsed_realtime_ms", Long.valueOf(SystemClock.elapsedRealtime()));
            }
            uSLEBaseShape0S0000000.A06();
        }
    }

    public void A0Z(String str, String str2, C54152lx r11, VideoPlayerParams videoPlayerParams) {
        int i = AnonymousClass1Y3.AvW;
        AnonymousClass0UN r2 = this.A00;
        if (((AnonymousClass8JP) AnonymousClass1XX.A02(27, i, r2)).A0H) {
            AnonymousClass07A.A04((ScheduledExecutorService) AnonymousClass1XX.A02(26, AnonymousClass1Y3.ARn, r2), new AnonymousClass8JM(this, videoPlayerParams, str2, str, r11), 603240051);
        }
    }

    public void A0a(String str, boolean z, AnonymousClass8Q6 r7, C54152lx r8, C53752lJ r9, C179128Om r10, C179138On r11, boolean z2, String str2) {
        String str3;
        String str4;
        String str5;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(11, AnonymousClass1Y3.Ap7, this.A00)).A01("reliability_detection"), AnonymousClass1Y3.A4K);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D(TraceFieldType.VideoId, str);
            uSLEBaseShape0S0000000.A08("is_live", Boolean.valueOf(z));
            String str6 = BuildConfig.FLAVOR;
            if (r7 != null) {
                str3 = r7.value;
            } else {
                str3 = str6;
            }
            uSLEBaseShape0S0000000.A0D("player_type", str3);
            if (r8 != null) {
                str4 = r8.A00();
            } else {
                str4 = str6;
            }
            uSLEBaseShape0S0000000.A0D("player_origin", str4);
            if (r9 != null) {
                str5 = r9.value;
            } else {
                str5 = str6;
            }
            uSLEBaseShape0S0000000.A0D("play_reason", str5);
            uSLEBaseShape0S0000000.A0D("reliability_label", r10.reliabilityLabel.A00());
            if (r11 != null) {
                str6 = r11.value;
            }
            uSLEBaseShape0S0000000.A0D("detection_category", str6);
            uSLEBaseShape0S0000000.A08("is_positive", Boolean.valueOf(z2));
            uSLEBaseShape0S0000000.A0D("details", str2);
            uSLEBaseShape0S0000000.A06();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x008c, code lost:
        if (r10.get() == com.facebook.common.util.TriState.YES) goto L_0x008e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C400420b(X.AnonymousClass1XY r6, X.AnonymousClass09P r7, X.C25051Yd r8, X.C04310Tq r9, X.C04310Tq r10, java.lang.String r11, X.AnonymousClass1YI r12) {
        /*
            r5 = this;
            r5.<init>()
            java.util.concurrent.atomic.AtomicInteger r0 = new java.util.concurrent.atomic.AtomicInteger
            r2 = 0
            r0.<init>(r2)
            r5.A0D = r0
            r5.A02 = r2
            X.0UN r1 = new X.0UN
            r0 = 32
            r1.<init>(r0, r6)
            r5.A00 = r1
            int r0 = X.AnonymousClass1Y3.Asj
            X.0UQ r0 = X.AnonymousClass0UQ.A00(r0, r6)
            r5.A0B = r0
            r5.A05 = r7
            X.1gw r0 = new X.1gw
            r1 = 200(0xc8, float:2.8E-43)
            r0.<init>(r1)
            r5.A0L = r0
            X.1gw r0 = new X.1gw
            r0.<init>(r1)
            r5.A07 = r0
            X.1gw r0 = new X.1gw
            r0.<init>(r1)
            r5.A08 = r0
            X.1gw r0 = new X.1gw
            r0.<init>(r1)
            r5.A09 = r0
            X.1gw r1 = new X.1gw
            r0 = 5
            r1.<init>(r0)
            r5.A06 = r1
            r5.A0A = r12
            r0 = 385(0x181, float:5.4E-43)
            boolean r0 = r12.AbO(r0, r2)
            r5.A0F = r0
            X.1YI r1 = r5.A0A
            r0 = 418(0x1a2, float:5.86E-43)
            boolean r0 = r1.AbO(r0, r2)
            r5.A0J = r0
            r5.A0W(r11)
            X.1YI r1 = r5.A0A
            r0 = 405(0x195, float:5.68E-43)
            boolean r0 = r1.AbO(r0, r2)
            r5.A0M = r0
            X.1YI r1 = r5.A0A
            r0 = 406(0x196, float:5.69E-43)
            boolean r0 = r1.AbO(r0, r2)
            r5.A0H = r0
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            X.C400420b.A0N = r0
            java.lang.Object r0 = r9.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r4 = 1
            if (r0 != 0) goto L_0x008e
            java.lang.Object r3 = r10.get()
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.YES
            r0 = 0
            if (r3 != r1) goto L_0x008f
        L_0x008e:
            r0 = 1
        L_0x008f:
            r5.A0G = r0
            r0 = 567189086144359(0x203db00000767, double:2.80228642160019E-309)
            int r0 = r8.AqL(r0, r4)
            r5.A03 = r0
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r5.A0C = r0
            r0 = 283429186833302(0x101c700000b96, double:1.400326242430547E-309)
            boolean r0 = r8.Aem(r0)
            r0 = r0 ^ r4
            r5.A0K = r0
            r0 = 283429186898839(0x101c700010b97, double:1.400326242754343E-309)
            boolean r0 = r8.Aem(r0)
            r5.A0E = r0
            X.1YI r1 = r5.A0A
            r0 = 573(0x23d, float:8.03E-43)
            boolean r0 = r1.AbO(r0, r2)
            r5.A0I = r0
            r0 = 568812583782954(0x2055500000a2a, double:2.810307565693533E-309)
            int r0 = r8.AqL(r0, r2)
            r5.A04 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.<init>(X.1XY, X.09P, X.1Yd, X.0Tq, X.0Tq, java.lang.String, X.1YI):void");
    }

    public static String A03(C400420b r3, String str, String str2, String str3) {
        if (A0N(str2)) {
            return str2;
        }
        if (A0N(str)) {
            return str;
        }
        if (r3.A05 == null) {
            return null;
        }
        r3.A05.CGS("reason", StringFormatUtil.formatStrLocaleSafe("from %s", str3));
        return null;
    }

    public static void A05(C11670nb r3, AnonymousClass0US r4) {
        C34451pb r2 = (C34451pb) r4.get();
        float A022 = r2.A02();
        if (A022 != -1.0f) {
            r3.A0C("battery_level", Integer.valueOf((int) (A022 * 100.0f)));
        }
        r3.A0D("battery_state", C173777zq.A00(r2.A04()).toLowerCase(Locale.US));
    }

    public static void A0I(C400420b r6, String str, String str2, AnonymousClass26K r9) {
        long j;
        try {
            j = Long.parseLong(str);
        } catch (NumberFormatException unused) {
            j = 0;
        }
        if (j != 0) {
            String str3 = str2;
            AnonymousClass26K r7 = r9;
            if (r9 != null) {
                ((C185414b) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AQl, r6.A00)).AON(C08870g7.A2r, j, str3, BuildConfig.FLAVOR, r7);
            } else {
                ((C185414b) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AQl, r6.A00)).AOH(C08870g7.A2r, j, str2);
            }
        }
    }

    public static boolean A0K(C400420b r3, String str, int i, boolean z) {
        boolean z2 = true;
        if (!Platform.stringIsNullOrEmpty(str)) {
            synchronized (r3.A0L) {
                if (r3.A0L.A03(str) != null) {
                    z2 = false;
                }
                if (z) {
                    r3.A0L.A05(str, str);
                }
            }
            return z2;
        } else if (i != 0) {
            return false;
        } else {
            return true;
        }
    }

    public static void A0E(C400420b r4, C11670nb r5, String str, JsonNode jsonNode, boolean z, C54152lx r9, AnonymousClass8Q6 r10, boolean z2) {
        JsonNode A032;
        String A052;
        C80783t5 r3;
        if (A0M(str)) {
            Preconditions.checkNotNull(r5);
            if (!Platform.stringIsNullOrEmpty(str)) {
                r5.A0D(TraceFieldType.VideoId, str);
            }
            if (r5.A07("notif_tracking") != null) {
                String A072 = r5.A07("notif_tracking");
                JsonNode jsonNode2 = jsonNode;
                if (jsonNode == null || (jsonNode instanceof ArrayNode)) {
                    if (jsonNode == null) {
                        jsonNode2 = new ArrayNode(JsonNodeFactory.instance);
                    }
                    ((ArrayNode) jsonNode2).add(A072);
                    jsonNode = jsonNode2;
                }
            }
            if (jsonNode != null) {
                r5.A0B("tracking", jsonNode);
            }
            r5.A08 = z;
            r5.A0E("sponsored", z);
            if (!(str == null || (r3 = (C80783t5) r4.A07.A03(str)) == null)) {
                HashMap hashMap = new HashMap();
                hashMap.put("initial_event", Boolean.toString(r3.A08));
                hashMap.put("autoplay_failure_reasons", new JSONArray((Collection) r3.A09).toString());
                hashMap.put(C99084oO.$const$string(AnonymousClass1Y3.A13), r3.A06);
                C11670nb.A02(r5, hashMap, false);
            }
            r5.A0D(AnonymousClass80H.$const$string(4), A02(r4));
            if (str != null && A0P.contains(r5.A05)) {
                AtomicInteger atomicInteger = (AtomicInteger) r4.A08.A03(str);
                if (atomicInteger == null) {
                    atomicInteger = new AtomicInteger(0);
                    r4.A08.A05(str, atomicInteger);
                }
                if (r4.A0I) {
                    synchronized (r4.A08) {
                        r5.A09("seq_num", atomicInteger.getAndIncrement());
                        r5.A0A("vpts", SystemClock.elapsedRealtime());
                    }
                } else {
                    r5.A09("seq_num", atomicInteger.getAndIncrement());
                }
            }
            A0D(r4, r5, str);
            int i = r4.A04;
            if (i > 0) {
                r5.A01 = (long) i;
            }
            if (!(r9 == null || r9 == null)) {
                r5.A0D("player_origin", r9.A00);
                r5.A0D("player_suborigin", r9.A01);
            }
            if (r10 != null) {
                r5.A0D("player", r10.value);
            }
            if (r4.A0K && (A052 = ((C07790eA) AnonymousClass1XX.A02(22, AnonymousClass1Y3.APk, r4.A00)).A05()) != null) {
                r5.A0D(C05360Yq.$const$string(273), A052);
            }
            if (r4.A0E && (A032 = ((AnonymousClass14M) AnonymousClass1XX.A02(29, AnonymousClass1Y3.A86, r4.A00)).A03()) != null) {
                r5.A0D("attribution_id", A032.toString());
            }
            if (jsonNode == null) {
                r4.A05.CGS("reason", StringFormatUtil.formatStrLocaleSafe("Missing tracking codes: %s", r5.A05()));
            }
            A0G(r4, r5, z2);
        }
    }

    public USLEBaseShape0S0000000 A0O(AnonymousClass8Q6 r14, String str, ParcelableFormat parcelableFormat, ParcelableFormat parcelableFormat2, int i, long j, int i2, String str2, C54152lx r23, String str3, VideoPlayerParams videoPlayerParams, String str4, String str5, boolean z, boolean z2, long j2, long j3, long j4, long j5, boolean z3, boolean z4, long j6, long j7, float f, String str6, boolean z5, String str7) {
        AnonymousClass8E4 r0;
        C54152lx r2 = r23;
        C80563sg r7 = (C80563sg) AnonymousClass1XX.A02(28, AnonymousClass1Y3.Ayb, this.A00);
        String str8 = str2;
        boolean contains = A0N.contains(str8);
        String str9 = null;
        if (A0M(str8)) {
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ap7, r7.A00)).A01(C05360Yq.$const$string(AnonymousClass1Y3.A51)), AnonymousClass1Y3.A4f);
            if (r23 == null) {
                r2 = C54152lx.A0P;
            }
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0D(TraceFieldType.VideoId, str8);
                uSLEBaseShape0S0000000.A0D("player", r14.value);
                uSLEBaseShape0S0000000.A0D("player_origin", r2.A00);
                uSLEBaseShape0S0000000.A0D("player_suborigin", r2.A01);
                uSLEBaseShape0S0000000.A0D("data_connection_quality", String.valueOf(((FbDataConnectionManager) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B3e, r7.A00)).A08()));
                uSLEBaseShape0S0000000.A08("is_network_connected", Boolean.valueOf(((C09340h3) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AeN, r7.A00)).A0Q()));
                uSLEBaseShape0S0000000.A08("in_play_stall", Boolean.valueOf(z5));
                VideoPlayerParams videoPlayerParams2 = videoPlayerParams;
                uSLEBaseShape0S0000000.A08("playback_is_broadcast", Boolean.valueOf(videoPlayerParams2.A0V));
                uSLEBaseShape0S0000000.A08("is_templated_manifest", Boolean.valueOf(z));
                uSLEBaseShape0S0000000.A0C("fb_bandwidth", Long.valueOf(((long) ((FbDataConnectionManager) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B3e, r7.A00)).A03()) * 1000));
                uSLEBaseShape0S0000000.A0D("streaming_format", str);
                uSLEBaseShape0S0000000.A0D(TraceFieldType.StreamType, str4);
                uSLEBaseShape0S0000000.A09("absolute_video_time_position", Double.valueOf(((double) j) / 1000.0d));
                uSLEBaseShape0S0000000.A09("video_last_start_time_position", Double.valueOf(((double) i2) / 1000.0d));
                uSLEBaseShape0S0000000.A0D("video_play_reason", str3);
                uSLEBaseShape0S0000000.A08("is_fbms", Boolean.valueOf(z2));
                uSLEBaseShape0S0000000.A0C("live_edge_position_ms", Long.valueOf(j2));
                uSLEBaseShape0S0000000.A0C("live_manifest_last_video_frame_time_ms", Long.valueOf(j3));
                uSLEBaseShape0S0000000.A0C("live_stale_manifest_count", Long.valueOf(j4));
                uSLEBaseShape0S0000000.A08("suspected_microstall_from_setting_surface", Boolean.valueOf(z3));
                boolean z6 = true;
                if (((Boolean) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGz, r7.A00)).booleanValue()) {
                    r0 = AnonymousClass8E4.TABLET;
                } else {
                    r0 = AnonymousClass8E4.MOBILE;
                }
                uSLEBaseShape0S0000000.A0D("device_type", r0.value);
                uSLEBaseShape0S0000000.A0C(AnonymousClass80H.$const$string(AnonymousClass1Y3.A1S), Long.valueOf((long) ((C26881cI) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BHQ, r7.A00)).A09()));
                uSLEBaseShape0S0000000.A0C("screen_height", Long.valueOf((long) ((C26881cI) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BHQ, r7.A00)).A07()));
                uSLEBaseShape0S0000000.A0D("debug_reason", str7);
                uSLEBaseShape0S0000000.A08(AnonymousClass80H.$const$string(20), Boolean.valueOf(z4));
                uSLEBaseShape0S0000000.A08("sponsored", Boolean.valueOf(videoPlayerParams2.A0g));
                uSLEBaseShape0S0000000.A0C("video_buffer_position", Long.valueOf(j6));
                uSLEBaseShape0S0000000.A0C("audio_buffer_position", Long.valueOf(j7));
                long j8 = 0;
                if (j5 != 0) {
                    j8 = SystemClock.elapsedRealtime() - j5;
                }
                uSLEBaseShape0S0000000.A0C("time_ms_since_last_manifest_refresh", Long.valueOf(j8));
                uSLEBaseShape0S0000000.A09("mos_score", Double.valueOf((double) f));
                uSLEBaseShape0S0000000.A0D("available_video_bitrates", str6);
                uSLEBaseShape0S0000000.A08("playback_is_live_streaming", Boolean.valueOf(videoPlayerParams2.A0a));
                uSLEBaseShape0S0000000.A08("is_servable_via_fbms", Boolean.valueOf(videoPlayerParams2.A0e));
                uSLEBaseShape0S0000000.A08("is_spherical_fallback", Boolean.valueOf(videoPlayerParams2.A0f));
                uSLEBaseShape0S0000000.A08("has_been_live", Boolean.valueOf(contains));
                if (videoPlayerParams2.A00() != null) {
                    uSLEBaseShape0S0000000.A0D("projection", videoPlayerParams2.A00().toString());
                }
                if (videoPlayerParams2.A01() != null) {
                    uSLEBaseShape0S0000000.A0D("audio_ch_conf", videoPlayerParams2.A01().toString());
                }
                int i3 = videoPlayerParams2.A0B;
                if (i3 != -1) {
                    uSLEBaseShape0S0000000.A0C("story_position", Long.valueOf((long) i3));
                }
                uSLEBaseShape0S0000000.A0D("data_latency_quality", String.valueOf(((FbDataConnectionManager) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B3e, r7.A00)).A07()));
                uSLEBaseShape0S0000000.A0C("fb_latency", Long.valueOf((long) ((FbDataConnectionManager) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B3e, r7.A00)).A04()));
                uSLEBaseShape0S0000000.A0C("video_bandwidth", Long.valueOf(((AnonymousClass222) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BMC, r7.A00)).A0E.A01()));
                if (((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, r7.A00)).AbO(418, false)) {
                    String str10 = str5;
                    if (str5 != null) {
                        uSLEBaseShape0S0000000.A0D("url", str10);
                    }
                }
                int i4 = i;
                uSLEBaseShape0S0000000.A0C("available_qualities", Long.valueOf((long) i4));
                if (i4 <= 1) {
                    z6 = false;
                }
                uSLEBaseShape0S0000000.A08("is_abr_enabled", Boolean.valueOf(z6));
                ParcelableFormat parcelableFormat3 = parcelableFormat;
                if (parcelableFormat != null) {
                    uSLEBaseShape0S0000000.A0D("representation_id", parcelableFormat3.id);
                    uSLEBaseShape0S0000000.A0D("codec", parcelableFormat3.codecs);
                    uSLEBaseShape0S0000000.A0C("video_width", Long.valueOf((long) parcelableFormat3.width));
                    uSLEBaseShape0S0000000.A0C("video_height", Long.valueOf((long) parcelableFormat3.height));
                    uSLEBaseShape0S0000000.A0C(TraceFieldType.Bitrate, Long.valueOf((long) parcelableFormat3.bitrate));
                    uSLEBaseShape0S0000000.A0D("quality_label", parcelableFormat3.fbQualityLabel);
                    uSLEBaseShape0S0000000.A0D("codec", parcelableFormat3.codecs);
                    uSLEBaseShape0S0000000.A0D("mime", parcelableFormat3.mimeType);
                    ParcelableFormat parcelableFormat4 = parcelableFormat2;
                    if (parcelableFormat2 != null) {
                        str9 = parcelableFormat4.id;
                    }
                    uSLEBaseShape0S0000000.A0D("audio_representation_id", str9);
                }
                return uSLEBaseShape0S0000000;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a6, code lost:
        if (r3.A0g == false) goto L_0x00a8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0S(com.fasterxml.jackson.databind.JsonNode r13, X.AnonymousClass8Q6 r14, X.AnonymousClass8Q6 r15, java.lang.String r16, X.C54152lx r17, java.lang.String r18, int r19, int r20, com.facebook.video.engine.api.VideoPlayerParams r21, X.AnonymousClass8ES r22, java.util.Map r23, boolean r24) {
        /*
            r12 = this;
            X.8Q6 r2 = X.AnonymousClass8Q6.A07
            r1 = 1
            r10 = r14
            if (r15 != r2) goto L_0x000a
            X.8Q6 r0 = X.AnonymousClass8Q6.A04
            if (r14 == r0) goto L_0x0010
        L_0x000a:
            X.8Q6 r0 = X.AnonymousClass8Q6.A04
            if (r15 != r0) goto L_0x00f4
            if (r14 != r2) goto L_0x00f4
        L_0x0010:
            r0 = 1
        L_0x0011:
            r0 = r0 ^ r1
            r3 = r21
            r6 = r16
            if (r0 == 0) goto L_0x0048
            X.1YI r2 = r12.A0A
            r1 = 773(0x305, float:1.083E-42)
            r0 = 0
            boolean r0 = r2.AbO(r1, r0)
            r2 = 0
            if (r0 == 0) goto L_0x00e7
            if (r24 == 0) goto L_0x00e7
        L_0x0026:
            X.8Q6 r0 = X.AnonymousClass8Q6.A02
            if (r14 == r0) goto L_0x00c7
            if (r15 == r0) goto L_0x00c7
            java.lang.String r4 = "video"
        L_0x002e:
            r5 = 1
            if (r2 == 0) goto L_0x00b4
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r0 = "content_id"
            r2.put(r0, r6)
            int r1 = X.AnonymousClass1Y3.A5w
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.146 r0 = (X.AnonymousClass146) r0
            r0.A0G(r4, r5, r2)
        L_0x0048:
            if (r15 == 0) goto L_0x00b2
            java.lang.String r1 = r15.value
        L_0x004c:
            X.0nb r5 = new X.0nb
            java.lang.Integer r0 = X.AnonymousClass07B.A0B
            java.lang.String r0 = X.C178548Ls.A00(r0)
            r5.<init>(r0)
            java.lang.String r0 = "previous_player_format"
            r5.A0D(r0, r1)
            r0 = r19
            float r0 = (float) r0
            r4 = 1148846080(0x447a0000, float:1000.0)
            float r0 = r0 / r4
            double r0 = (double) r0
            java.lang.String r2 = "video_time_position"
            r5.A08(r2, r0)
            r0 = r20
            float r0 = (float) r0
            float r0 = r0 / r4
            double r0 = (double) r0
            java.lang.String r2 = "video_last_start_time_position"
            r5.A08(r2, r0)
            java.lang.String r0 = "video_play_reason"
            r1 = r18
            r5.A0D(r0, r1)
            r0 = 0
            r1 = r23
            X.C11670nb.A02(r5, r1, r0)
            if (r22 == 0) goto L_0x009b
            r2 = 18
            int r1 = X.AnonymousClass1Y3.ASa
            X.0UN r0 = r12.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.8EM r1 = (X.AnonymousClass8EM) r1
            r0 = 0
            boolean r0 = r1.A02(r0)
            if (r0 == 0) goto L_0x009b
            X.8EV r0 = r22.A03()
            A09(r5, r0)
        L_0x009b:
            A07(r5, r3, r6)
            A0H(r12, r6, r5)
            if (r21 == 0) goto L_0x00a8
            boolean r0 = r3.A0g
            r8 = 1
            if (r0 != 0) goto L_0x00a9
        L_0x00a8:
            r8 = 0
        L_0x00a9:
            r11 = 0
            r4 = r12
            r9 = r17
            r7 = r13
            A0E(r4, r5, r6, r7, r8, r9, r10, r11)
            return
        L_0x00b2:
            r1 = 0
            goto L_0x004c
        L_0x00b4:
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            int r1 = X.AnonymousClass1Y3.A5w
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.146 r0 = (X.AnonymousClass146) r0
            r0.A0F(r4, r2)
            goto L_0x0048
        L_0x00c7:
            boolean r0 = r3.A0X
            if (r0 == 0) goto L_0x00cf
            java.lang.String r4 = "gaming_video_fullscreen_player"
            goto L_0x002e
        L_0x00cf:
            java.lang.String r1 = r3.A0O
            r0 = 0
            if (r1 == 0) goto L_0x00d5
            r0 = 1
        L_0x00d5:
            if (r0 == 0) goto L_0x00db
            java.lang.String r4 = "watch_party_fullscreen_player"
            goto L_0x002e
        L_0x00db:
            boolean r0 = r3.A0V
            if (r0 == 0) goto L_0x00e3
            java.lang.String r4 = "live_video_fullscreen_player"
            goto L_0x002e
        L_0x00e3:
            java.lang.String r4 = "vod_fullscreen_player"
            goto L_0x002e
        L_0x00e7:
            X.8Q6 r1 = X.AnonymousClass8Q6.A04
            if (r14 == r1) goto L_0x0026
            X.8Q6 r0 = X.AnonymousClass8Q6.A01
            if (r14 != r0) goto L_0x00f1
            if (r15 != r1) goto L_0x0026
        L_0x00f1:
            r2 = 1
            goto L_0x0026
        L_0x00f4:
            r0 = 0
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C400420b.A0S(com.fasterxml.jackson.databind.JsonNode, X.8Q6, X.8Q6, java.lang.String, X.2lx, java.lang.String, int, int, com.facebook.video.engine.api.VideoPlayerParams, X.8ES, java.util.Map, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void A0X(String str, AnonymousClass8Q6 r24, String str2, VideoDataSource videoDataSource, String str3, C54152lx r28, Optional optional, Optional optional2, String str4, VideoPlayerParams videoPlayerParams, Exception exc, String str5) {
        String str6;
        String str7;
        String str8;
        String str9;
        Uri uri;
        String str10 = str;
        Object[] objArr = {"reason", str10};
        Exception exc2 = exc;
        if (exc == null) {
            C010708t.A0O("VideoLoggingUtils", "%s: %s", objArr);
        } else {
            C010708t.A0U("VideoLoggingUtils", exc2, "%s: %s", objArr);
        }
        AnonymousClass8Q6 r13 = r24;
        if (r24 != null) {
            str6 = r13.value;
        } else {
            str6 = null;
        }
        VideoDataSource videoDataSource2 = videoDataSource;
        if (videoDataSource == null || (uri = videoDataSource2.A03) == null) {
            str7 = null;
        } else {
            str7 = uri.toString();
        }
        C76053ks r11 = (C76053ks) AnonymousClass1XX.A02(24, AnonymousClass1Y3.A4M, this.A00);
        if (optional.isPresent()) {
            str8 = ((C179148Oo) optional.get()).value;
        } else {
            str8 = null;
        }
        if (optional2.isPresent()) {
            str9 = String.valueOf(((C179128Om) optional2.get()).reliabilityLabel.A00());
        } else {
            str9 = null;
        }
        String str11 = str2;
        C54152lx r8 = r28;
        String str12 = str4;
        if ((C76053ks.A09(r11) || C76053ks.A07()) && str2 != null) {
            int hashCode = str11.hashCode();
            r11.A0E.markerStart(1900578, hashCode);
            HashMap hashMap = new HashMap();
            hashMap.put("state", C155627Hn.A00(AnonymousClass07B.A03));
            hashMap.put("time_ms", Long.toString(SystemClock.elapsedRealtime()));
            hashMap.put(TraceFieldType.VideoId, str11);
            hashMap.put("player_id", "0");
            if (str4 != null) {
                hashMap.put("play_version", str12);
            }
            if (str8 != null) {
                hashMap.put(TraceFieldType.ErrorDomain, str8);
            }
            if (str9 != null) {
                hashMap.put(TraceFieldType.ErrorCode, str9);
            }
            if (str != null) {
                hashMap.put(AnonymousClass80H.$const$string(AnonymousClass1Y3.A1A), str10);
            }
            if (r28 != null) {
                hashMap.put("player_origin", r8.A00());
            }
            if (r24 != null) {
                hashMap.put("player_type", r13.toString());
            }
            C76053ks.A02(r11, 1900578, hashCode, hashMap);
            r11.A0E.markerEnd(1900578, hashCode, (short) 2);
            C76053ks.A04(r11, 2);
        }
        C11670nb r2 = new C11670nb(C178548Ls.A00(AnonymousClass07B.A07));
        r2.A0D("reason", str10);
        r2.A0D("player", str6);
        r2.A0D(TraceFieldType.VideoId, str11);
        r2.A0D("url", str7);
        r2.A0D("video_play_reason", A03(this, null, str3, "logVideoException"));
        r2.A0D("product", Build.PRODUCT);
        r2.A0D("device", Build.DEVICE);
        r2.A0D("board", Build.BOARD);
        r2.A0D(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1U), Build.MANUFACTURER);
        r2.A0D("brand", Build.BRAND);
        r2.A0D("model", Build.MODEL);
        r2.A0D("player_version", str12);
        if (optional.isPresent()) {
            r2.A0D(TraceFieldType.ErrorDomain, ((C179148Oo) optional.get()).value);
        }
        if (optional2.isPresent()) {
            r2.A09(TraceFieldType.ErrorCode, ((C179128Om) optional2.get()).errorCode);
            r2.A0D("reliability_label", ((C179128Om) optional2.get()).reliabilityLabel.A00());
        }
        String $const$string = TurboLoader.Locator.$const$string(13);
        String str13 = str5;
        if (str5 != null) {
            r2.A0D($const$string, str13);
        } else if (exc != null) {
            r2.A0D($const$string, Log.getStackTraceString(exc2));
        }
        A07(r2, videoPlayerParams, str11);
        if (r28 != null) {
            r2.A0D("player_origin", r8.A00);
            r2.A0D("player_suborigin", r8.A01);
        }
        A0G(this, r2, false);
    }
}
