package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0a9  reason: invalid class name and case insensitive filesystem */
public final class C05680a9 {
    private static volatile C05680a9 A03;
    private final C04310Tq A00;
    private final C04310Tq A01;
    private final C04310Tq A02;

    public static final C05680a9 A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C05680a9.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C05680a9(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C05680a9(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0VB.A00(AnonymousClass1Y3.ALU, r2);
        this.A01 = AnonymousClass0VB.A00(AnonymousClass1Y3.BAd, r2);
        this.A02 = AnonymousClass0VB.A00(AnonymousClass1Y3.Ah6, r2);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        if (r8.equals("MESSENGER_DISCOVERY_GAMES_M4") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
        if (r8.equals("MESSENGER_DISCOVERY_BUSINESSES") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0043, code lost:
        if (r8.equals("WORKCHAT_DISCOVERY_BOTS") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004d, code lost:
        if (r8.equals("MESSENGER_DISCOVERY_GAMES") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0057, code lost:
        if (r8.equals("MESSENGER_DISCOVERY_M3_BUSINESSES") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0061, code lost:
        if (r8.equals("MESSENGER_DISCOVERY_BOTS") == false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C09470hQ A01(java.lang.String r8) {
        /*
            r7 = this;
            int r0 = r8.hashCode()
            r5 = 5
            r4 = 4
            r3 = 3
            r2 = 2
            r1 = 1
            switch(r0) {
                case -1413460057: goto L_0x005a;
                case -1364830452: goto L_0x0050;
                case -863395354: goto L_0x0046;
                case -862177807: goto L_0x003c;
                case -201186231: goto L_0x0032;
                case 1148238464: goto L_0x0028;
                default: goto L_0x000c;
            }
        L_0x000c:
            r6 = -1
        L_0x000d:
            if (r6 == 0) goto L_0x0025
            if (r6 == r1) goto L_0x0025
            if (r6 == r2) goto L_0x0025
            if (r6 == r3) goto L_0x0025
            if (r6 == r4) goto L_0x0022
            if (r6 == r5) goto L_0x0022
            X.0Tq r0 = r7.A00
        L_0x001b:
            java.lang.Object r0 = r0.get()
            X.0hQ r0 = (X.C09470hQ) r0
            return r0
        L_0x0022:
            X.0Tq r0 = r7.A02
            goto L_0x001b
        L_0x0025:
            X.0Tq r0 = r7.A01
            goto L_0x001b
        L_0x0028:
            java.lang.String r0 = "MESSENGER_DISCOVERY_GAMES_M4"
            boolean r0 = r8.equals(r0)
            r6 = 5
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x0032:
            java.lang.String r0 = "MESSENGER_DISCOVERY_BUSINESSES"
            boolean r0 = r8.equals(r0)
            r6 = 3
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x003c:
            java.lang.String r0 = "WORKCHAT_DISCOVERY_BOTS"
            boolean r0 = r8.equals(r0)
            r6 = 1
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x0046:
            java.lang.String r0 = "MESSENGER_DISCOVERY_GAMES"
            boolean r0 = r8.equals(r0)
            r6 = 4
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x0050:
            java.lang.String r0 = "MESSENGER_DISCOVERY_M3_BUSINESSES"
            boolean r0 = r8.equals(r0)
            r6 = 2
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x005a:
            java.lang.String r0 = "MESSENGER_DISCOVERY_BOTS"
            boolean r0 = r8.equals(r0)
            r6 = 0
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05680a9.A01(java.lang.String):X.0hQ");
    }
}
