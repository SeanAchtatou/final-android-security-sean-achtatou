package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.1mX  reason: invalid class name and case insensitive filesystem */
public final class C32911mX extends C15730vo {
    public final /* synthetic */ RecyclerView A00;

    public C32911mX(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    public static void A00(C32911mX r2) {
        if (RecyclerView.A1A) {
            RecyclerView recyclerView = r2.A00;
            if (recyclerView.A0V && recyclerView.A04) {
                C15320v6.postOnAnimation(recyclerView, recyclerView.A0q);
                return;
            }
        }
        RecyclerView recyclerView2 = r2.A00;
        recyclerView2.A01 = true;
        recyclerView2.requestLayout();
    }
}
