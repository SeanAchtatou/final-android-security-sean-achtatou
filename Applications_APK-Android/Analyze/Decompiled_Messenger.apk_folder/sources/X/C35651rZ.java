package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.1rZ  reason: invalid class name and case insensitive filesystem */
public final class C35651rZ {
    private static final char A00 = File.separatorChar;

    public static String A00(Context context, boolean z, String str, String str2, String str3) {
        String str4;
        char c;
        String str5;
        if (z) {
            str4 = context.getCacheDir().getAbsolutePath();
            c = A00;
            str5 = "compactdisk";
        } else {
            str4 = context.getApplicationInfo().dataDir;
            c = A00;
            str5 = "app_compactdisk";
        }
        String A07 = AnonymousClass08S.A07(str4, c, str5);
        StringBuilder sb = new StringBuilder();
        sb.append(A07);
        char c2 = A00;
        sb.append(c2);
        sb.append(str);
        sb.append(c2);
        sb.append(str2);
        sb.append(c2);
        sb.append(str3);
        sb.append(c2);
        sb.append("storage");
        return sb.toString();
    }
}
