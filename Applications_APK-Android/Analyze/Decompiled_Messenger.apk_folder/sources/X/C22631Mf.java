package X;

import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Mf  reason: invalid class name and case insensitive filesystem */
public final class C22631Mf implements C22641Mg {
    public static final Integer A06 = AnonymousClass07B.A0C;
    private static volatile C22631Mf A07;
    private AnonymousClass0UN A00;
    private final AnonymousClass0VL A01;
    private final AnonymousClass0VL A02;
    private final Executor A03;
    private final Executor A04;
    private final Executor A05;

    public static final C22631Mf A00(AnonymousClass1XY r9) {
        if (A07 == null) {
            synchronized (C22631Mf.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r9);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r9.getApplicationInjector();
                        A07 = new C22631Mf(applicationInjector, AnonymousClass1MY.A06(applicationInjector), AnonymousClass1MY.A0a(applicationInjector), AnonymousClass1MY.A0c(applicationInjector), AnonymousClass1MY.A07(applicationInjector), AnonymousClass1MY.A0b(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public Executor AaN() {
        return this.A01;
    }

    public Executor AaO() {
        return this.A03;
    }

    public Executor AaP() {
        return this.A02;
    }

    public Executor AaQ() {
        return this.A04;
    }

    public Executor AaR() {
        return (C06480bZ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ACz, this.A00);
    }

    public Executor AaT() {
        if (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282063389000783L)) {
            return this.A05;
        }
        return this.A04;
    }

    private C22631Mf(AnonymousClass1XY r3, C22661Mi r4, AnonymousClass0VL r5, AnonymousClass0VL r6, C22661Mi r7, AnonymousClass0VL r8) {
        this.A00 = new AnonymousClass0UN(2, r3);
        Integer num = A06;
        this.A04 = r4.AaS(num);
        this.A03 = r5;
        this.A01 = r6;
        this.A05 = r7.AaS(num);
        this.A02 = r8;
    }
}
