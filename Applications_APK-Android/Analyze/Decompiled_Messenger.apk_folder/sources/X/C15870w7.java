package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.messaging.chatheads.intents.ChatHeadsInterstitialNuxFragment;
import com.facebook.messaging.model.threadkey.ThreadKey;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0w7  reason: invalid class name and case insensitive filesystem */
public final class C15870w7 {
    private static volatile C15870w7 A02;
    public AnonymousClass0UN A00;
    public Runnable A01;

    public static final C15870w7 A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C15870w7.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C15870w7(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static void A01(C13060qW r2) {
        new ChatHeadsInterstitialNuxFragment().A25(r2, "chat_heads_interstitial_tag");
    }

    public static void A02(C15870w7 r4) {
        int i = AnonymousClass1Y3.BS0;
        AnonymousClass0UN r3 = r4.A00;
        ((C401420l) AnonymousClass1XX.A02(2, i, r3)).A01((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, r3));
        C44182Hf r1 = (C44182Hf) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AHe, r4.A00);
        C44182Hf.A03(r1, C44182Hf.A00(r1, C18030zx.A0A), false);
    }

    public static void A03(C15870w7 r4) {
        int i = AnonymousClass1Y3.BS0;
        AnonymousClass0UN r3 = r4.A00;
        ((C401420l) AnonymousClass1XX.A02(2, i, r3)).A01((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, r3));
        C44182Hf r1 = (C44182Hf) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AHe, r4.A00);
        C44182Hf.A03(r1, C44182Hf.A00(r1, C18030zx.A0B), false);
    }

    public static void A04(C15870w7 r4, ThreadKey threadKey, String str) {
        int i = AnonymousClass1Y3.BS0;
        AnonymousClass0UN r3 = r4.A00;
        ((C401420l) AnonymousClass1XX.A02(2, i, r3)).A01((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, r3));
        ((C44182Hf) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AHe, r4.A00)).A04(threadKey, str, null, null);
    }

    public static void A05(C15870w7 r4, ThreadKey threadKey, String str) {
        int i = AnonymousClass1Y3.BS0;
        AnonymousClass0UN r3 = r4.A00;
        ((C401420l) AnonymousClass1XX.A02(2, i, r3)).A01((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, r3));
        C44182Hf r42 = (C44182Hf) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AHe, r4.A00);
        Intent A002 = C44182Hf.A00(r42, C18030zx.A07);
        if (threadKey.A0O()) {
            A002.putExtra(C18030zx.A0R, threadKey.A02);
        } else {
            A002.putExtra(C18030zx.A0N, threadKey.A0G());
        }
        A002.putExtra(C18030zx.A0S, str);
        C44182Hf.A03(r42, A002, false);
    }

    public static void A06(C15870w7 r4, String str, String str2, String str3) {
        int i = AnonymousClass1Y3.BS0;
        AnonymousClass0UN r3 = r4.A00;
        ((C401420l) AnonymousClass1XX.A02(2, i, r3)).A01((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, r3));
        ((C44182Hf) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AHe, r4.A00)).A05(str, str2, str3);
    }

    public void A07() {
        Runnable runnable;
        if (((C12460pP) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AWJ, this.A00)).A05() && (runnable = this.A01) != null) {
            runnable.run();
        }
        this.A01 = null;
    }

    public void A08(ThreadKey threadKey, C13060qW r5, String str) {
        if (((C12460pP) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AWJ, this.A00)).A05()) {
            A04(this, threadKey, str);
            return;
        }
        this.A01 = new AnonymousClass5B5(this, threadKey, str);
        A01(r5);
    }

    private C15870w7(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }
}
