package X;

import android.content.Context;
import android.os.Process;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0sC  reason: invalid class name and case insensitive filesystem */
public final class C13860sC extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.messaging.optimisticinflation.SimpleBackgroundInflater$PreinflateThread";
    public final C07380dK A00;
    public final View[] A01;
    private final Context A02;
    private final C10920l5 A03;
    private final AtomicInteger A04;
    public volatile int A05 = 0;
    public volatile int A06 = 0;

    public C13860sC(C07380dK r3, Context context, C10920l5 r5, int i, AtomicInteger atomicInteger) {
        super(AnonymousClass08S.A0J("Preinflate ", r5.A04));
        this.A00 = r3;
        this.A02 = context;
        this.A03 = r5;
        this.A01 = new View[i];
        this.A04 = atomicInteger;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void run() {
        int i;
        try {
            Process.setThreadPriority(AnonymousClass0V7.FOREGROUND.mAndroidThreadPriority);
        } catch (Throwable unused) {
            this.A00.A03("android.messenger.preinflater.failed_to_set_thread_priority");
        }
        C10920l5 r3 = this.A03;
        Context context = this.A02;
        int i2 = r3.A01;
        if (i2 != 0) {
            context = new ContextThemeWrapper(context, i2);
        }
        C13830s9 r0 = r3.A03;
        if (r0 != null) {
            context = r0.CNV(context);
        }
        LayoutInflater from = LayoutInflater.from(context);
        while (this.A05 < this.A01.length) {
            if (isInterrupted()) {
                this.A00.A03("android.messenger.preinflater.interrupted");
                return;
            } else if (this.A04.get() > 2) {
                C010708t.A0K("SimpleBackgroundInflater", "many crashes in preinflation");
                return;
            } else {
                C005505z.A05("preinflate %s", this.A03.A04, -1680744502);
                try {
                    View[] viewArr = this.A01;
                    int i3 = this.A05;
                    this.A05 = i3 + 1;
                    viewArr[i3] = from.inflate(this.A03.A00, (ViewGroup) null, false);
                    this.A00.A03("android.messenger.preinflater.view_inflated");
                    i = 1164243249;
                } catch (Exception e) {
                    C010708t.A0L("SimpleBackgroundInflater", "exception preinflating view", e);
                    this.A00.A03(AnonymousClass08S.A0J("android.messenger.preinflater.inflation_crashed.", this.A03.A04));
                    this.A04.incrementAndGet();
                    i = 113222235;
                } catch (Throwable th) {
                    C005505z.A00(82618242);
                    throw th;
                }
                C005505z.A00(i);
            }
        }
    }
}
