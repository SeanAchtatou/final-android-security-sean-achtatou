package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0SR  reason: invalid class name */
public final class AnonymousClass0SR extends Enum {
    private static final /* synthetic */ AnonymousClass0SR[] A00;
    public static final AnonymousClass0SR A01;
    public static final AnonymousClass0SR A02;
    public static final AnonymousClass0SR A03;
    public static final AnonymousClass0SR A04;
    public static final AnonymousClass0SR A05;
    public static final AnonymousClass0SR A06;
    public static final AnonymousClass0SR A07;
    public static final AnonymousClass0SR A08;
    public static final AnonymousClass0SR A09;
    public static final AnonymousClass0SR A0A;
    public static final AnonymousClass0SR A0B;
    public static final AnonymousClass0SR A0C;
    public static final AnonymousClass0SR A0D;
    public static final AnonymousClass0SR A0E;
    public static final AnonymousClass0SR A0F;

    static {
        AnonymousClass0SR r15 = new AnonymousClass0SR("DATA_INVALID", 0);
        A04 = r15;
        AnonymousClass0SR r14 = new AnonymousClass0SR("DATA_EXPIRED", 1);
        A03 = r14;
        AnonymousClass0SR r13 = new AnonymousClass0SR("NOTIF_ACKED", 2);
        A05 = r13;
        AnonymousClass0SR r12 = new AnonymousClass0SR("NOTIF_DUPED", 3);
        A06 = r12;
        AnonymousClass0SR r11 = new AnonymousClass0SR("BROADCAST_SENT", 4);
        A02 = r11;
        AnonymousClass0SR r10 = new AnonymousClass0SR("BROADCAST_FAILED", 5);
        A01 = r10;
        AnonymousClass0SR r9 = new AnonymousClass0SR("PACKAGE_FAILED", 6);
        A08 = r9;
        AnonymousClass0SR r8 = new AnonymousClass0SR("PACKAGE_INVALID", 7);
        A0A = r8;
        AnonymousClass0SR r7 = new AnonymousClass0SR("PACKAGE_UNSUPPORTED", 8);
        A0E = r7;
        AnonymousClass0SR r6 = new AnonymousClass0SR("PACKAGE_INCOMPATIBLE", 9);
        A09 = r6;
        AnonymousClass0SR r5 = new AnonymousClass0SR("PACKAGE_NOT_INSTALLED", 10);
        A0B = r5;
        AnonymousClass0SR r4 = new AnonymousClass0SR("PACKAGE_DISABLED", 11);
        A07 = r4;
        AnonymousClass0SR r3 = new AnonymousClass0SR("PACKAGE_NOT_TRUSTED", 12);
        A0C = r3;
        AnonymousClass0SR r2 = new AnonymousClass0SR("PACKAGE_TRUSTED", 13);
        A0D = r2;
        AnonymousClass0SR r16 = new AnonymousClass0SR("PROCESSOR_FAILED", 14);
        A0F = r16;
        AnonymousClass0SR r29 = r16;
        AnonymousClass0SR r26 = r4;
        AnonymousClass0SR r25 = r5;
        AnonymousClass0SR r24 = r6;
        AnonymousClass0SR r23 = r7;
        AnonymousClass0SR r22 = r8;
        AnonymousClass0SR r21 = r9;
        AnonymousClass0SR r20 = r10;
        AnonymousClass0SR r19 = r11;
        AnonymousClass0SR r18 = r12;
        AnonymousClass0SR r17 = r13;
        AnonymousClass0SR r162 = r14;
        A00 = new AnonymousClass0SR[]{r15, r162, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r3, r2, r29};
    }

    public static AnonymousClass0SR valueOf(String str) {
        return (AnonymousClass0SR) Enum.valueOf(AnonymousClass0SR.class, str);
    }

    public static AnonymousClass0SR[] values() {
        return (AnonymousClass0SR[]) A00.clone();
    }

    public boolean A00() {
        if (this == A04 || this == A03 || this == A0A || this == A0E || this == A09 || this == A0B || this == A07 || this == A0C) {
            return true;
        }
        return false;
    }

    public boolean A01() {
        if (this == A05 || this == A06 || this == A02) {
            return true;
        }
        return false;
    }

    private AnonymousClass0SR(String str, int i) {
    }
}
