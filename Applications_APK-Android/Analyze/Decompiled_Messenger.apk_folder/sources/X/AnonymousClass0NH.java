package X;

import android.content.Context;
import android.os.Build;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.Set;

/* renamed from: X.0NH  reason: invalid class name */
public abstract class AnonymousClass0NH {
    public static final Object A03 = new Object();
    public final Context A00;
    public final AnonymousClass0Jp A01;
    public final AnonymousClass07M A02;

    public static File A00(File file, File[] fileArr, String str) {
        if (fileArr != null) {
            String A0J = AnonymousClass08S.A0J(file.getAbsolutePath(), str);
            for (File file2 : fileArr) {
                if (file2.getAbsolutePath().equals(A0J)) {
                    return file2;
                }
            }
        }
        return null;
    }

    public static void A02(AnonymousClass0NH r3, File file) {
        boolean z;
        IOException e = null;
        if (Build.VERSION.SDK_INT >= 26) {
            try {
                A05(file);
                z = true;
            } catch (IOException e2) {
                e = e2;
                z = false;
            }
        } else {
            z = file.delete();
        }
        if (z) {
            file.getAbsolutePath();
        } else {
            r3.A09(file, e);
        }
    }

    public abstract Boolean A07();

    public abstract void A08(AnonymousClass07N r1);

    public boolean A0A() {
        return false;
    }

    public boolean A0B() {
        return false;
    }

    public boolean A0C() {
        return false;
    }

    public abstract boolean A0D();

    public boolean A0E(AnonymousClass07N r2) {
        return false;
    }

    public abstract boolean A0F(AnonymousClass07N r1);

    public static void A03(AnonymousClass0NH r2, Exception exc, File file) {
        AnonymousClass0Jo ALd = r2.A01.ALd();
        StringWriter stringWriter = new StringWriter();
        exc.printStackTrace(new PrintWriter(stringWriter));
        ALd.ANO("logParseError", stringWriter.toString());
        A01(ALd, file);
        ALd.BIw();
    }

    public static void A04(AnonymousClass0NH r7, File[] fileArr, String str, Set set) {
        String absolutePath;
        int lastIndexOf;
        if (fileArr != null) {
            for (File file : fileArr) {
                if (file.exists() && (lastIndexOf = (absolutePath = file.getAbsolutePath()).lastIndexOf(str)) != -1 && !set.contains(absolutePath.substring(0, lastIndexOf))) {
                    C010708t.A0O("AppStateReporter", "Unpaired extra log file: %s", file.getAbsolutePath());
                    A02(r7, file);
                }
            }
        }
    }

    public void A09(File file, IOException iOException) {
        C010708t.A0U("AppStateReporter", iOException, "Failed to delete app state log file path: %s", file.getAbsolutePath());
    }

    public AnonymousClass0NH(Context context, AnonymousClass0Jp r5) {
        AnonymousClass07M r0;
        this.A00 = context;
        try {
            r0 = new AnonymousClass07M();
        } catch (Exception e) {
            C010708t.A0L("AppStateReporter", "Error instantiating app state log parser", e);
            r0 = null;
        }
        this.A02 = r0;
        this.A01 = r5;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|18|19|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0038 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x003f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.AnonymousClass0Jo r5, java.io.File r6) {
        /*
            long r3 = r6.length()     // Catch:{ 0Cz | IOException -> 0x0040 }
            java.lang.String r0 = "errorFileSize"
            r5.ANN(r0, r3)     // Catch:{ 0Cz | IOException -> 0x0040 }
            java.lang.String r1 = "errorFileName"
            java.lang.String r0 = r6.getName()     // Catch:{ 0Cz | IOException -> 0x0040 }
            r5.ANO(r1, r0)     // Catch:{ 0Cz | IOException -> 0x0040 }
            r1 = 1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0050
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ 0Cz | IOException -> 0x0040 }
            r3.<init>(r6)     // Catch:{ 0Cz | IOException -> 0x0040 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ all -> 0x0039 }
            r2.<init>(r3)     // Catch:{ all -> 0x0039 }
            char r1 = X.AnonymousClass07M.A00(r2)     // Catch:{ all -> 0x0032 }
            java.lang.String r0 = "errorStatus"
            r5.ANL(r0, r1)     // Catch:{ all -> 0x0032 }
            r2.close()     // Catch:{ all -> 0x0039 }
            r3.close()     // Catch:{ 0Cz | IOException -> 0x0040 }
            return
        L_0x0032:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0038 }
        L_0x0038:
            throw r0     // Catch:{ all -> 0x0039 }
        L_0x0039:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003b }
        L_0x003b:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x003f }
        L_0x003f:
            throw r0     // Catch:{ 0Cz | IOException -> 0x0040 }
        L_0x0040:
            r3 = move-exception
            java.lang.String r2 = "AppStateReporter"
            java.lang.String r0 = r6.getAbsolutePath()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Failed to extract status from log file path: %s"
            X.C010708t.A0U(r2, r3, r0, r1)
        L_0x0050:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0NH.A01(X.0Jo, java.io.File):void");
    }

    public static void A05(File file) {
        Files.delete(FileSystems.getDefault().getPath(file.getCanonicalPath(), new String[0]));
    }
}
