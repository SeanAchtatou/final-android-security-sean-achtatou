package X;

/* renamed from: X.0de  reason: invalid class name and case insensitive filesystem */
public final class C07490de implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.multiprocess.peer.PeerProcessManagerImpl$3";
    public final /* synthetic */ C07930eP A00;

    public C07490de(C07930eP r1) {
        this.A00 = r1;
    }

    public void run() {
        C07930eP r1 = this.A00;
        if (!r1.A0E) {
            C07930eP.A02(r1);
            AnonymousClass00S.A05(r1.A01, r1.A08, 1000, -737153025);
        } else if (C07930eP.A01(r1) == null) {
            C07930eP r2 = this.A00;
            if (r2.A0H == null) {
                r2.A0H = new C410323w(r2);
            }
            r2.A04.A02(r2.A0H);
            C07930eP.A01(this.A00);
        }
    }
}
