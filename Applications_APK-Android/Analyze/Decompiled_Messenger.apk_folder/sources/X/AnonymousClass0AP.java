package X;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.0AP  reason: invalid class name */
public class AnonymousClass0AP extends Handler {
    public volatile boolean A00;
    public final /* synthetic */ AnonymousClass0AE A01;

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x000e */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x000e A[LOOP:0: B:5:0x000e->B:15:0x000e, LOOP_START, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00() {
        /*
            r2 = this;
            r0 = 3
            android.os.Message r0 = r2.obtainMessage(r0)
            boolean r0 = r2.sendMessage(r0)
            if (r0 != 0) goto L_0x000c
            return
        L_0x000c:
            r1 = r2
            monitor-enter(r1)
        L_0x000e:
            boolean r0 = r2.A00     // Catch:{ all -> 0x0018 }
            if (r0 != 0) goto L_0x0016
            r2.wait()     // Catch:{ InterruptedException -> 0x000e }
            goto L_0x000e
        L_0x0016:
            monitor-exit(r1)
            return
        L_0x0018:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AP.A00():void");
    }

    public void A01() {
        sendMessage(obtainMessage(1));
    }

    public void A02(Intent intent, int i, int i2) {
        sendMessage(obtainMessage(2, i, i2, intent));
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0AP(AnonymousClass0AE r1, Looper looper) {
        super(looper);
        this.A01 = r1;
    }

    public void handleMessage(Message message) {
        if (message != null) {
            int i = message.what;
            if (i == 1) {
                this.A01.A0A();
            } else if (i == 2) {
                this.A01.A0D((Intent) message.obj, message.arg1, message.arg2);
            } else if (i == 3) {
                this.A01.A0E();
                synchronized (this) {
                    this.A00 = true;
                    notifyAll();
                }
            } else {
                throw new IllegalStateException("Unsupported message");
            }
        } else {
            throw new IllegalStateException("Message is null");
        }
    }
}
