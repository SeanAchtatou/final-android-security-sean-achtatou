package X;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0Q4  reason: invalid class name */
public final class AnonymousClass0Q4 {
    public static Set A00(Context context) {
        if (Build.VERSION.SDK_INT < 21) {
            return new HashSet();
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            HashSet hashSet = new HashSet();
            if (packageInfo == null || packageInfo.splitNames == null) {
                return hashSet;
            }
            int i = 0;
            while (true) {
                String[] strArr = packageInfo.splitNames;
                if (i >= strArr.length) {
                    return hashSet;
                }
                hashSet.add(strArr[i]);
                if (!AnonymousClass0GS.A01(packageInfo.splitNames[i], context)) {
                    C010708t.A0Q("InstalledSplitUtil", "Module %s is marked as installed through package manager but the split file does not exist at the expected location", packageInfo.splitNames[i]);
                } else {
                    AnonymousClass0GS.A00(packageInfo.splitNames[i], context).toString();
                }
                i++;
            }
        } catch (PackageManager.NameNotFoundException e) {
            C010708t.A0N("InstalledSplitUtil", "our package is not found in the package manager!", e);
            return new HashSet();
        }
    }
}
