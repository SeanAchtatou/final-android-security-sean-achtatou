package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import org.webrtc.audio.WebRtcAudioRecord;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.10J  reason: invalid class name */
public final class AnonymousClass10J extends Enum implements AnonymousClass10K {
    private static final /* synthetic */ AnonymousClass10J[] A00;
    public static final AnonymousClass10J A01;
    public static final AnonymousClass10J A02;
    public static final AnonymousClass10J A03;
    public static final AnonymousClass10J A04;
    public static final AnonymousClass10J A05;
    public static final AnonymousClass10J A06;
    public static final AnonymousClass10J A07;
    public static final AnonymousClass10J A08;
    public static final AnonymousClass10J A09;
    public static final AnonymousClass10J A0A;
    public static final AnonymousClass10J A0B;
    public static final AnonymousClass10J A0C;
    public static final AnonymousClass10J A0D;
    public static final AnonymousClass10J A0E;
    public static final AnonymousClass10J A0F;
    public static final AnonymousClass10J A0G;
    public static final AnonymousClass10J A0H;
    public static final AnonymousClass10J A0I;
    public static final AnonymousClass10J A0J;
    public static final AnonymousClass10J A0K;
    public static final AnonymousClass10J A0L;
    public static final AnonymousClass10J A0M;
    public static final AnonymousClass10J A0N;
    public static final AnonymousClass10J A0O;
    public static final AnonymousClass10J A0P;
    public static final AnonymousClass10J A0Q;
    public static final AnonymousClass10J A0R;
    public static final AnonymousClass10J A0S;
    public static final AnonymousClass10J A0T;
    public static final AnonymousClass10J A0U;
    public static final AnonymousClass10J A0V;
    public static final AnonymousClass10J A0W;
    public static final AnonymousClass10J A0X;
    public static final AnonymousClass10J A0Y;
    public static final AnonymousClass10J A0Z;
    private final boolean mAllCaps;
    public final AnonymousClass1JZ mTextColor;
    public final AnonymousClass1I7 mTextSize;
    private final AnonymousClass10M mTypeface;

    static {
        AnonymousClass10J r1 = new AnonymousClass10J("HEADLINE_PRIMARY", 0, AnonymousClass10M.ROBOTO_BOLD, AnonymousClass1I7.HEADLINE, AnonymousClass1JZ.A0C);
        A0E = r1;
        AnonymousClass10M r5 = AnonymousClass10M.ROBOTO_BOLD;
        AnonymousClass1I7 r6 = AnonymousClass1I7.XXLARGE;
        AnonymousClass1JZ r7 = AnonymousClass1JZ.A0C;
        AnonymousClass10J r2 = new AnonymousClass10J("TITLE_XXLARGE_PRIMARY", 1, r5, r6, r7);
        A0Z = r2;
        AnonymousClass10M r11 = AnonymousClass10M.ROBOTO_MEDIUM;
        AnonymousClass1I7 r12 = AnonymousClass1I7.XLARGE;
        AnonymousClass10J r8 = new AnonymousClass10J("TITLE_XLARGE_PRIMARY", 2, r11, r12, AnonymousClass1JZ.A0C);
        A0W = r8;
        AnonymousClass10J r13 = new AnonymousClass10J("TITLE_XLARGE_PRIMARY_BOLD", 3, r5, AnonymousClass1I7.XLARGE, r7);
        A0X = r13;
        AnonymousClass10J r14 = new AnonymousClass10J("TITLE_XLARGE_TERTIARY", 4, r11, r12, AnonymousClass1JZ.A0I);
        A0Y = r14;
        AnonymousClass10J r15 = new AnonymousClass10J("TITLE_LARGE_BOLD_PRIMARY", 5, r5, AnonymousClass1I7.LARGE, r7);
        A0I = r15;
        AnonymousClass10J r16 = new AnonymousClass10J("TITLE_LARGE_PRIMARY", 6, r11, AnonymousClass1I7.LARGE, AnonymousClass1JZ.A0C);
        A0J = r16;
        AnonymousClass1I7 r21 = AnonymousClass1I7.MEDIUM;
        AnonymousClass10J r17 = new AnonymousClass10J("TITLE_MEDIUM_BOLD_PRIMARY", 7, r5, r21, r7);
        A0L = r17;
        AnonymousClass10M r25 = AnonymousClass10M.ROBOTO_BOLD;
        AnonymousClass1I7 r26 = AnonymousClass1I7.MEDIUM;
        AnonymousClass1JZ r27 = AnonymousClass1JZ.A0G;
        AnonymousClass10J r22 = new AnonymousClass10J("TITLE_MEDIUM_BOLD_SECONDARY", 8, r25, r26, r27);
        A0M = r22;
        AnonymousClass10J r28 = new AnonymousClass10J("TITLE_MEDIUM_PRIMARY", 9, AnonymousClass10M.ROBOTO_MEDIUM, r21, r7);
        A0N = r28;
        AnonymousClass10J r32 = new AnonymousClass10J("TITLE_MEDIUM_SECONDARY", 10, AnonymousClass10M.ROBOTO_MEDIUM, r26, r27);
        A0P = r32;
        AnonymousClass10J r36 = new AnonymousClass10J("TITLE_MEDIUM_TERTIARY", 11, r11, r21, AnonymousClass1JZ.A0I);
        A0Q = r36;
        AnonymousClass10J r37 = new AnonymousClass10J("TITLE_MEDIUM_BLUE", 12, r11, r26, AnonymousClass1JZ.A02);
        A0K = r37;
        AnonymousClass10J r38 = new AnonymousClass10J("TITLE_MEDIUM_RED", 13, r11, r21, AnonymousClass1JZ.A0E);
        A0O = r38;
        AnonymousClass1I7 r43 = AnonymousClass1I7.SMALL;
        AnonymousClass10J r39 = new AnonymousClass10J("TITLE_SMALL_PRIMARY", 14, r11, r43, AnonymousClass1JZ.A0C);
        A0S = r39;
        AnonymousClass1I7 r48 = AnonymousClass1I7.SMALL;
        AnonymousClass10J r44 = new AnonymousClass10J("TITLE_SMALL_SECONDARY", 15, r11, r48, AnonymousClass1JZ.A0G);
        A0U = r44;
        AnonymousClass10J r49 = new AnonymousClass10J("TITLE_SMALL_TERTIARY", 16, r11, r43, AnonymousClass1JZ.A0I);
        A0V = r49;
        AnonymousClass10J r50 = new AnonymousClass10J("TITLE_SMALL_BLUE", 17, r11, r48, AnonymousClass1JZ.A02);
        A0R = r50;
        AnonymousClass10J r51 = new AnonymousClass10J("TITLE_SMALL_RED", 18, r11, r43, AnonymousClass1JZ.A0E);
        A0T = r51;
        AnonymousClass10M r55 = AnonymousClass10M.ROBOTO_REGULAR;
        AnonymousClass1I7 r56 = AnonymousClass1I7.XLARGE;
        AnonymousClass1JZ r57 = AnonymousClass1JZ.A0G;
        AnonymousClass10J r52 = new AnonymousClass10J("BODY_XLARGE_SECONDARY", 19, r55, r56, r57);
        A0D = r52;
        AnonymousClass10M r61 = AnonymousClass10M.ROBOTO_REGULAR;
        AnonymousClass1I7 r62 = AnonymousClass1I7.LARGE;
        AnonymousClass10J r58 = new AnonymousClass10J("BODY_LARGE_PRIMARY", 20, r61, r62, AnonymousClass1JZ.A0C);
        A01 = r58;
        AnonymousClass10J r63 = new AnonymousClass10J("BODY_LARGE_SECONDARY", 21, r55, AnonymousClass1I7.LARGE, r57);
        A02 = r63;
        AnonymousClass10J r64 = new AnonymousClass10J("BODY_LARGE_TERTIARY", 22, r61, r62, AnonymousClass1JZ.A0I);
        A03 = r64;
        AnonymousClass1I7 r69 = AnonymousClass1I7.MEDIUM;
        AnonymousClass10J r65 = new AnonymousClass10J("BODY_MEDIUM_PRIMARY", 23, r55, r69, AnonymousClass1JZ.A0C);
        A04 = r65;
        AnonymousClass10J r70 = new AnonymousClass10J("BODY_MEDIUM_SECONDARY", 24, r61, AnonymousClass1I7.MEDIUM, AnonymousClass1JZ.A0G);
        A05 = r70;
        AnonymousClass10J r71 = new AnonymousClass10J("BODY_MEDIUM_TERTIARY", 25, r55, r69, AnonymousClass1JZ.A0I);
        A06 = r71;
        AnonymousClass1I7 r76 = AnonymousClass1I7.SMALL;
        AnonymousClass10J r72 = new AnonymousClass10J("BODY_SMALL_RED", 26, r61, r76, AnonymousClass1JZ.A0E);
        A0A = r72;
        AnonymousClass1I7 r81 = AnonymousClass1I7.SMALL;
        AnonymousClass10J r77 = new AnonymousClass10J("BODY_SMALL_BLUE", 27, r55, r81, AnonymousClass1JZ.A02);
        A07 = r77;
        AnonymousClass10J r82 = new AnonymousClass10J("BODY_SMALL_PRIMARY", 28, r61, r76, AnonymousClass1JZ.A0C);
        A09 = r82;
        AnonymousClass10J r83 = new AnonymousClass10J("BODY_SMALL_SECONDARY", 29, r55, r81, AnonymousClass1JZ.A0G);
        A0B = r83;
        AnonymousClass10J r84 = new AnonymousClass10J("BODY_SMALL_TERTIARY", 30, r61, r76, AnonymousClass1JZ.A0I);
        A0C = r84;
        AnonymousClass10J r85 = new AnonymousClass10J("BODY_SMALL_DISABLED", 31, r55, r81, AnonymousClass1JZ.A04);
        A08 = r85;
        AnonymousClass10J r86 = new AnonymousClass10J("INBOX_UNREAD_TITLE", 32, AnonymousClass10M.ROBOTO_BOLD, AnonymousClass1I7.LARGE, AnonymousClass1JZ.A0C);
        A0H = r86;
        AnonymousClass10J r92 = new AnonymousClass10J("INBOX_READ_TITLE_DONOTUSE", 33, r55, AnonymousClass1I7.LARGE, AnonymousClass1JZ.A0C);
        A0G = r92;
        AnonymousClass10J r93 = new AnonymousClass10J("INBOX_READ_SUBTITLE_DONOTUSE", 34, AnonymousClass10M.ROBOTO_REGULAR, AnonymousClass1I7.MEDIUM, r7);
        A0F = r93;
        AnonymousClass10J[] r0 = new AnonymousClass10J[35];
        System.arraycopy(new AnonymousClass10J[]{r1, r2, r8, r13, r14, r15, r16, r17, r22, r28, r32, r36, r37, r38, r39, r44, r49, r50, r51, r52, r58, r63, r64, r65, r70, r71, r72}, 0, r0, 0, 27);
        System.arraycopy(new AnonymousClass10J[]{r77, r82, r83, r84, r85, r86, r92, r93}, 0, r0, 27, 8);
        A00 = r0;
    }

    public static AnonymousClass10J valueOf(String str) {
        return (AnonymousClass10J) Enum.valueOf(AnonymousClass10J.class, str);
    }

    public static AnonymousClass10J[] values() {
        return (AnonymousClass10J[]) A00.clone();
    }

    public boolean AcY() {
        return this.mAllCaps;
    }

    public /* bridge */ /* synthetic */ C16930y3 B5c() {
        return this.mTextColor;
    }

    public C16930y3 B5d(MigColorScheme migColorScheme) {
        switch (C16950y5.A00[ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case 8:
            case Process.SIGKILL:
            case AnonymousClass1Y3.A01 /*10*/:
            case AnonymousClass1Y3.A02 /*11*/:
            case AnonymousClass1Y3.A03 /*12*/:
            case 13:
                return migColorScheme.AzL();
            case 14:
            case 15:
            case 16:
            case 17:
            case Process.SIGCONT:
            case Process.SIGSTOP:
            case 20:
                return migColorScheme.B2C();
            case AnonymousClass1Y3.A05 /*21*/:
            case AnonymousClass1Y3.A06 /*22*/:
            case 23:
            case AnonymousClass1Y3.A07 /*24*/:
            case 25:
            case AnonymousClass1Y3.A08 /*26*/:
                return migColorScheme.B5U();
            case AnonymousClass1Y3.A09 /*27*/:
                return migColorScheme.AkY();
            case 28:
            case 29:
            case AnonymousClass1Y3.A0A /*30*/:
                return migColorScheme.Aeb();
            case AnonymousClass1Y3.A0B /*31*/:
            case 32:
            case 33:
                return migColorScheme.B0J();
            case AnonymousClass1Y3.A0C /*34*/:
            case 35:
                return migColorScheme.Apm();
            default:
                throw new IllegalArgumentException(this + " M4MigTextStyle is not supported.");
        }
    }

    public /* bridge */ /* synthetic */ AnonymousClass1JL B5i() {
        return this.mTextSize;
    }

    public AnonymousClass10M B7S() {
        return this.mTypeface;
    }

    private AnonymousClass10J(String str, int i, AnonymousClass10M r10, AnonymousClass1I7 r11, AnonymousClass1JZ r12) {
        this(str, i, r10, r11, r12, false);
    }

    private AnonymousClass10J(String str, int i, AnonymousClass10M r3, AnonymousClass1I7 r4, AnonymousClass1JZ r5, boolean z) {
        this.mTypeface = r3;
        this.mTextSize = r4;
        this.mTextColor = r5;
        this.mAllCaps = z;
    }
}
