package X;

import android.app.Activity;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1gb  reason: invalid class name and case insensitive filesystem */
public final class C29551gb extends C13190qs {
    public final /* synthetic */ C13030qT A00;
    public final /* synthetic */ C29531gZ A01;

    public C29551gb(C29531gZ r1, C13030qT r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void BpP(Activity activity) {
        super.BpP(activity);
        AnonymousClass13y.A01(activity.getWindow(), (MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, this.A01.A00));
        ((C13980sP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AXL, this.A01.A00)).A01(this.A00);
    }

    public void Bq4(Activity activity) {
        super.Bq4(activity);
        ((C13980sP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AXL, this.A01.A00)).A02(this.A00);
    }
}
