package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.01z  reason: invalid class name and case insensitive filesystem */
public final class C003301z extends C003201y {
    public C003301z(Context context, String str) {
        super(context, str, new File(context.getApplicationInfo().sourceDir), "^assets/lib/([^/]+)/([^/]+\\.so)$");
    }

    public byte[] A0E() {
        return C002401o.A01(this.A00, this.A03);
    }
}
