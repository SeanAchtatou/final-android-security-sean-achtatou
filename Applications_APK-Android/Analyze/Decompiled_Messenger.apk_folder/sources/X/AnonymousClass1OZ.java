package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1OZ  reason: invalid class name */
public final class AnonymousClass1OZ implements AnonymousClass1N6 {
    private static volatile AnonymousClass1OZ A01;
    private AnonymousClass0UN A00;

    public static final AnonymousClass1OZ A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1OZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1OZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass1OZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        if (r9.equals(X.AnonymousClass24B.$const$string(X.AnonymousClass1Y3.A1k)) == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        if (r9.equals("bandwidth_class") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
        if (r9.equals("latency_class") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004a, code lost:
        if (r9.equals("current_bandwidth_class") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        if (r9.equals("offline_fraction_in_fg") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005e, code lost:
        if (r9.equals("offline_fraction_in_bg") == false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C23191Oo AiV(java.lang.String r9) {
        /*
            r8 = this;
            int r0 = r9.hashCode()
            r6 = 5
            r5 = 4
            r4 = 3
            r1 = 2
            r3 = 0
            r2 = 1
            switch(r0) {
                case -1450266882: goto L_0x0057;
                case -1450266758: goto L_0x004d;
                case -827144796: goto L_0x0043;
                case -331275529: goto L_0x0039;
                case 53855338: goto L_0x002f;
                case 731866107: goto L_0x0021;
                default: goto L_0x000d;
            }
        L_0x000d:
            r7 = -1
        L_0x000e:
            if (r7 == 0) goto L_0x00b8
            if (r7 == r2) goto L_0x0099
            if (r7 == r1) goto L_0x008c
            if (r7 == r4) goto L_0x007f
            if (r7 == r5) goto L_0x0070
            if (r7 == r6) goto L_0x0061
            X.1Oo r1 = new X.1Oo
            r0 = 0
            r1.<init>(r0)
            return r1
        L_0x0021:
            r0 = 221(0xdd, float:3.1E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r9.equals(r0)
            r7 = 4
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x002f:
            java.lang.String r0 = "bandwidth_class"
            boolean r0 = r9.equals(r0)
            r7 = 5
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x0039:
            java.lang.String r0 = "latency_class"
            boolean r0 = r9.equals(r0)
            r7 = 3
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x0043:
            java.lang.String r0 = "current_bandwidth_class"
            boolean r0 = r9.equals(r0)
            r7 = 2
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x004d:
            java.lang.String r0 = "offline_fraction_in_fg"
            boolean r0 = r9.equals(r0)
            r7 = 0
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x0057:
            java.lang.String r0 = "offline_fraction_in_bg"
            boolean r0 = r9.equals(r0)
            r7 = 1
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x0061:
            int r1 = X.AnonymousClass1Y3.A0S
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.8Ro r0 = (X.C179608Ro) r0
            X.1Oo r0 = r0.A01()
            return r0
        L_0x0070:
            int r1 = X.AnonymousClass1Y3.A0S
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.8Ro r0 = (X.C179608Ro) r0
            X.1Oo r0 = r0.A02()
            return r0
        L_0x007f:
            int r1 = X.AnonymousClass1Y3.A0S
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.8Ro r0 = (X.C179608Ro) r0
            X.1Oo r0 = r0.A01
            return r0
        L_0x008c:
            int r1 = X.AnonymousClass1Y3.A0S
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.8Ro r0 = (X.C179608Ro) r0
            X.1Oo r0 = r0.A00
            return r0
        L_0x0099:
            int r1 = X.AnonymousClass1Y3.Ao3
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.4M2 r0 = (X.AnonymousClass4M2) r0
            X.1Oo r6 = new X.1Oo
            X.1IG r5 = r0.A00
            double r3 = r5.A04
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00b2
            X.AnonymousClass1IG.A01(r5)
        L_0x00b2:
            double r0 = r5.A04
            r6.<init>(r0)
            return r6
        L_0x00b8:
            int r1 = X.AnonymousClass1Y3.Ao3
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.4M2 r0 = (X.AnonymousClass4M2) r0
            X.1Oo r6 = new X.1Oo
            X.1IG r5 = r0.A00
            double r3 = r5.A05
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00d1
            X.AnonymousClass1IG.A01(r5)
        L_0x00d1:
            double r0 = r5.A05
            r6.<init>(r0)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OZ.AiV(java.lang.String):X.1Oo");
    }
}
