package X;

/* renamed from: X.0FL  reason: invalid class name */
public final class AnonymousClass0FL extends AnonymousClass0FM {
    public int bleOpportunisticScanCount;
    public long bleOpportunisticScanDurationMs;
    public int bleScanCount;
    public long bleScanDurationMs;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                AnonymousClass0FL r8 = (AnonymousClass0FL) obj;
                if (!(this.bleScanCount == r8.bleScanCount && this.bleScanDurationMs == r8.bleScanDurationMs && this.bleOpportunisticScanCount == r8.bleOpportunisticScanCount && this.bleOpportunisticScanDurationMs == r8.bleOpportunisticScanDurationMs)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    private void A00(AnonymousClass0FL r3) {
        this.bleScanCount = r3.bleScanCount;
        this.bleScanDurationMs = r3.bleScanDurationMs;
        this.bleOpportunisticScanCount = r3.bleOpportunisticScanCount;
        this.bleOpportunisticScanDurationMs = r3.bleOpportunisticScanDurationMs;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A00((AnonymousClass0FL) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        AnonymousClass0FL r52 = (AnonymousClass0FL) r5;
        AnonymousClass0FL r62 = (AnonymousClass0FL) r6;
        if (r62 == null) {
            r62 = new AnonymousClass0FL();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.bleScanCount = this.bleScanCount - r52.bleScanCount;
        r62.bleScanDurationMs = this.bleScanDurationMs - r52.bleScanDurationMs;
        r62.bleOpportunisticScanCount = this.bleOpportunisticScanCount - r52.bleOpportunisticScanCount;
        r62.bleOpportunisticScanDurationMs = this.bleOpportunisticScanDurationMs - r52.bleOpportunisticScanDurationMs;
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        AnonymousClass0FL r52 = (AnonymousClass0FL) r5;
        AnonymousClass0FL r62 = (AnonymousClass0FL) r6;
        if (r62 == null) {
            r62 = new AnonymousClass0FL();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.bleScanCount = this.bleScanCount + r52.bleScanCount;
        r62.bleScanDurationMs = this.bleScanDurationMs + r52.bleScanDurationMs;
        r62.bleOpportunisticScanCount = this.bleOpportunisticScanCount + r52.bleOpportunisticScanCount;
        r62.bleOpportunisticScanDurationMs = this.bleOpportunisticScanDurationMs + r52.bleOpportunisticScanDurationMs;
        return r62;
    }

    public int hashCode() {
        long j = this.bleScanDurationMs;
        long j2 = this.bleOpportunisticScanDurationMs;
        return (((((this.bleScanCount * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.bleOpportunisticScanCount) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "BluetoothMetrics{bleScanCount=" + this.bleScanCount + ", bleScanDurationMs=" + this.bleScanDurationMs + ", bleOpportunisticScanCount=" + this.bleOpportunisticScanCount + ", bleOpportunisticScanDurationMs=" + this.bleOpportunisticScanDurationMs + '}';
    }
}
