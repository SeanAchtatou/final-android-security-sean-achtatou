package X;

/* renamed from: X.1Sw  reason: invalid class name and case insensitive filesystem */
public final class C24251Sw extends AnonymousClass1PS {
    public /* bridge */ /* synthetic */ Object clone() {
        return clone();
    }

    public void finalize() {
        boolean z;
        int A03 = C000700l.A03(1827872410);
        try {
            synchronized (this) {
                z = this.A00;
            }
            if (z) {
                super.finalize();
                C000700l.A09(-2130142634, A03);
                return;
            }
            Integer valueOf = Integer.valueOf(System.identityHashCode(this));
            AnonymousClass1SA r2 = this.A02;
            AnonymousClass02w.A0E("DefaultCloseableReference", "Finalized without closing: %x %x (type = %s)", valueOf, Integer.valueOf(System.identityHashCode(r2)), r2.A01().getClass().getName());
            this.A01.C2m(this.A02, this.A03);
            close();
            super.finalize();
            C000700l.A09(2055837155, A03);
        } catch (Throwable th) {
            super.finalize();
            C000700l.A09(-871414249, A03);
            throw th;
        }
    }

    public C24251Sw(AnonymousClass1SA r1, C22991Nq r2, Throwable th) {
        super(r1, r2, th);
    }

    public C24251Sw(Object obj, C22891Nf r2, C22991Nq r3, Throwable th) {
        super(obj, r2, r3, th);
    }
}
