package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tI  reason: invalid class name and case insensitive filesystem */
public final class C36541tI {
    private static volatile C36541tI A03;
    private final AnonymousClass187 A00;
    private final C04310Tq A01;
    private final C04310Tq A02;

    public static final C36541tI A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C36541tI.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C36541tI(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C36541tI(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, r2);
        this.A00 = AnonymousClass187.A01(r2);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.B6n, r2);
        C194918j.A00(r2);
    }
}
