package X;

import com.facebook.messaging.neue.nullstate.RecentsTabEmptyView;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.orca.threadlist.ThreadListFragment;
import com.google.common.base.Objects;

/* renamed from: X.0sH  reason: invalid class name and case insensitive filesystem */
public final class C13910sH implements C13030qT {
    public final /* synthetic */ ThreadListFragment A00;

    public C13910sH(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public void Bn3() {
        ThreadListFragment threadListFragment = this.A00;
        MigColorScheme migColorScheme = (MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, threadListFragment.A0O);
        if (!Objects.equal(threadListFragment.A0s, migColorScheme)) {
            threadListFragment.A0s = migColorScheme;
            ThreadListFragment.A0K(threadListFragment, threadListFragment.A10, threadListFragment.A1J, false);
            RecentsTabEmptyView recentsTabEmptyView = threadListFragment.A0h;
            if (recentsTabEmptyView != null) {
                MigColorScheme migColorScheme2 = threadListFragment.A0s;
                if (!Objects.equal(recentsTabEmptyView.A02, migColorScheme2)) {
                    recentsTabEmptyView.A02 = migColorScheme2;
                    RecentsTabEmptyView.A02(recentsTabEmptyView);
                }
            }
            threadListFragment.A2K(2131300042).setBackgroundColor(threadListFragment.A0s.Agf());
            threadListFragment.A2K(2131298670).setBackgroundColor(threadListFragment.A0s.Agf());
            C196118y r1 = threadListFragment.A0w;
            if (r1 != null) {
                r1.A00(threadListFragment.A0s);
            }
            ((C16430x3) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AZ7, threadListFragment.A0x.A01)).A04 = migColorScheme;
            AnonymousClass1CV r12 = (AnonymousClass1CV) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AdF, threadListFragment.A0O);
            if (!Objects.equal(r12.A0A, migColorScheme)) {
                r12.A0A = migColorScheme;
                AnonymousClass1CV.A03(r12);
            }
            C27814DjR djR = threadListFragment.A0p;
            if (djR != null) {
                djR.A01(migColorScheme);
            }
        }
    }
}
