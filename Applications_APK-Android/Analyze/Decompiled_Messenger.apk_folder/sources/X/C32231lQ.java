package X;

import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1lQ  reason: invalid class name and case insensitive filesystem */
public final class C32231lQ implements AnonymousClass1LY {
    private static volatile C32231lQ A01;
    private final AnonymousClass0Z4 A00;

    public static final C32231lQ A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C32231lQ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C32231lQ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void BgD(String str, String str2, Map map) {
        AnonymousClass2YN r0;
        AnonymousClass0Z4 r1 = this.A00;
        if (!C06850cB.A0C(str, str2) && (r0 = r1.A04) != null) {
            r0.BfM(str, str2);
        }
    }

    private C32231lQ(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0Z4.A01(r2);
    }
}
