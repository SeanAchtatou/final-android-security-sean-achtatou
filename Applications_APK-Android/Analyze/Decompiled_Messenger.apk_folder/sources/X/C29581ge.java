package X;

import android.view.View;

/* renamed from: X.1ge  reason: invalid class name and case insensitive filesystem */
public final class C29581ge implements C29591gf {
    public AnonymousClass24G BOO(View view, AnonymousClass24G r7) {
        AnonymousClass24G onApplyWindowInsets = C15320v6.onApplyWindowInsets(view, r7);
        return onApplyWindowInsets.A04(onApplyWindowInsets.A01(), 0, onApplyWindowInsets.A02(), onApplyWindowInsets.A00());
    }
}
