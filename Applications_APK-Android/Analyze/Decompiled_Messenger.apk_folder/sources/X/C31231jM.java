package X;

import android.graphics.drawable.Animatable;

/* renamed from: X.1jM  reason: invalid class name and case insensitive filesystem */
public interface C31231jM {
    void BYg(String str, Throwable th);

    void BZ2(String str, Object obj, Animatable animatable);

    void Bbb(String str, Throwable th);

    void Bbc(String str, Object obj);

    void BlR(String str);

    void BqL(String str, Object obj);
}
