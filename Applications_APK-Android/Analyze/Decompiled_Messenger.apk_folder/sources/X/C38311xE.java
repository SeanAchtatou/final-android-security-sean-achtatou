package X;

import android.content.Context;
import android.os.Bundle;
import android.os.Messenger;
import android.os.PowerManager;
import com.facebook.common.build.BuildConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1xE  reason: invalid class name and case insensitive filesystem */
public final class C38311xE {
    private static List A09;
    public PowerManager.WakeLock A00;
    public AnonymousClass353 A01;
    public final int A02;
    public final Context A03;
    public final Bundle A04;
    public final Messenger A05;
    public final C631334v A06;
    public final C74493hx A07;
    public final String A08;

    public static C38311xE A00(C194539Bn r2, Bundle bundle, String str, C631334v r5, int i, C74493hx r7, Context context) {
        Messenger messenger;
        if (r2 != null) {
            C194529Bm r1 = new C194529Bm(r2);
            messenger = new Messenger(r1);
            A01().add(r1);
        } else {
            messenger = null;
        }
        return new C38311xE(messenger, bundle, str, r5, i, r7, context);
    }

    public static List A01() {
        List list;
        synchronized (C38311xE.class) {
            if (A09 == null) {
                A09 = Collections.synchronizedList(new ArrayList(1));
            }
            list = A09;
        }
        return list;
    }

    public Bundle A02() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("_messenger", this.A05);
        bundle.putBundle("_extras", this.A04);
        bundle.putString("_hack_action", this.A08);
        bundle.putBundle("_upload_job_config", new Bundle((Bundle) this.A06.A00(new C631434w(new Bundle()))));
        bundle.putInt("_job_id", this.A02);
        C74493hx r4 = this.A07;
        if (r4 != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("min_delay_ms", r4.A01);
            bundle2.putLong("max_delay_ms", r4.A00);
            bundle2.putString("action", r4.A02);
            bundle2.putInt("__VERSION_CODE", BuildConstants.A00());
            bundle.putBundle("_fallback_config", bundle2);
        }
        return bundle;
    }

    public C38311xE(Messenger messenger, Bundle bundle, String str, C631334v r4, int i, C74493hx r6, Context context) {
        this.A05 = messenger;
        this.A04 = bundle;
        this.A08 = str;
        this.A06 = r4;
        this.A02 = i;
        this.A03 = context;
        this.A07 = r6;
    }
}
