package X;

import com.google.common.base.Preconditions;
import java.util.NoSuchElementException;

/* renamed from: X.0UI  reason: invalid class name */
public abstract class AnonymousClass0UI extends C24971Xv {
    public Integer A00 = AnonymousClass07B.A01;
    public Object A01;

    public Object A02() {
        if (!(this instanceof AnonymousClass1Y0)) {
            AnonymousClass0UJ r1 = (AnonymousClass0UJ) this;
            if (!r1.A00.isEmpty()) {
                return r1.A00.remove();
            }
            r1.A00 = AnonymousClass07B.A0C;
            return null;
        }
        AnonymousClass1Y0 r2 = (AnonymousClass1Y0) this;
        while (r2.A01.hasNext()) {
            Object next = r2.A01.next();
            if (r2.A00.apply(next)) {
                return next;
            }
        }
        r2.A00 = AnonymousClass07B.A0C;
        return null;
    }

    public final boolean hasNext() {
        Integer num = this.A00;
        Integer num2 = AnonymousClass07B.A0N;
        boolean z = false;
        if (num != num2) {
            z = true;
        }
        Preconditions.checkState(z);
        switch (num.intValue()) {
            case 0:
                return true;
            case 1:
            default:
                this.A00 = num2;
                this.A01 = A02();
                if (this.A00 == AnonymousClass07B.A0C) {
                    return false;
                }
                this.A00 = AnonymousClass07B.A00;
                return true;
            case 2:
                return false;
        }
    }

    public final Object next() {
        if (hasNext()) {
            this.A00 = AnonymousClass07B.A01;
            Object obj = this.A01;
            this.A01 = null;
            return obj;
        }
        throw new NoSuchElementException();
    }
}
