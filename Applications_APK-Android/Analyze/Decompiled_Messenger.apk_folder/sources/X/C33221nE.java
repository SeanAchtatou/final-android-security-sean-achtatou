package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.1nE  reason: invalid class name and case insensitive filesystem */
public final class C33221nE implements Runnable {
    public static final String __redex_internal_original_name = "bolts.Task$14";
    public final /* synthetic */ C30405Evg A00;
    public final /* synthetic */ C23981Rs A01;
    public final /* synthetic */ C23901Rk A02;
    public final /* synthetic */ C23951Rp A03;

    public C33221nE(C30405Evg evg, C23951Rp r2, C23981Rs r3, C23901Rk r4) {
        this.A00 = evg;
        this.A03 = r2;
        this.A01 = r3;
        this.A02 = r4;
    }

    public void run() {
        if (this.A00 != null) {
            AnonymousClass7S9 r0 = null;
            if (r0.A00()) {
                this.A03.A00();
                return;
            }
        }
        try {
            this.A03.A02(this.A01.CIx(this.A02));
        } catch (CancellationException unused) {
            this.A03.A00();
        } catch (Exception e) {
            this.A03.A01(e);
        }
    }
}
