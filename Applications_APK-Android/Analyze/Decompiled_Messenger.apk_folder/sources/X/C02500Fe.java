package X;

/* renamed from: X.0Fe  reason: invalid class name and case insensitive filesystem */
public final class C02500Fe extends AnonymousClass0FM {
    public long coarseTimeMs;
    public long fineTimeMs;
    public boolean isAttributionEnabled;
    public long mediumTimeMs;
    public final AnonymousClass04b tagLocationDetails;
    public long wifiScanCount;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                C02500Fe r8 = (C02500Fe) obj;
                if (this.isAttributionEnabled == r8.isAttributionEnabled && this.fineTimeMs == r8.fineTimeMs && this.mediumTimeMs == r8.mediumTimeMs && this.coarseTimeMs == r8.coarseTimeMs && this.wifiScanCount == r8.wifiScanCount) {
                    AnonymousClass04b r2 = this.tagLocationDetails;
                    if (r2.size() == r8.tagLocationDetails.size()) {
                        int size = r2.size();
                        for (int i = 0; i < size; i++) {
                            AnonymousClass04b r0 = this.tagLocationDetails;
                            String str = (String) r0.A07(i);
                            C03130Kd r1 = (C03130Kd) r0.A09(i);
                            C03130Kd r02 = (C03130Kd) r8.tagLocationDetails.get(str);
                            if (r1 == null) {
                                if (r02 == null && r8.tagLocationDetails.containsKey(str)) {
                                }
                            } else if (!r1.equals(r02)) {
                                return false;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A09((C02500Fe) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r11, AnonymousClass0FM r12) {
        C02500Fe r112 = (C02500Fe) r11;
        C02500Fe r122 = (C02500Fe) r12;
        if (r122 == null) {
            r122 = new C02500Fe(this.isAttributionEnabled);
        }
        if (r112 == null) {
            r122.A09(this);
        } else {
            r122.wifiScanCount = this.wifiScanCount - r112.wifiScanCount;
            r122.fineTimeMs = this.fineTimeMs - r112.fineTimeMs;
            r122.coarseTimeMs = this.coarseTimeMs - r112.coarseTimeMs;
            r122.mediumTimeMs = this.mediumTimeMs - r112.mediumTimeMs;
            if (r122.isAttributionEnabled) {
                int size = this.tagLocationDetails.size();
                for (int i = 0; i < size; i++) {
                    String str = (String) this.tagLocationDetails.A07(i);
                    C03130Kd r6 = (C03130Kd) r112.tagLocationDetails.get(str);
                    C03130Kd r7 = (C03130Kd) this.tagLocationDetails.A09(i);
                    C03130Kd r4 = new C03130Kd();
                    if (r6 == null) {
                        r4.A00 = r7.A00;
                        r4.A02 = r7.A02;
                        r4.A01 = r7.A01;
                    } else {
                        r4.A00 = r7.A00 - r6.A00;
                        r4.A02 = r7.A02 - r6.A02;
                        r4.A01 = r7.A01 - r6.A01;
                    }
                    r122.tagLocationDetails.put(str, r4);
                }
            }
        }
        return r122;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r13, AnonymousClass0FM r14) {
        C02500Fe r132 = (C02500Fe) r13;
        C02500Fe r142 = (C02500Fe) r14;
        if (r142 == null) {
            r142 = new C02500Fe(this.isAttributionEnabled);
        }
        if (r132 == null) {
            r142.A09(this);
        } else {
            r142.wifiScanCount = this.wifiScanCount + r132.wifiScanCount;
            r142.fineTimeMs = this.fineTimeMs + r132.fineTimeMs;
            r142.coarseTimeMs = this.coarseTimeMs + r132.coarseTimeMs;
            r142.mediumTimeMs = this.mediumTimeMs + r132.mediumTimeMs;
            if (r142.isAttributionEnabled) {
                int size = this.tagLocationDetails.size();
                for (int i = 0; i < size; i++) {
                    String str = (String) this.tagLocationDetails.A07(i);
                    C03130Kd r7 = (C03130Kd) r132.tagLocationDetails.get(str);
                    AnonymousClass04b r9 = r142.tagLocationDetails;
                    C03130Kd r8 = (C03130Kd) this.tagLocationDetails.A09(i);
                    C03130Kd r4 = new C03130Kd();
                    if (r7 == null) {
                        r4.A00 = r8.A00;
                        r4.A02 = r8.A02;
                        r4.A01 = r8.A01;
                    } else {
                        r4.A00 = r8.A00 + r7.A00;
                        r4.A02 = r8.A02 + r7.A02;
                        r4.A01 = r8.A01 + r7.A01;
                    }
                    r9.put(str, r4);
                }
                int size2 = r132.tagLocationDetails.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str2 = (String) r132.tagLocationDetails.A07(i2);
                    if (this.tagLocationDetails.get(str2) == null) {
                        r142.tagLocationDetails.put(str2, r132.tagLocationDetails.A09(i2));
                    }
                }
            }
        }
        return r142;
    }

    public void A09(C02500Fe r8) {
        this.wifiScanCount = r8.wifiScanCount;
        this.fineTimeMs = r8.fineTimeMs;
        this.mediumTimeMs = r8.mediumTimeMs;
        this.coarseTimeMs = r8.coarseTimeMs;
        if (r8.isAttributionEnabled && this.isAttributionEnabled) {
            int size = this.tagLocationDetails.size();
            for (int i = 0; i < size; i++) {
                if (!r8.tagLocationDetails.containsKey((String) this.tagLocationDetails.A07(i))) {
                    this.tagLocationDetails.A08(i);
                } else {
                    C03130Kd r3 = (C03130Kd) this.tagLocationDetails.A09(i);
                    C03130Kd r2 = (C03130Kd) r8.tagLocationDetails.A09(i);
                    r3.A00 = r2.A00;
                    r3.A02 = r2.A02;
                    r3.A01 = r2.A01;
                }
            }
            int size2 = r8.tagLocationDetails.size();
            for (int i2 = 0; i2 < size2; i2++) {
                AnonymousClass04b r0 = r8.tagLocationDetails;
                String str = (String) r0.A07(i2);
                C03130Kd r22 = (C03130Kd) r0.A09(i2);
                if (!this.tagLocationDetails.containsKey(str)) {
                    this.tagLocationDetails.put(str, new C03130Kd(r22));
                }
            }
        }
    }

    public int hashCode() {
        long j = this.wifiScanCount;
        long j2 = this.coarseTimeMs;
        long j3 = this.mediumTimeMs;
        long j4 = this.fineTimeMs;
        return (((((((((this.tagLocationDetails.hashCode() * 31) + (this.isAttributionEnabled ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)));
    }

    public String toString() {
        return "LocationMetrics{wifiScanCount=" + this.wifiScanCount + ", isAttributionEnabled=" + this.isAttributionEnabled + ", tagLocationDetails=" + this.tagLocationDetails + ", fineTimeMs=" + this.fineTimeMs + ", mediumTimeMs=" + this.mediumTimeMs + ", coarseTimeMs=" + this.coarseTimeMs + '}';
    }

    public C02500Fe() {
        this(true);
    }

    public C02500Fe(boolean z) {
        this.tagLocationDetails = new AnonymousClass04b();
        this.isAttributionEnabled = z;
    }
}
