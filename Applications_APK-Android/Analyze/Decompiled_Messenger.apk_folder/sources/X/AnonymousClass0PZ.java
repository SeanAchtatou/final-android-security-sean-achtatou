package X;

import android.content.Intent;
import android.os.Looper;

/* renamed from: X.0PZ  reason: invalid class name */
public final class AnonymousClass0PZ extends AnonymousClass0AP {
    public final /* synthetic */ AnonymousClass0AE A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0PZ(AnonymousClass0AE r1, Looper looper) {
        super(r1, looper);
        this.A00 = r1;
    }

    public void A00() {
        this.A00.A0E();
    }

    public void A01() {
        this.A00.A0A();
    }

    public void A02(Intent intent, int i, int i2) {
        this.A00.A0D(intent, i, i2);
    }
}
