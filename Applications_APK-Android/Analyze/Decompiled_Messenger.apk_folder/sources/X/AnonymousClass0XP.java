package X;

import android.content.Context;
import com.facebook.inject.InjectorModule;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@InjectorModule
/* renamed from: X.0XP  reason: invalid class name */
public final class AnonymousClass0XP extends AnonymousClass0UV {
    public static boolean A00;
    private static volatile AnonymousClass0XQ A01;

    public static final AnonymousClass0XQ A01(AnonymousClass1XY r10) {
        ExecutorService executorService;
        if (A01 == null) {
            synchronized (AnonymousClass0XQ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r10);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r10.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.Ax3, applicationInjector);
                        AnonymousClass0VB A005 = AnonymousClass0VB.A00(AnonymousClass1Y3.ACz, applicationInjector);
                        AnonymousClass0WP A006 = C25091Yh.A00(applicationInjector);
                        if (AnonymousClass08X.A08(A003, "light_shared_pref_idle_executor", false)) {
                            executorService = (ExecutorService) A005.get();
                        } else {
                            executorService = (ExecutorService) A004.get();
                        }
                        Context applicationContext = A003.getApplicationContext();
                        if (executorService == null) {
                            executorService = Executors.newSingleThreadExecutor();
                        }
                        String A007 = AnonymousClass0XR.A00();
                        if (A007 == null) {
                            A007 = "default";
                        }
                        File file = new File(applicationContext.getDir("light_prefs", 0), A007);
                        file.mkdirs();
                        AnonymousClass0XQ r4 = new AnonymousClass0XQ(executorService, file, 0);
                        if (!A00) {
                            AnonymousClass0XS.A00(0);
                            A00 = true;
                            A006.CIG("LightSharedPreferencesModule-waitForInitialized", new AnonymousClass0XU(), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY, AnonymousClass07B.A01);
                        }
                        A01 = r4;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final AnonymousClass0XQ A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
