package X;

/* renamed from: X.05C  reason: invalid class name */
public final class AnonymousClass05C extends C000900o {
    private boolean A00;

    public AnonymousClass05C() {
        super(null);
    }

    public int getTracingProviders() {
        if (this.A00) {
            return AnonymousClass00n.A08;
        }
        return 0;
    }

    public void disable() {
        int A03 = C000700l.A03(-327011069);
        AnonymousClass08Z.A00 = 0;
        this.A00 = false;
        C000700l.A09(334625755, A03);
    }

    public void enable() {
        int A03 = C000700l.A03(-1921993945);
        AnonymousClass08Z.A00 = 352050453946913L;
        this.A00 = true;
        C000700l.A09(1149872055, A03);
    }

    public int getSupportedProviders() {
        return AnonymousClass00n.A08;
    }
}
