package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.10c  reason: invalid class name and case insensitive filesystem */
public final class C180810c {
    private static volatile C180810c A03;
    public final C32311lY A00;
    public final C13420rP A01;
    public final Boolean A02;

    public static final C180810c A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C180810c.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C180810c(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C180810c(AnonymousClass1XY r2) {
        this.A01 = C13420rP.A00(r2);
        this.A02 = AnonymousClass0UU.A08(r2);
        this.A00 = C32311lY.A00(r2);
    }
}
