package X;

import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

/* renamed from: X.1JI  reason: invalid class name */
public final class AnonymousClass1JI {
    private static byte A00;

    public static int A00(Resources resources, PackageManager packageManager, int i) {
        int i2;
        int mode = View.MeasureSpec.getMode(i);
        if (mode != 0) {
            if (A00 == 0) {
                try {
                    byte b = 1;
                    if (packageManager.hasSystemFeature("org.chromium.arc.device_management")) {
                        b = 2;
                    }
                    A00 = b;
                } catch (RuntimeException unused) {
                    A00 = 1;
                }
            }
            Configuration configuration = resources.getConfiguration();
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            float f = displayMetrics.density;
            float f2 = (float) configuration.screenWidthDp;
            if (A00 == 2) {
                i2 = (int) ((f2 * f) + 0.5f);
            } else {
                i2 = displayMetrics.widthPixels;
            }
            int i3 = (int) ((f * f2) + 0.5f);
            if (i2 != i3 && i3 == View.MeasureSpec.getSize(i)) {
                return View.MeasureSpec.makeMeasureSpec(i2, mode);
            }
        }
        return i;
    }
}
