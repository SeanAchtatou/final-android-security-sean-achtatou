package X;

import android.app.Application;

/* renamed from: X.00e  reason: invalid class name */
public final class AnonymousClass00e {
    public static volatile Application A00;

    public static Application A00() {
        if (A00 != null) {
            return A00;
        }
        throw new IllegalStateException("ApplicationHolder#set never called");
    }
}
