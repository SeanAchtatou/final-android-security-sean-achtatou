package X;

import java.io.File;
import java.util.Comparator;

/* renamed from: X.0PN  reason: invalid class name */
public final class AnonymousClass0PN implements Comparator {
    public int compare(Object obj, Object obj2) {
        long lastModified = ((File) obj).lastModified();
        long lastModified2 = ((File) obj2).lastModified();
        if (lastModified == lastModified2) {
            return 0;
        }
        if (lastModified < lastModified2) {
            return 1;
        }
        return -1;
    }
}
