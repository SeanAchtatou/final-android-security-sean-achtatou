package X;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0Cu  reason: invalid class name and case insensitive filesystem */
public final class C02080Cu implements ThreadFactory {
    private final AtomicInteger A00 = new AtomicInteger(1);

    public Thread newThread(Runnable runnable) {
        return new AnonymousClass0Jt(runnable, AnonymousClass08S.A09("RtiExecutor #", this.A00.getAndIncrement()), 9);
    }
}
