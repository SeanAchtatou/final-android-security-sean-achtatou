package X;

/* renamed from: X.0XI  reason: invalid class name */
public final class AnonymousClass0XI {
    public final AnonymousClass1YK A00 = new AnonymousClass1YK();
    public final AnonymousClass1YM A01 = new AnonymousClass1YM();
    public final AnonymousClass1YN A02 = new AnonymousClass1YN();
    public final AnonymousClass1YO A03 = new AnonymousClass1YO();

    public void A00(AnonymousClass1Y7 r3, AnonymousClass1ZH r4) {
        if (r4 instanceof AnonymousClass0ZM) {
            this.A00.A01(r3, (AnonymousClass0ZM) r4);
        }
        if (r4 instanceof C26001ak) {
            this.A02.A01(r3, (C26001ak) r4);
        }
    }

    public void A01(AnonymousClass1Y7 r3, AnonymousClass1ZH r4) {
        if (r4 instanceof AnonymousClass0ZM) {
            this.A01.A01(r3, (AnonymousClass0ZM) r4);
        }
        if (r4 instanceof C26001ak) {
            this.A03.A01(r3, (C26001ak) r4);
        }
    }

    public void A02(AnonymousClass1Y7 r3, AnonymousClass1ZH r4) {
        if (r4 instanceof AnonymousClass0ZM) {
            this.A00.A02(r3, (AnonymousClass0ZM) r4);
        }
        if (r4 instanceof C26001ak) {
            this.A02.A02(r3, (C26001ak) r4);
        }
    }

    public void A03(AnonymousClass1Y7 r3, AnonymousClass1ZH r4) {
        if (r4 instanceof AnonymousClass0ZM) {
            this.A01.A02(r3, (AnonymousClass0ZM) r4);
        }
        if (r4 instanceof C26001ak) {
            this.A03.A02(r3, (C26001ak) r4);
        }
    }
}
