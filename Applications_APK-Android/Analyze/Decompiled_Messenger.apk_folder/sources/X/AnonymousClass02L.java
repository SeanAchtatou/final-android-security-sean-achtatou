package X;

import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.Application;
import android.os.Build;
import android.os.PowerManager;
import android.os.Process;
import android.os.SystemClock;

/* renamed from: X.02L  reason: invalid class name */
public final class AnonymousClass02L {
    public static final AnonymousClass02L A05 = new AnonymousClass02L();
    public volatile int A00 = -1;
    public volatile Boolean A01;
    private volatile long A02 = -1;
    private volatile boolean A03;
    private volatile boolean A04 = false;

    public static boolean A03(AnonymousClass02L r4) {
        boolean z = false;
        try {
            String A002 = AnonymousClass00V.A00(AnonymousClass08S.A0A("/proc/", Process.myPid(), "/cgroup"));
            if (A002 != null && (A002.contains("/bg_non_interactive") || A002.contains("/background"))) {
                z = true;
            }
        } catch (RuntimeException unused) {
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        r4.A04 = z;
        r4.A02 = uptimeMillis;
        return z;
    }

    private static Application A01() {
        ActivityThread activityThread = AnonymousClass00d.A00;
        if (activityThread == null) {
            activityThread = ActivityThread.currentActivityThread();
            AnonymousClass00d.A00 = activityThread;
        }
        return activityThread.getApplication();
    }

    private static boolean A02(int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (A01().getBaseContext() == null) {
                return false;
            }
            if (!((PowerManager) A01().getSystemService("power")).isInteractive()) {
                if (i <= 325) {
                    return true;
                }
                return false;
            }
        }
        if (i <= 100) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0021, code lost:
        if (A02(A00()) != false) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0044, code lost:
        if (r2 != false) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A04() {
        /*
            r5 = this;
            int r0 = r5.A00
            r3 = 1
            if (r0 >= r3) goto L_0x0012
            int r2 = A00()
            r1 = 200(0xc8, float:2.8E-43)
            r0 = 0
            if (r2 > r1) goto L_0x000f
            r0 = 1
        L_0x000f:
            if (r0 != 0) goto L_0x0012
            r3 = 0
        L_0x0012:
            byte r2 = (byte) r3
            int r1 = r5.A00
            r0 = 2
            if (r1 >= r0) goto L_0x0023
            int r0 = A00()
            boolean r0 = A02(r0)
            r1 = 0
            if (r0 == 0) goto L_0x0024
        L_0x0023:
            r1 = 1
        L_0x0024:
            r0 = 1
            int r1 = r1 << r0
            r2 = r2 | r1
            byte r4 = (byte) r2
            int r1 = r5.A00
            r0 = 3
            if (r1 >= r0) goto L_0x0046
            int r3 = A00()
            boolean r0 = A02(r3)
            r2 = 0
            if (r0 == 0) goto L_0x0043
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 < r0) goto L_0x006a
            r0 = 100
            if (r3 > r0) goto L_0x0043
            r2 = 1
        L_0x0043:
            r0 = 0
            if (r2 == 0) goto L_0x0047
        L_0x0046:
            r0 = 1
        L_0x0047:
            int r0 = r0 << 2
            r4 = r4 | r0
            byte r1 = (byte) r4
            boolean r0 = A03(r5)
            int r0 = r0 << 3
            r1 = r1 | r0
            byte r1 = (byte) r1
            boolean r0 = r5.A05()
            int r0 = r0 << 4
            r1 = r1 | r0
            byte r0 = (byte) r1
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "0x%04X"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            return r0
        L_0x006a:
            android.app.Application r1 = A01()
            java.lang.String r0 = "power"
            java.lang.Object r0 = r1.getSystemService(r0)
            android.os.PowerManager r0 = (android.os.PowerManager) r0
            boolean r2 = r0.isScreenOn()
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02L.A04():java.lang.String");
    }

    public boolean A05() {
        long j = this.A02;
        if (j == -1 || SystemClock.uptimeMillis() - j >= 500) {
            return A03(this);
        }
        return this.A04;
    }

    private AnonymousClass02L() {
    }

    private static int A00() {
        if (A01().getBaseContext() != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
                ActivityManager.getMyMemoryState(runningAppProcessInfo);
                return runningAppProcessInfo.importance;
            }
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) A01().getSystemService("activity")).getRunningAppProcesses()) {
                if (next.pid == Process.myPid()) {
                    return next.importance;
                }
            }
        }
        return Integer.MAX_VALUE;
    }
}
