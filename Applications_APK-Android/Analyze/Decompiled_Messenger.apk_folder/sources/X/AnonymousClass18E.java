package X;

import java.security.Provider;

/* renamed from: X.18E  reason: invalid class name */
public final class AnonymousClass18E extends Provider {
    public AnonymousClass18E() {
        super("LinuxPRNG", 1.0d, "A Linux-specific random number provider that uses /dev/urandom");
        put("SecureRandom.SHA1PRNG", C30251hk.class.getName());
        put("SecureRandom.SHA1PRNG ImplementedIn", "Software");
    }
}
