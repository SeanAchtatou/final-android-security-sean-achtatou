package X;

/* renamed from: X.0Ws  reason: invalid class name and case insensitive filesystem */
public final class C04890Ws implements AnonymousClass1YQ {
    private final AnonymousClass02v A00;

    public String getSimpleName() {
        return "Initializer";
    }

    public static final C04890Ws A00(AnonymousClass1XY r1) {
        return new C04890Ws(r1);
    }

    private C04890Ws(AnonymousClass1XY r2) {
        this.A00 = C04880Wr.A00(r2);
    }

    public void init() {
        int A03 = C000700l.A03(-928251985);
        C010708t.A03(this.A00);
        C000700l.A09(-1151070288, A03);
    }
}
