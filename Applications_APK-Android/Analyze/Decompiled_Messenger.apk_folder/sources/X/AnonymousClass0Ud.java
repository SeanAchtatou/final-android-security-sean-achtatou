package X;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.DeadObjectException;
import android.os.MessageQueue;
import com.facebook.breakpad.BreakpadManager;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Ud  reason: invalid class name */
public final class AnonymousClass0Ud {
    public static final AnonymousClass1Y7 A0Y = ((AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("app_state/")).A09("last_first_run_time"));
    public static final AnonymousClass1Y7 A0Z = ((AnonymousClass1Y7) C04350Ue.A07.A09("ui_thread_watchdog"));
    private static volatile AnonymousClass0Ud A0a;
    public int A00 = 0;
    public int A01 = 0;
    public AnonymousClass0UN A02;
    public Boolean A03;
    public Boolean A04;
    public String A05;
    public ScheduledFuture A06;
    private Boolean A07;
    public final MessageQueue.IdleHandler A08 = new C04370Uk(this, "AppStateManager-EnteredAppDelayed");
    public final AnonymousClass1Y9 A09 = new AnonymousClass1Y9(this);
    public final Runnable A0A = new C04360Uj(this);
    public final Runnable A0B = new C04400Un(this);
    private final Runnable A0C = new C04390Um(this);
    public volatile long A0D;
    public volatile long A0E;
    public volatile long A0F;
    public volatile long A0G;
    public volatile long A0H;
    public volatile long A0I;
    public volatile long A0J = Long.MIN_VALUE;
    public volatile long A0K;
    public volatile long A0L;
    public volatile long A0M;
    public volatile long A0N;
    public volatile long A0O;
    public volatile long A0P;
    public volatile long A0Q;
    public volatile long A0R;
    public volatile String A0S;
    public volatile boolean A0T;
    public volatile boolean A0U = false;
    public volatile boolean A0V = false;
    public volatile boolean A0W;
    private volatile WeakReference A0X;

    public static void A04(AnonymousClass0Ud r6) {
        boolean A0J2;
        synchronized (r6) {
            A0J2 = r6.A0J();
            r6.A00++;
        }
        ScheduledFuture scheduledFuture = r6.A06;
        if (scheduledFuture == null && !A0J2) {
            A02(r6);
            r6.A0L = ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, r6.A02)).now();
            r6.A0M = ((AnonymousClass06B) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AgK, r6.A02)).now();
        } else if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
            r6.A06 = null;
        }
        A05(r6, true);
        int i = r6.A00;
        if (i > 1) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(14, AnonymousClass1Y3.Amr, r6.A02)).CGS("AppStateManager_activity_increase", AnonymousClass08S.A09("Count: ", i));
        }
        r6.A0T = true;
        r6.A0I = ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, r6.A02)).now();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r2.A00 > 0) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0J() {
        /*
            r2 = this;
            monitor-enter(r2)
            int r0 = r2.A01     // Catch:{ all -> 0x000d }
            if (r0 > 0) goto L_0x000a
            int r1 = r2.A00     // Catch:{ all -> 0x000d }
            r0 = 0
            if (r1 <= 0) goto L_0x000b
        L_0x000a:
            r0 = 1
        L_0x000b:
            monitor-exit(r2)
            return r0
        L_0x000d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Ud.A0J():boolean");
    }

    public static final AnonymousClass0Ud A01(AnonymousClass1XY r4) {
        if (A0a == null) {
            synchronized (AnonymousClass0Ud.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0a, r4);
                if (A002 != null) {
                    try {
                        A0a = new AnonymousClass0Ud(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0a;
    }

    public static void A02(AnonymousClass0Ud r3) {
        ((AnonymousClass1YR) AnonymousClass1XX.A02(15, AnonymousClass1Y3.AdX, r3.A02)).A01("app_foregrounded");
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(5, AnonymousClass1Y3.APr, r3.A02)).C4C(r3.A0C);
    }

    public static void A03(AnonymousClass0Ud r11) {
        AnonymousClass0WR r1;
        long At2 = ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, r11.A02)).At2(A0Y, 0);
        String packageName = ((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, r11.A02)).getPackageName();
        try {
            PackageInfo packageInfo = ((PackageManager) AnonymousClass1XX.A02(10, AnonymousClass1Y3.A6m, r11.A02)).getPackageInfo(packageName, 0);
            long max = Math.max(packageInfo.firstInstallTime, packageInfo.lastUpdateTime);
            r11.A0J = Math.max(max, At2);
            if (max > At2) {
                boolean z = false;
                if (max == packageInfo.firstInstallTime) {
                    z = true;
                }
                r11.A0U = z;
                boolean z2 = false;
                if (max == packageInfo.lastUpdateTime) {
                    z2 = true;
                }
                r11.A0V = z2;
                C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, r11.A02)).edit();
                edit.BzA(A0Y, max);
                edit.commit();
                if (!(packageInfo.firstInstallTime == packageInfo.lastUpdateTime || (r1 = (AnonymousClass0WR) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Aju, r11.A02)) == null || r1.BKI() == 0)) {
                    int BKI = r1.BKI();
                    for (int i = 0; i < BKI; i++) {
                        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BKH, r11.A02), r11.A0B, -1166179803);
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            C010708t.A0V("AppStateManager", e, "Can't find our own package name : %s", packageName);
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof DeadObjectException) {
                C010708t.A0S("AppStateManager", e2, "PackageManager connection lost");
                return;
            }
            throw e2;
        }
        ((AnonymousClass09P) AnonymousClass1XX.A02(14, AnonymousClass1Y3.Amr, r11.A02)).Bz2("is_first_run_after_upgrade", Boolean.toString(r11.A0V));
        if (BreakpadManager.isActive()) {
            BreakpadManager.setCustomData("is_first_run_after_upgrade", Boolean.toString(r11.A0V), new Object[0]);
        }
    }

    public static void A05(AnonymousClass0Ud r5, boolean z) {
        if (((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, r5.A02)).Aep(A0Z, false)) {
            Intent intent = new Intent("com.facebook.common.appstate.AppStateManager.USER_MAYBE_BECAME_ACTIVE_OR_INACTIVE_IN_APP");
            if (z) {
                intent.putExtra("WINDOW_ACTIVE_HINT", 1);
            }
            ((C04460Ut) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AKb, r5.A02)).C4x(intent);
        }
    }

    private boolean A06() {
        if (this.A07 == null) {
            this.A07 = Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(17, AnonymousClass1Y3.AOJ, this.A02)).Aem(283265978534158L));
        }
        return this.A07.booleanValue();
    }

    public long A07() {
        return ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now() - this.A0D;
    }

    public long A08() {
        return ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now() - this.A0E;
    }

    public long A09() {
        return Math.min(((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now() - this.A0H, ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now() - this.A0Q);
    }

    public long A0A() {
        return Math.min(((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now() - this.A0I, ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now() - this.A0R);
    }

    public Activity A0B() {
        WeakReference weakReference = this.A0X;
        if (weakReference != null) {
            return (Activity) weakReference.get();
        }
        return null;
    }

    public TriState A0C() {
        if (this.A0D != 0) {
            if (this.A0F == 0) {
                if (A07() > 4000) {
                    return TriState.YES;
                }
            } else if (this.A0F - this.A0D > 4000) {
                return TriState.YES;
            } else {
                return TriState.NO;
            }
        }
        return TriState.UNSET;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, boolean):void
     arg types: [int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, int):void
      com.facebook.quicklog.QuickPerformanceLogger.markerStart(int, boolean):void */
    public void A0E(Activity activity) {
        boolean CEc;
        int intExtra;
        this.A0X = new WeakReference(activity);
        if (A06()) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(16, AnonymousClass1Y3.BBd, this.A02)).markerStart(44826633, false);
            if (((QuickPerformanceLogger) AnonymousClass1XX.A02(16, AnonymousClass1Y3.BBd, this.A02)).isMarkerOn(44826633)) {
                String name = activity.getClass().getName();
                Intent intent = activity.getIntent();
                if (!(intent == null || (intExtra = intent.getIntExtra(AnonymousClass80H.$const$string(7), -1)) == -1)) {
                    name = AnonymousClass08S.A0P(name, ":", Integer.toString(intExtra));
                }
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(16, AnonymousClass1Y3.BBd, this.A02)).markerAnnotate(44826633, "activity_name", name);
            }
        }
        if (!(activity instanceof AnonymousClass145)) {
            CEc = true;
        } else {
            CEc = ((AnonymousClass145) activity).CEc();
        }
        if (CEc) {
            A04(this);
        }
    }

    public void A0F(Activity activity) {
        boolean CEc;
        if (!(activity instanceof AnonymousClass145)) {
            CEc = true;
        } else {
            CEc = ((AnonymousClass145) activity).CEc();
        }
        if (CEc) {
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(5, AnonymousClass1Y3.APr, this.A02)).AOz();
            this.A0N = ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now();
            A05(this, false);
        }
    }

    public boolean A0I() {
        if (this.A0I > 0 || this.A0R > 0) {
            return true;
        }
        return false;
    }

    private AnonymousClass0Ud(AnonymousClass1XY r4) {
        this.A02 = new AnonymousClass0UN(18, r4);
    }

    public static final AnonymousClass0Ud A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public void A0D(Activity activity) {
        boolean CEc;
        if (A06()) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(16, AnonymousClass1Y3.BBd, this.A02)).markerEnd(44826633, 2);
        }
        this.A0X = null;
        if (!(activity instanceof AnonymousClass145)) {
            CEc = true;
        } else {
            CEc = ((AnonymousClass145) activity).CEc();
        }
        if (CEc) {
            this.A06 = ((ScheduledExecutorService) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BKX, this.A02)).schedule(this.A0A, (long) LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT, TimeUnit.MILLISECONDS);
            A05(this, false);
            synchronized (this) {
                int i = this.A00;
                if (i != 1) {
                    ((AnonymousClass09P) AnonymousClass1XX.A02(14, AnonymousClass1Y3.Amr, this.A02)).CGS("AppStateManager_activity_decrease", AnonymousClass08S.A09("Count: ", i));
                }
                int i2 = this.A00;
                if (i2 > 0) {
                    this.A00 = i2 - 1;
                }
            }
            this.A0H = ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now();
            this.A0T = false;
        }
    }

    public boolean A0G() {
        if (A0J() || A09() <= LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT) {
            return false;
        }
        return true;
    }

    public boolean A0H() {
        if (A0J() && !((KeyguardManager) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AWM, this.A02)).inKeyguardRestrictedInputMode()) {
            return true;
        }
        if (A0J() || A08() >= 4000) {
            return false;
        }
        return true;
    }

    public boolean A0K(long j) {
        if (A08() < 4000 || ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, this.A02)).now() - this.A0N <= j) {
            return true;
        }
        return false;
    }
}
