package X;

import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import com.facebook.user.model.Name;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.facebook.user.model.UserPhoneNumber;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

/* renamed from: X.16a  reason: invalid class name and case insensitive filesystem */
public final class C189016a implements AnonymousClass1EM {
    public static final Class A0C = C189016a.class;
    public AnonymousClass1G0 A00;
    public C20021Ap A01;
    public AnonymousClass0UN A02;
    public C192617l A03;
    public C189816i A04 = C189816i.A0H;
    public boolean A05 = false;
    public final AnonymousClass09P A06;
    public final C05330Yn A07;
    public final C190116l A08;
    public final AnonymousClass0r6 A09;
    public final Executor A0A;
    private final C189416e A0B;

    public static final C189016a A00(AnonymousClass1XY r7) {
        return new C189016a(r7, C14890uJ.A00(r7), C190116l.A00(r7), AnonymousClass0UX.A0U(r7), C189416e.A02(r7), C04750Wa.A01(r7), C05330Yn.A00(r7));
    }

    public static ImmutableList A01(C189016a r3, Integer num) {
        C005505z.A03("getTopPhoneContacts", -1386779344);
        try {
            ImmutableList A052 = ((AnonymousClass7RS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ABK, r3.A02)).A05(10, num);
            A052.size();
            return A052;
        } finally {
            C005505z.A00(-1295831213);
        }
    }

    public static ImmutableList A02(C189016a r4, Map map, boolean z) {
        if (z) {
            C005505z.A03("getAllContactsWithCap", -1360826777);
        } else {
            C005505z.A03("getAllContacts", -82514487);
        }
        try {
            C42572Az A022 = ((C42562Ay) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ayz, r4.A02)).A02();
            if (z) {
                A022.A00 = C855143s.A00;
            }
            return A04(map, ((C42532Av) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BEw, r4.A02)).A01(A022), false);
        } finally {
            C005505z.A00(-1317725412);
        }
    }

    public static ImmutableList A03(C189016a r11, Map map, boolean z) {
        Cursor query;
        String str;
        C005505z.A03("getSmsInviteContacts", 1013009624);
        try {
            ImmutableList.Builder builder = ImmutableList.builder();
            try {
                C189416e r3 = r11.A0B;
                ArrayList A002 = C04300To.A00();
                Uri uri = ContactsContract.Contacts.CONTENT_URI;
                C06140av A032 = C06160ax.A03("has_phone_number", "1");
                query = r3.A00.query(uri, C189416e.A06, A032.A02(), A032.A04(), null);
                while (query.moveToNext()) {
                    A002.add(Integer.valueOf(query.getInt(0)));
                }
                query.close();
                Cursor A012 = C189416e.A01(r3, A002);
                C189516f r32 = r3.A02;
                new C50642eM(r32);
                AnonymousClass7US r6 = new AnonymousClass7US(r32, A012);
                AnonymousClass7US r10 = r6;
                while (r6.hasNext()) {
                    User user = (User) r6.next();
                    if (user.A05() != null && !user.A05().isEmpty()) {
                        C24971Xv it = user.A05().iterator();
                        while (it.hasNext()) {
                            UserPhoneNumber userPhoneNumber = (UserPhoneNumber) it.next();
                            if (!z || userPhoneNumber.A00 == 2) {
                                C07220cv r7 = new C07220cv();
                                r7.A04(user);
                                r7.A05(user.A0j, userPhoneNumber.A03);
                                r7.A17 = ImmutableList.of(userPhoneNumber);
                                if (C06850cB.A0B(user.A0t)) {
                                    Name name = user.A0L;
                                    boolean z2 = false;
                                    if (name.firstName != null) {
                                        z2 = true;
                                    }
                                    if (!z2) {
                                        boolean z3 = false;
                                        if (name.lastName != null) {
                                            z3 = true;
                                        }
                                        if (!z3) {
                                            boolean z4 = false;
                                            if (name.displayName != null) {
                                                z4 = true;
                                            }
                                            if (!z4) {
                                                str = null;
                                                r7.A0y = str;
                                            }
                                        }
                                    }
                                    C75753kO r1 = new C75753kO();
                                    r1.A00 = user.A0A();
                                    r1.A01 = user.A09();
                                    r1.A02 = user.A0B();
                                    str = ((AnonymousClass2BI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AzI, r11.A02)).A02(r11.A07.A06(), new C75763kP(r1));
                                    r7.A0y = str;
                                }
                                builder.add((Object) r7.A02());
                            }
                        }
                    }
                }
                r6.A00.close();
                ImmutableList A042 = A04(map, builder.build(), true);
                LinkedList linkedList = new LinkedList();
                AnonymousClass0j4.A0D(linkedList, A042);
                Collections.sort(linkedList, new AnonymousClass51E());
                return ImmutableList.copyOf((Collection) linkedList);
            } catch (Throwable th) {
                if (0 != 0) {
                    null.A00.close();
                }
                throw th;
            }
        } finally {
            C005505z.A00(-172133139);
        }
    }

    public static ImmutableList A04(Map map, List list, boolean z) {
        HashSet hashSet;
        if (z) {
            hashSet = new HashSet(list.size());
        } else {
            hashSet = null;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            User user = (User) it.next();
            UserKey userKey = user.A0Q;
            User user2 = (User) map.get(userKey);
            if (user2 == null) {
                map.put(userKey, user);
            } else {
                user = user2;
            }
            if (hashSet == null || !hashSet.contains(userKey)) {
                if (hashSet != null) {
                    hashSet.add(userKey);
                }
                builder.add((Object) user);
            }
        }
        return builder.build();
    }

    public void A05() {
        String str;
        Executor executor;
        C20021Ap r1;
        Preconditions.checkNotNull(this.A03);
        C189816i r2 = this.A04;
        if (!(r2 == null || r2 == C189816i.A0H || (r1 = this.A01) == null)) {
            r1.Bgh(null, r2);
        }
        boolean z = false;
        if (this.A00 == null) {
            z = true;
        }
        if (z) {
            AnonymousClass1FA r22 = new AnonymousClass1FA(this);
            AnonymousClass108 r12 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, this.A02);
            r12.A05 = r22;
            r12.A02 = "ContactsLoader";
            if (this.A05) {
                str = "Foreground";
            } else {
                str = "Default";
            }
            r12.A03(str);
            ListenableFuture A042 = ((AnonymousClass0g4) AnonymousClass1XX.A02(8, AnonymousClass1Y3.ASI, this.A02)).A04(r12.A01(), "None");
            C20021Ap r13 = this.A01;
            if (r13 != null) {
                r13.BdU(null, A042);
            }
            AnonymousClass1FB r14 = new AnonymousClass1FB(this);
            if (this.A05) {
                executor = C25141Ym.INSTANCE;
            } else {
                executor = this.A0A;
            }
            C05350Yp.A08(A042, r14, executor);
            this.A00 = AnonymousClass1G0.A00(A042, r14);
        }
    }

    public void ARp() {
        AnonymousClass1G0 r1 = this.A00;
        if (r1 != null) {
            r1.A01(false);
            this.A00 = null;
        }
        if (this.A04.A0E) {
            this.A04 = C189816i.A0H;
        }
    }

    private C189016a(AnonymousClass1XY r3, AnonymousClass0r6 r4, C190116l r5, Executor executor, C189416e r7, AnonymousClass09P r8, C05330Yn r9) {
        this.A02 = new AnonymousClass0UN(12, r3);
        this.A09 = r4;
        this.A08 = r5;
        this.A0A = executor;
        this.A0B = r7;
        this.A06 = r8;
        this.A07 = r9;
    }

    public void C6Z(C20021Ap r1) {
        this.A01 = r1;
    }

    public /* bridge */ /* synthetic */ void CHB(Object obj) {
        A05();
    }
}
