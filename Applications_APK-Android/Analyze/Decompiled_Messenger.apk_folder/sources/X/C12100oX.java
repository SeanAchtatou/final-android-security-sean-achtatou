package X;

/* renamed from: X.0oX  reason: invalid class name and case insensitive filesystem */
public final class C12100oX {
    public AnonymousClass0UN A00;
    public final AnonymousClass0jD A01;
    public final C11940oH A02;
    public final C08690fm A03;
    public final C12080oV A04;

    public static final C12100oX A00(AnonymousClass1XY r1) {
        return new C12100oX(r1);
    }

    public C12100oX(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = AnonymousClass0jD.A00(r3);
        this.A04 = C12080oV.A00(r3);
        this.A02 = new C11940oH(r3);
        this.A03 = new C08690fm(r3);
    }
}
