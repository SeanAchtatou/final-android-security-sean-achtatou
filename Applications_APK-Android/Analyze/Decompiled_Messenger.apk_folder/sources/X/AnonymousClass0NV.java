package X;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* renamed from: X.0NV  reason: invalid class name */
public final class AnonymousClass0NV extends AnonymousClass0EL {
    public final ZipEntry A00;
    public final ZipFile A01;
    public final AnonymousClass0Na[] A02;
    public final /* synthetic */ AnonymousClass02N A03;

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f5, code lost:
        r20.A01 = (java.lang.String[]) r11.toArray(new java.lang.String[r11.size()]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0105, code lost:
        if (r5 == null) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x010a, code lost:
        r3 = r10.values().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0116, code lost:
        if (r3.hasNext() == false) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0118, code lost:
        ((X.AnonymousClass0Na) r3.next()).A00 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0122, code lost:
        r4.A02 = (X.AnonymousClass0Na[]) r7.toArray(new X.AnonymousClass0Na[r7.size()]);
        r4.A00 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0135, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0136, code lost:
        if (r5 != null) goto L_0x0138;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:67:0x013b */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007f A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00e7 A[ADDED_TO_REGION, EDGE_INSN: B:79:0x00e7->B:49:0x00e7 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0NV(X.AnonymousClass02N r19, X.AnonymousClass02E r20) {
        /*
            r18 = this;
            r4 = r18
            r3 = r19
            r4.A03 = r3
            r18.<init>()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.util.HashMap r10 = new java.util.HashMap
            r10.<init>()
            java.lang.String[] r13 = X.C002401o.A02()
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile
            java.io.File r0 = r3.A01
            r2.<init>(r0)
            java.lang.String r0 = r3.A02     // Catch:{ all -> 0x0144 }
            java.util.zip.ZipEntry r1 = r2.getEntry(r0)     // Catch:{ all -> 0x0144 }
            java.lang.String r0 = r3.A03     // Catch:{ all -> 0x0144 }
            java.util.zip.ZipEntry r6 = r2.getEntry(r0)     // Catch:{ all -> 0x0144 }
            if (r1 == 0) goto L_0x013c
            if (r6 == 0) goto L_0x013c
            java.io.InputStream r5 = r2.getInputStream(r1)     // Catch:{ all -> 0x0144 }
            java.util.LinkedHashSet r11 = new java.util.LinkedHashSet     // Catch:{ all -> 0x0133 }
            r11.<init>()     // Catch:{ all -> 0x0133 }
            java.io.BufferedReader r12 = new java.io.BufferedReader     // Catch:{ all -> 0x0133 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ all -> 0x0133 }
            r0.<init>(r5)     // Catch:{ all -> 0x0133 }
            r12.<init>(r0)     // Catch:{ all -> 0x0133 }
            android.text.TextUtils$SimpleStringSplitter r9 = new android.text.TextUtils$SimpleStringSplitter     // Catch:{ all -> 0x0133 }
            r0 = 32
            r9.<init>(r0)     // Catch:{ all -> 0x0133 }
        L_0x0048:
            java.lang.String r8 = r12.readLine()     // Catch:{ all -> 0x0133 }
            if (r8 == 0) goto L_0x00f5
            int r0 = r8.length()     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x0048
            r9.setString(r8)     // Catch:{ all -> 0x0133 }
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x0076
            java.lang.String r3 = r9.next()     // Catch:{ all -> 0x0133 }
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x0077
            java.lang.String r1 = r9.next()     // Catch:{ all -> 0x0133 }
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x0078
            java.lang.String r14 = r9.next()     // Catch:{ all -> 0x0133 }
            goto L_0x0079
        L_0x0076:
            r3 = 0
        L_0x0077:
            r1 = 0
        L_0x0078:
            r14 = 0
        L_0x0079:
            java.lang.String r17 = "]"
            java.lang.String r16 = "illegal line in compressed metadata: ["
            if (r3 == 0) goto L_0x00e7
            if (r1 == 0) goto L_0x00e7
            if (r14 == 0) goto L_0x00e7
            java.lang.String r0 = "assets/lib/"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)     // Catch:{ all -> 0x0133 }
            java.util.zip.ZipEntry r0 = r2.getEntry(r0)     // Catch:{ all -> 0x0133 }
            if (r0 != 0) goto L_0x0048
            int r0 = java.lang.Integer.parseInt(r1)     // Catch:{ all -> 0x0133 }
            r1 = 47
            int r1 = r3.indexOf(r1)     // Catch:{ all -> 0x0133 }
            r15 = -1
            if (r1 == r15) goto L_0x00d9
            r8 = 0
            java.lang.String r15 = r3.substring(r8, r1)     // Catch:{ all -> 0x0133 }
            int r1 = r1 + 1
            java.lang.String r8 = r3.substring(r1)     // Catch:{ all -> 0x0133 }
            r3 = 0
        L_0x00a8:
            int r1 = r13.length     // Catch:{ all -> 0x0133 }
            if (r3 >= r1) goto L_0x00c1
            r1 = r13[r3]     // Catch:{ all -> 0x0133 }
            if (r1 == 0) goto L_0x00be
            boolean r1 = r15.equals(r1)     // Catch:{ all -> 0x0133 }
            if (r1 == 0) goto L_0x00be
        L_0x00b5:
            X.0Na r1 = new X.0Na     // Catch:{ all -> 0x0133 }
            r1.<init>(r8, r14, r0, r3)     // Catch:{ all -> 0x0133 }
            r7.add(r1)     // Catch:{ all -> 0x0133 }
            goto L_0x00c3
        L_0x00be:
            int r3 = r3 + 1
            goto L_0x00a8
        L_0x00c1:
            r3 = -1
            goto L_0x00b5
        L_0x00c3:
            if (r3 < 0) goto L_0x0048
            r11.add(r15)     // Catch:{ all -> 0x0133 }
            java.lang.Object r0 = r10.get(r8)     // Catch:{ all -> 0x0133 }
            X.0Na r0 = (X.AnonymousClass0Na) r0     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x00d4
            int r0 = r0.A01     // Catch:{ all -> 0x0133 }
            if (r3 >= r0) goto L_0x0048
        L_0x00d4:
            r10.put(r8, r1)     // Catch:{ all -> 0x0133 }
            goto L_0x0048
        L_0x00d9:
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ all -> 0x0133 }
            r1 = r16
            r0 = r17
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r8, r0)     // Catch:{ all -> 0x0133 }
            r3.<init>(r0)     // Catch:{ all -> 0x0133 }
            goto L_0x00f4
        L_0x00e7:
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ all -> 0x0133 }
            r1 = r16
            r0 = r17
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r8, r0)     // Catch:{ all -> 0x0133 }
            r3.<init>(r0)     // Catch:{ all -> 0x0133 }
        L_0x00f4:
            throw r3     // Catch:{ all -> 0x0133 }
        L_0x00f5:
            int r0 = r11.size()     // Catch:{ all -> 0x0133 }
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x0133 }
            java.lang.Object[] r0 = r11.toArray(r0)     // Catch:{ all -> 0x0133 }
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ all -> 0x0133 }
            r1 = r20
            r1.A01 = r0     // Catch:{ all -> 0x0133 }
            if (r5 == 0) goto L_0x010a
            r5.close()     // Catch:{ all -> 0x0144 }
        L_0x010a:
            java.util.Collection r0 = r10.values()     // Catch:{ all -> 0x0144 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x0144 }
        L_0x0112:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0144 }
            if (r0 == 0) goto L_0x0122
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x0144 }
            X.0Na r1 = (X.AnonymousClass0Na) r1     // Catch:{ all -> 0x0144 }
            r0 = 1
            r1.A00 = r0     // Catch:{ all -> 0x0144 }
            goto L_0x0112
        L_0x0122:
            int r0 = r7.size()     // Catch:{ all -> 0x0144 }
            X.0Na[] r0 = new X.AnonymousClass0Na[r0]     // Catch:{ all -> 0x0144 }
            java.lang.Object[] r0 = r7.toArray(r0)     // Catch:{ all -> 0x0144 }
            X.0Na[] r0 = (X.AnonymousClass0Na[]) r0     // Catch:{ all -> 0x0144 }
            r4.A02 = r0     // Catch:{ all -> 0x0144 }
            r4.A00 = r6     // Catch:{ all -> 0x0144 }
            goto L_0x0141
        L_0x0133:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0135 }
        L_0x0135:
            r0 = move-exception
            if (r5 == 0) goto L_0x013b
            r5.close()     // Catch:{ all -> 0x013b }
        L_0x013b:
            throw r0     // Catch:{ all -> 0x0144 }
        L_0x013c:
            r0 = 0
            X.0Na[] r0 = new X.AnonymousClass0Na[r0]     // Catch:{ all -> 0x0144 }
            r4.A02 = r0     // Catch:{ all -> 0x0144 }
        L_0x0141:
            r4.A01 = r2     // Catch:{ all -> 0x0144 }
            return
        L_0x0144:
            r0 = move-exception
            r2.close()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0NV.<init>(X.02N, X.02E):void");
    }

    public AnonymousClass0ET A00() {
        return new AnonymousClass0ET(this.A02);
    }

    public AnonymousClass0EM A01() {
        if (this.A00 == null) {
            return new AnonymousClass0NU();
        }
        return new AnonymousClass0NW(this);
    }

    public void close() {
        this.A01.close();
    }
}
