package X;

import android.os.Process;
import com.facebook.common.dextricks.DalvikInternals;

/* renamed from: X.1uf  reason: invalid class name and case insensitive filesystem */
public final class C37091uf implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messenger.app.MessengerApplicationImpl$2";
    public final /* synthetic */ boolean A00;

    public C37091uf(boolean z) {
        this.A00 = z;
    }

    public void run() {
        Integer num;
        if (this.A00) {
            num = Integer.valueOf(Process.getThreadPriority(Process.myTid()));
            Process.setThreadPriority(-14);
        } else {
            num = null;
        }
        DalvikInternals.mprotectExecCode();
        if (num != null) {
            Process.setThreadPriority(num.intValue());
        }
    }
}
