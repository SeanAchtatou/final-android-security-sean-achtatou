package X;

import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.google.common.base.Preconditions;

/* renamed from: X.12V  reason: invalid class name */
public final class AnonymousClass12V {
    public final GraphQLMessengerInboxUnitType A00;
    public final C27161ck A01;
    public final Object A02;

    public AnonymousClass12V(C27161ck r2, Object obj) {
        Preconditions.checkNotNull(r2);
        this.A00 = r2.A0Q();
        this.A01 = r2;
        this.A02 = obj;
    }
}
