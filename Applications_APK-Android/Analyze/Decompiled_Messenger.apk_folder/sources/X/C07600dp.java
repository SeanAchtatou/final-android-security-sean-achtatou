package X;

import java.util.ArrayList;

/* renamed from: X.0dp  reason: invalid class name and case insensitive filesystem */
public final class C07600dp implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.push.logging.PushBugReportBuffer$1";
    public final /* synthetic */ AnonymousClass1EX A00;
    public final /* synthetic */ ArrayList A01;

    public C07600dp(AnonymousClass1EX r1, ArrayList arrayList) {
        this.A00 = r1;
        this.A01 = arrayList;
    }

    public void run() {
        AnonymousClass1EX.A01(this.A00, this.A01);
    }
}
