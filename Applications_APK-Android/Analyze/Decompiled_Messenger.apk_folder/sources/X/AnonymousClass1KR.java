package X;

import android.net.Uri;
import com.facebook.user.model.UserKey;
import com.facebook.user.profilepic.PicSquare;

/* renamed from: X.1KR  reason: invalid class name */
public final class AnonymousClass1KR {
    public int A00;
    public int A01;
    public Uri A02;
    public UserKey A03;
    public PicSquare A04;
    public AnonymousClass1KS A05;
    public C21381Gs A06;
    public String A07;
    public String A08;
}
