package X;

import java.util.Iterator;

/* renamed from: X.191  reason: invalid class name */
public interface AnonymousClass191 extends Iterator {
    Object next();

    Object peek();

    void remove();
}
