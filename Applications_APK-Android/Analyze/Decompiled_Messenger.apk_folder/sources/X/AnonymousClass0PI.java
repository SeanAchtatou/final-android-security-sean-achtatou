package X;

/* renamed from: X.0PI  reason: invalid class name */
public final class AnonymousClass0PI {
    public static String A00(int i) {
        if (i == 1) {
            return "unknown";
        }
        if (i == 2) {
            return "controller_init";
        }
        if (i == 3) {
            return "missed_event";
        }
        if (i == 4) {
            return "timeout";
        }
        if (i != 5) {
            return AnonymousClass08S.A09("UNKNOWN REASON ", i);
        }
        return "new_start";
    }
}
