package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.1HL  reason: invalid class name */
public final class AnonymousClass1HL {
    private static C04470Uu A02;
    public final AnonymousClass1GC A00;
    public final C35471rH A01;

    public static final AnonymousClass1HL A00(AnonymousClass1XY r4) {
        AnonymousClass1HL r0;
        synchronized (AnonymousClass1HL.class) {
            C04470Uu A002 = C04470Uu.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass1HL((AnonymousClass1XY) A02.A01());
                }
                C04470Uu r1 = A02;
                r0 = (AnonymousClass1HL) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass1HL(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1GB.A00(r2);
        this.A01 = C35471rH.A00(r2);
    }
}
