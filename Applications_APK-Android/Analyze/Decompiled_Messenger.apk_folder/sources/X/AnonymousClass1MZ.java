package X;

import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1MZ  reason: invalid class name */
public final class AnonymousClass1MZ {
    public static final CancellationException A0E = new CancellationException("Prefetching is not enabled");
    public AtomicLong A00 = new AtomicLong();
    public final C23111Og A01;
    public final C23111Og A02;
    public final C23111Og A03;
    public final C30911iq A04;
    public final C30911iq A05;
    public final C22601Mc A06;
    public final C23311Pa A07;
    public final C23311Pa A08;
    public final AnonymousClass1PL A09;
    public final AnonymousClass1PW A0A;
    public final AnonymousClass1MK A0B;
    private final AnonymousClass1PD A0C;
    private final C23411Pk A0D;

    /* JADX WARNING: Code restructure failed: missing block: B:119:0x015f, code lost:
        if (X.AnonymousClass1NB.A03() != false) goto L_0x0161;
     */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0168 A[Catch:{ all -> 0x0181, Exception -> 0x018c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1QO A05(X.AnonymousClass1Q0 r13, java.lang.Object r14, X.C23411Pk r15) {
        /*
            r12 = this;
            r7 = r13
            android.net.Uri r0 = r13.A02
            X.C05520Zg.A02(r0)
            X.1PW r2 = r12.A0A     // Catch:{ Exception -> 0x018c }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0181 }
            if (r0 == 0) goto L_0x0013
            java.lang.String r0 = "ProducerSequenceFactory#getEncodedImageProducerSequence"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x0181 }
        L_0x0013:
            X.C05520Zg.A02(r13)     // Catch:{ all -> 0x0181 }
            X.1Pw r0 = r13.A0A     // Catch:{ all -> 0x0181 }
            int r3 = r0.mValue     // Catch:{ all -> 0x0181 }
            X.1Pw r0 = X.C23531Pw.ENCODED_MEMORY_CACHE     // Catch:{ all -> 0x0181 }
            int r1 = r0.mValue     // Catch:{ all -> 0x0181 }
            r0 = 0
            if (r3 > r1) goto L_0x0022
            r0 = 1
        L_0x0022:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x0181 }
            android.net.Uri r3 = r13.A02     // Catch:{ all -> 0x0181 }
            int r1 = r13.A01     // Catch:{ all -> 0x0181 }
            if (r1 == 0) goto L_0x0119
            r0 = 2
            if (r1 == r0) goto L_0x00da
            r0 = 3
            if (r1 == r0) goto L_0x00da
            r0 = 4
            if (r1 != r0) goto L_0x00ca
            monitor-enter(r2)     // Catch:{ all -> 0x0181 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x0040
            java.lang.String r0 = "ProducerSequenceFactory#getLocalContentUriFetchEncodedImageProducerSequence"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x00c6 }
        L_0x0040:
            X.1Q3 r0 = r2.A07     // Catch:{ all -> 0x00c6 }
            if (r0 != 0) goto L_0x00b2
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x004f
            java.lang.String r0 = "ProducerSequenceFactory#getLocalContentUriFetchEncodedImageProducerSequence:init"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x00c6 }
        L_0x004f:
            X.9Ms r4 = new X.9Ms     // Catch:{ all -> 0x00c6 }
            monitor-enter(r2)     // Catch:{ all -> 0x00c6 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00a0 }
            if (r0 == 0) goto L_0x005d
            java.lang.String r0 = "ProducerSequenceFactory#getBackgroundLocalContentUriFetchToEncodeMemorySequence"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x00a0 }
        L_0x005d:
            X.1Q3 r0 = r2.A04     // Catch:{ all -> 0x00a0 }
            if (r0 != 0) goto L_0x0094
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00a0 }
            if (r0 == 0) goto L_0x006c
            java.lang.String r0 = "ProducerSequenceFactory#getBackgroundLocalContentUriFetchToEncodeMemorySequence:init"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x00a0 }
        L_0x006c:
            X.1Pd r6 = r2.A0K     // Catch:{ all -> 0x00a0 }
            X.8tO r5 = new X.8tO     // Catch:{ all -> 0x00a0 }
            X.1Mg r0 = r6.A0E     // Catch:{ all -> 0x00a0 }
            java.util.concurrent.Executor r3 = r0.AaQ()     // Catch:{ all -> 0x00a0 }
            X.1iR r1 = r6.A0I     // Catch:{ all -> 0x00a0 }
            android.content.ContentResolver r0 = r6.A00     // Catch:{ all -> 0x00a0 }
            r5.<init>(r3, r1, r0)     // Catch:{ all -> 0x00a0 }
            X.1Q3 r3 = X.AnonymousClass1PW.A04(r2, r5)     // Catch:{ all -> 0x00a0 }
            X.1Pd r1 = r2.A0K     // Catch:{ all -> 0x00a0 }
            X.1PR r0 = r2.A0L     // Catch:{ all -> 0x00a0 }
            X.0mS r0 = r1.A01(r3, r0)     // Catch:{ all -> 0x00a0 }
            r2.A04 = r0     // Catch:{ all -> 0x00a0 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00a0 }
            if (r0 == 0) goto L_0x0094
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x00a0 }
        L_0x0094:
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00a0 }
            if (r0 == 0) goto L_0x009d
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x00a0 }
        L_0x009d:
            X.1Q3 r0 = r2.A04     // Catch:{ all -> 0x00a0 }
            goto L_0x00a3
        L_0x00a0:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00c6 }
            throw r0     // Catch:{ all -> 0x00c6 }
        L_0x00a3:
            monitor-exit(r2)     // Catch:{ all -> 0x00c6 }
            r4.<init>(r0)     // Catch:{ all -> 0x00c6 }
            r2.A07 = r4     // Catch:{ all -> 0x00c6 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x00b2
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x00c6 }
        L_0x00b2:
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x00bb
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x00c6 }
        L_0x00bb:
            monitor-exit(r2)     // Catch:{ all -> 0x00c6 }
            X.1Q3 r6 = r2.A07     // Catch:{ all -> 0x0181 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ Exception -> 0x018c }
            if (r0 == 0) goto L_0x0164
            goto L_0x0161
        L_0x00c6:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00c6 }
            goto L_0x0180
        L_0x00ca:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0181 }
            java.lang.String r1 = "Unsupported uri scheme for encoded image fetch! Uri is: "
            java.lang.String r0 = X.AnonymousClass1PW.A0B(r3)     // Catch:{ all -> 0x0181 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0181 }
            r2.<init>(r0)     // Catch:{ all -> 0x0181 }
            throw r2     // Catch:{ all -> 0x0181 }
        L_0x00da:
            monitor-enter(r2)     // Catch:{ all -> 0x0181 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0116 }
            if (r0 == 0) goto L_0x00e6
            java.lang.String r0 = "ProducerSequenceFactory#getLocalFileFetchEncodedImageProducerSequence"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x0116 }
        L_0x00e6:
            X.1Q3 r0 = r2.A09     // Catch:{ all -> 0x0116 }
            if (r0 != 0) goto L_0x0109
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0116 }
            if (r0 == 0) goto L_0x00f5
            java.lang.String r0 = "ProducerSequenceFactory#getLocalFileFetchEncodedImageProducerSequence:init"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x0116 }
        L_0x00f5:
            X.9Ms r1 = new X.9Ms     // Catch:{ all -> 0x0116 }
            X.1Q3 r0 = X.AnonymousClass1PW.A02(r2)     // Catch:{ all -> 0x0116 }
            r1.<init>(r0)     // Catch:{ all -> 0x0116 }
            r2.A09 = r1     // Catch:{ all -> 0x0116 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0116 }
            if (r0 == 0) goto L_0x0109
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x0116 }
        L_0x0109:
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0116 }
            if (r0 == 0) goto L_0x0112
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x0116 }
        L_0x0112:
            monitor-exit(r2)     // Catch:{ all -> 0x0116 }
            X.1Q3 r6 = r2.A09     // Catch:{ all -> 0x0181 }
            goto L_0x015b
        L_0x0116:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0116 }
            goto L_0x0180
        L_0x0119:
            monitor-enter(r2)     // Catch:{ all -> 0x0181 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0125
            java.lang.String r0 = "ProducerSequenceFactory#getNetworkFetchEncodedImageProducerSequence"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x017e }
        L_0x0125:
            X.1Q3 r0 = r2.A0D     // Catch:{ all -> 0x017e }
            if (r0 != 0) goto L_0x0148
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0134
            java.lang.String r0 = "ProducerSequenceFactory#getNetworkFetchEncodedImageProducerSequence:init"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x017e }
        L_0x0134:
            X.9Ms r1 = new X.9Ms     // Catch:{ all -> 0x017e }
            X.1Q3 r0 = X.AnonymousClass1PW.A03(r2)     // Catch:{ all -> 0x017e }
            r1.<init>(r0)     // Catch:{ all -> 0x017e }
            r2.A0D = r1     // Catch:{ all -> 0x017e }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0148
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x017e }
        L_0x0148:
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0151
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x017e }
        L_0x0151:
            monitor-exit(r2)     // Catch:{ all -> 0x017e }
            X.1Q3 r6 = r2.A0D     // Catch:{ all -> 0x0181 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ Exception -> 0x018c }
            if (r0 == 0) goto L_0x0164
            goto L_0x0161
        L_0x015b:
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ Exception -> 0x018c }
            if (r0 == 0) goto L_0x0164
        L_0x0161:
            X.AnonymousClass1NB.A01()     // Catch:{ Exception -> 0x018c }
        L_0x0164:
            X.36w r0 = r13.A06     // Catch:{ Exception -> 0x018c }
            if (r0 == 0) goto L_0x0173
            X.1Pv r1 = X.C23521Pv.A01(r13)     // Catch:{ Exception -> 0x018c }
            r0 = 0
            r1.A04 = r0     // Catch:{ Exception -> 0x018c }
            X.1Q0 r7 = r1.A02()     // Catch:{ Exception -> 0x018c }
        L_0x0173:
            X.1Pw r8 = X.C23531Pw.FULL_FETCH     // Catch:{ Exception -> 0x018c }
            r11 = 0
            r5 = r12
            r9 = r14
            r10 = r15
            X.1QO r0 = r5.A01(r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x018c }
            return r0
        L_0x017e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x017e }
        L_0x0180:
            throw r0     // Catch:{ all -> 0x0181 }
        L_0x0181:
            r1 = move-exception
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ Exception -> 0x018c }
            if (r0 == 0) goto L_0x018b
            X.AnonymousClass1NB.A01()     // Catch:{ Exception -> 0x018c }
        L_0x018b:
            throw r1     // Catch:{ Exception -> 0x018c }
        L_0x018c:
            r0 = move-exception
            X.1QO r0 = X.C77543nP.A00(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MZ.A05(X.1Q0, java.lang.Object, X.1Pk):X.1QO");
    }

    public static AnonymousClass1QO A00(AnonymousClass1MZ r11, AnonymousClass1Q3 r12, AnonymousClass1Q0 r13, C23531Pw r14, Object obj, C23561Pz r16) {
        AnonymousClass1Q0 r3 = r13;
        AnonymousClass1QH r5 = new AnonymousClass1QH(r11.A07(r13, null), r11.A0B);
        AnonymousClass1PD r1 = r11.A0C;
        Object obj2 = obj;
        if (r1 != null) {
            r1.CLb(obj, true);
        }
        try {
            C23531Pw r7 = r13.A0A;
            if (r7.mValue <= r14.mValue) {
                r7 = r14;
            }
            return new C196619Mt(r12, new AnonymousClass1QJ(r3, String.valueOf(r11.A00.getAndIncrement()), r5, obj2, r7, true, false, r16, r11.A09), r5);
        } catch (Exception e) {
            return C77543nP.A00(e);
        }
    }

    public AnonymousClass1QO A02(AnonymousClass1Q0 r7, Object obj) {
        return A06(r7, obj, C23531Pw.FULL_FETCH, null, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0072, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0077, code lost:
        return X.C77543nP.A00(r0);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1QO A03(X.AnonymousClass1Q0 r10, java.lang.Object r11) {
        /*
            r9 = this;
            X.1Pz r8 = X.C23561Pz.MEDIUM
            X.1Og r0 = r9.A01
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0017
            java.util.concurrent.CancellationException r0 = X.AnonymousClass1MZ.A0E
            X.1QO r0 = X.C77543nP.A00(r0)
            return r0
        L_0x0017:
            r5 = r10
            java.lang.Boolean r0 = r10.A0C     // Catch:{ Exception -> 0x0072 }
            if (r0 == 0) goto L_0x0024
            boolean r1 = r0.booleanValue()     // Catch:{ Exception -> 0x0072 }
            r0 = 0
            if (r1 != 0) goto L_0x0032
            goto L_0x0031
        L_0x0024:
            X.1Og r0 = r9.A03     // Catch:{ Exception -> 0x0072 }
            java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x0072 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Exception -> 0x0072 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x0072 }
            goto L_0x0032
        L_0x0031:
            r0 = 1
        L_0x0032:
            if (r0 == 0) goto L_0x0035
            goto L_0x0060
        L_0x0035:
            X.1PW r3 = r9.A0A     // Catch:{ Exception -> 0x0072 }
            X.1Q3 r2 = X.AnonymousClass1PW.A06(r3, r10)     // Catch:{ Exception -> 0x0072 }
            boolean r0 = r3.A0O     // Catch:{ Exception -> 0x0072 }
            if (r0 == 0) goto L_0x0043
            X.1Q3 r2 = X.AnonymousClass1PW.A05(r3, r2)     // Catch:{ Exception -> 0x0072 }
        L_0x0043:
            monitor-enter(r3)     // Catch:{ Exception -> 0x0072 }
            java.util.Map r0 = r3.A0H     // Catch:{ all -> 0x006f }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x006f }
            if (r0 != 0) goto L_0x0056
            X.9Mr r1 = new X.9Mr     // Catch:{ all -> 0x006f }
            r1.<init>(r2)     // Catch:{ all -> 0x006f }
            java.util.Map r0 = r3.A0H     // Catch:{ all -> 0x006f }
            r0.put(r2, r1)     // Catch:{ all -> 0x006f }
        L_0x0056:
            java.util.Map r0 = r3.A0H     // Catch:{ all -> 0x006f }
            java.lang.Object r4 = r0.get(r2)     // Catch:{ all -> 0x006f }
            X.1Q3 r4 = (X.AnonymousClass1Q3) r4     // Catch:{ all -> 0x006f }
            monitor-exit(r3)     // Catch:{ Exception -> 0x0072 }
            goto L_0x0066
        L_0x0060:
            X.1PW r0 = r9.A0A     // Catch:{ Exception -> 0x0072 }
            X.1Q3 r4 = r0.A0D(r10)     // Catch:{ Exception -> 0x0072 }
        L_0x0066:
            X.1Pw r6 = X.C23531Pw.FULL_FETCH     // Catch:{ Exception -> 0x0072 }
            r3 = r9
            r7 = r11
            X.1QO r0 = A00(r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0072 }
            return r0
        L_0x006f:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ Exception -> 0x0072 }
            throw r0     // Catch:{ Exception -> 0x0072 }
        L_0x0072:
            r0 = move-exception
            X.1QO r0 = X.C77543nP.A00(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MZ.A03(X.1Q0, java.lang.Object):X.1QO");
    }

    public AnonymousClass1QO A04(AnonymousClass1Q0 r7, Object obj, C23561Pz r9) {
        if (!((Boolean) this.A01.get()).booleanValue()) {
            return C77543nP.A00(A0E);
        }
        try {
            return A00(this, this.A0A.A0D(r7), r7, C23531Pw.FULL_FETCH, obj, r9);
        } catch (Exception e) {
            return C77543nP.A00(e);
        }
    }

    public AnonymousClass1QO A06(AnonymousClass1Q0 r8, Object obj, C23531Pw r10, C23411Pk r11, String str) {
        try {
            return A01(this.A0A.A0C(r8), r8, r10, obj, r11, str);
        } catch (Exception e) {
            return C77543nP.A00(e);
        }
    }

    public C23411Pk A07(AnonymousClass1Q0 r4, C23411Pk r5) {
        if (r5 == null) {
            C23411Pk r2 = r4.A08;
            if (r2 == null) {
                return this.A0D;
            }
            return new C23391Pi(this.A0D, r2);
        }
        C23411Pk r22 = r4.A08;
        if (r22 == null) {
            return new C23391Pi(this.A0D, r5);
        }
        return new C23391Pi(this.A0D, r5, r22);
    }

    public void A08() {
        C179688Sa r1 = new C179688Sa();
        this.A07.C1M(r1);
        this.A08.C1M(r1);
        this.A04.A01();
        this.A05.A01();
    }

    public boolean A09(AnonymousClass1Q0 r3) {
        if (r3 == null) {
            return false;
        }
        AnonymousClass1PS Ab8 = this.A07.Ab8(this.A06.A04(r3, null));
        try {
            return AnonymousClass1PS.A07(Ab8);
        } finally {
            AnonymousClass1PS.A05(Ab8);
        }
    }

    public boolean A0A(AnonymousClass1Q0 r3) {
        C30911iq r0;
        C23601Qd A052 = this.A06.A05(r3, null);
        switch (r3.A09.ordinal()) {
            case 0:
                r0 = this.A05;
                break;
            case 1:
                r0 = this.A04;
                break;
            default:
                return false;
        }
        return r0.A04(A052);
    }

    public AnonymousClass1MZ(AnonymousClass1PW r2, Set set, Set set2, C23111Og r5, C23311Pa r6, C23311Pa r7, C30911iq r8, C30911iq r9, C22601Mc r10, C23111Og r11, C23111Og r12, AnonymousClass1PD r13, AnonymousClass1PL r14) {
        this.A0A = r2;
        this.A0D = new C23391Pi(set);
        this.A0B = new AnonymousClass1MJ(set2);
        this.A01 = r5;
        this.A07 = r6;
        this.A08 = r7;
        this.A04 = r8;
        this.A05 = r9;
        this.A06 = r10;
        this.A03 = r11;
        this.A02 = r12;
        this.A0C = r13;
        this.A09 = r14;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        if (X.C006206h.A06(r15.A02) == false) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005a, code lost:
        if (X.AnonymousClass1NB.A03() != false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0066, code lost:
        if (X.AnonymousClass1NB.A03() == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0068, code lost:
        X.AnonymousClass1NB.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.AnonymousClass1QO A01(X.AnonymousClass1Q3 r14, X.AnonymousClass1Q0 r15, X.C23531Pw r16, java.lang.Object r17, X.C23411Pk r18, java.lang.String r19) {
        /*
            r13 = this;
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x000b
            java.lang.String r0 = "ImagePipeline#submitFetchRequest"
            X.AnonymousClass1NB.A02(r0)
        L_0x000b:
            X.1QH r6 = new X.1QH
            r3 = r15
            r0 = r18
            X.1Pk r1 = r13.A07(r15, r0)
            X.1MK r0 = r13.A0B
            r6.<init>(r1, r0)
            X.1PD r1 = r13.A0C
            r0 = 0
            r7 = r17
            if (r1 == 0) goto L_0x0023
            r1.CLb(r7, r0)
        L_0x0023:
            X.1Pw r8 = r15.A0A     // Catch:{ Exception -> 0x005d }
            int r1 = r8.mValue     // Catch:{ Exception -> 0x005d }
            r2 = r16
            int r0 = r2.mValue     // Catch:{ Exception -> 0x005d }
            if (r1 > r0) goto L_0x002e
            r8 = r2
        L_0x002e:
            X.1QJ r2 = new X.1QJ     // Catch:{ Exception -> 0x005d }
            java.util.concurrent.atomic.AtomicLong r0 = r13.A00     // Catch:{ Exception -> 0x005d }
            long r0 = r0.getAndIncrement()     // Catch:{ Exception -> 0x005d }
            java.lang.String r4 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x005d }
            r9 = 0
            boolean r0 = r15.A0H     // Catch:{ Exception -> 0x005d }
            if (r0 != 0) goto L_0x0048
            android.net.Uri r0 = r15.A02     // Catch:{ Exception -> 0x005d }
            boolean r0 = X.C006206h.A06(r0)     // Catch:{ Exception -> 0x005d }
            r10 = 0
            if (r0 != 0) goto L_0x0049
        L_0x0048:
            r10 = 1
        L_0x0049:
            X.1Pz r11 = r15.A05     // Catch:{ Exception -> 0x005d }
            X.1PL r12 = r13.A09     // Catch:{ Exception -> 0x005d }
            r5 = r19
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ Exception -> 0x005d }
            X.1QO r1 = X.AnonymousClass1QL.A00(r14, r2, r6)     // Catch:{ Exception -> 0x005d }
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x006b
            goto L_0x0068
        L_0x005d:
            r0 = move-exception
            X.1QO r1 = X.C77543nP.A00(r0)     // Catch:{ all -> 0x006c }
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x006b
        L_0x0068:
            X.AnonymousClass1NB.A01()
        L_0x006b:
            return r1
        L_0x006c:
            r1 = move-exception
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x0076
            X.AnonymousClass1NB.A01()
        L_0x0076:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MZ.A01(X.1Q3, X.1Q0, X.1Pw, java.lang.Object, X.1Pk, java.lang.String):X.1QO");
    }
}
