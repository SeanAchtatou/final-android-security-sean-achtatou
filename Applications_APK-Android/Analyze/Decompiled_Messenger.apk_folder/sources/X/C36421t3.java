package X;

/* renamed from: X.1t3  reason: invalid class name and case insensitive filesystem */
public final class C36421t3 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.omnistore.module.synchronous.SynchronousOmnistoreBroadcastReceiver$1";
    public final /* synthetic */ C36401t0 A00;
    public final /* synthetic */ C29271g9 A01;

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        X.C010708t.A0E(X.C36401t0.A02, r3, "Omnistore IO error during connect.", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0058, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0059, code lost:
        X.C010708t.A0F(X.C36401t0.A02, r3, "Starting omnistore failed because there's no valid user.", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0063, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0064, code lost:
        X.C010708t.A0F(X.C36401t0.A02, r3, "Starting Omnistore failed because there's no ViewerContext", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0072, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0073, code lost:
        r6.A00.A01 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0077, code lost:
        throw r1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r6 = this;
            r4 = 0
            X.1g9 r5 = r6.A01     // Catch:{ 3LY -> 0x0063, 3LZ -> 0x0058, OmnistoreIOException -> 0x004d }
            monitor-enter(r5)     // Catch:{ 3LY -> 0x0063, 3LZ -> 0x0058, OmnistoreIOException -> 0x004d }
            X.00x r0 = r5.A06     // Catch:{ all -> 0x004a }
            java.lang.String r1 = r0.A04     // Catch:{ all -> 0x004a }
            boolean r0 = com.facebook.omnistore.util.DeviceIdUtil.isPagesManager(r1)     // Catch:{ all -> 0x004a }
            if (r0 != 0) goto L_0x0048
            boolean r0 = com.facebook.omnistore.util.DeviceIdUtil.isCreatorApp(r1)     // Catch:{ all -> 0x004a }
            if (r0 != 0) goto L_0x0048
            boolean r0 = r5.A03     // Catch:{ all -> 0x004a }
            if (r0 != 0) goto L_0x0043
            com.facebook.omnistore.Omnistore r0 = X.C29271g9.A00(r5)     // Catch:{ all -> 0x004a }
            r0.start()     // Catch:{ all -> 0x004a }
            java.lang.Iterable r0 = X.C29271g9.A02(r5)     // Catch:{ all -> 0x004a }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x004a }
        L_0x0027:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x0040
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x004a }
            X.1V0 r0 = (X.AnonymousClass1V0) r0     // Catch:{ all -> 0x004a }
            r0.Bgx(r5)     // Catch:{ all -> 0x0037 }
            goto L_0x0027
        L_0x0037:
            r2 = move-exception
            X.09P r1 = r5.A05     // Catch:{ all -> 0x004a }
            java.lang.String r0 = "SynchronousOmnistoreWrapper_conected_ComponentThrew"
            r1.softReport(r0, r2)     // Catch:{ all -> 0x004a }
            goto L_0x0027
        L_0x0040:
            r0 = 1
            r5.A03 = r0     // Catch:{ all -> 0x004a }
        L_0x0043:
            com.facebook.omnistore.OmnistoreMqtt r0 = r5.A02     // Catch:{ all -> 0x004a }
            r0.onConnectionEstablished()     // Catch:{ all -> 0x004a }
        L_0x0048:
            monitor-exit(r5)     // Catch:{ 3LY -> 0x0063, 3LZ -> 0x0058, OmnistoreIOException -> 0x004d }
            goto L_0x006d
        L_0x004a:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ 3LY -> 0x0063, 3LZ -> 0x0058, OmnistoreIOException -> 0x004d }
            throw r0     // Catch:{ 3LY -> 0x0063, 3LZ -> 0x0058, OmnistoreIOException -> 0x004d }
        L_0x004d:
            r3 = move-exception
            java.lang.Class r2 = X.C36401t0.A02     // Catch:{ all -> 0x0072 }
            java.lang.String r1 = "Omnistore IO error during connect."
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x0072 }
            X.C010708t.A0E(r2, r3, r1, r0)     // Catch:{ all -> 0x0072 }
            goto L_0x006d
        L_0x0058:
            r3 = move-exception
            java.lang.Class r2 = X.C36401t0.A02     // Catch:{ all -> 0x0072 }
            java.lang.String r1 = "Starting omnistore failed because there's no valid user."
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x0072 }
            X.C010708t.A0F(r2, r3, r1, r0)     // Catch:{ all -> 0x0072 }
            goto L_0x006d
        L_0x0063:
            r3 = move-exception
            java.lang.Class r2 = X.C36401t0.A02     // Catch:{ all -> 0x0072 }
            java.lang.String r1 = "Starting Omnistore failed because there's no ViewerContext"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x0072 }
            X.C010708t.A0F(r2, r3, r1, r0)     // Catch:{ all -> 0x0072 }
        L_0x006d:
            X.1t0 r0 = r6.A00
            r0.A01 = r4
            return
        L_0x0072:
            r1 = move-exception
            X.1t0 r0 = r6.A00
            r0.A01 = r4
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36421t3.run():void");
    }

    public C36421t3(C36401t0 r1, C29271g9 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
