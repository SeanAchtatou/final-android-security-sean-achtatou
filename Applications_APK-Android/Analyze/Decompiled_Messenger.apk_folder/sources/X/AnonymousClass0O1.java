package X;

import com.facebook.rti.push.service.FbnsService;
import io.card.payment.BuildConfig;
import java.util.UUID;

/* renamed from: X.0O1  reason: invalid class name */
public final class AnonymousClass0O1 implements AnonymousClass0AU {
    public AnonymousClass0C5 A00;
    private AnonymousClass0AW A01;
    public final FbnsService A02;

    public String AdF() {
        return "567310203415052";
    }

    public String AdI() {
        return "MQTT";
    }

    public synchronized String AkN() {
        return (String) this.A00.second;
    }

    public void BIb() {
    }

    public synchronized boolean CKB(AnonymousClass0C5 r5) {
        if (this.A00.equals(r5)) {
            return false;
        }
        AnonymousClass0DD AY8 = this.A01.AbP(AnonymousClass07B.A0i).AY8();
        AY8.BzD("/settings/mqtt/id/mqtt_device_id", (String) r5.first);
        AY8.BzD("/settings/mqtt/id/mqtt_device_secret", (String) r5.second);
        AY8.BzB("/settings/mqtt/id/timestamp", r5.A00);
        AY8.commit();
        this.A00 = r5;
        return true;
    }

    public synchronized String getDeviceId() {
        return (String) this.A00.first;
    }

    public AnonymousClass0O1(FbnsService fbnsService, AnonymousClass0AW r9) {
        this.A02 = fbnsService;
        this.A01 = r9;
        AnonymousClass0B0 AbP = r9.AbP(AnonymousClass07B.A0i);
        AnonymousClass0C5 r5 = new AnonymousClass0C5(AbP.getString("/settings/mqtt/id/mqtt_device_id", BuildConfig.FLAVOR), AbP.getString("/settings/mqtt/id/mqtt_device_secret", BuildConfig.FLAVOR), AbP.getLong("/settings/mqtt/id/timestamp", Long.MAX_VALUE));
        this.A00 = r5;
        String str = (String) r5.first;
        if ((str == null || str.equals(BuildConfig.FLAVOR)) && AnonymousClass0A9.A01(this.A02.getPackageName())) {
            CKB(new AnonymousClass0C5(UUID.randomUUID().toString(), BuildConfig.FLAVOR, System.currentTimeMillis()));
        }
    }
}
