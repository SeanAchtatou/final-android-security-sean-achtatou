package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.fasterxml.jackson.databind.JsonNode;
import io.card.payment.BuildConfig;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.UUID;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0kx  reason: invalid class name and case insensitive filesystem */
public final class C10850kx {
    private static volatile C10850kx A04;
    public C45962Of A00;
    public AnonymousClass0UN A01;
    public final C32221lP A02 = new C32221lP();
    public final Runnable A03 = new AnonymousClass159(this);

    public synchronized void A06() {
        C04970Xc r1;
        if (this.A00 != null && A04(this)) {
            AnonymousClass00S.A02(this.A02, this.A03);
            this.A00.A02(A01());
            A03(this);
            C45962Of r4 = this.A00;
            if (!(r4 == null || r4.A00 == null)) {
                AnonymousClass1ZE r3 = (AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A01);
                if (r4.A02) {
                    r1 = C04970Xc.A04;
                } else {
                    r1 = C04970Xc.A02;
                }
                USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r3.A02("media_metrics_open_application", r1), 347);
                if (uSLEBaseShape0S0000000.A0G()) {
                    C101754t6 r12 = C101754t6.A01;
                    if (r12 == null) {
                        r12 = new C101754t6();
                        C101754t6.A01 = r12;
                    }
                    uSLEBaseShape0S0000000.A0E("tracking", r12.A01(this.A00.A00()));
                    uSLEBaseShape0S0000000.A0D("dest_module_uri", this.A00.A01("ads_navigation_url"));
                    uSLEBaseShape0S0000000.A08("batch_logged", true);
                    HashMap hashMap = new HashMap();
                    String A012 = this.A00.A01("event_trace_id");
                    if (A012 != null) {
                        hashMap.put("event_trace_id", A012);
                    }
                    uSLEBaseShape0S0000000.A0F("last_event_context", hashMap);
                    uSLEBaseShape0S0000000.A06();
                }
            }
            this.A00 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r1 == false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A07() {
        /*
            r2 = this;
            monitor-enter(r2)
            X.2Of r0 = r2.A00     // Catch:{ all -> 0x000f }
            if (r0 == 0) goto L_0x000c
            boolean r1 = r0.A03()     // Catch:{ all -> 0x000f }
            r0 = 1
            if (r1 != 0) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            monitor-exit(r2)
            return r0
        L_0x000f:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10850kx.A07():boolean");
    }

    public static final C10850kx A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C10850kx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C10850kx(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static void A02(C10850kx r2) {
        if (r2.A00 != null) {
            r2.A00.A02(A01());
        }
        if (r2.A00 != null) {
            A03(r2);
        }
        r2.A00 = null;
    }

    public static void A03(C10850kx r5) {
        HashMap hashMap;
        C04970Xc r1;
        EVQ A012;
        C45962Of r0 = r5.A00;
        if (!(r0 == null || !r0.A03() || (A012 = C09390hE.A01((C09390hE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BP0, r5.A01))) == null)) {
            A012.A00("external_app_launch");
        }
        C45962Of r3 = r5.A00;
        if (r3 != null && (hashMap = r3.A00) != null) {
            AnonymousClass1ZE r2 = (AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, r5.A01);
            if (r3.A02) {
                r1 = C04970Xc.A04;
            } else {
                r1 = C04970Xc.A02;
            }
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r2.A02("media_metrics", r1), AnonymousClass1Y3.A2j);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0O(r5.A00.A01("pigeon_reserved_keyword_module"));
                uSLEBaseShape0S0000000.A0D("pigeon_reserved_keyword_obj_type", r5.A00.A01("pigeon_reserved_keyword_obj_type"));
                uSLEBaseShape0S0000000.A0D("pigeon_reserved_keyword_obj_id", r5.A00.A01("pigeon_reserved_keyword_obj_id"));
                uSLEBaseShape0S0000000.A0D("event_trace_id", r5.A00.A01("event_trace_id"));
                uSLEBaseShape0S0000000.A0D("ads_navigation_url", r5.A00.A01("ads_navigation_url"));
                uSLEBaseShape0S0000000.A0D("browser_metrics_join_key", r5.A00.A01("browser_metrics_join_key"));
                C101754t6 r32 = C101754t6.A01;
                if (r32 == null) {
                    r32 = new C101754t6();
                    C101754t6.A01 = r32;
                }
                C101904tS r02 = (C101904tS) hashMap.get("tracking");
                if (r02 != null) {
                    uSLEBaseShape0S0000000.A0E("tracking", r32.A01((JsonNode) r02.A01()));
                }
                C101904tS r03 = (C101904tS) hashMap.get("tracking_nodes");
                if (r03 != null) {
                    uSLEBaseShape0S0000000.A0E("tracking_nodes", r32.A01((JsonNode) r03.A01()));
                }
                C101904tS r04 = (C101904tS) hashMap.get("log_context_keys");
                if (r04 != null) {
                    uSLEBaseShape0S0000000.A0D("log_context_keys", (String) r04.A01());
                }
                uSLEBaseShape0S0000000.A06();
            }
        }
    }

    public static boolean A04(C10850kx r1) {
        C45962Of r0 = r1.A00;
        if (r0 != null && !A05(r0.A00())) {
            return true;
        }
        C45962Of r02 = r1.A00;
        if (r02 != null) {
            A05(r02.A00());
        }
        A02(r1);
        return false;
    }

    private static boolean A05(JsonNode jsonNode) {
        if (jsonNode == null) {
            return true;
        }
        C11980oL nodeType = jsonNode.getNodeType();
        C11980oL r1 = C11980oL.STRING;
        if (nodeType == r1 && jsonNode.isNull()) {
            return true;
        }
        if (jsonNode.getNodeType() == r1 || jsonNode.size() != 0) {
            return false;
        }
        return true;
    }

    private C10850kx(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(3, r3);
    }

    public static String A01() {
        UUID A002 = C188215g.A00();
        ByteBuffer wrap = ByteBuffer.wrap(new byte[16]);
        wrap.putLong(A002.getMostSignificantBits());
        wrap.putLong(A002.getLeastSignificantBits());
        return AnonymousClass08S.A0J("MMtrcs", AnonymousClass18G.A03.A06(wrap.array()).replaceAll("=", BuildConfig.FLAVOR));
    }
}
