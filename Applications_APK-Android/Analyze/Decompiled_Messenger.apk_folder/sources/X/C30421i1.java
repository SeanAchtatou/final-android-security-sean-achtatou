package X;

import com.facebook.messaging.notify.type.NewMessageNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1i1  reason: invalid class name and case insensitive filesystem */
public final class C30421i1 extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$1";
    public final /* synthetic */ NewMessageNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C30421i1(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, NewMessageNotification newMessageNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = newMessageNotification;
    }
}
