package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0EF  reason: invalid class name */
public final class AnonymousClass0EF extends C03160Kg {
    public long A00() {
        return 6401547692887167407L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        C02630Fr r32 = (C02630Fr) r3;
        dataOutput.writeInt(r32.acraActiveRadioTimeS);
        dataOutput.writeInt(r32.acraTailRadioTimeS);
        dataOutput.writeInt(r32.acraRadioWakeupCount);
        dataOutput.writeLong(r32.acraTxBytes);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        C02630Fr r32 = (C02630Fr) r3;
        r32.acraActiveRadioTimeS = dataInput.readInt();
        r32.acraTailRadioTimeS = dataInput.readInt();
        r32.acraRadioWakeupCount = dataInput.readInt();
        r32.acraTxBytes = dataInput.readLong();
        return true;
    }
}
