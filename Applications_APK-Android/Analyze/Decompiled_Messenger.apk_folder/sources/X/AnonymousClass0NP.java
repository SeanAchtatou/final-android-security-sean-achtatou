package X;

import io.card.payment.BuildConfig;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0NP  reason: invalid class name */
public final class AnonymousClass0NP {
    public final long A00;

    public boolean equals(Object obj) {
        return this == obj || (obj != null && getClass() == obj.getClass() && this.A00 == ((AnonymousClass0NP) obj).A00);
    }

    public int hashCode() {
        long j = this.A00;
        return (int) (j ^ (j >>> 32));
    }

    public String toString() {
        String str;
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        long days = timeUnit.toDays(this.A00);
        long hours = timeUnit.toHours(this.A00) % 24;
        long minutes = timeUnit.toMinutes(this.A00) % 60;
        long seconds = timeUnit.toSeconds(this.A00) % 60;
        long millis = timeUnit.toMillis(this.A00) % 1000;
        long micros = timeUnit.toMicros(this.A00) % 1000;
        long j = this.A00 % 1000;
        StringBuilder sb = new StringBuilder("TimeSpan{");
        if (days > 0) {
            sb.append(days);
            sb.append(" ");
            A00(sb, "Day", days);
            str = ", ";
        } else {
            str = BuildConfig.FLAVOR;
        }
        if (hours > 0) {
            sb.append(str);
            sb.append(hours);
            sb.append(" ");
            A00(sb, "Hour", hours);
            str = ", ";
        }
        if (minutes > 0) {
            sb.append(str);
            sb.append(minutes);
            sb.append(" ");
            A00(sb, "Minute", minutes);
            str = ", ";
        }
        if (seconds > 0) {
            sb.append(str);
            sb.append(seconds);
            sb.append(" ");
            A00(sb, "Second", seconds);
            str = ", ";
        }
        if (millis > 0) {
            sb.append(str);
            sb.append(millis);
            sb.append(" ");
            A00(sb, "Milli", millis);
            str = ", ";
        }
        if (micros > 0) {
            sb.append(str);
            sb.append(micros);
            sb.append(" ");
            A00(sb, "Micro", micros);
            str = ", ";
        }
        if (j > 0) {
            sb.append(str);
            sb.append(j);
            sb.append(" ");
            A00(sb, "Nano", j);
        }
        sb.append("}");
        return sb.toString();
    }

    public AnonymousClass0NP(long j, TimeUnit timeUnit) {
        this.A00 = timeUnit.toNanos(j);
    }

    private static final void A00(StringBuilder sb, String str, long j) {
        sb.append(str);
        if (j > 1) {
            sb.append("s");
        }
    }
}
