package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.inject.ContextScoped;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import javax.inject.Singleton;

/* renamed from: X.1Xk  reason: invalid class name and case insensitive filesystem */
public final class C24871Xk {
    public final AnonymousClass1XX A00;
    public final Map A01 = AnonymousClass0TG.A04();
    public final Map A02 = AnonymousClass0TG.A04();
    public final Map A03 = AnonymousClass0TG.A04();
    public final Map A04 = new LinkedHashMap();
    public final Set A05 = new HashSet();

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00ba, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00bf, code lost:
        throw com.google.common.base.Throwables.propagate(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0158, code lost:
        if ((r14 instanceof X.AnonymousClass0Tu) != false) goto L_0x015a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ba A[ExcHandler: IllegalAccessException | NoSuchFieldException | NoSuchMethodException (r0v105 'e' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:17:0x0080] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A00(X.AnonymousClass1Xm r13, X.AnonymousClass1XW r14) {
        /*
            r12 = this;
            java.util.Map r1 = r12.A04
            java.util.Map r0 = r13.B1y()
            r1.putAll(r0)
            java.util.List r0 = r13.B12()
            java.util.Iterator r9 = r0.iterator()
        L_0x0011:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x0114
            java.lang.Object r4 = r9.next()
            java.lang.Class r4 = (java.lang.Class) r4
            java.util.Set r0 = r12.A05
            boolean r0 = r0.contains(r4)
            if (r0 != 0) goto L_0x0011
            java.util.Set r0 = r12.A05
            r0.add(r4)
            r2 = 0
            java.lang.Class[] r0 = new java.lang.Class[r2]     // Catch:{ NoSuchMethodException -> 0x00ff, InvocationTargetException -> 0x00ea, InstantiationException -> 0x00d5, IllegalAccessException -> 0x00c0 }
            java.lang.reflect.Constructor r1 = r4.getConstructor(r0)     // Catch:{ NoSuchMethodException -> 0x00ff, InvocationTargetException -> 0x00ea, InstantiationException -> 0x00d5, IllegalAccessException -> 0x00c0 }
            r0 = 1
            r1.setAccessible(r0)     // Catch:{ NoSuchMethodException -> 0x00ff, InvocationTargetException -> 0x00ea, InstantiationException -> 0x00d5, IllegalAccessException -> 0x00c0 }
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ NoSuchMethodException -> 0x00ff, InvocationTargetException -> 0x00ea, InstantiationException -> 0x00d5, IllegalAccessException -> 0x00c0 }
            java.lang.Object r5 = r1.newInstance(r0)     // Catch:{ NoSuchMethodException -> 0x00ff, InvocationTargetException -> 0x00ea, InstantiationException -> 0x00d5, IllegalAccessException -> 0x00c0 }
            X.0UV r5 = (X.AnonymousClass0UV) r5     // Catch:{ NoSuchMethodException -> 0x00ff, InvocationTargetException -> 0x00ea, InstantiationException -> 0x00d5, IllegalAccessException -> 0x00c0 }
            X.1Xl r3 = new X.1Xl
            X.1XX r0 = r12.A00
            java.lang.Class r8 = r5.getClass()
            r3.<init>(r0, r8)
            java.lang.Class<X.Euh> r0 = X.C30357Euh.class
            boolean r0 = r0.isAssignableFrom(r8)
            if (r0 != 0) goto L_0x0060
            java.lang.Class<com.facebook.inject.InjectorModule> r0 = com.facebook.inject.InjectorModule.class
            java.lang.annotation.Annotation r0 = r8.getAnnotation(r0)
            if (r0 != 0) goto L_0x0080
            java.lang.Class<com.facebook.inject.GeneratedInjectorModule> r0 = com.facebook.inject.GeneratedInjectorModule.class
            java.lang.annotation.Annotation r0 = r8.getAnnotation(r0)
            if (r0 != 0) goto L_0x0080
        L_0x0060:
            java.lang.Class<X.0UE> r1 = X.AnonymousClass0UE.class
            java.lang.String r0 = "mBinder"
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)     // Catch:{ InvocationTargetException -> 0x00ab }
            r6 = 1
            r0.setAccessible(r6)     // Catch:{ InvocationTargetException -> 0x00ab }
            r0.set(r5, r3)     // Catch:{ InvocationTargetException -> 0x00ab }
            java.lang.String r1 = "configure"
            java.lang.Class[] r0 = new java.lang.Class[r2]     // Catch:{ InvocationTargetException -> 0x00ab }
            java.lang.reflect.Method r1 = r8.getDeclaredMethod(r1, r0)     // Catch:{ InvocationTargetException -> 0x00ab }
            r1.setAccessible(r6)     // Catch:{ InvocationTargetException -> 0x00ab }
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ InvocationTargetException -> 0x00ab }
            r1.invoke(r5, r0)     // Catch:{ InvocationTargetException -> 0x00ab }
            goto L_0x00b5
        L_0x0080:
            java.lang.String r2 = r8.getName()     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            java.lang.String r1 = "$AutoGeneratedBindingsFor"
            java.lang.String r0 = r8.getSimpleName()     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            java.lang.Class r6 = java.lang.Class.forName(r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            java.lang.String r2 = "bind"
            r1 = 1
            java.lang.Class<X.1Xm> r0 = X.AnonymousClass1Xm.class
            java.lang.Class[] r0 = new java.lang.Class[]{r0}     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            java.lang.reflect.Method r2 = r6.getDeclaredMethod(r2, r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            r2.setAccessible(r1)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[]{r3}     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            r2.invoke(r1, r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba, IllegalAccessException | NoSuchFieldException | NoSuchMethodException -> 0x00ba }
            goto L_0x00b5
        L_0x00ab:
            r0 = move-exception
            java.lang.Throwable r1 = r0.getCause()
            java.lang.Class<java.lang.RuntimeException> r0 = java.lang.RuntimeException.class
            com.google.common.base.Throwables.propagateIfInstanceOf(r1, r0)
        L_0x00b5:
            r12.A00(r3, r5)
            goto L_0x0011
        L_0x00ba:
            r0 = move-exception
            java.lang.RuntimeException r0 = com.google.common.base.Throwables.propagate(r0)
            throw r0
        L_0x00c0:
            r3 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Failed to access constructor for "
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0, r3)
            throw r2
        L_0x00d5:
            r3 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Failed to instantiate "
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0, r3)
            throw r2
        L_0x00ea:
            r3 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Failed to invoke constructor for "
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0, r3)
            throw r2
        L_0x00ff:
            r3 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Failed to find public default constructor for "
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0, r3)
            throw r2
        L_0x0114:
            java.util.List r0 = r13.AeT()
            java.util.Iterator r9 = r0.iterator()
        L_0x011c:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x0229
            java.lang.Object r1 = r9.next()
            X.B3Y r1 = (X.B3Y) r1
            X.1XX r6 = r12.A00
            java.util.List r8 = r13.B12()
            X.BKm r7 = r1.A01
            int r2 = X.AnonymousClass1Y3.A00(r7)
            X.0Tq r5 = r1.A04
            java.util.Map r0 = r12.A01
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            java.lang.Object r4 = r0.get(r2)
            X.B3Y r4 = (X.B3Y) r4
            if (r4 == 0) goto L_0x014f
            java.lang.Class<X.0Tu> r3 = X.AnonymousClass0Tu.class
            java.lang.Class r0 = r4.A02
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x014f
            goto L_0x011c
        L_0x014f:
            if (r4 == 0) goto L_0x01ac
            boolean r0 = r14 instanceof X.C30357Euh
            if (r0 != 0) goto L_0x015a
            boolean r3 = r14 instanceof X.AnonymousClass0Tu
            r0 = 0
            if (r3 == 0) goto L_0x015b
        L_0x015a:
            r0 = 1
        L_0x015b:
            if (r0 != 0) goto L_0x01ac
            r0 = 1
            byte r3 = r4.A00
            r3 = r3 & r0
            if (r3 == r0) goto L_0x0164
            r0 = 0
        L_0x0164:
            if (r0 == 0) goto L_0x01db
            r0 = 1
            if (r3 == r0) goto L_0x016a
            r0 = 0
        L_0x016a:
            if (r0 != 0) goto L_0x01ac
            java.lang.Class r3 = r4.A02
            java.lang.Class r0 = r1.A02
            boolean r0 = r3.equals(r0)
            if (r0 != 0) goto L_0x01ac
            java.lang.Class r0 = r4.A02
            boolean r0 = r8.contains(r0)
            if (r0 != 0) goto L_0x01ac
            java.lang.IllegalArgumentException r7 = new java.lang.IllegalArgumentException
            java.lang.Class r0 = r1.A02
            java.lang.String r6 = r0.getCanonicalName()
            X.BKm r0 = r4.A01
            java.lang.String r5 = r0.toString()
            java.lang.Class r0 = r4.A02
            java.lang.String r3 = r0.getCanonicalName()
            java.lang.Class r0 = r4.A02
            java.lang.String r2 = r0.getCanonicalName()
            java.lang.Class r0 = r1.A02
            java.lang.String r0 = r0.getCanonicalName()
            java.lang.Object[] r1 = new java.lang.Object[]{r6, r5, r3, r2, r0}
            java.lang.String r0 = "Module %s is overriding binding for %s from module %s, but does not require that module. Add %s(base module) in the dependency list of %s."
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r7.<init>(r0)
            throw r7
        L_0x01ac:
            boolean r0 = r5 instanceof X.C38491xW
            if (r0 == 0) goto L_0x01b6
            r0 = r5
            X.1xW r0 = (X.C38491xW) r0
            r0.setInjector(r6)
        L_0x01b6:
            java.lang.Class r3 = r1.A03
            if (r3 == 0) goto L_0x01d2
            java.util.Map r0 = r12.A04
            java.lang.Object r0 = r0.get(r3)
            X.1Xn r0 = (X.C24891Xn) r0
            if (r0 == 0) goto L_0x0215
            X.0Tq r5 = r0.C4b(r7, r5)
            boolean r0 = r5 instanceof X.C38491xW
            if (r0 == 0) goto L_0x01d2
            r0 = r5
            X.1xW r0 = (X.C38491xW) r0
            r0.setInjector(r6)
        L_0x01d2:
            r1.A04 = r5
            java.util.Map r0 = r12.A01
            r0.put(r2, r1)
            goto L_0x011c
        L_0x01db:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.Class r0 = r1.A02
            java.lang.String r5 = r0.getCanonicalName()
            X.BKm r0 = r4.A01
            java.lang.String r6 = r0.toString()
            java.lang.Class r0 = r4.A02
            java.lang.String r7 = r0.getCanonicalName()
            java.lang.Class r0 = r4.A02
            java.lang.String r8 = r0.getCanonicalName()
            java.lang.Class r0 = r1.A02
            java.lang.String r9 = r0.getCanonicalName()
            X.BKm r0 = r4.A01
            java.lang.String r10 = r0.toString()
            java.lang.Class r0 = r1.A02
            java.lang.String r11 = r0.getCanonicalName()
            java.lang.Object[] r1 = new java.lang.Object[]{r5, r6, r7, r8, r9, r10, r11}
            java.lang.String r0 = "Module %s illegally overriding binding for %s from module %s. Either require module %s(base module) from %s or provide %s as a default binding so it can be overridden in module %s(top module) ."
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r2.<init>(r0)
            throw r2
        L_0x0215:
            X.1xU r2 = new X.1xU
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "No scope registered for "
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0229:
            java.util.List r0 = r13.Aho()
            java.util.Iterator r3 = r0.iterator()
        L_0x0231:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0251
            java.lang.Object r2 = r3.next()
            X.1xZ r2 = (X.C38521xZ) r2
            r1 = 0
            boolean r0 = r1 instanceof X.C38561xd
            if (r0 == 0) goto L_0x0249
            X.1xd r1 = (X.C38561xd) r1
            X.1XX r0 = r12.A00
            r1.setInjector(r0)
        L_0x0249:
            X.BKm r1 = r2.A00
            java.util.Map r0 = r12.A02
            r0.put(r1, r2)
            goto L_0x0231
        L_0x0251:
            java.util.Set r3 = r13.Av9()
            java.util.Map r0 = r13.AvA()
            java.util.Set r2 = r0.keySet()
            java.lang.String r0 = "set1"
            com.google.common.base.Preconditions.checkNotNull(r3, r0)
            java.lang.String r0 = "set2"
            com.google.common.base.Preconditions.checkNotNull(r2, r0)
            X.0UB r1 = X.C25011Xz.A02(r2, r3)
            X.0UF r0 = new X.0UF
            r0.<init>(r3, r1, r2)
            java.util.Iterator r3 = r0.iterator()
        L_0x0274:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0295
            java.lang.Object r2 = r3.next()
            X.BKm r2 = (X.C22916BKm) r2
            java.util.Map r0 = r12.A03
            java.lang.Object r0 = r0.get(r2)
            X.1xb r0 = (X.C38541xb) r0
            if (r0 != 0) goto L_0x0274
            X.1xb r1 = new X.1xb
            r1.<init>(r2)
            java.util.Map r0 = r12.A03
            r0.put(r2, r1)
            goto L_0x0274
        L_0x0295:
            java.util.Map r0 = r13.AvA()
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r6 = r0.iterator()
        L_0x02a1:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x02fb
            java.lang.Object r0 = r6.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getKey()
            X.BKm r2 = (X.C22916BKm) r2
            java.lang.Object r1 = r0.getValue()
            X.1xc r1 = (X.C38551xc) r1
            java.util.Map r0 = r12.A03
            java.lang.Object r5 = r0.get(r2)
            X.1xb r5 = (X.C38541xb) r5
            X.B7U r0 = r1.A00
            java.util.Iterator r4 = r0.iterator()
        L_0x02c7:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x02f3
            java.lang.Object r3 = r4.next()
            X.BKm r3 = (X.C22916BKm) r3
            java.util.List r0 = r5.A01
            int r2 = r0.size()
            r1 = 0
        L_0x02da:
            if (r1 >= r2) goto L_0x02ed
            java.util.List r0 = r5.A01
            java.lang.Object r0 = r0.get(r1)
            X.BKm r0 = (X.C22916BKm) r0
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x02c7
            int r1 = r1 + 1
            goto L_0x02da
        L_0x02ed:
            java.util.List r0 = r5.A01
            r0.add(r3)
            goto L_0x02c7
        L_0x02f3:
            java.util.List r1 = r5.A01
            java.util.Comparator r0 = X.C38541xb.A02
            java.util.Collections.sort(r1, r0)
            goto L_0x02a1
        L_0x02fb:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24871Xk.A00(X.1Xm, X.1XW):void");
    }

    /* JADX INFO: finally extract failed */
    public AnonymousClass1Y2 A01() {
        C005505z.A03("FbInjectorImpl.init#modules", 1670374105);
        try {
            AnonymousClass0Tu r5 = new AnonymousClass0Tu();
            AnonymousClass1XX r0 = this.A00;
            Class<?> cls = r5.getClass();
            C24881Xl r4 = new C24881Xl(r0, cls);
            r5.mBinder = r4;
            r5.bindScope(Singleton.class, new AnonymousClass0U1((AnonymousClass1XX) r4.Aq2()));
            r5.bindScope(ContextScoped.class, new C24911Xp((AnonymousClass1XX) r4.Aq2()));
            r5.bindScope(UserScoped.class, new C24921Xq((AnonymousClass1XX) r4.Aq2()));
            A00(r4, r5);
            this.A05.add(cls);
            C005505z.A00(-1483323875);
            return new AnonymousClass1Y2(this.A01, this.A04, this.A02, this.A05, this.A03);
        } catch (Throwable th) {
            C005505z.A00(-789958364);
            throw th;
        }
    }

    public C24871Xk(AnonymousClass1XX r2) {
        this.A00 = r2;
    }
}
