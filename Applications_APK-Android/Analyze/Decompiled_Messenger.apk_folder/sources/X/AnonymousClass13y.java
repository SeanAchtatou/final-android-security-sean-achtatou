package X;

import android.view.Window;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

@UserScoped
/* renamed from: X.13y  reason: invalid class name */
public final class AnonymousClass13y {
    private static C05540Zi A00;

    public static final AnonymousClass13y A00(AnonymousClass1XY r3) {
        AnonymousClass13y r0;
        synchronized (AnonymousClass13y.class) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r3)) {
                    A00.A01();
                    A00.A00 = new AnonymousClass13y();
                }
                C05540Zi r1 = A00;
                r0 = (AnonymousClass13y) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A01(Window window, MigColorScheme migColorScheme) {
        C15990wJ.A01(window, migColorScheme.B3r(), migColorScheme.AvQ());
    }
}
