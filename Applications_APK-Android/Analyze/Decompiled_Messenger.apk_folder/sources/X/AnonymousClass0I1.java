package X;

import com.facebook.acra.ACRA;
import com.facebook.common.dextricks.DexStore;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0I1  reason: invalid class name */
public final class AnonymousClass0I1 {
    public static String A00(int i) {
        switch (i) {
            case 1:
                return "START";
            case 2:
                return "SUCCESS";
            case 3:
                return "FAIL";
            case 4:
                return "CANCEL";
            case 5:
                return "DRAW_COMPLETE";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                return "ON_RESUME";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "ACTIVITY_CREATED";
            case 8:
                return "CONSISTENCY_MODEL_UPDATER";
            case Process.SIGKILL /*9*/:
                return "SEND_MESSAGE";
            case AnonymousClass1Y3.A01 /*10*/:
                return "SUCCESS_COLD";
            case AnonymousClass1Y3.A02 /*11*/:
                return "SUCCESS_WARM";
            case AnonymousClass1Y3.A03 /*12*/:
                return "UI_IDLE";
            case 13:
                return "PHASE_ONE";
            case 14:
                return "PHASE_TWO";
            case 15:
                return "DEQUEUE";
            case 16:
                return "NETWORK_COMPLETE";
            case 17:
                return "MEMORY_CACHE_VISIT";
            case Process.SIGCONT /*18*/:
                return "DISK_CACHE_VISIT";
            case Process.SIGSTOP /*19*/:
                return "CONSISTENCY_UPDATE";
            case 20:
                return "RETURN_TO_CALLER";
            case AnonymousClass1Y3.A05 /*21*/:
                return "PHOTO_UPLOAD_COMPLETE";
            case AnonymousClass1Y3.A06 /*22*/:
                return "USER_NAVIGATION_CANCELLATION";
            case 23:
                return "DB_FETCH";
            case AnonymousClass1Y3.A07 /*24*/:
                return "SERVER_FETCH";
            case 25:
                return "SUCCESS_CACHE";
            case AnonymousClass1Y3.A08 /*26*/:
                return "SUCCESS_DB";
            case AnonymousClass1Y3.A09 /*27*/:
                return "SUCCESS_NETWORK";
            case 28:
                return "SUCCESS_LOCAL_UNSPECIFIED";
            case 29:
                return "CACHE_UPDATED";
            case AnonymousClass1Y3.A0A /*30*/:
                return "DB_UPDATED";
            case AnonymousClass1Y3.A0B /*31*/:
                return "DATA_RECEIVED";
            case 32:
                return "DRAW_VIEW";
            case 33:
                return "DATA_EMPTY";
            case AnonymousClass1Y3.A0C /*34*/:
                return "CACHE_FETCH";
            case 35:
                return "PREPARE_BEGIN";
            case 36:
                return "PREPARE_END";
            case AnonymousClass1Y3.A0D /*37*/:
                return "ASYNC_BEGIN";
            case AnonymousClass1Y3.A0E /*38*/:
                return "ASYNC_END";
            case AnonymousClass1Y3.A0F /*39*/:
                return "REMOVE_BEGIN";
            case AnonymousClass1Y3.A0G /*40*/:
                return "REMOVE_END";
            case AnonymousClass1Y3.A0H /*41*/:
                return "BROADCAST_DONE";
            case 42:
                return "ON_RESUME_END";
            case 43:
                return "ON_ATTACH_END";
            case AnonymousClass1Y3.A0I /*44*/:
                return "ON_FRAGMENT_CREATE_END";
            case AnonymousClass1Y3.A0J /*45*/:
                return "ON_CREATE_VIEW_END";
            case AnonymousClass1Y3.A0K /*46*/:
                return "ON_ACTIVITY_CREATED_END";
            case AnonymousClass1Y3.A0L /*47*/:
                return "ON_START_END";
            case 48:
                return "QUEUED";
            case 49:
                return "IN_PROGRESS";
            case 50:
                return "INIT";
            case AnonymousClass1Y3.A0M /*51*/:
                return "UNKNOWN";
            case AnonymousClass1Y3.A0N /*52*/:
                return "RETRY_AFTER_FAILURE";
            case AnonymousClass1Y3.A0O /*53*/:
                return "RETRY_AFTER_RECONNECT";
            case 54:
                return "QUEUEING_BEGIN";
            case 55:
                return "QUEUEING_SUCCESS";
            case AnonymousClass1Y3.A0P /*56*/:
                return "QUEUEING_FAIL";
            case 57:
                return "MESSAGE_UPDATE_START";
            case AnonymousClass1Y3.A0Q /*58*/:
                return "MESSAGE_UPDATE_END";
            case 59:
                return "PHOTO_CAPTURED";
            case AnonymousClass1Y3.A0R /*60*/:
                return "MEDIA_PREVIEW_VISIBLE";
            case 61:
                return "COUNTER";
            case 62:
                return "INTERACTION_LOAD_TIMELINE_HEADER";
            case AnonymousClass1Y3.A0S /*63*/:
                return "INTERACTION_LOAD_EVENT_PERMALINK";
            case 64:
                return "INTERACTION_LOAD_GROUPS_FEED";
            case AnonymousClass1Y3.A0T /*65*/:
                return "INTERACTION_LOAD_PAGE_HEADER";
            case 66:
                return "INTERACTION_LOAD_PAGE_HEADER_ADMIN";
            case 67:
                return "INTERACTION_LOAD_PERMALINK";
            case AnonymousClass1Y3.A0U /*68*/:
                return "INTERACTION_OPEN_COMPOSER";
            case 69:
                return "INTERACTION_OPEN_MEDIA_PICKER";
            case 70:
                return "INTERACTION_OPEN_PHOTO_GALLERY";
            case AnonymousClass1Y3.A0V /*71*/:
                return "INTERACTION_OPEN_CHECK_IN";
            case 72:
                return "INTERACTION_LOAD_WEB_VIEW";
            case AnonymousClass1Y3.A0W /*73*/:
                return "INTENT_MAPPED";
            case 74:
                return "ACTIVITY_LAUNCHED";
            case AnonymousClass1Y3.A0X /*75*/:
                return "ACTIVITY_PAUSED";
            case AnonymousClass1Y3.A0Y /*76*/:
                return "ACTIVITY_STARTED";
            case AnonymousClass1Y3.A0Z /*77*/:
                return "ACTIVITY_RESUMED";
            case 78:
                return "FRAGMENT_CREATED";
            case 79:
                return "FRAGMENT_RESUMED";
            case AnonymousClass1Y3.A0a /*80*/:
                return "ACTIVITY_ON_CREATE";
            case AnonymousClass1Y3.A0b /*81*/:
                return "INTENT_MAPPING_BEGIN";
            case 82:
                return "FRAGMENT_ON_CREATE";
            case 83:
                return "FRAGMENT_NEW_INSTANCE";
            case AnonymousClass1Y3.A0c /*84*/:
                return "MARKER_SWAPPED";
            case 85:
                return "FRAGMENT_INSTANCE_CREATED";
            case 86:
                return "PREV_ACTIVITY_PAUSED";
            case 87:
                return "ERROR";
            case 88:
                return "METHOD_INVOKE";
            case AnonymousClass1Y3.A0d /*89*/:
                return "FINALLY";
            case AnonymousClass1Y3.A0e /*90*/:
                return "PHOTO_DOWNLOAD_COMPLETE";
            case 91:
                return "MINIPREVIEW_COMPLETE";
            case 92:
                return "SEARCH_TYPEAHEAD";
            case 93:
                return "ANIMATION_END";
            case AnonymousClass1Y3.A0f /*94*/:
                return "UDP_REQUEST_SEND";
            case 95:
                return "MAIN_COMPLETE";
            case AnonymousClass1Y3.A0g /*96*/:
                return "INTERRUPTED";
            case 97:
                return "NETWORK_FAILED";
            case 98:
                return "NETWORK_RESPONSE";
            case 99:
                return "EDGE_PROCESSING_BEGIN";
            case 100:
                return "NEWSFEED_PROCESS_RESPONSE";
            case AnonymousClass1Y3.A0i /*101*/:
                return "ON_VIEW_CREATED_END";
            case AnonymousClass1Y3.A0j /*102*/:
                return "DATA_LOAD_START";
            case 103:
                return "LEGACY_MARKER";
            case AnonymousClass1Y3.A0k /*104*/:
                return "ACTION_BAR_COMPLETE";
            case AnonymousClass1Y3.A0l /*105*/:
                return "ABORTED";
            case AnonymousClass1Y3.A0m /*106*/:
                return "QUERY_READY";
            case AnonymousClass1Y3.A0n /*107*/:
                return "RTMP_PACKET_RECEIVED";
            case 108:
                return "REQUESTED_PLAYING";
            case AnonymousClass1Y3.A0o /*109*/:
                return "RTMP_CONNECTION_REQUESTED";
            case AnonymousClass1Y3.A0p /*110*/:
                return "RTMP_CONNECTION_RELEASE";
            case 111:
                return "NEW_START_FOUND";
            case 112:
                return "MISSED_EVENT";
            case AnonymousClass1Y3.A0q /*113*/:
                return "TIMEOUT";
            case 114:
                return "CONTROLLER_INITIATED";
            case 115:
                return "RTMP_STREAM_PREPARED";
            case 116:
                return "VIDEO_PLAYING";
            case AnonymousClass1Y3.A0r /*117*/:
                return "RTMP_CONNECTION_CONNECTED";
            case 118:
                return "RTMP_CONNECTION_FAILED";
            case AnonymousClass1Y3.A0s /*119*/:
                return "RTMP_CONNECTION_INTERCEPTED";
            case AnonymousClass1Y3.A0t /*120*/:
                return "VIDEO_SET_RENDERER_CONTEXT";
            case AnonymousClass1Y3.A0u /*121*/:
                return "HEADER_DATA_LOADED";
            case AnonymousClass1Y3.A0v /*122*/:
                return "CARD_DATA_LOADED";
            case 123:
                return "VIEW_WILL_APPEAR_BEGIN";
            case 124:
                return "VIEW_DID_LOAD_BEGIN";
            case AnonymousClass1Y3.A0w /*125*/:
                return "COMPONENTS_DATA_SOURCE_WILL_BEGIN_UPDATES";
            case AnonymousClass1Y3.A0x /*126*/:
                return "COMPONENTS_DATA_SOURCE_DID_END_UPDATES";
            case 127:
                return "LOAD_VIEW_BEGIN";
            case 128:
                return "RTMP_FIRST_KEY_FRAME_RECEIVED";
            case 129:
                return "MESSENGER_QUEUE_CREATION";
            case AnonymousClass1Y3.A0y /*130*/:
                return "APP_DID_FINISH_LAUNCHING";
            case AnonymousClass1Y3.A0z /*131*/:
                return "APP_DID_BECOME_ACTIVE";
            case 132:
                return "APP_WILL_ENTER_FOREGROUND";
            case AnonymousClass1Y3.A10 /*133*/:
                return "APP_DID_ENTER_BACKGROUND";
            case 134:
                return "APP_MAIN";
            case AnonymousClass1Y3.A11 /*135*/:
                return "MQTT_CONNECTING";
            case AnonymousClass1Y3.A12 /*136*/:
                return "MQTT_CONNECTED";
            case 137:
                return "MQTT_DISCONNECTED";
            case AnonymousClass1Y3.A13 /*138*/:
                return "MESSENGER_DELTA_REQUEST";
            case 139:
                return "APP_FIRST_VIEW_CONTROLLER";
            case AnonymousClass1Y3.A14 /*140*/:
                return "MESSENGER_THREAD_LIST_LOADED";
            case AnonymousClass1Y3.A15 /*141*/:
                return "MESSENGER_THREAD_LIST_DISPLAYED";
            case AnonymousClass1Y3.A16 /*142*/:
                return "PREV_ACTIVITY_PAUSE";
            case 143:
                return "ACTIVITY_RESUME";
            case AnonymousClass1Y3.A17 /*144*/:
                return "ACTIVITY_START";
            case 145:
                return "BEGIN_START_ACTIVITY";
            case 146:
                return "END_START_ACTIVITY";
            case AnonymousClass1Y3.A18 /*147*/:
                return "FILE_SYSTEM_FAIL";
            case AnonymousClass1Y3.A19 /*148*/:
                return "FORMAT_ERROR";
            case AnonymousClass1Y3.A1A /*149*/:
                return "PRIVACY_VIOLATION";
            case AnonymousClass1Y3.A1B /*150*/:
                return "NETWORK_RESPONSE_INITIAL_SCAN";
            case 151:
                return "POPULATE_CONSISTENCY_MEMORY_CACHE";
            case 152:
                return "APPLY_OPTIMISTICS";
            case AnonymousClass1Y3.A1C /*153*/:
                return "APPLY_FINISHED_LIST";
            case 154:
                return "APPLY_FINISHED_LIST_AGAIN";
            case 155:
                return "FUTURE_LISTENERS_COMPLETE";
            case AnonymousClass1Y3.A1D /*156*/:
                return "SERVICE_ON_START_COMMAND";
            case AnonymousClass1Y3.A1E /*157*/:
                return "WAIT_FOR_BLOCKERS";
            case 158:
                return "NOTIFY_SUBSCRIBERS";
            case 159:
                return "FAIL_FILE_TOO_LARGE";
            case 160:
                return "OFFLINE";
            case AnonymousClass1Y3.A1F /*161*/:
                return "ASNYC_FAILED";
            case AnonymousClass1Y3.A1G /*162*/:
                return "ASYNC_FAIL";
            case AnonymousClass1Y3.A1H /*163*/:
                return "ON_ATTACH_FRAGMENT";
            case 164:
                return "VIEW_DID_APPEAR_BEGIN";
            case 165:
                return "DISPLAYED";
            case 166:
                return "DISPLAYED_ON_SCREEN";
            case AnonymousClass1Y3.A1I /*167*/:
                return "ASYNC_ACTION_SUCCESS";
            case 168:
                return "ASYNC_ACTION_FAIL";
            case 169:
                return "CONNECTIVITY_CHANGED";
            case AnonymousClass1Y3.A1J /*170*/:
                return "VIDEO_DISPLAYED";
            case AnonymousClass1Y3.A1K /*171*/:
                return "VIDEO_REQUESTED_PLAYING";
            case AnonymousClass1Y3.A1L /*172*/:
                return "LOADED_AUDIO_SESSION";
            case 173:
                return "LOADED_CAMERA_SESSION";
            case AnonymousClass1Y3.A1M /*174*/:
                return "SUCCESS_OPTIMISTIC";
            case AnonymousClass1Y3.A1N /*175*/:
                return "OUT_OF_ORDER";
            case AnonymousClass1Y3.A1O /*176*/:
                return "NOT_READY";
            case AnonymousClass1Y3.A1P /*177*/:
                return "JSON_PARSE";
            case 178:
                return "FILE_NOT_FOUND";
            case 179:
                return "METABOX_COMPLETE";
            case AnonymousClass1Y3.A1Q /*180*/:
                return "CALL_TO_ACTION_COMPLETE";
            case AnonymousClass1Y3.A1R /*181*/:
                return "HEADER_DRAW_COMPLETE";
            case 182:
                return "COVER_PHOTO_COMPLETE";
            case 183:
                return "COMPONENT_WILL_CREATE";
            case 184:
                return "COMPONENT_DID_CREATE";
            case AnonymousClass1Y3.A1S /*185*/:
                return "COMPONENT_WILL_LAYOUT";
            case 186:
                return "COMPONENT_DID_LAYOUT";
            case AnonymousClass1Y3.A1T /*187*/:
                return "COMPONENT_WILL_MOUNT";
            case AnonymousClass1Y3.A1U /*188*/:
                return "COMPONENT_DID_MOUNT";
            case AnonymousClass1Y3.A1V /*189*/:
                return "PRECALCULATE_EDGES";
            case AnonymousClass1Y3.A1W /*190*/:
                return "UI_THREAD_DEQUEUE";
            case AnonymousClass1Y3.A1X /*191*/:
                return "CALLBACKS_COMPLETE";
            case 192:
                return "CALLBACKS_DISPATCHED";
            case 193:
                return "NETWORK_PARSE_COMPLETE";
            case AnonymousClass1Y3.A1Y /*194*/:
                return "START_LOADING_JS_BUNDLE";
            case 195:
                return "FINISH_LOADING_JS_BUNDLE";
            case AnonymousClass1Y3.A1Z /*196*/:
                return "START_EXECUTING_JS_BUNDLE";
            case AnonymousClass1Y3.A1a /*197*/:
                return "FINISH_EXECUTING_JS_BUNDLE";
            case 198:
                return "START_CALLING_JS_FUNCTION";
            case AnonymousClass1Y3.A1b /*199*/:
                return "FINISH_CALLING_JS_FUNCTION";
            case AnonymousClass1Y3.A1c /*200*/:
                return "CREATED_MODEL_FILE";
            case AnonymousClass1Y3.A1d /*201*/:
                return "DB_SUPPLIER_GET";
            case 202:
                return "BEGIN_TRANSACTION";
            case AnonymousClass1Y3.A1e /*203*/:
                return "FILE_FLUSHED";
            case 204:
                return "OPTIMISTIC_UPDATES_APPLIED";
            case 205:
                return "FRAGMENT_VISIBLE";
            case 206:
                return "STALE";
            case AnonymousClass1Y3.A1f /*207*/:
                return "SUCCESS_GET_MODEL";
            case 208:
                return "FAIL_GET_MODEL";
            case 209:
                return "ALREADY_SEEN";
            case 210:
                return "UNKNOWN_SEEN_STATE";
            case 211:
                return "CREATED_INTENT";
            case 212:
                return "INVALID_INTENT";
            case 213:
                return "SHOW_NOTIFICATION";
            case AnonymousClass1Y3.A1g /*214*/:
                return "SUCCESS_FETCH_IMAGE";
            case AnonymousClass1Y3.A1h /*215*/:
                return "FAIL_FETCH_IMAGE";
            case 216:
                return "DROPPED";
            case AnonymousClass1Y3.A1i /*217*/:
                return "QUERY_CHUNKS";
            case 218:
                return "QUERY_ROWS";
            case AnonymousClass1Y3.A1j /*219*/:
                return "COMPUTE_CHUNKS";
            case 220:
                return "SCROLL_COMPLETE";
            case AnonymousClass1Y3.A1k /*221*/:
                return "USE_INSTANCE_STATE";
            case 222:
                return "USE_ARGUMENTS";
            case 223:
                return "OFFSCREEN";
            case 224:
                return "LOG_READ";
            case AnonymousClass1Y3.A1l /*225*/:
                return "LOG_COMPACTED";
            case 226:
                return "LOG_WRITER_OPENED";
            case AnonymousClass1Y3.A1m /*227*/:
                return "TAB_SWITCH";
            case 228:
                return "EXIT_VIEW_CONTROLLER";
            case AnonymousClass1Y3.A1n /*229*/:
                return "TAGS_PREPARED";
            case 230:
                return "VIEW_DID_APPEAR";
            case 231:
                return "MEDIA_TOO_SMALL";
            case 232:
                return "ITEM_SELECTED";
            case AnonymousClass1Y3.A1o /*233*/:
                return "REPOSITIONED";
            case 234:
                return "PHOTO_UPLOAD_START";
            case 235:
                return "MEDIA_EDIT";
            case AnonymousClass1Y3.A1p /*236*/:
                return "MEDIA_EDIT_COMPLETE";
            case 237:
                return "VIDEO_PAUSE";
            case 238:
                return "VIDEO_COMPLETE";
            case 239:
                return "VIDEO_PLAYING_TIMEOUT";
            case AnonymousClass1Y3.A1q /*240*/:
                return "VIDEO_PLAYING_QPL_TIMEOUT";
            case 241:
                return "SESSION_OPEN";
            case AnonymousClass1Y3.A1r /*242*/:
                return "SCHEMA_HASH_MISMATCH";
            case AnonymousClass1Y3.A1s /*243*/:
                return "OVERRIDES_EXIST";
            case AnonymousClass1Y3.A1t /*244*/:
                return "DI_DONE";
            case AnonymousClass1Y3.A1u /*245*/:
                return "FLATBUFFER_SCHEMA_ABSENT";
            case AnonymousClass1Y3.A1v /*246*/:
                return "SUCCESS_NONEXISTENCE";
            case AnonymousClass1Y3.A1w /*247*/:
                return "VIDEO_CANCELLED";
            case 248:
                return "MESSENGER_QUEUE_CREATION_FAILURE";
            case AnonymousClass1Y3.A1x /*249*/:
                return "MESSENGER_DELTA_REQUEST_FAILURE";
            case AnonymousClass1Y3.A1y /*250*/:
                return "RETURN_EXCEPTION_TO_CALLER";
            case AnonymousClass1Y3.A1z /*251*/:
                return "SURFACE_TEXTURE_AVAILABLE";
            case 252:
                return "NEWS_FEED_FRAGMENT";
            case AnonymousClass1Y3.A20 /*253*/:
                return "HEADER_CACHE_FETCH_STARTED";
            case 254:
            case 255:
            case DexStore.LOAD_RESULT_OATMEAL_QUICKENED /*256*/:
            case AnonymousClass1Y3.A21 /*257*/:
            case AnonymousClass1Y3.A26 /*270*/:
            case 271:
            case AnonymousClass1Y3.A27 /*272*/:
            case 273:
            case 315:
            case 316:
            case AnonymousClass1Y3.A2V /*317*/:
            case AnonymousClass1Y3.A2W /*318*/:
            case AnonymousClass1Y3.A2X /*319*/:
            case AnonymousClass1Y3.A2Y /*320*/:
            case 321:
            case AnonymousClass1Y3.A2Z /*322*/:
            case 323:
            case AnonymousClass1Y3.A2a /*324*/:
            case 347:
            case 348:
            case AnonymousClass1Y3.A2k /*349*/:
            case AnonymousClass1Y3.A2l /*350*/:
            case AnonymousClass1Y3.A2m /*351*/:
            case AnonymousClass1Y3.A2n /*352*/:
            case AnonymousClass1Y3.A2o /*353*/:
            case AnonymousClass1Y3.A2p /*354*/:
            case AnonymousClass1Y3.A2q /*355*/:
            case 356:
            case 357:
            case 358:
            case 359:
            case AnonymousClass1Y3.A2r /*360*/:
            case AnonymousClass1Y3.A2s /*361*/:
            case AnonymousClass1Y3.A2t /*362*/:
            case AnonymousClass1Y3.A2u /*363*/:
            case 364:
            case 365:
            case 366:
            case AnonymousClass1Y3.A2v /*367*/:
            case AnonymousClass1Y3.A2w /*368*/:
            case 369:
            case AnonymousClass1Y3.A2x /*370*/:
            case AnonymousClass1Y3.A2y /*371*/:
            case AnonymousClass1Y3.A2z /*372*/:
            case AnonymousClass1Y3.A30 /*373*/:
            case 374:
            case 375:
            case AnonymousClass1Y3.A31 /*376*/:
            case AnonymousClass1Y3.A32 /*377*/:
            case AnonymousClass1Y3.A33 /*378*/:
            case AnonymousClass1Y3.A34 /*379*/:
            case AnonymousClass1Y3.A35 /*380*/:
            case 381:
            case AnonymousClass1Y3.A36 /*382*/:
            case AnonymousClass1Y3.A37 /*383*/:
            case AnonymousClass1Y3.A38 /*384*/:
            case 385:
            case AnonymousClass1Y3.A39 /*386*/:
            case 387:
            case 388:
            case 389:
            case AnonymousClass1Y3.A3A /*390*/:
            case 391:
            case AnonymousClass1Y3.A3S /*421*/:
            case 422:
            case AnonymousClass1Y3.A3T /*423*/:
            case 424:
            case AnonymousClass1Y3.A3U /*425*/:
            case 426:
            case 427:
            case AnonymousClass1Y3.A3V /*428*/:
            case AnonymousClass1Y3.A4l /*568*/:
            case 584:
            case AnonymousClass1Y3.A4s /*585*/:
            case 586:
            case 587:
            case 588:
            case 589:
            case AnonymousClass1Y3.A4t /*590*/:
            case 591:
            case 592:
            case 593:
            case AnonymousClass1Y3.A4u /*594*/:
            case 595:
            case 596:
            case AnonymousClass1Y3.A4v /*597*/:
            case 602:
            case 607:
            case AnonymousClass1Y3.A4w /*608*/:
            case 609:
            case 611:
            case 612:
            case AnonymousClass1Y3.A4y /*616*/:
            case 617:
            case AnonymousClass1Y3.A4z /*618*/:
            case 619:
            case 620:
            case AnonymousClass1Y3.A50 /*621*/:
            case 622:
            case AnonymousClass1Y3.A51 /*623*/:
            case AnonymousClass1Y3.A52 /*624*/:
            case 625:
            default:
                return "UNDEFINED_QPL_ACTION";
            case 258:
                return "CHANGESET_ENQUEUED";
            case 259:
                return "RTC_BROADCAST_INITIALIZED";
            case AnonymousClass1Y3.A22 /*260*/:
                return "RTC_STREAMING_INITIALIZED";
            case 261:
                return "RTC_STREAMING_STARTED";
            case 262:
                return "CAMERA_VIDEO_OUTPUT_SWITCHED";
            case 263:
                return "RTMP_STREAM_STOPPED";
            case 264:
                return "LOCATION_FETCH_BEGIN";
            case 265:
                return "LOCATION_FETCH_SUCCESS";
            case AnonymousClass1Y3.A23 /*266*/:
                return "LOCATION_FETCH_FAILED";
            case AnonymousClass1Y3.A24 /*267*/:
                return "FIRST_UPDATE_SUCCESS";
            case 268:
                return "FIRST_UPDATE_FAILURE";
            case AnonymousClass1Y3.A25 /*269*/:
                return "RTMP_STREAMING_HANDLED_FRAME";
            case 274:
                return "RTMP_DVR_HANDLED_FRAME";
            case AnonymousClass1Y3.A28 /*275*/:
                return "LIVE_RTMP_STREAMING_HANDLED_FRAME";
            case 276:
                return "LIVE_RTMP_DVR_HANDLED_FRAME";
            case AnonymousClass1Y3.A29 /*277*/:
                return "PASS_QE_CHECK";
            case AnonymousClass1Y3.A2A /*278*/:
                return "PASS_AD_CHECK";
            case AnonymousClass1Y3.A2B /*279*/:
                return "PASS_SAMPLE_RATE_CHECK";
            case 280:
                return "HAS_VALID_HTML";
            case AnonymousClass1Y3.A2C /*281*/:
                return "FOUND_IMAGES";
            case AnonymousClass1Y3.A2D /*282*/:
                return "TIGON_REQUEST_BEGIN";
            case 283:
                return "TIGON_REQUEST_END";
            case 284:
                return "TIGON_RESPONSE_BEGIN";
            case AnonymousClass1Y3.A2E /*285*/:
                return "TIGON_RESPONSE_END";
            case 286:
                return "TIGON_PARSE_BEGIN";
            case AnonymousClass1Y3.A2F /*287*/:
                return "TIGON_PARSE_END";
            case AnonymousClass1Y3.A2G /*288*/:
                return "GRAPHQL_CACHE_FETCH_START";
            case AnonymousClass1Y3.A2H /*289*/:
                return "GRAPHQL_CACHE_FETCH_END";
            case AnonymousClass1Y3.A2I /*290*/:
                return "VIDEO_START_STALL";
            case 291:
                return "VIDEO_END_STALL";
            case AnonymousClass1Y3.A2J /*292*/:
                return "MERGE_LOCAL_FIELDS";
            case 293:
                return "LOAD_QUERY_STRING";
            case AnonymousClass1Y3.A2K /*294*/:
                return "CONFIG_TABLE_SCHEMA_HASH_MISMATCH";
            case 295:
                return "CONFIG_TABLE_SCHEMA_ABSENT";
            case AnonymousClass1Y3.A2L /*296*/:
                return "CONFIG_TABLE_MAGIC_MISMATCH";
            case AnonymousClass1Y3.A2M /*297*/:
                return "OBJSEL_FETCH";
            case AnonymousClass1Y3.A2N /*298*/:
                return "GO_TO_AD_ACTIVITY";
            case 299:
                return "SAVE_AD";
            case 300:
                return "UNSAVE_AD";
            case 301:
                return "SAVE_MODE";
            case 302:
                return "GO_AD_ACTIVITY_MODE";
            case AnonymousClass1Y3.A2O /*303*/:
                return "ACCESSIBILITY_ACTIVATE";
            case 304:
                return "ACCESSIBILITY_CUSTOM";
            case 305:
                return "ACCESSIBILITY_MAGIC_TAP";
            case 306:
                return "FINAL_IMAGE_SET";
            case AnonymousClass1Y3.A2P /*307*/:
                return "INTERMEDIATE_IMAGE_SET";
            case AnonymousClass1Y3.A2Q /*308*/:
                return "INTERMEDIATE_IMAGE_FAIL";
            case AnonymousClass1Y3.A2R /*309*/:
                return "INTERMEDIATE_IMAGE_GOOD_ENOUGH_SET";
            case AnonymousClass1Y3.A2S /*310*/:
                return "APP_BACKGROUND";
            case 311:
                return "APP_FOREGROUND";
            case 312:
                return "RECEIVED_HARDWARE_FRAME";
            case AnonymousClass1Y3.A2T /*313*/:
                return "CAMERA_COMPONENT_MOUNTED";
            case AnonymousClass1Y3.A2U /*314*/:
                return "MQTT_CONNECTION_ATTEMPTED";
            case 325:
                return "DELTA_APPLICATION_COMPLETED";
            case 326:
                return "DELTAS_RECEIVED_AFTER_CONNECT";
            case AnonymousClass1Y3.A2b /*327*/:
                return "MESSENGER_DAY_UNIT_DISPLAYED";
            case AnonymousClass1Y3.A2c /*328*/:
                return "DATA_LOAD_END";
            case 329:
                return "VIEW_DID_BECOME_VISIBLE";
            case 330:
                return "DELTA_APPLICATION_INTERRUPTED";
            case 331:
                return "DELTA_BATCH_APPLICATION_COMPLETED";
            case 332:
                return "MESSAGE_LIST_DID_UPDATE";
            case 333:
                return "MESSAGE_LIST_WILL_UPDATE";
            case 334:
                return "SUCCESS_ZERO_WAIT_TIME";
            case 335:
                return "USER_SCROLLED";
            case 336:
                return "SPINNER_APPEARED";
            case AnonymousClass1Y3.A2d /*337*/:
                return "MODEL_ENQUEUED";
            case 338:
                return "VIEW_MODEL_APPLIED";
            case 339:
                return "PRESENTED";
            case AnonymousClass1Y3.A2e /*340*/:
                return "VIEW_DID_BECOME_VISIBLE_END";
            case AnonymousClass1Y3.A2f /*341*/:
                return "MESSENGER_DELTA_REQUEST_INIT";
            case 342:
                return "FEED_REQUEST_STARTED";
            case AnonymousClass1Y3.A2g /*343*/:
                return "FEED_REQUEST_FAILED";
            case AnonymousClass1Y3.A2h /*344*/:
                return "FEED_REQUEST_SUCCEEDED";
            case AnonymousClass1Y3.A2i /*345*/:
                return "FEED_LOAD_FROM_DISK_FINISHED";
            case AnonymousClass1Y3.A2j /*346*/:
                return "STORIES_REQUEST_STARTED";
            case AnonymousClass1Y3.A3B /*392*/:
                return "STORIES_REQUEST_FAILED";
            case AnonymousClass1Y3.A3C /*393*/:
                return "STORIES_REQUEST_SUCCEEDED";
            case AnonymousClass1Y3.A3D /*394*/:
                return "STORIES_LOAD_FROM_DISK_FINISHED";
            case AnonymousClass1Y3.A3E /*395*/:
                return "USER_INFO_LOADED";
            case AnonymousClass1Y3.A3F /*396*/:
                return "APP_CREATED";
            case AnonymousClass1Y3.A3G /*397*/:
                return "INIT_TO_USABLE";
            case 398:
                return "INIT_TO_NETWORK_CONTENT";
            case AnonymousClass1Y3.A3H /*399*/:
                return "TOTAL";
            case AnonymousClass1Y3.A3I /*400*/:
                return "HEAD";
            case 401:
                return "TAIL";
            case AnonymousClass1Y3.A3J /*402*/:
                return "INITIAL";
            case AnonymousClass1Y3.A3K /*403*/:
                return "OTHER";
            case 404:
                return "ANR_START_DATA_CAPTURE";
            case 405:
                return "ANR_ENQUEUE";
            case 406:
                return "NOTIF_NOT_ALERTED";
            case AnonymousClass1Y3.A3L /*407*/:
                return "NOTIF_DUPLICATE";
            case AnonymousClass1Y3.A3M /*408*/:
                return "NOTIF_MUTED";
            case 409:
                return "NOTIF_BUZZED";
            case 410:
                return "FEED_RESPONSE_PROCESSED";
            case AnonymousClass1Y3.A3N /*411*/:
                return "STORIES_RESPONSE_PROCESSED";
            case AnonymousClass1Y3.A3O /*412*/:
                return "APP_DID_FINISH_LAUNCHING_ENDED";
            case 413:
                return "ASYNC_BEGIN_DB";
            case AnonymousClass1Y3.A3P /*414*/:
                return "DELTA_APPLICATION_STARTED";
            case 415:
                return "ASYNC_SUCCESS_DB";
            case AnonymousClass1Y3.A3Q /*416*/:
                return "ASYNC_FAIL_DB";
            case 417:
                return "ASYNC_BEGIN_SERVER";
            case 418:
                return "ASYNC_SUCCESS_SERVER";
            case 419:
                return "ASYNC_FAIL_SERVER";
            case AnonymousClass1Y3.A3R /*420*/:
                return "LEAVE";
            case AnonymousClass1Y3.A3W /*429*/:
                return "ASYNC_SUCCESS_DB_NO_DATA";
            case AnonymousClass1Y3.A3X /*430*/:
                return "ASYNC_FAIL_SERVER_NO_DATA";
            case AnonymousClass1Y3.A3Y /*431*/:
                return "SUCCESS_MEMORY";
            case 432:
                return "SUCCESS_SERVER";
            case 433:
                return "FAIL_NO_DATA";
            case AnonymousClass1Y3.A3Z /*434*/:
                return "ACTIVITY_NEW_INTENT";
            case AnonymousClass1Y3.A3a /*435*/:
                return "GRID_MEDIA_LOADED";
            case 436:
                return "STORY_TRAY_MEDIA_LOADED";
            case 437:
                return "INTERACTION_OPEN_QRCODE_SCANNER";
            case 438:
                return "MEDIA_LOAD_CACHE";
            case AnonymousClass1Y3.A3b /*439*/:
                return "MEDIA_LOAD_NETWORK";
            case AnonymousClass1Y3.A3c /*440*/:
                return "COMMENTS_LOAD_START";
            case 441:
                return "COMMENTS_LOAD_COMPLETE";
            case 442:
                return "MEDIA_LOAD_START";
            case 443:
                return "SCROLL_START";
            case AnonymousClass1Y3.A3d /*444*/:
                return "QRCODE_SCANNER_SCAN_SUCCESS";
            case AnonymousClass1Y3.A3e /*445*/:
                return "QRCODE_SCANNER_SCAN_FAILURE";
            case AnonymousClass1Y3.A3f /*446*/:
                return "APP_CREATED_MAIN_PROCESS";
            case 447:
                return "FINISH_REGISTERING_JS_NATIVE_MODULES";
            case AnonymousClass1Y3.A3g /*448*/:
                return "NON_ANR";
            case 449:
                return "FINISH_INITIALIZING_JS_BRIDGE";
            case 450:
                return "FINISH_INJECTING_JS_HOOKS";
            case AnonymousClass1Y3.A3h /*451*/:
                return "FINISH_RUNNING_JS_INITIALIZER";
            case AnonymousClass1Y3.A3i /*452*/:
                return "UI_RESPONSIVE";
            case AnonymousClass1Y3.A3j /*453*/:
                return "PHASE_ONE_COMPLETE";
            case AnonymousClass1Y3.A3k /*454*/:
                return "PHASE_TWO_COMPLETE";
            case AnonymousClass1Y3.A3l /*455*/:
                return "LS_HEADER_START";
            case 456:
                return "LS_HEADER_LOAD";
            case AnonymousClass1Y3.A3m /*457*/:
                return "LS_BODY_START";
            case AnonymousClass1Y3.A3n /*458*/:
                return "LS_BODY_LOAD";
            case 459:
                return "APP_ONCREATE";
            case AnonymousClass1Y3.A3o /*460*/:
                return "START_FETCH_IMAGE";
            case AnonymousClass1Y3.A3p /*461*/:
                return "LOGGED_OUT";
            case AnonymousClass1Y3.A3q /*462*/:
                return "LS_HEADER_LAYOUT_SUCCESS";
            case 463:
                return "STARTED_CAPTURE_SESSION";
            case 464:
                return "CAMERA_VIEW_READY";
            case 465:
                return "CAMERA_FIRST_FRAME";
            case AnonymousClass1Y3.A3r /*466*/:
                return "PHOTO_BITMAP_READY";
            case AnonymousClass1Y3.A3s /*467*/:
                return "END";
            case 468:
                return "FRAME_RENDERED";
            case AnonymousClass1Y3.A3t /*469*/:
                return "LS_MAP_LIST_START";
            case 470:
                return "LS_MAP_LIST_LOADED";
            case AnonymousClass1Y3.A3u /*471*/:
                return "LS_MAP_LIST_SUCCESS";
            case AnonymousClass1Y3.A3v /*472*/:
                return "FETCH_THREAD_STARTED";
            case 473:
                return "FETCH_THREAD_SUCCEEDED";
            case AnonymousClass1Y3.A3w /*474*/:
                return "FETCH_THREAD_FAILED";
            case AnonymousClass1Y3.A3x /*475*/:
                return "STRUCTURE_FETCH_COMPLETE";
            case 476:
                return "FOLLOWED_SHOWS_FETCHED";
            case 477:
                return "ABANDONED";
            case AnonymousClass1Y3.A3y /*478*/:
                return "ENTERED";
            case 479:
                return "LS_FILTER_START";
            case 480:
                return "LS_FILTER_LOAD";
            case 481:
                return "LS_FILTER_SUCCESS";
            case 482:
                return "LS_SEARCH_RESULT_START";
            case AnonymousClass1Y3.A3z /*483*/:
                return "LS_SEARCH_RESULT_LOAD";
            case 484:
                return "NETWORK_PARSE_START";
            case 485:
                return "NETWORK_REQUEST_SENT";
            case 486:
                return "COMPONENT_DATA_MODEL_UPDATE_START";
            case 487:
                return "COMPONENT_DATA_MODEL_UPDATE_COMPLETE";
            case 488:
                return "HIGH_RES_PHOTO_FILE_READY";
            case 489:
                return "NATIVE_PHOTO_BITMAP_READY";
            case 490:
                return "REEL_JSON_RECEIVED";
            case AnonymousClass1Y3.A40 /*491*/:
                return "REEL_MEDIA_RECEIVED";
            case AnonymousClass1Y3.A41 /*492*/:
                return "STORY_VIEWER_APPEAR";
            case AnonymousClass1Y3.A42 /*493*/:
                return "FETCH_INBOX_STARTED";
            case AnonymousClass1Y3.A43 /*494*/:
                return "FETCH_INBOX_SUCCEEDED";
            case AnonymousClass1Y3.A44 /*495*/:
                return "FETCH_INBOX_FAILED";
            case AnonymousClass1Y3.A45 /*496*/:
                return "REQUEST_ADDED";
            case AnonymousClass1Y3.A46 /*497*/:
                return "INIT_QE_START";
            case AnonymousClass1Y3.A47 /*498*/:
                return "INIT_QE_END";
            case AnonymousClass1Y3.A48 /*499*/:
                return "MULTIDEX_INSTALLED";
            case 500:
                return "PREPARE_CAMERA_SESSION";
            case AnonymousClass1Y3.A4A /*501*/:
                return "START_CAMERA_SESSION";
            case 502:
                return "DID_START_CAMERA_SESSION";
            case AnonymousClass1Y3.A4B /*503*/:
                return "FIRST_HARDWARE_FRAME";
            case 504:
                return "START_RENDERING_FIRST_USER_FRAME";
            case AnonymousClass1Y3.A4C /*505*/:
                return "FIRST_MEDIA_RENDERED";
            case AnonymousClass1Y3.A4D /*506*/:
                return "FIRST_CACHED_MEDIA_RENDERED";
            case AnonymousClass1Y3.A4E /*507*/:
                return "CAMERA_INITIALIZED";
            case 508:
                return "HTTP_TRANSACTION_STARTED";
            case 509:
                return "REMOTE_PROCESS";
            case AnonymousClass1Y3.A4F /*510*/:
                return "PRE_REQUEST_SEND_CALLED";
            case 511:
                return "BRIDGE_STARTUP_WILL_START";
            case 512:
                return "BRIDGE_STARTUP_DID_FINISH";
            case AnonymousClass1Y3.A4H /*513*/:
                return "COVER_PHOTO_LOW_RES";
            case 514:
                return "COVER_PHOTO_HIGH_RES";
            case AnonymousClass1Y3.A4I /*515*/:
                return "PROFILE_PIC_LOW_RES";
            case AnonymousClass1Y3.A4J /*516*/:
                return "PROFILE_PIC_HIGH_RES";
            case AnonymousClass1Y3.A4K /*517*/:
                return "CONTEXT_ITEMS";
            case AnonymousClass1Y3.A4L /*518*/:
                return "MEDIA_LOADED";
            case AnonymousClass1Y3.A4M /*519*/:
                return "STATE_UPDATE";
            case AnonymousClass1Y3.A4N /*520*/:
                return "ON_SHOW_LOGIN";
            case 521:
                return "EMPTY_REQUEST";
            case AnonymousClass1Y3.A4O /*522*/:
                return "METERED_CONNECTION";
            case AnonymousClass1Y3.A4P /*523*/:
                return "VIDEO_DOWNSTREAM_FORMAT_CHANGED";
            case 524:
                return "DISABLED";
            case AnonymousClass1Y3.A4Q /*525*/:
                return "NO_METADATA";
            case 526:
                return "INCOMPLETE_METADATA";
            case AnonymousClass1Y3.A4R /*527*/:
                return "PHOTO_CAPTURE_READY";
            case AnonymousClass1Y3.A4S /*528*/:
                return "CAMERA_START_READY";
            case AnonymousClass1Y3.A4T /*529*/:
                return "DELAY_START";
            case 530:
                return "DELAY_STOP";
            case AnonymousClass1Y3.A4U /*531*/:
                return "RANK_START";
            case AnonymousClass1Y3.A4V /*532*/:
                return "RANK_STOP";
            case AnonymousClass1Y3.A4W /*533*/:
                return "DB_WRITE_START";
            case 534:
                return "DB_WRITE_STOP";
            case AnonymousClass1Y3.A4X /*535*/:
                return "ADD_STORY_TO_UI";
            case AnonymousClass1Y3.A4Y /*536*/:
                return "WEBVIEW_URI_REDIRECTOR_CONSTRUCTION";
            case AnonymousClass1Y3.A4Z /*537*/:
                return "UNINTERRUPTED";
            case 538:
                return "VIDEO_TOGGLE_FULL_SCREEN";
            case AnonymousClass1Y3.A4a /*539*/:
                return "CONFIGURE_START";
            case 540:
                return "CONFIGURE_END";
            case AnonymousClass1Y3.A4b /*541*/:
                return "BROWSER_OPEN";
            case 542:
                return "FIRST_DATA_RECEIVED";
            case AnonymousClass1Y3.A4c /*543*/:
                return "RVP_WILL_LOAD";
            case AnonymousClass1Y3.A4d /*544*/:
                return "RVP_DID_LOAD";
            case 545:
                return "CACHE_WRITE_START";
            case 546:
                return "CACHE_WRITE_SUCCESS";
            case 547:
                return "CACHE_WRITE_FAIL";
            case 548:
                return "RVP_WILL_PAUSE";
            case AnonymousClass1Y3.A4e /*549*/:
                return "RVP_DID_PAUSE";
            case 550:
                return "RVP_WILL_PLAY";
            case 551:
                return "RVP_DID_PLAY";
            case 552:
                return "RVP_WILL_PREPARE";
            case 553:
                return "RVP_DID_PREPARE";
            case 554:
                return "RVP_WILL_RELOAD";
            case 555:
                return "RVP_DID_RELOAD";
            case 556:
                return "RVP_WILL_CREATE";
            case AnonymousClass1Y3.A4f /*557*/:
                return "RVP_DID_CREATE";
            case 558:
                return "RVP_WILL_FINISH_INFLATE";
            case AnonymousClass1Y3.A4g /*559*/:
                return "RVP_DID_FINISH_INFLATE";
            case AnonymousClass1Y3.A4h /*560*/:
                return "RVP_WILL_MOUNT";
            case 561:
                return "RVP_DID_MOUNT";
            case AnonymousClass1Y3.A4i /*562*/:
                return "START_COMPRESSING_MESSAGE";
            case AnonymousClass1Y3.A4j /*563*/:
                return "MESSAGE_COMPRESSED";
            case 564:
                return "START_DECOMPRESSING_MESSAGE";
            case 565:
                return "MESSAGE_DECOMPRESSED";
            case AnonymousClass1Y3.A4k /*566*/:
                return "START_SENDING_MESSAGE";
            case 567:
                return "CAMERA_PREVIEW_FROZEN";
            case 569:
                return "VC_INIT_START";
            case 570:
                return "VC_INIT_BEGIN";
            case 571:
                return "VC_INIT_END";
            case AnonymousClass1Y3.A4m /*572*/:
                return "VC_VIEW_DID_LOAD_BEGIN";
            case AnonymousClass1Y3.A4n /*573*/:
                return "VC_VIEW_DID_LOAD_END";
            case 574:
                return "ROOT_QUERY_START";
            case AnonymousClass1Y3.A4o /*575*/:
                return "ROOT_QUERY_SUCCESS";
            case 576:
                return "ROOT_QUERY_FAIL";
            case 577:
                return "CACHE_INITIALIZED";
            case 578:
                return "FETCH_BEGIN";
            case 579:
                return "FETCH_FINISHED";
            case AnonymousClass1Y3.A4p /*580*/:
                return "PARSING_FINISHED";
            case AnonymousClass1Y3.A4q /*581*/:
                return "LOAD_VIEW_END";
            case 582:
                return "LOAD_URL_BEGIN";
            case AnonymousClass1Y3.A4r /*583*/:
                return "LOAD_URL_END";
            case 598:
                return "SHOULD_LOAD_URL_BEGIN";
            case 599:
                return "SHOULD_LOAD_URL_END";
            case 600:
                return "BLOCKING_RESOURCES_LOADED";
            case 601:
                return "WEB_PAGE_LOADED";
            case 603:
                return "JS_TEARDOWN";
            case 604:
                return "JS_SETUP";
            case 605:
                return "PREP_STATE";
            case 606:
                return "PREP_FORMS";
            case AnonymousClass1Y3.A4x /*610*/:
                return "RUN_FUNCTION";
            case 613:
                return "LOAD_MODULES";
            case 614:
                return "VIDEO_READY_TO_PLAY";
            case 615:
                return "CANCEL_NAVIGATION";
            case AnonymousClass1Y3.A53 /*626*/:
                return "BEGIN_HANDLE_EVENT";
            case AnonymousClass1Y3.A54 /*627*/:
                return "END_HANDLE_EVENT";
            case AnonymousClass1Y3.A55 /*628*/:
                return "BEGIN_PROCESS_EVENT";
            case 629:
                return "END_PROCESS_EVENT";
            case AnonymousClass1Y3.A56 /*630*/:
                return "CANCEL_BACKGROUND";
            case 631:
                return "SC_TRACKER_SETUP_BEGIN";
            case AnonymousClass1Y3.A57 /*632*/:
                return "SC_TRACKER_SETUP_END";
            case 633:
                return "PROFILE_TOOLBOX_SETUP_BEGIN";
            case 634:
                return "PROFILE_TOOLBOX_SETUP_END";
            case AnonymousClass1Y3.A58 /*635*/:
                return "FEED_TOOLBOX_SETUP_BEGIN";
            case 636:
                return "FEED_TOOLBOX_SETUP_END";
            case AnonymousClass1Y3.A59 /*637*/:
                return "SCREEN_PART_RECEIVED";
            case AnonymousClass1Y3.A5A /*638*/:
                return "RVP_DID_FAIL_AUTOPLAY";
            case AnonymousClass1Y3.A5B /*639*/:
                return "VIEW_WILL_APPEAR_END";
            case AnonymousClass1Y3.A5C /*640*/:
                return "VIDEO_RENDERED";
            case 641:
                return "VIEW_DID_APPEAR_END";
            case 642:
                return "SHARE_FLOW_LOADED";
            case 643:
                return "START_DOWNLOAD_FACE_DETECTION_EFFECT";
            case 644:
                return "APPLY_FACE_DETECTION_EFFECT";
            case 645:
                return "INTERACTION_SWIPE_UP";
            case AnonymousClass1Y3.A5D /*646*/:
                return "LOOM_PROVIDER_FAILURE";
            case AnonymousClass1Y3.A5E /*647*/:
                return "ADS_SELECT_POST_VIEW";
            case AnonymousClass1Y3.A5F /*648*/:
                return "ADS_SELECT_IMAGE_VIEW";
            case 649:
                return "ADS_SELECT_BUDGET_VIEW";
            case 650:
                return "ADS_SELECT_AUDIENCE_VIEW";
            case 651:
                return "ADS_SELECT_CREATIVE_VIEW";
            case AnonymousClass1Y3.A5G /*652*/:
                return "VIDEO_FETCH_REQUEST_START";
            case AnonymousClass1Y3.A5H /*653*/:
                return "VIDEO_FETCH_REQUEST_RECEIVED";
            case AnonymousClass1Y3.A5I /*654*/:
                return "VIDEO_FETCH_REQUEST_ENTER_NETWORK_QUEUE";
            case 655:
                return "VIDEO_FETCH_REQUEST_NETWORK_REQUEST_START";
            case 656:
                return "VIDEO_FETCH_REQUEST_NETWORK_RESPONSE_RECEIVED";
            case AnonymousClass1Y3.A5J /*657*/:
                return "VIDEO_FETCH_REQUEST_NETWORK_FIRST_BYTE_ARRIVED";
            case 658:
                return "VIDEO_FETCH_REQUEST_NETWORK_TRANSFER_COMPLETE";
            case AnonymousClass1Y3.A5K /*659*/:
                return "VIDEO_FETCH_REQUEST_SATISFIED_BY_CACHE";
            case 660:
                return "VIDEO_FETCH_REQUEST_COMPLETE";
            case 661:
                return "VIDEO_FETCH_REQUEST_CACHE_CHECK_START";
            case 662:
                return "VIDEO_FETCH_REQUEST_CACHE_CHECK_END";
            case AnonymousClass1Y3.A5L /*663*/:
                return "VIDEO_FETCH_REQUEST_DID_ATTACH_TO_NETWORK_REQUEST";
            case 664:
                return "SELECT_PHOTOS_FAILED_SCORE";
            case AnonymousClass1Y3.A5M /*665*/:
                return "SELECT_PHOTOS_FAILED_TIMESTAMP";
            case 666:
                return "INTERACTION_CLICK";
            case 667:
                return "VIDEO_SCRUBBER_FIRST_THUMBNAIL_SHOWN";
            case AnonymousClass1Y3.A5N /*668*/:
                return "VIDEO_SCRUBBER_THUMBNAIL_SHOWN";
            case AnonymousClass1Y3.A5O /*669*/:
                return "COLD_START_BEGIN";
            case 670:
                return "COLD_START_END";
            case AnonymousClass1Y3.A5P /*671*/:
                return "COLD_START_LOAD_APP_JS";
            case 672:
                return "COLD_START_QUERY_SEND";
            case AnonymousClass1Y3.A5Q /*673*/:
                return "COLD_START_APP_SHELL_COMPONENT_DID_MOUNT";
            case AnonymousClass1Y3.A5R /*674*/:
                return "VIDEO_RECORDING_START_CALLED";
            case AnonymousClass1Y3.A5S /*675*/:
                return "VIDEO_RECORDING_STOP_CALLED";
            case AnonymousClass1Y3.A5T /*676*/:
                return "WIKTORK_TEST";
            case AnonymousClass1Y3.A5U /*677*/:
                return "WIKTORK_TEST_TWO";
            case 678:
                return "ON_VIDEO_RECORDING_FINISHED";
            case AnonymousClass1Y3.A5V /*679*/:
                return "MEASURE_IMAGE";
            case 680:
                return "PDP_RENDER_LOADING";
            case 681:
                return "PDP_RENDER_FETCHED";
            case AnonymousClass1Y3.A5W /*682*/:
                return "VIDEO_FETCH_REQUEST_FAILED";
            case 683:
                return "LOGIN_FLOW_STARTED";
            case AnonymousClass1Y3.A5X /*684*/:
                return "LOGIN_FLOW_COMPLETED";
            case AnonymousClass1Y3.A5Y /*685*/:
                return "VIDEO_DOWNLOAD_STARTED";
            case AnonymousClass1Y3.A5Z /*686*/:
                return "VIDEO_DOWNLOAD_READY_TO_PLAY";
            case 687:
                return "VIDEO_DOWNLOAD_FAILED";
            case 688:
                return "FBLITE_SCREEN_RECEIVED";
            case 689:
                return "FBLITE_SERVER_START";
            case AnonymousClass1Y3.A5a /*690*/:
                return "FBLITE_SERVER_END";
            case 691:
                return "FBLITE_INCOMPLETE";
            case 692:
                return "BACKGROUND_THREAD";
            case AnonymousClass1Y3.A5b /*693*/:
                return "MAIN_THREAD";
            case AnonymousClass1Y3.A5c /*694*/:
                return "BWE_ESTIMATE_COMPLETE";
            case AnonymousClass1Y3.A5d /*695*/:
                return "WIKTORK_TEST_THREE";
            case AnonymousClass1Y3.A5e /*696*/:
                return "FBLITE_UNEXPECTED";
            case AnonymousClass1Y3.A5f /*697*/:
                return "THREAD_ASYNC_BEGIN_SERVER";
            case 698:
                return "THREAD_ASYNC_SUCCESS_SERVER";
            case AnonymousClass1Y3.A5g /*699*/:
                return "CARD_ASYNC_BEGIN_SERVER";
            case AnonymousClass1Y3.A5h /*700*/:
                return "CARD_ASYNC_SUCCESS_SERVER";
            case AnonymousClass1Y3.A5i /*701*/:
                return "FBLITE_INSTANT_TRANSITION_FAILED";
            case 702:
                return "BB_TRACE_REQUESTED";
            case AnonymousClass1Y3.A5j /*703*/:
                return "CANCEL_PSP";
            case 704:
                return "AGGREGATED";
            case 705:
                return "UNINSTRUMENTED";
            case AnonymousClass1Y3.A5k /*706*/:
                return "CANCEL_UNLOAD";
        }
    }
}
