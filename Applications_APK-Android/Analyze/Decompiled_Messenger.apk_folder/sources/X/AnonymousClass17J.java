package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.17J  reason: invalid class name */
public final class AnonymousClass17J extends AnonymousClass0W4 {
    private static final C06060am A00 = new C06050al(ImmutableList.of(AnonymousClass17K.A01));
    private static final ImmutableList A01 = ImmutableList.of(AnonymousClass17K.A01, AnonymousClass17K.A00);

    public AnonymousClass17J() {
        super("favorite_sms_contacts", A01, A00);
    }
}
