package X;

import com.facebook.profilo.core.TriggerRegistry;
import com.facebook.profilo.ipc.TraceContext;
import java.util.Random;

/* renamed from: X.04k  reason: invalid class name and case insensitive filesystem */
public final class C003404k implements AnonymousClass04j, C003504l {
    public static final int A01;
    public static final int A02;
    public Random A00 = new Random();

    public boolean AUJ(long j, Object obj, long j2, Object obj2) {
        return j == j2;
    }

    public int AYs(long j, Object obj, AnonymousClass057 r7) {
        int i;
        C008507p A002 = ((AnonymousClass05Z) r7).A00((int) j);
        if (A002 == null || (i = A002.A04) == -1 || i == 0) {
            return 0;
        }
        if (i == 1 || this.A00.nextInt(i) == 0) {
            return A002.A00;
        }
        return 0;
    }

    public TraceContext.TraceConfigExtras B6o(long j, Object obj, AnonymousClass057 r8) {
        C008507p A002 = ((AnonymousClass05Z) r8).A00((int) j);
        return new TraceContext.TraceConfigExtras(A002.A03, A002.A01, A002.A02);
    }

    public boolean BE8() {
        return true;
    }

    public boolean BFS(long j, Object obj, int i) {
        if (((int) j) == i) {
            return true;
        }
        return false;
    }

    static {
        AnonymousClass08Y r1 = TriggerRegistry.A00;
        A01 = r1.A02("qpl");
        A02 = r1.A02("sequence_qpl");
    }
}
