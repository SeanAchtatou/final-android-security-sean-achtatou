package X;

/* renamed from: X.1Pw  reason: invalid class name and case insensitive filesystem */
public enum C23531Pw {
    FULL_FETCH(1),
    DISK_CACHE(2),
    ENCODED_MEMORY_CACHE(3),
    BITMAP_MEMORY_CACHE(4);
    
    public int mValue;

    private C23531Pw(int i) {
        this.mValue = i;
    }
}
