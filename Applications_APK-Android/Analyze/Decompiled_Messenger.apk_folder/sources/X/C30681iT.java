package X;

/* renamed from: X.1iT  reason: invalid class name and case insensitive filesystem */
public final class C30681iT implements AnonymousClass1O4 {
    private static final int A00;
    private static final byte[] A01;

    static {
        byte[] A012 = AnonymousClass1O5.A01("KEYF");
        A01 = A012;
        A00 = A012.length;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001a  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001d A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1O3 AXH(byte[] r7, int r8) {
        /*
            r6 = this;
            byte[] r5 = X.C30681iT.A01
            r4 = 4
            int r3 = r5.length
            int r0 = r7.length
            if (r3 > r0) goto L_0x0015
            r2 = 0
        L_0x0008:
            if (r2 >= r3) goto L_0x0017
            int r0 = r2 + r4
            byte r1 = r7[r0]
            byte r0 = r5[r2]
            if (r1 != r0) goto L_0x0015
            int r2 = r2 + 1
            goto L_0x0008
        L_0x0015:
            r0 = 0
            goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            if (r0 == 0) goto L_0x001d
            X.1O3 r0 = X.C30671iS.A00
            return r0
        L_0x001d:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30681iT.AXH(byte[], int):X.1O3");
    }

    public int Aoc() {
        return A00;
    }
}
