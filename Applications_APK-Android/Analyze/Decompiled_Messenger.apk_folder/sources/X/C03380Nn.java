package X;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.0Nn  reason: invalid class name and case insensitive filesystem */
public final class C03380Nn {
    public static final byte[] A00 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    public static String A00(String str) {
        try {
            return A03(str.getBytes("utf-8"), "MD5");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String A01(String str) {
        try {
            return A03(str.getBytes("utf-8"), "SHA-1");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String A02(byte[] bArr) {
        StringBuilder sb = new StringBuilder(r4);
        for (byte b : bArr) {
            byte b2 = b & 255;
            byte[] bArr2 = A00;
            sb.append((char) bArr2[b2 >>> 4]);
            sb.append((char) bArr2[b2 & 15]);
        }
        return sb.toString();
    }

    public static String A03(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.update(bArr, 0, bArr.length);
            return A02(instance.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
