package X;

/* renamed from: X.0Kd  reason: invalid class name and case insensitive filesystem */
public final class C03130Kd {
    public long A00;
    public long A01;
    public long A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C03130Kd r7 = (C03130Kd) obj;
            if (!(this.A00 == r7.A00 && this.A02 == r7.A02 && this.A01 == r7.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        long j = this.A00;
        long j2 = this.A02;
        long j3 = this.A01;
        return (((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)));
    }

    public C03130Kd() {
        this.A00 = 0;
        this.A02 = 0;
        this.A01 = 0;
    }

    public C03130Kd(C03130Kd r3) {
        this.A00 = r3.A00;
        this.A02 = r3.A02;
        this.A01 = r3.A01;
    }
}
