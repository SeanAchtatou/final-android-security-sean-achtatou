package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

/* renamed from: X.0XX  reason: invalid class name */
public class AnonymousClass0XX extends FutureTask implements ListenableFuture, AnonymousClass09S {
    private final C04330Ua A00;
    private final WeakReference A01;
    private final WeakReference A02;

    public void addListener(Runnable runnable, Executor executor) {
        this.A00.A02(runnable, executor);
    }

    public void done() {
        this.A00.A01();
    }

    public Object getInnerRunnable() {
        Callable callable;
        WeakReference weakReference = this.A01;
        Runnable runnable = null;
        if (weakReference != null) {
            callable = (Callable) weakReference.get();
        } else {
            callable = null;
        }
        if (callable != null) {
            return callable;
        }
        WeakReference weakReference2 = this.A02;
        if (weakReference2 != null) {
            runnable = (Runnable) weakReference2.get();
        }
        if (runnable != null) {
            return runnable;
        }
        return this;
    }

    public AnonymousClass0XX(Runnable runnable, Object obj) {
        super(runnable, obj);
        this.A00 = new C04330Ua();
        this.A01 = null;
        this.A02 = new WeakReference(runnable);
    }

    public AnonymousClass0XX(Callable callable) {
        super(callable);
        this.A00 = new C04330Ua();
        this.A01 = new WeakReference(callable);
        this.A02 = null;
    }
}
