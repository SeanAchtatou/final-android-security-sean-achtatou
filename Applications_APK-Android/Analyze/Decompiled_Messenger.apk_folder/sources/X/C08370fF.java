package X;

import com.facebook.quicklog.PerformanceLoggingEvent;

/* renamed from: X.0fF  reason: invalid class name and case insensitive filesystem */
public final class C08370fF extends AnonymousClass0f8 {
    private AnonymousClass0UN A00;

    public String Azg() {
        return "fb4a_startup_stats";
    }

    public static final C08370fF A00(AnonymousClass1XY r1) {
        return new C08370fF(r1);
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        C37981wh r5 = (C37981wh) obj;
        C37981wh r6 = (C37981wh) obj2;
        synchronized (this) {
            if (!(r5 == null || r6 == null)) {
                performanceLoggingEvent.A04("start_op_count", r6.A06 - r5.A06);
                performanceLoggingEvent.A04("activity_listener_count", r6.A00 - r5.A00);
                performanceLoggingEvent.A04("quick_event_listener_count", r6.A05 - r5.A05);
                performanceLoggingEvent.A04("broadcast_receiver_count", r6.A01 - r5.A01);
                performanceLoggingEvent.A04("executed_app_job_count", r6.A03 - r5.A03);
                performanceLoggingEvent.A04("controller_callback_count", r6.A02 - r5.A02);
                performanceLoggingEvent.A04("tigon_request_count", r6.A07 - r5.A07);
                performanceLoggingEvent.A04("pigeon_write_count", r6.A04 - r5.A04);
            }
        }
    }

    public long Azh() {
        return C08350fD.A06;
    }

    public Class B3O() {
        return C37981wh.class;
    }

    public Object CGO() {
        int i = AnonymousClass1Y3.AM2;
        return new C37981wh(((AnonymousClass1ZJ) AnonymousClass1XX.A02(0, i, this.A00)).A06.get(), ((AnonymousClass1ZJ) AnonymousClass1XX.A02(0, i, this.A00)).A00.get(), ((AnonymousClass1ZJ) AnonymousClass1XX.A02(0, i, this.A00)).A05.get(), ((AnonymousClass1ZJ) AnonymousClass1XX.A02(0, i, this.A00)).A01.get(), ((AnonymousClass1ZJ) AnonymousClass1XX.A02(0, i, this.A00)).A03.get(), ((AnonymousClass1ZJ) AnonymousClass1XX.A02(0, i, this.A00)).A02.get(), ((AnonymousClass1ZJ) AnonymousClass1XX.A02(0, i, this.A00)).A07.get(), ((AnonymousClass1ZJ) AnonymousClass1XX.A02(0, i, this.A00)).A04.get());
    }

    private C08370fF(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A04;
    }
}
