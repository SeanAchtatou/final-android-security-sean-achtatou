package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tc  reason: invalid class name and case insensitive filesystem */
public final class C36721tc {
    public static final String[] A04 = {C194818i.A0A.A00, C194818i.A07.A00, C194818i.A09.A00};
    public static final Class A05 = C36721tc.class;
    private static volatile C36721tc A06;
    public final AnonymousClass187 A00;
    public final C04310Tq A01;
    public final C04310Tq A02;
    private final C193917y A03;

    public static final C36721tc A00(AnonymousClass1XY r8) {
        if (A06 == null) {
            synchronized (C36721tc.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        A06 = new C36721tc(AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, applicationInjector), AnonymousClass187.A01(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.B6n, applicationInjector), C193917y.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static byte[] A01(C36721tc r1, byte[] bArr, int i) {
        if (bArr == null) {
            return null;
        }
        byte[] A042 = r1.A00.A04(bArr, "pre_keys", i);
        if (A042 == null) {
            C010708t.A05(A05, "Failed to decrypt pre-key");
        }
        return A042;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0093, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0094, code lost:
        if (r3 != null) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0099, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A02(com.facebook.messaging.model.threadkey.ThreadKey r14, java.lang.String r15) {
        /*
            r13 = this;
            if (r15 != 0) goto L_0x000a
            X.17y r2 = r13.A03
            long r0 = r14.A01
            java.lang.String r15 = r2.A08(r0)
        L_0x000a:
            r4 = 0
            if (r15 != 0) goto L_0x000e
            return r4
        L_0x000e:
            X.0W6 r0 = X.C194818i.A0A
            java.lang.String r1 = r0.A00
            java.lang.String r0 = r14.A0J()
            X.0av r3 = X.C06160ax.A03(r1, r0)
            X.0W6 r0 = X.C194818i.A01
            java.lang.String r2 = r0.A00
            long r0 = r14.A01
            java.lang.String r0 = java.lang.Long.toString(r0)
            X.0av r1 = X.C06160ax.A03(r2, r0)
            X.0W6 r0 = X.C194818i.A05
            java.lang.String r0 = r0.A00
            X.0av r0 = X.C06160ax.A03(r0, r15)
            X.0av[] r0 = new X.C06140av[]{r3, r1, r0}
            X.1a6 r1 = X.C06160ax.A01(r0)
            X.0Tq r0 = r13.A02
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r5 = r0.A01()
            java.lang.String[] r7 = X.C36721tc.A04
            java.lang.String r8 = r1.A02()
            java.lang.String[] r9 = r1.A04()
            r10 = 0
            r11 = 0
            r12 = 0
            java.lang.String r6 = "thread_devices"
            android.database.Cursor r3 = r5.query(r6, r7, r8, r9, r10, r11, r12)
            boolean r0 = r3.moveToNext()     // Catch:{ all -> 0x0091 }
            if (r0 == 0) goto L_0x008d
            r1 = -1
            X.0W6 r0 = X.C194818i.A09     // Catch:{ all -> 0x0091 }
            boolean r0 = r0.A06(r3)     // Catch:{ all -> 0x0091 }
            if (r0 != 0) goto L_0x006c
            X.0W6 r0 = X.C194818i.A09     // Catch:{ all -> 0x0091 }
            int r1 = r0.A00(r3)     // Catch:{ all -> 0x0091 }
        L_0x006c:
            X.0W6 r0 = X.C194818i.A07     // Catch:{ all -> 0x0091 }
            byte[] r2 = r0.A07(r3)     // Catch:{ all -> 0x0091 }
            if (r2 != 0) goto L_0x0075
            goto L_0x0088
        L_0x0075:
            X.0Tq r0 = r13.A01     // Catch:{ all -> 0x0091 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0091 }
            X.23J r0 = (X.AnonymousClass23J) r0     // Catch:{ all -> 0x0091 }
            byte[] r1 = r0.A02(r14, r1)     // Catch:{ all -> 0x0091 }
            X.187 r0 = r13.A00     // Catch:{ all -> 0x0091 }
            java.lang.String r0 = r0.A02(r1, r2)     // Catch:{ all -> 0x0091 }
            goto L_0x0089
        L_0x0088:
            r0 = 0
        L_0x0089:
            r3.close()
            return r0
        L_0x008d:
            r3.close()
            return r4
        L_0x0091:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0093 }
        L_0x0093:
            r0 = move-exception
            if (r3 == 0) goto L_0x0099
            r3.close()     // Catch:{ all -> 0x0099 }
        L_0x0099:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36721tc.A02(com.facebook.messaging.model.threadkey.ThreadKey, java.lang.String):java.lang.String");
    }

    private C36721tc(C04310Tq r1, AnonymousClass187 r2, C04310Tq r3, C193917y r4) {
        this.A02 = r1;
        this.A00 = r2;
        this.A01 = r3;
        this.A03 = r4;
    }
}
