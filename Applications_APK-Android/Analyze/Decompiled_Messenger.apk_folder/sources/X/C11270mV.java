package X;

/* renamed from: X.0mV  reason: invalid class name and case insensitive filesystem */
public abstract class C11270mV {
    public C422428v converterInstance(C10080jW r5, Object obj) {
        if (obj != null) {
            if (obj instanceof C422428v) {
                return (C422428v) obj;
            }
            if (obj instanceof Class) {
                Class<C422228t> cls = (Class) obj;
                if (!(cls == C422328u.class || cls == C422228t.class)) {
                    if (C422428v.class.isAssignableFrom(cls)) {
                        return (C422428v) C29081fq.createInstance(cls, getConfig().canOverrideAccessModifiers());
                    }
                    throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned Class ", cls.getName(), "; expected Class<Converter>"));
                }
            } else {
                throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned Converter definition of type ", obj.getClass().getName(), "; expected type Converter or Class<Converter> instead"));
            }
        }
        return null;
    }

    public abstract C10470kA getConfig();

    public abstract C10300js getTypeFactory();

    public C25128CaN objectIdGeneratorInstance(C10080jW r3, CZj cZj) {
        return ((C25128CaN) C29081fq.createInstance(cZj._generator, getConfig().canOverrideAccessModifiers())).forScope(cZj._scope);
    }

    public C10030jR constructSpecializedType(C10030jR r2, Class cls) {
        return getConfig()._base._typeFactory.constructSpecializedType(r2, cls);
    }
}
