package X;

/* renamed from: X.0C9  reason: invalid class name */
public enum AnonymousClass0C9 {
    ACKNOWLEDGED_DELIVERY(0),
    PROCESSING_LASTACTIVE_PRESENCEINFO(1),
    EXACT_KEEPALIVE(2),
    REQUIRES_JSON_UNICODE_ESCAPES(3),
    DELTA_SENT_MESSAGE_ENABLED(4),
    USE_ENUM_TOPIC(5),
    SUPPRESS_GETDIFF_IN_CONNECT(6),
    USE_THRIFT_FOR_INBOX(7),
    USE_SEND_PINGRESP(8),
    REQUIRE_REPLAY_PROTECTION(9),
    DATA_SAVING_MODE(10),
    TYPING_OFF_WHEN_SENDING_MESSAGE(11),
    PERMISSION_USER_AUTH_CODE(12),
    FBNS_EXPLICIT_DELIVERY_ACK(13),
    IS_LARGE_PAYLOAD_SUPPORTED(14);
    
    public final byte mPosition;

    private AnonymousClass0C9(int i) {
        boolean z = true;
        AnonymousClass0A1.A01(i >= 0);
        AnonymousClass0A1.A01(i >= 64 ? false : z);
        this.mPosition = (byte) i;
    }
}
