package X;

import android.animation.StateListAnimator;
import android.content.res.TypedArray;
import android.graphics.PathEffect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.util.StatFsUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Kw  reason: invalid class name and case insensitive filesystem */
public class C22321Kw extends C31991kw implements C17470yx, Cloneable {
    private static final boolean A0m;
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public AnonymousClass0p4 A06;
    public C22571Lz A07;
    public AnonymousClass14K A08;
    public AnonymousClass10W A09;
    public List A0A;
    public Set A0B;
    public boolean[] A0C;
    private float A0D;
    private float A0E;
    private float A0F;
    private float A0G;
    private int A0H;
    private int A0I;
    private int A0J;
    private int A0K;
    private long A0L;
    private StateListAnimator A0M;
    private PathEffect A0N;
    private Drawable A0O;
    private Drawable A0P;
    private C31401jd A0Q;
    private AnonymousClass1IF A0R;
    private AnonymousClass10N A0S;
    private AnonymousClass10N A0T;
    private AnonymousClass10N A0U;
    private AnonymousClass10N A0V;
    private AnonymousClass10N A0W;
    private AnonymousClass10N A0X;
    private C28814E3p A0Y;
    private Integer A0Z;
    private String A0a;
    private String A0b;
    private String A0c;
    private ArrayList A0d;
    private ArrayList A0e;
    private ArrayList A0f;
    private List A0g;
    private boolean A0h;
    private boolean A0i;
    private boolean A0j;
    public final int[] A0k;
    private final float[] A0l;

    public C17470yx CNW() {
        this.A0j = true;
        return this;
    }

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 17) {
            z = true;
        }
        A0m = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0033, code lost:
        if (X.AnonymousClass1K2.A00(r1) == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
        if (r1 != false) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003c, code lost:
        r0 = X.AnonymousClass10G.START;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return r4.A01(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0027, code lost:
        if (r1 != false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0029, code lost:
        r0 = X.AnonymousClass10G.END;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        r1 = r4.A02(r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private float A00(X.AnonymousClass1IF r4, X.AnonymousClass10G r5) {
        /*
            r3 = this;
            X.10W r0 = r3.A09
            X.0zG r2 = r0.getLayoutDirection()
            X.0zG r0 = X.C17660zG.RTL
            r1 = 0
            if (r2 != r0) goto L_0x000c
            r1 = 1
        L_0x000c:
            int r0 = r5.ordinal()
            switch(r0) {
                case 0: goto L_0x0027;
                case 1: goto L_0x0013;
                case 2: goto L_0x003a;
                default: goto L_0x0013;
            }
        L_0x0013:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Not an horizontal padding edge: "
            r1.<init>(r0)
            r1.append(r5)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0027:
            if (r1 == 0) goto L_0x003c
        L_0x0029:
            X.10G r0 = X.AnonymousClass10G.END
        L_0x002b:
            float r1 = r4.A02(r0)
            boolean r0 = X.AnonymousClass1K2.A00(r1)
            if (r0 == 0) goto L_0x0039
            float r1 = r4.A01(r5)
        L_0x0039:
            return r1
        L_0x003a:
            if (r1 == 0) goto L_0x0029
        L_0x003c:
            X.10G r0 = X.AnonymousClass10G.START
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22321Kw.A00(X.1IF, X.10G):float");
    }

    private C28814E3p A01() {
        if (this.A0Y == null) {
            this.A0Y = new C28814E3p();
        }
        return this.A0Y;
    }

    private static C17470yx A02(AnonymousClass0p4 r4, C22321Kw r5, C17770zR r6, Set set) {
        int i;
        AnonymousClass0p4 r0 = r6.A03;
        List Ahs = r5.Ahs();
        C17770zR AoX = r5.AoX();
        if (r0 != null && AoX != null && !r5.BFx()) {
            Iterator it = Ahs.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (set.contains(((C17770zR) it.next()).A06)) {
                        break;
                    }
                } else {
                    Iterator it2 = set.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (((String) it2.next()).startsWith(AoX.A06)) {
                                i = 1;
                                break;
                            }
                        } else {
                            i = 0;
                            break;
                        }
                    }
                }
            }
        } else {
            i = 2;
        }
        if (i != 0) {
            if (i == 1) {
                return A03(r5, r6, set, 1);
            }
            if (i == 2) {
                return C31951kr.A02(r4, r6, false, true);
            }
            throw new IllegalArgumentException(AnonymousClass08S.A01(i, " is not a valid ReconciliationMode"));
        } else if (AnonymousClass07c.shouldUseDeepCloneDuringReconciliation) {
            return r5.AWl();
        } else {
            return A03(r5, r6, set, 0);
        }
    }

    private void A04(C17470yx r4) {
        if (AnonymousClass07c.isDebugModeEnabled && r4 != null) {
            AnonymousClass0p4 r2 = this.A06;
            if (r4.Ahs() != null && !r4.Ahs().isEmpty()) {
                String A012 = C49912d4.A01(r2, (C17770zR) r4.Ahs().get(0));
                C636838g r1 = (C636838g) C49912d4.A02.get(A012);
                if (r1 != null) {
                    r1.applyLayoutOverrides(A012, new C123195rC(r4));
                }
            }
            int Ah4 = r4.Ah4();
            for (int i = 0; i < Ah4; i++) {
                A04(r4.Ah3(i));
            }
            if (r4.BBY()) {
                A04(r4.AvV());
            }
        }
    }

    private void A05(C17470yx r4) {
        if (r4 != null && r4 != AnonymousClass0p4.A0F) {
            this.A09.addChildAt(r4.BAF(), this.A09.getChildCount());
        }
    }

    private void A06(AnonymousClass10G r3, boolean z) {
        if (this.A0C == null && z) {
            this.A0C = new boolean[(AnonymousClass10G.ALL.mIntValue + 1)];
        }
        boolean[] zArr = this.A0C;
        if (zArr != null) {
            zArr[r3.mIntValue] = z;
        }
    }

    private boolean A07() {
        C31401jd r0;
        if (this.A0R == null || (r0 = this.A0Q) == null || !r0.A01()) {
            return false;
        }
        return true;
    }

    /* renamed from: A09 */
    public C22321Kw AWl() {
        if (this == AnonymousClass0p4.A0F) {
            return this;
        }
        C22321Kw A082 = clone();
        AnonymousClass10W cloneWithoutChildren = this.A09.cloneWithoutChildren();
        A082.A09 = cloneWithoutChildren;
        cloneWithoutChildren.setData(A082);
        int Ah4 = Ah4();
        for (int i = 0; i < Ah4; i++) {
            A082.A05(Ah2(i).AWl());
        }
        A082.A01 = Float.NaN;
        A082.A02 = Float.NaN;
        A082.A04 = Float.NaN;
        A082.A05 = Float.NaN;
        A082.A03 = Float.NaN;
        A082.A00 = Float.NaN;
        return A082;
    }

    public void A0A(C17770zR r2) {
        this.A0A.clear();
        this.A0A.add(r2);
    }

    public void A0B(AnonymousClass0p4 r8, AnonymousClass10W r9, List list, C22571Lz r11) {
        this.A06 = r8;
        this.A09 = r9;
        r9.setData(this);
        this.A0A = list;
        this.A07 = r11;
        this.A0d = null;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            it.next();
        }
        ArrayList arrayList = this.A0f;
        this.A0f = null;
        if (arrayList != null && !arrayList.isEmpty()) {
            this.A0f = new ArrayList(arrayList.size());
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                AnonymousClass38a r5 = (AnonymousClass38a) it2.next();
                this.A0f.add(new AnonymousClass38a(r5.A02, r5.A00, r5.A01.A17(r8)));
            }
        }
    }

    public void AMg(C22571Lz r3) {
        throw new UnsupportedOperationException("DefaultInternalNode does not support this method. This is a bug. The InternalNode hierarchy is created during layout creation. If Litho is using the InternalNode tree for layout diffing then DiffNode tree creation should be skipped.");
    }

    public void ANp(C17760zQ r3) {
        if (this.A0e == null) {
            this.A0e = new ArrayList(1);
        }
        this.A0e.add(r3);
    }

    public void ANs(List list) {
        if (this.A0f == null) {
            this.A0f = new ArrayList(list.size());
        }
        this.A0f.addAll(list);
    }

    public C17470yx ANw(C14940uO r2) {
        this.A09.setAlignContent(r2);
        return this;
    }

    public C17470yx ANx(C14940uO r2) {
        this.A09.setAlignItems(r2);
        return this;
    }

    public void ANz(C14940uO r5) {
        this.A0L |= 2;
        this.A09.setAlignSelf(r5);
    }

    public void AOS(C17770zR r2) {
        this.A0A.add(r2);
    }

    public void AOU(C17770zR r2) {
        if (this.A0g == null) {
            this.A0g = new ArrayList();
        }
        this.A0g.add(r2);
    }

    public boolean AOl() {
        return this.A0h;
    }

    public void AOt(float f) {
        this.A0L |= 67108864;
        this.A09.setAspectRatio(f);
    }

    public void AOw() {
        LinkedList linkedList = null;
        if ((this.A0L & 2) != 0) {
            linkedList = new LinkedList();
            linkedList.add("alignSelf");
        }
        if ((this.A0L & 4) != 0) {
            if (linkedList == null) {
                linkedList = new LinkedList();
            }
            linkedList.add("positionType");
        }
        if ((this.A0L & 8) != 0) {
            if (linkedList == null) {
                linkedList = new LinkedList();
            }
            linkedList.add("flex");
        }
        if ((this.A0L & 16) != 0) {
            if (linkedList == null) {
                linkedList = new LinkedList();
            }
            linkedList.add("flexGrow");
        }
        if ((this.A0L & 512) != 0) {
            if (linkedList == null) {
                linkedList = new LinkedList();
            }
            linkedList.add("margin");
        }
        if (linkedList != null) {
            String join = TextUtils.join(", ", linkedList);
            Integer num = AnonymousClass07B.A00;
            C09070gU.A01(num, "DefaultInternalNode:ContextSpecificStyleSet", "You should not set " + ((Object) join) + " to a root layout in " + B5K().getClass().getSimpleName());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0023, code lost:
        if (r2.right != 0) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C17470yx API(android.graphics.drawable.Drawable r5) {
        /*
            r4 = this;
            long r2 = r4.A0L
            r0 = 262144(0x40000, double:1.295163E-318)
            long r2 = r2 | r0
            r4.A0L = r2
            r4.A0O = r5
            if (r5 == 0) goto L_0x0044
            android.graphics.Rect r2 = new android.graphics.Rect
            r2.<init>()
            r5.getPadding(r2)
            int r0 = r2.bottom
            if (r0 != 0) goto L_0x0025
            int r0 = r2.top
            if (r0 != 0) goto L_0x0025
            int r0 = r2.left
            if (r0 != 0) goto L_0x0025
            int r1 = r2.right
            r0 = 0
            if (r1 == 0) goto L_0x0026
        L_0x0025:
            r0 = 1
        L_0x0026:
            if (r0 == 0) goto L_0x0044
            X.10G r1 = X.AnonymousClass10G.LEFT
            int r0 = r2.left
            r4.BwM(r1, r0)
            X.10G r1 = X.AnonymousClass10G.TOP
            int r0 = r2.top
            r4.BwM(r1, r0)
            X.10G r1 = X.AnonymousClass10G.RIGHT
            int r0 = r2.right
            r4.BwM(r1, r0)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            int r0 = r2.bottom
            r4.BwM(r1, r0)
        L_0x0044:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22321Kw.API(android.graphics.drawable.Drawable):X.0yx");
    }

    public C17470yx AQW(AnonymousClass38W r8) {
        AnonymousClass10G r5;
        this.A0L |= 268435456;
        int length = r8.A02.length;
        for (int i = 0; i < length; i++) {
            if (i < 0 || i >= 4) {
                throw new IllegalArgumentException(AnonymousClass08S.A09("Given index out of range of acceptable edges: ", i));
            }
            if (i == 0) {
                r5 = AnonymousClass10G.LEFT;
            } else if (i == 1) {
                r5 = AnonymousClass10G.TOP;
            } else if (i == 2) {
                r5 = AnonymousClass10G.RIGHT;
            } else if (i == 3) {
                r5 = AnonymousClass10G.BOTTOM;
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A09("Given unknown edge index: ", i));
            }
            int i2 = r8.A02[i];
            AnonymousClass14K r0 = this.A08;
            if (r0 == null || !r0.A04) {
                this.A09.setBorder(r5, (float) i2);
            } else {
                AnonymousClass14K AwP = AwP();
                if (AwP.A02 == null) {
                    AwP.A02 = new AnonymousClass1IF();
                }
                AwP.A02.A03(r5, (float) i2);
            }
        }
        int[] iArr = r8.A01;
        int[] iArr2 = this.A0k;
        System.arraycopy(iArr, 0, iArr2, 0, iArr2.length);
        float[] fArr = r8.A00;
        float[] fArr2 = this.A0l;
        System.arraycopy(fArr, 0, fArr2, 0, fArr2.length);
        this.A0N = null;
        return this;
    }

    public void AQX(AnonymousClass1IF r5, int[] iArr, float[] fArr) {
        this.A0L |= 268435456;
        AnonymousClass10W r2 = this.A09;
        AnonymousClass10G r1 = AnonymousClass10G.LEFT;
        r2.setBorder(r1, r5.A02(r1));
        AnonymousClass10W r22 = this.A09;
        AnonymousClass10G r12 = AnonymousClass10G.TOP;
        r22.setBorder(r12, r5.A02(r12));
        AnonymousClass10W r23 = this.A09;
        AnonymousClass10G r13 = AnonymousClass10G.RIGHT;
        r23.setBorder(r13, r5.A02(r13));
        AnonymousClass10W r24 = this.A09;
        AnonymousClass10G r14 = AnonymousClass10G.BOTTOM;
        r24.setBorder(r14, r5.A02(r14));
        AnonymousClass10W r25 = this.A09;
        AnonymousClass10G r15 = AnonymousClass10G.A09;
        r25.setBorder(r15, r5.A02(r15));
        AnonymousClass10W r26 = this.A09;
        AnonymousClass10G r16 = AnonymousClass10G.A04;
        r26.setBorder(r16, r5.A02(r16));
        AnonymousClass10W r27 = this.A09;
        AnonymousClass10G r17 = AnonymousClass10G.START;
        r27.setBorder(r17, r5.A02(r17));
        AnonymousClass10W r28 = this.A09;
        AnonymousClass10G r18 = AnonymousClass10G.END;
        r28.setBorder(r18, r5.A02(r18));
        AnonymousClass10W r29 = this.A09;
        AnonymousClass10G r19 = AnonymousClass10G.ALL;
        r29.setBorder(r19, r5.A02(r19));
        System.arraycopy(iArr, 0, this.A0k, 0, iArr.length);
        System.arraycopy(fArr, 0, this.A0l, 0, fArr.length);
    }

    public C17470yx ASH(C17770zR r3) {
        if (r3 == null) {
            return this;
        }
        A05(C31951kr.A02(this.A06, r3, false, false));
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00f4, code lost:
        if (r1[r2.mIntValue] == false) goto L_0x00f6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AUX(java.lang.Object r8) {
        /*
            r7 = this;
            X.0yx r8 = (X.C17470yx) r8
            X.0yx r0 = X.AnonymousClass0p4.A0F
            if (r8 == r0) goto L_0x018a
            X.1jd r1 = r7.A0Q
            if (r1 == 0) goto L_0x0013
            X.1jd r0 = r8.Avp()
            if (r0 != 0) goto L_0x0104
            r8.C9r(r1)
        L_0x0013:
            boolean r0 = r8.BFZ()
            if (r0 == 0) goto L_0x0020
            X.0zG r0 = r7.B16()
            r8.BHv(r0)
        L_0x0020:
            boolean r0 = r8.BFL()
            if (r0 == 0) goto L_0x002b
            int r0 = r7.A0H
            r8.BCN(r0)
        L_0x002b:
            long r2 = r7.A0L
            r0 = 256(0x100, double:1.265E-321)
            long r2 = r2 & r0
            r5 = 0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x003b
            boolean r0 = r7.A0i
            r8.AY1(r0)
        L_0x003b:
            long r2 = r7.A0L
            r0 = 262144(0x40000, double:1.295163E-318)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x004a
            android.graphics.drawable.Drawable r0 = r7.A0O
            r8.API(r0)
        L_0x004a:
            long r2 = r7.A0L
            r0 = 524288(0x80000, double:2.590327E-318)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0059
            android.graphics.drawable.Drawable r0 = r7.A0P
            r8.Aac(r0)
        L_0x0059:
            boolean r0 = r7.A0j
            if (r0 == 0) goto L_0x0060
            r8.CNW()
        L_0x0060:
            long r2 = r7.A0L
            r0 = 1048576(0x100000, double:5.180654E-318)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x006f
            X.10N r0 = r7.A0X
            r8.CLi(r0)
        L_0x006f:
            long r2 = r7.A0L
            r0 = 2097152(0x200000, double:1.0361308E-317)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x007e
            X.10N r0 = r7.A0S
            r8.AaM(r0)
        L_0x007e:
            long r2 = r7.A0L
            r0 = 4194304(0x400000, double:2.0722615E-317)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x008d
            X.10N r0 = r7.A0T
            r8.Aaj(r0)
        L_0x008d:
            long r2 = r7.A0L
            r0 = 8388608(0x800000, double:4.144523E-317)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x009c
            X.10N r0 = r7.A0U
            r8.BDS(r0)
        L_0x009c:
            long r2 = r7.A0L
            r0 = 16777216(0x1000000, double:8.289046E-317)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00ab
            X.10N r0 = r7.A0V
            r8.CJe(r0)
        L_0x00ab:
            long r2 = r7.A0L
            r0 = 2147483648(0x80000000, double:1.0609978955E-314)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00bc
            X.10N r0 = r7.A0W
            r8.CLh(r0)
        L_0x00bc:
            java.lang.String r0 = r7.A0a
            if (r0 == 0) goto L_0x00c3
            r8.CIv(r0)
        L_0x00c3:
            long r2 = r7.A0L
            r0 = 1024(0x400, double:5.06E-321)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0115
            X.14K r0 = r7.A08
            if (r0 == 0) goto L_0x010d
            X.1IF r0 = r0.A03
            if (r0 == 0) goto L_0x010d
            r4 = 0
        L_0x00d5:
            int r0 = X.AnonymousClass1IF.A03
            if (r4 >= r0) goto L_0x0115
            X.14K r0 = r7.A08
            X.1IF r0 = r0.A03
            float r3 = r0.A00(r4)
            boolean r0 = X.AnonymousClass1K2.A00(r3)
            if (r0 != 0) goto L_0x00fc
            X.10G r2 = X.AnonymousClass10G.A00(r4)
            boolean[] r1 = r7.A0C
            if (r1 == 0) goto L_0x00f6
            int r0 = r2.mIntValue
            boolean r1 = r1[r0]
            r0 = 1
            if (r1 != 0) goto L_0x00f7
        L_0x00f6:
            r0 = 0
        L_0x00f7:
            if (r0 == 0) goto L_0x00ff
            r8.BwL(r2, r3)
        L_0x00fc:
            int r4 = r4 + 1
            goto L_0x00d5
        L_0x00ff:
            int r0 = (int) r3
            r8.BwM(r2, r0)
            goto L_0x00fc
        L_0x0104:
            X.1jd r0 = r8.AwQ()
            r1.A00(r0)
            goto L_0x0013
        L_0x010d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "copyInto() must be used when resolving a nestedTree. If padding was set on the holder node, we must have a mNestedTreePadding instance"
            r1.<init>(r0)
            throw r1
        L_0x0115:
            long r2 = r7.A0L
            r0 = 268435456(0x10000000, double:1.32624737E-315)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x012e
            X.14K r0 = r7.A08
            if (r0 == 0) goto L_0x0182
            X.1IF r2 = r0.A02
            if (r2 == 0) goto L_0x0182
            int[] r1 = r7.A0k
            float[] r0 = r7.A0l
            r8.AQX(r2, r1, r0)
        L_0x012e:
            long r2 = r7.A0L
            r0 = 134217728(0x8000000, double:6.63123685E-316)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x013f
            java.lang.String r1 = r7.A0b
            java.lang.String r0 = r7.A0c
            r8.CJN(r1, r0)
        L_0x013f:
            long r2 = r7.A0L
            r0 = 4294967296(0x100000000, double:2.121995791E-314)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0150
            java.lang.Integer r0 = r7.A0Z
            r8.CJO(r0)
        L_0x0150:
            float r1 = r7.A0F
            r2 = 0
            int r0 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x015a
            r8.CLj(r1)
        L_0x015a:
            float r1 = r7.A0G
            int r0 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0163
            r8.CLk(r1)
        L_0x0163:
            long r2 = r7.A0L
            r0 = 536870912(0x20000000, double:2.652494739E-315)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0172
            android.animation.StateListAnimator r0 = r7.A0M
            r8.CHg(r0)
        L_0x0172:
            long r2 = r7.A0L
            r0 = 1073741824(0x40000000, double:5.304989477E-315)
            long r2 = r2 & r0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x018a
            int r0 = r7.A0K
            r8.CHh(r0)
            return
        L_0x0182:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "copyInto() must be used when resolving a nestedTree.If border width was set on the holder node, we must have a mNestedTreeBorderWidth instance"
            r1.<init>(r0)
            throw r1
        L_0x018a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22321Kw.AUX(java.lang.Object):void");
    }

    public C17470yx AY1(boolean z) {
        this.A0L |= 256;
        this.A0i = z;
        return this;
    }

    public void Aa6(float f) {
        this.A0L |= 8;
        this.A09.setFlex(f);
    }

    public void Aa7(float f) {
        this.A0L |= 64;
        this.A09.setFlexBasisPercent(f);
    }

    public void Aa8(int i) {
        this.A0L |= 64;
        this.A09.setFlexBasis((float) i);
    }

    public C17470yx Aa9(AnonymousClass10X r2) {
        this.A09.setFlexDirection(r2);
        return this;
    }

    public void AaB(float f) {
        this.A0L |= 16;
        this.A09.setFlexGrow(f);
    }

    public void AaD(float f) {
        this.A0L |= 32;
        this.A09.setFlexShrink(f);
    }

    public C17470yx AaM(AnonymousClass10N r5) {
        this.A0L |= 2097152;
        AnonymousClass10N r1 = this.A0S;
        if (r1 != null) {
            if (r5 == null) {
                r5 = r1;
            } else {
                r5 = new C1299767b(r1, r5);
            }
        }
        this.A0S = r5;
        return this;
    }

    public C17470yx Aac(Drawable drawable) {
        this.A0L |= 524288;
        this.A0P = drawable;
        return this;
    }

    public C17470yx Aaj(AnonymousClass10N r5) {
        this.A0L |= 4194304;
        AnonymousClass10N r1 = this.A0T;
        if (r1 != null) {
            if (r5 == null) {
                r5 = r1;
            } else {
                r5 = new C1299767b(r1, r5);
            }
        }
        this.A0T = r5;
        return this;
    }

    public Drawable Ads() {
        return this.A0O;
    }

    public C17730zN Adz() {
        C28814E3p e3p = this.A0Y;
        if (e3p != null) {
            return e3p.A00;
        }
        return null;
    }

    public int[] Aeu() {
        return this.A0k;
    }

    public C17730zN Aev() {
        C28814E3p e3p = this.A0Y;
        if (e3p != null) {
            return e3p.A01;
        }
        return null;
    }

    public PathEffect Aew() {
        return this.A0N;
    }

    public float[] Aex() {
        return this.A0l;
    }

    /* renamed from: Ah3 */
    public C17470yx Ah2(int i) {
        return (C17470yx) this.A09.getChildAt(i).getData();
    }

    public int Ah4() {
        return this.A09.getChildCount();
    }

    public List Ahs() {
        return this.A0A;
    }

    public ArrayList Ahu() {
        return this.A0d;
    }

    public C17730zN AiP() {
        C28814E3p e3p = this.A0Y;
        if (e3p != null) {
            return e3p.A02;
        }
        return null;
    }

    public AnonymousClass0p4 AiS() {
        return this.A06;
    }

    public C22571Lz AkV() {
        return this.A07;
    }

    public AnonymousClass10N AnI() {
        return this.A0S;
    }

    public Drawable AnL() {
        return this.A0P;
    }

    public C17730zN AnN() {
        C28814E3p e3p = this.A0Y;
        if (e3p != null) {
            return e3p.A03;
        }
        return null;
    }

    public AnonymousClass10N Anq() {
        return this.A0T;
    }

    public C17770zR AoX() {
        if (this.A0A.isEmpty()) {
            return null;
        }
        List list = this.A0A;
        return (C17770zR) list.get(list.size() - 1);
    }

    public int Apg() {
        return this.A0H;
    }

    public AnonymousClass10N Aqk() {
        return this.A0U;
    }

    public int Ary() {
        return this.A0I;
    }

    public float As1() {
        return this.A0D;
    }

    public float As2() {
        return this.A0E;
    }

    public int AsD() {
        return this.A0J;
    }

    public int AsF(AnonymousClass10G r2) {
        return AnonymousClass1K3.A00(this.A09.getLayoutBorder(r2));
    }

    public C17470yx AvV() {
        AnonymousClass14K r0 = this.A08;
        if (r0 != null) {
            return r0.A00;
        }
        return null;
    }

    public C31401jd Avp() {
        return this.A0Q;
    }

    public AnonymousClass14K AwP() {
        if (this.A08 == null) {
            this.A08 = new AnonymousClass14K();
        }
        return this.A08;
    }

    public C31401jd AwQ() {
        if (this.A0Q == null) {
            this.A0Q = new C31401jd();
        }
        return this.A0Q;
    }

    public int Aws() {
        return AnonymousClass1K3.A00(this.A09.getLayoutPadding(AnonymousClass10G.BOTTOM));
    }

    public int Awt() {
        return AnonymousClass1K3.A00(this.A09.getLayoutPadding(AnonymousClass10G.LEFT));
    }

    public int Awu() {
        return AnonymousClass1K3.A00(this.A09.getLayoutPadding(AnonymousClass10G.RIGHT));
    }

    public int Awv() {
        return AnonymousClass1K3.A00(this.A09.getLayoutPadding(AnonymousClass10G.TOP));
    }

    public C17470yx AxD() {
        AnonymousClass10W r1 = this.A09;
        if (r1 == null || r1.getOwner() == null) {
            return null;
        }
        return (C17470yx) r1.getOwner().getData();
    }

    public AnonymousClass1KE Axp() {
        AnonymousClass14K r0 = this.A08;
        if (r0 != null) {
            return r0.A01;
        }
        return null;
    }

    public C17660zG B16() {
        return this.A09.getLayoutDirection();
    }

    public StateListAnimator B3m() {
        return this.A0M;
    }

    public int B3n() {
        return this.A0K;
    }

    public C17660zG B4P() {
        return this.A09.getStyleDirection();
    }

    public float B4Q() {
        return this.A09.getHeight().A00;
    }

    public float B4V() {
        return this.A09.getWidth().A00;
    }

    public C17770zR B5K() {
        if (this.A0A.isEmpty()) {
            return null;
        }
        return (C17770zR) this.A0A.get(0);
    }

    public String B5X() {
        return this.A0a;
    }

    public String B73() {
        return this.A0b;
    }

    public Integer B74() {
        return this.A0Z;
    }

    public String B75() {
        return this.A0c;
    }

    public ArrayList B76() {
        return this.A0e;
    }

    public AnonymousClass10N B7X() {
        return this.A0V;
    }

    public List B7d() {
        return this.A0g;
    }

    public AnonymousClass10N B9W() {
        return this.A0W;
    }

    public AnonymousClass10N B9Y() {
        return this.A0X;
    }

    public float B9Z() {
        return this.A0F;
    }

    public float B9b() {
        return this.A0G;
    }

    public ArrayList BA2() {
        return this.A0f;
    }

    public int BA8() {
        if (AnonymousClass1K2.A00(this.A04)) {
            this.A04 = this.A09.getLayoutX();
        }
        return (int) this.A04;
    }

    public int BAE() {
        if (AnonymousClass1K2.A00(this.A05)) {
            this.A05 = this.A09.getLayoutY();
        }
        return (int) this.A05;
    }

    public AnonymousClass10W BAF() {
        return this.A09;
    }

    public boolean BBY() {
        AnonymousClass14K r0 = this.A08;
        if (r0 == null || r0.A00 == null) {
            return false;
        }
        return true;
    }

    public boolean BBZ() {
        return this.A09.hasNewLayout();
    }

    public boolean BBs() {
        if ((this.A0L & StatFsUtil.IN_GIGA_BYTE) != 0) {
            return true;
        }
        return false;
    }

    public boolean BBu() {
        if ((this.A0L & 33554432) != 0) {
            return true;
        }
        return false;
    }

    public boolean BBv() {
        return !TextUtils.isEmpty(this.A0b);
    }

    public boolean BBx() {
        if (this.A0X == null && this.A0S == null && this.A0V == null && this.A0T == null && this.A0U == null && this.A0W == null) {
            return false;
        }
        return true;
    }

    public void BC1(float f) {
        this.A0L |= 32768;
        this.A09.setHeightPercent(f);
    }

    public void BC2(int i) {
        this.A0L |= 32768;
        this.A09.setHeight((float) i);
    }

    public C17470yx BCN(int i) {
        this.A0L |= 128;
        this.A0H = i;
        return this;
    }

    public C17470yx BDS(AnonymousClass10N r5) {
        this.A0L |= ErrorReporter.NATIVE_MAX_REPORT_SIZE;
        AnonymousClass10N r1 = this.A0U;
        if (r1 != null) {
            if (r5 == null) {
                r5 = r1;
            } else {
                r5 = new C1299767b(r1, r5);
            }
        }
        this.A0U = r5;
        return this;
    }

    public boolean BER() {
        return this.A0i;
    }

    public boolean BF5() {
        return this.A0j;
    }

    public boolean BFL() {
        if ((this.A0L & 128) == 0 || this.A0H == 0) {
            return true;
        }
        return false;
    }

    public boolean BFQ() {
        if (this.A09 == null || this.A06 == null) {
            return false;
        }
        return true;
    }

    public boolean BFZ() {
        if ((this.A0L & 1) == 0 || B16() == C17660zG.INHERIT) {
            return true;
        }
        return false;
    }

    public boolean BFx() {
        AnonymousClass14K r0 = this.A08;
        if (r0 == null || !r0.A04) {
            return false;
        }
        return true;
    }

    public boolean BG6() {
        if ((this.A0L & StatFsUtil.IN_KILO_BYTE) != 0) {
            return true;
        }
        return false;
    }

    public void BGL(boolean z) {
        this.A09.setIsReferenceBaseline(z);
    }

    public C17470yx BHb(C14950uP r2) {
        this.A09.setJustifyContent(r2);
        return this;
    }

    public void BHv(C17660zG r5) {
        this.A0L |= 1;
        this.A09.setDirection(r5);
    }

    public void BJt(AnonymousClass10G r5) {
        this.A0L |= 512;
        this.A09.setMarginAuto(r5);
    }

    public void BJv(AnonymousClass10G r5, float f) {
        this.A0L |= 512;
        this.A09.setMarginPercent(r5, f);
    }

    public void BJx(AnonymousClass10G r5, int i) {
        this.A0L |= 512;
        this.A09.setMargin(r5, (float) i);
    }

    public void BK0() {
        this.A09.markLayoutSeen();
    }

    public void BKG(float f) {
        this.A0L |= 131072;
        this.A09.setMaxHeightPercent(f);
    }

    public void BKH(int i) {
        this.A0L |= 131072;
        this.A09.setMaxHeight((float) i);
    }

    public void BKK(float f) {
        this.A0L |= 16384;
        this.A09.setMaxWidthPercent(f);
    }

    public void BKL(int i) {
        this.A0L |= 16384;
        this.A09.setMaxWidth((float) i);
    }

    public void BL7(float f) {
        this.A0L |= 65536;
        this.A09.setMinHeightPercent(f);
    }

    public void BL8(int i) {
        this.A0L |= 65536;
        this.A09.setMinHeight((float) i);
    }

    public void BL9(float f) {
        this.A0L |= 8192;
        this.A09.setMinWidthPercent(f);
    }

    public void BLA(int i) {
        this.A0L |= 8192;
        this.A09.setMinWidth((float) i);
    }

    public void BwL(AnonymousClass10G r5, float f) {
        this.A0L |= StatFsUtil.IN_KILO_BYTE;
        AnonymousClass14K r0 = this.A08;
        if (r0 == null || !r0.A04) {
            this.A09.setPaddingPercent(r5, f);
            return;
        }
        AnonymousClass14K AwP = AwP();
        if (AwP.A03 == null) {
            AwP.A03 = new AnonymousClass1IF();
        }
        AwP.A03.A03(r5, f);
        A06(r5, true);
    }

    public void BwM(AnonymousClass10G r5, int i) {
        this.A0L |= StatFsUtil.IN_KILO_BYTE;
        AnonymousClass14K r0 = this.A08;
        if (r0 == null || !r0.A04) {
            this.A09.setPadding(r5, (float) i);
            return;
        }
        AnonymousClass14K AwP = AwP();
        if (AwP.A03 == null) {
            AwP.A03 = new AnonymousClass1IF();
        }
        AwP.A03.A03(r5, (float) i);
        A06(r5, false);
    }

    public void BxH(AnonymousClass10G r5, float f) {
        this.A0L |= 2048;
        this.A09.setPositionPercent(r5, f);
    }

    public void BxI(AnonymousClass10G r5, int i) {
        this.A0L |= 2048;
        this.A09.setPosition(r5, (float) i);
    }

    public void BxJ(AnonymousClass1LC r5) {
        this.A0L |= 4;
        this.A09.setPositionType(r5);
    }

    public C17470yx Bzu(AnonymousClass0p4 r4, C17770zR r5) {
        Set hashSet;
        AnonymousClass1JC r2 = r4.A0C;
        if (r2 == null) {
            hashSet = Collections.emptySet();
        } else {
            synchronized (r2) {
                hashSet = new HashSet();
                Map map = r2.A02;
                if (map != null) {
                    hashSet.addAll(map.keySet());
                }
                Map map2 = r2.A07;
                if (map2 != null) {
                    hashSet.addAll(map2.keySet());
                }
            }
        }
        return A02(r4, this, r5, hashSet);
    }

    public C17660zG C0A() {
        AnonymousClass10W r2 = this.A09;
        while (r2 != null && r2.getLayoutDirection() == C17660zG.INHERIT) {
            r2 = r2.getOwner();
        }
        if (r2 == null) {
            return C17660zG.INHERIT;
        }
        return r2.getLayoutDirection();
    }

    public void C0S(C49912d4 r2) {
        if (this.A0B == null) {
            this.A0B = new HashSet();
        }
        this.A0B.add(r2);
    }

    public void C70(C17770zR r3) {
        throw new UnsupportedOperationException("DefaultInternalNode does not support this method. This is a bug. The InternalNode hierarchy is created during layout creation. If Litho is using the InternalNode tree for layout diffing then DiffNode tree creation should be skipped.");
    }

    public void C7h(C22571Lz r3) {
        if (r3 instanceof C17470yx) {
            C17470yx r1 = (C17470yx) r3;
            if (r1.BFx()) {
                this.A07 = r1.AvV();
                return;
            }
        }
        this.A07 = r3;
    }

    public void C9P(AnonymousClass119 r2) {
        this.A09.setMeasureFunction(r2);
    }

    public void C9n(C17470yx r2) {
        if (r2 != AnonymousClass0p4.A0F) {
            r2.AwP();
        }
        AwP().A00 = r2;
    }

    public boolean CDz() {
        boolean z;
        int[] iArr = this.A0k;
        int length = iArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (iArr[i] != 0) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            return false;
        }
        AnonymousClass10W r2 = this.A09;
        if (r2.getLayoutBorder(AnonymousClass10G.LEFT) == 0.0f && r2.getLayoutBorder(AnonymousClass10G.TOP) == 0.0f && r2.getLayoutBorder(AnonymousClass10G.RIGHT) == 0.0f && r2.getLayoutBorder(AnonymousClass10G.BOTTOM) == 0.0f) {
            return false;
        }
        return true;
    }

    public C17470yx CHg(StateListAnimator stateListAnimator) {
        this.A0L |= 536870912;
        this.A0M = stateListAnimator;
        CNW();
        return this;
    }

    public C17470yx CHh(int i) {
        this.A0L |= StatFsUtil.IN_GIGA_BYTE;
        this.A0K = i;
        CNW();
        return this;
    }

    public C17470yx CJD(AnonymousClass10G r5, int i) {
        if (this.A0R == null) {
            this.A0R = new AnonymousClass1IF();
        }
        this.A0L |= 33554432;
        this.A0R.A03(r5, (float) i);
        return this;
    }

    public C17470yx CJN(String str, String str2) {
        if (Build.VERSION.SDK_INT >= 14 && !TextUtils.isEmpty(str)) {
            this.A0L |= 134217728;
            this.A0b = str;
            this.A0c = str2;
        }
        return this;
    }

    public C17470yx CJO(Integer num) {
        this.A0L |= 4294967296L;
        this.A0Z = num;
        return this;
    }

    public C17470yx CJe(AnonymousClass10N r5) {
        this.A0L |= 16777216;
        AnonymousClass10N r1 = this.A0V;
        if (r1 != null) {
            if (r5 == null) {
                r5 = r1;
            } else {
                r5 = new C1299767b(r1, r5);
            }
        }
        this.A0V = r5;
        return this;
    }

    public void CLJ(boolean z) {
        if (z) {
            this.A09.setBaselineFunction(new C132626In());
        }
    }

    public C17470yx CLh(AnonymousClass10N r5) {
        this.A0L |= 2147483648L;
        AnonymousClass10N r1 = this.A0W;
        if (r1 != null) {
            if (r5 == null) {
                r5 = r1;
            } else {
                r5 = new C1299767b(r1, r5);
            }
        }
        this.A0W = r5;
        return this;
    }

    public C17470yx CLi(AnonymousClass10N r5) {
        this.A0L |= StatFsUtil.IN_MEGA_BYTE;
        AnonymousClass10N r1 = this.A0X;
        if (r1 != null) {
            if (r5 == null) {
                r5 = r1;
            } else {
                r5 = new C1299767b(r1, r5);
            }
        }
        this.A0X = r5;
        return this;
    }

    public void CNJ(float f) {
        this.A0L |= 4096;
        this.A09.setWidthPercent(f);
    }

    public void CNK(int i) {
        this.A0L |= 4096;
        this.A09.setWidth((float) i);
    }

    public C17470yx CNU(AnonymousClass6I0 r2) {
        this.A09.setWrap(r2);
        return this;
    }

    public int getHeight() {
        if (AnonymousClass1K2.A00(this.A00)) {
            this.A00 = this.A09.getLayoutHeight();
        }
        return (int) this.A00;
    }

    public String getSimpleName() {
        if (this.A0A.isEmpty()) {
            return "<null>";
        }
        return ((C17770zR) this.A0A.get(0)).A1A();
    }

    public int getWidth() {
        if (AnonymousClass1K2.A00(this.A03)) {
            this.A03 = this.A09.getLayoutWidth();
        }
        return (int) this.A03;
    }

    private static C17470yx A03(C22321Kw r10, C17770zR r11, Set set, int i) {
        C17470yx A022;
        String str;
        boolean A023 = C27041cY.A02();
        if (A023) {
            if (i == 0) {
                str = "copy:";
            } else {
                str = "reconcile:";
            }
            C27041cY.A01(AnonymousClass08S.A0J(str, r11.A1A()));
        }
        AnonymousClass10W BAF = r10.BAF();
        if (A023) {
            C27041cY.A01(AnonymousClass08S.A0J("cloneYogaNode:", r11.A1A()));
        }
        AnonymousClass10W cloneWithoutChildren = BAF.cloneWithoutChildren();
        if (A023) {
            C27041cY.A00();
        }
        boolean A024 = C27041cY.A02();
        if (A024) {
            C27041cY.A01(AnonymousClass08S.A0J("clone:", r11.A1A()));
        }
        C22321Kw A082 = r10.clone();
        if (A024) {
            C27041cY.A00();
            C27041cY.A01(AnonymousClass08S.A0J("clean:", r11.A1A()));
        }
        A082.A0A = new ArrayList();
        A082.A07 = null;
        A082.A0B = null;
        A082.A01 = Float.NaN;
        A082.A02 = Float.NaN;
        A082.A04 = Float.NaN;
        A082.A05 = Float.NaN;
        A082.A03 = Float.NaN;
        A082.A00 = Float.NaN;
        if (A024) {
            C27041cY.A00();
            C27041cY.A01(AnonymousClass08S.A0J("update:", r11.A1A()));
        }
        int size = r10.A0A.size();
        ArrayList arrayList = new ArrayList(size);
        arrayList.add(r11);
        AnonymousClass0p4 r2 = r11.A03;
        for (int i2 = size - 2; i2 >= 0; i2--) {
            C17770zR A17 = ((C17770zR) r10.A0A.get(i2)).A17(r2);
            arrayList.add(A17);
            r2 = A17.A03;
        }
        Collections.reverse(arrayList);
        A082.A0B(r11.A03, cloneWithoutChildren, arrayList, null);
        if (A024) {
            C27041cY.A00();
        }
        AnonymousClass0p4 r8 = A082.B5K().A03;
        if (A082.AvV() != null) {
            A082.AwP().A00 = null;
        }
        int childCount = BAF.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            C22321Kw r22 = (C22321Kw) BAF.getChildAt(i3).getData();
            List Ahs = r22.Ahs();
            C17770zR A172 = ((C17770zR) Ahs.get(Math.max(0, Ahs.size() - 1))).A17(r8);
            if (i == 0) {
                A022 = A03(r22, A172, set, 0);
            } else {
                A022 = A02(r8, r22, A172, set);
            }
            A082.A05(A022);
        }
        if (A023) {
            C27041cY.A00();
        }
        return A082;
    }

    /* renamed from: A08 */
    public C22321Kw clone() {
        try {
            return (C22321Kw) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void AOc(TypedArray typedArray) {
        int i;
        AnonymousClass10G r1;
        AnonymousClass1LC r0;
        C14950uP r02;
        AnonymousClass6I0 r03;
        AnonymousClass10G r12;
        AnonymousClass10G r13;
        int indexCount = typedArray.getIndexCount();
        for (int i2 = 0; i2 < indexCount; i2++) {
            int index = typedArray.getIndex(i2);
            if (index == 7) {
                int layoutDimension = typedArray.getLayoutDimension(index, -1);
                if (layoutDimension >= 0) {
                    CNK(layoutDimension);
                }
            } else if (index == 8) {
                int layoutDimension2 = typedArray.getLayoutDimension(index, -1);
                if (layoutDimension2 >= 0) {
                    BC2(layoutDimension2);
                }
            } else if (index == 16) {
                BL8(typedArray.getDimensionPixelSize(index, 0));
            } else if (index == 15) {
                BLA(typedArray.getDimensionPixelSize(index, 0));
            } else {
                if (index == 2) {
                    r13 = AnonymousClass10G.LEFT;
                } else if (index == 3) {
                    r13 = AnonymousClass10G.TOP;
                } else if (index == 4) {
                    r13 = AnonymousClass10G.RIGHT;
                } else if (index == 5) {
                    r13 = AnonymousClass10G.BOTTOM;
                } else if (index == 19 && A0m) {
                    r13 = AnonymousClass10G.START;
                } else if (index == 20 && A0m) {
                    r13 = AnonymousClass10G.END;
                } else if (index == 1) {
                    r13 = AnonymousClass10G.ALL;
                } else {
                    if (index == 10) {
                        r12 = AnonymousClass10G.LEFT;
                    } else if (index == 11) {
                        r12 = AnonymousClass10G.TOP;
                    } else if (index == 12) {
                        r12 = AnonymousClass10G.RIGHT;
                    } else if (index == 13) {
                        r12 = AnonymousClass10G.BOTTOM;
                    } else if (index == 21 && A0m) {
                        r12 = AnonymousClass10G.START;
                    } else if (index == 22 && A0m) {
                        r12 = AnonymousClass10G.END;
                    } else if (index == 9) {
                        r12 = AnonymousClass10G.ALL;
                    } else if (index == 18 && Build.VERSION.SDK_INT >= 16) {
                        BCN(typedArray.getInt(index, 0));
                    } else if (index == 6) {
                        AY1(typedArray.getBoolean(index, false));
                    } else if (index == 0) {
                        if (C1753887e.A00(typedArray, 0)) {
                            API(C16990y9.A00(typedArray.getColor(index, 0)));
                        } else {
                            int resourceId = typedArray.getResourceId(index, -1);
                            if (resourceId == 0) {
                                API(null);
                            } else {
                                API(AnonymousClass01R.A03(this.A06.A09, resourceId));
                            }
                        }
                    } else if (index == 14) {
                        if (C1753887e.A00(typedArray, 14)) {
                            Aac(C16990y9.A00(typedArray.getColor(index, 0)));
                        } else {
                            int resourceId2 = typedArray.getResourceId(index, -1);
                            if (resourceId2 == 0) {
                                Aac(null);
                            } else {
                                Aac(AnonymousClass01R.A03(this.A06.A09, resourceId2));
                            }
                        }
                    } else if (index == 17) {
                        C31401jd AwQ = AwQ();
                        String string = typedArray.getString(index);
                        AwQ.A0A |= 1;
                        AwQ.A0S = string;
                    } else if (index == 27) {
                        Aa9(AnonymousClass10X.A00(typedArray.getInteger(index, 0)));
                    } else {
                        if (index == 34) {
                            i = typedArray.getInteger(index, 0);
                            if (i == 0) {
                                r03 = AnonymousClass6I0.NO_WRAP;
                            } else if (i == 1) {
                                r03 = AnonymousClass6I0.WRAP;
                            } else if (i == 2) {
                                r03 = AnonymousClass6I0.WRAP_REVERSE;
                            }
                            CNU(r03);
                        } else if (index == 28) {
                            i = typedArray.getInteger(index, 0);
                            if (i == 0) {
                                r02 = C14950uP.FLEX_START;
                            } else if (i == 1) {
                                r02 = C14950uP.CENTER;
                            } else if (i == 2) {
                                r02 = C14950uP.FLEX_END;
                            } else if (i == 3) {
                                r02 = C14950uP.SPACE_BETWEEN;
                            } else if (i == 4) {
                                r02 = C14950uP.SPACE_AROUND;
                            } else if (i == 5) {
                                r02 = C14950uP.SPACE_EVENLY;
                            }
                            BHb(r02);
                        } else if (index == 24) {
                            ANx(C14940uO.A00(typedArray.getInteger(index, 0)));
                        } else if (index == 25) {
                            ANz(C14940uO.A00(typedArray.getInteger(index, 0)));
                        } else if (index == 31) {
                            i = typedArray.getInteger(index, 0);
                            if (i == 0) {
                                r0 = AnonymousClass1LC.A01;
                            } else if (i == 1) {
                                r0 = AnonymousClass1LC.A00;
                            }
                            BxJ(r0);
                        } else if (index == 23) {
                            float f = typedArray.getFloat(index, -1.0f);
                            if (f >= 0.0f) {
                                Aa6(f);
                            }
                        } else {
                            if (index == 30) {
                                r1 = AnonymousClass10G.LEFT;
                            } else if (index == 33) {
                                r1 = AnonymousClass10G.TOP;
                            } else if (index == 32) {
                                r1 = AnonymousClass10G.RIGHT;
                            } else if (index == 26) {
                                r1 = AnonymousClass10G.BOTTOM;
                            } else if (index == 29) {
                                BHv(C17660zG.A00(typedArray.getInteger(index, -1)));
                            }
                            BxI(r1, typedArray.getDimensionPixelOffset(index, 0));
                        }
                        throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown enum value: ", i));
                    }
                    BJx(r12, typedArray.getDimensionPixelOffset(index, 0));
                }
                BwM(r13, typedArray.getDimensionPixelOffset(index, 0));
            }
        }
    }

    public void AR7(float f, float f2) {
        A04(this);
        this.A09.calculateLayout(f, f2);
    }

    public C17770zR Ahm() {
        return B5K();
    }

    public int B6k() {
        if (!A07()) {
            return 0;
        }
        return AnonymousClass1K3.A00(this.A0R.A01(AnonymousClass10G.BOTTOM));
    }

    public int B6l() {
        if (!A07()) {
            return 0;
        }
        if (AnonymousClass1K2.A00(this.A01)) {
            this.A01 = A00(this.A0R, AnonymousClass10G.LEFT);
        }
        return AnonymousClass1K3.A00(this.A01);
    }

    public int B6m() {
        if (!A07()) {
            return 0;
        }
        if (AnonymousClass1K2.A00(this.A02)) {
            this.A02 = A00(this.A0R, AnonymousClass10G.RIGHT);
        }
        return AnonymousClass1K3.A00(this.A02);
    }

    public int B6n() {
        if (!A07()) {
            return 0;
        }
        return AnonymousClass1K3.A00(this.A0R.A01(AnonymousClass10G.TOP));
    }

    public void BJz(AnonymousClass1KE r3) {
        AnonymousClass1KE A002;
        AwP().A04 = true;
        AnonymousClass14K AwP = AwP();
        if (r3 == null) {
            A002 = null;
        } else {
            A002 = AnonymousClass1KE.A00(r3);
        }
        AwP.A01 = A002;
    }

    public void C6H(C17730zN r2) {
        A01().A00 = r2;
    }

    public void C6O(C17730zN r2) {
        A01().A01 = r2;
    }

    public void C78(C17730zN r2) {
        A01().A02 = r2;
    }

    public void C84(C17730zN r2) {
        A01().A03 = r2;
    }

    public void CC0(int i) {
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE) {
            this.A09.setMaxHeight((float) View.MeasureSpec.getSize(i));
        } else if (mode == 0) {
            this.A09.setHeight(Float.NaN);
        } else if (mode == 1073741824) {
            this.A09.setHeight((float) View.MeasureSpec.getSize(i));
        }
    }

    public void CC1(int i) {
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE) {
            this.A09.setMaxWidth((float) View.MeasureSpec.getSize(i));
        } else if (mode == 0) {
            this.A09.setWidth(Float.NaN);
        } else if (mode == 1073741824) {
            this.A09.setWidth((float) View.MeasureSpec.getSize(i));
        }
    }

    public void C6X(boolean z) {
        this.A0h = z;
    }

    public void C8J(C17730zN r1) {
        A01();
    }

    public void C8l(int i) {
        this.A0I = i;
    }

    public void C8m(float f) {
        this.A0D = f;
    }

    public void C8n(float f) {
        this.A0E = f;
    }

    public void C8r(int i) {
        this.A0J = i;
    }

    public void C9r(C31401jd r1) {
        this.A0Q = r1;
    }

    public void CDF(AnonymousClass117 r1) {
        A01();
    }

    public C17470yx CIv(String str) {
        this.A0a = str;
        return this;
    }

    public C17470yx CLj(float f) {
        this.A0F = f;
        return this;
    }

    public C17470yx CLk(float f) {
        this.A0G = f;
        return this;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C22321Kw(X.AnonymousClass0p4 r4) {
        /*
            r3 = this;
            r2 = 1
            r1 = 0
            if (r1 == 0) goto L_0x000e
            X.1Ru r0 = X.AnonymousClass1KO.A00
            X.10W r0 = r1.create(r0)
        L_0x000a:
            r3.<init>(r4, r0, r2)
            return
        L_0x000e:
            X.1Ru r1 = X.AnonymousClass1KO.A00
            X.1K4 r0 = new X.1K4
            r0.<init>(r1)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22321Kw.<init>(X.0p4):void");
    }

    private C22321Kw(AnonymousClass0p4 r3, AnonymousClass10W r4, boolean z) {
        super(null);
        this.A0A = new ArrayList(1);
        this.A0k = new int[4];
        this.A0l = new float[4];
        this.A0H = 0;
        this.A01 = Float.NaN;
        this.A02 = Float.NaN;
        this.A04 = Float.NaN;
        this.A05 = Float.NaN;
        this.A03 = Float.NaN;
        this.A00 = Float.NaN;
        this.A0J = -1;
        this.A0I = -1;
        this.A0E = -1.0f;
        this.A0D = -1.0f;
        this.A06 = r3;
        if (r4 != null) {
            r4.setData(this);
        }
        this.A09 = r4;
        if (z) {
            this.A0B = new HashSet();
        }
    }
}
