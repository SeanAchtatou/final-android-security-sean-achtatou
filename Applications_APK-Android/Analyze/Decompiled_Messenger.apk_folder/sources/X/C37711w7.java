package X;

import android.iawareperf.UniPerf;
import io.card.payment.BuildConfig;
import org.json.JSONObject;

/* renamed from: X.1w7  reason: invalid class name and case insensitive filesystem */
public final class C37711w7 implements C26381bM {
    public static boolean A00() {
        Class<UniPerf> cls = UniPerf.class;
        try {
            cls.toString();
            if (C34061oa.A01(cls, "uniPerfEvent", Integer.TYPE, String.class, int[].class)) {
                Class<UniPerf> cls2 = UniPerf.class;
                if (C34061oa.A01(cls, "getInstance", new Class[0])) {
                    return true;
                }
            }
            return false;
        } catch (Error | Exception unused) {
            return false;
        }
    }

    public int AyN() {
        return 6;
    }

    public int AyO() {
        return 8;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", "huawei");
            jSONObject.put("framework", "UniPerf");
            jSONObject.put("extra", BuildConfig.FLAVOR);
            return jSONObject.toString();
        } catch (Exception unused) {
            return BuildConfig.FLAVOR;
        }
    }

    public AnonymousClass0f7 AUy(C26401bO r5, C26501bY r6) {
        int[] Aet = r5.Aet(r6);
        if (Aet == null || Aet.length == 0) {
            return null;
        }
        if (Aet[0] >= 90) {
            Aet[0] = 4099;
        } else {
            Aet[0] = 4112;
        }
        return new C52962jy(UniPerf.getInstance(), r6.A01, Aet);
    }
}
