package X;

import java.util.Locale;

/* renamed from: X.0m4  reason: invalid class name */
public final class AnonymousClass0m4 {
    public AnonymousClass0UN A00;
    private final C04310Tq A01;

    public static final AnonymousClass0m4 A00(AnonymousClass1XY r1) {
        return new AnonymousClass0m4(r1);
    }

    public String A01(C10880l0 r4, String str) {
        StringBuilder sb = new StringBuilder((int) AnonymousClass1Y3.A0p);
        sb.append(r4.A06);
        sb.append(':');
        sb.append(r4.A02());
        sb.append(':');
        sb.append(((Locale) this.A01.get()).toString());
        if (str != null) {
            sb.append(':');
            sb.append(str);
        }
        return sb.toString();
    }

    private AnonymousClass0m4(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.BQw, r3);
    }
}
