package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0DZ  reason: invalid class name */
public final class AnonymousClass0DZ implements AnonymousClass0AY {
    public byte[] AUR(Long l, Boolean bool, Integer num, List list, List list2) {
        return null;
    }

    public List AiC(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            SubscribeTopic subscribeTopic = (SubscribeTopic) it.next();
            if (AnonymousClass0AI.A00(subscribeTopic.A01) != null) {
                arrayList.add(subscribeTopic);
            }
        }
        return arrayList;
    }

    public int BAV(DataOutputStream dataOutputStream, AnonymousClass0P6 r14) {
        Long valueOf;
        C01990Ck r3 = r14.A00;
        AnonymousClass0Rv A03 = r14.A03();
        AnonymousClass0CK A02 = r14.A02();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
        AnonymousClass0D8 r4 = new AnonymousClass0D8(byteArrayOutputStream);
        AnonymousClass0CJ r8 = A02.A01;
        C02460Ew r9 = new C02460Ew(AnonymousClass07B.A00);
        r9.A01.put(C04080Rt.A0P, r8.A0C);
        r9.A01.put(C04080Rt.A0O, r8.A0J);
        r9.A01.put(C04080Rt.A01, r8.A08);
        r9.A01.put(C04080Rt.A09, r8.A0A);
        r9.A01.put(C04080Rt.A0L, Integer.valueOf(r8.A00));
        r9.A01.put(C04080Rt.A0J, r8.A04);
        r9.A01.put(C04080Rt.A0G, r8.A02);
        r9.A01.put(C04080Rt.A07, r8.A0H);
        r9.A01.put(C04080Rt.A0E, r8.A03);
        r9.A01.put(C04080Rt.A0I, r8.A07);
        r9.A01.put(C04080Rt.A0H, r8.A06);
        r9.A01.put(C04080Rt.A03, r8.A0B);
        r9.A01.put(C04080Rt.A02, null);
        ArrayList arrayList = new ArrayList();
        for (String A00 : r8.A0K) {
            Integer A002 = AnonymousClass0AI.A00(A00);
            if (A002 != null) {
                arrayList.add(A002);
            }
        }
        r9.A01.put(C04080Rt.A0N, arrayList);
        r9.A01.put(C04080Rt.A05, r8.A0E);
        AnonymousClass08m r7 = C04080Rt.A00;
        String str = r8.A0D;
        if (str == null) {
            valueOf = null;
        } else {
            valueOf = Long.valueOf(Long.parseLong(str));
        }
        r9.A01.put(r7, valueOf);
        r9.A01.put(C04080Rt.A0K, null);
        r9.A01.put(C04080Rt.A06, null);
        r9.A01.put(C04080Rt.A0M, r8.A0G);
        r9.A01.put(C04080Rt.A08, r8.A0I);
        r9.A01.put(C04080Rt.A04, r8.A05);
        r9.A01.put(C04080Rt.A0F, r8.A09);
        C02460Ew r72 = new C02460Ew(AnonymousClass07B.A0Y);
        r72.A01.put(C04080Rt.A0U, A02.A02);
        r72.A01.put(C04080Rt.A0b, A02.A05);
        r72.A01.put(C04080Rt.A0a, A02.A04);
        r72.A01.put(C04080Rt.A0V, r9);
        r72.A01.put(C04080Rt.A0Y, A02.A03);
        r72.A01.put(C04080Rt.A0X, null);
        r72.A01.put(C04080Rt.A0Z, null);
        r72.A01.put(C04080Rt.A0W, null);
        r72.A01.put(C04080Rt.A0c, null);
        r72.A01.put(C04080Rt.A0T, r8.A0L);
        r72.A01(r4);
        byte[] A003 = AnonymousClass0Q0.A00(byteArrayOutputStream.toByteArray());
        int length = A003.length;
        int i = length + 12;
        dataOutputStream.writeByte(C04000Rj.A01(r3));
        int A022 = C04000Rj.A02(dataOutputStream, i) + 1;
        dataOutputStream.writeByte(0);
        dataOutputStream.writeByte(6);
        dataOutputStream.writeByte(77);
        dataOutputStream.writeByte(81);
        dataOutputStream.writeByte(84);
        dataOutputStream.writeByte(84);
        dataOutputStream.writeByte(111);
        dataOutputStream.writeByte(84);
        dataOutputStream.write(A03.A01);
        dataOutputStream.write(C04000Rj.A00(A03));
        dataOutputStream.writeShort(A03.A00);
        dataOutputStream.write(A003, 0, length);
        dataOutputStream.flush();
        return A022 + i;
    }
}
