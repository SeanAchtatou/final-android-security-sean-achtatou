package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

/* renamed from: X.0Ke  reason: invalid class name and case insensitive filesystem */
public final class C03140Ke extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0Jq A00;

    public C03140Ke(AnonymousClass0Jq r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(2089830399);
        NetworkInfo activeNetworkInfo = this.A00.A03.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            int type = activeNetworkInfo.getType();
            AnonymousClass0Jq r1 = this.A00;
            if (type != r1.A00) {
                r1.A04();
                this.A00.A00 = type;
                C000700l.A0D(intent, -2117935304, A01);
                return;
            }
        }
        C000700l.A0D(intent, 781017938, A01);
    }
}
