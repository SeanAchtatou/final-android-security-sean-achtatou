package X;

/* renamed from: X.0l2  reason: invalid class name and case insensitive filesystem */
public final class C10900l2 {
    public final int A00;
    public final Class A01;
    public final String A02;
    public final String A03 = C188215g.A00().toString();

    public String toString() {
        return "{activityClass=" + this.A01.getSimpleName() + ", " + "activityInstanceId=" + this.A00 + ", " + "moduleName=" + this.A02 + ", " + "SessionId=" + this.A03 + "}";
    }

    public C10900l2(Class cls, int i, String str) {
        this.A01 = cls;
        this.A00 = i;
        this.A02 = str;
    }
}
