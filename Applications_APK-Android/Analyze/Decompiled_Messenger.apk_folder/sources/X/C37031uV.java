package X;

import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;

/* renamed from: X.1uV  reason: invalid class name and case insensitive filesystem */
public final class C37031uV implements Function {
    public final /* synthetic */ C33191nB A00;

    public C37031uV(C33191nB r1) {
        this.A00 = r1;
    }

    public Object apply(Object obj) {
        GraphQLResult graphQLResult = (GraphQLResult) obj;
        Preconditions.checkNotNull(graphQLResult);
        C22251Ko r0 = this.A00.A00.A05;
        if (graphQLResult == null) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r0.A00)).CGS("message_requests_count_query_result", "Graphql MessageRequestsSnipperQuery result is null");
            return null;
        }
        GSTModelShape1S0000000 A1e = ((GSTModelShape1S0000000) graphQLResult.A03).A1e();
        Preconditions.checkNotNull(A1e);
        int A0X = A1e.A0X();
        GSTModelShape1S0000000 A1e2 = ((GSTModelShape1S0000000) graphQLResult.A03).A1e();
        Preconditions.checkNotNull(A1e2);
        int intValue = A1e2.getIntValue(1949198463);
        GSTModelShape1S0000000 A1e3 = ((GSTModelShape1S0000000) graphQLResult.A03).A1e();
        Preconditions.checkNotNull(A1e3);
        String A0P = A1e3.A0P(-1195699723);
        GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) ((GSTModelShape1S0000000) graphQLResult.A03).A0J(-419694070, GSTModelShape1S0000000.class, 1082819921);
        Preconditions.checkNotNull(gSTModelShape1S0000000);
        return new MessageRequestsSnippet(A0X, intValue, A0P, gSTModelShape1S0000000.A0X());
    }
}
