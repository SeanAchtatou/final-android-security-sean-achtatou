package X;

import java.util.HashMap;

/* renamed from: X.1XE  reason: invalid class name */
public final class AnonymousClass1XE extends HashMap<AnonymousClass0TO, AnonymousClass0TO> {
    public AnonymousClass1XE() {
        put(AnonymousClass0TN.A03, AnonymousClass0TN.A04);
        put(AnonymousClass0TN.A0c, AnonymousClass0TN.A0b);
        put(AnonymousClass0TN.A0M, AnonymousClass0TN.A0N);
        put(AnonymousClass0TN.A0K, AnonymousClass0TN.A0L);
        put(AnonymousClass0TN.A0O, AnonymousClass0TN.A0P);
        put(AnonymousClass0TN.A07, AnonymousClass0TN.A06);
        put(AnonymousClass0TN.A0H, AnonymousClass0TN.A0I);
        put(AnonymousClass0TN.A0X, AnonymousClass0TN.A0Y);
        put(AnonymousClass0TN.A0R, AnonymousClass0TN.A0S);
        put(AnonymousClass0TN.A0T, AnonymousClass0TN.A0U);
        put(AnonymousClass0TN.A0V, AnonymousClass0TN.A0W);
        put(AnonymousClass0TN.A0G, AnonymousClass0TN.A0F);
    }
}
