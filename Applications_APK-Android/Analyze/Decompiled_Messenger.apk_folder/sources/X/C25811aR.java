package X;

import com.facebook.inject.InjectorModule;
import com.facebook.quicklog.xplat.QPLXplatInitializerImpl;

@InjectorModule
/* renamed from: X.1aR  reason: invalid class name and case insensitive filesystem */
public final class C25811aR extends AnonymousClass0UV {
    private static volatile QPLXplatInitializerImpl A00;

    public static final QPLXplatInitializerImpl A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (QPLXplatInitializerImpl.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new QPLXplatInitializerImpl();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
