package X;

import com.google.common.base.Preconditions;

/* renamed from: X.1EO  reason: invalid class name */
public final class AnonymousClass1EO {
    public final Integer A00;
    public final Object A01;

    public static AnonymousClass1EO A00(Object obj) {
        return new AnonymousClass1EO(obj, AnonymousClass07B.A0C);
    }

    public static AnonymousClass1EO A01(Object obj) {
        return new AnonymousClass1EO(obj, AnonymousClass07B.A01);
    }

    public AnonymousClass1EO(Object obj, Integer num) {
        this.A01 = obj;
        Preconditions.checkNotNull(num);
        this.A00 = num;
    }
}
