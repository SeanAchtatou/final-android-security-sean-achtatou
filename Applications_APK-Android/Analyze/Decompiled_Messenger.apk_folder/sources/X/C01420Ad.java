package X;

import android.content.Context;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0Ad  reason: invalid class name and case insensitive filesystem */
public final class C01420Ad {
    public final Context A00;
    public final C009207y A01;
    public final ConcurrentHashMap A02;

    public C01540Aq A00(String str, Class cls) {
        Object obj = null;
        int i = 0;
        while (obj == null) {
            int i2 = i + 1;
            if (i >= 3) {
                break;
            }
            obj = this.A02.get(cls);
            if (obj == null && (obj = this.A01.A01(this.A00, str, cls)) != null) {
                this.A02.putIfAbsent(cls, obj);
            }
            i = i2;
        }
        if (obj == null) {
            C010708t.A0P("SystemServiceManager", "Cannot get system service after MAX_RETRIES: %s", cls.getName());
        }
        if (obj == null) {
            return C01660Bc.A00;
        }
        return new C01550Ar(obj);
    }

    public C01420Ad(Context context, AnonymousClass09P r3) {
        C009207y r0;
        this.A00 = context;
        if (r3 == null) {
            r0 = C009207y.A01;
        } else {
            r0 = new C009207y(r3);
        }
        this.A01 = r0;
        this.A02 = new ConcurrentHashMap();
    }
}
