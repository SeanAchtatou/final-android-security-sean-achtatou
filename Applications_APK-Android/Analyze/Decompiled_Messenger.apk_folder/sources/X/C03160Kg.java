package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0Kg  reason: invalid class name and case insensitive filesystem */
public abstract class C03160Kg {
    public abstract long A00();

    public abstract void A01(AnonymousClass0FM r1, DataOutput dataOutput);

    public abstract boolean A03(AnonymousClass0FM r1, DataInput dataInput);

    public final boolean A02(AnonymousClass0FM r6, DataInput dataInput) {
        if (dataInput.readShort() == 251 && dataInput.readShort() == 2 && dataInput.readLong() == A00()) {
            return A03(r6, dataInput);
        }
        return false;
    }
}
