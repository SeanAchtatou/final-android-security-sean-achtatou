package X;

import android.graphics.Bitmap;

/* renamed from: X.1va  reason: invalid class name and case insensitive filesystem */
public class C37381va extends C22871Nd implements C37391vb {
    public C37381va(C14320t5 r2, AnonymousClass1N7 r3, AnonymousClass1N4 r4, boolean z) {
        super(r2, r3, r4, z);
        this.A04.C0d(this);
        this.A08.C6L(this);
    }

    public Object A08(C22911Nh r3) {
        Bitmap bitmap = (Bitmap) super.A08(r3);
        if (bitmap != null) {
            bitmap.eraseColor(0);
        }
        return bitmap;
    }
}
