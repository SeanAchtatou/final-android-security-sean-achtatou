package X;

import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.service.model.FetchMoreMessagesResult;

/* renamed from: X.1i4  reason: invalid class name and case insensitive filesystem */
public abstract class C30451i4 {
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00cc, code lost:
        if (r2.size() < r4) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e6, code lost:
        if (r7 != null) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00eb, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.MessagesCollection A0A(com.facebook.messaging.model.threadkey.ThreadKey r17, long r18, int r20) {
        /*
            r16 = this;
            r2 = r16
            X.1i3 r2 = (X.C30441i3) r2
            java.lang.String r1 = "TincanDbMessagesFetcher"
            X.1a6 r7 = X.C06160ax.A00()
            X.0W6 r4 = X.C195818v.A0E
            r3 = r17
            java.lang.String r0 = r3.toString()
            X.0av r0 = r4.A03(r0)
            r7.A05(r0)
            X.0av r0 = X.C30441i3.A01()
            r7.A05(r0)
            r4 = 0
            int r0 = (r18 > r4 ? 1 : (r18 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0036
            X.0W6 r0 = X.C195818v.A0F
            java.lang.String r5 = java.lang.String.valueOf(r18)
            java.lang.String r4 = r0.A00
            X.2jZ r0 = new X.2jZ
            r0.<init>(r4, r5)
            r7.A05(r0)
        L_0x0036:
            X.0W6 r0 = X.C195818v.A0F
            java.lang.String r15 = r0.A04()
            r4 = r20
            if (r20 <= 0) goto L_0x0046
            java.lang.String r0 = " LIMIT "
            java.lang.String r15 = X.AnonymousClass08S.A0L(r15, r0, r4)
        L_0x0046:
            java.lang.String r9 = X.C30441i3.A07()
            java.util.LinkedList r5 = new java.util.LinkedList
            r5.<init>()
            com.google.common.collect.ImmutableList$Builder r6 = new com.google.common.collect.ImmutableList$Builder
            r6.<init>()
            X.0Tq r0 = r2.A03
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r8 = r0.A01()
            if (r8 != 0) goto L_0x0067
            com.facebook.messaging.model.messages.MessagesCollection r0 = com.facebook.messaging.model.messages.MessagesCollection.A02(r3)
            return r0
        L_0x0067:
            java.lang.String[] r10 = X.C30441i3.A08
            java.lang.String r11 = r7.A02()
            java.lang.String[] r12 = r7.A04()
            r13 = 0
            r14 = 0
            android.database.Cursor r7 = r8.query(r9, r10, r11, r12, r13, r14, r15)
        L_0x0077:
            boolean r0 = r7.moveToNext()     // Catch:{ all -> 0x00e3 }
            if (r0 == 0) goto L_0x0089
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x00e3 }
            r0.<init>()     // Catch:{ all -> 0x00e3 }
            android.database.DatabaseUtils.cursorRowToContentValues(r7, r0)     // Catch:{ all -> 0x00e3 }
            r5.add(r0)     // Catch:{ all -> 0x00e3 }
            goto L_0x0077
        L_0x0089:
            r7.close()
            java.util.Iterator r9 = r5.iterator()
        L_0x0090:
            boolean r0 = r9.hasNext()
            r8 = 1
            if (r0 == 0) goto L_0x00c1
            java.lang.Object r7 = r9.next()
            android.content.ContentValues r7 = (android.content.ContentValues) r7
            com.facebook.messaging.model.messages.Message r5 = X.C30441i3.A02(r2, r7)     // Catch:{ all -> 0x00ba }
            X.0W6 r0 = X.C195818v.A03     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x00ba }
            java.lang.Integer r0 = r7.getAsInteger(r0)     // Catch:{ all -> 0x00ba }
            if (r0 == 0) goto L_0x00b6
            int r0 = r0.intValue()     // Catch:{ all -> 0x00ba }
            if (r0 != r8) goto L_0x00b6
            java.lang.String r0 = "Returning an expired message, this should never happen!"
            X.C010708t.A0K(r1, r0)     // Catch:{ all -> 0x00ba }
        L_0x00b6:
            r6.add(r5)     // Catch:{ all -> 0x00ba }
            goto L_0x0090
        L_0x00ba:
            r5 = move-exception
            java.lang.String r0 = "Unable to decrypt message. Skipping"
            X.C010708t.A0S(r1, r5, r0)
            goto L_0x0090
        L_0x00c1:
            com.google.common.collect.ImmutableList r2 = r6.build()
            if (r20 <= 0) goto L_0x00ce
            int r1 = r2.size()
            r0 = 0
            if (r1 >= r4) goto L_0x00cf
        L_0x00ce:
            r0 = 1
        L_0x00cf:
            X.1oI r1 = new X.1oI
            r1.<init>()
            r1.A00 = r3
            r1.A01(r2)
            r1.A03 = r0
            r0 = 1
            r1.A02 = r0
            com.facebook.messaging.model.messages.MessagesCollection r0 = r1.A00()
            return r0
        L_0x00e3:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00e5 }
        L_0x00e5:
            r0 = move-exception
            if (r7 == 0) goto L_0x00eb
            r7.close()     // Catch:{ all -> 0x00eb }
        L_0x00eb:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30451i4.A0A(com.facebook.messaging.model.threadkey.ThreadKey, long, int):com.facebook.messaging.model.messages.MessagesCollection");
    }

    public FetchMoreMessagesResult A0B(ThreadKey threadKey, long j, int i) {
        C30441i3 r0 = (C30441i3) this;
        return new FetchMoreMessagesResult(DataFetchDisposition.A0E, r0.A0A(threadKey, j, i), r0.A00.now());
    }
}
