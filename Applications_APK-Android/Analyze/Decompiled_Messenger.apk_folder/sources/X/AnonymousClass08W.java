package X;

import android.os.SystemClock;
import java.util.ArrayList;

/* renamed from: X.08W  reason: invalid class name */
public final class AnonymousClass08W implements C000000c {
    public AnonymousClass0KR A00;
    public final ArrayList A01 = new ArrayList(8);

    public static void A00(AnonymousClass0KR r8, C010308p r9) {
        AnonymousClass0KR r4 = r8;
        if (0 == 0) {
            r4.AOg(r9.A00, r9.A01, r9.A02);
        } else if (0 == 1) {
            r8.AOi(AnonymousClass08S.A0J(null, "_begin"), r9.A01);
            r8.AOi(AnonymousClass08S.A0J(null, "_end"), r9.A02);
        } else if (0 == 2) {
            r8.AOi(null, r9.A01);
        }
    }

    public AnonymousClass02K AOr(int i) {
        C010308p r2 = new C010308p(this);
        r2.A00 = i;
        r2.A01 = SystemClock.uptimeMillis();
        return r2;
    }
}
