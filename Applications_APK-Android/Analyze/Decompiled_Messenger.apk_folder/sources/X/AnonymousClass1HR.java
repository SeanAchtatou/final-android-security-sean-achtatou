package X;

import com.facebook.auth.userscope.UserScoped;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

@UserScoped
/* renamed from: X.1HR  reason: invalid class name */
public final class AnonymousClass1HR implements C12390pG {
    private static C05540Zi A01;
    public String A00;

    public synchronized Map Ajk() {
        ImmutableMap.Builder builder;
        builder = ImmutableMap.builder();
        builder.put("latestRankingUpdate", Strings.nullToEmpty(this.A00));
        return builder.build();
    }

    public static final AnonymousClass1HR A00(AnonymousClass1XY r3) {
        AnonymousClass1HR r0;
        synchronized (AnonymousClass1HR.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r3)) {
                    A01.A01();
                    A01.A00 = new AnonymousClass1HR();
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass1HR) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }
}
