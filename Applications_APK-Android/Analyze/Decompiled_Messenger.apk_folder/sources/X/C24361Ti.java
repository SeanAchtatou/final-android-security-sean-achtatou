package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.debug.activitytracer.ActivityTracer;
import com.facebook.fbservice.service.ServiceException;
import com.facebook.http.protocol.ApiErrorResult;
import java.util.Map;

/* renamed from: X.1Ti  reason: invalid class name and case insensitive filesystem */
public final class C24361Ti {
    public final DeprecatedAnalyticsLogger A00;
    public final AnonymousClass146 A01;
    public final ActivityTracer A02;
    public final C17270yd A03;
    public final AnonymousClass1BS A04;
    public final C04310Tq A05;

    public void A09(String str, String str2, Map map) {
        String str3 = (String) this.A05.get();
        A00(this, str, str2, str3);
        AnonymousClass146 r0 = this.A01;
        r0.A0C(null, str, str2, str3, map);
    }

    public static C45702Nd A00(C24361Ti r2, String str, String str2, String str3) {
        C45702Nd A022 = r2.A02.A02("navigation", StringFormatUtil.formatStrLocaleSafe("Navigation from %s to %s", str, str2));
        if (A022 == null) {
            return null;
        }
        A022.A03(str);
        A022.A02(str2);
        A022.A06 = str3;
        return A022;
    }

    public static final C24361Ti A01(AnonymousClass1XY r1) {
        return new C24361Ti(r1);
    }

    public static final C24361Ti A02(AnonymousClass1XY r1) {
        return new C24361Ti(r1);
    }

    public void A03(Integer num) {
        String str;
        if (1 - num.intValue() != 0) {
            str = "mk_mn_settings_tab_appearance";
        } else {
            str = "mk_mn_settings_tab_tap";
        }
        C11670nb r2 = new C11670nb(str);
        r2.A0D("pigeon_reserved_keyword_module", "neo");
        this.A00.A09(r2);
    }

    public void A04(String str) {
        C45702Nd r0 = this.A02.A00;
        if (r0 != null) {
            r0.A01(str);
        }
    }

    public void A05(String str) {
        C22361La A042 = this.A00.A04(str, false);
        if (A042.A0B()) {
            A042.A0A();
        }
    }

    public void A06(String str) {
        C45702Nd r1 = this.A02.A00;
        if (r1 != null && r1.A04 == str) {
            r1.A02 = (String) this.A05.get();
            ActivityTracer activityTracer = this.A02;
            activityTracer.A02.AOz();
            AnonymousClass00S.A04(activityTracer.A01, new C622930v(activityTracer), 963974095);
        }
    }

    public void A07(String str, ServiceException serviceException, Map map) {
        if (map == null) {
            map = AnonymousClass0TG.A04();
        }
        map.put("error", serviceException.errorCode.toString());
        ApiErrorResult apiErrorResult = (ApiErrorResult) serviceException.result.A08();
        if (apiErrorResult != null) {
            map.put(AnonymousClass24B.$const$string(184), String.valueOf(apiErrorResult.A02()));
        }
        C11670nb r1 = new C11670nb(str);
        r1.A0E(C99084oO.$const$string(AnonymousClass1Y3.A1U), false);
        C11670nb.A02(r1, map, false);
        this.A00.A07(r1);
    }

    public void A08(String str, Object obj) {
        C45702Nd r0 = this.A02.A00;
        if (r0 != null) {
            r0.A04(str, obj);
        }
    }

    private C24361Ti(AnonymousClass1XY r2) {
        this.A00 = C06920cI.A00(r2);
        this.A01 = AnonymousClass146.A00(r2);
        C28841fS.A04(r2);
        new C32851mR(r2);
        this.A04 = AnonymousClass1BS.A00(r2);
        this.A03 = C17270yd.A00(r2);
        this.A02 = ActivityTracer.A00(r2);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.A80, r2);
    }
}
