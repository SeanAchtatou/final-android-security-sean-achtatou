package X;

import android.content.Context;
import android.view.accessibility.AccessibilityManager;

/* renamed from: X.0z6  reason: invalid class name and case insensitive filesystem */
public final class C17560z6 {
    public static volatile boolean A00;
    public static volatile boolean A01;

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0044, code lost:
        if (r0 == false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0048, code lost:
        if (r4 != false) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void A00(android.view.accessibility.AccessibilityManager r7) {
        /*
            java.lang.Class<X.0z6> r6 = X.C17560z6.class
            monitor-enter(r6)
            java.lang.String r0 = "is_accessibility_enabled"
            boolean r0 = java.lang.Boolean.getBoolean(r0)     // Catch:{ all -> 0x0051 }
            r5 = 1
            if (r0 != 0) goto L_0x004a
            r4 = 0
            if (r7 == 0) goto L_0x0047
            boolean r0 = r7.isEnabled()     // Catch:{ all -> 0x0051 }
            if (r0 == 0) goto L_0x0047
            boolean r0 = r7.isTouchExplorationEnabled()     // Catch:{ all -> 0x0051 }
            if (r0 != 0) goto L_0x0046
            r0 = -1
            java.util.List r0 = r7.getEnabledAccessibilityServiceList(r0)     // Catch:{ all -> 0x0051 }
            if (r0 == 0) goto L_0x0043
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x0051 }
        L_0x0026:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0051 }
            if (r0 == 0) goto L_0x0043
            java.lang.Object r2 = r3.next()     // Catch:{ all -> 0x0051 }
            android.accessibilityservice.AccessibilityServiceInfo r2 = (android.accessibilityservice.AccessibilityServiceInfo) r2     // Catch:{ all -> 0x0051 }
            int r1 = r2.eventTypes     // Catch:{ all -> 0x0051 }
            r0 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            if (r1 != r0) goto L_0x0026
            int r0 = X.AnonymousClass38G.A00(r2)     // Catch:{ all -> 0x0051 }
            r0 = r0 & r5
            if (r0 != r5) goto L_0x0026
            r0 = 1
            goto L_0x0044
        L_0x0043:
            r0 = 0
        L_0x0044:
            if (r0 == 0) goto L_0x0047
        L_0x0046:
            r4 = 1
        L_0x0047:
            r0 = 0
            if (r4 == 0) goto L_0x004b
        L_0x004a:
            r0 = 1
        L_0x004b:
            X.C17560z6.A00 = r0     // Catch:{ all -> 0x0051 }
            X.C17560z6.A01 = r5     // Catch:{ all -> 0x0051 }
            monitor-exit(r6)
            return
        L_0x0051:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17560z6.A00(android.view.accessibility.AccessibilityManager):void");
    }

    public static boolean A01(Context context) {
        if (!A01) {
            A00((AccessibilityManager) context.getSystemService("accessibility"));
        }
        return A00;
    }
}
