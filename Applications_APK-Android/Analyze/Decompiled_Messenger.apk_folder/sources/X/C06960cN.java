package X;

import com.facebook.secure.intentswitchoff.FbReceiverSwitchOffDI;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0cN  reason: invalid class name and case insensitive filesystem */
public final class C06960cN extends AnonymousClass1ZS {
    private static volatile C06960cN A00;

    public static final C06960cN A00(AnonymousClass1XY r6) {
        if (A00 == null) {
            synchronized (C06960cN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A00 = new C06960cN(FbReceiverSwitchOffDI.A00(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AxH, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private C06960cN(FbReceiverSwitchOffDI fbReceiverSwitchOffDI, AnonymousClass0US r2) {
        super(fbReceiverSwitchOffDI, r2);
    }
}
