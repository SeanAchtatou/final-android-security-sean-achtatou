package X;

import com.facebook.acra.ACRA;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.content.ContentModule;
import com.facebook.content.SecureContextHelper;
import java.util.Set;

@UserScoped
/* renamed from: X.147  reason: invalid class name */
public final class AnonymousClass147 {
    private static C05540Zi A04;
    public AnonymousClass0UN A00;
    public final Set A01;
    private final C31781kN A02;
    private final SecureContextHelper A03;

    public static int A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return 6;
            default:
                return 0;
        }
    }

    public static final AnonymousClass147 A01(AnonymousClass1XY r7) {
        AnonymousClass147 r0;
        synchronized (AnonymousClass147.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r7)) {
                    AnonymousClass1XY r5 = (AnonymousClass1XY) A04.A01();
                    A04.A00 = new AnonymousClass147(r5, new AnonymousClass0X5(r5, AnonymousClass0X6.A1D), ContentModule.A00(r5), C31781kN.A00(r5));
                }
                C05540Zi r1 = A04;
                r0 = (AnonymousClass147) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0053, code lost:
        throw r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(android.app.Activity r7) {
        /*
            r6 = this;
            java.lang.String r1 = "BlockingFlowsManager.chooseBestBlockingFlowToShow"
            r0 = 23996577(0x16e28a1, float:4.3742846E-38)
            X.C005505z.A03(r1, r0)
            java.util.Set r0 = r6.A01     // Catch:{ all -> 0x009c }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x009c }
            r2 = 0
            r1 = 2147483647(0x7fffffff, float:NaN)
        L_0x0012:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0054
            java.lang.Object r4 = r5.next()     // Catch:{ all -> 0x009c }
            X.14q r4 = (X.C186714q) r4     // Catch:{ all -> 0x009c }
            java.lang.String r3 = "Checking BlockingFlowLauncher"
            r0 = -1080869524(0xffffffffbf933d6c, float:-1.150312)
            X.C005505z.A03(r3, r0)     // Catch:{ all -> 0x009c }
            java.lang.Integer r0 = r4.AeZ()     // Catch:{ all -> 0x004c }
            int r0 = A00(r0)     // Catch:{ all -> 0x004c }
            if (r0 >= r1) goto L_0x0045
            boolean r0 = r4.BDw(r7)     // Catch:{ all -> 0x004c }
            if (r0 == 0) goto L_0x0045
            boolean r0 = r4.CEE(r7)     // Catch:{ all -> 0x004c }
            if (r0 == 0) goto L_0x0045
            java.lang.Integer r0 = r4.AeZ()     // Catch:{ all -> 0x004c }
            int r1 = A00(r0)     // Catch:{ all -> 0x004c }
            r2 = r4
        L_0x0045:
            r0 = 68094504(0x40f0a28, float:1.6814233E-36)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x009c }
            goto L_0x0012
        L_0x004c:
            r1 = move-exception
            r0 = 1301170440(0x4d8e4908, float:2.98393856E8)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x009c }
            throw r1     // Catch:{ all -> 0x009c }
        L_0x0054:
            if (r2 != 0) goto L_0x005d
            r0 = 1618867308(0x607df46c, float:7.319748E19)
        L_0x0059:
            X.C005505z.A00(r0)
            return
        L_0x005d:
            boolean r0 = r7 instanceof X.AnonymousClass6QZ     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0065
            r0 = -1715365296(0xffffffff99c19a50, float:-2.0018059E-23)
            goto L_0x0059
        L_0x0065:
            X.1kN r1 = r6.A02     // Catch:{ all -> 0x009c }
            java.lang.Class<com.facebook.messaging.blockingflows.TaskRunningInBlockingFlowContext> r0 = com.facebook.messaging.blockingflows.TaskRunningInBlockingFlowContext.class
            boolean r0 = r1.A01(r7, r0)     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0073
            r0 = -665686345(0xffffffffd8526eb7, float:-9.2549183E14)
            goto L_0x0059
        L_0x0073:
            X.1kN r1 = r6.A02     // Catch:{ all -> 0x009c }
            java.lang.Class<com.facebook.platform.common.annotations.TaskRunningInPlatformContext> r0 = com.facebook.platform.common.annotations.TaskRunningInPlatformContext.class
            boolean r0 = r1.A01(r7, r0)     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0081
            r0 = 1383602783(0x52781a5f, float:2.66398581E11)
            goto L_0x0059
        L_0x0081:
            android.content.Intent r1 = r2.AqX(r7)     // Catch:{ all -> 0x009c }
            if (r1 == 0) goto L_0x0095
            com.facebook.content.SecureContextHelper r0 = r6.A03     // Catch:{ all -> 0x009c }
            r0.startFacebookActivity(r1, r7)     // Catch:{ all -> 0x009c }
            boolean r0 = r2.CE5()     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0095
            r7.finish()     // Catch:{ all -> 0x009c }
        L_0x0095:
            r0 = 1564397715(0x5d3ed093, float:8.5935321E17)
            X.C005505z.A00(r0)
            return
        L_0x009c:
            r1 = move-exception
            r0 = -1450787190(0xffffffffa986be8a, float:-5.983849E-14)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass147.A02(android.app.Activity):void");
    }

    private AnonymousClass147(AnonymousClass1XY r3, Set set, SecureContextHelper secureContextHelper, C31781kN r6) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = set;
        this.A03 = secureContextHelper;
        this.A02 = r6;
    }
}
