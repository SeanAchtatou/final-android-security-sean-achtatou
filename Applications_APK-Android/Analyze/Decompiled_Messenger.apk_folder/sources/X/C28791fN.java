package X;

/* renamed from: X.1fN  reason: invalid class name and case insensitive filesystem */
public abstract class C28791fN implements Runnable, C012409l {
    public static final String __redex_internal_original_name = "com.facebook.common.executors.NamedRunnable";
    public final String A00;
    public final String A01;

    public Object getInnerRunnable() {
        return this;
    }

    public String getRunnableName() {
        return AnonymousClass08S.A0P(this.A00, "/", this.A01);
    }

    public C28791fN(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    public String toString() {
        return getRunnableName();
    }
}
