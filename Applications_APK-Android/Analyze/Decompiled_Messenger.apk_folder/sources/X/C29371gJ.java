package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1gJ  reason: invalid class name and case insensitive filesystem */
public final class C29371gJ extends AbstractExecutorService implements AnonymousClass0VK {
    public final AnonymousClass069 A00;
    public final PriorityQueue A01 = new PriorityQueue();
    private final AlarmManager A02;
    private final PendingIntent A03;
    private final Context A04;
    private final Handler A05;
    private final AnonymousClass09P A06;

    public /* bridge */ /* synthetic */ AnonymousClass0Y0 C4Y(Runnable runnable, long j, TimeUnit timeUnit) {
        AnonymousClass14Y r4 = new AnonymousClass14Y(this, runnable, null);
        A01(r4, this.A00.now() + timeUnit.toMillis(j));
        return r4;
    }

    public /* bridge */ /* synthetic */ ListenableFuture CIC(Runnable runnable) {
        AnonymousClass14Y r2 = new AnonymousClass14Y(this, runnable, null);
        A01(r2, this.A00.now());
        return r2;
    }

    public void execute(Runnable runnable) {
        A01(new AnonymousClass14Y(this, runnable, null), this.A00.now());
    }

    public boolean isShutdown() {
        return false;
    }

    public boolean isTerminated() {
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: A00 */
    public AnonymousClass14Y schedule(Callable callable, long j, TimeUnit timeUnit) {
        AnonymousClass14Y r4 = new AnonymousClass14Y(this, callable);
        A01(r4, this.A00.now() + timeUnit.toMillis(j));
        return r4;
    }

    private void A01(AnonymousClass14Y r3, long j) {
        this.A00.now();
        synchronized (this) {
            this.A01.add(new C72013dW(r3, j));
            A02(this);
        }
    }

    public static void A02(C29371gJ r5) {
        if (r5.A01.isEmpty()) {
            r5.A02.cancel(r5.A03);
            return;
        }
        long j = ((C72013dW) r5.A01.peek()).A00;
        r5.A00.now();
        if (Build.VERSION.SDK_INT < 19) {
            r5.A02.set(2, j, r5.A03);
        } else {
            AnonymousClass0E8.A00(r5.A06, r5.A02, 2, j, r5.A03);
        }
    }

    public /* bridge */ /* synthetic */ ListenableFuture CIE(Runnable runnable, Object obj) {
        AnonymousClass14Y r2 = new AnonymousClass14Y(this, runnable, obj);
        A01(r2, this.A00.now());
        return r2;
    }

    public /* bridge */ /* synthetic */ ListenableFuture CIF(Callable callable) {
        return schedule(callable, 0, TimeUnit.MILLISECONDS);
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void shutdown() {
        throw new UnsupportedOperationException();
    }

    public List shutdownNow() {
        throw new UnsupportedOperationException();
    }

    public C29371gJ(AnonymousClass1XY r7, String str, Handler handler) {
        String str2;
        this.A06 = C04920Ww.A00(r7);
        this.A04 = AnonymousClass1YA.A00(r7);
        this.A00 = AnonymousClass067.A05(r7);
        this.A02 = (AlarmManager) AnonymousClass1YA.A00(r7).getSystemService("alarm");
        this.A05 = handler;
        if (str == null) {
            str2 = "WakingExecutorService.ACTION_ALARM." + AnonymousClass00M.A00();
        } else {
            str2 = "WakingExecutorService.ACTION_ALARM." + AnonymousClass00M.A00() + "." + str;
        }
        Intent intent = new Intent(str2);
        intent.setPackage(this.A04.getPackageName());
        this.A03 = PendingIntent.getBroadcast(this.A04, 0, intent, 134217728);
        this.A04.registerReceiver(new C29381gK(this, str2), new IntentFilter(str2), null, this.A05);
    }

    public RunnableFuture newTaskFor(Runnable runnable, Object obj) {
        return new C43252Dq(this, runnable, obj);
    }

    public RunnableFuture newTaskFor(Callable callable) {
        return new C43252Dq(this, callable);
    }

    public /* bridge */ /* synthetic */ ScheduledFuture schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        AnonymousClass14Y r4 = new AnonymousClass14Y(this, runnable, null);
        A01(r4, this.A00.now() + timeUnit.toMillis(j));
        return r4;
    }

    public /* bridge */ /* synthetic */ Future submit(Runnable runnable) {
        AnonymousClass14Y r2 = new AnonymousClass14Y(this, runnable, null);
        A01(r2, this.A00.now());
        return r2;
    }

    public /* bridge */ /* synthetic */ Future submit(Runnable runnable, Object obj) {
        AnonymousClass14Y r2 = new AnonymousClass14Y(this, runnable, obj);
        A01(r2, this.A00.now());
        return r2;
    }

    public /* bridge */ /* synthetic */ Future submit(Callable callable) {
        return schedule(callable, 0, TimeUnit.MILLISECONDS);
    }
}
