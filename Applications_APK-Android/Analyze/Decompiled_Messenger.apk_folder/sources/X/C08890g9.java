package X;

/* renamed from: X.0g9  reason: invalid class name and case insensitive filesystem */
public abstract class C08890g9 {
    public Object A01() {
        if (this instanceof C21221Fs) {
            C21221Fs r1 = (C21221Fs) this;
            if (r1 instanceof C33021mi) {
                return ((C33021mi) r1).A06();
            }
            if (r1 instanceof C33031mj) {
                return ((C33031mj) r1).A06();
            }
            if (!(r1 instanceof AnonymousClass1H5)) {
                return r1.A03();
            }
            AnonymousClass1H5 r12 = (AnonymousClass1H5) r1;
            return !(r12 instanceof C21211Fr) ? r12.A06() : ((C21211Fr) r12).A06();
        } else if (!(this instanceof C21251Fv)) {
            C08880g8 r13 = (C08880g8) this;
            return !(r13 instanceof AnonymousClass14X) ? !(r13 instanceof AnonymousClass14Y) ? !(r13 instanceof C08960gG) ? r13.A02() : ((C08960gG) r13).A00 : ((AnonymousClass14Y) r13).A00 : ((AnonymousClass14X) r13).A03();
        } else {
            C21251Fv r14 = (C21251Fv) this;
            return !(r14 instanceof C21241Fu) ? r14.A02() : ((C21241Fu) r14).A02();
        }
    }

    public String toString() {
        return A01().toString();
    }
}
