package X;

import com.facebook.common.dextricks.DexStore;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0Sa  reason: invalid class name and case insensitive filesystem */
public final class C04120Sa implements AnonymousClass00X {
    private final Set A00 = new HashSet();

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0028 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0029 */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0034 A[SYNTHETIC, Splitter:B:25:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0045 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int A00(java.lang.String r7, byte[] r8) {
        /*
            r6 = this;
            java.util.Set r0 = r6.A00
            boolean r0 = r0.contains(r7)
            r5 = -1
            if (r0 != 0) goto L_0x005e
            r4 = 0
            int r0 = android.os.Build.VERSION.SDK_INT
            r3 = 9
            if (r0 < r3) goto L_0x0014
            android.os.StrictMode$ThreadPolicy r4 = android.os.StrictMode.allowThreadDiskReads()
        L_0x0014:
            int r0 = r8.length     // Catch:{ all -> 0x0051 }
            r1 = 0
            java.io.FileDescriptor r2 = android.system.Os.open(r7, r1, r1)     // Catch:{ ErrnoException -> 0x002c }
            int r0 = r0 + -1
            int r1 = android.system.Os.read(r2, r8, r1, r0)     // Catch:{ ErrnoException | InterruptedIOException -> 0x0029, all -> 0x0024 }
            android.system.Os.close(r2)     // Catch:{ ErrnoException -> 0x002f }
            goto L_0x002f
        L_0x0024:
            r0 = move-exception
            android.system.Os.close(r2)     // Catch:{ ErrnoException -> 0x0028 }
        L_0x0028:
            throw r0     // Catch:{ all -> 0x0051 }
        L_0x0029:
            android.system.Os.close(r2)     // Catch:{ ErrnoException -> 0x002c }
        L_0x002c:
            r1 = -2147483647(0xffffffff80000001, float:-1.4E-45)
        L_0x002f:
            r0 = -2147483647(0xffffffff80000001, float:-1.4E-45)
            if (r1 != r0) goto L_0x0045
            java.util.Set r0 = r6.A00     // Catch:{ all -> 0x0051 }
            r0.add(r7)     // Catch:{ all -> 0x0051 }
            if (r4 == 0) goto L_0x005e
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r3) goto L_0x005e
            android.os.StrictMode$ThreadPolicy r4 = (android.os.StrictMode.ThreadPolicy) r4
            android.os.StrictMode.setThreadPolicy(r4)
            return r5
        L_0x0045:
            if (r4 == 0) goto L_0x0050
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r3) goto L_0x0050
            android.os.StrictMode$ThreadPolicy r4 = (android.os.StrictMode.ThreadPolicy) r4
            android.os.StrictMode.setThreadPolicy(r4)
        L_0x0050:
            return r1
        L_0x0051:
            r1 = move-exception
            if (r4 == 0) goto L_0x005d
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r3) goto L_0x005d
            android.os.StrictMode$ThreadPolicy r4 = (android.os.StrictMode.ThreadPolicy) r4
            android.os.StrictMode.setThreadPolicy(r4)
        L_0x005d:
            throw r1
        L_0x005e:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04120Sa.A00(java.lang.String, byte[]):int");
    }

    public boolean Bzf(String str, int[] iArr, String[] strArr, long[] jArr, float[] fArr) {
        byte[] bArr = new byte[DexStore.LOAD_RESULT_OATMEAL_QUICKENED];
        int A002 = A00(str, bArr);
        if (A002 < 0) {
            return false;
        }
        return BwU(bArr, 0, A002, iArr, strArr, jArr, fArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:52:0x008e, code lost:
        if (r8[r3] != 10) goto L_0x0090;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean Bzg(java.lang.String r20, java.lang.String[] r21, long[] r22) {
        /*
            r19 = this;
            r3 = r20
            if (r20 == 0) goto L_0x00ca
            r11 = r21
            if (r21 == 0) goto L_0x00ca
            r10 = r22
            if (r22 == 0) goto L_0x00ca
            int r9 = r11.length
            int r0 = r10.length
            if (r9 > r0) goto L_0x00c2
            r18 = 0
            r2 = 0
        L_0x0013:
            if (r2 >= r9) goto L_0x001c
            r0 = 0
            r22[r2] = r0
            int r2 = r2 + 1
            goto L_0x0013
        L_0x001c:
            r0 = 2048(0x800, float:2.87E-42)
            byte[] r8 = new byte[r0]
            r0 = r19
            int r7 = r0.A00(r3, r8)
            r17 = 1
            if (r7 >= 0) goto L_0x002d
            r17 = 0
            r7 = 0
        L_0x002d:
            r6 = 2048(0x800, float:2.87E-42)
            if (r7 >= r6) goto L_0x0033
            r8[r7] = r18
        L_0x0033:
            r1 = 0
            r5 = 0
        L_0x0035:
            if (r1 >= r7) goto L_0x00c1
            byte r0 = r8[r1]
            if (r0 == 0) goto L_0x00c1
            if (r5 >= r9) goto L_0x00c1
            r12 = 0
        L_0x003e:
            r4 = 10
            if (r12 >= r9) goto L_0x00a7
            r15 = r21[r12]
            r14 = r1
            int r13 = r15.length()
            r16 = 0
            r3 = 0
        L_0x004c:
            if (r14 >= r6) goto L_0x005d
            if (r3 >= r13) goto L_0x005d
            byte r2 = r8[r14]
            char r0 = r15.charAt(r3)
            if (r2 != r0) goto L_0x0061
            int r14 = r14 + 1
            int r3 = r3 + 1
            goto L_0x004c
        L_0x005d:
            if (r3 != r13) goto L_0x0061
            r16 = 1
        L_0x0061:
            if (r16 == 0) goto L_0x00a4
            int r0 = r15.length()
            int r1 = r1 + r0
        L_0x0068:
            if (r1 >= r6) goto L_0x0079
            byte r2 = r8[r1]
            if (r2 == 0) goto L_0x0076
            r0 = 32
            if (r2 == r0) goto L_0x0076
            r0 = 9
            if (r2 != r0) goto L_0x0079
        L_0x0076:
            int r1 = r1 + 1
            goto L_0x0068
        L_0x0079:
            r3 = r1
        L_0x007a:
            if (r3 >= r6) goto L_0x0089
            byte r2 = r8[r3]
            r0 = 48
            if (r2 < r0) goto L_0x0089
            r0 = 57
            if (r2 > r0) goto L_0x0089
            int r3 = r3 + 1
            goto L_0x007a
        L_0x0089:
            if (r3 >= r6) goto L_0x0090
            byte r0 = r8[r3]
            r2 = 0
            if (r0 == r4) goto L_0x0091
        L_0x0090:
            r2 = 1
        L_0x0091:
            if (r3 >= r6) goto L_0x009b
            byte r0 = r8[r3]
            if (r0 == 0) goto L_0x009b
            r8[r3] = r18
            int r3 = r3 + 1
        L_0x009b:
            long r0 = X.AnonymousClass0N6.A00(r8, r1, r4)
            r22[r12] = r0
            int r5 = r5 + 1
            goto L_0x00a9
        L_0x00a4:
            int r12 = r12 + 1
            goto L_0x003e
        L_0x00a7:
            r3 = r1
            r2 = 1
        L_0x00a9:
            if (r2 == 0) goto L_0x00be
        L_0x00ab:
            if (r3 >= r7) goto L_0x00b6
            byte r0 = r8[r3]
            if (r0 == 0) goto L_0x00b6
            if (r0 == r4) goto L_0x00b6
            int r3 = r3 + 1
            goto L_0x00ab
        L_0x00b6:
            if (r3 >= r6) goto L_0x00be
            byte r0 = r8[r3]
            if (r0 != r4) goto L_0x00be
            int r3 = r3 + 1
        L_0x00be:
            r1 = r3
            goto L_0x0035
        L_0x00c1:
            return r17
        L_0x00c2:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Array lengths differ"
            r1.<init>(r0)
            throw r1
        L_0x00ca:
            java.lang.NullPointerException r1 = new java.lang.NullPointerException
            java.lang.String r0 = "Cannot pass null values"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04120Sa.Bzg(java.lang.String, java.lang.String[], long[]):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:164:0x01c1, code lost:
        if (r14 > '9') goto L_0x01c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x00e6, code lost:
        if (r3 > '9') goto L_0x00e8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x0044 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x014d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean BwU(byte[] r31, int r32, int r33, int[] r34, java.lang.String[] r35, long[] r36, float[] r37) {
        /*
            r30 = this;
            r10 = r32
            r11 = r31
            int r8 = r11.length
            r28 = r34
            r0 = r28
            int r0 = r0.length
            r29 = r0
            r27 = r35
            if (r35 == 0) goto L_0x0222
            r0 = r27
            int r0 = r0.length
            r25 = r0
        L_0x0015:
            r9 = r36
            if (r36 == 0) goto L_0x021e
            int r0 = r9.length
            r24 = r0
        L_0x001c:
            r26 = r37
            if (r37 == 0) goto L_0x021a
            r0 = r26
            int r0 = r0.length
            r23 = r0
        L_0x0025:
            r7 = 0
            r6 = 0
        L_0x0027:
            r0 = r29
            if (r7 >= r0) goto L_0x0226
            r5 = r34[r7]
            r0 = r5 & 512(0x200, float:7.175E-43)
            r1 = 34
            if (r0 != 0) goto L_0x0216
            r0 = r5 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x003d
            byte r0 = r31[r10]
            if (r0 == r1) goto L_0x0216
            r5 = r5 & -1025(0xfffffffffffffbff, float:NaN)
        L_0x003d:
            r0 = r5 & 255(0xff, float:3.57E-43)
            char r2 = (char) r0
            r3 = r33
            if (r10 < r3) goto L_0x0046
            r0 = 0
            return r0
        L_0x0046:
            r22 = -1
            r0 = r5 & 512(0x200, float:7.175E-43)
            r12 = r10
            if (r0 != 0) goto L_0x0066
            r0 = r5 & 1024(0x400, float:1.435E-42)
            if (r0 != 0) goto L_0x005b
            r4 = r10
        L_0x0052:
            if (r4 >= r3) goto L_0x0076
            byte r0 = r31[r4]
            if (r0 == r2) goto L_0x0076
            int r4 = r4 + 1
            goto L_0x0052
        L_0x005b:
            byte r1 = r31[r12]
            r0 = 34
            if (r1 == r0) goto L_0x0071
            if (r12 >= r3) goto L_0x0071
            int r12 = r12 + 1
            goto L_0x005b
        L_0x0066:
            if (r12 >= r3) goto L_0x0071
            byte r1 = r31[r12]
            r0 = 41
            if (r1 == r0) goto L_0x0071
            int r12 = r12 + 1
            goto L_0x0066
        L_0x0071:
            int r4 = r12 + 1
            r22 = r12
            goto L_0x0052
        L_0x0076:
            if (r22 >= 0) goto L_0x007a
            r22 = r4
        L_0x007a:
            if (r4 >= r3) goto L_0x008b
            int r4 = r4 + 1
            r0 = r5 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L_0x008b
        L_0x0082:
            if (r4 >= r3) goto L_0x008b
            byte r0 = r31[r4]
            if (r0 != r2) goto L_0x008b
            int r4 = r4 + 1
            goto L_0x0082
        L_0x008b:
            r0 = r5 & 28672(0x7000, float:4.0178E-41)
            if (r0 == 0) goto L_0x014d
            r21 = 0
            r0 = r22
            if (r0 >= r8) goto L_0x0097
            r21 = 1
        L_0x0097:
            if (r21 == 0) goto L_0x00c3
            byte r20 = r31[r22]
            r0 = 0
            r31[r22] = r0
        L_0x009e:
            r0 = r5 & 16384(0x4000, float:2.2959E-41)
            if (r0 == 0) goto L_0x0118
            r0 = r23
            if (r6 >= r0) goto L_0x0118
            if (r37 == 0) goto L_0x0118
            r12 = r10
        L_0x00a9:
            if (r12 >= r8) goto L_0x00c6
            byte r0 = r31[r12]
            char r3 = (char) r0
            r2 = 0
        L_0x00af:
            char[] r1 = X.AnonymousClass0N6.A01
            int r0 = r1.length
            if (r2 >= r0) goto L_0x00c1
            char r0 = r1[r2]
            if (r0 != r3) goto L_0x00be
            r0 = 1
        L_0x00b9:
            if (r0 == 0) goto L_0x00c6
            int r12 = r12 + 1
            goto L_0x00a9
        L_0x00be:
            int r2 = r2 + 1
            goto L_0x00af
        L_0x00c1:
            r0 = 0
            goto L_0x00b9
        L_0x00c3:
            r20 = 0
            goto L_0x009e
        L_0x00c6:
            r17 = 43
            r16 = 45
            r2 = 1
            if (r12 >= r8) goto L_0x00f5
            byte r1 = r31[r12]
            r0 = r16
            if (r1 != r0) goto L_0x00f5
            int r12 = r12 + r2
            r19 = 1
        L_0x00d6:
            r13 = -1
            r2 = 0
        L_0x00d8:
            r15 = 46
            if (r12 >= r8) goto L_0x0102
            byte r14 = r31[r12]
            char r3 = (char) r14
            r0 = 48
            if (r0 > r3) goto L_0x00e8
            r1 = 57
            r0 = 1
            if (r3 <= r1) goto L_0x00e9
        L_0x00e8:
            r0 = 0
        L_0x00e9:
            if (r0 != 0) goto L_0x00f0
            if (r14 != r15) goto L_0x0102
            if (r13 >= 0) goto L_0x0102
            r13 = r2
        L_0x00f0:
            int r12 = r12 + 1
            int r2 = r2 + 1
            goto L_0x00d8
        L_0x00f5:
            if (r12 >= r8) goto L_0x00ff
            byte r1 = r31[r12]
            r0 = r17
            if (r1 != r0) goto L_0x00ff
            int r12 = r12 + 1
        L_0x00ff:
            r19 = 0
            goto L_0x00d6
        L_0x0102:
            int r3 = r12 - r2
            if (r13 >= 0) goto L_0x0212
            r13 = r2
        L_0x0107:
            r1 = 18
            if (r2 <= r1) goto L_0x020e
            int r13 = r13 - r1
        L_0x010c:
            if (r1 != 0) goto L_0x015e
            r2 = 0
        L_0x0110:
            if (r19 == 0) goto L_0x0115
            r0 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            double r2 = r2 * r0
        L_0x0115:
            float r0 = (float) r2
            r37[r6] = r0
        L_0x0118:
            r0 = r5 & 8192(0x2000, float:1.14794E-41)
            if (r0 == 0) goto L_0x012b
            r0 = r24
            if (r6 >= r0) goto L_0x012b
            if (r36 == 0) goto L_0x012b
            r0 = r5 & 2048(0x800, float:2.87E-42)
            if (r0 == 0) goto L_0x0155
            byte r0 = r31[r10]
            long r0 = (long) r0
            r36[r6] = r0
        L_0x012b:
            r0 = r5 & 4096(0x1000, float:5.74E-42)
            if (r0 == 0) goto L_0x0147
            r0 = r25
            if (r6 >= r0) goto L_0x0147
            if (r35 == 0) goto L_0x0147
            r3 = r8
            r2 = r10
            r1 = 0
        L_0x0138:
            if (r2 >= r8) goto L_0x013f
            byte r0 = r31[r2]
            if (r0 != r1) goto L_0x0152
            r3 = r2
        L_0x013f:
            java.lang.String r0 = new java.lang.String
            int r3 = r3 - r10
            r0.<init>(r11, r10, r3)
            r35[r6] = r0
        L_0x0147:
            if (r21 == 0) goto L_0x014b
            r31[r22] = r20
        L_0x014b:
            int r6 = r6 + 1
        L_0x014d:
            int r7 = r7 + 1
            r10 = r4
            goto L_0x0027
        L_0x0152:
            int r2 = r2 + 1
            goto L_0x0138
        L_0x0155:
            r0 = 10
            long r0 = X.AnonymousClass0N6.A00(r11, r10, r0)
            r36[r6] = r0
            goto L_0x012b
        L_0x015e:
            r2 = 0
        L_0x015f:
            r0 = 9
            if (r1 <= r0) goto L_0x0177
            if (r3 >= r8) goto L_0x0177
            byte r0 = r31[r3]
            int r3 = r3 + 1
            if (r0 != r15) goto L_0x016f
            byte r0 = r31[r3]
            int r3 = r3 + 1
        L_0x016f:
            int r2 = r2 * 10
            int r0 = r0 + -48
            int r2 = r2 + r0
            int r1 = r1 + -1
            goto L_0x015f
        L_0x0177:
            r14 = 0
        L_0x0178:
            if (r1 <= 0) goto L_0x018e
            if (r3 >= r8) goto L_0x018e
            byte r0 = r31[r3]
            int r3 = r3 + 1
            if (r0 != r15) goto L_0x0186
            byte r0 = r31[r3]
            int r3 = r3 + 1
        L_0x0186:
            int r14 = r14 * 10
            int r0 = r0 + -48
            int r14 = r14 + r0
            int r1 = r1 + -1
            goto L_0x0178
        L_0x018e:
            r0 = 4741671816366391296(0x41cdcd6500000000, double:1.0E9)
            double r2 = (double) r2
            double r2 = r2 * r0
            double r0 = (double) r14
            double r2 = r2 + r0
            if (r12 >= r8) goto L_0x01de
            byte r1 = r31[r12]
            r0 = 69
            if (r1 == r0) goto L_0x01a3
            r0 = 101(0x65, float:1.42E-43)
            if (r1 != r0) goto L_0x01de
        L_0x01a3:
            int r12 = r12 + 1
            if (r12 >= r8) goto L_0x01cf
            byte r1 = r31[r12]
            r0 = r16
            if (r1 != r0) goto L_0x01cf
            r18 = 1
            int r12 = r12 + r18
            r17 = 1
        L_0x01b3:
            r1 = 0
        L_0x01b4:
            if (r12 >= r8) goto L_0x01e3
            byte r15 = r31[r12]
            char r14 = (char) r15
            r0 = 48
            if (r0 > r14) goto L_0x01c3
            r0 = 57
            r16 = 1
            if (r14 <= r0) goto L_0x01c5
        L_0x01c3:
            r16 = 0
        L_0x01c5:
            if (r16 == 0) goto L_0x01e3
            int r1 = r1 * 10
            int r0 = r15 + -48
            int r1 = r1 + r0
            int r12 = r12 + 1
            goto L_0x01b4
        L_0x01cf:
            r18 = 1
            if (r12 >= r8) goto L_0x01db
            byte r1 = r31[r12]
            r0 = r17
            if (r1 != r0) goto L_0x01db
            int r12 = r12 + 1
        L_0x01db:
            r17 = 0
            goto L_0x01b3
        L_0x01de:
            r18 = 1
            r17 = 0
            r1 = 0
        L_0x01e3:
            if (r17 == 0) goto L_0x0204
            int r13 = r13 - r1
        L_0x01e6:
            if (r13 >= 0) goto L_0x0201
            int r13 = -r13
        L_0x01e9:
            r12 = 511(0x1ff, float:7.16E-43)
            if (r13 > r12) goto L_0x01ee
            r12 = r13
        L_0x01ee:
            r14 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r13 = 0
        L_0x01f1:
            if (r12 == 0) goto L_0x0206
            r0 = r12 & 1
            if (r0 == 0) goto L_0x01fc
            double[] r0 = X.AnonymousClass0N6.A00
            r0 = r0[r13]
            double r14 = r14 * r0
        L_0x01fc:
            int r12 = r12 >> 1
            int r13 = r13 + 1
            goto L_0x01f1
        L_0x0201:
            r18 = 0
            goto L_0x01e9
        L_0x0204:
            int r13 = r13 + r1
            goto L_0x01e6
        L_0x0206:
            if (r18 == 0) goto L_0x020b
            double r2 = r2 / r14
            goto L_0x0110
        L_0x020b:
            double r2 = r2 * r14
            goto L_0x0110
        L_0x020e:
            int r13 = r13 - r2
            r1 = r2
            goto L_0x010c
        L_0x0212:
            int r2 = r2 + -1
            goto L_0x0107
        L_0x0216:
            int r10 = r10 + 1
            goto L_0x003d
        L_0x021a:
            r23 = 0
            goto L_0x0025
        L_0x021e:
            r24 = 0
            goto L_0x001c
        L_0x0222:
            r25 = 0
            goto L_0x0015
        L_0x0226:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04120Sa.BwU(byte[], int, int, int[], java.lang.String[], long[], float[]):boolean");
    }
}
