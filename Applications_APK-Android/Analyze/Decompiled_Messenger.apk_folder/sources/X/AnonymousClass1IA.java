package X;

import android.os.Build;
import android.view.View;

/* renamed from: X.1IA  reason: invalid class name */
public final class AnonymousClass1IA {
    public static void A00(View view, int i) {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 21) {
            z = true;
        }
        if (z) {
            AnonymousClass1vW.A00(view, i);
        }
    }

    public static void A01(AnonymousClass0p4 r3, E76 e76, int i) {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 21) {
            z = true;
        }
        if (z) {
            AnonymousClass1vW.A01(e76, i);
        } else {
            e76.A33(AnonymousClass1IM.A00(r3));
        }
    }
}
