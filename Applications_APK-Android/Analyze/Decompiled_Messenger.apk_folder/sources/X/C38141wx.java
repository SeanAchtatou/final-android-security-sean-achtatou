package X;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.PutDataRequest;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

/* renamed from: X.1wx  reason: invalid class name and case insensitive filesystem */
public final class C38141wx {
    public final AnonymousClass94S A00;
    public final PutDataRequest A01;

    public static C23337Bcn A00(List list, Object obj) {
        String str;
        C23337Bcn bcn = new C23337Bcn();
        if (obj == null) {
            bcn.A00 = 14;
            return bcn;
        }
        C23336Bcm bcm = new C23336Bcm();
        bcn.A01 = bcm;
        if (obj instanceof String) {
            bcn.A00 = 2;
            bcm.A06 = (String) obj;
            return bcn;
        } else if (obj instanceof Integer) {
            bcn.A00 = 6;
            bcm.A02 = ((Integer) obj).intValue();
            return bcn;
        } else if (obj instanceof Long) {
            bcn.A00 = 5;
            bcm.A04 = ((Long) obj).longValue();
            return bcn;
        } else if (obj instanceof Double) {
            bcn.A00 = 3;
            bcm.A00 = ((Double) obj).doubleValue();
            return bcn;
        } else if (obj instanceof Float) {
            bcn.A00 = 4;
            bcm.A01 = ((Float) obj).floatValue();
            return bcn;
        } else if (obj instanceof Boolean) {
            bcn.A00 = 8;
            bcm.A07 = ((Boolean) obj).booleanValue();
            return bcn;
        } else if (obj instanceof Byte) {
            bcn.A00 = 7;
            bcm.A03 = ((Byte) obj).byteValue();
            return bcn;
        } else if (obj instanceof byte[]) {
            bcn.A00 = 1;
            bcm.A08 = (byte[]) obj;
            return bcn;
        } else if (obj instanceof String[]) {
            bcn.A00 = 11;
            bcm.A0D = (String[]) obj;
            return bcn;
        } else if (obj instanceof long[]) {
            bcn.A00 = 12;
            bcm.A0A = (long[]) obj;
            return bcn;
        } else if (obj instanceof float[]) {
            bcn.A00 = 15;
            bcm.A09 = (float[]) obj;
            return bcn;
        } else if (obj instanceof Asset) {
            bcn.A00 = 13;
            list.add((Asset) obj);
            bcm.A05 = (long) (list.size() - 1);
            return bcn;
        } else {
            int i = 0;
            if (obj instanceof AnonymousClass94S) {
                bcn.A00 = 9;
                AnonymousClass94S r14 = (AnonymousClass94S) obj;
                TreeSet treeSet = new TreeSet(r14.A00.keySet());
                C23338Bco[] bcoArr = new C23338Bco[treeSet.size()];
                Iterator it = treeSet.iterator();
                while (it.hasNext()) {
                    String str2 = (String) it.next();
                    C23338Bco bco = new C23338Bco();
                    bcoArr[i] = bco;
                    bco.A01 = str2;
                    bco.A00 = A00(list, r14.A00.get(str2));
                    i++;
                }
                bcn.A01.A0B = bcoArr;
                return bcn;
            } else if (obj instanceof ArrayList) {
                bcn.A00 = 10;
                ArrayList arrayList = (ArrayList) obj;
                C23337Bcn[] bcnArr = new C23337Bcn[arrayList.size()];
                Object obj2 = null;
                int size = arrayList.size();
                int i2 = 14;
                while (i < size) {
                    Object obj3 = arrayList.get(i);
                    C23337Bcn A002 = A00(list, obj3);
                    int i3 = A002.A00;
                    if (i3 == 14 || i3 == 2 || i3 == 6 || i3 == 9) {
                        if (i2 == 14 && i3 != 14) {
                            i2 = i3;
                            obj2 = obj3;
                        } else if (i3 != i2) {
                            String valueOf = String.valueOf(obj2.getClass());
                            String valueOf2 = String.valueOf(obj3.getClass());
                            StringBuilder sb = new StringBuilder(valueOf.length() + 80 + valueOf2.length());
                            sb.append("ArrayList elements must all be of the sameclass, but this one contains a ");
                            sb.append(valueOf);
                            sb.append(" and a ");
                            sb.append(valueOf2);
                            throw new IllegalArgumentException(sb.toString());
                        }
                        bcnArr[i] = A002;
                        i++;
                    } else {
                        String valueOf3 = String.valueOf(obj3.getClass());
                        StringBuilder sb2 = new StringBuilder(valueOf3.length() + AnonymousClass1Y3.A0y);
                        sb2.append("The only ArrayList element types supported by DataBundleUtil are String, Integer, Bundle, and null, but this ArrayList contains a ");
                        sb2.append(valueOf3);
                        throw new IllegalArgumentException(sb2.toString());
                    }
                }
                bcn.A01.A0C = bcnArr;
                return bcn;
            } else {
                String simpleName = obj.getClass().getSimpleName();
                if (simpleName.length() != 0) {
                    str = "newFieldValueFromValue: unexpected value ".concat(simpleName);
                } else {
                    str = new String("newFieldValueFromValue: unexpected value ");
                }
                throw new RuntimeException(str);
            }
        }
    }

    public static C38141wx A01(String str) {
        C205499mQ.A00(str, "path must not be null");
        C205499mQ.A00(str, "path must not be null");
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("An empty path was supplied.");
        } else if (!str.startsWith("/") || str.startsWith("//")) {
            throw new IllegalArgumentException("A path must start with a single / .");
        } else {
            Uri build = new Uri.Builder().scheme("wear").path(str).build();
            C205499mQ.A00(build, ECX.$const$string(165));
            return new C38141wx(new PutDataRequest(build, new Bundle(), null, PutDataRequest.A04), null);
        }
    }

    public PutDataRequest A02() {
        String str;
        AnonymousClass94S r6 = this.A00;
        C23340Bcq bcq = new C23340Bcq();
        ArrayList arrayList = new ArrayList();
        TreeSet treeSet = new TreeSet(r6.A00.keySet());
        C23338Bco[] bcoArr = new C23338Bco[treeSet.size()];
        Iterator it = treeSet.iterator();
        int i = 0;
        while (it.hasNext()) {
            String str2 = (String) it.next();
            Object obj = r6.A00.get(str2);
            C23338Bco bco = new C23338Bco();
            bcoArr[i] = bco;
            bco.A01 = str2;
            bco.A00 = A00(arrayList, obj);
            i++;
        }
        bcq.A00 = bcoArr;
        C23344Bcu bcu = new C23344Bcu(bcq, arrayList);
        PutDataRequest putDataRequest = this.A01;
        C23340Bcq bcq2 = bcu.A00;
        int A04 = bcq2.A04();
        bcq2.A00 = A04;
        byte[] bArr = new byte[A04];
        try {
            C23328Bce bce = new C23328Bce(ByteBuffer.wrap(bArr, 0, A04));
            bcq2.A06(bce);
            if (bce.A00.remaining() == 0) {
                putDataRequest.A01 = bArr;
                int size = bcu.A01.size();
                int i2 = 0;
                while (i2 < size) {
                    String num = Integer.toString(i2);
                    Asset asset = (Asset) bcu.A01.get(i2);
                    if (num == null) {
                        String valueOf = String.valueOf(asset);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 26);
                        sb.append("asset key cannot be null: ");
                        sb.append(valueOf);
                        throw new IllegalStateException(sb.toString());
                    } else if (asset == null) {
                        if (num.length() != 0) {
                            str = "asset cannot be null: key=".concat(num);
                        } else {
                            str = new String("asset cannot be null: key=");
                        }
                        throw new IllegalStateException(str);
                    } else {
                        if (Log.isLoggable(AnonymousClass80H.$const$string(86), 3)) {
                            String.valueOf(asset);
                        }
                        PutDataRequest putDataRequest2 = this.A01;
                        AnonymousClass09E.A01(num);
                        AnonymousClass09E.A01(asset);
                        putDataRequest2.A03.putParcelable(num, asset);
                        i2++;
                    }
                }
                return this.A01;
            }
            throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", Integer.valueOf(bce.A00.remaining())));
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    private C38141wx(PutDataRequest putDataRequest, AnonymousClass94S r7) {
        this.A01 = putDataRequest;
        AnonymousClass94S r4 = new AnonymousClass94S();
        this.A00 = r4;
        if (r7 != null) {
            for (String str : r7.A00.keySet()) {
                r4.A00.put(str, r7.A00.get(str));
            }
        }
    }
}
