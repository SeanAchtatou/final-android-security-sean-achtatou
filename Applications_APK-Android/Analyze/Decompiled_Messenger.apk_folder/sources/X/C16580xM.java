package X;

import android.view.View;
import android.view.ViewGroup;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/* renamed from: X.0xM  reason: invalid class name and case insensitive filesystem */
public final class C16580xM {
    public View A00;
    public ViewGroup A01;
    public C14360tB A02;
    public C16650xU A03;
    public boolean A04 = true;
    private ImmutableSet A05;
    public final C16610xP A06;
    private final C16590xN A07;

    public static final C16580xM A00(AnonymousClass1XY r1) {
        return new C16580xM(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        if (r5.A06.isEmpty() != false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A01(X.C32511ls r6) {
        /*
            r5 = this;
            android.view.ViewGroup r0 = r5.A01
            android.view.View r3 = r6.B90(r0)
            android.view.ViewGroup r2 = r5.A01
            if (r2 == 0) goto L_0x002e
            android.view.View r0 = r5.A00
            if (r0 == 0) goto L_0x003b
            X.AnonymousClass3AP.A00(r0, r3)
        L_0x0011:
            r5.A00 = r3
            boolean r0 = r5.A04
            if (r3 == 0) goto L_0x0027
            if (r0 == 0) goto L_0x0022
            X.0xP r0 = r5.A06
            boolean r1 = r0.isEmpty()
            r0 = 0
            if (r1 == 0) goto L_0x0024
        L_0x0022:
            r0 = 8
        L_0x0024:
            r3.setVisibility(r0)
        L_0x0027:
            X.0tB r0 = r5.A02
            if (r0 == 0) goto L_0x002e
            r0.BPT(r3)
        L_0x002e:
            X.0xN r4 = r5.A07
            java.lang.String r3 = r6.Act()
            java.lang.String r2 = "view"
            r0 = 0
            r4.A01(r2, r2, r3, r0)
            return
        L_0x003b:
            r1 = 2131296723(0x7f0901d3, float:1.821137E38)
            android.view.View r0 = r2.findViewById(r1)
            if (r0 == 0) goto L_0x0011
            android.view.ViewGroup r0 = r5.A01
            android.view.View r0 = r0.findViewById(r1)
            X.AnonymousClass3AP.A00(r0, r3)
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16580xM.A01(X.1ls):void");
    }

    public void A02() {
        C24971Xv it = this.A05.iterator();
        while (it.hasNext()) {
            ((C32511ls) it.next()).onPause();
        }
    }

    public void A03() {
        C24971Xv it = this.A05.iterator();
        while (it.hasNext()) {
            ((C32511ls) it.next()).onResume();
        }
    }

    public void A04(Set set, C16650xU r4) {
        this.A03 = r4;
        ImmutableSet A0A = ImmutableSet.A0A(set);
        this.A05 = A0A;
        C24971Xv it = A0A.iterator();
        while (it.hasNext()) {
            ((C32511ls) it.next()).C6K(this);
        }
    }

    public boolean A05(C32511ls r3) {
        if (this.A06.A03() == r3) {
            this.A06.remove(r3);
            if (this.A06.isEmpty()) {
                View view = this.A00;
                if (view != null) {
                    view.setVisibility(8);
                }
                C14360tB r0 = this.A02;
                if (r0 == null) {
                    return true;
                }
                r0.BPR();
                return true;
            }
            A01((C32511ls) this.A06.A03());
            return true;
        }
        this.A06.remove(r3);
        return false;
    }

    public boolean A06(C32511ls r5) {
        boolean contains = this.A05.contains(r5);
        Class<?> cls = r5.getClass();
        Preconditions.checkState(contains, "%s must be registered before being shown.", cls.getName());
        if (this.A06.A03() == null || this.A03.AeD(cls) < this.A03.AeD(((C32511ls) this.A06.A03()).getClass())) {
            if (this.A01 != null) {
                if (!this.A06.contains(r5)) {
                    this.A06.add(r5);
                }
                A01(r5);
                return true;
            }
        } else if (!this.A06.contains(r5)) {
            this.A06.add(r5);
        }
        return false;
    }

    public C16580xM(AnonymousClass1XY r7) {
        this.A07 = new C16590xN(r7);
        C33101mv r5 = new C33101mv(new C16600xO(this));
        Set<Object> emptySet = Collections.emptySet();
        C16610xP r3 = new C16610xP(r5, Math.min((emptySet instanceof Collection ? Math.max(11, emptySet.size()) : 11) - 1, Integer.MAX_VALUE) + 1);
        for (Object offer : emptySet) {
            r3.offer(offer);
        }
        this.A06 = r3;
    }
}
