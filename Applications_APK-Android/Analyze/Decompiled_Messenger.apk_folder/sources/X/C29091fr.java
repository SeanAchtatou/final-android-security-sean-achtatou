package X;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.HashMap;

/* renamed from: X.1fr  reason: invalid class name and case insensitive filesystem */
public final class C29091fr extends C183512m implements Serializable {
    private static final long serialVersionUID = 7364428299211355871L;
    public final transient Field _field;
    public B9A _serialization;

    public /* bridge */ /* synthetic */ AnnotatedElement getAnnotated() {
        return this._field;
    }

    public Annotation getAnnotation(Class cls) {
        HashMap hashMap;
        C10090jX r0 = this._annotations;
        if (r0 == null || (hashMap = r0._annotations) == null) {
            return null;
        }
        return (Annotation) hashMap.get(cls);
    }

    public Class getDeclaringClass() {
        return this._field.getDeclaringClass();
    }

    public Type getGenericType() {
        return this._field.getGenericType();
    }

    public Member getMember() {
        return this._field;
    }

    public String getName() {
        return this._field.getName();
    }

    public Class getRawType() {
        return this._field.getType();
    }

    public Object getValue(Object obj) {
        try {
            return this._field.get(obj);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Failed to getValue() for field ", getFullName(), ": ", e.getMessage()), e);
        }
    }

    public Object readResolve() {
        B9A b9a = this._serialization;
        Class cls = b9a.clazz;
        try {
            Field declaredField = cls.getDeclaredField(b9a.name);
            if (!declaredField.isAccessible()) {
                C29081fq.checkAndFixAccess(declaredField);
            }
            return new C29091fr(declaredField, null);
        } catch (Exception unused) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Could not find method '", this._serialization.name, "' from Class '", cls.getName()));
        }
    }

    public void setValue(Object obj, Object obj2) {
        try {
            this._field.set(obj, obj2);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Failed to setValue() for field ", getFullName(), ": ", e.getMessage()), e);
        }
    }

    public String toString() {
        return AnonymousClass08S.A0P("[field ", getFullName(), "]");
    }

    public Object writeReplace() {
        return new C29091fr(new B9A(this._field));
    }

    public String getFullName() {
        return AnonymousClass08S.A0P(getDeclaringClass().getName(), "#", getName());
    }

    private C29091fr(B9A b9a) {
        super(null);
        this._field = null;
        this._serialization = b9a;
    }

    public C29091fr(Field field, C10090jX r2) {
        super(r2);
        this._field = field;
    }
}
