package X;

/* renamed from: X.0FE  reason: invalid class name */
public class AnonymousClass0FE {
    public final Object A00;
    public final Object A01;

    public boolean equals(Object obj) {
        AnonymousClass0FE r4;
        Object obj2;
        Object obj3;
        if (obj == null || !(obj instanceof AnonymousClass0FE) || ((obj2 = this.A00) != (obj3 = (r4 = (AnonymousClass0FE) obj).A00) && obj2 != null && !obj2.equals(obj3))) {
            return false;
        }
        Object obj4 = this.A01;
        Object obj5 = r4.A01;
        return obj4 == obj5 || obj4 == null || obj4.equals(obj5);
    }

    public int hashCode() {
        Object obj = this.A00;
        int i = 0;
        if (obj != null) {
            i = 0 ^ obj.hashCode();
        }
        Object obj2 = this.A01;
        if (obj2 != null) {
            return i ^ obj2.hashCode();
        }
        return i;
    }

    public String toString() {
        Object obj = this.A00;
        String str = "<";
        if (obj != null) {
            str = str + obj;
        }
        String A0J = AnonymousClass08S.A0J(str, ":");
        Object obj2 = this.A01;
        if (obj2 != null) {
            A0J = A0J + obj2;
        }
        return AnonymousClass08S.A0J(A0J, ">");
    }

    public AnonymousClass0FE(Object obj, Object obj2) {
        this.A00 = obj;
        this.A01 = obj2;
    }
}
