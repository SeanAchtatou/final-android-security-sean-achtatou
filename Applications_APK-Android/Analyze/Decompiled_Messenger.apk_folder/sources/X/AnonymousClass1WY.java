package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1WY  reason: invalid class name */
public final class AnonymousClass1WY {
    public int A00;
    public int A01;
    public C24551Wa A02;
    public Set A03;
    public final Set A04 = new HashSet();
    public final Set A05 = new HashSet();

    public AnonymousClass1WX A00() {
        boolean z = false;
        if (this.A02 != null) {
            z = true;
        }
        AnonymousClass09E.A09(z, "Missing required property: factory.");
        return new AnonymousClass1WX(new HashSet(this.A05), new HashSet(this.A04), this.A00, this.A01, this.A02, this.A03);
    }

    public void A01(C24601Wf r3) {
        AnonymousClass09E.A02(r3, "Null dependency");
        AnonymousClass09E.A08(!this.A05.contains(r3.A02), "Components are not allowed to depend on interfaces they themselves provide.");
        this.A04.add(r3);
    }

    public AnonymousClass1WY(Class cls, Class... clsArr) {
        this.A00 = 0;
        this.A01 = 0;
        this.A03 = new HashSet();
        AnonymousClass09E.A02(cls, "Null interface");
        this.A05.add(cls);
        for (Class A022 : clsArr) {
            AnonymousClass09E.A02(A022, "Null interface");
        }
        Collections.addAll(this.A05, clsArr);
    }
}
