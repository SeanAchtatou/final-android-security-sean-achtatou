package X;

import android.os.Process;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0Wt  reason: invalid class name and case insensitive filesystem */
public final class C04900Wt extends AnonymousClass0UV {
    public static final AnonymousClass00M A00(AnonymousClass1XY r0) {
        C04910Wu.A00(r0);
        return AnonymousClass00M.A00();
    }

    public static final AnonymousClass00M A01(AnonymousClass1XY r0) {
        C04910Wu.A00(r0);
        return AnonymousClass00M.A00();
    }

    public static final Integer A02() {
        return Integer.valueOf(Process.myPid());
    }
}
