package X;

import com.facebook.messaging.model.threadkey.ThreadKey;

/* renamed from: X.1Gw  reason: invalid class name */
public final class AnonymousClass1Gw {
    public static boolean A00(ThreadKey threadKey) {
        if (threadKey == null || !ThreadKey.A0E(threadKey) || threadKey.A0G() != -102) {
            return false;
        }
        return true;
    }
}
