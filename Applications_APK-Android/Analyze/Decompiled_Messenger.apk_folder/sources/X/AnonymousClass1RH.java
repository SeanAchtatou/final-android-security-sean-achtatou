package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1RH  reason: invalid class name */
public final class AnonymousClass1RH extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public ThreadSummary A01;
    @Comparable(type = 13)
    public C16400x0 A02;
    @Comparable(type = 13)
    public MigColorScheme A03;

    public AnonymousClass1RH(Context context) {
        super("M4ThreadItemSnippetComponent");
        this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }
}
