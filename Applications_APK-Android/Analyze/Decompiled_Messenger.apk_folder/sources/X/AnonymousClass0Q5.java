package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0Q5  reason: invalid class name */
public final class AnonymousClass0Q5 extends Enum implements AnonymousClass0C4 {
    private static final /* synthetic */ AnonymousClass0Q5[] A00;
    public static final AnonymousClass0Q5 A01;
    public static final AnonymousClass0Q5 A02;
    public static final AnonymousClass0Q5 A03;
    public static final AnonymousClass0Q5 A04;
    public static final AnonymousClass0Q5 A05;
    public static final AnonymousClass0Q5 A06;
    public static final AnonymousClass0Q5 A07;
    public static final AnonymousClass0Q5 A08;
    public static final AnonymousClass0Q5 A09;
    public static final AnonymousClass0Q5 A0A;
    public static final AnonymousClass0Q5 A0B;
    public static final AnonymousClass0Q5 A0C;
    public static final AnonymousClass0Q5 A0D;
    public static final AnonymousClass0Q5 A0E;
    public static final AnonymousClass0Q5 A0F;
    private final String mJsonKey;
    private final Class mType;

    static {
        Class<String> cls = String.class;
        AnonymousClass0Q5 r13 = new AnonymousClass0Q5("ServiceName", 0, "sn", cls);
        A0D = r13;
        AnonymousClass0Q5 r11 = new AnonymousClass0Q5("ClientCoreName", 1, "cn", cls);
        A02 = r11;
        AnonymousClass0Q5 r10 = new AnonymousClass0Q5("NotificationStoreName", 2, "nsn", cls);
        A0A = r10;
        AnonymousClass0Q5 r9 = new AnonymousClass0Q5("Country", 3, "ct", cls);
        A03 = r9;
        AnonymousClass0Q5 r8 = new AnonymousClass0Q5("NetworkType", 4, "nt", cls);
        A09 = r8;
        AnonymousClass0Q5 r7 = new AnonymousClass0Q5("NetworkSubtype", 5, "ns", cls);
        A08 = r7;
        AnonymousClass0Q5 r0 = new AnonymousClass0Q5("ConnectionQuality", 6, "cq", cls);
        AnonymousClass0Q5 r14 = new AnonymousClass0Q5("AppState", 7, "as", cls);
        A01 = r14;
        AnonymousClass0Q5 r6 = new AnonymousClass0Q5("ScreenState", 8, "ss", cls);
        A0C = r6;
        AnonymousClass0Q5 r5 = new AnonymousClass0Q5("YearClass", 9, "yc", cls);
        A0F = r5;
        AnonymousClass0Q5 r4 = new AnonymousClass0Q5("MqttGKs", 10, "gk", cls);
        A07 = r4;
        AnonymousClass0Q5 r02 = new AnonymousClass0Q5("MqttQEs", 11, "qe", cls);
        AnonymousClass0Q5 r15 = new AnonymousClass0Q5("MqttFlags", 12, "f", cls);
        A06 = r15;
        AnonymousClass0Q5 r3 = new AnonymousClass0Q5("IsEmployee", 13, "e", cls);
        A05 = r3;
        Class<String> cls2 = cls;
        AnonymousClass0Q5 r21 = new AnonymousClass0Q5("ValidCompatibleApps", 14, "va", cls2);
        A0E = r21;
        AnonymousClass0Q5 r212 = new AnonymousClass0Q5("EnabledCompatibleApps", 15, "ea", cls2);
        A04 = r212;
        AnonymousClass0Q5 r213 = new AnonymousClass0Q5("RegisteredApps", 16, "ra", cls2);
        A0B = r213;
        AnonymousClass0Q5 r28 = r21;
        AnonymousClass0Q5 r29 = r212;
        AnonymousClass0Q5 r30 = r213;
        AnonymousClass0Q5 r16 = r10;
        AnonymousClass0Q5 r17 = r9;
        AnonymousClass0Q5 r18 = r8;
        AnonymousClass0Q5 r19 = r7;
        AnonymousClass0Q5 r142 = r13;
        AnonymousClass0Q5 r152 = r11;
        A00 = new AnonymousClass0Q5[]{r142, r152, r16, r17, r18, r19, r0, r14, r6, r5, r4, r02, r15, r3, r28, r29, r30};
    }

    public static AnonymousClass0Q5 valueOf(String str) {
        return (AnonymousClass0Q5) Enum.valueOf(AnonymousClass0Q5.class, str);
    }

    public static AnonymousClass0Q5[] values() {
        return (AnonymousClass0Q5[]) A00.clone();
    }

    private AnonymousClass0Q5(String str, int i, String str2, Class cls) {
        this.mJsonKey = str2;
        this.mType = cls;
    }

    public String Arl() {
        return this.mJsonKey;
    }

    public Class B8J() {
        return this.mType;
    }
}
