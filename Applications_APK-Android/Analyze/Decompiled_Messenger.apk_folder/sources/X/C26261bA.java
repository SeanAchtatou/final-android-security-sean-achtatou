package X;

import android.content.res.Resources;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.Locale;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1bA  reason: invalid class name and case insensitive filesystem */
public final class C26261bA {
    private static volatile C26261bA A01;
    public final FbSharedPreferences A00;

    public static final C26261bA A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (C26261bA.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        FbSharedPreferences A003 = FbSharedPreferencesModule.A00(applicationInjector);
                        AnonymousClass0ZS.A00(applicationInjector);
                        A01 = new C26261bA(A003);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public Locale A01() {
        if (!this.A00.BFQ()) {
            return Resources.getSystem().getConfiguration().locale;
        }
        String B4F = this.A00.B4F(C26241b8.A00, "device");
        if (B4F.equals("device")) {
            Locale locale = Resources.getSystem().getConfiguration().locale;
            if ("my_ZG".equals(locale.toString())) {
                return new Locale("qz", "ZG");
            }
            return locale;
        }
        Locale A012 = C007006q.A01(B4F);
        if (C06850cB.A0B(A012.getCountry())) {
            return new Locale(A012.getLanguage(), Resources.getSystem().getConfiguration().locale.getCountry());
        }
        return A012;
    }

    private C26261bA(FbSharedPreferences fbSharedPreferences) {
        this.A00 = fbSharedPreferences;
    }
}
