package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.04r  reason: invalid class name */
public final class AnonymousClass04r extends C000900o implements AnonymousClass1XT, AnonymousClass1XU {
    public static final int A00 = ProvidersRegistry.A00.A02("frameworks");
    public static final AnonymousClass04r A01 = new AnonymousClass04r();

    public AnonymousClass04r() {
        super(null);
    }

    public void BN6(AnonymousClass1XV r13) {
        if (TraceEvents.isEnabled(A00)) {
            int i = A00;
            int writeStandardEntry = Logger.writeStandardEntry(i, 7, 22, 0, 0, 0, 0, 0);
            Logger.writeBytesEntry(i, 1, 83, writeStandardEntry, r13.A06);
            int writeBytesEntry = Logger.writeBytesEntry(i, 1, 56, writeStandardEntry, "runnable_parent");
            String A002 = A00(r13);
            int i2 = A00;
            Logger.writeBytesEntry(i2, 1, 57, writeBytesEntry, A002);
            Logger.writeBytesEntry(i2, 1, 57, Logger.writeBytesEntry(i2, 1, 56, writeStandardEntry, "runnable_identifier"), String.valueOf(r13.A04.A00));
            int writeBytesEntry2 = Logger.writeBytesEntry(i2, 1, 56, writeStandardEntry, "app_custom_type");
            int i3 = A00;
            Logger.writeBytesEntry(i3, 1, 57, writeBytesEntry2, String.valueOf(r13.A03));
            if (!r13.A00(1)) {
                Logger.writeBytesEntry(i3, 1, 57, Logger.writeBytesEntry(i3, 1, 56, writeStandardEntry, "indirect_edge"), Boolean.TRUE.toString());
            }
            if (r13.A00(2)) {
                Logger.writeBytesEntry(i3, 1, 57, Logger.writeBytesEntry(i3, 1, 56, writeStandardEntry, "manual_point"), Boolean.TRUE.toString());
            }
        }
    }

    public void BVi(AnonymousClass1XV r11) {
        if (TraceEvents.isEnabled(A00)) {
            Logger.writeStandardEntry(A00, 7, 23, 0, 0, 0, 0, 0);
        }
    }

    public int getTracingProviders() {
        return A00 & TraceEvents.sProviders;
    }

    public boolean isEnabled() {
        return TraceEvents.isEnabled(A00);
    }

    private static String A00(AnonymousClass1XV r0) {
        String A002 = AnonymousClass8t0.A00(r0);
        if (A002 == null) {
            return "null";
        }
        return A002;
    }

    public void disable() {
        C000700l.A09(1367702729, C000700l.A03(490300291));
    }

    public void enable() {
        C000700l.A09(-121883827, C000700l.A03(-369493429));
    }

    public int getSupportedProviders() {
        return A00;
    }
}
