package X;

import android.view.View;

/* renamed from: X.1dd  reason: invalid class name and case insensitive filesystem */
public abstract class C27711dd {
    public View A00(int i) {
        if (!(this instanceof C13020qR)) {
            C29901hA r3 = (C29901hA) this;
            View view = r3.A00.A0I;
            if (view != null) {
                return view.findViewById(i);
            }
            throw new IllegalStateException("Fragment " + r3 + " does not have a view");
        }
        C13020qR r1 = (C13020qR) this;
        if (r1 instanceof AnonymousClass14S) {
            return ((AnonymousClass14S) r1).A00.findViewById(i);
        }
        if (r1 instanceof AnonymousClass14V) {
            return ((AnonymousClass14V) r1).A00.findViewById(i);
        }
        if (!(r1 instanceof C13010qQ)) {
            return null;
        }
        return ((C13010qQ) r1).A00.findViewById(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        if (r0 != null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003a, code lost:
        if (r0 != null) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01() {
        /*
            r2 = this;
            boolean r0 = r2 instanceof X.C13020qR
            if (r0 != 0) goto L_0x0010
            r0 = r2
            X.1hA r0 = (X.C29901hA) r0
            androidx.fragment.app.Fragment r0 = r0.A00
            android.view.View r1 = r0.A0I
            r0 = 0
            if (r1 == 0) goto L_0x000f
            r0 = 1
        L_0x000f:
            return r0
        L_0x0010:
            r1 = r2
            X.0qR r1 = (X.C13020qR) r1
            boolean r0 = r1 instanceof X.AnonymousClass14S
            if (r0 != 0) goto L_0x003d
            boolean r0 = r1 instanceof X.AnonymousClass14V
            if (r0 != 0) goto L_0x0032
            boolean r0 = r1 instanceof X.C13010qQ
            if (r0 == 0) goto L_0x0044
            X.0qQ r1 = (X.C13010qQ) r1
            androidx.fragment.app.FragmentActivity r0 = r1.A00
            android.view.Window r0 = r0.getWindow()
            if (r0 == 0) goto L_0x0030
        L_0x0029:
            android.view.View r0 = r0.peekDecorView()
            r1 = 1
            if (r0 != 0) goto L_0x0031
        L_0x0030:
            r1 = 0
        L_0x0031:
            return r1
        L_0x0032:
            X.14V r1 = (X.AnonymousClass14V) r1
            android.app.Activity r0 = r1.A00
            android.view.Window r0 = r0.getWindow()
            if (r0 == 0) goto L_0x0030
            goto L_0x0029
        L_0x003d:
            X.14S r1 = (X.AnonymousClass14S) r1
            android.view.ViewGroup r0 = r1.A00
            r1 = 0
            if (r0 == 0) goto L_0x0031
        L_0x0044:
            r1 = 1
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27711dd.A01():boolean");
    }
}
