package X;

import com.fasterxml.jackson.core.json.PackageVersion;
import io.card.payment.BuildConfig;
import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.1eV  reason: invalid class name and case insensitive filesystem */
public abstract class C28251eV extends C28261eW {
    public static final BigDecimal BD_MAX_INT = new BigDecimal(BI_MAX_INT);
    public static final BigDecimal BD_MAX_LONG = new BigDecimal(BI_MAX_LONG);
    public static final BigDecimal BD_MIN_INT = new BigDecimal(BI_MIN_INT);
    public static final BigDecimal BD_MIN_LONG = new BigDecimal(BI_MIN_LONG);
    public static final BigInteger BI_MAX_INT = BigInteger.valueOf(2147483647L);
    public static final BigInteger BI_MAX_LONG = BigInteger.valueOf(Long.MAX_VALUE);
    public static final BigInteger BI_MIN_INT = BigInteger.valueOf(-2147483648L);
    public static final BigInteger BI_MIN_LONG = BigInteger.valueOf(Long.MIN_VALUE);
    public byte[] _binaryValue;
    public A21 _byteArrayBuilder = null;
    public boolean _closed;
    public long _currInputProcessed = 0;
    public int _currInputRow = 1;
    public int _currInputRowStart = 0;
    public int _expLength;
    public int _fractLength;
    public int _inputEnd = 0;
    public int _inputPtr = 0;
    public int _intLength;
    public final AnonymousClass11K _ioContext;
    public boolean _nameCopied = false;
    public char[] _nameCopyBuffer = null;
    public C182811d _nextToken;
    public int _numTypesValid = 0;
    public BigDecimal _numberBigDecimal;
    public BigInteger _numberBigInt;
    public double _numberDouble;
    public int _numberInt;
    public long _numberLong;
    public boolean _numberNegative;
    public C28291eZ _parsingContext;
    public final C28281eY _textBuffer;
    public int _tokenInputCol = 0;
    public int _tokenInputRow = 1;
    public long _tokenInputTotal = 0;

    public abstract void _closeInput();

    public Object getEmbeddedObject() {
        return null;
    }

    public abstract boolean loadMore();

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x008d, code lost:
        if (r1 < 0) goto L_0x008f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void _parseNumericValue(int r12) {
        /*
            r11 = this;
            X.11d r2 = r11._currToken
            X.11d r0 = X.C182811d.VALUE_NUMBER_INT
            if (r2 != r0) goto L_0x00a9
            X.1eY r0 = r11._textBuffer
            char[] r8 = r0.getTextBuffer()
            X.1eY r1 = r11._textBuffer
            int r7 = r1._inputStart
            if (r7 >= 0) goto L_0x0013
            r7 = 0
        L_0x0013:
            int r2 = r11._intLength
            boolean r9 = r11._numberNegative
            if (r9 == 0) goto L_0x001b
            int r7 = r7 + 1
        L_0x001b:
            r0 = 9
            r5 = 1
            if (r2 > r0) goto L_0x002c
            int r0 = X.C29491gV.parseInt(r8, r7, r2)
            if (r9 == 0) goto L_0x0027
            int r0 = -r0
        L_0x0027:
            r11._numberInt = r0
            r11._numTypesValid = r5
            return
        L_0x002c:
            r0 = 18
            if (r2 > r0) goto L_0x0064
            r10 = 9
            int r6 = r2 - r10
            int r0 = X.C29491gV.parseInt(r8, r7, r6)
            long r3 = (long) r0
            r0 = 1000000000(0x3b9aca00, double:4.94065646E-315)
            long r3 = r3 * r0
            int r7 = r7 + r6
            int r0 = X.C29491gV.parseInt(r8, r7, r10)
            long r0 = (long) r0
            long r3 = r3 + r0
            if (r9 == 0) goto L_0x0047
            long r3 = -r3
        L_0x0047:
            r0 = 10
            if (r2 != r0) goto L_0x005e
            if (r9 == 0) goto L_0x0056
            r1 = -2147483648(0xffffffff80000000, double:NaN)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x005e
        L_0x0054:
            int r0 = (int) r3
            goto L_0x0027
        L_0x0056:
            r1 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x005e
            goto L_0x0054
        L_0x005e:
            r11._numberLong = r3
            r0 = 2
            r11._numTypesValid = r0
            return
        L_0x0064:
            java.lang.String r5 = r1.contentsAsString()
            boolean r0 = r11._numberNegative     // Catch:{ NumberFormatException -> 0x010a }
            if (r0 == 0) goto L_0x0076
            java.lang.String r6 = X.C29491gV.MIN_LONG_STR_NO_SIGN     // Catch:{ NumberFormatException -> 0x010a }
        L_0x006e:
            int r4 = r6.length()     // Catch:{ NumberFormatException -> 0x010a }
            r3 = 1
            if (r2 < r4) goto L_0x008f
            goto L_0x0079
        L_0x0076:
            java.lang.String r6 = X.C29491gV.MAX_LONG_STR     // Catch:{ NumberFormatException -> 0x010a }
            goto L_0x006e
        L_0x0079:
            if (r2 > r4) goto L_0x0092
            r2 = 0
        L_0x007c:
            if (r2 >= r4) goto L_0x008f
            int r0 = r7 + r2
            char r1 = r8[r0]     // Catch:{ NumberFormatException -> 0x010a }
            char r0 = r6.charAt(r2)     // Catch:{ NumberFormatException -> 0x010a }
            int r1 = r1 - r0
            if (r1 == 0) goto L_0x008a
            goto L_0x008d
        L_0x008a:
            int r2 = r2 + 1
            goto L_0x007c
        L_0x008d:
            if (r1 >= 0) goto L_0x0092
        L_0x008f:
            if (r3 == 0) goto L_0x009e
            goto L_0x0094
        L_0x0092:
            r3 = 0
            goto L_0x008f
        L_0x0094:
            long r0 = java.lang.Long.parseLong(r5)     // Catch:{ NumberFormatException -> 0x010a }
            r11._numberLong = r0     // Catch:{ NumberFormatException -> 0x010a }
            r0 = 2
            r11._numTypesValid = r0     // Catch:{ NumberFormatException -> 0x010a }
            return
        L_0x009e:
            java.math.BigInteger r0 = new java.math.BigInteger     // Catch:{ NumberFormatException -> 0x010a }
            r0.<init>(r5)     // Catch:{ NumberFormatException -> 0x010a }
            r11._numberBigInt = r0     // Catch:{ NumberFormatException -> 0x010a }
            r0 = 4
            r11._numTypesValid = r0     // Catch:{ NumberFormatException -> 0x010a }
            return
        L_0x00a9:
            X.11d r0 = X.C182811d.VALUE_NUMBER_FLOAT
            if (r2 != r0) goto L_0x011d
            r5 = 16
            if (r12 != r5) goto L_0x00e9
            X.1eY r4 = r11._textBuffer     // Catch:{ NumberFormatException -> 0x00fa }
            char[] r0 = r4._resultArray     // Catch:{ NumberFormatException -> 0x00fa }
            if (r0 == 0) goto L_0x00c1
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x00fa }
            r3.<init>(r0)     // Catch:{ NumberFormatException -> 0x00fa }
        L_0x00bc:
            r11._numberBigDecimal = r3     // Catch:{ NumberFormatException -> 0x00fa }
            r11._numTypesValid = r5     // Catch:{ NumberFormatException -> 0x00fa }
            goto L_0x00e8
        L_0x00c1:
            int r2 = r4._inputStart     // Catch:{ NumberFormatException -> 0x00fa }
            if (r2 < 0) goto L_0x00cf
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x00fa }
            char[] r1 = r4._inputBuffer     // Catch:{ NumberFormatException -> 0x00fa }
            int r0 = r4._inputLen     // Catch:{ NumberFormatException -> 0x00fa }
            r3.<init>(r1, r2, r0)     // Catch:{ NumberFormatException -> 0x00fa }
            goto L_0x00bc
        L_0x00cf:
            int r0 = r4._segmentSize     // Catch:{ NumberFormatException -> 0x00fa }
            if (r0 != 0) goto L_0x00de
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x00fa }
            char[] r2 = r4._currentSegment     // Catch:{ NumberFormatException -> 0x00fa }
            r1 = 0
            int r0 = r4._currentSize     // Catch:{ NumberFormatException -> 0x00fa }
            r3.<init>(r2, r1, r0)     // Catch:{ NumberFormatException -> 0x00fa }
            goto L_0x00bc
        L_0x00de:
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x00fa }
            char[] r0 = r4.contentsAsArray()     // Catch:{ NumberFormatException -> 0x00fa }
            r3.<init>(r0)     // Catch:{ NumberFormatException -> 0x00fa }
            goto L_0x00bc
        L_0x00e8:
            return
        L_0x00e9:
            X.1eY r0 = r11._textBuffer     // Catch:{ NumberFormatException -> 0x00fa }
            java.lang.String r0 = r0.contentsAsString()     // Catch:{ NumberFormatException -> 0x00fa }
            double r0 = X.C29491gV.parseDouble(r0)     // Catch:{ NumberFormatException -> 0x00fa }
            r11._numberDouble = r0     // Catch:{ NumberFormatException -> 0x00fa }
            r0 = 8
            r11._numTypesValid = r0     // Catch:{ NumberFormatException -> 0x00fa }
            return
        L_0x00fa:
            r3 = move-exception
            java.lang.String r2 = "Malformed numeric value '"
            X.1eY r0 = r11._textBuffer
            java.lang.String r1 = r0.contentsAsString()
            java.lang.String r0 = "'"
            java.lang.String r2 = X.AnonymousClass08S.A0P(r2, r1, r0)
            goto L_0x0113
        L_0x010a:
            r3 = move-exception
            java.lang.String r1 = "Malformed numeric value '"
            java.lang.String r0 = "'"
            java.lang.String r2 = X.AnonymousClass08S.A0P(r1, r5, r0)
        L_0x0113:
            X.1vs r1 = new X.1vs
            X.3Bu r0 = r11.getCurrentLocation()
            r1.<init>(r2, r0, r3)
            throw r1
        L_0x011d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r0 = 19
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r1.<init>(r0)
            r1.append(r2)
            r0 = 105(0x69, float:1.47E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r11._reportError(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28251eV._parseNumericValue(int):void");
    }

    public static IllegalArgumentException reportInvalidBase64Char(C10350jx r4, int i, int i2, String str) {
        String A0P;
        if (i <= 32) {
            A0P = AnonymousClass08S.A0R("Illegal white space character (code 0x", Integer.toHexString(i), ") as character #", i2 + 1, " of 4-char base64 unit: can only used between units");
        } else {
            char c = r4._paddingChar;
            boolean z = false;
            if (i == c) {
                z = true;
            }
            if (z) {
                A0P = "Unexpected padding character ('" + c + "') as character #" + (i2 + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
            } else if (!Character.isDefined(i) || Character.isISOControl(i)) {
                A0P = AnonymousClass08S.A0P("Illegal character (code 0x", Integer.toHexString(i), ") in base64 content");
            } else {
                A0P = "Illegal character '" + ((char) i) + "' (code 0x" + Integer.toHexString(i) + ") in base64 content";
            }
        }
        if (str != null) {
            A0P = AnonymousClass08S.A0P(A0P, ": ", str);
        }
        return new IllegalArgumentException(A0P);
    }

    private void reportOverflowInt() {
        _reportError("Numeric value (" + getText() + ") out of range of int (" + Integer.MIN_VALUE + " - " + Integer.MAX_VALUE + ")");
    }

    private void reportOverflowLong() {
        _reportError("Numeric value (" + getText() + ") out of range of long (" + Long.MIN_VALUE + " - " + Long.MAX_VALUE + ")");
    }

    public char _decodeEscaped() {
        throw new UnsupportedOperationException();
    }

    public A21 _getByteArrayBuilder() {
        A21 a21 = this._byteArrayBuilder;
        if (a21 == null) {
            this._byteArrayBuilder = new A21();
        } else {
            a21.reset();
        }
        return this._byteArrayBuilder;
    }

    public void _handleEOF() {
        C28291eZ r2 = this._parsingContext;
        boolean z = false;
        if (r2._type == 0) {
            z = true;
        }
        if (!z) {
            _reportInvalidEOF(": expected close marker for " + r2.getTypeDesc() + " (from " + new C64443Bu(this._ioContext._sourceRef, -1, -1, r2._lineNr, r2._columnNr) + ")");
        }
    }

    public void _releaseBuffers() {
        this._textBuffer.releaseBuffers();
        char[] cArr = this._nameCopyBuffer;
        if (cArr != null) {
            this._nameCopyBuffer = null;
            AnonymousClass11K r1 = this._ioContext;
            if (cArr == null) {
                return;
            }
            if (cArr == r1._nameCopyBuffer) {
                r1._nameCopyBuffer = null;
                r1._bufferRecycler._charBuffers[AnonymousClass11P.NAME_COPY_BUFFER.ordinal()] = cArr;
                return;
            }
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
    }

    public void _reportMismatchedEndMarker(int i, char c) {
        StringBuilder sb = new StringBuilder(BuildConfig.FLAVOR);
        C28291eZ r1 = this._parsingContext;
        sb.append(new C64443Bu(this._ioContext._sourceRef, -1, -1, r1._lineNr, r1._columnNr));
        String sb2 = sb.toString();
        _reportError("Unexpected close marker '" + ((char) i) + "': expected '" + c + "' (for " + this._parsingContext.getTypeDesc() + " starting at " + sb2 + ")");
    }

    public void close() {
        if (!this._closed) {
            this._closed = true;
            try {
                _closeInput();
            } finally {
                _releaseBuffers();
            }
        }
    }

    public BigInteger getBigIntegerValue() {
        BigDecimal valueOf;
        long j;
        int i = this._numTypesValid;
        if ((i & 4) == 0) {
            if (i == 0) {
                _parseNumericValue(4);
            }
            int i2 = this._numTypesValid;
            if ((4 & i2) == 0) {
                if ((i2 & 16) != 0) {
                    valueOf = this._numberBigDecimal;
                } else {
                    if ((i2 & 2) != 0) {
                        j = this._numberLong;
                    } else if ((i2 & 1) != 0) {
                        j = (long) this._numberInt;
                    } else if ((i2 & 8) != 0) {
                        valueOf = BigDecimal.valueOf(this._numberDouble);
                    } else {
                        C1926190k.throwInternal();
                        this._numTypesValid |= 4;
                    }
                    this._numberBigInt = BigInteger.valueOf(j);
                    this._numTypesValid |= 4;
                }
                this._numberBigInt = valueOf.toBigInteger();
                this._numTypesValid |= 4;
            }
        }
        return this._numberBigInt;
    }

    public C64443Bu getCurrentLocation() {
        int i = this._inputPtr;
        return new C64443Bu(this._ioContext._sourceRef, -1, (this._currInputProcessed + ((long) i)) - 1, this._currInputRow, (i - this._currInputRowStart) + 1);
    }

    public String getCurrentName() {
        C28291eZ r0;
        C182811d r1 = this._currToken;
        if (r1 == C182811d.START_OBJECT || r1 == C182811d.START_ARRAY) {
            r0 = this._parsingContext._parent;
        } else {
            r0 = this._parsingContext;
        }
        return r0._currentName;
    }

    public BigDecimal getDecimalValue() {
        long j;
        int i = this._numTypesValid;
        if ((i & 16) == 0) {
            if (i == 0) {
                _parseNumericValue(16);
            }
            int i2 = this._numTypesValid;
            if ((16 & i2) == 0) {
                if ((i2 & 8) != 0) {
                    this._numberBigDecimal = new BigDecimal(getText());
                } else if ((i2 & 4) != 0) {
                    this._numberBigDecimal = new BigDecimal(this._numberBigInt);
                } else {
                    if ((i2 & 2) != 0) {
                        j = this._numberLong;
                    } else if ((i2 & 1) != 0) {
                        j = (long) this._numberInt;
                    } else {
                        C1926190k.throwInternal();
                    }
                    this._numberBigDecimal = BigDecimal.valueOf(j);
                }
                this._numTypesValid |= 16;
            }
        }
        return this._numberBigDecimal;
    }

    public double getDoubleValue() {
        int i = this._numTypesValid;
        if ((i & 8) == 0) {
            if (i == 0) {
                _parseNumericValue(8);
            }
            int i2 = this._numTypesValid;
            if ((8 & i2) == 0) {
                if ((i2 & 16) != 0) {
                    this._numberDouble = this._numberBigDecimal.doubleValue();
                } else if ((i2 & 4) != 0) {
                    this._numberDouble = this._numberBigInt.doubleValue();
                } else if ((i2 & 2) != 0) {
                    this._numberDouble = (double) this._numberLong;
                } else if ((i2 & 1) != 0) {
                    this._numberDouble = (double) this._numberInt;
                } else {
                    C1926190k.throwInternal();
                }
                this._numTypesValid |= 8;
            }
        }
        return this._numberDouble;
    }

    public int getIntValue() {
        int i = this._numTypesValid;
        if ((i & 1) == 0) {
            if (i == 0) {
                _parseNumericValue(1);
            }
            int i2 = this._numTypesValid;
            if ((1 & i2) == 0) {
                if ((i2 & 2) != 0) {
                    long j = this._numberLong;
                    int i3 = (int) j;
                    if (((long) i3) != j) {
                        _reportError(AnonymousClass08S.A0P("Numeric value (", getText(), ") out of range of int"));
                    }
                    this._numberInt = i3;
                } else if ((i2 & 4) != 0) {
                    if (BI_MIN_INT.compareTo(this._numberBigInt) > 0 || BI_MAX_INT.compareTo(this._numberBigInt) < 0) {
                        reportOverflowInt();
                    }
                    this._numberInt = this._numberBigInt.intValue();
                } else if ((i2 & 8) != 0) {
                    double d = this._numberDouble;
                    if (d < -2.147483648E9d || d > 2.147483647E9d) {
                        reportOverflowInt();
                    }
                    this._numberInt = (int) this._numberDouble;
                } else if ((i2 & 16) != 0) {
                    if (BD_MIN_INT.compareTo(this._numberBigDecimal) > 0 || BD_MAX_INT.compareTo(this._numberBigDecimal) < 0) {
                        reportOverflowInt();
                    }
                    this._numberInt = this._numberBigDecimal.intValue();
                } else {
                    C1926190k.throwInternal();
                }
                this._numTypesValid |= 1;
            }
        }
        return this._numberInt;
    }

    public long getLongValue() {
        int i = this._numTypesValid;
        if ((i & 2) == 0) {
            if (i == 0) {
                _parseNumericValue(2);
            }
            int i2 = this._numTypesValid;
            if ((2 & i2) == 0) {
                if ((i2 & 1) != 0) {
                    this._numberLong = (long) this._numberInt;
                } else if ((i2 & 4) != 0) {
                    if (BI_MIN_LONG.compareTo(this._numberBigInt) > 0 || BI_MAX_LONG.compareTo(this._numberBigInt) < 0) {
                        reportOverflowLong();
                    }
                    this._numberLong = this._numberBigInt.longValue();
                } else if ((i2 & 8) != 0) {
                    double d = this._numberDouble;
                    if (d < -9.223372036854776E18d || d > 9.223372036854776E18d) {
                        reportOverflowLong();
                    }
                    this._numberLong = (long) this._numberDouble;
                } else if ((i2 & 16) != 0) {
                    if (BD_MIN_LONG.compareTo(this._numberBigDecimal) > 0 || BD_MAX_LONG.compareTo(this._numberBigDecimal) < 0) {
                        reportOverflowLong();
                    }
                    this._numberLong = this._numberBigDecimal.longValue();
                } else {
                    C1926190k.throwInternal();
                }
                this._numTypesValid |= 2;
            }
        }
        return this._numberLong;
    }

    public C29501gW getNumberType() {
        if (this._numTypesValid == 0) {
            _parseNumericValue(0);
        }
        if (this._currToken == C182811d.VALUE_NUMBER_INT) {
            int i = this._numTypesValid;
            if ((i & 1) != 0) {
                return C29501gW.INT;
            }
            if ((i & 2) != 0) {
                return C29501gW.LONG;
            }
            return C29501gW.BIG_INTEGER;
        } else if ((this._numTypesValid & 16) != 0) {
            return C29501gW.BIG_DECIMAL;
        } else {
            return C29501gW.DOUBLE;
        }
    }

    public Number getNumberValue() {
        if (this._numTypesValid == 0) {
            _parseNumericValue(0);
        }
        if (this._currToken == C182811d.VALUE_NUMBER_INT) {
            int i = this._numTypesValid;
            if ((i & 1) != 0) {
                return Integer.valueOf(this._numberInt);
            }
            if ((i & 2) != 0) {
                return Long.valueOf(this._numberLong);
            }
            if ((i & 4) != 0) {
                return this._numberBigInt;
            }
        } else {
            int i2 = this._numTypesValid;
            if ((i2 & 16) == 0) {
                if ((i2 & 8) == 0) {
                    C1926190k.throwInternal();
                }
                return Double.valueOf(this._numberDouble);
            }
        }
        return this._numberBigDecimal;
    }

    public C64443Bu getTokenLocation() {
        Object obj = this._ioContext._sourceRef;
        long j = this._tokenInputTotal;
        int i = this._tokenInputRow;
        int i2 = this._tokenInputCol;
        if (i2 >= 0) {
            i2++;
        }
        return new C64443Bu(obj, -1, j, i, i2);
    }

    public boolean hasTextCharacters() {
        C182811d r1 = this._currToken;
        if (r1 == C182811d.VALUE_STRING) {
            return true;
        }
        if (r1 == C182811d.FIELD_NAME) {
            return this._nameCopied;
        }
        return false;
    }

    public void reportInvalidNumber(String str) {
        _reportError(AnonymousClass08S.A0J("Invalid numeric value: ", str));
    }

    public void reportUnexpectedNumberChar(int i, String str) {
        String A0P = AnonymousClass08S.A0P("Unexpected character (", C28261eW._getCharDesc(i), ") in numeric value");
        if (str != null) {
            A0P = AnonymousClass08S.A0P(A0P, ": ", str);
        }
        _reportError(A0P);
    }

    public final C182811d resetAsNaN(String str, double d) {
        C28281eY r3 = this._textBuffer;
        r3._inputBuffer = null;
        r3._inputStart = -1;
        r3._inputLen = 0;
        r3._resultString = str;
        r3._resultArray = null;
        if (r3._hasSegments) {
            C28281eY.clearSegments(r3);
        }
        r3._currentSize = 0;
        this._numberDouble = d;
        this._numTypesValid = 8;
        return C182811d.VALUE_NUMBER_FLOAT;
    }

    public final C182811d resetInt(boolean z, int i) {
        this._numberNegative = z;
        this._intLength = i;
        this._fractLength = 0;
        this._expLength = 0;
        this._numTypesValid = 0;
        return C182811d.VALUE_NUMBER_INT;
    }

    public C11780nw version() {
        return PackageVersion.VERSION;
    }

    public C28251eV(AnonymousClass11K r6, int i) {
        this._features = i;
        this._ioContext = r6;
        this._textBuffer = new C28281eY(r6._bufferRecycler);
        this._parsingContext = new C28291eZ(null, 0, 1, 0);
    }

    public float getFloatValue() {
        return (float) getDoubleValue();
    }

    public final void loadMoreGuaranteed() {
        if (!loadMore()) {
            _reportInvalidEOF(" in " + this._currToken);
        }
    }

    public final int _decodeBase64Escape(C10350jx r3, char c, int i) {
        if (c == '\\') {
            char _decodeEscaped = _decodeEscaped();
            if (_decodeEscaped <= ' ' && i == 0) {
                return -1;
            }
            int decodeBase64Char = r3.decodeBase64Char(_decodeEscaped);
            if (decodeBase64Char >= 0) {
                return decodeBase64Char;
            }
            throw reportInvalidBase64Char(r3, _decodeEscaped, i, null);
        }
        throw reportInvalidBase64Char(r3, c, i, null);
    }

    public final int _decodeBase64Escape(C10350jx r3, int i, int i2) {
        int i3;
        if (i == 92) {
            char _decodeEscaped = _decodeEscaped();
            if (_decodeEscaped <= ' ' && i2 == 0) {
                return -1;
            }
            if (_decodeEscaped <= 127) {
                i3 = r3._asciiToBase64[_decodeEscaped];
            } else {
                i3 = -1;
            }
            if (i3 >= 0) {
                return i3;
            }
            throw reportInvalidBase64Char(r3, _decodeEscaped, i2, null);
        }
        throw reportInvalidBase64Char(r3, i, i2, null);
    }
}
