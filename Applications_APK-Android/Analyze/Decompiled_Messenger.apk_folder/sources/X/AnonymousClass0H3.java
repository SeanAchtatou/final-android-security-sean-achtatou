package X;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.rti.push.service.FbnsService;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0H3  reason: invalid class name */
public final class AnonymousClass0H3 {
    public static void A01(Context context, Bundle bundle) {
        SharedPreferences.Editor edit = AnonymousClass0B2.A00(context, AnonymousClass07B.A01).edit();
        for (String next : bundle.keySet()) {
            try {
                AnonymousClass0HA r0 = (AnonymousClass0HA) Enum.valueOf(AnonymousClass0HA.class, next);
                AnonymousClass0HB r3 = r0.mWrapper;
                String name = r0.name();
                try {
                    r3.A03(edit, r0.mPrefKey, r3.A02(bundle, name, null));
                } catch (ClassCastException e) {
                    C010708t.A0L("KeyValueWrapper", "bundleToSharedPrefs got ClassCastException", e);
                }
            } catch (IllegalArgumentException e2) {
                C010708t.A0U("FbnsClient", e2, "aidlBundleKey: %s not exist in FbnsAIDLConstants", next);
            }
        }
        AnonymousClass0B4.A00(edit);
    }

    public static void A03(Context context, String str, String str2, boolean z, String str3, String str4, AnonymousClass08E r12) {
        if (z && context.getPackageName().equals(str3)) {
            A04(context, true, str);
        }
        ComponentName componentName = new ComponentName(str3, str);
        Intent intent = new Intent(str4);
        intent.setComponent(componentName);
        if (str2 != null) {
            intent.putExtra("caller", str2);
        }
        if (r12 != null) {
            String str5 = r12.A03;
            if (str5 != null) {
                intent.putExtra("caller", str5);
            }
            long j = r12.A00;
            if (j != 0) {
                intent.putExtra("EXPIRED_SESSION", j);
            }
            Boolean bool = r12.A01;
            if (bool != null) {
                intent.putExtra("EXPLICIT_DELIVERY_ACK", bool);
            }
            Integer num = r12.A02;
            if (num != null) {
                intent.putExtra("DELIVERY_RETRY_INTERVAL", num);
            }
        }
        new C012309k(context, null).A02(intent);
    }

    private static void A04(Context context, boolean z, String str) {
        ComponentName componentName = new ComponentName(context, str);
        PackageManager packageManager = context.getPackageManager();
        int i = 2;
        if (z) {
            i = 1;
        }
        packageManager.setComponentEnabledSetting(componentName, i, 1);
        componentName.getShortClassName();
    }

    public static void A00(Context context) {
        List<ActivityManager.RunningServiceInfo> list;
        boolean z;
        String A03 = FbnsService.A03(context.getPackageName());
        boolean z2 = false;
        try {
            int componentEnabledSetting = context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, A03));
            if (componentEnabledSetting == 1 || componentEnabledSetting == 0) {
                z2 = true;
            }
        } catch (Exception unused) {
        }
        if (z2) {
            String packageName = context.getPackageName();
            C009207y r4 = C009207y.A01;
            try {
                list = ((ActivityManager) r4.A01(context, "activity", ActivityManager.class)).getRunningServices(Integer.MAX_VALUE);
            } catch (NullPointerException e) {
                C010708t.A0R("RtiGracefulSystemMethodHelper", e, "Failed to getRunningServices");
                AnonymousClass09P r1 = r4.A00;
                if (r1 != null) {
                    r1.softReport("RtiGracefulSystemMethodHelper", "getRunningServices", e);
                }
                list = null;
            }
            if (list != null) {
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo) it.next();
                    String className = runningServiceInfo.service.getClassName();
                    String packageName2 = runningServiceInfo.service.getPackageName();
                    if (A03.equals(className) && packageName.equals(packageName2)) {
                        z = runningServiceInfo.started;
                        break;
                    }
                }
            }
            z = false;
            if (z) {
                Intent intent = new Intent("Orca.STOP");
                intent.setComponent(new ComponentName(context.getPackageName(), A03));
                new C012309k(context, null).A02(intent);
            }
            A04(context, false, A03);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void A02(Context context, String str, String str2, String str3, boolean z, C012309k r9) {
        if (!TextUtils.isEmpty(str)) {
            if (str2 == null) {
                str2 = context.getPackageName();
            }
            if (context.getPackageName().equals(str2)) {
                A04(context, true, str3);
            }
            Intent intent = new Intent("com.facebook.rti.fbns.intent.REGISTER");
            intent.setComponent(new ComponentName(str2, str3));
            intent.putExtra("pkg_name", context.getPackageName());
            intent.putExtra("appid", str);
            if (z) {
                intent.putExtra("local_generation", true);
            }
            r9.A02(intent);
            return;
        }
        throw new IllegalArgumentException("Missing appId");
    }
}
