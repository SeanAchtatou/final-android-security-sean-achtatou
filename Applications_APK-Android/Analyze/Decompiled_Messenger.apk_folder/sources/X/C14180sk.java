package X;

import android.util.LruCache;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;

/* renamed from: X.0sk  reason: invalid class name and case insensitive filesystem */
public class C14180sk {
    public final LruCache A00 = new LruCache(10);
    public final C04310Tq A01;
    private final C04310Tq A02;
    private final C04310Tq A03;

    public final void A09(int i, int i2, float f) {
        C191028uk A05 = A05(this);
        Object valueOf = Integer.valueOf(i);
        LruCache lruCache = this.A00;
        if (valueOf == null) {
            valueOf = "NULL";
        }
        lruCache.put(A05, valueOf);
        Boolean bool = false;
        A05.A01 = bool.booleanValue();
        A05.A07(i, i2, f);
    }

    public void A0A(String str) {
        if (this instanceof C14160si) {
            C14160si r1 = (C14160si) this;
            if (r1.A0G()) {
                r1.A08(null, str);
            }
        }
    }

    public static final C191028uk A05(C14180sk r1) {
        C191028uk r0 = (C191028uk) r1.A03.get();
        r0.A07 = r1;
        return r0;
    }

    public static void A06(C14180sk r3, String str, C71243c4 r5) {
        String str2;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) r3.A02.get()).A01("android_sound_player_event"), 38);
        if (uSLEBaseShape0S0000000.A0G()) {
            if (r5 instanceof C191258vJ) {
                str2 = "Remote sound not available";
            } else if (r5 == C191288vM.A00) {
                str2 = "Noop sound (likely incorrect setup)";
            } else {
                str2 = "Sound not available (unknown reason)";
            }
            uSLEBaseShape0S0000000.A0D("sound_type", str);
            uSLEBaseShape0S0000000.A0A("volume", Float.valueOf(0.0f));
            uSLEBaseShape0S0000000.A08("was_played", false);
            uSLEBaseShape0S0000000.A0D("reason_not_played", str2);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public static void A07(C14180sk r2, String str, C71243c4 r4, float f) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) r2.A02.get()).A01("android_sound_player_event"), 38);
        if (uSLEBaseShape0S0000000.A0G()) {
            if (f == -1.0f && (r4 instanceof C71273c7)) {
                f = ((C71273c7) r4).B9e();
            }
            uSLEBaseShape0S0000000.A0D("sound_type", str);
            uSLEBaseShape0S0000000.A0A("volume", Float.valueOf(f));
            uSLEBaseShape0S0000000.A08("was_played", false);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public final C191028uk A08(C191028uk r3, String str) {
        if (r3 == null) {
            r3 = A05(this);
        } else {
            r3.A06();
        }
        C71243c4 A012 = ((C71233c3) this.A01.get()).A01(str);
        if (A012.A02(r3, 1)) {
            A07(this, str, A012, -1.0f);
            return r3;
        }
        A06(this, str, A012);
        this.A00.remove(r3);
        return null;
    }

    public final boolean A0B(String str) {
        C71243c4 A012 = ((C71233c3) this.A01.get()).A01(str);
        if (!(A012 instanceof C191258vJ)) {
            return true;
        }
        C191258vJ r1 = (C191258vJ) A012;
        if (r1.A02.A02(r1.A00, r1.A01, null) != null) {
            return true;
        }
        return false;
    }

    public C14180sk(C04310Tq r3, C04310Tq r4, C04310Tq r5) {
        this.A03 = r3;
        this.A01 = r4;
        this.A02 = r5;
    }
}
