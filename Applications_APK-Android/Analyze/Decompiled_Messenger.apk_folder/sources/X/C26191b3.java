package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1b3  reason: invalid class name and case insensitive filesystem */
public final class C26191b3 {
    public static C26191b3 A04;
    public AnonymousClass09P A00;
    public Map A01 = new HashMap();
    public Map A02 = new HashMap();
    public boolean A03 = false;

    public boolean A02(AnonymousClass0f7 r9) {
        int i;
        int i2;
        int i3;
        boolean z = false;
        if (!(r9 == null || r9.A02() == -1)) {
            synchronized (r9) {
                i = r9.A00;
            }
            if (i != 0) {
                if (this.A01.containsKey(r9) && this.A01.get(r9) != null && 5 < ((Integer) this.A01.get(r9)).intValue()) {
                    double intValue = (double) ((Integer) this.A01.get(r9)).intValue();
                    synchronized (r9) {
                        i3 = r9.A00;
                    }
                    if ((intValue / ((double) i3)) * 100.0d > 1.0d && 0 != 0) {
                        z = true;
                    }
                }
                if (this.A02.containsKey(r9) && this.A02.get(r9) != null && 5 < ((Integer) this.A02.get(r9)).intValue()) {
                    double intValue2 = (double) ((Integer) this.A02.get(r9)).intValue();
                    synchronized (r9) {
                        i2 = r9.A00;
                    }
                    if ((intValue2 / ((double) i2)) * 100.0d <= 50.0d || 0 == 0) {
                        return z;
                    }
                    return true;
                }
            }
        }
        return z;
    }

    public static void A00(Map map, AnonymousClass0f7 r3) {
        if (r3 != null && r3.A02() != -1) {
            if (!map.containsKey(r3) || map.get(r3) == null) {
                map.put(r3, 1);
            } else {
                map.put(r3, Integer.valueOf(((Integer) map.get(r3)).intValue() + 1));
            }
        }
    }

    public void A01(AnonymousClass0f7 r4, Exception exc) {
        if (!(r4 == null || r4.A02() == -1)) {
            A00(this.A01, r4);
        }
        this.A00.CGZ("MobileBoost", "BoosterFailsReleaseWithException", exc);
    }

    public C26191b3(AnonymousClass09P r2) {
        if (r2 != null) {
            this.A00 = r2;
            this.A03 = true;
            return;
        }
        this.A00 = new C26201b4();
    }
}
