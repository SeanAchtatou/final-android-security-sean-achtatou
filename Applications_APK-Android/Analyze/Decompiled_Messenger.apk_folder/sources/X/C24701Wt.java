package X;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

/* renamed from: X.1Wt  reason: invalid class name and case insensitive filesystem */
public final class C24701Wt {
    public String A00;
    private int A01;
    private int A02 = 0;
    private String A03;
    private final Context A04;

    public static final synchronized void A02(C24701Wt r2) {
        synchronized (r2) {
            PackageInfo A002 = r2.A00(r2.A04.getPackageName());
            if (A002 != null) {
                r2.A03 = Integer.toString(A002.versionCode);
                r2.A00 = A002.versionName;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005a, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0071, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int A03() {
        /*
            r6 = this;
            monitor-enter(r6)
            int r0 = r6.A02     // Catch:{ all -> 0x0072 }
            if (r0 != 0) goto L_0x0070
            android.content.Context r0 = r6.A04     // Catch:{ all -> 0x0072 }
            android.content.pm.PackageManager r5 = r0.getPackageManager()     // Catch:{ all -> 0x0072 }
            java.lang.String r0 = "com.google.android.c2dm.permission.SEND"
            java.lang.String r4 = "com.google.android.gms"
            int r1 = r5.checkPermission(r0, r4)     // Catch:{ all -> 0x0072 }
            r0 = -1
            r2 = 0
            if (r1 != r0) goto L_0x001f
            java.lang.String r1 = "FirebaseInstanceId"
            java.lang.String r0 = "Google Play services missing or without correct permission."
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x0072 }
            goto L_0x0059
        L_0x001f:
            boolean r0 = X.AnonymousClass2XR.A04()     // Catch:{ all -> 0x0072 }
            r3 = 1
            if (r0 != 0) goto L_0x0040
            android.content.Intent r1 = new android.content.Intent     // Catch:{ all -> 0x0072 }
            java.lang.String r0 = "com.google.android.c2dm.intent.REGISTER"
            r1.<init>(r0)     // Catch:{ all -> 0x0072 }
            r1.setPackage(r4)     // Catch:{ all -> 0x0072 }
            java.util.List r0 = r5.queryIntentServices(r1, r2)     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x0040
            int r0 = r0.size()     // Catch:{ all -> 0x0072 }
            if (r0 <= 0) goto L_0x0040
            r6.A02 = r3     // Catch:{ all -> 0x0072 }
            monitor-exit(r6)
            return r3
        L_0x0040:
            android.content.Intent r1 = new android.content.Intent     // Catch:{ all -> 0x0072 }
            java.lang.String r0 = "com.google.iid.TOKEN_REQUEST"
            r1.<init>(r0)     // Catch:{ all -> 0x0072 }
            r1.setPackage(r4)     // Catch:{ all -> 0x0072 }
            java.util.List r0 = r5.queryBroadcastReceivers(r1, r2)     // Catch:{ all -> 0x0072 }
            r2 = 2
            if (r0 == 0) goto L_0x005b
            int r0 = r0.size()     // Catch:{ all -> 0x0072 }
            if (r0 <= 0) goto L_0x005b
            r6.A02 = r2     // Catch:{ all -> 0x0072 }
        L_0x0059:
            monitor-exit(r6)
            return r2
        L_0x005b:
            java.lang.String r1 = "FirebaseInstanceId"
            java.lang.String r0 = "Failed to resolve IID implementation package, falling back"
            android.util.Log.w(r1, r0)     // Catch:{ all -> 0x0072 }
            boolean r0 = X.AnonymousClass2XR.A04()     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x006d
            r6.A02 = r2     // Catch:{ all -> 0x0072 }
        L_0x006a:
            int r0 = r6.A02     // Catch:{ all -> 0x0072 }
            goto L_0x0070
        L_0x006d:
            r6.A02 = r3     // Catch:{ all -> 0x0072 }
            goto L_0x006a
        L_0x0070:
            monitor-exit(r6)
            return r0
        L_0x0072:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24701Wt.A03():int");
    }

    public final synchronized int A04() {
        PackageInfo A002;
        if (this.A01 == 0 && (A002 = A00("com.google.android.gms")) != null) {
            this.A01 = A002.versionCode;
        }
        return this.A01;
    }

    public final synchronized String A05() {
        if (this.A03 == null) {
            A02(this);
        }
        return this.A03;
    }

    private final PackageInfo A00(String str) {
        try {
            return this.A04.getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(valueOf.length() + 23);
            sb.append("Failed to find package ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    public C24701Wt(Context context) {
        this.A04 = context;
    }

    public static String A01(AnonymousClass1WN r3) {
        AnonymousClass1WN.A02(r3);
        String str = r3.A02.A01;
        if (str != null) {
            return str;
        }
        AnonymousClass1WN.A02(r3);
        String str2 = r3.A02.A00;
        if (str2.startsWith("1:")) {
            String[] split = str2.split(":");
            if (split.length >= 2) {
                str2 = split[1];
                if (str2.isEmpty()) {
                    return null;
                }
            }
            return null;
        }
        return str2;
    }
}
