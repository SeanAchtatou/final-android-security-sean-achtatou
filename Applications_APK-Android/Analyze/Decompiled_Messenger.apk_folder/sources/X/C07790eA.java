package X;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import com.facebook.common.util.StringLocaleUtil;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eA  reason: invalid class name and case insensitive filesystem */
public final class C07790eA {
    public static final ImmutableList A0B = ImmutableList.of("HierarchicalSessionManagerListenerManager");
    private static volatile C07790eA A0C;
    public AnonymousClass0UN A00;
    public String A01 = new String();
    public LinkedList A02 = new LinkedList();
    public List A03 = new ArrayList();
    private String A04;
    public final AnonymousClass1YL A05;
    public final AnonymousClass1YL A06;
    public final AnonymousClass1YL A07;
    public final LinkedList A08 = new LinkedList();
    public final LinkedList A09;
    public final Map A0A = new HashMap();

    public static synchronized String A01(C07790eA r1) {
        synchronized (r1) {
            if (r1.A08.isEmpty()) {
                return null;
            }
            String A0G = ((C12330pA) r1.A08.getFirst()).A0G();
            return A0G;
        }
    }

    public static synchronized void A03(C07790eA r2, C12330pA r3) {
        synchronized (r2) {
            r2.A08.addFirst(r3);
            r2.A0A.put(Integer.valueOf(r3.A0E), r3);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public C12350pC A04(String str) {
        LinkedList linkedList;
        synchronized (this) {
            linkedList = new LinkedList(this.A08);
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            C12330pA r2 = (C12330pA) it.next();
            if (!str.equals(r2.A0D())) {
                C97954mH r0 = r2.A03;
                while (true) {
                    if (r0 == null) {
                        r2 = null;
                        continue;
                        break;
                    } else if (str.equals(r0.A07)) {
                        r2 = r0;
                        continue;
                        break;
                    } else {
                        r0 = (C97954mH) r2.A0I.get(r0.A0E);
                    }
                }
                if (r2 != null) {
                }
            }
            return r2;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    public synchronized String A05() {
        if (((C001500z) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BH4, this.A00)) == C001500z.A03) {
            if (!this.A02.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append(A02((C860946s) this.A02.getFirst()));
                LinkedList linkedList = this.A02;
                for (C860946s A022 : linkedList.subList(1, linkedList.size())) {
                    sb.insert(0, '|');
                    sb.insert(0, A02(A022));
                }
                this.A04 = sb.toString();
                return sb.toString();
            }
            String str = this.A04;
            if (str != null) {
                return str;
            }
        }
        return null;
    }

    public synchronized List A06() {
        LinkedList linkedList;
        linkedList = new LinkedList();
        Iterator it = this.A09.iterator();
        while (it.hasNext()) {
            linkedList.addFirst(((C10900l2) it.next()).A02);
        }
        return linkedList;
    }

    public synchronized List A07() {
        LinkedList linkedList;
        linkedList = new LinkedList();
        Iterator it = this.A08.iterator();
        while (it.hasNext()) {
            C12330pA r5 = (C12330pA) it.next();
            LinkedList linkedList2 = new LinkedList();
            C97954mH r2 = r5.A03;
            if (r2 == null || !r5.A0K) {
                linkedList2.add(C12330pA.A01(r5.A0G, r5.A0F));
            } else {
                while (r2.A0H.size() == 1) {
                    r2 = (C97954mH) r2.A0H.values().toArray()[0];
                    linkedList2.add(C12330pA.A01(r2.A0G, r2.A0D));
                }
                C97954mH r22 = r5.A03;
                if (!C12350pC.A00.contains(AnonymousClass07V.A01(r22.A0D))) {
                    linkedList2.add(C12330pA.A01(r22.A0G, r22.A0D));
                }
                while (true) {
                    Integer num = r22.A0E;
                    if (num == null) {
                        break;
                    }
                    r22 = (C97954mH) r5.A0I.get(num);
                    if (!C12350pC.A00.contains(AnonymousClass07V.A01(r22.A0D))) {
                        linkedList2.add(C12330pA.A01(r22.A0G, r22.A0D));
                    }
                }
                String str = r5.A0G;
                if (str != null && !str.equals("unknown")) {
                    linkedList2.add(C12330pA.A01(str, r5.A0F));
                }
            }
            linkedList.addAll(linkedList2);
        }
        return linkedList;
    }

    public synchronized List A08() {
        LinkedList linkedList;
        linkedList = new LinkedList();
        Iterator it = this.A08.iterator();
        while (it.hasNext()) {
            C12330pA r4 = (C12330pA) it.next();
            LinkedList linkedList2 = new LinkedList();
            C97954mH r0 = r4.A03;
            if (r0 != null) {
                while (r0.A0H.size() == 1) {
                    r0 = (C97954mH) r0.A0H.values().toArray()[0];
                    linkedList2.addFirst(r0);
                }
                C97954mH r02 = r4.A03;
                linkedList2.add(r02);
                while (true) {
                    Integer num = r02.A0E;
                    if (num == null) {
                        break;
                    }
                    r02 = (C97954mH) r4.A0I.get(num);
                    linkedList2.add(r02);
                }
            }
            linkedList2.add(r4);
            linkedList.addAll(linkedList2);
        }
        return linkedList;
    }

    public synchronized void A09() {
        if (!this.A08.isEmpty()) {
            List<C12350pC> A082 = A08();
            LinkedList linkedList = new LinkedList();
            loop0:
            while (true) {
                C860946s r4 = null;
                for (C12350pC r8 : A082) {
                    if (r8 instanceof C12340pB) {
                        C12340pB r6 = (C12340pB) r8;
                        if (C30372Ev5.class.isAssignableFrom(r8.A08())) {
                            if (r4 != null) {
                                linkedList.addFirst(r4);
                                r4 = null;
                            } else if (r6.A0K() != null) {
                                linkedList.addFirst(new C860946s(r6.A0K(), r6.A0G(), r6.A05(), r6.A0H()));
                            }
                        }
                        if (C30374Ev7.class.isAssignableFrom(r8.A08()) && r6.A0K() != null) {
                            linkedList.addFirst(new C860946s(r6.A0K(), r6.A0G(), r6.A05(), r6.A0H()));
                        } else if (r6.A0K() != null) {
                            r4 = new C860946s(r6.A0K(), r6.A0G(), r6.A05(), r6.A0H());
                        }
                    }
                }
                break loop0;
            }
            this.A02 = linkedList;
        }
    }

    public synchronized void A0A(Fragment fragment) {
        C97954mH r2;
        Activity activity = (Activity) AnonymousClass065.A00(fragment.A1j(), Activity.class);
        if (activity != null) {
            Iterator it = this.A08.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                C12330pA r4 = (C12330pA) it.next();
                if (r4.A08() == activity.getClass() && r4.A0E == activity.hashCode()) {
                    if (fragment != null && r4.A0I.containsKey(Integer.valueOf(fragment.hashCode()))) {
                        Fragment fragment2 = fragment.A0L;
                        if (fragment2 == null) {
                            r2 = (C97954mH) r4.A0I.get(Integer.valueOf(fragment.hashCode()));
                            r4.A0J.remove(Integer.valueOf(fragment.hashCode()));
                        } else {
                            C97954mH r3 = (C97954mH) r4.A0I.get(Integer.valueOf(fragment2.hashCode()));
                            if (r3 != null) {
                                r2 = (C97954mH) r4.A0I.get(Integer.valueOf(fragment.hashCode()));
                                r3.A0H.remove(Integer.valueOf(r2.A0C));
                            }
                            C97954mH r0 = r4.A03;
                            if (r0 != null && r0.A0C == fragment.hashCode()) {
                                r4.A03 = null;
                            }
                            r4.A0I.remove(Integer.valueOf(fragment.hashCode()));
                        }
                        C12330pA.A03(r4, r2);
                        C97954mH r02 = r4.A03;
                        r4.A03 = null;
                        r4.A0I.remove(Integer.valueOf(fragment.hashCode()));
                    }
                }
            }
        }
    }

    public static final C07790eA A00(AnonymousClass1XY r4) {
        if (A0C == null) {
            synchronized (C07790eA.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r4);
                if (A002 != null) {
                    try {
                        A0C = new C07790eA(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    private static String A02(C860946s r4) {
        return StringLocaleUtil.A00("%s:%s:%s:%s", r4.A03, r4.A01, Integer.valueOf(r4.A00), r4.A02);
    }

    private C07790eA(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A09 = new LinkedList();
        this.A06 = new C07720e3();
        this.A07 = new C08170en();
        this.A05 = new C08160em();
    }
}
