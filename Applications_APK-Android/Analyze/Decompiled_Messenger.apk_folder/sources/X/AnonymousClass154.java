package X;

/* renamed from: X.154  reason: invalid class name */
public final class AnonymousClass154 extends C14590te {
    private Boolean A00 = null;
    private final C04310Tq A01;

    public String Amb() {
        return "zero_rating";
    }

    public static final AnonymousClass154 A00(AnonymousClass1XY r1) {
        return new AnonymousClass154(r1);
    }

    public boolean BEx() {
        if (this.A00 == null) {
            this.A00 = (Boolean) this.A01.get();
        }
        return this.A00.booleanValue();
    }

    private AnonymousClass154(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.A7j, r2);
    }
}
