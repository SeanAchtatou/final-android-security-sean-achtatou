package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.base.Supplier;

/* renamed from: X.1eo  reason: invalid class name and case insensitive filesystem */
public final class C28441eo implements Supplier {
    public final /* synthetic */ SQLiteDatabase A00;

    public C28441eo(SQLiteDatabase sQLiteDatabase) {
        this.A00 = sQLiteDatabase;
    }

    public Object get() {
        return this.A00;
    }
}
