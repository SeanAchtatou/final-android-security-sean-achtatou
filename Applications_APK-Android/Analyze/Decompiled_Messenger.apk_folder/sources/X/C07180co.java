package X;

import com.google.common.base.Preconditions;
import java.nio.charset.Charset;
import java.util.Arrays;

/* renamed from: X.0co  reason: invalid class name and case insensitive filesystem */
public abstract class C07180co {
    public C07180co A02(byte b) {
        if (!(this instanceof C21341Gj)) {
            C07170cl r0 = (C07170cl) this;
            r0.A0A(b);
            return r0;
        }
        C21341Gj r2 = (C21341Gj) this;
        r2.A02.put(b);
        if (r2.A02.remaining() < 8) {
            C21341Gj.A00(r2);
        }
        return r2;
    }

    public C07180co A03(int i) {
        if (!(this instanceof C21341Gj)) {
            C07170cl r1 = (C07170cl) this;
            r1.A00.putInt(i);
            C07170cl.A00(r1, 4);
            return r1;
        }
        C21341Gj r2 = (C21341Gj) this;
        r2.A02.putInt(i);
        if (r2.A02.remaining() < 8) {
            C21341Gj.A00(r2);
        }
        return r2;
    }

    public C07180co A04(long j) {
        if (!(this instanceof C21341Gj)) {
            C07170cl r1 = (C07170cl) this;
            r1.A00.putLong(j);
            C07170cl.A00(r1, 8);
            return r1;
        }
        C21341Gj r2 = (C21341Gj) this;
        r2.A02.putLong(j);
        if (r2.A02.remaining() < 8) {
            C21341Gj.A00(r2);
        }
        return r2;
    }

    public C07180co A06(byte[] bArr) {
        if (!(this instanceof C21341Gj)) {
            C07170cl r0 = (C07170cl) this;
            Preconditions.checkNotNull(bArr);
            r0.A0B(bArr);
            return r0;
        }
        C21341Gj r2 = (C21341Gj) this;
        C21341Gj.A01(r2, bArr, 0, bArr.length);
        return r2;
    }

    public C07180co A07(byte[] bArr) {
        if (!(this instanceof C21341Gj)) {
            C07170cl r0 = (C07170cl) this;
            r0.A06(bArr);
            return r0;
        }
        C21341Gj r02 = (C21341Gj) this;
        r02.A06(bArr);
        return r02;
    }

    public C07180co A08(byte[] bArr, int i, int i2) {
        if (!(this instanceof C21341Gj)) {
            C07170cl r2 = (C07170cl) this;
            Preconditions.checkPositionIndexes(i, i + i2, bArr.length);
            r2.A0C(bArr, i, i2);
            return r2;
        }
        C21341Gj r0 = (C21341Gj) this;
        C21341Gj.A01(r0, bArr, i, i2);
        return r0;
    }

    public C07190cq A09() {
        if (!(this instanceof C25431Zp)) {
            C21341Gj r1 = (C21341Gj) this;
            C21341Gj.A00(r1);
            r1.A02.flip();
            if (r1.A02.remaining() > 0) {
                r1.A0C(r1.A02);
            }
            return r1.A0A();
        }
        C25431Zp r2 = (C25431Zp) this;
        Preconditions.checkState(!r2.A00, "Cannot re-use a Hasher after calling hash() on it");
        r2.A00 = true;
        return new C25441Zq(r2.A01 == r2.A02.getDigestLength() ? r2.A02.digest() : Arrays.copyOf(r2.A02.digest(), r2.A01));
    }

    public C07180co A05(CharSequence charSequence, Charset charset) {
        return A06(charSequence.toString().getBytes(charset));
    }
}
