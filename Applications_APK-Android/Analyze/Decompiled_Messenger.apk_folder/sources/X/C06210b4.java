package X;

/* renamed from: X.0b4  reason: invalid class name and case insensitive filesystem */
public final class C06210b4 implements C06840cA {
    public C06360bN A00;
    public AnonymousClass00K A01;
    public AnonymousClass0US A02;

    private void A00() {
        this.A00 = new C06360bN((long) C0o(), (long) C0o(), (long) C0o(), (long) C0o());
    }

    public int AO3() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWK, ((C25751aL) this.A02.get()).A00)).AqL(18577391412641806L, 60);
    }

    public C06360bN Ae0() {
        if (this.A00 == null) {
            A00();
        }
        return this.A00;
    }

    public C06360bN AnO() {
        if (this.A00 == null) {
            A00();
        }
        return this.A00;
    }

    public boolean BFp() {
        return this.A01.A1n;
    }

    public int BLK() {
        return this.A01.A0S;
    }

    public int C0o() {
        return this.A01.A0Z;
    }

    public boolean CEP() {
        return this.A01.A1p;
    }

    public C06210b4(AnonymousClass00K r1, AnonymousClass0US r2) {
        this.A01 = r1;
        this.A02 = r2;
    }
}
