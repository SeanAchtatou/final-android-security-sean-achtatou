package X;

import io.card.payment.BuildConfig;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.HashMap;

/* renamed from: X.137  reason: invalid class name */
public final class AnonymousClass137 extends C183512m {
    private static final long serialVersionUID = 1;
    public final int _index;
    public final C184112y _owner;
    public final Type _type;

    public AnnotatedElement getAnnotated() {
        return null;
    }

    public String getName() {
        return BuildConfig.FLAVOR;
    }

    public Annotation getAnnotation(Class cls) {
        HashMap hashMap;
        C10090jX r0 = this._annotations;
        if (r0 == null || (hashMap = r0._annotations) == null) {
            return null;
        }
        return (Annotation) hashMap.get(cls);
    }

    public Class getDeclaringClass() {
        return this._owner.getDeclaringClass();
    }

    public Type getGenericType() {
        return this._type;
    }

    public Member getMember() {
        return this._owner.getMember();
    }

    public Class getRawType() {
        Type type = this._type;
        if (type instanceof Class) {
            return (Class) type;
        }
        return C10300js.instance._constructType(type, null)._class;
    }

    public Object getValue(Object obj) {
        throw new UnsupportedOperationException(AnonymousClass08S.A0J("Cannot call getValue() on constructor parameter of ", getDeclaringClass().getName()));
    }

    public void setValue(Object obj, Object obj2) {
        throw new UnsupportedOperationException(AnonymousClass08S.A0J("Cannot call setValue() on constructor parameter of ", getDeclaringClass().getName()));
    }

    public String toString() {
        return "[parameter #" + this._index + ", annotations: " + this._annotations + "]";
    }

    public AnonymousClass137(C184112y r1, Type type, C10090jX r3, int i) {
        super(r3);
        this._owner = r1;
        this._type = type;
        this._index = i;
    }
}
