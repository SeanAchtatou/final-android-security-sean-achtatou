package X;

/* renamed from: X.0mr  reason: invalid class name and case insensitive filesystem */
public final class C11400mr extends AnonymousClass1c2 {
    private static final long serialVersionUID = 1;

    public AnonymousClass1c2 createInstance(C10490kF r2, C28271eX r3, CYj cYj) {
        return new C11400mr(this, r2, r3, cYj);
    }

    public AnonymousClass1c2 with(AnonymousClass1c6 r2) {
        return new C11400mr(this, r2);
    }

    private C11400mr(C11400mr r1, C10490kF r2, C28271eX r3, CYj cYj) {
        super(r1, r2, r3, cYj);
    }

    private C11400mr(C11400mr r1, AnonymousClass1c6 r2) {
        super(r1, r2);
    }

    public C11400mr(AnonymousClass1c6 r2) {
        super(r2, (C26841cE) null);
    }
}
