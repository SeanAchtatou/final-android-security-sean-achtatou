package X;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1YR  reason: invalid class name */
public final class AnonymousClass1YR {
    private static volatile AnonymousClass1YR A04;
    public AnonymousClass1YS A00;
    public AnonymousClass1YS A01;
    public AnonymousClass0UN A02;
    public final AtomicBoolean A03 = new AtomicBoolean(true);

    public void A01(String str) {
        AnonymousClass07A.A04((AnonymousClass0VL) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AZx, this.A02), new C25081Yg(this, str, ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A02)).now(), null), -994127369);
    }

    public static final AnonymousClass1YR A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass1YR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass1YR(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private AnonymousClass1YR(AnonymousClass1XY r8) {
        AnonymousClass0UN r1 = new AnonymousClass0UN(7, r8);
        this.A02 = r1;
        this.A01 = new AnonymousClass1YS((AnonymousClass1YT) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Avc, r1), false, (AnonymousClass1YZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BTp, r1));
        int i = AnonymousClass1Y3.Avc;
        AnonymousClass0UN r2 = this.A02;
        this.A00 = new AnonymousClass1YS((AnonymousClass1YT) AnonymousClass1XX.A02(5, i, r2), true, (AnonymousClass1YZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BTp, r2));
    }
}
