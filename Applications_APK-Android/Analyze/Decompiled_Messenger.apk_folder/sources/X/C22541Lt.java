package X;

import android.content.Context;

/* renamed from: X.1Lt  reason: invalid class name and case insensitive filesystem */
public final class C22541Lt {
    public static boolean A00(Context context) {
        C29211g3 r0 = (C29211g3) AnonymousClass065.A00(context, C29211g3.class);
        if (r0 == null || !r0.AdG().equals("chat_heads")) {
            return false;
        }
        return true;
    }
}
