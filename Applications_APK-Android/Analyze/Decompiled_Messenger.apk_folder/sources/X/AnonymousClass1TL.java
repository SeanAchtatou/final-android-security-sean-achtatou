package X;

/* renamed from: X.1TL  reason: invalid class name */
public final class AnonymousClass1TL {
    public AnonymousClass1GL A00;
    public boolean A01 = false;
    public int A02 = 0;
    public final AnonymousClass0XX A03;
    public final AnonymousClass0VL A04;
    public final String A05;
    public final String A06;
    public final String A07;

    public AnonymousClass1TL(AnonymousClass0XX r3, AnonymousClass1GL r4, AnonymousClass108 r5) {
        this.A04 = r5.A00;
        this.A03 = r3;
        String str = r5.A02;
        this.A06 = r5.A04;
        this.A00 = r4;
        this.A07 = str;
        this.A05 = r5.A03;
    }
}
