package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.facebook.acra.constants.ErrorReportingConstants;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.0pc  reason: invalid class name and case insensitive filesystem */
public class C12570pc extends C12580pd implements C11470nD, AnonymousClass09C {
    public static final String __redex_internal_original_name = "com.facebook.ui.dialogs.FbDialogFragment";
    public AnonymousClass0UN A00;
    public List A01;
    private AnonymousClass6K9 A02;
    public final Object A03 = new Object();
    public final CopyOnWriteArrayList A04 = new CopyOnWriteArrayList();
    public volatile boolean A05;

    public boolean BPB() {
        return false;
    }

    private void A04(C29801h0 r4) {
        C101604sp r0 = (C101604sp) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B8D, this.A00);
        synchronized (r0.A01) {
            r0.A01.add(r4);
        }
        this.A04.add(r4);
    }

    public static void A05(C12570pc r3, C29801h0 r4) {
        C101604sp r0 = (C101604sp) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B8D, r3.A00);
        synchronized (r0.A01) {
            r0.A01.remove(r4);
        }
        r3.A04.remove(r4);
    }

    public void A1v(View view, Bundle bundle) {
        boolean z;
        Dialog dialog = this.A09;
        Bundle bundle2 = this.A0G;
        if (bundle2 == null) {
            z = true;
        } else {
            z = !bundle2.getBoolean("disable_host_activity_overrides");
        }
        if (z && dialog != null) {
            C123045qx.A00(dialog);
        }
        C101604sp r4 = (C101604sp) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B8D, this.A00);
        for (C29801h0 BWH : r4.A00) {
            BWH.BWH(this);
        }
        synchronized (r4.A01) {
            for (C29801h0 BWH2 : r4.A01) {
                BWH2.BWH(this);
            }
        }
        super.A1v(view, bundle);
    }

    public Dialog A20(Bundle bundle) {
        boolean z;
        C121265nx r3 = new C121265nx(this, A1j(), A1y());
        Bundle bundle2 = this.A0G;
        if (bundle2 == null) {
            z = true;
        } else {
            z = !bundle2.getBoolean("disable_host_activity_overrides");
        }
        if (z) {
            C123045qx.A01(r3);
        }
        return r3;
    }

    public View A2C(int i) {
        return C012809p.A01(this.A0I, i);
    }

    public void A2D(C29801h0 r3) {
        if (r3 == null) {
            return;
        }
        if (this.A05) {
            A04(r3);
            return;
        }
        synchronized (this.A03) {
            if (this.A05) {
                A04(r3);
            } else {
                if (this.A01 == null) {
                    this.A01 = new ArrayList();
                }
                this.A01.add(r3);
            }
        }
    }

    public boolean A2E() {
        if (this.A0f || !A1X() || this.A0Y || !A1a()) {
            return false;
        }
        return true;
    }

    public static void A06(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        String str2;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        StringBuilder sb = new StringBuilder(128);
        sb.append(view.getClass().getName());
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(view)));
        sb.append(' ');
        int visibility = view.getVisibility();
        char c = 'V';
        char c2 = '.';
        if (visibility == 0) {
            sb.append('V');
        } else if (visibility == 4) {
            sb.append('I');
        } else if (visibility != 8) {
            sb.append('.');
        } else {
            sb.append('G');
        }
        char c3 = 'F';
        char c4 = '.';
        if (view.isFocusable()) {
            c4 = 'F';
        }
        sb.append(c4);
        char c5 = '.';
        if (view.isEnabled()) {
            c5 = 'E';
        }
        sb.append(c5);
        char c6 = 'D';
        if (view.willNotDraw()) {
            c6 = '.';
        }
        sb.append(c6);
        char c7 = '.';
        if (view.isHorizontalScrollBarEnabled()) {
            c7 = 'H';
        }
        sb.append(c7);
        if (!view.isVerticalScrollBarEnabled()) {
            c = '.';
        }
        sb.append(c);
        char c8 = '.';
        if (view.isClickable()) {
            c8 = 'C';
        }
        sb.append(c8);
        char c9 = '.';
        if (view.isLongClickable()) {
            c9 = 'L';
        }
        sb.append(c9);
        sb.append(' ');
        if (!view.isFocused()) {
            c3 = '.';
        }
        sb.append(c3);
        char c10 = '.';
        if (view.isSelected()) {
            c10 = 'S';
        }
        sb.append(c10);
        if (view.isPressed()) {
            c2 = 'P';
        }
        sb.append(c2);
        sb.append(' ');
        sb.append(view.getLeft());
        sb.append(',');
        sb.append(view.getTop());
        sb.append('-');
        sb.append(view.getRight());
        sb.append(',');
        sb.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            sb.append(" #");
            sb.append(Integer.toHexString(id));
            Resources resources = view.getResources();
            if (!(id == 0 || resources == null)) {
                int i = -16777216 & id;
                if (i == 16777216) {
                    str2 = "android";
                } else if (i != 2130706432) {
                    try {
                        str2 = resources.getResourcePackageName(id);
                    } catch (Resources.NotFoundException unused) {
                    }
                } else {
                    str2 = ErrorReportingConstants.APP_NAME_KEY;
                }
                String resourceTypeName = resources.getResourceTypeName(id);
                String resourceEntryName = resources.getResourceEntryName(id);
                sb.append(" ");
                sb.append(str2);
                sb.append(":");
                sb.append(resourceTypeName);
                sb.append("/");
                sb.append(resourceEntryName);
            }
        }
        sb.append("}");
        printWriter.println(sb.toString());
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String A0J = AnonymousClass08S.A0J(str, "  ");
            for (int i2 = 0; i2 < childCount; i2++) {
                A06(A0J, printWriter, viewGroup.getChildAt(i2));
            }
        }
    }

    public void A1c(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.A1c(str, fileDescriptor, printWriter, strArr);
        View view = this.A0I;
        if (view != null) {
            printWriter.write(str);
            printWriter.write("FbDialogFragment View Hierarchy:\n");
            A06(str, printWriter, view);
        }
    }

    public void A1h(Bundle bundle) {
        int A022 = C000700l.A02(1455026319);
        super.A1h(bundle);
        this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(A1j()));
        synchronized (this.A03) {
            try {
                List list = this.A01;
                if (list != null) {
                    C101604sp r0 = (C101604sp) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B8D, this.A00);
                    synchronized (r0.A01) {
                        r0.A01.addAll(list);
                    }
                    this.A04.addAll(list);
                    this.A01 = null;
                }
                this.A05 = true;
            } catch (Throwable th) {
                while (true) {
                    C000700l.A08(-1074422681, A022);
                    throw th;
                }
            }
        }
        C000700l.A08(-1440950049, A022);
    }

    public Context A1j() {
        Context A1j = super.A1j();
        if (this.A0A) {
            AnonymousClass6K9 r0 = this.A02;
            if (r0 == null || r0.getBaseContext() != A1j) {
                this.A02 = new AnonymousClass6K9(A1j);
            }
            return this.A02;
        }
        this.A02 = null;
        return A1j;
    }

    public View A1k(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int A022 = C000700l.A02(-2012448608);
        View A1k = super.A1k(layoutInflater, viewGroup, bundle);
        C000700l.A08(1514102195, A022);
        return A1k;
    }

    public void A1l() {
        int A022 = C000700l.A02(-603755127);
        C101604sp r0 = (C101604sp) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B8D, this.A00);
        CopyOnWriteArrayList copyOnWriteArrayList = this.A04;
        synchronized (r0.A01) {
            r0.A01.removeAll(copyOnWriteArrayList);
        }
        super.A1l();
        C000700l.A08(990311704, A022);
    }

    public void A1m() {
        int A022 = C000700l.A02(454401071);
        super.A1m();
        C101604sp r3 = (C101604sp) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B8D, this.A00);
        for (C29801h0 BWF : r3.A00) {
            BWF.BWF(this);
        }
        synchronized (r3.A01) {
            for (C29801h0 BWF2 : r3.A01) {
                BWF2.BWF(this);
            }
        }
        C000700l.A08(1601625266, A022);
    }

    public void A29(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, View view) {
        super.A29(layoutInflater, viewGroup, bundle, view);
        ((C11650nY) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AS7, this.A00)).A01(this);
    }

    public void A2A(boolean z, boolean z2) {
        C11650nY r0;
        super.A2A(z, z2);
        if (z && (r0 = (C11650nY) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AS7, this.A00)) != null) {
            r0.A02(this);
        }
    }

    public Activity A2B() {
        return (Activity) AnonymousClass065.A00(A1j(), Activity.class);
    }

    public boolean A2F() {
        if (AnonymousClass065.A00(A1j(), Activity.class) != null) {
            return true;
        }
        return false;
    }

    public void BNH(int i, int i2, Intent intent) {
        super.BNH(i, i2, intent);
        C101604sp r3 = (C101604sp) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B8D, this.A00);
        for (C29801h0 BNH : r3.A00) {
            BNH.BNH(i, i2, intent);
        }
        synchronized (r3.A01) {
            for (C29801h0 BNH2 : r3.A01) {
                BNH2.BNH(i, i2, intent);
            }
        }
    }

    public Object BzI(Class cls) {
        C12570pc r0 = this;
        if (!cls.isInstance(this)) {
            r0 = null;
        }
        if (r0 != null) {
            return r0;
        }
        Fragment fragment = this.A0L;
        if (fragment instanceof C11470nD) {
            return ((C11470nD) fragment).BzI(cls);
        }
        Context A1j = A1j();
        if (A1j instanceof C11470nD) {
            return ((C11470nD) A1j).BzI(cls);
        }
        return null;
    }
}
