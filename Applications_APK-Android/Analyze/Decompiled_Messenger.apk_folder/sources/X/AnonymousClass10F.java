package X;

/* renamed from: X.10F  reason: invalid class name */
public enum AnonymousClass10F {
    AUTO_EXPOSURE("auto"),
    MANUAL_EXPOSURE("man");
    
    public String mValue;

    public String toString() {
        return this.mValue;
    }

    private AnonymousClass10F(String str) {
        this.mValue = str;
    }
}
