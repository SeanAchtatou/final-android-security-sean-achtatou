package X;

import android.util.SparseArray;
import com.facebook.common.callercontext.CallerContextable;

/* renamed from: X.1oV  reason: invalid class name and case insensitive filesystem */
public final class C34011oV implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.prefetcher.ThreadsPrefetchHelper";
    public SparseArray A00;
    public AnonymousClass0UN A01;
    public boolean A02 = false;
    public final C33891oJ A03;
    public final Object A04 = new Object();

    public C34011oV(AnonymousClass1XY r3, C33891oJ r4) {
        this.A01 = new AnonymousClass0UN(7, r3);
        this.A03 = r4;
        this.A00 = new SparseArray();
    }
}
