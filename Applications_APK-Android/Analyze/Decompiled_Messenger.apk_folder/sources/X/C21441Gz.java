package X;

/* renamed from: X.1Gz  reason: invalid class name and case insensitive filesystem */
public final class C21441Gz {
    private AnonymousClass0UN A00;
    public final Boolean A01;

    public static final C21441Gz A00(AnonymousClass1XY r1) {
        return new C21441Gz(r1);
    }

    public boolean A01(String str) {
        C72253dx queryStatus;
        if (!this.A01.booleanValue() || (queryStatus = ((C72243dw) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AL1, this.A00)).queryStatus(str)) == null || queryStatus.A00() == null) {
            return false;
        }
        return true;
    }

    public C21441Gz(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = AnonymousClass0UU.A08(r3);
    }
}
