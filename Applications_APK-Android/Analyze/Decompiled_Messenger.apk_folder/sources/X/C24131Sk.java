package X;

import android.graphics.Rect;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.1Sk  reason: invalid class name and case insensitive filesystem */
public class C24131Sk extends AnonymousClass19S {
    public boolean A00 = false;
    public int A01 = -1;
    public C26299Cvd A02 = new C26325Cw4();
    public int[] A03;
    public View[] A04;
    public final Rect A05 = new Rect();
    public final SparseIntArray A06 = new SparseIntArray();
    public final SparseIntArray A07 = new SparseIntArray();

    public static int A00(C24131Sk r3, int i, int i2) {
        int i3;
        int i4;
        if (r3.A01 != 1 || !r3.A25()) {
            int[] iArr = r3.A03;
            i3 = iArr[i2 + i];
            i4 = iArr[i];
        } else {
            int[] iArr2 = r3.A03;
            int i5 = r3.A01 - i;
            i3 = iArr2[i5];
            i4 = iArr2[i5 - i2];
        }
        return i3 - i4;
    }

    public static int A01(C24131Sk r3, C15740vp r4, C15930wD r5, int i) {
        if (!r5.A08) {
            return r3.A02.A01(i, r3.A01);
        }
        int i2 = r3.A06.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int A032 = r4.A03(i);
        if (A032 != -1) {
            return r3.A02.A01(A032, r3.A01);
        }
        Log.w("GridLayoutManager", AnonymousClass08S.A09("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:", i));
        return 0;
    }

    public static int A02(C24131Sk r3, C15740vp r4, C15930wD r5, int i) {
        if (!r5.A08) {
            return r3.A02.A00(i);
        }
        int i2 = r3.A07.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int A032 = r4.A03(i);
        if (A032 != -1) {
            return r3.A02.A00(A032);
        }
        Log.w("GridLayoutManager", AnonymousClass08S.A09("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:", i));
        return 1;
    }

    private int A06(C15740vp r4, C15930wD r5, int i) {
        if (!r5.A08) {
            return this.A02.A02(i, this.A01);
        }
        int A032 = r4.A03(i);
        if (A032 != -1) {
            return this.A02.A02(A032, this.A01);
        }
        Log.w("GridLayoutManager", AnonymousClass08S.A09("Cannot find span size for pre layout position. ", i));
        return 0;
    }

    private void A07() {
        View[] viewArr = this.A04;
        if (viewArr == null || viewArr.length != this.A01) {
            this.A04 = new View[this.A01];
        }
    }

    private void A08(int i) {
        if (i != this.A01) {
            this.A00 = true;
            if (i >= 1) {
                this.A01 = i;
                this.A02.A00.clear();
                A0y();
                return;
            }
            throw new IllegalArgumentException(AnonymousClass08S.A09("Span count should be at least 1. Provided ", i));
        }
    }

    public static void A09(C24131Sk r2) {
        int A0e;
        int A0h;
        if (r2.A01 == 1) {
            A0e = r2.A04 - r2.A0g();
            A0h = r2.A0f();
        } else {
            A0e = r2.A01 - r2.A0e();
            A0h = r2.A0h();
        }
        A0A(r2, A0e - A0h);
    }

    public static void A0A(C24131Sk r7, int i) {
        int i2;
        int length;
        int[] iArr = r7.A03;
        int i3 = r7.A01;
        if (!(iArr != null && (length = iArr.length) == i3 + 1 && iArr[length - 1] == i)) {
            iArr = new int[(i3 + 1)];
        }
        int i4 = 0;
        iArr[0] = 0;
        int i5 = i / i3;
        int i6 = i % i3;
        int i7 = 0;
        for (int i8 = 1; i8 <= i3; i8++) {
            i4 += i6;
            if (i4 <= 0 || i3 - i4 >= i6) {
                i2 = i5;
            } else {
                i2 = i5 + 1;
                i4 -= i3;
            }
            i7 += i2;
            iArr[i8] = i7;
        }
        r7.A03 = iArr;
    }

    public int A1d(C15740vp r3, C15930wD r4) {
        if (this.A01 == 1) {
            return this.A01;
        }
        if (r4.A00() < 1) {
            return 0;
        }
        return A06(r3, r4, r4.A00() - 1) + 1;
    }

    public int A1e(C15740vp r3, C15930wD r4) {
        if (this.A01 == 0) {
            return this.A01;
        }
        if (r4.A00() < 1) {
            return 0;
        }
        return A06(r3, r4, r4.A00() - 1) + 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004b, code lost:
        if (A25() == false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00e7, code lost:
        if (r10 == r0) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0108, code lost:
        if (r14.A0A.A01(r26, 24579) == false) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0118, code lost:
        if (r10 == r4) goto L_0x00a7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00bd A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A1f(android.view.View r30, int r31, X.C15740vp r32, X.C15930wD r33) {
        /*
            r29 = this;
            r4 = r29
            r14 = r4
            r2 = r30
            android.view.View r13 = r4.A0v(r2)
            r24 = 0
            if (r13 == 0) goto L_0x0127
            android.view.ViewGroup$LayoutParams r0 = r13.getLayoutParams()
            X.Cau r0 = (X.C25158Cau) r0
            int r12 = r0.A00
            int r0 = r0.A01
            int r11 = r12 + r0
            r1 = r31
            r15 = r33
            r28 = r32
            r0 = r28
            android.view.View r0 = super.A1f(r2, r1, r0, r15)
            if (r0 == 0) goto L_0x0127
            int r0 = r4.A1w(r1)
            r3 = 1
            r2 = 0
            if (r0 != r3) goto L_0x0030
            r2 = 1
        L_0x0030:
            boolean r1 = r4.A08
            r0 = 0
            if (r2 == r1) goto L_0x0036
            r0 = 1
        L_0x0036:
            if (r0 == 0) goto L_0x011d
            int r23 = r4.A0c()
            int r23 = r23 - r3
            r22 = -1
            r21 = -1
        L_0x0042:
            int r0 = r4.A01
            if (r0 != r3) goto L_0x004d
            boolean r0 = r4.A25()
            r10 = 1
            if (r0 != 0) goto L_0x004e
        L_0x004d:
            r10 = 0
        L_0x004e:
            r1 = r23
            r0 = r28
            int r20 = r4.A06(r0, r15, r1)
            r19 = r24
            r9 = -1
            r3 = 0
            r2 = 0
            r18 = -1
        L_0x005d:
            r1 = r23
            r0 = r22
            if (r1 == r0) goto L_0x007d
            r0 = r28
            int r1 = r14.A06(r0, r15, r1)
            r0 = r23
            android.view.View r8 = r14.A0u(r0)
            if (r8 == r13) goto L_0x007d
            boolean r0 = r8.hasFocusable()
            if (r0 == 0) goto L_0x0080
            r0 = r20
            if (r1 == r0) goto L_0x0080
            if (r24 == 0) goto L_0x00bd
        L_0x007d:
            if (r24 != 0) goto L_0x0127
            return r19
        L_0x0080:
            android.view.ViewGroup$LayoutParams r7 = r8.getLayoutParams()
            X.Cau r7 = (X.C25158Cau) r7
            int r6 = r7.A00
            int r0 = r7.A01
            int r5 = r6 + r0
            boolean r0 = r8.hasFocusable()
            if (r0 == 0) goto L_0x0097
            if (r6 != r12) goto L_0x0097
            if (r5 != r11) goto L_0x0097
            return r8
        L_0x0097:
            boolean r0 = r8.hasFocusable()
            if (r0 == 0) goto L_0x009f
            if (r24 == 0) goto L_0x00a7
        L_0x009f:
            boolean r0 = r8.hasFocusable()
            if (r0 != 0) goto L_0x00d0
            if (r19 != 0) goto L_0x00d0
        L_0x00a7:
            r4 = 1
        L_0x00a8:
            if (r4 == 0) goto L_0x00bd
            boolean r0 = r8.hasFocusable()
            if (r0 == 0) goto L_0x00c0
            int r9 = r7.A00
            int r3 = java.lang.Math.min(r5, r11)
            int r0 = java.lang.Math.max(r6, r12)
            int r3 = r3 - r0
            r24 = r8
        L_0x00bd:
            int r23 = r23 + r21
            goto L_0x005d
        L_0x00c0:
            int r0 = r7.A00
            r18 = r0
            int r2 = java.lang.Math.min(r5, r11)
            int r0 = java.lang.Math.max(r6, r12)
            int r2 = r2 - r0
            r19 = r8
            goto L_0x00bd
        L_0x00d0:
            int r0 = java.lang.Math.max(r6, r12)
            int r1 = java.lang.Math.min(r5, r11)
            int r1 = r1 - r0
            boolean r0 = r8.hasFocusable()
            if (r0 == 0) goto L_0x00ea
            if (r1 > r3) goto L_0x00a7
            if (r1 != r3) goto L_0x011b
            r0 = 0
            if (r6 <= r9) goto L_0x00e7
            r0 = 1
        L_0x00e7:
            if (r10 != r0) goto L_0x011b
            goto L_0x00a7
        L_0x00ea:
            if (r24 != 0) goto L_0x011b
            r4 = 1
            X.19f r0 = r14.A09
            r16 = 24579(0x6003, float:3.4443E-41)
            r25 = r0
            r26 = r8
            r27 = r16
            boolean r0 = r25.A01(r26, r27)
            r17 = 1
            if (r0 == 0) goto L_0x010a
            X.19f r0 = r14.A0A
            r25 = r0
            boolean r16 = r25.A01(r26, r27)
            r0 = 1
            if (r16 != 0) goto L_0x010b
        L_0x010a:
            r0 = 0
        L_0x010b:
            r0 = r0 ^ r17
            if (r0 == 0) goto L_0x011b
            if (r1 > r2) goto L_0x00a8
            if (r1 != r2) goto L_0x011b
            r0 = r18
            if (r6 > r0) goto L_0x0118
            r4 = 0
        L_0x0118:
            if (r10 != r4) goto L_0x011b
            goto L_0x00a7
        L_0x011b:
            r4 = 0
            goto L_0x00a8
        L_0x011d:
            int r22 = r4.A0c()
            r23 = 0
            r21 = 1
            goto L_0x0042
        L_0x0127:
            return r24
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24131Sk.A1f(android.view.View, int, X.0vp, X.0wD):android.view.View");
    }

    public AnonymousClass1R7 A1g(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C25158Cau((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C25158Cau(layoutParams);
    }

    public void A1k(Rect rect, int i, int i2) {
        int A0H;
        int A0H2;
        if (this.A03 == null) {
            super.A1k(rect, i, i2);
        }
        int A0f = A0f() + A0g();
        int A0h = A0h() + A0e();
        if (this.A01 == 1) {
            A0H2 = AnonymousClass19T.A0H(i2, rect.height() + A0h, C15320v6.getMinimumHeight(this.A08));
            int[] iArr = this.A03;
            A0H = AnonymousClass19T.A0H(i, iArr[iArr.length - 1] + A0f, C15320v6.getMinimumWidth(this.A08));
        } else {
            A0H = AnonymousClass19T.A0H(i, rect.width() + A0f, C15320v6.getMinimumWidth(this.A08));
            int[] iArr2 = this.A03;
            A0H2 = AnonymousClass19T.A0H(i2, iArr2[iArr2.length - 1] + A0h, C15320v6.getMinimumHeight(this.A08));
        }
        this.A08.setMeasuredDimension(A0H, A0H2);
    }

    public void A1m(C15740vp r7, C15930wD r8) {
        if (r8.A08) {
            int A0c = A0c();
            for (int i = 0; i < A0c; i++) {
                C25158Cau cau = (C25158Cau) A0u(i).getLayoutParams();
                int A052 = cau.mViewHolder.A05();
                this.A07.put(A052, cau.A01);
                this.A06.put(A052, cau.A00);
            }
        }
        super.A1m(r7, r8);
        this.A07.clear();
        this.A06.clear();
    }

    public boolean A1s() {
        if (this.A05 != null || this.A00) {
            return false;
        }
        return true;
    }

    public void A24(boolean z) {
        if (!z) {
            super.A24(false);
            return;
        }
        throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0023, code lost:
        if (X.AnonymousClass19T.A0P(r4.getMeasuredHeight(), r6, r2.height) == false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0B(X.C24131Sk r3, android.view.View r4, int r5, int r6, boolean r7) {
        /*
            android.view.ViewGroup$LayoutParams r2 = r4.getLayoutParams()
            X.1R7 r2 = (X.AnonymousClass1R7) r2
            if (r7 == 0) goto L_0x002c
            boolean r0 = r3.A0D
            if (r0 == 0) goto L_0x0025
            int r1 = r4.getMeasuredWidth()
            int r0 = r2.width
            boolean r0 = X.AnonymousClass19T.A0P(r1, r5, r0)
            if (r0 == 0) goto L_0x0025
            int r1 = r4.getMeasuredHeight()
            int r0 = r2.height
            boolean r1 = X.AnonymousClass19T.A0P(r1, r6, r0)
            r0 = 0
            if (r1 != 0) goto L_0x0026
        L_0x0025:
            r0 = 1
        L_0x0026:
            if (r0 == 0) goto L_0x002b
            r4.measure(r5, r6)
        L_0x002b:
            return
        L_0x002c:
            boolean r0 = r3.A1X(r4, r5, r6, r2)
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24131Sk.A0B(X.1Sk, android.view.View, int, int, boolean):void");
    }

    public static void A0C(C24131Sk r8, View view, int i, boolean z) {
        int A0I;
        int A0I2;
        C25158Cau cau = (C25158Cau) view.getLayoutParams();
        Rect rect = cau.A02;
        int i2 = rect.top + rect.bottom + cau.topMargin + cau.bottomMargin;
        int i3 = rect.left + rect.right + cau.leftMargin + cau.rightMargin;
        int A002 = A00(r8, cau.A00, cau.A01);
        if (r8.A01 == 1) {
            A0I2 = AnonymousClass19T.A0I(A002, i, i3, cau.width, false);
            A0I = AnonymousClass19T.A0I(r8.A06.A07(), r8.A02, i2, cau.height, true);
        } else {
            A0I = AnonymousClass19T.A0I(A002, i, i2, cau.height, false);
            A0I2 = AnonymousClass19T.A0I(r8.A06.A07(), r8.A05, i3, cau.width, true);
        }
        A0B(r8, view, A0I2, A0I, z);
    }

    public int A1b(int i, C15740vp r3, C15930wD r4) {
        A09(this);
        A07();
        return super.A1b(i, r3, r4);
    }

    public int A1c(int i, C15740vp r3, C15930wD r4) {
        A09(this);
        A07();
        return super.A1c(i, r3, r4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        if (r8 != r0) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (r4 != r0) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1n(X.C15740vp r12, X.C15930wD r13, android.view.View r14, androidx.core.view.accessibility.AccessibilityNodeInfoCompat r15) {
        /*
            r11 = this;
            android.view.ViewGroup$LayoutParams r2 = r14.getLayoutParams()
            boolean r0 = r2 instanceof X.C25158Cau
            if (r0 != 0) goto L_0x000c
            super.A1B(r14, r15)
            return
        L_0x000c:
            X.Cau r2 = (X.C25158Cau) r2
            X.1o8 r0 = r2.mViewHolder
            int r0 = r0.A05()
            int r5 = r11.A06(r12, r13, r0)
            int r0 = r11.A01
            r1 = 1
            if (r0 != 0) goto L_0x0033
            int r3 = r2.A00
            int r4 = r2.A01
            r6 = 1
            int r0 = r11.A01
            if (r0 <= r1) goto L_0x0029
            r7 = 1
            if (r4 == r0) goto L_0x002a
        L_0x0029:
            r7 = 0
        L_0x002a:
            r8 = 0
            X.4CJ r0 = X.AnonymousClass4CJ.A00(r3, r4, r5, r6, r7, r8)
            r15.A0Q(r0)
            return
        L_0x0033:
            r6 = 1
            int r7 = r2.A00
            int r8 = r2.A01
            int r0 = r11.A01
            if (r0 <= r1) goto L_0x003f
            r9 = 1
            if (r8 == r0) goto L_0x0040
        L_0x003f:
            r9 = 0
        L_0x0040:
            r10 = 0
            X.4CJ r0 = X.AnonymousClass4CJ.A00(r5, r6, r7, r8, r9, r10)
            r15.A0Q(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24131Sk.A1n(X.0vp, X.0wD, android.view.View, androidx.core.view.accessibility.AccessibilityNodeInfoCompat):void");
    }

    public void A1o(C15930wD r2) {
        super.A1o(r2);
        this.A00 = false;
    }

    public void A20(C15740vp r6, C15930wD r7, C196919h r8, int i) {
        super.A20(r6, r7, r8, i);
        A09(this);
        if (r7.A00() > 0 && !r7.A08) {
            boolean z = false;
            if (i == 1) {
                z = true;
            }
            int A012 = A01(this, r6, r7, r8.A03);
            if (z) {
                while (A012 > 0) {
                    int i2 = r8.A03;
                    if (i2 <= 0) {
                        break;
                    }
                    int i3 = i2 - 1;
                    r8.A03 = i3;
                    A012 = A01(this, r6, r7, i3);
                }
            } else {
                int A002 = r7.A00() - 1;
                int i4 = r8.A03;
                while (i4 < A002) {
                    int i5 = i4 + 1;
                    int A013 = A01(this, r6, r7, i5);
                    if (A013 <= A012) {
                        break;
                    }
                    i4 = i5;
                    A012 = A013;
                }
                r8.A03 = i4;
            }
        }
        A07();
    }

    public C24131Sk(int i) {
        A08(i);
    }

    public C24131Sk(int i, int i2, boolean z) {
        super(i2, z);
        A08(i);
    }
}
