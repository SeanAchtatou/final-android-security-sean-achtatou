package X;

import java.lang.ref.PhantomReference;

/* renamed from: X.08y  reason: invalid class name and case insensitive filesystem */
public abstract class C011208y extends PhantomReference {
    public C011208y A00;
    public C011208y A01;

    public abstract void destruct();

    public C011208y() {
        super(null, AnonymousClass09B.A02);
    }

    public C011208y(Object obj) {
        super(obj, AnonymousClass09B.A02);
        C011208y r1;
        AnonymousClass04K r2 = AnonymousClass09B.A01;
        do {
            r1 = (C011208y) r2.A00.get();
            this.A00 = r1;
        } while (!r2.A00.compareAndSet(r1, this));
    }
}
