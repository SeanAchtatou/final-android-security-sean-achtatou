package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.google.common.base.Preconditions;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1Gj  reason: invalid class name and case insensitive filesystem */
public abstract class C21341Gj extends C07180co {
    public final int A00;
    public final int A01;
    public final ByteBuffer A02;

    public C07190cq A0A() {
        C33961oQ r4 = (C33961oQ) this;
        long j = r4.A01;
        long j2 = (long) r4.A00;
        long j3 = j ^ j2;
        r4.A01 = j3;
        long j4 = r4.A02 ^ j2;
        r4.A02 = j4;
        long j5 = j3 + j4;
        r4.A01 = j5;
        long j6 = j4 + j5;
        r4.A02 = j6;
        long j7 = (j5 ^ (j5 >>> 33)) * -49064778989728563L;
        long j8 = (j7 ^ (j7 >>> 33)) * -4265267296055464877L;
        long j9 = j8 ^ (j8 >>> 33);
        r4.A01 = j9;
        long j10 = (j6 ^ (j6 >>> 33)) * -49064778989728563L;
        long j11 = (j10 ^ (j10 >>> 33)) * -4265267296055464877L;
        long j12 = j11 ^ (j11 >>> 33);
        r4.A02 = j12;
        long j13 = j9 + j12;
        r4.A01 = j13;
        r4.A02 = j12 + j13;
        ByteBuffer order = ByteBuffer.wrap(new byte[16]).order(ByteOrder.LITTLE_ENDIAN);
        order.putLong(r4.A01);
        order.putLong(r4.A02);
        return new C25441Zq(order.array());
    }

    public void A0B(ByteBuffer byteBuffer) {
        C33961oQ r4 = (C33961oQ) this;
        long j = byteBuffer.getLong();
        long j2 = byteBuffer.getLong();
        long rotateLeft = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r4.A01;
        r4.A01 = rotateLeft;
        long rotateLeft2 = Long.rotateLeft(rotateLeft, 27);
        r4.A01 = rotateLeft2;
        long j3 = r4.A02;
        long j4 = rotateLeft2 + j3;
        r4.A01 = j4;
        r4.A01 = (j4 * 5) + 1390208809;
        long rotateLeft3 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ j3;
        r4.A02 = rotateLeft3;
        long rotateLeft4 = Long.rotateLeft(rotateLeft3, 31);
        r4.A02 = rotateLeft4;
        long j5 = rotateLeft4 + r4.A01;
        r4.A02 = j5;
        r4.A02 = (j5 * 5) + 944331445;
        r4.A00 += 16;
    }

    public static void A00(C21341Gj r2) {
        r2.A02.flip();
        while (r2.A02.remaining() >= r2.A01) {
            r2.A0B(r2.A02);
        }
        r2.A02.compact();
    }

    public void A0C(ByteBuffer byteBuffer) {
        long j;
        long j2;
        long j3;
        long j4;
        long j5;
        long j6;
        long j7;
        long j8;
        long j9;
        long j10;
        long j11;
        long j12;
        long j13;
        long j14;
        if (!(this instanceof C33961oQ)) {
            byteBuffer.position(byteBuffer.limit());
            byteBuffer.limit(this.A01 + 7);
            while (true) {
                int position = byteBuffer.position();
                int i = this.A01;
                if (position < i) {
                    byteBuffer.putLong(0);
                } else {
                    byteBuffer.limit(i);
                    byteBuffer.flip();
                    A0B(byteBuffer);
                    return;
                }
            }
        } else {
            C33961oQ r8 = (C33961oQ) this;
            r8.A00 += byteBuffer.remaining();
            switch (byteBuffer.remaining()) {
                case 1:
                    j3 = 0;
                    j = ((long) (byteBuffer.get(0) & 255)) ^ j3;
                    j2 = 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case 2:
                    j4 = 0;
                    j3 = j4 ^ (((long) (byteBuffer.get(1) & 255)) << 8);
                    j = ((long) (byteBuffer.get(0) & 255)) ^ j3;
                    j2 = 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case 3:
                    j5 = 0;
                    j4 = j5 ^ (((long) (byteBuffer.get(2) & 255)) << 16);
                    j3 = j4 ^ (((long) (byteBuffer.get(1) & 255)) << 8);
                    j = ((long) (byteBuffer.get(0) & 255)) ^ j3;
                    j2 = 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case 4:
                    j6 = 0;
                    j5 = j6 ^ (((long) (byteBuffer.get(3) & 255)) << 24);
                    j4 = j5 ^ (((long) (byteBuffer.get(2) & 255)) << 16);
                    j3 = j4 ^ (((long) (byteBuffer.get(1) & 255)) << 8);
                    j = ((long) (byteBuffer.get(0) & 255)) ^ j3;
                    j2 = 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case 5:
                    j7 = 0;
                    j6 = j7 ^ (((long) (byteBuffer.get(4) & 255)) << 32);
                    j5 = j6 ^ (((long) (byteBuffer.get(3) & 255)) << 24);
                    j4 = j5 ^ (((long) (byteBuffer.get(2) & 255)) << 16);
                    j3 = j4 ^ (((long) (byteBuffer.get(1) & 255)) << 8);
                    j = ((long) (byteBuffer.get(0) & 255)) ^ j3;
                    j2 = 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    j8 = 0;
                    j7 = j8 ^ (((long) (byteBuffer.get(5) & 255)) << 40);
                    j6 = j7 ^ (((long) (byteBuffer.get(4) & 255)) << 32);
                    j5 = j6 ^ (((long) (byteBuffer.get(3) & 255)) << 24);
                    j4 = j5 ^ (((long) (byteBuffer.get(2) & 255)) << 16);
                    j3 = j4 ^ (((long) (byteBuffer.get(1) & 255)) << 8);
                    j = ((long) (byteBuffer.get(0) & 255)) ^ j3;
                    j2 = 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    j8 = (((long) (byteBuffer.get(6) & 255)) << 48) ^ 0;
                    j7 = j8 ^ (((long) (byteBuffer.get(5) & 255)) << 40);
                    j6 = j7 ^ (((long) (byteBuffer.get(4) & 255)) << 32);
                    j5 = j6 ^ (((long) (byteBuffer.get(3) & 255)) << 24);
                    j4 = j5 ^ (((long) (byteBuffer.get(2) & 255)) << 16);
                    j3 = j4 ^ (((long) (byteBuffer.get(1) & 255)) << 8);
                    j = ((long) (byteBuffer.get(0) & 255)) ^ j3;
                    j2 = 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case 8:
                    j2 = 0;
                    j = byteBuffer.getLong() ^ 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case Process.SIGKILL:
                    j9 = 0;
                    j2 = j9 ^ ((long) (byteBuffer.get(8) & 255));
                    j = byteBuffer.getLong() ^ 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case AnonymousClass1Y3.A01 /*10*/:
                    j10 = 0;
                    j9 = j10 ^ (((long) (byteBuffer.get(9) & 255)) << 8);
                    j2 = j9 ^ ((long) (byteBuffer.get(8) & 255));
                    j = byteBuffer.getLong() ^ 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case AnonymousClass1Y3.A02 /*11*/:
                    j11 = 0;
                    j10 = j11 ^ (((long) (byteBuffer.get(10) & 255)) << 16);
                    j9 = j10 ^ (((long) (byteBuffer.get(9) & 255)) << 8);
                    j2 = j9 ^ ((long) (byteBuffer.get(8) & 255));
                    j = byteBuffer.getLong() ^ 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case AnonymousClass1Y3.A03 /*12*/:
                    j12 = 0;
                    j11 = j12 ^ (((long) (byteBuffer.get(11) & 255)) << 24);
                    j10 = j11 ^ (((long) (byteBuffer.get(10) & 255)) << 16);
                    j9 = j10 ^ (((long) (byteBuffer.get(9) & 255)) << 8);
                    j2 = j9 ^ ((long) (byteBuffer.get(8) & 255));
                    j = byteBuffer.getLong() ^ 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case 13:
                    j13 = 0;
                    j12 = j13 ^ (((long) (byteBuffer.get(12) & 255)) << 32);
                    j11 = j12 ^ (((long) (byteBuffer.get(11) & 255)) << 24);
                    j10 = j11 ^ (((long) (byteBuffer.get(10) & 255)) << 16);
                    j9 = j10 ^ (((long) (byteBuffer.get(9) & 255)) << 8);
                    j2 = j9 ^ ((long) (byteBuffer.get(8) & 255));
                    j = byteBuffer.getLong() ^ 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case 14:
                    j14 = 0;
                    j13 = j14 ^ (((long) (byteBuffer.get(13) & 255)) << 40);
                    j12 = j13 ^ (((long) (byteBuffer.get(12) & 255)) << 32);
                    j11 = j12 ^ (((long) (byteBuffer.get(11) & 255)) << 24);
                    j10 = j11 ^ (((long) (byteBuffer.get(10) & 255)) << 16);
                    j9 = j10 ^ (((long) (byteBuffer.get(9) & 255)) << 8);
                    j2 = j9 ^ ((long) (byteBuffer.get(8) & 255));
                    j = byteBuffer.getLong() ^ 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                case 15:
                    j14 = (((long) (byteBuffer.get(14) & 255)) << 48) ^ 0;
                    j13 = j14 ^ (((long) (byteBuffer.get(13) & 255)) << 40);
                    j12 = j13 ^ (((long) (byteBuffer.get(12) & 255)) << 32);
                    j11 = j12 ^ (((long) (byteBuffer.get(11) & 255)) << 24);
                    j10 = j11 ^ (((long) (byteBuffer.get(10) & 255)) << 16);
                    j9 = j10 ^ (((long) (byteBuffer.get(9) & 255)) << 8);
                    j2 = j9 ^ ((long) (byteBuffer.get(8) & 255));
                    j = byteBuffer.getLong() ^ 0;
                    r8.A01 = (Long.rotateLeft(j * -8663945395140668459L, 31) * 5545529020109919103L) ^ r8.A01;
                    r8.A02 = (Long.rotateLeft(j2 * 5545529020109919103L, 33) * -8663945395140668459L) ^ r8.A02;
                    return;
                default:
                    throw new AssertionError("Should never get here.");
            }
        }
    }

    public C21341Gj(int i, int i2) {
        Preconditions.checkArgument(i2 % i == 0);
        this.A02 = ByteBuffer.allocate(i2 + 7).order(ByteOrder.LITTLE_ENDIAN);
        this.A00 = i2;
        this.A01 = i;
    }

    public static final void A01(C21341Gj r5, byte[] bArr, int i, int i2) {
        ByteBuffer order = ByteBuffer.wrap(bArr, i, i2).order(ByteOrder.LITTLE_ENDIAN);
        if (order.remaining() <= r5.A02.remaining()) {
            r5.A02.put(order);
            if (r5.A02.remaining() < 8) {
                A00(r5);
                return;
            }
            return;
        }
        int position = r5.A00 - r5.A02.position();
        for (int i3 = 0; i3 < position; i3++) {
            r5.A02.put(order.get());
        }
        A00(r5);
        while (order.remaining() >= r5.A01) {
            r5.A0B(order);
        }
        r5.A02.put(order);
    }
}
