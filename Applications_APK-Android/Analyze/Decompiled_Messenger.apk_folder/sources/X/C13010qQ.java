package X;

import android.os.Handler;
import androidx.fragment.app.FragmentActivity;

/* renamed from: X.0qQ  reason: invalid class name and case insensitive filesystem */
public final class C13010qQ extends C13020qR implements AnonymousClass0n8, C27531dL {
    public final /* synthetic */ FragmentActivity A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C13010qQ(FragmentActivity fragmentActivity) {
        super(fragmentActivity, fragmentActivity, new Handler());
        this.A00 = fragmentActivity;
    }

    public AnonymousClass0qM AsK() {
        return this.A00.A06;
    }

    public C27641dW AwJ() {
        return this.A00.AwJ();
    }

    public C27911dx B9I() {
        return this.A00.B9I();
    }
}
