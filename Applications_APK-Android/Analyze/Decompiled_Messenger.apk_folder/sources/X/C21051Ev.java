package X;

import android.net.Uri;
import com.facebook.user.profilepic.PicSquare;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

/* renamed from: X.1Ev  reason: invalid class name and case insensitive filesystem */
public final class C21051Ev {
    public int A00;
    public Uri A01;
    public PicSquare A02;
    public C21391Gt A03;
    public C21381Gs A04 = C21381Gs.A0L;
    public ImmutableList A05 = RegularImmutableList.A02;
    public ImmutableList A06;
    public boolean A07;

    public AnonymousClass1H3 A00() {
        C21391Gt r1 = this.A03;
        Uri uri = this.A01;
        PicSquare picSquare = this.A02;
        boolean z = this.A07;
        ImmutableList immutableList = this.A06;
        if (immutableList == null) {
            immutableList = RegularImmutableList.A02;
        }
        return new AnonymousClass1H3(r1, uri, null, picSquare, z, immutableList, this.A04, this.A05, this.A00);
    }
}
