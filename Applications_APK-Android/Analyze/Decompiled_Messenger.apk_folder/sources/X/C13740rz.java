package X;

import com.facebook.common.perftest.PerfTestConfig;
import com.facebook.common.perftest.base.PerfTestConfigBase;
import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.user.model.User;

@InjectorModule
/* renamed from: X.0rz  reason: invalid class name and case insensitive filesystem */
public final class C13740rz extends AnonymousClass0UV {
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0023, code lost:
        if (((java.lang.Boolean) r1.get()).booleanValue() != false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A02(X.AnonymousClass1XY r3) {
        /*
            int r0 = X.AnonymousClass1Y3.AWa
            X.0VG r2 = X.AnonymousClass0VG.A00(r0, r3)
            int r0 = X.AnonymousClass1Y3.BHj
            X.0VG r1 = X.AnonymousClass0VG.A00(r0, r3)
            java.lang.Object r0 = r2.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0025
            java.lang.Object r0 = r1.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r1 = r0.booleanValue()
            r0 = 0
            if (r1 == 0) goto L_0x0026
        L_0x0025:
            r0 = 1
        L_0x0026:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13740rz.A02(X.1XY):java.lang.Boolean");
    }

    public static final Boolean A00(AnonymousClass1XY r5) {
        FbSharedPreferences A00 = FbSharedPreferencesModule.A00(r5);
        User user = (User) AnonymousClass0WY.A01(r5).get();
        if (user != null) {
            boolean z = user.A1Q;
            AnonymousClass1Y8 r0 = C05690aA.A1j;
            if (z) {
                return Boolean.valueOf(!A00.Aep(r0, false));
            }
            if (A00.BBh(r0)) {
                C30281hn edit = A00.edit();
                edit.C1B(C05690aA.A1j);
                edit.commit();
            }
        }
        return false;
    }

    public static final Boolean A01(AnonymousClass1XY r4) {
        AnonymousClass1YI A00 = AnonymousClass0WA.A00(r4);
        FbSharedPreferences A002 = FbSharedPreferencesModule.A00(r4);
        PerfTestConfig.A01(r4);
        boolean z = false;
        if (C006006f.A01() || C006006f.A02("nux_killswitch") || PerfTestConfigBase.A00() || A00.Ab9(228).asBoolean(false)) {
            z = true;
        }
        boolean z2 = false;
        if (!z && A002.AqN(C05690aA.A1l, 0) < 10) {
            z2 = true;
        }
        return Boolean.valueOf(z2);
    }
}
