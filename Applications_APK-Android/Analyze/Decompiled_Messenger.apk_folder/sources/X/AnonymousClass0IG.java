package X;

import com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector;

/* renamed from: X.0IG  reason: invalid class name */
public final class AnonymousClass0IG implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector$2";
    public final /* synthetic */ AnonymousClass0R9 A00;
    public final /* synthetic */ SkywalkerSubscriptionConnector A01;
    public final /* synthetic */ C04810Wg A02;
    public final /* synthetic */ String A03;

    public AnonymousClass0IG(SkywalkerSubscriptionConnector skywalkerSubscriptionConnector, String str, C04810Wg r3, AnonymousClass0R9 r4) {
        this.A01 = skywalkerSubscriptionConnector;
        this.A03 = str;
        this.A02 = r3;
        this.A00 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0030, code lost:
        if (com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector.A08(r4.A01, r1) == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        r3 = r4.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r4.A01.A04.put(r4.A03, r4.A02);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
        r0 = r4.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0043, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0045, code lost:
        r0.BqS();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0048, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0049, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004c, code lost:
        r3 = r4.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004e, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r4.A01.A03.put(r4.A03, r4.A02);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005a, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005b, code lost:
        r1 = r4.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005d, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005f, code lost:
        r1.BqR(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0064, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        r1 = r4.A01.A02.createArrayNode();
        r1.add(r4.A03);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r4 = this;
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r3 = r4.A01
            monitor-enter(r3)
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r0 = r4.A01     // Catch:{ all -> 0x0067 }
            java.util.Map r1 = r0.A04     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = r4.A03     // Catch:{ all -> 0x0067 }
            boolean r0 = r1.containsKey(r0)     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x001c
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r0 = r4.A01     // Catch:{ all -> 0x0067 }
            java.util.Map r2 = r0.A04     // Catch:{ all -> 0x0067 }
            java.lang.String r1 = r4.A03     // Catch:{ all -> 0x0067 }
            X.0Wg r0 = r4.A02     // Catch:{ all -> 0x0067 }
            r2.put(r1, r0)     // Catch:{ all -> 0x0067 }
            monitor-exit(r3)     // Catch:{ all -> 0x0067 }
            return
        L_0x001c:
            monitor-exit(r3)     // Catch:{ all -> 0x0067 }
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r0 = r4.A01
            X.0jJ r0 = r0.A02
            com.fasterxml.jackson.databind.node.ArrayNode r1 = r0.createArrayNode()
            java.lang.String r0 = r4.A03
            r1.add(r0)
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r0 = r4.A01
            boolean r0 = com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector.A08(r0, r1)
            if (r0 == 0) goto L_0x004c
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r3 = r4.A01
            monitor-enter(r3)
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r0 = r4.A01     // Catch:{ all -> 0x0049 }
            java.util.Map r2 = r0.A04     // Catch:{ all -> 0x0049 }
            java.lang.String r1 = r4.A03     // Catch:{ all -> 0x0049 }
            X.0Wg r0 = r4.A02     // Catch:{ all -> 0x0049 }
            r2.put(r1, r0)     // Catch:{ all -> 0x0049 }
            monitor-exit(r3)     // Catch:{ all -> 0x0049 }
            X.0R9 r0 = r4.A00
            if (r0 == 0) goto L_0x0063
            r0.BqS()
            return
        L_0x0049:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0049 }
            goto L_0x0069
        L_0x004c:
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r3 = r4.A01
            monitor-enter(r3)
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r0 = r4.A01     // Catch:{ all -> 0x0064 }
            java.util.Map r2 = r0.A03     // Catch:{ all -> 0x0064 }
            java.lang.String r1 = r4.A03     // Catch:{ all -> 0x0064 }
            X.0Wg r0 = r4.A02     // Catch:{ all -> 0x0064 }
            r2.put(r1, r0)     // Catch:{ all -> 0x0064 }
            monitor-exit(r3)     // Catch:{ all -> 0x0064 }
            X.0R9 r1 = r4.A00
            if (r1 == 0) goto L_0x0063
            r0 = 0
            r1.BqR(r0)
        L_0x0063:
            return
        L_0x0064:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0064 }
            goto L_0x0069
        L_0x0067:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0067 }
        L_0x0069:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0IG.run():void");
    }
}
