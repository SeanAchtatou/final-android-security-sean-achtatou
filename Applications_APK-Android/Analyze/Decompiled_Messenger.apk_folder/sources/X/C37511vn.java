package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1vn  reason: invalid class name and case insensitive filesystem */
public final class C37511vn {
    public static String A00(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "UNKNOWN" : "DATA_SUSPENDED" : "DATA_CONNECTED" : "DATA_CONNECTING" : "DATA_DISCONNECTED";
    }

    public static String A01(int i) {
        switch (i) {
            case 1:
                return "GPRS";
            case 2:
                return "EDGE";
            case 3:
                return "UMTS";
            case 4:
                return "CDMA";
            case 5:
                return "EVDO_0";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return "EVDO_A";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "1xRTT";
            case 8:
                return "HSDPA";
            case Process.SIGKILL:
                return "HSUPA";
            case AnonymousClass1Y3.A01:
                return "HSPA";
            case AnonymousClass1Y3.A02:
                return "IDEN";
            case AnonymousClass1Y3.A03:
                return "EVDO_B";
            case 13:
                return "LTE";
            case 14:
                return "EHRPD";
            case 15:
                return "HSPAP";
            default:
                return "UNKNOWN";
        }
    }

    public static String A02(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "UNKNOWN" : "SIP" : "CDMA" : "GSM" : "NONE";
    }

    public static String A03(int i) {
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? i != 5 ? "UNKNOWN" : "READY" : "NETWORK_LOCKED" : "PUK_REQUIRED" : "PIN_REQUIRED" : "ABSENT";
    }

    public static boolean A04(int i, int i2) {
        if (i != 1 && i == 0) {
            switch (i2) {
                case 1:
                case 2:
                case 4:
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case AnonymousClass1Y3.A02:
                    return true;
            }
        }
        return false;
    }
}
