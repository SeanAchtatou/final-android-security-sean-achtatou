package X;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.facebook.inject.InjectorModule;
import java.util.concurrent.ExecutorService;

@InjectorModule
/* renamed from: X.1gE  reason: invalid class name and case insensitive filesystem */
public final class C29321gE extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static volatile Handler A02;
    private static volatile Looper A03;

    public static final Handler A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new Handler(A01(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final Looper A01(AnonymousClass1XY r5) {
        if (A03 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r5);
                if (A002 != null) {
                    try {
                        HandlerThread A022 = AnonymousClass0V4.A00(r5.getApplicationInjector()).A02("MqttHandler", AnonymousClass0V7.URGENT);
                        A022.start();
                        A03 = A022.getLooper();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final C06550bg A02(AnonymousClass1XY r2) {
        C06550bg r1;
        for (C13810s7 r12 : new AnonymousClass0X5(r2, AnonymousClass0X6.A1q)) {
            if (!r12.A00.A0A() || r12.A00.A0D()) {
                r1 = C06550bg.APP_USE;
            } else {
                r1 = C06550bg.ALWAYS;
            }
            C06550bg r0 = C06550bg.ALWAYS;
            if (r1 == r0) {
                return r0;
            }
        }
        return C06550bg.APP_USE;
    }

    public static final ExecutorService A03(AnonymousClass1XY r2) {
        return AnonymousClass0VM.A00(r2).A04(AnonymousClass0VS.URGENT, "whistle");
    }
}
