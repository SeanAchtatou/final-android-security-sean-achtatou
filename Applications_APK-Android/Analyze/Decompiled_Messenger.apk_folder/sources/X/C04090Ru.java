package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.rti.mqtt.protocol.messages.GqlsTopicExtraInfo;

/* renamed from: X.0Ru  reason: invalid class name and case insensitive filesystem */
public final class C04090Ru implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new GqlsTopicExtraInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new GqlsTopicExtraInfo[i];
    }
}
