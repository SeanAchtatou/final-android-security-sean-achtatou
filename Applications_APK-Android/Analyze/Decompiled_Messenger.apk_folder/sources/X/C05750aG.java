package X;

import android.content.Context;
import com.facebook.inject.InjectorModule;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;

@InjectorModule
/* renamed from: X.0aG  reason: invalid class name and case insensitive filesystem */
public final class C05750aG extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static final Object A01 = new Object();
    private static volatile CIR A02;

    public static final AnonymousClass06B A00() {
        return AnonymousClass06A.A00;
    }

    public static final C27311cz A01(AnonymousClass1XY r7) {
        C27311cz r0;
        synchronized (A01) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r7)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C27301cy(C182388dE.A00(r02), new C27301cy(C186488lE.A00(r02), new C27331d1()));
                }
                C05540Zi r1 = A00;
                r0 = (C27311cz) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final CIR A02(AnonymousClass1XY r5) {
        CIR c0k;
        if (A02 == null) {
            synchronized (CIR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        Context A003 = AnonymousClass1YA.A00(r5.getApplicationInjector());
                        int intValue = C04490Ux.A0q().intValue();
                        if (intValue >= 29) {
                            c0k = new CLA(A003);
                        } else if (intValue >= 23) {
                            c0k = new CLB(A003);
                        } else {
                            c0k = new C0K();
                        }
                        A02 = c0k;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final KeyFactory A03() {
        try {
            return KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Failed to get an instance of KeyFactory", e);
        }
    }

    public static final KeyPairGenerator A04() {
        try {
            return KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get an instance of KeyPairGenerator", e);
        }
    }

    public static final KeyStore A05() {
        try {
            return KeyStore.getInstance("AndroidKeyStore");
        } catch (KeyStoreException e) {
            throw new RuntimeException("Failed to get an instance of KeyStore", e);
        }
    }

    public static final Cipher A06() {
        try {
            return Cipher.getInstance("RSA/ECB/PKCS1Padding");
        } catch (GeneralSecurityException e) {
            throw new RuntimeException("Failed to get an instance of Cipher", e);
        }
    }
}
