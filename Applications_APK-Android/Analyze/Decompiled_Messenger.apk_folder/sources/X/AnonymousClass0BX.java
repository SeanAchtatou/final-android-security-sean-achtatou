package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.0BX  reason: invalid class name */
public final class AnonymousClass0BX extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0BW A00;

    public AnonymousClass0BX(AnonymousClass0BW r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(1202724665);
        if (!AnonymousClass0A2.A00(intent.getAction(), this.A00.A09)) {
            C000700l.A0D(intent, 1895660206, A01);
            return;
        }
        intent.getAction();
        this.A00.A0C.run();
        C000700l.A0D(intent, 1290445616, A01);
    }
}
