package X;

import com.facebook.auth.credentials.UserTokenCredentials;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import io.card.payment.BuildConfig;

/* renamed from: X.1sL  reason: invalid class name and case insensitive filesystem */
public final class C36111sL implements AnonymousClass0AQ {
    private final FbSharedPreferences A00;
    private final C04310Tq A01;

    public String AhN() {
        return BuildConfig.FLAVOR;
    }

    public boolean CKA(C01830Bu r2) {
        return false;
    }

    public void clear() {
    }

    public static final C36111sL A00(AnonymousClass1XY r1) {
        return new C36111sL(r1);
    }

    public void ASh() {
        C30281hn edit = this.A00.edit();
        edit.C1B(C10930l6.A04);
        edit.commit();
    }

    public String AiB() {
        return this.A00.B4F(C10930l6.A04, BuildConfig.FLAVOR);
    }

    public C01830Bu Arn() {
        UserTokenCredentials userTokenCredentials = (UserTokenCredentials) this.A01.get();
        if (userTokenCredentials == null) {
            return C01830Bu.A00;
        }
        return C01830Bu.A00(userTokenCredentials.A00, userTokenCredentials.A01);
    }

    public void C73(String str) {
        C30281hn edit = this.A00.edit();
        edit.BzC(C10930l6.A04, str);
        edit.commit();
    }

    public C36111sL(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.AsW, r2);
        this.A00 = FbSharedPreferencesModule.A00(r2);
    }
}
