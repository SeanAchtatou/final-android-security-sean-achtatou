package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0VT  reason: invalid class name */
public final class AnonymousClass0VT implements AnonymousClass0VU {
    private final int A00;
    private final C04550Vd A01;
    private final C04590Vi A02;
    private final AnonymousClass0VY A03;

    private C04640Vn A00(AnonymousClass0VZ r11, int i, AnonymousClass0VS r13, String str, AnonymousClass0VF r15) {
        boolean z = false;
        int i2 = i;
        if (i > 0) {
            z = true;
        }
        Preconditions.checkArgument(z);
        C04610Vk r2 = new C04610Vk(r11, this.A03, i2, str, this.A00);
        AnonymousClass0VS r8 = r13;
        AnonymousClass0VF r9 = r15;
        if (i == 1) {
            return new AnonymousClass1TI(this, this.A02, this.A01, r2, r8, r9);
        }
        return new C04640Vn(this, this.A02, this.A01, r2, r8, r9);
    }

    public C04650Vo A01(int i, AnonymousClass0VS r8, String str, AnonymousClass0VF r10) {
        boolean z = false;
        int i2 = i;
        if (i > 0) {
            z = true;
        }
        Preconditions.checkArgument(z);
        return A00(this.A03, i2, r8, str, r10);
    }

    public C04650Vo A02(C04650Vo r7, int i, String str, AnonymousClass0VF r10) {
        Preconditions.checkArgument(r7 instanceof C04640Vn, "executor must be created by CombinedThreadPool");
        boolean z = false;
        int i2 = i;
        if (i > 0) {
            z = true;
        }
        Preconditions.checkArgument(z);
        C04640Vn r72 = (C04640Vn) r7;
        C04610Vk r1 = r72.A04;
        Preconditions.checkState(r1 instanceof AnonymousClass0VZ);
        AnonymousClass0VS r3 = r72.A02;
        AnonymousClass0VF r5 = r72.A01;
        AnonymousClass0VF r0 = r5;
        if (r10 != null && r5 != null) {
            r5 = new C21416ADi(r10, r0);
        } else if (r10 != null) {
            r5 = r10;
        }
        return A00(r1, i2, r3, str, r5);
    }

    public AnonymousClass0VT(AnonymousClass0VX r6) {
        AnonymousClass0VY r1 = new AnonymousClass0VY(r6.A02);
        this.A03 = r1;
        this.A01 = new C04550Vd(r1, r6);
        C04570Vg r4 = new C04570Vg("CombinedTP", r6.A00);
        C04550Vd r2 = this.A01;
        this.A02 = new C04590Vi(r6, r2, new C04580Vh(r4, r2), r6.A07);
        this.A00 = r6.A03;
        C04650Vo A012 = A01(Integer.MAX_VALUE, AnonymousClass0VS.BACKGROUND, "CtpPrivateExecutor", null);
        C04550Vd r22 = this.A01;
        C04590Vi r12 = this.A02;
        r22.A06.A00();
        try {
            r22.A02 = r12;
            r22.A0E = A012;
        } finally {
            r22.A06.A02();
        }
    }
}
