package X;

import com.google.common.base.Preconditions;

/* renamed from: X.1xV  reason: invalid class name and case insensitive filesystem */
public final class C38481xV {
    private static final C32411li A01 = C32411li.A00("rtc_call", C05360Yq.$const$string(2203));
    private AnonymousClass0UN A00;

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "PORTRAIT";
            case 2:
                return "LANDSCAPE";
            default:
                return "UNKNOWN";
        }
    }

    public void A03(String str) {
        A02(str, null, null);
    }

    public static final C38481xV A00(AnonymousClass1XY r1) {
        return new C38481xV(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0099, code lost:
        if (r3 == false) goto L_0x009b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x048a  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x048e  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0277  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A02(java.lang.String r14, java.lang.String r15, X.C155747Ib r16) {
        /*
            r13 = this;
            int r2 = X.AnonymousClass1Y3.BTX
            X.0UN r1 = r13.A00
            r0 = 9
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0bA r1 = (X.C06230bA) r1
            X.1li r0 = X.C38481xV.A01
            X.0ZF r5 = r1.A03(r0)
            boolean r0 = r5.A0G()
            java.lang.String r10 = "CallActionLogger"
            if (r0 == 0) goto L_0x0527
            r0 = 42
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r5.A0A(r0, r14)
            if (r15 == 0) goto L_0x002a
            java.lang.String r0 = "action_source"
            r5.A0A(r0, r15)
        L_0x002a:
            java.lang.String r1 = ""
            if (r16 == 0) goto L_0x0518
            java.lang.String r9 = r16.Abn()
        L_0x0032:
            java.lang.String r0 = "action_params"
            r5.A0A(r0, r9)
            int r2 = X.AnonymousClass1Y3.AkZ
            X.0UN r0 = r13.A00
            r6 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            r4 = 0
            if (r0 == 0) goto L_0x0173
            java.lang.String r3 = r0.A01()
            java.lang.String r0 = "call_id"
            r5.A0A(r0, r3)
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            java.lang.String r0 = r0.A03()
            if (r0 == 0) goto L_0x0515
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            java.lang.String r3 = r0.A03()
        L_0x0068:
            java.lang.String r0 = "caller_id"
            r5.A0A(r0, r3)
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            java.lang.String r3 = r0.A02()
            java.lang.String r0 = "call_type"
            r5.A0A(r0, r3)
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            int r7 = X.AnonymousClass1Y3.AME
            X.0UN r3 = r0.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r4, r7, r3)
            X.0sv r7 = (X.C14260sv) r7
            X.7iB r0 = r7.A0C
            if (r0 == 0) goto L_0x009b
            boolean r3 = r0.A02()
            r0 = 1
            if (r3 != 0) goto L_0x009c
        L_0x009b:
            r0 = 0
        L_0x009c:
            if (r0 == 0) goto L_0x04fd
            java.lang.String r3 = "MESSENGER"
        L_0x00a0:
            r0 = 161(0xa1, float:2.26E-43)
            java.lang.String r0 = X.ECX.$const$string(r0)
            r5.A0A(r0, r3)
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            int r7 = X.AnonymousClass1Y3.AME
            X.0UN r3 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r7, r3)
            X.0sv r0 = (X.C14260sv) r0
            boolean r0 = r0.A0h
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "outgoing_call_param"
            r5.A08(r0, r3)
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            java.lang.String r0 = r0.A04()
            if (r0 == 0) goto L_0x00e0
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            java.lang.String r1 = r0.A04()
        L_0x00e0:
            java.lang.String r0 = "conference_name"
            r5.A0A(r0, r1)
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            java.lang.String r1 = r0.A05()
            java.lang.String r0 = "server_info_data"
            r5.A0A(r0, r1)
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r0 = (X.C166927nG) r0
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r7, r1)
            X.0sv r0 = (X.C14260sv) r0
            com.facebook.rtc.interfaces.RtcCallStartParams r0 = r0.A0D
            if (r0 != 0) goto L_0x04f9
            r0 = 0
        L_0x010b:
            java.lang.String r1 = "navigation_trigger_param"
            r5.A0A(r1, r0)
            X.0UN r0 = r13.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r1 = (X.C166927nG) r1
            X.0Tq r0 = r1.A01
            java.lang.Object r7 = r0.get()
            java.lang.String r7 = (java.lang.String) r7
            int r3 = X.AnonymousClass1Y3.Ayw
            X.0UN r1 = r1.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r3, r1)
            X.1m3 r0 = (X.C32621m3) r0
            com.google.common.collect.ImmutableList r3 = r0.A0D()
            com.google.common.base.Predicate r1 = com.google.common.base.Predicates.equalTo(r7)
            com.google.common.base.Predicates$NotPredicate r0 = new com.google.common.base.Predicates$NotPredicate
            r0.<init>(r1)
            java.lang.Iterable r0 = X.AnonymousClass0j4.A01(r3, r0)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = ","
            java.lang.String r1 = X.C06850cB.A06(r0, r1)
            java.lang.String r0 = "peer_user_ids"
            r5.A0A(r0, r1)
            X.0UN r0 = r13.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r6, r2, r0)
            X.7nG r8 = (X.C166927nG) r8
            int r0 = X.AnonymousClass1Y3.AME
            X.0UN r3 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r0, r3)
            X.0sv r0 = (X.C14260sv) r0
            long r0 = r0.A04
            r11 = 0
            int r2 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r2 != 0) goto L_0x04d2
            r7 = -4616189618054758400(0xbff0000000000000, double:-1.0)
        L_0x016a:
            java.lang.Double r1 = java.lang.Double.valueOf(r7)
            java.lang.String r0 = "seconds_since_call_start"
            r5.A09(r0, r1)
        L_0x0173:
            int r1 = X.AnonymousClass1Y3.AWL
            X.0UN r0 = r13.A00
            r2 = 4
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3zV r0 = (X.C84463zV) r0
            if (r0 == 0) goto L_0x01b8
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            java.lang.String r1 = A01(r0)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r2)
            r5.A0A(r0, r1)
            int r1 = X.AnonymousClass1Y3.AWL
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3zV r0 = (X.C84463zV) r0
            int r1 = X.AnonymousClass1Y3.AEg
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            android.content.res.Resources r0 = (android.content.res.Resources) r0
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r1 = r0.orientation
            r0 = 1
            if (r1 == r0) goto L_0x04ce
            r0 = 2
            if (r1 == r0) goto L_0x04ca
            java.lang.Integer r0 = X.AnonymousClass07B.A00
        L_0x01af:
            java.lang.String r1 = A01(r0)
            java.lang.String r0 = "interface_orientation"
            r5.A0A(r0, r1)
        L_0x01b8:
            int r1 = X.AnonymousClass1Y3.AHn
            X.0UN r0 = r13.A00
            r2 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3zX r0 = (X.C84483zX) r0
            if (r0 == 0) goto L_0x027e
            int r3 = X.AnonymousClass1Y3.Asj
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r3, r1)
            X.1pb r0 = (X.C34451pb) r0
            float r0 = r0.A02()
            java.lang.Float r1 = java.lang.Float.valueOf(r0)
            java.lang.String r0 = "battery_level"
            r5.A09(r0, r1)
            int r1 = X.AnonymousClass1Y3.AHn
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3zX r0 = (X.C84483zX) r0
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r3, r1)
            X.1pb r0 = (X.C34451pb) r0
            java.lang.Integer r0 = r0.A06()
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x04c2;
                case 2: goto L_0x04c6;
                case 3: goto L_0x04c6;
                case 4: goto L_0x04c6;
                default: goto L_0x01f9;
            }
        L_0x01f9:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
        L_0x01fb:
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x04be;
                case 2: goto L_0x04ba;
                default: goto L_0x0202;
            }
        L_0x0202:
            java.lang.String r1 = "UNKNOWN"
        L_0x0204:
            java.lang.String r0 = "battery_plugged_state"
            r5.A0A(r0, r1)
            int r1 = X.AnonymousClass1Y3.AHn
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3zX r0 = (X.C84483zX) r0
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r3, r1)
            X.1pb r0 = (X.C34451pb) r0
            java.lang.Integer r0 = r0.A04()
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x04ae;
                case 2: goto L_0x04ae;
                case 3: goto L_0x04b2;
                case 4: goto L_0x04b2;
                case 5: goto L_0x04b2;
                case 6: goto L_0x04b6;
                default: goto L_0x0226;
            }
        L_0x0226:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
        L_0x0228:
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x04aa;
                case 2: goto L_0x04a6;
                case 3: goto L_0x04a2;
                default: goto L_0x022f;
            }
        L_0x022f:
            java.lang.String r1 = "UNKNOWN"
        L_0x0231:
            java.lang.String r0 = "battery_charge_state"
            r5.A0A(r0, r1)
            int r1 = X.AnonymousClass1Y3.AHn
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3zX r0 = (X.C84483zX) r0
            int r2 = X.AnonymousClass1Y3.A8E
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r1)
            android.os.PowerManager r0 = (android.os.PowerManager) r0
            java.util.Map r2 = X.C163077gR.A00(r0)
            r0 = 38
            java.lang.String r1 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            boolean r0 = r2.containsKey(r1)
            if (r0 == 0) goto L_0x049e
            java.lang.Object r0 = r2.get(r1)
            if (r0 == 0) goto L_0x049e
            java.lang.String r1 = r0.toString()
            if (r1 == 0) goto L_0x049e
            java.lang.String r0 = "true"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0492
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
        L_0x0270:
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x048e;
                case 2: goto L_0x048a;
                default: goto L_0x0277;
            }
        L_0x0277:
            java.lang.String r1 = "UNKNOWN"
        L_0x0279:
            java.lang.String r0 = "battery_mode"
            r5.A0A(r0, r1)
        L_0x027e:
            int r2 = X.AnonymousClass1Y3.AM6
            X.0UN r1 = r13.A00
            r0 = 8
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.22e r0 = (X.C405922e) r0
            if (r0 == 0) goto L_0x02ae
            int r2 = X.AnonymousClass1Y3.AMf
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.7g1 r0 = (X.C162817g1) r0
            if (r0 == 0) goto L_0x0486
            X.7en r0 = r0.A01
            if (r0 == 0) goto L_0x0486
            java.lang.Integer r0 = r0.Ag1()
        L_0x02a0:
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x0482;
                case 2: goto L_0x047e;
                default: goto L_0x02a7;
            }
        L_0x02a7:
            java.lang.String r1 = "UNKNOWN"
        L_0x02a9:
            java.lang.String r0 = "camera_facing"
            r5.A0A(r0, r1)
        L_0x02ae:
            int r1 = X.AnonymousClass1Y3.AdK
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.3zY r0 = (X.C84493zY) r0
            if (r0 == 0) goto L_0x033e
            int r2 = X.AnonymousClass1Y3.A0a
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.7az r0 = (X.C159867az) r0
            X.7dx r0 = r0.A0s()
            android.media.AudioManager r0 = r0.A07
            boolean r0 = r0.isMicrophoneMute()
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "is_microphone_muted"
            r5.A08(r0, r1)
            int r1 = X.AnonymousClass1Y3.AdK
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.3zY r0 = (X.C84493zY) r0
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.7az r0 = (X.C159867az) r0
            X.7dx r0 = r0.A0s()
            X.7dy r0 = r0.A09
            boolean r0 = r0.A01()
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "is_bluetooth_available"
            r5.A08(r0, r1)
            int r1 = X.AnonymousClass1Y3.AdK
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.3zY r0 = (X.C84493zY) r0
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.7az r0 = (X.C159867az) r0
            X.7dx r0 = r0.A0s()
            boolean r0 = r0.A04
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "is_headset_attached"
            r5.A08(r0, r1)
            int r1 = X.AnonymousClass1Y3.AdK
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.3zY r0 = (X.C84493zY) r0
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.7az r0 = (X.C159867az) r0
            X.7dx r0 = r0.A0s()
            X.7fX r0 = r0.A01
            java.lang.String r1 = r0.name()
            java.lang.String r0 = "audio_output_name"
            r5.A0A(r0, r1)
        L_0x033e:
            int r1 = X.AnonymousClass1Y3.B4P
            X.0UN r0 = r13.A00
            r2 = 5
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.7hN r4 = (X.C163507hN) r4
            if (r4 == 0) goto L_0x0393
            long r0 = r4.A00
            r7 = 0
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 != 0) goto L_0x0356
            r4.A01()
        L_0x0356:
            long r0 = r4.A00
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "logging_instance_id"
            r5.A09(r0, r1)
            int r1 = X.AnonymousClass1Y3.B4P
            X.0UN r0 = r13.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.7hN r3 = (X.C163507hN) r3
            long r0 = r3.A00
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 != 0) goto L_0x0374
            r3.A01()
        L_0x0374:
            java.util.concurrent.atomic.AtomicLong r0 = r3.A01
            if (r0 != 0) goto L_0x0478
            java.lang.String r1 = "RtcLoggingSession"
            java.lang.String r0 = "Unexpected unset row number"
            X.C010708t.A0K(r1, r0)
            r0 = -1
        L_0x0381:
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "rownum"
            r5.A09(r0, r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)
            java.lang.String r0 = "logger_schema_version"
            r5.A09(r0, r1)
        L_0x0393:
            int r1 = X.AnonymousClass1Y3.A0f
            X.0UN r0 = r13.A00
            r2 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3zW r0 = (X.C84473zW) r0
            if (r0 == 0) goto L_0x03d9
            int r3 = X.AnonymousClass1Y3.Asv
            X.0UN r1 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r3, r1)
            X.069 r0 = (X.AnonymousClass069) r0
            long r0 = r0.now()
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "ms_steady"
            r5.A09(r0, r1)
            int r1 = X.AnonymousClass1Y3.A0f
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3zW r0 = (X.C84473zW) r0
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.06B r0 = (X.AnonymousClass06B) r0
            long r0 = r0.now()
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "ms_since_epoch"
            r5.A09(r0, r1)
        L_0x03d9:
            int r2 = X.AnonymousClass1Y3.ABE
            X.0UN r1 = r13.A00
            r0 = 6
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.55h r4 = (X.C1060855h) r4
            if (r4 == 0) goto L_0x03fd
            int r2 = X.AnonymousClass1Y3.B1G
            X.0UN r1 = r4.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.7Ic r0 = (X.C155757Ic) r0
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r0.A01()
            if (r3 != 0) goto L_0x0461
            r0 = 0
        L_0x03f8:
            java.lang.String r1 = "living_room_id"
            r5.A0A(r1, r0)
        L_0x03fd:
            int r2 = X.AnonymousClass1Y3.AkD
            X.0UN r1 = r13.A00
            r0 = 7
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.55i r0 = (X.AnonymousClass55i) r0
            if (r0 == 0) goto L_0x0422
            int r2 = X.AnonymousClass1Y3.BLG
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.7pr r0 = (X.C168437pr) r0
            boolean r0 = r0.A03()
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "device_locked"
            r5.A08(r0, r1)
        L_0x0422:
            java.lang.String r3 = "Logging"
            r6 = 4
            java.lang.String r2 = r5.A0C()
            X.0os r0 = r5.A0B()
            int r0 = r0.A00
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "%s: %s EventBuilder with %d key/value pairs:"
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r3, r2, r1)
            X.C010708t.A02(r6, r10, r0)
            r3 = 0
        L_0x043d:
            X.0os r0 = r5.A0B()
            int r0 = r0.A00
            if (r3 >= r0) goto L_0x051b
            X.0os r0 = r5.A0B()
            java.lang.String r2 = r0.A0H(r3)
            X.0os r0 = r5.A0B()
            java.lang.Object r1 = r0.A0G(r3)
            java.lang.String r0 = "\t%s: %s"
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r2, r1)
            X.C010708t.A02(r6, r10, r0)
            int r3 = r3 + 1
            goto L_0x043d
        L_0x0461:
            int r1 = X.AnonymousClass1Y3.Av5
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.21H r0 = (X.AnonymousClass21H) r0
            java.lang.String r1 = r3.A0I()
            java.util.Map r0 = r0.A02
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x03f8
        L_0x0478:
            long r0 = r0.incrementAndGet()
            goto L_0x0381
        L_0x047e:
            java.lang.String r1 = "BACK"
            goto L_0x02a9
        L_0x0482:
            java.lang.String r1 = "FRONT"
            goto L_0x02a9
        L_0x0486:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            goto L_0x02a0
        L_0x048a:
            java.lang.String r1 = "SYSTEM_LOW_POWER"
            goto L_0x0279
        L_0x048e:
            java.lang.String r1 = "NORMAL"
            goto L_0x0279
        L_0x0492:
            java.lang.String r0 = "false"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x049e
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            goto L_0x0270
        L_0x049e:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            goto L_0x0270
        L_0x04a2:
            java.lang.String r1 = "FULL"
            goto L_0x0231
        L_0x04a6:
            java.lang.String r1 = "NOT_CHARGING"
            goto L_0x0231
        L_0x04aa:
            java.lang.String r1 = "CHARGING"
            goto L_0x0231
        L_0x04ae:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            goto L_0x0228
        L_0x04b2:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            goto L_0x0228
        L_0x04b6:
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            goto L_0x0228
        L_0x04ba:
            java.lang.String r1 = "PLUGGED"
            goto L_0x0204
        L_0x04be:
            java.lang.String r1 = "UNPLUGGED"
            goto L_0x0204
        L_0x04c2:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            goto L_0x01fb
        L_0x04c6:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            goto L_0x01fb
        L_0x04ca:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            goto L_0x01af
        L_0x04ce:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            goto L_0x01af
        L_0x04d2:
            r1 = 2
            int r0 = X.AnonymousClass1Y3.Asv
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r3)
            X.069 r0 = (X.AnonymousClass069) r0
            long r2 = r0.now()
            int r1 = X.AnonymousClass1Y3.AME
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0sv r0 = (X.C14260sv) r0
            long r0 = r0.A04
            long r2 = r2 - r0
            double r7 = (double) r2
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.SECONDS
            r0 = 1
            long r2 = r2.toMillis(r0)
            double r0 = (double) r2
            double r7 = r7 / r0
            goto L_0x016a
        L_0x04f9:
            java.lang.String r0 = r0.A0D
            goto L_0x010b
        L_0x04fd:
            boolean r0 = r7.A0k()
            if (r0 == 0) goto L_0x0507
            java.lang.String r3 = "GROUP"
            goto L_0x00a0
        L_0x0507:
            boolean r0 = r7.A0m()
            if (r0 == 0) goto L_0x0511
            java.lang.String r3 = "P2P"
            goto L_0x00a0
        L_0x0511:
            java.lang.String r3 = "UNKNOWN"
            goto L_0x00a0
        L_0x0515:
            r3 = r1
            goto L_0x0068
        L_0x0518:
            r9 = r1
            goto L_0x0032
        L_0x051b:
            java.lang.Object[] r1 = new java.lang.Object[]{r14, r15, r9}
            java.lang.String r0 = "%s: source: %s params: %s"
            X.C157217Oq.A02(r10, r0, r1)
            r5.A0E()
        L_0x0527:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38481xV.A02(java.lang.String, java.lang.String, X.7Ib):void");
    }

    public C38481xV(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(10, r3);
    }

    public void A04(String str, C155747Ib r3) {
        Preconditions.checkNotNull(r3);
        A02(str, null, r3);
    }

    public void A05(String str, String str2) {
        Preconditions.checkNotNull(str2);
        A02(str, str2, null);
    }

    public void A06(String str, String str2, C155747Ib r3) {
        Preconditions.checkNotNull(str2);
        Preconditions.checkNotNull(r3);
        A02(str, str2, r3);
    }
}
