package X;

import android.os.Build;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0yl  reason: invalid class name and case insensitive filesystem */
public final class C17350yl implements C05460Za {
    private static volatile C17350yl A03;
    public AnonymousClass0UN A00;
    private final C001500z A01;
    private final Boolean A02;

    public void clearUserData() {
    }

    public static final C17350yl A01(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C17350yl.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C17350yl(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public int A02() {
        return (int) ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).At0(563976455062154L);
    }

    public int A03() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).AqL(563976452375175L, 40);
    }

    public boolean A04() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501476255194L);
    }

    public boolean A05() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501487199842L);
    }

    public boolean A06() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501476386268L);
    }

    public boolean A07() {
        if (!((AnonymousClass1GE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BHw, this.A00)).A02() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(2306125510697354800L)) {
            return false;
        }
        return true;
    }

    public boolean A08() {
        if (this.A02.booleanValue() || this.A01 != C001500z.A07 || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501477238249L)) {
            return false;
        }
        return true;
    }

    public boolean A09() {
        return ((AnonymousClass1GE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BHw, this.A00)).A02();
    }

    public boolean A0A() {
        if (Build.VERSION.SDK_INT < 16 || !((AnonymousClass1GE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BHw, this.A00)).A02()) {
            return false;
        }
        return true;
    }

    public boolean A0B() {
        if (Build.VERSION.SDK_INT < 16 || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501476648416L)) {
            return false;
        }
        return true;
    }

    public boolean A0C() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501474289091L);
    }

    public boolean A0D() {
        if (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501476713953L) || A02() != 0) {
            return true;
        }
        return false;
    }

    public boolean A0E() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501487724138L);
    }

    public boolean A0F() {
        if (!((C16510xE) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADT, this.A00)).A0B() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501479663099L)) {
            return false;
        }
        return true;
    }

    public boolean A0H() {
        if (!((C16510xE) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADT, this.A00)).A0B() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501479794173L)) {
            return false;
        }
        return true;
    }

    public boolean A0I() {
        if (!((C16510xE) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADT, this.A00)).A0B() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501479728636L)) {
            return false;
        }
        return true;
    }

    public boolean A0J() {
        if (!((C16510xE) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADT, this.A00)).A0B() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501474944459L)) {
            return false;
        }
        return true;
    }

    public boolean A0K() {
        if (!((C16510xE) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADT, this.A00)).A0B() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501477565934L)) {
            return false;
        }
        return true;
    }

    public boolean A0L() {
        if (!((C16510xE) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADT, this.A00)).A0B() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501482153500L)) {
            return false;
        }
        return true;
    }

    public boolean A0M() {
        if (!this.A01.equals(C001500z.A08) || !((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(778, false)) {
            return false;
        }
        return true;
    }

    private C17350yl(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A01 = AnonymousClass0UU.A05(r3);
        this.A02 = AnonymousClass0UU.A08(r3);
    }

    public static final C17350yl A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public boolean A0G() {
        if (A0H() || A0I()) {
            return true;
        }
        return false;
    }
}
