package X;

import com.facebook.common.util.TriState;
import com.facebook.gk.sessionless.GkSessionlessModule;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.156  reason: invalid class name */
public final class AnonymousClass156 extends AnonymousClass0UV {
    public static final String A07(AnonymousClass1XY r1) {
        if (((Boolean) AnonymousClass0VG.A00(AnonymousClass1Y3.Amx, r1).get()).booleanValue()) {
            return "dialtone";
        }
        return "normal";
    }

    public static final TriState A00(AnonymousClass1XY r1) {
        return AnonymousClass0WA.A00(r1).Ab9(450);
    }

    public static final Boolean A01(AnonymousClass1XY r2) {
        return Boolean.valueOf(AnonymousClass0WA.A00(r2).AbO(731, false));
    }

    public static final Boolean A02(AnonymousClass1XY r2) {
        return Boolean.valueOf(AnonymousClass0WA.A00(r2).AbO(63, false));
    }

    public static final Boolean A03(AnonymousClass1XY r2) {
        return Boolean.valueOf(AnonymousClass0WA.A00(r2).AbO(732, false));
    }

    public static final Boolean A04(AnonymousClass1XY r2) {
        return Boolean.valueOf(GkSessionlessModule.A00(r2).AbO(137, false));
    }

    public static final Boolean A05(AnonymousClass1XY r2) {
        return Boolean.valueOf(AnonymousClass0WA.A00(r2).AbO(852, true));
    }

    public static final Boolean A06(AnonymousClass1XY r2) {
        return Boolean.valueOf(GkSessionlessModule.A00(r2).AbO(AnonymousClass1Y3.A14, true));
    }
}
