package X;

import android.content.res.ColorStateList;

/* renamed from: X.1KA  reason: invalid class name */
public final class AnonymousClass1KA {
    public static final int[] A00 = {-16842910};
    private static final int[] A01 = {16842910};
    private static final int[] A02 = {16842919};

    public static int A00(ColorStateList colorStateList) {
        int[] iArr = A01;
        if (colorStateList != null) {
            return colorStateList.getColorForState(iArr, 0);
        }
        return 0;
    }

    public static int A01(ColorStateList colorStateList) {
        int[] iArr = A02;
        if (colorStateList != null) {
            return colorStateList.getColorForState(iArr, 0);
        }
        return 0;
    }
}
