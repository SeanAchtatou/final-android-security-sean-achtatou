package X;

/* renamed from: X.0NY  reason: invalid class name */
public final class AnonymousClass0NY extends Exception {
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public AnonymousClass0NY(String str) {
        super(str);
    }
}
