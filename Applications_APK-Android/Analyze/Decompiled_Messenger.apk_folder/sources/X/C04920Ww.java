package X;

import android.content.Context;
import com.facebook.inject.InjectorModule;
import java.util.concurrent.ExecutorService;

@InjectorModule
/* renamed from: X.0Ww  reason: invalid class name and case insensitive filesystem */
public final class C04920Ww extends AnonymousClass0UV {
    private static volatile AnonymousClass09P A00;
    private static volatile AnonymousClass06D A01;

    public static final AnonymousClass09P A00(AnonymousClass1XY r5) {
        if (A00 == null) {
            synchronized (AnonymousClass09P.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        Context A02 = AnonymousClass1YA.A02(applicationInjector);
                        ExecutorService A04 = C04750Wa.A04(applicationInjector);
                        AnonymousClass09P A012 = A01(applicationInjector);
                        if (AnonymousClass00J.A01(A02).A1Z) {
                            A012 = new C187028mM(A04, new C187058mP());
                        }
                        A00 = A012;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r4.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0062, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.AnonymousClass06D A01(X.AnonymousClass1XY r13) {
        /*
            X.06D r0 = X.C04920Ww.A01
            if (r0 != 0) goto L_0x006b
            java.lang.Class<X.06D> r5 = X.AnonymousClass06D.class
            monitor-enter(r5)
            X.06D r0 = X.C04920Ww.A01     // Catch:{ all -> 0x0068 }
            X.0WD r4 = X.AnonymousClass0WD.A00(r0, r13)     // Catch:{ all -> 0x0068 }
            if (r4 == 0) goto L_0x0066
            X.1XY r3 = r13.getApplicationInjector()     // Catch:{ all -> 0x005e }
            int r0 = X.AnonymousClass1Y3.AD4     // Catch:{ all -> 0x005e }
            X.0VB r8 = X.AnonymousClass0VB.A00(r0, r3)     // Catch:{ all -> 0x005e }
            java.util.concurrent.ExecutorService r2 = X.C04750Wa.A04(r3)     // Catch:{ all -> 0x005e }
            java.util.Random r11 = X.AnonymousClass0W9.A01()     // Catch:{ all -> 0x005e }
            android.content.Context r12 = X.AnonymousClass1YA.A02(r3)     // Catch:{ all -> 0x005e }
            X.069 r10 = X.AnonymousClass067.A03(r3)     // Catch:{ all -> 0x005e }
            X.1YI r1 = X.AnonymousClass0WA.A00(r3)     // Catch:{ all -> 0x005e }
            int r0 = X.AnonymousClass1Y3.B0l     // Catch:{ all -> 0x005e }
            X.AnonymousClass0VG.A00(r0, r3)     // Catch:{ all -> 0x005e }
            X.0Wx r13 = new X.0Wx     // Catch:{ all -> 0x005e }
            r13.<init>(r1)     // Catch:{ all -> 0x005e }
            X.0Wy r7 = new X.0Wy     // Catch:{ all -> 0x005e }
            r7.<init>(r1)     // Catch:{ all -> 0x005e }
            X.06D r6 = new X.06D     // Catch:{ all -> 0x005e }
            r9 = r2
            r6.<init>(r7, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x005e }
            java.lang.Class<com.facebook.jni.NativeSoftErrorReporterProxy> r1 = com.facebook.jni.NativeSoftErrorReporterProxy.class
            monitor-enter(r1)     // Catch:{ all -> 0x005e }
            com.facebook.jni.NativeSoftErrorReporterProxy.sErrorReportingGkReader = r13     // Catch:{ all -> 0x005b }
            com.facebook.jni.NativeSoftErrorReporterProxy.sErrorReportingExecutorService = r2     // Catch:{ all -> 0x005b }
            java.lang.ref.WeakReference r0 = com.facebook.jni.NativeSoftErrorReporterProxy.sFbErrorReporterWeakReference     // Catch:{ all -> 0x005b }
            if (r0 != 0) goto L_0x0057
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x005b }
            r0.<init>(r6)     // Catch:{ all -> 0x005b }
            com.facebook.jni.NativeSoftErrorReporterProxy.sFbErrorReporterWeakReference = r0     // Catch:{ all -> 0x005b }
            com.facebook.jni.NativeSoftErrorReporterProxy.flushSoftErrorCacheAsync()     // Catch:{ all -> 0x005b }
        L_0x0057:
            monitor-exit(r1)     // Catch:{ all -> 0x005e }
            X.C04920Ww.A01 = r6     // Catch:{ all -> 0x005e }
            goto L_0x0063
        L_0x005b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            r4.A01()     // Catch:{ all -> 0x0068 }
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x0063:
            r4.A01()     // Catch:{ all -> 0x0068 }
        L_0x0066:
            monitor-exit(r5)     // Catch:{ all -> 0x0068 }
            goto L_0x006b
        L_0x0068:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0068 }
            throw r0
        L_0x006b:
            X.06D r0 = X.C04920Ww.A01
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04920Ww.A01(X.1XY):X.06D");
    }
}
