package X;

import java.io.File;

/* renamed from: X.0EX  reason: invalid class name */
public final class AnonymousClass0EX implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.soloader.UnpackingSoSource$1";
    public final /* synthetic */ AnonymousClass01B A00;
    public final /* synthetic */ AnonymousClass0ET A01;
    public final /* synthetic */ AnonymousClass02E A02;
    public final /* synthetic */ File A03;
    public final /* synthetic */ File A04;
    public final /* synthetic */ byte[] A05;

    public AnonymousClass0EX(AnonymousClass02E r1, File file, byte[] bArr, AnonymousClass0ET r4, File file2, AnonymousClass01B r6) {
        this.A02 = r1;
        this.A03 = file;
        this.A05 = bArr;
        this.A01 = r4;
        this.A04 = file2;
        this.A00 = r6;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:29|30) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0065, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006c, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0070 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            java.lang.String r3 = "rw"
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0071 }
            java.io.File r0 = r5.A03     // Catch:{ all -> 0x0071 }
            r2.<init>(r0, r3)     // Catch:{ all -> 0x0071 }
            byte[] r0 = r5.A05     // Catch:{ all -> 0x006a }
            r2.write(r0)     // Catch:{ all -> 0x006a }
            long r0 = r2.getFilePointer()     // Catch:{ all -> 0x006a }
            r2.setLength(r0)     // Catch:{ all -> 0x006a }
            r2.close()     // Catch:{ all -> 0x0071 }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x0071 }
            X.02E r0 = r5.A02     // Catch:{ all -> 0x0071 }
            java.io.File r1 = r0.A00     // Catch:{ all -> 0x0071 }
            java.lang.String r0 = "dso_manifest"
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0071 }
            java.io.RandomAccessFile r4 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0071 }
            r4.<init>(r2, r3)     // Catch:{ all -> 0x0071 }
            X.0ET r3 = r5.A01     // Catch:{ all -> 0x0063 }
            r0 = 1
            r4.writeByte(r0)     // Catch:{ all -> 0x0063 }
            X.0EV[] r0 = r3.A00     // Catch:{ all -> 0x0063 }
            int r0 = r0.length     // Catch:{ all -> 0x0063 }
            r4.writeInt(r0)     // Catch:{ all -> 0x0063 }
            r2 = 0
        L_0x0035:
            X.0EV[] r1 = r3.A00     // Catch:{ all -> 0x0063 }
            int r0 = r1.length     // Catch:{ all -> 0x0063 }
            if (r2 >= r0) goto L_0x004d
            r0 = r1[r2]     // Catch:{ all -> 0x0063 }
            java.lang.String r0 = r0.A01     // Catch:{ all -> 0x0063 }
            r4.writeUTF(r0)     // Catch:{ all -> 0x0063 }
            X.0EV[] r0 = r3.A00     // Catch:{ all -> 0x0063 }
            r0 = r0[r2]     // Catch:{ all -> 0x0063 }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x0063 }
            r4.writeUTF(r0)     // Catch:{ all -> 0x0063 }
            int r2 = r2 + 1
            goto L_0x0035
        L_0x004d:
            r4.close()     // Catch:{ all -> 0x0071 }
            X.02E r0 = r5.A02     // Catch:{ all -> 0x0071 }
            java.io.File r0 = r0.A00     // Catch:{ all -> 0x0071 }
            A00(r0)     // Catch:{ all -> 0x0071 }
            java.io.File r1 = r5.A04     // Catch:{ all -> 0x0071 }
            r0 = 1
            X.AnonymousClass02E.A04(r1, r0)     // Catch:{ all -> 0x0071 }
            X.01B r0 = r5.A00     // Catch:{ IOException -> 0x0078 }
            r0.close()     // Catch:{ IOException -> 0x0078 }
            return
        L_0x0063:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0065 }
        L_0x0065:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x0070 }
            goto L_0x0070
        L_0x006a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x006c }
        L_0x006c:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0070 }
        L_0x0070:
            throw r0     // Catch:{ all -> 0x0071 }
        L_0x0071:
            r1 = move-exception
            X.01B r0 = r5.A00     // Catch:{ IOException -> 0x0078 }
            r0.close()     // Catch:{ IOException -> 0x0078 }
            throw r1     // Catch:{ IOException -> 0x0078 }
        L_0x0078:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0EX.run():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0050, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(java.io.File r3) {
        /*
            boolean r0 = r3.isDirectory()
            if (r0 == 0) goto L_0x002c
            java.io.File[] r2 = r3.listFiles()
            if (r2 == 0) goto L_0x0018
            r1 = 0
        L_0x000d:
            int r0 = r2.length
            if (r1 >= r0) goto L_0x0051
            r0 = r2[r1]
            A00(r0)
            int r1 = r1 + 1
            goto L_0x000d
        L_0x0018:
            java.io.IOException r2 = new java.io.IOException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "cannot list directory "
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x002c:
            java.lang.String r1 = r3.getPath()
            java.lang.String r0 = "_lock"
            boolean r0 = r1.endsWith(r0)
            if (r0 != 0) goto L_0x0051
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile
            java.lang.String r0 = "r"
            r1.<init>(r3, r0)
            java.io.FileDescriptor r0 = r1.getFD()     // Catch:{ all -> 0x004a }
            r0.sync()     // Catch:{ all -> 0x004a }
            r1.close()
            return
        L_0x004a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004c }
        L_0x004c:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0050 }
        L_0x0050:
            throw r0
        L_0x0051:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0EX.A00(java.io.File):void");
    }
}
