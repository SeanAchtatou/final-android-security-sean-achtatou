package X;

import android.os.Build;
import android.os.Trace;

/* renamed from: X.0ad  reason: invalid class name and case insensitive filesystem */
public final class C05970ad {
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 18 && i < 29) {
            Class<Trace> cls = Trace.class;
            try {
                cls.getField("TRACE_TAG_APP").getLong(null);
                cls.getMethod("isTagEnabled", Long.TYPE);
                Class<String> cls2 = String.class;
                cls.getMethod("asyncTraceBegin", Long.TYPE, cls2, Integer.TYPE);
                Class cls3 = Long.TYPE;
                Class cls4 = Integer.TYPE;
                cls.getMethod("asyncTraceEnd", cls3, cls2, cls4);
                cls.getMethod("traceCounter", Long.TYPE, cls2, cls4);
            } catch (Exception unused) {
            }
        }
    }

    public static void A00() {
        if (Build.VERSION.SDK_INT >= 18) {
            Trace.endSection();
        }
    }

    public static void A01(String str) {
        if (Build.VERSION.SDK_INT >= 18) {
            Trace.beginSection(str);
        }
    }
}
