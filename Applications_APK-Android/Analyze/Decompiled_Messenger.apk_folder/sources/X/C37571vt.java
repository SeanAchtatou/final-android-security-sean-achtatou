package X;

import java.io.IOException;

/* renamed from: X.1vt  reason: invalid class name and case insensitive filesystem */
public class C37571vt extends IOException {
    public static final long serialVersionUID = 123;
    public C64443Bu _location;

    public String getMessageSuffix() {
        return null;
    }

    public String getMessage() {
        String message = super.getMessage();
        if (message == null) {
            message = "N/A";
        }
        C64443Bu r3 = this._location;
        String messageSuffix = getMessageSuffix();
        if (r3 == null && messageSuffix == null) {
            return message;
        }
        StringBuilder sb = new StringBuilder(100);
        sb.append(message);
        if (messageSuffix != null) {
            sb.append(messageSuffix);
        }
        if (r3 != null) {
            sb.append(10);
            sb.append(" at ");
            sb.append(r3.toString());
        }
        return sb.toString();
    }

    public String toString() {
        return AnonymousClass08S.A0P(getClass().getName(), ": ", getMessage());
    }

    public C37571vt(String str) {
        super(str);
    }

    public C37571vt(String str, C64443Bu r2, Throwable th) {
        super(str);
        if (th != null) {
            initCause(th);
        }
        this._location = r2;
    }
}
