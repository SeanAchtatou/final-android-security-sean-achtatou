package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.video.analytics.TimedMicroStorage;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.222  reason: invalid class name */
public final class AnonymousClass222 implements AnonymousClass0qy {
    public static final Class A0O = AnonymousClass222.class;
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06 = 0;
    public long A07 = 0;
    public List A08;
    public Map A09;
    public final DeprecatedAnalyticsLogger A0A;
    public final AnonymousClass13b A0B;
    public final C09340h3 A0C;
    public final AnonymousClass069 A0D;
    public final C76083kw A0E;
    public final AnonymousClass1YI A0F;
    public final AnonymousClass2YD A0G;
    public final C76073kv A0H;
    public final C76073kv A0I;
    public final C76043kr A0J;
    public final TimedMicroStorage A0K;
    public final C76033kq A0L;
    public final C76053ks A0M;
    private final ExecutorService A0N;

    public static synchronized void A02(AnonymousClass222 r2) {
        synchronized (r2) {
            r2.A00 = 0;
            r2.A02 = 0;
            r2.A01 = 0;
            r2.A03 = 0;
            r2.A05 = 0;
            r2.A04 = 0;
        }
    }

    public synchronized void A03(C11670nb r8) {
        boolean z;
        synchronized (this) {
            z = false;
            if (this.A08 == null) {
                z = true;
            }
        }
        if (z) {
            r8.A0A(AnonymousClass80H.$const$string(AnonymousClass1Y3.A10), this.A00);
            r8.A0A("bytes_downloaded_cell", this.A02);
            r8.A0A("bytes_downloaded_metered", this.A01);
            r8.A0A("bytes_prefetched", this.A03);
            r8.A0A("bytes_prefetched_wifi", this.A05);
            r8.A0A("bytes_prefetched_cell", this.A04);
            A02(this);
            A01(this);
            double A012 = (double) this.A0H.A01();
            Double.isNaN(A012);
            r8.A08("time_spent", A012 / 1000.0d);
            double A013 = (double) this.A0I.A01();
            Double.isNaN(A013);
            r8.A08("time_spent_in_cell", A013 / 1000.0d);
            r8.A0A("bytes_watched", this.A0H.A00());
            r8.A0A("bytes_watched_in_cell", this.A0I.A00());
        }
    }

    public static JsonNode A00(Map map) {
        AnonymousClass0jJ r6 = new AnonymousClass0jJ();
        ObjectNode createObjectNode = r6.createObjectNode();
        for (Map.Entry entry : map.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof Integer) {
                createObjectNode.put((String) entry.getKey(), ((Integer) value).intValue());
            } else if (value instanceof String) {
                createObjectNode.put((String) entry.getKey(), (String) value);
            } else {
                try {
                    createObjectNode.put((String) entry.getKey(), r6.writeValueAsString(value));
                } catch (C37571vt unused) {
                    entry.getKey();
                    createObjectNode.put((String) entry.getKey(), value.toString());
                }
            }
        }
        return createObjectNode;
    }

    public static void A01(AnonymousClass222 r6) {
        TimedMicroStorage timedMicroStorage = r6.A0K;
        C23029BQx bQx = new C23029BQx(r6);
        Preconditions.checkArgument(timedMicroStorage.A01.get(), "Calling write without having read info first!");
        if (!timedMicroStorage.A00.getAndSet(true)) {
            timedMicroStorage.A06.schedule(new C23027BQv(timedMicroStorage, bQx), (long) timedMicroStorage.A02, TimeUnit.MILLISECONDS);
        }
    }

    public void A04(String str, Map map) {
        if (!this.A0N.isShutdown()) {
            AnonymousClass07A.A04(this.A0N, new C75243jW(this, str, map), -1762458149);
        }
    }

    public void Aay(AnonymousClass18d r2) {
        r2.A00(29);
        r2.A00(30);
        r2.A00(36);
        r2.A00(37);
        r2.A00(38);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0426, code lost:
        if (r3.getType() != 1) goto L_0x0428;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Aaz(X.AnonymousClass2W7 r29) {
        /*
            r28 = this;
            r4 = r29
            int r1 = r4.Aax()
            r0 = 29
            r2 = r28
            if (r1 != r0) goto L_0x02ed
            X.AA9 r4 = (X.AA9) r4
            X.8So r7 = r4.A00
            X.3ks r6 = r2.A0M
            boolean r0 = X.C76053ks.A09(r6)
            if (r0 == 0) goto L_0x0028
            boolean r0 = r7.isAudioAbrDecision
            if (r0 == 0) goto L_0x0068
            java.lang.String r1 = r7.decisionReasons
            X.AC1 r0 = X.AC1.A05
            java.lang.String r0 = r0.shortName
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0068
        L_0x0028:
            java.util.Map r0 = r2.A09
            if (r0 == 0) goto L_0x0368
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0368
            java.lang.String r1 = r7.videoId
            if (r1 == 0) goto L_0x0368
            java.util.Map r0 = r2.A09
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x0368
            java.util.Map r1 = r2.A09
            java.lang.String r0 = r7.videoId
            java.lang.Object r0 = r1.get(r0)
            java.util.List r0 = (java.util.List) r0
            java.util.Iterator r2 = r0.iterator()
        L_0x004c:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0368
            java.lang.Object r1 = r2.next()
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1
            java.lang.Object r0 = r1.get()
            if (r0 == 0) goto L_0x004c
            java.lang.Object r0 = r1.get()
            X.8Kk r0 = (X.C178228Kk) r0
            r0.ByF(r7)
            goto L_0x004c
        L_0x0068:
            java.lang.String r0 = r7.videoId
            int r5 = r0.hashCode()
            com.facebook.quicklog.QuickPerformanceLogger r0 = r6.A0E
            r4 = 1900580(0x1d0024, float:2.66328E-39)
            r0.markerStart(r4, r5)
            com.google.common.collect.ImmutableMap$Builder r3 = com.google.common.collect.ImmutableMap.builder()
            long r0 = r7.timeMs
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "time_ms"
            r3.put(r0, r1)
            java.lang.String r1 = r7.videoId
            java.lang.String r0 = "video_id"
            r3.put(r0, r1)
            long r0 = r7.playerId
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "player_id"
            r3.put(r0, r1)
            boolean r0 = r7.isLive
            java.lang.String r1 = java.lang.Boolean.toString(r0)
            java.lang.String r0 = "is_live"
            r3.put(r0, r1)
            long r0 = r7.videoPositionMs
            java.lang.String r1 = java.lang.Long.toString(r0)
            r0 = 782(0x30e, float:1.096E-42)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r3.put(r0, r1)
            long r0 = r7.bufferDurationMs
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "buffer_duration_ms"
            r3.put(r0, r1)
            long r0 = r7.segmentStartMs
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "segment_start_ms"
            r3.put(r0, r1)
            long r0 = r7.segmentDurationMs
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "segment_duration_ms"
            r3.put(r0, r1)
            long r0 = r7.bandwidthEstimate
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "bandwidth_estimate"
            r3.put(r0, r1)
            int r0 = r7.currentBitrate
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "current_bitrate"
            r3.put(r0, r1)
            int r0 = r7.nextBitrate
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "next_bitrate"
            r3.put(r0, r1)
            int r0 = r7.constraintFormatBitrate
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "constraint_bitrate"
            r3.put(r0, r1)
            java.lang.String r1 = r7.decisionReasons
            java.lang.String r0 = "decision_reasons"
            r3.put(r0, r1)
            int r0 = r7.constraintWidth
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "constraint_width"
            r3.put(r0, r1)
            java.lang.String r1 = r7.constraintReasons
            java.lang.String r0 = "constraint_reasons"
            r3.put(r0, r1)
            java.lang.String r1 = r7.formatBandwidthEstimates
            java.lang.String r0 = "format_bandwidth_estimates"
            r3.put(r0, r1)
            boolean r0 = r7.isPrefetch
            java.lang.String r1 = java.lang.Boolean.toString(r0)
            java.lang.String r0 = "is_prefetch"
            r3.put(r0, r1)
            boolean r0 = r7.isBufferFalling
            java.lang.String r1 = java.lang.Boolean.toString(r0)
            java.lang.String r0 = "is_buffer_falling"
            r3.put(r0, r1)
            int r0 = r7.bandwidthConfidencePct
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "bandwidth_confidence_pct"
            r3.put(r0, r1)
            long r0 = r7.bandwidthEstimateConfBased
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "bandwidth_estimate_confidence_based"
            r3.put(r0, r1)
            int r0 = r7.minViewportDimension
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "min_viewport_dimension"
            r3.put(r0, r1)
            float r0 = r7.formatMos
            java.lang.String r1 = java.lang.Float.toString(r0)
            java.lang.String r0 = "format_mos"
            r3.put(r0, r1)
            java.lang.String r1 = r7.playerOrigin
            java.lang.String r0 = "player_origin"
            r3.put(r0, r1)
            boolean r0 = r7.isAudioAbrDecision
            java.lang.String r1 = java.lang.Boolean.toString(r0)
            java.lang.String r0 = "is_audio"
            r3.put(r0, r1)
            boolean r0 = r7.isWifi
            java.lang.String r1 = java.lang.Boolean.toString(r0)
            java.lang.String r0 = "is_wifi"
            r3.put(r0, r1)
            java.lang.String r1 = r7.currentQualityLabel
            if (r1 == 0) goto L_0x0186
            java.lang.String r0 = "current_quality_label"
            r3.put(r0, r1)
        L_0x0186:
            java.lang.String r1 = r7.nextQualityLabel
            if (r1 == 0) goto L_0x018f
            java.lang.String r0 = "next_quality_label"
            r3.put(r0, r1)
        L_0x018f:
            java.lang.String r1 = r7.highestFormatQualityLabelFromManifest
            if (r1 == 0) goto L_0x0198
            java.lang.String r0 = "highest_quality_label_from_manifest"
            r3.put(r0, r1)
        L_0x0198:
            java.lang.String r1 = r7.constraintFormatQualityLabel
            if (r1 == 0) goto L_0x01a1
            java.lang.String r0 = "constraint_quality_label"
            r3.put(r0, r1)
        L_0x01a1:
            java.lang.String r1 = r7.dataConnectionQuality
            if (r1 == 0) goto L_0x01aa
            java.lang.String r0 = "data_connection_quality"
            r3.put(r0, r1)
        L_0x01aa:
            com.google.common.collect.ImmutableMap r0 = r3.build()
            X.C76053ks.A02(r6, r4, r5, r0)
            com.facebook.quicklog.QuickPerformanceLogger r1 = r6.A0E
            r0 = 2
            r1.markerEnd(r4, r5, r0)
            java.lang.String r1 = r7.videoId
            java.lang.String r0 = r6.A0J
            boolean r0 = r1.equals(r0)
            r11 = 0
            if (r0 != 0) goto L_0x01ca
            java.lang.String r0 = r7.videoId
            r6.A0J = r0
            r6.A0L = r11
            r6.A0K = r11
        L_0x01ca:
            boolean r0 = r7.isAudioAbrDecision
            if (r0 != 0) goto L_0x01d2
            boolean r0 = r6.A0L
            if (r0 == 0) goto L_0x01da
        L_0x01d2:
            boolean r0 = r7.isAudioAbrDecision
            if (r0 == 0) goto L_0x0028
            boolean r0 = r6.A0K
            if (r0 != 0) goto L_0x0028
        L_0x01da:
            com.facebook.video.heroplayer.ipc.ParcelableFormat[] r10 = r7.formats
            int r5 = r10.length
        L_0x01dd:
            if (r11 >= r5) goto L_0x02e0
            r12 = r10[r11]
            java.lang.String r1 = r7.videoId
            java.lang.String r0 = r12.id
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            int r1 = r0.hashCode()
            com.facebook.quicklog.QuickPerformanceLogger r3 = r6.A0E
            r0 = 1900582(0x1d0026, float:2.663283E-39)
            r3.markerStart(r0, r1)
            java.lang.String r0 = r12.id
            r27 = r0
            long r8 = r7.timeMs
            java.lang.String r0 = r7.videoId
            r26 = r0
            int r0 = r12.bitrate
            r25 = r0
            int r0 = r12.width
            r24 = r0
            int r0 = r12.height
            r23 = r0
            boolean r0 = r12.fbIsHvqLandscape
            r22 = r0
            boolean r0 = r12.fbIsHvqPortrait
            r21 = r0
            boolean r0 = r12.fbAvoidOnCellularForUnintentionalView
            r20 = r0
            boolean r0 = r12.fbAvoidOnCellularForIntentionalView
            r19 = r0
            java.lang.String r0 = r12.fbQualityLabel
            r17 = r0
            java.lang.String r0 = r12.fbPlaybackResolutionMos
            r14 = r0
            java.lang.String r13 = r12.fbPlaybackResolutionMosConfidence
            long r3 = r7.playerId
            boolean r15 = r7.isAudioAbrDecision
            boolean r0 = r12.fbIsDefaultFormat
            r16 = r0
            r18 = r15
            java.util.HashMap r12 = new java.util.HashMap
            r12.<init>()
            if (r15 != 0) goto L_0x023f
            java.lang.String r0 = "vd"
            r15 = r27
            r16 = r0
            boolean r16 = r15.endsWith(r16)
        L_0x023f:
            java.lang.String r15 = java.lang.Boolean.toString(r18)
            java.lang.String r0 = "is_audio"
            r12.put(r0, r15)
            java.lang.String r8 = java.lang.Long.toString(r8)
            java.lang.String r0 = "time_ms"
            r12.put(r0, r8)
            java.lang.String r8 = "video_id"
            r0 = r26
            r12.put(r8, r0)
            java.lang.String r8 = "format_id"
            r0 = r27
            r12.put(r8, r0)
            java.lang.String r8 = java.lang.Integer.toString(r25)
            java.lang.String r0 = "bitrate"
            r12.put(r0, r8)
            java.lang.String r8 = java.lang.Integer.toString(r24)
            java.lang.String r0 = "width"
            r12.put(r0, r8)
            java.lang.String r8 = java.lang.Integer.toString(r23)
            java.lang.String r0 = "height"
            r12.put(r0, r8)
            java.lang.String r8 = java.lang.Boolean.toString(r16)
            java.lang.String r0 = "default"
            r12.put(r0, r8)
            java.lang.String r8 = java.lang.Boolean.toString(r22)
            java.lang.String r0 = "hvq_landscape"
            r12.put(r0, r8)
            java.lang.String r8 = java.lang.Boolean.toString(r21)
            java.lang.String r0 = "hvq_portrait"
            r12.put(r0, r8)
            java.lang.String r8 = java.lang.Boolean.toString(r20)
            java.lang.String r0 = "avoid_on_cell"
            r12.put(r0, r8)
            java.lang.String r8 = java.lang.Boolean.toString(r19)
            java.lang.String r0 = "avoid_on_cell_intentional"
            r12.put(r0, r8)
            if (r17 == 0) goto L_0x02b1
            java.lang.String r0 = "quality_label"
            r15 = r12
            r16 = r0
            r15.put(r16, r17)
        L_0x02b1:
            if (r14 == 0) goto L_0x02c0
            java.lang.String r8 = ","
            java.lang.String r0 = ";"
            java.lang.String r8 = r14.replaceAll(r8, r0)
            java.lang.String r0 = "mos"
            r12.put(r0, r8)
        L_0x02c0:
            if (r13 == 0) goto L_0x02c7
            java.lang.String r8 = "mos_confidence"
            r12.put(r8, r13)
        L_0x02c7:
            java.lang.String r3 = java.lang.Long.toString(r3)
            java.lang.String r0 = "player_id"
            r12.put(r0, r3)
            r4 = 1900582(0x1d0026, float:2.663283E-39)
            X.C76053ks.A02(r6, r4, r1, r12)
            com.facebook.quicklog.QuickPerformanceLogger r3 = r6.A0E
            r0 = 2
            r3.markerEnd(r4, r1, r0)
            int r11 = r11 + 1
            goto L_0x01dd
        L_0x02e0:
            boolean r1 = r7.isAudioAbrDecision
            r0 = 1
            if (r1 == 0) goto L_0x02e9
            r6.A0K = r0
            goto L_0x0028
        L_0x02e9:
            r6.A0L = r0
            goto L_0x0028
        L_0x02ed:
            r0 = 30
            if (r1 != r0) goto L_0x0310
            X.AA8 r4 = (X.AA8) r4
            r3 = r2
            X.0jJ r2 = new X.0jJ     // Catch:{ IOException -> 0x0368 }
            r2.<init>()     // Catch:{ IOException -> 0x0368 }
            X.AAN r0 = r4.A00     // Catch:{ IOException -> 0x0368 }
            java.lang.String r1 = r0.extraData     // Catch:{ IOException -> 0x0368 }
            X.571 r0 = new X.571     // Catch:{ IOException -> 0x0368 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0368 }
            java.lang.Object r1 = r2.readValue(r1, r0)     // Catch:{ IOException -> 0x0368 }
            java.util.Map r1 = (java.util.Map) r1     // Catch:{ IOException -> 0x0368 }
            X.AAN r0 = r4.A00     // Catch:{ IOException -> 0x0368 }
            java.lang.String r0 = r0.eventName     // Catch:{ IOException -> 0x0368 }
            r3.A04(r0, r1)     // Catch:{ IOException -> 0x0368 }
            return
        L_0x0310:
            r0 = 36
            if (r1 != r0) goto L_0x0369
            X.AA7 r4 = (X.AA7) r4
            X.AAM r6 = r4.A00
            X.3ks r4 = r2.A0M
            java.lang.String r0 = r6.videoId
            int r3 = r0.hashCode()
            com.facebook.quicklog.QuickPerformanceLogger r0 = r4.A0E
            r2 = 1900578(0x1d0022, float:2.663277E-39)
            r0.markerStart(r2, r3)
            com.google.common.collect.ImmutableMap$Builder r5 = com.google.common.collect.ImmutableMap.builder()
            java.lang.String r1 = r6.videoId
            java.lang.String r0 = "video_id"
            r5.put(r0, r1)
            long r0 = r6.playerId
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "player_id"
            r5.put(r0, r1)
            java.lang.String r1 = r6.trackType
            java.lang.String r0 = "track"
            r5.put(r0, r1)
            long r0 = r6.renderTimeMs
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "time_ms"
            r5.put(r0, r1)
            long r0 = r6.presentationTimeMs
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "pts_ms"
            r5.put(r0, r1)
            com.google.common.collect.ImmutableMap r0 = r5.build()
            X.C76053ks.A02(r4, r2, r3, r0)
            com.facebook.quicklog.QuickPerformanceLogger r1 = r4.A0E
            r0 = 2
            r1.markerEnd(r2, r3, r0)
        L_0x0368:
            return
        L_0x0369:
            r0 = 37
            if (r1 != r0) goto L_0x0445
            X.3iY r4 = (X.C74823iY) r4
            com.facebook.video.heroplayer.ipc.HttpTransferEndEvent r5 = r4.A00
            java.util.Map r0 = r2.A09
            if (r0 == 0) goto L_0x03b5
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x03b5
            boolean r0 = r5.isPrefetch
            if (r0 != 0) goto L_0x03b5
            java.lang.String r1 = r5.videoId
            if (r1 == 0) goto L_0x03b5
            java.util.Map r0 = r2.A09
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x03b5
            java.util.Map r1 = r2.A09
            java.lang.String r0 = r5.videoId
            java.lang.Object r0 = r1.get(r0)
            java.util.List r0 = (java.util.List) r0
            java.util.Iterator r3 = r0.iterator()
        L_0x0399:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x03b5
            java.lang.Object r1 = r3.next()
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1
            java.lang.Object r0 = r1.get()
            if (r0 == 0) goto L_0x0399
            java.lang.Object r0 = r1.get()
            X.8Kk r0 = (X.C178228Kk) r0
            r0.ByL(r5)
            goto L_0x0399
        L_0x03b5:
            java.lang.String r0 = r5.url
            if (r0 == 0) goto L_0x0368
            int r0 = r5.readBytesLength
            if (r0 <= 0) goto L_0x0368
            X.3iW r1 = r5.cacheType
            X.3iW r0 = X.C74803iW.NOT_CACHED
            if (r1 != r0) goto L_0x0368
            long r0 = r5.transferEndDurationMs
            long r3 = r5.transferStartDurationMs
            long r0 = r0 - r3
            boolean r3 = r5.isChunkedTransfer
            r6 = 0
            if (r3 == 0) goto L_0x03d7
            X.1YI r4 = r2.A0F
            r3 = 392(0x188, float:5.5E-43)
            boolean r3 = r4.AbO(r3, r6)
            if (r3 != 0) goto L_0x040b
        L_0x03d7:
            r6 = 0
            int r3 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r3 <= 0) goto L_0x03f5
            int r6 = r5.readBytesLength
            long r7 = (long) r6
            r3 = 8000(0x1f40, double:3.9525E-320)
            long r7 = r7 * r3
            long r7 = r7 / r0
            double r3 = (double) r6
            double r3 = java.lang.Math.sqrt(r3)
            int r11 = (int) r3
            X.3kw r6 = r2.A0E
            X.069 r3 = r2.A0D
            long r9 = r3.now()
            r6.A02(r7, r9, r11)
        L_0x03f5:
            java.lang.String r4 = r5.dataSourceFactory
            r3 = 294(0x126, float:4.12E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r3)
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x040b
            X.2YD r6 = r2.A0G
            int r3 = r5.readBytesLength
            long r3 = (long) r3
            r6.A02(r3, r0)
        L_0x040b:
            X.0h3 r0 = r2.A0C
            android.net.NetworkInfo r3 = r0.A0E()
            java.lang.String r0 = r5.exceptionMessage
            if (r0 == 0) goto L_0x041b
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0368
        L_0x041b:
            int r0 = r5.readBytesLength
            long r5 = (long) r0
            r1 = 1
            if (r3 == 0) goto L_0x0428
            int r0 = r3.getType()
            r7 = 1
            if (r0 == r1) goto L_0x0429
        L_0x0428:
            r7 = 0
        L_0x0429:
            X.0h3 r0 = r2.A0C
            boolean r8 = r0.A0P()
            X.3kq r4 = r2.A0L
            java.lang.String r3 = "downloaded"
            r4.A04(r3, r5)
            X.3kq r4 = r2.A0L
            java.lang.String r3 = "served"
            r4.A04(r3, r5)
            X.BQw r3 = new X.BQw
            r4 = r2
            r3.<init>(r4, r5, r7, r8)
            monitor-enter(r2)
            goto L_0x0457
        L_0x0445:
            r0 = 38
            if (r1 != r0) goto L_0x0368
            r5 = 0
            X.3ks r4 = r2.A0M
            java.lang.String r3 = r5.videoId
            java.lang.String r1 = r5.errorDomain
            java.lang.String r0 = r5.errorDetails
            r4.A0B(r3, r1, r0)
            r4 = 0
            goto L_0x046b
        L_0x0457:
            java.util.List r1 = r2.A08     // Catch:{ all -> 0x0468 }
            r0 = 0
            if (r1 == 0) goto L_0x045d
            r0 = 1
        L_0x045d:
            if (r0 != 0) goto L_0x0463
            r3.run()     // Catch:{ all -> 0x0468 }
            goto L_0x0466
        L_0x0463:
            r1.add(r3)     // Catch:{ all -> 0x0468 }
        L_0x0466:
            monitor-exit(r2)
            return
        L_0x0468:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x046b:
            java.lang.String r0 = r5.errorCode     // Catch:{ IllegalArgumentException | NullPointerException -> 0x0479 }
            X.8Om r0 = X.C179128Om.valueOf(r0)     // Catch:{ IllegalArgumentException | NullPointerException -> 0x0479 }
            if (r0 == 0) goto L_0x0479
            X.8Kq r0 = r0.reliabilityLabel     // Catch:{ IllegalArgumentException | NullPointerException -> 0x0479 }
            java.lang.String r4 = r0.A00()     // Catch:{ IllegalArgumentException | NullPointerException -> 0x0479 }
        L_0x0479:
            if (r4 != 0) goto L_0x047d
            java.lang.String r4 = r5.errorCode
        L_0x047d:
            X.0nb r3 = new X.0nb
            java.lang.Integer r0 = X.AnonymousClass07B.A0U
            java.lang.String r0 = X.C178548Ls.A00(r0)
            r3.<init>(r0)
            java.lang.String r1 = r5.videoId
            java.lang.String r0 = "video_id"
            r3.A0D(r0, r1)
            java.lang.String r1 = r5.errorDomain
            java.lang.String r0 = "error_domain"
            r3.A0D(r0, r1)
            java.lang.String r0 = "reliability_label"
            r3.A0D(r0, r4)
            java.lang.String r1 = r5.errorDetails
            java.lang.String r0 = "debug_reason"
            r3.A0D(r0, r1)
            java.lang.String r1 = "pigeon_reserved_keyword_module"
            java.lang.String r0 = "video"
            r3.A0D(r1, r0)
            com.google.common.base.Preconditions.checkNotNull(r3)
            com.facebook.analytics.DeprecatedAnalyticsLogger r0 = r2.A0A
            r0.A05(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass222.Aaz(X.2W7):void");
    }

    public AnonymousClass222(C76033kq r3, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, C76043kr r5, C09340h3 r6, AnonymousClass1W1 r7, TimedMicroStorage timedMicroStorage, C194518f r9, AnonymousClass13b r10, AnonymousClass069 r11, C76053ks r12, ExecutorService executorService, AnonymousClass1YI r14) {
        this.A0F = r14;
        this.A0L = r3;
        this.A0A = deprecatedAnalyticsLogger;
        this.A0J = r5;
        this.A0C = r6;
        this.A0G = new AnonymousClass2YD(r7, r11);
        this.A0K = timedMicroStorage;
        this.A0D = r11;
        this.A0M = r12;
        this.A0H = new C76073kv();
        this.A0I = new C76073kv();
        this.A08 = new ArrayList();
        this.A0B = r10;
        if (r9 != null) {
            r9.A03(this);
            C010708t.A06(A0O, "creating VideoPerformanceTracking with event bus");
        } else {
            C010708t.A06(A0O, "creating VideoPerformanceTracking without event bus");
        }
        this.A0E = new C76083kw();
        this.A0N = executorService;
    }
}
