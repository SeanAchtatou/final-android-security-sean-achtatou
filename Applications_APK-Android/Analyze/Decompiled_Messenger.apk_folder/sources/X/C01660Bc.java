package X;

/* renamed from: X.0Bc  reason: invalid class name and case insensitive filesystem */
public final class C01660Bc extends C01540Aq {
    public static final C01660Bc A00 = new C01660Bc();
    private static final long serialVersionUID = 0;

    public boolean A02() {
        return false;
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public int hashCode() {
        return 1502476572;
    }

    public String toString() {
        return "Optional.absent()";
    }

    public Object A01() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }

    private C01660Bc() {
    }

    private Object readResolve() {
        return A00;
    }
}
