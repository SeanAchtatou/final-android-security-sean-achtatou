package X;

/* renamed from: X.1NW  reason: invalid class name */
public final class AnonymousClass1NW {
    public int A00;
    public long A01;
    public final int A02;
    public final int A03;
    public final C22891Nf A04;

    public AnonymousClass1NW(int i, int i2) {
        boolean z = true;
        C05520Zg.A04(i > 0);
        C05520Zg.A04(i2 <= 0 ? false : z);
        this.A02 = i;
        this.A03 = i2;
        this.A04 = new C22951Nl(this);
    }
}
