package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;

@UserScoped
/* renamed from: X.0t0  reason: invalid class name and case insensitive filesystem */
public final class C14300t0 implements C05460Za {
    private static C05540Zi A03;
    public final ConcurrentMap A00 = AnonymousClass0TG.A07();
    public final ConcurrentMap A01 = AnonymousClass0TG.A07();
    private final AnonymousClass0XN A02;

    public synchronized void A06(Collection collection) {
        A07(collection, false);
    }

    public synchronized void A07(Collection collection, boolean z) {
        User user;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            User user2 = (User) it.next();
            if (z || (user = (User) this.A01.get(user2.A0Q)) == null || user.A00 < user2.A00) {
                AnonymousClass0XN r2 = this.A02;
                User A09 = r2.A09();
                if (A09 != null && Objects.equal(A09.A0Q, user2.A0Q)) {
                    r2.A0E(user2);
                }
                this.A01.put(user2.A0Q, user2);
            }
        }
    }

    public static final C14300t0 A01(AnonymousClass1XY r4) {
        C14300t0 r0;
        synchronized (C14300t0.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C14300t0(AnonymousClass0XN.A01((AnonymousClass1XY) A03.A01()));
                }
                C05540Zi r1 = A03;
                r0 = (C14300t0) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C04310Tq A02(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.AxE, r1);
    }

    public User A03(UserKey userKey) {
        if (userKey == null) {
            return null;
        }
        return (User) this.A01.get(userKey);
    }

    public ImmutableList A04(ImmutableList immutableList) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            User A032 = A03((UserKey) it.next());
            if (A032 != null) {
                builder.add((Object) A032);
            }
        }
        return builder.build();
    }

    public void A05(UserKey userKey, long j) {
        if (userKey != null) {
            Long l = (Long) this.A00.get(userKey);
            if (l == null || l.longValue() < j) {
                this.A00.put(userKey, Long.valueOf(j));
            }
        }
    }

    public void clearUserData() {
        this.A01.clear();
        this.A00.clear();
    }

    private C14300t0(AnonymousClass0XN r2) {
        this.A02 = r2;
    }

    public static final C14300t0 A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
