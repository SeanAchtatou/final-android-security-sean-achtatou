package X;

import android.content.Context;
import com.facebook.common.util.StringLocaleUtil;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.sharedimage.SharedMedia;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;
import java.util.BitSet;

/* renamed from: X.21C  reason: invalid class name */
public final class AnonymousClass21C extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C135686Wa A01;
    @Comparable(type = 13)
    public SharedMedia A02;
    @Comparable(type = 5)
    public ImmutableList A03;

    public AnonymousClass21C(Context context) {
        super("WorkSharedContentImageRowComponent");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }

    public C17770zR A0O(AnonymousClass0p4 r16) {
        ImmutableList immutableList = this.A03;
        SharedMedia sharedMedia = this.A02;
        C135686Wa r12 = this.A01;
        int i = AnonymousClass1Y3.Anh;
        AnonymousClass0UN r2 = this.A00;
        MigColorScheme migColorScheme = (MigColorScheme) AnonymousClass1XX.A02(0, i, r2);
        AnonymousClass1S0 r8 = (AnonymousClass1S0) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aaw, r2);
        AnonymousClass0p4 r7 = r16;
        AnonymousClass5Q8 r3 = new AnonymousClass5Q8(r7.A09);
        C17540z4 r4 = r7.A0B;
        C17770zR r0 = r7.A04;
        if (r0 != null) {
            r3.A07 = r0.A06;
        }
        String[] strArr = {"listener", "sharedImage", "sharedImages"};
        BitSet bitSet = new BitSet(3);
        AnonymousClass5IR r22 = new AnonymousClass5IR(r7.A09);
        C17540z4 r11 = r7.A0B;
        C17770zR r02 = r7.A04;
        if (r02 != null) {
            r22.A07 = r02.A06;
        }
        bitSet.clear();
        r22.A02 = sharedMedia;
        bitSet.set(1);
        r22.A04 = immutableList;
        bitSet.set(2);
        r22.A00 = AnonymousClass36c.A01((float) C007106r.A03(r7.A03(), (float) AnonymousClass1JQ.XSMALL.B3A()));
        C80903tM A022 = AnonymousClass38W.A02(r7);
        AnonymousClass10G r1 = AnonymousClass10G.ALL;
        A022.A07(r1, 1.0f);
        A022.A08(r1, 167772160);
        r22.A14().A0F(A022.A01());
        r22.A03 = new C108955If(r12);
        bitSet.set(0);
        r22.A14().A0A(2);
        r22.A14().CNK(r11.A00(40.0f));
        r22.A14().BC2(r11.A00(40.0f));
        AnonymousClass11F.A0C(3, bitSet, strArr);
        r3.A0B = r22.A16();
        int i2 = 2131835126;
        if (sharedMedia.Atk().A0L == C421828p.PHOTO) {
            i2 = 2131835120;
        }
        r3.A0E = r7.A0C(i2);
        r3.A06 = 8;
        r3.A04 = 2;
        r3.A0C = StringLocaleUtil.A00(r7.A0C(2131835121), sharedMedia.B2t(), r8.A04(sharedMedia.Atk().A05));
        r3.A02 = 2;
        r3.A14().A0G(C17780zS.A0E(AnonymousClass21C.class, r7, -1351902487, new Object[]{r7}));
        r3.A14().A0C(C16990y9.A00(migColorScheme.B9m()));
        r3.A14().BwM(AnonymousClass10G.A04, r4.A00((float) AnonymousClass1JQ.SMALL.B3A()));
        return r3;
    }

    public Object AXa(AnonymousClass10N r8, Object obj) {
        int i = r8.A01;
        if (i == -1351902487) {
            AnonymousClass21C r0 = (AnonymousClass21C) r8.A00;
            ImmutableList immutableList = r0.A03;
            SharedMedia sharedMedia = r0.A02;
            C135686Wa r3 = r0.A01;
            ((C107725Di) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AS2, this.A00)).A01(sharedMedia.Atk().A0L);
            r3.A0L(immutableList, sharedMedia);
            return null;
        } else if (i != -1048037474) {
            return null;
        } else {
            C17780zS.A0G((AnonymousClass0p4) r8.A02[0], (AnonymousClass5QC) obj);
            return null;
        }
    }
}
