package X;

import android.content.Context;

/* renamed from: X.0A0  reason: invalid class name */
public interface AnonymousClass0A0 {
    void badTimeToDoGc(AnonymousClass084 r1);

    String getErrorMessage();

    boolean isPlatformSupported();

    void manualGcComplete();

    void manualGcConcurrent();

    void manualGcForAlloc();

    void notAsBadTimeToDoGc();

    void setUpHook(Context context, AnonymousClass0N9 r2);
}
