package X;

import java.security.SecureRandom;

/* renamed from: X.18B  reason: invalid class name */
public abstract class AnonymousClass18B {
    public final AnonymousClass18C A00;
    public final SecureRandom A01;

    public AnonymousClass18B(AnonymousClass18C r1, SecureRandom secureRandom) {
        this.A00 = r1;
        this.A01 = secureRandom;
    }
}
