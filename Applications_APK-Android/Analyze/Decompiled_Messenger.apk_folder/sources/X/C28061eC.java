package X;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.1eC  reason: invalid class name and case insensitive filesystem */
public final class C28061eC {
    static {
        new C11290mf();
        new C04200Sw();
    }

    public static Signature A01(PackageInfo packageInfo) {
        int length;
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null || (length = signatureArr.length) == 0) {
            throw new EED(packageInfo.packageName);
        } else if (length <= 1) {
            Signature signature = signatureArr[0];
            if (signature != null) {
                return signature;
            }
            throw new EEE(packageInfo.packageName);
        } else {
            throw new C53322kZ(packageInfo.packageName);
        }
    }

    public static PackageInfo A00(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 64);
            if (packageInfo != null) {
                String str2 = packageInfo.packageName;
                if (str.equals(str2)) {
                    return packageInfo;
                }
                throw new EEB(AnonymousClass08S.A0S("Package name mismatch: expected=", str, ", was=", str2));
            }
            throw new EEC(str);
        } catch (PackageManager.NameNotFoundException unused) {
            throw new C53332ka(AnonymousClass08S.A0J(str, " not found by PackageManager."));
        } catch (RuntimeException e) {
            throw new SecurityException(e);
        }
    }

    public static AnonymousClass0TO A02(Context context, String str) {
        return A03(A01(A00(context, str)));
    }

    public static AnonymousClass0TO A03(Signature signature) {
        try {
            byte[] byteArray = signature.toByteArray();
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(byteArray);
            String encodeToString = Base64.encodeToString(instance.digest(), 11);
            byte[] byteArray2 = signature.toByteArray();
            MessageDigest instance2 = MessageDigest.getInstance("SHA-256");
            instance2.update(byteArray2);
            return new AnonymousClass0TO("test", encodeToString, Base64.encodeToString(instance2.digest(), 11));
        } catch (NoSuchAlgorithmException unused) {
            throw new SecurityException("Error obtaining SHA1/SHA256");
        }
    }

    public static boolean A04(Context context, int i, int i2) {
        try {
            int checkSignatures = context.getPackageManager().checkSignatures(i, i2);
            boolean z = false;
            if (i == i2) {
                z = true;
            }
            if (z || checkSignatures == 0) {
                return true;
            }
            return false;
        } catch (RuntimeException e) {
            throw new SecurityException(e);
        }
    }
}
