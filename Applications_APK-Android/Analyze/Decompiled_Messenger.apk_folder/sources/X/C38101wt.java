package X;

/* renamed from: X.1wt  reason: invalid class name and case insensitive filesystem */
public final class C38101wt implements AnonymousClass9QQ {
    private int A00;
    private A9c A01;
    public final int A02;
    public final boolean A03;
    public final byte[] A04;

    public void cancel() {
    }

    public long BvJ(C21297A8a a8a) {
        A9c a9c = this.A01;
        if (a9c != null) {
            a9c.Bsn(a8a, C74803iW.CACHED);
        }
        this.A00 = 0;
        A9c a9c2 = this.A01;
        if (a9c2 != null) {
            a9c2.Bso(false);
        }
        return (long) this.A02;
    }

    public void close() {
        A9c a9c = this.A01;
        if (a9c != null) {
            a9c.Bsk();
            this.A01 = null;
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3 = this.A02;
        int i4 = this.A00;
        int i5 = i3 - i4;
        if (i5 == 0) {
            return -1;
        }
        if (i2 > i5) {
            i2 = i5;
        }
        if (i2 > 0) {
            System.arraycopy(this.A04, i4, bArr, i, i2);
            this.A00 += i2;
            A9c a9c = this.A01;
            if (a9c != null) {
                a9c.BQI(i2);
            }
        }
        return i2;
    }

    public C38101wt(byte[] bArr, int i, A9c a9c, boolean z) {
        this.A04 = bArr;
        this.A02 = i;
        this.A01 = a9c;
        this.A03 = z;
    }
}
