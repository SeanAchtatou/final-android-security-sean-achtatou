package X;

import io.card.payment.BuildConfig;

/* renamed from: X.217  reason: invalid class name */
public final class AnonymousClass217 {
    public AnonymousClass218 A00;
    private AnonymousClass218 A01;
    private final String A02;

    public static void A00(AnonymousClass217 r2, String str, Object obj) {
        AnonymousClass218 r1 = new AnonymousClass218();
        r2.A00.A02 = r1;
        r2.A00 = r1;
        r1.A00 = obj;
        C05520Zg.A02(str);
        r1.A01 = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.A02);
        sb.append('{');
        String str = BuildConfig.FLAVOR;
        for (AnonymousClass218 r1 = this.A01.A02; r1 != null; r1 = r1.A02) {
            sb.append(str);
            String str2 = r1.A01;
            if (str2 != null) {
                sb.append(str2);
                sb.append('=');
            }
            sb.append(r1.A00);
            str = ", ";
        }
        sb.append('}');
        return sb.toString();
    }

    public AnonymousClass217(String str) {
        AnonymousClass218 r0 = new AnonymousClass218();
        this.A01 = r0;
        this.A00 = r0;
        C05520Zg.A02(str);
        this.A02 = str;
    }

    public void A01(String str, boolean z) {
        A00(this, str, String.valueOf(z));
    }
}
