package X;

import android.os.Build;
import android.os.PowerManager;
import android.os.Process;
import android.os.SystemClock;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fB  reason: invalid class name and case insensitive filesystem */
public final class C08330fB extends AnonymousClass0f8 {
    private static volatile C08330fB A01;
    private AnonymousClass0UN A00;

    public String Azg() {
        return "cpu_stats";
    }

    public static final C08330fB A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C08330fB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C08330fB(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        C08340fC r6 = (C08340fC) obj;
        C08340fC r7 = (C08340fC) obj2;
        if (r6 != null && r7 != null && performanceLoggingEvent.A0F == null) {
            performanceLoggingEvent.A04("start_pri", r6.A00);
            performanceLoggingEvent.A04("stop_pri", r7.A00);
            performanceLoggingEvent.A0B("ps_cpu_ms", String.valueOf(r7.A02 - r6.A02));
            if (r6.A01 == r7.A01) {
                performanceLoggingEvent.A06("th_cpu_ms", r7.A03 - r6.A03);
            }
            performanceLoggingEvent.A0B(TurboLoader.Locator.$const$string(38), r6.A04);
        }
    }

    public long Azh() {
        return C08350fD.A01;
    }

    public Class B3O() {
        return C08340fC.class;
    }

    public Object CGO() {
        String str;
        C08340fC r3 = new C08340fC();
        r3.A01 = Process.myTid();
        r3.A02 = Process.getElapsedCpuTime();
        r3.A03 = SystemClock.currentThreadTimeMillis();
        r3.A00 = Process.getThreadPriority(r3.A01);
        r3.A04 = "unknown";
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                if (((PowerManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8E, this.A00)).isPowerSaveMode()) {
                    str = "true";
                } else {
                    str = "false";
                }
                r3.A04 = str;
                return r3;
            } catch (SecurityException unused) {
            }
        }
        return r3;
    }

    private C08330fB(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public boolean BEZ(C08360fE r2) {
        return true;
    }
}
