package X;

/* renamed from: X.0Co  reason: invalid class name and case insensitive filesystem */
public final class C02030Co implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$CallbackHandler$6";
    public final /* synthetic */ AnonymousClass0CC A00;
    public final /* synthetic */ C02020Cn A01;

    public C02030Co(AnonymousClass0CC r1, C02020Cn r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0064, code lost:
        if (r4.A09() == false) goto L_0x0066;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r18 = this;
            r3 = r18
            X.0CC r0 = r3.A00
            X.0AH r0 = r0.A02
            X.0C8 r1 = r0.A0n
            X.0CC r4 = r3.A00
            X.0C8 r0 = r4.A00
            if (r1 != r0) goto L_0x012a
            X.0Bc r7 = X.C01660Bc.A00
            X.0Cn r1 = r3.A01
            X.0Ck r0 = r1.A00
            X.0CL r2 = r0.A03
            int r0 = r2.ordinal()
            switch(r0) {
                case 2: goto L_0x003a;
                case 3: goto L_0x007d;
                case 4: goto L_0x001d;
                case 5: goto L_0x001d;
                case 6: goto L_0x001d;
                case 7: goto L_0x001d;
                case 8: goto L_0x007d;
                case 9: goto L_0x001d;
                case 10: goto L_0x007d;
                case 11: goto L_0x004e;
                case 12: goto L_0x007b;
                default: goto L_0x001d;
            }
        L_0x001d:
            boolean r0 = r7.A02()
            if (r0 == 0) goto L_0x0119
            r7.A01()
            X.0CC r0 = r3.A00
            X.0AH r0 = r0.A02
            X.0BN r5 = r0.A0L
            java.lang.Object r0 = r7.A01()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r11 = r0.intValue()
            java.util.Map r6 = r5.A04
            monitor-enter(r6)
            goto L_0x008e
        L_0x003a:
            X.0Cp r1 = (X.C02040Cp) r1
            X.0Ck r0 = r1.A00
            int r1 = r0.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            int r0 = X.AnonymousClass08G.A00(r0)
            if (r1 != r0) goto L_0x001d
            X.0AH r0 = r4.A02
            r0.A0E()
            goto L_0x001d
        L_0x004e:
            X.0AH r0 = r4.A02
            r0.A0E()
            X.0CC r0 = r3.A00
            X.0AH r6 = r0.A02
            boolean r0 = r6.A0Y
            if (r0 == 0) goto L_0x001d
            X.0C8 r4 = r6.A0n
            if (r4 == 0) goto L_0x0066
            boolean r1 = r4.A09()
            r0 = 1
            if (r1 != 0) goto L_0x0067
        L_0x0066:
            r0 = 0
        L_0x0067:
            if (r0 == 0) goto L_0x001d
            long r0 = r4.A0W
            long r4 = android.os.SystemClock.elapsedRealtime()
            long r4 = r4 - r0
            X.0B7 r0 = r6.A0A
            X.0Sd r1 = r0.A05(r4)
            r0 = 1
            X.AnonymousClass0AH.A04(r6, r1, r0)
            goto L_0x001d
        L_0x007b:
            r0 = -1
            goto L_0x0085
        L_0x007d:
            java.lang.Object r0 = r1.A01()
            X.0Cl r0 = (X.C02000Cl) r0
            int r0 = r0.A00
        L_0x0085:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            X.0Aq r7 = X.C01540Aq.A00(r0)
            goto L_0x001d
        L_0x008e:
            java.util.Map r1 = r5.A04     // Catch:{ all -> 0x009c }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x009c }
            java.lang.Object r4 = r1.remove(r0)     // Catch:{ all -> 0x009c }
            X.0CV r4 = (X.AnonymousClass0CV) r4     // Catch:{ all -> 0x009c }
            monitor-exit(r6)     // Catch:{ all -> 0x009c }
            goto L_0x009f
        L_0x009c:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x009c }
            throw r0
        L_0x009f:
            if (r4 == 0) goto L_0x00f0
            r4.A00()
            android.os.SystemClock.elapsedRealtime()
            long r12 = android.os.SystemClock.elapsedRealtime()
            long r0 = r4.A02
            long r12 = r12 - r0
            X.0CL r1 = r4.A04
            X.0CL r0 = X.AnonymousClass0CL.A07
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00c7
            X.0B7 r1 = r5.A01
            java.lang.Class<X.0Cq> r0 = X.C02050Cq.class
            X.0By r1 = r1.A07(r0)
            X.0Cq r1 = (X.C02050Cq) r1
            X.0Cr r0 = X.C02060Cr.PublishAcknowledgementMs
            r1.A03(r0, r12)
        L_0x00c7:
            int r14 = r4.AwM()
            boolean r1 = r4 instanceof X.C02070Cs
            if (r1 == 0) goto L_0x00d4
            r0 = r4
            X.0Cs r0 = (X.C02070Cs) r0
            int r14 = r0.A00
        L_0x00d4:
            if (r1 == 0) goto L_0x012e
            r0 = r4
            X.0Cs r0 = (X.C02070Cs) r0
            int r15 = r0.A01
        L_0x00db:
            X.0C8 r0 = r4.A03
            if (r0 != 0) goto L_0x012b
            r0 = 0
        L_0x00e1:
            X.0B6 r8 = r5.A00
            java.lang.String r9 = r4.A05
            java.lang.Integer r4 = X.AnonymousClass07B.A01
            int r10 = X.AnonymousClass08G.A00(r4)
            r16 = r0
            r8.A03(r9, r10, r11, r12, r14, r15, r16)
        L_0x00f0:
            java.lang.Object r0 = r7.A01()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r4 = r0.intValue()
            X.0CC r0 = r3.A00
            X.0AH r1 = r0.A02
            int r0 = r1.A00
            if (r4 != r0) goto L_0x0119
            X.0C8 r0 = r1.A0o
            if (r0 == 0) goto L_0x0119
            X.0CC r0 = r3.A00
            X.0AH r0 = r0.A02
            X.0C8 r0 = r0.A0o
            java.util.concurrent.ExecutorService r4 = r0.A0G
            X.0S3 r1 = new X.0S3
            r1.<init>(r0)
            r0 = 1939066538(0x7393ceaa, float:2.3420999E31)
            X.AnonymousClass07A.A04(r4, r1, r0)
        L_0x0119:
            X.0CC r0 = r3.A00
            X.0AH r1 = r0.A02
            X.07g r0 = r1.A06
            if (r0 == 0) goto L_0x0123
            X.0CL r0 = X.AnonymousClass0CL.A08
        L_0x0123:
            X.0AG r1 = r1.A0I
            X.0Cn r0 = r3.A01
            r1.Bf4(r0)
        L_0x012a:
            return
        L_0x012b:
            long r0 = r0.A0W
            goto L_0x00e1
        L_0x012e:
            r15 = 0
            goto L_0x00db
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02030Co.run():void");
    }
}
