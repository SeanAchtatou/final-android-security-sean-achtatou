package X;

import android.os.RemoteException;
import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.facebook.fbservice.service.ICompletionHandler;
import com.facebook.fbservice.service.OperationResult;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0la  reason: invalid class name and case insensitive filesystem */
public final class C11000la {
    public C11020lc A00;
    public ScheduledExecutorService A01;
    public boolean A02 = false;
    public final C06230bA A03;
    public final C05920aY A04;
    public final AnonymousClass0Ud A05;
    public final AnonymousClass09P A06;
    public final AnonymousClass0lU A07;
    public final AnonymousClass0V7 A08;
    public final AnonymousClass0UW A09;
    public final AnonymousClass069 A0A;
    public final AnonymousClass0lT A0B;
    public final AnonymousClass1YI A0C;
    public final Class A0D;
    public final LinkedList A0E = new LinkedList();
    public final Map A0F = AnonymousClass0TG.A04();
    public final Set A0G;
    public final AtomicBoolean A0H = new AtomicBoolean(false);
    public final C04310Tq A0I;
    public volatile C11020lc A0J;

    /* JADX INFO: finally extract failed */
    public static synchronized void A01(C11000la r13, C11020lc r14, OperationResult operationResult) {
        synchronized (r13) {
            C005505z.A03("BlueServiceQueue.operationDone", 491031813);
            try {
                r14.A03 = operationResult;
                r14.A00 = r13.A0A.now();
                AnonymousClass0ZF A052 = r13.A03.A05("blue_service_execution", false, AnonymousClass07B.A00, false);
                if (A052.A0G()) {
                    A052.A0A("op_type", r14.A09.A04);
                    A052.A08(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, Boolean.valueOf(r14.A03.success));
                    A052.A0A("error_desc", r14.A03.errorDescription);
                    A052.A09("time_ms", Long.valueOf(r14.A00 - r14.A01));
                    A052.A08("app_backgrounded", Boolean.valueOf(r13.A05.A0G()));
                    A052.A0A("queue_name", r13.A0D.getSimpleName());
                    A052.A0A("thread_pri", r13.A08.toString());
                    A052.A09("android_thread_pri", Integer.valueOf(r13.A08.mAndroidThreadPriority));
                    A052.A0E();
                }
                r13.A0F.remove(r14.A09.A03);
                if (r13.A00 == r14) {
                    r13.A00 = null;
                }
                for (ICompletionHandler BhE : r14.A06) {
                    try {
                        BhE.BhE(r14.A03);
                    } catch (RemoteException unused) {
                    }
                }
                for (AnonymousClass0lf BhF : r13.A0G) {
                    BhF.BhF(r13.A0D, r14.A09, r14.A04, r14.A08, r14.A01, r14.A00);
                }
                r14.A06 = null;
                C005505z.A00(1703615009);
            } catch (Throwable th) {
                C005505z.A00(-1506870666);
                throw th;
            }
        }
    }

    public static synchronized void A02(C11000la r3, C11020lc r4, ListenableFuture listenableFuture) {
        synchronized (r3) {
            C005505z.A03("BlueServiceQueue.operationDeferred", -1844405228);
            try {
                r4.A05 = listenableFuture;
                r3.A0J = r4;
                C05350Yp.A08(r4.A05, new AnonymousClass9HW(r3, r4), r3.A01);
                C005505z.A00(808651295);
            } finally {
                C005505z.A00(593466214);
            }
        }
    }

    public static void A00(C11000la r3, long j) {
        r3.A01.schedule(new AnonymousClass0lh(r3), j, TimeUnit.MILLISECONDS);
    }

    public C11000la(Class cls, C04310Tq r4, Set set, AnonymousClass0lU r6, AnonymousClass0lT r7, C05920aY r8, AnonymousClass09P r9, C06230bA r10, AnonymousClass069 r11, AnonymousClass0V7 r12, AnonymousClass0UW r13, AnonymousClass0Ud r14, AnonymousClass1YI r15) {
        this.A0D = cls;
        this.A0I = r4;
        this.A0G = set;
        this.A07 = r6;
        this.A0B = r7;
        this.A04 = r8;
        this.A06 = r9;
        this.A03 = r10;
        this.A0A = r11;
        this.A08 = r12;
        this.A09 = r13;
        this.A05 = r14;
        this.A0C = r15;
    }
}
