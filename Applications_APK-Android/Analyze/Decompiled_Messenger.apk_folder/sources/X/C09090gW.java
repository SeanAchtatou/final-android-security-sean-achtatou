package X;

import android.content.Context;
import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.google.common.base.Preconditions;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0gW  reason: invalid class name and case insensitive filesystem */
public final class C09090gW implements BlueServiceOperationFactory {
    private final C05920aY A00;
    private final C09100gX A01;
    private final boolean A02 = AnonymousClass0VW.A00();

    /* access modifiers changed from: private */
    /* renamed from: A00 */
    public C27171cl newInstance(String str, Bundle bundle, int i, CallerContext callerContext) {
        String str2 = str;
        Preconditions.checkNotNull(str2);
        Bundle bundle2 = bundle;
        Preconditions.checkNotNull(bundle2);
        int i2 = i;
        Preconditions.checkNotNull(Integer.valueOf(i2));
        if (!this.A02) {
            C09100gX r3 = this.A01;
            C05920aY r13 = this.A00;
            Context A002 = AnonymousClass1YA.A00(r3);
            AnonymousClass0VB A003 = AnonymousClass0VB.A00(AnonymousClass1Y3.AVT, r3);
            ExecutorService A0a = AnonymousClass0UX.A0a(r3);
            C04910Wu.A00(r3);
            return new C27171cl(r3, A002, A003, A0a, new AnonymousClass0lQ(AnonymousClass1YA.A00(r3)), C04750Wa.A01(r3), str2, bundle2, i2, callerContext, r13, AnonymousClass0UQ.A00(AnonymousClass1Y3.AiM, r3), C07390dL.A00(r3));
        }
        throw new IllegalStateException("Can't create BlueServiceOperations in tests. You could bind\nBlueServiceOperationFactory to something likeFakeBlueServiceOperationFactory if you just need some behavior\n");
    }

    public static final C09090gW A01(AnonymousClass1XY r3) {
        return new C09090gW(new C09100gX(r3), C10580kT.A01(r3));
    }

    public C09090gW(C09100gX r2, C05920aY r3) {
        this.A01 = r2;
        this.A00 = r3;
    }

    public AnonymousClass0lL newInstance(String str, Bundle bundle) {
        return newInstance(str, bundle, 0, null);
    }

    public AnonymousClass0lL newInstance(String str, Bundle bundle, int i) {
        return newInstance(str, bundle, i, null);
    }

    public AnonymousClass0lL newInstance(String str, Bundle bundle, CallerContext callerContext) {
        return newInstance(str, bundle, 0, callerContext);
    }
}
