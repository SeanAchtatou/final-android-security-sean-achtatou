package X;

import android.content.ContentResolver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.google.common.base.Optional;
import io.card.payment.BuildConfig;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0h3  reason: invalid class name and case insensitive filesystem */
public final class C09340h3 implements AnonymousClass1YQ, C09320h0 {
    private static volatile C09340h3 A0N;
    public long A00 = Long.MIN_VALUE;
    public NetworkInfo A01;
    public C07730e4 A02;
    public AnonymousClass0UN A03;
    public Boolean A04;
    private long A05 = Long.MIN_VALUE;
    private long A06 = 0;
    private String A07;
    private String A08;
    private String A09;
    public final C09330h1 A0A;
    public final Object A0B = new Object();
    public final Object A0C = new Object();
    public final Object A0D = new Object();
    public final Object A0E = new Object();
    public final AtomicInteger A0F = new AtomicInteger(0);
    private final AtomicInteger A0G = new AtomicInteger(0);
    public volatile long A0H;
    public volatile long A0I;
    public volatile AnonymousClass8U2 A0J;
    public volatile String A0K;
    private volatile Boolean A0L;
    private volatile boolean A0M;

    private boolean A08() {
        try {
            return C12880q9.A00((ConnectivityManager) AnonymousClass1XX.A03(AnonymousClass1Y3.BLN, this.A03));
        } catch (SecurityException e) {
            A07(e);
            return true;
        } catch (RuntimeException e2) {
            C010708t.A0M("FbNetworkManager", "isActiveNetworkMeteredSync caught Exception", e2);
            return true;
        }
    }

    public void enterLameDuckMode() {
        this.A0M = true;
        A04(this);
    }

    public void exitLameDuckMode() {
        this.A0M = false;
        A04(this);
    }

    public String getSimpleName() {
        return "FbNetworkManager";
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0032 A[Catch:{ SecurityException -> 0x0025, RuntimeException -> 0x001c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.net.NetworkInfo A00() {
        /*
            r7 = this;
            java.lang.Object r4 = r7.A0C
            monitor-enter(r4)
            r6 = 0
            int r1 = X.AnonymousClass1Y3.BLN     // Catch:{ SecurityException -> 0x0025, RuntimeException -> 0x001c }
            X.0UN r0 = r7.A03     // Catch:{ SecurityException -> 0x0025, RuntimeException -> 0x001c }
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ SecurityException -> 0x0025, RuntimeException -> 0x001c }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ SecurityException -> 0x0025, RuntimeException -> 0x001c }
            if (r0 == 0) goto L_0x001a
            android.net.NetworkInfo r1 = r0.getActiveNetworkInfo()     // Catch:{ SecurityException -> 0x0025, RuntimeException -> 0x001c }
        L_0x0014:
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A0F     // Catch:{ SecurityException -> 0x0025, RuntimeException -> 0x001c }
            r0.incrementAndGet()     // Catch:{ SecurityException -> 0x0025, RuntimeException -> 0x001c }
            goto L_0x002a
        L_0x001a:
            r1 = r6
            goto L_0x0014
        L_0x001c:
            r2 = move-exception
            java.lang.String r1 = "FbNetworkManager"
            java.lang.String r0 = "getActiveNetworkInfoSync caught Exception"
            X.C010708t.A0M(r1, r0, r2)     // Catch:{ all -> 0x0095 }
            goto L_0x0029
        L_0x0025:
            r0 = move-exception
            r7.A07(r0)     // Catch:{ all -> 0x0095 }
        L_0x0029:
            r1 = r6
        L_0x002a:
            r7.A01 = r1     // Catch:{ all -> 0x0095 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0095 }
            r0 = 14
            if (r1 < r0) goto L_0x0091
            int r2 = X.AnonymousClass1Y3.BCt     // Catch:{ all -> 0x0095 }
            X.0UN r0 = r7.A03     // Catch:{ all -> 0x0095 }
            r1 = 8
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r2, r0)     // Catch:{ all -> 0x0095 }
            android.content.Context r5 = (android.content.Context) r5     // Catch:{ all -> 0x0095 }
            boolean r0 = r5 instanceof android.app.Application     // Catch:{ all -> 0x0095 }
            if (r0 == 0) goto L_0x0045
            android.app.Application r5 = (android.app.Application) r5     // Catch:{ all -> 0x0095 }
            goto L_0x005d
        L_0x0045:
            android.content.Context r0 = r5.getApplicationContext()     // Catch:{ all -> 0x0095 }
            boolean r0 = r0 instanceof android.app.Application     // Catch:{ all -> 0x0095 }
            if (r0 == 0) goto L_0x005c
            X.0UN r0 = r7.A03     // Catch:{ all -> 0x0095 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r2, r0)     // Catch:{ all -> 0x0095 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x0095 }
            android.content.Context r5 = r0.getApplicationContext()     // Catch:{ all -> 0x0095 }
            android.app.Application r5 = (android.app.Application) r5     // Catch:{ all -> 0x0095 }
            goto L_0x005d
        L_0x005c:
            r5 = r6
        L_0x005d:
            if (r5 == 0) goto L_0x0091
            java.lang.Object r3 = r7.A0C     // Catch:{ all -> 0x0095 }
            monitor-enter(r3)     // Catch:{ all -> 0x0095 }
            android.net.NetworkInfo r0 = r7.A01     // Catch:{ all -> 0x008e }
            if (r0 == 0) goto L_0x0080
            android.net.NetworkInfo$DetailedState r2 = r0.getDetailedState()     // Catch:{ all -> 0x008e }
            android.net.NetworkInfo$DetailedState r0 = android.net.NetworkInfo.DetailedState.BLOCKED     // Catch:{ all -> 0x008e }
            r1 = 0
            if (r2 != r0) goto L_0x0070
            r1 = 1
        L_0x0070:
            if (r1 == 0) goto L_0x0082
            X.0e4 r0 = r7.A02     // Catch:{ all -> 0x008e }
            if (r0 != 0) goto L_0x0082
            X.0e4 r0 = new X.0e4     // Catch:{ all -> 0x008e }
            r0.<init>(r7)     // Catch:{ all -> 0x008e }
            r7.A02 = r0     // Catch:{ all -> 0x008e }
            r5.registerActivityLifecycleCallbacks(r0)     // Catch:{ all -> 0x008e }
        L_0x0080:
            monitor-exit(r3)     // Catch:{ all -> 0x008e }
            goto L_0x0091
        L_0x0082:
            if (r1 != 0) goto L_0x0080
            X.0e4 r0 = r7.A02     // Catch:{ all -> 0x008e }
            if (r0 == 0) goto L_0x0080
            r5.unregisterActivityLifecycleCallbacks(r0)     // Catch:{ all -> 0x008e }
            r7.A02 = r6     // Catch:{ all -> 0x008e }
            goto L_0x0080
        L_0x008e:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x008e }
            throw r0     // Catch:{ all -> 0x0095 }
        L_0x0091:
            android.net.NetworkInfo r0 = r7.A01     // Catch:{ all -> 0x0095 }
            monitor-exit(r4)     // Catch:{ all -> 0x0095 }
            return r0
        L_0x0095:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0095 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09340h3.A00():android.net.NetworkInfo");
    }

    public static final C09340h3 A02(AnonymousClass1XY r4) {
        if (A0N == null) {
            synchronized (C09340h3.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0N, r4);
                if (A002 != null) {
                    try {
                        A0N = new C09340h3(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0N;
    }

    private void A03(NetworkInfo networkInfo) {
        if (networkInfo == null || !networkInfo.isConnected()) {
            this.A0I = 0;
        } else if (this.A0I == 0) {
            this.A0I = ((AnonymousClass069) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBa, this.A03)).now();
        }
    }

    public static void A05(C09340h3 r3) {
        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BKH, r3.A03), new C07700e1(r3), 501344964);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0053, code lost:
        if (r4.isConnected() == false) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005d, code lost:
        if (r3.isConnected() == false) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A06(X.C09340h3 r6, boolean r7) {
        /*
            java.lang.Object r5 = r6.A0C
            monitor-enter(r5)
            android.net.NetworkInfo r4 = r6.A01     // Catch:{ all -> 0x0070 }
            r6.A00()     // Catch:{ all -> 0x0070 }
            r0 = 0
            r6.A09 = r0     // Catch:{ all -> 0x0070 }
            r6.A08 = r0     // Catch:{ all -> 0x0070 }
            r6.A07 = r0     // Catch:{ all -> 0x0070 }
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x0070 }
            X.0UN r0 = r6.A03     // Catch:{ all -> 0x0070 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0070 }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x0070 }
            long r0 = r0.now()     // Catch:{ all -> 0x0070 }
            r6.A06 = r0     // Catch:{ all -> 0x0070 }
            android.net.NetworkInfo r0 = r6.A01     // Catch:{ all -> 0x0070 }
            r6.A03(r0)     // Catch:{ all -> 0x0070 }
            android.net.NetworkInfo r3 = r6.A01     // Catch:{ all -> 0x0070 }
            monitor-exit(r5)     // Catch:{ all -> 0x0070 }
            boolean r0 = r6.A08()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r6.A0L = r0
            int r2 = X.AnonymousClass1Y3.AtR
            X.0UN r1 = r6.A03
            r0 = 10
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1TM r0 = (X.AnonymousClass1TM) r0
            java.lang.String r0 = r0.A01()
            r6.A0K = r0
            if (r7 == 0) goto L_0x006f
            if (r4 == r3) goto L_0x006f
            if (r4 == 0) goto L_0x006c
            if (r3 == 0) goto L_0x006c
            if (r4 == 0) goto L_0x0055
            boolean r0 = r4.isConnected()
            r2 = 1
            if (r0 != 0) goto L_0x0056
        L_0x0055:
            r2 = 0
        L_0x0056:
            if (r3 == 0) goto L_0x005f
            boolean r1 = r3.isConnected()
            r0 = 1
            if (r1 != 0) goto L_0x0060
        L_0x005f:
            r0 = 0
        L_0x0060:
            if (r2 != r0) goto L_0x006c
            int r1 = r4.getType()
            int r0 = r3.getType()
            if (r1 == r0) goto L_0x006f
        L_0x006c:
            A04(r6)
        L_0x006f:
            return
        L_0x0070:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0070 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09340h3.A06(X.0h3, boolean):void");
    }

    private void A07(SecurityException securityException) {
        int incrementAndGet = this.A0G.incrementAndGet();
        if (incrementAndGet % 64 == 1) {
            AnonymousClass06G A022 = AnonymousClass06F.A02("FbNetworkManager", AnonymousClass08S.A0B("success: ", this.A0F.get(), " failures: ", incrementAndGet));
            A022.A03 = securityException;
            A022.A04 = true;
            ((AnonymousClass09P) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Amr, this.A03)).CGQ(A022.A00());
        }
    }

    public static boolean A09(Context context) {
        int i = Build.VERSION.SDK_INT;
        String $const$string = TurboLoader.Locator.$const$string(20);
        ContentResolver contentResolver = context.getContentResolver();
        if (i < 17) {
            if (Settings.System.getInt(contentResolver, $const$string, 0) != 0) {
                return true;
            }
            return false;
        } else if (Settings.Global.getInt(contentResolver, $const$string, 0) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public int A0B() {
        WifiInfo A022 = ((C56022p8) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B5A, this.A03)).A02("FbNetworkManager");
        if (A022 != null) {
            return A022.getRssi();
        }
        return Integer.MIN_VALUE;
    }

    public NetworkInfo A0D() {
        NetworkInfo networkInfo;
        if (this.A0M) {
            return null;
        }
        boolean z = false;
        long now = ((AnonymousClass069) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBa, this.A03)).now();
        long AqL = (long) (((C25051Yd) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AOJ, this.A03)).AqL(564981473019040L, 300) * AnonymousClass1Y3.A87);
        synchronized (this.A0C) {
            networkInfo = this.A01;
            if (networkInfo == null) {
                networkInfo = A00();
            }
            if (now - this.A06 > AqL) {
                this.A06 = now;
                z = true;
            }
        }
        if (z) {
            A05(this);
        }
        return networkInfo;
    }

    public Optional A0G() {
        synchronized (this.A0B) {
            if (this.A05 == Long.MIN_VALUE) {
                Optional absent = Optional.absent();
                return absent;
            }
            Optional of = Optional.of(Long.valueOf(((AnonymousClass069) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBa, this.A03)).now() - this.A05));
            return of;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0062, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A0H() {
        /*
            r5 = this;
            java.lang.Object r3 = r5.A0C
            monitor-enter(r3)
            android.net.NetworkInfo r4 = r5.A0E()     // Catch:{ all -> 0x0063 }
            r2 = 0
            if (r4 == 0) goto L_0x0061
            android.net.NetworkInfo$State r1 = r4.getState()     // Catch:{ all -> 0x0063 }
            android.net.NetworkInfo$State r0 = android.net.NetworkInfo.State.DISCONNECTED     // Catch:{ all -> 0x0063 }
            if (r1 == r0) goto L_0x0061
            android.net.NetworkInfo$State r0 = android.net.NetworkInfo.State.DISCONNECTING     // Catch:{ all -> 0x0063 }
            if (r1 == r0) goto L_0x0061
            int r1 = r4.getType()     // Catch:{ all -> 0x0063 }
            if (r1 == 0) goto L_0x0051
            r0 = 1
            if (r1 == r0) goto L_0x002f
            java.lang.String r2 = r4.getTypeName()     // Catch:{ all -> 0x0063 }
            java.lang.String r1 = "/"
            java.lang.String r0 = r4.getSubtypeName()     // Catch:{ all -> 0x0063 }
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x0063 }
            monitor-exit(r3)     // Catch:{ all -> 0x0063 }
            return r0
        L_0x002f:
            android.net.wifi.WifiInfo r0 = r5.A0F()     // Catch:{ all -> 0x0063 }
            if (r0 == 0) goto L_0x0041
            java.lang.String r1 = "WIFI/"
            java.lang.String r0 = r0.getSSID()     // Catch:{ all -> 0x0063 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0063 }
            monitor-exit(r3)     // Catch:{ all -> 0x0063 }
            return r0
        L_0x0041:
            java.lang.String r2 = r4.getTypeName()     // Catch:{ all -> 0x0063 }
            java.lang.String r1 = "/"
            java.lang.String r0 = r4.getSubtypeName()     // Catch:{ all -> 0x0063 }
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x0063 }
            monitor-exit(r3)     // Catch:{ all -> 0x0063 }
            return r0
        L_0x0051:
            java.lang.String r2 = r4.getSubtypeName()     // Catch:{ all -> 0x0063 }
            java.lang.String r1 = "/"
            java.lang.String r0 = r5.A0M()     // Catch:{ all -> 0x0063 }
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x0063 }
            monitor-exit(r3)     // Catch:{ all -> 0x0063 }
            return r0
        L_0x0061:
            monitor-exit(r3)     // Catch:{ all -> 0x0063 }
            return r2
        L_0x0063:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0063 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09340h3.A0H():java.lang.String");
    }

    public String A0I() {
        String str;
        synchronized (this.A0C) {
            str = this.A07;
            if (str == null) {
                str = AnonymousClass08S.A0P(A0K(), "-", A0J());
                this.A07 = str;
            }
        }
        return str;
    }

    public String A0J() {
        String str;
        synchronized (this.A0C) {
            str = this.A08;
            if (str == null) {
                NetworkInfo A0E2 = A0E();
                if (A0E2 == null || C06850cB.A0B(A0E2.getSubtypeName())) {
                    str = "none";
                } else {
                    str = A0E2.getSubtypeName().toLowerCase(Locale.US);
                }
                this.A08 = str;
            }
        }
        return str;
    }

    public String A0K() {
        String str;
        synchronized (this.A0C) {
            str = this.A09;
            if (str == null) {
                NetworkInfo A0E2 = A0E();
                if (A0E2 == null || C06850cB.A0B(A0E2.getTypeName())) {
                    str = "none";
                } else {
                    str = A0E2.getTypeName().toLowerCase(Locale.US);
                }
                this.A09 = str;
            }
        }
        return str;
    }

    public String A0M() {
        return ((TelephonyManager) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BO1, this.A03)).getNetworkOperatorName();
    }

    public void A0N(long j) {
        int i = AnonymousClass1Y3.BBa;
        long now = ((AnonymousClass069) AnonymousClass1XX.A02(4, i, this.A03)).now() + j;
        long now2 = now - ((AnonymousClass069) AnonymousClass1XX.A02(4, i, this.A03)).now();
        synchronized (this.A0B) {
            while (now2 > 0) {
                if (A0Q()) {
                    break;
                }
                this.A0B.wait(now2);
                now2 = now - ((AnonymousClass069) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBa, this.A03)).now();
            }
        }
    }

    public boolean A0P() {
        Boolean valueOf;
        if (this.A0L != null) {
            valueOf = this.A0L;
        } else {
            valueOf = Boolean.valueOf(A08());
            this.A0L = valueOf;
        }
        return valueOf.booleanValue();
    }

    public boolean A0T() {
        boolean booleanValue;
        if (Build.VERSION.SDK_INT < 23) {
            return false;
        }
        synchronized (this.A0D) {
            if (this.A04 == null) {
                this.A04 = Boolean.valueOf(((PowerManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8E, this.A03)).isDeviceIdleMode());
            }
            booleanValue = this.A04.booleanValue();
        }
        return booleanValue;
    }

    public List AlV() {
        return this.A0A.A03();
    }

    private C09340h3(AnonymousClass1XY r3) {
        this.A03 = new AnonymousClass0UN(14, r3);
        this.A0A = new C09330h1(10);
    }

    public static final C09340h3 A01(AnonymousClass1XY r0) {
        return A02(r0);
    }

    public static void A04(C09340h3 r5) {
        long j;
        boolean A0Q = r5.A0Q();
        ((C04460Ut) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AKb, r5.A03)).C4y("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED");
        ((AnonymousClass1YR) AnonymousClass1XX.A02(12, AnonymousClass1Y3.AdX, r5.A03)).A01("action_network_connectivity_changed");
        synchronized (r5.A0B) {
            if (A0Q) {
                j = ((AnonymousClass069) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBa, r5.A03)).now();
            } else {
                j = Long.MIN_VALUE;
            }
            r5.A00 = j;
            r5.A05 = ((AnonymousClass069) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBa, r5.A03)).now();
            r5.A0B.notifyAll();
        }
    }

    public int A0A() {
        return WifiManager.calculateSignalLevel(A0B(), 5);
    }

    public long A0C() {
        int i;
        int i2;
        NetworkInfo A0D2 = A0D();
        WifiInfo A022 = ((C56022p8) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B5A, this.A03)).A02("FbNetworkManager");
        NetworkInfo.State state = NetworkInfo.State.DISCONNECTED;
        String str = BuildConfig.FLAVOR;
        if (A0D2 != null) {
            if (A022 != null) {
                str = A022.getSSID();
            }
            i = A0D2.getType();
            i2 = A0D2.getSubtype();
            state = A0D2.getState();
            A0D2.getTypeName();
            A0D2.getSubtypeName();
            A0D2.getState().toString();
        } else {
            i = 0;
            i2 = 0;
        }
        return (long) Arrays.hashCode(new Object[]{Integer.valueOf(i), Integer.valueOf(i2), state, str});
    }

    public NetworkInfo A0E() {
        NetworkInfo A0D2 = A0D();
        if (A0D2 == null || !A0D2.isConnected()) {
            return null;
        }
        return A0D2;
    }

    public WifiInfo A0F() {
        if (A0Q()) {
            try {
                return ((C56022p8) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B5A, this.A03)).A02("FbNetworkManager");
            } catch (Exception unused) {
            }
        }
        return null;
    }

    public String A0L() {
        String str;
        NetworkInfo.DetailedState detailedState;
        NetworkInfo A0E2 = A0E();
        if (A0E2 == null || (detailedState = A0E2.getDetailedState()) == null) {
            str = null;
        } else {
            str = detailedState.name();
        }
        if (C06850cB.A0B(str)) {
            return "none";
        }
        return str;
    }

    public boolean A0O() {
        NetworkInfo A0E2 = A0E();
        if (A0E2 == null || A0E2.getType() != 1) {
            return false;
        }
        return true;
    }

    public boolean A0Q() {
        NetworkInfo A0D2 = A0D();
        if (A0D2 == null || !A0D2.isConnected()) {
            return false;
        }
        return true;
    }

    public boolean A0R() {
        NetworkInfo A0E2 = A0E();
        if (A0E2 == null || A0E2.getType() != 0) {
            return false;
        }
        return true;
    }

    public boolean A0S() {
        NetworkInfo A0E2 = A0E();
        if (A0E2 != null) {
            return C37511vn.A04(A0E2.getType(), A0E2.getSubtype());
        }
        return false;
    }

    public void init() {
        int A032 = C000700l.A03(1053058936);
        A03(A0D());
        C07770e8 r4 = new C07770e8(this);
        C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A2N, this.A03)).BMm();
        BMm.A02(TurboLoader.Locator.$const$string(3), r4);
        BMm.A02("android.net.conn.INET_CONDITION_ACTION", r4);
        if (Build.VERSION.SDK_INT >= 23) {
            BMm.A02("android.os.action.DEVICE_IDLE_MODE_CHANGED", new C07710e2(this));
        }
        if (Build.VERSION.SDK_INT >= 21) {
            BMm.A02(TurboLoader.Locator.$const$string(61), new C07780e9(this));
        }
        BMm.A00().A00();
        C000700l.A09(907752564, A032);
    }
}
