package X;

/* renamed from: X.1fM  reason: invalid class name and case insensitive filesystem */
public final class C28781fM implements C08470fP {
    public long A00;
    public long A01;
    public long A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public final C09020gN A06;
    public final C25051Yd A07;
    public final String A08;
    private final AnonymousClass1T4 A09;
    private final AnonymousClass1T3 A0A;

    public void onFrameRendered(int i) {
        if (this.A03 || !this.A04) {
            int max = Math.max(1, i);
            int A022 = this.A0A.A02();
            long A012 = this.A02 + ((long) ((this.A09.A01(max, A022, 100) + 1) * A022));
            this.A02 = A012;
            long j = this.A00;
            if (j > 0 && A012 > this.A01) {
                try {
                    Thread.sleep(j * ((long) A022));
                } catch (InterruptedException unused) {
                }
                this.A02 = 0;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0038, code lost:
        if (r10.A07.Aem(283858683629111L) == false) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C28781fM(X.C27031cX r11, X.AnonymousClass1T3 r12, X.C25051Yd r13, X.AnonymousClass1T4 r14, boolean r15, java.lang.String r16) {
        /*
            r10 = this;
            r10.<init>()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r15)
            X.0gN r0 = r11.A00(r0)
            r10.A06 = r0
            r0.A01 = r10
            r10.A0A = r12
            r10.A07 = r13
            r10.A09 = r14
            r1 = r16
            r10.A08 = r1
            java.lang.String r0 = "news_feed_scroll"
            boolean r2 = r1.equals(r0)
            r0 = 283858683563574(0x1022b00000e36, double:1.40244823822483E-309)
            if (r2 == 0) goto L_0x0075
            boolean r0 = r13.Aem(r0)
            if (r0 == 0) goto L_0x003a
            X.1Yd r2 = r10.A07
            r0 = 283858683629111(0x1022b00010e37, double:1.40244823854863E-309)
            boolean r1 = r2.Aem(r0)
            r0 = 1
            if (r1 != 0) goto L_0x003b
        L_0x003a:
            r0 = 0
        L_0x003b:
            r10.A04 = r0
        L_0x003d:
            X.1Yd r2 = r10.A07
            r0 = 565333660403013(0x2022b00020545, double:2.793119400428106E-309)
            long r8 = r2.At0(r0)
            X.1Yd r2 = r10.A07
            r0 = 565333660468550(0x2022b00030546, double:2.7931194007519E-309)
            long r0 = r2.At0(r0)
            r10.A00 = r0
            X.1Yd r2 = r10.A07
            r0 = 565333660534087(0x2022b00040547, double:2.793119401075697E-309)
            long r6 = r2.At0(r0)
            r4 = 0
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x006a
            r4 = 60000(0xea60, double:2.9644E-319)
            long r4 = r4 / r8
        L_0x006a:
            r10.A01 = r4
            r2 = 100
            long r0 = r2 - r6
            long r4 = r4 * r0
            long r4 = r4 / r2
            r10.A02 = r4
            return
        L_0x0075:
            boolean r0 = r13.Aem(r0)
            r10.A04 = r0
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28781fM.<init>(X.1cX, X.1T3, X.1Yd, X.1T4, boolean, java.lang.String):void");
    }
}
