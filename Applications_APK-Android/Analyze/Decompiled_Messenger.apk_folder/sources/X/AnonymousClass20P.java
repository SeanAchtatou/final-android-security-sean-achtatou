package X;

/* renamed from: X.20P  reason: invalid class name */
public final class AnonymousClass20P {
    public C22218Aqu A00;

    public AnI A00() {
        C22219Aqv aqv = this.A00.senderChainKey_;
        return new AnI(aqv.iteration_, aqv.seed_.A0B());
    }

    public void A01(AnI anI) {
        C22222Aqy aqy = new C22222Aqy();
        int i = anI.A00;
        aqy.A00 |= 1;
        aqy.A01 = i;
        aqy.A0W();
        aqy.A0a(C49782cr.A01(anI.A01));
        C22219Aqv A0Y = aqy.AQe();
        C22217Aqt A09 = this.A00.CJ2();
        A09.A0Z(A0Y);
        this.A00 = A09.AQe();
    }

    public AnonymousClass20P(int i, int i2, byte[] bArr, C60932y4 r9, C22128Ap3 ap3) {
        C22222Aqy aqy = new C22222Aqy();
        aqy.A00 |= 1;
        aqy.A01 = i2;
        aqy.A0W();
        aqy.A0a(C49782cr.A01(bArr));
        C22219Aqv A0Y = aqy.AQe();
        C22221Aqx aqx = new C22221Aqx();
        C49782cr A01 = C49782cr.A01(r9.A00());
        if (A01 != null) {
            aqx.A00 |= 1;
            aqx.A02 = A01;
            aqx.A0W();
            if (ap3.A02()) {
                C49782cr A012 = C49782cr.A01(((C49002bU) ap3.A01()).A00);
                if (A012 != null) {
                    aqx.A00 |= 2;
                    aqx.A01 = A012;
                    aqx.A0W();
                }
            }
            C22217Aqt aqt = new C22217Aqt();
            aqt.A00 |= 1;
            aqt.A01 = i;
            aqt.A0W();
            aqt.A0Z(A0Y);
            C22201Aqd aqd = null;
            C22220Aqw A0Y2 = aqx.AQe();
            if (aqd == null) {
                aqt.A05 = A0Y2;
                aqt.A0W();
            } else {
                aqd.A05(A0Y2);
            }
            aqt.A00 |= 4;
            this.A00 = aqt.AQe();
            return;
        }
        throw new NullPointerException();
    }

    public AnonymousClass20P(C22218Aqu aqu) {
        this.A00 = aqu;
    }
}
