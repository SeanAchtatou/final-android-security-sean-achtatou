package X;

/* renamed from: X.0AN  reason: invalid class name */
public final class AnonymousClass0AN implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$4";
    public final /* synthetic */ AnonymousClass0AH A00;

    public AnonymousClass0AN(AnonymousClass0AH r1) {
        this.A00 = r1;
    }

    public void run() {
        boolean z;
        if (!this.A00.A0I.CDh()) {
            AnonymousClass07g r1 = this.A00.A06;
            if (r1 != null) {
                r1.BIp("keep_alive", "should_not_be_connected");
            }
            this.A00.A0K(AnonymousClass0CE.A05);
        } else if (this.A00.A0Q()) {
            AnonymousClass07g r12 = this.A00.A06;
            if (r12 != null) {
                r12.BIp("keep_alive", "send ping");
            }
            this.A00.A0N(null);
        } else {
            AnonymousClass07g r13 = this.A00.A06;
            if (r13 != null) {
                r13.BIp("keep_alive", "not connected");
            }
            if (!this.A00.A0R()) {
                this.A00.A0H();
                AnonymousClass0BL r14 = this.A00.A0P;
                synchronized (r14) {
                    if (!AnonymousClass0BL.A02(r14)) {
                        if (r14.A04 == null) {
                            r14.A04();
                        } else {
                            r14.A05();
                        }
                        z = true;
                    } else {
                        z = false;
                    }
                }
                if (z) {
                    this.A00.A0A.A0E = C01770Bo.A0B;
                }
            }
        }
    }
}
