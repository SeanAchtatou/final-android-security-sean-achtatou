package X;

import com.facebook.common.dextricks.DexStore;

/* renamed from: X.09s  reason: invalid class name and case insensitive filesystem */
public final class C013109s {
    private static final byte[] A00;
    private static final char[] A01;
    private static final char[] A02;

    public static String A00(byte[] bArr, boolean z) {
        char[] cArr = new char[(r7 << 1)];
        int i = 0;
        for (byte b : bArr) {
            byte b2 = b & 255;
            if (b2 == 0 && z) {
                break;
            }
            int i2 = i + 1;
            cArr[i] = A01[b2];
            i = i2 + 1;
            cArr[i2] = A02[b2];
        }
        return new String(cArr, 0, i);
    }

    static {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] cArr2 = new char[DexStore.LOAD_RESULT_OATMEAL_QUICKENED];
        A01 = cArr2;
        char[] cArr3 = new char[DexStore.LOAD_RESULT_OATMEAL_QUICKENED];
        A02 = cArr3;
        for (int i = 0; i < 256; i++) {
            cArr2[i] = cArr[(i >> 4) & 15];
            cArr3[i] = cArr[i & 15];
        }
        byte[] bArr = new byte[103];
        A00 = bArr;
        for (int i2 = 0; i2 <= 70; i2++) {
            bArr[i2] = -1;
        }
        for (byte b = 0; b < 10; b = (byte) (b + 1)) {
            bArr[b + 48] = b;
        }
        for (byte b2 = 0; b2 < 6; b2 = (byte) (b2 + 1)) {
            byte b3 = (byte) (b2 + 10);
            bArr[b2 + 65] = b3;
            bArr[b2 + 97] = b3;
        }
    }

    public static byte[] A01(String str) {
        boolean z;
        byte[] bArr;
        byte b;
        byte b2;
        int length = str.length();
        if ((length & 1) == 0) {
            byte[] bArr2 = new byte[(length >> 1)];
            int i = 0;
            int i2 = 0;
            while (true) {
                z = true;
                if (i >= length) {
                    z = false;
                    break;
                }
                int i3 = i + 1;
                char charAt = str.charAt(i);
                if (charAt > 'f' || (b = (bArr = A00)[charAt]) == -1) {
                    break;
                }
                i = i3 + 1;
                char charAt2 = str.charAt(i3);
                if (charAt2 > 'f' || (b2 = bArr[charAt2]) == -1) {
                    break;
                }
                bArr2[i2] = (byte) ((b << 4) | b2);
                i2++;
            }
            if (!z) {
                return bArr2;
            }
            throw new IllegalArgumentException(AnonymousClass08S.A0J("Invalid hexadecimal digit: ", str));
        }
        throw new IllegalArgumentException("Odd number of characters.");
    }
}
