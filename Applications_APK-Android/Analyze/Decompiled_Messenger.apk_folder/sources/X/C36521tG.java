package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.send.SendError;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tG  reason: invalid class name and case insensitive filesystem */
public final class C36521tG {
    private static final Class A0E = C36521tG.class;
    private static volatile C36521tG A0F;
    public C06790c5 A00 = null;
    public AnonymousClass0UN A01;
    public final C04460Ut A02;
    public final C194918j A03;
    public final C36511tF A04;
    public final C195018k A05;
    public final C36531tH A06;
    public final C12420pJ A07;
    public final FbSharedPreferences A08;
    public final ExecutorService A09;
    public final C04310Tq A0A;
    private final AnonymousClass187 A0B;
    private final C193917y A0C;
    private final C04310Tq A0D;

    public void A0O(String str, SendError sendError) {
        boolean z = true;
        boolean z2 = false;
        if (sendError != null) {
            z2 = true;
        }
        Preconditions.checkArgument(z2);
        if (sendError.A02 == C36891u4.A07) {
            z = false;
        }
        Preconditions.checkArgument(z);
        ContentValues contentValues = new ContentValues();
        A06(sendError, contentValues);
        contentValues.put(C195818v.A06.A00, Integer.valueOf(AnonymousClass1V7.A0A.dbKeyValue));
        A09(this, str, contentValues);
    }

    private int A01(SQLiteDatabase sQLiteDatabase, ThreadKey threadKey, String str) {
        int A012;
        C25601a6 A013 = C06160ax.A01(C06160ax.A01(C06160ax.A03(C194818i.A0A.A00, threadKey.A0J()), C06160ax.A03(C194818i.A05.A00, str)), C06160ax.A02(new AnonymousClass1CG(C194818i.A07.A00), new AnonymousClass1CG(C194818i.A02.A00)));
        Cursor query = sQLiteDatabase.query("thread_devices", new String[]{C194818i.A09.A00}, A013.A02(), A013.A04(), null, null, null);
        if (query == null || !query.moveToNext()) {
            A012 = this.A03.A01();
        } else if (!C194818i.A09.A06(query)) {
            A012 = C194818i.A09.A00(query);
        } else {
            A012 = -1;
        }
        if (query != null) {
            query.close();
        }
        return A012;
    }

    public static ContentValues A02(C36521tG r4, Message message, byte[] bArr, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(C195818v.A05.A00, message.A0q);
        contentValues.put(C195818v.A0E.A00, message.A0U.toString());
        contentValues.put(C195818v.A02.A00, r4.A05.A02(message.A0U, bArr, i));
        contentValues.put(C195818v.A09.A00, Long.valueOf(Long.parseLong(message.A0K.A01.id)));
        contentValues.put(C195818v.A0F.A00, Long.valueOf(message.A03));
        contentValues.put(C195818v.A0G.A00, Long.valueOf(message.A02));
        contentValues.put(C195818v.A06.A00, Integer.valueOf(message.A0D.dbKeyValue));
        contentValues.put(C195818v.A07.A00, message.A0w);
        contentValues.put(C195818v.A01.A00, message.A0m);
        return contentValues;
    }

    public static final C36521tG A03(AnonymousClass1XY r7) {
        if (A0F == null) {
            synchronized (C36521tG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0F, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A0F = new C36521tG(applicationInjector, AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, applicationInjector), AnonymousClass187.A01(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.B6n, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0F;
    }

    public static void A04(SQLiteDatabase sQLiteDatabase, ThreadKey threadKey, long j) {
        ContentValues contentValues = new ContentValues();
        AnonymousClass0W6 r2 = C193717w.A05;
        contentValues.put(r2.A00, Long.valueOf(j));
        C25601a6 A012 = C06160ax.A01(C06160ax.A03(C193717w.A0F.A00, threadKey.toString()), new C52712jZ(r2.A00, Long.toString(j)));
        sQLiteDatabase.update("threads", contentValues, A012.A02(), A012.A04());
    }

    private static void A05(SQLiteDatabase sQLiteDatabase, ThreadKey threadKey, long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(C193717w.A0H.A00, Long.valueOf(j));
        C06140av A032 = C06160ax.A03(C193717w.A0F.A00, threadKey.toString());
        sQLiteDatabase.update("threads", contentValues, A032.A02(), A032.A04());
    }

    private static void A06(SendError sendError, ContentValues contentValues) {
        String str;
        String str2 = C195818v.A0A.A00;
        C36891u4 r1 = sendError.A02;
        if (r1 == C36891u4.A07) {
            str = null;
        } else {
            str = r1.serializedString;
        }
        contentValues.put(str2, str);
        contentValues.put(C195818v.A0B.A00, sendError.A06);
        contentValues.put(C195818v.A0C.A00, Long.valueOf(sendError.A01));
    }

    public static void A07(ThreadKey threadKey, ContentValues contentValues) {
        contentValues.put(C193717w.A0F.A00, threadKey.A0J());
        contentValues.put(C193717w.A09.A00, Long.valueOf(threadKey.A01));
    }

    public static void A08(C36521tG r0, ThreadKey threadKey, ContentValues contentValues) {
        A00(((AnonymousClass183) r0.A0A.get()).A01(), threadKey, contentValues);
    }

    public static void A09(C36521tG r3, String str, ContentValues contentValues) {
        SQLiteDatabase A012 = ((AnonymousClass183) r3.A0A.get()).A01();
        C06140av A032 = C06160ax.A03(C195818v.A05.A00, str);
        A012.update("messages", contentValues, A032.A02(), A032.A04());
    }

    public static byte[] A0A(C36521tG r0, byte[] bArr, int i) {
        try {
            return r0.A0B.A03(bArr, i);
        } catch (C37911wY | C37921wZ | IOException e) {
            C010708t.A08(A0E, "Failed to encrypt message for local storage", e);
            throw new RuntimeException(e);
        }
    }

    public void A0B() {
        ContentValues contentValues = new ContentValues();
        contentValues.putNull(C194818i.A07.A00);
        ((AnonymousClass183) this.A0A.get()).A01().update("thread_devices", contentValues, null, null);
    }

    public void A0C(long j, String str, String str2, String str3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AnonymousClass182.A05.A00, Long.valueOf(j));
        contentValues.put(AnonymousClass182.A00.A00, str);
        contentValues.put(AnonymousClass182.A02.A00, str2);
        contentValues.put(AnonymousClass182.A03.A00, str3);
        SQLiteDatabase A012 = ((AnonymousClass183) this.A0A.get()).A01();
        C007406x.A00(712588973);
        A012.replaceOrThrow("thread_participants", null, contentValues);
        C007406x.A00(-1551629633);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x00da  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D(com.facebook.messaging.model.messages.Message r7, byte[] r8, java.lang.String r9, boolean r10) {
        /*
            r6 = this;
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0U
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r0)
            com.google.common.base.Preconditions.checkArgument(r0)
            android.content.ContentValues r2 = new android.content.ContentValues
            r2.<init>()
            X.0W6 r0 = X.C195818v.A05
            java.lang.String r1 = r0.A00
            java.lang.String r0 = r7.A0q
            r2.put(r1, r0)
            X.0W6 r0 = X.C195818v.A0E
            java.lang.String r1 = r0.A00
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0U
            java.lang.String r0 = r0.toString()
            r2.put(r1, r0)
            X.18j r0 = r6.A03
            int r5 = r0.A01()
            X.0W6 r0 = X.C195818v.A0D
            java.lang.String r1 = r0.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r2.put(r1, r0)
            X.0W6 r0 = X.C195818v.A02
            java.lang.String r3 = r0.A00
            X.18k r1 = r6.A05
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0U
            byte[] r0 = r1.A02(r0, r8, r5)
            r2.put(r3, r0)
            X.0W6 r0 = X.C195818v.A09
            java.lang.String r3 = r0.A00
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r7.A0K
            com.facebook.user.model.UserKey r0 = r0.A01
            java.lang.String r0 = r0.id
            long r0 = java.lang.Long.parseLong(r0)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.put(r3, r0)
            X.0W6 r0 = X.C195818v.A0F
            java.lang.String r3 = r0.A00
            long r0 = r7.A03
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.put(r3, r0)
            X.0W6 r0 = X.C195818v.A0G
            java.lang.String r3 = r0.A00
            long r0 = r7.A02
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.put(r3, r0)
            X.0W6 r0 = X.C195818v.A06
            java.lang.String r1 = r0.A00
            X.1V7 r0 = r7.A0D
            int r0 = r0.dbKeyValue
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.put(r1, r0)
            X.0W6 r0 = X.C195818v.A07
            java.lang.String r1 = r0.A00
            java.lang.String r0 = r7.A0w
            r2.put(r1, r0)
            r4 = 0
            if (r9 == 0) goto L_0x00a5
            X.18k r3 = r6.A05     // Catch:{ UnsupportedEncodingException -> 0x009d }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r7.A0U     // Catch:{ UnsupportedEncodingException -> 0x009d }
            java.lang.String r0 = "UTF-8"
            byte[] r0 = r9.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x009d }
            byte[] r1 = r3.A02(r1, r0, r5)     // Catch:{ UnsupportedEncodingException -> 0x009d }
            goto L_0x00a6
        L_0x009d:
            r3 = move-exception
            java.lang.Class r1 = X.C36521tG.A0E
            java.lang.String r0 = "Failed to encrypt attachment for local storage"
            X.C010708t.A08(r1, r0, r3)
        L_0x00a5:
            r1 = r4
        L_0x00a6:
            X.0W6 r0 = X.C195818v.A08
            java.lang.String r0 = r0.A00
            r2.put(r0, r1)
            X.0W6 r0 = X.C195818v.A01
            java.lang.String r1 = r0.A00
            java.lang.Long r0 = r7.A0m
            r2.put(r1, r0)
            com.facebook.messaging.model.send.SendError r0 = r7.A0S
            A06(r0, r2)
            X.0Tq r0 = r6.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r3 = r0.A01()
            r0 = 2071006773(0x7b710e35, float:1.2516317E36)
            X.C007406x.A00(r0)
            java.lang.String r0 = "messages"
            r3.replaceOrThrow(r0, r4, r2)
            r0 = -1584807214(0xffffffffa189c2d2, float:-9.33505E-19)
            X.C007406x.A00(r0)
            if (r10 == 0) goto L_0x00e1
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r7.A0U
            long r0 = r7.A03
            A05(r3, r2, r0)
        L_0x00e1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36521tG.A0D(com.facebook.messaging.model.messages.Message, byte[], java.lang.String, boolean):void");
    }

    public void A0E(ThreadKey threadKey, long j) {
        A05(((AnonymousClass183) this.A0A.get()).A01(), threadKey, j);
    }

    public void A0F(ThreadKey threadKey, Integer num) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(C193717w.A06.A00, Integer.valueOf(C22075Anv.A00(num)));
        A08(this, threadKey, contentValues);
    }

    public void A0G(ThreadKey threadKey, Integer num) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(C193717w.A0A.A00, num);
        A08(this, threadKey, contentValues);
    }

    public void A0H(ThreadKey threadKey, String str, String str2) {
        ContentValues contentValues = new ContentValues();
        SQLiteDatabase A012 = ((AnonymousClass183) this.A0A.get()).A01();
        C007406x.A01(A012, -1504184862);
        int A013 = A01(A012, threadKey, str);
        contentValues.put(C194818i.A09.A00, Integer.valueOf(A013));
        this.A05.A01(C194818i.A07.A00, threadKey, str2, A013, contentValues);
        String A0J = threadKey.A0J();
        C25601a6 A014 = C06160ax.A01(C06160ax.A03(C194818i.A0A.A00, A0J), C06160ax.A03(C194818i.A01.A00, Long.toString(threadKey.A01)), C06160ax.A03(C194818i.A05.A00, str));
        try {
            if (A012.update("thread_devices", contentValues, A014.A02(), A014.A04()) == 0) {
                contentValues.put(C194818i.A0A.A00, A0J);
                contentValues.put(C194818i.A01.A00, Long.valueOf(threadKey.A01));
                contentValues.put(C194818i.A05.A00, str);
                C007406x.A00(566630001);
                A012.insertOrThrow("thread_devices", null, contentValues);
                C007406x.A00(-1386870717);
            }
            A012.setTransactionSuccessful();
        } finally {
            C007406x.A02(A012, -1307644096);
        }
    }

    public void A0I(ThreadKey threadKey, String str, String str2, Long l) {
        ContentValues contentValues = new ContentValues();
        int A012 = this.A03.A01();
        ThreadKey threadKey2 = threadKey;
        this.A05.A01(C193717w.A00.A00, threadKey2, str, A012, contentValues);
        this.A05.A01(C193717w.A0C.A00, threadKey2, str2, A012, contentValues);
        contentValues.put(C193717w.A0D.A00, l);
        contentValues.put(C193717w.A0E.A00, Integer.valueOf(A012));
        A08(this, threadKey, contentValues);
    }

    public void A0J(ThreadKey threadKey, Set set) {
        C25601a6 A012 = C06160ax.A01(C06160ax.A03(C195818v.A0E.A00, threadKey.toString()), C06160ax.A04(C195818v.A05.A00, set));
        ((AnonymousClass183) this.A0A.get()).A01().delete("messages", A012.A02(), A012.A04());
    }

    public void A0K(ThreadKey threadKey, boolean z) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(C193717w.A07.A00, Integer.valueOf(z ? 1 : 0));
        A08(this, threadKey, contentValues);
    }

    public void A0L(Long l, Long l2, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.putNull(C194818i.A07.A00);
        if (str == null) {
            str = this.A0C.A08(l2.longValue());
        }
        if (str != null) {
            C25601a6 A012 = C06160ax.A01(C06160ax.A03(C194818i.A0A.A00, ThreadKey.A05(l2.longValue(), l.longValue()).A0J()), C06160ax.A03(C194818i.A01.A00, l2.toString()), C06160ax.A03(C194818i.A05.A00, str));
            ((AnonymousClass183) this.A0A.get()).A01().update("thread_devices", contentValues, A012.A02(), A012.A04());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x00b6, code lost:
        if (r4.equals(r9.A0D.get()) == false) goto L_0x00b8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0M(java.lang.String r10, com.facebook.messaging.model.messages.Message r11, byte[] r12, byte[] r13, java.lang.String r14, boolean r15) {
        /*
            r9 = this;
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r11.A0U
            boolean r0 = r0.A0K()
            com.google.common.base.Preconditions.checkArgument(r0)
            X.0Tq r0 = r9.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r2 = r0.A01()
            r0 = -1490575376(0xffffffffa7279ff0, float:-2.3262608E-15)
            X.C007406x.A01(r2, r0)
            r6 = r14
            if (r14 == 0) goto L_0x006a
            android.content.ContentValues r8 = new android.content.ContentValues     // Catch:{ all -> 0x00f2 }
            r8.<init>()     // Catch:{ all -> 0x00f2 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r11.A0U     // Catch:{ all -> 0x00f2 }
            int r7 = r9.A01(r2, r0, r10)     // Catch:{ all -> 0x00f2 }
            X.0W6 r0 = X.C194818i.A09     // Catch:{ all -> 0x00f2 }
            java.lang.String r1 = r0.A00     // Catch:{ all -> 0x00f2 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x00f2 }
            r8.put(r1, r0)     // Catch:{ all -> 0x00f2 }
            com.facebook.messaging.model.threadkey.ThreadKey r5 = r11.A0U     // Catch:{ all -> 0x00f2 }
            X.18k r3 = r9.A05     // Catch:{ all -> 0x00f2 }
            X.0W6 r0 = X.C194818i.A07     // Catch:{ all -> 0x00f2 }
            java.lang.String r4 = r0.A00     // Catch:{ all -> 0x00f2 }
            r3.A01(r4, r5, r6, r7, r8)     // Catch:{ all -> 0x00f2 }
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r11.A0U     // Catch:{ all -> 0x00f2 }
            X.0W6 r0 = X.C194818i.A0A     // Catch:{ all -> 0x00f2 }
            java.lang.String r1 = r0.A00     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = r3.A0J()     // Catch:{ all -> 0x00f2 }
            X.0av r1 = X.C06160ax.A03(r1, r0)     // Catch:{ all -> 0x00f2 }
            X.0W6 r0 = X.C194818i.A05     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x00f2 }
            X.0av r0 = X.C06160ax.A03(r0, r10)     // Catch:{ all -> 0x00f2 }
            X.0av[] r0 = new X.C06140av[]{r1, r0}     // Catch:{ all -> 0x00f2 }
            X.1a6 r0 = X.C06160ax.A01(r0)     // Catch:{ all -> 0x00f2 }
            java.lang.String r3 = r0.A02()     // Catch:{ all -> 0x00f2 }
            java.lang.String[] r1 = r0.A04()     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = "thread_devices"
            r2.update(r0, r8, r3, r1)     // Catch:{ all -> 0x00f2 }
        L_0x006a:
            if (r15 == 0) goto L_0x00e8
            X.18j r0 = r9.A03     // Catch:{ all -> 0x00f2 }
            int r4 = r0.A01()     // Catch:{ all -> 0x00f2 }
            android.content.ContentValues r3 = A02(r9, r11, r12, r4)     // Catch:{ all -> 0x00f2 }
            X.0W6 r0 = X.C195818v.A0D     // Catch:{ all -> 0x00f2 }
            java.lang.String r1 = r0.A00     // Catch:{ all -> 0x00f2 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x00f2 }
            r3.put(r1, r0)     // Catch:{ all -> 0x00f2 }
            X.0W6 r0 = X.C195818v.A04     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x00f2 }
            r3.put(r0, r13)     // Catch:{ all -> 0x00f2 }
            java.lang.String r1 = "messages"
            r0 = 1411504755(0x5421da73, float:2.78061882E12)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x00f2 }
            r4 = 0
            r2.insertOrThrow(r1, r4, r3)     // Catch:{ all -> 0x00f2 }
            r0 = 1201267101(0x4799e19d, float:78787.23)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x00f2 }
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r11.A0U     // Catch:{ all -> 0x00f2 }
            long r0 = r11.A03     // Catch:{ all -> 0x00f2 }
            A05(r2, r3, r0)     // Catch:{ all -> 0x00f2 }
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r11.A0K     // Catch:{ all -> 0x00f2 }
            if (r0 == 0) goto L_0x00a9
            java.lang.String r4 = r0.A00()     // Catch:{ all -> 0x00f2 }
        L_0x00a9:
            if (r4 == 0) goto L_0x00b8
            X.0Tq r0 = r9.A0D     // Catch:{ all -> 0x00f2 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00f2 }
            boolean r1 = r4.equals(r0)     // Catch:{ all -> 0x00f2 }
            r0 = 1
            if (r1 != 0) goto L_0x00b9
        L_0x00b8:
            r0 = 0
        L_0x00b9:
            if (r0 == 0) goto L_0x00e8
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r11.A0U     // Catch:{ all -> 0x00f2 }
            long r5 = r11.A03     // Catch:{ all -> 0x00f2 }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ all -> 0x00f2 }
            r4.<init>()     // Catch:{ all -> 0x00f2 }
            X.0W6 r0 = X.C193717w.A05     // Catch:{ all -> 0x00f2 }
            java.lang.String r1 = r0.A00     // Catch:{ all -> 0x00f2 }
            java.lang.Long r0 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x00f2 }
            r4.put(r1, r0)     // Catch:{ all -> 0x00f2 }
            X.0W6 r0 = X.C193717w.A0F     // Catch:{ all -> 0x00f2 }
            java.lang.String r1 = r0.A00     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x00f2 }
            X.0av r0 = X.C06160ax.A03(r1, r0)     // Catch:{ all -> 0x00f2 }
            java.lang.String r3 = r0.A02()     // Catch:{ all -> 0x00f2 }
            java.lang.String[] r1 = r0.A04()     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = "threads"
            r2.update(r0, r4, r3, r1)     // Catch:{ all -> 0x00f2 }
        L_0x00e8:
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x00f2 }
            r0 = 1717211545(0x665a9199, float:2.5804042E23)
            X.C007406x.A02(r2, r0)
            return
        L_0x00f2:
            r1 = move-exception
            r0 = -881036307(0xffffffffcb7c73ed, float:-1.6544749E7)
            X.C007406x.A02(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36521tG.A0M(java.lang.String, com.facebook.messaging.model.messages.Message, byte[], byte[], java.lang.String, boolean):void");
    }

    public void A0N(String str, AnonymousClass1V7 r5) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(C195818v.A06.A00, Integer.valueOf(r5.dbKeyValue));
        A09(this, str, contentValues);
    }

    private C36521tG(AnonymousClass1XY r5, C04310Tq r6, AnonymousClass187 r7, C04310Tq r8) {
        this.A01 = new AnonymousClass0UN(1, r5);
        this.A02 = C04430Uq.A02(r5);
        this.A09 = AnonymousClass0UX.A0a(r5);
        this.A06 = C36531tH.A00(r5);
        this.A08 = FbSharedPreferencesModule.A00(r5);
        this.A0C = C193917y.A00(r5);
        this.A07 = C12420pJ.A00(r5);
        this.A04 = C36511tF.A00(r5);
        this.A0D = AnonymousClass0WY.A02(r5);
        C36541tI.A00(r5);
        C194918j A002 = C194918j.A00(r5);
        this.A03 = A002;
        this.A0A = r6;
        this.A0B = r7;
        this.A05 = new C195018k(r7, r8, r6, A002);
        C06600bl BMm = this.A02.BMm();
        BMm.A02(C06680bu.A13, new C36551tJ(this));
        BMm.A00().A00();
        C06600bl BMm2 = this.A02.BMm();
        BMm2.A02("com.facebook.common.appstate.AppStateManager.USER_LEFT_APP", new C36561tK(this));
        C06790c5 A003 = BMm2.A00();
        this.A00 = A003;
        A003.A00();
    }

    public static int A00(SQLiteDatabase sQLiteDatabase, ThreadKey threadKey, ContentValues contentValues) {
        C06140av A032 = C06160ax.A03(C193717w.A0F.A00, threadKey.toString());
        return sQLiteDatabase.update("threads", contentValues, A032.A02(), A032.A04());
    }
}
