package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.10s  reason: invalid class name and case insensitive filesystem */
public final class C182010s {
    private static volatile C182010s A01;
    public final AnonymousClass0iR A00;

    public static final C182010s A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C182010s.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C182010s(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C182010s(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0iR.A00(r2);
    }
}
