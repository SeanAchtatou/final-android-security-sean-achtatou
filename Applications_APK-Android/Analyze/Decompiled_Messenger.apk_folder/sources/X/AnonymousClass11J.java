package X;

import com.facebook.litho.ComponentTree;

/* renamed from: X.11J  reason: invalid class name */
public final class AnonymousClass11J {
    public C17770zR A00;
    public C35561rQ A01;
    public C637038i A02;
    public C31551js A03;
    public C31551js A04;
    public AnonymousClass1JC A05;
    public String A06;
    public boolean A07 = AnonymousClass07c.canInterruptAndMoveLayoutsBetweenThreads;
    public boolean A08 = false;
    public boolean A09 = true;
    public boolean A0A = AnonymousClass07c.incrementalVisibilityHandling;
    public boolean A0B = true;
    public boolean A0C = AnonymousClass07c.isReconciliationEnabled;
    public boolean A0D;
    public boolean A0E = AnonymousClass07c.useCancelableLayoutFutures;
    public final AnonymousClass0p4 A0F;

    public ComponentTree A00() {
        if (this.A00 == null) {
            this.A00 = C16980y8.A00(this.A0F).A00;
        }
        if (this.A02 != null && this.A06 == null) {
            this.A06 = this.A00.A1A();
        }
        return new ComponentTree(this);
    }

    public AnonymousClass11J(AnonymousClass0p4 r2) {
        this.A0F = r2;
    }
}
