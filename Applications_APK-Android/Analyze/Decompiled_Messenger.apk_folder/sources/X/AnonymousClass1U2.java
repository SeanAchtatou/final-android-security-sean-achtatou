package X;

import android.content.Context;
import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.inject.ContextScoped;
import java.util.concurrent.ScheduledExecutorService;

@ContextScoped
/* renamed from: X.1U2  reason: invalid class name */
public final class AnonymousClass1U2 implements C29281gA {
    private static C04470Uu A0A;
    public static final Class A0B = AnonymousClass1U2.class;
    public final Context A00;
    public final AnonymousClass1Y6 A01;
    public final AnonymousClass06B A02 = AnonymousClass067.A02();
    public final AnonymousClass069 A03;
    public final C29291gB A04;
    public final C24411Tn A05;
    public final AnonymousClass1U3 A06;
    public final Long A07;
    public final ScheduledExecutorService A08;
    private final AnonymousClass1U0 A09;

    public static final AnonymousClass1U2 A00(AnonymousClass1XY r4) {
        AnonymousClass1U2 r0;
        synchronized (AnonymousClass1U2.class) {
            C04470Uu A002 = C04470Uu.A00(A0A);
            A0A = A002;
            try {
                if (A002.A03(r4)) {
                    A0A.A00 = new AnonymousClass1U2((AnonymousClass1XY) A0A.A01());
                }
                C04470Uu r1 = A0A;
                r0 = (AnonymousClass1U2) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A0A.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0079, code lost:
        if (r1 == false) goto L_0x007b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0092 A[Catch:{ RemoteException -> 0x00a3 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C36991uR BvK() {
        /*
            r18 = this;
            r2 = r18
            X.1U0 r0 = r2.A09
            r0.init()
            X.1uR r9 = new X.1uR
            android.content.Context r10 = r2.A00
            X.1U3 r11 = r2.A06
            X.06B r12 = r2.A02
            X.1Tn r13 = r2.A05
            X.1Y6 r14 = r2.A01
            X.069 r15 = r2.A03
            java.util.concurrent.ScheduledExecutorService r1 = r2.A08
            X.1gB r0 = r2.A04
            r16 = r1
            r17 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)
            java.lang.Long r0 = r2.A07     // Catch:{ RemoteException -> 0x00a3 }
            long r1 = r0.longValue()     // Catch:{ RemoteException -> 0x00a3 }
            X.1Y6 r0 = r9.A03     // Catch:{ RemoteException -> 0x00a3 }
            r0.AOx()     // Catch:{ RemoteException -> 0x00a3 }
            r3 = r9
            monitor-enter(r3)     // Catch:{ RemoteException -> 0x00a3 }
            boolean r0 = r9.A01     // Catch:{ all -> 0x00a0 }
            if (r0 != 0) goto L_0x009a
            monitor-exit(r3)     // Catch:{ RemoteException -> 0x00a3 }
            X.1Tn r5 = r9.A08     // Catch:{ RemoteException -> 0x00a3 }
            android.content.Context r4 = r9.A02     // Catch:{ RemoteException -> 0x00a3 }
            android.content.Intent r3 = new android.content.Intent     // Catch:{ RemoteException -> 0x00a3 }
            r3.<init>()     // Catch:{ RemoteException -> 0x00a3 }
            X.1mN r0 = r9.A07     // Catch:{ RemoteException -> 0x00a3 }
            r8 = 1
            X.1pF r3 = r5.A02(r4, r3, r0, r8)     // Catch:{ RemoteException -> 0x00a3 }
            boolean r0 = r3.A01     // Catch:{ RemoteException -> 0x00a3 }
            if (r0 == 0) goto L_0x007b
            android.os.IBinder r3 = r3.A00     // Catch:{ RemoteException -> 0x00a3 }
            if (r3 == 0) goto L_0x0051
            X.1mN r0 = r9.A07     // Catch:{ RemoteException -> 0x00a3 }
            r0.A00(r3)     // Catch:{ RemoteException -> 0x00a3 }
        L_0x004f:
            monitor-enter(r9)     // Catch:{ RemoteException -> 0x00a3 }
            goto L_0x007d
        L_0x0051:
            X.1mN r0 = r9.A07     // Catch:{ RemoteException -> 0x00a3 }
            java.util.concurrent.CountDownLatch r7 = r0.A00     // Catch:{ RemoteException -> 0x00a3 }
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ RemoteException -> 0x00a3 }
            r6 = 0
            long r4 = r0.toNanos(r1)     // Catch:{ all -> 0x0084 }
            long r2 = java.lang.System.nanoTime()     // Catch:{ all -> 0x0084 }
            long r2 = r2 + r4
        L_0x0061:
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ InterruptedException -> 0x006a }
            boolean r1 = r7.await(r4, r0)     // Catch:{ InterruptedException -> 0x006a }
            if (r6 == 0) goto L_0x0079
            goto L_0x0072
        L_0x006a:
            r6 = 1
            long r0 = java.lang.System.nanoTime()     // Catch:{ all -> 0x0084 }
            long r4 = r2 - r0
            goto L_0x0061
        L_0x0072:
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ RemoteException -> 0x00a3 }
            r0.interrupt()     // Catch:{ RemoteException -> 0x00a3 }
        L_0x0079:
            if (r1 != 0) goto L_0x004f
        L_0x007b:
            r0 = 0
            goto L_0x0090
        L_0x007d:
            r9.A01 = r8     // Catch:{ all -> 0x0081 }
            monitor-exit(r9)     // Catch:{ all -> 0x0081 }
            goto L_0x008f
        L_0x0081:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0081 }
            goto L_0x00a2
        L_0x0084:
            r1 = move-exception
            if (r6 == 0) goto L_0x008e
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ RemoteException -> 0x00a3 }
            r0.interrupt()     // Catch:{ RemoteException -> 0x00a3 }
        L_0x008e:
            throw r1     // Catch:{ RemoteException -> 0x00a3 }
        L_0x008f:
            r0 = 1
        L_0x0090:
            if (r0 != 0) goto L_0x0099
            java.lang.Class r1 = X.AnonymousClass1U2.A0B     // Catch:{ RemoteException -> 0x00a3 }
            java.lang.String r0 = "Failed to bind to MqttPushService"
            X.C010708t.A06(r1, r0)     // Catch:{ RemoteException -> 0x00a3 }
        L_0x0099:
            return r9
        L_0x009a:
            android.os.RemoteException r0 = new android.os.RemoteException     // Catch:{ all -> 0x00a0 }
            r0.<init>()     // Catch:{ all -> 0x00a0 }
            throw r0     // Catch:{ all -> 0x00a0 }
        L_0x00a0:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ RemoteException -> 0x00a3 }
        L_0x00a2:
            throw r0     // Catch:{ RemoteException -> 0x00a3 }
        L_0x00a3:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1U2.BvK():X.1uR");
    }

    private AnonymousClass1U2(AnonymousClass1XY r3) {
        this.A00 = AnonymousClass1YA.A00(r3);
        this.A01 = AnonymousClass0UX.A07(r3);
        this.A06 = AnonymousClass1U3.A00(r3);
        this.A05 = C24411Tn.A00(r3);
        this.A03 = AnonymousClass067.A03(r3);
        this.A04 = C29291gB.A00(r3);
        this.A07 = Long.valueOf((long) StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS);
        this.A08 = AnonymousClass0UX.A0Q(r3);
        this.A09 = AnonymousClass1U0.A00(r3);
    }
}
