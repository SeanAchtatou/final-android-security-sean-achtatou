package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

/* renamed from: X.0B4  reason: invalid class name */
public abstract class AnonymousClass0B4 {
    public static final AnonymousClass0B4 A00;

    public abstract SharedPreferences A01(Context context, String str, boolean z);

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            A00 = new AnonymousClass0B5();
        } else {
            A00 = new AnonymousClass0E9();
        }
    }

    public static void A00(SharedPreferences.Editor editor) {
        if (Build.VERSION.SDK_INT >= 9) {
            editor.apply();
        } else {
            editor.commit();
        }
    }
}
