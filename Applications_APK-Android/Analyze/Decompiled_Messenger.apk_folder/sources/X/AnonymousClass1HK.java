package X;

import android.content.Context;
import android.content.IntentFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1HK  reason: invalid class name */
public final class AnonymousClass1HK {
    private static volatile AnonymousClass1HK A01;
    public AnonymousClass1S7 A00;

    public static final AnonymousClass1HK A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (AnonymousClass1HK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        AnonymousClass0ZS A003 = AnonymousClass0ZS.A00(applicationInjector);
                        A01 = new AnonymousClass1HK(A003.A07(), AnonymousClass1YA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public DateFormat A01() {
        return this.A00.A02();
    }

    public SimpleDateFormat A02() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A01.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = (SimpleDateFormat) r3.A01().clone();
        AnonymousClass1S7.A00(simpleDateFormat2, "MMMM yyyy", r3.A0C);
        r3.A01.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A03() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A02.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE", r3.A0C);
        r3.A02.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A04() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A03.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = (SimpleDateFormat) r3.A01().clone();
        AnonymousClass1S7.A00(simpleDateFormat2, "MMMd", r3.A0C);
        r3.A03.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A05() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A04.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = (SimpleDateFormat) r3.A01().clone();
        AnonymousClass1S7.A00(simpleDateFormat2, "MMM d h:mm a", r3.A0C);
        r3.A04.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A06() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A05.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = (SimpleDateFormat) r3.A01().clone();
        AnonymousClass1S7.A00(simpleDateFormat2, "MMMd, yyyy", r3.A0C);
        r3.A05.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A07() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A06.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MMM d, yyyy h:mm a", r3.A0C);
        r3.A06.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A08() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A07.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = (SimpleDateFormat) r3.A01().clone();
        AnonymousClass1S7.A00(simpleDateFormat2, "MMM yyyy", r3.A0C);
        r3.A07.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A09() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A08.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EE", r3.A0C);
        r3.A08.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A0A() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A09.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = (SimpleDateFormat) r3.A01().clone();
        AnonymousClass1S7.A00(simpleDateFormat2, "EE, MMM d", r3.A0C);
        r3.A0A.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    public SimpleDateFormat A0B() {
        AnonymousClass1S7 r3 = this.A00;
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) r3.A0A.get();
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = (SimpleDateFormat) r3.A01().clone();
        AnonymousClass1S7.A00(simpleDateFormat2, "EEEE, MMMM d", r3.A0C);
        r3.A0A.set(simpleDateFormat2);
        return simpleDateFormat2;
    }

    private AnonymousClass1HK(Locale locale, Context context) {
        this.A00 = new AnonymousClass1S7(locale, context);
        if (context != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            context.registerReceiver(new AnonymousClass06W("android.intent.action.TIMEZONE_CHANGED", new AnonymousClass1S8(this, locale, context)), intentFilter);
        }
    }
}
