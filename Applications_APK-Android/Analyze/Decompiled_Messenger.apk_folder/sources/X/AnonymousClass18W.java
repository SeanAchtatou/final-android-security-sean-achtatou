package X;

import android.content.Context;
import android.content.Intent;
import com.google.common.base.Platform;

/* renamed from: X.18W  reason: invalid class name */
public final class AnonymousClass18W implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass18W(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(406451019);
        if (this.A00.A03 != null) {
            String stringExtra = intent.getStringExtra("user_id");
            if (!Platform.stringIsNullOrEmpty(stringExtra)) {
                this.A00.A03.BtR(stringExtra);
            }
        }
        AnonymousClass09Y.A01(-1505900683, A002);
    }
}
