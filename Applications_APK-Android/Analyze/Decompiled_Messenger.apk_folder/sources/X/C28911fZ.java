package X;

import android.database.Cursor;

/* renamed from: X.1fZ  reason: invalid class name and case insensitive filesystem */
public final class C28911fZ extends C28751fJ {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final AnonymousClass12W A0A;

    public C28911fZ(Cursor cursor, AnonymousClass12W r4) {
        super(cursor);
        this.A0A = r4;
        this.A00 = cursor.getColumnIndexOrThrow("id");
        this.A01 = cursor.getColumnIndexOrThrow("large_background_uri");
        this.A08 = cursor.getColumnIndexOrThrow("small_icon_asset_uri");
        this.A02 = cursor.getColumnIndexOrThrow("large_icon_asset_uri");
        this.A03 = cursor.getColumnIndexOrThrow("reaction_asset_ids");
        this.A04 = cursor.getColumnIndexOrThrow("reaction_emojis");
        this.A05 = cursor.getColumnIndexOrThrow("reaction_kf_uris");
        this.A07 = cursor.getColumnIndexOrThrow("small_reaction_static_uris");
        this.A06 = cursor.getColumnIndexOrThrow("small_reaction_static_uris");
        this.A09 = cursor.getColumnIndexOrThrow("view_mode");
    }
}
