package X;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: X.0fj  reason: invalid class name and case insensitive filesystem */
public final class C08660fj extends C08670fk {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final C08680fl A06;

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0018  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(X.C15130um r2, int r3) {
        /*
            if (r3 == 0) goto L_0x002c
            r0 = 1
            if (r3 == r0) goto L_0x0029
            r0 = 2
            if (r3 != r0) goto L_0x0027
            r0 = 6
        L_0x0009:
            int r1 = r2.A02(r0)
            if (r1 == 0) goto L_0x0027
            int r0 = r2.A00
            int r1 = r1 + r0
            java.lang.String r0 = r2.A05(r1)
        L_0x0016:
            if (r0 != 0) goto L_0x0026
            r0 = 4
            int r1 = r2.A02(r0)
            if (r1 == 0) goto L_0x002e
            int r0 = r2.A00
            int r1 = r1 + r0
            java.lang.String r0 = r2.A05(r1)
        L_0x0026:
            return r0
        L_0x0027:
            r0 = 0
            goto L_0x0016
        L_0x0029:
            r0 = 8
            goto L_0x0009
        L_0x002c:
            r0 = 4
            goto L_0x0009
        L_0x002e:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08660fj.A00(X.0um, int):java.lang.String");
    }

    public C08660fj(ByteBuffer byteBuffer) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        C08680fl r3 = new C08680fl();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int i7 = byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position();
        r3.A00 = i7;
        r3.A01 = byteBuffer;
        r3.A02 = r3.A01(r3.A02(8) + i7);
        r3.A00 = r3.A01(r3.A02(14) + i7);
        r3.A01 = r3.A01(r3.A02(20) + i7);
        this.A06 = r3;
        int A022 = r3.A02(6);
        if (A022 != 0) {
            i = byteBuffer.getInt(A022 + i7);
        } else {
            i = 0;
        }
        this.A04 = i;
        int A023 = r3.A02(12);
        if (A023 != 0) {
            i2 = r3.A01.getInt(A023 + r3.A00);
        } else {
            i2 = 0;
        }
        this.A00 = i2;
        int A024 = r3.A02(18);
        if (A024 != 0) {
            i3 = r3.A01.getInt(A024 + r3.A00);
        } else {
            i3 = 0;
        }
        this.A02 = i3;
        int A025 = r3.A02(10);
        if (A025 != 0) {
            i4 = r3.A04(A025);
        } else {
            i4 = 0;
        }
        this.A05 = i4;
        int A026 = r3.A02(16);
        if (A026 != 0) {
            i5 = r3.A04(A026);
        } else {
            i5 = 0;
        }
        this.A01 = i5;
        int A027 = r3.A02(22);
        if (A027 != 0) {
            i6 = r3.A04(A027);
        } else {
            i6 = 0;
        }
        this.A03 = i6;
    }
}
