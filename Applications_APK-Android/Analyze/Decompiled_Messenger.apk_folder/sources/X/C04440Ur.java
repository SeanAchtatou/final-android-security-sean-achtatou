package X;

import android.content.Intent;
import android.os.Handler;
import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.0Ur  reason: invalid class name and case insensitive filesystem */
public final class C04440Ur extends C04450Us {
    private static C04470Uu A01;
    public final C04480Uv A00;

    public static final C04440Ur A00(AnonymousClass1XY r5) {
        C04440Ur r0;
        synchronized (C04440Ur.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A01.A01();
                    A01.A00 = new C04440Ur(C04490Ux.A0n(r02), C04430Uq.A00(r02));
                }
                C04470Uu r1 = A01;
                r0 = (C04440Ur) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public void C4x(Intent intent) {
        this.A00.A04(intent);
    }

    private C04440Ur(C04480Uv r1, Handler handler) {
        super(handler);
        this.A00 = r1;
    }
}
