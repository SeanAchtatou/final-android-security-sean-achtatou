package X;

import android.util.Log;
import android.view.Choreographer;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.0Te  reason: invalid class name and case insensitive filesystem */
public final class C04250Te implements AnonymousClass05V {
    public long A00 = -1;
    public long A01 = -1;
    public AnonymousClass05U A02;
    public boolean A03 = false;
    private boolean A04;
    public final Choreographer A05;
    public final Runnable A06;
    public final Method A07;
    public final Method A08;
    public final Method A09;

    public void disable() {
        this.A03 = false;
        Runnable runnable = this.A06;
        Method method = this.A09;
        if (method != null) {
            try {
                method.invoke(this.A05, 0, runnable, null);
            } catch (IllegalAccessException | InvocationTargetException e) {
                A00(this, e);
            }
        }
    }

    public void enable() {
        if (!this.A03) {
            this.A00 = -1;
        }
        this.A03 = true;
        A01(this, this.A06);
    }

    public static void A01(C04250Te r4, Runnable runnable) {
        if (!r4.A04) {
            try {
                Method method = r4.A08;
                if (method != null) {
                    method.invoke(r4.A05, 0, runnable, null);
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                A00(r4, e);
            }
        }
    }

    public C04250Te(Choreographer choreographer, AnonymousClass05U r9) {
        Method method;
        Method method2;
        Method method3;
        this.A02 = r9;
        this.A05 = choreographer;
        try {
            method = Choreographer.class.getDeclaredMethod(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1J), Integer.TYPE, Runnable.class, Object.class);
        } catch (NoSuchMethodException e) {
            A00(this, e);
            method = null;
        }
        this.A08 = method;
        try {
            method2 = Choreographer.class.getDeclaredMethod("removeCallbacks", Integer.TYPE, Runnable.class, Object.class);
        } catch (NoSuchMethodException e2) {
            A00(this, e2);
            method2 = null;
        }
        this.A09 = method2;
        try {
            method3 = Choreographer.class.getDeclaredMethod("getFrameTimeNanos", new Class[0]);
        } catch (NoSuchMethodException e3) {
            A00(this, e3);
            method3 = null;
        }
        this.A07 = method3;
        this.A06 = new AnonymousClass8NS(this);
    }

    public static void A00(C04250Te r2, Exception exc) {
        Log.e(r2.getClass().getSimpleName(), "Choreographer reflection failed.", exc);
        r2.A04 = true;
    }
}
