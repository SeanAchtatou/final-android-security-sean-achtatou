package X;

import com.facebook.acra.ACRA;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.User;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.List;
import org.webrtc.audio.WebRtcAudioRecord;

@UserScoped
/* renamed from: X.1bq  reason: invalid class name and case insensitive filesystem */
public final class C26681bq {
    private static C05540Zi A02;
    public AnonymousClass0UN A00;
    public final AnonymousClass0US A01;

    public static final C26681bq A01(AnonymousClass1XY r4) {
        C26681bq r0;
        synchronized (C26681bq.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new C26681bq((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (C26681bq) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public static AnonymousClass0m6 A02(C26681bq r3, ThreadKey threadKey) {
        int i;
        int i2;
        switch (threadKey.A05.ordinal()) {
            case 0:
            case 1:
            case 4:
            case 5:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case 8:
                i = 1;
                i2 = AnonymousClass1Y3.ATy;
                break;
            case 2:
            case 3:
                i = 4;
                i2 = AnonymousClass1Y3.AL3;
                break;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                i = 2;
                i2 = AnonymousClass1Y3.Ad9;
                break;
            default:
                throw new IllegalArgumentException("Thread Key with unexpected type: " + threadKey);
        }
        return (AnonymousClass0m6) AnonymousClass1XX.A02(i, i2, r3.A00);
    }

    public static final C04310Tq A05(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.AuB, r1);
    }

    public static boolean A06(C26681bq r3) {
        return ((C10960l9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BOk, r3.A00)).A0B();
    }

    public MessagesCollection A07(ThreadKey threadKey) {
        if (threadKey == null) {
            return null;
        }
        return A02(this, threadKey).A0P(threadKey);
    }

    public ThreadSummary A08(ThreadKey threadKey) {
        if (threadKey == null) {
            return null;
        }
        return A02(this, threadKey).A0R(threadKey);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (A06(r10) == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.threads.ThreadsCollection A09(X.C10950l8 r11, X.C10700ki r12, com.google.common.collect.ImmutableSet r13) {
        /*
            r10 = this;
            X.0ki r0 = X.C10700ki.NON_SMS
            r9 = 0
            r2 = 1
            if (r12 == r0) goto L_0x0011
            X.0ki r0 = X.C10700ki.PENDING
            if (r12 == r0) goto L_0x0011
            boolean r0 = A06(r10)
            r4 = 1
            if (r0 != 0) goto L_0x0012
        L_0x0011:
            r4 = 0
        L_0x0012:
            X.0ki r0 = X.C10700ki.SMS
            if (r12 == r0) goto L_0x002c
            X.0ki r0 = X.C10700ki.PENDING
            if (r12 == r0) goto L_0x002c
            r3 = 6
            int r1 = X.AnonymousClass1Y3.Abq
            X.0UN r0 = r10.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pJ r0 = (X.C12420pJ) r0
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x002c
            r9 = 1
        L_0x002c:
            X.0ki r0 = X.C10700ki.SMS
            if (r12 != r0) goto L_0x0037
            com.facebook.messaging.model.threads.ThreadsCollection r0 = com.facebook.messaging.model.threads.ThreadsCollection.A02
        L_0x0032:
            if (r4 != 0) goto L_0x0046
            if (r9 != 0) goto L_0x0046
            return r0
        L_0x0037:
            int r1 = X.AnonymousClass1Y3.ATy
            X.0UN r0 = r10.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A0U(r11)
            goto L_0x0032
        L_0x0046:
            java.util.ArrayList r3 = new java.util.ArrayList
            r8 = 3
            r3.<init>(r8)
            r3.add(r0)
            if (r4 == 0) goto L_0x00c0
            r4 = 2
            int r1 = X.AnonymousClass1Y3.Ad9
            X.0UN r0 = r10.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            com.facebook.messaging.model.threads.ThreadsCollection r4 = r0.A0U(r11)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            X.1Xv r7 = r13.iterator()
        L_0x0069:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0090
            java.lang.Object r6 = r7.next()
            X.2e3 r6 = (X.C50452e3) r6
            int r1 = X.AnonymousClass1Y3.BPw
            X.0UN r0 = r10.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.2eN r1 = (X.C50652eN) r1
            monitor-enter(r1)
            X.2eP r0 = X.C50652eN.A00(r1, r6)     // Catch:{ all -> 0x008d }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A01     // Catch:{ all -> 0x008d }
            monitor-exit(r1)
            if (r0 == 0) goto L_0x0069
            r5.add(r0)
            goto L_0x0069
        L_0x008d:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0090:
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x00bb
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x00aa
            int r0 = r5.size()
            if (r0 <= r2) goto L_0x00aa
            X.4JB r0 = new X.4JB
            r0.<init>()
            java.util.Collections.sort(r5, r0)
        L_0x00aa:
            com.facebook.messaging.model.threads.ThreadsCollection r1 = new com.facebook.messaging.model.threads.ThreadsCollection
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r5)
            r1.<init>(r0, r2)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r1, r4)
            com.facebook.messaging.model.threads.ThreadsCollection r4 = X.AnonymousClass170.A00(r0)
        L_0x00bb:
            if (r4 == 0) goto L_0x00c0
            r3.add(r4)
        L_0x00c0:
            if (r9 == 0) goto L_0x00d4
            r2 = 4
            int r1 = X.AnonymousClass1Y3.AL3
            X.0UN r0 = r10.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A0U(r11)
            r3.add(r0)
        L_0x00d4:
            com.facebook.messaging.model.threads.ThreadsCollection r0 = X.AnonymousClass170.A00(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26681bq.A09(X.0l8, X.0ki, com.google.common.collect.ImmutableSet):com.facebook.messaging.model.threads.ThreadsCollection");
    }

    public String A0C(ThreadSummary threadSummary, User user, boolean z) {
        String A0C;
        String A09;
        if (threadSummary == null) {
            A0C = null;
        } else {
            A0C = ((C17270yd) this.A01.get()).A0C(threadSummary, user.A0Q);
        }
        if (A0C != null) {
            return A0C;
        }
        if (user == null) {
            return null;
        }
        if (!z || (A09 = user.A09()) == null) {
            return C189116b.A03(user);
        }
        return A09;
    }

    public void A0E(C50452e3 r4) {
        C50652eN r1 = (C50652eN) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BPw, this.A00);
        if (r1 != null) {
            synchronized (r1) {
                r1.A00.remove(r4);
            }
        }
    }

    public boolean A0F(C10950l8 r6) {
        if (r6 == C10950l8.A0B) {
            return ((AnonymousClass0m6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ATy, this.A00)).A0j(r6);
        }
        if (!((AnonymousClass0m6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ATy, this.A00)).A0j(r6) || (A06(this) && !((AnonymousClass0m6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ad9, this.A00)).A0j(r6))) {
            return false;
        }
        if (!((C12420pJ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Abq, this.A00)).A02() || ((AnonymousClass0m6) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AL3, this.A00)).A0j(r6)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if (r6 != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0G(com.facebook.messaging.model.messages.Message r8) {
        /*
            r7 = this;
            if (r8 == 0) goto L_0x003b
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0U
            if (r0 == 0) goto L_0x003b
            X.0m6 r1 = A02(r7, r0)
            X.0mL r0 = r1.A0A
            X.0mM r6 = r0.A00()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0U     // Catch:{ all -> 0x0029 }
            X.AnonymousClass0m6.A0A(r1, r0)     // Catch:{ all -> 0x0029 }
            X.0mN r1 = r1.A08     // Catch:{ all -> 0x0029 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0U     // Catch:{ all -> 0x0029 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A01(r0)     // Catch:{ all -> 0x0029 }
            r5 = 0
            if (r0 == 0) goto L_0x0033
            long r3 = r0.A07     // Catch:{ all -> 0x0029 }
            long r1 = r8.A03     // Catch:{ all -> 0x0029 }
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0033
            goto L_0x0032
        L_0x0029:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            if (r6 == 0) goto L_0x0031
            r6.close()     // Catch:{ all -> 0x0031 }
        L_0x0031:
            throw r0
        L_0x0032:
            r5 = 1
        L_0x0033:
            if (r6 == 0) goto L_0x0038
            r6.close()
        L_0x0038:
            r0 = 1
            if (r5 != 0) goto L_0x003c
        L_0x003b:
            r0 = 0
        L_0x003c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26681bq.A0G(com.facebook.messaging.model.messages.Message):boolean");
    }

    private C26681bq(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(8, r3);
        this.A01 = AnonymousClass0UQ.A00(AnonymousClass1Y3.A3x, r3);
    }

    public static final C26681bq A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public static AnonymousClass0m6 A03(C26681bq r5, Integer num) {
        Object obj;
        int i;
        int i2 = 1;
        switch (num.intValue()) {
            case 0:
                if (A06(r5)) {
                    obj = AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ad9, r5.A00);
                    return (AnonymousClass0m6) obj;
                }
                return null;
            case 1:
                i = AnonymousClass1Y3.ATy;
                obj = AnonymousClass1XX.A02(i2, i, r5.A00);
                return (AnonymousClass0m6) obj;
            case 2:
                if (((C12420pJ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Abq, r5.A00)).A02()) {
                    i2 = 4;
                    i = AnonymousClass1Y3.AL3;
                    obj = AnonymousClass1XX.A02(i2, i, r5.A00);
                    return (AnonymousClass0m6) obj;
                }
                return null;
            default:
                return null;
        }
    }

    public static String A04(C26681bq r1, ThreadKey threadKey, ParticipantInfo participantInfo) {
        ThreadSummary A08 = r1.A08(threadKey);
        if (A08 == null) {
            return null;
        }
        return ((C17270yd) r1.A01.get()).A0C(A08, participantInfo.A01);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0064, code lost:
        if (com.google.common.base.Platform.stringIsNullOrEmpty(r1) != false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002b, code lost:
        if (r0.isEmpty() != false) goto L_0x002d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A0A(com.facebook.messaging.model.threadkey.ThreadKey r9, java.util.List r10, boolean r11, com.google.common.collect.ImmutableList.Builder r12) {
        /*
            r8 = this;
            com.google.common.collect.ImmutableList$Builder r2 = com.google.common.collect.ImmutableList.builder()
            com.facebook.messaging.model.threads.ThreadSummary r1 = r8.A08(r9)
            r7 = 0
            if (r1 == 0) goto L_0x002d
            X.0US r0 = r8.A01
            java.lang.Object r3 = r0.get()
            X.0yd r3 = (X.C17270yd) r3
            X.0Tq r0 = r3.A03
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0081
            r0 = 0
            r5 = r7
        L_0x0025:
            if (r0 == 0) goto L_0x002e
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x002e
        L_0x002d:
            r5 = r7
        L_0x002e:
            java.util.Iterator r6 = r10.iterator()
        L_0x0032:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x008d
            java.lang.Object r4 = r6.next()
            com.facebook.messaging.model.messages.ParticipantInfo r4 = (com.facebook.messaging.model.messages.ParticipantInfo) r4
            if (r5 != 0) goto L_0x0076
            r1 = r7
        L_0x0041:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 == 0) goto L_0x0067
            r3 = 0
            int r1 = X.AnonymousClass1Y3.AYW
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.16Z r0 = (X.AnonymousClass16Z) r0
            java.lang.String r1 = r0.A02(r4)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 == 0) goto L_0x0067
            if (r11 == 0) goto L_0x0066
            java.lang.String r1 = r4.A02
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 == 0) goto L_0x0067
        L_0x0066:
            r1 = r7
        L_0x0067:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x0032
            r2.add(r1)
            if (r12 == 0) goto L_0x0032
            r12.add(r4)
            goto L_0x0032
        L_0x0076:
            com.facebook.user.model.UserKey r0 = r4.A01
            java.lang.String r0 = r0.id
            java.lang.Object r1 = r5.get(r0)
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0041
        L_0x0081:
            com.facebook.messaging.model.threads.ThreadCustomization r0 = r1.A0e
            com.facebook.messaging.model.threads.NicknamesMap r1 = r0.A00
            X.0jE r0 = r3.A01
            com.google.common.collect.ImmutableMap r0 = r1.A00(r0)
            r5 = r0
            goto L_0x0025
        L_0x008d:
            com.google.common.collect.ImmutableList r0 = r2.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26681bq.A0A(com.facebook.messaging.model.threadkey.ThreadKey, java.util.List, boolean, com.google.common.collect.ImmutableList$Builder):com.google.common.collect.ImmutableList");
    }

    public String A0B(ThreadKey threadKey, ParticipantInfo participantInfo) {
        String A04 = A04(this, threadKey, participantInfo);
        if (A04 != null) {
            return A04;
        }
        return ((AnonymousClass16Z) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AYW, this.A00)).A02(participantInfo);
    }

    public List A0D(Integer num) {
        AnonymousClass0m6 A03 = A03(this, num);
        if (A03 == null) {
            return RegularImmutableList.A02;
        }
        AnonymousClass0mN r4 = A03.A08;
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            AnonymousClass04b r1 = r4.A01;
            if (i >= r1.size()) {
                return arrayList;
            }
            arrayList.add(r1.A07(i));
            i++;
        }
    }
}
