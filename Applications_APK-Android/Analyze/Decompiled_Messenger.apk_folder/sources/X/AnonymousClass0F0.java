package X;

import android.content.Context;
import com.facebook.common.jit.common.JitDisabledChecker;

/* renamed from: X.0F0  reason: invalid class name */
public final class AnonymousClass0F0 implements AnonymousClass0F1 {
    public boolean BFX() {
        return !JitDisabledChecker.sIsJitDisabled;
    }

    public String getErrorMessage() {
        return null;
    }

    public void BSp(Context context) {
        AnonymousClass0F3.A02(context);
    }
}
