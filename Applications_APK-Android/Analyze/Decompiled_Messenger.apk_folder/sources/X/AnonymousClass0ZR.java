package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ZR  reason: invalid class name */
public final class AnonymousClass0ZR {
    private static volatile AnonymousClass0ZR A01;
    private AnonymousClass0UN A00;

    public static final AnonymousClass0ZR A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0ZR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0ZR(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass0ZR(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }

    public C04450Us A01(Integer num) {
        switch (num.intValue()) {
            case 0:
                return (C05880aU) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ail, this.A00);
            case 1:
                return (C07390dL) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AMQ, this.A00);
            case 2:
                return (C04440Ur) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ak7, this.A00);
            case 3:
                return (C07510dg) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AAI, this.A00);
            default:
                throw new IllegalArgumentException("Unknown broadcast manager type!");
        }
    }
}
