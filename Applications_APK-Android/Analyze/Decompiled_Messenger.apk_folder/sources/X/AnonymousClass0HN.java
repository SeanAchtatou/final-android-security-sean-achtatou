package X;

import android.os.Build;
import android.os.Environment;
import android.os.StatFs;

/* renamed from: X.0HN  reason: invalid class name */
public final class AnonymousClass0HN {
    private static long A00 = 1;

    public static synchronized long A01() {
        synchronized (AnonymousClass0HN.class) {
            long j = A00;
            if (j != 1) {
                return j;
            }
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            if (Build.VERSION.SDK_INT >= 18) {
                A00 = A03(statFs);
            } else {
                A00 = ((long) statFs.getBlockSize()) * ((long) statFs.getBlockCount());
            }
            long j2 = A00;
            return j2;
        }
    }

    public static long A00() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        if (Build.VERSION.SDK_INT >= 18) {
            return A02(statFs);
        }
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    public static long A02(StatFs statFs) {
        return statFs.getAvailableBytes();
    }

    public static long A03(StatFs statFs) {
        return statFs.getTotalBytes();
    }
}
