package X;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.1hI  reason: invalid class name and case insensitive filesystem */
public final class C29981hI {
    public static C29991hJ A00;
    public static final ReadWriteLock A01 = new ReentrantReadWriteLock();
    public static volatile boolean A02;

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (X.C29981hI.A00 == null) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00() {
        /*
            java.util.concurrent.locks.ReadWriteLock r0 = X.C29981hI.A01     // Catch:{ all -> 0x001d }
            java.util.concurrent.locks.Lock r0 = r0.readLock()     // Catch:{ all -> 0x001d }
            r0.lock()     // Catch:{ all -> 0x001d }
            boolean r0 = X.C29981hI.A02     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x0012
            X.1hJ r0 = X.C29981hI.A00     // Catch:{ all -> 0x001d }
            r1 = 1
            if (r0 != 0) goto L_0x0013
        L_0x0012:
            r1 = 0
        L_0x0013:
            java.util.concurrent.locks.ReadWriteLock r0 = X.C29981hI.A01
            java.util.concurrent.locks.Lock r0 = r0.readLock()
            r0.unlock()
            return r1
        L_0x001d:
            r1 = move-exception
            java.util.concurrent.locks.ReadWriteLock r0 = X.C29981hI.A01
            java.util.concurrent.locks.Lock r0 = r0.readLock()
            r0.unlock()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29981hI.A00():boolean");
    }
}
