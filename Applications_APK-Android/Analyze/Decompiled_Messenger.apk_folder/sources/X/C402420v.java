package X;

import java.lang.reflect.Type;
import java.util.Collection;

/* renamed from: X.20v  reason: invalid class name and case insensitive filesystem */
public final class C402420v extends AnonymousClass397 {
    private final AnonymousClass397 A00;
    private final AnonymousClass3A5 A01;

    public void write(AnonymousClass3DQ r4, Object obj) {
        Collection<Object> collection = (Collection) obj;
        if (collection == null) {
            r4.A0A();
            return;
        }
        r4.A06();
        for (Object write : collection) {
            this.A00.write(r4, write);
        }
        r4.A08();
    }

    public C402420v(C637238l r2, Type type, AnonymousClass397 r4, AnonymousClass3A5 r5) {
        this.A00 = new AnonymousClass3A7(r2, r4, type);
        this.A01 = r5;
    }

    public Object read(C640839x r3) {
        if (r3.A0F() == AnonymousClass07B.A0p) {
            r3.A0O();
            return null;
        }
        Collection collection = (Collection) this.A01.AU6();
        r3.A0K();
        while (r3.A0Q()) {
            collection.add(this.A00.read(r3));
        }
        r3.A0M();
        return collection;
    }
}
