package X;

import android.content.SharedPreferences;
import android.os.Bundle;

/* renamed from: X.0HB  reason: invalid class name */
public abstract class AnonymousClass0HB {
    public static final AnonymousClass0HB A00 = new AnonymousClass0HE();
    public static final AnonymousClass0HB A01 = new AnonymousClass0HD();
    public static final AnonymousClass0HB A02 = new AnonymousClass0HF();
    public static final AnonymousClass0HB A03 = new AnonymousClass0HC();

    public abstract Class A00();

    public abstract Object A01(SharedPreferences sharedPreferences, String str, Object obj);

    public abstract Object A02(Bundle bundle, String str, Object obj);

    public abstract void A03(SharedPreferences.Editor editor, String str, Object obj);

    public abstract void A04(Bundle bundle, String str, Object obj);
}
