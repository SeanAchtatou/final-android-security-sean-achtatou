package X;

import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.google.common.base.MoreObjects;

/* renamed from: X.0sF  reason: invalid class name and case insensitive filesystem */
public final class C13890sF {
    public static final C13890sF A03 = new C13890sF(ThreadsCollection.A02, 0, DataFetchDisposition.A0I);
    public final long A00;
    public final DataFetchDisposition A01;
    public final ThreadsCollection A02;

    public C13890sF(ThreadsCollection threadsCollection, long j, DataFetchDisposition dataFetchDisposition) {
        this.A02 = threadsCollection;
        this.A00 = j;
        this.A01 = dataFetchDisposition;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("dataSource", this.A01.A07);
        stringHelper.add("lastUpdatedTimeMs", this.A00);
        stringHelper.add("threadsCollection", this.A02);
        return stringHelper.toString();
    }
}
