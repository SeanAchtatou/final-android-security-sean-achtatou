package X;

import java.util.List;
import java.util.Set;

/* renamed from: X.1Xc  reason: invalid class name and case insensitive filesystem */
public abstract class C24791Xc extends AnonymousClass1XX {
    public abstract AnonymousClass1XY getApplicationInjector();

    public abstract Object getInstance(C22916BKm bKm);

    public abstract AnonymousClass0US getLazy(C22916BKm bKm);

    public abstract C04310Tq getProvider(C22916BKm bKm);

    public AnonymousClass0US getLazyList(C22916BKm bKm) {
        return getLazy(AnonymousClass1XX.A00(bKm));
    }

    public AnonymousClass0US getLazySet(C22916BKm bKm) {
        return getLazy(AnonymousClass1XX.A01(bKm));
    }

    public List getList(C22916BKm bKm) {
        return (List) getInstance(AnonymousClass1XX.A00(bKm));
    }

    public C04310Tq getListProvider(C22916BKm bKm) {
        return getProvider(AnonymousClass1XX.A00(bKm));
    }

    public Set getSet(C22916BKm bKm) {
        return (Set) getInstance(AnonymousClass1XX.A01(bKm));
    }

    public C04310Tq getSetProvider(C22916BKm bKm) {
        return getProvider(AnonymousClass1XX.A01(bKm));
    }

    public Object getInstance(Class cls) {
        return getInstance(new C22916BKm(cls, C22919BKp.INSTANCE));
    }

    public Object getInstance(Class cls, Class cls2) {
        return getInstance(new C22916BKm(cls, C22916BKm.A00(cls2)));
    }
}
