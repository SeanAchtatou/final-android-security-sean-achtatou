package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.1Tb  reason: invalid class name and case insensitive filesystem */
public final class C24301Tb {
    private static C04470Uu A01;
    public final AnonymousClass1GC A00;

    public static final C24301Tb A00(AnonymousClass1XY r4) {
        C24301Tb r0;
        synchronized (C24301Tb.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C24301Tb((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C24301Tb) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C24301Tb(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1GB.A00(r2);
    }
}
