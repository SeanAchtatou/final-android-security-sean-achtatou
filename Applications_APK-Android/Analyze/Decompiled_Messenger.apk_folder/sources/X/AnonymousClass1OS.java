package X;

import java.util.Map;

/* renamed from: X.1OS  reason: invalid class name */
public final class AnonymousClass1OS {
    public final AnonymousClass069 A00;
    public final AnonymousClass0US A01;
    public final String A02;
    public final ThreadLocal A03 = new ThreadLocal();
    private final C04980Xe A04;
    private final Map A05 = AnonymousClass0TG.A04();

    public static synchronized boolean A01(AnonymousClass1OS r3) {
        boolean z;
        synchronized (r3) {
            C04980Xe r1 = r3.A04;
            if (r1.A00 == null) {
                C04980Xe.A01(r1);
            }
            z = false;
            if (r1.A00 == C35701re.A02) {
                z = true;
            }
        }
        return z;
    }

    public synchronized String A02(Integer num) {
        String A002;
        A002 = C35711rf.A00(num);
        if (!this.A05.containsKey(A002)) {
            this.A05.put(A002, AnonymousClass08S.A0P(this.A02, "_", A002));
        }
        return (String) this.A05.get(A002);
    }

    public AnonymousClass1OS(AnonymousClass0US r2, String str, C04980Xe r4, AnonymousClass069 r5) {
        this.A01 = r2;
        this.A02 = str;
        this.A04 = r4;
        this.A00 = r5;
    }

    public static void A00(AnonymousClass1OS r2, Integer num, long j) {
        if (A01(r2)) {
            ((C23001Nv) r2.A01.get()).A04(r2.A02(num), j);
        }
    }

    public void A03(Integer num, int i, long j) {
        switch (num.intValue()) {
            case 0:
                A00(this, AnonymousClass07B.A0Y, 1);
                A00(this, AnonymousClass07B.A0i, (long) i);
                A00(this, AnonymousClass07B.A0n, j);
                return;
            case 1:
                A00(this, AnonymousClass07B.A0o, 1);
                A00(this, AnonymousClass07B.A0p, (long) i);
                A00(this, AnonymousClass07B.A0q, j);
                return;
            case 2:
                A00(this, AnonymousClass07B.A02, 1);
                A00(this, AnonymousClass07B.A03, (long) i);
                A00(this, AnonymousClass07B.A04, j);
                return;
            case 3:
                A00(this, AnonymousClass07B.A05, 1);
                A00(this, AnonymousClass07B.A06, (long) i);
                A00(this, AnonymousClass07B.A07, j);
                return;
            default:
                return;
        }
    }
}
