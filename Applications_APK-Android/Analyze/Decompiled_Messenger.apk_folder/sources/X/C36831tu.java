package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tu  reason: invalid class name and case insensitive filesystem */
public final class C36831tu {
    private static volatile C36831tu A01;
    public AnonymousClass0UN A00;

    public static final C36831tu A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C36831tu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C36831tu(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public int A01() {
        return (int) ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).At1(564247033545463L, 1);
    }

    public boolean A02() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501477828081L);
    }

    public boolean A03() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501478090229L);
    }

    public boolean A05() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501477172712L);
    }

    public boolean A06() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501487003231L);
    }

    public boolean A07() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501487330916L);
    }

    public boolean A08() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501486741083L);
    }

    public boolean A0B() {
        return ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501487265379L);
    }

    private C36831tu(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public boolean A04() {
        if (!A08() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501486806620L)) {
            return false;
        }
        return true;
    }

    public boolean A09() {
        if (!A08() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501486872157L)) {
            return false;
        }
        return true;
    }

    public boolean A0A() {
        if (!A08() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501486937694L)) {
            return false;
        }
        return true;
    }

    public boolean A0C() {
        if (!A08() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501488707190L)) {
            return false;
        }
        return true;
    }
}
