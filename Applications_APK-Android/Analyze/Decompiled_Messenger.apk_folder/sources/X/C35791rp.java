package X;

/* renamed from: X.1rp  reason: invalid class name and case insensitive filesystem */
public final class C35791rp implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appchoreographer.BusySignalHandler$2";
    public final /* synthetic */ C25101Yi A00;
    public final /* synthetic */ C28461eq A01;
    public final /* synthetic */ boolean A02;

    public C35791rp(C28461eq r1, C25101Yi r2, boolean z) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = z;
    }

    public void run() {
        synchronized (this.A01.A05) {
            this.A01.A07.put(this.A00, null);
            this.A00.BQF(this.A02, true);
        }
    }
}
