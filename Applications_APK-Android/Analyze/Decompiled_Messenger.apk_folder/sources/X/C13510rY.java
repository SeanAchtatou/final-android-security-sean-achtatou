package X;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0rY  reason: invalid class name and case insensitive filesystem */
public abstract class C13510rY {
    public Thread A00;
    public final CountDownLatch A01 = new CountDownLatch(1);
    public final AtomicBoolean A02 = new AtomicBoolean(false);

    public String A01() {
        return "com.facebook.orca.threadlist.ThreadListFragment";
    }

    public void A02(C13480rV r5) {
        if (this instanceof C10620kY) {
            C13870sD r2 = (C13870sD) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AFM, ((C10620kY) this).A00);
            if (r2.A00.compareAndSet(false, true)) {
                r2.A01();
            }
        }
    }
}
