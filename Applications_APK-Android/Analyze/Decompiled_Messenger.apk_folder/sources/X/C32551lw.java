package X;

import android.util.SparseBooleanArray;

/* renamed from: X.1lw  reason: invalid class name and case insensitive filesystem */
public final class C32551lw implements AnonymousClass1GL {
    public final SparseBooleanArray A00;

    public boolean BEJ(int i) {
        return this.A00.get(i);
    }

    public C32551lw(int i) {
        SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
        sparseBooleanArray.append(i, true);
        this.A00 = sparseBooleanArray;
    }
}
