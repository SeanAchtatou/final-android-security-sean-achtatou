package X;

/* renamed from: X.1xF  reason: invalid class name and case insensitive filesystem */
public final class C38321xF extends C38341xH {
    public static final String __redex_internal_original_name = "com.facebook.android.maps.internal.MapConfig$ConfigUpdateDispatchable";
    private int A00;

    /* JADX WARN: Failed to insert an additional move for type inference into block B:54:? */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r13v0 */
    /* JADX WARN: Type inference failed for: r13v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r13v2 */
    /* JADX WARN: Type inference failed for: r13v3, types: [org.json.JSONObject] */
    /* JADX WARN: Type inference failed for: r13v5 */
    /* JADX WARN: Type inference failed for: r13v7 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00de A[ExcHandler: IOException (e java.io.IOException), Splitter:B:20:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0238 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:20:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x023b A[SYNTHETIC, Splitter:B:83:0x023b] */
    public void run() {
        /*
            r29 = this;
            r6 = r29
            int r0 = r6.A00
            int r0 = r0 + 1
            r6.A00 = r0
            boolean r0 = X.C27111DRr.A00()
            r8 = 1
            r13 = 0
            if (r0 == 0) goto L_0x0059
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 < r0) goto L_0x002a
            android.content.Context r1 = X.C47892Ym.A02
            java.lang.String r0 = "android.permission.ACCESS_COARSE_LOCATION"
            int r0 = r1.checkSelfPermission(r0)
            if (r0 != 0) goto L_0x0059
            android.content.Context r1 = X.C47892Ym.A02
            java.lang.String r0 = "android.permission.ACCESS_FINE_LOCATION"
            int r0 = r1.checkSelfPermission(r0)
            if (r0 != 0) goto L_0x0059
        L_0x002a:
            android.content.Context r1 = X.C47892Ym.A02
            java.lang.String r0 = "location"
            java.lang.Object r3 = r1.getSystemService(r0)
            android.location.LocationManager r3 = (android.location.LocationManager) r3
            java.util.List r0 = r3.getProviders(r8)
            if (r0 == 0) goto L_0x0059
            java.util.Iterator r2 = r0.iterator()
            r9 = r13
        L_0x003f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x005a
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            android.location.Location r1 = X.AnonymousClass0QI.A00(r3, r0)
            if (r9 == 0) goto L_0x0057
            boolean r0 = X.C27143DUj.A00(r1, r9)
            if (r0 == 0) goto L_0x003f
        L_0x0057:
            r9 = r1
            goto L_0x003f
        L_0x0059:
            r9 = r13
        L_0x005a:
            r0 = 512(0x200, float:7.175E-43)
            byte[] r7 = new byte[r0]     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            java.lang.String r5 = X.C47892Ym.A0E     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            java.lang.String r3 = X.C47892Ym.A0D     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            if (r9 != 0) goto L_0x0074
            java.lang.String r0 = ""
        L_0x0068:
            java.lang.String r0 = X.AnonymousClass08S.A0P(r5, r3, r0)     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            java.lang.String r0 = X.C47892Ym.A01(r0)     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            r4.<init>(r0)     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            goto L_0x0096
        L_0x0074:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            r2.<init>()     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            java.lang.String r0 = "&latitude="
            r2.append(r0)     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            double r0 = r9.getLatitude()     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            r2.append(r0)     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            java.lang.String r0 = "&longitude="
            r2.append(r0)     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            double r0 = r9.getLongitude()     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            r2.append(r0)     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            java.lang.String r0 = r2.toString()     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            goto L_0x0068
        L_0x0096:
            java.io.InputStream r3 = r4.openStream()     // Catch:{ Exception -> 0x00cb, IOException -> 0x00de, all -> 0x0238 }
            r5 = 0
            r4 = 0
        L_0x009c:
            int r2 = r7.length     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            int r0 = r2 - r4
            int r1 = r3.read(r7, r4, r0)     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            r0 = -1
            if (r1 == r0) goto L_0x00b2
            int r4 = r4 + r1
            if (r4 < r2) goto L_0x009c
            int r0 = r2 << r8
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            java.lang.System.arraycopy(r7, r5, r0, r5, r2)     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            r7 = r0
            goto L_0x009c
        L_0x00b2:
            java.lang.String r0 = new java.lang.String     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            r0.<init>(r7, r5, r4)     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            java.lang.String r0 = "data"
            org.json.JSONArray r0 = r1.getJSONArray(r0)     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            org.json.JSONObject r13 = r0.getJSONObject(r5)     // Catch:{ IOException -> 0x00c9, Exception -> 0x00c7 }
            goto L_0x00e9
        L_0x00c7:
            r2 = move-exception
            goto L_0x00d6
        L_0x00c9:
            r2 = move-exception
            goto L_0x00e0
        L_0x00cb:
            r2 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            java.lang.String r0 = "Exception while loading map config"
            r1.<init>(r0, r2)     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
            throw r1     // Catch:{ IOException -> 0x00de, Exception -> 0x00d4, all -> 0x0238 }
        L_0x00d4:
            r2 = move-exception
            r3 = r13
        L_0x00d6:
            X.2E5 r1 = X.AnonymousClass2E5.A09     // Catch:{ all -> 0x0235 }
            java.lang.String r0 = ""
            X.AnonymousClass2E5.A03(r1, r0, r2)     // Catch:{ all -> 0x0235 }
            goto L_0x00e7
        L_0x00de:
            r2 = move-exception
            r3 = r13
        L_0x00e0:
            java.lang.String r1 = "MapConfig"
            java.lang.String r0 = "Unable to download config"
            android.util.Log.e(r1, r0, r2)     // Catch:{ all -> 0x0235 }
        L_0x00e7:
            if (r3 == 0) goto L_0x00ec
        L_0x00e9:
            r3.close()     // Catch:{ IOException -> 0x00ec }
        L_0x00ec:
            if (r13 == 0) goto L_0x01e1
            java.lang.String r22 = "south"
            java.lang.String r21 = "east"
            java.lang.String r20 = "north"
            java.lang.String r15 = "west"
            java.lang.String r14 = "rectangles"
            X.2Yn r0 = X.C47892Ym.A0C
            java.lang.String r0 = r0.A01
            java.lang.String r10 = "base_url"
            java.lang.String r23 = r13.optString(r10, r0)
            X.2Yn r0 = X.C47892Ym.A0C
            java.lang.String r1 = r0.A03
            java.lang.String r0 = "static_base_url"
            java.lang.String r24 = r13.optString(r0, r1)
            java.lang.String r0 = "osm_config"
            org.json.JSONObject r1 = r13.getJSONObject(r0)     // Catch:{ JSONException -> 0x0166 }
            java.lang.String r0 = "zoom_threshold"
            int r19 = r1.getInt(r0)     // Catch:{ JSONException -> 0x0166 }
            org.json.JSONArray r12 = r1.getJSONArray(r14)     // Catch:{ JSONException -> 0x0166 }
            int r11 = r12.length()     // Catch:{ JSONException -> 0x0166 }
            android.graphics.Rect[] r0 = new android.graphics.Rect[r11]     // Catch:{ JSONException -> 0x0166 }
            r18 = r0
            int r9 = r8 << r19
            r8 = 0
        L_0x0127:
            if (r8 >= r11) goto L_0x0172
            org.json.JSONObject r7 = r12.getJSONObject(r8)     // Catch:{ JSONException -> 0x0166 }
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ JSONException -> 0x0166 }
            float r4 = (float) r9     // Catch:{ JSONException -> 0x0166 }
            double r0 = r7.getDouble(r15)     // Catch:{ JSONException -> 0x0166 }
            float r0 = X.C28141DpL.A03(r0)     // Catch:{ JSONException -> 0x0166 }
            float r0 = r0 * r4
            int r3 = (int) r0     // Catch:{ JSONException -> 0x0166 }
            r1 = r20
            double r0 = r7.getDouble(r1)     // Catch:{ JSONException -> 0x0166 }
            float r0 = X.C28141DpL.A02(r0)     // Catch:{ JSONException -> 0x0166 }
            float r0 = r0 * r4
            int r2 = (int) r0     // Catch:{ JSONException -> 0x0166 }
            r1 = r21
            double r0 = r7.getDouble(r1)     // Catch:{ JSONException -> 0x0166 }
            float r0 = X.C28141DpL.A03(r0)     // Catch:{ JSONException -> 0x0166 }
            float r0 = r0 * r4
            int r1 = (int) r0     // Catch:{ JSONException -> 0x0166 }
            r0 = r22
            double r16 = r7.getDouble(r0)     // Catch:{ JSONException -> 0x0166 }
            float r0 = X.C28141DpL.A02(r16)     // Catch:{ JSONException -> 0x0166 }
            float r4 = r4 * r0
            int r0 = (int) r4     // Catch:{ JSONException -> 0x0166 }
            r5.<init>(r3, r2, r1, r0)     // Catch:{ JSONException -> 0x0166 }
            r18[r8] = r5     // Catch:{ JSONException -> 0x0166 }
            int r8 = r8 + 1
            goto L_0x0127
        L_0x0166:
            X.2Yn r0 = X.C47892Ym.A0C
            int r0 = r0.A00
            r19 = r0
            X.2Yn r0 = X.C47892Ym.A0C
            android.graphics.Rect[] r0 = r0.A04
            r18 = r0
        L_0x0172:
            java.lang.String r0 = "url_override_config"
            org.json.JSONArray r13 = r13.getJSONArray(r0)     // Catch:{ JSONException -> 0x0207 }
            int r7 = r13.length()     // Catch:{ JSONException -> 0x0207 }
            java.lang.String[] r11 = new java.lang.String[r7]     // Catch:{ JSONException -> 0x0207 }
            X.DqS[][] r5 = new X.C28196DqS[r7][]     // Catch:{ JSONException -> 0x0207 }
            r4 = 0
        L_0x0181:
            if (r4 >= r7) goto L_0x020f
            org.json.JSONObject r1 = r13.getJSONObject(r4)     // Catch:{ JSONException -> 0x0207 }
            java.lang.String r0 = r1.getString(r10)     // Catch:{ JSONException -> 0x0207 }
            r11[r4] = r0     // Catch:{ JSONException -> 0x0207 }
            org.json.JSONArray r12 = r1.getJSONArray(r14)     // Catch:{ JSONException -> 0x0207 }
            int r8 = r12.length()     // Catch:{ JSONException -> 0x0207 }
            X.DqS[] r0 = new X.C28196DqS[r8]     // Catch:{ JSONException -> 0x0207 }
            r5[r4] = r0     // Catch:{ JSONException -> 0x0207 }
            r3 = 0
        L_0x019a:
            if (r3 >= r8) goto L_0x01de
            X.DqS r2 = new X.DqS     // Catch:{ JSONException -> 0x0207 }
            r2.<init>()     // Catch:{ JSONException -> 0x0207 }
            org.json.JSONObject r9 = r12.getJSONObject(r3)     // Catch:{ JSONException -> 0x0207 }
            double r0 = r9.getDouble(r15)     // Catch:{ JSONException -> 0x0207 }
            float r0 = X.C28141DpL.A03(r0)     // Catch:{ JSONException -> 0x0207 }
            double r0 = (double) r0     // Catch:{ JSONException -> 0x0207 }
            r2.A01 = r0     // Catch:{ JSONException -> 0x0207 }
            r1 = r20
            double r0 = r9.getDouble(r1)     // Catch:{ JSONException -> 0x0207 }
            float r0 = X.C28141DpL.A02(r0)     // Catch:{ JSONException -> 0x0207 }
            double r0 = (double) r0     // Catch:{ JSONException -> 0x0207 }
            r2.A03 = r0     // Catch:{ JSONException -> 0x0207 }
            r1 = r21
            double r0 = r9.getDouble(r1)     // Catch:{ JSONException -> 0x0207 }
            float r0 = X.C28141DpL.A03(r0)     // Catch:{ JSONException -> 0x0207 }
            double r0 = (double) r0     // Catch:{ JSONException -> 0x0207 }
            r2.A02 = r0     // Catch:{ JSONException -> 0x0207 }
            r0 = r22
            double r0 = r9.getDouble(r0)     // Catch:{ JSONException -> 0x0207 }
            float r0 = X.C28141DpL.A02(r0)     // Catch:{ JSONException -> 0x0207 }
            double r0 = (double) r0     // Catch:{ JSONException -> 0x0207 }
            r2.A00 = r0     // Catch:{ JSONException -> 0x0207 }
            r0 = r5[r4]     // Catch:{ JSONException -> 0x0207 }
            r0[r3] = r2     // Catch:{ JSONException -> 0x0207 }
            int r3 = r3 + 1
            goto L_0x019a
        L_0x01de:
            int r4 = r4 + 1
            goto L_0x0181
        L_0x01e1:
            int r1 = r6.A00
            r0 = 3
            if (r1 >= r0) goto L_0x022c
            java.lang.String r7 = "MapConfigUpdateDispatchable"
            r3 = 5000(0x1388, double:2.4703E-320)
            int r1 = X.BYN.A00
            int r0 = r1 + -1
            X.BYN.A00 = r0
            long r1 = (long) r1
            r0 = 32
            long r1 = r1 << r0
            r6.A00 = r1
            r6.A02 = r7
            long r0 = android.os.SystemClock.uptimeMillis()
            long r0 = r0 + r3
            r6.A01 = r0
            X.1xI r0 = X.BYN.A00()
            r0.AMQ(r6)
            return
        L_0x0207:
            X.2Yn r0 = X.C47892Ym.A0C
            java.lang.String[] r11 = r0.A05
            X.2Yn r0 = X.C47892Ym.A0C
            X.DqS[][] r5 = r0.A06
        L_0x020f:
            X.2Yn r22 = new X.2Yn
            r25 = r18
            r26 = r19
            r27 = r11
            r28 = r5
            r22.<init>(r23, r24, r25, r26, r27, r28)
            X.C47892Ym.A0C = r22
            long r0 = android.os.SystemClock.uptimeMillis()
            X.C47892Ym.A00 = r0
            X.DeX r0 = new X.DeX
            r0.<init>()
            X.BYN.A02(r0)
        L_0x022c:
            r0 = 0
            r6.A00 = r0
            java.util.concurrent.Semaphore r0 = X.C47892Ym.A0A
            r0.release()
            return
        L_0x0235:
            r0 = move-exception
            r13 = r3
            goto L_0x0239
        L_0x0238:
            r0 = move-exception
        L_0x0239:
            if (r13 == 0) goto L_0x023e
            r13.close()     // Catch:{ IOException -> 0x023e }
        L_0x023e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38321xF.run():void");
    }

    public void A00() {
        super.A00();
        C47892Ym.A0A.release();
    }
}
