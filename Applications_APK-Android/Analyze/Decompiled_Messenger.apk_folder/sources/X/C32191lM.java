package X;

import com.facebook.common.util.TriState;

/* renamed from: X.1lM  reason: invalid class name and case insensitive filesystem */
public final class C32191lM {
    public TriState A00 = TriState.UNSET;
    public final long A01;
    public final boolean A02;
    public final /* synthetic */ AnonymousClass1GE A03;

    public C32191lM(AnonymousClass1GE r2, long j, boolean z) {
        this.A03 = r2;
        this.A01 = j;
        this.A02 = z;
    }
}
