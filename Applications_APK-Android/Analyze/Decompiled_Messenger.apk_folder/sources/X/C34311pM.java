package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1pM  reason: invalid class name and case insensitive filesystem */
public final class C34311pM {
    private static volatile C34311pM A01;
    public final C04460Ut A00;

    public static final C34311pM A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C34311pM.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C34311pM(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C34311pM(AnonymousClass1XY r2) {
        this.A00 = C04430Uq.A02(r2);
    }
}
