package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.0rC  reason: invalid class name and case insensitive filesystem */
public final class C13300rC implements AnonymousClass06U {
    public final /* synthetic */ C16660xW A00;

    public C13300rC(C16660xW r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(-1199519736);
        C16660xW.A02(this.A00, "connectivity_changed_broadcast");
        AnonymousClass09Y.A01(2093729792, A002);
    }
}
