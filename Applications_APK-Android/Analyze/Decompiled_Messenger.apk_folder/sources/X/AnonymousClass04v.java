package X;

import com.facebook.profilo.ipc.TraceContext;
import java.io.File;

/* renamed from: X.04v  reason: invalid class name */
public abstract class AnonymousClass04v implements AnonymousClass09H {
    public void BTJ() {
    }

    public void Bds(Throwable th) {
    }

    public void BgU() {
    }

    public void BkO() {
    }

    public void BkP(int i) {
    }

    public void Bsa(File file, long j) {
    }

    public void Bsb(int i, int i2, int i3, int i4) {
    }

    public void Btg(File file) {
    }

    public void Bth(File file) {
    }

    public void onTraceAbort(TraceContext traceContext) {
    }

    public void onTraceStart(TraceContext traceContext) {
    }

    public void onTraceStop(TraceContext traceContext) {
    }

    public void onTraceWriteAbort(long j, int i) {
    }

    public void onTraceWriteEnd(long j, int i) {
    }

    public void onTraceWriteStart(long j, int i, String str) {
    }
}
