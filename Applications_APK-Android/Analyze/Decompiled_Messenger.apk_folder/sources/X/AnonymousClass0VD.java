package X;

import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import io.card.payment.BuildConfig;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0VD  reason: invalid class name */
public final class AnonymousClass0VD implements FbSharedPreferences {
    private static volatile AnonymousClass0VD A08;
    public AnonymousClass0UN A00;
    public final AnonymousClass0XI A01;
    public final Queue A02;
    public final ExecutorService A03;
    public final ExecutorService A04;
    private final C24501Tw A05;
    private final Integer A06 = 500;
    private volatile boolean A07;

    public synchronized void AQP() {
        while (!BFQ()) {
            wait();
        }
    }

    /* JADX INFO: finally extract failed */
    public synchronized void BCy() {
        C24501Tw r2 = this.A05;
        synchronized (r2) {
            C005505z.A03("FbSharedPreferencesCache.init", 1208743010);
            try {
                C005505z.A03("FbSharedPreferencesCache.loadInitialValues", 352518331);
                r2.A03.BIW(r2.A08);
                C005505z.A00(-1033573323);
                r2.A0C = true;
                r2.notifyAll();
                C005505z.A00(-277902368);
            } catch (Throwable th) {
                C005505z.A00(-753239182);
                throw th;
            }
        }
        this.A05.A07.add(this);
        this.A05.A06.add(this);
        this.A05.A0B = (long) this.A06.intValue();
        this.A07 = true;
        AnonymousClass07A.A04(this.A03, new C05590Zp(this), 1225607241);
        notifyAll();
    }

    public static final AnonymousClass0VD A00(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass0VD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new AnonymousClass0VD(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public static AnonymousClass1Y7 A01(AnonymousClass0VD r3, AnonymousClass1Y7 r4) {
        boolean BBh;
        if (!(r4 instanceof AnonymousClass1Y8) || !r3.BFQ()) {
            return r4;
        }
        if (!r3.BFQ()) {
            BBh = false;
        } else {
            BBh = r3.BBh(C04350Ue.A04);
        }
        if (!BBh || !((AnonymousClass1YI) AnonymousClass1XX.A03(AnonymousClass1Y3.B50, r3.A00)).AbO(65, false)) {
            return r4;
        }
        return AnonymousClass1Y8.A02((AnonymousClass1Y8) r4, r3.B4F(C04350Ue.A04, BuildConfig.FLAVOR));
    }

    public static void A03(AnonymousClass0VD r6, Map map, Set set, Long l) {
        AnonymousClass38R r0;
        C24501Tw r4 = r6.A05;
        if (!map.isEmpty() || !set.isEmpty()) {
            ArrayList arrayList = new ArrayList(map.size() + set.size());
            synchronized (r4) {
                for (Map.Entry entry : map.entrySet()) {
                    AnonymousClass1Y7 r2 = (AnonymousClass1Y7) entry.getKey();
                    Object value = entry.getValue();
                    if (!Objects.equal(r4.A08.get(r2), value)) {
                        arrayList.add(r2);
                        r4.A08.put(r2, value);
                        C24501Tw.A02(r4, r2);
                        r4.A09.put(r2, value);
                        r4.A05.remove(r2);
                    }
                }
                Iterator it = set.iterator();
                while (it.hasNext()) {
                    AnonymousClass1Y7 r62 = (AnonymousClass1Y7) it.next();
                    if (r4.A08.containsKey(r62)) {
                        arrayList.add(r62);
                        r4.A08.remove(r62);
                        AnonymousClass38R r02 = r4.A00;
                        if (r02 != null) {
                            ArrayList A012 = C24501Tw.A01(r62);
                            ArrayDeque arrayDeque = new ArrayDeque();
                            Iterator it2 = A012.iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    String str = (String) it2.next();
                                    if (!r02.A03.containsKey(str)) {
                                        break;
                                    }
                                    r02 = (AnonymousClass38R) r02.A03.get(str);
                                    arrayDeque.add(r02);
                                } else {
                                    while (!arrayDeque.isEmpty()) {
                                        AnonymousClass38R r22 = (AnonymousClass38R) arrayDeque.pop();
                                        if (r22.A03.isEmpty() && (r0 = r22.A01) != null) {
                                            r0.A03.remove(r22.A02);
                                        }
                                    }
                                }
                            }
                        }
                        r4.A05.add(r62);
                        r4.A09.remove(r62);
                    }
                }
            }
            C24501Tw.A03(r4, l);
            for (AnonymousClass0VD r3 : r4.A07) {
                AnonymousClass0XI r23 = r3.A01;
                ExecutorService executorService = r3.A04;
                r23.A00.A04(arrayList, r3, executorService);
                r23.A01.A04(arrayList, r3, executorService);
            }
        }
    }

    public void ASu(Set set) {
        C005505z.A03("FbSharedPreferencesImpl.clearPreferencesSet", 418357592);
        try {
            C30281hn edit = edit();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                edit.C24((AnonymousClass1Y7) it.next());
            }
            edit.commit();
        } finally {
            C005505z.A00(1145054567);
        }
    }

    public boolean BFQ() {
        return this.A07;
    }

    public void C0f(AnonymousClass1Y7 r3, AnonymousClass1ZH r4) {
        this.A01.A00(r3, r4);
        if (r3 instanceof AnonymousClass1Y8) {
            this.A01.A00(A01(this, r3), r4);
        }
    }

    public void C0g(String str, AnonymousClass1ZH r6) {
        AnonymousClass0XI r3 = this.A01;
        if (r6 instanceof AnonymousClass0ZM) {
            r3.A00.A01(new AnonymousClass1Y7(str), (AnonymousClass0ZM) r6);
        }
        if (r6 instanceof C26001ak) {
            r3.A02.A01(new AnonymousClass1Y7(str), (C26001ak) r6);
        }
    }

    public void C0h(Set set, AnonymousClass1ZH r7) {
        AnonymousClass0XI r4 = this.A01;
        synchronized (r4) {
            Iterator it = set.iterator();
            while (it.hasNext()) {
                AnonymousClass1Y7 r2 = (AnonymousClass1Y7) it.next();
                if (r7 instanceof AnonymousClass0ZM) {
                    r4.A00.A01(r2, (AnonymousClass0ZM) r7);
                }
                if (r7 instanceof C26001ak) {
                    r4.A02.A01(r2, (C26001ak) r7);
                }
            }
        }
        Iterator it2 = set.iterator();
        while (it2.hasNext()) {
            AnonymousClass1Y7 r22 = (AnonymousClass1Y7) it2.next();
            if (r22 instanceof AnonymousClass1Y8) {
                this.A01.A00(A01(this, r22), r7);
            }
        }
    }

    public void C0i(AnonymousClass1Y7 r3, AnonymousClass1ZH r4) {
        this.A01.A01(r3, r4);
        if (r3 instanceof AnonymousClass1Y8) {
            this.A01.A01(A01(this, r3), r4);
        }
    }

    public void CJr(AnonymousClass1Y7 r3, AnonymousClass1ZH r4) {
        this.A01.A02(r3, r4);
        if (r3 instanceof AnonymousClass1Y8) {
            this.A01.A02(A01(this, r3), r4);
        }
    }

    public void CJs(Set set, AnonymousClass1ZH r7) {
        AnonymousClass0XI r4 = this.A01;
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AnonymousClass1Y7 r2 = (AnonymousClass1Y7) it.next();
            if (r7 instanceof AnonymousClass0ZM) {
                r4.A00.A02(r2, (AnonymousClass0ZM) r7);
            }
            if (r7 instanceof C26001ak) {
                r4.A02.A02(r2, (C26001ak) r7);
            }
        }
        Iterator it2 = set.iterator();
        while (it2.hasNext()) {
            AnonymousClass1Y7 r22 = (AnonymousClass1Y7) it2.next();
            if (r22 instanceof AnonymousClass1Y8) {
                this.A01.A02(A01(this, r22), r7);
            }
        }
    }

    public void CJt(AnonymousClass1Y7 r3, AnonymousClass1ZH r4) {
        this.A01.A03(r3, r4);
        if (r3 instanceof AnonymousClass1Y8) {
            this.A01.A03(A01(this, r3), r4);
        }
    }

    public C30281hn edit() {
        return new C186914s(this);
    }

    private AnonymousClass0VD(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A04 = AnonymousClass0UX.A0X(r3);
        this.A03 = AnonymousClass0UX.A0a(r3);
        this.A05 = C24501Tw.A00(r3);
        C005505z.A03("FbSharedPreferences.ctor", -1131453781);
        try {
            this.A01 = new AnonymousClass0XI();
            this.A02 = new ConcurrentLinkedQueue();
        } finally {
            C005505z.A00(930856420);
        }
    }

    private SortedMap A02(AnonymousClass1Y7 r12) {
        SortedMap sortedMap;
        boolean BBh;
        AnonymousClass1Y8 A002;
        AnonymousClass1Y7 A012 = A01(this, r12);
        C24501Tw r5 = this.A05;
        synchronized (r5) {
            synchronized (r5) {
                Preconditions.checkState(r5.A0C, "FbSharedPreferencesCache used before initialized");
            }
            AnonymousClass38R r10 = r5.A00;
            if (r10 != null) {
                Map map = r5.A08;
                ArrayList A013 = C24501Tw.A01(A012);
                Stack stack = new Stack();
                int i = 0;
                while (true) {
                    if (i < A013.size()) {
                        String str = (String) A013.get(i);
                        boolean z = false;
                        if (i == A013.size() - 1) {
                            z = true;
                        }
                        if (!r10.A03.containsKey(str)) {
                            if (!z) {
                                sortedMap = AnonymousClass07Z.A00;
                                break;
                            }
                            boolean z2 = false;
                            for (String str2 : r10.A03.keySet()) {
                                if (str2.startsWith(str)) {
                                    stack.push(r10.A03.get(str2));
                                    z2 = true;
                                }
                            }
                            if (!z2) {
                                sortedMap = AnonymousClass07Z.A00;
                                break;
                            }
                        } else {
                            r10 = (AnonymousClass38R) r10.A03.get(str);
                            if (z) {
                                stack.push(r10);
                            }
                        }
                        i++;
                    } else if (stack.isEmpty()) {
                        sortedMap = AnonymousClass07Z.A00;
                    } else {
                        sortedMap = new TreeMap();
                        while (!stack.isEmpty()) {
                            AnonymousClass38R r2 = (AnonymousClass38R) stack.pop();
                            AnonymousClass1Y7 r1 = r2.A00;
                            if (r1 != null) {
                                sortedMap.put(r1, map.get(r1));
                            }
                            stack.addAll(r2.A03.values());
                        }
                    }
                }
            } else {
                sortedMap = null;
                for (Map.Entry entry : r5.A08.entrySet()) {
                    if (((AnonymousClass1Y7) entry.getKey()).A07(A012)) {
                        if (sortedMap == null) {
                            sortedMap = new TreeMap();
                        }
                        sortedMap.put(entry.getKey(), entry.getValue());
                    }
                }
                if (sortedMap == null) {
                    sortedMap = AnonymousClass07Z.A00;
                }
            }
        }
        if (BFQ()) {
            if (!BFQ()) {
                BBh = false;
            } else {
                BBh = BBh(C04350Ue.A04);
            }
            if (BBh && ((AnonymousClass1YI) AnonymousClass1XX.A03(AnonymousClass1Y3.B50, this.A00)).AbO(65, false)) {
                String B4F = B4F(C04350Ue.A04, BuildConfig.FLAVOR);
                for (AnonymousClass1Y7 r52 : ImmutableSet.A0A(sortedMap.keySet())) {
                    if (r52.A05().startsWith(AnonymousClass08S.A0J("/", B4F))) {
                        Object remove = sortedMap.remove(r52);
                        if (!r52.A05().startsWith(AnonymousClass08S.A0J("/", B4F))) {
                            A002 = AnonymousClass1Y8.A01(r52, false);
                        } else {
                            A002 = AnonymousClass1Y8.A00(r52, B4F);
                        }
                        sortedMap.put(A002, remove);
                    }
                }
            }
        }
        return sortedMap;
    }

    public boolean Aep(AnonymousClass1Y7 r3, boolean z) {
        Boolean bool = (Boolean) this.A05.A04(A01(this, r3));
        if (bool != null) {
            return bool.booleanValue();
        }
        return z;
    }

    public TriState Aeq(AnonymousClass1Y7 r3) {
        Boolean bool = (Boolean) this.A05.A04(A01(this, r3));
        if (bool != null) {
            return TriState.valueOf(bool);
        }
        return TriState.UNSET;
    }

    public double Akk(AnonymousClass1Y7 r3, double d) {
        Double d2 = (Double) this.A05.A04(A01(this, r3));
        if (d2 != null) {
            return d2.doubleValue();
        }
        return d;
    }

    public SortedMap AlW(AnonymousClass1Y7 r2) {
        return A02(r2);
    }

    public float AnA(AnonymousClass1Y7 r3, float f) {
        Float f2 = (Float) this.A05.A04(A01(this, r3));
        if (f2 != null) {
            return f2.floatValue();
        }
        return f;
    }

    public int AqN(AnonymousClass1Y7 r3, int i) {
        Integer num = (Integer) this.A05.A04(A01(this, r3));
        if (num != null) {
            return num.intValue();
        }
        return i;
    }

    public Set Arq(AnonymousClass1Y7 r2) {
        return A02(r2).keySet();
    }

    public long At2(AnonymousClass1Y7 r3, long j) {
        Long l = (Long) this.A05.A04(A01(this, r3));
        if (l != null) {
            return l.longValue();
        }
        return j;
    }

    public String B4F(AnonymousClass1Y7 r3, String str) {
        String str2 = (String) this.A05.A04(A01(this, r3));
        if (str2 == null) {
            return str;
        }
        return str2;
    }

    public Set B87(AnonymousClass1Y8 r5) {
        AnonymousClass1Y8 A012;
        Set<AnonymousClass1Y7> keySet = A02(r5).keySet();
        TreeSet treeSet = new TreeSet();
        for (AnonymousClass1Y7 r1 : keySet) {
            if (r1 instanceof AnonymousClass1Y8) {
                A012 = (AnonymousClass1Y8) r1;
            } else {
                A012 = AnonymousClass1Y8.A01(r1, false);
            }
            treeSet.add(A012);
        }
        return treeSet;
    }

    public Object B8F(AnonymousClass1Y7 r3) {
        return this.A05.A04(A01(this, r3));
    }

    public boolean BBh(AnonymousClass1Y7 r6) {
        boolean containsKey;
        AnonymousClass1Y7 A012 = A01(this, r6);
        C24501Tw r3 = this.A05;
        synchronized (r3) {
            synchronized (r3) {
                Preconditions.checkState(r3.A0C, "FbSharedPreferencesCache used before initialized");
            }
            containsKey = r3.A08.containsKey(A012);
        }
        return containsKey;
    }

    public void C0e(Runnable runnable) {
        if (BFQ()) {
            AnonymousClass07A.A04(this.A03, runnable, -1063282296);
        } else {
            this.A02.offer(runnable);
        }
    }
}
