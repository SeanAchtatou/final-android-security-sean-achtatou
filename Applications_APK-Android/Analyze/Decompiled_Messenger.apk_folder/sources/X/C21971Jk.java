package X;

import android.content.Context;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount;

/* renamed from: X.1Jk  reason: invalid class name and case insensitive filesystem */
public final class C21971Jk extends C17770zR {
    public static final CallerContext A06 = CallerContext.A09("AccountRegSoftmatchComponentSpec");
    @Comparable(type = 13)
    public C25998CqM A00;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public C204049jg A02;
    @Comparable(type = 13)
    public C121605oW A03;
    @Comparable(type = 13)
    public RecoveredAccount A04;
    public C04310Tq A05;

    public C21971Jk(Context context) {
        super("AccountRegSoftmatchComponent");
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A01 = new AnonymousClass0UN(1, r2);
        this.A05 = C61832zZ.A02(r2);
    }
}
