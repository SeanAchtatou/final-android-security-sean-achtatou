package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.1gM  reason: invalid class name and case insensitive filesystem */
public final class C29401gM {
    public final Context A00;
    public final C001300x A01;
    private final C04310Tq A02;

    public static final C29401gM A00(AnonymousClass1XY r1) {
        return new C29401gM(r1);
    }

    public static String A02(C29401gM r2) {
        String str = (String) r2.A02.get();
        if (str != null && !str.equals("0")) {
            return AnonymousClass08S.A0P("omnistore_", str, "_v01");
        }
        throw new AnonymousClass3LY();
    }

    public void A03() {
        this.A00.deleteDatabase(A01(this));
        this.A00.deleteDatabase(AnonymousClass08S.A0J(A02(this), "_status.dat"));
        File[] listFiles = this.A00.getDir("omnistore", 0).listFiles();
        if (listFiles == null) {
            listFiles = new File[0];
        }
        for (File delete : listFiles) {
            delete.delete();
        }
    }

    public C29401gM(AnonymousClass1XY r2) {
        this.A02 = C10580kT.A04(r2);
        this.A00 = AnonymousClass1YA.A00(r2);
        this.A01 = AnonymousClass0UU.A02(r2);
    }

    public static String A01(C29401gM r1) {
        return AnonymousClass08S.A0J(A02(r1), ".db");
    }
}
