package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.ArrayList;
import java.util.HashMap;

@UserScoped
/* renamed from: X.1Ae  reason: invalid class name and case insensitive filesystem */
public final class C19911Ae {
    private static C05540Zi A02;
    public final C17610zB A00 = new C17610zB();
    public final C17610zB A01 = new C17610zB();

    public static final C19911Ae A00(AnonymousClass1XY r3) {
        C19911Ae r0;
        synchronized (C19911Ae.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r3)) {
                    A02.A01();
                    A02.A00 = new C19911Ae();
                }
                C05540Zi r1 = A02;
                r0 = (C19911Ae) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    private static ArrayList A01(C17610zB r3) {
        if (r3 == null) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList(r3.A01());
        for (int i = 0; i < r3.A01(); i++) {
            arrayList.add(r3.A06(i));
        }
        return arrayList;
    }

    public String A02(boolean z) {
        int i;
        int i2;
        int i3;
        ArrayList A012;
        C17610zB r1 = this.A00;
        int A013 = r1.A01();
        if (z) {
            i = this.A01.A01();
        } else {
            i = 0;
        }
        int i4 = A013 + i;
        if (i4 <= 0) {
            this.A01.A09();
            return "{}";
        }
        ArrayList A014 = A01(r1);
        if (z && (A012 = A01(this.A01)) != null && !A012.isEmpty()) {
            A014.addAll(A012);
        }
        HashMap hashMap = new HashMap();
        int size = A014.size();
        for (int i5 = 0; i5 < size; i5++) {
            C33901oK r2 = (C33901oK) A014.get(i5);
            if (hashMap.containsKey(r2)) {
                Integer num = (Integer) hashMap.get(r2);
                if (num != null) {
                    i3 = num.intValue();
                } else {
                    i3 = 0;
                }
                hashMap.put(r2, Integer.valueOf(i3 + 1));
            } else {
                hashMap.put(r2, 1);
            }
        }
        StringBuilder sb = new StringBuilder(i4 * 50);
        sb.append('{');
        r1 = null;
        for (C33901oK r12 : hashMap.keySet()) {
            if (r12 != null) {
                sb.append(", ");
            }
            sb.append(r12);
            sb.append('=');
            Integer num2 = (Integer) hashMap.get(r12);
            if (num2 != null) {
                i2 = num2.intValue();
            } else {
                i2 = 0;
            }
            sb.append(i2);
        }
        sb.append('}');
        this.A01.A09();
        return sb.toString();
    }
}
