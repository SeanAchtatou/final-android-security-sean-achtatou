package X;

/* renamed from: X.1Yv  reason: invalid class name and case insensitive filesystem */
public final class C25231Yv {
    public static volatile AnonymousClass1XU A00;

    public static boolean A01() {
        AnonymousClass1XU r0 = A00;
        if (r0 == null || !r0.isEnabled()) {
            return false;
        }
        return true;
    }

    public static Runnable A00(String str, Runnable runnable, int i) {
        if (!A01()) {
            return runnable;
        }
        return new AnonymousClass8t1(str, runnable, i);
    }
}
