package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.1OD  reason: invalid class name */
public final class AnonymousClass1OD implements C22761Ms {
    private AnonymousClass0UN A00;

    public static final AnonymousClass1OD A00(AnonymousClass1XY r1) {
        return new AnonymousClass1OD(r1);
    }

    public AnonymousClass1OD(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public AnonymousClass1S9 AWg(AnonymousClass1NY r5, int i, C33271nJ r7, C23541Px r8) {
        Drawable drawable;
        InputStream A09 = r5.A09();
        try {
            C03420Nr r0 = new C03420Nr(A09.available());
            AnonymousClass0h4.A00(A09, r0);
            String str = new String(r0.toByteArray());
            if (!str.startsWith("CD_") || (drawable = ((Resources) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AEg, this.A00)).getDrawable(Integer.parseInt(str.substring(3)))) == null) {
                return null;
            }
            return new C74693iK(drawable);
        } catch (IOException unused) {
            return null;
        }
    }
}
