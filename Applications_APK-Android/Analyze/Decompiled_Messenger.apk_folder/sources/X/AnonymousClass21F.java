package X;

import android.content.Context;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import com.google.common.base.Preconditions;

/* renamed from: X.21F  reason: invalid class name */
public final class AnonymousClass21F {
    private static final AnonymousClass3E8 A0H = AnonymousClass3E8.A01(40.0d, 7.0d);
    public float A00;
    public float A01;
    public float A02 = 2.0f;
    public C64873Dz A03;
    public C15760vw A04;
    public AnonymousClass4TN A05;
    public Float A06;
    public Integer A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public final float A0B;
    public final int A0C;
    public final VelocityTracker A0D;
    public final View A0E;
    public final View A0F;
    public final AnonymousClass3E6 A0G;

    public static void A00(AnonymousClass21F r4) {
        r4.A0A = true;
        AnonymousClass4TN r3 = r4.A05;
        if (r3 != null) {
            r4.A0D.computeCurrentVelocity(AnonymousClass1Y3.A87);
            float yVelocity = r4.A0D.getYVelocity();
            if (r4.A07 == AnonymousClass07B.A00) {
                yVelocity = -yVelocity;
            }
            r3.BX9((double) (yVelocity / r4.A02));
        }
    }

    public static void A01(AnonymousClass21F r1, MotionEvent motionEvent) {
        r1.A09 = false;
        r1.A01 = motionEvent.getRawY();
        r1.A00 = motionEvent.getRawX();
        r1.A0D.clear();
        r1.A0D.addMovement(motionEvent);
    }

    public void A03() {
        this.A09 = false;
        this.A0A = true;
        int i = 0;
        if (this.A07 == AnonymousClass07B.A00) {
            i = -0;
        }
        AnonymousClass3E6 r2 = this.A0G;
        r2.A04((double) i);
        r2.A03();
    }

    public static boolean A02(AnonymousClass21F r1) {
        if (Math.abs((int) r1.A0F.getTranslationY()) != 0) {
            return true;
        }
        return false;
    }

    public void A04(float f) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(this.A0F.getContext());
        float scaledMinimumFlingVelocity = (float) viewConfiguration.getScaledMinimumFlingVelocity();
        this.A06 = Float.valueOf(scaledMinimumFlingVelocity + ((((float) viewConfiguration.getScaledMaximumFlingVelocity()) - scaledMinimumFlingVelocity) * AnonymousClass0O6.A00(f, 0.0f, 1.0f)));
    }

    public void A05(float f, double d) {
        AnonymousClass3E6 r2 = this.A0G;
        if (this.A07 == AnonymousClass07B.A00) {
            d = -d;
        }
        r2.A06(d);
        r2.A04((double) this.A0F.getTranslationY());
        r2.A05((double) f);
    }

    public AnonymousClass21F(View view, View view2, Integer num) {
        Preconditions.checkNotNull(view);
        this.A0F = view;
        Preconditions.checkNotNull(view2);
        this.A0E = view2;
        Preconditions.checkNotNull(num);
        this.A07 = num;
        Context context = view.getContext();
        C64873Dz A002 = C64873Dz.A00(AnonymousClass1XX.get(view.getContext()));
        this.A03 = A002;
        AnonymousClass3E6 A072 = A002.A07();
        A072.A07(A0H);
        A072.A07 = true;
        A072.A08(new AnonymousClass4TL(this));
        this.A0G = A072;
        this.A0D = VelocityTracker.obtain();
        this.A0B = (float) context.getResources().getDimensionPixelSize(2132148259);
        this.A0C = ViewConfiguration.get(context).getScaledTouchSlop();
    }
}
