package X;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.facebook.litho.ComponentHost;

/* renamed from: X.0z5  reason: invalid class name and case insensitive filesystem */
public final class C17550z5 {
    public int A00;
    public int A01;
    public Canvas A02;
    public final /* synthetic */ ComponentHost A03;

    public C17550z5(ComponentHost componentHost) {
        this.A03 = componentHost;
    }

    public static void A00(C17550z5 r7) {
        int i;
        if (r7.A02 != null) {
            C07870eJ r0 = r7.A03.A02;
            if (r0 == null) {
                i = 0;
            } else {
                i = r0.A01();
            }
            for (int i2 = r7.A00; i2 < i; i2++) {
                C17830zY r4 = (C17830zY) r7.A03.A02.A05(i2);
                Object A002 = r4.A00();
                if (A002 instanceof View) {
                    r7.A00 = i2 + 1;
                    return;
                }
                if (r4.A09) {
                    boolean A022 = C27041cY.A02();
                    if (A022) {
                        C27041cY.A01(AnonymousClass08S.A0J("draw: ", r4.A04.A1A()));
                    }
                    ((Drawable) A002).draw(r7.A02);
                    if (A022) {
                        C27041cY.A00();
                    }
                }
            }
            r7.A00 = r7.A01;
        }
    }
}
