package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0TX  reason: invalid class name */
public final class AnonymousClass0TX {
    public static Map A00(Set set, Set set2) {
        HashMap hashMap = new HashMap();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            hashMap.put((AnonymousClass0TO) it.next(), Collections.unmodifiableSet(set2));
        }
        return Collections.unmodifiableMap(hashMap);
    }
}
