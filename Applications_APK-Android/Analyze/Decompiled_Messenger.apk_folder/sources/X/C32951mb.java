package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.auth.credentials.UserTokenCredentials;

/* renamed from: X.1mb  reason: invalid class name and case insensitive filesystem */
public final class C32951mb implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new UserTokenCredentials(parcel.readString(), parcel.readString());
    }

    public Object[] newArray(int i) {
        return new UserTokenCredentials[i];
    }
}
