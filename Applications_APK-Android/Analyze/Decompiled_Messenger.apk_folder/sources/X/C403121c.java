package X;

import com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo;
import com.google.common.collect.ImmutableList;

/* renamed from: X.21c  reason: invalid class name and case insensitive filesystem */
public final class C403121c extends C33541nk {
    public AnonymousClass0UN A00;
    private final C168517pz A01 = new C169157r4(this);
    private final C167237nm A02 = new C169167r5(this);
    private final C34951qR A03 = new C169147r3(this);
    private final C17040yE A04 = new C169177r6(this);

    private C403121c(AnonymousClass1XY r3) {
        super("ChicletParticipantsPresenter");
        this.A00 = new AnonymousClass0UN(6, r3);
    }

    public static final C403121c A00(AnonymousClass1XY r1) {
        return new C403121c(r1);
    }

    public void A0L() {
        ((C167647oX) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BLg, this.A00)).A05(this.A02);
        ((C32621m3) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ayw, this.A00)).A0L(this.A04);
        ((C34991qV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BEY, this.A00)).A01.remove(this.A03);
        ((C168507py) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMn, this.A00)).A05(this.A01);
    }

    public void A0M() {
        ((C167647oX) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BLg, this.A00)).A04(this.A02);
        ((C32621m3) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ayw, this.A00)).A0K(this.A04);
        ((C34991qV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BEY, this.A00)).A01.add(this.A03);
        ((C168507py) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMn, this.A00)).A04(this.A01);
        A02(this);
    }

    public static C169207r9 A01(C403121c r1) {
        if (r1.A0G().isPresent()) {
            return new C169207r9((C169197r8) r1.A0G().get());
        }
        return new C169207r9();
    }

    public static void A02(C403121c r6) {
        C169207r9 A012 = A01(r6);
        ImmutableList.Builder builder = ImmutableList.builder();
        C24971Xv it = ((C32621m3) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ayw, r6.A00)).A0H().iterator();
        while (it.hasNext()) {
            builder.add((Object) ((FbWebrtcConferenceParticipantInfo) it.next()).A02());
        }
        ImmutableList build = builder.build();
        A012.A01 = build;
        C28931fb.A06(build, AnonymousClass80H.$const$string(437));
        int i = AnonymousClass1Y3.BEY;
        AnonymousClass0UN r2 = r6.A00;
        boolean z = false;
        if (!((C34991qV) AnonymousClass1XX.A02(2, i, r2)).A02() && ((C167647oX) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BLg, r2)).A01 == 4 && ((AnonymousClass2DK) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B9P, r2)).A01(false)) {
            int i2 = AnonymousClass1Y3.BMn;
            AnonymousClass0UN r1 = r6.A00;
            if (((C168507py) AnonymousClass1XX.A02(5, i2, r1)).A07() && ((C165907lc) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A4C, r1)).A0C()) {
                z = true;
            }
        }
        A012.A02 = z;
        r6.A0K(new C169197r8(A012));
    }
}
