package X;

import androidx.fragment.app.Fragment;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.io.PrintWriter;
import java.util.ArrayList;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0wn  reason: invalid class name and case insensitive filesystem */
public final class C16280wn extends C16290wo implements C16310wq, C29461gS {
    public int A00;
    public boolean A01;
    public final C13060qW A02;

    public boolean A0R(ArrayList arrayList, int i, int i2) {
        int i3;
        int i4;
        if (i2 != i) {
            int size = this.A0C.size();
            int i5 = -1;
            for (int i6 = 0; i6 < size; i6++) {
                Fragment fragment = ((C29471gT) this.A0C.get(i6)).A05;
                if (fragment != null) {
                    i3 = fragment.A0D;
                } else {
                    i3 = 0;
                }
                if (!(i3 == 0 || i3 == i5)) {
                    for (int i7 = i; i7 < i2; i7++) {
                        C16280wn r3 = (C16280wn) arrayList.get(i7);
                        int size2 = r3.A0C.size();
                        for (int i8 = 0; i8 < size2; i8++) {
                            Fragment fragment2 = ((C29471gT) r3.A0C.get(i8)).A05;
                            if (fragment2 != null) {
                                i4 = fragment2.A0D;
                            } else {
                                i4 = 0;
                            }
                            if (i4 == i3) {
                                return true;
                            }
                        }
                    }
                    i5 = i3;
                }
            }
        }
        return false;
    }

    public boolean Aap(ArrayList arrayList, ArrayList arrayList2) {
        C13060qW.A0I(2);
        arrayList.add(this);
        arrayList2.add(false);
        if (!this.A05) {
            return true;
        }
        C13060qW r1 = this.A02;
        if (r1.A09 == null) {
            r1.A09 = new ArrayList();
        }
        r1.A09.add(this);
        return true;
    }

    public static int A00(C16280wn r3, boolean z) {
        if (!r3.A01) {
            if (C13060qW.A0I(2)) {
                PrintWriter printWriter = new PrintWriter(new AnonymousClass8Q2());
                r3.A0O("  ", printWriter, true);
                printWriter.close();
            }
            r3.A01 = true;
            if (r3.A05) {
                r3.A00 = r3.A02.A0S.getAndIncrement();
            } else {
                r3.A00 = -1;
            }
            r3.A02.A0w(r3, z);
            return r3.A00;
        }
        throw new IllegalStateException("commit already called");
    }

    public static boolean A01(C29471gT r1) {
        Fragment fragment = r1.A05;
        if (fragment == null || !fragment.A0W || fragment.A0I == null || fragment.A0Y || fragment.A0b || 0 == 0) {
            return false;
        }
        return true;
    }

    public C16290wo A0G(Fragment fragment) {
        C13060qW r1 = fragment.A0P;
        if (r1 == null || r1 == this.A02) {
            super.A0G(fragment);
            return this;
        }
        throw new IllegalStateException(AnonymousClass08S.A0P("Cannot detach Fragment attached to a different FragmentManager. Fragment ", fragment.toString(), " is already attached to a FragmentManager."));
    }

    public C16290wo A0H(Fragment fragment) {
        C13060qW r1 = fragment.A0P;
        if (r1 == null || r1 == this.A02) {
            super.A0H(fragment);
            return this;
        }
        throw new IllegalStateException(AnonymousClass08S.A0P("Cannot hide Fragment attached to a different FragmentManager. Fragment ", fragment.toString(), " is already attached to a FragmentManager."));
    }

    public C16290wo A0I(Fragment fragment) {
        C13060qW r1 = fragment.A0P;
        if (r1 == null || r1 == this.A02) {
            super.A0I(fragment);
            return this;
        }
        throw new IllegalStateException(AnonymousClass08S.A0P("Cannot remove Fragment attached to a different FragmentManager. Fragment ", fragment.toString(), " is already attached to a FragmentManager."));
    }

    public C16290wo A0J(Fragment fragment) {
        C13060qW r1 = fragment.A0P;
        if (r1 == null || r1 == this.A02) {
            super.A0J(fragment);
            return this;
        }
        throw new IllegalStateException(AnonymousClass08S.A0P("Cannot show Fragment attached to a different FragmentManager. Fragment ", fragment.toString(), " is already attached to a FragmentManager."));
    }

    public C16290wo A0K(Fragment fragment, AnonymousClass1XL r6) {
        C13060qW r0 = fragment.A0P;
        C13060qW r3 = this.A02;
        if (r0 == r3) {
            boolean z = false;
            if (r6.compareTo((Enum) AnonymousClass1XL.CREATED) >= 0) {
                z = true;
            }
            if (z) {
                super.A0K(fragment, r6);
                return this;
            }
            throw new IllegalArgumentException("Cannot set maximum Lifecycle below " + AnonymousClass1XL.CREATED);
        }
        throw new IllegalArgumentException("Cannot setMaxLifecycle for Fragment not attached to FragmentManager " + r3);
    }

    public void A0M() {
        int size = this.A0C.size();
        for (int i = 0; i < size; i++) {
            C29471gT r7 = (C29471gT) this.A0C.get(i);
            Fragment fragment = r7.A05;
            if (fragment != null) {
                int i2 = this.A0B;
                if (!(fragment.A0K == null && i2 == 0)) {
                    Fragment.A01(fragment);
                    fragment.A0K.A02 = i2;
                }
            }
            int i3 = r7.A00;
            switch (i3) {
                case 1:
                    fragment.A1J(r7.A01);
                    this.A02.A0u(fragment, false);
                    this.A02.A0h(fragment);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown cmd: ", i3));
                case 3:
                    fragment.A1J(r7.A02);
                    this.A02.A0p(fragment);
                    break;
                case 4:
                    fragment.A1J(r7.A02);
                    this.A02.A0l(fragment);
                    break;
                case 5:
                    fragment.A1J(r7.A01);
                    this.A02.A0u(fragment, false);
                    C13060qW.A0I(2);
                    if (fragment.A0b) {
                        fragment.A0b = false;
                        fragment.A0c = !fragment.A0c;
                        break;
                    }
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    fragment.A1J(r7.A02);
                    this.A02.A0k(fragment);
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    fragment.A1J(r7.A01);
                    this.A02.A0u(fragment, false);
                    this.A02.A0j(fragment);
                    break;
                case 8:
                    this.A02.A0r(fragment);
                    break;
                case Process.SIGKILL:
                    this.A02.A0r(null);
                    break;
                case AnonymousClass1Y3.A01 /*10*/:
                    this.A02.A0t(fragment, r7.A06);
                    break;
            }
            if (!(this.A0F || r7.A00 == 1 || fragment == null)) {
                this.A02.A0n(fragment);
            }
        }
        if (!this.A0F) {
            C13060qW r1 = this.A02;
            r1.A0b(r1.A01, true);
        }
    }

    public void A0N(int i) {
        if (this.A05) {
            C13060qW.A0I(2);
            int size = this.A0C.size();
            for (int i2 = 0; i2 < size; i2++) {
                Fragment fragment = ((C29471gT) this.A0C.get(i2)).A05;
                if (fragment != null) {
                    fragment.A0C += i;
                    C13060qW.A0I(2);
                }
            }
        }
    }

    public void A0O(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.A04);
            printWriter.print(" mIndex=");
            printWriter.print(this.A00);
            printWriter.print(" mCommitted=");
            printWriter.println(this.A01);
            if (this.A0B != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.A0B));
            }
            if (!(this.A07 == 0 && this.A08 == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.A07));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.A08));
            }
            if (!(this.A09 == 0 && this.A0A == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.A09));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.A0A));
            }
            if (!(this.A01 == 0 && this.A03 == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.A01));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.A03);
            }
            if (!(this.A00 == 0 && this.A02 == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.A00));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.A02);
            }
        }
        if (!this.A0C.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            int size = this.A0C.size();
            for (int i = 0; i < size; i++) {
                C29471gT r3 = (C29471gT) this.A0C.get(i);
                int i2 = r3.A00;
                switch (i2) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                        str2 = "DETACH";
                        break;
                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case Process.SIGKILL:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    case AnonymousClass1Y3.A01 /*10*/:
                        str2 = "OP_SET_MAX_LIFECYCLE";
                        break;
                    default:
                        str2 = AnonymousClass08S.A09("cmd=", i2);
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(r3.A05);
                if (z) {
                    if (!(r3.A01 == 0 && r3.A02 == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(r3.A01));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(r3.A02));
                    }
                    if (r3.A03 != 0 || r3.A04 != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(r3.A03));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(r3.A04));
                    }
                }
            }
        }
    }

    public void A0P(boolean z) {
        for (int size = this.A0C.size() - 1; size >= 0; size--) {
            C29471gT r5 = (C29471gT) this.A0C.get(size);
            Fragment fragment = r5.A05;
            if (fragment != null) {
                int i = this.A0B;
                int i2 = AnonymousClass1Y3.B7M;
                if (i != 4097) {
                    if (i != 4099) {
                        i2 = AnonymousClass1Y3.AXr;
                        if (i != 8194) {
                            i2 = 0;
                        }
                    } else {
                        i2 = 4099;
                    }
                }
                if (!(fragment.A0K == null && i2 == 0)) {
                    Fragment.A01(fragment);
                    fragment.A0K.A02 = i2;
                }
            }
            int i3 = r5.A00;
            switch (i3) {
                case 1:
                    fragment.A1J(r5.A04);
                    this.A02.A0u(fragment, true);
                    this.A02.A0p(fragment);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown cmd: ", i3));
                case 3:
                    fragment.A1J(r5.A03);
                    this.A02.A0h(fragment);
                    break;
                case 4:
                    fragment.A1J(r5.A03);
                    C13060qW.A0I(2);
                    if (fragment.A0b) {
                        fragment.A0b = false;
                        fragment.A0c = !fragment.A0c;
                        break;
                    }
                    break;
                case 5:
                    fragment.A1J(r5.A04);
                    this.A02.A0u(fragment, true);
                    this.A02.A0l(fragment);
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    fragment.A1J(r5.A03);
                    this.A02.A0j(fragment);
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    fragment.A1J(r5.A04);
                    this.A02.A0u(fragment, true);
                    this.A02.A0k(fragment);
                    break;
                case 8:
                    this.A02.A0r(null);
                    break;
                case Process.SIGKILL:
                    this.A02.A0r(fragment);
                    break;
                case AnonymousClass1Y3.A01 /*10*/:
                    this.A02.A0t(fragment, r5.A07);
                    break;
            }
            if (!(this.A0F || r5.A00 == 3 || fragment == null)) {
                this.A02.A0n(fragment);
            }
        }
        if (!this.A0F && z) {
            C13060qW r1 = this.A02;
            r1.A0b(r1.A01, true);
        }
    }

    public boolean A0Q(int i) {
        int i2;
        int size = this.A0C.size();
        for (int i3 = 0; i3 < size; i3++) {
            Fragment fragment = ((C29471gT) this.A0C.get(i3)).A05;
            if (fragment != null) {
                i2 = fragment.A0D;
            } else {
                i2 = 0;
            }
            if (i2 != 0 && i2 == i) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return this.A04;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        int i = this.A00;
        if (i >= 0) {
            sb.append(" #");
            sb.append(i);
        }
        String str = this.A04;
        if (str != null) {
            sb.append(" ");
            sb.append(str);
        }
        sb.append("}");
        return sb.toString();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C16280wn(X.C13060qW r3) {
        /*
            r2 = this;
            X.1dm r1 = r3.A0S()
            X.0qR r0 = r3.A06
            if (r0 == 0) goto L_0x0017
            android.content.Context r0 = r0.A01
            java.lang.ClassLoader r0 = r0.getClassLoader()
        L_0x000e:
            r2.<init>(r1, r0)
            r0 = -1
            r2.A00 = r0
            r2.A02 = r3
            return
        L_0x0017:
            r0 = 0
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16280wn.<init>(X.0qW):void");
    }

    public void A0L(int i, Fragment fragment, String str, int i2) {
        super.A0L(i, fragment, str, i2);
        fragment.A0P = this.A02;
    }
}
