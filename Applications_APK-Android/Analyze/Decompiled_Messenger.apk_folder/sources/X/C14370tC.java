package X;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.google.common.base.Preconditions;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0tC  reason: invalid class name and case insensitive filesystem */
public abstract class C14370tC {
    public final List A00 = C04300To.A02(3);
    private final IntentFilter A01;
    private final C15080uh A02;
    private final String A03;

    public static synchronized C51522hB A00(C14370tC r3, Looper looper) {
        synchronized (r3) {
            for (C51522hB r1 : r3.A00) {
                if (r1.A01 == looper) {
                    return r1;
                }
            }
            return null;
        }
    }

    public void A01(BroadcastReceiver broadcastReceiver) {
        ((C33621ns) this).A00.unregisterReceiver(broadcastReceiver);
    }

    public void A02(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter, String str, Handler handler) {
        ((C33621ns) this).A00.registerReceiver(broadcastReceiver, intentFilter, str, handler);
    }

    public synchronized void A03(Object obj) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            C51522hB r1 = (C51522hB) it.next();
            if (r1.A02.remove(obj) && r1.A02.isEmpty()) {
                A01(r1.A00);
                it.remove();
            }
        }
    }

    public synchronized void A04(Object obj, Handler handler) {
        Looper looper;
        Preconditions.checkNotNull(obj);
        if (handler == null) {
            looper = Looper.getMainLooper();
        } else {
            looper = handler.getLooper();
        }
        C51522hB A002 = A00(this, looper);
        if (A002 != null) {
            A002.A02.add(obj);
        } else {
            C51512hA r2 = new C51512hA(this.A02, this, looper);
            this.A00.add(new C51522hB(r2, looper, obj));
            A02(r2, this.A01, this.A03, handler);
        }
    }

    public C14370tC(C15080uh r2, IntentFilter intentFilter, String str) {
        Preconditions.checkNotNull(r2);
        this.A02 = r2;
        Preconditions.checkNotNull(intentFilter);
        this.A01 = intentFilter;
        this.A03 = str;
    }
}
