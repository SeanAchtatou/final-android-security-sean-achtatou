package X;

import android.content.Context;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.08X  reason: invalid class name */
public final class AnonymousClass08X {
    private static final Map A00 = Collections.synchronizedMap(new HashMap());

    public static boolean A07(Context context, String str) {
        if (A00(context, str, 0) != 1) {
            return false;
        }
        return true;
    }

    public static int A00(Context context, String str, int i) {
        return A01(context, str, i, A01(context, "GKBOOTSTRAP_CRASH_DETECT", 0, 0));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:46|(2:48|49)|50|51) */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x007d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x0081 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:50:0x0088 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(android.content.Context r7, java.lang.String r8, int r9, int r10) {
        /*
            if (r7 != 0) goto L_0x0003
            return r9
        L_0x0003:
            java.util.Map r0 = X.AnonymousClass08X.A00
            java.lang.Object r0 = r0.get(r8)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x0012
            int r0 = r0.intValue()
            return r0
        L_0x0012:
            java.io.File r0 = A02(r7, r8)     // Catch:{ IOException -> 0x008a }
            boolean r6 = r0.exists()     // Catch:{ IOException -> 0x008a }
            if (r6 == 0) goto L_0x0021
            java.io.File r5 = A02(r7, r8)     // Catch:{ IOException -> 0x008a }
            goto L_0x0022
        L_0x0021:
            r5 = 0
        L_0x0022:
            java.io.DataInputStream r4 = new java.io.DataInputStream     // Catch:{ all -> 0x0082 }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0082 }
            if (r6 == 0) goto L_0x0036
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x0082 }
            r1.<init>(r5)     // Catch:{ all -> 0x0082 }
        L_0x002d:
            r0 = 8
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0082 }
            r4.<init>(r2)     // Catch:{ all -> 0x0082 }
            goto L_0x003b
        L_0x0036:
            java.io.FileInputStream r1 = r7.openFileInput(r8)     // Catch:{ all -> 0x0082 }
            goto L_0x002d
        L_0x003b:
            int r2 = r4.readInt()     // Catch:{ all -> 0x007b }
            r1 = 1
            r0 = 0
            if (r10 <= 0) goto L_0x0064
            int r0 = r4.readInt()     // Catch:{ EOFException -> 0x0047 }
        L_0x0047:
            if (r0 < r10) goto L_0x0064
            java.lang.String r3 = "GkBootstrap"
            java.lang.String r2 = "Detected crash loop crashCount=%d maxCrashCount=%d on %s"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x007b }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x007b }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0, r5}     // Catch:{ all -> 0x007b }
            X.C010708t.A0P(r3, r2, r0)     // Catch:{ all -> 0x007b }
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x007b }
            java.lang.String r0 = "Crash Count"
            r1.<init>(r0)     // Catch:{ all -> 0x007b }
            throw r1     // Catch:{ all -> 0x007b }
        L_0x0064:
            if (r6 != 0) goto L_0x006a
            A04(r7, r8, r2)     // Catch:{ all -> 0x007b }
            goto L_0x0072
        L_0x006a:
            if (r5 == 0) goto L_0x0072
            if (r10 <= 0) goto L_0x0072
            int r0 = r0 + r1
            A06(r5, r2, r0)     // Catch:{ all -> 0x007b }
        L_0x0072:
            r4.close()     // Catch:{ all -> 0x0082 }
            if (r6 != 0) goto L_0x0089
            r7.deleteFile(r8)     // Catch:{ SecurityException -> 0x0089 }
            goto L_0x0089
        L_0x007b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x007d }
        L_0x007d:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x0081 }
        L_0x0081:
            throw r0     // Catch:{ all -> 0x0082 }
        L_0x0082:
            r0 = move-exception
            if (r6 != 0) goto L_0x0088
            r7.deleteFile(r8)     // Catch:{ SecurityException -> 0x0088 }
        L_0x0088:
            throw r0     // Catch:{ IOException -> 0x008a }
        L_0x0089:
            r9 = r2
        L_0x008a:
            java.util.Map r1 = X.AnonymousClass08X.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)
            r1.put(r8, r0)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass08X.A01(android.content.Context, java.lang.String, int, int):int");
    }

    private static File A02(Context context, String str) {
        return new File(new File(context.getFilesDir(), "GkBootstrap"), str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A03(android.content.Context r2, java.lang.String r3) {
        /*
            if (r2 == 0) goto L_0x0036
            java.io.File r0 = A02(r2, r3)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0011
            java.io.File r1 = A02(r2, r3)
            goto L_0x001a
        L_0x0011:
            java.io.File r1 = new java.io.File
            java.io.File r0 = r2.getFilesDir()
            r1.<init>(r0, r3)
        L_0x001a:
            boolean r0 = r1.delete()     // Catch:{ SecurityException -> 0x0026 }
            if (r0 != 0) goto L_0x0028
            boolean r0 = r1.exists()     // Catch:{ SecurityException -> 0x0026 }
            if (r0 == 0) goto L_0x0028
        L_0x0026:
            r0 = 1
            goto L_0x0029
        L_0x0028:
            r0 = 0
        L_0x0029:
            if (r0 == 0) goto L_0x0036
            java.lang.Object[] r2 = new java.lang.Object[]{r3}
            java.lang.String r1 = "GkBootstrap"
            java.lang.String r0 = "Unable to clean up GK file %s"
            X.C010708t.A0P(r1, r0, r2)
        L_0x0036:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass08X.A03(android.content.Context, java.lang.String):void");
    }

    public static void A04(Context context, String str, int i) {
        if (context != null) {
            try {
                File file = new File(context.getFilesDir(), "GkBootstrap");
                if (file.exists() || file.mkdir()) {
                    A06(A02(context, str), i, 0);
                }
            } catch (SecurityException e) {
                C010708t.A0U("GkBootstrap", e, "Unable to create %s directory", "GkBootstrap");
            }
        }
    }

    public static void A05(Context context, String str, boolean z) {
        if (context != null) {
            A04(context, str, z ? 1 : 0);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0021 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A06(java.io.File r4, int r5, int r6) {
        /*
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ IOException | SecurityException -> 0x0022 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException | SecurityException -> 0x0022 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException | SecurityException -> 0x0022 }
            r1.<init>(r4)     // Catch:{ IOException | SecurityException -> 0x0022 }
            r0 = 8
            r2.<init>(r1, r0)     // Catch:{ IOException | SecurityException -> 0x0022 }
            r3.<init>(r2)     // Catch:{ IOException | SecurityException -> 0x0022 }
            r3.writeInt(r5)     // Catch:{ all -> 0x001b }
            r3.writeInt(r6)     // Catch:{ all -> 0x001b }
            r3.close()     // Catch:{ IOException | SecurityException -> 0x0022 }
            return
        L_0x001b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001d }
        L_0x001d:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x0021 }
        L_0x0021:
            throw r0     // Catch:{ IOException | SecurityException -> 0x0022 }
        L_0x0022:
            r3 = move-exception
            java.lang.Object[] r2 = new java.lang.Object[]{r4}
            java.lang.String r1 = "GkBootstrap"
            java.lang.String r0 = "Unable to persist GK value to %s"
            X.C010708t.A0V(r1, r3, r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass08X.A06(java.io.File, int, int):void");
    }

    public static boolean A08(Context context, String str, boolean z) {
        try {
            boolean z2 = true;
            if (A00(context, str, z ? 1 : 0) != 1) {
                z2 = false;
            }
            return z2;
        } finally {
            A03(context, str);
        }
    }
}
