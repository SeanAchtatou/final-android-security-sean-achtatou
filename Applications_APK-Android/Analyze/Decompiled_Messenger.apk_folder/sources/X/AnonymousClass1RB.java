package X;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/* renamed from: X.1RB  reason: invalid class name */
public final class AnonymousClass1RB extends Drawable implements Drawable.Callback {
    public int A00;
    public int A01;
    public int A02;
    public int A03 = 0;
    public int A04 = 0;
    public Drawable A05;
    public Integer A06;
    public Integer A07;
    private int A08 = 255;
    private ColorFilter A09;
    public final Paint A0A;

    public static void A00(AnonymousClass1RB r8, Drawable drawable) {
        int height;
        int i;
        if (drawable != null) {
            switch (r8.A07.intValue()) {
                case 0:
                    Rect bounds = r8.getBounds();
                    if (!bounds.isEmpty()) {
                        int i2 = r8.A01;
                        float f = (float) (i2 << 1);
                        float f2 = (float) i2;
                        int width = (int) ((((float) bounds.width()) * 0.25f) + f);
                        int height2 = (int) ((((float) bounds.height()) * 0.25f) + f);
                        int width2 = bounds.left + ((int) ((((float) bounds.width()) * 0.73f) - f2));
                        int height3 = bounds.top + ((int) ((((float) bounds.height()) * 0.73f) - f2));
                        drawable.setBounds(width2, height3, width + width2, height2 + height3);
                        return;
                    }
                    return;
                case 1:
                    Rect bounds2 = r8.getBounds();
                    int intrinsicWidth = drawable.getIntrinsicWidth();
                    int intrinsicHeight = drawable.getIntrinsicHeight();
                    int i3 = bounds2.right;
                    int i4 = i3 - intrinsicWidth;
                    int i5 = bounds2.bottom;
                    drawable.setBounds(i4, i5 - intrinsicHeight, i3, i5);
                    return;
                case 2:
                    Rect bounds3 = r8.getBounds();
                    int i6 = r8.A04;
                    int i7 = r8.A01 << 1;
                    int i8 = i6 + i7;
                    int i9 = r8.A03 + i7;
                    int width3 = (bounds3.right - ((int) (((float) bounds3.width()) * 0.1464f))) + (i8 >> 1);
                    int i10 = width3 - i8;
                    if (r8.A06 == AnonymousClass07B.A00) {
                        i = (bounds3.top + ((int) (((float) bounds3.height()) * 0.1464f))) - (i9 >> 1);
                        height = i9 + i;
                    } else {
                        height = (i9 >> 1) + (bounds3.bottom - ((int) (((float) bounds3.height()) * 0.1464f)));
                        i = height - i9;
                    }
                    drawable.setBounds(i10, i, width3, height);
                    return;
                default:
                    return;
            }
        }
    }

    public void A01(Drawable drawable) {
        if (this.A05 != drawable) {
            this.A05 = drawable;
            if (drawable != null) {
                drawable.mutate();
                this.A05.setAlpha(this.A08);
                ColorFilter colorFilter = this.A09;
                if (colorFilter != null) {
                    this.A05.setColorFilter(colorFilter);
                }
                A00(this, this.A05);
            }
            invalidateSelf();
        }
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.A05;
        if (drawable != null) {
            if (!(drawable == null || Color.alpha(this.A0A.getColor()) == 0)) {
                Drawable drawable2 = this.A05;
                if (drawable2 instanceof C77033mZ) {
                    if (drawable2 != null) {
                        Rect bounds = drawable2.getBounds();
                        int i = bounds.left;
                        int i2 = this.A00;
                        RectF rectF = new RectF((float) (i - i2), (float) (bounds.top - i2), (float) (bounds.right + i2), (float) (bounds.bottom + i2));
                        float height = rectF.height() / 2.0f;
                        canvas.drawRoundRect(rectF, height, height, this.A0A);
                    }
                } else if (drawable2 != null) {
                    Rect bounds2 = drawable2.getBounds();
                    canvas.drawCircle((float) bounds2.centerX(), (float) bounds2.centerY(), (float) ((Math.max(bounds2.width(), bounds2.height()) >> 1) + this.A00), this.A0A);
                }
            }
            this.A05.draw(canvas);
        }
    }

    public int getOpacity() {
        Drawable drawable = this.A05;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return 0;
    }

    public void invalidateDrawable(Drawable drawable) {
        if (drawable == this.A05) {
            invalidateSelf();
        }
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        if (drawable == this.A05) {
            scheduleSelf(runnable, j);
        }
    }

    public void setAlpha(int i) {
        this.A08 = i;
        Drawable drawable = this.A05;
        if (drawable != null) {
            drawable.setAlpha(i);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.A09 = colorFilter;
        Drawable drawable = this.A05;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        }
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        if (drawable == this.A05) {
            unscheduleSelf(runnable);
        }
    }

    public AnonymousClass1RB(Resources resources) {
        Paint paint = new Paint();
        this.A0A = paint;
        Integer num = AnonymousClass07B.A01;
        this.A07 = num;
        this.A06 = num;
        paint.setAntiAlias(true);
        this.A0A.setColor(0);
        int A032 = C007106r.A03(resources, 2.0f);
        this.A02 = A032;
        this.A00 = A032;
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        A00(this, this.A05);
    }
}
