package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.0ba  reason: invalid class name and case insensitive filesystem */
public abstract class C06490ba implements AnonymousClass0VL {
    /* renamed from: CIC */
    public ListenableFuture submit(Runnable runnable) {
        if (runnable != null) {
            AnonymousClass0XX A04 = A04(runnable, null);
            AnonymousClass07A.A04(this, A04, 1643763158);
            return A04;
        }
        throw new NullPointerException();
    }

    public abstract void execute(Runnable runnable);

    public boolean isShutdown() {
        return false;
    }

    public boolean isTerminated() {
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r0 = new java.util.concurrent.TimeoutException();
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007f A[Catch:{ ExecutionException -> 0x0091, RuntimeException -> 0x008a, all -> 0x00b2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0092 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object A00(java.util.Collection r19, boolean r20, long r21) {
        /*
            r18 = this;
            r2 = r21
            if (r19 == 0) goto L_0x00ce
            int r5 = r19.size()
            if (r5 == 0) goto L_0x00c8
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>(r5)
            java.util.concurrent.ExecutorCompletionService r10 = new java.util.concurrent.ExecutorCompletionService
            r0 = r18
            r10.<init>(r0)
            r4 = 1
            if (r20 == 0) goto L_0x001a
            goto L_0x001d
        L_0x001a:
            r16 = 0
            goto L_0x0021
        L_0x001d:
            long r16 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00b2 }
        L_0x0021:
            java.util.Iterator r15 = r19.iterator()     // Catch:{ all -> 0x00b2 }
            java.lang.Object r1 = r15.next()     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.Callable r1 = (java.util.concurrent.Callable) r1     // Catch:{ all -> 0x00b2 }
            r0 = -787404722(0xffffffffd111284e, float:-3.8965404E10)
            java.util.concurrent.Callable r0 = X.AnonymousClass07A.A01(r1, r0)     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.Future r0 = r10.submit(r0)     // Catch:{ all -> 0x00b2 }
            r9.add(r0)     // Catch:{ all -> 0x00b2 }
            int r14 = r5 + -1
            r11 = 0
            r0 = r11
            r13 = 1
        L_0x003e:
            java.util.concurrent.Future r12 = r10.poll()     // Catch:{ all -> 0x00b2 }
            if (r12 != 0) goto L_0x007b
            if (r14 <= 0) goto L_0x005d
            int r14 = r14 + -1
            java.lang.Object r5 = r15.next()     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.Callable r5 = (java.util.concurrent.Callable) r5     // Catch:{ all -> 0x00b2 }
            r1 = 529032757(0x1f886635, float:5.776733E-20)
            java.util.concurrent.Callable r1 = X.AnonymousClass07A.A01(r5, r1)     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.Future r1 = r10.submit(r1)     // Catch:{ all -> 0x00b2 }
            r9.add(r1)     // Catch:{ all -> 0x00b2 }
            goto L_0x0079
        L_0x005d:
            if (r13 != 0) goto L_0x0062
            if (r0 != 0) goto L_0x00a0
            goto L_0x009b
        L_0x0062:
            if (r20 == 0) goto L_0x0074
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.Future r12 = r10.poll(r2, r1)     // Catch:{ all -> 0x00b2 }
            if (r12 == 0) goto L_0x0095
            long r7 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00b2 }
            long r5 = r7 - r16
            long r2 = r2 - r5
            goto L_0x007d
        L_0x0074:
            java.util.concurrent.Future r12 = r10.take()     // Catch:{ all -> 0x00b2 }
            goto L_0x007b
        L_0x0079:
            int r13 = r13 + 1
        L_0x007b:
            r7 = r16
        L_0x007d:
            if (r12 == 0) goto L_0x0092
            int r13 = r13 + -1
            java.lang.Object r2 = r12.get()     // Catch:{ ExecutionException -> 0x0091, RuntimeException -> 0x008a }
            java.util.Iterator r1 = r9.iterator()
            goto L_0x00a1
        L_0x008a:
            r1 = move-exception
            java.util.concurrent.ExecutionException r0 = new java.util.concurrent.ExecutionException     // Catch:{ all -> 0x00b2 }
            r0.<init>(r1)     // Catch:{ all -> 0x00b2 }
            goto L_0x0092
        L_0x0091:
            r0 = move-exception
        L_0x0092:
            r16 = r7
            goto L_0x003e
        L_0x0095:
            java.util.concurrent.TimeoutException r0 = new java.util.concurrent.TimeoutException     // Catch:{ all -> 0x00b2 }
            r0.<init>()     // Catch:{ all -> 0x00b2 }
            goto L_0x00a0
        L_0x009b:
            java.util.concurrent.ExecutionException r0 = new java.util.concurrent.ExecutionException     // Catch:{ all -> 0x00b2 }
            r0.<init>(r11)     // Catch:{ all -> 0x00b2 }
        L_0x00a0:
            throw r0     // Catch:{ all -> 0x00b2 }
        L_0x00a1:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00b1
            java.lang.Object r0 = r1.next()
            java.util.concurrent.Future r0 = (java.util.concurrent.Future) r0
            r0.cancel(r4)
            goto L_0x00a1
        L_0x00b1:
            return r2
        L_0x00b2:
            r2 = move-exception
            java.util.Iterator r1 = r9.iterator()
        L_0x00b7:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00c7
            java.lang.Object r0 = r1.next()
            java.util.concurrent.Future r0 = (java.util.concurrent.Future) r0
            r0.cancel(r4)
            goto L_0x00b7
        L_0x00c7:
            throw r2
        L_0x00c8:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x00ce:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06490ba.A00(java.util.Collection, boolean, long):java.lang.Object");
    }

    public AnonymousClass0XX A04(Runnable runnable, Object obj) {
        return new C25331Zf(runnable, obj);
    }

    public AnonymousClass0XX A05(Callable callable) {
        return new C25331Zf(callable);
    }

    public /* bridge */ /* synthetic */ ListenableFuture CIE(Runnable runnable, Object obj) {
        if (runnable != null) {
            AnonymousClass0XX A04 = A04(runnable, obj);
            AnonymousClass07A.A04(this, A04, 1643763158);
            return A04;
        }
        throw new NullPointerException();
    }

    public /* bridge */ /* synthetic */ ListenableFuture CIF(Callable callable) {
        if (callable != null) {
            AnonymousClass0XX A05 = A05(callable);
            AnonymousClass07A.A04(this, A05, 1739327267);
            return A05;
        }
        throw new NullPointerException();
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void shutdown() {
        throw new UnsupportedOperationException();
    }

    public List shutdownNow() {
        throw new UnsupportedOperationException();
    }

    public List invokeAll(Collection collection) {
        if (collection != null) {
            ArrayList<Future> arrayList = new ArrayList<>(collection.size());
            try {
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    AnonymousClass0XX A05 = A05((Callable) it.next());
                    arrayList.add(A05);
                    AnonymousClass07A.A04(this, A05, 272375085);
                }
                for (Future future : arrayList) {
                    if (!future.isDone()) {
                        try {
                            future.get();
                        } catch (CancellationException | ExecutionException unused) {
                        }
                    }
                }
                return arrayList;
            } catch (Throwable th) {
                for (Future cancel : arrayList) {
                    cancel.cancel(true);
                }
                throw th;
            }
        } else {
            throw new NullPointerException();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r8 = r7.iterator();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x009e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List invokeAll(java.util.Collection r14, long r15, java.util.concurrent.TimeUnit r17) {
        /*
            r13 = this;
            if (r14 == 0) goto L_0x00d2
            r0 = r17
            if (r17 == 0) goto L_0x00d2
            r1 = r15
            long r4 = r0.toNanos(r1)
            java.util.ArrayList r7 = new java.util.ArrayList
            int r0 = r14.size()
            r7.<init>(r0)
            r6 = 1
            java.util.Iterator r1 = r14.iterator()     // Catch:{ all -> 0x00bc }
        L_0x0019:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x002d
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x00bc }
            java.util.concurrent.Callable r0 = (java.util.concurrent.Callable) r0     // Catch:{ all -> 0x00bc }
            X.0XX r0 = r13.A05(r0)     // Catch:{ all -> 0x00bc }
            r7.add(r0)     // Catch:{ all -> 0x00bc }
            goto L_0x0019
        L_0x002d:
            long r11 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00bc }
            java.util.Iterator r8 = r7.iterator()     // Catch:{ all -> 0x00bc }
        L_0x0035:
            boolean r0 = r8.hasNext()     // Catch:{ all -> 0x00bc }
            r9 = 0
            if (r0 == 0) goto L_0x006b
            java.lang.Object r1 = r8.next()     // Catch:{ all -> 0x00bc }
            java.lang.Runnable r1 = (java.lang.Runnable) r1     // Catch:{ all -> 0x00bc }
            r0 = -1236421293(0xffffffffb64db553, float:-3.0652925E-6)
            X.AnonymousClass07A.A04(r13, r1, r0)     // Catch:{ all -> 0x00bc }
            long r2 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00bc }
            long r0 = r2 - r11
            long r4 = r4 - r0
            int r0 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r0 > 0) goto L_0x0055
            goto L_0x0057
        L_0x0055:
            r11 = r2
            goto L_0x0035
        L_0x0057:
            java.util.Iterator r1 = r7.iterator()
        L_0x005b:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00bb
            java.lang.Object r0 = r1.next()
            java.util.concurrent.Future r0 = (java.util.concurrent.Future) r0
            r0.cancel(r6)
            goto L_0x005b
        L_0x006b:
            java.util.Iterator r8 = r7.iterator()     // Catch:{ all -> 0x00bc }
        L_0x006f:
            boolean r0 = r8.hasNext()     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x00bb
            java.lang.Object r1 = r8.next()     // Catch:{ all -> 0x00bc }
            java.util.concurrent.Future r1 = (java.util.concurrent.Future) r1     // Catch:{ all -> 0x00bc }
            boolean r0 = r1.isDone()     // Catch:{ all -> 0x00bc }
            if (r0 != 0) goto L_0x006f
            int r0 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r0 > 0) goto L_0x0099
            java.util.Iterator r1 = r7.iterator()
        L_0x0089:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00bb
            java.lang.Object r0 = r1.next()
            java.util.concurrent.Future r0 = (java.util.concurrent.Future) r0
            r0.cancel(r6)
            goto L_0x0089
        L_0x0099:
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ CancellationException | ExecutionException -> 0x009e, TimeoutException -> 0x00a7 }
            r1.get(r4, r0)     // Catch:{ CancellationException | ExecutionException -> 0x009e, TimeoutException -> 0x00a7 }
        L_0x009e:
            long r2 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00bc }
            long r0 = r2 - r11
            long r4 = r4 - r0
            r11 = r2
            goto L_0x006f
        L_0x00a7:
            java.util.Iterator r1 = r7.iterator()
        L_0x00ab:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00bb
            java.lang.Object r0 = r1.next()
            java.util.concurrent.Future r0 = (java.util.concurrent.Future) r0
            r0.cancel(r6)
            goto L_0x00ab
        L_0x00bb:
            return r7
        L_0x00bc:
            r2 = move-exception
            java.util.Iterator r1 = r7.iterator()
        L_0x00c1:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00d1
            java.lang.Object r0 = r1.next()
            java.util.concurrent.Future r0 = (java.util.concurrent.Future) r0
            r0.cancel(r6)
            goto L_0x00c1
        L_0x00d1:
            throw r2
        L_0x00d2:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06490ba.invokeAll(java.util.Collection, long, java.util.concurrent.TimeUnit):java.util.List");
    }

    public Object invokeAny(Collection collection) {
        try {
            return A00(collection, false, 0);
        } catch (TimeoutException unused) {
            return null;
        }
    }

    public Object invokeAny(Collection collection, long j, TimeUnit timeUnit) {
        return A00(collection, true, timeUnit.toNanos(j));
    }

    public /* bridge */ /* synthetic */ Future submit(Runnable runnable, Object obj) {
        if (runnable != null) {
            AnonymousClass0XX A04 = A04(runnable, obj);
            AnonymousClass07A.A04(this, A04, 1643763158);
            return A04;
        }
        throw new NullPointerException();
    }

    public /* bridge */ /* synthetic */ Future submit(Callable callable) {
        if (callable != null) {
            AnonymousClass0XX A05 = A05(callable);
            AnonymousClass07A.A04(this, A05, 1739327267);
            return A05;
        }
        throw new NullPointerException();
    }
}
