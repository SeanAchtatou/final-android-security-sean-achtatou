package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.181  reason: invalid class name */
public final class AnonymousClass181 implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass181(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        C193617v r0;
        int A002 = AnonymousClass09Y.A00(-330872578);
        if (AnonymousClass195.A00(intent.getIntExtra("event", AnonymousClass195.UNKNOWN.value)) == AnonymousClass195.CHANNEL_CONNECTED && (r0 = this.A00.A03) != null) {
            r0.BRu();
        }
        AnonymousClass09Y.A01(2055428882, A002);
    }
}
