package X;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;

/* renamed from: X.0Fz  reason: invalid class name and case insensitive filesystem */
public final class C02710Fz {
    public final Sensor A00;
    public final SensorEventListener A01;

    public C02710Fz(SensorEventListener sensorEventListener, Sensor sensor) {
        this.A01 = sensorEventListener;
        this.A00 = sensor;
    }
}
