package X;

import android.text.TextUtils;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.07h  reason: invalid class name and case insensitive filesystem */
public abstract class C008007h {
    public final List A00 = new LinkedList();

    public final synchronized void A02(JSONObject jSONObject) {
        for (C01630Az r2 : this.A00) {
            try {
                String Av5 = r2.Av5();
                if (!TextUtils.isEmpty(Av5)) {
                    jSONObject.put("host_name_v6", Av5);
                }
                String Acq = r2.Acq();
                if (!TextUtils.isEmpty(Acq)) {
                    jSONObject.put("analytics_endpoint", Acq);
                }
            } catch (JSONException unused) {
            }
        }
    }

    public abstract AnonymousClass07i A03();

    public abstract void A04();

    public abstract void A05();
}
