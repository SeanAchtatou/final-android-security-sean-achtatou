package X;

import java.util.List;

/* renamed from: X.1OT  reason: invalid class name */
public final class AnonymousClass1OT implements AnonymousClass1OL {
    private final List A00;

    public void BSA() {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass1OL) this.A00.get(i)).BSA();
        }
    }

    public void BY8(C33211nD r4) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass1OL) this.A00.get(i)).BY8(r4);
        }
    }

    public void Baj(C33211nD r4) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass1OL) this.A00.get(i)).Baj(r4);
        }
    }

    public void BfG(C33211nD r4) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass1OL) this.A00.get(i)).BfG(r4);
        }
    }

    public void Bkx(C33211nD r4) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass1OL) this.A00.get(i)).Bkx(r4);
        }
    }

    public void BvC(C33211nD r4) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass1OL) this.A00.get(i)).BvC(r4);
        }
    }

    public void BvD(C33211nD r4) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass1OL) this.A00.get(i)).BvD(r4);
        }
    }

    public void BvE(C33211nD r4) {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass1OL) this.A00.get(i)).BvE(r4);
        }
    }

    public AnonymousClass1OT(List list) {
        this.A00 = list;
    }
}
