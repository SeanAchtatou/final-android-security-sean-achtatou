package X;

import com.google.common.base.Preconditions;
import java.io.File;

/* renamed from: X.0q3  reason: invalid class name and case insensitive filesystem */
public final class C12820q3 extends C12830q4 {
    public final File A00;

    public String toString() {
        return "Files.asByteSource(" + this.A00 + ")";
    }

    public C12820q3(File file) {
        Preconditions.checkNotNull(file);
        this.A00 = file;
    }
}
