package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1k2  reason: invalid class name and case insensitive filesystem */
public final class C31651k2 extends AnonymousClass0UV {
    private static volatile C31661k3 A00;

    public static final C31661k3 A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C31661k3.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = null;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
