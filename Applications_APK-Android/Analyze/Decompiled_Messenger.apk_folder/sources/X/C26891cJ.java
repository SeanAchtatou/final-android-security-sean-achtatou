package X;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/* renamed from: X.1cJ  reason: invalid class name and case insensitive filesystem */
public class C26891cJ {
    public DisplayMetrics A00;
    public DisplayMetrics A01;
    public DisplayMetrics A02;
    public DisplayMetrics A03;
    public Display A04;
    private final Context A05;

    private DisplayMetrics A03(int i, boolean z) {
        if (i == 1) {
            if (z) {
                return this.A03;
            }
            return this.A02;
        } else if (z) {
            return this.A01;
        } else {
            return this.A00;
        }
    }

    public synchronized float A04() {
        return A03(A02(false), false).density;
    }

    public synchronized int A05() {
        return A03(A02(false), true).heightPixels;
    }

    public synchronized int A06() {
        return A03(A02(false), true).widthPixels;
    }

    public synchronized int A07() {
        return A03(A02(false), false).heightPixels;
    }

    public synchronized int A08() {
        return Math.max(A09(), A07());
    }

    public synchronized int A09() {
        return A03(A02(false), false).widthPixels;
    }

    public synchronized int A0A() {
        return Math.min(A09(), A07());
    }

    private int A02(boolean z) {
        if (this.A04 == null) {
            this.A04 = ((WindowManager) this.A05.getSystemService("window")).getDefaultDisplay();
        }
        int i = this.A05.getResources().getConfiguration().orientation;
        DisplayMetrics A032 = A03(i, false);
        if (z || A032 == null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            Display display = this.A04;
            AnonymousClass064.A00(display);
            display.getMetrics(displayMetrics);
            DisplayMetrics displayMetrics2 = new DisplayMetrics();
            if (Build.VERSION.SDK_INT >= 17) {
                this.A04.getRealMetrics(displayMetrics2);
            }
            if (i == 1) {
                this.A03 = displayMetrics2;
                this.A02 = displayMetrics;
            } else {
                this.A01 = displayMetrics2;
                this.A00 = displayMetrics;
                return i;
            }
        }
        return i;
    }

    public C26891cJ(Context context) {
        this.A05 = context;
        A02(true);
    }
}
