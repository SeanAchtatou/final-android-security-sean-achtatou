package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;

/* renamed from: X.20n  reason: invalid class name and case insensitive filesystem */
public final class C401620n extends C403921k {
    private AnonymousClass0UN A00;
    private final C28141eK A01;

    public static final C401620n A00(AnonymousClass1XY r3) {
        return new C401620n(r3, AnonymousClass0WT.A00(r3), C04750Wa.A01(r3));
    }

    public boolean A09(Intent intent, int i, Activity activity) {
        Intent A0A;
        int i2 = AnonymousClass1Y3.AMX;
        if (!((AnonymousClass1ES) AnonymousClass1XX.A02(0, i2, this.A00)).A0L() || !C189168qU.A01(intent, (AnonymousClass1ES) AnonymousClass1XX.A02(0, i2, this.A00)) || (A0A = this.A01.A0A(C189168qU.A00(activity, intent, i, true), activity)) == null) {
            return false;
        }
        activity.startActivityForResult(A0A, i);
        return true;
    }

    public boolean A0A(Intent intent, int i, Fragment fragment) {
        Context A1j;
        Intent A0A;
        int i2 = AnonymousClass1Y3.AMX;
        if (!((AnonymousClass1ES) AnonymousClass1XX.A02(0, i2, this.A00)).A0L() || !C189168qU.A01(intent, (AnonymousClass1ES) AnonymousClass1XX.A02(0, i2, this.A00)) || (A0A = this.A01.A0A(C189168qU.A00((A1j = fragment.A1j()), intent, i, true), A1j)) == null) {
            return false;
        }
        fragment.startActivityForResult(A0A, i);
        return true;
    }

    public boolean A0B(Intent intent, Context context) {
        Intent A0A;
        int i = AnonymousClass1Y3.AMX;
        if (!((AnonymousClass1ES) AnonymousClass1XX.A02(0, i, this.A00)).A0L() || !C189168qU.A01(intent, (AnonymousClass1ES) AnonymousClass1XX.A02(0, i, this.A00)) || (A0A = this.A01.A0A(C189168qU.A00(context, intent, 0, false), context)) == null) {
            return false;
        }
        context.startActivity(A0A);
        return true;
    }

    private C401620n(AnonymousClass1XY r4, C25051Yd r5, AnonymousClass09P r6) {
        this.A00 = new AnonymousClass0UN(1, r4);
        this.A01 = new C14070sZ(C008807t.A00(r5.At0(566587790853843L)), new AnonymousClass21E(r6, "DialtoneAwareExternalIntentHandler"));
    }
}
