package X;

import android.app.ActivityManager;
import android.os.Build;
import com.facebook.common.dextricks.DalvikConstants;
import com.facebook.common.dextricks.DexStore;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1P7  reason: invalid class name */
public final class AnonymousClass1P7 implements C23111Og {
    private static final long A01 = TimeUnit.MINUTES.toMillis(5);
    public final ActivityManager A00;

    /* renamed from: A00 */
    public C30851ik get() {
        int i;
        int min = Math.min(this.A00.getMemoryClass() * 1048576, Integer.MAX_VALUE);
        if (min < 33554432) {
            i = 4194304;
        } else if (min < 67108864) {
            i = 6291456;
        } else {
            int i2 = Build.VERSION.SDK_INT;
            i = DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE;
            if (i2 >= 11) {
                i = min >> 2;
            }
        }
        return new C30851ik(i, DexStore.LOAD_RESULT_OATMEAL_QUICKENED, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, A01);
    }

    public AnonymousClass1P7(ActivityManager activityManager) {
        this.A00 = activityManager;
    }
}
