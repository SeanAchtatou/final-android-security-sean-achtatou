package X;

import java.util.Iterator;

/* renamed from: X.0yp  reason: invalid class name and case insensitive filesystem */
public final class C17390yp implements Iterator, C17400yq {
    private C27701dc A00;
    private boolean A01 = true;
    public final /* synthetic */ AnonymousClass1XK A02;

    public C17390yp(AnonymousClass1XK r2) {
        this.A02 = r2;
    }

    public void CIX(C27701dc r3) {
        C27701dc r0 = this.A00;
        if (r3 == r0) {
            C27701dc r1 = r0.A01;
            this.A00 = r1;
            boolean z = false;
            if (r1 == null) {
                z = true;
            }
            this.A01 = z;
        }
    }

    public boolean hasNext() {
        if (!this.A01) {
            C27701dc r0 = this.A00;
            if (r0 == null || r0.A00 == null) {
                return false;
            }
            return true;
        } else if (this.A02.A02 != null) {
            return true;
        } else {
            return false;
        }
    }

    public Object next() {
        C27701dc r0;
        if (this.A01) {
            this.A01 = false;
            this.A00 = this.A02.A02;
        } else {
            C27701dc r02 = this.A00;
            if (r02 != null) {
                r0 = r02.A00;
            } else {
                r0 = null;
            }
            this.A00 = r0;
        }
        return this.A00;
    }
}
