package X;

import android.content.Context;

/* renamed from: X.0vU  reason: invalid class name and case insensitive filesystem */
public final class C15560vU {
    public AnonymousClass0UN A00;
    public C35281qy A01;
    public AnonymousClass1AR A02;
    public final AnonymousClass0p4 A03;
    public final C20151Bc A04;
    public final AnonymousClass1BC A05;
    public final AnonymousClass1BY A06;
    public final C20061At A07;
    public final C20141Bb A08 = new C20141Bb(this);
    public final AnonymousClass1BP A09;
    public final C20171Be A0A;
    public final C04310Tq A0B;
    private final C20131Ba A0C = new C15530vR(this);

    public C15560vU(AnonymousClass1XY r5, C15570vV r6, Context context, AnonymousClass1BC r8, AnonymousClass1BP r9, AnonymousClass1AR r10, AnonymousClass1BY r11, C20061At r12) {
        this.A00 = new AnonymousClass0UN(3, r5);
        this.A0B = AnonymousClass0VG.A00(AnonymousClass1Y3.AiJ, r5);
        this.A04 = C20151Bc.A00(r5);
        this.A06 = r11;
        this.A0A = new C20171Be(r6, context, r8);
        AnonymousClass0p4 r3 = new AnonymousClass0p4(context);
        this.A03 = r3;
        this.A05 = r8;
        this.A09 = r9;
        this.A02 = r10;
        this.A07 = r12;
        this.A01 = new C35281qy(0 == 0 ? (C35261qw) C27271cv.A00("com_facebook_messaging_threadlist_plugins_interfaces_inboxviewbinder_InboxViewBinderInterfaceSpec", "InboxViewBinder", context, new Object[]{r3, r8, this.A06}) : null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0064, code lost:
        if (r3 != false) goto L_0x0066;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 A00(X.C15560vU r7, X.AnonymousClass0p4 r8, X.C10700ki r9, com.facebook.mig.scheme.interfaces.MigColorScheme r10) {
        /*
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.AnonymousClass1GN.A00(r8)
            X.1GU r1 = X.AnonymousClass1I2.A00(r10)
            java.lang.Object r0 = r2.A00
            X.1GN r0 = (X.AnonymousClass1GN) r0
            r0.A05 = r1
            int r0 = r10.B9m()
            r2.A23(r0)
            X.1Ba r0 = r7.A0C
            r2.A3b(r0)
            if (r9 == 0) goto L_0x0095
            int r1 = X.AnonymousClass1Y3.APi
            X.0UN r0 = r7.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r1, r0)
            X.1I4 r5 = (X.AnonymousClass1I4) r5
            X.0ki r0 = X.C10700ki.SMS
            r8 = 0
            if (r9 != r0) goto L_0x002c
            r8 = 1
        L_0x002c:
            X.1Bb r9 = r7.A08
            int r1 = X.AnonymousClass1Y3.BUW
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.1qK r1 = (X.C34881qK) r1
            r0 = 7
            boolean r0 = r1.A01(r0)
            r6 = 0
            if (r0 == 0) goto L_0x0067
            int r1 = X.AnonymousClass1Y3.BOk
            X.0UN r0 = r5.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r1, r0)
            X.0l9 r4 = (X.C10960l9) r4
            boolean r0 = r4.A0B()
            if (r0 != 0) goto L_0x0066
            java.lang.Integer r1 = r4.A05()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r3 = 0
            if (r1 != r0) goto L_0x0064
            com.facebook.prefs.shared.FbSharedPreferences r1 = r4.A07
            X.1Y7 r0 = X.C10990lD.A08
            boolean r0 = r1.Aep(r0, r3)
            if (r0 == 0) goto L_0x0064
            r3 = 1
        L_0x0064:
            if (r3 == 0) goto L_0x0067
        L_0x0066:
            r6 = 1
        L_0x0067:
            if (r6 == 0) goto L_0x0095
            int r1 = X.AnonymousClass1Y3.B4t
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Gu r0 = (X.C21401Gu) r0
            X.1LB r3 = new X.1LB
            r4 = 2131825801(0x7f111489, float:1.9284468E38)
            r5 = 2131825802(0x7f11148a, float:1.928447E38)
            r6 = 2131825803(0x7f11148b, float:1.9284472E38)
            int r7 = r0.A01()
            r3.<init>(r4, r5, r6, r7, r8, r9)
        L_0x0085:
            java.lang.Object r0 = r2.A00
            X.1GN r0 = (X.AnonymousClass1GN) r0
            r0.A01 = r3
            r0 = 219(0xdb, float:3.07E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r2.A2p(r0)
            return r2
        L_0x0095:
            r3 = 0
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15560vU.A00(X.0vU, X.0p4, X.0ki, com.facebook.mig.scheme.interfaces.MigColorScheme):com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000");
    }
}
