package X;

import com.facebook.messaging.montage.omnistore.MontageOmnistoreCacheUpdater;
import com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1mH  reason: invalid class name and case insensitive filesystem */
public final class C32751mH implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent$3";
    public final /* synthetic */ MontageOmnistoreComponent A00;

    public C32751mH(MontageOmnistoreComponent montageOmnistoreComponent) {
        this.A00 = montageOmnistoreComponent;
    }

    public void run() {
        int i;
        C005505z.A03("MontageOmnistoreComponent:onCollectionAvailable:load", 432692902);
        try {
            ((C20921Ei) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BMw, this.A00.A00)).A08("omni_initial_load_start");
            MontageOmnistoreCacheUpdater montageOmnistoreCacheUpdater = (MontageOmnistoreCacheUpdater) this.A00.A05.get();
            ImmutableList A03 = montageOmnistoreCacheUpdater.A05.A03(AnonymousClass07B.A00);
            ((C20921Ei) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMw, montageOmnistoreCacheUpdater.A00)).A08("omni_db_load_end");
            ImmutableList.Builder builder = new ImmutableList.Builder();
            C24971Xv it = A03.iterator();
            while (it.hasNext()) {
                AnonymousClass1v1 A01 = MontageOmnistoreCacheUpdater.A01(montageOmnistoreCacheUpdater, (C33531nj) it.next());
                if (A01 != null) {
                    builder.add((Object) A01.A01);
                }
            }
            montageOmnistoreCacheUpdater.A06.A01 = AnonymousClass07B.A01;
            if (((C195518s) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AQC, montageOmnistoreCacheUpdater.A00)).A01()) {
                ((C194518f) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Ayd, montageOmnistoreCacheUpdater.A00)).A02(new AnonymousClass2W6());
            } else {
                montageOmnistoreCacheUpdater.A04.A0N("MontageOmnistoreCacheUpdater");
            }
            ((C20921Ei) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BMw, this.A00.A00)).A08("omni_initial_load_end");
            this.A00.A03.set(false);
            i = -1982582669;
        } catch (Exception e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00.A00)).softReport("com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent", "onCollectionAvailable", e);
            ((C20921Ei) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BMw, this.A00.A00)).A08("omni_initial_load_fail");
            this.A00.A03.set(false);
            i = 757243458;
        } catch (Throwable th) {
            this.A00.A03.set(false);
            C005505z.A00(1258372181);
            throw th;
        }
        C005505z.A00(i);
    }
}
