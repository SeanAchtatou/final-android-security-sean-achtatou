package X;

import android.content.Context;
import android.util.Log;
import com.facebook.acra.info.ExternalProcessInfo;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.1y1  reason: invalid class name and case insensitive filesystem */
public final class C38801y1 extends AnonymousClass0NH implements AnonymousClass1YQ {
    private AnonymousClass0UN A00;
    public final C05030Xj A01;
    public final ScheduledExecutorService A02;
    public final C04310Tq A03;
    private final C38751xw A04 = new C38751xw(this);
    private final AnonymousClass0Ud A05;

    public boolean A0A() {
        return false;
    }

    public String getSimpleName() {
        return "FBAppStateReporter";
    }

    public static final C38801y1 A06(AnonymousClass1XY r4) {
        return new C38801y1(r4, AnonymousClass1YA.A00(r4), new C39171yc(C06920cI.A00(r4)));
    }

    public Boolean A07() {
        return this.A05.A0C().asBooleanObject();
    }

    public boolean A0B() {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(588, false);
    }

    public boolean A0C() {
        return ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aer(281582352269436L, AnonymousClass0XE.A05);
    }

    public boolean A0D() {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(68, false);
    }

    public boolean A0E(AnonymousClass07N r4) {
        Integer num;
        String A032 = AnonymousClass07N.A03(r4.A0K, "installedSplits");
        if (A032 != null) {
            num = Integer.valueOf(Integer.parseInt(A032));
        } else {
            num = null;
        }
        if (num == null || num.intValue() == AnonymousClass0Mk.A00(this.A00)) {
            return false;
        }
        return true;
    }

    private C38801y1(AnonymousClass1XY r3, Context context, C39171yc r5) {
        super(context, r5);
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A01 = C05020Xi.A00(r3);
        this.A02 = AnonymousClass0UX.A0f(r3);
        this.A03 = AnonymousClass0WY.A02(r3);
        this.A05 = AnonymousClass0Ud.A00(r3);
    }

    public void A08(AnonymousClass07N r6) {
        ExternalProcessInfo A052 = r6.A05();
        ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, this.A00)).CGR(AnonymousClass06F.A01("UnexplainedFAD", A052.mMessage, 1), A052);
    }

    public void A09(File file, IOException iOException) {
        String str;
        super.A09(file, iOException);
        try {
            str = file.getCanonicalPath();
        } catch (IOException unused) {
            str = file.getPath();
        }
        ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, this.A00)).softReport("Error deleting file", AnonymousClass08S.A0J("Error deleting ASL file ", str), iOException);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        if (r1 == X.AnonymousClass01Y.BYTE_NOT_PRESENT.mLogSymbol) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0F(X.AnonymousClass07N r5) {
        /*
            r4 = this;
            boolean r0 = r5.A08()
            r3 = 0
            if (r0 == 0) goto L_0x0018
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 79
        L_0x0013:
            boolean r0 = r1.AbO(r0, r3)
            return r0
        L_0x0018:
            char r2 = r5.A05
            X.01k r0 = X.C002101k.A09
            char r1 = r0.mSymbol
            r0 = 0
            if (r1 == r2) goto L_0x0022
            r0 = 1
        L_0x0022:
            if (r0 == 0) goto L_0x0031
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 23
            goto L_0x0013
        L_0x0031:
            char r1 = r5.A00
            X.01Y r0 = X.AnonymousClass01Y.INITIAL_STATE
            char r0 = r0.mLogSymbol
            if (r1 == r0) goto L_0x0046
            X.01Y r0 = X.AnonymousClass01Y.BYTE_NOT_USED
            char r0 = r0.mLogSymbol
            if (r1 == r0) goto L_0x0046
            X.01Y r0 = X.AnonymousClass01Y.BYTE_NOT_PRESENT
            char r0 = r0.mLogSymbol
            r2 = 1
            if (r1 != r0) goto L_0x0047
        L_0x0046:
            r2 = 0
        L_0x0047:
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            if (r2 == 0) goto L_0x0056
            r0 = 24
            goto L_0x0013
        L_0x0056:
            r0 = 22
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38801y1.A0F(X.07N):boolean");
    }

    public void init() {
        int A032 = C000700l.A03(-1146149429);
        boolean z = false;
        if (AnonymousClass01P.A0Y != null) {
            z = true;
        }
        if (!z) {
            C000700l.A09(-1933322050, A032);
            return;
        }
        if (!C006006f.A02) {
            boolean equals = "true".equals(AnonymousClass00I.A02("fb.running_sapienz"));
            C006006f.A04 = equals;
            if (equals) {
                Log.w("Sapienz", "Is running Sapienz test");
            }
            C006006f.A02 = true;
        }
        if (!C006006f.A04) {
            if (!C006006f.A01) {
                boolean equals2 = "true".equals(AnonymousClass00I.A02("fb.running_mobilelab"));
                C006006f.A03 = equals2;
                if (equals2) {
                    Log.w("Mobilelab", "Is running Mobilelab test");
                }
                C006006f.A01 = true;
            }
            if (!C006006f.A03) {
                if (AnonymousClass00J.A01(this.A00).A2D) {
                    C000700l.A09(1551338522, A032);
                    return;
                }
                AnonymousClass07A.A04(this.A02, this.A04, -1563463501);
                C000700l.A09(1686651851, A032);
                return;
            }
        }
        C000700l.A09(-1920729585, A032);
    }
}
