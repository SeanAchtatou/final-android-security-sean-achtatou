package X;

/* renamed from: X.0fD  reason: invalid class name and case insensitive filesystem */
public final class C08350fD {
    public static final long A00 = (1 << 29);
    public static final long A01 = (1 << 8);
    public static final long A02 = (1 << 10);
    public static final long A03 = (1 << 13);
    public static final long A04 = (1 << 16);
    public static final long A05 = (1 << 28);
    public static final long A06 = (1 << 27);
    public static final long A07 = (1 << 9);
    public static final long A08 = (1 << 14);
    public static final long A09 = (1 << 22);
    public static final long A0A = (1 << 4);
    public static final long A0B = (1 << 12);
    public static final long A0C = (1 << 0);
    public static final long A0D = (1 << 19);
    public static final long A0E = (1 << 6);
    public static final long A0F = (1 << 11);
    public static final long A0G = (1 << 20);
    public static final long A0H = (1 << 21);
}
