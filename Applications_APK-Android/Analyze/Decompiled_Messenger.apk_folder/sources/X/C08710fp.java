package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Optional;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fp  reason: invalid class name and case insensitive filesystem */
public final class C08710fp {
    private static volatile C08710fp A02;
    public final QuickPerformanceLogger A00;
    private final AnonymousClass1ZE A01;

    public static final C08710fp A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C08710fp.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C08710fp(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static Map A01(AnonymousClass9U8 r3) {
        HashMap hashMap = new HashMap();
        hashMap.put("qt_locale", r3.A05);
        hashMap.put("qt_user_id", r3.A00);
        Optional optional = r3.A03;
        if (optional.isPresent()) {
            hashMap.put("qt_client_checksum", optional.get());
        }
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static void A02(C08710fp r4, int i, Throwable th) {
        r4.A00.markerTag(i, AnonymousClass08S.A0P("qt_error", "::", th.toString()).replace(',', ';'));
        r4.A00.markerEnd(i, 3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static void A03(C08710fp r5, int i, Map map) {
        r5.A00.markerStart(i);
        for (Map.Entry entry : map.entrySet()) {
            r5.A00.markerTag(i, AnonymousClass08S.A0P((String) entry.getKey(), "::", (String) entry.getValue()).replace(',', ';'));
        }
    }

    public static void A04(C08710fp r5, String str, AnonymousClass9U8 r7, Map map) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r5.A01.A01("fb_qt_resources_language_pack_info"), 159);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("qt_locale", r7.A05);
            uSLEBaseShape0S0000000.A0D("qt_type", str);
            uSLEBaseShape0S0000000.A0D("qt_user_id", r7.A00);
            Optional optional = r7.A03;
            if (optional.isPresent()) {
                map.put("qt_client_checksum", optional.get());
            }
            for (Map.Entry entry : map.entrySet()) {
                String str2 = (String) entry.getValue();
                if (str2 != null) {
                    String str3 = (String) entry.getKey();
                    AnonymousClass2KA r0 = (AnonymousClass2KA) C45542Mm.A00.get(str3);
                    if (r0 != null) {
                        r0.AOY(uSLEBaseShape0S0000000, str2);
                    } else {
                        C010708t.A0C(C45542Mm.class, "request param <%s> with value <%s> will not be logged as it is not defined in logger <%s>", str3, str2, uSLEBaseShape0S0000000.getClass().getSimpleName());
                    }
                }
            }
            uSLEBaseShape0S0000000.A06();
        }
    }

    private C08710fp(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass1ZD.A00(r2);
        this.A00 = AnonymousClass0ZD.A03(r2);
    }
}
