package X;

import android.os.Handler;

/* renamed from: X.0ia  reason: invalid class name */
public final class AnonymousClass0ia {
    public AnonymousClass0UN A00;

    public static final AnonymousClass0ia A00(AnonymousClass1XY r1) {
        return new AnonymousClass0ia(r1);
    }

    public void A01(Object obj) {
        ((Handler) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abm, this.A00)).removeCallbacksAndMessages(obj);
    }

    public void A02(Runnable runnable) {
        AnonymousClass00S.A04((Handler) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abm, this.A00), runnable, 764795487);
    }

    public void A03(Runnable runnable) {
        AnonymousClass00S.A02((Handler) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abm, this.A00), runnable);
    }

    public void A04(Runnable runnable, long j) {
        AnonymousClass00S.A05((Handler) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abm, this.A00), runnable, j, -2077408186);
    }

    public AnonymousClass0ia(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
