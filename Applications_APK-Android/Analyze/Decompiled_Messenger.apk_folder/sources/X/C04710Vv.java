package X;

import android.content.Context;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Vv  reason: invalid class name and case insensitive filesystem */
public final class C04710Vv extends AnonymousClass1YH {
    private static volatile C04710Vv A00;

    public static final C04710Vv A00(AnonymousClass1XY r6) {
        if (A00 == null) {
            synchronized (C04710Vv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A00 = new C04710Vv(AnonymousClass1YA.A00(applicationInjector), C04720Vx.A01(applicationInjector), AnonymousClass0W0.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private C04710Vv(Context context, C04740Vz r4, AnonymousClass0W0 r5) {
        super(context, r4, ImmutableList.of(r5), "prefs_db");
    }
}
