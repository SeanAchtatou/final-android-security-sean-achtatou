package X;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.checkpoint.CheckpointMetadata;
import com.facebook.user.model.User;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/* renamed from: X.1mB  reason: invalid class name and case insensitive filesystem */
public final class C32701mB {
    public AnonymousClass0UN A00;
    private ArrayList A01;

    public static boolean A02(C32701mB r8, CheckpointMetadata checkpointMetadata, Activity activity) {
        boolean z;
        if (checkpointMetadata != null) {
            Iterator it = A01(r8).iterator();
            while (it.hasNext()) {
                C32711mC r7 = (C32711mC) it.next();
                if (new ArrayList(Arrays.asList("516660891844314", "150171745330322")).contains(checkpointMetadata.A01)) {
                    if (!((C25051Yd) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AOJ, r7.A01)).Aem(283583805721612L)) {
                        z = false;
                    } else if (r7.A02) {
                        z = true;
                    } else {
                        r7.A02 = true;
                        if (((C25051Yd) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AOJ, r7.A01)).Aem(283583805656075L)) {
                            int i = AnonymousClass1Y3.BCt;
                            AnonymousClass0UN r6 = r7.A01;
                            String string = ((Context) AnonymousClass1XX.A02(0, i, r6)).getString(2131833584, ((User) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AoG, r6)).A0A());
                            C132836Jp r3 = new C132836Jp(activity);
                            View inflate = LayoutInflater.from(activity).inflate(2132412160, (ViewGroup) null);
                            C26902DGw dGw = r3.A01;
                            dGw.A0D = inflate;
                            dGw.A0L = string;
                            dGw.A0H = dGw.A0T.getText(2131833583);
                            r3.A02(2131833582, new C94014dm(r7, checkpointMetadata, activity));
                            r3.A01.A0M = false;
                            try {
                                r3.A00().show();
                            } catch (Exception unused) {
                            }
                        } else {
                            C32711mC.A01(r7, checkpointMetadata, activity);
                        }
                        z = true;
                    }
                    if (z) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static final C32701mB A00(AnonymousClass1XY r1) {
        return new C32701mB(r1);
    }

    public static ArrayList A01(C32701mB r4) {
        if (r4.A01 == null) {
            r4.A01 = new ArrayList(Arrays.asList((C32711mC) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AwA, r4.A00)));
        }
        return r4.A01;
    }

    private C32701mB(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
