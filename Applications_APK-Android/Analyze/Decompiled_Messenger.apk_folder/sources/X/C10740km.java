package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.perftest.base.PerfTestConfigBase;

/* renamed from: X.0km  reason: invalid class name and case insensitive filesystem */
public abstract class C10740km {
    public final Object A05(C10810kt r2, Object obj) {
        return A07(r2, obj, null, null);
    }

    public final Object A06(C10810kt r2, Object obj, CallerContext callerContext) {
        return A07(r2, obj, null, callerContext);
    }

    public Object A07(C10810kt r18, Object obj, C47692Xl r20, CallerContext callerContext) {
        AnonymousClass2W8 r5;
        C47702Xm A04;
        CallerContext callerContext2 = callerContext;
        C30271hm r4 = (C30271hm) this;
        if (callerContext == null) {
            callerContext2 = (CallerContext) C11050ll.A00.get();
        }
        C10810kt r13 = r18;
        Object obj2 = obj;
        C47692Xl r8 = r20;
        if (!(r13 instanceof AnonymousClass2W8) || (A04 = (r5 = (AnonymousClass2W8) r13).A04(obj2)) == null) {
            return r4.A09(r13.B0t(obj2), r8, r13, (r18 == null || !(r13 instanceof C47712Xn)) ? null : (C47712Xn) r13, obj2, callerContext2).A00;
        }
        AnonymousClass0UN r1 = r4.A00;
        if (!PerfTestConfigBase.A04 || !((C98184mi) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AXM, r1)).A01.contains(A04.A0E)) {
            return C30271hm.A01(r4, r5, A04, obj2, r8, callerContext2);
        }
        Object obj3 = ((C98184mi) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AXM, r4.A00)).A00.get(C98184mi.A01(A04));
        if (obj3 != null) {
            return obj3;
        }
        Object A01 = C30271hm.A01(r4, r5, A04, obj2, r8, callerContext2);
        ((C98184mi) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AXM, r4.A00)).A00.put(C98184mi.A01(A04), A01);
        return A01;
    }

    public void A08(AnonymousClass9HU r5) {
        AnonymousClass2Y3 r3;
        if (this instanceof C30271hm) {
            C30271hm r0 = (C30271hm) this;
            if (r5 != null) {
                synchronized (r5) {
                    r3 = r5.A00;
                }
                if (r3 != null) {
                    AnonymousClass0Z4.A00((AnonymousClass0Z4) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AAv, r0.A00)).ARx(r3);
                }
            }
        }
    }
}
