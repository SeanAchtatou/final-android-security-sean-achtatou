package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Parcel;
import com.facebook.soloader.SysUtil$LollipopSysdeps;
import java.io.File;
import java.io.IOException;

/* renamed from: X.01o  reason: invalid class name and case insensitive filesystem */
public final class C002401o {
    public static String[] A02() {
        if (Build.VERSION.SDK_INT < 21) {
            return new String[]{Build.CPU_ABI, Build.CPU_ABI2};
        }
        return SysUtil$LollipopSysdeps.getSupportedAbis();
    }

    public static void A00(File file) {
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File A00 : listFiles) {
                    A00(A00);
                }
            } else {
                return;
            }
        }
        if (!file.delete() && file.exists()) {
            throw new IOException("could not delete: " + file);
        }
    }

    public static byte[] A01(File file, Context context) {
        int i;
        File canonicalFile = file.getCanonicalFile();
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeByte((byte) 1);
            obtain.writeString(canonicalFile.getPath());
            obtain.writeLong(canonicalFile.lastModified());
            PackageManager packageManager = context.getPackageManager();
            if (packageManager != null) {
                try {
                    i = packageManager.getPackageInfo(context.getPackageName(), 0).versionCode;
                } catch (PackageManager.NameNotFoundException | RuntimeException unused) {
                }
                obtain.writeInt(i);
                return obtain.marshall();
            }
            i = 0;
            obtain.writeInt(i);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }
}
