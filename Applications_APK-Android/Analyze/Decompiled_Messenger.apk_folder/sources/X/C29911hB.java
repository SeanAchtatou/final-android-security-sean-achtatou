package X;

import android.app.ActivityManager;
import android.os.Build;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import java.util.List;

/* renamed from: X.1hB  reason: invalid class name and case insensitive filesystem */
public final class C29911hB {
    public final TelephonyManager A00;
    public final C30001hK A01;
    public final C29971hH A02;
    public final C29951hF A03;

    private void A00(String str, String str2, boolean z) {
        C29951hF r0 = this.A03;
        if (r0 != null) {
            String str3 = str;
            r0.A00("SafeTelephonyManager", str3, z, false, null, str2);
        }
    }

    private boolean A01() {
        if (this.A02.A00 || this.A01 == null) {
            return false;
        }
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 29) {
            z = true;
        }
        if (!z) {
            return false;
        }
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(runningAppProcessInfo);
        int i = runningAppProcessInfo.importance;
        boolean z2 = false;
        if (i == 100) {
            z2 = true;
        }
        return !z2;
    }

    public void A04(PhoneStateListener phoneStateListener, int i) {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 29) {
            z = true;
        }
        if (z) {
            i = i & -1025 & -17 & -2;
        }
        this.A00.listen(phoneStateListener, i);
    }

    public C29911hB(TelephonyManager telephonyManager, C29971hH r2, C30001hK r3, C29951hF r4) {
        this.A00 = telephonyManager;
        this.A02 = r2;
        this.A01 = r3;
        this.A03 = r4;
    }

    public CellLocation A02(String str) {
        CellLocation cellLocation;
        if (A01()) {
            A00("getCellLocation", str, true);
            return null;
        }
        A00("getCellLocation", str, false);
        try {
            TelephonyManager telephonyManager = this.A00;
            if (!C29981hI.A00()) {
                return telephonyManager.getCellLocation();
            }
            C29981hI.A01.readLock().lock();
            C29991hJ r2 = C29981hI.A00;
            if (r2.A02 && C25891aZ.A02(r2.A00)) {
                C25891aZ.A01(r2.A00, C25891aZ.A06);
            }
            if (!r2.A01 || !C25891aZ.A02(r2.A00)) {
                cellLocation = telephonyManager.getCellLocation();
            } else {
                cellLocation = null;
            }
            C29981hI.A01.readLock().unlock();
            return cellLocation;
        } catch (SecurityException unused) {
            return null;
        } catch (Throwable th) {
            C29981hI.A01.readLock().unlock();
            throw th;
        }
    }

    public List A03(String str) {
        if (A01()) {
            A00("getAllCellInfo", str, true);
            return null;
        }
        A00("getAllCellInfo", str, false);
        try {
            return C013409v.A00(this.A00);
        } catch (SecurityException unused) {
            return null;
        }
    }
}
