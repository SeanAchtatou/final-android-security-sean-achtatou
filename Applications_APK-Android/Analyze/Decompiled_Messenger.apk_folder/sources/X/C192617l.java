package X;

import com.google.common.base.MoreObjects;
import java.util.EnumSet;

/* renamed from: X.17l  reason: invalid class name and case insensitive filesystem */
public final class C192617l {
    public final int A00;
    public final EnumSet A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C192617l) {
                C192617l r5 = (C192617l) obj;
                if (!this.A01.equals(r5.A01) || this.A00 != r5.A00) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A01.hashCode();
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(C192617l.class);
        stringHelper.add("listsToLoad", this.A01);
        stringHelper.add("maxContacts", this.A00);
        return stringHelper.toString();
    }

    public C192617l(EnumSet enumSet, int i) {
        this.A01 = enumSet;
        this.A00 = i;
    }
}
