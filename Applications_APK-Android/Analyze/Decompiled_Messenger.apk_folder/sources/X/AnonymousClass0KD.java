package X;

/* renamed from: X.0KD  reason: invalid class name */
public final class AnonymousClass0KD {
    public static char A00(int i) {
        if (i < 0) {
            return '!';
        }
        switch (i) {
            case 0:
                return ' ';
            case 100:
                return 'a';
            case AnonymousClass1Y3.A0w /*125*/:
                return 'b';
            case AnonymousClass1Y3.A0y /*130*/:
                return 'c';
            case AnonymousClass1Y3.A1B /*150*/:
                return 'd';
            case AnonymousClass1Y3.A1J /*170*/:
                return 'e';
            case AnonymousClass1Y3.A1c /*200*/:
                return 'f';
            case 230:
                return 'g';
            case 300:
                return 'h';
            case 325:
                return 'i';
            case AnonymousClass1Y3.A2l /*350*/:
                return 'j';
            case AnonymousClass1Y3.A3I /*400*/:
                return 'k';
            case 500:
                return 'l';
            case AnonymousClass1Y3.A87 /*1000*/:
                return 'm';
            default:
                return 'z';
        }
    }
}
