package X;

import android.os.HandlerThread;
import android.util.Log;
import android.util.SparseArray;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;
import com.facebook.profilo.mmapbuf.MmapBufferManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceArray;

/* renamed from: X.054  reason: invalid class name */
public final class AnonymousClass054 {
    public static final ThreadLocal A06 = new C005305u();
    public static volatile AnonymousClass054 A07;
    private AnonymousClass0HZ A00;
    public final SparseArray A01;
    public final AtomicInteger A02 = new AtomicInteger(0);
    public final AtomicReference A03;
    public final AtomicReferenceArray A04 = new AtomicReferenceArray(2);
    private final C004605l A05;

    private void A03(TraceContext traceContext) {
        int i;
        boolean z = false;
        int i2 = 0;
        while (true) {
            if (i2 < 2) {
                if (this.A04.compareAndSet(i2, traceContext, null)) {
                    do {
                        i = this.A02.get();
                    } while (!this.A02.compareAndSet(i, (1 << i2) ^ i));
                    z = true;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        if (!z) {
            Log.w("Profilo/TraceControl", "Could not reset Trace Context to null");
        }
    }

    public static boolean A04(AnonymousClass054 r14, int i, TraceContext traceContext, AnonymousClass05d r17) {
        AnonymousClass054 r2;
        int i2;
        int i3;
        int i4;
        int i5;
        do {
            r2 = r14;
            i2 = r14.A02.get();
            i3 = i;
            i4 = i & 2;
            int i6 = 1;
            if (i4 != 0) {
                i6 = 65534;
            }
            int i7 = i2 | i6;
            i5 = (i7 ^ -1) & (i7 + 1) & ((1 << 2) - 1);
            if (i5 == 0) {
                return false;
            }
        } while (!r14.A02.compareAndSet(i2, i2 | i5));
        AtomicReferenceArray atomicReferenceArray = r14.A04;
        int i8 = -1;
        while (i5 != 0) {
            i8++;
            i5 >>= 1;
        }
        TraceContext traceContext2 = traceContext;
        if (atomicReferenceArray.compareAndSet(i8, null, traceContext2)) {
            int A002 = traceContext2.A06.A00("trace_config.trace_timeout_ms", r17.AiX().B6p());
            if (A002 == -1) {
                A002 = 30000;
            }
            if ((i & 3) != 0) {
                A002 = Integer.MAX_VALUE;
            }
            long j = traceContext2.A05;
            if (Logger.sInitialized) {
                MmapBufferManager mmapBufferManager = Logger.sMmapBufferManager;
                boolean z = true;
                if (mmapBufferManager != null) {
                    z = !mmapBufferManager.allocateBuffer(Logger.sRingBufferSize);
                }
                if (z) {
                    Logger.nativeInitRingBuffer(Logger.sRingBufferSize);
                }
                if (i4 == 0) {
                    Logger.startWorkerThreadIfNecessary();
                    Logger.loggerWriteAndWakeupTraceWriter(Logger.sTraceWriter, j, 40, A002, i3, j);
                }
            }
            Logger.writeStandardEntry(0, 7, 94, 0, 0, traceContext2.A06.A00("trace_config.logger_priority", 5), 0, traceContext2.A05);
            synchronized (r2) {
                r2.A02();
                if (A01(r2, traceContext2.A05) != null) {
                    AnonymousClass0HZ r6 = r2.A00;
                    synchronized (r6) {
                        r6.A01.add(Long.valueOf(traceContext2.A05));
                        C004605l r3 = r6.A00;
                        if (r3 != null) {
                            r3.Bsd(traceContext2);
                        }
                        r6.sendMessage(r6.obtainMessage(1, traceContext2));
                        r6.sendMessageDelayed(r6.obtainMessage(0, traceContext2), (long) A002);
                    }
                }
            }
            return true;
        }
        throw new RuntimeException("ORDERING VIOLATION - ACQUIRED SLOT BUT SLOT NOT EMPTY");
    }

    public boolean A0B(int i, Object obj, long j) {
        return A05(this, i, obj, 1, j, 0);
    }

    public static TraceContext A00(AnonymousClass054 r11, int i, long j, Object obj) {
        if (r11.A02.get() != 0) {
            for (int i2 = 0; i2 < 2; i2++) {
                TraceContext traceContext = (TraceContext) r11.A04.get(i2);
                if (traceContext != null && (traceContext.A01 & i) != 0 && ((AnonymousClass04j) traceContext.A08).AUJ(traceContext.A04, traceContext.A07, j, obj)) {
                    return traceContext;
                }
            }
        }
        return null;
    }

    public static TraceContext A01(AnonymousClass054 r6, long j) {
        if (r6.A02.get() != 0) {
            for (int i = 0; i < 2; i++) {
                TraceContext traceContext = (TraceContext) r6.A04.get(i);
                if (traceContext != null && traceContext.A05 == j) {
                    return traceContext;
                }
            }
        }
        return null;
    }

    private void A02() {
        C02930Ha r2;
        HandlerThread handlerThread;
        if (this.A00 == null) {
            C004605l r3 = this.A05;
            synchronized (C02930Ha.class) {
                if (C02930Ha.A01 == null) {
                    C02930Ha.A01 = new C02930Ha();
                }
                r2 = C02930Ha.A01;
            }
            synchronized (r2) {
                if (r2.A00 == null) {
                    HandlerThread handlerThread2 = new HandlerThread("Prflo:TraceCtl");
                    r2.A00 = handlerThread2;
                    handlerThread2.start();
                }
                handlerThread = r2.A00;
            }
            this.A00 = new AnonymousClass0HZ(r3, handlerThread.getLooper());
        }
    }

    public AnonymousClass057 A06(int i) {
        AnonymousClass05d r0 = (AnonymousClass05d) this.A03.get();
        if (r0 == null) {
            return null;
        }
        return r0.AiX().Ai1(i);
    }

    public String A07(int i) {
        if (this.A02.get() != 0) {
            for (int i2 = 0; i2 < 2; i2++) {
                TraceContext traceContext = (TraceContext) this.A04.get(i2);
                if (traceContext != null) {
                    Object obj = traceContext.A08;
                    if ((obj instanceof C003504l) && ((C003504l) obj).BFS(traceContext.A04, traceContext.A07, i)) {
                        return traceContext.A09;
                    }
                }
            }
        }
        return null;
    }

    public List A08() {
        if (this.A02.get() == 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(2);
        for (int i = 0; i < 2; i++) {
            TraceContext traceContext = (TraceContext) this.A04.get(i);
            if (traceContext != null) {
                long j = traceContext.A05;
                String str = traceContext.A09;
                int i2 = traceContext.A01;
                Object obj = traceContext.A08;
                Object obj2 = traceContext.A07;
                long j2 = traceContext.A04;
                int i3 = traceContext.A02;
                int i4 = traceContext.A03;
                int i5 = i4;
                int i6 = i3;
                arrayList.add(new TraceContext(j, str, i2, obj, obj2, j2, i6, i5, traceContext.A00, traceContext.A06));
            }
        }
        return arrayList;
    }

    public boolean A0A(int i, int i2, Object obj, long j) {
        int AYs;
        long abs;
        TraceContext.TraceConfigExtras B6o;
        int i3 = this.A02.get();
        int i4 = i2;
        int i5 = 1;
        if ((i2 & 2) != 0) {
            i5 = 65534;
        }
        int i6 = i3 | i5;
        if (((i6 ^ -1) & (i6 + 1) & ((1 << 2) - 1)) != 0) {
            int i7 = i;
            AnonymousClass04j r12 = (AnonymousClass04j) this.A01.get(i7);
            if (r12 != null) {
                AnonymousClass05d r1 = (AnonymousClass05d) this.A03.get();
                if (r1 != null) {
                    AnonymousClass057 r5 = null;
                    if (!r12.BE8() || (r5 = r1.AiX().Ai1(i7)) != null) {
                        long j2 = j;
                        Object obj2 = obj;
                        if (A00(this, i7, j2, obj2) == null && (AYs = r12.AYs(j2, obj2, r5)) != 0) {
                            do {
                                abs = Math.abs(((Random) A06.get()).nextLong());
                            } while (abs <= 0);
                            Log.w("Profilo/TraceControl", AnonymousClass08S.A0J("START PROFILO_TRACEID: ", AnonymousClass0HU.A02(abs)));
                            String A022 = AnonymousClass0HU.A02(abs);
                            if (r5 == null) {
                                B6o = TraceContext.TraceConfigExtras.A03;
                            } else {
                                B6o = r12.B6o(j2, obj2, r5);
                            }
                            return A04(this, i4, new TraceContext(abs, A022, i7, r12, obj2, j2, AYs, i4, 0, B6o), r1);
                        }
                    }
                }
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A09("Unregistered controller for id = ", i7));
            }
        }
        return false;
    }

    public String[] A0C() {
        if (this.A02.get() != 0) {
            String[] strArr = new String[2];
            int i = 0;
            for (int i2 = 0; i2 < 2; i2++) {
                TraceContext traceContext = (TraceContext) this.A04.get(i2);
                if (traceContext != null) {
                    strArr[i] = traceContext.A09;
                    i++;
                }
            }
            if (i != 0) {
                return (String[]) Arrays.copyOf(strArr, i);
            }
        }
        return null;
    }

    public AnonymousClass054(SparseArray sparseArray, AnonymousClass05d r4, C004605l r5) {
        this.A01 = sparseArray;
        this.A03 = new AtomicReference(r4);
        this.A05 = r5;
    }

    public static boolean A05(AnonymousClass054 r6, int i, Object obj, int i2, long j, int i3) {
        TraceContext A002 = A00(r6, i, j, obj);
        if (A002 == null) {
            return false;
        }
        r6.A03(A002);
        Log.w("Profilo/TraceControl", AnonymousClass08S.A0J("STOP PROFILO_TRACEID: ", AnonymousClass0HU.A02(A002.A05)));
        synchronized (r6) {
            r6.A02();
            if (i2 == 0) {
                Logger.postFinishTrace(38, A002.A05);
                r6.A00.A00(new TraceContext(A002, i3));
            } else if (i2 == 1) {
                Logger.postFinishTrace(61, A002.A05);
                AnonymousClass0HZ r5 = r6.A00;
                synchronized (r5) {
                    if (r5.A01.contains(Long.valueOf(A002.A05))) {
                        r5.sendMessage(r5.obtainMessage(2, A002));
                        r5.A01.remove(Long.valueOf(A002.A05));
                    }
                }
            }
        }
        return true;
    }

    public void A09(long j, int i) {
        TraceContext A012 = A01(this, j);
        if (A012 != null && A012.A05 == j) {
            A03(A012);
            synchronized (this) {
                A02();
                this.A00.A00(new TraceContext(A012, i));
            }
        }
    }
}
