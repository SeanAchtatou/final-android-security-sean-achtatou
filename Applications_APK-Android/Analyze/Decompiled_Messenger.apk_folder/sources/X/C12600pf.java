package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.folders.FolderCounts;

/* renamed from: X.0pf  reason: invalid class name and case insensitive filesystem */
public final class C12600pf implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new FolderCounts(parcel);
    }

    public Object[] newArray(int i) {
        return new FolderCounts[i];
    }
}
