package X;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.07X  reason: invalid class name */
public final class AnonymousClass07X<K, V> extends AbstractMap<K, V> {
    public final List A00 = C04300To.A00();
    public final List A01;
    public final Map A02;
    private final Comparator A03;
    private final Map A04;

    private int A00(Object obj) {
        Object obj2;
        int binarySearch = Collections.binarySearch(this.A00, obj, this.A03);
        if (binarySearch < 0) {
            for (int i = 0; i < this.A00.size(); i++) {
                if (this.A00.get(i) == obj) {
                    return i;
                }
            }
        } else {
            int i2 = binarySearch;
            while (i2 < this.A00.size()) {
                Object obj3 = this.A00.get(i2);
                if (this.A03.compare(obj, obj3) != 0) {
                    break;
                } else if (obj == obj3) {
                    return i2;
                } else {
                    i2++;
                }
            }
            do {
                binarySearch--;
                if (binarySearch >= 0) {
                    obj2 = this.A00.get(binarySearch);
                    if (this.A03.compare(obj, obj2) == 0) {
                    }
                }
                for (int i3 = 0; i3 < this.A00.size(); i3++) {
                    if (this.A00.get(i3) == obj) {
                        return i3;
                    }
                }
            } while (obj != obj2);
            return binarySearch;
        }
        return -1;
    }

    private Object A01(Object obj) {
        Object remove = this.A02.remove(obj);
        if (remove == null) {
            return null;
        }
        int A002 = A00(remove);
        if (A002 >= 0) {
            this.A00.remove(A002);
            return remove;
        }
        throw new RuntimeException("The collection is in an invalid state");
    }

    private void A02(Object obj) {
        int binarySearch = Collections.binarySearch(this.A00, obj, this.A03);
        if (binarySearch >= 0) {
            do {
                binarySearch++;
                if (binarySearch >= this.A00.size()) {
                    break;
                }
            } while (this.A03.compare(this.A00.get(binarySearch - 1), this.A00.get(binarySearch)) == 0);
        } else {
            binarySearch = -(binarySearch + 1);
        }
        this.A00.add(binarySearch, obj);
    }

    public void clear() {
        Object[] array = this.A02.keySet().toArray();
        for (Object A012 : array) {
            A01(A012);
        }
    }

    public boolean containsKey(Object obj) {
        return this.A02.containsKey(obj);
    }

    public Set entrySet() {
        return this.A04.entrySet();
    }

    public Object get(Object obj) {
        return this.A02.get(obj);
    }

    public int size() {
        return this.A02.size();
    }

    public AnonymousClass07X(Comparator comparator) {
        this.A03 = comparator;
        HashMap A042 = AnonymousClass0TG.A04();
        this.A02 = A042;
        this.A04 = Collections.unmodifiableMap(A042);
        this.A01 = Collections.unmodifiableList(this.A00);
    }

    public Object put(Object obj, Object obj2) {
        Object obj3 = get(obj);
        if (obj3 != null) {
            if (this.A03.compare(obj3, obj2) == 0) {
                int A002 = A00(obj3);
                if (A002 >= 0) {
                    this.A00.set(A002, obj2);
                }
            } else {
                int A003 = A00(this.A02.remove(obj));
                if (A003 >= 0) {
                    this.A00.remove(A003);
                    A02(obj2);
                    this.A02.put(obj, obj2);
                    return obj3;
                }
            }
            throw new RuntimeException("The collection is in an invalid state");
        }
        A02(obj2);
        this.A02.put(obj, obj2);
        return obj3;
    }

    public Object remove(Object obj) {
        return A01(obj);
    }
}
