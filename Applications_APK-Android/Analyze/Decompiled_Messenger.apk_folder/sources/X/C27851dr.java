package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.facebook.common.memory.leaklistener.MemoryLeakListener;
import com.facebook.location.foreground.ForegroundLocationFrameworkController;
import com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Optional;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1dr  reason: invalid class name and case insensitive filesystem */
public final class C27851dr implements C13200qt {
    private AnonymousClass0UN A00;

    public int AzM() {
        return 1;
    }

    public static final C27851dr A00(AnonymousClass1XY r1) {
        return new C27851dr(r1);
    }

    public void AZv(Activity activity) {
        C21031Et r2 = (C21031Et) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Ar8, this.A00);
        if (!r2.A00) {
            r2.A01.A02(activity.getClass());
            r2.A00 = true;
        }
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(1);
    }

    public void BN7(Activity activity) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    /* JADX INFO: finally extract failed */
    public void BNA(Activity activity) {
        View findViewById;
        C005505z.A03("CollectiveLifetimeActivityListenerImpl.onActivityCreate[TTRCActivityListener.onActivityCreate]", 369531202);
        try {
            C29771gx r3 = (C29771gx) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ai0, this.A00);
            Activity activity2 = activity;
            if (((AnonymousClass1YI) AnonymousClass1XX.A03(AnonymousClass1Y3.AcD, r3.A00)).AbO(375, true) && (findViewById = activity.findViewById(16908290)) != null) {
                View rootView = findViewById.getRootView();
                rootView.getViewTreeObserver().addOnGlobalLayoutListener(new C51872i1(r3, rootView));
            }
            C005505z.A00(-469645827);
            C005505z.A03("CollectiveLifetimeActivityListenerImpl.onActivityCreate[AuthenticatedActivityHelper.onActivityCreate]", -31109326);
            try {
                C27871dt r5 = (C27871dt) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMr, this.A00);
                if (!(activity instanceof C27891dv)) {
                    AnonymousClass07A.A02((ExecutorService) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Aoc, r5.A06), new C17290yf(r5, activity), -1523484706);
                }
                C005505z.A00(-799217774);
                C005505z.A03("CollectiveLifetimeActivityListenerImpl.onActivityCreate[ActivityCleaner.onActivityCreate]", 1440329456);
                try {
                    new AnonymousClass0tT((AnonymousClass0t4) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BSw, this.A00), 0, activity2, Long.MIN_VALUE).A00();
                    C005505z.A00(-1271050570);
                    C005505z.A03("CollectiveLifetimeActivityListenerImpl.onActivityCreate[ChoreographedActivityListener.onActivityCreate]", 1200752557);
                    try {
                        C21031Et r2 = (C21031Et) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Ar8, this.A00);
                        if (!r2.A00) {
                            r2.A01.A02(activity.getClass());
                            r2.A00 = true;
                        }
                        C005505z.A00(-517739229);
                        C005505z.A03("CollectiveLifetimeActivityListenerImpl.onActivityCreate[FirstFrameTracker.onActivityCreate]", -480818568);
                        try {
                            C29791gz r52 = (C29791gz) AnonymousClass1XX.A02(27, AnonymousClass1Y3.ADn, this.A00);
                            int i = AnonymousClass1Y3.BBd;
                            ((QuickPerformanceLogger) AnonymousClass1XX.A02(2, i, r52.A00)).markerStart(44826634);
                            if (((QuickPerformanceLogger) AnonymousClass1XX.A02(2, i, r52.A00)).isMarkerOn(44826634)) {
                                View findViewById2 = activity.findViewById(16908290);
                                if (findViewById2 != null) {
                                    C29791gz.A02(r52, findViewById2, r52.A01);
                                } else {
                                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBd, r52.A00)).markerEnd(44826634, 4);
                                }
                            }
                            C005505z.A00(-751631403);
                            ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(5);
                        } catch (Throwable th) {
                            C005505z.A00(548781353);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        C005505z.A00(-449750453);
                        throw th2;
                    }
                } catch (Throwable th3) {
                    C005505z.A00(1108324423);
                    throw th3;
                }
            } catch (Throwable th4) {
                C005505z.A00(-27972535);
                throw th4;
            }
        } catch (Throwable th5) {
            C005505z.A00(-338706931);
            throw th5;
        }
    }

    /* JADX INFO: finally extract failed */
    public void BNJ(Activity activity, int i, int i2, Intent intent) {
        C005505z.A03("CollectiveLifetimeActivityListenerImpl.onActivityResult[CheckpointActivityListener.onActivityResult]", 760807591);
        try {
            Iterator it = C32701mB.A01((C32701mB) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BTy, this.A00)).iterator();
            while (it.hasNext()) {
                C32711mC r6 = (C32711mC) it.next();
                if (i == 3888) {
                    r6.A02 = false;
                    int i3 = AnonymousClass1Y3.BI3;
                    AnonymousClass0UN r2 = r6.A01;
                    C11670nb r22 = new C11670nb("compassion_resources_webview_dismissed");
                    r22.A0D(AnonymousClass24B.$const$string(AnonymousClass1Y3.A0i), Long.toString((((AnonymousClass069) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BBa, r2)).now() - r6.A00) / 1000));
                    ((AnonymousClass4WS) AnonymousClass1XX.A02(5, i3, r2)).A00.A09(r22);
                }
            }
            C005505z.A00(895435004);
            ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(1);
        } catch (Throwable th) {
            C005505z.A00(-51363606);
            throw th;
        }
    }

    public void BON(Activity activity, Resources.Theme theme, int i, boolean z) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008b, code lost:
        if (r1.equals(r8.getClass().getName()) != false) goto L_0x008d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BOW(android.app.Activity r7, androidx.fragment.app.Fragment r8) {
        /*
            r6 = this;
            java.lang.String r1 = "CollectiveLifetimeActivityListenerImpl.onAttachFragment[MemoryLeakListener.onAttachFragment]"
            r0 = -1578514063(0xffffffffa1e9c971, float:-1.5842015E-18)
            X.C005505z.A03(r1, r0)
            r2 = 13
            int r1 = X.AnonymousClass1Y3.AVh     // Catch:{ all -> 0x00c2 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x00c2 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00c2 }
            com.facebook.common.memory.leaklistener.MemoryLeakListener r1 = (com.facebook.common.memory.leaklistener.MemoryLeakListener) r1     // Catch:{ all -> 0x00c2 }
            boolean r0 = com.facebook.common.memory.leaklistener.MemoryLeakListener.A01(r1)     // Catch:{ all -> 0x00c2 }
            if (r0 == 0) goto L_0x0024
            boolean r0 = r8 instanceof X.C13460rT     // Catch:{ all -> 0x00c2 }
            if (r0 == 0) goto L_0x0024
            r0 = r8
            X.0rT r0 = (X.C13460rT) r0     // Catch:{ all -> 0x00c2 }
            r0.A2N(r1)     // Catch:{ all -> 0x00c2 }
        L_0x0024:
            r0 = 1183661988(0x468d3fa4, float:18079.82)
            X.C005505z.A00(r0)
            r1 = 82836372(0x4effb94, float:5.6419668E-36)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onAttachFragment[LifecycleMemoryLeakListener.onAttachFragment]"
            X.C005505z.A03(r0, r1)
            r2 = 17
            int r1 = X.AnonymousClass1Y3.AUK     // Catch:{ all -> 0x00ba }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x00ba }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00ba }
            com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener r1 = (com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener) r1     // Catch:{ all -> 0x00ba }
            boolean r0 = r8 instanceof X.C13460rT     // Catch:{ all -> 0x00ba }
            if (r0 == 0) goto L_0x0048
            r0 = r8
            X.0rT r0 = (X.C13460rT) r0     // Catch:{ all -> 0x00ba }
            r0.A2N(r1)     // Catch:{ all -> 0x00ba }
        L_0x0048:
            r0 = 1754980348(0x689adffc, float:5.85101E24)
            X.C005505z.A00(r0)
            r1 = 282348204(0x10d44aac, float:8.3734306E-29)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onAttachFragment[IncomingIntentLifetimeActivityListener.onAttachFragment]"
            X.C005505z.A03(r0, r1)
            r2 = 26
            int r1 = X.AnonymousClass1Y3.B06     // Catch:{ all -> 0x00b2 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x00b2 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00b2 }
            X.0yU r0 = (X.C17200yU) r0     // Catch:{ all -> 0x00b2 }
            com.facebook.secure.intentswitchoff.ActivityIntentSwitchOffDI r5 = r0.A01     // Catch:{ all -> 0x00b2 }
            X.06v r0 = r5.A00     // Catch:{ all -> 0x00b2 }
            if (r0 == 0) goto L_0x009b
            boolean r0 = r0.CDp()     // Catch:{ all -> 0x00b2 }
            if (r0 == 0) goto L_0x009b
            X.06v r0 = r5.A00     // Catch:{ all -> 0x00b2 }
            X.09U[] r4 = r0.AqR()     // Catch:{ all -> 0x00b2 }
            int r3 = r4.length     // Catch:{ all -> 0x00b2 }
            r2 = 0
        L_0x0076:
            if (r2 >= r3) goto L_0x009b
            r0 = r4[r2]     // Catch:{ all -> 0x00b2 }
            java.lang.String r1 = r0.A02     // Catch:{ all -> 0x00b2 }
            if (r1 == 0) goto L_0x008d
            java.lang.Class r0 = r8.getClass()     // Catch:{ all -> 0x00b2 }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x00b2 }
            boolean r1 = r1.equals(r0)     // Catch:{ all -> 0x00b2 }
            r0 = 0
            if (r1 == 0) goto L_0x008e
        L_0x008d:
            r0 = 1
        L_0x008e:
            if (r0 == 0) goto L_0x0091
            goto L_0x0094
        L_0x0091:
            int r2 = r2 + 1
            goto L_0x0076
        L_0x0094:
            android.content.Intent r0 = r7.getIntent()     // Catch:{ all -> 0x00b2 }
            r5.A05(r7, r0)     // Catch:{ all -> 0x00b2 }
        L_0x009b:
            r3 = 3
            r0 = -903605672(0xffffffffca241258, float:-2688150.0)
            X.C005505z.A00(r0)
            r2 = 30
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r3)
            return
        L_0x00b2:
            r1 = move-exception
            r0 = 1775505744(0x69d41150, float:3.2046754E25)
            X.C005505z.A00(r0)
            throw r1
        L_0x00ba:
            r1 = move-exception
            r0 = 170034085(0xa2283a5, float:7.824776E-33)
            X.C005505z.A00(r0)
            throw r1
        L_0x00c2:
            r1 = move-exception
            r0 = -348382610(0xffffffffeb3c1a6e, float:-2.2740286E26)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27851dr.BOW(android.app.Activity, androidx.fragment.app.Fragment):void");
    }

    public boolean BPC(Activity activity) {
        AnonymousClass1EY r2 = (AnonymousClass1EY) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A7A, this.A00);
        if (AnonymousClass1EY.A03(r2)) {
            ((AnonymousClass0ia) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A9n, r2.A00)).A02(new C83973yY(r2));
        } else {
            AnonymousClass1EY.A01(r2);
        }
        activity.getClass().equals(((C191416y) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AbE, this.A00)).A00.A05);
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(2);
        return false;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03e4, code lost:
        if (r1 != false) goto L_0x03e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0401, code lost:
        if (r1 != false) goto L_0x0403;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0438, code lost:
        if (r16.booleanValue() != false) goto L_0x043b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x0481, code lost:
        if (r0.booleanValue() != false) goto L_0x0483;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x04ca, code lost:
        if (r6.equalsIgnoreCase("https") != false) goto L_0x04cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x054a, code lost:
        if (r4.equalsIgnoreCase("https") != false) goto L_0x054c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0110, code lost:
        if (r3.A0G.A0V != false) goto L_0x0112;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:336:0x06de, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:337:0x06df, code lost:
        X.C005505z.A00(-1786976395);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:338:0x06e5, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x013d, code lost:
        if ((r4 - r0) > com.facebook.proxygen.LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT) goto L_0x013f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0268, code lost:
        if (X.AnonymousClass1CT.class.isAssignableFrom(r12) == false) goto L_0x026a;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02ee A[Catch:{ all -> 0x06b6, all -> 0x06be }] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x02f3 A[Catch:{ all -> 0x06b6, all -> 0x06be }] */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x04af A[Catch:{ all -> 0x06b6, all -> 0x06be }] */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x05d8 A[Catch:{ all -> 0x06c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:274:0x05de A[Catch:{ all -> 0x06c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:281:0x0633 A[Catch:{ all -> 0x06c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0142 A[Catch:{ all -> 0x06de }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0242 A[Catch:{ all -> 0x06ce, all -> 0x06d6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0246 A[Catch:{ all -> 0x06ce, all -> 0x06d6 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BPY(android.app.Activity r23, android.os.Bundle r24) {
        /*
            r22 = this;
            java.lang.String r1 = "CollectiveLifetimeActivityListenerImpl.onBeforeActivityCreate[ActivityViewerContextListener.onBeforeActivityCreate]"
            r0 = -976815637(0xffffffffc5c6f9eb, float:-6367.2397)
            X.C005505z.A03(r1, r0)
            r8 = r23
            android.content.Intent r1 = r8.getIntent()     // Catch:{ all -> 0x06ee }
            r0 = 2
            java.lang.String r0 = X.C22298Ase.$const$string(r0)     // Catch:{ all -> 0x06ee }
            android.os.Parcelable r1 = r1.getParcelableExtra(r0)     // Catch:{ all -> 0x06ee }
            com.facebook.auth.viewercontext.ViewerContext r1 = (com.facebook.auth.viewercontext.ViewerContext) r1     // Catch:{ all -> 0x06ee }
            if (r1 == 0) goto L_0x0025
            X.5Ac r0 = new X.5Ac     // Catch:{ all -> 0x06ee }
            r0.<init>(r8)     // Catch:{ all -> 0x06ee }
            X.0aY r0 = r0.A00     // Catch:{ all -> 0x06ee }
            r0.CA8(r1)     // Catch:{ all -> 0x06ee }
        L_0x0025:
            r0 = 673710212(0x28280084, float:9.325985E-15)
            X.C005505z.A00(r0)
            r1 = 1209648874(0x4819c6ea, float:157467.66)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onBeforeActivityCreate[AppFalseRelaunchActivityListener.onBeforeActivityCreate]"
            X.C005505z.A03(r0, r1)
            r2 = 11
            int r1 = X.AnonymousClass1Y3.BLl     // Catch:{ all -> 0x06e6 }
            r9 = r22
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x06e6 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x06e6 }
            X.1Bi r4 = (X.C20211Bi) r4     // Catch:{ all -> 0x06e6 }
            java.lang.Class r12 = r8.getClass()     // Catch:{ all -> 0x06e6 }
            java.lang.Class<X.1eF> r0 = X.C28091eF.class
            boolean r0 = r0.isAssignableFrom(r12)     // Catch:{ all -> 0x06e6 }
            if (r0 == 0) goto L_0x00d7
            boolean r0 = r8.isTaskRoot()     // Catch:{ all -> 0x06e6 }
            r2 = 0
            if (r0 == 0) goto L_0x0056
            r2 = 0
            goto L_0x007d
        L_0x0056:
            android.content.Intent r1 = r8.getIntent()     // Catch:{ all -> 0x06e6 }
            if (r1 == 0) goto L_0x007d
            java.lang.String r0 = "com.facebook.fromSplashScreen"
            boolean r0 = r1.getBooleanExtra(r0, r2)     // Catch:{ all -> 0x06e6 }
            if (r0 != 0) goto L_0x007d
            r0 = 14
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ all -> 0x06e6 }
            boolean r0 = r1.hasCategory(r0)     // Catch:{ all -> 0x06e6 }
            if (r0 == 0) goto L_0x007d
            java.lang.String r1 = r1.getAction()     // Catch:{ all -> 0x06e6 }
            java.lang.String r0 = "android.intent.action.MAIN"
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x06e6 }
            if (r0 == 0) goto L_0x007d
            r2 = 1
        L_0x007d:
            if (r2 == 0) goto L_0x00d7
            java.lang.Class r3 = X.C20211Bi.A01     // Catch:{ all -> 0x06e6 }
            java.lang.String r2 = r12.getSimpleName()     // Catch:{ all -> 0x06e6 }
            java.lang.String r1 = " is not the root. "
            java.lang.String r0 = "Finishing activity instead of launching."
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x06e6 }
            X.C010708t.A06(r3, r0)     // Catch:{ all -> 0x06e6 }
            r8.finish()     // Catch:{ all -> 0x06e6 }
            com.facebook.analytics.DeprecatedAnalyticsLogger r3 = r4.A00     // Catch:{ all -> 0x06e6 }
            android.content.Intent r4 = r8.getIntent()     // Catch:{ all -> 0x06e6 }
            X.0nb r2 = new X.0nb     // Catch:{ all -> 0x06e6 }
            java.lang.String r0 = "app_false_relaunch_finish"
            r2.<init>(r0)     // Catch:{ all -> 0x06e6 }
            java.lang.String r1 = "pigeon_reserved_keyword_module"
            java.lang.String r0 = "app_false_relaunch"
            r2.A0D(r1, r0)     // Catch:{ all -> 0x06e6 }
            android.content.ComponentName r1 = r8.getCallingActivity()     // Catch:{ all -> 0x06e6 }
            java.lang.String r0 = "calling_activity"
            r2.A0C(r0, r1)     // Catch:{ all -> 0x06e6 }
            android.content.ComponentName r1 = r4.getComponent()     // Catch:{ all -> 0x06e6 }
            java.lang.String r0 = "intent_component"
            r2.A0C(r0, r1)     // Catch:{ all -> 0x06e6 }
            int r1 = r4.getFlags()     // Catch:{ all -> 0x06e6 }
            java.lang.String r0 = "intent_flags"
            r2.A09(r0, r1)     // Catch:{ all -> 0x06e6 }
            android.net.Uri r1 = r4.getData()     // Catch:{ all -> 0x06e6 }
            java.lang.String r0 = "intent_data"
            r2.A0C(r0, r1)     // Catch:{ all -> 0x06e6 }
            android.os.Bundle r1 = r4.getExtras()     // Catch:{ all -> 0x06e6 }
            java.lang.String r0 = "intent_extras"
            r2.A0C(r0, r1)     // Catch:{ all -> 0x06e6 }
            r3.A09(r2)     // Catch:{ all -> 0x06e6 }
        L_0x00d7:
            r0 = -222727337(0xfffffffff2b97357, float:-7.346453E30)
            X.C005505z.A00(r0)
            r1 = -171829033(0xfffffffff5c218d7, float:-4.9209443E32)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onBeforeActivityCreate[MessagingLoggerActivityListener.onBeforeActivityCreate]"
            X.C005505z.A03(r0, r1)
            r2 = 18
            int r1 = X.AnonymousClass1Y3.AbE     // Catch:{ all -> 0x06de }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x06de }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x06de }
            X.16y r7 = (X.C191416y) r7     // Catch:{ all -> 0x06de }
            X.16z r6 = r7.A01     // Catch:{ all -> 0x06de }
            X.069 r0 = r6.A04     // Catch:{ all -> 0x06de }
            long r10 = r0.now()     // Catch:{ all -> 0x06de }
            X.00T r20 = new X.00T     // Catch:{ all -> 0x06de }
            r20.<init>()     // Catch:{ all -> 0x06de }
            r20.A02()     // Catch:{ all -> 0x06de }
            r6.A00 = r10     // Catch:{ all -> 0x06de }
            X.0fv r3 = r6.A05     // Catch:{ all -> 0x06de }
            X.0Ud r0 = r3.A0G     // Catch:{ all -> 0x06de }
            boolean r0 = r0.A0U     // Catch:{ all -> 0x06de }
            if (r0 != 0) goto L_0x0112
            X.0Ud r0 = r3.A0G     // Catch:{ all -> 0x06de }
            boolean r1 = r0.A0V     // Catch:{ all -> 0x06de }
            r0 = 0
            if (r1 == 0) goto L_0x0113
        L_0x0112:
            r0 = 1
        L_0x0113:
            if (r0 == 0) goto L_0x0122
            com.facebook.quicklog.QuickPerformanceLogger r2 = r3.A05     // Catch:{ all -> 0x06de }
            r1 = 5505027(0x540003, float:7.714186E-39)
            java.lang.String r0 = "first_cold_start"
            r2.markerTag(r1, r0)     // Catch:{ all -> 0x06de }
            r0 = 1
            r3.A0R = r0     // Catch:{ all -> 0x06de }
        L_0x0122:
            long r4 = r6.A00     // Catch:{ all -> 0x06de }
            long r2 = r6.A01     // Catch:{ all -> 0x06de }
            long r16 = r4 - r2
            r13 = 0
            int r0 = (r2 > r13 ? 1 : (r2 == r13 ? 0 : -1))
            if (r0 == 0) goto L_0x013f
            r14 = 5000(0x1388, double:2.4703E-320)
            int r0 = (r16 > r14 ? 1 : (r16 == r14 ? 0 : -1))
            if (r0 <= 0) goto L_0x0143
            long r0 = r6.A03     // Catch:{ all -> 0x06de }
            int r13 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r13 > 0) goto L_0x0143
            long r4 = r4 - r0
            int r0 = (r4 > r14 ? 1 : (r4 == r14 ? 0 : -1))
            if (r0 <= 0) goto L_0x0143
        L_0x013f:
            r0 = 1
        L_0x0140:
            if (r0 == 0) goto L_0x020d
            goto L_0x0145
        L_0x0143:
            r0 = 0
            goto L_0x0140
        L_0x0145:
            X.069 r0 = r6.A04     // Catch:{ all -> 0x06de }
            long r0 = r0.now()     // Catch:{ all -> 0x06de }
            r6.A03 = r0     // Catch:{ all -> 0x06de }
            java.lang.String r1 = X.C191516z.A01(r8)     // Catch:{ all -> 0x06de }
            X.0fv r2 = r6.A05     // Catch:{ all -> 0x06de }
            boolean r0 = r2.A0M()     // Catch:{ all -> 0x06de }
            if (r0 != 0) goto L_0x01fb
            r2.A01 = r10     // Catch:{ all -> 0x06de }
            com.facebook.perf.startupstatemachine.StartupStateMachine r3 = r2.A0N     // Catch:{ all -> 0x06de }
            monitor-enter(r3)     // Catch:{ all -> 0x06de }
            r0 = 3
            r3.A02 = r0     // Catch:{ all -> 0x01f8 }
            monitor-exit(r3)     // Catch:{ all -> 0x06de }
            com.facebook.quicklog.QuickPerformanceLogger r13 = r2.A05     // Catch:{ all -> 0x06de }
            r14 = 5505028(0x540004, float:7.714187E-39)
            r15 = 0
            r16 = 0
            r21 = 0
            r19 = r15
            r17 = r10
            r13.markerStartWithCounterForLegacy(r14, r15, r16, r17, r19, r20, r21)     // Catch:{ all -> 0x06de }
            X.0ko r4 = r2.A0L     // Catch:{ all -> 0x06de }
            r3 = 1
            r4.A02(r3)     // Catch:{ all -> 0x06de }
            boolean r3 = r2.A0Q     // Catch:{ all -> 0x06de }
            r4 = 5505028(0x540004, float:7.714187E-39)
            if (r3 == 0) goto L_0x0192
            com.facebook.quicklog.QuickPerformanceLogger r5 = r2.A05     // Catch:{ all -> 0x06de }
            java.lang.String r3 = "background_cold_start"
            r5.markerTag(r14, r3)     // Catch:{ all -> 0x06de }
            boolean r3 = r2.A0R     // Catch:{ all -> 0x06de }
            if (r3 == 0) goto L_0x0192
            com.facebook.quicklog.QuickPerformanceLogger r5 = r2.A05     // Catch:{ all -> 0x06de }
            java.lang.String r3 = "first_cold_start"
            r5.markerTag(r14, r3)     // Catch:{ all -> 0x06de }
        L_0x0192:
            if (r1 == 0) goto L_0x019f
            com.facebook.quicklog.QuickPerformanceLogger r5 = r2.A05     // Catch:{ all -> 0x06de }
            java.lang.String r3 = "nav_"
            java.lang.String r3 = X.AnonymousClass08S.A0J(r3, r1)     // Catch:{ all -> 0x06de }
            r5.markerTag(r14, r3)     // Catch:{ all -> 0x06de }
        L_0x019f:
            com.facebook.quicklog.QuickPerformanceLogger r13 = r2.A05     // Catch:{ all -> 0x06de }
            java.lang.String r14 = "+"
            com.google.common.base.Joiner r11 = com.google.common.base.Joiner.on(r14)     // Catch:{ all -> 0x06de }
            java.lang.String r10 = r12.getSimpleName()     // Catch:{ all -> 0x06de }
            java.lang.Object[] r3 = new java.lang.Object[r15]     // Catch:{ all -> 0x06de }
            java.lang.String r5 = "activity"
            java.lang.String r3 = r11.join(r5, r10, r3)     // Catch:{ all -> 0x06de }
            r13.markerTag(r4, r3)     // Catch:{ all -> 0x06de }
            com.facebook.quicklog.QuickPerformanceLogger r3 = r2.A05     // Catch:{ all -> 0x06de }
            r3.markerAnnotate(r4, r5, r10)     // Catch:{ all -> 0x06de }
            android.content.Intent r3 = r8.getIntent()     // Catch:{ all -> 0x06de }
            r5 = 0
            if (r3 == 0) goto L_0x01e2
            java.lang.String r5 = r3.getAction()     // Catch:{ all -> 0x06de }
            if (r5 == 0) goto L_0x01ca
            r11 = r5
            goto L_0x01cc
        L_0x01ca:
            java.lang.String r11 = ""
        L_0x01cc:
            com.google.common.base.Joiner r3 = com.google.common.base.Joiner.on(r14)     // Catch:{ all -> 0x06de }
            java.lang.Object[] r0 = new java.lang.Object[r15]     // Catch:{ all -> 0x06de }
            java.lang.String r10 = "action"
            java.lang.String r3 = r3.join(r10, r11, r0)     // Catch:{ all -> 0x06de }
            com.facebook.quicklog.QuickPerformanceLogger r0 = r2.A05     // Catch:{ all -> 0x06de }
            r0.markerTag(r4, r3)     // Catch:{ all -> 0x06de }
            com.facebook.quicklog.QuickPerformanceLogger r0 = r2.A05     // Catch:{ all -> 0x06de }
            r0.markerAnnotate(r4, r10, r11)     // Catch:{ all -> 0x06de }
        L_0x01e2:
            X.0g6 r0 = r2.A0K     // Catch:{ all -> 0x06de }
            r0.A01(r4)     // Catch:{ all -> 0x06de }
            X.0fx r3 = r2.A0H     // Catch:{ all -> 0x06de }
            java.lang.String r0 = "android.intent.action.MAIN"
            boolean r0 = r0.equalsIgnoreCase(r5)     // Catch:{ all -> 0x06de }
            if (r0 == 0) goto L_0x01f4
            X.C08790fx.A04(r3, r4)     // Catch:{ all -> 0x06de }
        L_0x01f4:
            X.C08770fv.A03(r2)     // Catch:{ all -> 0x06de }
            goto L_0x01fb
        L_0x01f8:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x06de }
            throw r0     // Catch:{ all -> 0x06de }
        L_0x01fb:
            if (r1 == 0) goto L_0x020d
            X.0fv r0 = r6.A05     // Catch:{ all -> 0x06de }
            com.facebook.quicklog.QuickPerformanceLogger r2 = r0.A05     // Catch:{ all -> 0x06de }
            java.lang.String r0 = "nav_"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r1)     // Catch:{ all -> 0x06de }
            r0 = 5505027(0x540003, float:7.714186E-39)
            r2.markerTag(r0, r1)     // Catch:{ all -> 0x06de }
        L_0x020d:
            X.0ko r0 = r7.A00     // Catch:{ all -> 0x06de }
            r0.A05 = r12     // Catch:{ all -> 0x06de }
            X.16z r0 = r7.A01     // Catch:{ all -> 0x06de }
            r0.A02()     // Catch:{ all -> 0x06de }
            r0 = -996596716(0xffffffffc4992414, float:-1225.1274)
            X.C005505z.A00(r0)
            r1 = -1072779137(0xffffffffc00eb07f, float:-2.2295225)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onBeforeActivityCreate[StringResourcesActivityListener.onBeforeActivityCreate]"
            X.C005505z.A03(r0, r1)
            r2 = 25
            int r1 = X.AnonymousClass1Y3.B0n     // Catch:{ all -> 0x06d6 }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x06d6 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x06d6 }
            X.1eG r4 = (X.C28101eG) r4     // Catch:{ all -> 0x06d6 }
            java.lang.String r1 = "StringResourcesActivityListener.onBeforeActivityCreate"
            r0 = 1737517367(0x67906937, float:1.3639233E24)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x06d6 }
            android.content.res.Resources r0 = r8.getResources()     // Catch:{ all -> 0x06ce }
            X.0UN r1 = r4.A00     // Catch:{ all -> 0x06ce }
            boolean r0 = r0 instanceof X.AnonymousClass0YX     // Catch:{ all -> 0x06ce }
            if (r0 != 0) goto L_0x0246
            r0 = 2021075854(0x78772b8e, float:2.0052824E34)
            goto L_0x02b0
        L_0x0246:
            int r0 = X.AnonymousClass1Y3.BM9     // Catch:{ all -> 0x06ce }
            r2 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r0, r1)     // Catch:{ all -> 0x06ce }
            X.0Yj r0 = (X.C05290Yj) r0     // Catch:{ all -> 0x06ce }
            r0.A05()     // Catch:{ all -> 0x06ce }
            int r1 = X.AnonymousClass1Y3.BM9     // Catch:{ all -> 0x06ce }
            X.0UN r0 = r4.A00     // Catch:{ all -> 0x06ce }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x06ce }
            X.0Yj r0 = (X.C05290Yj) r0     // Catch:{ all -> 0x06ce }
            r0.A09()     // Catch:{ all -> 0x06ce }
            if (r23 == 0) goto L_0x026a
            java.lang.Class<X.1CT> r0 = X.AnonymousClass1CT.class
            boolean r1 = r0.isAssignableFrom(r12)     // Catch:{ all -> 0x06ce }
            r0 = 1
            if (r1 != 0) goto L_0x026b
        L_0x026a:
            r0 = 0
        L_0x026b:
            if (r0 != 0) goto L_0x02ad
            int r1 = X.AnonymousClass1Y3.BM9     // Catch:{ all -> 0x06ce }
            X.0UN r0 = r4.A00     // Catch:{ all -> 0x06ce }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x06ce }
            X.0Yj r0 = (X.C05290Yj) r0     // Catch:{ all -> 0x06ce }
            boolean r0 = r0.A0A()     // Catch:{ all -> 0x06ce }
            if (r0 == 0) goto L_0x0285
            if (r23 == 0) goto L_0x02ad
            java.lang.Class<X.1dv> r0 = X.C27891dv.class
            r0.isAssignableFrom(r12)     // Catch:{ all -> 0x06ce }
            goto L_0x02ad
        L_0x0285:
            android.content.Intent r3 = new android.content.Intent     // Catch:{ all -> 0x06ce }
            java.lang.Class<com.facebook.resources.impl.WaitingForStringsActivity> r0 = com.facebook.resources.impl.WaitingForStringsActivity.class
            r3.<init>(r8, r0)     // Catch:{ all -> 0x06ce }
            android.content.Intent r1 = r8.getIntent()     // Catch:{ all -> 0x06ce }
            java.lang.String r0 = "return_intent"
            r3.putExtra(r0, r1)     // Catch:{ all -> 0x06ce }
            int r2 = X.AnonymousClass1Y3.A7S     // Catch:{ all -> 0x06ce }
            X.0UN r1 = r4.A00     // Catch:{ all -> 0x06ce }
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x06ce }
            com.facebook.content.SecureContextHelper r0 = (com.facebook.content.SecureContextHelper) r0     // Catch:{ all -> 0x06ce }
            r0.startFacebookActivity(r3, r8)     // Catch:{ all -> 0x06ce }
            r8.finish()     // Catch:{ all -> 0x06ce }
            r0 = -127485859(0xfffffffff866b85d, float:-1.8718244E34)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06d6 }
            goto L_0x02b3
        L_0x02ad:
            r0 = -1747593101(0xffffffff97d5d873, float:-1.3819442E-24)
        L_0x02b0:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06d6 }
        L_0x02b3:
            r0 = -534115109(0xffffffffe02a0cdb, float:-4.901364E19)
            X.C005505z.A00(r0)
            r1 = 1895520443(0x70fb58bb, float:6.2230405E29)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onBeforeActivityCreate[IncomingIntentLifetimeActivityListener.onBeforeActivityCreate]"
            X.C005505z.A03(r0, r1)
            r2 = 26
            int r1 = X.AnonymousClass1Y3.B06     // Catch:{ all -> 0x06c6 }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x06c6 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x06c6 }
            X.0yU r2 = (X.C17200yU) r2     // Catch:{ all -> 0x06c6 }
            X.1eH r3 = r2.A00     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = "IntentLogger.onIncomingIntent"
            r0 = 448777572(0x1abfcd64, float:7.9327575E-23)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x06c6 }
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x06be }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x06be }
            r6 = 2
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x06be }
            X.1Yd r4 = (X.C25051Yd) r4     // Catch:{ all -> 0x06be }
            r0 = 283223028599034(0x10197000308fa, double:1.399307685418956E-309)
            boolean r0 = r4.Aem(r0)     // Catch:{ all -> 0x06be }
            r4 = 0
            if (r0 != 0) goto L_0x02f3
            r0 = 787317833(0x2eed8449, float:1.0801011E-10)
            goto L_0x05b9
        L_0x02f3:
            android.content.Intent r7 = r8.getIntent()     // Catch:{ all -> 0x06be }
            java.lang.String r1 = "IntentLogger.parseIntent"
            r0 = -1306881156(0xffffffffb21a937c, float:-8.997514E-9)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x06be }
            int r1 = X.AnonymousClass1Y3.BHz     // Catch:{ JSONException -> 0x032a }
            X.0UN r0 = r3.A00     // Catch:{ JSONException -> 0x032a }
            r10 = 3
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r10, r1, r0)     // Catch:{ JSONException -> 0x032a }
            X.380 r1 = (X.AnonymousClass380) r1     // Catch:{ JSONException -> 0x032a }
            monitor-enter(r1)     // Catch:{ JSONException -> 0x032a }
            X.2iE r5 = r1.A02     // Catch:{ all -> 0x0327 }
            monitor-exit(r1)     // Catch:{ JSONException -> 0x032a }
            int r1 = X.AnonymousClass1Y3.BHz     // Catch:{ JSONException -> 0x032a }
            X.0UN r0 = r3.A00     // Catch:{ JSONException -> 0x032a }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r10, r1, r0)     // Catch:{ JSONException -> 0x032a }
            X.380 r1 = (X.AnonymousClass380) r1     // Catch:{ JSONException -> 0x032a }
            monitor-enter(r1)     // Catch:{ JSONException -> 0x032a }
            X.2iE r0 = r1.A01     // Catch:{ all -> 0x0327 }
            monitor-exit(r1)     // Catch:{ JSONException -> 0x032a }
            X.0bp r5 = X.C49822cv.A00(r7, r5, r0)     // Catch:{ JSONException -> 0x032a }
            r0 = 1567481750(0x5d6ddf96, float:1.07128648E18)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06be }
            goto L_0x0344
        L_0x0327:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ JSONException -> 0x032a }
            throw r0     // Catch:{ JSONException -> 0x032a }
        L_0x032a:
            r10 = move-exception
            r5 = 0
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x06b6 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x06b6 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x06b6 }
            X.09P r5 = (X.AnonymousClass09P) r5     // Catch:{ all -> 0x06b6 }
            java.lang.String r1 = "IntentLogger"
            java.lang.String r0 = "Error parsing incoming intent."
            r5.softReport(r1, r0, r10)     // Catch:{ all -> 0x06b6 }
            r0 = 185488314(0xb0e53ba, float:2.7411194E-32)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06be }
            r5 = r4
        L_0x0344:
            if (r5 != 0) goto L_0x034b
            r0 = 1919418911(0x7268021f, float:4.5953975E30)
            goto L_0x05b9
        L_0x034b:
            org.json.JSONObject r4 = r5.A01     // Catch:{ all -> 0x06be }
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x06be }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x06be }
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x06be }
            X.1Yd r10 = (X.C25051Yd) r10     // Catch:{ all -> 0x06be }
            r0 = 2306126227947129079(0x20010196000008f7, double:1.5854749670142703E-154)
            boolean r0 = r10.Aem(r0)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x037a
            int r10 = X.AnonymousClass1Y3.A7U     // Catch:{ all -> 0x06be }
            X.0UN r1 = r3.A00     // Catch:{ all -> 0x06be }
            r0 = 4
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r0, r10, r1)     // Catch:{ all -> 0x06be }
            X.2Fs r10 = (X.C43792Fs) r10     // Catch:{ all -> 0x06be }
            monitor-enter(r10)     // Catch:{ all -> 0x06be }
            java.util.Set r1 = r10.A00     // Catch:{ all -> 0x06b3 }
            monitor-exit(r10)     // Catch:{ all -> 0x06be }
            if (r1 != 0) goto L_0x0471
            r0 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
            goto L_0x047d
        L_0x037a:
            int r10 = X.AnonymousClass1Y3.BHz     // Catch:{ all -> 0x06be }
            X.0UN r1 = r3.A00     // Catch:{ all -> 0x06be }
            r0 = 3
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r10, r1)     // Catch:{ all -> 0x06be }
            X.380 r1 = (X.AnonymousClass380) r1     // Catch:{ all -> 0x06be }
            monitor-enter(r1)     // Catch:{ all -> 0x06be }
            X.3yN r0 = r1.A00     // Catch:{ all -> 0x06b0 }
            monitor-exit(r1)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x043a
            java.util.List r1 = r0.A00     // Catch:{ all -> 0x06be }
            r0 = 0
            java.lang.Boolean r15 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
            if (r1 == 0) goto L_0x042a
            java.util.Iterator r14 = r1.iterator()     // Catch:{ all -> 0x06be }
        L_0x0398:
            boolean r0 = r14.hasNext()     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x042a
            java.lang.Object r1 = r14.next()     // Catch:{ all -> 0x06be }
            java.util.List r1 = (java.util.List) r1     // Catch:{ all -> 0x06be }
            r0 = 1
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
            r16 = r13
            java.util.Iterator r11 = r1.iterator()     // Catch:{ all -> 0x06be }
        L_0x03af:
            boolean r0 = r11.hasNext()     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x042d
            java.lang.Object r10 = r11.next()     // Catch:{ all -> 0x06be }
            X.8oS r10 = (X.C188158oS) r10     // Catch:{ all -> 0x06be }
            java.util.regex.Pattern r0 = r10.A00     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x03f6
            java.lang.String r1 = r0.toString()     // Catch:{ all -> 0x06be }
            java.lang.String r0 = "_null_"
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x06be }
            if (r0 != 0) goto L_0x03f6
            java.util.regex.Pattern r0 = r10.A00     // Catch:{ all -> 0x06be }
            java.lang.String r1 = r0.toString()     // Catch:{ all -> 0x06be }
            java.lang.String r0 = "_any_"
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x0409
            java.lang.String r0 = X.C188158oS.A00(r10, r7)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x03e6
            boolean r1 = r0.isEmpty()     // Catch:{ all -> 0x06be }
            r0 = 0
            if (r1 == 0) goto L_0x03e7
        L_0x03e6:
            r0 = 1
        L_0x03e7:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x06be }
            r0 = r0 ^ 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
            goto L_0x0422
        L_0x03f6:
            java.lang.String r0 = X.C188158oS.A00(r10, r7)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x0403
            boolean r1 = r0.isEmpty()     // Catch:{ all -> 0x06be }
            r0 = 0
            if (r1 == 0) goto L_0x0404
        L_0x0403:
            r0 = 1
        L_0x0404:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
            goto L_0x0422
        L_0x0409:
            java.util.regex.Pattern r1 = r10.A00     // Catch:{ all -> 0x06be }
            r0 = r15
            if (r1 == 0) goto L_0x0422
            java.lang.String r1 = X.C188158oS.A00(r10, r7)     // Catch:{ all -> 0x06be }
            if (r1 == 0) goto L_0x0422
            java.util.regex.Pattern r0 = r10.A00     // Catch:{ all -> 0x06be }
            java.util.regex.Matcher r0 = r0.matcher(r1)     // Catch:{ all -> 0x06be }
            boolean r0 = r0.matches()     // Catch:{ all -> 0x06be }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
        L_0x0422:
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x06be }
            if (r0 != 0) goto L_0x03af
            r13 = r15
            goto L_0x042d
        L_0x042a:
            r16 = r15
            goto L_0x0433
        L_0x042d:
            boolean r0 = r13.booleanValue()     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x0398
        L_0x0433:
            boolean r1 = r16.booleanValue()     // Catch:{ all -> 0x06be }
            r0 = 0
            if (r1 != 0) goto L_0x043b
        L_0x043a:
            r0 = 1
        L_0x043b:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x045b
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x06be }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x06be }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x06be }
            X.1Yd r7 = (X.C25051Yd) r7     // Catch:{ all -> 0x06be }
            r0 = 283223028402424(0x10197000008f8, double:1.399307684447573E-309)
            boolean r0 = r7.Aem(r0)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x0486
            goto L_0x0483
        L_0x045b:
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x06be }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x06be }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x06be }
            X.1Yd r7 = (X.C25051Yd) r7     // Catch:{ all -> 0x06be }
            r0 = 2306126232242161913(0x20010197000108f9, double:1.5854763896015772E-154)
            boolean r0 = r7.Aem(r0)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x0486
            goto L_0x0483
        L_0x0471:
            java.lang.String r0 = r12.getName()     // Catch:{ all -> 0x06be }
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x06be }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x06be }
        L_0x047d:
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x037a
        L_0x0483:
            X.C28111eH.A01(r3, r4, r8)     // Catch:{ all -> 0x06be }
        L_0x0486:
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x06be }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x06be }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x06be }
            X.1Yd r4 = (X.C25051Yd) r4     // Catch:{ all -> 0x06be }
            r0 = 283223028664571(0x10197000408fb, double:1.39930768574275E-309)
            boolean r0 = r4.Aem(r0)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x05b1
            java.util.List r1 = r5.A00     // Catch:{ all -> 0x06be }
            if (r1 == 0) goto L_0x05b1
            boolean r0 = r1.isEmpty()     // Catch:{ all -> 0x06be }
            if (r0 != 0) goto L_0x05b1
            java.util.Iterator r15 = r1.iterator()     // Catch:{ all -> 0x06be }
        L_0x04a9:
            boolean r0 = r15.hasNext()     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x05b1
            java.lang.Object r7 = r15.next()     // Catch:{ all -> 0x06be }
            android.net.Uri r7 = (android.net.Uri) r7     // Catch:{ all -> 0x06be }
            java.lang.String r6 = r7.getScheme()     // Catch:{ all -> 0x06be }
            if (r6 == 0) goto L_0x04cc
            java.lang.String r0 = "http"
            boolean r0 = r6.equalsIgnoreCase(r0)     // Catch:{ all -> 0x06be }
            if (r0 != 0) goto L_0x04cc
            java.lang.String r0 = "https"
            boolean r1 = r6.equalsIgnoreCase(r0)     // Catch:{ all -> 0x06be }
            r0 = 1
            if (r1 == 0) goto L_0x04cd
        L_0x04cc:
            r0 = 0
        L_0x04cd:
            if (r0 == 0) goto L_0x04a9
            java.util.Set r0 = r7.getQueryParameterNames()     // Catch:{ all -> 0x06be }
            java.util.Iterator r14 = r0.iterator()     // Catch:{ all -> 0x06be }
        L_0x04d7:
            boolean r0 = r14.hasNext()     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x04a9
            java.lang.Object r13 = r14.next()     // Catch:{ all -> 0x06be }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x06be }
            java.lang.String r10 = r7.getQueryParameter(r13)     // Catch:{ all -> 0x06be }
            if (r10 == 0) goto L_0x04d7
            java.lang.String r4 = ":"
            boolean r0 = r10.contains(r4)     // Catch:{ all -> 0x06be }
            r1 = 0
            if (r0 == 0) goto L_0x0518
            int r0 = r10.indexOf(r4)     // Catch:{ all -> 0x06be }
            java.lang.String r11 = r10.substring(r1, r0)     // Catch:{ all -> 0x06be }
            boolean r0 = r11.isEmpty()     // Catch:{ all -> 0x06be }
            if (r0 != 0) goto L_0x0518
            char r0 = r11.charAt(r1)     // Catch:{ all -> 0x06be }
            boolean r0 = java.lang.Character.isLetter(r0)     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x0518
            r4 = 1
            goto L_0x051e
        L_0x050c:
            r0 = 43
            if (r1 == r0) goto L_0x051c
            r0 = 45
            if (r1 == r0) goto L_0x051c
            r0 = 46
            if (r1 == r0) goto L_0x051c
        L_0x0518:
            r0 = 0
        L_0x0519:
            if (r0 == 0) goto L_0x04d7
            goto L_0x0531
        L_0x051c:
            int r4 = r4 + 1
        L_0x051e:
            int r0 = r11.length()     // Catch:{ all -> 0x06be }
            if (r4 >= r0) goto L_0x052f
            char r1 = r11.charAt(r4)     // Catch:{ all -> 0x06be }
            boolean r0 = java.lang.Character.isLetterOrDigit(r1)     // Catch:{ all -> 0x06be }
            if (r0 != 0) goto L_0x051c
            goto L_0x050c
        L_0x052f:
            r0 = 1
            goto L_0x0519
        L_0x0531:
            android.net.Uri r11 = android.net.Uri.parse(r10)     // Catch:{ all -> 0x06be }
            java.lang.String r4 = r11.getScheme()     // Catch:{ all -> 0x06be }
            if (r4 == 0) goto L_0x054c
            java.lang.String r0 = "http"
            boolean r0 = r4.equalsIgnoreCase(r0)     // Catch:{ all -> 0x06be }
            if (r0 != 0) goto L_0x054c
            java.lang.String r0 = "https"
            boolean r1 = r4.equalsIgnoreCase(r0)     // Catch:{ all -> 0x06be }
            r0 = 1
            if (r1 == 0) goto L_0x054d
        L_0x054c:
            r0 = 0
        L_0x054d:
            if (r0 == 0) goto L_0x04d7
            r10 = 1
            int r1 = X.AnonymousClass1Y3.Ap7     // Catch:{ all -> 0x06be }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x06be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r10, r1, r0)     // Catch:{ all -> 0x06be }
            X.1ZE r1 = (X.AnonymousClass1ZE) r1     // Catch:{ all -> 0x06be }
            java.lang.String r0 = "android_security_fb4a_intent_with_redirect_uri"
            X.0bW r1 = r1.A01(r0)     // Catch:{ all -> 0x06be }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r10 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x06be }
            r0 = 36
            r10.<init>(r1, r0)     // Catch:{ all -> 0x06be }
            boolean r0 = r10.A0G()     // Catch:{ all -> 0x06be }
            if (r0 == 0) goto L_0x04d7
            java.lang.String r0 = "param_name_with_redirect_uri"
            r10.A0D(r0, r13)     // Catch:{ all -> 0x06be }
            java.lang.String r0 = "uri_scheme"
            r10.A0D(r0, r6)     // Catch:{ all -> 0x06be }
            java.lang.String r1 = r7.getAuthority()     // Catch:{ all -> 0x06be }
            java.lang.String r0 = "uri_authority"
            r10.A0D(r0, r1)     // Catch:{ all -> 0x06be }
            java.lang.String r0 = "redirect_uri_scheme"
            r10.A0D(r0, r4)     // Catch:{ all -> 0x06be }
            java.lang.String r1 = r11.getAuthority()     // Catch:{ all -> 0x06be }
            java.lang.String r0 = "redirect_uri_authority"
            r10.A0D(r0, r1)     // Catch:{ all -> 0x06be }
            X.2iE r0 = X.AnonymousClass2SH.A04     // Catch:{ all -> 0x06be }
            X.2SH r0 = X.C70833bM.A00(r7, r0)     // Catch:{ all -> 0x06be }
            java.lang.String r4 = r0.A00()     // Catch:{ all -> 0x06be }
            java.lang.String r1 = "sanitized_uri"
            r10.A0D(r1, r4)     // Catch:{ all -> 0x06be }
            X.2iE r0 = X.AnonymousClass2SH.A04     // Catch:{ all -> 0x06be }
            X.2SH r0 = X.C70833bM.A00(r11, r0)     // Catch:{ all -> 0x06be }
            java.lang.String r4 = r0.A00()     // Catch:{ all -> 0x06be }
            java.lang.String r1 = "sanitized_redirect_uri"
            r10.A0D(r1, r4)     // Catch:{ all -> 0x06be }
            r10.A06()     // Catch:{ all -> 0x06be }
            goto L_0x04d7
        L_0x05b1:
            r0 = -1452355494(0xffffffffa96ed05a, float:-5.3027332E-14)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06c6 }
            r4 = r5
            goto L_0x05bc
        L_0x05b9:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06c6 }
        L_0x05bc:
            com.facebook.secure.intentswitchoff.ActivityIntentSwitchOffDI r3 = r2.A01     // Catch:{ all -> 0x06c6 }
            android.content.Intent r2 = r8.getIntent()     // Catch:{ all -> 0x06c6 }
            X.06v r0 = r3.A00     // Catch:{ all -> 0x06c6 }
            boolean r0 = r0.CDm()     // Catch:{ all -> 0x06c6 }
            if (r0 == 0) goto L_0x062f
            android.net.Uri r10 = r2.getData()     // Catch:{ all -> 0x06c6 }
            if (r10 == 0) goto L_0x062f
            X.06v r0 = r3.A00     // Catch:{ all -> 0x06c6 }
            java.util.Map r6 = r0.Ajo()     // Catch:{ all -> 0x06c6 }
            if (r10 != 0) goto L_0x0633
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x06c6 }
        L_0x05da:
            java.lang.Integer r1 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x06c6 }
            if (r0 == r1) goto L_0x062f
            X.1ZE r6 = r3.A00     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = "android_security_deeplink_handler_blacklisted_deeplink"
            X.0bW r6 = r6.A01(r1)     // Catch:{ all -> 0x06c6 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r7 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x06c6 }
            r1 = 33
            r7.<init>(r6, r1)     // Catch:{ all -> 0x06c6 }
            boolean r1 = r7.A0G()     // Catch:{ all -> 0x06c6 }
            if (r1 == 0) goto L_0x0628
            java.lang.String r6 = r8.getPackageName()     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = r12.getName()     // Catch:{ all -> 0x06c6 }
            java.lang.Object[] r6 = new java.lang.Object[]{r6, r1}     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = "%s/%s"
            java.lang.String r6 = java.lang.String.format(r1, r6)     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = "app_name"
            r7.A0D(r1, r6)     // Catch:{ all -> 0x06c6 }
            java.lang.String r6 = r10.getScheme()     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = "scheme"
            r7.A0D(r1, r6)     // Catch:{ all -> 0x06c6 }
            java.lang.String r6 = r10.getAuthority()     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = "authority"
            r7.A0D(r1, r6)     // Catch:{ all -> 0x06c6 }
            java.lang.String r6 = r10.getPath()     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = "path"
            r7.A0D(r1, r6)     // Catch:{ all -> 0x06c6 }
            r7.A06()     // Catch:{ all -> 0x06c6 }
        L_0x0628:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x06c6 }
            if (r0 != r1) goto L_0x062f
            r3.A05(r8, r2)     // Catch:{ all -> 0x06c6 }
        L_0x062f:
            r3.A03(r8, r8, r2, r4)     // Catch:{ all -> 0x06c6 }
            goto L_0x0699
        L_0x0633:
            if (r6 == 0) goto L_0x0695
            boolean r0 = r6.isEmpty()     // Catch:{ all -> 0x06c6 }
            if (r0 != 0) goto L_0x0695
            java.lang.String r1 = r12.getName()     // Catch:{ all -> 0x06c6 }
            boolean r0 = r6.containsKey(r1)     // Catch:{ all -> 0x06c6 }
            if (r0 != 0) goto L_0x0648
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x06c6 }
            goto L_0x05da
        L_0x0648:
            java.lang.Object r7 = r6.get(r1)     // Catch:{ all -> 0x06c6 }
            X.1Ts r7 = (X.C24461Ts) r7     // Catch:{ all -> 0x06c6 }
            java.lang.String r6 = r10.getScheme()     // Catch:{ all -> 0x06c6 }
            java.lang.String r1 = r10.getAuthority()     // Catch:{ all -> 0x06c6 }
            if (r6 != 0) goto L_0x065b
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x06c6 }
            goto L_0x05da
        L_0x065b:
            java.util.Map r0 = r7.A00     // Catch:{ all -> 0x06c6 }
            boolean r0 = r0.containsKey(r6)     // Catch:{ all -> 0x06c6 }
            if (r0 != 0) goto L_0x066f
            boolean r0 = r7.A02     // Catch:{ all -> 0x06c6 }
            if (r0 == 0) goto L_0x066b
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x06c6 }
            goto L_0x05da
        L_0x066b:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x06c6 }
            goto L_0x05da
        L_0x066f:
            if (r1 != 0) goto L_0x0673
            java.lang.String r1 = ""
        L_0x0673:
            java.util.Map r0 = r7.A00     // Catch:{ all -> 0x06c6 }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ all -> 0x06c6 }
            java.util.regex.Pattern r0 = (java.util.regex.Pattern) r0     // Catch:{ all -> 0x06c6 }
            java.util.regex.Matcher r0 = r0.matcher(r1)     // Catch:{ all -> 0x06c6 }
            boolean r0 = r0.matches()     // Catch:{ all -> 0x06c6 }
            if (r0 != 0) goto L_0x0691
            boolean r0 = r7.A01     // Catch:{ all -> 0x06c6 }
            if (r0 == 0) goto L_0x068d
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x06c6 }
            goto L_0x05da
        L_0x068d:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x06c6 }
            goto L_0x05da
        L_0x0691:
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x06c6 }
            goto L_0x05da
        L_0x0695:
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x06c6 }
            goto L_0x05da
        L_0x0699:
            r3 = 5
            r0 = -690099557(0xffffffffd6ddea9b, float:-1.21999846E14)
            X.C005505z.A00(r0)
            r2 = 30
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r9.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r3)
            return
        L_0x06b0:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x06be }
            goto L_0x06b5
        L_0x06b3:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x06be }
        L_0x06b5:
            throw r0     // Catch:{ all -> 0x06be }
        L_0x06b6:
            r1 = move-exception
            r0 = 1602665522(0x5f86bc32, float:1.941738E19)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06be }
            throw r1     // Catch:{ all -> 0x06be }
        L_0x06be:
            r1 = move-exception
            r0 = 1209736309(0x481b1c75, float:158833.83)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06c6 }
            throw r1     // Catch:{ all -> 0x06c6 }
        L_0x06c6:
            r1 = move-exception
            r0 = -2016292602(0xffffffff87d1d106, float:-3.1569678E-34)
            X.C005505z.A00(r0)
            throw r1
        L_0x06ce:
            r1 = move-exception
            r0 = -967304771(0xffffffffc65819bd, float:-13830.435)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x06d6 }
            throw r1     // Catch:{ all -> 0x06d6 }
        L_0x06d6:
            r1 = move-exception
            r0 = 397166458(0x17ac477a, float:1.1133283E-24)
            X.C005505z.A00(r0)
            throw r1
        L_0x06de:
            r1 = move-exception
            r0 = -1786976395(0xffffffff957ce775, float:-5.1073582E-26)
            X.C005505z.A00(r0)
            throw r1
        L_0x06e6:
            r1 = move-exception
            r0 = 246810078(0xeb605de, float:4.4872114E-30)
            X.C005505z.A00(r0)
            throw r1
        L_0x06ee:
            r1 = move-exception
            r0 = -1172925051(0xffffffffba169585, float:-5.744326E-4)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27851dr.BPY(android.app.Activity, android.os.Bundle):void");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0051 A[Catch:{ all -> 0x013b, all -> 0x014b }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008d A[Catch:{ all -> 0x0133 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b9 A[Catch:{ all -> 0x0133 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00db A[EDGE_INSN: B:56:0x00db->B:35:0x00db ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BPo(android.app.Activity r8, android.os.Bundle r9) {
        /*
            r7 = this;
            java.lang.String r1 = "CollectiveLifetimeActivityListenerImpl.onBeforeSuperOnCreate[AuthenticatedActivityHelper.onBeforeSuperOnCreate]"
            r0 = 1203684767(0x47bec59f, float:97675.24)
            X.C005505z.A03(r1, r0)
            r2 = 5
            int r1 = X.AnonymousClass1Y3.BMr     // Catch:{ all -> 0x014b }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x014b }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x014b }
            X.1dt r3 = (X.C27871dt) r3     // Catch:{ all -> 0x014b }
            java.lang.String r2 = "loggedInUser"
            java.lang.String r1 = "AuthenticatedActivityHelper.getLoggedInUser"
            r0 = 634484081(0x25d17571, float:3.6335302E-16)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x014b }
            if (r9 == 0) goto L_0x002c
            boolean r0 = r9.containsKey(r2)     // Catch:{ all -> 0x0143 }
            if (r0 == 0) goto L_0x002c
            java.lang.String r0 = r9.getString(r2)     // Catch:{ all -> 0x0143 }
            r3.A07 = r0     // Catch:{ all -> 0x0143 }
            goto L_0x0036
        L_0x002c:
            X.0Tq r0 = r3.A08     // Catch:{ all -> 0x0143 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0143 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0143 }
            r3.A07 = r0     // Catch:{ all -> 0x0143 }
        L_0x0036:
            r0 = -802170194(0xffffffffd02fdaae, float:-1.18013768E10)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x014b }
            r1 = 1660628771(0x62fb2f23, float:2.3167647E21)
            java.lang.String r0 = "AuthenticatedActivityHelper.isAuthenticated"
            X.C005505z.A03(r0, r1)     // Catch:{ all -> 0x014b }
            boolean r0 = X.C27871dt.A02(r3, r8)     // Catch:{ all -> 0x013b }
            if (r0 == 0) goto L_0x0051
            r0 = 771065288(0x2df585c8, float:2.7912686E-11)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x014b }
            goto L_0x0069
        L_0x0051:
            r0 = -1295553031(0xffffffffb2c76df9, float:-2.3216659E-8)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x014b }
            r2 = 3
            int r1 = X.AnonymousClass1Y3.Am8     // Catch:{ all -> 0x014b }
            X.0UN r0 = r3.A06     // Catch:{ all -> 0x014b }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x014b }
            X.2I0 r1 = (X.AnonymousClass2I0) r1     // Catch:{ all -> 0x014b }
            r0 = 0
            r1.A00(r8, r0)     // Catch:{ all -> 0x014b }
            r8.finish()     // Catch:{ all -> 0x014b }
        L_0x0069:
            r0 = 2114336367(0x7e06366f, float:4.4599797E37)
            X.C005505z.A00(r0)
            r1 = -1908018502(0xffffffff8e45f2ba, float:-2.4398993E-30)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onBeforeSuperOnCreate[AppStateManager.onBeforeSuperOnCreate]"
            X.C005505z.A03(r0, r1)
            r2 = 12
            int r1 = X.AnonymousClass1Y3.B5j     // Catch:{ all -> 0x0133 }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x0133 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0133 }
            X.0Ud r3 = (X.AnonymousClass0Ud) r3     // Catch:{ all -> 0x0133 }
            long r1 = r3.A0F     // Catch:{ all -> 0x0133 }
            r4 = 8
            r5 = 0
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x009d
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x0133 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x0133 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x0133 }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x0133 }
            long r0 = r0.now()     // Catch:{ all -> 0x0133 }
            r3.A0F = r0     // Catch:{ all -> 0x0133 }
        L_0x009d:
            if (r8 == 0) goto L_0x00db
            android.content.Intent r0 = r8.getIntent()     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x00db
            android.content.Intent r0 = r8.getIntent()     // Catch:{ all -> 0x0133 }
            java.util.Set r0 = r0.getCategories()     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x00db
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0133 }
        L_0x00b3:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x00db
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0133 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0133 }
            r0 = 14
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ all -> 0x0133 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0133 }
            if (r0 == 0) goto L_0x00b3
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x0133 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x0133 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x0133 }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x0133 }
            long r0 = r0.now()     // Catch:{ all -> 0x0133 }
            r3.A0G = r0     // Catch:{ all -> 0x0133 }
        L_0x00db:
            r0 = 282994659(0x10de27e3, float:8.7625016E-29)
            X.C005505z.A00(r0)
            r1 = 1146337819(0x4453ba1b, float:846.9079)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onBeforeSuperOnCreate[AppStartupNotifierActivityListener.onCreated]"
            X.C005505z.A03(r0, r1)
            r2 = 24
            int r1 = X.AnonymousClass1Y3.AEn     // Catch:{ all -> 0x012b }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x012b }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x012b }
            X.0u8 r0 = (X.C14790u8) r0     // Catch:{ all -> 0x012b }
            int r2 = X.AnonymousClass1Y3.AMF     // Catch:{ all -> 0x012b }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x012b }
            r0 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x012b }
            X.1Zw r3 = (X.C25501Zw) r3     // Catch:{ all -> 0x012b }
            int r2 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x012b }
            X.0UN r1 = r3.A04     // Catch:{ all -> 0x012b }
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x012b }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x012b }
            long r0 = r0.now()     // Catch:{ all -> 0x012b }
            r3.A00 = r0     // Catch:{ all -> 0x012b }
            X.C25501Zw.A01(r3, r0)     // Catch:{ all -> 0x012b }
            r3 = 3
            r0 = -1102378357(0xffffffffbe4b0a8b, float:-0.1982824)
            X.C005505z.A00(r0)
            r2 = 30
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r3)
            return
        L_0x012b:
            r1 = move-exception
            r0 = 1203232910(0x47b7e08e, float:94145.11)
            X.C005505z.A00(r0)
            throw r1
        L_0x0133:
            r1 = move-exception
            r0 = 639088622(0x2617b7ee, float:5.263792E-16)
            X.C005505z.A00(r0)
            throw r1
        L_0x013b:
            r1 = move-exception
            r0 = -261903162(0xfffffffff063acc6, float:-2.8184787E29)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x014b }
            goto L_0x014a
        L_0x0143:
            r1 = move-exception
            r0 = -466666311(0xffffffffe42f3cb9, float:-1.2930223E22)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x014b }
        L_0x014a:
            throw r1     // Catch:{ all -> 0x014b }
        L_0x014b:
            r1 = move-exception
            r0 = -653822360(0xffffffffd9077668, float:-2.38308193E15)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27851dr.BPo(android.app.Activity, android.os.Bundle):void");
    }

    public void BTK(Activity activity, Configuration configuration) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public void BTy(Activity activity) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public Dialog BUl(Activity activity, int i) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
        return null;
    }

    public void BUs(Menu menu) {
        BEY bey = (BEY) AnonymousClass1XX.A02(28, AnonymousClass1Y3.AUb, this.A00);
        ((C134756Ry) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BRZ, bey.A00)).A01(menu, bey.A01.getMenuInflater(), AnonymousClass07B.A00);
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(1);
    }

    public void BVx(Activity activity) {
        C27871dt r4 = (C27871dt) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMr, this.A00);
        Activity activity2 = activity;
        if (!(activity instanceof C27891dv)) {
            C17310yh r3 = r4.A04;
            if (r3 != null) {
                if (r3.A00) {
                    r3.A02.A01(r3.A01);
                    r3.A00 = false;
                }
                r4.A04 = null;
            }
            C06790c5 r0 = r4.A02;
            if (r0 != null) {
                r0.A01();
                r4.A02 = null;
            }
            C06790c5 r02 = r4.A00;
            if (r02 != null) {
                r02.A01();
                r4.A00 = null;
            }
            C06790c5 r03 = r4.A03;
            if (r03 != null) {
                r03.A01();
                r4.A03 = null;
            }
            C06790c5 r04 = r4.A01;
            if (r04 != null) {
                r04.A01();
            }
        }
        new AnonymousClass0tT((AnonymousClass0t4) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BSw, this.A00), 4, activity2, Long.MIN_VALUE).A00();
        MemoryLeakListener memoryLeakListener = (MemoryLeakListener) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AVh, this.A00);
        if (MemoryLeakListener.A01(memoryLeakListener)) {
            activity.getLocalClassName();
            memoryLeakListener.A00.A04(activity);
        }
        C28671fB A01 = ((C28611f5) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AIJ, ((LifecycleMemoryLeakListener) AnonymousClass1XX.A02(17, AnonymousClass1Y3.AUK, this.A00)).A00)).A01();
        if (C28671fB.A00().A06().A01() && A01.A0B != null) {
            C28671fB.A01(C28671fB.A00()).A00.A04(activity);
        }
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.base.Optional BcM(android.app.Activity r9, int r10, android.view.KeyEvent r11) {
        /*
            r8 = this;
            int r2 = X.AnonymousClass1Y3.BFf
            X.0UN r1 = r8.A00
            r0 = 21
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.59x r1 = (X.AnonymousClass59x) r1
            r0 = 24
            if (r10 == r0) goto L_0x0018
            r0 = 25
            if (r10 == r0) goto L_0x0018
            r0 = 164(0xa4, float:2.3E-43)
            if (r10 != r0) goto L_0x00bf
        L_0x0018:
            java.util.Set r0 = r1.A00
            java.util.Iterator r7 = r0.iterator()
        L_0x001e:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x00bf
            java.lang.Object r3 = r7.next()
            X.86V r3 = (X.AnonymousClass86V) r3
            int r0 = X.AnonymousClass1Y3.B7v
            X.0UN r2 = r3.A04
            r5 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r0, r2)
            X.7qL r0 = (X.C168717qL) r0
            boolean r0 = r0.A08
            if (r0 == 0) goto L_0x006f
            r0 = 25
            r1 = 4
            r4 = 0
            r6 = 1
            if (r10 != r0) goto L_0x009c
            int r0 = X.AnonymousClass1Y3.AK6
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.64D r2 = (X.AnonymousClass64D) r2
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r3.A05
            java.lang.String r0 = X.AnonymousClass86V.A01(r3)
            r2.A05(r1, r0, r4)
            int r1 = X.AnonymousClass1Y3.AAT
            X.0UN r0 = r3.A04
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            android.media.AudioManager r1 = (android.media.AudioManager) r1
            r0 = -1
            r1.adjustStreamVolume(r5, r0, r4)
        L_0x005f:
            java.util.concurrent.ScheduledFuture r1 = r3.A09
            if (r1 == 0) goto L_0x0069
            r1.cancel(r6)
            r0 = 0
            r3.A09 = r0
        L_0x0069:
            X.AnonymousClass86V.A04(r3)
            X.AnonymousClass86V.A03(r3)
        L_0x006f:
            com.google.common.base.Optional r4 = com.google.common.base.Optional.absent()
            boolean r0 = r4.isPresent()
            if (r0 == 0) goto L_0x001e
            java.lang.Object r0 = r4.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x001e
        L_0x0085:
            boolean r0 = r4.isPresent()
            r3 = 30
            r2 = 1
            if (r0 == 0) goto L_0x00c4
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r2)
            return r4
        L_0x009c:
            r0 = 24
            if (r10 != r0) goto L_0x006f
            int r0 = X.AnonymousClass1Y3.AK6
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.64D r2 = (X.AnonymousClass64D) r2
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r3.A05
            java.lang.String r0 = X.AnonymousClass86V.A01(r3)
            r2.A05(r1, r0, r6)
            int r1 = X.AnonymousClass1Y3.AAT
            X.0UN r0 = r3.A04
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)
            android.media.AudioManager r0 = (android.media.AudioManager) r0
            r0.adjustStreamVolume(r5, r6, r4)
            goto L_0x005f
        L_0x00bf:
            com.google.common.base.Optional r4 = com.google.common.base.Optional.absent()
            goto L_0x0085
        L_0x00c4:
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r2)
            com.google.common.base.Optional r0 = com.google.common.base.Optional.absent()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27851dr.BcM(android.app.Activity, int, android.view.KeyEvent):com.google.common.base.Optional");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x008c, code lost:
        if (r1 == 164) goto L_0x008e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.base.Optional BcN(android.app.Activity r20, int r21, android.view.KeyEvent r22) {
        /*
            r19 = this;
            int r3 = X.AnonymousClass1Y3.AYo
            r2 = r19
            X.0UN r1 = r2.A00
            r0 = 4
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.2dE r4 = (X.C50012dE) r4
            int r3 = X.AnonymousClass1Y3.AcD
            X.0UN r1 = r4.A00
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 376(0x178, float:5.27E-43)
            r5 = 0
            boolean r0 = r1.AbO(r0, r5)
            if (r0 == 0) goto L_0x0057
            java.util.concurrent.atomic.AtomicLong r3 = r4.A04
            long r0 = r22.getEventTime()
            r3.set(r0)
            java.util.concurrent.atomic.AtomicLong r3 = r4.A03
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.069 r0 = (X.AnonymousClass069) r0
            long r0 = r0.now()
            r3.set(r0)
            r3 = 1
            int r1 = X.AnonymousClass1Y3.Asq
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.14J r0 = (X.AnonymousClass14J) r0
            java.lang.String r0 = r0.A02()
            r4.A01 = r0
            android.os.MessageQueue r1 = android.os.Looper.myQueue()
            android.os.MessageQueue$IdleHandler r0 = r4.A02
            r1.addIdleHandler(r0)
        L_0x0057:
            com.google.common.base.Optional r4 = com.google.common.base.Optional.absent()
            boolean r0 = r4.isPresent()
            r3 = 30
            if (r0 == 0) goto L_0x0072
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r2.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1ZJ r1 = (X.AnonymousClass1ZJ) r1
            r0 = 1
            r1.A00(r0)
            return r4
        L_0x0072:
            r4 = 29
            int r1 = X.AnonymousClass1Y3.B58
            X.0UN r0 = r2.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.2dH r0 = (X.C50042dH) r0
            monitor-enter(r0)
            r4 = 25
            r1 = r21
            if (r1 == r4) goto L_0x008e
            r4 = 24
            if (r1 == r4) goto L_0x008e
            r5 = 164(0xa4, float:2.3E-43)
            r4 = 0
            if (r1 != r5) goto L_0x008f
        L_0x008e:
            r4 = 1
        L_0x008f:
            if (r4 == 0) goto L_0x01b0
            java.util.List r4 = r0.A03     // Catch:{ all -> 0x01dc }
            java.util.List r4 = X.C50042dH.A01(r0, r4)     // Catch:{ all -> 0x01dc }
            java.util.Iterator r9 = r4.iterator()     // Catch:{ all -> 0x01dc }
        L_0x009b:
            boolean r4 = r9.hasNext()     // Catch:{ all -> 0x01dc }
            if (r4 == 0) goto L_0x01b0
            java.lang.Object r4 = r9.next()     // Catch:{ all -> 0x01dc }
            java.lang.ref.WeakReference r4 = (java.lang.ref.WeakReference) r4     // Catch:{ all -> 0x01dc }
            java.lang.Object r4 = r4.get()     // Catch:{ all -> 0x01dc }
            com.facebook.video.player.RichVideoPlayer r4 = (com.facebook.video.player.RichVideoPlayer) r4     // Catch:{ all -> 0x01dc }
            if (r4 == 0) goto L_0x009b
            r5 = 25
            boolean r5 = r4.isPlaying()     // Catch:{ all -> 0x01dc }
            if (r5 == 0) goto L_0x009b
            int r15 = r4.A0H()     // Catch:{ all -> 0x01dc }
            java.lang.Boolean r8 = r4.A0W     // Catch:{ all -> 0x01dc }
            r7 = 1
            if (r8 != 0) goto L_0x00db
            java.lang.String r6 = "RichVideoPlayer"
            java.lang.String r5 = "isMinStreamVolume is null"
            X.06G r5 = X.AnonymousClass06F.A02(r6, r5)     // Catch:{ all -> 0x01dc }
            X.06F r6 = r5.A00()     // Catch:{ all -> 0x01dc }
            X.0US r5 = r4.A08     // Catch:{ all -> 0x01dc }
            java.lang.Object r5 = r5.get()     // Catch:{ all -> 0x01dc }
            X.8Jy r5 = (X.C178108Jy) r5     // Catch:{ all -> 0x01dc }
            r5.A03(r6)     // Catch:{ all -> 0x01dc }
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r7)     // Catch:{ all -> 0x01dc }
        L_0x00db:
            r5 = 24
            if (r1 == r5) goto L_0x014c
            r5 = 25
            if (r1 == r5) goto L_0x00e8
            r5 = 164(0xa4, float:2.3E-43)
            if (r1 == r5) goto L_0x00e8
            goto L_0x009b
        L_0x00e8:
            X.8Aq r6 = r4.A0K     // Catch:{ all -> 0x01dc }
            X.58V r5 = new X.58V     // Catch:{ all -> 0x01dc }
            r5.<init>(r15)     // Catch:{ all -> 0x01dc }
            r6.A04(r5)     // Catch:{ all -> 0x01dc }
            if (r15 != 0) goto L_0x009b
            boolean r5 = r8.booleanValue()     // Catch:{ all -> 0x01dc }
            if (r5 != 0) goto L_0x009b
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)     // Catch:{ all -> 0x01dc }
            r4.A0W = r5     // Catch:{ all -> 0x01dc }
            X.8Aj r5 = r4.A0H     // Catch:{ all -> 0x01dc }
            if (r5 == 0) goto L_0x0133
            com.facebook.video.engine.api.VideoPlayerParams r8 = r5.A02     // Catch:{ all -> 0x01dc }
            if (r8 == 0) goto L_0x0133
            r7 = 4
            int r6 = X.AnonymousClass1Y3.A7Y     // Catch:{ all -> 0x01dc }
            X.0UN r5 = r4.A03     // Catch:{ all -> 0x01dc }
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r7, r6, r5)     // Catch:{ all -> 0x01dc }
            X.20b r10 = (X.C400420b) r10     // Catch:{ all -> 0x01dc }
            com.fasterxml.jackson.databind.node.ArrayNode r11 = r8.A0K     // Catch:{ all -> 0x01dc }
            X.8Q6 r12 = r4.A0B     // Catch:{ all -> 0x01dc }
            X.2lJ r5 = X.C53752lJ.A0w     // Catch:{ all -> 0x01dc }
            java.lang.String r13 = r5.value     // Catch:{ all -> 0x01dc }
            int r14 = r4.Aj7()     // Catch:{ all -> 0x01dc }
            X.8Aj r5 = r4.A0H     // Catch:{ all -> 0x01dc }
            com.facebook.video.engine.api.VideoPlayerParams r6 = r5.A02     // Catch:{ all -> 0x01dc }
            java.lang.String r5 = r6.A0Q     // Catch:{ all -> 0x01dc }
            X.2lx r17 = r4.AyX()     // Catch:{ all -> 0x01dc }
            r15 = 0
            r18 = r6
            r16 = r5
            r10.A0T(r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ all -> 0x01dc }
            goto L_0x009b
        L_0x0133:
            java.lang.String r6 = "RichVideoPlayer"
            java.lang.String r5 = "videoPlayerParams is null"
            X.06G r5 = X.AnonymousClass06F.A02(r6, r5)     // Catch:{ all -> 0x01dc }
            X.06F r5 = r5.A00()     // Catch:{ all -> 0x01dc }
            X.0US r4 = r4.A08     // Catch:{ all -> 0x01dc }
            java.lang.Object r4 = r4.get()     // Catch:{ all -> 0x01dc }
            X.8Jy r4 = (X.C178108Jy) r4     // Catch:{ all -> 0x01dc }
            r4.A03(r5)     // Catch:{ all -> 0x01dc }
            goto L_0x009b
        L_0x014c:
            X.8Aq r6 = r4.A0K     // Catch:{ all -> 0x01dc }
            X.58V r5 = new X.58V     // Catch:{ all -> 0x01dc }
            r5.<init>(r15)     // Catch:{ all -> 0x01dc }
            r6.A04(r5)     // Catch:{ all -> 0x01dc }
            if (r15 <= 0) goto L_0x009b
            boolean r5 = r8.booleanValue()     // Catch:{ all -> 0x01dc }
            if (r5 == 0) goto L_0x009b
            r5 = 0
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ all -> 0x01dc }
            r4.A0W = r5     // Catch:{ all -> 0x01dc }
            X.8Aj r5 = r4.A0H     // Catch:{ all -> 0x01dc }
            if (r5 == 0) goto L_0x0197
            com.facebook.video.engine.api.VideoPlayerParams r8 = r5.A02     // Catch:{ all -> 0x01dc }
            if (r8 == 0) goto L_0x0197
            r7 = 4
            int r6 = X.AnonymousClass1Y3.A7Y     // Catch:{ all -> 0x01dc }
            X.0UN r5 = r4.A03     // Catch:{ all -> 0x01dc }
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r7, r6, r5)     // Catch:{ all -> 0x01dc }
            X.20b r10 = (X.C400420b) r10     // Catch:{ all -> 0x01dc }
            com.fasterxml.jackson.databind.node.ArrayNode r11 = r8.A0K     // Catch:{ all -> 0x01dc }
            X.8Q6 r12 = r4.A0B     // Catch:{ all -> 0x01dc }
            X.2lJ r5 = X.C53752lJ.A0w     // Catch:{ all -> 0x01dc }
            java.lang.String r13 = r5.value     // Catch:{ all -> 0x01dc }
            int r14 = r4.Aj7()     // Catch:{ all -> 0x01dc }
            X.8Aj r5 = r4.A0H     // Catch:{ all -> 0x01dc }
            com.facebook.video.engine.api.VideoPlayerParams r6 = r5.A02     // Catch:{ all -> 0x01dc }
            java.lang.String r5 = r6.A0Q     // Catch:{ all -> 0x01dc }
            X.2lx r17 = r4.AyX()     // Catch:{ all -> 0x01dc }
            r18 = r6
            r16 = r5
            r10.A0U(r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ all -> 0x01dc }
            goto L_0x009b
        L_0x0197:
            java.lang.String r6 = "RichVideoPlayer"
            java.lang.String r5 = "videoPlayerParams is null"
            X.06G r5 = X.AnonymousClass06F.A02(r6, r5)     // Catch:{ all -> 0x01dc }
            X.06F r5 = r5.A00()     // Catch:{ all -> 0x01dc }
            X.0US r4 = r4.A08     // Catch:{ all -> 0x01dc }
            java.lang.Object r4 = r4.get()     // Catch:{ all -> 0x01dc }
            X.8Jy r4 = (X.C178108Jy) r4     // Catch:{ all -> 0x01dc }
            r4.A03(r5)     // Catch:{ all -> 0x01dc }
            goto L_0x009b
        L_0x01b0:
            com.google.common.base.Optional r5 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01dc }
            monitor-exit(r0)
            r4 = 2
            boolean r0 = r5.isPresent()
            if (r0 == 0) goto L_0x01ca
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r2.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r4)
            return r5
        L_0x01ca:
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r2.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r4)
            com.google.common.base.Optional r0 = com.google.common.base.Optional.absent()
            return r0
        L_0x01dc:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27851dr.BcN(android.app.Activity, int, android.view.KeyEvent):com.google.common.base.Optional");
    }

    public void BgX(Activity activity, Intent intent) {
        new AnonymousClass0tT((AnonymousClass0t4) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BSw, this.A00), 2, activity, Long.MIN_VALUE).A00();
        C14710tr r1 = (C14710tr) AnonymousClass1XX.A02(15, AnonymousClass1Y3.BFl, this.A00);
        r1.A06 = r1.A02.A0R(activity, intent);
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(2);
    }

    public boolean BhL(MenuItem menuItem) {
        boolean A02 = ((C134756Ry) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BRZ, ((BEY) AnonymousClass1XX.A02(28, AnonymousClass1Y3.AUb, this.A00)).A00)).A02(menuItem);
        AnonymousClass1ZJ r0 = (AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00);
        if (A02) {
            r0.A00(1);
            return true;
        }
        r0.A00(1);
        return false;
    }

    /* JADX INFO: finally extract failed */
    public void Bhz(Activity activity) {
        C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[AutomatedLoggingActivityListener.onActivityPause]", 688404168);
        try {
            AnonymousClass11y r4 = (AnonymousClass11y) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGQ, this.A00);
            int i = AnonymousClass1Y3.BH4;
            AnonymousClass0UN r3 = r4.A00;
            C001500z r1 = (C001500z) AnonymousClass1XX.A02(1, i, r3);
            if (!(r1 == C001500z.A0C || r1 == C001500z.A07 || !((C10850kx) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aoq, r3)).A07())) {
                ((C10850kx) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aoq, r4.A00)).A06();
            }
            C005505z.A00(75252309);
            C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[AnalyticsActivityListener.onPause]", 1213641275);
            try {
                AnonymousClass1EY r32 = (AnonymousClass1EY) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A7A, this.A00);
                Activity activity2 = activity;
                if (AnonymousClass1EY.A03(r32)) {
                    ((AnonymousClass0ia) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A9n, r32.A00)).A02(new C11850o3(r32, activity));
                } else {
                    ((AnonymousClass146) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A5w, r32.A00)).A0B(activity);
                }
                C005505z.A00(-1871356552);
                C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[RageShakeDetector.onPause]", -397269517);
                try {
                    C31751kK r33 = (C31751kK) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Apd, this.A00);
                    C010708t.A0P("RageShakeDetector", "onPause, %s", activity);
                    ((AnonymousClass1Y6) AnonymousClass1XX.A02(8, AnonymousClass1Y3.APr, r33.A03)).AOz();
                    r33.A00 = null;
                    C31751kK.A02(r33);
                    C005505z.A00(-1087353959);
                    C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[ActivityCleaner.onPause]", 1207379595);
                    try {
                        AnonymousClass0t4 r2 = (AnonymousClass0t4) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BSw, this.A00);
                        if (!r2.A05) {
                            new AnonymousClass0tT(r2, 3, activity2, Long.MIN_VALUE).A00();
                        }
                        C005505z.A00(-99155332);
                        C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[AppStateManager.onPause]", -2067763361);
                        try {
                            ((AnonymousClass0Ud) AnonymousClass1XX.A02(12, AnonymousClass1Y3.B5j, this.A00)).A0D(activity);
                            C005505z.A00(-426679538);
                            C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[DefaultUserInteractionController.onPause]", -1598958861);
                            try {
                                AnonymousClass0YT r34 = (AnonymousClass0YT) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AIP, this.A00);
                                synchronized (r34.A01) {
                                    Iterator it = r34.A06.keySet().iterator();
                                    while (it.hasNext()) {
                                        if (((View) it.next()).getContext() == activity) {
                                            it.remove();
                                        }
                                    }
                                }
                                AnonymousClass0YT.A01(r34);
                                C005505z.A00(340459978);
                                C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[DialtoneActivityListener.onPause]", 900191838);
                                try {
                                    ((C14710tr) AnonymousClass1XX.A02(15, AnonymousClass1Y3.BFl, this.A00)).A01 = null;
                                    C005505z.A00(-1101920364);
                                    C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[ForegroundLocationFrameworkController.onPause]", 787147540);
                                    try {
                                        ((ForegroundLocationFrameworkController) AnonymousClass1XX.A02(16, AnonymousClass1Y3.A7p, this.A00)).A0A();
                                        C005505z.A00(1860846197);
                                        C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[MessagingLoggerActivityListener.onPause]", 2140354817);
                                        try {
                                            C191416y r35 = (C191416y) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AbE, this.A00);
                                            C191516z r22 = r35.A01;
                                            r22.A01 = r22.A04.now();
                                            r35.A03.remove(activity.getClass().getSimpleName());
                                            Intent intent = activity.getIntent();
                                            if (intent != null) {
                                                r35.A02.remove(intent.getAction());
                                            }
                                            C005505z.A00(-1083569546);
                                            C005505z.A03("CollectiveLifetimeActivityListenerImpl.onPause[MessagesForegroundActivityListener.onPause]", -1252955135);
                                            try {
                                                C06880cE r23 = ((C14600tf) AnonymousClass1XX.A02(23, AnonymousClass1Y3.B31, this.A00)).A00;
                                                if (r23 != null) {
                                                    r23.A06(C06980cP.A08, false);
                                                }
                                                C005505z.A00(-552144343);
                                                ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(10);
                                            } catch (Throwable th) {
                                                C005505z.A00(-1987903262);
                                                throw th;
                                            }
                                        } catch (Throwable th2) {
                                            C005505z.A00(913577803);
                                            throw th2;
                                        }
                                    } catch (Throwable th3) {
                                        C005505z.A00(-1819404045);
                                        throw th3;
                                    }
                                } catch (Throwable th4) {
                                    C005505z.A00(-1480567062);
                                    throw th4;
                                }
                            } catch (Throwable th5) {
                                C005505z.A00(1508408054);
                                throw th5;
                            }
                        } catch (Throwable th6) {
                            C005505z.A00(-9354444);
                            throw th6;
                        }
                    } catch (Throwable th7) {
                        C005505z.A00(1031867167);
                        throw th7;
                    }
                } catch (Throwable th8) {
                    C005505z.A00(1076929649);
                    throw th8;
                }
            } catch (Throwable th9) {
                C005505z.A00(-1003326120);
                throw th9;
            }
        } catch (Throwable th10) {
            C005505z.A00(382941132);
            throw th10;
        }
    }

    public void Bif(Activity activity, boolean z, Configuration configuration) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public void BjC(Activity activity, Bundle bundle) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public boolean BjS(Activity activity, int i, Dialog dialog) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
        return false;
    }

    public void BjX(Menu menu) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01f1, code lost:
        if (((java.lang.Boolean) r3.A01.get()).booleanValue() != false) goto L_0x01f3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BmN(android.app.Activity r9) {
        /*
            r8 = this;
            java.lang.String r1 = "CollectiveLifetimeActivityListenerImpl.onResume[AnalyticsActivityListener.onResume]"
            r0 = -589615764(0xffffffffdcdb2d6c, float:-4.93543694E17)
            X.C005505z.A03(r1, r0)
            r2 = 1
            int r1 = X.AnonymousClass1Y3.A7A     // Catch:{ all -> 0x02e0 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x02e0 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02e0 }
            X.1EY r3 = (X.AnonymousClass1EY) r3     // Catch:{ all -> 0x02e0 }
            boolean r0 = X.AnonymousClass1EY.A03(r3)     // Catch:{ all -> 0x02e0 }
            if (r0 == 0) goto L_0x002c
            int r1 = X.AnonymousClass1Y3.A9n     // Catch:{ all -> 0x02e0 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x02e0 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02e0 }
            X.0ia r1 = (X.AnonymousClass0ia) r1     // Catch:{ all -> 0x02e0 }
            X.1EZ r0 = new X.1EZ     // Catch:{ all -> 0x02e0 }
            r0.<init>(r3, r9)     // Catch:{ all -> 0x02e0 }
            r1.A02(r0)     // Catch:{ all -> 0x02e0 }
            goto L_0x002f
        L_0x002c:
            X.AnonymousClass1EY.A02(r3, r9)     // Catch:{ all -> 0x02e0 }
        L_0x002f:
            r0 = -298065200(0xffffffffee3be2d0, float:-1.4536974E28)
            X.C005505z.A00(r0)
            r1 = 938996818(0x37f7f452, float:2.9558465E-5)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[ImpressionManager.generateNewImpressionId]"
            X.C005505z.A03(r0, r1)
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AUN     // Catch:{ all -> 0x02d8 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x02d8 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02d8 }
            X.144 r0 = (X.AnonymousClass144) r0     // Catch:{ all -> 0x02d8 }
            java.util.WeakHashMap r2 = r0.A00     // Catch:{ all -> 0x02d8 }
            monitor-enter(r2)     // Catch:{ all -> 0x02d8 }
            java.util.WeakHashMap r1 = r0.A00     // Catch:{ all -> 0x02d5 }
            java.lang.String r0 = ""
            r1.put(r9, r0)     // Catch:{ all -> 0x02d5 }
            monitor-exit(r2)     // Catch:{ all -> 0x02d5 }
            r0 = 2026118441(0x78c41d29, float:3.18213E34)
            X.C005505z.A00(r0)
            r1 = -243765788(0xfffffffff1786de4, float:-1.2301621E30)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[AuthenticatedActivityHelper.onResume]"
            X.C005505z.A03(r0, r1)
            r2 = 5
            int r1 = X.AnonymousClass1Y3.BMr     // Catch:{ all -> 0x02cd }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x02cd }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02cd }
            X.1dt r3 = (X.C27871dt) r3     // Catch:{ all -> 0x02cd }
            boolean r0 = X.C27871dt.A02(r3, r9)     // Catch:{ all -> 0x02cd }
            if (r0 != 0) goto L_0x0084
            r2 = 3
            int r1 = X.AnonymousClass1Y3.Am8     // Catch:{ all -> 0x02cd }
            X.0UN r0 = r3.A06     // Catch:{ all -> 0x02cd }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02cd }
            X.2I0 r2 = (X.AnonymousClass2I0) r2     // Catch:{ all -> 0x02cd }
            r0 = 0
            r2.A00(r9, r0)     // Catch:{ all -> 0x02cd }
            r9.finish()     // Catch:{ all -> 0x02cd }
        L_0x0084:
            r0 = 1115449225(0x427c6789, float:63.10111)
            X.C005505z.A00(r0)
            r1 = -149009033(0xfffffffff71e4d77, float:-3.210758E33)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[RageShakeDetector.onResume]"
            X.C005505z.A03(r0, r1)
            r2 = 7
            int r1 = X.AnonymousClass1Y3.Apd     // Catch:{ all -> 0x02c5 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x02c5 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02c5 }
            X.1kK r3 = (X.C31751kK) r3     // Catch:{ all -> 0x02c5 }
            java.lang.Object[] r2 = new java.lang.Object[]{r9}     // Catch:{ all -> 0x02c5 }
            java.lang.String r1 = "RageShakeDetector"
            java.lang.String r0 = "onResume, %s"
            X.C010708t.A0P(r1, r0, r2)     // Catch:{ all -> 0x02c5 }
            int r2 = X.AnonymousClass1Y3.APr     // Catch:{ all -> 0x02c5 }
            X.0UN r1 = r3.A03     // Catch:{ all -> 0x02c5 }
            r0 = 8
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x02c5 }
            X.1Y6 r0 = (X.AnonymousClass1Y6) r0     // Catch:{ all -> 0x02c5 }
            r0.AOz()     // Catch:{ all -> 0x02c5 }
            r3.A00 = r9     // Catch:{ all -> 0x02c5 }
            X.C31751kK.A03(r3)     // Catch:{ all -> 0x02c5 }
            r0 = -457514270(0xffffffffe4bae2e2, float:-2.7579544E22)
            X.C005505z.A00(r0)
            r1 = -1611551091(0xffffffff9ff1ae8d, float:-1.0235624E-19)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[AppStateManager.onResume]"
            X.C005505z.A03(r0, r1)
            r2 = 12
            int r1 = X.AnonymousClass1Y3.B5j     // Catch:{ all -> 0x02bd }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x02bd }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02bd }
            X.0Ud r0 = (X.AnonymousClass0Ud) r0     // Catch:{ all -> 0x02bd }
            r0.A0E(r9)     // Catch:{ all -> 0x02bd }
            r0 = 564786852(0x21a9f6a4, float:1.1517171E-18)
            X.C005505z.A00(r0)
            r1 = -536290021(0xffffffffe008dd1b, float:-3.9448274E19)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[DialtoneActivityListener.onResume]"
            X.C005505z.A03(r0, r1)
            r2 = 15
            int r1 = X.AnonymousClass1Y3.BFl     // Catch:{ all -> 0x02b5 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x02b5 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02b5 }
            X.0tr r0 = (X.C14710tr) r0     // Catch:{ all -> 0x02b5 }
            r0.A01 = r9     // Catch:{ all -> 0x02b5 }
            java.util.Set r0 = r0.A03     // Catch:{ all -> 0x02b5 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x02b5 }
        L_0x00fb:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x02b5 }
            if (r0 == 0) goto L_0x011e
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x02b5 }
            X.5VO r1 = (X.AnonymousClass5VO) r1     // Catch:{ all -> 0x02b5 }
            X.1eM r0 = r1.A00     // Catch:{ all -> 0x02b5 }
            com.google.common.base.Optional r0 = r0.A00     // Catch:{ all -> 0x02b5 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x02b5 }
            X.0tr r0 = (X.C14710tr) r0     // Catch:{ all -> 0x02b5 }
            java.util.Set r0 = r0.A03     // Catch:{ all -> 0x02b5 }
            r0.remove(r1)     // Catch:{ all -> 0x02b5 }
            X.1eM r1 = r1.A00     // Catch:{ all -> 0x02b5 }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x02b5 }
            r1.A0H(r0)     // Catch:{ all -> 0x02b5 }
            goto L_0x00fb
        L_0x011e:
            r0 = 1426722961(0x550a1091, float:9.4877348E12)
            X.C005505z.A00(r0)
            r1 = -271828767(0xffffffffefcc38e1, float:-1.2640741E29)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[MessagingLoggerActivityListener.onResume]"
            X.C005505z.A03(r0, r1)
            r2 = 18
            int r1 = X.AnonymousClass1Y3.AbE     // Catch:{ all -> 0x02ad }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x02ad }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02ad }
            X.16y r2 = (X.C191416y) r2     // Catch:{ all -> 0x02ad }
            java.util.Set r1 = r2.A03     // Catch:{ all -> 0x02ad }
            java.lang.Class r0 = r9.getClass()     // Catch:{ all -> 0x02ad }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ all -> 0x02ad }
            r1.add(r0)     // Catch:{ all -> 0x02ad }
            android.content.Intent r0 = r9.getIntent()     // Catch:{ all -> 0x02ad }
            if (r0 == 0) goto L_0x0154
            java.util.Set r1 = r2.A02     // Catch:{ all -> 0x02ad }
            java.lang.String r0 = r0.getAction()     // Catch:{ all -> 0x02ad }
            r1.add(r0)     // Catch:{ all -> 0x02ad }
        L_0x0154:
            X.16z r0 = r2.A01     // Catch:{ all -> 0x02ad }
            r0.A03(r9)     // Catch:{ all -> 0x02ad }
            r0 = -1603699002(0xffffffffa0697ec6, float:-1.9777813E-19)
            X.C005505z.A00(r0)
            r1 = -1805933953(0xffffffff945ba27f, float:-1.1088721E-26)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[BlockingFlowsManager.chooseBestBlockingFlowToShow]"
            X.C005505z.A03(r0, r1)
            r2 = 19
            int r1 = X.AnonymousClass1Y3.A2e     // Catch:{ all -> 0x02a5 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x02a5 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02a5 }
            X.147 r5 = (X.AnonymousClass147) r5     // Catch:{ all -> 0x02a5 }
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x02a5 }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x02a5 }
            r2 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02a5 }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ all -> 0x02a5 }
            r0 = 183(0xb7, float:2.56E-43)
            boolean r0 = r1.AbO(r0, r2)     // Catch:{ all -> 0x02a5 }
            if (r0 == 0) goto L_0x01a0
            r2 = 1
            int r1 = X.AnonymousClass1Y3.A8a     // Catch:{ all -> 0x02a5 }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x02a5 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02a5 }
            X.0WP r4 = (X.AnonymousClass0WP) r4     // Catch:{ all -> 0x02a5 }
            X.4Jv r3 = new X.4Jv     // Catch:{ all -> 0x02a5 }
            r3.<init>(r5, r9)     // Catch:{ all -> 0x02a5 }
            X.0XV r2 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY     // Catch:{ all -> 0x02a5 }
            java.lang.Integer r1 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x02a5 }
            java.lang.String r0 = "delay_blocking_flow"
            r4.CIG(r0, r3, r2, r1)     // Catch:{ all -> 0x02a5 }
            goto L_0x01a3
        L_0x01a0:
            r5.A02(r9)     // Catch:{ all -> 0x02a5 }
        L_0x01a3:
            r0 = 1844100102(0x6deabc06, float:9.08085E27)
            X.C005505z.A00(r0)
            r1 = -607282750(0xffffffffdbcd99c2, float:-1.15742857E17)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[ChatHeadsActivityListener.onResume]"
            X.C005505z.A03(r0, r1)
            r2 = 20
            int r1 = X.AnonymousClass1Y3.ASw     // Catch:{ all -> 0x029d }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x029d }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x029d }
            X.14x r3 = (X.C187414x) r3     // Catch:{ all -> 0x029d }
            android.app.Activity r0 = r9.getParent()     // Catch:{ all -> 0x029d }
            if (r0 != 0) goto L_0x0255
            boolean r6 = r9 instanceof X.C11580nP     // Catch:{ all -> 0x029d }
            if (r6 == 0) goto L_0x01d4
            r0 = r9
            X.0nP r0 = (X.C11580nP) r0     // Catch:{ all -> 0x029d }
            java.lang.Integer r1 = r0.Agn()     // Catch:{ all -> 0x029d }
            java.lang.Integer r0 = X.AnonymousClass07B.A0N     // Catch:{ all -> 0x029d }
            if (r1 != r0) goto L_0x01d4
            goto L_0x0255
        L_0x01d4:
            int r1 = X.AnonymousClass1Y3.BH4     // Catch:{ all -> 0x029d }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x029d }
            r7 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x029d }
            X.00z r1 = (X.C001500z) r1     // Catch:{ all -> 0x029d }
            X.00z r0 = X.C001500z.A07     // Catch:{ all -> 0x029d }
            r2 = 0
            if (r1 != r0) goto L_0x01f3
            X.0Tq r0 = r3.A01     // Catch:{ all -> 0x029d }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x029d }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x029d }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x029d }
            r4 = 1
            if (r0 == 0) goto L_0x01f4
        L_0x01f3:
            r4 = 0
        L_0x01f4:
            r5 = 2
            if (r6 == 0) goto L_0x0232
            r6 = r9
            X.0nP r6 = (X.C11580nP) r6     // Catch:{ all -> 0x029d }
            java.lang.Integer r1 = r6.Agn()     // Catch:{ all -> 0x029d }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x029d }
            if (r1 != r0) goto L_0x0210
            X.0Tq r0 = r3.A01     // Catch:{ all -> 0x029d }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x029d }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x029d }
            boolean r4 = r0.booleanValue()     // Catch:{ all -> 0x029d }
            r4 = r4 ^ r7
            goto L_0x0219
        L_0x0210:
            java.lang.Integer r1 = r6.Agn()     // Catch:{ all -> 0x029d }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x029d }
            if (r1 != r0) goto L_0x0219
            r4 = 0
        L_0x0219:
            if (r4 != 0) goto L_0x0232
            int r1 = X.AnonymousClass1Y3.Aum     // Catch:{ all -> 0x029d }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x029d }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x029d }
            X.1kT r0 = (X.C31841kT) r0     // Catch:{ all -> 0x029d }
            java.lang.ref.WeakReference r0 = r0.A00     // Catch:{ all -> 0x029d }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x029d }
            android.app.Activity r0 = (android.app.Activity) r0     // Catch:{ all -> 0x029d }
            if (r0 == 0) goto L_0x0232
            X.C187414x.A01(r3)     // Catch:{ all -> 0x029d }
        L_0x0232:
            if (r4 == 0) goto L_0x0255
            int r1 = X.AnonymousClass1Y3.Aum     // Catch:{ all -> 0x029d }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x029d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x029d }
            X.1kT r1 = (X.C31841kT) r1     // Catch:{ all -> 0x029d }
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x029d }
            r0.<init>(r9)     // Catch:{ all -> 0x029d }
            r1.A00 = r0     // Catch:{ all -> 0x029d }
            int r1 = X.AnonymousClass1Y3.Ahm     // Catch:{ all -> 0x029d }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x029d }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x029d }
            X.0zv r2 = (X.C18010zv) r2     // Catch:{ all -> 0x029d }
            java.lang.String r1 = "activityResumed"
            r0 = 0
            X.C18010zv.A03(r2, r0, r1)     // Catch:{ all -> 0x029d }
        L_0x0255:
            r0 = -1092799099(0xffffffffbedd3585, float:-0.43204895)
            X.C005505z.A00(r0)
            r1 = 1326674198(0x4f137116, float:2.47366195E9)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onResume[MessagesForegroundActivityListener.onResume]"
            X.C005505z.A03(r0, r1)
            r2 = 23
            int r1 = X.AnonymousClass1Y3.B31     // Catch:{ all -> 0x0295 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x0295 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0295 }
            X.0tf r0 = (X.C14600tf) r0     // Catch:{ all -> 0x0295 }
            X.0cE r2 = r0.A00     // Catch:{ all -> 0x0295 }
            if (r2 == 0) goto L_0x027d
            android.net.Uri r1 = X.C06980cP.A08     // Catch:{ all -> 0x0295 }
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0295 }
            r2.A06(r1, r0)     // Catch:{ all -> 0x0295 }
        L_0x027d:
            r3 = 10
            r0 = -1389242540(0xffffffffad31d754, float:-1.0109098E-11)
            X.C005505z.A00(r0)
            r2 = 30
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r3)
            return
        L_0x0295:
            r1 = move-exception
            r0 = 1175521200(0x461107b0, float:9281.922)
            X.C005505z.A00(r0)
            throw r1
        L_0x029d:
            r1 = move-exception
            r0 = -455768430(0xffffffffe4d58692, float:-3.1510826E22)
            X.C005505z.A00(r0)
            throw r1
        L_0x02a5:
            r1 = move-exception
            r0 = -1465647295(0xffffffffa8a3ff41, float:-1.8207334E-14)
            X.C005505z.A00(r0)
            throw r1
        L_0x02ad:
            r1 = move-exception
            r0 = -2100433344(0xffffffff82cdee40, float:-3.0258792E-37)
            X.C005505z.A00(r0)
            throw r1
        L_0x02b5:
            r1 = move-exception
            r0 = -2119523034(0xffffffff81aaa526, float:-6.2685114E-38)
            X.C005505z.A00(r0)
            throw r1
        L_0x02bd:
            r1 = move-exception
            r0 = 120347289(0x72c5a99, float:1.2966466E-34)
            X.C005505z.A00(r0)
            throw r1
        L_0x02c5:
            r1 = move-exception
            r0 = 1289586999(0x4cdd8937, float:1.16148664E8)
            X.C005505z.A00(r0)
            throw r1
        L_0x02cd:
            r1 = move-exception
            r0 = -936520800(0xffffffffc82dd3a0, float:-177998.5)
            X.C005505z.A00(r0)
            throw r1
        L_0x02d5:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x02d5 }
            throw r0     // Catch:{ all -> 0x02d8 }
        L_0x02d8:
            r1 = move-exception
            r0 = -414989287(0xffffffffe743c419, float:-9.244788E23)
            X.C005505z.A00(r0)
            throw r1
        L_0x02e0:
            r1 = move-exception
            r0 = -720049966(0xffffffffd514e8d2, float:-1.02329798E13)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27851dr.BmN(android.app.Activity):void");
    }

    public void BmT(Activity activity) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public void Bmx(Bundle bundle) {
        String str = ((C27871dt) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMr, this.A00)).A07;
        if (str != null) {
            bundle.putString("loggedInUser", str);
        }
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(1);
    }

    public Optional BnW(Activity activity) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
        return Optional.absent();
    }

    public boolean Bo7(Activity activity, Throwable th) {
        boolean z;
        C27871dt r1 = (C27871dt) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BMr, this.A00);
        if ((activity instanceof C27891dv) || !AnonymousClass7NS.A01(th)) {
            z = false;
        } else {
            C27871dt.A01(r1, activity);
            z = true;
        }
        if (z) {
            ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(1);
            return true;
        }
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(1);
        return false;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0060, code lost:
        if (r1 != false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0068, code lost:
        if (r2.A04 != false) goto L_0x006a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BpP(android.app.Activity r8) {
        /*
            r7 = this;
            java.lang.String r1 = "CollectiveLifetimeActivityListenerImpl.onStart[ActivityCleaner.onStart]"
            r0 = 1613198036(0x602772d4, float:4.8263758E19)
            X.C005505z.A03(r1, r0)
            r2 = 9
            int r1 = X.AnonymousClass1Y3.BSw     // Catch:{ all -> 0x010b }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x010b }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x010b }
            X.0t4 r2 = (X.AnonymousClass0t4) r2     // Catch:{ all -> 0x010b }
            X.0tT r1 = new X.0tT     // Catch:{ all -> 0x010b }
            X.06B r0 = r2.A02     // Catch:{ all -> 0x010b }
            long r5 = r0.now()     // Catch:{ all -> 0x010b }
            r3 = 1
            r4 = r8
            r1.<init>(r2, r3, r4, r5)     // Catch:{ all -> 0x010b }
            r1.A00()     // Catch:{ all -> 0x010b }
            r0 = 1438490773(0x55bda095, float:2.6062174E13)
            X.C005505z.A00(r0)
            r1 = 423990672(0x19459590, float:1.0214865E-23)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onStart[DialtoneActivityListener.onStart]"
            X.C005505z.A03(r0, r1)
            r2 = 15
            int r1 = X.AnonymousClass1Y3.BFl     // Catch:{ all -> 0x0103 }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x0103 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0103 }
            X.0tr r2 = (X.C14710tr) r2     // Catch:{ all -> 0x0103 }
            monitor-enter(r2)     // Catch:{ all -> 0x0103 }
            X.1ES r0 = r2.A02     // Catch:{ all -> 0x0100 }
            boolean r0 = r0.A0L()     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x0062
            boolean r0 = r2.A05     // Catch:{ all -> 0x0100 }
            if (r0 != 0) goto L_0x0062
            boolean r0 = r2.A04     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x0053
            boolean r0 = r2.A06     // Catch:{ all -> 0x0100 }
            if (r0 != 0) goto L_0x0062
        L_0x0053:
            monitor-enter(r2)     // Catch:{ all -> 0x0100 }
            int r0 = r2.A00     // Catch:{ all -> 0x005c }
            r1 = 0
            if (r0 <= 0) goto L_0x005a
            r1 = 1
        L_0x005a:
            monitor-exit(r2)     // Catch:{ all -> 0x0100 }
            goto L_0x005f
        L_0x005c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0100 }
            throw r0     // Catch:{ all -> 0x0100 }
        L_0x005f:
            r0 = 1
            if (r1 == 0) goto L_0x0063
        L_0x0062:
            r0 = 0
        L_0x0063:
            if (r0 == 0) goto L_0x006a
            boolean r1 = r2.A04     // Catch:{ all -> 0x0100 }
            r0 = 1
            if (r1 == 0) goto L_0x006b
        L_0x006a:
            r0 = 0
        L_0x006b:
            if (r0 == 0) goto L_0x0072
            X.1ES r0 = r2.A02     // Catch:{ all -> 0x0100 }
            r0.A0b(r8)     // Catch:{ all -> 0x0100 }
        L_0x0072:
            r0 = 0
            r2.A06 = r0     // Catch:{ all -> 0x0100 }
            int r0 = r2.A00     // Catch:{ all -> 0x0100 }
            int r0 = r0 + 1
            r2.A00 = r0     // Catch:{ all -> 0x0100 }
            monitor-exit(r2)     // Catch:{ all -> 0x0103 }
            r0 = 1996657029(0x77029185, float:2.6482425E33)
            X.C005505z.A00(r0)
            r1 = -1164079362(0xffffffffba9d8efe, float:-0.0012020764)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onStart[MessagingLoggerActivityListener.onStart]"
            X.C005505z.A03(r0, r1)
            r2 = 18
            int r1 = X.AnonymousClass1Y3.AbE     // Catch:{ all -> 0x00f8 }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x00f8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00f8 }
            X.16y r1 = (X.C191416y) r1     // Catch:{ all -> 0x00f8 }
            X.16z r0 = r1.A01     // Catch:{ all -> 0x00f8 }
            r0.A03(r8)     // Catch:{ all -> 0x00f8 }
            X.16z r0 = r1.A01     // Catch:{ all -> 0x00f8 }
            r0.A02()     // Catch:{ all -> 0x00f8 }
            r0 = 1691689716(0x64d522f4, float:3.14534E22)
            X.C005505z.A00(r0)
            r1 = 1372697950(0x51d1b55e, float:1.12586375E11)
            java.lang.String r0 = "CollectiveLifetimeActivityListenerImpl.onStart[AppStartupNotifierActivityListener.onStarted]"
            X.C005505z.A03(r0, r1)
            r2 = 24
            int r1 = X.AnonymousClass1Y3.AEn     // Catch:{ all -> 0x00f0 }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x00f0 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00f0 }
            X.0u8 r0 = (X.C14790u8) r0     // Catch:{ all -> 0x00f0 }
            int r2 = X.AnonymousClass1Y3.AMF     // Catch:{ all -> 0x00f0 }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x00f0 }
            r0 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x00f0 }
            X.1Zw r3 = (X.C25501Zw) r3     // Catch:{ all -> 0x00f0 }
            int r2 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x00f0 }
            X.0UN r1 = r3.A04     // Catch:{ all -> 0x00f0 }
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x00f0 }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x00f0 }
            long r0 = r0.now()     // Catch:{ all -> 0x00f0 }
            r3.A01 = r0     // Catch:{ all -> 0x00f0 }
            X.C25501Zw.A01(r3, r0)     // Catch:{ all -> 0x00f0 }
            r3 = 4
            r0 = -1232817636(0xffffffffb684b21c, float:-3.954641E-6)
            X.C005505z.A00(r0)
            r2 = 30
            int r1 = X.AnonymousClass1Y3.AjO
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            r0.A00(r3)
            return
        L_0x00f0:
            r1 = move-exception
            r0 = -1620543539(0xffffffff9f6877cd, float:-4.9227008E-20)
            X.C005505z.A00(r0)
            throw r1
        L_0x00f8:
            r1 = move-exception
            r0 = -30836935(0xfffffffffe297739, float:-5.6314643E37)
            X.C005505z.A00(r0)
            throw r1
        L_0x0100:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0103 }
            throw r0     // Catch:{ all -> 0x0103 }
        L_0x0103:
            r1 = move-exception
            r0 = -774846185(0xffffffffd1d0c917, float:-1.12090866E11)
            X.C005505z.A00(r0)
            throw r1
        L_0x010b:
            r1 = move-exception
            r0 = 50106337(0x2fc8fe1, float:3.7110654E-37)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27851dr.BpP(android.app.Activity):void");
    }

    /* JADX INFO: finally extract failed */
    public void Bq4(Activity activity) {
        C005505z.A03("CollectiveLifetimeActivityListenerImpl.onStop[DialtoneActivityListener.onStop]", 1277612113);
        try {
            C14710tr r2 = (C14710tr) AnonymousClass1XX.A02(15, AnonymousClass1Y3.BFl, this.A00);
            synchronized (r2) {
                r2.A05 = false;
                int i = r2.A00 - 1;
                r2.A00 = i;
                if (i < 1) {
                    r2.A04 = r2.A02.A0L();
                }
            }
            C005505z.A00(-1028763674);
            C005505z.A03("CollectiveLifetimeActivityListenerImpl.onStop[MessagingLoggerActivityListener.onStop]", -382905318);
            try {
                C191416y r3 = (C191416y) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AbE, this.A00);
                C191516z r22 = r3.A01;
                r22.A01 = r22.A04.now();
                activity.getClass().equals(r3.A00.A05);
                C005505z.A00(1797843461);
                C005505z.A03("CollectiveLifetimeActivityListenerImpl.onStop[ChatHeadsActivityListener.onStop]", 1810862662);
                try {
                    C187414x r32 = (C187414x) AnonymousClass1XX.A02(20, AnonymousClass1Y3.ASw, this.A00);
                    if (((Activity) ((C31841kT) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Aum, r32.A00)).A00.get()) == activity) {
                        C187414x.A01(r32);
                    }
                    C005505z.A00(517933926);
                    C005505z.A03("CollectiveLifetimeActivityListenerImpl.onStop[MobileConfigEmergencyPushResetHandlerImpl.onEnterBackground]", 706001840);
                    try {
                        C17450yv r1 = (C17450yv) AnonymousClass1XX.A02(22, AnonymousClass1Y3.BNv, this.A00);
                        if (!AnonymousClass01P.A0D()) {
                            C17450yv.A01(r1, "Background Emergency Push Restart");
                        }
                        C005505z.A00(1485727761);
                        C005505z.A03("CollectiveLifetimeActivityListenerImpl.onStop[AppStartupNotifierActivityListener.onStopped]", -2077594550);
                        try {
                            C25501Zw r33 = (C25501Zw) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AMF, ((C14790u8) AnonymousClass1XX.A02(24, AnonymousClass1Y3.AEn, this.A00)).A00);
                            long now = ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, r33.A04)).now();
                            r33.A02 = now;
                            C25501Zw.A01(r33, now);
                            C005505z.A00(1348317971);
                            ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(5);
                        } catch (Throwable th) {
                            C005505z.A00(-1888354126);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        C005505z.A00(31419749);
                        throw th2;
                    }
                } catch (Throwable th3) {
                    C005505z.A00(1706068567);
                    throw th3;
                }
            } catch (Throwable th4) {
                C005505z.A00(-1453184994);
                throw th4;
            }
        } catch (Throwable th5) {
            C005505z.A00(320374657);
            throw th5;
        }
    }

    public void BsC(CharSequence charSequence, int i) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public void Bt3(Activity activity, int i) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public void Btv(Activity activity) {
        ((AnonymousClass0Ud) AnonymousClass1XX.A02(12, AnonymousClass1Y3.B5j, this.A00)).A0F(activity);
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(1);
    }

    public void Bty(Activity activity) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public void Bv9(Activity activity, boolean z) {
        ((AnonymousClass1ZJ) AnonymousClass1XX.A02(30, AnonymousClass1Y3.AjO, this.A00)).A00(0);
    }

    public C27851dr(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(31, r3);
    }
}
