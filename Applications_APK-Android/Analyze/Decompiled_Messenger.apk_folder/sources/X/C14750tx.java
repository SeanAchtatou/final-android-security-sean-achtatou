package X;

/* renamed from: X.0tx  reason: invalid class name and case insensitive filesystem */
public final class C14750tx extends C14590te {
    private final C32211lO A00;

    public String Amb() {
        return "datasavingsinactive";
    }

    public static final C14750tx A00(AnonymousClass1XY r1) {
        return new C14750tx(r1);
    }

    public boolean BEx() {
        if (this.A00.A02() || !this.A00.A03()) {
            return false;
        }
        return true;
    }

    private C14750tx(AnonymousClass1XY r2) {
        this.A00 = C32211lO.A00(r2);
    }
}
