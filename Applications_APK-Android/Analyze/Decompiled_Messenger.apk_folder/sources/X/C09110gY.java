package X;

import com.facebook.base.app.AbstractApplicationLike;
import com.google.common.base.Throwables;

/* renamed from: X.0gY  reason: invalid class name and case insensitive filesystem */
public final class C09110gY implements C04810Wg {
    public final /* synthetic */ AbstractApplicationLike A00;

    public C09110gY(AbstractApplicationLike abstractApplicationLike) {
        this.A00 = abstractApplicationLike;
    }

    public void Bqf(Object obj) {
        this.A00.A04();
    }

    public void BYh(Throwable th) {
        throw Throwables.propagate(th);
    }
}
