package X;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.fragment.app.Fragment;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.orca.threadview.ThreadViewFragment;
import com.facebook.widget.CustomLinearLayout;
import com.google.common.base.Preconditions;
import java.util.Map;

/* renamed from: X.0qf  reason: invalid class name and case insensitive filesystem */
public final class C13100qf extends C13110qg {
    public static final String __redex_internal_original_name = "com.facebook.messenger.neue.M4MainActivityDelegate";
    public C06790c5 A00;
    public AnonymousClass0UN A01;
    public CustomLinearLayout A02;
    public boolean A03;
    public boolean A04;
    private C32301lX A05;
    private C13130qk A06;
    private C15750vt A07;
    private boolean A08;
    private boolean A09;
    public final C34731q5 A0A;
    public final C34741q6 A0B;
    public final C04310Tq A0C;
    private final C13030qT A0D;

    public void BPe(int i) {
        this.A03 = true;
    }

    public void BpD(int i, int i2, int i3, int i4, boolean z) {
        this.A03 = false;
        A05();
    }

    public void Bxr(Menu menu) {
    }

    public static C32301lX A00(C13100qf r5) {
        if (r5.A05 == null) {
            C15800w0 r4 = (C15800w0) AnonymousClass1XX.A03(AnonymousClass1Y3.BH2, r5.A01);
            r5.A05 = new C32301lX(r4, r5.B4m(), r5.A0B, new C13120qj(r4));
        }
        return r5.A05;
    }

    public static final C13100qf A01(AnonymousClass1XY r1) {
        return new C13100qf(r1);
    }

    private C13130qk A02() {
        if (this.A06 == null) {
            this.A06 = new C13130qk((C13960sN) AnonymousClass1XX.A03(AnonymousClass1Y3.BGR, this.A01), B4m());
        }
        return this.A06;
    }

    public static C15750vt A03(C13100qf r3) {
        if (r3.A07 == null) {
            ViewStubCompat viewStubCompat = new ViewStubCompat(r3.A00, null);
            viewStubCompat.A01 = 2132411191;
            r3.A07 = C15750vt.A00(viewStubCompat);
        }
        return r3.A07;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ee A[SYNTHETIC, Splitter:B:35:0x00ee] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A04() {
        /*
            r17 = this;
            r2 = r17
            boolean r0 = r2.A01
            if (r0 == 0) goto L_0x0445
            boolean r0 = r2.A08
            if (r0 == 0) goto L_0x0445
            boolean r0 = r2.A09
            if (r0 == 0) goto L_0x0445
            X.0qc r0 = r2.A01
            android.content.Intent r10 = r0.getIntent()
            r0 = 0
            r2.A09 = r0
            int r1 = X.AnonymousClass1Y3.AyL
            X.0UN r0 = r2.A01
            java.lang.Object r9 = X.AnonymousClass1XX.A03(r1, r0)
            X.0qu r9 = (X.C13210qu) r9
            int r1 = X.AnonymousClass1Y3.A2x
            X.0UN r0 = r2.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.1lW r0 = (X.C32291lW) r0
            android.app.Activity r15 = r2.A00
            X.0qW r16 = r2.B4m()
            X.1q7 r13 = new X.1q7
            r13.<init>(r2, r0)
            int r1 = X.AnonymousClass1Y3.A6j
            X.0UN r0 = r9.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.0ci r0 = (X.C07150ci) r0
            com.facebook.messaging.model.threadkey.ThreadKey r14 = r0.A02(r10)
            r12 = 0
            if (r14 == 0) goto L_0x01ac
            r0 = 21
            java.lang.String r3 = X.AnonymousClass24B.$const$string(r0)
            boolean r2 = X.C64793Dn.A00(r10, r3, r12)
            r10.putExtra(r3, r2)
            java.lang.String r1 = "trigger2"
            boolean r0 = r10.hasExtra(r1)
            if (r0 != 0) goto L_0x00b9
            r0 = 65
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r10.hasExtra(r1)
            if (r0 != 0) goto L_0x00b9
            java.lang.String r1 = "trigger"
            boolean r0 = r10.hasExtra(r1)
            if (r0 == 0) goto L_0x00ab
            java.lang.String r0 = r10.getStringExtra(r1)
            com.facebook.messaging.send.trigger.NavigationTrigger r11 = com.facebook.messaging.send.trigger.NavigationTrigger.A01(r0)
        L_0x0078:
            if (r11 != 0) goto L_0x0080
            java.lang.String r0 = "unknown"
            com.facebook.messaging.send.trigger.NavigationTrigger r11 = com.facebook.messaging.send.trigger.NavigationTrigger.A00(r0)
        L_0x0080:
            r0 = 333(0x14d, float:4.67E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            android.os.Parcelable r8 = r10.getParcelableExtra(r0)
            com.facebook.messaging.threadview.params.ThreadViewMessagesInitParams r8 = (com.facebook.messaging.threadview.params.ThreadViewMessagesInitParams) r8
            r0 = 19
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            java.io.Serializable r2 = r10.getSerializableExtra(r0)
            X.0qv r2 = (X.C13220qv) r2
            if (r2 != 0) goto L_0x00c9
            java.lang.String r1 = "extra_thread_view_source_string"
            java.lang.String r0 = r10.getStringExtra(r1)
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00c9
            java.lang.String r1 = r10.getStringExtra(r1)
            goto L_0x00c0
        L_0x00ab:
            if (r2 == 0) goto L_0x00b2
            com.facebook.messaging.send.trigger.NavigationTrigger r11 = com.facebook.messaging.send.trigger.NavigationTrigger.A00(r3)
            goto L_0x0078
        L_0x00b2:
            java.lang.String r0 = "from_intent"
            com.facebook.messaging.send.trigger.NavigationTrigger r11 = com.facebook.messaging.send.trigger.NavigationTrigger.A00(r0)
            goto L_0x0078
        L_0x00b9:
            android.os.Parcelable r11 = r10.getParcelableExtra(r1)
            com.facebook.messaging.send.trigger.NavigationTrigger r11 = (com.facebook.messaging.send.trigger.NavigationTrigger) r11
            goto L_0x0078
        L_0x00c0:
            java.lang.Class<X.0qv> r0 = X.C13220qv.class
            java.lang.Enum r7 = java.lang.Enum.valueOf(r0, r1)     // Catch:{ IllegalArgumentException -> 0x00c9 }
            X.0qv r7 = (X.C13220qv) r7     // Catch:{ IllegalArgumentException -> 0x00c9 }
            goto L_0x00ca
        L_0x00c9:
            r7 = r2
        L_0x00ca:
            if (r7 != 0) goto L_0x00ce
            X.0qv r7 = X.C13220qv.A0I
        L_0x00ce:
            r0 = 66
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            android.os.Parcelable r6 = r10.getParcelableExtra(r0)
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r6 = (com.facebook.messaging.threadview.params.MessageDeepLinkInfo) r6
            int r2 = X.AnonymousClass1Y3.BGz
            X.0UN r1 = r9.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0qw r0 = (X.C13230qw) r0
            java.lang.String r1 = "cowatch_launcher_params_json"
            java.lang.String r1 = r10.getStringExtra(r1)
            r5 = 0
            if (r1 == 0) goto L_0x0178
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0178 }
            r4.<init>(r1)     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r1 = "cowatch_launcher_key_thread_id"
            java.lang.String r2 = r4.getString(r1)     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r1 = "cowatch_launcher_key_is_canonical"
            boolean r1 = r4.getBoolean(r1)     // Catch:{ JSONException -> 0x0178 }
            if (r2 != 0) goto L_0x0102
            goto L_0x0124
        L_0x0102:
            if (r1 == 0) goto L_0x011b
            long r2 = java.lang.Long.parseLong(r2)     // Catch:{ JSONException -> 0x0178 }
            X.0Tq r0 = r0.A00     // Catch:{ JSONException -> 0x0178 }
            java.lang.Object r0 = r0.get()     // Catch:{ JSONException -> 0x0178 }
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r0 = r0.A0j     // Catch:{ JSONException -> 0x0178 }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ JSONException -> 0x0178 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A04(r2, r0)     // Catch:{ JSONException -> 0x0178 }
            goto L_0x0125
        L_0x011b:
            long r0 = java.lang.Long.parseLong(r2)     // Catch:{ JSONException -> 0x0178 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A00(r0)     // Catch:{ JSONException -> 0x0178 }
            goto L_0x0125
        L_0x0124:
            r0 = 0
        L_0x0125:
            if (r0 == 0) goto L_0x0178
            X.0r0 r3 = com.facebook.messaging.cowatch.launcher.parameters.CoWatchLauncherParams.A00(r0)     // Catch:{ JSONException -> 0x0178 }
            com.facebook.graphql.enums.GraphQLLivingRoomEntrySource r0 = com.facebook.graphql.enums.GraphQLLivingRoomEntrySource.A06     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r1 = r0.name()     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r0 = "cowatch_launcher_key_join_surface"
            java.lang.String r1 = r4.optString(r0, r1)     // Catch:{ JSONException -> 0x0178 }
            com.facebook.graphql.enums.GraphQLLivingRoomEntrySource r0 = com.facebook.graphql.enums.GraphQLLivingRoomEntrySource.A06     // Catch:{ JSONException -> 0x0178 }
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r1, r0)     // Catch:{ JSONException -> 0x0178 }
            com.facebook.graphql.enums.GraphQLLivingRoomEntrySource r0 = (com.facebook.graphql.enums.GraphQLLivingRoomEntrySource) r0     // Catch:{ JSONException -> 0x0178 }
            r3.A00 = r0     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r1 = "cowatch_launcher_key_try_optimistic"
            boolean r0 = r4.optBoolean(r1, r12)     // Catch:{ JSONException -> 0x0178 }
            r3.A04 = r0     // Catch:{ JSONException -> 0x0178 }
            X.0r9 r2 = new X.0r9     // Catch:{ JSONException -> 0x0178 }
            r2.<init>()     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r0 = "cowatch_launcher_key_video_id"
            java.lang.String r1 = r4.getString(r0)     // Catch:{ JSONException -> 0x0178 }
            r2.A02 = r1     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r0 = "videoId"
            X.C28931fb.A06(r1, r0)     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r0 = "cowatch_launcher_key_video_attribution"
            java.lang.String r0 = r4.optString(r0, r5)     // Catch:{ JSONException -> 0x0178 }
            r2.A01 = r0     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r0 = "cowatch_launcher_key_video_title"
            java.lang.String r0 = r4.optString(r0, r5)     // Catch:{ JSONException -> 0x0178 }
            r2.A03 = r0     // Catch:{ JSONException -> 0x0178 }
            com.facebook.messaging.cowatch.launcher.parameters.VideoInfo r0 = new com.facebook.messaging.cowatch.launcher.parameters.VideoInfo     // Catch:{ JSONException -> 0x0178 }
            r0.<init>(r2)     // Catch:{ JSONException -> 0x0178 }
            r3.A01 = r0     // Catch:{ JSONException -> 0x0178 }
            com.facebook.messaging.cowatch.launcher.parameters.CoWatchLauncherParams r0 = new com.facebook.messaging.cowatch.launcher.parameters.CoWatchLauncherParams     // Catch:{ JSONException -> 0x0178 }
            r0.<init>(r3)     // Catch:{ JSONException -> 0x0178 }
            r5 = r0
        L_0x0178:
            r0 = 1428(0x594, float:2.001E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            int r2 = r10.getIntExtra(r0, r12)
            java.lang.String r0 = "show_add_members_page"
            boolean r1 = r10.getBooleanExtra(r0, r12)
            X.0rA r0 = new X.0rA
            r0.<init>()
            r0.A01(r14)
            r0.A04 = r6
            r0.A03 = r11
            r0.A02(r7)
            r0.A05 = r8
            r0.A01 = r5
            r0.A00 = r2
            r0.A07 = r1
            com.facebook.messaging.threadview.params.ThreadViewParams r1 = r0.A00()
            X.0qf r0 = r13.A01
            X.1lX r0 = A00(r0)
            r0.A07(r1)
        L_0x01ac:
            java.lang.String r0 = "show_inbox"
            boolean r0 = r10.getBooleanExtra(r0, r12)
            if (r0 == 0) goto L_0x01bd
            X.0qf r0 = r13.A01
            X.1lX r0 = A00(r0)
            r0.A05()
        L_0x01bd:
            java.lang.String r0 = "EXTRA_SHOW_MONTAGE_COMPOSER"
            boolean r0 = r10.getBooleanExtra(r0, r12)
            if (r0 == 0) goto L_0x01f5
            java.lang.String r0 = "EXTRA_MONTAGE_ENTRY_POINT"
            java.io.Serializable r7 = r10.getSerializableExtra(r0)
            X.3g5 r7 = (X.C73443g5) r7
            X.1lW r6 = r13.A00
            X.0qf r0 = r13.A01
            android.app.Activity r5 = r0.A00
            java.lang.String r0 = r7.toString()
            com.facebook.messaging.send.trigger.NavigationTrigger r4 = com.facebook.messaging.send.trigger.NavigationTrigger.A00(r0)
            X.0qf r0 = r13.A01
            X.0UN r3 = r0.A01
            X.3g7 r2 = X.C73453g6.A00(r7)
            int r1 = X.AnonymousClass1Y3.ASj
            r0 = 8
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.3g8 r0 = (X.C73473g8) r0
            com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams r0 = com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams.A01(r7, r2, r0)
            r6.BvS(r5, r4, r0)
            return
        L_0x01f5:
            java.lang.String r0 = "EXTRA_SHOW_MONTAGE_POST_UPSELL"
            boolean r0 = r10.getBooleanExtra(r0, r12)
            if (r0 == 0) goto L_0x0211
            int r1 = X.AnonymousClass1Y3.APr
            X.0UN r0 = r9.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Y6 r3 = (X.AnonymousClass1Y6) r3
            X.0u4 r2 = new X.0u4
            r2.<init>()
            r0 = 1200(0x4b0, double:5.93E-321)
            r3.BxS(r2, r0)
        L_0x0211:
            r0 = 209(0xd1, float:2.93E-43)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            r10.getBooleanExtra(r1, r12)
            r0 = 490(0x1ea, float:6.87E-43)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r10.hasExtra(r1)
            if (r0 == 0) goto L_0x03a6
            X.0qf r0 = r13.A01
            X.1lX r0 = A00(r0)
            r0.A05()
            java.lang.String r5 = r10.getStringExtra(r1)
            if (r5 == 0) goto L_0x024e
            r0 = 11
            java.lang.Integer[] r3 = X.AnonymousClass07B.A00(r0)
            int r2 = r3.length
            r1 = 0
        L_0x023d:
            if (r1 >= r2) goto L_0x024e
            r4 = r3[r1]
            java.lang.String r0 = X.AnonymousClass21B.A00(r4)
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x024f
            int r1 = r1 + 1
            goto L_0x023d
        L_0x024e:
            r4 = 0
        L_0x024f:
            if (r4 != 0) goto L_0x0253
            java.lang.Integer r4 = X.AnonymousClass07B.A00
        L_0x0253:
            java.lang.Integer r0 = X.AnonymousClass07B.A0p
            java.lang.String r1 = "EXTRA_FOCUS_TAB_ENTRY_POINT"
            if (r4 != r0) goto L_0x039a
            r10.getStringExtra(r1)
            java.lang.Integer r1 = X.AnonymousClass07B.A0p
            r0 = 0
            r13.A00(r1, r0)
        L_0x0262:
            java.lang.String r0 = "extra_show_nux_tincan"
            boolean r0 = r10.getBooleanExtra(r0, r12)
            if (r0 == 0) goto L_0x02d4
            int r1 = X.AnonymousClass1Y3.B6q
            X.0UN r0 = r9.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0
            X.1hn r2 = r0.edit()
            X.1Y7 r1 = X.C14810uA.A02
            r0 = 1
            r2.putBoolean(r1, r0)
            r2.commit()
            com.facebook.messaging.messengerprefs.tincan.TincanNuxFragment r2 = new com.facebook.messaging.messengerprefs.tincan.TincanNuxFragment
            r2.<init>()
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            r2.A1P(r0)
            java.lang.String r1 = "TincanNuxFragment"
            r0 = r16
            r2.A25(r0, r1)
        L_0x0295:
            java.lang.Integer r0 = X.AnonymousClass07B.A0i
            if (r4 != r0) goto L_0x0445
            java.lang.String r0 = "EXTRA_PAGE_REPLY_URI"
            java.lang.String r3 = r10.getStringExtra(r0)
            java.lang.String r0 = "EXTRA_PAGE_REPLY_PAGE_ID"
            java.lang.String r2 = r10.getStringExtra(r0)
            java.lang.String r0 = "EXTRA_PAGE_REPLY_SENDER_ID"
            java.lang.String r5 = r10.getStringExtra(r0)
            java.lang.String r0 = "EXTRA_PAGE_REPLY_NOTIF_ID"
            java.lang.String r6 = r10.getStringExtra(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)
            if (r0 != 0) goto L_0x0445
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r0 != 0) goto L_0x0445
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 != 0) goto L_0x0445
            int r1 = X.AnonymousClass1Y3.Ajf
            X.0UN r0 = r9.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.3gA r0 = (X.C73483gA) r0
            java.lang.Integer r4 = X.AnonymousClass07B.A01
            r1 = r15
            r0.A01(r1, r2, r3, r4, r5, r6)
            return
        L_0x02d4:
            r0 = 1387(0x56b, float:1.944E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r10.getBooleanExtra(r0, r12)
            if (r0 == 0) goto L_0x0332
            int r1 = X.AnonymousClass1Y3.BOk
            X.0UN r0 = r9.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r1, r0)
            X.0l9 r8 = (X.C10960l9) r8
            int r1 = X.AnonymousClass1Y3.BEM
            X.0UN r0 = r9.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A03(r1, r0)
            X.2rr r6 = (X.C57362rr) r6
            int r1 = X.AnonymousClass1Y3.Awq
            X.0UN r0 = r9.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r1, r0)
            java.lang.String r5 = (java.lang.String) r5
            int r1 = X.AnonymousClass1Y3.AFw
            X.0UN r0 = r9.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Ha r3 = (X.C21451Ha) r3
            X.0rX r2 = new X.0rX
            r2.<init>(r15)
            r0 = 2131823885(0x7f110d0d, float:1.9280582E38)
            r2.A08(r0)
            r1 = 2131823886(0x7f110d0e, float:1.9280584E38)
            X.0r3 r0 = new X.0r3
            r0.<init>(r8, r5, r3, r6)
            r2.A02(r1, r0)
            r1 = 2131823889(0x7f110d11, float:1.928059E38)
            X.0r4 r0 = new X.0r4
            r0.<init>(r9, r15)
            r2.A00(r1, r0)
            X.3At r0 = r2.A06()
            r0.show()
            goto L_0x0295
        L_0x0332:
            java.lang.String r1 = "update_app_text"
            java.lang.String r0 = r10.getStringExtra(r1)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 != 0) goto L_0x0361
            java.lang.String r0 = r10.getStringExtra(r1)
            X.0rX r2 = new X.0rX
            r2.<init>(r15)
            r2.A0C(r0)
            r1 = 2131821645(0x7f11044d, float:1.927604E38)
            X.0rL r0 = new X.0rL
            r0.<init>(r9, r15)
            r2.A02(r1, r0)
            r1 = 2131823842(0x7f110ce2, float:1.9280495E38)
            r0 = 0
            r2.A00(r1, r0)
            r2.A07()
            goto L_0x0295
        L_0x0361:
            java.lang.String r1 = "switch_accounts_text"
            java.lang.String r0 = r10.getStringExtra(r1)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 != 0) goto L_0x0295
            java.lang.String r1 = r10.getStringExtra(r1)
            r0 = 236(0xec, float:3.31E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            java.lang.String r5 = r10.getStringExtra(r0)
            X.0rX r2 = new X.0rX
            r2.<init>(r15)
            r2.A0C(r1)
            r1 = 2131823889(0x7f110d11, float:1.928059E38)
            X.0rM r0 = new X.0rM
            r0.<init>(r9, r15, r5)
            r2.A02(r1, r0)
            r1 = 2131823842(0x7f110ce2, float:1.9280495E38)
            r0 = 0
            r2.A00(r1, r0)
            r2.A07()
            goto L_0x0295
        L_0x039a:
            android.os.Bundle r0 = r10.getExtras()
            r10.getStringExtra(r1)
            r13.A00(r4, r0)
            goto L_0x0262
        L_0x03a6:
            r0 = 1045(0x415, float:1.464E-42)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r10.getBooleanExtra(r1, r12)
            r10.putExtra(r1, r0)
            r0 = 20
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r10.getBooleanExtra(r1, r12)
            r10.putExtra(r1, r0)
            r0 = 492(0x1ec, float:6.9E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r10.getBooleanExtra(r0, r12)
            if (r0 == 0) goto L_0x03e8
            r0 = 488(0x1e8, float:6.84E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            android.os.Parcelable r2 = r10.getParcelableExtra(r0)
            com.facebook.messaging.groups.create.model.CreateGroupFragmentParams r2 = (com.facebook.messaging.groups.create.model.CreateGroupFragmentParams) r2
            int r1 = X.AnonymousClass1Y3.B3b
            X.0UN r0 = r9.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.3fk r1 = (X.C73243fk) r1
            r0 = r16
            r1.A02(r15, r0, r2)
            return
        L_0x03e8:
            r0 = 40
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r10.hasExtra(r0)
            if (r0 == 0) goto L_0x0416
            r0 = 109(0x6d, float:1.53E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            java.util.ArrayList r3 = r10.getParcelableArrayListExtra(r0)
            java.lang.String r0 = "EXTRA_OMNI_PICKER_ENTRY_POINT"
            r10.getStringExtra(r0)
            X.1lW r2 = r13.A00
            X.0qf r0 = r13.A01
            android.app.Activity r1 = r0.A00
            if (r3 != 0) goto L_0x0411
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
        L_0x040d:
            r2.BvV(r1, r0)
            return
        L_0x0411:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r3)
            goto L_0x040d
        L_0x0416:
            r0 = 79
            java.lang.String r1 = X.C99084oO.$const$string(r0)
            android.os.Parcelable r0 = r10.getParcelableExtra(r1)
            if (r0 == 0) goto L_0x0445
            android.os.Bundle r0 = r10.getExtras()
            android.os.Parcelable r3 = r0.getParcelable(r1)
            com.facebook.messaging.groups.links.datamodel.GroupLinkThreadInfoParam r3 = (com.facebook.messaging.groups.links.datamodel.GroupLinkThreadInfoParam) r3
            java.lang.String r0 = "preview_link_hash"
            java.lang.String r1 = r10.getStringExtra(r0)
            java.lang.String r2 = "group_link_join_fragment_tag"
            r0 = r16
            androidx.fragment.app.Fragment r0 = r0.A0Q(r2)
            if (r0 != 0) goto L_0x0445
            com.facebook.messaging.groups.links.GroupLinkJoinRequestFragment r1 = com.facebook.messaging.groups.links.GroupLinkJoinRequestFragment.A00(r3, r1)
            r0 = r16
            r1.A25(r0, r2)
        L_0x0445:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13100qf.A04():void");
    }

    private void A05() {
        if (!this.A08 && !this.A03 && this.A01) {
            Fragment A0O = A00(this).A01.A0O(2131300895);
            Preconditions.checkNotNull(A0O);
            C13450rS.A07((C13450rS) A0O, null);
            this.A08 = true;
        }
    }

    public static void A06(C13100qf r4) {
        r4.A01.B9w().setBackgroundDrawable(new ColorDrawable(((MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, r4.A01)).B9m()));
    }

    public void A0A() {
        ((C13980sP) AnonymousClass1XX.A02(10, AnonymousClass1Y3.AXL, this.A01)).A02(this.A0D);
        super.A0A();
        this.A00.A01();
    }

    public void A0B() {
        if (this.A04) {
            this.A0A.A01();
        }
        super.A0B();
        C32301lX r0 = this.A05;
        if (r0 != null) {
            C13260r5 r2 = (C13260r5) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ase, r0.A00);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r2.A00)).AOz();
            C13260r5.A01(r2.A01);
            C13260r5.A01(r2.A02);
        }
        AnonymousClass13I r1 = (AnonymousClass13I) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AxD, this.A01);
        int i = r1.A00 - 1;
        r1.A00 = i;
        if (i == 0) {
            ((AnonymousClass13J) r1.A04.get()).A03(false);
        }
    }

    public void A0C() {
        boolean Aem = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).Aem(286839390936323L);
        if (Aem) {
            int AqL = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).AqL(568314368035082L, -4);
            int AqL2 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).AqL(568314367969545L, Integer.MAX_VALUE);
            C34731q5 r0 = this.A0A;
            r0.A0D = Aem;
            r0.A03 = AqL;
            r0.A04 = AqL2;
        }
        boolean Aem2 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).Aem(286839391067397L);
        if (Aem2) {
            int AqL3 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).AqL(568314368166156L, -4);
            int AqL4 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).AqL(568314368100619L, Integer.MAX_VALUE);
            C34731q5 r02 = this.A0A;
            r02.A0C = Aem2;
            r02.A01 = AqL3;
            r02.A02 = AqL4;
        }
        boolean Aem3 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).Aem(286839391198471L);
        if (Aem3) {
            int AqL5 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).AqL(568314368297230L, 0);
            int AqL6 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).AqL(568314368362767L, 0);
            int AqL7 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01)).AqL(568314368231693L, Integer.MAX_VALUE);
            C34731q5 r3 = this.A0A;
            C73263fn A002 = C73263fn.A00(AqL5);
            if (A002 != null) {
                r3.A0B = Aem3;
                r3.A05 = new C34761q8(A002, AqL6);
                r3.A00 = AqL7;
            }
        }
        if (Aem || Aem2 || Aem3) {
            this.A04 = true;
            this.A0A.A00();
        }
        super.A0C();
        C32301lX A003 = A00(this);
        if (A003.A01.A0O(2131301002) == null) {
            ((C13260r5) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ase, A003.A00)).A02(new AnonymousClass13F(A003));
            C32311lY r5 = (C32311lY) AnonymousClass1XX.A03(AnonymousClass1Y3.Akp, A003.A00);
            if (r5.A00.Aem(284979670095177L)) {
                AnonymousClass108 r1 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, A003.A00);
                r1.A02(new C15840w4(A003));
                r1.A02 = "PreloadMontage";
                r1.A03("ForUiThread");
                ((AnonymousClass0g4) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ASI, A003.A00)).A04(r1.A01(), "KeepExisting");
            } else if (r5.A00.Aem(284979670029640L)) {
                ((C13260r5) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ase, A003.A00)).A02(new C34781qA(A003));
            }
        }
        if (C32301lX.A03(A003)) {
            ((C13400rN) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A7C, A003.A00)).A03.A00 = "thread";
        } else if (C32301lX.A02(A003)) {
            ((C13400rN) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A7C, A003.A00)).A03.A00 = "search";
        } else {
            C13400rN r03 = (C13400rN) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A7C, A003.A00);
            r03.A03.A00 = r03.A02;
        }
        if (((C16920y2) AnonymousClass1XX.A03(AnonymousClass1Y3.B0t, this.A01)).A00.Aem(282256661021965L) && !this.A03 && this.A01) {
            C17050yF r6 = (C17050yF) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8I, this.A01);
            r6.A01.add(r6.A00.CIG("Aloha handoff banner init", new C34771q9(this), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE, AnonymousClass07B.A00));
        }
        AnonymousClass13I r2 = (AnonymousClass13I) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AxD, this.A01);
        int i = r2.A00 + 1;
        r2.A00 = i;
        if (i == 1) {
            ((AnonymousClass13J) r2.A04.get()).A03(true);
        }
        int i2 = AnonymousClass1Y3.AxD;
        AnonymousClass0UN r32 = this.A01;
        ((AnonymousClass13I) AnonymousClass1XX.A02(5, i2, r32)).A01((AnonymousClass0WP) AnonymousClass1XX.A02(6, AnonymousClass1Y3.A8a, r32));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002d, code lost:
        if (r1.A02(r5) != null) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0H(android.os.Bundle r12) {
        /*
            r11 = this;
            int r2 = X.AnonymousClass1Y3.AgL
            X.0UN r1 = r11.A01
            r0 = 4
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0vz r4 = (X.C15790vz) r4
            android.app.Activity r3 = r11.A00
            X.0qc r0 = r11.A01
            android.content.Intent r5 = r0.getIntent()
            int r1 = X.AnonymousClass1Y3.A6j
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.0ci r1 = (X.C07150ci) r1
            r2 = 1
            if (r12 == 0) goto L_0x0028
            java.lang.String r0 = "EXTRA_SELECTED_THREAD"
            boolean r0 = r12.containsKey(r0)
            if (r0 != 0) goto L_0x002f
        L_0x0028:
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r1.A02(r5)
            r0 = 1
            if (r1 == 0) goto L_0x0030
        L_0x002f:
            r0 = 0
        L_0x0030:
            if (r0 == 0) goto L_0x00d3
            X.0rV r5 = r4.A01
            java.lang.Class<com.facebook.orca.threadlist.ThreadListFragment> r8 = com.facebook.orca.threadlist.ThreadListFragment.class
            monitor-enter(r5)
            X.04b r0 = r5.A01     // Catch:{ all -> 0x00c1 }
            boolean r0 = r0.containsKey(r8)     // Catch:{ all -> 0x00c1 }
            if (r0 == 0) goto L_0x0046
            X.04b r0 = r5.A01     // Catch:{ all -> 0x00c1 }
            r0.get(r8)     // Catch:{ all -> 0x00c1 }
        L_0x0044:
            monitor-exit(r5)     // Catch:{ all -> 0x00c1 }
            goto L_0x00c4
        L_0x0046:
            r6 = 0
            int r1 = X.AnonymousClass1Y3.Aup     // Catch:{ all -> 0x00c1 }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x00c1 }
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x00c1 }
            X.0rW r10 = (X.C13490rW) r10     // Catch:{ all -> 0x00c1 }
            if (r8 != 0) goto L_0x0054
            goto L_0x0086
        L_0x0054:
            java.lang.String r1 = r8.getName()     // Catch:{ all -> 0x00c1 }
            boolean r0 = r1.equals(r1)     // Catch:{ all -> 0x00c1 }
            if (r0 == 0) goto L_0x0061
            java.lang.String r0 = "com.facebook.orca.threadlist.ThreadListFragment"
            goto L_0x0062
        L_0x0061:
            r0 = 0
        L_0x0062:
            r9 = r0
            r6 = 0
            if (r0 == 0) goto L_0x0087
            r7 = -1
            int r1 = r0.hashCode()     // Catch:{ all -> 0x00c1 }
            r0 = -835706388(0xffffffffce3021ec, float:-7.3875328E8)
            if (r1 != r0) goto L_0x0079
            java.lang.String r0 = "com.facebook.orca.threadlist.ThreadListFragment"
            boolean r0 = r9.equals(r0)     // Catch:{ all -> 0x00c1 }
            if (r0 == 0) goto L_0x0079
            r7 = 0
        L_0x0079:
            if (r7 != 0) goto L_0x0087
            int r1 = X.AnonymousClass1Y3.AdN     // Catch:{ all -> 0x00c1 }
            X.0UN r0 = r10.A00     // Catch:{ all -> 0x00c1 }
            java.lang.Object r6 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x00c1 }
            X.0kY r6 = (X.C10620kY) r6     // Catch:{ all -> 0x00c1 }
            goto L_0x0087
        L_0x0086:
            r6 = 0
        L_0x0087:
            if (r6 == 0) goto L_0x0044
            X.04b r0 = r5.A01     // Catch:{ all -> 0x00c1 }
            r0.put(r8, r6)     // Catch:{ all -> 0x00c1 }
            r6.A01()     // Catch:{ all -> 0x00c1 }
            java.util.concurrent.atomic.AtomicBoolean r0 = r6.A02     // Catch:{ all -> 0x00c1 }
            boolean r0 = r0.get()     // Catch:{ all -> 0x00c1 }
            if (r0 != 0) goto L_0x0044
            monitor-enter(r6)     // Catch:{ all -> 0x00c1 }
            java.lang.Thread r0 = r6.A00     // Catch:{ all -> 0x00be }
            if (r0 != 0) goto L_0x00bc
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ all -> 0x00be }
            X.0qS r0 = new X.0qS     // Catch:{ all -> 0x00be }
            r0.<init>(r6, r5)     // Catch:{ all -> 0x00be }
            r1.<init>(r0)     // Catch:{ all -> 0x00be }
            r6.A00 = r1     // Catch:{ all -> 0x00be }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x00be }
            int r1 = r0.getPriority()     // Catch:{ all -> 0x00be }
            java.lang.Thread r0 = r6.A00     // Catch:{ all -> 0x00be }
            r0.setPriority(r1)     // Catch:{ all -> 0x00be }
            java.lang.Thread r0 = r6.A00     // Catch:{ all -> 0x00be }
            r0.start()     // Catch:{ all -> 0x00be }
        L_0x00bc:
            monitor-exit(r6)     // Catch:{ all -> 0x00be }
            goto L_0x0044
        L_0x00be:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00be }
            throw r0     // Catch:{ all -> 0x00c1 }
        L_0x00c1:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00c1 }
            throw r0
        L_0x00c4:
            int r1 = X.AnonymousClass1Y3.B4R
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.0l5 r1 = (X.C10920l5) r1
            X.1Lh r0 = r4.A02
            r0.A02(r3, r1, r2)
        L_0x00d3:
            int r2 = X.AnonymousClass1Y3.AXL
            X.0UN r1 = r11.A01
            r0 = 10
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0sP r1 = (X.C13980sP) r1
            X.0qT r0 = r11.A0D
            r1.A01(r0)
            A06(r11)
            android.app.Activity r0 = r11.A00
            X.0ra r3 = X.C13950sM.A00(r0)
            r0 = 1
            r3.A07(r0)
            r11.A02()
            android.app.Activity r0 = r11.A00
            X.16W r1 = X.C13950sM.A01(r0)
            r0 = 2131301375(0x7f0913ff, float:1.8220806E38)
            r1.A02(r0)
            android.view.View r0 = r1.A00
            r3.A04(r0)
            X.0vt r0 = A03(r11)
            android.view.View r0 = r0.A02()
            r3.A04(r0)
            A00(r11)
            android.app.Activity r4 = r11.A00
            X.16W r2 = X.C13950sM.A01(r4)
            r0 = -1
            r2.A03(r0, r0)
            X.16W r1 = X.C13950sM.A01(r4)
            r0 = 2131300895(0x7f09121f, float:1.8219833E38)
            r1.A02(r0)
            r2.A06(r1)
            X.16W r1 = X.C13950sM.A01(r4)
            r0 = 2131298121(0x7f090749, float:1.8214206E38)
            r1.A02(r0)
            r2.A06(r1)
            X.16W r1 = X.C13950sM.A01(r4)
            r0 = 2131300410(0x7f09103a, float:1.8218849E38)
            r1.A02(r0)
            r2.A06(r1)
            X.16W r1 = X.C13950sM.A01(r4)
            r0 = 2131301002(0x7f09128a, float:1.822005E38)
            r1.A02(r0)
            r2.A06(r1)
            X.16W r1 = X.C13950sM.A01(r4)
            r0 = 2131299191(0x7f090b77, float:1.8216376E38)
            r1.A02(r0)
            r2.A06(r1)
            android.view.View r0 = r2.A00
            r3.A04(r0)
            android.view.View r2 = r3.A00
            com.facebook.widget.CustomLinearLayout r2 = (com.facebook.widget.CustomLinearLayout) r2
            r11.A02 = r2
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r1 <= r0) goto L_0x0174
            r0 = 8
            r2.setImportantForAutofill(r0)
        L_0x0174:
            com.facebook.widget.CustomLinearLayout r1 = r11.A02
            X.0qc r0 = r11.A01
            r0.C7C(r1)
            X.1lX r2 = A00(r11)
            X.0qn r3 = X.C32301lX.A00(r2)
            r3.A00()
            boolean r0 = X.C32301lX.A03(r2)
            if (r0 != 0) goto L_0x01ac
            int r1 = X.AnonymousClass1Y3.Akp
            X.0UN r0 = r2.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.1lY r0 = (X.C32311lY) r0
            X.1Yd r2 = r0.A00
            r0 = 282235187430647(0x100b1001704f7, double:1.39442710157048E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x01ac
            androidx.fragment.app.Fragment r1 = r3.A04
            if (r1 == 0) goto L_0x01ac
            X.0wo r0 = r3.A05
            r0.A0I(r1)
        L_0x01ac:
            r3.A01()
            X.0qk r2 = r11.A02()
            X.0qW r1 = r2.A01
            r0 = 2131301375(0x7f0913ff, float:1.8220806E38)
            androidx.fragment.app.Fragment r0 = r1.A0O(r0)
            if (r0 != 0) goto L_0x01db
            int r1 = X.AnonymousClass1Y3.A9V
            X.0UN r0 = r2.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r1, r0)
            X.1qE r3 = (X.C34821qE) r3
            X.0qW r0 = r2.A01
            X.0wo r2 = r0.A0T()
            r1 = 2131301375(0x7f0913ff, float:1.8220806E38)
            androidx.fragment.app.Fragment r0 = r3.A01()
            r2.A08(r1, r0)
            r2.A02()
        L_0x01db:
            r0 = 0
            if (r12 != 0) goto L_0x01df
            r0 = 1
        L_0x01df:
            r11.A09 = r0
            int r2 = X.AnonymousClass1Y3.AKb
            X.0UN r1 = r11.A01
            r0 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0Ut r0 = (X.C04460Ut) r0
            X.0bl r2 = r0.BMm()
            X.0rR r1 = new X.0rR
            r1.<init>(r11)
            java.lang.String r0 = "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"
            r2.A02(r0, r1)
            X.0c5 r0 = r2.A00()
            r11.A00 = r0
            r0.A00()
            int r2 = X.AnonymousClass1Y3.A8I
            X.0UN r1 = r11.A01
            r0 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0yF r5 = (X.C17050yF) r5
            X.0qo r4 = new X.0qo
            r4.<init>(r11)
            X.0XV r3 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            java.lang.String r1 = "fetchTextStyles"
            X.0WP r0 = r5.A00
            X.0XX r1 = r0.CIG(r1, r4, r3, r2)
            java.util.List r0 = r5.A01
            r0.add(r1)
            int r1 = X.AnonymousClass1Y3.AXH
            X.0UN r0 = r11.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.1gZ r1 = (X.C29531gZ) r1
            android.app.Activity r0 = r11.A00
            r1.A01(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13100qf.A0H(android.os.Bundle):void");
    }

    private C13100qf(AnonymousClass1XY r3) {
        if (C34731q5.A0F == null) {
            C34731q5.A0F = new C34731q5();
        }
        this.A0A = C34731q5.A0F;
        this.A04 = false;
        this.A0D = new C13160qp(this);
        this.A0B = new C34741q6(this);
        this.A01 = new AnonymousClass0UN(12, r3);
        this.A0C = AnonymousClass0VG.A00(AnonymousClass1Y3.AJK, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00bb, code lost:
        if (r1.A0b != false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d7, code lost:
        if (((X.C13450rS) r0).A2U() != false) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x004d, code lost:
        if (((com.facebook.orca.threadview.ThreadViewFragment) r2.A01.A0O(2131301002)).A15 != false) goto L_0x004f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0052 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00dc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09() {
        /*
            r4 = this;
            X.1lX r2 = A00(r4)
            int r3 = X.AnonymousClass1Y3.A7C
            X.0UN r1 = r2.A00
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.0rN r0 = (X.C13400rN) r0
            X.0rO r0 = r0.A03
            java.lang.String r1 = "tap_back_button"
            X.1Ti r0 = r0.A01
            X.146 r0 = r0.A01
            r0.A0D(r1)
            boolean r0 = X.C32301lX.A03(r2)
            if (r0 == 0) goto L_0x0053
            r1 = 2131301002(0x7f09128a, float:1.822005E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r1 = r0.A0O(r1)
            com.facebook.orca.threadview.ThreadViewFragment r1 = (com.facebook.orca.threadview.ThreadViewFragment) r1
            java.lang.String r0 = "back"
            boolean r0 = r1.A2a(r0)
            if (r0 != 0) goto L_0x00d9
            X.1la r0 = r2.A07
            r0.ATL()
            r1 = 2131301002(0x7f09128a, float:1.822005E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r0 = r0.A0O(r1)
            if (r0 == 0) goto L_0x00d9
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r0 = r0.A0O(r1)
            com.facebook.orca.threadview.ThreadViewFragment r0 = (com.facebook.orca.threadview.ThreadViewFragment) r0
            boolean r0 = r0.A15
            if (r0 == 0) goto L_0x00d9
        L_0x004f:
            r0 = 0
        L_0x0050:
            if (r0 == 0) goto L_0x00dc
            return
        L_0x0053:
            boolean r0 = X.C32301lX.A02(r2)
            if (r0 == 0) goto L_0x00ad
            r1 = 2131300410(0x7f09103a, float:1.8218849E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r3 = r0.A0O(r1)
            X.0wL r3 = (X.C16010wL) r3
            X.0qW r1 = r3.A17()
            java.lang.String r0 = "edit_history_fragment_tag"
            androidx.fragment.app.Fragment r0 = r1.A0Q(r0)
            if (r0 == 0) goto L_0x0090
            X.3gD r0 = X.C16010wL.A00(r3)
            boolean r0 = r0.A08()
            if (r0 == 0) goto L_0x0081
            X.3gD r0 = X.C16010wL.A00(r3)
            r0.A02()
        L_0x0081:
            X.0qW r0 = r3.A17()
            r0.A0a()
            r0 = 1
        L_0x0089:
            if (r0 != 0) goto L_0x00d9
            r2.A06()
            r0 = 1
            goto L_0x0050
        L_0x0090:
            X.0qW r0 = r3.A17()
            java.lang.String r1 = "tabbed_search_fragment_tag"
            androidx.fragment.app.Fragment r0 = r0.A0Q(r1)
            if (r0 == 0) goto L_0x00ab
            X.0qW r0 = r3.A17()
            androidx.fragment.app.Fragment r0 = r0.A0Q(r1)
            X.3eW r0 = (X.C72573eW) r0
            boolean r0 = r0.A2W()
            goto L_0x0089
        L_0x00ab:
            r0 = 0
            goto L_0x0089
        L_0x00ad:
            r1 = 2131298121(0x7f090749, float:1.8214206E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r1 = r0.A0O(r1)
            if (r1 == 0) goto L_0x00bd
            boolean r1 = r1.A0b
            r0 = 1
            if (r1 == 0) goto L_0x00be
        L_0x00bd:
            r0 = 0
        L_0x00be:
            if (r0 == 0) goto L_0x00c5
            r2.A05()
            r0 = 1
            goto L_0x0050
        L_0x00c5:
            r1 = 2131300895(0x7f09121f, float:1.8219833E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r1 = r0.A0O(r1)
            r0 = r1
            if (r1 == 0) goto L_0x004f
            X.0rS r0 = (X.C13450rS) r0
            boolean r0 = r0.A2U()
            if (r0 == 0) goto L_0x004f
        L_0x00d9:
            r0 = 1
            goto L_0x0050
        L_0x00dc:
            int r1 = X.AnonymousClass1Y3.BBP
            X.0UN r0 = r4.A01
            X.AnonymousClass1XX.A03(r1, r0)
            int r1 = X.AnonymousClass1Y3.Akp
            X.0UN r0 = r4.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.1lY r1 = (X.C32311lY) r1
            boolean r0 = com.facebook.common.perftest.base.PerfTestConfigBase.A00()
            if (r0 != 0) goto L_0x011c
            X.1Yd r2 = r1.A00
            r0 = 2306125244400862452(0x200100b1001304f4, double:1.5851491999040315E-154)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x011c
            r1 = 1
            X.0qc r0 = r4.A01
            boolean r0 = r0.BLI(r1)
            if (r0 == 0) goto L_0x011c
            android.app.Activity r0 = r4.A00
            com.facebook.messenger.neue.MainActivity r0 = (com.facebook.messenger.neue.MainActivity) r0
            int r2 = X.AnonymousClass1Y3.AHa
            X.0UN r1 = r0.A01
            r0 = 7
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0nQ r0 = (X.C11590nQ) r0
            r0.A0U()
            return
        L_0x011c:
            super.A09()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13100qf.A09():void");
    }

    public void A0D() {
        super.A0D();
        A05();
        A04();
    }

    public void A0E(Intent intent) {
        super.A0E(intent);
        this.A01.C8U(intent);
        this.A09 = true;
        A04();
    }

    public void A0F(Bundle bundle) {
        ThreadKey A042;
        super.A0F(bundle);
        C32301lX A002 = A00(this);
        if (C32301lX.A03(A002) && (A042 = A002.A04()) != null) {
            bundle.putParcelable("EXTRA_SELECTED_THREAD", A042);
        }
    }

    public void A0G(Fragment fragment) {
        super.A0G(fragment);
        C32301lX A002 = A00(this);
        C32331la r2 = A002.A07;
        if (fragment instanceof C13450rS) {
            ((C13450rS) fragment).A09.A00 = r2;
        }
        if (fragment instanceof ThreadViewFragment) {
            ((ThreadViewFragment) fragment).A0b = new C32341lb(r2);
        }
        if (fragment instanceof C16010wL) {
            ((C16010wL) fragment).A01 = new C34791qB(r2);
        }
        C55632oP A012 = ((C33661nw) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AJS, A002.A00)).A01();
        if (A012 != null) {
            A012.A02(fragment, A002.A07);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
        if (r1.A0b != false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean ARW() {
        /*
            r3 = this;
            X.1lX r2 = A00(r3)
            r1 = 2131300895(0x7f09121f, float:1.8219833E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r1 = r0.A0O(r1)
            if (r1 == 0) goto L_0x0014
            boolean r1 = r1.A0b
            r0 = 1
            if (r1 == 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            if (r0 == 0) goto L_0x0054
            r1 = 2131300895(0x7f09121f, float:1.8219833E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r0 = r0.A0O(r1)
            com.google.common.base.Preconditions.checkNotNull(r0)
            X.0rS r0 = (X.C13450rS) r0
            X.0qW r2 = r0.A17()
            X.0wO r1 = r0.A2S()
            X.13r r0 = X.C13450rS.A00(r0)
            boolean r0 = r0 instanceof X.C184813q
            if (r0 == 0) goto L_0x0054
            java.lang.String r0 = r1.name()
            androidx.fragment.app.Fragment r1 = r2.A0Q(r0)
            com.google.common.base.Preconditions.checkNotNull(r1)
            com.facebook.orca.threadlist.ThreadListFragment r1 = (com.facebook.orca.threadlist.ThreadListFragment) r1
            r2 = 0
            boolean r0 = r1.A1a()
            if (r0 == 0) goto L_0x0054
            X.1Ri r0 = r1.A0T
            if (r0 == 0) goto L_0x0054
            int r1 = r0.AZq()
            r0 = 1
            if (r1 <= r2) goto L_0x0055
        L_0x0054:
            r0 = 0
        L_0x0055:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13100qf.ARW():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        if (r1 != false) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String Act() {
        /*
            r3 = this;
            X.1lX r2 = A00(r3)
            boolean r0 = X.C32301lX.A03(r2)
            if (r0 == 0) goto L_0x000f
            java.lang.String r0 = "thread"
        L_0x000c:
            if (r0 == 0) goto L_0x0044
            return r0
        L_0x000f:
            boolean r0 = X.C32301lX.A02(r2)
            if (r0 == 0) goto L_0x0018
            java.lang.String r0 = "search"
            goto L_0x000c
        L_0x0018:
            r1 = 2131300895(0x7f09121f, float:1.8219833E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r0 = r0.A0O(r1)
            if (r0 == 0) goto L_0x0028
            boolean r1 = r0.A0b
            r0 = 1
            if (r1 == 0) goto L_0x0029
        L_0x0028:
            r0 = 0
        L_0x0029:
            if (r0 == 0) goto L_0x0042
            r1 = 2131300895(0x7f09121f, float:1.8219833E38)
            X.0qW r0 = r2.A01
            androidx.fragment.app.Fragment r0 = r0.A0O(r1)
            com.google.common.base.Preconditions.checkNotNull(r0)
            X.0rS r0 = (X.C13450rS) r0
            X.0wO r0 = r0.A2S()
            java.lang.String r0 = r0.A01()
            goto L_0x000c
        L_0x0042:
            r0 = 0
            goto L_0x000c
        L_0x0044:
            java.lang.String r0 = "orca_neue_main"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13100qf.Act():java.lang.String");
    }

    public ThreadKey AjF() {
        return A00(this).A04();
    }

    public Map Ajk() {
        C32301lX A002 = A00(this);
        if (!C32301lX.A03(A002)) {
            return null;
        }
        Fragment A0O = A002.A01.A0O(2131301002);
        Preconditions.checkNotNull(A0O);
        return ((ThreadViewFragment) A0O).Ajk();
    }
}
