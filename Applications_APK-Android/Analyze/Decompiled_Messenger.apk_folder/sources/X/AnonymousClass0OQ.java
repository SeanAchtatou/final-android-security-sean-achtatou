package X;

import android.content.Context;
import android.content.Intent;
import org.json.JSONObject;

/* renamed from: X.0OQ  reason: invalid class name */
public final class AnonymousClass0OQ extends C008007h {
    private final Context A00;
    private volatile AnonymousClass07i A01 = new AnonymousClass07i(new JSONObject());

    public void A04() {
        JSONObject jSONObject = new JSONObject();
        A02(jSONObject);
        this.A01 = new AnonymousClass07i(jSONObject);
    }

    public void A05() {
        this.A00.sendBroadcast(new Intent("com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_CHANGED").setPackage(this.A00.getPackageName()));
    }

    public AnonymousClass0OQ(Context context) {
        this.A00 = context;
    }

    public AnonymousClass07i A03() {
        return this.A01;
    }
}
