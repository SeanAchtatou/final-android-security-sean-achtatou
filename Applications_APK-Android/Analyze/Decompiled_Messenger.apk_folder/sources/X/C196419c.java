package X;

/* renamed from: X.19c  reason: invalid class name and case insensitive filesystem */
public final class C196419c {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("timestamp_ms", "INTEGER");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6("id", "TEXT");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("key", "BLOB");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("master_key_version", "INTEGER");
}
