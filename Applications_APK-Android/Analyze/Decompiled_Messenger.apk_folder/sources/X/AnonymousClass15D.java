package X;

import java.util.ArrayDeque;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.15D  reason: invalid class name */
public final class AnonymousClass15D {
    private static volatile AnonymousClass15D A04;
    public final AnonymousClass0V6 A00 = new AnonymousClass15F(this);
    public final AnonymousClass0V8 A01;
    public final AnonymousClass0V4 A02;
    public final ArrayDeque A03;

    public static final AnonymousClass15D A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass15D.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass15D(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private AnonymousClass15D(AnonymousClass1XY r3) {
        new AnonymousClass0UN(0, r3);
        this.A02 = AnonymousClass0V4.A00(r3);
        this.A01 = AnonymousClass0V8.A00(r3);
        this.A03 = new ArrayDeque(3);
    }
}
