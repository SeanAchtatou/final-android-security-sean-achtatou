package X;

import android.content.Context;
import android.os.Handler;
import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.0aU  reason: invalid class name and case insensitive filesystem */
public final class C05880aU extends AnonymousClass1ZK {
    private static C04470Uu A00;

    public static final C05880aU A00(AnonymousClass1XY r6) {
        C05880aU r0;
        synchronized (C05880aU.class) {
            C04470Uu A002 = C04470Uu.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r6)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C05880aU(AnonymousClass1YA.A00(r02), AnonymousClass0UU.A00(r02), C04430Uq.A00(r02));
                }
                C04470Uu r1 = A00;
                r0 = (C05880aU) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    private C05880aU(Context context, AnonymousClass010 r3, Handler handler) {
        super(context, r3.A00(), handler);
    }
}
