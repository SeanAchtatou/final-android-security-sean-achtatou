package X;

import com.facebook.messaging.model.messages.Message;
import java.util.Comparator;

/* renamed from: X.0mC  reason: invalid class name and case insensitive filesystem */
public final class C11160mC implements Comparator {
    public int compare(Object obj, Object obj2) {
        Message message = (Message) obj;
        Message message2 = (Message) obj2;
        long j = message.A03;
        long j2 = message2.A03;
        if (j <= j2) {
            if (j >= j2) {
                String str = message.A0q;
                if (str == null && message2.A0q == null) {
                    return 0;
                }
                if (str != null) {
                    String str2 = message2.A0q;
                    if (str2 != null) {
                        return -str.compareTo(str2);
                    }
                }
            }
            return 1;
        }
        return -1;
    }
}
