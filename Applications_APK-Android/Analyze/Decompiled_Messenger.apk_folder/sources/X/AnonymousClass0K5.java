package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0K5  reason: invalid class name */
public final class AnonymousClass0K5 {
    public List A00 = new ArrayList();

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0043, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0047 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A00(java.io.File r7) {
        /*
            java.lang.String r4 = "Error reading %s"
            boolean r0 = r7.exists()
            if (r0 != 0) goto L_0x000d
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        L_0x000d:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r6 = 0
            r5 = 1
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ FileNotFoundException | IOException -> 0x0048 }
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ FileNotFoundException | IOException -> 0x0048 }
            r1.<init>(r7)     // Catch:{ FileNotFoundException | IOException -> 0x0048 }
            r0 = 64
            r2.<init>(r1, r0)     // Catch:{ FileNotFoundException | IOException -> 0x0048 }
        L_0x0020:
            java.lang.String r1 = r2.readLine()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x003d
            boolean r0 = r1.isEmpty()     // Catch:{ all -> 0x0041 }
            if (r0 != 0) goto L_0x0020
            java.lang.String r0 = " "
            java.lang.String[] r1 = r1.split(r0)     // Catch:{ all -> 0x0041 }
            r0 = r1[r6]     // Catch:{ all -> 0x0041 }
            r3.add(r0)     // Catch:{ all -> 0x0041 }
            r0 = r1[r5]     // Catch:{ all -> 0x0041 }
            r3.add(r0)     // Catch:{ all -> 0x0041 }
            goto L_0x0020
        L_0x003d:
            r2.close()     // Catch:{ FileNotFoundException | IOException -> 0x0048 }
            return r3
        L_0x0041:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0047 }
        L_0x0047:
            throw r0     // Catch:{ FileNotFoundException | IOException -> 0x0048 }
        L_0x0048:
            r2 = move-exception
            java.lang.String r1 = "DeviceStateServiceReport"
            java.lang.String r0 = r7.getName()
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            X.C010708t.A0U(r1, r2, r4, r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0K5.A00(java.io.File):java.util.List");
    }
}
