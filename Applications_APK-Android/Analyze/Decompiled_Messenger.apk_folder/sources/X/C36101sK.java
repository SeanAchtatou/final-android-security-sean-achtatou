package X;

import android.content.Context;
import android.os.Looper;

/* renamed from: X.1sK  reason: invalid class name and case insensitive filesystem */
public final class C36101sK implements C04310Tq, AnonymousClass062 {
    public Looper A00;

    public /* bridge */ /* synthetic */ Object get() {
        return this.A00;
    }

    public C36101sK(Context context) {
        this.A00 = C29321gE.A01(AnonymousClass1XX.get(context));
    }
}
