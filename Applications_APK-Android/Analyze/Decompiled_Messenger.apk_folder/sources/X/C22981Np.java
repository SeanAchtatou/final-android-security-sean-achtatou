package X;

import io.card.payment.BuildConfig;
import java.io.PrintWriter;
import java.io.StringWriter;

/* renamed from: X.1Np  reason: invalid class name and case insensitive filesystem */
public final class C22981Np implements C22991Nq {
    public final /* synthetic */ C30641iP A00;

    public C22981Np(C30641iP r1) {
        this.A00 = r1;
    }

    public void C2m(AnonymousClass1SA r6, Throwable th) {
        String stringWriter;
        this.A00.CJG(r6, th);
        Integer valueOf = Integer.valueOf(System.identityHashCode(this));
        Integer valueOf2 = Integer.valueOf(System.identityHashCode(r6));
        String name = r6.A01().getClass().getName();
        if (th == null) {
            stringWriter = BuildConfig.FLAVOR;
        } else {
            StringWriter stringWriter2 = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter2));
            stringWriter = stringWriter2.toString();
        }
        AnonymousClass02w.A0E("Fresco", "Finalized without closing: %x %x (type = %s).\nStack:\n%s", valueOf, valueOf2, name, stringWriter);
    }

    public boolean C3V() {
        return this.A00.BGo();
    }
}
