package X;

import com.google.common.base.Preconditions;
import java.util.Collection;
import java.util.Set;

/* renamed from: X.1Fh  reason: invalid class name and case insensitive filesystem */
public final class C21121Fh<K, V> extends C21101Ff<K, Collection<V>> {
    public final AnonymousClass0V0 A00;

    public void clear() {
        this.A00.clear();
    }

    public boolean containsKey(Object obj) {
        return this.A00.containsKey(obj);
    }

    public boolean isEmpty() {
        return this.A00.isEmpty();
    }

    public Set keySet() {
        return this.A00.keySet();
    }

    public int size() {
        return this.A00.keySet().size();
    }

    public C21121Fh(AnonymousClass0V0 r1) {
        Preconditions.checkNotNull(r1);
        this.A00 = r1;
    }

    public Object get(Object obj) {
        if (containsKey(obj)) {
            return this.A00.AbK(obj);
        }
        return null;
    }

    public Object remove(Object obj) {
        if (containsKey(obj)) {
            return this.A00.C1N(obj);
        }
        return null;
    }
}
