package X;

import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0W0  reason: invalid class name */
public final class AnonymousClass0W0 extends AnonymousClass0W1 {
    private static volatile AnonymousClass0W0 A01;
    public AnonymousClass0UN A00;

    private AnonymousClass0W0(AnonymousClass1XY r4) {
        super("preferences", 2, ImmutableList.of(new AnonymousClass0W3()));
        this.A00 = new AnonymousClass0UN(2, r4);
    }

    public static final AnonymousClass0W0 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0W0.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0W0(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:24|(2:26|27)|28|29) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x01ab */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(android.database.sqlite.SQLiteDatabase r24, int r25, int r26) {
        /*
            r23 = this;
            r0 = r25
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r2 = r26
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            r1 = 2
            if (r0 >= r1) goto L_0x02ee
            if (r1 > r2) goto L_0x02ee
            java.lang.String r5 = "#maybeMigrate"
            r0 = 1944328856(0x73e41a98, float:3.6144503E31)
            X.C005505z.A03(r5, r0)
            r0 = -635208200(0xffffffffda237df8, float:-1.15047313E16)
            r15 = r24
            X.C007406x.A01(r15, r0)     // Catch:{ all -> 0x02e0 }
            java.util.TreeMap r0 = new java.util.TreeMap     // Catch:{ all -> 0x02d8 }
            r0.<init>()     // Catch:{ all -> 0x02d8 }
            java.lang.String[] r17 = X.C07470dc.A00     // Catch:{ all -> 0x02d8 }
            java.lang.String r16 = "preferences"
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            android.database.Cursor r5 = r15.query(r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02d8 }
            X.C07480dd.A01(r5, r0)     // Catch:{ all -> 0x02d1 }
            if (r5 == 0) goto L_0x0040
            r5.close()     // Catch:{ all -> 0x02d8 }
        L_0x0040:
            int r7 = X.C188628pH.A00(r0)     // Catch:{ all -> 0x02d8 }
            r6 = 6
            r5 = 0
            if (r7 == r6) goto L_0x0049
            r5 = 1
        L_0x0049:
            if (r5 == 0) goto L_0x02c7
            java.lang.String r9 = "preferences"
            int r6 = X.AnonymousClass1Y3.BQp     // Catch:{ all -> 0x02d8 }
            r10 = r23
            X.0UN r5 = r10.A00     // Catch:{ all -> 0x02d8 }
            r8 = 0
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r8, r6, r5)     // Catch:{ all -> 0x02d8 }
            X.8pH r13 = (X.C188628pH) r13     // Catch:{ all -> 0x02d8 }
            com.google.common.collect.ImmutableMap$Builder r7 = com.google.common.collect.ImmutableMap.builder()     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = X.C188628pH.A01     // Catch:{ all -> 0x02d8 }
            java.lang.String r5 = "/app_info"
            r7.put(r5, r6)     // Catch:{ all -> 0x02d8 }
            java.lang.String r5 = "//gk"
            r7.put(r5, r6)     // Catch:{ all -> 0x02d8 }
            com.google.common.collect.ImmutableMap r7 = r7.build()     // Catch:{ all -> 0x02d8 }
            com.google.common.collect.ImmutableMap$Builder r11 = com.google.common.collect.ImmutableMap.builder()     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/auth/"
            java.lang.String r5 = "/auth/user_data/"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/auth_machine_id"
            java.lang.String r5 = "/auth/auth_machine_id"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/me_user_version"
            java.lang.String r5 = "/auth/me_user_version"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/app_info"
            java.lang.String r5 = "/config/app_info"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/gk/"
            java.lang.String r5 = "/config/gk/"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/gk_version"
            java.lang.String r5 = "/config/gk/version"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/rollout/"
            java.lang.String r5 = "/config/rollout"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/rollout_version"
            java.lang.String r5 = "/config/rollout/version"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r12 = "/shared/device_id"
            java.lang.String r5 = "/orca/device_id/"
            r11.put(r5, r12)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/c2dm/"
            java.lang.String r5 = "/messenger/c2dm/"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/first_install_time"
            java.lang.String r5 = "/messenger/first_install_time"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/nux_completed"
            java.lang.String r5 = "/messenger/nux_completed"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/login_reminder_trigger_state"
            java.lang.String r5 = "/messenger/login_reminder_trigger_state"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/phone_confirm"
            java.lang.String r5 = "/messenger/phone_confirm"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/sms"
            java.lang.String r5 = "/messages/sms"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/ui_counters"
            java.lang.String r5 = "/messages/ui_counters"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/notifications/recent_threads"
            java.lang.String r5 = "/messages/notifications/recent_threads"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/preferences/notifications/location_services"
            java.lang.String r5 = "/settings/messages/location_services"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/preferences/notifications"
            java.lang.String r5 = "/settings/messages/notifications"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/preferences/threads"
            java.lang.String r5 = "/settings/messages/threads"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/internal/debug_logs"
            java.lang.String r5 = "/settings/logging/debug_logs"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/internal/logging_level"
            java.lang.String r5 = "/settings/logging/logging_level"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/internal/php_profiling"
            java.lang.String r5 = "/settings/http/php_profiling"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/internal/wirehog_profiling"
            java.lang.String r5 = "/settings/http/wirehog_profiling"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/internal/force_fb4a_look_and_feel"
            java.lang.String r5 = "/settings/messenger/force_fb4a_look_and_feel"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/internal/web"
            java.lang.String r5 = "/settings/sandbox/web"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/internal/mqtt"
            java.lang.String r5 = "/settings/sandbox/mqtt"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r5 = "/orca/device_id"
            r11.put(r5, r12)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = "/orca/mqtt"
            java.lang.String r5 = "/mqtt"
            r11.put(r6, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r6 = X.C188628pH.A01     // Catch:{ all -> 0x02d8 }
            java.lang.String r5 = "/orca/top_last_active_sync_time"
            r11.put(r5, r6)     // Catch:{ all -> 0x02d8 }
            com.google.common.collect.ImmutableMap r5 = r11.build()     // Catch:{ all -> 0x02d8 }
            java.util.TreeMap r11 = new java.util.TreeMap     // Catch:{ all -> 0x02d8 }
            r11.<init>(r0)     // Catch:{ all -> 0x02d8 }
            int r14 = X.C188628pH.A00(r11)     // Catch:{ all -> 0x02d8 }
            X.1Y7 r12 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/orca/pref_version"
            r12.<init>(r0)     // Catch:{ all -> 0x02d8 }
            X.1Y7 r6 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/_meta_/prefs_version"
            r6.<init>(r0)     // Catch:{ all -> 0x02d8 }
            r0 = 1
            if (r14 != 0) goto L_0x0165
            java.util.SortedMap r11 = X.C188628pH.A02(r11, r7)     // Catch:{ all -> 0x02d8 }
            r14 = 1
        L_0x0165:
            if (r14 != r0) goto L_0x0176
            r11.remove(r12)     // Catch:{ all -> 0x02d8 }
            java.util.SortedMap r11 = X.C188628pH.A02(r11, r5)     // Catch:{ all -> 0x02d8 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x02d8 }
            r11.put(r6, r0)     // Catch:{ all -> 0x02d8 }
            r14 = 2
        L_0x0176:
            r7 = 3
            if (r14 != r1) goto L_0x01b3
            X.1Y7 r5 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/auth/user_data/fb_me_user"
            r5.<init>(r0)     // Catch:{ all -> 0x02d8 }
            X.1Y7 r12 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/auth/user_data/fb_uid"
            r12.<init>(r0)     // Catch:{ all -> 0x02d8 }
            java.lang.Object r5 = r11.get(r5)     // Catch:{ all -> 0x02d8 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x02d8 }
            if (r5 == 0) goto L_0x01ab
            int r0 = X.AnonymousClass1Y3.AmL     // Catch:{ IOException -> 0x01ab }
            X.0UN r13 = r13.A00     // Catch:{ IOException -> 0x01ab }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r0, r13)     // Catch:{ IOException -> 0x01ab }
            X.0jJ r0 = (X.AnonymousClass0jJ) r0     // Catch:{ IOException -> 0x01ab }
            com.fasterxml.jackson.databind.JsonNode r5 = r0.readTree(r5)     // Catch:{ IOException -> 0x01ab }
            java.lang.String r0 = "uid"
            com.fasterxml.jackson.databind.JsonNode r5 = r5.get(r0)     // Catch:{ IOException -> 0x01ab }
            r0 = 0
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0Q(r5, r0)     // Catch:{ IOException -> 0x01ab }
            r11.put(r12, r0)     // Catch:{ IOException -> 0x01ab }
        L_0x01ab:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x02d8 }
            r11.put(r6, r0)     // Catch:{ all -> 0x02d8 }
            r14 = 3
        L_0x01b3:
            r5 = 4
            if (r14 != r7) goto L_0x01fe
            X.1Y7 r12 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/fb_android/bookmarks/newsfeed_filter_type_key"
            r12.<init>(r0)     // Catch:{ all -> 0x02d8 }
            com.google.common.collect.ImmutableMap$Builder r13 = com.google.common.collect.ImmutableMap.builder()     // Catch:{ all -> 0x02d8 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "most_recent"
            r13.put(r7, r0)     // Catch:{ all -> 0x02d8 }
            r0 = 1
            java.lang.Integer r7 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "top_stories"
            r13.put(r7, r0)     // Catch:{ all -> 0x02d8 }
            com.google.common.collect.ImmutableMap r13 = r13.build()     // Catch:{ all -> 0x02d8 }
            java.lang.Object r7 = r11.remove(r12)     // Catch:{ all -> 0x02d8 }
            boolean r0 = r7 instanceof java.lang.String     // Catch:{ all -> 0x02d8 }
            if (r0 == 0) goto L_0x01eb
            r11.put(r12, r7)     // Catch:{ all -> 0x02d8 }
        L_0x01e3:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x02d8 }
            r11.put(r6, r0)     // Catch:{ all -> 0x02d8 }
            goto L_0x01fd
        L_0x01eb:
            boolean r0 = r7 instanceof java.lang.Integer     // Catch:{ all -> 0x02d8 }
            if (r0 == 0) goto L_0x01e3
            java.lang.Integer r7 = (java.lang.Integer) r7     // Catch:{ all -> 0x02d8 }
            java.lang.Object r0 = r13.get(r7)     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x02d8 }
            if (r0 == 0) goto L_0x01e3
            r11.put(r12, r0)     // Catch:{ all -> 0x02d8 }
            goto L_0x01e3
        L_0x01fd:
            r14 = 4
        L_0x01fe:
            r12 = 5
            if (r14 != r5) goto L_0x0222
            X.1Y7 r5 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/fb_android/notifications/polling_interval"
            r5.<init>(r0)     // Catch:{ all -> 0x02d8 }
            X.1Y7 r7 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/notifications/polling_interval"
            r7.<init>(r0)     // Catch:{ all -> 0x02d8 }
            java.lang.Object r5 = r11.remove(r5)     // Catch:{ all -> 0x02d8 }
            boolean r0 = r5 instanceof java.lang.String     // Catch:{ all -> 0x02d8 }
            if (r0 == 0) goto L_0x021a
            r11.put(r7, r5)     // Catch:{ all -> 0x02d8 }
        L_0x021a:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x02d8 }
            r11.put(r6, r0)     // Catch:{ all -> 0x02d8 }
            r14 = 5
        L_0x0222:
            if (r14 != r12) goto L_0x0251
            X.1Y7 r5 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/fb_android/uvm/sync"
            r5.<init>(r0)     // Catch:{ all -> 0x02d8 }
            X.1Y7 r7 = new X.1Y7     // Catch:{ all -> 0x02d8 }
            java.lang.String r0 = "/contactsync/nux_shown"
            r7.<init>(r0)     // Catch:{ all -> 0x02d8 }
            java.lang.Object r5 = r11.remove(r5)     // Catch:{ all -> 0x02d8 }
            if (r5 == 0) goto L_0x0249
            boolean r0 = r5 instanceof java.lang.String     // Catch:{ all -> 0x02d8 }
            if (r0 == 0) goto L_0x0249
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x02d8 }
            boolean r0 = java.lang.Boolean.parseBoolean(r5)     // Catch:{ all -> 0x02d8 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x02d8 }
            r11.put(r7, r0)     // Catch:{ all -> 0x02d8 }
        L_0x0249:
            r0 = 6
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x02d8 }
            r11.put(r6, r0)     // Catch:{ all -> 0x02d8 }
        L_0x0251:
            int r6 = X.C188628pH.A00(r11)     // Catch:{ all -> 0x02d8 }
            r5 = 6
            if (r5 != r6) goto L_0x02b3
            java.lang.String r5 = "#migrate"
            r0 = 328736049(0x13981d31, float:3.839898E-27)
            X.C005505z.A03(r5, r0)     // Catch:{ all -> 0x02d8 }
            r7 = 0
            r15.delete(r9, r7, r7)     // Catch:{ all -> 0x02ab }
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ all -> 0x02ab }
            r6.<init>()     // Catch:{ all -> 0x02ab }
            java.util.Set r0 = r11.entrySet()     // Catch:{ all -> 0x02ab }
            java.util.Iterator r14 = r0.iterator()     // Catch:{ all -> 0x02ab }
        L_0x0271:
            boolean r0 = r14.hasNext()     // Catch:{ all -> 0x02ab }
            if (r0 == 0) goto L_0x02c1
            java.lang.Object r0 = r14.next()     // Catch:{ all -> 0x02ab }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x02ab }
            java.lang.Object r12 = r0.getKey()     // Catch:{ all -> 0x02ab }
            X.1Y7 r12 = (X.AnonymousClass1Y7) r12     // Catch:{ all -> 0x02ab }
            java.lang.Object r11 = r0.getValue()     // Catch:{ all -> 0x02ab }
            r13 = 1
            int r5 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x02ab }
            X.0UN r0 = r10.A00     // Catch:{ all -> 0x02ab }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r13, r5, r0)     // Catch:{ all -> 0x02ab }
            X.1YI r5 = (X.AnonymousClass1YI) r5     // Catch:{ all -> 0x02ab }
            r0 = 324(0x144, float:4.54E-43)
            boolean r0 = r5.AbO(r0, r8)     // Catch:{ all -> 0x02ab }
            X.C07480dd.A00(r6, r12, r11, r0)     // Catch:{ all -> 0x02ab }
            r0 = -324312999(0xffffffffecab6059, float:-1.6574504E27)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x02ab }
            r15.replaceOrThrow(r9, r7, r6)     // Catch:{ all -> 0x02ab }
            r0 = -702140069(0xffffffffd626315b, float:-4.5682728E13)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x02ab }
            goto L_0x0271
        L_0x02ab:
            r1 = move-exception
            r0 = -1499020106(0xffffffffa6a6c4b6, float:-1.1571882E-15)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x02d8 }
            throw r1     // Catch:{ all -> 0x02d8 }
        L_0x02b3:
            X.5ke r2 = new X.5ke     // Catch:{ all -> 0x02d8 }
            java.lang.String r1 = "expected = "
            java.lang.String r0 = " , actual = "
            java.lang.String r0 = X.AnonymousClass08S.A0B(r1, r5, r0, r6)     // Catch:{ all -> 0x02d8 }
            r2.<init>(r0)     // Catch:{ all -> 0x02d8 }
            throw r2     // Catch:{ all -> 0x02d8 }
        L_0x02c1:
            r0 = -776397348(0xffffffffd1b91ddc, float:-9.9383738E10)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x02d8 }
        L_0x02c7:
            r15.setTransactionSuccessful()     // Catch:{ all -> 0x02d8 }
            r0 = 1185085535(0x46a2f85f, float:20860.186)
            X.C007406x.A02(r15, r0)     // Catch:{ all -> 0x02e0 }
            goto L_0x02e8
        L_0x02d1:
            r0 = move-exception
            if (r5 == 0) goto L_0x02d7
            r5.close()     // Catch:{ all -> 0x02d8 }
        L_0x02d7:
            throw r0     // Catch:{ all -> 0x02d8 }
        L_0x02d8:
            r1 = move-exception
            r0 = 1050182733(0x3e98844d, float:0.29788437)
            X.C007406x.A02(r15, r0)     // Catch:{ all -> 0x02e0 }
            throw r1     // Catch:{ all -> 0x02e0 }
        L_0x02e0:
            r1 = move-exception
            r0 = 653018496(0x26ec4580, float:1.6394628E-15)
            X.C005505z.A00(r0)
            throw r1
        L_0x02e8:
            r0 = 593414778(0x235eca7a, float:1.207752E-17)
            X.C005505z.A00(r0)
        L_0x02ee:
            if (r1 < r2) goto L_0x02f1
            return
        L_0x02f1:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.Object[] r1 = new java.lang.Object[]{r3, r4}
            java.lang.String r0 = "You are upgrading to %d from %d and do not have update code. Write some damn upgrade code!!!1!"
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0W0.A08(android.database.sqlite.SQLiteDatabase, int, int):void");
    }
}
