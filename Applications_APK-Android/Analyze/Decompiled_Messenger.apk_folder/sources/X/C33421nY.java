package X;

import java.util.Collections;

/* renamed from: X.1nY  reason: invalid class name and case insensitive filesystem */
public final class C33421nY {
    public AnonymousClass0UN A00;

    private static int A00(long j) {
        long j2 = j / 1000;
        if (j2 > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) j2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        if (r3 != 1) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass102 A01(com.facebook.graphservice.interfaces.Summary r5) {
        /*
            if (r5 == 0) goto L_0x0021
            java.lang.String r4 = r5.source
            r3 = -1
            int r2 = r4.hashCode()
            r0 = 94416770(0x5a0af82, float:1.5110799E-35)
            r1 = 1
            if (r2 == r0) goto L_0x0024
            r0 = 1561714200(0x5d15de18, float:6.7494346E17)
            if (r2 != r0) goto L_0x001d
            java.lang.String r0 = "consistency"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x001d
            r3 = 1
        L_0x001d:
            if (r3 == 0) goto L_0x002e
            if (r3 == r1) goto L_0x0035
        L_0x0021:
            X.102 r0 = X.AnonymousClass102.FROM_SERVER
            return r0
        L_0x0024:
            java.lang.String r0 = "cache"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x001d
            r3 = 0
            goto L_0x001d
        L_0x002e:
            boolean r0 = r5.isNetworkComplete
            if (r0 != 0) goto L_0x0035
            X.102 r0 = X.AnonymousClass102.FROM_CACHE_STALE
            return r0
        L_0x0035:
            X.102 r0 = X.AnonymousClass102.FROM_CACHE_UP_TO_DATE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33421nY.A01(com.facebook.graphservice.interfaces.Summary):X.102");
    }

    public static final C33421nY A02(AnonymousClass1XY r1) {
        return new C33421nY(r1);
    }

    public C33431nZ A03(C07440dX r6, boolean z, int i) {
        C33431nZ r2 = new C33431nZ();
        r2.locale = ((AnonymousClass0ZS) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A9d, this.A00)).A03();
        boolean z2 = true;
        if (z) {
            z2 = false;
        }
        r2.terminateAfterFreshResponse = z2;
        r2.excludedCacheKeyParameters = r6.A08;
        r2.additionalHttpHeaders = r6.A0A;
        r2.networkTimeoutSeconds = -1;
        r2.parseOnClientExecutor = false;
        r2.markHttpRequestReplaySafe = false;
        r2.onlyCacheInitialNetworkResponse = r6.A06;
        r2.enableExperimentalGraphStoreCache = r6.A04;
        r2.enableOfflineCaching = r6.A05;
        r2.analyticTags = (String[]) Collections.unmodifiableList(r6.A03).toArray(new String[0]);
        r2.requestPurpose = i;
        r2.doNotResumeLiveQuery = false;
        r2.sendCacheAgeForAdaptiveFetch = r6.A07;
        r2.adaptiveFetchClientParams = r6.A01;
        int i2 = C33441na.A00[r6.A02.ordinal()];
        if (i2 == 1) {
            int A002 = A00(r6.A00);
            r2.cacheTtlSeconds = A002;
            r2.freshCacheTtlSeconds = A002;
            r2.networkTimeoutSeconds = 0;
            return r2;
        } else if (i2 == 2) {
            int A003 = A00(r6.A00);
            r2.cacheTtlSeconds = A003;
            r2.freshCacheTtlSeconds = A003;
            return r2;
        } else if (i2 == 3) {
            r2.cacheTtlSeconds = 0;
            r2.freshCacheTtlSeconds = 0;
            return r2;
        } else if (i2 != 4 && i2 != 5) {
            return r2;
        } else {
            r2.cacheTtlSeconds = A00(r6.A00);
            r2.freshCacheTtlSeconds = A00(0);
            r2.ensureCacheWrite = true;
            return r2;
        }
    }

    private C33421nY(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
