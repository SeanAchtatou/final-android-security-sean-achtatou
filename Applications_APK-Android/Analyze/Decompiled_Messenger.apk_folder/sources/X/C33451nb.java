package X;

import android.content.Context;
import com.facebook.acra.LogCatCollector;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.io.InputStream;
import org.json.JSONObject;

/* renamed from: X.1nb  reason: invalid class name and case insensitive filesystem */
public final class C33451nb {
    private final Context A00;
    private final AnonymousClass09P A01;

    public static final C33451nb A00(AnonymousClass1XY r1) {
        return new C33451nb(r1);
    }

    public Long A01(String str, String str2, String str3) {
        return Long.valueOf(new JSONObject(A02(str, str3)).getLong(str2));
    }

    public String A02(String str, String str2) {
        try {
            InputStream open = this.A00.getAssets().open(str);
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            return new String(bArr, LogCatCollector.UTF_8_ENCODING);
        } catch (IOException e) {
            this.A01.CGZ(str2, AnonymousClass08S.A0J("IOException reading file: ", str), e);
            return BuildConfig.FLAVOR;
        }
    }

    public C33451nb(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1YA.A00(r2);
        this.A01 = C04750Wa.A01(r2);
    }
}
