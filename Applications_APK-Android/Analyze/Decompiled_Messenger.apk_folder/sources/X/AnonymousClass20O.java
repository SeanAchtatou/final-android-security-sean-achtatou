package X;

import com.facebook.auth.userscope.UserScoped;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

@UserScoped
/* renamed from: X.20O  reason: invalid class name */
public final class AnonymousClass20O implements C12390pG {
    private static C05540Zi A04;
    private C44642Iz A00 = new C44642Iz(null, AnonymousClass07B.A0C, null, false);
    public final AnonymousClass06A A01;
    public final AnonymousClass4Ob A02 = new AnonymousClass4Ob();
    private final AnonymousClass4Ob A03 = new AnonymousClass4Ob();

    public synchronized void A01(C44642Iz r2) {
        this.A00 = r2;
    }

    public synchronized Map Ajk() {
        ImmutableMap.Builder builder;
        builder = ImmutableMap.builder();
        builder.put("latestInboxFromPush", this.A02.toString());
        builder.put("latestInboxFromFetch", this.A03.toString());
        builder.put("latestMarkFolderSeenResult", this.A00.toString());
        return builder.build();
    }

    public static final AnonymousClass20O A00(AnonymousClass1XY r4) {
        AnonymousClass20O r0;
        synchronized (AnonymousClass20O.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new AnonymousClass20O((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (AnonymousClass20O) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass20O(AnonymousClass1XY r5) {
        this.A01 = AnonymousClass067.A0A(r5);
    }
}
