package X;

/* renamed from: X.16p  reason: invalid class name and case insensitive filesystem */
public final class C190516p {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("added_time_ms", "INTEGER");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6("add_source", "TEXT");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("aloha_proxy_user_owners", "TEXT");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("bday_day", "INTEGER");
    public static final AnonymousClass0W6 A04 = new AnonymousClass0W6("bday_month", "INTEGER");
    public static final AnonymousClass0W6 A05 = new AnonymousClass0W6("big_picture_size", "INTEGER");
    public static final AnonymousClass0W6 A06 = new AnonymousClass0W6("big_picture_url", "TEXT");
    public static final AnonymousClass0W6 A07 = new AnonymousClass0W6("communication_rank", "REAL");
    public static final AnonymousClass0W6 A08 = new AnonymousClass0W6("contact_id", "TEXT UNIQUE");
    public static final AnonymousClass0W6 A09 = new AnonymousClass0W6("data", "TEXT");
    public static final AnonymousClass0W6 A0A = new AnonymousClass0W6("display_name", "TEXT");
    public static final AnonymousClass0W6 A0B = new AnonymousClass0W6("favorite_color", "TEXT");
    public static final AnonymousClass0W6 A0C = new AnonymousClass0W6("fbid", "TEXT");
    public static final AnonymousClass0W6 A0D = new AnonymousClass0W6("first_name", "TEXT");
    public static final AnonymousClass0W6 A0E = new AnonymousClass0W6("huge_picture_size", "INTEGER");
    public static final AnonymousClass0W6 A0F = new AnonymousClass0W6("huge_picture_url", "TEXT");
    public static final AnonymousClass0W6 A0G = new AnonymousClass0W6("internal_id", "INTEGER PRIMARY KEY AUTOINCREMENT");
    public static final AnonymousClass0W6 A0H = new AnonymousClass0W6("is_aloha_proxy_confirmed", "INTEGER");
    public static final AnonymousClass0W6 A0I = new AnonymousClass0W6("is_broadcast_recipient_holdout", "INTEGER");
    public static final AnonymousClass0W6 A0J = new AnonymousClass0W6("is_indexed", "INTEGER");
    public static final AnonymousClass0W6 A0K = new AnonymousClass0W6("is_managing_parent_approved_user", "INTEGER");
    public static final AnonymousClass0W6 A0L = new AnonymousClass0W6("is_memorialized", "INTEGER");
    public static final AnonymousClass0W6 A0M = new AnonymousClass0W6("is_message_ignored_by_viewer", "INTEGER");
    public static final AnonymousClass0W6 A0N = new AnonymousClass0W6("is_messenger_user", "TEXT");
    public static final AnonymousClass0W6 A0O = new AnonymousClass0W6("is_mobile_pushable", "INTEGER");
    public static final AnonymousClass0W6 A0P = new AnonymousClass0W6("is_on_viewer_contact_list", "TEXT");
    public static final AnonymousClass0W6 A0Q = new AnonymousClass0W6("is_partial", "INTEGER");
    public static final AnonymousClass0W6 A0R = new AnonymousClass0W6("is_viewer_managing_parent", "INTEGER");
    public static final AnonymousClass0W6 A0S = new AnonymousClass0W6("last_fetch_time_ms", "INTEGER");
    public static final AnonymousClass0W6 A0T = new AnonymousClass0W6("last_name", "TEXT");
    public static final AnonymousClass0W6 A0U = new AnonymousClass0W6("link_type", "TEXT");
    public static final AnonymousClass0W6 A0V = new AnonymousClass0W6("messenger_install_time_ms", "INTEGER");
    public static final AnonymousClass0W6 A0W = new AnonymousClass0W6("messenger_invite_priority", "REAL");
    public static final AnonymousClass0W6 A0X = new AnonymousClass0W6("phonebook_section_key", "TEXT");
    public static final AnonymousClass0W6 A0Y = new AnonymousClass0W6("small_picture_size", "INTEGER");
    public static final AnonymousClass0W6 A0Z = new AnonymousClass0W6("small_picture_url", "TEXT");
    public static final AnonymousClass0W6 A0a = new AnonymousClass0W6("type", "TEXT");
    public static final AnonymousClass0W6 A0b = new AnonymousClass0W6("viewer_connection_status", "TEXT");
    public static final AnonymousClass0W6 A0c = new AnonymousClass0W6("work_info", "TEXT");
}
