package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1re  reason: invalid class name and case insensitive filesystem */
public final class C35701re extends Enum {
    private static final /* synthetic */ C35701re[] A00;
    public static final C35701re A01;
    public static final C35701re A02;

    static {
        C35701re r5 = new C35701re("NONE", 0);
        C35701re r4 = new C35701re("CORE", 1);
        A01 = r4;
        C35701re r3 = new C35701re("CORE_AND_SAMPLED", 2);
        A02 = r3;
        A00 = new C35701re[]{r5, r4, r3, new C35701re("UNSET", 3)};
    }

    public static C35701re valueOf(String str) {
        return (C35701re) Enum.valueOf(C35701re.class, str);
    }

    public static C35701re[] values() {
        return (C35701re[]) A00.clone();
    }

    private C35701re(String str, int i) {
    }
}
