package X;

/* renamed from: X.1wg  reason: invalid class name and case insensitive filesystem */
public enum C37971wg implements C36201sg {
    UNKNOWN_TEXT_ACTION_TYPE(0),
    AT_HOME(1),
    AT_WORK(2),
    HAPPY_BIRTHDAY(3),
    HAPPY_FATHERS_DAY(4),
    SMART_REPLIES(5);
    
    private final int value;

    private C37971wg(int i) {
        this.value = i;
    }

    public int getValue() {
        return this.value;
    }
}
