package X;

import android.util.Log;
import com.facebook.acra.BlackBoxRecorderControl;
import com.facebook.acra.constants.ErrorReportingConstants;
import java.util.Map;

/* renamed from: X.051  reason: invalid class name */
public final class AnonymousClass051 implements BlackBoxRecorderControl {
    public static AnonymousClass051 A00;

    public Object captureBlackBoxTrace(Map map, int i) {
        int i2;
        AnonymousClass09N A002;
        if (i != 1) {
            i2 = 0;
        } else {
            AnonymousClass050.A05(15859715);
            i2 = 15859715;
        }
        if (i2 == 0 || (A002 = AnonymousClass050.A00(i2, "crash")) == null) {
            return null;
        }
        map.put(ErrorReportingConstants.BLACK_BOX_TRACE_ID, A002.A02);
        return A002;
    }

    public void awaitForBlackBoxTraceCompletion(Object obj) {
        if (obj != null) {
            try {
                ((AnonymousClass09N) obj).A01.block(500);
            } catch (InterruptedException e) {
                Log.e("Profilo/BlackBox", "Wait for Black Box trace interrupted", e);
            }
        }
    }
}
