package X;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0cB  reason: invalid class name and case insensitive filesystem */
public final class C06850cB {
    public static void A09(StringBuilder sb, String str, C23081Od r7, Object... objArr) {
        boolean z = true;
        for (Object obj : objArr) {
            if (z) {
                z = false;
            } else {
                sb.append(str);
            }
            if (obj instanceof Collection) {
                A09(sb, str, r7, ((Collection) obj).toArray());
            } else if (obj instanceof Object[]) {
                A09(sb, str, r7, (Object[]) obj);
            } else if (r7 != null) {
                sb.append(r7.ByA(obj));
            } else {
                sb.append(obj.toString());
            }
        }
    }

    public static boolean A0D(String str, String... strArr) {
        if (strArr != null) {
            for (String A0C : strArr) {
                if (A0C(str, A0C)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int A01(String str, String str2) {
        if (str == null) {
            if (str2 == null) {
                return 0;
            }
            return -1;
        } else if (str2 == null) {
            return 1;
        } else {
            return str.compareTo(str2);
        }
    }

    public static String A04(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(str.length());
        String[] split = str.split("\\s");
        int length = split.length;
        for (int i = 0; i < length; i++) {
            String str2 = split[i];
            if (str2.length() != 0) {
                if (i > 0) {
                    sb.append(" ");
                }
                sb.append(Character.toUpperCase(str2.charAt(0)));
                sb.append(split[i].substring(1));
            }
        }
        return sb.toString();
    }

    public static String A05(String str) {
        if (str == null) {
            return BuildConfig.FLAVOR;
        }
        int length = str.length();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '&') {
                sb.append("&amp;");
            } else if (charAt < ' ' || charAt > '~') {
                sb.append("&#");
                sb.append(Integer.toString(charAt));
                sb.append(";");
            } else {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    public static String A06(String str, Object... objArr) {
        StringBuilder sb = new StringBuilder();
        A09(sb, str, null, objArr);
        return sb.toString();
    }

    public static String A07(byte[] bArr) {
        try {
            return new String(bArr, Charsets.UTF_8.name());
        } catch (UnsupportedEncodingException unused) {
            throw new RuntimeException("UTF-8 not supported");
        }
    }

    public static boolean A0B(CharSequence charSequence) {
        if (charSequence == null || charSequence.length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean A0C(String str, String str2) {
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    public static byte[] A0G(String str) {
        try {
            return str.getBytes(Charsets.UTF_8.name());
        } catch (UnsupportedEncodingException unused) {
            throw new RuntimeException("UTF-8 not supported");
        }
    }

    public static int A00(String str) {
        int i = 0;
        if (A0B(str)) {
            return 0;
        }
        int codePointCount = str.codePointCount(0, str.length());
        int i2 = 0;
        while (i < codePointCount) {
            char codePointAt = (char) str.codePointAt(i);
            if (codePointAt == 13) {
                i2++;
                int i3 = i + 1;
                if (i3 < codePointCount && ((char) str.codePointAt(i3)) == 10) {
                    i = i3;
                }
            } else if (codePointAt == 10) {
                i2++;
            }
            i++;
        }
        return i2;
    }

    public static long A02(String str) {
        int length = str.length();
        long j = 1125899906842597L;
        for (int i = 0; i < length; i++) {
            j = (j * 31) + ((long) str.charAt(i));
        }
        return j;
    }

    public static CharSequence A03(CharSequence charSequence, boolean z, boolean z2) {
        int length = charSequence.length();
        if (z2) {
            while (length > 0) {
                int i = length - 1;
                if (!Character.isWhitespace(charSequence.charAt(i))) {
                    break;
                }
                length = i;
            }
        }
        int i2 = 0;
        if (z) {
            while (length > i2 && Character.isWhitespace(charSequence.charAt(i2))) {
                i2++;
            }
        }
        if (length == charSequence.length() && i2 == 0) {
            return charSequence;
        }
        return charSequence.subSequence(i2, length);
    }

    public static List A08(String str, char c) {
        if (str.length() == 0) {
            return Collections.singletonList(BuildConfig.FLAVOR);
        }
        Splitter on = Splitter.on(c);
        Preconditions.checkNotNull(str);
        return C04300To.A03(new Iterable(str) {
            public final /* synthetic */ CharSequence val$sequence;

            {
                this.val$sequence = r2;
            }

            public Iterator iterator() {
                Splitter splitter = Splitter.this;
                return splitter.strategy.iterator(splitter, this.val$sequence);
            }

            public String toString() {
                Joiner on = Joiner.on(", ");
                StringBuilder sb = new StringBuilder("[");
                try {
                    on.appendTo(sb, iterator());
                    sb.append(']');
                    return sb.toString();
                } catch (IOException e) {
                    throw new AssertionError(e);
                }
            }
        });
    }

    public static boolean A0A(CharSequence charSequence) {
        if (!A0B(charSequence)) {
            for (int i = 0; i < charSequence.length(); i++) {
                if (!Character.isWhitespace(charSequence.charAt(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean A0E(CharSequence... charSequenceArr) {
        Preconditions.checkNotNull(charSequenceArr);
        for (CharSequence A0B : charSequenceArr) {
            if (!A0B(A0B)) {
                return false;
            }
        }
        return true;
    }

    public static boolean A0F(CharSequence... charSequenceArr) {
        Preconditions.checkNotNull(charSequenceArr);
        for (CharSequence A0B : charSequenceArr) {
            if (A0B(A0B)) {
                return true;
            }
        }
        return false;
    }
}
