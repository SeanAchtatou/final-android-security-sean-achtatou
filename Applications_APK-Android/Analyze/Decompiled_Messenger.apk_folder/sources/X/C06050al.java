package X;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0al  reason: invalid class name and case insensitive filesystem */
public final class C06050al implements C06060am {
    private final ImmutableList A00;

    public String B3Y() {
        ImmutableList immutableList = this.A00;
        if (immutableList != null && !immutableList.isEmpty()) {
            return AnonymousClass08S.A0P("PRIMARY KEY (", Joiner.on(", ").join(AnonymousClass0TH.A00(this.A00, AnonymousClass0W6.A03)), ")");
        }
        throw new UnsupportedOperationException("Columns for primary key must be specified");
    }

    public C06050al(ImmutableList immutableList) {
        this.A00 = immutableList;
    }
}
