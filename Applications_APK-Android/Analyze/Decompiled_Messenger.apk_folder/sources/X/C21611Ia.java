package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

/* renamed from: X.1Ia  reason: invalid class name and case insensitive filesystem */
public final class C21611Ia extends C17770zR {
    @Comparable(type = AnonymousClass1Y3.A01)
    public C17770zR A00;
    @Comparable(type = 13)
    public InboxUnitItem A01;
    @Comparable(type = 13)
    public AnonymousClass1BC A02;

    public C21611Ia() {
        super("InboxClickableRowWrapperComponent");
    }

    public C17770zR A16() {
        C17770zR r0;
        C21611Ia r1 = (C21611Ia) super.A16();
        C17770zR r02 = r1.A00;
        if (r02 != null) {
            r0 = r02.A16();
        } else {
            r0 = null;
        }
        r1.A00 = r0;
        return r1;
    }
}
