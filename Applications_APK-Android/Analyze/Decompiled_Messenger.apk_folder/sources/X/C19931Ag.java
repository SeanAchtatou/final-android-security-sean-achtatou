package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.widget.recyclerview.BetterRecyclerView;

/* renamed from: X.1Ag  reason: invalid class name and case insensitive filesystem */
public final class C19931Ag extends AnonymousClass1A9 {
    public final /* synthetic */ BetterRecyclerView A00;

    public void A07(RecyclerView recyclerView, int i) {
        if (i == 0) {
            int i2 = AnonymousClass1Y3.Akn;
            BetterRecyclerView betterRecyclerView = this.A00;
            ((AnonymousClass1Z4) AnonymousClass1XX.A02(0, i2, betterRecyclerView.A05)).A02(betterRecyclerView);
            return;
        }
        int i3 = AnonymousClass1Y3.Akn;
        BetterRecyclerView betterRecyclerView2 = this.A00;
        ((AnonymousClass1Z4) AnonymousClass1XX.A02(0, i3, betterRecyclerView2.A05)).A04(AnonymousClass07B.A0i, betterRecyclerView2);
    }

    public C19931Ag(BetterRecyclerView betterRecyclerView) {
        this.A00 = betterRecyclerView;
    }
}
