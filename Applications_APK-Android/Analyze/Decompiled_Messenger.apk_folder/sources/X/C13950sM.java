package X;

import android.content.Context;
import com.facebook.resources.ui.FbFrameLayout;
import com.facebook.widget.CustomLinearLayout;

/* renamed from: X.0sM  reason: invalid class name and case insensitive filesystem */
public final class C13950sM {
    public static C13520ra A00(Context context) {
        return new C13520ra(new CustomLinearLayout(context));
    }

    public static AnonymousClass16W A01(Context context) {
        return new AnonymousClass16W(new FbFrameLayout(context));
    }
}
