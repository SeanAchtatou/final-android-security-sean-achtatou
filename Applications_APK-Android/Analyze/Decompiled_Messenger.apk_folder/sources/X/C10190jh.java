package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.0jh  reason: invalid class name and case insensitive filesystem */
public final class C10190jh extends AnonymousClass0Wl {
    public final FbSharedPreferences A00;

    public String getSimpleName() {
        return "OrcaFirstInstallInitializer";
    }

    public static final C10190jh A00(AnonymousClass1XY r2) {
        return new C10190jh(FbSharedPreferencesModule.A00(r2));
    }

    private C10190jh(FbSharedPreferences fbSharedPreferences) {
        this.A00 = fbSharedPreferences;
    }

    public void init() {
        boolean z;
        int A03 = C000700l.A03(1560826610);
        if (this.A00.At2(C05690aA.A0W, -1) == -1) {
            C30281hn edit = this.A00.edit();
            edit.BzA(C05690aA.A0W, System.currentTimeMillis());
            edit.commit();
            z = true;
        } else {
            z = false;
        }
        if (z) {
            C30281hn edit2 = this.A00.edit();
            edit2.putBoolean(C05690aA.A19, false);
            edit2.commit();
        }
        C000700l.A09(-1061820872, A03);
    }
}
