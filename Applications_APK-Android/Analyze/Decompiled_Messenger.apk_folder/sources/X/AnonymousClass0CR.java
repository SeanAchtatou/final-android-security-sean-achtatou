package X;

/* renamed from: X.0CR  reason: invalid class name */
public final class AnonymousClass0CR {
    public final C01540Aq A00;
    public final C01540Aq A01;
    public final C01540Aq A02;
    public final C01540Aq A03;
    public final C01540Aq A04;
    public final boolean A05;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0CR(X.C01830Bu r8, X.AnonymousClass0C5 r9) {
        /*
            r7 = this;
            X.0Bc r2 = X.C01660Bc.A00
            r3 = r2
            r4 = r2
            X.0Bu r0 = X.C01830Bu.A00
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0021
            r5 = r2
        L_0x000d:
            X.0C5 r0 = X.AnonymousClass0C5.A01
            boolean r0 = r0.equals(r9)
            if (r0 == 0) goto L_0x001c
            r6 = r2
        L_0x0016:
            r1 = 1
            r0 = r7
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        L_0x001c:
            X.0Aq r6 = X.C01540Aq.A00(r9)
            goto L_0x0016
        L_0x0021:
            X.0Aq r5 = X.C01540Aq.A00(r8)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0CR.<init>(X.0Bu, X.0C5):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0CR(X.AnonymousClass0SB r8) {
        /*
            r7 = this;
            X.0Aq r2 = X.C01540Aq.A00(r8)
            X.0Bc r3 = X.C01660Bc.A00
            r4 = r3
            r5 = r3
            r6 = r3
            r1 = 0
            r0 = r7
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0CR.<init>(X.0SB):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0CR(X.AnonymousClass0SB r8, byte r9) {
        /*
            r7 = this;
            X.0Aq r2 = X.C01540Aq.A00(r8)
            X.0Bc r3 = X.C01660Bc.A00
            java.lang.Byte r0 = java.lang.Byte.valueOf(r9)
            X.0Aq r4 = X.C01540Aq.A00(r0)
            r5 = r3
            r6 = r3
            r1 = 0
            r0 = r7
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0CR.<init>(X.0SB, byte):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0CR(X.AnonymousClass0SB r8, java.lang.Exception r9) {
        /*
            r7 = this;
            X.0Aq r2 = X.C01540Aq.A00(r8)
            if (r9 != 0) goto L_0x0012
            X.0Bc r3 = X.C01660Bc.A00
        L_0x0008:
            X.0Bc r4 = X.C01660Bc.A00
            r5 = r4
            r6 = r4
            r1 = 0
            r0 = r7
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        L_0x0012:
            X.0Ar r3 = new X.0Ar
            r3.<init>(r9)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0CR.<init>(X.0SB, java.lang.Exception):void");
    }

    private AnonymousClass0CR(boolean z, C01540Aq r2, C01540Aq r3, C01540Aq r4, C01540Aq r5, C01540Aq r6) {
        this.A05 = z;
        this.A04 = r2;
        this.A01 = r3;
        this.A00 = r4;
        this.A02 = r5;
        this.A03 = r6;
    }
}
