package X;

import android.os.RemoteException;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.0Z8  reason: invalid class name */
public final class AnonymousClass0Z8 implements AnonymousClass0Z1 {
    private AnonymousClass0UN A00;

    public String Amj() {
        return TurboLoader.Locator.$const$string(166);
    }

    public static final AnonymousClass0Z8 A00(AnonymousClass1XY r1) {
        return new AnonymousClass0Z8(r1);
    }

    public String getCustomData(Throwable th) {
        String str;
        if (th != null) {
            return null;
        }
        C36991uR BvK = ((C29281gA) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BAB, this.A00)).BvK();
        try {
            synchronized (BvK) {
                try {
                    str = C36991uR.A00(BvK).Av6();
                } catch (RemoteException e) {
                    str = e.toString();
                }
            }
            BvK.A06();
            return str;
        } catch (Throwable th2) {
            try {
                return th2.toString();
            } finally {
                BvK.A06();
            }
        }
    }

    private AnonymousClass0Z8(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
