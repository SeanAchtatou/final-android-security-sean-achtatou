package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0ak  reason: invalid class name and case insensitive filesystem */
public final class C06040ak extends AnonymousClass0W4 {
    private static final C06060am A00 = new C06050al(ImmutableList.of(C06070an.A01, C06070an.A05, C06070an.A03, C06070an.A04));
    private static final ImmutableList A01;
    private static final String A02;

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    static {
        AnonymousClass0W6 r0 = C06070an.A01;
        AnonymousClass0W6 r1 = C06070an.A06;
        AnonymousClass0W6 r2 = C06070an.A02;
        A01 = ImmutableList.construct(r0, r1, r2, C06070an.A08, C06070an.A00, C06070an.A07, C06070an.A05, C06070an.A03, C06070an.A04);
        A02 = AnonymousClass0W4.A06("units", "units_position_index", ImmutableList.of(r2.A00));
    }

    public C06040ak() {
        super("units", A01, A00);
    }

    public void A09(SQLiteDatabase sQLiteDatabase) {
        super.A09(sQLiteDatabase);
        String str = A02;
        C007406x.A00(-2056448604);
        sQLiteDatabase.execSQL(str);
        C007406x.A00(-1696873919);
    }
}
