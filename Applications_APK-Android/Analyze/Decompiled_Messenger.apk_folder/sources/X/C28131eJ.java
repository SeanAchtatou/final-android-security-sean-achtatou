package X;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;

/* renamed from: X.1eJ  reason: invalid class name and case insensitive filesystem */
public abstract class C28131eJ {
    public boolean A06(Intent intent, Context context) {
        if (!(this instanceof AnonymousClass15w)) {
            throw new UnsupportedOperationException();
        }
        AnonymousClass15w r2 = (AnonymousClass15w) this;
        Intent A0D = r2.A01.A0D(intent, context, r2.A00);
        r2.A00 = null;
        if (A0D == null) {
            return false;
        }
        return context.stopService(A0D);
    }

    public ComponentName A07(Intent intent, Context context) {
        if (!(this instanceof AnonymousClass15w)) {
            throw new UnsupportedOperationException();
        }
        AnonymousClass15w r2 = (AnonymousClass15w) this;
        Intent A0D = r2.A01.A0D(intent, context, r2.A00);
        r2.A00 = null;
        if (A0D != null) {
            return context.startService(A0D);
        }
        return null;
    }

    public ComponentName A08(Intent intent, Context context) {
        throw new UnsupportedOperationException();
    }

    public boolean A09(Intent intent, int i, Activity activity) {
        Intent A01;
        if (!(this instanceof AnonymousClass15w)) {
            throw new UnsupportedOperationException();
        }
        AnonymousClass15w r3 = (AnonymousClass15w) this;
        Intent A0B = r3.A01.A0B(intent, activity, r3.A00);
        r3.A00 = null;
        if (A0B == null || (A01 = AnonymousClass15w.A01(r3, A0B, activity)) == null) {
            return false;
        }
        activity.startActivityForResult(A01, i);
        return true;
    }

    public boolean A0A(Intent intent, int i, Fragment fragment) {
        Intent A01;
        if (!(this instanceof AnonymousClass15w)) {
            throw new UnsupportedOperationException();
        }
        AnonymousClass15w r3 = (AnonymousClass15w) this;
        Intent A0B = r3.A01.A0B(intent, fragment.A1j(), r3.A00);
        r3.A00 = null;
        if (A0B == null || (A01 = AnonymousClass15w.A01(r3, A0B, fragment.A1j())) == null) {
            return false;
        }
        fragment.startActivityForResult(A01, i);
        return true;
    }

    public boolean A0B(Intent intent, Context context) {
        Intent A01;
        if (!(this instanceof AnonymousClass15w)) {
            throw new UnsupportedOperationException();
        }
        AnonymousClass15w r3 = (AnonymousClass15w) this;
        Intent A0B = r3.A01.A0B(intent, context, r3.A00);
        r3.A00 = null;
        if (A0B == null || (A01 = AnonymousClass15w.A01(r3, A0B, context)) == null) {
            return false;
        }
        context.startActivity(A01);
        return true;
    }
}
