package X;

import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.204  reason: invalid class name */
public final class AnonymousClass204 {
    public static final ImmutableList A00 = ImmutableList.of(IOException.class, CancellationException.class, TimeoutException.class);
    private static volatile AnonymousClass204 A01;

    public static final AnonymousClass204 A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass204.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new AnonymousClass204();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
