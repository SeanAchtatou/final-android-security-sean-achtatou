package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.1mV  reason: invalid class name and case insensitive filesystem */
public final class C32891mV implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass1CV A00;

    public C32891mV(AnonymousClass1CV r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(-191942088);
        if (this.A00.A09 != C10700ki.ALL) {
            AnonymousClass09Y.A01(-763977952, A002);
            return;
        }
        AnonymousClass1CV.A05(this.A00, intent.getIntExtra("EXTRA_BADGE_COUNT", 0));
        AnonymousClass09Y.A01(351815402, A002);
    }
}
