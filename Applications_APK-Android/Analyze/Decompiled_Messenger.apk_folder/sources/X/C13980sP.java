package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.Set;

@UserScoped
/* renamed from: X.0sP  reason: invalid class name and case insensitive filesystem */
public final class C13980sP {
    private static C05540Zi A04;
    public boolean A00;
    public final AnonymousClass0ZM A01 = new C14060sY(this);
    public final FbSharedPreferences A02;
    public final Set A03 = C008907u.A00();

    public synchronized void A01(C13030qT r4) {
        AnonymousClass064.A00(r4);
        if (!this.A00) {
            this.A02.C0f(C14090sb.A00, this.A01);
            this.A00 = true;
        }
        this.A03.add(r4);
    }

    public synchronized void A02(C13030qT r4) {
        AnonymousClass064.A00(r4);
        this.A03.remove(r4);
        if (this.A03.isEmpty() && this.A00) {
            this.A02.CJr(C14090sb.A00, this.A01);
            this.A00 = false;
        }
    }

    public static final C13980sP A00(AnonymousClass1XY r4) {
        C13980sP r0;
        synchronized (C13980sP.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new C13980sP((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (C13980sP) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    private C13980sP(AnonymousClass1XY r2) {
        this.A02 = FbSharedPreferencesModule.A00(r2);
    }
}
