package X;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1AB  reason: invalid class name */
public final class AnonymousClass1AB implements C15590vX, AnonymousClass1AK {
    public final AnonymousClass1Ri A00;
    public final boolean A01;

    public boolean ARR() {
        return this.A00.ARR();
    }

    public void AS5(AnonymousClass20H r3) {
        this.A00.A00 = r3.A00;
    }

    public void AWp(int i) {
        if (this.A01) {
            AnonymousClass1Ri r2 = this.A00;
            AnonymousClass1Ri.A0G(r2);
            if (AnonymousClass1AJ.A00) {
                r2.hashCode();
            }
            C30409Evk evk = new C30409Evk(i);
            synchronized (r2) {
                r2.A17 = true;
                r2.A0g.remove(i);
                AnonymousClass1Ri.A0N(r2, evk);
            }
            return;
        }
        this.A00.A0V(i);
    }

    public void AWu(int i, int i2) {
        if (this.A01) {
            AnonymousClass1Ri r3 = this.A00;
            AnonymousClass1Ri.A0G(r3);
            AnonymousClass1Ri.A0L(r3, i2);
            if (AnonymousClass1AJ.A00) {
                r3.hashCode();
            }
            C30408Evj evj = new C30408Evj(i, i2);
            synchronized (r3) {
                r3.A17 = true;
                for (int i3 = 0; i3 < i2; i3++) {
                    r3.A0g.remove(i);
                }
                AnonymousClass1Ri.A0N(r3, evj);
            }
            return;
        }
        this.A00.A0Y(i, i2);
    }

    public void AXC() {
        this.A00.AXC();
    }

    public void BD9(int i, C21681Ih r6) {
        if (this.A01) {
            AnonymousClass1Ri r3 = this.A00;
            AnonymousClass1Ri.A0G(r3);
            AnonymousClass1Ri.A0F(r3);
            if (AnonymousClass1AJ.A00) {
                r3.hashCode();
                r6.getName();
            }
            AnonymousClass1Ri.A0P(r6);
            C30411Evm A05 = AnonymousClass1Ri.A05(r3, i, r6);
            synchronized (r3) {
                r3.A17 = true;
                r3.A0g.add(i, A05.A01);
                AnonymousClass1Ri.A0M(r3, A05);
            }
            return;
        }
        this.A00.A0Z(i, r6);
    }

    public void BDC(int i, int i2, List list) {
        if (this.A01) {
            AnonymousClass1Ri r5 = this.A00;
            AnonymousClass1Ri.A0G(r5);
            AnonymousClass1Ri.A0F(r5);
            if (AnonymousClass1AJ.A00) {
                String[] strArr = new String[list.size()];
                for (int i3 = 0; i3 < list.size(); i3++) {
                    strArr[i3] = ((C21681Ih) list.get(i3)).getName();
                }
                r5.hashCode();
                list.size();
                Arrays.toString(strArr);
            }
            synchronized (r5) {
                r5.A17 = true;
                int size = list.size();
                for (int i4 = 0; i4 < size; i4++) {
                    C21681Ih r0 = (C21681Ih) list.get(i4);
                    AnonymousClass1Ri.A0P(r0);
                    int i5 = i + i4;
                    C30411Evm A05 = AnonymousClass1Ri.A05(r5, i5, r0);
                    r5.A0g.add(i5, A05.A01);
                    AnonymousClass1Ri.A0M(r5, A05);
                }
            }
            return;
        }
        this.A00.A0b(i, list);
    }

    public boolean BHW() {
        return this.A00.BHW();
    }

    public void BKw(AnonymousClass10L r2, int i, int i2, AnonymousClass10N r5) {
        this.A00.BKw(r2, i, i2, r5);
    }

    public /* bridge */ /* synthetic */ void BLE(ViewGroup viewGroup) {
        this.A00.BLE((RecyclerView) viewGroup);
    }

    public void BLF(int i, int i2) {
        if (this.A01) {
            AnonymousClass1Ri r3 = this.A00;
            AnonymousClass1Ri.A0G(r3);
            if (AnonymousClass1AJ.A00) {
                r3.hashCode();
            }
            C30410Evl evl = new C30410Evl(i, i2);
            synchronized (r3) {
                r3.A17 = true;
                List list = r3.A0g;
                list.add(i2, list.remove(i));
                AnonymousClass1Ri.A0N(r3, evl);
            }
            return;
        }
        this.A00.A0X(i, i2);
    }

    public void BM3(boolean z, C20841Ea r7) {
        if (this.A01) {
            AnonymousClass1Ri r2 = this.A00;
            boolean A02 = C27041cY.A02();
            if (A02) {
                C27041cY.A01("notifyChangeSetCompleteAsync");
            }
            try {
                if (AnonymousClass1AJ.A00) {
                    r2.hashCode();
                }
                r2.A17 = true;
                AnonymousClass1Ri.A0G(r2);
                synchronized (r2) {
                    if (r2.A0C == null) {
                        r2.A0C = new AnonymousClass6QB(r2.A00);
                    }
                    AnonymousClass6QB r1 = r2.A0C;
                    r1.A02 = z;
                    r1.A01 = r7;
                    r2.A0e.addLast(r1);
                    r2.A0i.set(true);
                    r2.A0C = null;
                }
                if (C191216w.A00()) {
                    r2.A0U();
                    if (z) {
                        if (C35301r0.A01(r2.A0A)) {
                            r2.A0G = r2.A0A.A00;
                        }
                        AnonymousClass1Ri.A0J(r2);
                    }
                } else if (r2.A0j.get()) {
                    C70293aP.A02.A00(r2.A0V);
                }
                if (AnonymousClass07c.isDebugModeEnabled || AnonymousClass07c.isEndToEndTestRun) {
                    r2.A0I.set(-1);
                }
                if (A02) {
                    C27041cY.A00();
                }
            } catch (Throwable th) {
                if (A02) {
                    C27041cY.A00();
                }
                throw th;
            }
        } else {
            this.A00.A0f(z, r7);
        }
    }

    public void C3C(int i, int i2) {
        this.A00.A0W(i, i2);
    }

    public void CBs(int i, int i2) {
        this.A00.CBs(i, i2);
    }

    public boolean CIZ() {
        return this.A01;
    }

    public /* bridge */ /* synthetic */ void CJk(ViewGroup viewGroup) {
        this.A00.CJk((RecyclerView) viewGroup);
    }

    public void CK6(int i, C21681Ih r4) {
        if (this.A01) {
            AnonymousClass1Ri r1 = this.A00;
            AnonymousClass1Ri.A0G(r1);
            if (AnonymousClass1AJ.A00) {
                r1.hashCode();
            }
            synchronized (r1) {
                AnonymousClass1Ri.A0N(r1, new C30407Evi(i, r4));
            }
            return;
        }
        this.A00.A0a(i, r4);
    }

    public void CKm(int i, int i2, List list) {
        if (this.A01) {
            AnonymousClass1Ri r1 = this.A00;
            AnonymousClass1Ri.A0G(r1);
            if (AnonymousClass1AJ.A00) {
                r1.hashCode();
                list.size();
            }
            synchronized (r1) {
                AnonymousClass1Ri.A0N(r1, new C39381yx(i, list));
            }
            return;
        }
        this.A00.A0c(i, list);
    }

    public AnonymousClass1AB(AnonymousClass1Ri r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    public void APv(ViewGroup viewGroup) {
    }

    public void CJZ(ViewGroup viewGroup) {
    }
}
