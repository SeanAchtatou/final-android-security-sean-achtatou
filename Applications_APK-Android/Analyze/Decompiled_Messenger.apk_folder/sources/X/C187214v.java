package X;

import android.text.TextUtils;
import android.text.format.DateUtils;
import com.facebook.appupdate.ReleaseInfo;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.base.Platform;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;

/* renamed from: X.14v  reason: invalid class name and case insensitive filesystem */
public final class C187214v {
    private static final Class A03 = C187214v.class;
    private AnonymousClass0UN A00;
    private final AnonymousClass06B A01 = AnonymousClass067.A02();
    private final String A02;

    public static ReleaseInfo A00(C187214v r3, boolean z) {
        String B4F = ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, r3.A00)).B4F(C31821kR.A0P, null);
        if (Platform.stringIsNullOrEmpty(B4F)) {
            A02(z, "ReleaseInfo is null or empty, skipping");
            return null;
        }
        try {
            return new ReleaseInfo(B4F);
        } catch (JSONException unused) {
            A02(z, "Fail to parse ReleaseInfo, skipping");
            return null;
        }
    }

    public static final C187214v A01(AnonymousClass1XY r1) {
        return new C187214v(r1);
    }

    private static void A02(boolean z, String str) {
        if (z) {
            C010708t.A05(A03, str);
            throw new C194969Ds(str);
        }
    }

    public static boolean A03(C187214v r9, ReleaseInfo releaseInfo, boolean z) {
        String A0T;
        if (releaseInfo.expirationTimestampInSec <= 0 || r9.A01.now() < TimeUnit.SECONDS.toMillis(releaseInfo.expirationTimestampInSec)) {
            if (r9.A02.equals(releaseInfo.packageName)) {
                C09400hF r5 = (C09400hF) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BMV, r9.A00);
                int A012 = r5.A01();
                int i = releaseInfo.versionCode;
                if (i <= A012) {
                    A0T = AnonymousClass08S.A0C("ReleaseInfo has outdated version code (", i, " <= ", A012, "), skipping");
                } else if (!TextUtils.isEmpty(releaseInfo.versionName)) {
                    String A022 = r5.A02();
                    if (((C180868aK) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AVo, r9.A00)).compare(releaseInfo.versionName, A022) <= 0) {
                        A0T = AnonymousClass08S.A0T("ReleaseInfo has outdated version name (", releaseInfo.versionName, " <= ", A022, "), skipping");
                    }
                }
            }
            return true;
        }
        A0T = "ReleaseInfo has expired (" + ((Object) DateUtils.getRelativeTimeSpanString(TimeUnit.SECONDS.toMillis(releaseInfo.expirationTimestampInSec), r9.A01.now(), 0)) + "), skipping";
        A02(z, A0T);
        return false;
    }

    private C187214v(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A02 = C04490Ux.A0t(r3);
    }

    public boolean A04(boolean z) {
        ReleaseInfo A002 = A00(this, z);
        if (A002 == null) {
            return false;
        }
        if (((C194819Da) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AfZ, this.A00)).A03(A002)) {
            return true;
        }
        A02(z, "Insufficient space to upgrade, skipping");
        return false;
    }
}
