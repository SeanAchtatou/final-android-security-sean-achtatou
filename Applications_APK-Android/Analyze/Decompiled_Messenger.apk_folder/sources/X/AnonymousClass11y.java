package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.11y  reason: invalid class name */
public final class AnonymousClass11y {
    private static volatile AnonymousClass11y A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass11y A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass11y.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass11y(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass11y(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
