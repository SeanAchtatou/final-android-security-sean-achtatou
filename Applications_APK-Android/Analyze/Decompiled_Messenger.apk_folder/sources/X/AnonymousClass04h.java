package X;

import android.content.Context;
import android.util.SparseArray;
import com.facebook.acra.ErrorReporter;
import com.facebook.profilo.core.TriggerRegistry;
import com.facebook.profilo.logger.api.ProfiloLogger;
import com.facebook.profilo.provider.device_info.DeviceInfoProvider;
import java.util.Arrays;

/* renamed from: X.04h  reason: invalid class name */
public final class AnonymousClass04h {
    public static void A00(Context context) {
        A01(context, null, null);
    }

    private static void A01(Context context, AnonymousClass09H r10, AnonymousClass09G r11) {
        AnonymousClass051 r0;
        int i;
        AnonymousClass05i AiX;
        AnonymousClass056 r02;
        AnonymousClass09G r4 = r11;
        SparseArray sparseArray = new SparseArray(5);
        sparseArray.put(AnonymousClass04i.A00, AnonymousClass04i.A01);
        sparseArray.put(C003404k.A01, new C003404k());
        sparseArray.put(C003604m.A01, new C003604m());
        sparseArray.put(C003704n.A01, new C003704n());
        sparseArray.put(AnonymousClass04o.A00, new AnonymousClass04o());
        Context context2 = context;
        C000900o[] A00 = AnonymousClass05A.A00(context);
        C000900o[] r7 = (C000900o[]) Arrays.copyOf(A00, A00.length + 4);
        int length = r7.length;
        r7[length - 4] = new DeviceInfoProvider(context);
        r7[length - 3] = new AnonymousClass04q(context);
        r7[length - 2] = AnonymousClass04r.A01;
        r7[length - 1] = AnonymousClass04s.A06;
        if (r11 == null) {
            r4 = new AnonymousClass09G(context);
        }
        if (r10 == null) {
            r10 = new AnonymousClass04u();
        }
        AnonymousClass05e.A00(context2, r4, "main", true, r7, sparseArray, new AnonymousClass09H[]{r10}, null);
        ProfiloLogger.sHasProfilo = true;
        C004004z.A00 = true;
        AnonymousClass050.A00 = true;
        ErrorReporter instance = ErrorReporter.getInstance();
        synchronized (AnonymousClass051.class) {
            if (AnonymousClass051.A00 == null) {
                AnonymousClass051.A00 = new AnonymousClass051();
            }
            r0 = AnonymousClass051.A00;
        }
        instance.setBlackBoxRecorderControl(r0);
        AnonymousClass09L.A01();
        C001000r.A00(new AnonymousClass053());
        AnonymousClass054 r5 = AnonymousClass054.A07;
        if (r5 != null) {
            int i2 = C003604m.A01;
            AnonymousClass05d r1 = r4.A00;
            if (r1 == null || (AiX = r1.AiX()) == null || (r02 = (AnonymousClass056) AiX.Ai1(TriggerRegistry.A00.A01("cold_start"))) == null) {
                i = 0;
            } else {
                i = r02.A05;
            }
            r5.A0A(i2, 0, null, (long) i);
        }
    }
}
