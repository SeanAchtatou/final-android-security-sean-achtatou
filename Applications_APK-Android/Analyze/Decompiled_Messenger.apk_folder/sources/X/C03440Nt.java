package X;

import android.content.Context;
import android.graphics.drawable.Drawable;

/* renamed from: X.0Nt  reason: invalid class name and case insensitive filesystem */
public final class C03440Nt {
    public static Drawable A00(Context context, int i, int i2, int i3) {
        Context A05 = AnonymousClass0HQ.A05(context, 2130968608, i);
        return C14960uQ.A00(AnonymousClass01R.A03(A05, i2), AnonymousClass0HQ.A01(A05, 2130968946, i3), AnonymousClass0HQ.A00(A05, 16842803, 0.5f));
    }
}
