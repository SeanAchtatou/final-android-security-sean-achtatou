package X;

import java.util.List;

/* renamed from: X.1Rd  reason: invalid class name and case insensitive filesystem */
public final class C23841Rd extends AnonymousClass1QN {
    public int A00 = 0;
    public AnonymousClass1QO A01 = null;
    public AnonymousClass1QO A02 = null;
    public final /* synthetic */ AnonymousClass36x A03;

    public static synchronized AnonymousClass1QO A00(C23841Rd r1) {
        AnonymousClass1QO r0;
        synchronized (r1) {
            r0 = r1.A02;
        }
        return r0;
    }

    public static void A01(C23841Rd r1, AnonymousClass1QO r2) {
        boolean z;
        synchronized (r1) {
            if (r1.isClosed() || r2 != r1.A01) {
                z = false;
            } else {
                r1.A01 = null;
                z = true;
            }
        }
        if (z) {
            if (!(r2 == A00(r1) || r2 == null)) {
                r2.AT6();
            }
            if (!r1.A02()) {
                r1.A09(r2.AmS());
            }
        }
    }

    private boolean A02() {
        C23111Og r0;
        boolean z;
        synchronized (this) {
            if (isClosed() || this.A00 >= this.A03.A00.size()) {
                r0 = null;
            } else {
                List list = this.A03.A00;
                int i = this.A00;
                this.A00 = i + 1;
                r0 = (C23111Og) list.get(i);
            }
        }
        AnonymousClass1QO r2 = null;
        if (r0 != null) {
            r2 = (AnonymousClass1QO) r0.get();
        }
        synchronized (this) {
            if (isClosed()) {
                z = false;
            } else {
                this.A01 = r2;
                z = true;
            }
        }
        if (z && r2 != null) {
            r2.CIK(new AnonymousClass374(this), C50292dj.A00);
            return true;
        } else if (r2 == null) {
            return false;
        } else {
            r2.AT6();
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r0.AT6();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
        r2.AT6();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        if (r0 == null) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AT6() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = super.AT6()     // Catch:{ all -> 0x0020 }
            if (r0 != 0) goto L_0x000a
            r0 = 0
            monitor-exit(r3)     // Catch:{ all -> 0x0020 }
            return r0
        L_0x000a:
            X.1QO r2 = r3.A01     // Catch:{ all -> 0x0020 }
            r1 = 0
            r3.A01 = r1     // Catch:{ all -> 0x0020 }
            X.1QO r0 = r3.A02     // Catch:{ all -> 0x0020 }
            r3.A02 = r1     // Catch:{ all -> 0x0020 }
            monitor-exit(r3)     // Catch:{ all -> 0x0020 }
            if (r0 == 0) goto L_0x0019
            r0.AT6()
        L_0x0019:
            if (r2 == 0) goto L_0x001e
            r2.AT6()
        L_0x001e:
            r0 = 1
            return r0
        L_0x0020:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0020 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C23841Rd.AT6():boolean");
    }

    public synchronized Object B1I() {
        Object obj;
        AnonymousClass1QO A002 = A00(this);
        if (A002 != null) {
            obj = A002.B1I();
        } else {
            obj = null;
        }
        return obj;
    }

    public C23841Rd(AnonymousClass36x r3) {
        this.A03 = r3;
        if (!A02()) {
            A09(new RuntimeException("No data source supplier or supplier returned null."));
        }
    }
}
