package X;

import com.google.common.base.Preconditions;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: X.0cl  reason: invalid class name and case insensitive filesystem */
public abstract class C07170cl extends C07180co {
    public final ByteBuffer A00 = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);

    public void A0A(byte b) {
        C25431Zp r2 = (C25431Zp) this;
        Preconditions.checkState(!r2.A00, "Cannot re-use a Hasher after calling hash() on it");
        r2.A02.update(b);
    }

    public static void A00(C07170cl r2, int i) {
        try {
            r2.A0C(r2.A00.array(), 0, i);
        } finally {
            r2.A00.clear();
        }
    }

    public void A0B(byte[] bArr) {
        if (!(this instanceof C25431Zp)) {
            A0C(bArr, 0, bArr.length);
            return;
        }
        C25431Zp r2 = (C25431Zp) this;
        Preconditions.checkState(!r2.A00, "Cannot re-use a Hasher after calling hash() on it");
        r2.A02.update(bArr);
    }

    public void A0C(byte[] bArr, int i, int i2) {
        if (!(this instanceof C25431Zp)) {
            for (int i3 = i; i3 < i + i2; i3++) {
                A0A(bArr[i3]);
            }
            return;
        }
        C25431Zp r2 = (C25431Zp) this;
        Preconditions.checkState(!r2.A00, "Cannot re-use a Hasher after calling hash() on it");
        r2.A02.update(bArr, i, i2);
    }
}
