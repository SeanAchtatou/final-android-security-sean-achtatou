package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0TV  reason: invalid class name */
public final class AnonymousClass0TV<E> extends HashSet<E> {
    public static AnonymousClass0TV A00(Object... objArr) {
        HashSet hashSet = new HashSet(objArr.length);
        Collections.addAll(hashSet, objArr);
        return new AnonymousClass0TV(hashSet);
    }

    public AnonymousClass0TV(Set set) {
        super(set);
    }
}
