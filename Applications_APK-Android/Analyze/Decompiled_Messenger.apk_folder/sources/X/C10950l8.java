package X;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0l8  reason: invalid class name and case insensitive filesystem */
public final class C10950l8 extends Enum {
    public static final ImmutableSet A00;
    public static final ImmutableSet A01 = ImmutableSet.A05(A05, A06);
    private static final ImmutableMap A02;
    private static final /* synthetic */ C10950l8[] A03;
    public static final C10950l8 A04;
    public static final C10950l8 A05;
    public static final C10950l8 A06;
    public static final C10950l8 A07;
    public static final C10950l8 A08;
    public static final C10950l8 A09;
    public static final C10950l8 A0A;
    public static final C10950l8 A0B;
    public static final C10950l8 A0C;
    public static final C10950l8 A0D;
    public static final C10950l8 A0E;
    public final String dbName;

    static {
        C10950l8 r13 = new C10950l8("NONE", 0, "none");
        A07 = r13;
        C10950l8 r14 = new C10950l8("INBOX", 1, "inbox");
        A05 = r14;
        C10950l8 r11 = new C10950l8("PENDING", 2, "pending");
        A0A = r11;
        C10950l8 r10 = new C10950l8("OTHER", 3, "other");
        A08 = r10;
        C10950l8 r12 = new C10950l8("ARCHIVED", 4, "archived");
        A04 = r12;
        C10950l8 r9 = new C10950l8("SPAM", 5, "spam");
        A0D = r9;
        C10950l8 r8 = new C10950l8("MONTAGE", 6, "montage");
        A06 = r8;
        C10950l8 r7 = new C10950l8("PAGE_FOLLOWUP", 7, "page_followup");
        A09 = r7;
        C10950l8 r6 = new C10950l8("SMS_BUSINESS", 8, "sms_business");
        A0C = r6;
        C10950l8 r4 = new C10950l8("UNREAD", 9, "unread");
        A0E = r4;
        C10950l8 r3 = new C10950l8("PINNED", 10, "pinned");
        A0B = r3;
        C10950l8 r20 = r7;
        C10950l8 r19 = r8;
        C10950l8 r18 = r9;
        C10950l8 r17 = r12;
        A03 = new C10950l8[]{r13, r14, r11, r10, r17, r18, r19, r20, r6, r4, r3};
        A00 = ImmutableSet.A05(r11, r10);
        ImmutableMap.Builder builder = ImmutableMap.builder();
        for (C10950l8 r1 : values()) {
            builder.put(r1.dbName, r1);
        }
        A02 = builder.build();
    }

    public static C10950l8 A00(String str) {
        C10950l8 r0 = (C10950l8) A02.get(str);
        if (r0 != null) {
            return r0;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0J("Invalid name ", str));
    }

    public static C10950l8 valueOf(String str) {
        return (C10950l8) Enum.valueOf(C10950l8.class, str);
    }

    public static C10950l8[] values() {
        return (C10950l8[]) A03.clone();
    }

    public boolean A01() {
        if (this == A0A || this == A08) {
            return true;
        }
        return false;
    }

    public String toString() {
        return this.dbName;
    }

    private C10950l8(String str, int i, String str2) {
        this.dbName = str2;
    }
}
