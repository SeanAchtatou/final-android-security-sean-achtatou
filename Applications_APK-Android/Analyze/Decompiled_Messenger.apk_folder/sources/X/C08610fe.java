package X;

import com.facebook.quicklog.PerformanceLoggingEvent;

/* renamed from: X.0fe  reason: invalid class name and case insensitive filesystem */
public final class C08610fe {
    public final /* synthetic */ C08520fU A00;

    public C08610fe(C08520fU r1) {
        this.A00 = r1;
    }

    public void A00(int i, PerformanceLoggingEvent performanceLoggingEvent) {
        C08520fU.A07(this.A00, "markerEnd", i);
        if (performanceLoggingEvent != null) {
            C08520fU r2 = this.A00;
            r2.A09.A00(new C14000sR(r2, performanceLoggingEvent));
        }
    }
}
