package X;

import com.facebook.litho.ComponentTree;

/* renamed from: X.16M  reason: invalid class name */
public final class AnonymousClass16M extends AnonymousClass16J {
    public static final String __redex_internal_original_name = "com.facebook.litho.ComponentTree$UpdateStateSyncRunnable";
    public final String A00;
    public final boolean A01;
    public final /* synthetic */ ComponentTree A02;

    public AnonymousClass16M(ComponentTree componentTree, String str, boolean z) {
        this.A02 = componentTree;
        this.A00 = str;
        this.A01 = z;
    }
}
