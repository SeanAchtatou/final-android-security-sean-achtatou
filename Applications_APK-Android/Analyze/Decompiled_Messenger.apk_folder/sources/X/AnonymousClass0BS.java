package X;

import android.content.BroadcastReceiver;

/* renamed from: X.0BS  reason: invalid class name */
public final class AnonymousClass0BS extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0BO A00;

    public AnonymousClass0BS(AnonymousClass0BO r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006e, code lost:
        r10.A00.A0L.run();
        X.C000700l.A0D(r12, 721822788, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007b, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r11, android.content.Intent r12) {
        /*
            r10 = this;
            r0 = 1654585752(0x629ef998, float:1.4662853E21)
            int r4 = X.C000700l.A01(r0)
            java.lang.String r1 = r12.getAction()
            X.0BO r0 = r10.A00
            java.lang.String r0 = r0.A0F
            boolean r0 = X.AnonymousClass0A2.A00(r1, r0)
            if (r0 != 0) goto L_0x001c
            r0 = -345939171(0xffffffffeb61631d, float:-2.7247636E26)
            X.C000700l.A0D(r12, r0, r4)
            return
        L_0x001c:
            X.0BO r7 = r10.A00
            monitor-enter(r7)
            r12.getAction()     // Catch:{ all -> 0x007c }
            android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x007c }
            X.0BO r9 = r10.A00     // Catch:{ all -> 0x007c }
            long r5 = r9.A00     // Catch:{ all -> 0x007c }
            r1 = 900000(0xdbba0, double:4.44659E-318)
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0038
            monitor-exit(r7)     // Catch:{ all -> 0x007c }
            r0 = 1454019520(0x56aa93c0, float:9.3775779E13)
            X.C000700l.A0D(r12, r0, r4)
            return
        L_0x0038:
            long r2 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x007c }
            long r0 = r9.A01     // Catch:{ all -> 0x007c }
            int r8 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r8 >= 0) goto L_0x004a
            monitor-exit(r7)     // Catch:{ all -> 0x007c }
            r0 = -1367302429(0xffffffffae809ee3, float:-5.84899E-11)
            X.C000700l.A0D(r12, r0, r4)
            return
        L_0x004a:
            long r2 = r2 + r5
            r9.A01 = r2     // Catch:{ all -> 0x007c }
            X.07y r2 = r9.A0E     // Catch:{ all -> 0x007c }
            android.app.AlarmManager r1 = r9.A05     // Catch:{ all -> 0x007c }
            android.app.PendingIntent r0 = r9.A08     // Catch:{ all -> 0x007c }
            r2.A05(r1, r0)     // Catch:{ all -> 0x007c }
            X.0BO r5 = r10.A00     // Catch:{ all -> 0x007c }
            boolean r0 = r5.A03     // Catch:{ all -> 0x007c }
            if (r0 == 0) goto L_0x006d
            long r2 = r5.A01     // Catch:{ all -> 0x007c }
            long r0 = r5.A00     // Catch:{ all -> 0x007c }
            X.AnonymousClass0BO.A02(r5, r2, r0)     // Catch:{ all -> 0x007c }
            X.0BO r5 = r10.A00     // Catch:{ all -> 0x007c }
            long r0 = r5.A01     // Catch:{ all -> 0x007c }
            r2 = 20000(0x4e20, double:9.8813E-320)
            long r0 = r0 + r2
            X.AnonymousClass0BO.A01(r5, r0)     // Catch:{ all -> 0x007c }
        L_0x006d:
            monitor-exit(r7)     // Catch:{ all -> 0x007c }
            X.0BO r0 = r10.A00
            java.lang.Runnable r0 = r0.A0L
            r0.run()
            r0 = 721822788(0x2b062444, float:4.765669E-13)
            X.C000700l.A0D(r12, r0, r4)
            return
        L_0x007c:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x007c }
            r0 = 1475794416(0x57f6d5f0, float:5.4279743E14)
            X.C000700l.A0D(r12, r0, r4)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BS.onReceive(android.content.Context, android.content.Intent):void");
    }
}
