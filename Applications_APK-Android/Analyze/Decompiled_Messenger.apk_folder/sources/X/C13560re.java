package X;

import com.facebook.graphservice.interfaces.GraphQLConsistency;
import com.facebook.graphservice.interfaces.GraphQLService;

/* renamed from: X.0re  reason: invalid class name and case insensitive filesystem */
public final class C13560re {
    private final C36941uM A00;
    private final C04310Tq A01;
    private final C04310Tq A02;

    public static final C13560re A00(AnonymousClass1XY r1) {
        return new C13560re(r1);
    }

    public GraphQLConsistency A01(C26941cO r3) {
        C36961uO A012 = this.A00.A01(r3);
        try {
            return (GraphQLConsistency) this.A01.get();
        } finally {
            A012.ASQ();
        }
    }

    public GraphQLService A02(C26941cO r3) {
        C36961uO A012 = this.A00.A01(r3);
        try {
            return (GraphQLService) this.A02.get();
        } finally {
            A012.ASQ();
        }
    }

    private C13560re(AnonymousClass1XY r4) {
        this.A00 = new C36941uM(AnonymousClass0XJ.A02(r4), C10580kT.A01(r4));
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.Ave, r4);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.BCs, r4);
    }
}
