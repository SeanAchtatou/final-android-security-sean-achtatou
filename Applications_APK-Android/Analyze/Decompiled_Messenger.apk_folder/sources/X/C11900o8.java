package X;

import com.google.common.base.Optional;
import java.lang.reflect.Method;
import java.util.List;

/* renamed from: X.0o8  reason: invalid class name and case insensitive filesystem */
public final class C11900o8 extends C11740ns {
    public List changeProperties(C10450k8 r5, C10120ja r6, List list) {
        Class<?> type;
        for (int i = 0; i < list.size(); i++) {
            CZZ czz = (CZZ) list.get(i);
            Class<Optional> cls = Optional.class;
            Method method = czz._accessorMethod;
            if (method != null) {
                type = method.getReturnType();
            } else {
                type = czz._field.getType();
            }
            if (cls.isAssignableFrom(type)) {
                list.set(i, new CZn(czz));
            }
        }
        return list;
    }
}
