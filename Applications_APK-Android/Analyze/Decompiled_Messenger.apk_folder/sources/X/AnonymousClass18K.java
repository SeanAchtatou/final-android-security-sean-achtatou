package X;

/* renamed from: X.18K  reason: invalid class name */
public final class AnonymousClass18K {
    public static int[] A00 = {Integer.MAX_VALUE, Integer.MAX_VALUE, 65536, 2345, 477, 193, AnonymousClass1Y3.A0p, 75, 58, 49, 43, 39, 37, 35, 34, 34, 33};
    public static final byte[] A01 = {9, 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0, 0, 0};
    public static final int[] A02 = {3, 31, 316, AnonymousClass1Y3.APy, 31622, 316227, 3162277, 31622776, 316227766, Integer.MAX_VALUE};
    public static final int[] A03 = {1, 10, 100, AnonymousClass1Y3.A87, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
        if ((r2 & r0) != false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0043, code lost:
        if (r1 > 0) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0046, code lost:
        if (r4 > 0) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0058, code lost:
        if (r4 < 0) goto L_0x005a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(int r6, int r7, java.math.RoundingMode r8) {
        /*
            com.google.common.base.Preconditions.checkNotNull(r8)
            if (r7 == 0) goto L_0x005f
            int r5 = r6 / r7
            int r0 = r7 * r5
            int r2 = r6 - r0
            if (r2 == 0) goto L_0x005e
            r6 = r6 ^ r7
            int r4 = r6 >> 31
            r3 = 1
            r4 = r4 | r3
            int[] r1 = X.AnonymousClass18J.A00
            int r0 = r8.ordinal()
            r0 = r1[r0]
            switch(r0) {
                case 1: goto L_0x0049;
                case 2: goto L_0x0056;
                case 3: goto L_0x0058;
                case 4: goto L_0x005a;
                case 5: goto L_0x0046;
                case 6: goto L_0x0023;
                case 7: goto L_0x0023;
                case 8: goto L_0x0023;
                default: goto L_0x001d;
            }
        L_0x001d:
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0023:
            int r1 = java.lang.Math.abs(r2)
            int r0 = java.lang.Math.abs(r7)
            int r0 = r0 - r1
            int r1 = r1 - r0
            if (r1 != 0) goto L_0x0043
            java.math.RoundingMode r0 = java.math.RoundingMode.HALF_UP
            if (r8 == r0) goto L_0x005a
            java.math.RoundingMode r0 = java.math.RoundingMode.HALF_EVEN
            r2 = 0
            if (r8 != r0) goto L_0x0039
            r2 = 1
        L_0x0039:
            r1 = r5 & 1
            r0 = 0
            if (r1 == 0) goto L_0x003f
            r0 = 1
        L_0x003f:
            r2 = r2 & r0
            if (r2 == 0) goto L_0x0056
            goto L_0x005a
        L_0x0043:
            if (r1 <= 0) goto L_0x0056
            goto L_0x005a
        L_0x0046:
            if (r4 <= 0) goto L_0x0056
            goto L_0x005a
        L_0x0049:
            if (r2 == 0) goto L_0x004c
            r3 = 0
        L_0x004c:
            if (r3 != 0) goto L_0x0056
            java.lang.ArithmeticException r1 = new java.lang.ArithmeticException
            java.lang.String r0 = "mode was UNNECESSARY, but rounding was necessary"
            r1.<init>(r0)
            throw r1
        L_0x0056:
            r3 = 0
            goto L_0x005a
        L_0x0058:
            if (r4 >= 0) goto L_0x0056
        L_0x005a:
            if (r3 == 0) goto L_0x005e
            int r5 = r5 + r4
            return r5
        L_0x005e:
            return r5
        L_0x005f:
            java.lang.ArithmeticException r1 = new java.lang.ArithmeticException
            java.lang.String r0 = "/ by zero"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18K.A00(int, int, java.math.RoundingMode):int");
    }
}
