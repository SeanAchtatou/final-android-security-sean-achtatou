package X;

import java.lang.ref.WeakReference;

/* renamed from: X.1rq  reason: invalid class name and case insensitive filesystem */
public final class C35801rq implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appchoreographer.BusySignalHandler$SignalTimeoutCleanupRunnable";
    private final WeakReference A00;
    public final /* synthetic */ C28461eq A01;

    public C35801rq(C28461eq r2, Object obj) {
        this.A01 = r2;
        this.A00 = new WeakReference(obj);
    }

    public void run() {
        Object obj = this.A00.get();
        if (obj != null) {
            obj.toString();
            this.A01.A08(obj);
        }
    }
}
