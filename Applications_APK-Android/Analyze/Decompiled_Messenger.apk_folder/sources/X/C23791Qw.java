package X;

import android.content.Context;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Qw  reason: invalid class name and case insensitive filesystem */
public final class C23791Qw extends C20831Dz {
    public C205139ln A00;
    public List A01 = new ArrayList();
    public final Context A02;
    public final View.OnClickListener A03 = new C205039ld(this);
    public final C204939lR A04;

    public static final C23791Qw A00(AnonymousClass1XY r3) {
        return new C23791Qw(AnonymousClass1YA.A00(r3), C204939lR.A00(r3));
    }

    public int ArU() {
        return this.A01.size();
    }

    public C23791Qw(Context context, C204939lR r3) {
        this.A02 = context;
        this.A04 = r3;
    }
}
