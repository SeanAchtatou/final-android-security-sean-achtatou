package X;

import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0Nu  reason: invalid class name and case insensitive filesystem */
public final class C03450Nu extends ClassLoader {
    public static C03450Nu A06;
    public ClassLoader A00;
    public ClassLoader A01;
    public Field A02;
    public AtomicReference A03 = new AtomicReference(new ArrayList());
    public boolean A04;
    private Method A05;

    /* JADX WARNING: Can't wrap try/catch for region: R(6:4|5|6|7|8|(1:18)(2:12|13)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x003e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A01(X.C03460Nv r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x007c }
            java.util.concurrent.atomic.AtomicReference r0 = r5.A03     // Catch:{ all -> 0x007c }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x007c }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x007c }
            int r0 = r0.size()     // Catch:{ all -> 0x007c }
            r4 = 1
            int r0 = r0 + r4
            r1.<init>(r0)     // Catch:{ all -> 0x007c }
            java.util.concurrent.atomic.AtomicReference r0 = r5.A03     // Catch:{ all -> 0x007c }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x007c }
            java.util.Collection r0 = (java.util.Collection) r0     // Catch:{ all -> 0x007c }
            r1.addAll(r0)     // Catch:{ all -> 0x007c }
            r1.add(r6)     // Catch:{ all -> 0x007c }
            java.util.concurrent.atomic.AtomicReference r0 = r5.A03     // Catch:{ all -> 0x007c }
            r0.set(r1)     // Catch:{ all -> 0x007c }
            java.util.concurrent.atomic.AtomicReference r0 = r5.A03     // Catch:{ all -> 0x007c }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x007c }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x007c }
            int r0 = r0.size()     // Catch:{ all -> 0x007c }
            if (r0 != r4) goto L_0x007a
            java.lang.Class<com.facebook.common.dextricks.classid.ClassId> r0 = com.facebook.common.dextricks.classid.ClassId.class
            java.lang.String r0 = r0.getName()     // Catch:{ ClassNotFoundException -> 0x003e }
            java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x003e }
        L_0x003e:
            java.lang.ClassLoader r0 = r5.A00     // Catch:{ all -> 0x007c }
            java.lang.String r3 = r0.toString()     // Catch:{ all -> 0x007c }
            java.lang.ClassLoader r0 = r5.A00     // Catch:{ all -> 0x007c }
            java.lang.ClassLoader r0 = r0.getParent()     // Catch:{ all -> 0x007c }
            java.lang.String r1 = r0.toString()     // Catch:{ all -> 0x007c }
            java.lang.String r0 = "java.lang.BootClassLoader"
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x007c }
            java.lang.String r2 = "PluginClassLoader"
            if (r0 == 0) goto L_0x0075
            java.lang.String r0 = "dalvik.system.PathClassLoader"
            boolean r0 = r3.startsWith(r0)     // Catch:{ all -> 0x007c }
            if (r0 == 0) goto L_0x0075
            java.lang.reflect.Field r1 = r5.A02     // Catch:{ IllegalAccessException -> 0x006f }
            java.lang.ClassLoader r0 = r5.A00     // Catch:{ IllegalAccessException -> 0x006f }
            r1.set(r0, r5)     // Catch:{ IllegalAccessException -> 0x006f }
            r5.A04 = r4     // Catch:{ IllegalAccessException -> 0x006f }
            java.lang.String r0 = "Installed PluginClassLoader"
            android.util.Log.w(r2, r0)     // Catch:{ IllegalAccessException -> 0x006f }
            goto L_0x007a
        L_0x006f:
            java.lang.String r0 = "Failed to install PluginClassLoader"
            android.util.Log.w(r2, r0)     // Catch:{ all -> 0x007c }
            goto L_0x007a
        L_0x0075:
            java.lang.String r0 = "Non-standard class loader chain. PluginClassLoader not installed"
            android.util.Log.w(r2, r0)     // Catch:{ all -> 0x007c }
        L_0x007a:
            monitor-exit(r5)
            return
        L_0x007c:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03450Nu.A01(X.0Nv):void");
    }

    public static C03450Nu A00() {
        if (A06 == null) {
            try {
                A06 = new C03450Nu(C03450Nu.class.getClassLoader());
            } catch (NoSuchFieldException | NoSuchMethodException e) {
                Log.w("PluginClassLoader", "PluginClassLoader.get: failed to create instance", e);
            }
        }
        return A06;
    }

    public Class findClass(String str) {
        Class cls;
        ArrayList arrayList = (ArrayList) this.A03.get();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ((C03460Nv) it.next()).ASM(str);
        }
        try {
            cls = (Class) this.A05.invoke(this.A00, str);
        } catch (IllegalAccessException | InvocationTargetException e) {
            Log.w("PluginClassLoader", AnonymousClass08S.A0J("PluginClassLoader failed: mChildClassLoader.findClass: ", e.getMessage()));
            e.printStackTrace();
            cls = null;
        }
        Iterator it2 = arrayList.iterator();
        if (cls != null) {
            while (it2.hasNext()) {
                ((C03460Nv) it2.next()).ASK(str, cls);
            }
            return cls;
        }
        while (it2.hasNext()) {
            ((C03460Nv) it2.next()).ASL(str);
        }
        throw new ClassNotFoundException(str);
    }

    public String toString() {
        return "PluginClassLoader[" + this.A00 + "," + this.A01 + "]";
    }

    private C03450Nu(ClassLoader classLoader) {
        super(classLoader.getParent());
        this.A00 = classLoader;
        this.A01 = classLoader.getParent();
        Class<ClassLoader> cls = ClassLoader.class;
        Method declaredMethod = cls.getDeclaredMethod("findClass", String.class);
        this.A05 = declaredMethod;
        declaredMethod.setAccessible(true);
        Field declaredField = cls.getDeclaredField("parent");
        this.A02 = declaredField;
        declaredField.setAccessible(true);
    }
}
