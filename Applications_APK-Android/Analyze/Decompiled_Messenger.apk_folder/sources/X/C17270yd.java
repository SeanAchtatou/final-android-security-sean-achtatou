package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadCustomization;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.base.Objects;
import com.google.common.base.Platform;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@UserScoped
/* renamed from: X.0yd  reason: invalid class name and case insensitive filesystem */
public final class C17270yd {
    private static C05540Zi A07;
    private AnonymousClass0UN A00;
    public final AnonymousClass0jE A01;
    public final C14300t0 A02;
    public final C04310Tq A03;
    private final AnonymousClass16Z A04;
    private final UserKey A05;
    private final Set A06 = new HashSet();

    public static ThreadParticipant A04(C17270yd r4, ThreadSummary threadSummary, C28711fF r6) {
        if (threadSummary != null) {
            if (threadSummary.A0m.size() < 1) {
                ThreadKey threadKey = threadSummary.A0S;
                C28711fF r1 = threadKey.A05;
                if ((r1 == C28711fF.ONE_TO_ONE || r1 == C28711fF.TINCAN) && !threadKey.A0Q()) {
                    ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r4.A00)).CGS("ThreadParticipantUtils.EMPTY_PARTICIPANTS", "Unable to process participants in Canonical Thread for " + threadSummary);
                }
            } else {
                ThreadKey threadKey2 = threadSummary.A0S;
                if (threadKey2.A05 == r6 || (ThreadKey.A0E(threadKey2) && threadSummary.A0m.size() == 2)) {
                    return r4.A06(threadSummary);
                }
            }
        }
        return null;
    }

    public static final C17270yd A00(AnonymousClass1XY r4) {
        C17270yd r0;
        synchronized (C17270yd.class) {
            C05540Zi A002 = C05540Zi.A00(A07);
            A07 = A002;
            try {
                if (A002.A03(r4)) {
                    A07.A00 = new C17270yd((AnonymousClass1XY) A07.A01());
                }
                C05540Zi r1 = A07;
                r0 = (C17270yd) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A07.A02();
                throw th;
            }
        }
        return r0;
    }

    public static ParticipantInfo A01(ThreadSummary threadSummary, Message message) {
        ParticipantInfo participantInfo = message.A0K;
        if (threadSummary != null) {
            if (participantInfo == null) {
                return null;
            }
            ParticipantInfo A022 = A02(threadSummary.A0m, participantInfo.A01);
            if (!(A022 == null && (A022 = A02(threadSummary.A0k, participantInfo.A01)) == null && (!participantInfo.A01.A08() || (A022 = A03(threadSummary.A0m, participantInfo.A05)) == null))) {
                return A022;
            }
        }
        return participantInfo;
    }

    private ImmutableList A05(ThreadSummary threadSummary) {
        C22141Kb r4 = new C22141Kb(threadSummary.A0m.size());
        C24971Xv it = threadSummary.A0m.iterator();
        while (it.hasNext()) {
            ParticipantInfo participantInfo = ((ThreadParticipant) it.next()).A04;
            if (!Objects.equal(participantInfo.A01, this.A05)) {
                r4.put(participantInfo.A01, participantInfo);
            }
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        C24971Xv it2 = threadSummary.A0n.iterator();
        while (it2.hasNext()) {
            ParticipantInfo participantInfo2 = (ParticipantInfo) it2.next();
            if (r4.remove(participantInfo2.A01) != null) {
                builder.add((Object) participantInfo2);
            }
        }
        builder.addAll((Iterable) r4.values());
        return builder.build();
    }

    public ThreadParticipant A06(ThreadSummary threadSummary) {
        if (this.A05 != null) {
            C24971Xv it = threadSummary.A0m.iterator();
            while (it.hasNext()) {
                ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
                if (!Objects.equal(threadParticipant.A00(), this.A05)) {
                    return threadParticipant;
                }
            }
        }
        if (threadSummary.A0m.isEmpty()) {
            return null;
        }
        return (ThreadParticipant) threadSummary.A0m.get(0);
    }

    public ImmutableList A07(ThreadSummary threadSummary) {
        C24971Xv it = threadSummary.A0m.iterator();
        while (it.hasNext()) {
            ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
            if (!Objects.equal(threadParticipant.A00(), this.A05)) {
                return ImmutableList.of(threadParticipant.A04);
            }
        }
        return RegularImmutableList.A02;
    }

    public ImmutableList A08(ThreadSummary threadSummary) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = threadSummary.A0m.iterator();
        while (it.hasNext()) {
            UserKey A002 = ((ThreadParticipant) it.next()).A00();
            if (!A002.equals((UserKey) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B7T, this.A00))) {
                builder.add((Object) A002);
            }
        }
        return builder.build();
    }

    public ImmutableList A09(ThreadSummary threadSummary) {
        List<ParticipantInfo> A052;
        ThreadKey threadKey = threadSummary.A0S;
        if (threadKey.A05 == C28711fF.ONE_TO_ONE) {
            A052 = A07(threadSummary);
        } else {
            A052 = A05(threadSummary);
        }
        if (A052.size() == 1) {
            ParticipantInfo participantInfo = (ParticipantInfo) A052.get(0);
            String A0C = A0C(threadSummary, participantInfo.A01);
            if (A0C != null || (A0C = this.A04.A01(participantInfo)) != null) {
                return ImmutableList.of(A0C);
            }
            if (!this.A06.contains(participantInfo.A01)) {
                this.A06.add(participantInfo.A01);
                C010708t.A0Q("ThreadParticipantUtils", "ThreadKey [%s], ParticipantInfo [%s]", threadKey, participantInfo);
            }
            return RegularImmutableList.A02;
        }
        ArrayList arrayList = new ArrayList();
        if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass1Hx) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AUz, this.A00)).A00)).Aem(285696929568580L)) {
            List arrayList2 = new ArrayList(A052);
            Collections.sort(arrayList2, ((C21591Hy) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AjN, this.A00)).A00);
            A052 = arrayList2;
        }
        for (ParticipantInfo participantInfo2 : A052) {
            String A0C2 = A0C(threadSummary, participantInfo2.A01);
            if (Platform.stringIsNullOrEmpty(A0C2)) {
                A0C2 = this.A04.A02(participantInfo2);
                if (Platform.stringIsNullOrEmpty(A0C2)) {
                    String str = participantInfo2.A02;
                    A0C2 = null;
                    if (!Platform.stringIsNullOrEmpty(str)) {
                        A0C2 = str;
                    }
                }
            }
            if (!Platform.stringIsNullOrEmpty(A0C2)) {
                arrayList.add(A0C2);
            }
        }
        return ImmutableList.copyOf((Collection) arrayList);
    }

    public ImmutableList A0A(ThreadSummary threadSummary) {
        if (threadSummary.A0S.A05 == C28711fF.ONE_TO_ONE) {
            return A07(threadSummary);
        }
        return A05(threadSummary);
    }

    public String A0B(ThreadCustomization threadCustomization, UserKey userKey) {
        if (!((Boolean) this.A03.get()).booleanValue()) {
            return null;
        }
        String A022 = threadCustomization.A00.A02(userKey.id, this.A01);
        if (C06850cB.A0B(A022)) {
            return null;
        }
        return A022;
    }

    public String A0C(ThreadSummary threadSummary, UserKey userKey) {
        if (threadSummary == null) {
            return null;
        }
        return A0B(threadSummary.A0e, userKey);
    }

    public boolean A0D(ThreadSummary threadSummary) {
        C24971Xv it = threadSummary.A0m.iterator();
        while (it.hasNext()) {
            if (Objects.equal(((ThreadParticipant) it.next()).A00(), this.A05)) {
                return true;
            }
        }
        return false;
    }

    public boolean A0E(ThreadSummary threadSummary) {
        C24971Xv it = threadSummary.A0m.iterator();
        while (it.hasNext()) {
            User A032 = this.A02.A03(((ThreadParticipant) it.next()).A00());
            if (A032 != null && A032.A06() != AnonymousClass07B.A00) {
                return true;
            }
        }
        return false;
    }

    private C17270yd(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A01 = C05040Xk.A00();
        this.A05 = AnonymousClass0XJ.A09(r3);
        this.A02 = C14300t0.A00(r3);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.A6h, r3);
        this.A04 = AnonymousClass16Z.A00(r3);
    }

    public static ParticipantInfo A02(List list, UserKey userKey) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
            if (Objects.equal(userKey, threadParticipant.A00())) {
                return threadParticipant.A04;
            }
        }
        return null;
    }

    public static ParticipantInfo A03(List list, String str) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
            if (Objects.equal(str, threadParticipant.A01())) {
                return threadParticipant.A04;
            }
        }
        return null;
    }
}
