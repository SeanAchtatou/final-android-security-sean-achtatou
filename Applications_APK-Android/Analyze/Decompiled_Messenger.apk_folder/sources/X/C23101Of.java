package X;

import android.content.Context;

/* renamed from: X.1Of  reason: invalid class name and case insensitive filesystem */
public final class C23101Of implements C23111Og {
    public final /* synthetic */ Context A00;

    public C23101Of(Context context) {
        this.A00 = context;
    }

    public Object get() {
        return this.A00.getCacheDir();
    }
}
