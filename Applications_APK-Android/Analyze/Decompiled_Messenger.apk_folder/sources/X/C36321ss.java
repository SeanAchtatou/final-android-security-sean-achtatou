package X;

import java.lang.reflect.Method;

/* renamed from: X.1ss  reason: invalid class name and case insensitive filesystem */
public final class C36321ss {
    public static Method A01;
    public final C36341su A00;

    static {
        try {
            A01 = Class.forName("org.slf4j.LoggerFactory").getDeclaredMethod("getLogger", String.class);
        } catch (ClassNotFoundException | NoSuchMethodException unused) {
        }
    }

    public C36321ss(C36341su r1) {
        this.A00 = r1;
    }
}
