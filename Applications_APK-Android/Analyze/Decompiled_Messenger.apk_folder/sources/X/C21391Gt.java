package X;

import android.net.Uri;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.facebook.user.profilepic.PicSquare;
import com.facebook.user.profilepic.PicSquareUrlWithSize;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Gt  reason: invalid class name and case insensitive filesystem */
public final class C21391Gt {
    public static final Class A05 = C21391Gt.class;
    private static volatile C21391Gt A06;
    private AnonymousClass0UN A00;
    public final Boolean A01 = false;
    public final C04310Tq A02;
    private final AnonymousClass09P A03;
    private final String A04;

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        if (r1 == false) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0024, code lost:
        if (r0 != false) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.net.Uri A04(X.AnonymousClass1JY r4) {
        /*
            r3 = this;
            r2 = 0
            if (r4 == 0) goto L_0x002b
            int r1 = r4.A00
            if (r1 == 0) goto L_0x0012
            java.lang.String r0 = "res:///"
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r1)
            android.net.Uri r0 = android.net.Uri.parse(r0)
            return r0
        L_0x0012:
            X.1KS r1 = r4.A05
            X.1KS r0 = X.AnonymousClass1KS.SMS_CONTACT
            if (r1 == r0) goto L_0x0026
            com.facebook.user.model.UserKey r0 = r4.A03
            if (r0 == 0) goto L_0x0023
            boolean r1 = r0.A08()
            r0 = 1
            if (r1 != 0) goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            if (r0 == 0) goto L_0x002b
        L_0x0026:
            android.net.Uri r0 = A01(r3)
            return r0
        L_0x002b:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21391Gt.A04(X.1JY):android.net.Uri");
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C21391Gt r5 = (C21391Gt) obj;
            if (!Objects.equal(this.A01, r5.A01) || !Objects.equal(this.A04, r5.A04)) {
                return false;
            }
        }
        return true;
    }

    public static Uri A01(C21391Gt r2) {
        AnonymousClass1XX.A03(AnonymousClass1Y3.A11, r2.A00);
        int i = 2132346501;
        if (C012609n.A02(((MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, r2.A00)).B9m())) {
            i = 2132346502;
        }
        return Uri.parse(AnonymousClass08S.A09("res:///", i));
    }

    public static final C21391Gt A02(AnonymousClass1XY r5) {
        if (A06 == null) {
            synchronized (C21391Gt.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A06 = new C21391Gt(applicationInjector, AnonymousClass13O.A00(applicationInjector).A02());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public int A03(AnonymousClass1JY r3) {
        if (r3.A05 != AnonymousClass1KS.SMS_CONTACT) {
            return r3.A01;
        }
        AnonymousClass1XX.A03(AnonymousClass1Y3.A11, this.A00);
        return 167772160;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0075, code lost:
        if (r4.A08() == false) goto L_0x0077;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.net.Uri A05(X.AnonymousClass1JY r10, int r11, int r12) {
        /*
            r9 = this;
            if (r10 != 0) goto L_0x0004
            r0 = 0
            return r0
        L_0x0004:
            X.1KS r0 = r10.A05
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x00d3;
                case 1: goto L_0x0041;
                case 2: goto L_0x0018;
                case 3: goto L_0x00b8;
                case 4: goto L_0x0015;
                default: goto L_0x000d;
            }
        L_0x000d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "not reached"
            r1.<init>(r0)
            throw r1
        L_0x0015:
            android.net.Uri r0 = r10.A02
            return r0
        L_0x0018:
            com.facebook.user.model.UserKey r5 = r10.A03
            X.0Tq r0 = r9.A02
            java.lang.Object r0 = r0.get()
            X.0t0 r0 = (X.C14300t0) r0
            com.facebook.user.model.User r4 = r0.A03(r5)
            if (r4 == 0) goto L_0x003f
            com.facebook.user.profilepic.PicSquare r0 = r4.A04()
            if (r0 == 0) goto L_0x003f
            com.facebook.user.profilepic.PicSquare r0 = r4.A04()
            android.net.Uri r0 = r9.A00(r0, r11, r12, r5)
        L_0x0036:
            if (r0 != 0) goto L_0x00d0
            com.facebook.user.profilepic.PicSquare r0 = r10.A04
            android.net.Uri r0 = r9.A00(r0, r11, r12, r5)
            return r0
        L_0x003f:
            r0 = 0
            goto L_0x0036
        L_0x0041:
            com.facebook.user.model.UserKey r4 = r10.A03
            X.0Tq r0 = r9.A02
            java.lang.Object r0 = r0.get()
            X.0t0 r0 = (X.C14300t0) r0
            com.facebook.user.model.User r5 = r0.A03(r4)
            if (r5 == 0) goto L_0x005c
            boolean r0 = r5.A0H()
            if (r0 == 0) goto L_0x005c
            com.facebook.user.model.User r0 = r5.A0N
            if (r0 == 0) goto L_0x005c
            r5 = r0
        L_0x005c:
            if (r5 == 0) goto L_0x00b6
            com.facebook.user.profilepic.PicSquare r0 = r5.A04()
            if (r0 == 0) goto L_0x00b6
            com.facebook.user.profilepic.PicSquare r0 = r5.A04()
            android.net.Uri r0 = r9.A00(r0, r11, r12, r4)
        L_0x006c:
            if (r0 != 0) goto L_0x00d0
            if (r4 == 0) goto L_0x0077
            boolean r1 = r4.A08()
            r0 = 1
            if (r1 != 0) goto L_0x0078
        L_0x0077:
            r0 = 0
        L_0x0078:
            if (r0 == 0) goto L_0x0084
            if (r5 == 0) goto L_0x009d
            X.1aB r1 = r5.A0M
            X.1aB r0 = X.C25651aB.A03
            if (r1 != r0) goto L_0x009d
            com.facebook.user.model.UserKey r4 = r5.A0Q
        L_0x0084:
            if (r4 == 0) goto L_0x00cc
            X.1aB r1 = r4.type
            X.1aB r0 = X.C25651aB.A03
            if (r1 != r0) goto L_0x00cc
            java.lang.String r1 = r4.id
            java.lang.String r0 = "0"
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 != 0) goto L_0x00cc
            java.lang.String r0 = r4.id
            android.net.Uri r0 = r9.A06(r0, r11, r12)
            return r0
        L_0x009d:
            java.lang.String r2 = r4.A04()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r0 != 0) goto L_0x00b4
            android.net.Uri r1 = android.provider.ContactsContract.Contacts.CONTENT_URI
            java.lang.String r0 = java.lang.String.valueOf(r2)
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r1, r0)
        L_0x00b1:
            if (r0 == 0) goto L_0x0084
            return r0
        L_0x00b4:
            r0 = 0
            goto L_0x00b1
        L_0x00b6:
            r0 = 0
            goto L_0x006c
        L_0x00b8:
            java.lang.String r2 = r10.A07
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r0 != 0) goto L_0x00d1
            android.net.Uri r1 = android.provider.ContactsContract.Contacts.CONTENT_URI
            java.lang.String r0 = java.lang.String.valueOf(r2)
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r1, r0)
        L_0x00ca:
            if (r0 != 0) goto L_0x00d0
        L_0x00cc:
            android.net.Uri r0 = r9.A04(r10)
        L_0x00d0:
            return r0
        L_0x00d1:
            r0 = 0
            goto L_0x00ca
        L_0x00d3:
            com.facebook.user.profilepic.PicSquare r1 = r10.A04
            com.facebook.user.model.UserKey r0 = r10.A03
            android.net.Uri r0 = r9.A00(r1, r11, r12, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21391Gt.A05(X.1JY, int, int):android.net.Uri");
    }

    public Uri A06(String str, int i, int i2) {
        StringBuilder sb = new StringBuilder(128);
        sb.append(this.A04);
        sb.append(str);
        sb.append("/picture?type=square&width=");
        sb.append(i);
        sb.append("&height=");
        sb.append(i2);
        if (this.A01.booleanValue()) {
            AnonymousClass0XN r1 = (AnonymousClass0XN) AnonymousClass1XX.A03(AnonymousClass1Y3.AXA, this.A00);
            if (r1.A08() != null) {
                sb.append("&access_token=");
                sb.append(Uri.encode(r1.A08().mAuthToken));
            }
        }
        return Uri.parse(sb.toString());
    }

    public int hashCode() {
        return AnonymousClass07K.A01(this.A01, this.A04);
    }

    private C21391Gt(AnonymousClass1XY r3, AnonymousClass13Q r4) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A02 = C14300t0.A02(r3);
        this.A03 = C04750Wa.A01(r3);
        C21401Gu.A00(r3);
        String builder = r4.AoB().toString();
        this.A04 = !builder.endsWith("/") ? AnonymousClass08S.A06(builder, '/') : builder;
    }

    private Uri A00(PicSquare picSquare, int i, int i2, UserKey userKey) {
        PicSquareUrlWithSize A002 = picSquare.A00(i2);
        PicSquareUrlWithSize A003 = picSquare.A00(i2 - ((i2 * 25) / 100));
        if (Math.abs(i2 - A003.size) < Math.abs(i2 - A002.size)) {
            A002 = A003;
        }
        C010708t.A01.BFj(2);
        Uri parse = Uri.parse(A002.url);
        if (parse.isAbsolute()) {
            return parse;
        }
        StringBuilder sb = new StringBuilder("Invalid PicSquareUri:");
        sb.append(A002.url);
        if (userKey != null) {
            sb.append("|user=");
            sb.append(userKey.toString());
        }
        sb.append("|tw=");
        sb.append(i);
        sb.append("|th=");
        sb.append(i2);
        C24971Xv it = picSquare.mPicSquareUrlsWithSizes.iterator();
        while (it.hasNext()) {
            PicSquareUrlWithSize picSquareUrlWithSize = (PicSquareUrlWithSize) it.next();
            sb.append("|url_");
            sb.append(picSquareUrlWithSize.size);
            sb.append(":");
            sb.append(picSquareUrlWithSize.url);
        }
        this.A03.CGS(A05.toString(), sb.toString());
        return null;
    }

    public ImmutableList A07(ImmutableList immutableList) {
        ImmutableList.Builder builder = ImmutableList.builder();
        C14300t0 r3 = (C14300t0) this.A02.get();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            User A032 = r3.A03((UserKey) it.next());
            if (!(A032 == null || A032.A04() == null)) {
                builder.add((Object) A032.A04());
            }
        }
        return builder.build();
    }
}
