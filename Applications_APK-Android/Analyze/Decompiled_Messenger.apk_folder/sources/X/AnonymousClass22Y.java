package X;

import com.facebook.graphql.model.GraphQLMedia;

/* renamed from: X.22Y  reason: invalid class name */
public final class AnonymousClass22Y extends AnonymousClass0UT {
    public AHK A00(C176408Cq r9, GraphQLMedia graphQLMedia) {
        new C96754jN(this);
        AnonymousClass0VB A00 = AnonymousClass0VB.A00(AnonymousClass1Y3.B1v, this);
        new AnonymousClass2H9(this);
        return new AHK(this, r9, graphQLMedia, A00, AnonymousClass0WT.A00(this));
    }

    public AnonymousClass22Y(AnonymousClass1XY r1) {
        super(r1);
    }
}
