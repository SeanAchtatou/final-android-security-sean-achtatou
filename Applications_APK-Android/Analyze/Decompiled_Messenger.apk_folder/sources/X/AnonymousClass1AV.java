package X;

import com.facebook.orca.threadlist.ThreadListFragment;
import com.facebook.perf.startupstatemachine.StartupStateMachine;

/* renamed from: X.1AV  reason: invalid class name */
public final class AnonymousClass1AV implements C20041Ar {
    public final /* synthetic */ ThreadListFragment A00;

    public AnonymousClass1AV(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public boolean BNl() {
        if (this.A00.A1F.getChildCount() <= 0) {
            return false;
        }
        ThreadListFragment threadListFragment = this.A00;
        if (threadListFragment.A1b) {
            C08770fv r3 = threadListFragment.A0V;
            r3.A05.markerTag(5505028, "group_thread_contained");
            r3.A05.markerTag(5505026, "group_thread_contained");
            r3.A05.markerTag(5505027, "group_thread_contained");
        }
        this.A00.A0U.A06("thread_list");
        ThreadListFragment threadListFragment2 = this.A00;
        C08770fv r4 = threadListFragment2.A0V;
        boolean z = !threadListFragment2.A2R();
        if (r4.A0N()) {
            C08770fv.A05(r4, 5505028, z);
            C08770fv.A06(r4, "draw_complete");
        } else if (C08770fv.A0A(r4)) {
            C08770fv.A05(r4, 5505026, z);
            C08770fv.A07(r4, "draw_complete");
        } else if (r4.A0M()) {
            C08770fv.A05(r4, 5505027, z);
            if (z) {
                r4.A05.markerTag(5505175, "is_in_chat_heads");
                r4.A05.markerAnnotate(5505175, "source", "chat_heads");
            }
            C08770fv.A08(r4, 5);
        }
        C08860g6 r9 = r4.A0K;
        StartupStateMachine startupStateMachine = r4.A0N;
        if (r9.A02) {
            C185414b r32 = (C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r9.A00);
            C08900gA r2 = r9.A05;
            String startupStateMachine2 = startupStateMachine.toString();
            if (z) {
                r32.AOM(r2, "threadlist_draw_complete", AnonymousClass08S.A0J(startupStateMachine2, ";chatheads"));
            } else {
                r32.AOM(r2, "threadlist_draw_complete", startupStateMachine2);
            }
            r9.A02 = false;
        }
        r4.A05.markerEnd(5505074, 2);
        r4.A05.markerEnd(5505186, 2);
        r4.A04 = null;
        AnonymousClass4Q0 r5 = (AnonymousClass4Q0) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BI2, this.A00.A0O);
        if (r5.A00 == 0) {
            r5.A00 = r5.A01.now();
        }
        ThreadListFragment.A0C(this.A00);
        return false;
    }
}
