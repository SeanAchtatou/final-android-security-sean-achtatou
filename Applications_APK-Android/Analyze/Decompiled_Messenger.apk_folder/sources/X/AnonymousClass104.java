package X;

/* renamed from: X.104  reason: invalid class name */
public enum AnonymousClass104 {
    NEEDS_ADMIN_APPROVAL(1),
    NONE(0);
    
    public final int value;

    private AnonymousClass104(int i) {
        this.value = i;
    }
}
