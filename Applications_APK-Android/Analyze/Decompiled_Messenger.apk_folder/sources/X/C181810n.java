package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.ComparatorOrdering;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.NullsFirstOrdering;
import com.google.common.collect.NullsLastOrdering;
import com.google.common.collect.ReverseOrdering;
import java.util.Arrays;
import java.util.Comparator;

/* renamed from: X.10n  reason: invalid class name and case insensitive filesystem */
public abstract class C181810n implements Comparator {
    public int compare(Object obj, Object obj2) {
        C181810n r0;
        if (this instanceof ReverseOrdering) {
            return ((ReverseOrdering) this).forwardOrder.compare(obj2, obj);
        }
        if (this instanceof NullsLastOrdering) {
            NullsLastOrdering nullsLastOrdering = (NullsLastOrdering) this;
            if (obj == obj2) {
                return 0;
            }
            if (obj == null) {
                return 1;
            }
            if (obj2 == null) {
                return -1;
            }
            r0 = nullsLastOrdering.ordering;
        } else if (!(this instanceof NullsFirstOrdering)) {
            return ((ComparatorOrdering) this).comparator.compare(obj, obj2);
        } else {
            NullsFirstOrdering nullsFirstOrdering = (NullsFirstOrdering) this;
            if (obj == obj2) {
                return 0;
            }
            if (obj == null) {
                return -1;
            }
            if (obj2 == null) {
                return 1;
            }
            r0 = nullsFirstOrdering.ordering;
        }
        return r0.compare(obj, obj2);
    }

    public static C181810n A00(Comparator comparator) {
        if (comparator instanceof C181810n) {
            return (C181810n) comparator;
        }
        return new ComparatorOrdering(comparator);
    }

    public C181810n A02() {
        return new NullsFirstOrdering(this);
    }

    public C181810n A03() {
        return new NullsLastOrdering(this);
    }

    public C181810n A04() {
        if (this instanceof ReverseOrdering) {
            return ((ReverseOrdering) this).forwardOrder;
        }
        if (this instanceof NullsLastOrdering) {
            return ((NullsLastOrdering) this).ordering.A04().A02();
        }
        if (!(this instanceof NullsFirstOrdering)) {
            return new ReverseOrdering(this);
        }
        return ((NullsFirstOrdering) this).ordering.A04().A03();
    }

    public ImmutableList A01(Iterable iterable) {
        Preconditions.checkNotNull(this);
        Object[] A0E = AnonymousClass0j4.A0E(iterable);
        int length = A0E.length;
        for (int i = 0; i < length; i++) {
            AnonymousClass0U8.A00(A0E[i], i);
        }
        Arrays.sort(A0E, this);
        return ImmutableList.asImmutableList(A0E);
    }
}
