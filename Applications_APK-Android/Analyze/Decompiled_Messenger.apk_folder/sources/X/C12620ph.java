package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.FetchThreadListParams;
import com.facebook.messaging.service.model.FetchThreadListResult;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

@UserScoped
/* renamed from: X.0ph  reason: invalid class name and case insensitive filesystem */
public final class C12620ph {
    private static C05540Zi A06;
    private static final String[] A07 = {"unread_count", "unseen_count", "last_seen_time"};
    public final C28011e7 A00;
    public final C12160of A01;
    public final C07040cX A02;
    public final C12670pm A03;
    public final C04310Tq A04;
    public final C04310Tq A05;

    public static final C12620ph A00(AnonymousClass1XY r4) {
        C12620ph r0;
        synchronized (C12620ph.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r4)) {
                    A06.A00 = new C12620ph((AnonymousClass1XY) A06.A01());
                }
                C05540Zi r1 = A06;
                r0 = (C12620ph) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    public static Set A03(ImmutableList immutableList) {
        C05180Xy r5 = new C05180Xy();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            ThreadSummary threadSummary = (ThreadSummary) it.next();
            C24971Xv it2 = threadSummary.A0m.iterator();
            while (it2.hasNext()) {
                ThreadParticipant threadParticipant = (ThreadParticipant) it2.next();
                if (threadParticipant.A00() != null) {
                    r5.add(threadParticipant.A00());
                }
            }
            C24971Xv it3 = threadSummary.A0k.iterator();
            while (it3.hasNext()) {
                ThreadParticipant threadParticipant2 = (ThreadParticipant) it3.next();
                if (threadParticipant2.A00() != null) {
                    r5.add(threadParticipant2.A00());
                }
            }
        }
        return r5;
    }

    public long A04(C10950l8 r5) {
        SQLiteDatabase A062 = ((C25771aN) this.A04.get()).A06();
        String str = r5.dbName;
        Cursor rawQuery = A062.rawQuery("SELECT MIN(timestamp_ms) FROM folders WHERE folder=? AND thread_key != ?", new String[]{str, C42762Bs.A01(str)});
        try {
            if (rawQuery.moveToNext()) {
                return rawQuery.getLong(0);
            }
            rawQuery.close();
            return -1;
        } finally {
            rawQuery.close();
        }
    }

    public FolderCounts A05(C10950l8 r11) {
        Cursor query;
        FolderCounts folderCounts;
        int i;
        C005505z.A03("DbFetchThreadsHandler.getFolderCounts", 1937569720);
        try {
            C06140av A032 = C06160ax.A03("folder", r11.dbName);
            query = ((C25771aN) this.A04.get()).A06().query("folder_counts", A07, A032.A02(), A032.A04(), null, null, null);
            if (query.moveToNext()) {
                folderCounts = new FolderCounts(query.getInt(0), query.getInt(1), query.getLong(2));
                query.close();
                i = 539919299;
            } else {
                folderCounts = null;
                query.close();
                i = 1945902963;
            }
            C005505z.A00(i);
            return folderCounts;
        } catch (Throwable th) {
            C005505z.A00(482601819);
            throw th;
        }
    }

    public FetchThreadListResult A06(FetchThreadListParams fetchThreadListParams) {
        int i;
        FetchThreadListResult fetchThreadListResult;
        DataFetchDisposition dataFetchDisposition;
        C005505z.A03("DbFetchThreadsHandler.fetchThreadListFromDb", 303986619);
        try {
            C07080cb r6 = (C07080cb) this.A05.get();
            C10950l8 r7 = fetchThreadListParams.A03;
            long A012 = r6.A01(C12740pt.A01(r7), -1);
            boolean z = true;
            boolean A062 = r6.A06(C12740pt.A00(r7), true);
            if (A012 == -1) {
                fetchThreadListResult = FetchThreadListResult.A00(r7);
                i = 1222761550;
            } else {
                if (A062) {
                    dataFetchDisposition = DataFetchDisposition.A0E;
                } else {
                    dataFetchDisposition = DataFetchDisposition.A0F;
                }
                ImmutableList A072 = A07(fetchThreadListParams.A03, -1, fetchThreadListParams.A01());
                ImmutableList A042 = this.A00.A04(A03(A072));
                if (A072.size() >= fetchThreadListParams.A01() || !A0A(C42762Bs.A01(r7.dbName))) {
                    z = false;
                }
                ImmutableList A022 = A02(A072, fetchThreadListParams.A01());
                ThreadsCollection threadsCollection = new ThreadsCollection(A022, z);
                FolderCounts A052 = A05(fetchThreadListParams.A03);
                if (!A022.isEmpty()) {
                    this.A01.A01(((ThreadSummary) A022.get(0)).A0B);
                }
                AnonymousClass161 r1 = new AnonymousClass161();
                r1.A02 = dataFetchDisposition;
                r1.A04 = r7;
                r1.A06 = threadsCollection;
                r1.A09 = A042;
                r1.A03 = A052;
                r1.A00 = A012;
                fetchThreadListResult = new FetchThreadListResult(r1);
                i = 2145234905;
            }
            C005505z.A00(i);
            return fetchThreadListResult;
        } catch (Throwable th) {
            C005505z.A00(855726647);
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r3.close();
        r10 = java.lang.System.currentTimeMillis();
        r9 = new X.C05180Xy();
        r4 = r1.values().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0095, code lost:
        if (r4.hasNext() == false) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0097, code lost:
        r9.add(java.lang.Long.valueOf(((X.C17920zh) r4.next()).A0A));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a7, code lost:
        r12 = ((X.C25771aN) r5.A04.get()).A06();
        r0 = X.C06160ax.A01(X.C06160ax.A04("thread_key", r1.keySet()), X.C06160ax.A04(com.facebook.proxygen.TraceFieldType.MsgType, com.google.common.collect.ImmutableSet.A05(java.lang.Integer.toString(X.AnonymousClass1V7.A0A.dbKeyValue), java.lang.Integer.toString(X.AnonymousClass1V7.A0J.dbKeyValue))), X.C06160ax.A02(new X.C29661gm("timestamp_ms", java.lang.Long.toString(r10 - 86400000)), X.C06160ax.A04("timestamp_ms", r9)));
        r10 = r12.query(true, "messages", new java.lang.String[]{"thread_key", "timestamp_ms", com.facebook.proxygen.TraceFieldType.MsgType}, r0.A02(), r0.A04(), null, null, null, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x011b, code lost:
        if (r10.moveToNext() == false) goto L_0x0153;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x011d, code lost:
        r11 = (X.C17920zh) r1.get(com.facebook.messaging.model.threadkey.ThreadKey.A06(r10.getString(0)));
        r5 = X.AnonymousClass1V7.A00(java.lang.Integer.parseInt(r10.getString(2)));
        r8 = r10.getLong(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x013d, code lost:
        if (r5 != X.AnonymousClass1V7.A0A) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x013f, code lost:
        r11.A0z = true;
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0146, code lost:
        if (r8 != r11.A0A) goto L_0x0149;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0148, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0149, code lost:
        r11.A0y = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x014e, code lost:
        if (r5 != X.AnonymousClass1V7.A0J) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0150, code lost:
        r11.A11 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r10.close();
        r2 = com.google.common.collect.ImmutableList.builder();
        r1 = r1.values().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0166, code lost:
        if (r1.hasNext() == false) goto L_0x0176;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0168, code lost:
        r2.add((java.lang.Object) ((X.C17920zh) r1.next()).A00());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0176, code lost:
        r1 = r2.build();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0179, code lost:
        X.C005505z.A00(-310887974);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0180, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0188, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0189, code lost:
        if (r3 != null) goto L_0x018b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x018e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A07(X.C10950l8 r23, long r24, int r26) {
        /*
            r22 = this;
            java.lang.String r1 = "DbFetchThreadsHandler.doThreadListQuery"
            r0 = 605062710(0x24108636, float:3.1338704E-17)
            X.C005505z.A03(r1, r0)
            java.util.LinkedHashMap r1 = new java.util.LinkedHashMap     // Catch:{ all -> 0x018f }
            r1.<init>()     // Catch:{ all -> 0x018f }
            X.0l8 r0 = X.C10950l8.A0B     // Catch:{ all -> 0x018f }
            r7 = r23
            r5 = r22
            if (r7 == r0) goto L_0x004b
            X.0pm r6 = r5.A03     // Catch:{ all -> 0x018f }
            X.1a6 r4 = X.C06160ax.A00()     // Catch:{ all -> 0x018f }
            java.lang.String r2 = r7.dbName     // Catch:{ all -> 0x018f }
            java.lang.String r0 = "folder"
            X.0av r0 = X.C06160ax.A03(r0, r2)     // Catch:{ all -> 0x018f }
            r4.A05(r0)     // Catch:{ all -> 0x018f }
            r2 = 0
            int r0 = (r24 > r2 ? 1 : (r24 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x003a
            java.lang.String r3 = java.lang.Long.toString(r24)     // Catch:{ all -> 0x018f }
            java.lang.String r2 = "timestamp_in_folder_ms"
            X.2Sr r0 = new X.2Sr     // Catch:{ all -> 0x018f }
            r0.<init>(r2, r3)     // Catch:{ all -> 0x018f }
            r4.A05(r0)     // Catch:{ all -> 0x018f }
        L_0x003a:
            java.lang.String r2 = "timestamp_in_folder_ms DESC"
            r3 = r26
            if (r26 <= 0) goto L_0x0046
            java.lang.String r0 = " LIMIT "
            java.lang.String r2 = X.AnonymousClass08S.A0L(r2, r0, r3)     // Catch:{ all -> 0x018f }
        L_0x0046:
            X.1e8 r3 = X.C12670pm.A01(r6, r4, r2, r7)     // Catch:{ all -> 0x018f }
            goto L_0x0071
        L_0x004b:
            X.0pm r4 = r5.A03     // Catch:{ all -> 0x018f }
            X.1a6 r3 = X.C06160ax.A00()     // Catch:{ all -> 0x018f }
            X.0l8 r0 = X.C10950l8.A05     // Catch:{ all -> 0x018f }
            java.lang.String r2 = r0.dbName     // Catch:{ all -> 0x018f }
            java.lang.String r0 = "folder"
            X.0av r0 = X.C06160ax.A03(r0, r2)     // Catch:{ all -> 0x018f }
            r3.A05(r0)     // Catch:{ all -> 0x018f }
            java.lang.String r2 = "is_thread_pinned"
            java.lang.String r0 = "1"
            X.0av r0 = X.C06160ax.A03(r2, r0)     // Catch:{ all -> 0x018f }
            r3.A05(r0)     // Catch:{ all -> 0x018f }
            X.0l8 r2 = X.C10950l8.A05     // Catch:{ all -> 0x018f }
            java.lang.String r0 = "thread_pin_timestamp ASC"
            X.1e8 r3 = X.C12670pm.A01(r4, r3, r0, r2)     // Catch:{ all -> 0x018f }
        L_0x0071:
            X.0zh r2 = r3.BLo()     // Catch:{ all -> 0x0186 }
            if (r2 == 0) goto L_0x007d
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r2.A0R     // Catch:{ all -> 0x0186 }
            r1.put(r0, r2)     // Catch:{ all -> 0x0186 }
            goto L_0x0071
        L_0x007d:
            r3.close()     // Catch:{ all -> 0x018f }
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x018f }
            X.0Xy r9 = new X.0Xy     // Catch:{ all -> 0x018f }
            r9.<init>()     // Catch:{ all -> 0x018f }
            java.util.Collection r0 = r1.values()     // Catch:{ all -> 0x018f }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ all -> 0x018f }
        L_0x0091:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x018f }
            if (r0 == 0) goto L_0x00a7
            java.lang.Object r0 = r4.next()     // Catch:{ all -> 0x018f }
            X.0zh r0 = (X.C17920zh) r0     // Catch:{ all -> 0x018f }
            long r2 = r0.A0A     // Catch:{ all -> 0x018f }
            java.lang.Long r0 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x018f }
            r9.add(r0)     // Catch:{ all -> 0x018f }
            goto L_0x0091
        L_0x00a7:
            X.0Tq r0 = r5.A04     // Catch:{ all -> 0x018f }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x018f }
            X.1aN r0 = (X.C25771aN) r0     // Catch:{ all -> 0x018f }
            android.database.sqlite.SQLiteDatabase r12 = r0.A06()     // Catch:{ all -> 0x018f }
            java.util.Set r0 = r1.keySet()     // Catch:{ all -> 0x018f }
            java.lang.String r6 = "thread_key"
            X.0av r8 = X.C06160ax.A04(r6, r0)     // Catch:{ all -> 0x018f }
            r4 = 0
            X.1V7 r0 = X.AnonymousClass1V7.A0A     // Catch:{ all -> 0x018f }
            int r0 = r0.dbKeyValue     // Catch:{ all -> 0x018f }
            java.lang.String r2 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x018f }
            X.1V7 r0 = X.AnonymousClass1V7.A0J     // Catch:{ all -> 0x018f }
            int r0 = r0.dbKeyValue     // Catch:{ all -> 0x018f }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x018f }
            com.google.common.collect.ImmutableSet r0 = com.google.common.collect.ImmutableSet.A05(r2, r0)     // Catch:{ all -> 0x018f }
            java.lang.String r5 = "msg_type"
            X.0av r7 = X.C06160ax.A04(r5, r0)     // Catch:{ all -> 0x018f }
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r10 = r10 - r2
            java.lang.String r0 = java.lang.Long.toString(r10)     // Catch:{ all -> 0x018f }
            java.lang.String r3 = "timestamp_ms"
            X.1gm r2 = new X.1gm     // Catch:{ all -> 0x018f }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x018f }
            X.0av r0 = X.C06160ax.A04(r3, r9)     // Catch:{ all -> 0x018f }
            X.0av[] r0 = new X.C06140av[]{r2, r0}     // Catch:{ all -> 0x018f }
            X.1a6 r0 = X.C06160ax.A02(r0)     // Catch:{ all -> 0x018f }
            X.0av[] r0 = new X.C06140av[]{r8, r7, r0}     // Catch:{ all -> 0x018f }
            X.1a6 r0 = X.C06160ax.A01(r0)     // Catch:{ all -> 0x018f }
            r13 = 1
            java.lang.String[] r15 = new java.lang.String[]{r6, r3, r5}     // Catch:{ all -> 0x018f }
            java.lang.String r16 = r0.A02()     // Catch:{ all -> 0x018f }
            java.lang.String[] r17 = r0.A04()     // Catch:{ all -> 0x018f }
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            java.lang.String r14 = "messages"
            r3 = 2
            android.database.Cursor r10 = r12.query(r13, r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ all -> 0x018f }
        L_0x0117:
            boolean r0 = r10.moveToNext()     // Catch:{ all -> 0x0181 }
            if (r0 == 0) goto L_0x0153
            java.lang.String r0 = r10.getString(r4)     // Catch:{ all -> 0x0181 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)     // Catch:{ all -> 0x0181 }
            java.lang.Object r11 = r1.get(r0)     // Catch:{ all -> 0x0181 }
            X.0zh r11 = (X.C17920zh) r11     // Catch:{ all -> 0x0181 }
            java.lang.String r0 = r10.getString(r3)     // Catch:{ all -> 0x0181 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x0181 }
            X.1V7 r5 = X.AnonymousClass1V7.A00(r0)     // Catch:{ all -> 0x0181 }
            long r8 = r10.getLong(r13)     // Catch:{ all -> 0x0181 }
            X.1V7 r0 = X.AnonymousClass1V7.A0A     // Catch:{ all -> 0x0181 }
            if (r5 != r0) goto L_0x014c
            r11.A0z = r13     // Catch:{ all -> 0x0181 }
            long r6 = r11.A0A     // Catch:{ all -> 0x0181 }
            int r5 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            r0 = 0
            if (r5 != 0) goto L_0x0149
            r0 = 1
        L_0x0149:
            r11.A0y = r0     // Catch:{ all -> 0x0181 }
            goto L_0x0117
        L_0x014c:
            X.1V7 r0 = X.AnonymousClass1V7.A0J     // Catch:{ all -> 0x0181 }
            if (r5 != r0) goto L_0x0117
            r11.A11 = r13     // Catch:{ all -> 0x0181 }
            goto L_0x0117
        L_0x0153:
            r10.close()     // Catch:{ all -> 0x018f }
            com.google.common.collect.ImmutableList$Builder r2 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x018f }
            java.util.Collection r0 = r1.values()     // Catch:{ all -> 0x018f }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x018f }
        L_0x0162:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x018f }
            if (r0 == 0) goto L_0x0176
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x018f }
            X.0zh r0 = (X.C17920zh) r0     // Catch:{ all -> 0x018f }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A00()     // Catch:{ all -> 0x018f }
            r2.add(r0)     // Catch:{ all -> 0x018f }
            goto L_0x0162
        L_0x0176:
            com.google.common.collect.ImmutableList r1 = r2.build()     // Catch:{ all -> 0x018f }
            r0 = -310887974(0xffffffffed7839da, float:-4.8013888E27)
            X.C005505z.A00(r0)
            return r1
        L_0x0181:
            r0 = move-exception
            r10.close()     // Catch:{ all -> 0x018f }
            throw r0     // Catch:{ all -> 0x018f }
        L_0x0186:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0188 }
        L_0x0188:
            r0 = move-exception
            if (r3 == 0) goto L_0x018e
            r3.close()     // Catch:{ all -> 0x018e }
        L_0x018e:
            throw r0     // Catch:{ all -> 0x018f }
        L_0x018f:
            r1 = move-exception
            r0 = 223772537(0xd567f79, float:6.609728E-31)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12620ph.A07(X.0l8, long, int):com.google.common.collect.ImmutableList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0052, code lost:
        if (r1 == null) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0057, code lost:
        r1 = r4.build();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005a, code lost:
        X.C005505z.A00(-196434763);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0061, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0064, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0065, code lost:
        if (r1 != null) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x006a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A08(X.C10950l8 r8, long r9, int r11) {
        /*
            r7 = this;
            java.lang.String r1 = "DbFetchThreadsHandler.fetchGroupThreadsList"
            r0 = -858509658(0xffffffffccd42ea6, float:-1.11244592E8)
            X.C005505z.A03(r1, r0)
            com.google.common.collect.ImmutableList$Builder r4 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x006b }
            X.0pm r5 = r7.A03     // Catch:{ all -> 0x006b }
            java.lang.String r6 = "timestamp_ms"
            X.1fF r0 = X.C28711fF.GROUP     // Catch:{ all -> 0x006b }
            X.1a6 r1 = X.C12670pm.A00(r0, r8)     // Catch:{ all -> 0x006b }
            X.1fF r0 = X.C28711fF.OPTIMISTIC_GROUP_THREAD     // Catch:{ all -> 0x006b }
            X.1a6 r0 = X.C12670pm.A00(r0, r8)     // Catch:{ all -> 0x006b }
            X.0av[] r0 = new X.C06140av[]{r1, r0}     // Catch:{ all -> 0x006b }
            X.1a6 r3 = X.C06160ax.A02(r0)     // Catch:{ all -> 0x006b }
            r1 = 0
            int r0 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0036
            java.lang.String r1 = java.lang.Long.toString(r9)     // Catch:{ all -> 0x006b }
            X.2jZ r0 = new X.2jZ     // Catch:{ all -> 0x006b }
            r0.<init>(r6, r1)     // Catch:{ all -> 0x006b }
            r3.A05(r0)     // Catch:{ all -> 0x006b }
        L_0x0036:
            java.lang.String r0 = " DESC"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r6, r0)     // Catch:{ all -> 0x006b }
            if (r11 <= 0) goto L_0x0044
            java.lang.String r0 = " LIMIT "
            java.lang.String r1 = X.AnonymousClass08S.A0L(r1, r0, r11)     // Catch:{ all -> 0x006b }
        L_0x0044:
            X.1e8 r1 = X.C12670pm.A02(r5, r3, r1)     // Catch:{ all -> 0x006b }
        L_0x0048:
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.BLt()     // Catch:{ all -> 0x0062 }
            if (r0 == 0) goto L_0x0052
            r4.add(r0)     // Catch:{ all -> 0x0062 }
            goto L_0x0048
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ all -> 0x006b }
        L_0x0057:
            com.google.common.collect.ImmutableList r1 = r4.build()     // Catch:{ all -> 0x006b }
            r0 = -196434763(0xfffffffff44aa4b5, float:-6.4220253E31)
            X.C005505z.A00(r0)
            return r1
        L_0x0062:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0064 }
        L_0x0064:
            r0 = move-exception
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ all -> 0x006a }
        L_0x006a:
            throw r0     // Catch:{ all -> 0x006b }
        L_0x006b:
            r1 = move-exception
            r0 = 807204265(0x301cf5a9, float:5.7101573E-10)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12620ph.A08(X.0l8, long, int):com.google.common.collect.ImmutableList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0037, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0038, code lost:
        if (r1 != null) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x003d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A09(java.util.Set r5) {
        /*
            r4 = this;
            java.lang.String r1 = "DbFetchThreadsHandler.doCustomThreadSetQuery"
            r0 = -798718906(0xffffffffd0648446, float:-1.53354957E10)
            X.C005505z.A03(r1, r0)
            com.google.common.collect.ImmutableList$Builder r3 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x003e }
            X.0pm r2 = r4.A03     // Catch:{ all -> 0x003e }
            java.lang.String r0 = "thread_key"
            X.0av r1 = X.C06160ax.A04(r0, r5)     // Catch:{ all -> 0x003e }
            r0 = 0
            X.1e8 r1 = X.C12670pm.A02(r2, r1, r0)     // Catch:{ all -> 0x003e }
        L_0x0019:
            X.0zh r0 = r1.BLo()     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x0027
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A00()     // Catch:{ all -> 0x0035 }
            r3.add(r0)     // Catch:{ all -> 0x0035 }
            goto L_0x0019
        L_0x0027:
            r1.close()     // Catch:{ all -> 0x003e }
            com.google.common.collect.ImmutableList r1 = r3.build()     // Catch:{ all -> 0x003e }
            r0 = -966184901(0xffffffffc669303b, float:-14924.058)
            X.C005505z.A00(r0)
            return r1
        L_0x0035:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ all -> 0x003d }
        L_0x003d:
            throw r0     // Catch:{ all -> 0x003e }
        L_0x003e:
            r1 = move-exception
            r0 = -349334561(0xffffffffeb2d93df, float:-2.0984247E26)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12620ph.A09(java.util.Set):com.google.common.collect.ImmutableList");
    }

    public boolean A0A(String str) {
        Cursor query;
        C005505z.A03("DbFetchThreadsHandler.containsFirstThreadSentinalForFolder", 1202996795);
        try {
            C06140av A032 = C06160ax.A03("thread_key", str);
            query = ((C25771aN) this.A04.get()).A06().query("folders", new String[]{"thread_key"}, A032.A02(), A032.A04(), null, null, null);
            boolean moveToNext = query.moveToNext();
            query.close();
            C005505z.A00(563559799);
            return moveToNext;
        } catch (Throwable th) {
            C005505z.A00(975687891);
            throw th;
        }
    }

    private C12620ph(AnonymousClass1XY r2) {
        this.A04 = C25771aN.A02(r2);
        this.A00 = C28011e7.A00(r2);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.Aeq, r2);
        this.A03 = C12670pm.A03(r2);
        this.A01 = C12160of.A00(r2);
        this.A02 = new C07040cX(r2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0082, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0083, code lost:
        if (r1 != null) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x0088 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.common.collect.ImmutableList A01(X.C12620ph r6, X.C10950l8 r7, long r8, int r10, X.AnonymousClass2BV r11) {
        /*
            int r0 = r11.ordinal()
            switch(r0) {
                case 1: goto L_0x000a;
                case 2: goto L_0x0091;
                default: goto L_0x0007;
            }
        L_0x0007:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            return r0
        L_0x000a:
            java.lang.String r1 = "DbFetchThreadsHandler.fetchVideoRoomThreadsList"
            r0 = -917868809(0xffffffffc94a6ef7, float:-829167.44)
            X.C005505z.A03(r1, r0)
            com.google.common.collect.ImmutableList$Builder r3 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x0089 }
            X.0pm r4 = r6.A03     // Catch:{ all -> 0x0089 }
            java.lang.String r6 = "timestamp_ms"
            X.1fF r0 = X.C28711fF.GROUP     // Catch:{ all -> 0x0089 }
            X.1a6 r1 = X.C12670pm.A00(r0, r7)     // Catch:{ all -> 0x0089 }
            X.1fF r0 = X.C28711fF.OPTIMISTIC_GROUP_THREAD     // Catch:{ all -> 0x0089 }
            X.1a6 r0 = X.C12670pm.A00(r0, r7)     // Catch:{ all -> 0x0089 }
            X.0av[] r0 = new X.C06140av[]{r1, r0}     // Catch:{ all -> 0x0089 }
            X.1a6 r5 = X.C06160ax.A02(r0)     // Catch:{ all -> 0x0089 }
            java.lang.String r1 = "1"
            java.lang.String r0 = "is_joinable"
            X.0av r0 = X.C06160ax.A03(r0, r1)     // Catch:{ all -> 0x0089 }
            r5.A05(r0)     // Catch:{ all -> 0x0089 }
            java.lang.String r0 = "video_room_mode"
            X.0av r0 = X.C06160ax.A03(r0, r1)     // Catch:{ all -> 0x0089 }
            r5.A05(r0)     // Catch:{ all -> 0x0089 }
            r1 = 0
            int r0 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0054
            java.lang.String r1 = java.lang.Long.toString(r8)     // Catch:{ all -> 0x0089 }
            X.2Sr r0 = new X.2Sr     // Catch:{ all -> 0x0089 }
            r0.<init>(r6, r1)     // Catch:{ all -> 0x0089 }
            r5.A05(r0)     // Catch:{ all -> 0x0089 }
        L_0x0054:
            java.lang.String r0 = " DESC"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r6, r0)     // Catch:{ all -> 0x0089 }
            if (r10 <= 0) goto L_0x0062
            java.lang.String r0 = " LIMIT "
            java.lang.String r1 = X.AnonymousClass08S.A0L(r1, r0, r10)     // Catch:{ all -> 0x0089 }
        L_0x0062:
            X.1e8 r1 = X.C12670pm.A02(r4, r5, r1)     // Catch:{ all -> 0x0089 }
        L_0x0066:
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.BLt()     // Catch:{ all -> 0x0080 }
            if (r0 == 0) goto L_0x0070
            r3.add(r0)     // Catch:{ all -> 0x0080 }
            goto L_0x0066
        L_0x0070:
            if (r1 == 0) goto L_0x0075
            r1.close()     // Catch:{ all -> 0x0089 }
        L_0x0075:
            com.google.common.collect.ImmutableList r1 = r3.build()     // Catch:{ all -> 0x0089 }
            r0 = -759500790(0xffffffffd2baf00a, float:-4.01445552E11)
            X.C005505z.A00(r0)
            return r1
        L_0x0080:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0082 }
        L_0x0082:
            r0 = move-exception
            if (r1 == 0) goto L_0x0088
            r1.close()     // Catch:{ all -> 0x0088 }
        L_0x0088:
            throw r0     // Catch:{ all -> 0x0089 }
        L_0x0089:
            r1 = move-exception
            r0 = -1865489700(0xffffffff90cee2dc, float:-8.1602205E-29)
            X.C005505z.A00(r0)
            throw r1
        L_0x0091:
            com.google.common.collect.ImmutableList r0 = r6.A08(r7, r8, r10)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12620ph.A01(X.0ph, X.0l8, long, int, X.2BV):com.google.common.collect.ImmutableList");
    }

    public static ImmutableList A02(Collection collection, int i) {
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = collection.iterator();
        int i2 = 0;
        while (it.hasNext() && i2 < i) {
            builder.add(it.next());
            i2++;
        }
        return builder.build();
    }
}
