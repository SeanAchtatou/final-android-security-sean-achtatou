package X;

import com.facebook.profilo.core.TriggerRegistry;
import com.facebook.profilo.ipc.TraceContext;
import java.util.Random;

/* renamed from: X.04n  reason: invalid class name and case insensitive filesystem */
public final class C003704n implements AnonymousClass04j {
    public static final int A01 = TriggerRegistry.A00.A02("black_box");
    private final ThreadLocal A00 = new AnonymousClass059();

    public boolean AUJ(long j, Object obj, long j2, Object obj2) {
        return j == 0 || j2 == 0 || j == j2;
    }

    public boolean BE8() {
        return true;
    }

    public int AYs(long j, Object obj, AnonymousClass057 r6) {
        AnonymousClass072 r62 = (AnonymousClass072) r6;
        if (((Random) this.A00.get()).nextInt(r62.A03) == 0) {
            return r62.A04;
        }
        return 0;
    }

    public TraceContext.TraceConfigExtras B6o(long j, Object obj, AnonymousClass057 r8) {
        AnonymousClass072 r82 = (AnonymousClass072) r8;
        return new TraceContext.TraceConfigExtras(r82.A02, r82.A00, r82.A01);
    }
}
