package X;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import io.card.payment.BuildConfig;
import java.util.Map;

/* renamed from: X.0RW  reason: invalid class name */
public abstract class AnonymousClass0RW extends IntentService {
    public AnonymousClass0B0 A00;
    public C012309k A01 = new C012309k(this, A00());

    public AnonymousClass09P A00() {
        return null;
    }

    public abstract void A01();

    public abstract void A02(int i);

    public abstract void A03(Intent intent);

    public abstract void A04(String str);

    public void A05(String str, String str2, Map map) {
    }

    public abstract void A06(String str, boolean z);

    public void onHandleIntent(Intent intent) {
        if (intent != null) {
            try {
                if ("com.facebook.rti.fbns.intent.RECEIVE".equals(intent.getAction())) {
                    intent.toString();
                    if (!this.A01.A04(intent)) {
                        A05(null, "INVALID_SENDER", null);
                    } else {
                        String stringExtra = intent.getStringExtra("receive_type");
                        if ("message".equals(stringExtra)) {
                            String stringExtra2 = intent.getStringExtra("token");
                            String string = this.A00.getString("token_key", BuildConfig.FLAVOR);
                            String stringExtra3 = intent.getStringExtra("extra_notification_id");
                            if (TextUtils.isEmpty(string) || string.equals(stringExtra2)) {
                                A05(stringExtra3, "FBNS_LITE_NOTIFICATION_RECEIVED", null);
                                A03(intent);
                            } else {
                                C010708t.A0J("FbnsCallbackHandlerBase", "Dropping unintended message.");
                                A05(stringExtra3, "TOKEN_MISMATCH", null);
                            }
                        } else if ("registered".equals(stringExtra)) {
                            String stringExtra4 = intent.getStringExtra("data");
                            AnonymousClass0DD AY8 = this.A00.AY8();
                            AY8.BzD("token_key", stringExtra4);
                            AY8.commit();
                            A06(stringExtra4, AnonymousClass0A9.A01(C012309k.A00(intent)));
                        } else if ("reg_error".equals(stringExtra)) {
                            A04(intent.getStringExtra("data"));
                        } else if ("deleted".equals(stringExtra)) {
                            A02(-1);
                        } else if ("unregistered".equals(stringExtra)) {
                            A01();
                        } else {
                            C010708t.A0I("FbnsCallbackHandlerBase", "Unknown message type");
                        }
                    }
                }
            } finally {
                AnonymousClass0K2.A00(intent);
            }
        }
    }

    public AnonymousClass0RW(String str) {
        super(str);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        int A012 = AnonymousClass02C.A01(this, -483181011);
        this.A00 = new AnonymousClass0QF(this).AbP(AnonymousClass07B.A03);
        int onStartCommand = super.onStartCommand(intent, i, i2);
        AnonymousClass02C.A02(-860283456, A012);
        return onStartCommand;
    }
}
