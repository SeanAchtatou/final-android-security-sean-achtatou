package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.google.common.base.Optional;
import java.util.Iterator;

/* renamed from: X.0rT  reason: invalid class name and case insensitive filesystem */
public class C13460rT extends C13470rU implements C11450nB, C11470nD, AnonymousClass09C {
    public static final String __redex_internal_original_name = "com.facebook.base.fragment.FbFragment";
    public C21081Ey A00;
    public AnonymousClass0UN A01;
    private LayoutInflater A02;

    public void A2M(Bundle bundle) {
    }

    public LayoutInflater A13(Bundle bundle) {
        C31661k3 r1;
        if (this.A02 == null) {
            AnonymousClass580 A13 = super.A13(bundle);
            Fragment fragment = this.A0L;
            if (!(fragment == null || A1j() == fragment.A1j())) {
                A13 = A13.cloneInContext(fragment.A1j());
            }
            if (((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A01)).Aem(284232346111981L) && (r1 = (C31661k3) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6h, this.A01)) != null) {
                A13 = new AnonymousClass580(r1, A13);
            }
            this.A02 = A13;
        }
        return this.A02;
    }

    public void A29() {
        String A09 = AnonymousClass08S.A09("FRAGMENT_", hashCode());
        Class<?> cls = getClass();
        ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A01)).Bz2(A09, cls.getName());
        C005505z.A05("%s.onResume", C05260Yg.A00(cls), -1401205100);
    }

    public void A2C(Bundle bundle) {
        try {
            C21081Ey r4 = this.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), -1950404072);
                        r2.BNh(r4.A00, bundle);
                        C005505z.A00(1347801616);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
            super.A2C(bundle);
            C005505z.A00(2065626741);
        } finally {
            C005505z.A00(808468466);
        }
    }

    public View A2K(int i) {
        return C012809p.A01(this.A0I, i);
    }

    public Optional A2L(int i) {
        return C012809p.A03(this.A0I, i);
    }

    public void A2N(AnonymousClass165 r3) {
        if (this.A00 == null) {
            this.A00 = new C21081Ey(this);
        }
        C21081Ey r1 = this.A00;
        synchronized (r1) {
            r1.A01.add(r3);
        }
    }

    public void A2O(AnonymousClass165 r3) {
        C21081Ey r1 = this.A00;
        synchronized (r1) {
            r1.A01.remove(r3);
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean A2P() {
        C138056cU r5 = new C138056cU(false);
        C21081Ey r4 = this.A00;
        if (r4 != null) {
            synchronized (r4) {
                Iterator it = r4.A01.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    AnonymousClass165 r2 = (AnonymousClass165) it.next();
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), -1854520008);
                        r2.BP9(r5);
                        if (((Boolean) r5.A00).booleanValue()) {
                            C005505z.A00(661237110);
                            break;
                        }
                        C005505z.A00(822092896);
                    } catch (Throwable th) {
                        C005505z.A00(1446944095);
                        throw th;
                    }
                }
            }
        }
        return ((Boolean) r5.A00).booleanValue();
    }

    public boolean A2Q() {
        if (this.A0f || !A1X() || this.A0Y || !A1a()) {
            return false;
        }
        return true;
    }

    public boolean A2R() {
        if (AnonymousClass065.A00(A1j(), Activity.class) != null) {
            return true;
        }
        return false;
    }

    public C13060qW B4m() {
        return this.A0P;
    }

    public final void A1h(Bundle bundle) {
        int A022 = C000700l.A02(1832933768);
        super.A1h(bundle);
        this.A01 = new AnonymousClass0UN(4, AnonymousClass1XX.get(A1j()));
        if (this.A00 == null) {
            this.A00 = new C21081Ey(this);
        }
        for (AnonymousClass165 r2 : this.A00.A01) {
            try {
                C005505z.A03(r2.getClass().getSimpleName(), 772770972);
                r2.BPf(bundle);
            } finally {
                C005505z.A00(1806584297);
            }
        }
        A2M(bundle);
        C000700l.A08(1217939623, A022);
    }

    public void A1u(Bundle bundle) {
        super.A1u(bundle);
        C21081Ey r4 = this.A00;
        if (r4 != null) {
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 1134555061);
                        r2.Bmy(r4.A00, bundle);
                        C005505z.A00(-1981462405);
                    } catch (Throwable th) {
                        C005505z.A00(1311885922);
                        throw th;
                    }
                }
            }
        }
    }

    public void A1v(View view, Bundle bundle) {
        C005505z.A05("%s.onViewCreated", C05260Yg.A00(getClass()), -1211154469);
        try {
            super.A1v(view, bundle);
            C21081Ey r4 = this.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), -2002463404);
                        r2.BNx(r4.A00, view, bundle);
                        C005505z.A00(-1559454142);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
            C005505z.A00(1580119103);
        } finally {
            C005505z.A00(1625298970);
        }
    }

    public void A1w(Fragment fragment) {
        super.A1w(fragment);
        C21081Ey r4 = this.A00;
        if (r4 != null) {
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 1576177080);
                        r2.BOY(r4.A00, fragment);
                        C005505z.A00(-898419874);
                    } catch (Throwable th) {
                        C005505z.A00(-513405463);
                        throw th;
                    }
                }
            }
        }
    }

    public void A1x(boolean z) {
        super.A1x(z);
        C21081Ey r4 = this.A00;
        if (r4 != null) {
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 659770340);
                        r2.BNp(r4.A00, z);
                        C005505z.A00(-1049557390);
                    } catch (Throwable th) {
                        C005505z.A00(873320660);
                        throw th;
                    }
                }
            }
        }
    }

    public void A22() {
        try {
            super.A22();
            C21081Ey r4 = this.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 1047857422);
                        r2.BVy(r4.A00);
                        C005505z.A00(1701890423);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
            C005505z.A00(-19207192);
        } finally {
            C005505z.A00(547620585);
        }
    }

    public void A23() {
        try {
            super.A23();
            C21081Ey r4 = this.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), -305148222);
                        r2.BNy(r4.A00);
                        C005505z.A00(-171204383);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
            C005505z.A00(1801962505);
        } finally {
            C005505z.A00(-335802921);
        }
    }

    public void A24() {
        try {
            super.A24();
            C21081Ey r4 = this.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 1679971604);
                        r2.Bi0(r4.A00);
                        C005505z.A00(926833078);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
            C005505z.A00(2132537714);
        } finally {
            C005505z.A00(1644876778);
        }
    }

    public void A25() {
        try {
            super.A25();
            C21081Ey r4 = this.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 1731392217);
                        r2.BmO(r4.A00);
                        C005505z.A00(1918375647);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
            C005505z.A00(-540164819);
        } finally {
            C005505z.A00(518041576);
        }
    }

    public void A26() {
        try {
            super.A26();
            C21081Ey r4 = this.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), -1706945220);
                        r2.BpQ(r4.A00);
                        C005505z.A00(2098561520);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
            C005505z.A00(1341449629);
        } finally {
            C005505z.A00(-173455984);
        }
    }

    public void A27() {
        try {
            super.A27();
            C21081Ey r4 = this.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), -1933706286);
                        r2.Bq5(r4.A00);
                        C005505z.A00(-1390519008);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
            C005505z.A00(1178012668);
        } finally {
            C005505z.A00(586431970);
        }
    }

    public void A28() {
        C005505z.A05(C99084oO.$const$string(AnonymousClass1Y3.A1q), C05260Yg.A00(getClass()), -936672118);
        C11650nY r2 = (C11650nY) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AS7, this.A01);
        if (r2 != null && r2.A00 == C001500z.A03) {
            r2.A01.A0A(this);
        }
    }

    public void A2A() {
        C005505z.A05("%s.onStart", C05260Yg.A00(getClass()), 2075918527);
    }

    public void A2D(Bundle bundle) {
        super.A2D(bundle);
        C005505z.A00(1148587928);
    }

    public void A2E(Bundle bundle) {
        C005505z.A05("%s.onCreate", C05260Yg.A00(getClass()), -1327036841);
        super.A2E(bundle);
    }

    public void A2F(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        C005505z.A05("%s.onCreateView", C05260Yg.A00(getClass()), 1127012524);
    }

    /* JADX INFO: finally extract failed */
    public void A2G(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, View view) {
        try {
            super.A2G(layoutInflater, viewGroup, bundle, view);
            C005505z.A00(1658969227);
            C11650nY r0 = (C11650nY) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AS7, this.A01);
            if (r0 != null) {
                r0.A01(this);
            }
        } catch (Throwable th) {
            C005505z.A00(-1579259326);
            throw th;
        }
    }

    public void A2H(boolean z, boolean z2) {
        C11650nY r0;
        super.A2H(z, z2);
        C21081Ey r4 = this.A00;
        if (r4 != null) {
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 1299305308);
                        r2.BoL(r4.A00, z);
                        C005505z.A00(-898303272);
                    } catch (Throwable th) {
                        C005505z.A00(-481898577);
                        throw th;
                    }
                }
            }
        }
        if (z && (r0 = (C11650nY) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AS7, this.A01)) != null) {
            r0.A02(this);
        }
    }

    public Activity A2J() {
        return (Activity) AnonymousClass065.A00(A1j(), Activity.class);
    }

    public void BNH(int i, int i2, Intent intent) {
        super.BNH(i, i2, intent);
        C21081Ey r4 = this.A00;
        if (r4 != null) {
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 1379523295);
                        r2.BNK(r4.A00, i, i2, intent);
                        C005505z.A00(-1291216453);
                    } catch (Throwable th) {
                        C005505z.A00(-735064262);
                        throw th;
                    }
                }
            }
        }
    }

    public Object BzI(Class cls) {
        C13460rT r0 = this;
        if (!cls.isInstance(this)) {
            r0 = null;
        }
        if (r0 != null) {
            return r0;
        }
        Fragment fragment = this.A0L;
        if (fragment instanceof C11470nD) {
            return ((C11470nD) fragment).BzI(cls);
        }
        Context A1j = A1j();
        if (A1j instanceof C11470nD) {
            return ((C11470nD) A1j).BzI(cls);
        }
        return null;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        C21081Ey r4 = this.A00;
        if (r4 != null) {
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), 1253769451);
                        r2.BTL(r4.A00, configuration);
                        C005505z.A00(209091315);
                    } catch (Throwable th) {
                        C005505z.A00(-1568852963);
                        throw th;
                    }
                }
            }
        }
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        if (A2R()) {
            super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        }
    }

    public void CIW() {
        A21();
    }
}
