package X;

/* renamed from: X.0Np  reason: invalid class name and case insensitive filesystem */
public final class C03400Np {
    public final int A00;
    public final int A01;
    public final Object A02;
    public final Object A03;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
        if (r3 != 0) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C03400Np(java.lang.Object r2, int r3, java.lang.Object r4, int r5) {
        /*
            r1 = this;
            r1.<init>()
            if (r2 != 0) goto L_0x0008
            r0 = 0
            if (r3 == 0) goto L_0x0009
        L_0x0008:
            r0 = 1
        L_0x0009:
            com.google.common.base.Preconditions.checkArgument(r0)
            r1.A03 = r2
            r1.A01 = r3
            r1.A02 = r4
            r1.A00 = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03400Np.<init>(java.lang.Object, int, java.lang.Object, int):void");
    }
}
