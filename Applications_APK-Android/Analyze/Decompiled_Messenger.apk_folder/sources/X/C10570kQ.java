package X;

import io.card.payment.BuildConfig;

/* renamed from: X.0kQ  reason: invalid class name and case insensitive filesystem */
public final class C10570kQ {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "new";
            case 2:
                return "delta";
            default:
                return BuildConfig.FLAVOR;
        }
    }
}
