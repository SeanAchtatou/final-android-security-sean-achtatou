package X;

import android.net.Uri;
import android.provider.Settings;

/* renamed from: X.1cj  reason: invalid class name and case insensitive filesystem */
public final class C27151cj implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.sms.abtest.SmsIntegrationState$2";
    public final /* synthetic */ C10960l9 A00;

    public C27151cj(C10960l9 r1) {
        this.A00 = r1;
    }

    public void run() {
        try {
            this.A00.A03.getContentResolver().registerContentObserver(Uri.withAppendedPath(Settings.Secure.CONTENT_URI, "sms_default_application"), false, this.A00.A00);
        } catch (Exception e) {
            C010708t.A0N("SmsIntegrationState", "Unable to register content observer", e);
        }
    }
}
