package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.06k  reason: invalid class name and case insensitive filesystem */
public final class C006406k implements ServiceConnection {
    public static final AnonymousClass020 A03 = new AnonymousClass020(null);
    public static final ReferenceQueue A04 = new ReferenceQueue();
    public static final AtomicInteger A05 = new AtomicInteger(0);
    public int A00;
    public int A01;
    public ServiceConnection A02;

    public static void A01(Context context, ServiceConnection serviceConnection, int i) {
        C006406k A002 = A00(serviceConnection, false);
        if (A002 == null) {
            context.unbindService(serviceConnection);
            return;
        }
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 16, 0, 0, i, 0, 0);
        A002.A02 = serviceConnection;
        A002.A01 = writeStandardEntry;
        A002.A00 = i;
        context.unbindService(A002);
    }

    private static C006406k A00(ServiceConnection serviceConnection, boolean z) {
        if (A05.incrementAndGet() >= 5 && Thread.currentThread().getId() != 1) {
            synchronized (A03) {
                while (true) {
                    try {
                        Reference poll = A04.poll();
                        if (poll == null) {
                            break;
                        }
                        ((AnonymousClass020) poll).A00();
                    } catch (Throwable th) {
                        while (true) {
                            th = th;
                            break;
                        }
                    }
                }
            }
            A05.set(0);
        }
        AnonymousClass020 r4 = A03;
        synchronized (r4) {
            try {
                AnonymousClass020 r2 = r4.A00;
                while (r2 != r4 && r2.get() != serviceConnection) {
                    r2 = r2.A00;
                }
                if (r2 != r4) {
                    AnonymousClass020 r1 = r2.A02;
                    if (r1 == r2 || r1.get() == null) {
                        r1.A00();
                        r2.A00();
                        C006406k A002 = A00(serviceConnection, z);
                        return A002;
                    }
                    C006406k r0 = (C006406k) r1.get();
                    return r0;
                } else if (!z) {
                    return null;
                } else {
                    AnonymousClass020 r3 = new AnonymousClass020(serviceConnection, A04);
                    C006406k r22 = new C006406k();
                    AnonymousClass020 r12 = new AnonymousClass020(r22);
                    AnonymousClass020 r02 = r3.A02;
                    r02.A01 = r12;
                    r12.A02 = r02;
                    r12.A01 = r3;
                    r3.A02 = r12;
                    AnonymousClass020 r03 = r4.A00;
                    r03.A03 = r3;
                    r3.A00 = r03;
                    r3.A03 = r4;
                    r4.A00 = r3;
                    return r22;
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public static boolean A02(Context context, Intent intent, ServiceConnection serviceConnection, int i, int i2) {
        if (!TraceEvents.isEnabled(AnonymousClass00n.A01)) {
            return context.bindService(intent, serviceConnection, i);
        }
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 16, 0, 0, i2, 0, 0);
        C006406k A002 = A00(serviceConnection, true);
        A002.A02 = serviceConnection;
        A002.A01 = writeStandardEntry;
        A002.A00 = i2;
        return context.bindService(intent, A002, i);
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 17, 0, 0, this.A00, this.A01, 0);
        this.A02.onServiceConnected(componentName, iBinder);
        Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 19, 0, 0, this.A00, writeStandardEntry, 0);
    }

    public void onServiceDisconnected(ComponentName componentName) {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 18, 0, 0, this.A00, this.A01, 0);
        this.A02.onServiceDisconnected(componentName);
        Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 19, 0, 0, this.A00, writeStandardEntry, 0);
    }

    public String toString() {
        return "ServiceConnectionDetour for " + this.A02;
    }

    private C006406k() {
    }
}
