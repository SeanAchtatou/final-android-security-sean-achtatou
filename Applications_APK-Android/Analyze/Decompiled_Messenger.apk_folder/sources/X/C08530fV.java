package X;

import com.facebook.common.util.TriState;

/* renamed from: X.0fV  reason: invalid class name and case insensitive filesystem */
public final class C08530fV {
    private AnonymousClass0Ud A00;
    private TriState A01 = TriState.UNSET;

    public TriState A00() {
        if (!this.A01.isSet()) {
            TriState A0C = this.A00.A0C();
            this.A01 = A0C;
            if (A0C == TriState.UNSET) {
                this.A01 = TriState.valueOf(AnonymousClass02L.A05.A01);
            }
        }
        return this.A01;
    }

    public C08530fV(AnonymousClass0Ud r2) {
        this.A00 = r2;
    }
}
