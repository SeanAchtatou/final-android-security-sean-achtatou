package X;

import android.database.Cursor;

/* renamed from: X.0rk  reason: invalid class name and case insensitive filesystem */
public final class C13620rk {
    private final C13610rj A00;

    public C13620rk(C13610rj r1) {
        this.A00 = r1;
    }

    public Cursor A00(C28381ei r13) {
        Object[] AxB = r13.AxB();
        if (AxB[5] != null) {
            return this.A00.Ab4().rawQuery((String) AxB[5], (String[]) AxB[6]);
        }
        return this.A00.Ab4().query((String) AxB[0], (String[]) AxB[1], (String) AxB[2], (String[]) AxB[3], null, null, (String) AxB[4]);
    }
}
