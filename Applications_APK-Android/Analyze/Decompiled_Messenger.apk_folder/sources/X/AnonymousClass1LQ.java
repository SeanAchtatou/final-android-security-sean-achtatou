package X;

/* renamed from: X.1LQ  reason: invalid class name */
public abstract class AnonymousClass1LQ extends AnonymousClass1LR {
    public final int A00;
    public final int A01;
    public final int[] A02;

    public int A02(int[] iArr, int i, int i2, CharSequence charSequence, int i3, int i4) {
        if (!(this instanceof AnonymousClass1LP)) {
            int i5 = -1;
            while (i3 < i4) {
                int codePointAt = Character.codePointAt(charSequence, i3);
                int A002 = C21941Jh.A00(iArr, i, i2, codePointAt);
                if (A002 >= 0) {
                    i3 += Character.charCount(codePointAt);
                    i = A002 >>> 16;
                    i2 = A002 & 65535;
                    if (iArr[i] == 0) {
                        i5 = i3;
                    }
                } else if (codePointAt == 65038) {
                    return -1;
                } else {
                    return i5;
                }
            }
            return i5;
        }
        AnonymousClass1LP r2 = (AnonymousClass1LP) this;
        int i6 = -1;
        while (i3 < i4) {
            int codePointAt2 = Character.codePointAt(charSequence, i3);
            int A04 = r2.A04(iArr, i, i2, codePointAt2);
            if (A04 < 0) {
                break;
            }
            i3 += Character.charCount(codePointAt2);
            i = A04 >>> 16;
            i2 = A04 & 65535;
            if (iArr[i] == 0) {
                i6 = i3;
            }
        }
        return i6;
    }

    public int A03(int[] iArr, int i, int i2, int[] iArr2, int i3, int i4) {
        if (!(this instanceof AnonymousClass1LP)) {
            int i5 = -1;
            while (i3 < i4) {
                int i6 = i3 + 1;
                int i7 = iArr2[i3];
                int A002 = C21941Jh.A00(iArr, i, i2, i7);
                if (A002 >= 0) {
                    i = A002 >>> 16;
                    i2 = 65535 & A002;
                    if (iArr[i] == 0) {
                        i5 = i6;
                    }
                    i3 = i6;
                } else if (i7 == 65038) {
                    return -1;
                } else {
                    return i5;
                }
            }
            return i5;
        }
        AnonymousClass1LP r3 = (AnonymousClass1LP) this;
        int i8 = -1;
        while (i3 < i4) {
            int i9 = i3 + 1;
            int A04 = r3.A04(iArr, i, i2, iArr2[i3]);
            if (A04 < 0) {
                break;
            }
            i = A04 >>> 16;
            i2 = A04 & 65535;
            if (iArr[i] == 0) {
                i8 = i9;
            }
            i3 = i9;
        }
        return i8;
    }

    public final int A01(CharSequence charSequence, int i, int i2) {
        return A02(this.A02, this.A00, this.A01, charSequence, i, i2);
    }

    public AnonymousClass1LQ(int[] iArr, int i, int i2) {
        this.A02 = iArr;
        this.A00 = i;
        this.A01 = i2;
    }
}
