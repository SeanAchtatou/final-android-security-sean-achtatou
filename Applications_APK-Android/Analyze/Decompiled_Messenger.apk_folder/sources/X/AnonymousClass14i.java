package X;

/* renamed from: X.14i  reason: invalid class name */
public final class AnonymousClass14i implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.sms.defaultapp.SmsDefaultAppManager$1";
    public final /* synthetic */ C185914h A00;

    public AnonymousClass14i(C185914h r1) {
        this.A00 = r1;
    }

    public void run() {
        for (Runnable run : this.A00.A06) {
            run.run();
        }
        this.A00.A06.clear();
    }
}
