package X;

import android.database.Cursor;
import java.util.Iterator;

/* renamed from: X.1fY  reason: invalid class name and case insensitive filesystem */
public final class C28901fY implements Iterable, AutoCloseable {
    private final Cursor A00;
    private final AnonymousClass12W A01;

    public void close() {
        this.A00.close();
    }

    public Iterator iterator() {
        return new C28911fZ(this.A00, this.A01);
    }

    public C28901fY(Cursor cursor, AnonymousClass12W r2) {
        this.A00 = cursor;
        this.A01 = r2;
    }
}
