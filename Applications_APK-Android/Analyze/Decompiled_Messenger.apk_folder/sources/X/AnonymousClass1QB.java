package X;

import java.util.concurrent.Executor;

/* renamed from: X.1QB  reason: invalid class name */
public final class AnonymousClass1QB implements AnonymousClass1Q3 {
    public final C30661iR A00;
    public final Executor A01;
    private final AnonymousClass1Q3 A02;
    private final C23351Pe A03;
    private final boolean A04;

    public void ByS(C23581Qb r8, AnonymousClass1QK r9) {
        this.A02.ByS(new AnonymousClass1SP(this, r8, r9, this.A04, this.A03), r9);
    }

    public AnonymousClass1QB(Executor executor, C30661iR r2, AnonymousClass1Q3 r3, boolean z, C23351Pe r5) {
        C05520Zg.A02(executor);
        this.A01 = executor;
        C05520Zg.A02(r2);
        this.A00 = r2;
        C05520Zg.A02(r3);
        this.A02 = r3;
        C05520Zg.A02(r5);
        this.A03 = r5;
        this.A04 = z;
    }
}
