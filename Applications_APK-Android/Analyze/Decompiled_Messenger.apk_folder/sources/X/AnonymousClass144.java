package X;

import android.content.Context;
import io.card.payment.BuildConfig;
import java.util.WeakHashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.144  reason: invalid class name */
public final class AnonymousClass144 {
    private static volatile AnonymousClass144 A01;
    public final WeakHashMap A00 = new WeakHashMap(4);

    public static final AnonymousClass144 A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass144.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new AnonymousClass144();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public String A01(Context context) {
        String str;
        synchronized (this.A00) {
            str = (String) this.A00.get(context);
            if (BuildConfig.FLAVOR.equals(str)) {
                str = C188215g.A00().toString();
                this.A00.put(context, str);
            }
        }
        return str;
    }
}
