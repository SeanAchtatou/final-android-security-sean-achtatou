package X;

/* renamed from: X.09A  reason: invalid class name */
public final class AnonymousClass09A implements AnonymousClass03a {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001a, code lost:
        if (com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite.A02 != false) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Bse() {
        /*
            r4 = this;
            r2 = 34359738368(0x800000000, double:1.69759663277E-313)
            boolean r0 = X.AnonymousClass08Z.A05(r2)
            if (r0 == 0) goto L_0x001f
            r1 = 0
            java.lang.String r0 = "CLASS_LOAD_TRACE"
            X.AnonymousClass08Z.A01(r2, r0, r1)
            r0 = 1
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite.A02 = r0
            r0 = 0
            if (r0 != 0) goto L_0x001c
            boolean r1 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite.A02
            r0 = 0
            if (r1 == 0) goto L_0x001d
        L_0x001c:
            r0 = 1
        L_0x001d:
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite.A01 = r0
        L_0x001f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass09A.Bse():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite.A02 != false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Bsf() {
        /*
            r5 = this;
            r2 = 34359738368(0x800000000, double:1.69759663277E-313)
            boolean r0 = X.AnonymousClass08Z.A05(r2)
            if (r0 == 0) goto L_0x001e
            r4 = 0
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite.A02 = r4
            r0 = 0
            if (r0 != 0) goto L_0x0016
            boolean r1 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite.A02
            r0 = 0
            if (r1 == 0) goto L_0x0017
        L_0x0016:
            r0 = 1
        L_0x0017:
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite.A01 = r0
            java.lang.String r0 = "CLASS_LOAD_TRACE"
            X.AnonymousClass08Z.A02(r2, r0, r4)
        L_0x001e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass09A.Bsf():void");
    }
}
