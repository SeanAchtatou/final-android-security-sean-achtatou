package X;

import android.os.AsyncTask;
import com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000;
import com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000;
import com.facebook.graphql.enums.GraphQLMontageDirectState;
import com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.model.messages.Message;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.concurrent.locks.Lock;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0mE  reason: invalid class name and case insensitive filesystem */
public final class C11180mE {
    private static volatile C11180mE A02;
    public AnonymousClass0UN A00;
    public final C11170mD A01;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setTime(java.lang.String, java.lang.Long):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setTime(int, java.lang.Long):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setTime(java.lang.String, java.lang.Long):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setTime(int, java.lang.Long):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setTime(int, java.lang.Long):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setTime(java.lang.String, java.lang.Long):com.facebook.graphservice.tree.TreeBuilderJNI */
    public static Message A00(Message message, long j, long j2) {
        C100354qi B46;
        C77163mm B5M;
        C61192yU r0 = message.A06;
        if (!(r0 == null || r0.B46() == null || message.A06.B46().B5M() == null || (B46 = message.A06.B46()) == null || (B5M = B46.B5M()) == null)) {
            GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MessagingParticipant", GSMBuilderShape0S0000000.class, 572556773);
            String valueOf = String.valueOf(j);
            gSMBuilderShape0S0000000.A0I(valueOf);
            GSMBuilderShape0S0000000 gSMBuilderShape0S00000002 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MontageDirectOpenInfo", GSMBuilderShape0S0000000.class, 39595093);
            gSMBuilderShape0S00000002.setTree("open_by", (GSTModelShape1S0000000) gSMBuilderShape0S0000000.getResult(GSTModelShape1S0000000.class, 572556773));
            gSMBuilderShape0S00000002.setTime("open_time", Long.valueOf(j2));
            GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) gSMBuilderShape0S00000002.getResult(GSTModelShape1S0000000.class, 39595093);
            ImmutableList.Builder builder = ImmutableList.builder();
            C24971Xv it = B5M.A0g().iterator();
            while (it.hasNext()) {
                GSTModelShape1S0000000 gSTModelShape1S00000002 = (GSTModelShape1S0000000) it.next();
                GSTModelShape1S0000000 A21 = gSTModelShape1S00000002.A21();
                if (A21 != null) {
                    String A3m = A21.A3m();
                    if (A3m == null || !A3m.equals(valueOf)) {
                        long timeValue = gSTModelShape1S00000002.getTimeValue(1546339234);
                        GSMBuilderShape0S0000000 gSMBuilderShape0S00000003 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MessagingParticipant", GSMBuilderShape0S0000000.class, 572556773);
                        gSMBuilderShape0S00000003.A0I(A3m);
                        GSMBuilderShape0S0000000 gSMBuilderShape0S00000004 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MontageDirectOpenInfo", GSMBuilderShape0S0000000.class, 39595093);
                        gSMBuilderShape0S00000004.setTree("open_by", (GSTModelShape1S0000000) gSMBuilderShape0S00000003.getResult(GSTModelShape1S0000000.class, 572556773));
                        gSMBuilderShape0S00000004.setTime("open_time", Long.valueOf(timeValue));
                        builder.add((Object) ((GSTModelShape1S0000000) gSMBuilderShape0S00000004.getResult(GSTModelShape1S0000000.class, 39595093)));
                    }
                }
            }
            builder.add((Object) gSTModelShape1S0000000);
            GSMBuilderShape0S0000000 A002 = C77163mm.A00(B5M, C05850aR.A02());
            Preconditions.checkNotNull(A002);
            A002.setTreeList("opens", builder.build());
            C77163mm A08 = A002.A08();
            GSMBuilderShape0S0000000 A012 = C100354qi.A01(B46, C05850aR.A02());
            Preconditions.checkNotNull(A012);
            A012.setTree("target", A08);
            C100354qi A09 = A012.A09();
            GSMBuilderShape0S0000000 A013 = C58012t1.A01(message.A06, C05850aR.A02());
            Preconditions.checkNotNull(A013);
            A013.setTree(C99084oO.$const$string(86), A09);
            C58012t1 A0A = A013.A0A();
            AnonymousClass1TG A003 = Message.A00();
            A003.A03(message);
            A003.A01(A0A);
            return A003.A00();
        }
        return message;
    }

    public static Message A01(Message message, GraphQLMontageDirectState graphQLMontageDirectState) {
        C100354qi B46;
        C61192yU r0 = message.A06;
        if (r0 == null || r0.B46() == null || message.A06.B46().B5M() == null || (B46 = message.A06.B46()) == null) {
            return message;
        }
        GSMBuilderShape0S0000000 A002 = C77163mm.A00(B46.B5M(), C05850aR.A02());
        Preconditions.checkNotNull(A002);
        A002.A01(AnonymousClass80H.$const$string(165), graphQLMontageDirectState);
        C77163mm A08 = A002.A08();
        GSMBuilderShape0S0000000 A012 = C100354qi.A01(B46, C05850aR.A02());
        Preconditions.checkNotNull(A012);
        A012.setTree("target", A08);
        C100354qi A09 = A012.A09();
        GSMBuilderShape0S0000000 A013 = C58012t1.A01(message.A06, C05850aR.A02());
        Preconditions.checkNotNull(A013);
        A013.setTree(C99084oO.$const$string(86), A09);
        C58012t1 A0A = A013.A0A();
        AnonymousClass1TG A003 = Message.A00();
        A003.A03(message);
        A003.A01(A0A);
        return A003.A00();
    }

    public static final C11180mE A02(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C11180mE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C11180mE(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    /* JADX INFO: finally extract failed */
    public void A03(Message message, boolean z, String str) {
        if (z) {
            str = "sender_auto_expire";
        }
        GQLCallInputCInputShape1S0000000 gQLCallInputCInputShape1S0000000 = new GQLCallInputCInputShape1S0000000(AnonymousClass1Y3.A1F);
        gQLCallInputCInputShape1S0000000.A09("message_id", message.A0q);
        GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S0000000 = new GQLCallInputCInputShape0S0000000(91);
        gQLCallInputCInputShape0S0000000.A09("tag_key", "entry_point");
        gQLCallInputCInputShape0S0000000.A09("tag_value", str);
        gQLCallInputCInputShape1S0000000.A0A("client_tags", ImmutableList.of(gQLCallInputCInputShape0S0000000));
        gQLCallInputCInputShape1S0000000.A09("action", "OPEN");
        C82433vy r1 = new C82433vy();
        r1.A04("input", gQLCallInputCInputShape1S0000000);
        new AnonymousClass48p(this, C26931cN.A01(r1)).A01(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        C69613Xy r12 = (C69613Xy) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1T, this.A00);
        String str2 = message.A0w;
        Lock writeLock = r12.A02.writeLock();
        writeLock.lock();
        try {
            r12.A00.put(str2, C117385hJ.A01);
            writeLock.unlock();
            C117495hU r3 = (C117495hU) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Ae7, this.A00);
            String str3 = message.A0q;
            Lock writeLock2 = r3.A01.writeLock();
            writeLock2.lock();
            try {
                r3.A00.remove(str3);
            } finally {
                writeLock2.unlock();
            }
        } catch (Throwable th) {
            writeLock.unlock();
            throw th;
        }
    }

    private C11180mE(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A01 = C11170mD.A00(r3);
    }
}
