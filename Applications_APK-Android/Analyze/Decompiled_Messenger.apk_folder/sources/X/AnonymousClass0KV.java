package X;

import android.os.StrictMode;
import android.os.strictmode.Violation;

/* renamed from: X.0KV  reason: invalid class name */
public final class AnonymousClass0KV implements StrictMode.OnVmViolationListener {
    public void onVmViolation(Violation violation) {
        synchronized (AnonymousClass00O.class) {
        }
        synchronized (AnonymousClass00O.class) {
            if (AnonymousClass00O.A01.size() < 10) {
                AnonymousClass00O.A01.addLast(violation);
            }
        }
    }
}
