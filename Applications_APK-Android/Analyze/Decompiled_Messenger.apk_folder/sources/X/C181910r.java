package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.facebook.litho.ComponentHost;
import com.facebook.litho.LithoView;
import java.util.List;

/* renamed from: X.10r  reason: invalid class name and case insensitive filesystem */
public final class C181910r implements AnonymousClass1M8 {
    public void C3Y(Object obj) {
    }

    public void C5f(Object obj, float f) {
        Object obj2 = obj;
        if (obj instanceof ComponentHost) {
            ComponentHost componentHost = (ComponentHost) obj2;
            if (componentHost instanceof LithoView) {
                LithoView lithoView = (LithoView) componentHost;
                lithoView.A01 = (int) f;
                lithoView.requestLayout();
            } else {
                int left = componentHost.getLeft();
                AnonymousClass1LA.A01(componentHost, left, componentHost.getTop(), (int) (((float) left) + f), componentHost.getBottom(), false);
            }
            List A0E = componentHost.A0E();
            if (A0E != null) {
                int i = (int) f;
                int height = componentHost.getHeight();
                for (int i2 = 0; i2 < A0E.size(); i2++) {
                    AnonymousClass1LA.A00((Drawable) A0E.get(i2), i, height);
                }
            }
        } else if (obj instanceof View) {
            View view = (View) obj2;
            int left2 = view.getLeft();
            AnonymousClass1LA.A01(view, left2, view.getTop(), (int) (((float) left2) + f), view.getBottom(), false);
        } else if (obj instanceof Drawable) {
            Drawable drawable = (Drawable) obj2;
            AnonymousClass1LA.A00(drawable, (int) f, drawable.getBounds().height());
        } else {
            throw new UnsupportedOperationException("Setting width on unsupported mount content: " + obj);
        }
    }

    public String getName() {
        return "width";
    }

    public float Ab1(C17730zN r2) {
        return (float) r2.A08.width();
    }

    public float Ab2(Object obj) {
        int width;
        if (obj instanceof View) {
            width = ((View) obj).getWidth();
        } else if (obj instanceof Drawable) {
            width = ((Drawable) obj).getBounds().width();
        } else {
            throw new UnsupportedOperationException("Getting width from unsupported mount content: " + obj);
        }
        return (float) width;
    }
}
