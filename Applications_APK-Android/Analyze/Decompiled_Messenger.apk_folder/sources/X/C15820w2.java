package X;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

/* renamed from: X.0w2  reason: invalid class name and case insensitive filesystem */
public final class C15820w2 {
    public static final C15820w2 A04;
    public final C34801qC A00;
    public final ImmutableList A01;
    public final ImmutableList A02;
    public final boolean A03;

    static {
        C34801qC r2 = C34801qC.A03;
        ImmutableList immutableList = RegularImmutableList.A02;
        A04 = new C15820w2(true, r2, immutableList, immutableList);
    }

    public C15820w2(boolean z, C34801qC r2, ImmutableList immutableList, ImmutableList immutableList2) {
        this.A03 = z;
        this.A00 = r2 == null ? C34801qC.A03 : r2;
        this.A01 = immutableList;
        this.A02 = immutableList2;
    }
}
