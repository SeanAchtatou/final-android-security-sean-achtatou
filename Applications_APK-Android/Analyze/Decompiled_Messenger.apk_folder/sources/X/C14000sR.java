package X;

import android.util.SparseArray;
import com.facebook.quicklog.PerformanceLoggingEvent;

/* renamed from: X.0sR  reason: invalid class name and case insensitive filesystem */
public final class C14000sR implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.quicklog.QuickPerformanceLoggerImpl$3";
    public final /* synthetic */ PerformanceLoggingEvent A00;
    public final /* synthetic */ C08520fU A01;

    public C14000sR(C08520fU r1, PerformanceLoggingEvent performanceLoggingEvent) {
        this.A01 = r1;
        this.A00 = performanceLoggingEvent;
    }

    public void run() {
        C08360fE AuU;
        Object obj;
        Object obj2;
        C08360fE AuU2;
        C08520fU r10 = this.A01;
        PerformanceLoggingEvent performanceLoggingEvent = this.A00;
        if (!(r10.mEventDecorators == null || performanceLoggingEvent.A0A == 0)) {
            C12210op r0 = r10.A07;
            if (r0 == null) {
                AuU2 = null;
            } else {
                AuU2 = r0.AuU();
            }
            for (C08560fY r5 : r10.mEventDecorators) {
                if ((performanceLoggingEvent.A0A & r5.BL4()) > 0 && (AuU2 == null || r5.BEZ(AuU2))) {
                    performanceLoggingEvent.A03(r5.AWk());
                    r5.BjZ(performanceLoggingEvent);
                }
            }
        }
        if (!(r10.mDataProviders == null || performanceLoggingEvent.A0A == 0)) {
            C12210op r02 = r10.A07;
            if (r02 == null) {
                AuU = null;
            } else {
                AuU = r02.AuU();
            }
            for (C08230et r3 : r10.mDataProviders) {
                if ((performanceLoggingEvent.A0A & r3.Azh()) > 0 && (AuU == null || r3.BEZ(AuU))) {
                    int numberOfTrailingZeros = Long.numberOfTrailingZeros(r3.Azh());
                    SparseArray sparseArray = performanceLoggingEvent.A0D;
                    if (sparseArray != null) {
                        obj = sparseArray.get(numberOfTrailingZeros);
                    } else {
                        obj = null;
                    }
                    SparseArray sparseArray2 = performanceLoggingEvent.A0E;
                    if (sparseArray2 != null) {
                        obj2 = sparseArray2.get(numberOfTrailingZeros);
                    } else {
                        obj2 = null;
                    }
                    performanceLoggingEvent.A03(r3.Azg());
                    r3.AWj(performanceLoggingEvent, r3.B3e().cast(obj), r3.B3O().cast(obj2));
                }
            }
        }
        C26601bi r9 = r10.A0G.A03;
        long A02 = r9.A02(performanceLoggingEvent.A06);
        if (A02 != 0 && r9.A03 != null) {
            long j = 1;
            int i = 0;
            while (true) {
                AnonymousClass0Td[] r32 = r9.A03;
                if (i >= r32.length) {
                    break;
                }
                if ((A02 & j) != 0) {
                    r32[i].BfD(performanceLoggingEvent);
                }
                i++;
                j <<= 1;
            }
        }
        C26121aw[] r33 = r10.A0D;
        if (r33 != null) {
            for (C26121aw CMc : r33) {
                CMc.CMc(performanceLoggingEvent);
            }
        }
        C08520fU.A06(this.A01, this.A00);
    }
}
