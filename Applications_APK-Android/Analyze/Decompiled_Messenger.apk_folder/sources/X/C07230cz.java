package X;

/* renamed from: X.0cz  reason: invalid class name and case insensitive filesystem */
public final class C07230cz {
    public static String A01(int i) {
        if (i == 0) {
            return "UNKNOWN";
        }
        if (i == 1) {
            return "FEMALE";
        }
        if (i == 2) {
            return "MALE";
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0J("Out of bounds gender ", String.valueOf(i)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if (r5.equals("UNKNOWN") == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0038, code lost:
        if (r5.equals("MALE") == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001d, code lost:
        if (r5.equals("FEMALE") == false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.String r5) {
        /*
            int r1 = r5.hashCode()
            r0 = 2358797(0x23fe0d, float:3.305379E-39)
            r4 = 0
            r3 = 2
            r2 = 1
            if (r1 == r0) goto L_0x0031
            r0 = 433141802(0x19d1382a, float:2.1632778E-23)
            if (r1 == r0) goto L_0x0027
            r0 = 2070122316(0x7b638f4c, float:1.1815578E36)
            if (r1 != r0) goto L_0x001f
            java.lang.String r0 = "FEMALE"
            boolean r0 = r5.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0020
        L_0x001f:
            r1 = -1
        L_0x0020:
            if (r1 == 0) goto L_0x004a
            if (r1 == r2) goto L_0x0049
            if (r1 != r3) goto L_0x003b
            return r3
        L_0x0027:
            java.lang.String r0 = "UNKNOWN"
            boolean r0 = r5.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0020
            goto L_0x001f
        L_0x0031:
            java.lang.String r0 = "MALE"
            boolean r0 = r5.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0020
            goto L_0x001f
        L_0x003b:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Unrecognized gender '"
            java.lang.String r0 = "'"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r5, r0)
            r2.<init>(r0)
            throw r2
        L_0x0049:
            return r2
        L_0x004a:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07230cz.A00(java.lang.String):int");
    }
}
