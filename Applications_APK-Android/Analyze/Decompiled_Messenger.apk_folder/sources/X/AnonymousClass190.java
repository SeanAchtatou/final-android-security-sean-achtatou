package X;

/* renamed from: X.190  reason: invalid class name */
public final class AnonymousClass190 implements AnonymousClass0Z1 {
    public final /* synthetic */ C09350h5 A00;

    public String Amj() {
        return "last_launcher_intent_ms";
    }

    public AnonymousClass190(C09350h5 r1) {
        this.A00 = r1;
    }

    public String getCustomData(Throwable th) {
        AnonymousClass0Ud r4 = this.A00.A01;
        return Long.toString(((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, r4.A02)).now() - r4.A0G);
    }
}
