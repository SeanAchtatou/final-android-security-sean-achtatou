package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.01y  reason: invalid class name and case insensitive filesystem */
public class C003201y extends AnonymousClass02E {
    public final File A00;
    public final String A01;

    public AnonymousClass0EL A0D() {
        return new AnonymousClass0ES(this, this);
    }

    public C003201y(Context context, String str, File file, String str2) {
        super(context, str);
        this.A00 = file;
        this.A01 = str2;
    }
}
