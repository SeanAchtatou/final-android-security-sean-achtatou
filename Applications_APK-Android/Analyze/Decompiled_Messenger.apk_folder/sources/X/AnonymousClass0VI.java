package X;

import android.os.Handler;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0VI  reason: invalid class name */
public class AnonymousClass0VI extends AbstractExecutorService implements AnonymousClass0VJ {
    public final Handler A00;

    public Runnable A01(Runnable runnable) {
        return runnable;
    }

    /* renamed from: C4Y */
    public AnonymousClass0Y0 schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        C08960gG r5 = new C08960gG(getHandler(), runnable, null);
        AnonymousClass00S.A05(this.A00, A01(r5), timeUnit.toMillis(j), -193045406);
        return r5;
    }

    public /* bridge */ /* synthetic */ ListenableFuture CIC(Runnable runnable) {
        return submit(runnable, null);
    }

    public boolean isShutdown() {
        return false;
    }

    public boolean isTerminated() {
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: A00 */
    public AnonymousClass0Y0 submit(Runnable runnable, Object obj) {
        if (runnable != null) {
            C08960gG r1 = new C08960gG(getHandler(), runnable, obj);
            AnonymousClass07A.A04(this, r1, -944956636);
            return r1;
        }
        throw new NullPointerException();
    }

    /* renamed from: C4Z */
    public AnonymousClass0Y0 schedule(Callable callable, long j, TimeUnit timeUnit) {
        C08960gG r5 = new C08960gG(getHandler(), callable);
        AnonymousClass00S.A05(this.A00, A01(r5), timeUnit.toMillis(j), 225367384);
        return r5;
    }

    public /* bridge */ /* synthetic */ ListenableFuture CIF(Callable callable) {
        if (callable != null) {
            C08960gG r1 = new C08960gG(getHandler(), callable);
            AnonymousClass07A.A04(this, r1, -162035655);
            return r1;
        }
        throw new NullPointerException();
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void execute(Runnable runnable) {
        if ((runnable instanceof Future) && !(runnable instanceof C08950gF)) {
            Class<?> cls = getClass();
            C010708t.A0C(cls, "%s submitted as runnable to %s. Potential deadlocks on get().", runnable.getClass(), cls);
        }
        AnonymousClass00S.A04(this.A00, A01(runnable), 547196587);
    }

    public Handler getHandler() {
        return this.A00;
    }

    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void shutdown() {
        throw new UnsupportedOperationException();
    }

    public List shutdownNow() {
        throw new UnsupportedOperationException();
    }

    public AnonymousClass0VI(Handler handler) {
        this.A00 = handler;
    }

    public RunnableFuture newTaskFor(Runnable runnable, Object obj) {
        return new C08960gG(getHandler(), runnable, obj);
    }

    public RunnableFuture newTaskFor(Callable callable) {
        return new C08960gG(getHandler(), callable);
    }

    public /* bridge */ /* synthetic */ Future submit(Runnable runnable) {
        return submit(runnable, null);
    }

    public /* bridge */ /* synthetic */ Future submit(Callable callable) {
        if (callable != null) {
            C08960gG r1 = new C08960gG(getHandler(), callable);
            AnonymousClass07A.A04(this, r1, -162035655);
            return r1;
        }
        throw new NullPointerException();
    }
}
