package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.16Y  reason: invalid class name */
public final class AnonymousClass16Y extends Enum {
    private static final /* synthetic */ AnonymousClass16Y[] A00;
    public static final AnonymousClass16Y A01;
    public static final AnonymousClass16Y A02;
    public static final AnonymousClass16Y A03;
    public static final AnonymousClass16Y A04;
    public static final AnonymousClass16Y A05;
    public static final AnonymousClass16Y A06;
    public static final AnonymousClass16Y A07;
    public static final AnonymousClass16Y A08;
    public static final AnonymousClass16Y A09;
    public static final AnonymousClass16Y A0A;
    public static final AnonymousClass16Y A0B;
    public static final AnonymousClass16Y A0C;
    public static final AnonymousClass16Y A0D;
    public static final AnonymousClass16Y A0E;
    public static final AnonymousClass16Y A0F;
    public static final AnonymousClass16Y A0G;
    public static final AnonymousClass16Y A0H;
    public static final AnonymousClass16Y A0I;

    static {
        AnonymousClass16Y r15 = new AnonymousClass16Y("TOP_FRIENDS", 0);
        A0E = r15;
        AnonymousClass16Y r14 = new AnonymousClass16Y("ONLINE_FRIENDS", 1);
        A06 = r14;
        AnonymousClass16Y r13 = new AnonymousClass16Y("ONLINE_FRIENDS_ACTIVE_NOW_SCORE_SORTED", 2);
        A07 = r13;
        AnonymousClass16Y r12 = new AnonymousClass16Y("FRIENDS_ON_MESSENGER", 3);
        A04 = r12;
        AnonymousClass16Y r11 = new AnonymousClass16Y("TOP_FRIENDS_ON_MESSENGER", 4);
        A0F = r11;
        AnonymousClass16Y r10 = new AnonymousClass16Y("NOT_ON_MESSENGER_FRIENDS", 5);
        A05 = r10;
        AnonymousClass16Y r9 = new AnonymousClass16Y("PHAT_CONTACTS", 6);
        A08 = r9;
        AnonymousClass16Y r8 = new AnonymousClass16Y(C99084oO.$const$string(AnonymousClass1Y3.A2u), 7);
        A0D = r8;
        AnonymousClass16Y r7 = new AnonymousClass16Y("TOP_RTC_CONTACTS", 8);
        A0I = r7;
        AnonymousClass16Y r6 = new AnonymousClass16Y("SMS_INVITE_ALL_PHONE_CONTACTS", 9);
        A09 = r6;
        AnonymousClass16Y r5 = new AnonymousClass16Y("SMS_INVITE_MOBILE_CONTACTS", 10);
        A0A = r5;
        AnonymousClass16Y r4 = new AnonymousClass16Y("TOP_PHONE_CONTACTS", 11);
        A0G = r4;
        AnonymousClass16Y r3 = new AnonymousClass16Y("TOP_PHONE_CONTACTS_NULL_STATE", 12);
        A0H = r3;
        AnonymousClass16Y r2 = new AnonymousClass16Y("ALL_CONTACTS_WITH_CAP", 13);
        A02 = r2;
        AnonymousClass16Y r16 = new AnonymousClass16Y("ALL_CONTACTS", 14);
        A01 = r16;
        AnonymousClass16Y r20 = new AnonymousClass16Y("SPECIFIC_USERS", 15);
        A0B = r20;
        AnonymousClass16Y r202 = new AnonymousClass16Y("FAVORITE_MESSENGER_CONTACTS", 16);
        A03 = r202;
        AnonymousClass16Y r203 = new AnonymousClass16Y("TOP_BLOCKABLE_CONTACTS", 17);
        A0C = r203;
        AnonymousClass16Y r29 = r16;
        AnonymousClass16Y r30 = r20;
        AnonymousClass16Y r31 = r202;
        AnonymousClass16Y r32 = r203;
        AnonymousClass16Y r17 = r13;
        AnonymousClass16Y r18 = r12;
        AnonymousClass16Y r19 = r11;
        AnonymousClass16Y r204 = r10;
        AnonymousClass16Y r162 = r14;
        A00 = new AnonymousClass16Y[]{r15, r162, r17, r18, r19, r204, r9, r8, r7, r6, r5, r4, r3, r2, r29, r30, r31, r32};
    }

    public static AnonymousClass16Y valueOf(String str) {
        return (AnonymousClass16Y) Enum.valueOf(AnonymousClass16Y.class, str);
    }

    public static AnonymousClass16Y[] values() {
        return (AnonymousClass16Y[]) A00.clone();
    }

    private AnonymousClass16Y(String str, int i) {
    }
}
