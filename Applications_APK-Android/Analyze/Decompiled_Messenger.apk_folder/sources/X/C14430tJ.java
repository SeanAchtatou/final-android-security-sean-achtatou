package X;

import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.QueueIdentifier;
import java.nio.ByteBuffer;
import java.util.List;

/* renamed from: X.0tJ  reason: invalid class name and case insensitive filesystem */
public interface C14430tJ {
    IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer);

    void BVs(List list);

    void Boz(int i);

    void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier);

    void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier);
}
