package X;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.facebook.inject.ContextScoped;
import com.google.common.base.Preconditions;

@ContextScoped
/* renamed from: X.0dg  reason: invalid class name and case insensitive filesystem */
public final class C07510dg extends C04450Us {
    private static C04470Uu A01;
    public final Context A00;

    public static final C07510dg A00(AnonymousClass1XY r5) {
        C07510dg r0;
        synchronized (C07510dg.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A01.A01();
                    A01.A00 = new C07510dg(AnonymousClass1YA.A00(r02), C04430Uq.A00(r02));
                }
                C04470Uu r1 = A01;
                r0 = (C07510dg) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public void C4x(Intent intent) {
        this.A00.sendBroadcast(intent, null);
    }

    private C07510dg(Context context, Handler handler) {
        super(handler);
        Preconditions.checkNotNull(context);
        this.A00 = context;
    }
}
