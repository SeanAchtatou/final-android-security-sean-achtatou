package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import java.util.ArrayList;

/* renamed from: X.0H5  reason: invalid class name */
public final class AnonymousClass0H5 extends ArrayList<SubscribeTopic> {
    public AnonymousClass0H5() {
        add(new SubscribeTopic("/fbns_msg", 1));
    }
}
