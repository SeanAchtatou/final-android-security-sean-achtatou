package X;

/* renamed from: X.0A1  reason: invalid class name */
public final class AnonymousClass0A1 {
    public static void A00(Object obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
    }

    public static void A01(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    public static void A02(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }
}
