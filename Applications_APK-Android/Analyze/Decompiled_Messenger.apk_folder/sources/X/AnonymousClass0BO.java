package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0BO  reason: invalid class name */
public final class AnonymousClass0BO {
    public static final String A0M = AnonymousClass08S.A0J("KeepaliveManager", ".ACTION_INEXACT_ALARM.");
    public static final List A0N = Collections.unmodifiableList(new AnonymousClass0BP());
    private static final String A0O = AnonymousClass08S.A0J("KeepaliveManager", ".ACTION_BACKUP_ALARM.");
    private static final String A0P = AnonymousClass08S.A0J("KeepaliveManager", ".ACTION_EXACT_ALARM.");
    public long A00;
    public long A01;
    public long A02 = -1;
    public boolean A03;
    public final int A04;
    public final AlarmManager A05;
    public final PendingIntent A06;
    public final PendingIntent A07;
    public final PendingIntent A08;
    public final BroadcastReceiver A09;
    public final BroadcastReceiver A0A;
    public final BroadcastReceiver A0B;
    public final Context A0C;
    public final Handler A0D;
    public final C009207y A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final boolean A0I;
    private final String A0J;
    private final AtomicInteger A0K;
    public volatile Runnable A0L;

    public synchronized void A03() {
        if (this.A03) {
            this.A03 = false;
            this.A0E.A05(this.A05, this.A08);
            if (!this.A0I) {
                this.A0E.A05(this.A05, this.A06);
            }
            this.A0E.A05(this.A05, this.A07);
        }
        this.A00 = 900000;
        this.A02 = -1;
    }

    public synchronized void A04() {
        long j = (long) (this.A0K.get() * AnonymousClass1Y3.A87);
        if (j > 900000) {
            boolean z = false;
            if (j >= 900000) {
                z = true;
            }
            AnonymousClass0A1.A01(z);
            Iterator it = A0N.iterator();
            while (true) {
                if (!it.hasNext()) {
                    j = 900000;
                    break;
                }
                long longValue = ((Long) it.next()).longValue();
                if (j >= longValue) {
                    j = longValue;
                    break;
                }
            }
        }
        this.A00 = j;
        this.A01 = SystemClock.elapsedRealtime() + j;
        if (this.A03) {
            this.A0E.A05(this.A05, this.A07);
            if (!this.A0I) {
                this.A0E.A05(this.A05, this.A06);
            }
        } else {
            this.A03 = true;
        }
        try {
            long j2 = this.A00;
            if (j2 < 900000) {
                long j3 = this.A01;
                int i = this.A04;
                if (i >= 23 && this.A0I) {
                    this.A0E.A04(this.A05, 2, j3, this.A07);
                } else if (i >= 19) {
                    this.A0E.A02(this.A05, 2, j3, this.A07);
                } else {
                    this.A05.setRepeating(2, j3, j2, this.A07);
                }
            } else {
                if (this.A02 != j2) {
                    this.A02 = j2;
                    this.A0E.A05(this.A05, this.A08);
                    A02(this, this.A01, this.A00);
                }
                if (!this.A0I) {
                    A01(this, this.A01 + 20000);
                }
            }
        } catch (Throwable unused) {
        }
    }

    private String A00(String str, Context context) {
        StringBuilder sb = new StringBuilder(str);
        sb.append(this.A0J);
        String packageName = context.getPackageName();
        if (!TextUtils.isEmpty(packageName)) {
            sb.append('.');
            sb.append(packageName);
        }
        return sb.toString();
    }

    public static void A01(AnonymousClass0BO r5, long j) {
        int i = r5.A04;
        long j2 = j;
        if (i >= 23 && r5.A0I) {
            r5.A0E.A04(r5.A05, 2, j2, r5.A06);
        } else if (i >= 19) {
            r5.A0E.A02(r5.A05, 2, j2, r5.A06);
        } else {
            r5.A05.set(2, j, r5.A06);
        }
    }

    public static void A02(AnonymousClass0BO r5, long j, long j2) {
        long j3 = j;
        if (r5.A04 < 23 || !r5.A0I) {
            r5.A05.setInexactRepeating(2, j3, j2, r5.A08);
            return;
        }
        r5.A0E.A03(r5.A05, 2, j3, r5.A08);
    }

    public AnonymousClass0BO(Context context, C01420Ad r6, String str, AtomicInteger atomicInteger, Handler handler, C009207y r10) {
        this.A0C = context;
        this.A0J = str;
        this.A0I = AnonymousClass0A9.A01(context.getPackageName());
        this.A0K = atomicInteger;
        C01540Aq A002 = r6.A00("alarm", AlarmManager.class);
        if (A002.A02()) {
            this.A05 = (AlarmManager) A002.A01();
            this.A04 = Build.VERSION.SDK_INT;
            this.A0D = handler;
            this.A0E = r10;
            this.A0A = new AnonymousClass0BQ(this);
            String A003 = A00(A0P, context);
            this.A0G = A003;
            Intent intent = new Intent(A003);
            intent.setPackage(context.getPackageName());
            this.A07 = PendingIntent.getBroadcast(context, 0, intent, 134217728);
            this.A0B = new AnonymousClass0BR(this);
            String A004 = A00(A0M, context);
            this.A0H = A004;
            Intent intent2 = new Intent(A004);
            intent2.setPackage(context.getPackageName());
            this.A08 = PendingIntent.getBroadcast(context, 0, intent2, 134217728);
            this.A09 = new AnonymousClass0BS(this);
            String A005 = A00(A0O, context);
            this.A0F = A005;
            Intent intent3 = new Intent(A005);
            intent3.setPackage(context.getPackageName());
            this.A06 = PendingIntent.getBroadcast(context, 0, intent3, 134217728);
            return;
        }
        throw new IllegalArgumentException("Cannot acquire Alarm service");
    }
}
