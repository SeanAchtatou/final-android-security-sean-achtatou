package X;

import java.io.Serializable;
import java.util.Arrays;

/* renamed from: X.1uS  reason: invalid class name and case insensitive filesystem */
public final class C37001uS implements C36221si, Serializable, Cloneable {
    private static final C36241sk A00 = new C36241sk("traceInfo", (byte) 11, 1);
    private static final C36231sj A01 = new C36231sj("MqttThriftHeader");
    public final String traceInfo;

    public boolean equals(Object obj) {
        C37001uS r5;
        if (obj == null || !(obj instanceof C37001uS) || (r5 = (C37001uS) obj) == null) {
            return false;
        }
        if (this == r5) {
            return true;
        }
        String str = this.traceInfo;
        boolean z = false;
        if (str != null) {
            z = true;
        }
        String str2 = r5.traceInfo;
        boolean z2 = false;
        if (str2 != null) {
            z2 = true;
        }
        if (!z && !z2) {
            return true;
        }
        if (!z || !z2 || !str.equals(str2)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return CJ9(1, true);
    }

    public void CNX(C35781ro r3) {
        r3.A0i(A01);
        String str = this.traceInfo;
        if (str != null) {
            boolean z = false;
            if (str != null) {
                z = true;
            }
            if (z) {
                r3.A0e(A00);
                r3.A0j(this.traceInfo);
                r3.A0S();
            }
        }
        r3.A0T();
        r3.A0X();
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{this.traceInfo});
    }

    public C37001uS(String str) {
        this.traceInfo = str;
    }

    public static C37001uS A00(C35781ro r4) {
        r4.A0Q();
        String str = null;
        while (true) {
            C36241sk A0F = r4.A0F();
            byte b = A0F.A00;
            if (b == 0) {
                r4.A0R();
                return new C37001uS(str);
            }
            if (A0F.A02 == 1 && b == 11) {
                str = r4.A0K();
            } else {
                C74173hO.A00(r4, b);
            }
            r4.A0M();
        }
    }

    public String CJ9(int i, boolean z) {
        return B36.A06(this, i, z);
    }
}
