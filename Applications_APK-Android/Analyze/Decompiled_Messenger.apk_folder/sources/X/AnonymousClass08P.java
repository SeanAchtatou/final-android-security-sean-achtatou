package X;

import java.util.Iterator;
import java.util.Set;

/* renamed from: X.08P  reason: invalid class name */
public final class AnonymousClass08P {
    public static long A00(Set set) {
        Iterator it = set.iterator();
        long j = 0;
        while (it.hasNext()) {
            j |= (long) (1 << ((Enum) it.next()).ordinal());
        }
        return j;
    }
}
