package X;

import java.util.concurrent.Executor;

/* renamed from: X.1Ym  reason: invalid class name and case insensitive filesystem */
public enum C25141Ym implements Executor {
    INSTANCE;

    public String toString() {
        return "MoreExecutors.directExecutor()";
    }

    public void execute(Runnable runnable) {
        runnable.run();
    }
}
