package X;

/* renamed from: X.01u  reason: invalid class name */
public final class AnonymousClass01u {
    private static C003001w A00;

    public static synchronized void A00(C003001w r3) {
        synchronized (AnonymousClass01u.class) {
            if (A00 == null) {
                A00 = r3;
            } else {
                throw new IllegalStateException("Cannot re-initialize NativeLoader.");
            }
        }
    }

    public static void A01(String str) {
        synchronized (AnonymousClass01u.class) {
            if (A00 == null) {
                throw new IllegalStateException("NativeLoader has not been initialized.  To use standard native library loading, call NativeLoader.init(new SystemDelegate()).");
            }
        }
        A00.BIQ(str);
    }

    public static synchronized boolean A02() {
        boolean z;
        synchronized (AnonymousClass01u.class) {
            z = false;
            if (A00 != null) {
                z = true;
            }
        }
        return z;
    }

    private AnonymousClass01u() {
    }
}
