package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.mig.scheme.schemes.LightColorScheme;

/* renamed from: X.1vU  reason: invalid class name and case insensitive filesystem */
public final class C37341vU extends C17770zR {
    public static final MigColorScheme A03 = LightColorScheme.A00();
    @Comparable(type = 13)
    public C121605oW A00;
    @Comparable(type = 13)
    public MigColorScheme A01 = A03;
    @Comparable(type = 13)
    public CharSequence A02;

    public C37341vU() {
        super("TitleComponent");
    }
}
