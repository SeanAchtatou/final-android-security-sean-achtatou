package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.13K  reason: invalid class name */
public final class AnonymousClass13K implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0WP A00;
    public final /* synthetic */ AnonymousClass13I A01;

    public AnonymousClass13K(AnonymousClass13I r1, AnonymousClass0WP r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(811824996);
        this.A01.A01(this.A00);
        AnonymousClass09Y.A01(1078134167, A002);
    }
}
