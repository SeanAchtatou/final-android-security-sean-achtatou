package X;

/* renamed from: X.098  reason: invalid class name */
public final class AnonymousClass098 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.appstatelogger.foregroundstate.ForegroundEntityMapper$1";
    public final /* synthetic */ AnonymousClass02G A00;

    public AnonymousClass098(AnonymousClass02G r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x002f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r4 = this;
            java.io.File r3 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            X.02G r0 = r4.A00
            java.io.File r0 = r0.A02
            r1.append(r0)
            java.lang.String r0 = "_entity"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r3.<init>(r0)
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0039, AssertionError -> 0x0030 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0039, AssertionError -> 0x0030 }
            java.util.Properties r1 = X.AnonymousClass02G.A03     // Catch:{ all -> 0x0029 }
            r0 = 0
            r1.store(r2, r0)     // Catch:{ all -> 0x0029 }
            r2.close()     // Catch:{ IOException -> 0x0039, AssertionError -> 0x0030 }
            return
        L_0x0029:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x002f }
        L_0x002f:
            throw r0     // Catch:{ IOException -> 0x0039, AssertionError -> 0x0030 }
        L_0x0030:
            r2 = move-exception
            java.lang.String r1 = "ForegroundEntityMapper"
            java.lang.String r0 = "Error storing properties"
            X.C010708t.A0L(r1, r0, r2)
            return
        L_0x0039:
            r2 = move-exception
            java.lang.String r1 = "ForegroundEntityMapper"
            java.lang.String r0 = "Error saving entity map"
            X.C010708t.A0L(r1, r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass098.run():void");
    }
}
