package X;

import com.facebook.graphql.enums.GraphQLSubscribeStatus;
import io.card.payment.BuildConfig;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.20X  reason: invalid class name */
public final class AnonymousClass20X {
    public int A00;
    public GraphQLSubscribeStatus A01;
    public String A02 = BuildConfig.FLAVOR;
    public String A03 = BuildConfig.FLAVOR;
    public String A04;
    public Set A05 = new HashSet();
    public boolean A06;
    public boolean A07;
}
