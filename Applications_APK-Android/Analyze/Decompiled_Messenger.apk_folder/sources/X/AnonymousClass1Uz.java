package X;

import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.module.OmnistoreComponent;

/* renamed from: X.1Uz  reason: invalid class name */
public final class AnonymousClass1Uz implements AnonymousClass1V0 {
    private final OmnistoreComponent A00;

    public void Bgx(C29271g9 r8) {
        synchronized (r8) {
            C32171lK provideSubscriptionInfo = this.A00.provideSubscriptionInfo(C29271g9.A00(r8));
            Integer num = provideSubscriptionInfo.A02;
            if (num == AnonymousClass07B.A01) {
                this.A00.onCollectionInvalidated();
                C29271g9.A00(r8).unsubscribeCollection(provideSubscriptionInfo.A00);
            } else if (num == AnonymousClass07B.A0C) {
                CollectionName collectionName = provideSubscriptionInfo.A00;
                C32161lJ r5 = provideSubscriptionInfo.A01;
                if (r5 == null) {
                    r5 = new C32161lJ(new C32151lI());
                }
                OmnistoreComponent omnistoreComponent = this.A00;
                synchronized (r8) {
                    Omnistore A002 = C29271g9.A00(r8);
                    C29421gO r2 = r8.A07;
                    synchronized (r2) {
                        r2.A00.put(collectionName, omnistoreComponent);
                    }
                    Collection subscribeCollection = r8.A01.subscribeCollection(A002, collectionName, r5);
                    this.A00.onCollectionAvailable(subscribeCollection);
                }
            }
        }
    }

    public void Bgy() {
        this.A00.onCollectionInvalidated();
    }

    public AnonymousClass1Uz(OmnistoreComponent omnistoreComponent) {
        this.A00 = omnistoreComponent;
    }
}
