package X;

import com.facebook.secure.intentswitchoff.FbReceiverSwitchOffDI;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Ze  reason: invalid class name and case insensitive filesystem */
public final class C05500Ze extends AnonymousClass1ZS {
    private static volatile C05500Ze A00;

    public static final C05500Ze A00(AnonymousClass1XY r6) {
        if (A00 == null) {
            synchronized (C05500Ze.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A00 = new C05500Ze(FbReceiverSwitchOffDI.A00(applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.Aw8, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private C05500Ze(FbReceiverSwitchOffDI fbReceiverSwitchOffDI, AnonymousClass0US r2) {
        super(fbReceiverSwitchOffDI, r2);
    }
}
