package X;

/* renamed from: X.0VG  reason: invalid class name */
public final class AnonymousClass0VG extends AnonymousClass0VC {
    public final int A00;

    public static AnonymousClass0VG A00(int i, AnonymousClass1XY r2) {
        return new AnonymousClass0VG(i, r2);
    }

    private AnonymousClass0VG(int i, AnonymousClass1XY r2) {
        super(r2);
        this.A00 = i;
    }
}
