package X;

import android.util.SparseIntArray;
import com.facebook.imagepipeline.memory.NativeMemoryChunk;

/* renamed from: X.1Ni  reason: invalid class name and case insensitive filesystem */
public abstract class C22921Ni extends C22871Nd {
    public final int[] A00;

    public AnonymousClass1NZ A0C(int i) {
        return new NativeMemoryChunk(i);
    }

    public C22921Ni(C14320t5 r5, AnonymousClass1N7 r6, AnonymousClass1N4 r7) {
        super(r5, r6, r7);
        SparseIntArray sparseIntArray = r6.A04;
        this.A00 = new int[sparseIntArray.size()];
        int i = 0;
        while (true) {
            int[] iArr = this.A00;
            if (i < iArr.length) {
                iArr[i] = sparseIntArray.keyAt(i);
                i++;
            } else {
                this.A04.C0d(this);
                this.A08.C6L(this);
                return;
            }
        }
    }
}
