package X;

import com.google.common.base.Objects;

/* renamed from: X.1Ka  reason: invalid class name and case insensitive filesystem */
public final class C22131Ka {
    public int A00 = 0;
    public int A01 = -1;
    public int A02;
    public int A03 = 0;
    public int A04 = 0;
    public int A05 = 0;
    public int A06;
    public Integer A07 = AnonymousClass07B.A01;
    public Integer A08 = AnonymousClass07B.A00;
    public String A09;
    public final C22141Kb A0A = new C22141Kb();
    public final C22141Kb A0B = new C22141Kb();

    public void A01(C21381Gs r2, int i) {
        A02(r2, i, 0);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C22131Ka r5 = (C22131Ka) obj;
            if (!(this.A00 == r5.A00 && this.A07 == r5.A07 && this.A08 == r5.A08 && this.A01 == r5.A01 && this.A06 == r5.A06 && this.A02 == r5.A02 && this.A05 == r5.A05 && this.A03 == r5.A03 && this.A04 == r5.A04 && Objects.equal(this.A09, r5.A09) && Objects.equal(this.A0B, r5.A0B) && Objects.equal(this.A0A, r5.A0A))) {
                return false;
            }
        }
        return true;
    }

    public AnonymousClass1KZ A00() {
        return new AnonymousClass1KZ(this);
    }

    public void A02(C21381Gs r3, int i, int i2) {
        this.A0B.put(r3, new AnonymousClass2T6(i, i2, this.A01));
    }

    public void A03(AnonymousClass1KZ r3) {
        this.A0B.putAll(r3.A08);
        this.A00 = r3.A00;
        this.A08 = r3.A0A;
        this.A03 = r3.A03;
        this.A04 = r3.A04;
        this.A01 = r3.A01;
        this.A09 = r3.A0B;
        this.A06 = r3.A06;
        this.A02 = r3.A02;
        this.A05 = r3.A05;
        this.A0A.putAll(r3.A07);
    }

    public int hashCode() {
        int hashCode;
        int A002 = (((((((((((((((((AnonymousClass1Y3.A4R + this.A00) * 31) + C90214Sl.A00(this.A07)) * 31) + AnonymousClass4HH.A00(this.A08)) * 31) + this.A01) * 31) + this.A06) * 31) + this.A02) * 31) + this.A05) * 31) + this.A03) * 31) + this.A04) * 31;
        String str = this.A09;
        if (str == null) {
            hashCode = 0;
        } else {
            hashCode = str.hashCode();
        }
        return ((((A002 + hashCode) * 31) + this.A0B.hashCode()) * 31) + this.A0A.hashCode();
    }
}
