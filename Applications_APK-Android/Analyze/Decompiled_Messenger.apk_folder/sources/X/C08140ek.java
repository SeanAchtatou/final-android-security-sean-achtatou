package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0ek  reason: invalid class name and case insensitive filesystem */
public final class C08140ek extends LinkedHashMap<Integer, C25871aX> {
    public final /* synthetic */ C08050eb this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C08140ek(C08050eb r1, int i) {
        super(i);
        this.this$0 = r1;
    }

    public boolean removeEldestEntry(Map.Entry entry) {
        if (size() > 75) {
            return true;
        }
        return false;
    }
}
