package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.0fs  reason: invalid class name and case insensitive filesystem */
public final class C08740fs implements Runnable {
    public static final String __redex_internal_original_name = "com.google.common.util.concurrent.AbstractFuture$SetFuture";
    public final C07920eO A00;
    public final ListenableFuture A01;

    public void run() {
        if (this.A00.value == this) {
            if (C07920eO.ATOMIC_HELPER.A04(this.A00, this, C07920eO.getFutureValue(this.A01))) {
                C07920eO.complete(this.A00);
            }
        }
    }

    public C08740fs(C07920eO r1, ListenableFuture listenableFuture) {
        this.A00 = r1;
        this.A01 = listenableFuture;
    }
}
