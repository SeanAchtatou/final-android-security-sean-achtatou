package X;

import com.facebook.acra.util.StatFsUtil;
import com.google.common.base.Preconditions;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1Fs  reason: invalid class name and case insensitive filesystem */
public abstract class C21221Fs<E> extends C08890g9 implements Collection<E> {
    public Collection A03() {
        if (this instanceof C33021mi) {
            return ((C33021mi) this).A06();
        }
        if (this instanceof C33031mj) {
            return ((C33031mj) this).A06();
        }
        AnonymousClass1H5 r1 = (AnonymousClass1H5) this;
        return !(r1 instanceof C21211Fr) ? r1.A06() : ((C21211Fr) r1).A06();
    }

    public String A02() {
        if (this instanceof C33031mj) {
            return ((C33031mj) this).entrySet().toString();
        }
        int size = size();
        C25001Xy.A00(size, "size");
        StringBuilder sb = new StringBuilder((int) Math.min(((long) size) * 8, (long) StatFsUtil.IN_GIGA_BYTE));
        sb.append('[');
        boolean z = true;
        for (Object next : this) {
            if (!z) {
                sb.append(", ");
            }
            z = false;
            if (next == this) {
                sb.append("(this Collection)");
            } else {
                sb.append(next);
            }
        }
        sb.append(']');
        return sb.toString();
    }

    public boolean A04(Collection collection) {
        if (!(this instanceof C33031mj)) {
            return C24931Xr.A09(this, collection.iterator());
        }
        return C22685B7s.A01((C33031mj) this, collection);
    }

    public boolean A05(Collection collection) {
        if (!(this instanceof C33031mj)) {
            Iterator it = iterator();
            Preconditions.checkNotNull(collection);
            boolean z = false;
            while (it.hasNext()) {
                if (!collection.contains(it.next())) {
                    it.remove();
                    z = true;
                }
            }
            return z;
        }
        C33031mj r1 = (C33031mj) this;
        Preconditions.checkNotNull(collection);
        if (collection instanceof AnonymousClass0UG) {
            collection = ((AnonymousClass0UG) collection).AYC();
        }
        return r1.AYC().retainAll(collection);
    }

    public boolean add(Object obj) {
        return A03().add(obj);
    }

    public boolean addAll(Collection collection) {
        return A03().addAll(collection);
    }

    public void clear() {
        A03().clear();
    }

    public boolean contains(Object obj) {
        return A03().contains(obj);
    }

    public boolean containsAll(Collection collection) {
        return A03().containsAll(collection);
    }

    public boolean isEmpty() {
        return A03().isEmpty();
    }

    public Iterator iterator() {
        return A03().iterator();
    }

    public boolean remove(Object obj) {
        return A03().remove(obj);
    }

    public boolean removeAll(Collection collection) {
        return A03().removeAll(collection);
    }

    public boolean retainAll(Collection collection) {
        return A03().retainAll(collection);
    }

    public int size() {
        return A03().size();
    }

    public Object[] toArray() {
        return A03().toArray();
    }

    public Object[] toArray(Object[] objArr) {
        return A03().toArray(objArr);
    }
}
