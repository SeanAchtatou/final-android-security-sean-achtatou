package X;

import com.facebook.inject.InjectorModule;
import java.text.Collator;
import java.util.Locale;

@InjectorModule
/* renamed from: X.1yV  reason: invalid class name and case insensitive filesystem */
public final class C39101yV extends AnonymousClass0UV {
    public static final Collator A00(AnonymousClass1XY r0) {
        return Collator.getInstance(A03(r0));
    }

    public static final Collator A01(AnonymousClass1XY r2) {
        Locale A03 = A03(r2);
        String locale = A03.toString();
        if ("ja".equalsIgnoreCase(locale) || "ja_JP".equalsIgnoreCase(locale)) {
            A03 = new Locale("ja@collation=phonebook");
        }
        return Collator.getInstance(A03);
    }

    public static final Collator A02(AnonymousClass1XY r1) {
        Collator instance = Collator.getInstance(A03(r1));
        instance.setStrength(0);
        instance.setDecomposition(1);
        return instance;
    }

    public static final Locale A03(AnonymousClass1XY r0) {
        return C26261bA.A00(r0).A01();
    }
}
