package X;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.util.AttributeSet;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.user.model.UserKey;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.concurrent.Executor;

/* renamed from: X.1Jd  reason: invalid class name and case insensitive filesystem */
public final class C21901Jd implements CallerContextable {
    public static final int A0Q = Color.rgb(238, 238, 238);
    public static final CallerContext A0R = CallerContext.A08(C21901Jd.class, "profile_user_tile_view", "user_tile");
    public static final String __redex_internal_original_name = "com.facebook.user.tiles.UserTileDrawableController";
    public int A00;
    public int A01;
    public Context A02;
    public Drawable A03;
    public Drawable A04;
    public AnonymousClass1PS A05;
    public AnonymousClass1QO A06;
    public AnonymousClass1Q0 A07;
    public AnonymousClass1Q0 A08;
    public AnonymousClass0UN A09;
    public AnonymousClass1M9 A0A;
    public AnonymousClass57n A0B;
    public AnonymousClass1KS A0C;
    public AnonymousClass1JY A0D;
    public AnonymousClass1KZ A0E;
    public AnonymousClass1R8 A0F;
    public String A0G;
    public boolean A0H;
    private boolean A0I = true;
    public final AnonymousClass1ZE A0J;
    public final C001300x A0K;
    public final C21391Gt A0L;
    public final AnonymousClass1RB A0M;
    public final Executor A0N;
    private final C06790c5 A0O;
    private final AnonymousClass1RA A0P;

    /* JADX WARN: Type inference failed for: r1v0, types: [java.util.concurrent.Future, X.1PS, X.1QO, X.1Q0] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void A02() {
        /*
            r2 = this;
            r1 = 0
            r2.A07 = r1
            X.1QO r0 = r2.A06
            if (r0 == 0) goto L_0x000c
            r0.AT6()
            r2.A06 = r1
        L_0x000c:
            X.1PS r0 = r2.A05
            if (r0 == 0) goto L_0x0015
            r0.close()
            r2.A05 = r1
        L_0x0015:
            if (r1 == 0) goto L_0x001b
            r0 = 1
            r1.cancel(r0)
        L_0x001b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21901Jd.A02():void");
    }

    public void A0A(int i) {
        boolean z = false;
        if (i > 0) {
            z = true;
        }
        Preconditions.checkArgument(z);
        if (this.A00 != i) {
            this.A00 = i;
            this.A0A.CBr(i);
            A04(this);
        }
    }

    public void A0B(Context context, AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C008407o.A0A, i, 0);
        AnonymousClass1KZ A002 = AnonymousClass3DL.A00(context, obtainStyledAttributes).A00();
        obtainStyledAttributes.recycle();
        A0C(context, attributeSet, i, 0, A002, null);
    }

    public void A0D(Context context, boolean z, int i, int i2, boolean z2, Drawable drawable, AnonymousClass1R8 r12, float f, AnonymousClass1KZ r14, Path path) {
        if (path == null || (f <= 0.0f && !z)) {
            this.A02 = context;
            this.A0A = new AnonymousClass1RD(this, i2);
            this.A0H = z2;
            this.A03 = drawable;
            this.A0E = r14;
            if (r12 == null) {
                AnonymousClass1R8 r1 = new AnonymousClass1R8();
                this.A0F = r1;
                r1.A04(context, 2132083163);
                this.A0F.A02((float) C007106r.A04(context.getResources(), 2132148404));
                AnonymousClass1R8 r0 = this.A0F;
                r0.A03 = path;
                r0.invalidateSelf();
            } else {
                this.A0F = r12;
            }
            if (i <= 0) {
                i = C007106r.A00(context, 50.0f);
            }
            A0A(i);
            if (path != null) {
                AnonymousClass1M9 r2 = this.A0A;
                float f2 = (float) this.A00;
                r2.C7T(new PathShape(path, f2, f2));
                AnonymousClass1R8 r02 = this.A0F;
                r02.A03 = path;
                r02.invalidateSelf();
            } else if (f > 0.0f) {
                this.A0A.C60(f);
            } else {
                A0F(z);
            }
            ArrayList arrayList = new ArrayList(5);
            arrayList.add(this.A0A.Akn());
            Drawable drawable2 = this.A03;
            if (drawable2 != null) {
                arrayList.add(drawable2);
            }
            arrayList.add(this.A0F);
            arrayList.add(this.A0M);
            this.A04 = new LayerDrawable((Drawable[]) arrayList.toArray(new Drawable[arrayList.size()]));
            return;
        }
        throw new IllegalArgumentException("Setting a round rect radius or circle with custom path is not supported.");
    }

    public static final C21901Jd A00(AnonymousClass1XY r3) {
        return new C21901Jd(r3, C04490Ux.A0L(r3), C04430Uq.A02(r3));
    }

    public static final C21901Jd A01(AnonymousClass1XY r3) {
        return new C21901Jd(r3, C04490Ux.A0L(r3), C04430Uq.A02(r3));
    }

    public static void A03(C21901Jd r3) {
        AnonymousClass1XX.A03(AnonymousClass1Y3.BEk, r3.A09);
        AnonymousClass1JY r2 = r3.A0D;
        if (r2 != null && r2.A06 == C21381Gs.A0R) {
            int A032 = r3.A0L.A03(r2);
            r3.A01 = A032;
            AnonymousClass1R8 r1 = r3.A0F;
            r1.A07 = true;
            r1.A01 = A032;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003e, code lost:
        if (com.google.common.base.Objects.equal(r1.A06, r2) != false) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A04(X.C21901Jd r7) {
        /*
            java.lang.String r0 = "UserTileDrawableController:updateDrawable"
            X.C27041cY.A01(r0)
            X.1KS r6 = r7.A0C
            X.1JY r1 = r7.A0D
            if (r1 == 0) goto L_0x0075
            X.1KS r0 = r1.A05
        L_0x000d:
            r7.A0C = r0
            if (r1 == 0) goto L_0x0072
            X.1Gs r4 = r1.A06
        L_0x0013:
            java.lang.String r0 = "updateBadgeDrawable"
            X.C27041cY.A01(r0)
            X.1RA r3 = r7.A0P
            android.content.Context r2 = r7.A02
            X.1RB r1 = r7.A0M
            X.1KZ r0 = r7.A0E
            r3.A01(r2, r1, r4, r0)
            X.C27041cY.A00()
            X.1KS r1 = r7.A0C
            X.1KS r0 = X.AnonymousClass1KS.SMS_CONTACT
            if (r1 != r0) goto L_0x0077
            X.1JY r0 = r7.A0D
            java.lang.String r2 = r0.A08
            X.1R8 r1 = r7.A0F
            java.lang.String r0 = r1.A05
            r5 = 0
            if (r0 == 0) goto L_0x0040
            java.lang.String r0 = r1.A06
            boolean r0 = com.google.common.base.Objects.equal(r0, r2)
            r4 = 0
            if (r0 == 0) goto L_0x0041
        L_0x0040:
            r4 = 1
        L_0x0041:
            int r3 = r7.A01
            X.1Gt r1 = r7.A0L
            X.1JY r0 = r7.A0D
            int r0 = r1.A03(r0)
            r2 = 0
            if (r3 != r0) goto L_0x004f
            r2 = 1
        L_0x004f:
            X.1JY r0 = r7.A0D
            java.lang.String r1 = r0.A07
            X.1KS r0 = r7.A0C
            if (r6 != r0) goto L_0x0064
            java.lang.String r0 = r7.A0G
            boolean r0 = X.C06850cB.A0C(r0, r1)
            if (r0 == 0) goto L_0x0064
            if (r4 == 0) goto L_0x0064
            if (r2 == 0) goto L_0x0064
            r5 = 1
        L_0x0064:
            if (r5 == 0) goto L_0x0077
            X.1M9 r0 = r7.A0A
            boolean r0 = r0.BGo()
            if (r0 == 0) goto L_0x0077
            X.C27041cY.A00()
            return
        L_0x0072:
            X.1Gs r4 = X.C21381Gs.A0L
            goto L_0x0013
        L_0x0075:
            r0 = 0
            goto L_0x000d
        L_0x0077:
            java.lang.String r0 = "updateUserTile"
            X.C27041cY.A01(r0)
            X.1Gt r2 = r7.A0L
            X.1JY r1 = r7.A0D
            int r0 = r7.A00
            android.net.Uri r0 = r2.A05(r1, r0, r0)
            X.1Q0 r1 = X.AnonymousClass1Q0.A00(r0)
            r0 = 1
            A05(r7, r1, r0)
            X.C27041cY.A00()
            X.C27041cY.A00()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21901Jd.A04(X.1Jd):void");
    }

    public static void A05(C21901Jd r5, AnonymousClass1Q0 r6, boolean z) {
        String str;
        AnonymousClass1JY r3 = r5.A0D;
        boolean z2 = true;
        if (!(r3 == null || r5.A01 == r5.A0L.A03(r3))) {
            z2 = false;
        }
        if (Objects.equal(r6, r5.A07) && z2) {
            return;
        }
        if (r6 == null) {
            r5.A06();
            return;
        }
        if (!r5.A0H) {
            r5.A06();
        }
        if (r5.A0C == AnonymousClass1KS.SMS_CONTACT) {
            str = r5.A0D.A07;
        } else {
            str = null;
        }
        r5.A0G = str;
        r5.A08 = r6;
        r5.A07 = r6;
        C23521Pv A012 = C23521Pv.A01(r6);
        if (r5.A0G != null && r5.A0F.A07(r5.A0D.A08)) {
            A03(r5);
        }
        if (r5.A0K.A02 == C001500z.A07) {
            A012.A07 = AnonymousClass1OM.SMALL;
        }
        UserKey userKey = null;
        AnonymousClass1JY r0 = r5.A0D;
        if (r0 != null) {
            userKey = r0.A03;
        }
        AnonymousClass1QO A013 = ((AnonymousClass1MB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ALB, r5.A09)).A01(A012.A02(), userKey, A0R);
        r5.A06 = A013;
        A013.CIK(new C182110t(r5, r6, z), r5.A0N);
    }

    public void A07() {
        if (this.A0I) {
            this.A0I = false;
            this.A0O.A00();
            A04(this);
        }
    }

    public void A08() {
        if (!this.A0I) {
            this.A0I = true;
            this.A0O.A01();
            A02();
        }
    }

    public void A09(int i) {
        if (i != 0) {
            C22131Ka r1 = new C22131Ka();
            r1.A03(this.A0E);
            r1.A00 = i;
            this.A0E = r1.A00();
            A04(this);
        }
    }

    public void A0C(Context context, AttributeSet attributeSet, int i, int i2, AnonymousClass1KZ r22, AnonymousClass1R8 r23) {
        AnonymousClass1R8 r13 = r23;
        AttributeSet attributeSet2 = attributeSet;
        Context context2 = context;
        int i3 = i;
        int i4 = i2;
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet2, C008407o.A2r, i3, i4);
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context2.obtainStyledAttributes(attributeSet2, C008407o.A2q, i3, i4);
        int dimensionPixelSize = obtainStyledAttributes2.getDimensionPixelSize(4, 0);
        boolean z = obtainStyledAttributes2.getBoolean(0, false);
        float dimension = obtainStyledAttributes2.getDimension(1, 0.0f);
        boolean z2 = obtainStyledAttributes2.getBoolean(3, false);
        int color = obtainStyledAttributes2.getColor(2, A0Q);
        obtainStyledAttributes2.recycle();
        if (r23 == null) {
            r13 = new AnonymousClass1R8(context2, attributeSet2, i3, i4);
        }
        A0D(context2, z, dimensionPixelSize, color, z2, drawable, r13, dimension, r22, null);
    }

    public void A0E(AnonymousClass1JY r1) {
        this.A0D = r1;
        A04(this);
    }

    public void A0F(boolean z) {
        if (z) {
            this.A0A.C5v();
        } else {
            this.A0A.C5z();
        }
    }

    private C21901Jd(AnonymousClass1XY r4, Resources resources, C04460Ut r6) {
        this.A09 = new AnonymousClass0UN(2, r4);
        this.A0L = C21391Gt.A02(r4);
        this.A0N = AnonymousClass0UX.A0S(r4);
        this.A0J = AnonymousClass1ZD.A00(r4);
        this.A0K = AnonymousClass0UU.A02(r4);
        this.A0P = new AnonymousClass1RA(r4);
        this.A0M = new AnonymousClass1RB(resources);
        C06600bl BMm = r6.BMm();
        BMm.A02(C99084oO.$const$string(1), new AnonymousClass1RC(this));
        this.A0O = BMm.A00();
    }

    public void A06() {
        A02();
        AnonymousClass1M9 r0 = this.A0A;
        if (r0 != null) {
            r0.clear();
        }
        AnonymousClass1R8 r2 = this.A0F;
        if (r2 != null) {
            r2.A06 = null;
            r2.A07 = false;
            r2.A06(null);
        }
        this.A0G = null;
    }
}
