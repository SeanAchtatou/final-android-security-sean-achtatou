package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0cF  reason: invalid class name and case insensitive filesystem */
public final class C06890cF extends C06900cG {
    private static volatile C06890cF A00;

    public static final C06890cF A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C06890cF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C06890cF();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
