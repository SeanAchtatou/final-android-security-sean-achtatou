package X;

import android.app.Activity;
import android.content.Intent;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import java.util.Set;

/* renamed from: X.1dt  reason: invalid class name and case insensitive filesystem */
public final class C27871dt {
    private static final Set A09 = ImmutableSet.A05(C27881du.A01, C27881du.A0x);
    public C06790c5 A00;
    public C06790c5 A01;
    public C06790c5 A02;
    public C06790c5 A03;
    public C17310yh A04;
    public AnonymousClass3At A05;
    public AnonymousClass0UN A06;
    public String A07;
    public final C04310Tq A08;

    public static final C27871dt A00(AnonymousClass1XY r1) {
        return new C27871dt(r1);
    }

    public static boolean A02(C27871dt r5, Activity activity) {
        if (activity instanceof C27891dv) {
            return true;
        }
        C005505z.A03("AuthenticatedActivityHelper.isAuthenticated.appInitLock", -174924369);
        try {
            if (!((AnonymousClass0UW) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ALy, r5.A06)).A05()) {
                return true;
            }
            C005505z.A00(-430918301);
            C005505z.A03("AuthenticatedActivityHelper.isAuthenticated.isLoggedIn", -1153589827);
            try {
                if (((AnonymousClass0XN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ArW, r5.A06)).A0I() && Objects.equal(r5.A08.get(), r5.A07)) {
                    return true;
                }
                C005505z.A00(813786301);
                C005505z.A03("AuthenticatedActivityHelper.isAuthenticated.authNotRequired", 2083066420);
                try {
                    Intent intent = activity.getIntent();
                    if (intent != null && !C06850cB.A0B(intent.getDataString())) {
                        String dataString = intent.getDataString();
                        for (String startsWith : A09) {
                            if (dataString.startsWith(startsWith)) {
                                return true;
                            }
                        }
                    }
                    C005505z.A00(1460874394);
                    return false;
                } finally {
                    C005505z.A00(-252070126);
                }
            } finally {
                C005505z.A00(-654218711);
            }
        } finally {
            C005505z.A00(702632164);
        }
    }

    private C27871dt(AnonymousClass1XY r3) {
        this.A06 = new AnonymousClass0UN(9, r3);
        this.A08 = AnonymousClass0XJ.A0K(r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r1 == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C27871dt r4, android.app.Activity r5) {
        /*
            boolean r0 = r5.isFinishing()
            if (r0 != 0) goto L_0x004a
            X.3At r0 = r4.A05
            if (r0 == 0) goto L_0x0011
            boolean r1 = r0.isShowing()
            r0 = 1
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 != 0) goto L_0x004a
            X.0rX r3 = new X.0rX
            r3.<init>(r5)
            r0 = 2131821738(0x7f1104aa, float:1.9276228E38)
            r3.A09(r0)
            r0 = 2131821736(0x7f1104a8, float:1.9276224E38)
            r3.A08(r0)
            r1 = 2131821737(0x7f1104a9, float:1.9276226E38)
            X.9yK r0 = new X.9yK
            r0.<init>(r4, r5)
            r3.A02(r1, r0)
            r0 = 0
            r3.A0E(r0)
            r2 = 7
            int r1 = X.AnonymousClass1Y3.A6Z
            X.0UN r0 = r4.A06
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            java.util.concurrent.Executor r2 = (java.util.concurrent.Executor) r2
            X.2Qa r1 = new X.2Qa
            r1.<init>(r4, r3)
            r0 = -1609750953(0xffffffffa00d2657, float:-1.195585E-19)
            X.AnonymousClass07A.A04(r2, r1, r0)
        L_0x004a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27871dt.A01(X.1dt, android.app.Activity):void");
    }
}
