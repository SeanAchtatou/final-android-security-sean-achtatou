package X;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0ea  reason: invalid class name and case insensitive filesystem */
public final class C08040ea {
    public static volatile boolean A06 = true;
    private static final C08080ee A07 = C08080ee.A01;
    public static volatile boolean A08;
    public C26191b3 A00;
    private C08290f0 A01 = null;
    public final int A02;
    public final SparseArray A03 = new SparseArray();
    public final List A04 = new ArrayList();
    private final C26161b0 A05;

    public static AnonymousClass0f7 A00(C08040ea r4, C07210ct r5) {
        AnonymousClass0f7 r2;
        AnonymousClass0f6 r22;
        AnonymousClass0f7 r23 = r5.A00;
        if (r23 != null) {
            return r23;
        }
        synchronized (r4) {
            r2 = r5.A00;
            if (r2 == null) {
                if (0 != 0) {
                    try {
                        r22 = A07.A00(r5.A04);
                    } catch (Exception e) {
                        r4.A00.A00.CGZ("MobileBoost", "BoosterBuilderInitializationWithException", e);
                        r22 = C57282rj.A00;
                    }
                    try {
                        r2 = r22.A00(r5.A03, r5.A05);
                    } catch (Exception e2) {
                        r4.A00.A00.CGZ("MobileBoost", "BoosterInitializationWithException", e2);
                        return C08240eu.A00;
                    }
                } else {
                    r2 = A07.A00(r5.A04).A00(r5.A03, r5.A05);
                }
                if (r5.A04 != 1 || !A08) {
                    C26161b0 r1 = r4.A05;
                    if (r1 != null && r1.A00) {
                        r4.A01 = new C34691q1(r1);
                    }
                } else {
                    r4.A01 = new C48792b9(r4.A05);
                }
                C08290f0 r0 = r4.A01;
                if (r0 != null) {
                    r2.A04(r0);
                    r5.A01 = r4.A01;
                }
                r5.A00 = r2;
                if (0 != 0) {
                    r2.A04(null);
                }
            }
        }
        return r2;
    }

    public void A01(C07210ct r3) {
        this.A04.add(r3);
        this.A03.put(r3.A04, r3);
    }

    public C08040ea(int i, C26191b3 r3, C26161b0 r4) {
        this.A02 = i;
        this.A05 = r4;
        this.A00 = r3;
    }
}
