package X;

/* renamed from: X.1lr  reason: invalid class name and case insensitive filesystem */
public abstract class C32501lr implements C32511ls {
    public C16580xM A00;
    private final String A01;

    public boolean BEW() {
        if (!(this instanceof C186114k)) {
            return (this instanceof C16660xW) && ((C16660xW) this).A04 == AnonymousClass07B.A00;
        }
        return true;
    }

    public void onPause() {
        if (this instanceof C186114k) {
            C186114k.A0A((C186114k) this);
        } else if (this instanceof C16690xc) {
            C16690xc r0 = (C16690xc) this;
            r0.A01.CJr(C26211b5.A06, r0.A00);
        } else if (this instanceof C16660xW) {
            C16660xW r1 = (C16660xW) this;
            C16660xW.A00(r1);
            r1.A01.A01();
            r1.A00.A01();
        } else if (this instanceof C16680xb) {
            C16680xb r3 = (C16680xb) this;
            r3.A06.CJr(C05690aA.A0z, r3.A00);
            r3.A04.A01();
        }
    }

    public void onResume() {
    }

    public String Act() {
        if (!(this instanceof C16660xW)) {
            return this.A01;
        }
        switch (((C33701o0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxV, ((C16660xW) this).A02)).AiE().ordinal()) {
            case 1:
                return "ConnectionStatusNotification - Connected To Captive Portal";
            case 2:
                return "ConnectionStatusNotification - Connecting";
            case 3:
                return "ConnectionStatusNotification - Waiting To Connect";
            case 4:
                return "ConnectionStatusNotification - No Internet";
            default:
                return "ConnectionStatusNotification - Connected";
        }
    }

    public C32501lr(String str) {
        this.A01 = str;
    }

    public void C6K(C16580xM r1) {
        this.A00 = r1;
    }
}
