package X;

import com.google.common.collect.ImmutableList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/* renamed from: X.20N  reason: invalid class name */
public final class AnonymousClass20N {
    public final ImmutableList A00;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(A00(this.A00), A00(((AnonymousClass20N) obj).A00));
    }

    public int hashCode() {
        return Arrays.hashCode(A00(this.A00));
    }

    public AnonymousClass20N(List list) {
        this.A00 = ImmutableList.copyOf((Collection) list);
    }

    private static long[] A00(List list) {
        long[] jArr = new long[list.size()];
        for (int i = 0; i < list.size(); i++) {
            jArr[i] = ((C69283Wl) list.get(i)).ArZ();
        }
        return jArr;
    }
}
