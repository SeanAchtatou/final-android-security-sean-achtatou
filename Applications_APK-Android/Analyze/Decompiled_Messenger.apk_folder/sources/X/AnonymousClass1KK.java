package X;

import java.util.Comparator;

/* renamed from: X.1KK  reason: invalid class name */
public final class AnonymousClass1KK implements Comparator {
    public int compare(Object obj, Object obj2) {
        C17730zN r3 = (C17730zN) obj;
        C17730zN r4 = (C17730zN) obj2;
        int i = r3.A08.bottom;
        int i2 = r4.A08.bottom;
        if (i == i2) {
            return r4.A00 - r3.A00;
        }
        return i - i2;
    }
}
