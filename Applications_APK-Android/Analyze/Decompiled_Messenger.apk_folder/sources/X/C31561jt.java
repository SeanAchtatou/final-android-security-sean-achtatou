package X;

import android.os.Handler;
import android.os.SystemClock;

/* renamed from: X.1jt  reason: invalid class name and case insensitive filesystem */
public final class C31561jt extends C27441dC implements C31551js, C31571ju {
    private final Handler A00;

    public boolean BG0() {
        if (this.A00.getLooper().getThread() == Thread.currentThread()) {
            return true;
        }
        return false;
    }

    public void BxL(Runnable runnable, String str) {
        Runnable A002 = C25231Yv.A00(AnonymousClass08S.A0J(this.A01, str), runnable, this.A00);
        this.A00.postAtTime(AnonymousClass00S.A00(A002, -1243507495), runnable, SystemClock.uptimeMillis());
    }

    public void BxM(Runnable runnable, String str) {
        this.A00.postAtTime(AnonymousClass00S.A00(C25231Yv.A00(AnonymousClass08S.A0J(this.A01, str), runnable, this.A00), 1131840757), runnable, 0);
    }

    public void C1H(Runnable runnable) {
        this.A00.removeCallbacksAndMessages(runnable);
    }

    public C31561jt(Handler handler, String str, int i) {
        super(str, i);
        this.A00 = handler;
    }

    public boolean BHG() {
        return C25231Yv.A01();
    }

    public boolean BxN(Runnable runnable, String str) {
        BxL(runnable, str);
        return true;
    }
}
