package X;

import com.facebook.messaging.model.threads.SyncedGroupData;

/* renamed from: X.0xt  reason: invalid class name and case insensitive filesystem */
public final class C16860xt {
    public long A00;
    public String A01;
    public boolean A02;

    public C16860xt() {
    }

    public C16860xt(SyncedGroupData syncedGroupData) {
        C28931fb.A05(syncedGroupData);
        boolean z = syncedGroupData instanceof SyncedGroupData;
        this.A00 = syncedGroupData.A00;
        this.A01 = syncedGroupData.A01;
        this.A02 = syncedGroupData.A02;
    }
}
