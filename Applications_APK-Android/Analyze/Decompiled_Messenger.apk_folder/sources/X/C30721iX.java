package X;

import com.facebook.common.file.FileModule;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1iX  reason: invalid class name and case insensitive filesystem */
public final class C30721iX extends C30731iY implements C30741iZ {
    private static volatile C30721iX A01;
    public AnonymousClass0UN A00;

    public static final C30721iX A00(AnonymousClass1XY r8) {
        if (A01 == null) {
            synchronized (C30721iX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        A01 = new C30721iX(applicationInjector, FBAppsStorageResourceMonitor.A00(applicationInjector), FileModule.A01(applicationInjector), C04750Wa.A01(applicationInjector), AnonymousClass0ZD.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C30721iX(AnonymousClass1XY r3, FBAppsStorageResourceMonitor fBAppsStorageResourceMonitor, C012109i r5, AnonymousClass09P r6, QuickPerformanceLogger quickPerformanceLogger) {
        super(r5, r6, quickPerformanceLogger);
        this.A00 = new AnonymousClass0UN(1, r3);
        fBAppsStorageResourceMonitor.A01(this);
    }
}
