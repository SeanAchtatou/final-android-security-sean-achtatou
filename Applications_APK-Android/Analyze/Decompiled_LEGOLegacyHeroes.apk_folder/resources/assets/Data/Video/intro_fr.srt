﻿1
00:00:02,431 --> 00:00:03,431
<font face="Montserrat SemiBold">Salut !</font>

2
00:00:03,431 --> 00:00:05,281
<font face="Montserrat SemiBold">Salut !
Bienvenue dans notre humble dépotoir !</font>

3
00:00:05,281 --> 00:00:06,981
<font face="Montserrat SemiBold">Du genre morne, hein ?</font>

4
00:00:06,981 --> 00:00:09,981
<font face="Montserrat SemiBold">Ça a toujours été comme ça,
d'aussi loin que quiconque se souvienne.</font>

5
00:00:09,981 --> 00:00:13,981
<font face="Montserrat SemiBold">Nous avons passé des années à marauder en quête
de n'importe quoi qui redresserait cet ÂGE OBSCUR...</font>

6
00:00:14,220 --> 00:00:15,020
<font face="Montserrat SemiBold">T'ES ENCORE EN TRAIN DE PARLER TOUT SEUL?!</font>

7
00:00:15,020 --> 00:00:17,520
<font face="Montserrat SemiBold">T'ES ENCORE EN TRAIN DE PARLER TOUT SEUL?!
Retourne trier !</font>

8
00:00:18,500 --> 00:00:20,400
<font face="Montserrat SemiBold">J'en ai marre de trier ces déchets !</font>

9
00:00:20,400 --> 00:00:23,900
<font face="Montserrat SemiBold">On devrait aller CONSTRUIRE, EXPLORER
et aller TROUVER <font color="#f0c350">NOS SEMBLABLES</font> !</font>

