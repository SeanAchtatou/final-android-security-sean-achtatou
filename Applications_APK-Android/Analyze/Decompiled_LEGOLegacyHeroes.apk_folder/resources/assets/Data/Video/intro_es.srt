﻿1
00:00:02,431 --> 00:00:03,431
<font face="Montserrat SemiBold">¡Hola!</font>

2
00:00:03,431 --> 00:00:05,281
<font face="Montserrat SemiBold">¡Hola!
¡Te doy la bienvenida a nuestro humilde pantano!</font>

3
00:00:05,281 --> 00:00:06,981
<font face="Montserrat SemiBold">Un poco desolador, ¿no?</font>

4
00:00:06,981 --> 00:00:09,981
<font face="Montserrat SemiBold">Ha estado así
desde que tengo uso de razón.</font>

5
00:00:09,981 --> 00:00:13,981
<font face="Montserrat SemiBold">Hemos estado años buscando algo
que pueda acabar con esta EDAD OSCURA...</font>

6
00:00:14,220 --> 00:00:15,020
<font face="Montserrat SemiBold">¿OTRA VEZ HABLANDO SOLO?</font>

7
00:00:15,020 --> 00:00:17,520
<font face="Montserrat SemiBold">¿OTRA VEZ HABLANDO SOLO?
¡Vuelve al trabajo!</font>

8
00:00:18,500 --> 00:00:20,400
<font face="Montserrat SemiBold">¡No quiero seguir rebuscando entre esta basura!</font>

9
00:00:20,400 --> 00:00:23,900
<font face="Montserrat SemiBold">¡Deberíamos estar ahí fuera CONSTRUYENDO, EXPLORANDO
y BUSCANDO <font color="#f0c350">OTROS COMO NOSOTROS</font>!</font>

