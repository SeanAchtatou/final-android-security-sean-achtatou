﻿1
00:00:02,431 --> 00:00:03,431
<font face="Montserrat SemiBold">Hello!</font>

2
00:00:03,431 --> 00:00:05,281
<font face="Montserrat SemiBold">Hello!
Welcome to our humble dump!</font>

3
00:00:05,281 --> 00:00:06,981
<font face="Montserrat SemiBold">Kinda bleak huh?</font>

4
00:00:06,981 --> 00:00:09,981
<font face="Montserrat SemiBold">It’s been this way
for as long as anybody can remember.</font>

5
00:00:09,981 --> 00:00:13,981
<font face="Montserrat SemiBold">We’ve spent years scrounging for anything
that could turn this DARK AGE around…</font>

6
00:00:14,220 --> 00:00:15,020
<font face="Montserrat SemiBold">ARE YOU TALKING TO YOURSELF AGAIN?!</font>

7
00:00:15,020 --> 00:00:17,520
<font face="Montserrat SemiBold">ARE YOU TALKING TO YOURSELF AGAIN?!
Get back to sifting!</font>

8
00:00:18,500 --> 00:00:20,400
<font face="Montserrat SemiBold">I don’t wanna sift through this junk anymore!</font>

9
00:00:20,400 --> 00:00:23,900
<font face="Montserrat SemiBold">We should be out there BUILDING, EXPLORING,
and FINDING <font color="#f0c350">OTHERS LIKE US!</font></font>

