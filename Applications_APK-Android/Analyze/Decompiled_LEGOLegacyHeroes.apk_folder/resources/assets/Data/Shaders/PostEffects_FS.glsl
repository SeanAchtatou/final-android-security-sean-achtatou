#version 300 es
// # 18 "E:/Lego/Trunk/Engine/Data/Shaders/Engine/include/Version.gx"
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif


// # 4 "E:/Lego/Trunk/Data/Shaders/PostEffects.gx"

//#define DEBUG_VIEW
// varyings
varying highp vec2 ScreenUV;


// # 25 "E:/Lego/Trunk/Data/Shaders/PostEffects.gx"

highp float Luma( highp vec3 color )
{
    return dot(color, vec3(0.2126, 0.7152, 0.0722));
}


highp float WorldSpaceDistance(highp float depth, highp float near, highp float far)
{
    highp float farNear = far/near;
    highp float invFar = 1.0/far;
    
    highp float a = invFar * (1.0 - farNear);
    highp float b = invFar * farNear;
    
    return 1.0 / (depth * a + b);
}

#ifdef DISTANCE_FOG
    uniform highp sampler2D texture0; // frame buffer
    GLITCH_UNIFORM_PROPERTIES(texture1, (depthtexture = 1)) // for compiled metal conversion
    uniform highp sampler2D texture1; // depth buffer
    uniform lowp sampler2D FogGradient;
    uniform lowp sampler2D Noise;
    uniform mediump float CamNear;
    uniform mediump float CamFar;
    uniform mediump vec2 NoiseScale;
    uniform mediump float NoiseStrength;
    
    uniform mediump float MaxDist;

    void main(void)
    {
        highp vec2 uv = ScreenUV;
        highp vec4 color = texture2D(texture0, uv);

        mediump float noise = texture2D( Noise, NoiseScale * uv ).r;
        noise = NoiseStrength*(noise-0.5);
        
        highp float depth = texture2D(texture1, uv).r;
        
        highp float dist = WorldSpaceDistance( depth, CamNear, CamFar );
        mediump float t = min(dist, MaxDist) / MaxDist;
        t += noise;
        
        lowp vec4 fog = texture2D( FogGradient, vec2(t, 0.0) );
        
        lowp vec4 finalColor = color;
        finalColor.rgb = (1.0 - fog.a)*color.rgb + fog.a*fog.rgb;
        
        gl_FragColor = finalColor;
     }
#elif defined(FXAA)
    uniform lowp sampler2D texture0;
    uniform highp sampler2D texture1; // NotUsedHere
    uniform highp vec2 texture0TexelSize;
    
    #ifdef DEBUG_VIEW
        uniform highp float time; //Temprary Varaible - Remove on final version
    #endif   

    highp float Rangemap(highp float value, highp float istart, highp float istop, highp float ostart, highp float  ostop)
    {
    return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
    }

    void main(void)
    {
        mediump float texelrange = 0.5;

        mediump float dX = texture0TexelSize.x * texelrange;
        mediump float dY = texture0TexelSize.y * texelrange;

        highp float FXAA_SPAN_MAX   = 8.0;
        highp float FXAA_REDUCE_MUL = 1.0 / 8.0;
        highp float FXAA_REDUCE_MIN = 1.0 / 128.0;

        // 1st stage - Find edge
        highp vec3 rgbNW = texture2D(texture0, ScreenUV + vec2(-dX, -dY) ).rgb;
        highp vec3 rgbNE = texture2D(texture0, ScreenUV + vec2( dX, -dY) ).rgb;
        highp vec3 rgbSW = texture2D(texture0, ScreenUV + vec2(-dX,  dY) ).rgb;
        highp vec3 rgbSE = texture2D(texture0, ScreenUV + vec2( dX,  dY) ).rgb;
        highp vec3 rgbM  = texture2D(texture0, ScreenUV).rgb;

        highp vec3 luma = vec3(0.299, 0.587, 0.114);

        highp float lumaNW = dot(rgbNW, luma);
        highp float lumaNE = dot(rgbNE, luma);
        highp float lumaSW = dot(rgbSW, luma);
        highp float lumaSE = dot(rgbSE, luma);
        highp float lumaM  = dot(rgbM,  luma);

        highp vec2 dir;
        dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
        dir.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));
        
        highp float lumaSum   = lumaNW + lumaNE + lumaSW + lumaSE;
        highp float dirReduce = max(lumaSum * (0.25 * FXAA_REDUCE_MUL), FXAA_REDUCE_MIN);
        highp float rcpDirMin = 1. / (min(abs(dir.x), abs(dir.y)) + dirReduce);

        dir = min(vec2(FXAA_SPAN_MAX), max(vec2(-FXAA_SPAN_MAX), dir * rcpDirMin)) * texture0TexelSize;

        // 2nd stage - Blur
        highp vec3 rgbA = 0.5 * (texture2D(texture0, ScreenUV + dir * (1.0/3.0 - 0.5)) +
                        texture2D(texture0, ScreenUV + dir * (2.0/3.0 - 0.5))).rgb;
        highp vec3 rgbB = rgbA * 0.5 + 0.25 * (
                        texture2D(texture0, ScreenUV + dir * (0.0/3.0 - 0.5)) +
                        texture2D(texture0, ScreenUV + dir * (3.0/3.0 - 0.5))).rgb;
        
        highp float lumaB = dot(rgbB, luma);
        
        highp float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
        highp float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));


        highp vec3 rgbAB = ((lumaB < lumaMin) || (lumaB > lumaMax)) ? rgbA : rgbB;
            
        #ifdef DEBUG_VIEW
            //Temprary Varaibles - Remove on final version
                highp vec3 IndicationTriangle = (clamp (floor (vec3((ScreenUV.x + ScreenUV.y*0.5) * 8.0)), 0.0, 1.0));
                highp float ComparisonSwitch = floor(sin(time) + 1.0);

                rgbAB = mix (rgbM, rgbAB * IndicationTriangle, ComparisonSwitch);
            //
        #endif   


        gl_FragColor = vec4(rgbAB,  1.0);

    }
#elif defined(SSAO)
    //http://theorangeduck.com/page/pure-depth-ssao
    uniform lowp sampler2D texture0;
    uniform highp sampler2D texture1;
    uniform highp vec2 texture0TexelSize;
    uniform highp sampler2D RandomTextureSampler;
    
    uniform highp mat4 CamView;
    uniform highp mat4 CamInvView;
    uniform highp vec3 CamPos;
    uniform highp mat4 CamProjection;
    uniform highp mat4 CamInvProjection;

    uniform highp float radius;// = 0.05;

    uniform highp float occ_ray_angle_min;// = 0.3;
    uniform highp float occ_base;// = 0.25;
    uniform highp float occ_strength;// = 1.0;
    uniform highp vec3 occ_color;// = vec3(0.0, 0.0, 0.0);

    uniform highp float hlt_ray_angle_min;// = 0.3;
    uniform highp float hlt_base;// = 0.8;
    uniform highp float hlt_strength;// = 1.0;
    uniform highp vec3 hlt_color;// = vec3(1.0, 1.0, 1.0);

    highp vec4 VSPositionFromDepth(highp vec2 vTexCoord)
    {
        // Get the depth value for this pixel
        highp float z = texture2D(texture1, vTexCoord).x * 2.0 - 1.0;  
        // Get x/w and y/w from the viewport position
        highp float x = vTexCoord.x * 2.0 - 1.0;
        highp float y = vTexCoord.y * 2.0 - 1.0;
        highp vec4 vProjectedPos = vec4(x, y, z, 1.0);

        // Transform by the inverse projection matrix
        highp vec4 vPositionVS = CamInvProjection*vProjectedPos ;
        
        // Divide by w to get the view-space position
        return vec4((vPositionVS.xyz / vPositionVS.w), vPositionVS.w);  
    }

    // Gets the screen-space texel coord from clip-space position
    highp vec2 CalcSSTexCoord (highp vec4 vPositionCS, highp mat4 CamProjection)
    {
        highp vec2 vSSTexCoord = (CamProjection*vPositionCS).xy * vPositionCS.w;
        vSSTexCoord = vSSTexCoord * 0.5 + 0.5;
        return vSSTexCoord;
    }
    /*
    highp vec3 normal_from_depth(highp vec3 sourcePosition, highp vec2 texcoords) {
        highp vec2 offset1 = vec2(0.0, texture0TexelSize.y);
        highp vec2 offset2 = vec2(texture0TexelSize.x, 0.0);
        
        highp vec3 p1 = VSPositionFromDepth(texcoords + offset1).xyz;
        highp vec3 p2 = VSPositionFromDepth(texcoords + offset2).xyz;
        highp vec3 p3 = sourcePosition;


        highp vec3 normal  = cross(p3 - p2, p1 - p2);
        //normal *= vec3(-1.0, 1.0, -1.0);  // For Worldspace
        normal = normalize (normal);
        //normal = ( inverse(CamView) * vec4(normal, 0.0)).rgb;  // For Worldspace

        return  normal;
    }
    */

    void main(void)
    {

        
        //Convert these to uniforms
        /*
        highp float radius = 0.05;
        
        highp float occ_ray_angle_min = 0.3;
        highp float occ_base = 0.25;
        highp float occ_strength = 1.0;
        highp vec3 occ_color = vec3(0.0, 0.0, 0.0);
        
        highp float hlt_ray_angle_min = 0.3;
        highp float hlt_base = 0.8;
        highp float hlt_strength = 1.0;
        highp vec3 hlt_color = vec3(1.0, 1.0, 1.0);
        */
        //End

        //highp mat4 CamProjection = inverse(CamInvProjection);
        const int samples = 16;
		highp vec3 sample_sphere[16];
		sample_sphere[0] = vec3( 0.5381, 0.1856,-0.4319);
		sample_sphere[1] = vec3( 0.1379, 0.2486, 0.4430);
		sample_sphere[2] = vec3( 0.3371, 0.5679,-0.0057);
		sample_sphere[3] = vec3(-0.6999,-0.0451,-0.0019);
		sample_sphere[4] = vec3( 0.0689,-0.1598,-0.8547);
		sample_sphere[5] = vec3( 0.0560, 0.0069,-0.1843);
		sample_sphere[6] = vec3(-0.0146, 0.1402, 0.0762);
		sample_sphere[7] = vec3( 0.0100,-0.1924,-0.0344);
		sample_sphere[8] = vec3(-0.3577,-0.5301,-0.4358);
		sample_sphere[9] = vec3(-0.3169, 0.1063, 0.0158);
		sample_sphere[10] = vec3( 0.0103,-0.5869, 0.0046);
		sample_sphere[11] = vec3(-0.0897,-0.4940, 0.3287);
		sample_sphere[12] = vec3( 0.7119,-0.0154,-0.0918);
		sample_sphere[13] = vec3(-0.0533, 0.0596,-0.5411);
		sample_sphere[14] = vec3( 0.0352,-0.0631, 0.5460);
		sample_sphere[15] = vec3(-0.4776, 0.2847,-0.0271);
        
        
        //Get Positions
        highp vec4 position = VSPositionFromDepth(ScreenUV); //Position is using a real world scale
        highp vec3 worldposition = position.xyz * vec3(-1.0, 1.0, -1.0);
        worldposition = ( CamInvView * vec4(worldposition, 0.0)).rgb; 
        worldposition = worldposition - CamPos;
        //End Get Positions


        //Get Normals
        //highp vec3 normal = normal_from_depth(position.xyz, ScreenUV);

        highp vec3 p1 = VSPositionFromDepth(ScreenUV + vec2(0.0, texture0TexelSize.y) ).xyz;
        highp vec3 p2 = VSPositionFromDepth(ScreenUV + vec2(texture0TexelSize.x, 0.0) ).xyz;

        highp vec3 rawNormal  = cross(position.xyz - p2, p1 - p2);
        highp vec3 normal = normalize (rawNormal);

        highp vec3 worldnormal = rawNormal * vec3(-1.0, 1.0, -1.0);
        worldnormal = normalize (worldnormal);
        worldnormal = ( CamInvView * vec4(worldnormal, 0.0)).rgb;

        //End Get Normals



        highp vec2 worldUV_X = vec2 (worldposition.z, worldposition.y);
        highp vec2 worldUV_Y = vec2 (worldposition.x, worldposition.z);
        highp vec2 worldUV_Z = vec2 (worldposition.x, worldposition.y);



        highp vec2 worldUV = vec2 (0.0);
        worldUV += abs(worldnormal.x) * worldUV_X;
        worldUV += abs(worldnormal.g) * worldUV_Y;
        worldUV += abs(worldnormal.z) * worldUV_Z;
        worldUV *= 50.0;
        
        highp vec3 random = texture2D(RandomTextureSampler, worldUV).rgb;
        //random = fract(sin(cross(ceil(worldposition) ,vec3(12.9898,78.233, 51.2687))) * 43758.5453); // This is randomness based on only the world position
        random = normalize(random);


        highp float depth = distance (position.xyz, vec3(0.0)) ;
        highp float radius_depth = max (radius, radius * depth);



        highp float occlusion_mask = 0.0;
        highp float highlight_mask = 0.0;

        //Debug Variables
        highp float avg_HitDist = 0.0;
        highp float avg_HitAngle = 0.0;
        highp float avg_RayReversal = 0.0;


        for(int i=0; i < samples; i++) {

            //ray position calculation
            highp vec3 ray = radius_depth * reflect(sample_sphere[i], random); // This randomizes the ray target positions
            highp vec3 hemi_ray = position.xyz + ray; 
            //highp vec3 hemi_ray = position.xyz + sign(dot(normalize (ray),normal)) * ray; // This forces all ray positions to be on the normal side
            
            //avg_RayReversal += ceil (dot(normalize (radius_depth * sample_sphere[i]),normal));
            
            // hit depth, possition, and addjusted ray vector
            highp vec2 hemi_raySC = clamp ( CalcSSTexCoord(vec4(hemi_ray.xyz, position.w), CamProjection) , 0.0, 0.999);
            highp vec3 hit_possition = VSPositionFromDepth( hemi_raySC ).xyz;
            

            // depth radius calculation
            highp float hit_dist = distance (position.xyz, hit_possition.xyz );
            hit_dist = 1.0 - hit_dist;
            hit_dist = max (0.0, hit_dist);
            //avg_HitDist += hit_dist; // This is a debug variables

            
            // anlge calculation
            //highp vec3 hit_vector = (inverse(CamView) * vec4(normalize (position.xyz - hit_possition) * vec3(-1.0, 1.0, -1.0), 0.0)).rgb; //  For Worldspace
            highp vec3 hit_vector = normalize (position.xyz - hit_possition);
            highp float hit_rayangle = dot (normal, hit_vector);
            highp float hit_rayangle_highlight = max (hlt_ray_angle_min, -1.0 * hit_rayangle) - hlt_ray_angle_min;
            
            hit_rayangle = max (occ_ray_angle_min, hit_rayangle) - occ_ray_angle_min; // This is to reduce banding on flat surfaces
            //avg_HitAngle +=  hit_rayangle; // This is a debug variable


            // final influence calculation
            occlusion_mask += (hit_rayangle * hit_dist);
            highlight_mask += (hit_rayangle_highlight * hit_dist) ;
        }

        highp vec3 albedo = texture2D(texture0, ScreenUV).rgb ;
        
        highp float SampMul = 1.0 / float(samples);

        occlusion_mask = pow(
            mix (1.0, occ_base, occlusion_mask * SampMul),   //this controlls the base
            occ_strength                                // this controlls the strength                   
        );
        
        highlight_mask = pow(
            mix (1.0, hlt_base, highlight_mask * SampMul),   //this controlls the base
            hlt_strength                                // this controlls the strength                   
        );



        highp vec3 occ_hlt = mix (occ_color, mix (hlt_color, vec3(0.5), highlight_mask), occlusion_mask);

        highp vec3 finalColor = // overlay blend funtion
            (step (albedo.xyz, vec3(0.5)))       * (1.0 - (1.0-2.0*(albedo-0.5)) * (1.0-occ_hlt)) +
            (1.0-(step (albedo.xyz, vec3(0.5)))) * ((2.0*albedo) * occ_hlt)
        ;


        gl_FragColor = 
            vec4(finalColor, 1.0)
            
            //*0.001 + vec4((worldposition.rgb), 1.0)      
            //*0.001 + vec4((vec3(occlusion)), 1.0)
            //*0.001 + vec4((vec3(highlight)), 1.0)  
            //*0.001 + vec4((vec3(occ_hlt)), 1.0)   


            //*0.001 + vec4((vec3(avg_HitDist / 16.0)), 1.0)  
            //*0.001 + vec4((vec3(avg_HitAngle / 16.0)), 1.0) 
            //*0.001 + vec4((vec3(avg_RayReversal / 16.0)), 1.0) 

            //*0.001 + vec4(normal, 1.0)
            //*0.001 + vec4((position.rgb), 1.0)
            //*0.001 + vec4(ceil(worldposition), 1.0)
            //*0.001 + vec4((worldnormal), 1.0)


            //*0.001 + vec4(random, 1.0)
            //*0.001 + vec4((worldUV.xy), 0.0, 1.0)  
            
        ;
    }
#else
    void main(void)
    {
        gl_FragColor = vec4(1.0, 0.0, 1.0, 1.0);
    }
#endif

