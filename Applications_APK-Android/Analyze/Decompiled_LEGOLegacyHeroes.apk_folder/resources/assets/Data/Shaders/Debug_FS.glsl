// varyings
varying mediump vec4 vTexCoord;


// uniforms
uniform lowp sampler2D AlbedoMap;

void main(void)
{
	gl_FragColor = texture2D(AlbedoMap, vTexCoord.xy);
}


