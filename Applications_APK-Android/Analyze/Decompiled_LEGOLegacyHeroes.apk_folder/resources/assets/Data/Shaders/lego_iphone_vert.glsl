#ifdef DCC_MAX
	#define DCC(x) x
#else
	#define DCC(x)
#endif
 
#define PI 3.14159265359
//#define ANIMFX 
//#define SCCON 
attribute highp   vec4 Vertex DCC(: POSITION);

#ifdef DCC_MAX
	attribute lowp vec3  Color : TEXCOORD3;
	attribute lowp float Alpha : TEXCOORD4;
#else
	attribute lowp vec4  Color;
#endif

#ifdef ANIMFX
	uniform highp sampler2D AnimFXPos1;
	uniform highp sampler2D AnimFXPos2;
	uniform highp sampler2D AnimFXRot;
	uniform highp sampler2D AnimFXCol;
	uniform highp float time;
	uniform highp float Speed;
	uniform highp vec3 BoundsShift;
	uniform highp vec3 BoundingBox;
#endif

attribute mediump vec4 Normal DCC(: NORMAL);
attribute mediump vec4 Tangent DCC(: TANGENT);
attribute mediump vec2 TexCoord0 DCC(: TEXCOORD0);
attribute mediump vec2 TexCoord1 DCC(: TEXCOORD1);
attribute mediump vec2 TexCoord2 DCC(: TEXCOORD2);

uniform highp 	mat4 matWorld;
uniform highp 	mat4 matView;
uniform highp 	mat4 matProjection;
uniform highp 	mat4 matworldview;
uniform highp 	mat4 matviewprojection;
uniform highp 	mat4 matWorldIT;
uniform highp   mat4 WorldViewProjectionMatrix;
uniform mediump vec3 eyepos;

#ifdef DEFERRED
	varying highp   vec3 vWPosition	   DCC(:TEXCOORD4);
#else
	#ifdef SHADOWS
        #define SHADOW_MAP_CASCADE_COUNT 2
        varying highp vec3 vSMCoord0;
        //varying highp vec3 vSMCoord1;
        //varying highp float vViewZ;
        
        //uniform highp mat4 global_ShadowMapView;

        //#if defined SHADOW_MAP_OFFSET_POSITION || defined EDITOR
            uniform mediump vec3 global_ShadowDir;
            uniform mediump vec2 global_ShadowMapOffsets;
        //#endif

        //uniform highp mat4 global_ShadowMapViewProjs[SHADOW_MAP_CASCADE_COUNT];
        uniform highp mat4 global_ShadowMapViewProj;

        void ReceiveShadow(highp vec3 worldPos, mediump vec3 worldNormal)
        {
            //#if defined SHADOW_MAP_OFFSET_POSITION || defined EDITOR
                // Compute offset world vertex pos
                mediump vec3 constantOffset = global_ShadowDir * global_ShadowMapOffsets.x;
                mediump float normalOffsetSlopeScale = clamp(1.0 - dot(worldNormal, global_ShadowDir), 0., 1.);
                mediump vec3 normalOffset = worldNormal * normalOffsetSlopeScale * global_ShadowMapOffsets.y;
                
                highp vec3 offsetPosition = worldPos + normalOffset + constantOffset;
            //#else
            //    highp vec3 offsetPosition = worldPos;
            //#endif

            vSMCoord0 = (global_ShadowMapViewProj * vec4(offsetPosition, 1.)).xyz;

            //vSMCoord0 = (global_ShadowMapViewProjs[0] * vec4(offsetPosition, 1.)).xyz;
            //vSMCoord1 = (global_ShadowMapViewProjs[1] * vec4(offsetPosition, 1.)).xyz;

            //vViewZ = -(global_ShadowMapView * vec4(worldPos, 1.)).z;
        }
	#endif
#endif

#ifdef CURVATURE
	uniform mediump vec2 curveRange;
	uniform highp vec4 curvature;
	uniform highp vec3 curveStartPos;
#endif

varying	mediump vec4 vCoord01      DCC(:TEXCOORD0);
varying mediump vec2 vCoord03	   DCC(:TEXCOORD1);
varying mediump vec4 vColor	       DCC(:TEXCOORD2);
varying mediump vec3 vWNormal 	   DCC(:TEXCOORD3);
varying mediump vec3 vWView  	   DCC(:TEXCOORD4);



mediump vec3 SphereIntersect( mediump vec3 rayStart, mediump vec3 rayDir, mediump float sphereRadiusSq )
{
	mediump vec3 L = -rayStart;
	mediump float tca = dot(L, rayDir);
	mediump float d2 = dot(L,L) - (tca*tca);
	mediump float thc = sqrt(sphereRadiusSq - d2);
	mediump float t0 = tca - thc;
	
	return (rayStart + t0*rayDir);
}

mediump vec3 BoxIntersect( mediump vec3 rayStart, mediump vec3 rayDir, mediump vec3 boxMin, mediump vec3 boxMax)
{
	mediump vec3 inv_ray = 1.0/rayDir;
	
	mediump vec3 t0 = inv_ray*(boxMin-rayStart);
	mediump vec3 t1 = inv_ray*(boxMax-rayStart);
	
//	mediump vec3 minVal = min(t0,t1);
	mediump vec3 maxVal = max(t0,t1);
	
//	mediump float t_min = max(minVal.x, max(minVal.y, minVal.z));
	mediump float t_max = min(maxVal.x, min(maxVal.y, maxVal.z));
	
	return rayStart + t_max*rayDir;
}

mediump vec3 sRGB_To_Linear(lowp vec4 sRGB)
{
	return sRGB.rgb * (sRGB.rgb * (sRGB.rgb * 0.305306011 + 0.682171111) + 0.012522878);
}


/* #ifdef ANIMFX
highp float2 EncodeFloatRG( float v )
{
 float2 kEncodeMul = float2(1.0, 255.0);
 float kEncodeBit = 1.0/255.0;
 float2 enc = kEncodeMul * v;
 enc = frac (enc);
 enc.x -= enc.y * kEncodeBit;
 return enc;
}

float DecodeFloatRG( float2 enc )
{
 float2 kDecodeDot = float2(1.0, 1/255.0);
 return dot( enc, kDecodeDot );
}

#endif 
 */
 
//#define REACTOR_DESIGNER
void main()
{	
	vCoord01 = vec4(TexCoord0.xy, TexCoord1.xy);
	vCoord03 = TexCoord2;
	
	
	#ifdef DCC_MAX
		vCoord01.y += 1.0;
		vCoord01.w += 1.0;
		vCoord03.y += 1.0;
	#endif
	
	#ifdef ANIMFX
		//lowp float Frame = 8.0;
		//lowp float TexSize = 200.0;//textureSize(AnimFXRot).y; //texture height value
		//highp vec2 PanCoords = vec2(vCoord01.x, (floor((vCoord01.y * TexSize) + (time * Speed))/TexSize + (0.5/TexSize)-0.5));
		
			highp vec2 PanCoords = vec2(vCoord01.x, (vCoord01.y + Speed - 0.5));
		// #ifdef SCCON
		// #else 
			// highp vec2 PanCoords = vec2(vCoord01.x, ((vCoord01.y + (time * Speed)) -0.5));
		// #endif
		
		highp vec4 AnimRot = texture2D(AnimFXRot,  PanCoords);
		highp vec4 AnimPos1 = texture2D(AnimFXPos1, PanCoords);
		highp vec4 AnimPos2 = texture2D(AnimFXPos2, PanCoords);

		highp vec4 AnimPos = vec4(
			dot(vec2(AnimPos1.x, AnimPos2.x), vec2(1.0, 1.0/255.0)), 
			dot(vec2(AnimPos1.y, AnimPos2.y), vec2(1.0, 1.0/255.0)), 
			dot(vec2(AnimPos1.z, AnimPos2.z), vec2(1.0, 1.0/255.0)), 
			AnimPos1.w);

		//float2 kDecodeDot = float2(1.0, 1/255.0);
		//return dot( enc, kDecodeDot );
 
		highp vec4 axis_angle = normalize((AnimRot - 0.5) * 2.0);
		//highp vec4 TEMPFloat = WorldViewProjectionMatrix[3] * 1.0;
		
		// vec4(eyepos.xyz, 0.0)
		highp vec4 worldPos = mix (vec4(eyepos, 1.0),//vec4(Vertex.xyz + eyepos + TEMPFloat, 0.0),   // Hidden Vert Location behind camera
			matWorld * //World Matrix
			(vec4((Vertex.xyz + 2.0 * cross(axis_angle.xyz, cross(axis_angle.xyz, Vertex.xyz) + axis_angle.w * Vertex.xyz)), 1.0)  //Rotation at zero point
			+ 
			vec4( // Position within bounding box
				mix((BoundingBox.x/2.0)*-1.0, (BoundingBox.x/2.0) ,AnimPos.x) + BoundsShift.x, //X value Calculation
				mix((BoundingBox.y/2.0)*-1.0, (BoundingBox.y/2.0) ,AnimPos.y) + BoundsShift.y, //Y value Calculation 
				mix((BoundingBox.z/2.0), (BoundingBox.z/2.0)*-1.0 ,AnimPos.z) + BoundsShift.z, //Z value Calculation 
				0.0)
			),
			floor(AnimPos.a)// Mask for Hidden Verts
		); 
		
		vWNormal = normalize(matWorld * vec4(Normal.xyz + 2.0 * cross(axis_angle.xyz, cross(axis_angle.xyz, Normal.xyz) + axis_angle.w * Normal.xyz),0.0)).xyz;
	#else
		highp vec4 worldPos = matWorld*Vertex;
	
		vWNormal = normalize(matWorld * vec4(Normal.xyz,0.0)).xyz;
	#endif
	
	#ifdef CURVATURE
		highp float xDist = abs(curveStartPos.x - worldPos.x);
		mediump float t = clamp(smoothstep(curveRange.x, curveRange.y, xDist), 0.0, 1.0);
		mediump vec4 q = mix( vec4(0,0,0,0), curvature, t );
		worldPos.xyz += 2.0 * cross(q.xyz, cross(q.xyz, worldPos.xyz) + q.w*worldPos.xyz);
	#endif
	
	#ifdef DEFERRED
		vWPosition = worldPos.xyz;
	#else
		#ifdef SHADOWS
			ReceiveShadow(worldPos.xyz, vWNormal);
		#endif
	#endif
	
	mediump vec3 viewDir = normalize( eyepos - worldPos.xyz );
	vWView = viewDir;
	
	#ifndef ANIMFX
		#ifdef DCC_MAX
			#ifdef MFBODY
				vColor = vec4(Color.rgb, Alpha); // Not sRGB because it needs to be used as a mask
			#else
				vColor.rgb = sRGB_To_Linear( vec4(Color,0.0) ) / PI;
				vColor.a = Alpha;
			#endif
			
		#else
			#ifdef MFBODY
				vColor = Color;	// Not sRGB because it needs to be used as a mask
			#else
				mediump vec3 albedo = sRGB_To_Linear(Color) / PI;
				vColor = vec4(albedo, Color.a);		
			#endif

		#endif
	#else
		
		#ifdef DCC_MAX
		
		#else 
			
		#endif
		#ifdef ANIMFXCOL
			highp vec4 AnimCol = texture2D(AnimFXCol, PanCoords);

			vColor.rgb = sRGB_To_Linear( vec4(AnimCol.rgb,0.0) ) / PI;
			vColor.a = 1.0;
		#else
			
			#ifdef DCC_MAX
				vColor.rgb = sRGB_To_Linear( vec4(Color,0.0) ) / PI;
				vColor.a = Alpha;
			#else 
				mediump vec3 albedo = sRGB_To_Linear(Color) / PI;
				vColor = vec4(albedo, Color.a);	
			#endif
		#endif
	#endif

	
	gl_Position = matviewprojection * worldPos;

} // main end
