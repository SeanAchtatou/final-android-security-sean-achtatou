#version 300 es
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif


#define PI 3.141592

// varyings
varying highp vec3 vNormal;

// attributes
attribute highp vec4 position;
attribute highp vec3 normal;

// uniforms
uniform highp mat4 worldviewprojection;

void main(void)
{
    vNormal = normal;
	gl_Position = worldviewprojection * position;
}

