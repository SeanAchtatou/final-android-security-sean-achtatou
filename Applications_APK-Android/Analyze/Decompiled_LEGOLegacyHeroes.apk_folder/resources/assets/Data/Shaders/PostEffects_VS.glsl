#version 300 es
// # 1 "E:/Lego/Trunk/Engine/Data/Shaders/Engine/include/Version.gx"
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif


// # 4 "E:/Lego/Trunk/Data/Shaders/PostEffects.gx"

//#define DEBUG_VIEW
// varyings
varying highp vec2 ScreenUV;


// attributes
attribute highp vec4 position;
attribute highp vec2 TexCoord0;

// uniforms
uniform highp mat4 worldviewprojection;

void main(void)
{
    ScreenUV.xy = TexCoord0.xy;
    gl_Position = worldviewprojection * position;
}



