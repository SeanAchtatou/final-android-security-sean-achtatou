#version 300 es

#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif


#ifdef DCC_MAX
    #define DCC(x) x
#else
    #define DCC(x)
#endif


varying	mediump vec2 vCoord0 DCC(: TEXCOORD4);
varying	mediump vec2 vCoord1 DCC(: TEXCOORD5);

#ifndef IGNORE_VERT_COLOR
varying mediump vec4 vColor  DCC(: TEXCOORD6);
#endif

uniform lowp 	sampler2D	Diffuse;

// Gamma 1.0 to 2.2
mediump vec3 Linear_To_sRGB(mediump vec3 lin)
{
    return pow( lin, vec3(0.45) );
}

// Gamma 2.2 to 1.0
mediump vec3 sRGB_To_Linear(mediump vec3 sRGB)
{
	// cubic is a faster approximation of the pow function
	//return sRGB.rgb * (sRGB.rgb * (sRGB.rgb * 0.305306011 + 0.682171111) + 0.012522878);
	mediump vec3 a = sRGB.rgb * 0.305306011 + 0.682171111;
	mediump vec3 b = sRGB.rgb * a + 0.012522878;
	return sRGB.rgb * b;
}


void main()
{
    mediump vec4 diffuseMap = texture2D(Diffuse, vCoord0);
    #if !defined(ADD)
        diffuseMap.a = texture2D(Diffuse, vCoord1).a;
    #endif

    #ifdef IGNORE_VERT_COLOR
        mediump vec4 finalColor = diffuseMap;
    #else
        mediump vec4 finalColor = diffuseMap * vColor;
    #endif
    
    #if defined(ADD)
        finalColor.rgb = sRGB_To_Linear(finalColor.rgb);
    #endif

    #ifndef DCC_MAX
        #if defined(DEPTH_WRITE)
            #if defined(ADD)
                mediump float depthAlpha = max (max (finalColor.r, finalColor.g), finalColor.b);
            #else
                mediump float depthAlpha = finalColor.a;
            #endif
            gl_FragDepth = mix(1.0, gl_FragCoord.z, ceil (depthAlpha-0.5) );
        #endif
    #endif

	gl_FragColor = finalColor;
}
