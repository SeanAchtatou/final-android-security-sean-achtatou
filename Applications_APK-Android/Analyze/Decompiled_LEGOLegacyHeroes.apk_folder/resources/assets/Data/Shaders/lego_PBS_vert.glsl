#ifdef DCC_MAX
	#define DCC(x) x
#else
	#define DCC(x)
#endif
 
#define PI 3.14159265359
 
attribute highp   vec4 Vertex DCC(: POSITION);

#ifdef DCC_MAX
	attribute lowp vec3  Color : TEXCOORD3;
	attribute lowp float Alpha : TEXCOORD4;
#else
	attribute lowp vec4  Color;
#endif

attribute mediump vec4 Normal DCC(: NORMAL);
attribute mediump vec4 Tangent DCC(: TANGENT);
attribute mediump vec2 TexCoord0 DCC(: TEXCOORD0);
attribute mediump vec2 TexCoord1 DCC(: TEXCOORD1);

uniform highp 	mat4 matWorld;
uniform highp   mat4 WorldViewProjectionMatrix;
uniform highp 	mat4 ShadowMapViewProj;

uniform mediump vec3 eyepos;

uniform highp float sh_data[27];

varying	mediump vec4 vCoord01      DCC(:TEXCOORD0);
varying mediump vec4 vColor	       DCC(:TEXCOORD1);
varying mediump vec3 vWNormal 	   DCC(:TEXCOORD2);
varying mediump vec3 vWView  	   DCC(:TEXCOORD3);
varying mediump vec3 vIrradiance   DCC(:TEXCOORD4);


#ifdef SHADOWS
	varying highp vec4 shadowPos   DCC(:TEXCOORD7);
#endif

mediump vec3 sRGB_To_Linear(lowp vec4 sRGB)
{
	return sRGB.rgb * (sRGB.rgb * (sRGB.rgb * 0.305306011 + 0.682171111) + 0.012522878);
}

vec3 EvaluateSH(const vec3 dir )
{
	vec4 vNormal = vec4(dir, 1.0);
	mediump vec4 cAr = vec4(sh_data[0],  sh_data[1],  sh_data[2],  sh_data[3] );
	mediump vec4 cAg = vec4(sh_data[4],  sh_data[5],  sh_data[6],  sh_data[7] );
	mediump vec4 cAb = vec4(sh_data[8],  sh_data[9],  sh_data[10], sh_data[11]);
	mediump vec4 cBr = vec4(sh_data[12], sh_data[13], sh_data[14], sh_data[15]);
	mediump vec4 cBg = vec4(sh_data[16], sh_data[17], sh_data[18], sh_data[19]);
	mediump vec4 cBb = vec4(sh_data[20], sh_data[21], sh_data[22], sh_data[23]);
	mediump vec3 cC =  vec3(sh_data[24], sh_data[25], sh_data[26]);

	vec3 x1, x2, x3;

	// Linear + constant polynomial terms
	x1.r = dot(cAr, vNormal);
	x1.g = dot(cAg, vNormal);
	x1.b = dot(cAb, vNormal);

	// 4 of the quadratic polynomials
	vec4 vB = vNormal.xyzz * vNormal.yzzx;
	x2.r = dot(cBr, vB);
	x2.g = dot(cBg, vB);
	x2.b = dot(cBb, vB);

	// Final quadratic polynomial
	float vC = vNormal.x*vNormal.x - vNormal.y*vNormal.y;
	x3 = cC.rgb * vC;

	return max(x1 + x2 + x3, vec3(0.0));
}

//#define REACTOR_DESIGNER
void main()
{	
	gl_Position = WorldViewProjectionMatrix * Vertex;

	vCoord01 = vec4(TexCoord0.xy, TexCoord1.xy); 
	
	#ifdef DCC_MAX
		vCoord01.y += 1.0;
		vCoord01.w += 1.0;
	#endif
	
	highp vec4 worldPos = matWorld*Vertex;
	
	#ifdef SHADOWS
		shadowPos = ShadowMapViewProj * worldPos;
	#endif
	
	mediump vec3 viewDir = normalize( eyepos - worldPos.xyz );	
	vWView = viewDir;
	vWNormal = normalize(matWorld * vec4(Normal.xyz,0.0)).xyz;
	
	vIrradiance = EvaluateSH( vWNormal );
	
	#ifdef DCC_MAX
		vColor.rgb = sRGB_To_Linear( vec4(Color,0.0) ) / PI;
		vColor.a = Alpha;
	#else 
		mediump vec3 albedo = sRGB_To_Linear(Color) / PI;
		vColor = vec4(albedo, Color.a);
	#endif

} // main end
