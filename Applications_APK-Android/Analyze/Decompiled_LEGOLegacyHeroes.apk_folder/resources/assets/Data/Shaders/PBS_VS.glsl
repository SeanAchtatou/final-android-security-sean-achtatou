#version 300 es

//#if defined(MESHWRAP) && !defined(DISABLE_DOF)
//    #extension GL_EXT_frag_depth : enable
//#endif

#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif

#ifdef SHADOWS
#define SHADOW_MAP_CASCADE_COUNT 2
varying highp vec3 vSMCoord0;
//varying highp vec3 vSMCoord1;
//varying highp float vViewZ;
#endif

#ifdef SHADOWS
//uniform highp mat4 global_ShadowMapView;

//#if defined SHADOW_MAP_OFFSET_POSITION || defined EDITOR
uniform mediump vec3 global_ShadowDir;
uniform mediump vec2 global_ShadowMapOffsets;
//#endif

//uniform highp mat4 global_ShadowMapViewProjs[SHADOW_MAP_CASCADE_COUNT];
uniform highp mat4 global_ShadowMapViewProj;

void ReceiveShadow(highp vec3 worldPos, mediump vec3 worldNormal)
{
    //#if defined SHADOW_MAP_OFFSET_POSITION || defined EDITOR
        // Compute offset world vertex pos
        mediump vec3 constantOffset = global_ShadowDir * global_ShadowMapOffsets.x;
        mediump float normalOffsetSlopeScale = clamp(1.0 - dot(worldNormal, global_ShadowDir), 0., 1.);
        mediump vec3 normalOffset = worldNormal * normalOffsetSlopeScale * global_ShadowMapOffsets.y;
        
        highp vec3 offsetPosition = worldPos + normalOffset + constantOffset;
    //#else
    //    highp vec3 offsetPosition = worldPos;
    //#endif

    vSMCoord0 = (global_ShadowMapViewProj * vec4(offsetPosition, 1.)).xyz;

    //vSMCoord0 = (global_ShadowMapViewProjs[0] * vec4(offsetPosition, 1.)).xyz;
    //vSMCoord1 = (global_ShadowMapViewProjs[1] * vec4(offsetPosition, 1.)).xyz;

    //vViewZ = -(global_ShadowMapView * vec4(worldPos, 1.)).z;
}
#endif


#if (!defined(ALLOW_SHADOW_BLOBS) || defined(SHADOWS)) && defined(SHADOW_BLOB)
	#undef SHADOW_BLOB
#endif



#ifdef DISABLE_LIGHTMAP
	#undef LIGHTMAP
#endif

// Extensions

#if defined(DISABLE_REFLECTIONS) && defined(REFLECTIONS)
	#undef REFLECTIONS
#endif

#if !defined(GL_OES_standard_derivatives) || defined(LOW_END)
    #ifdef CURVSMOOTH
        #undef CURVSMOOTH
    #endif
#endif

#ifdef DCC_MAX
    #define DCC(x) x
#else
    #define DCC(x)
#endif

#define PI 3.14159265359
#define ONE_OVER_PI 0.3183098862



// uniforms 
uniform highp mat4 omniLightMatrices[1];

#ifdef ALTERNATIVE_LIGHT_SOURCE
	uniform mediump vec3 SunDirection;
	uniform mediump vec4 SunColor;
	uniform mediump vec3 ShadowColor;

	#define global_SunDirection		SunDirection
	#define global_SunColor			SunColor
	#define global_ShadowColor	    ShadowColor
#else
	uniform mediump vec3 global_SunDirection;
	uniform mediump vec4 global_SunColor;
	uniform mediump vec3 global_ShadowColor;
#endif

uniform highp vec4 eyepos;
uniform highp vec4 eyeposos;

// varyings
varying mediump vec4 vTexCoord;
varying mediump vec3 vWNormal;
varying mediump vec3 vWTangent;
varying mediump vec3 vWBinormal;
varying mediump vec3 vWLightDir;
varying mediump vec3 vWorldPos;
varying mediump vec4 vColor;

//#ifdef GL_EXT_frag_depth
//    varying mediump float vDepth;
//#endif



#ifdef PIPNORMAL	
    varying mediump float PipMask;
#endif

#ifdef TRIPLANAR
    varying mediump vec2 vTriPlanUV_X;
    varying mediump vec2 vTriPlanUV_Y;
    varying mediump vec2 vTriPlanUV_Z;
#endif

// Gamma 2.2 to 1.0
mediump vec3 sRGB_To_Linear(mediump vec3 sRGB)
{
	// cubic is a faster approximation of the pow function
	return sRGB.rgb * (sRGB.rgb * (sRGB.rgb * 0.305306011 + 0.682171111) + 0.012522878);
}

// Gamma 1.0 to 2.2
mediump vec3 Linear_To_sRGB(mediump vec3 lin)
{
    return pow( lin, vec3(0.45) );
}

// Calculates the L1 spherical harmonics value for a specified directional vector.
// cC corresponds to the L0 coefficients.
// cAr, cAg, cAb are the L1 coefficients organized by color channel.
mediump vec3 EvaluateL1SH(mediump vec3 dir, mediump vec3 cAr, mediump vec3 cAg, mediump vec3 cAb, mediump vec3 cC)
{
    mediump float x1 = dot(dir.yzx, cAr);
    mediump float x2 = dot(dir.yzx, cAg);
    mediump float x3 = dot(dir.yzx, cAb);
    return ((0.886227*cC) + (1.023328*vec3(x1, x2, x3)));
}

// Returns the luma (brightness) of a color.
mediump float Luma(mediump vec3 color)
{
    return dot(color, vec3(0.2126, 0.7152, 0.0722));
}


#if defined(ANIMFX) || defined(MESHWRAPEFFECT)
    uniform highp float Speed;
#endif

// attributes
attribute highp vec4 Vertex DCC(: POSITION);
attribute mediump vec4 Normal DCC(: NORMAL);
attribute mediump vec4 Tangent DCC(: TANGENT0);


#ifdef USEATT_UVONE
    attribute mediump vec2 TexCoord0 DCC(: TEXCOORD0);
#endif

#ifdef USEATT_UVTWO
    attribute mediump vec2 TexCoord1 DCC(: TEXCOORD1);
#endif

#ifdef USEATT_COL
    #ifdef DCC_MAX
        attribute lowp vec3  Color : TEXCOORD3;
        attribute lowp float Alpha : TEXCOORD4;
    #else
        attribute lowp vec4  Color;  
    #endif
#endif    

// uniforms
#ifdef ANIMFX
    uniform highp mat4 matviewprojection;
#else
    uniform highp mat4 worldviewprojection;
#endif

//#ifdef GL_EXT_frag_depth
//    uniform highp vec2 global_CamProjDepthLinearOps;
//    uniform highp mat4 matmodelview;
//    uniform highp mat4 projection;
//#endif



uniform highp mat4 matworld;
uniform highp mat4 matworldinv;

#ifdef ANIMFX
    uniform highp sampler2D AnimFXPos1;
    uniform highp sampler2D AnimFXPos2;
    uniform highp sampler2D AnimFXRot;
    uniform highp float time;
    uniform highp vec3 BoundsShift;
    uniform highp vec3 BoundingBox;
#endif

#ifdef SLICE
    //uniform highp float time;
    uniform highp float RotationAmount; // Deg / 180 * pi
    uniform highp vec2 SliceOffset;
#endif

#ifdef ANIMCOL
    uniform highp sampler2D AnimFXCol;
#endif

#ifdef GHOST
    uniform lowp float StrokeBrightness;
#endif


#ifdef SKINNED

uniform highp mat4 BoneMatrices[48];
uniform highp vec4 WeightMask;

attribute highp vec4 SkinWeights;
attribute mediump vec4 SkinIndices;

struct SkinResult 
{
    highp vec3 Position;
    highp vec3 Normal;
    highp vec3 Tangent;
};

SkinResult SkinAll()
{
    SkinResult result;
	highp vec4 weights = SkinWeights * WeightMask;

    highp mat4 amt0 = BoneMatrices[int(SkinIndices[0])];
    highp mat4 amt1 = BoneMatrices[int(SkinIndices[1])];
    highp mat4 amt2 = BoneMatrices[int(SkinIndices[2])];

    result.Position = weights[0] * (amt0 * vec4(Vertex.xyz, 1.0)).xyz;
    result.Position += weights[1] * (amt1 * vec4(Vertex.xyz, 1.0)).xyz;
    result.Position += weights[2] * (amt2 * vec4(Vertex.xyz, 1.0)).xyz;
	
	mediump mat3 boneMatrix = mat3(BoneMatrices[int(SkinIndices[0])]);

	result.Normal = normalize(boneMatrix * Normal.xyz);
	result.Tangent = normalize(boneMatrix * Tangent.xyz);
	
	return result;
}

#endif


//Fog
#ifdef VFOG
    uniform lowp sampler2D global_FogGradientTex;
    uniform lowp float global_CameraFogMax;
#endif

#ifndef SHADOWLOD
void VertPBR()
{
	#ifdef SKINNED
		SkinResult skinResult = SkinAll();
		highp vec3 inPos = skinResult.Position;
		highp vec3 inNormal = skinResult.Normal;
		highp vec3 inTangent = skinResult.Tangent;
	#else
		highp vec3 inPos = Vertex.xyz;
		highp vec3 inNormal = Normal.xyz;
		highp vec3 inTangent = Tangent.xyz;
	#endif

    #if defined(USEATT_UVONE) && !defined(USEATT_UVTWO)
            vTexCoord = vec4( TexCoord0, TexCoord0 );
    #elif defined(USEATT_UVONE) && defined(USEATT_UVTWO)
            vTexCoord = vec4( TexCoord0, TexCoord1 );
    #elif !defined(USEATT_UVONE) && defined(USEATT_UVTWO)
            vTexCoord = vec4( TexCoord1, TexCoord1 );
    #endif 


    
    #ifdef DCC_MAX
        vTexCoord.y += 1.0;
        vTexCoord.w += 1.0;
    #endif 


    #ifdef ANIMFX  

        //highp vec2 PanCoords = vec2(vTexCoord.x, (vTexCoord.y + Speed - 0.5));
        highp vec2 PanCoords = vec2(vTexCoord.z, (vTexCoord.w + Speed - 0.5));


        highp vec4 AnimRot = texture2D(AnimFXRot,  PanCoords);
        highp vec4 AnimPos1 = texture2D(AnimFXPos1, PanCoords);
        highp vec4 AnimPos2 = texture2D(AnimFXPos2, PanCoords);

        highp vec4 AnimPos = vec4(
            dot(vec2(AnimPos1.x, AnimPos2.x), vec2(1.0, 1.0/255.0)), 
            dot(vec2(AnimPos1.y, AnimPos2.y), vec2(1.0, 1.0/255.0)), 
            dot(vec2(AnimPos1.z, AnimPos2.z), vec2(1.0, 1.0/255.0)), 
            AnimPos1.w);
    
        highp vec4 axis_angle = normalize((AnimRot - 0.5) * 2.0);
        
        highp vec4 worldPos = mix ( eyepos,   // Hidden Vert Location behind camera
                matworld * //World Matrix
                (vec4((inPos + 2.0 * cross(axis_angle.xyz, cross(axis_angle.xyz, inPos) + axis_angle.w * inPos)), 1.0)  //Rotation at zero point
                + 
                vec4( // Position within bounding box
                    mix((BoundingBox.x/2.0)*-1.0, (BoundingBox.x/2.0) ,AnimPos.x) + BoundsShift.x, //X value Calculation
                    mix((BoundingBox.y/2.0)*-1.0, (BoundingBox.y/2.0) ,AnimPos.y) + BoundsShift.y, //Y value Calculation 
                    mix((BoundingBox.z/2.0), (BoundingBox.z/2.0)*-1.0 ,AnimPos.z) + BoundsShift.z, //Z value Calculation 
                    0.0)
            ),
            floor(AnimPos.a)// Mask for Hidden Verts
        ); 
            
        mediump vec3 wNormal  = normalize(matworld * vec4(inNormal + 2.0 * cross(axis_angle.xyz, cross(axis_angle.xyz, inNormal) + axis_angle.w * inNormal),0.0)).xyz;
        mediump vec3 wTangent = normalize(matworld * vec4(inTangent + 2.0 * cross(axis_angle.xyz, cross(axis_angle.xyz, inTangent) + axis_angle.w * inTangent),0.0)).xyz;
        
    #else

        highp vec4 worldPos = matworld * vec4(inPos, 1.0);
        mediump vec3 wNormal  = normalize( (matworld * vec4(inNormal, 0.0)).xyz );
        mediump vec3 wTangent = normalize( (matworld * vec4(inTangent, 0.0)).xyz );
        
    #endif  
    
        mediump vec3 wBinormal = normalize( cross(wNormal.xyz, wTangent.xyz) );
        mediump vec3 wLightDir = global_SunDirection.xyz;
        
    #ifdef TRIPLANAR
        const mediump float triPlanScale = 1.0;
        vTriPlanUV_X = triPlanScale * worldPos.zy;
        vTriPlanUV_Y = triPlanScale * worldPos.xz;
        vTriPlanUV_Z = triPlanScale * worldPos.xy;
    #endif
        
    mediump vec3 vertexColor = vec3(0.0);	 
    mediump float vertexAlpha = 0.0;
        
    #ifdef ANIMFX
        #ifdef ANIMCOL
            highp vec4 AnimCol = texture2D(AnimFXCol, PanCoords);

            vertexColor = ONE_OVER_PI * sRGB_To_Linear( AnimCol.rgb );
            vertexAlpha = 1.0;
        #else
            #ifdef DCC_MAX
                vertexColor = ONE_OVER_PI * sRGB_To_Linear( Color.rgb );
                vertexAlpha = Alpha;
            #else 
                vertexColor = ONE_OVER_PI * sRGB_To_Linear( Color.rgb );
                vertexAlpha = Color.a;	
            #endif
        #endif
    #elif defined(VERTCOLOR)
        #ifdef DCC_MAX
            vertexColor = ONE_OVER_PI * sRGB_To_Linear( Color.rgb );
            vertexAlpha = Alpha;
        #else
            vertexColor = ONE_OVER_PI * sRGB_To_Linear( Color.rgb );
            vertexAlpha = Color.a;
        #endif
    #endif    
        
    vWNormal = wNormal;
    vWTangent = wTangent;
    vWBinormal = wBinormal;
    
    vWLightDir = wLightDir;
    vWorldPos = worldPos.xyz;

    #ifdef SHADOWS
        ReceiveShadow(vWorldPos, vWNormal);
    #endif

    #ifdef GHOST
        vertexAlpha = StrokeBrightness;
    #endif
    
    #ifdef VFOG
        highp float dist = distance (eyepos.rgb, worldPos.xyz);
        vColor = texture2D( global_FogGradientTex, vec2( min(dist, global_CameraFogMax) / global_CameraFogMax, 0.0) );
    #else
        vColor = vec4( vertexColor, vertexAlpha );
    #endif

    #ifdef PIPNORMAL	
        PipMask =  clamp( ceil(vTexCoord.x), 0.0, 1.0 );
    #endif


#ifdef SLICE
    highp float s = sin ( RotationAmount );
    highp float c = cos ( RotationAmount );
    highp mat2 rotationMatrix = mat2( c, s, -s, c);
    rotationMatrix *= 0.5;
    rotationMatrix = rotationMatrix + mat2(0.5);
    rotationMatrix *= 2.0 ;
    rotationMatrix = rotationMatrix - mat2(1.0);
    
    //Rotate around center by default
    highp vec2 pivot = vec2(0.5, 0.5) + SliceOffset;
    vTexCoord.zw = (rotationMatrix * (vTexCoord.zw - pivot)) + pivot - SliceOffset;
    
#endif


    //#ifdef GL_EXT_frag_depth
    //    highp vec4 cPos = projection * matmodelview * Vertex;
    //    highp float ndc_z = cPos.z / cPos.w;
    //    vDepth = ndc_z*global_CamProjDepthLinearOps.x + global_CamProjDepthLinearOps.y;
    //#endif  

    #ifdef ANIMFX 
        gl_Position = matviewprojection * worldPos;
    #else
        gl_Position = worldviewprojection * vec4(inPos, 1.0);
    #endif
}
#endif

#ifdef SHADOWLOD
void VertShadowLOD()
{
	#ifndef ANIMFX 
		#ifdef SKINNED
			SkinResult skinResult = SkinAll();
			gl_Position = worldviewprojection * vec4(skinResult.Position, 1.0);
		#else
			gl_Position = worldviewprojection * Vertex;
		#endif
    #else

        #if defined(USEATT_UVONE) && !defined(USEATT_UVTWO)
                vTexCoord = vec4( TexCoord0, TexCoord0 );
        #elif defined(USEATT_UVONE) && defined(USEATT_UVTWO)
                vTexCoord = vec4( TexCoord0, TexCoord1 );
        #elif !defined(USEATT_UVONE) && defined(USEATT_UVTWO)
                vTexCoord = vec4( TexCoord1, TexCoord1 );
        #endif

        highp vec3 inPos = Vertex.xyz;

        highp vec2 PanCoords = vec2(vTexCoord.z, (vTexCoord.w + Speed - 0.5));

        highp vec4 AnimRot = texture2D(AnimFXRot,  PanCoords);
        highp vec4 AnimPos1 = texture2D(AnimFXPos1, PanCoords);
        highp vec4 AnimPos2 = texture2D(AnimFXPos2, PanCoords);

        highp vec4 AnimPos = vec4(
            dot(vec2(AnimPos1.x, AnimPos2.x), vec2(1.0, 1.0/255.0)), 
            dot(vec2(AnimPos1.y, AnimPos2.y), vec2(1.0, 1.0/255.0)), 
            dot(vec2(AnimPos1.z, AnimPos2.z), vec2(1.0, 1.0/255.0)), 
            AnimPos1.w);
    
        highp vec4 axis_angle = normalize((AnimRot - 0.5) * 2.0);
        
        highp vec4 worldPos = mix ( eyepos,   // Hidden Vert Location behind camera
                matworld * //World Matrix
                (vec4((inPos + 2.0 * cross(axis_angle.xyz, cross(axis_angle.xyz, inPos) + axis_angle.w * inPos)), 1.0)  //Rotation at zero point
                + 
                vec4( // Position within bounding box
                    mix((BoundingBox.x/2.0)*-1.0, (BoundingBox.x/2.0) ,AnimPos.x) + BoundsShift.x, //X value Calculation
                    mix((BoundingBox.y/2.0)*-1.0, (BoundingBox.y/2.0) ,AnimPos.y) + BoundsShift.y, //Y value Calculation 
                    mix((BoundingBox.z/2.0), (BoundingBox.z/2.0)*-1.0 ,AnimPos.z) + BoundsShift.z, //Z value Calculation 
                    0.0)
            ),
            floor(AnimPos.a)// Mask for Hidden Verts
        ); 

        gl_Position = matviewprojection * worldPos;
	#endif
}
#endif

void main()
{
    #ifdef SHADOWLOD 
        VertShadowLOD();
    #else
        VertPBR();
    #endif
}