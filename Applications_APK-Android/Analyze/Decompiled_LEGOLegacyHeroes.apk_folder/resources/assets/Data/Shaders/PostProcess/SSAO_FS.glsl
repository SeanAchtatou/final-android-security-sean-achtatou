#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
  #define ES3
#endif

#if defined(GL_OES_standard_derivatives)
  #extension GL_OES_standard_derivatives : enable
  #define USE_DERIVATIVES
#else
#endif

#if defined(GL_EXT_shader_framebuffer_fetch)
  #extension GL_EXT_shader_framebuffer_fetch : enable
  #define USE_LAST_FRAG_DATA
#endif

#ifdef ES3
  #define varying in
  #define texture2D texture
  #define gl_FragColor fragData0
  #define frag0 fragData0
  #define frag1 fragData1
  #define frag2 fragData2
  #define frag3 fragData3
  #if (defined USE_LAST_FRAG_DATA && defined USE_SOFT)
    layout (location = 0) inout highp vec4 fragData0;
    layout (location = 1) inout highp vec4 fragData1;
    layout (location = 2) inout highp vec4 fragData2;
    layout (location = 3) inout highp vec4 fragData3;
  #else
    layout (location = 0) out highp vec4 fragData0;
    layout (location = 1) out highp vec4 fragData1;
    layout (location = 2) out highp vec4 fragData2;
    layout (location = 3) out highp vec4 fragData3;
  #endif
  #define shadowmapSampler sampler2Dshadow
#else
  #define shadowmapSampler sampler2D
  #define frag0 gl_FragColor[0]
  #define frag1 gl_FragColor[1]
  #define frag2 gl_FragColor[2]
  #define frag3 gl_FragColor[3]
#endif




varying lowp  vec2   vUV;

uniform highp mat4 WorldViewProjection;
uniform highp float time;


 highp vec3 rdm;
 highp vec3 test;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D normalSampler;

uniform highp float radius;

uniform highp mat4 vpiMat;
uniform highp mat4 vpMat;
uniform highp mat4 piMat;
uniform highp mat4 pMat;
uniform highp mat4 vpMatPast;
uniform highp vec3 camPosition;

uniform highp float normalContribution;

uniform lowp int ssaoEnabled;
uniform lowp float motion;

uniform lowp float ssaoStrength;

uniform lowp vec3 global_ShadowColor;

#define RAND_TEXTURE
#define HASHSCALE1 .1031
#define HASHSCALE3 vec3(.1031, .1030, .0973)
#define HASHSCALE4 vec4(1031, .1030, .0973, .1099)
highp vec3 rand(highp vec2 p)
{
#ifdef RAND_TEXTURE
    return (texture2D(texture2, fract(p)).rgb * 2. - 1.);
#else
	  highp vec3 p3 = fract(vec3(p.xyx) * HASHSCALE3);
    p3 += dot(p3, p3.yxz + 19.19);
    highp vec3 r = fract(vec3((p3.x + p3.y) * p3.z, (p3.x + p3.z) * p3.y, (p3.y + p3.z) * p3.x));
    r = r * vec3(2., 2., 2.) - vec3(1., 1., 1.);
    return r;
#endif
}

highp vec3 screenToWorld(highp vec3 p)
{
    highp vec4 t = vec4(p.x, 1. - p.y, p.z, 1.) * 2. - 1.;
    t = vpiMat * t;
    return t.xyz / t.w;
}

highp vec3 worldToScreen(highp vec3 p)
{
	  highp vec4 t = vec4(p, 1.);
    t = vpMat * t;
    t.xyz /= t.w;
    t = t * .5 + .5;
    t.y = 1. - t.y;
    return t.xyz;
}

highp vec3 worldToPastScreen(highp vec3 p)
{
	  highp vec4 t = vec4(p, 1.);
    t = vpMatPast * t;
    t.xyz /= t.w;
    t = t * .5 + .5;
    t.y = 1. - t.y;
    return t.xyz;
}

highp vec3 screenToView(highp vec3 p)
{
    highp vec4 t = vec4(p.x, 1. - p.y, p.z, 1.) * 2. - 1.;
    t = piMat * t;
    return t.xyz / t.w;
}

highp vec3 viewToScreen(highp vec3 p)
{
	  highp vec4 t = vec4(p, 1.);
    t = pMat * t;
    t.xyz /= t.w;
    t = t * .5 + .5;
    t.y = 1. - t.y;
    return t.xyz;
}

highp float diff(highp vec3 p, highp vec3 offset)
{
    highp float d = 0.;
    highp vec3 po = p + offset;

  #ifdef DEFERRED

    highp vec2 uv = worldToScreen(po).xy;
    uv = clamp(uv, vec2(0.001), vec2(.999));
    highp float poz = texture2D(texture1, uv).r;
    highp vec3 poW = screenToWorld(vec3(uv, poz));
    d = dot(po - poW, po - camPosition);

  #else

    highp vec2 uv = viewToScreen(po).xy;
    uv = clamp(uv, vec2(0.001), vec2(.999));
    highp float poz = texture2D(texture1, uv).r;
    d = screenToView(vec3(po.xy, poz)).z - p.z;

  #endif

    return step(.01, d) * step(d, 1. * radius);
}

void main()
{	  
    rdm = vec3(3425.245);
    test = rdm;
    
    highp float ssao = 0.;
    
	#ifdef APPLY

		highp vec4 c = texture2D(texture0, vUV);
		ssao = texture2D(texture1, vUV).r;
		ssao = smoothstep(0., .5, ssao);
		ssao = pow(ssao, ssaoStrength);
		
		#ifdef DEBUG
			gl_FragColor = vec4(vec3(ssao), 1.0);
		#else
			gl_FragColor = vec4(mix(c.rgb * global_ShadowColor, c.rgb, vec3(ssao)), c.a);
		#endif
    
		return; 
    
	#else
    
		//highp float z = min(.99999, texture2D(texture1, vUV).r);
		highp float z = texture2D(texture1, vUV).r;
		highp vec3 pS = vec3(vUV, z);
		highp vec3 pW = screenToWorld(vec3(vUV, z));

		#ifdef DEFERRED

			highp vec3 normal = texture2D(normalSampler, vUV).rgb;

			highp int samples = 3;
			for(int r = 1; r <= samples; r++)
			{
				highp float size = float(r) / float(samples);
				size = radius * size * size;
				highp vec3 offset = size * (rand(float(r) * vUV * time * 100.));
				offset *= sign(dot(normal, offset));
				ssao += diff(pW, offset);
			}
		#else

			highp vec3 pV = screenToView(pS);

			highp int samples = 4;
			for(int r = 1; r <= samples; r++)
			{
				highp float size = float(r) / float(samples);
				size = radius * size * size;
				highp vec3 offset = size * rand(float(r) * vUV * time * 100.);
				ssao += diff(pV, offset);
			}

		#endif

		ssao /= float(samples);
		ssao = 1. - ssao;

		highp vec2 pastUV = worldToPastScreen(pW).xy;
		highp float pastssao = texture2D(texture0, pastUV).r;
		highp float pastz = texture2D(texture1, pastUV).r;

		highp float blend = motion == 1. ? 0.05 : 0.05;
		ssao = mix(pastssao, ssao, blend);

		ssao = ssaoEnabled == 1 ? ssao : 1.;
		
		gl_FragColor = vec4(ssao);

	#endif
	
	//if(rdm != test) gl_FragColor = gl_FragColor * .00001 + vec4(test, 1.);
}


