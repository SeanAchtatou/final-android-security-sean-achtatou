// # 1 "E:/Lego/Trunk/Data/Shaders/PostProcess/DepthOfField.gx"

// # 1 "E:/Lego/Trunk/Engine/Data/Shaders/Engine/include/Version.gx"
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif


// # 3 "E:/Lego/Trunk/Data/Shaders/PostProcess/DepthOfField.gx"

// varyings
varying highp vec2 vCoord00;
varying highp vec2 vCoord11;


// attributes
attribute highp vec4 Vertex;
attribute highp vec2 TexCoord0;

// uniforms
uniform highp mat4 WorldViewProjectionMatrix;

#if defined(HORIZ) || defined(VERT) || defined(COMBINE)
    uniform highp vec2 texture0_sizeinv;
#endif

#if defined(HORIZ) || defined(VERT)
    uniform highp vec2 texture0_size;
    varying highp vec2 vCoord0;
    varying highp vec2 vCoord1;
    varying highp vec2 vCoord2;
    varying highp vec2 vCoord3;
    varying highp vec2 vCoord4;
    varying highp vec2 vCoord5;
    varying highp vec2 vCoord6;
#endif


void main(void)
{
    gl_Position = WorldViewProjectionMatrix * Vertex;
    vCoord11 = TexCoord0.xy;
    vCoord00 = TexCoord0.xy;

    #if defined(HORIZ) || defined(VERT)
        
        highp vec2 twoPx = 2.0 * texture0_sizeinv;
        //assign all offsets the original coord

        #if defined(VERT)
            vCoord11 += texture0_sizeinv / 2.0;
        #endif
        vCoord0 = vCoord1 = vCoord2 = vCoord3 = vCoord4 = vCoord5 = vCoord6 = vCoord11;
        
        //perturb whichever direction in -/+2px steps
        #ifdef HORIZ
            int i = 0; //x offsets
        #elif defined(VERT)
            int i = 1; //y offsets
        #endif
    

        highp float offset = twoPx[i];

        vCoord2[i] -= offset;
        vCoord4[i] += offset;
        
        offset += twoPx[i];
        vCoord1[i] -= offset;
        vCoord5[i] += offset;
        
        offset += twoPx[i];
        vCoord0[i] -= offset;
        vCoord6[i] += offset;
    #endif
}


