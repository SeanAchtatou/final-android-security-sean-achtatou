// # 1 "E:/Lego/Trunk/Data/Shaders/PostProcess/DepthOfField.gx"

// # 18 "E:/Lego/Trunk/Engine/Data/Shaders/Engine/include/Version.gx"
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif


// # 3 "E:/Lego/Trunk/Data/Shaders/PostProcess/DepthOfField.gx"

// varyings
varying highp vec2 vCoord00;
varying highp vec2 vCoord11;


// # 72 "E:/Lego/Trunk/Data/Shaders/PostProcess/DepthOfField.gx"

uniform lowp sampler2D texture0; //usual source


#if defined(PREPDEPTH)
    GLITCH_UNIFORM_PROPERTIES(texture1, (depthtexture = 1))
    uniform mediump sampler2D texture1; //depth
    //stuff to lerp on depth with
    uniform   highp float ratio;
    uniform   highp float focus;
    uniform   highp float focusRange;
    uniform   highp float fNear;
    uniform   highp float fFar;
#endif

uniform lowp  sampler2D origSource; //alt source


#if defined(HORIZ) || defined(VERT)
    uniform lowp sampler2D flipFlop;	//checkerboard texture
    uniform highp vec2 texture0_sizeinv;
    varying highp vec2 vCoord0;
    varying highp vec2 vCoord1;
    varying highp vec2 vCoord2;
    varying highp vec2 vCoord3;
    varying highp vec2 vCoord4;
    varying highp vec2 vCoord5;
    varying highp vec2 vCoord6;
#endif



void main()
{
    //This Encodes the corrected depth buffer into the alpha channel
    #if defined(PREPDEPTH)
        highp vec4 DepthTexture = texture2D(texture1, vCoord00);
        highp float fEyeZ = (fNear * fFar) / ( DepthTexture.r * (fNear - fFar) + fFar );
        highp float fZVal  = (fEyeZ - fNear) / (fFar - fNear);
        highp float fLodBias = min( max(abs(fZVal - focus)-focusRange, 0.0) * ratio, 1.0);
        gl_FragColor = vec4((texture2D( texture0, vCoord00 )).rgb, fLodBias);
    #endif



    #if defined(HORIZ) || defined(VERT)

        highp float kern[7]; //size 7, sigma 3
        kern[0] = kern[6] = 0.106595;
        kern[1] = kern[5] = 0.140367;
        kern[2] = kern[4] = 0.165569;
        kern[3] = 0.174938;
        
        /* -- This is just in caes we needed a different kernal value for the depth map
        highp float Dkern[7]; //size 7, sigma 3
        Dkern[0] = Dkern[6] = 0.106595;
        Dkern[1] = Dkern[5] = 0.140367;
        Dkern[2] = Dkern[4] = 0.165569;
        Dkern[3] = 0.174938;
        */

        //theoretical max (direct & no mix, 55ish)
        lowp vec4 pixel0 = texture2D( texture0, vCoord0);
        lowp vec4 pixel1 = texture2D( texture0, vCoord1);
        lowp vec4 pixel2 = texture2D( texture0, vCoord2);
        lowp vec4 pixel3 = texture2D( texture0, vCoord3); // RootPixel
        lowp vec4 pixel4 = texture2D( texture0, vCoord4);
        lowp vec4 pixel5 = texture2D( texture0, vCoord5);
        lowp vec4 pixel6 = texture2D( texture0, vCoord6);

        highp vec4  colorSum = pixel3 * kern[3];
        colorSum +=  mix (pixel0, pixel3, vec4(   max (pixel3.a - pixel0.a, 0.0) * 1.0 )    )* kern[0];
        colorSum +=  mix (pixel1, pixel3, vec4(   max (pixel3.a - pixel1.a, 0.0) * 1.0 )    )* kern[1];
        colorSum +=  mix (pixel2, pixel3, vec4(   max (pixel3.a - pixel2.a, 0.0) * 1.0 )    )* kern[2];

        colorSum +=  mix (pixel4, pixel3, vec4(   max (pixel3.a - pixel4.a, 0.0) * 1.0 )    )* kern[4];
        colorSum +=  mix (pixel5, pixel3, vec4(   max (pixel3.a - pixel5.a, 0.0) * 1.0 )    )* kern[5];
        colorSum +=  mix (pixel6, pixel3, vec4(   max (pixel3.a - pixel6.a, 0.0) * 1.0 )    )* kern[6];

        
        #if defined(COMBINE)
            //colorSum.a = colorSum.a * colorSum.a ;
            lowp vec4 origColor = texture2D(origSource, vCoord00);
            
            gl_FragColor = 
            vec4( 
                mix (
                    origColor.rgb,
                    colorSum.rgb, 
                    colorSum.a // this mixes the original depth buffer and the blured depth buffer
                ),
            1.0) 
            // * 0.00001 +      vec4(vec3(colorSum.a), 1.0) // Debug Viewer
            ;
            
        #else
            gl_FragColor = colorSum;
        #endif

    #endif
}

