
//@include "../../../Engine/Data/Shaders/Engine/include/Version.gx"

// varyings
varying highp vec2 ScreenUV;
uniform mediump vec2 texture0TexelSize;


varying mediump vec2 vCoord0;

// source depth texture
uniform highp sampler2D texture0;

#if defined(SSAO)
		
	// scene camera near/far
	uniform highp vec4 v4CameraNearFar;
	
	// SSAO effect strenght factor
	uniform highp float fMultiplier;
	
	uniform highp vec4 v4SamplingRadius;
	
	#if defined(TEXTURESPACE)
		
		// defines depth-dependent scaling of sampling radius for texture space SSAO
		// x - base radius
		// y - radius offset scale
		// z, w - min and max for radius offset clamp
		
		
		// this is the valueof the z difference between pixels that gives darkest result. 
		uniform highp float fMaxz; 
		
		//used for ssao fade after the maximum z is reached.
	    uniform highp float fDivFactor; 
	
		#if defined(ROTATING_KERNEL)
			// 2D normal noise texture and UV from vertex shader
			uniform highp sampler2D texNormalNoise2d;
			varying mediump vec2 vRandRotUV;
		#endif
		
	#elif defined(VIEWSPACE)
		
		// scene camera projection matrices for NDC<->ViewSpace conversion
		uniform highp mat4 m44CameraProj;
    	uniform highp mat4 m44CameraProjInv;
	
		#if defined(ROTATING_KERNEL)
			// 3D normal noise texture and UVs from vertex shader
			uniform highp sampler2D texNormalNoise3d;
			varying mediump vec2 vRandRotUV0;
			varying mediump vec2 vRandRotUV1;
			varying mediump vec2 vRandRotUV2;
			varying mediump vec2 vRandRotUV3;
			varying mediump vec2 vRandRotUV4;
			varying mediump vec2 vRandRotUV5;
			varying mediump vec2 vRandRotUV6;
			varying mediump vec2 vRandRotUV7;
		#endif
	#endif
	
#endif
	
#if defined(SSAO_MERGE)
	// source color texture (rendered scene)
	uniform lowp sampler2D texture1;
	
	// blur strenght
	uniform highp float fBlurScale;
#endif

/////////////////////////////////////////////////////////////////////////

#if defined(SSAO)

	// Z -> Linear Z
	highp float DepthLinear(highp float z)
	{
		return 1.0 / (v4CameraNearFar[2] * z + v4CameraNearFar[3]);
	}
	
	// Z -> Linear Z, relative to camera far
	highp float DepthLinearMultiplied(highp float z)
	{
		highp float fDepth_multiply = v4CameraNearFar.y * 0.00005; 
		return fDepth_multiply / (v4CameraNearFar[2] * z + v4CameraNearFar[3]);
	}

#endif


#if defined(SSAO) && defined(TEXTURESPACE)

	highp float SSAOTextureSpaceSample(highp float crtDepth, mediump vec2 uv1, mediump vec2 uv2)  //the main function for comparing current depth with sampled pixel depth
	{
		// used to fade the ssao value 
		highp float fMaxz2 =  fMaxz * fDivFactor * fDivFactor;  
	
		//calculate difference betwen current depth and sampled depth
		highp float d1 = texture2D(texture0, uv1).r;
		highp float dif1 = crtDepth - DepthLinearMultiplied(d1);
		highp float d2 = texture2D(texture0, uv2).r;
		highp float dif2 = crtDepth - DepthLinearMultiplied(d2);
		
		// we clamp the difference value so it is within maxz boundries and calculate a fade based on maxz2
		highp float diff1 = clamp(dif1, -fMaxz, fMaxz) - clamp(dif1, 0.0, fMaxz2) / fDivFactor;
		highp float diff2 = clamp(dif2, -fMaxz, fMaxz) - clamp(dif2, 0.0, fMaxz2) / fDivFactor;
		
		return max(diff1 + diff2, 0.0);
	}
	
#endif


#if defined(SSAO) && defined(VIEWSPACE)

	highp float SSAOViewSpaceSample(highp vec2 UV, highp float depth_linear_orig, highp vec4 offset)
	{
		// decode current fragment NDC coords from UV and Z
		highp vec3 vNdcSpace = vec3(UV.x * 2.0 - 1.0, UV.y * 2.0 - 1.0, depth_linear_orig * 2.0 - 1.0);
		
		// calculate current fragment CameraSpace coords
		highp float fCamSpaceZ = m44CameraProj[3][2] / (vNdcSpace.z + m44CameraProj[2][2]);
		highp float fClipSpaceW = -fCamSpaceZ;
		highp vec4 ClipSpace = vec4(vNdcSpace * fClipSpaceW, fClipSpaceW);
		highp vec4 CameraSpace = m44CameraProjInv * ClipSpace;
		
		// generate current SSAO sample CamereSpace coords
		highp vec4 P1_CamSpace = CameraSpace + offset;
		
		// calculate current SSAO sample NDC coords
		highp vec4 P1_ClipSpace = m44CameraProj * P1_CamSpace;
		highp vec4 P1_NdcSpace = P1_ClipSpace / P1_ClipSpace.w;
		
		// calculate screen UV coords of current SSAO sample
		highp vec2 P1_UV = vec2((P1_NdcSpace.x + 1.0) * 0.5, (P1_NdcSpace.y + 1.0) * 0.5);
		
		// calculate NDC Z value of pixel in framebuffer at the same UV coords as current SSAO sample
		highp float P1_depth_linear = DepthLinear(texture2D(texture0, P1_UV).r);
		highp float P1_depth_ndc_Z = P1_depth_linear * 2.0 - 1.0;
		
		// if current SSAO sample is a bit in front of pixel in framebuffer then
		// contribute to SSAO
		highp float fSSAO = 0.0;
		highp float fDiff = P1_NdcSpace.z - P1_depth_ndc_Z;
		if(fDiff > 0.0003 && fDiff < 0.010 )
		{
			fSSAO = 1.0;
		}
		
		return 0.0;
	}
	
#endif


/////////////////////////////////////////////////////////////////////////

void main(void)
{
	#if defined(SSAO)
		lowp vec4 FinalColor;
		mediump vec2 UV = vCoord0;
	
	#if defined(TEXTURESPACE) && defined(STATIC_KERNEL)
	
		// current fragment linear depth, relative to far clip value
		highp float fDepthLinearMult = DepthLinearMultiplied(texture2D(texture0, UV).r);
		
		//increment is based on the first number that is the sample radius increment for camera near and a formula that decreases the increment value as the z gets further from camera
		mediump float fBaseRadius = (v4SamplingRadius.x - clamp(fDepthLinearMult * v4SamplingRadius.y, v4SamplingRadius.z, v4SamplingRadius.w)) / texture0TexelSize.x;
		
		// calculate 3 UV offsets with calculated radius, spread 60 degrees from each other
		mediump vec2 inc[9];   
		inc[0] = vec2(fBaseRadius * cos(0.0)   , fBaseRadius * sin(0.0));
		inc[1] = vec2(fBaseRadius * cos(1.047) , fBaseRadius * sin(1.047));
		inc[2] = vec2(fBaseRadius * cos(-1.047), fBaseRadius * sin(-1.047));
		
		// clamp UV offsets so they point in the middle of target buffer pixels
		inc[0] = texture0TexelSize * floor(inc[0]); 
		inc[1] = texture0TexelSize * floor(inc[1]); 
		inc[2] = texture0TexelSize * floor(inc[2]); 
		
		// calulate another 6 UV offsets, 2 and 3 times farther
		inc[3] = inc[0] * 2.0;
		inc[4] = inc[1] * 2.0;
		inc[5] = inc[2] * 2.0;
		inc[6] = inc[0] * 3.0;
		inc[7] = inc[1] * 3.0;
		inc[8] = inc[2] * 3.0;
		
		// sum SSAO for all generated offsets
		// we sample symmetrically around current fragment
	  	highp float finalDif = 0.0;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[0], UV + inc[0]);
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[1], UV + inc[1]);
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[2], UV + inc[2]);
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[3], UV + inc[3]) * 0.7;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[4], UV + inc[4]) * 0.7;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[5], UV + inc[5]) * 0.7;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[6], UV + inc[6]) * 0.5;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[7], UV + inc[7]) * 0.5;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[8], UV + inc[8]) * 0.5;
		
		//we invert the value of the ssao so white becomes black and also amplify it by an empiricaly found value based on maxZ, divfactor etc.
		finalDif = 1.0 - clamp(finalDif * fMultiplier, 0.0, 1.0); 
		
		FinalColor.rgb = vec3(finalDif);
		FinalColor.a = 1.0;
		gl_FragColor = FinalColor;
	#endif

	
	#if defined(TEXTURESPACE) && defined(ROTATING_KERNEL)
	
		// current fragment linear depth, relative to far clip value
		highp float fDepthLinearMult = DepthLinearMultiplied(texture2D(texture0, UV).r);
		
		//increment is based on the first number that is the sample radius increment for camera near and a formula that decreases the increment value as the z gets further from camera
		mediump float fBaseRadius = (v4SamplingRadius.x - clamp(fDepthLinearMult * v4SamplingRadius.y, v4SamplingRadius.z, v4SamplingRadius.w)) / texture0TexelSize.x;

		// get and decode random 2D normal vector
		mediump vec4 vRandomRotation = texture2D(texNormalNoise2d, vRandRotUV);
		vRandomRotation -= 0.5;
		vRandomRotation *= 2.0;
		
		mediump vec2 inc[6];
		
		// UV offset with calculated radius in random direction
		inc[0] = vec2(fBaseRadius * vRandomRotation.r, fBaseRadius * vRandomRotation.g);
		
		// clamp UV offset so it points in th middle of a target buffer pixel
		inc[0].x = floor(inc[0].x) * texture0TexelSize.x; 
		inc[0].y = floor(inc[0].y) * texture0TexelSize.x;
		
		// generate 90 degrees rotated UV offset
		inc[1] = vec2(inc[0].y, -inc[0].x);
		
		// generate 4 more UV offsets, 2 and 3 further away
		inc[2] = inc[0] * 2.0;
		inc[3] = inc[1] * 2.0;
		inc[4] = inc[0] * 3.0;
		inc[5] = inc[1] * 3.0;
		
		//final gathering difference
	  	highp float finalDif = 0.0;

		// sum SSAO for all generated offsets
		// we sample symmetrically around current fragment
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[0], UV + inc[0]);
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[1], UV + inc[1]);
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[2], UV + inc[2]) * 0.7;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[3], UV + inc[3]) * 0.7;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[4], UV + inc[4]) * 0.5;
		finalDif += SSAOTextureSpaceSample(fDepthLinearMult, UV - inc[5], UV + inc[5]) * 0.5;
	
		//we invert the value of the ssao so white becomes black and also amplify it by an empiricaly found value based on maxZ, divfactor etc.
		finalDif = 1.0 - clamp(finalDif * fMultiplier, 0.0, 1.0); 
		
		FinalColor.rgb = vec3(finalDif);
		FinalColor.a = 1.0;
		gl_FragColor = FinalColor;
	#endif


	#if defined(VIEWSPACE)	

		// current fragment linear Z
		highp float fDepthLinear = DepthLinear(texture2D(texture0, UV).r);
		
		mediump float fBaseRadius = (v4SamplingRadius.x - clamp(fDepthLinear * v4SamplingRadius.y, v4SamplingRadius.z, v4SamplingRadius.w));

		#if defined(ROTATING_KERNEL)
			// 8 random, dynamic offsets in 3D space
			mediump vec4 vRandomRotation0 = fBaseRadius * vec4( (texture2D(texNormalNoise3d, vRandRotUV0).xyz-0.5)*2.0, 0.0);
			mediump vec4 vRandomRotation1 = fBaseRadius * vec4( (texture2D(texNormalNoise3d, vRandRotUV1).xyz-0.5)*2.0, 0.0);
			mediump vec4 vRandomRotation2 = fBaseRadius * vec4( (texture2D(texNormalNoise3d, vRandRotUV2).xyz-0.5)*2.0, 0.0);
			mediump vec4 vRandomRotation3 = fBaseRadius * vec4( (texture2D(texNormalNoise3d, vRandRotUV3).xyz-0.5)*2.0, 0.0);
			mediump vec4 vRandomRotation4 = fBaseRadius * vec4( (texture2D(texNormalNoise3d, vRandRotUV4).xyz-0.5)*2.0, 0.0);
			mediump vec4 vRandomRotation5 = fBaseRadius * vec4( (texture2D(texNormalNoise3d, vRandRotUV5).xyz-0.5)*2.0, 0.0);
			mediump vec4 vRandomRotation6 = fBaseRadius * vec4( (texture2D(texNormalNoise3d, vRandRotUV6).xyz-0.5)*2.0, 0.0);
			mediump vec4 vRandomRotation7 = fBaseRadius * vec4( (texture2D(texNormalNoise3d, vRandRotUV7).xyz-0.5)*2.0, 0.0);
		#elif defined(STATIC_KERNEL)
			// 8 random but static offsets in 3D space
			mediump vec4 vRandomRotation0 = fBaseRadius * vec4( 0.189,  0.625,  0.756, 0.0 );
			mediump vec4 vRandomRotation1 = fBaseRadius * vec4(-0.084,  0.977,  0.193, 0.0 );
			mediump vec4 vRandomRotation2 = fBaseRadius * vec4( 0.273, -0.288, -0.917, 0.0 );
			mediump vec4 vRandomRotation3 = fBaseRadius * vec4(-0.706,  0.707,  0.018, 0.0 );
			mediump vec4 vRandomRotation4 = fBaseRadius * vec4( 0.427,  0.882,  0.194, 0.0 );
			mediump vec4 vRandomRotation5 = fBaseRadius * vec4( 0.523,  0.245,  0.815, 0.0 );
			mediump vec4 vRandomRotation6 = fBaseRadius * vec4(-0.930, -0.195,  0.310, 0.0 );
			mediump vec4 vRandomRotation7 = fBaseRadius * vec4( 0.352,  0.933,  0.061, 0.0 );
		#endif
		
		// sum SSAO for all generated offsets (symmetrically to keep ~50% ratio of hits for flat surfaces)
		highp float fSSAO = 0.0;	
		
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, 2.0 * vRandomRotation0 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, -2.0 * vRandomRotation0 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, 2.0 * vRandomRotation1 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, -2.0 * vRandomRotation1 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, 4.0 * vRandomRotation2 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, -4.0 * vRandomRotation2 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, 4.0 * vRandomRotation3 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, -4.0 * vRandomRotation3 );
			
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, 3.0 * vRandomRotation4 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, -3.0 * vRandomRotation4 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, 3.0 * vRandomRotation5 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, -3.0 * vRandomRotation5 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, 6.0 * vRandomRotation6 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, -6.0 * vRandomRotation6 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, 6.0 * vRandomRotation7 );
		fSSAO += SSAOViewSpaceSample(UV, fDepthLinear, -6.0 * vRandomRotation7 );
		
		// normalize SSAO value
		fSSAO = fSSAO * (1.0/16.0);
		
		// get rid of ~50% hits for flat surfaces 
		fSSAO = (fSSAO - 0.5) * 2.0;
		
		// apply multiplier (stenght of the effect)
		fSSAO = 1.0 - clamp((fSSAO * fMultiplier * 0.003), 0.0, 1.0); ;
		fSSAO = max(0.0, fSSAO);
		
		gl_FragColor = vec4(vec3(fSSAO), 1.0);
	#endif
	
	
	#endif
	
	#if defined(SSAO_MERGE)
		// blur SSAO 
		lowp vec4 color1 = texture2D(texture0, vCoord0, fBlurScale-2.0 );
	    lowp vec4 color2 = texture2D(texture0, vCoord0, fBlurScale-1.0);
	    lowp vec4 color3 = texture2D(texture0, vCoord0, fBlurScale);
	    gl_FragColor = (color1 * 0.15 + color2 * 0.35 + color3 * 0.5);
	    
	    #ifndef SSAO_DEBUG
	    	// merge with color
	    	gl_FragColor *= texture2D(texture1, vCoord0);
	    #endif
	#endif	
}

//////////////////////////////////////////////////////////////////////////


