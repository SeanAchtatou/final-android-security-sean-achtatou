#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
  #define ES3
#endif

#if defined(GL_OES_standard_derivatives)
  #extension GL_OES_standard_derivatives : enable
  #define USE_DERIVATIVES
#else
#endif

#if defined(GL_EXT_shader_framebuffer_fetch)
  #extension GL_EXT_shader_framebuffer_fetch : enable
  #define USE_LAST_FRAG_DATA
#endif

#ifdef ES3
#define varying out
#define attribute in
#define texture2D texture
#endif



varying lowp vec2 vUV;

#if !(defined GLITCH_HLSL_VS_4_0_LEVEL_9_3 || defined GLITCH_HLSL_PS_4_0_LEVEL_9_3)
#ifndef GAMMA_CORRECT
varying lowp float vExposure;
#endif
#endif // not (defined GLITCH_HLSL_VS_4_0_LEVEL_9_3 || defined GLITCH_HLSL_PS_4_0_LEVEL_9_3)

uniform lowp int exposureEnabled;



attribute highp vec4 position;
attribute highp vec2 texcoord0;

uniform highp mat4 WorldViewProjection;

uniform highp float exposureCompensation;
#ifndef GLITCH_HLSL_VS_4_0_LEVEL_9_3
uniform sampler2D luminanceTexture;
uniform highp sampler2D global_exposureTexture;
#endif //not defined GLITCH_HLSL_VS_4_0_LEVEL_9_3
uniform highp float global_exposureCompensation;

// Common vertex shader
void main()
{	  
    #ifndef GLITCH_HLSL_VS_4_0_LEVEL_9_3
    #ifndef GAMMA_CORRECT
	  
	  // Auto exposure
	  if(exposureEnabled != 0)
	  {
        highp float exposureTexture = texture2D(global_exposureTexture, vec2(0,0)).r;
        vExposure = 0.18 / exp2(exposureTexture - global_exposureCompensation);
	  }
	  
    #endif
    #endif // not defined GLITCH_HLSL_VS_4_0_LEVEL_9_3
    
	  // UVs
	  vUV = texcoord0;
	
	  // Position
    highp vec4 pos = WorldViewProjection * position;
    gl_Position = pos;
}

