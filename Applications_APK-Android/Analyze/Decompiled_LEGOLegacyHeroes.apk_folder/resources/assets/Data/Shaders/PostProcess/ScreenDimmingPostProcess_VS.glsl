
#if defined GL_ES && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#endif


varying highp vec2 vCoord00; //screenspace coords


attribute highp vec4 Vertex;
attribute highp vec2 TexCoord0;

uniform   highp   mat4 WorldViewProjectionMatrix;

void main(void)
{
	vCoord00 = TexCoord0;
	gl_Position = WorldViewProjectionMatrix*Vertex;
}
