
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif


varying highp vec2 vCoord00; //screenspace coords


uniform sampler2D texture0; //ghost source
uniform sampler2D texture1; //scene source
uniform sampler2D texture2; //blurred target
//uniform highp vec4 GhostingColor; // Remove
//uniform highp vec4 AccentColor; // Remove
//uniform highp vec4 OutlineColor; // Remove
uniform highp float OutlineSize;
uniform highp float BGBrightness;
uniform highp float GhostTransparency;
//uniform mediump float BlurStrength; // Remove


void main()
{
	highp vec4 UnbuiltPass = texture2D(texture0, vCoord00);	
    highp vec4 ScenePass = texture2D(texture1, vCoord00);
	highp vec4 BGColorFinal = texture2D(texture2, vCoord00);
	
	lowp float UnbuiltPassA = texture2D(texture0, vCoord00 + vec2(OutlineSize)).a;
	lowp float UnbuiltPassB = texture2D(texture0, vCoord00 + vec2(-OutlineSize)).a;
	lowp float UnbuiltPassC = texture2D(texture0, vCoord00 + vec2(OutlineSize, -OutlineSize)).a;
	lowp float UnbuiltPassD = texture2D(texture0, vCoord00 + vec2(-OutlineSize, OutlineSize)).a;
	



	lowp float Outline =
    ceil (
        max(
            max(
                max(0.0, (UnbuiltPassA - UnbuiltPassB)),
                max(0.0, (UnbuiltPassB - UnbuiltPassA))
            ),
            max(
                max(0.0, (UnbuiltPassC - UnbuiltPassD)),
                max(0.0, (UnbuiltPassD - UnbuiltPassC))
            )
        )
    );

    lowp vec3 OutlineColor =
    vec3 (
        max (
            max (
                UnbuiltPassA,
                UnbuiltPassB
            ),
            max (
                UnbuiltPassC,
                UnbuiltPassD
            )
        )
    ) * Outline;


	gl_FragColor = vec4(
        mix(            
            ScenePass.rgb,
            mix (
                BGColorFinal.rgb, 
                mix (
                    (BGColorFinal.rgb * BGBrightness) + (UnbuiltPass.rgb * GhostTransparency),
                    OutlineColor,//vec3(ceil (UnbuiltPass.a)),
                    Outline
                ),
                ceil(UnbuiltPass.a)
            ),
            ceil(UnbuiltPass.a)
        ),
        ScenePass.a
    );
}
