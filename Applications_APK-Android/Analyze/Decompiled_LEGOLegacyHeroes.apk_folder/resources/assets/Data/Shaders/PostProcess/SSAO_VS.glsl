#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
  #define ES3
#endif

#if defined(GL_OES_standard_derivatives)
  #extension GL_OES_standard_derivatives : enable
  #define USE_DERIVATIVES
#else
#endif

#if defined(GL_EXT_shader_framebuffer_fetch)
  #extension GL_EXT_shader_framebuffer_fetch : enable
  #define USE_LAST_FRAG_DATA
#endif

#ifdef ES3
#define varying out
#define attribute in
#define texture2D texture
#endif



varying lowp  vec2   vUV;

uniform highp mat4 WorldViewProjection;
uniform highp float time;


attribute highp vec4 position;
attribute highp vec2 texcoord0;


void main()
{	   
	  vUV = texcoord0;
    gl_Position = WorldViewProjection * position;
}

