
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
#endif

// varyings
varying highp vec2 vCoord00;

// attributes
attribute highp vec4 Vertex;
attribute highp vec2 TexCoord0;

// uniforms
uniform highp mat4 WorldViewProjectionMatrix;


void main(void)
{
	vCoord00 = TexCoord0.xy;
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
