
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

// varyings
varying highp vec2 vCoord00;


uniform lowp sampler2D texture0; //usual source

uniform   highp float BlurAmount;
uniform   highp vec3 BGDimming;

void main()
{
/*
    highp vec4 color = texture2D(texture0, vCoord00, BlurAmount);
    gl_FragColor = vec4(color.rgb * BGDimming, 1.0);
    */
    highp vec3 color = vec3(0.0);
    highp float fLodBias = BlurAmount;// min( max(abs(fZVal - focus)-focusRange, 0.0) * ratio, 1.0) * scale;
    #ifdef LOW
        color = texture2D(texture0, vCoord00, fLodBias).rgb;
    #else
        lowp vec3 color5 = texture2D(texture0, vCoord00, fLodBias-4.0 ).rgb;
        lowp vec3 color4 = texture2D(texture0, vCoord00, fLodBias-3.0 ).rgb;
        lowp vec3 color3 = texture2D(texture0, vCoord00, fLodBias-2.0 ).rgb;
        lowp vec3 color2 = texture2D(texture0, vCoord00, fLodBias-1.0).rgb;
        lowp vec3 color1 = texture2D(texture0, vCoord00, fLodBias).rgb;
        //color = (color3 * 0.15 + color2 * 0.35 + color1 * 0.5) ;
        //color = color4;
        //color = (color4 * 0.1 + color3 * 0.2 + color2 * 0.3 + color1 * 0.4) ;
        //color = (color4 * 0.05 + color3 * 0.22 + color2 * 0.33 + color1 * 0.4) ;
        //color = (color4 * 0.05 + color3 * 0.1 + color2 * 0.2 + color1 * 0.65) ;
        
        
        color = color5 * 0.2 + color4 * 0.2 + color3 * 0.2 + color2 * 0.2 + color1 * 0.2;
        //gl_FragColor = vec4(fLodBias/scale, fLodBias/scale, fLodBias/scale, 1.0);
    #endif

    //gl_FragColor = vec4(color, 1.0);
    gl_FragColor = vec4(color * BGDimming , 1.0); 
    
}

