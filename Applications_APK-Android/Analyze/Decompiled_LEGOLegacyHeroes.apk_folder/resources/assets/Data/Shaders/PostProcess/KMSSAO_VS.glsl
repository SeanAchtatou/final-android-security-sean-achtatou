
//@include "../../../Engine/Data/Shaders/Engine/include/Version.gx"

// varyings
varying highp vec2 ScreenUV;
uniform mediump vec2 texture0TexelSize;

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////

attribute highp vec4 Vertex;
attribute mediump vec2 TexCoord0;

/////////////////////////////////////////////////////////////////////////

uniform highp mat4 WorldViewProjectionMatrix;
varying mediump vec2 vCoord0;

#if defined(SSAO)	
#if defined(ROTATING_KERNEL)

	#if defined(TEXTURESPACE)
	
		// 2D normal nois texture width
		uniform highp float texNormalNoise2d_width;
		
		// UV for 2D normal noise texture
		// we repeat that texture (square) on screen with 1:1 pixel ratio
		varying mediump vec2 vRandRotUV;
	#elif defined(VIEWSPACE)
	
		// 3D normal noise texture dimensions
		uniform highp float texNormalNoise3d_width;
		uniform highp float texNormalNoise3d_height;
		
		// UVs for 3D normal noise texture
		// we repeat that texture on screen 1:1 width, 8:1 height
		// (8 samples per screen pixel needed)
		varying mediump vec2 vRandRotUV0;
		varying mediump vec2 vRandRotUV1;
		varying mediump vec2 vRandRotUV2;
		varying mediump vec2 vRandRotUV3;
		varying mediump vec2 vRandRotUV4;
		varying mediump vec2 vRandRotUV5;
		varying mediump vec2 vRandRotUV6;
		varying mediump vec2 vRandRotUV7;
	#endif
	
#endif
#endif

/////////////////////////////////////////////////////////////////////////

void main(void)
{
	vCoord0 = TexCoord0;
	
#if defined(SSAO)
#if defined(ROTATING_KERNEL) 

	#if defined(TEXTURESPACE)
		// UV for 2D normal noise texture
		// we repeat that texture (square) on screen with 1:1 pixel ratio
		vRandRotUV = TexCoord0 / (texNormalNoise2d_width / texture0TexelSize.x);
	#elif defined(VIEWSPACE)
		// UVs for 3D normal noise texture
		// we repeat that texture on screen 1:1 width, 8:1 height
		// (8 samples per screen pixel needed)
		vRandRotUV0 = TexCoord0 / (texNormalNoise2d_width / texture0TexelSize.x);
		vRandRotUV1 = vRandRotUV0 + vec2(0.0, 1.0/texNormalNoise3d_height);
		vRandRotUV2 = vRandRotUV0 + vec2(0.0, 2.0/texNormalNoise3d_height);
		vRandRotUV3 = vRandRotUV0 + vec2(0.0, 3.0/texNormalNoise3d_height);
		vRandRotUV4 = vRandRotUV0 + vec2(0.0, 4.0/texNormalNoise3d_height);
		vRandRotUV5 = vRandRotUV0 + vec2(0.0, 5.0/texNormalNoise3d_height);
		vRandRotUV6 = vRandRotUV0 + vec2(0.0, 6.0/texNormalNoise3d_height);
		vRandRotUV7 = vRandRotUV0 + vec2(0.0, 7.0/texNormalNoise3d_height);
	#endif
	
#endif
#endif

	gl_Position = WorldViewProjectionMatrix * Vertex;
}

