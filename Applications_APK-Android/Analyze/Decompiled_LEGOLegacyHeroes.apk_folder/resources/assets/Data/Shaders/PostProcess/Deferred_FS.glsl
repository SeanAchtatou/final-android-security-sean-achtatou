#extension GL_EXT_shadow_samplers : enable

#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
  #define ES3
#endif

#if defined(GL_OES_standard_derivatives)
  #extension GL_OES_standard_derivatives : enable
  #define USE_DERIVATIVES
#else
#endif

#if defined(GL_EXT_shader_framebuffer_fetch)
  #extension GL_EXT_shader_framebuffer_fetch : enable
  #define USE_LAST_FRAG_DATA
#endif

#ifdef ES3
  #define varying in
  #define texture2D texture
  #define gl_FragColor fragData0
  #define frag0 fragData0
  #define frag1 fragData1
  #define frag2 fragData2
  #define frag3 fragData3
  #if (defined USE_LAST_FRAG_DATA && defined USE_SOFT)
    layout (location = 0) inout highp vec4 fragData0;
    layout (location = 1) inout highp vec4 fragData1;
    layout (location = 2) inout highp vec4 fragData2;
    layout (location = 3) inout highp vec4 fragData3;
  #else
    layout (location = 0) out highp vec4 fragData0;
    layout (location = 1) out highp vec4 fragData1;
    layout (location = 2) out highp vec4 fragData2;
    layout (location = 3) out highp vec4 fragData3;
  #endif
  #define shadowmapSampler sampler2Dshadow
  #define textureCube texture
#else
  #define shadowmapSampler sampler2D
  #define frag0 gl_FragColor[0]
  #define frag1 gl_FragColor[1]
  #define frag2 gl_FragColor[2]
  #define frag3 gl_FragColor[3]
#endif

#define USE_3_AMBIENT

uniform lowp samplerCube global_IrradianceCube;
uniform lowp samplerCube global_ReflectionCube;
//uniform mediump float global_SpecStrength;
//uniform mediump float global_ReflectStrength;
//uniform mediump vec2  global_Roughness;

mediump vec4 computeAmbientLighting(highp vec3 _worldNormal, lowp vec4 _ambientColorA, lowp vec4 _ambientColorB, lowp vec4 _ambientColorC)
{
	lowp float norm = normalize(_worldNormal).y;
	
	lowp vec4 mixAB = mix(_ambientColorB, _ambientColorA, max(norm,0.0));	
	
	#ifdef USE_3_AMBIENT
		lowp vec4 mixBC = vec4(0.0);
		mixBC = mix(mixAB, _ambientColorC, max(-norm,0.0));	
		
		return mixBC;
	#endif
		return mixAB;
}

mediump vec3 DirectLight(mediump vec3 _normal, highp vec3 _sunDirection, highp vec3 _reflectDir, mediump float _specStrength, mediump vec2 _roughness, inout highp vec3 _ls)
{
	mediump float ndotl = max(dot(_normal, _sunDirection),0.00001);
	mediump float rdotl = max(dot(_reflectDir, _sunDirection), 0.00001);
	
	mediump float spec = 0.003 * _roughness.x * pow(2.0, _roughness.y * rdotl - _roughness.y );
	
	mediump vec3 diffuse = vec3(max( (1.0-spec), 0.001) * ndotl);// * _albedo; 
	
	_ls += vec3(spec);
	
	return diffuse;
}

mediump vec3 IndirectLight(mediump vec3 _normal, mediump vec3 _reflectDir, mediump float _NoV, mediump float _reflectStrength, inout highp vec3 _ls)
{
	mediump vec4 ambientRGBM = textureCube( global_IrradianceCube, _normal );
	mediump vec3 ambientColor = 8.0 * ambientRGBM.a * ambientRGBM.rgb;
	mediump vec4 envRGBM = textureCube( global_ReflectionCube, _reflectDir );
	mediump vec3 envColor = 8.0 * envRGBM.a * envRGBM.rgb;
	
	mediump float refl = _reflectStrength * (min( 0.49, pow(2.0, -9.28 * _NoV) ) * 0.7 + 0.015); 
	
	mediump vec3 ambient = max( (1.0-refl), 0.001 ) * ambientColor;// * _albedo;
	mediump vec3 env = refl*envColor;
	
	//mediump vec3 retVal = ambient+env;
	
	_ls += env;
	
	return ambient;
}

/**** STRUCTS ****/

struct DirectSpecularLightStruct 
{
	mediump vec4 directLight; // direct light intensity
	mediump vec3 specularLight; // spec light intensity
};

struct PointLightStruct
{
	mediump vec4 s_lightColor; // rgb = color, a = diffuse attenuation factor
	//mediump vec4 s_specColor; 	// rgb = specColor, a = specPower, or second light for box lights
	mediump float s_specIntensity; 	
	highp vec3 s_lightPos; 			// light position in world coordinates
	mediump vec2 s_attenuation; 	// attenuation start / end
	lowp float s_ambientFactor; 	// NdotL intensity - only for sun and point lights
	lowp float s_useSpec;					// only for point lights
};



#define NB_BOX_LIGHTS 3
#define BOX_LIGHT_ARRAY_SIZE 21     // Number of floats in uniform array - must macth code side array size
struct BoxLightStruct
{
	mediump vec4 s_lightColor1; 	// rgb = color, a = diffuse attenuation factor
	mediump vec4 s_lightColor2; 	// rgb = specColor, a = specPower, or second light for box lights
	highp vec3 s_lightPos; 			// light position in world coordinates
	highp vec3 s_lightDir;			// axis-aligned light direction (1.,0.,0.), (0.,1.,0.) or (0.,0.,1.)
	mediump vec2 s_attenuation; 	// attenuation start / end
	mediump vec3 s_aabbSize;
	mediump vec2 s_scaleOffset;   // gradient scale and offset
	bool s_invertAxis;
};

/**** MATRIX MAPPING ****/

// omni with and without specular
PointLightStruct MatToStructOmni (highp mat4 matrix)
{
	  PointLightStruct omni;
	  
	  omni.s_lightColor     = vec4(matrix[1][0],matrix[1][1],matrix[1][2],matrix[0][3]);
	  //omni.s_specColor      = vec4(matrix[2][2],matrix[2][3],matrix[3][0],matrix[3][1]); // spec = 0 if no spec
	  omni.s_lightPos       = vec3(matrix[0][0],matrix[0][1],matrix[0][2]);
	  omni.s_attenuation    = vec2(matrix[2][0],matrix[2][1]);
	  omni.s_specIntensity  = matrix[2][2];
	  omni.s_ambientFactor  = matrix[1][3];
	  
	  return omni;
}	

// fog and box lights	
BoxLightStruct MatToStructBox (highp float matrix[NB_BOX_LIGHTS * BOX_LIGHT_ARRAY_SIZE], int id)
{
    BoxLightStruct box;
    
    box.s_lightColor1   = vec4(matrix[id +  6],matrix[id +  7],matrix[id +  8],matrix[id +  9]);
    box.s_lightColor2   = vec4(matrix[id + 10],matrix[id + 11],matrix[id + 12],matrix[id + 13]);
    box.s_lightPos      = vec3(matrix[id +  0],matrix[id +  1],matrix[id +  2]);
    box.s_lightDir      = vec3(matrix[id + 16],matrix[id + 17],matrix[id + 18]);
    box.s_attenuation   = vec2(matrix[id + 14],matrix[id + 15]);
    box.s_aabbSize      = vec3(matrix[id +  3],matrix[id +  4],matrix[id +  5]);
	  box.s_scaleOffset   = vec2(matrix[id + 19],matrix[id + 20]);
    
    return box;	
}	

/**** FUNCTIONS ****/

lowp float isInsideAABB (highp vec3 _vertexPos, highp vec3 _lightPos, mediump vec3 _aabbSize)
{
	mediump vec3 aabbMin = _lightPos - 0.5 * _aabbSize;
	mediump vec3 aabbMax = _lightPos + 0.5 * _aabbSize;
	
	lowp float xStep = step(aabbMin.x, _vertexPos.x) * (step(_vertexPos.x, aabbMax.x));
	lowp float yStep = step(aabbMin.y, _vertexPos.y) * (step(_vertexPos.y, aabbMax.y));
	lowp float zStep = step(aabbMin.z, _vertexPos.z) * (step(_vertexPos.z, aabbMax.z));
	
	return (xStep * yStep * zStep);
}

lowp float computeStartEndAttenuation(highp vec3 _pos, highp vec3 _lightpos, mediump vec2 _attenuation)
{
	highp 	float dist 			= distance(_pos, _lightpos);	
	mediump float attenuation = 1.0 - clamp((dist-_attenuation.x)/(_attenuation.y - _attenuation.x), 0., 1.);
	
	return attenuation;
}


mediump float getSpecularFactor(highp 	vec3  _pos,	highp 	vec3  _eyePos, highp   vec3  _lightPos,	lowp 	vec3  _normal, mediump float _gloss)
{
	lowp vec3 rayDir = _pos - _lightPos;
	lowp vec3 ref = normalize(reflect(rayDir, _normal));	
	lowp vec3 eyeDir = normalize(_eyePos - _pos);
	lowp float specFactor = max(.001, dot(ref, eyeDir)); 
	specFactor = pow(specFactor, _gloss);
	
	return specFactor;
}


mediump vec3 computeSpecularLighting(highp vec3 _EyePos, mediump vec3 _specColor, mediump float _specularPower, highp vec3 _lightPos, mediump vec3 _worldNormal, highp vec3 _vertexPos)
{
	mediump vec3 specular = vec3(1.0);
	mediump float spec = getSpecularFactor(_vertexPos, _EyePos, _lightPos, _worldNormal, _specularPower);
	specular = _specColor * spec;
	
	return specular;
}


mediump vec4 computeDirectLighting(highp vec3 _lightPos, mediump vec4 _lightColor, mediump vec3 _worldNormal, highp vec3 _vertexPos, lowp float _ambientFactor)
{
	mediump vec4 result = vec4(0.0);
	
	lowp float NdotL = 1.0;
	NdotL = max(dot(_worldNormal, normalize(_lightPos - _vertexPos)),0.0); // Light incidence
	NdotL = mix(NdotL, 1.0, _ambientFactor);
	
	result.rgb = _lightColor.rgb * NdotL;
	result.a = (NdotL) * _lightColor.a;

	return result;
}




mediump vec4 computeBoxLighting(lowp vec3 _lightDir, mediump vec4 _lightColor1, mediump vec4 _lightColor2, mediump vec2 _attenuation, highp vec3 _lightPos, mediump vec3 _aabbSize, mediump vec2 _scaleOffset, highp vec3 _vertexPos)
{	
    mediump vec4 result = vec4(0.);
    highp vec3 lightPosYCorrected = _lightPos;
    mediump vec3 aabbSizeCorrected = _aabbSize;

    lowp float insideAABB = isInsideAABB(_vertexPos, lightPosYCorrected, aabbSizeCorrected);
    mediump vec3 aabbMin = lightPosYCorrected - 0.5 * aabbSizeCorrected;
    mediump vec3 aabbMax = lightPosYCorrected + 0.5 * aabbSizeCorrected;

    // lightDir should be normalized before.
    mediump float att = clamp(_scaleOffset.y + _scaleOffset.x * dot(_lightDir, (_vertexPos - lightPosYCorrected) / (aabbSizeCorrected * .5)) * .5 + .5, 0., 1.);

    mediump vec4 finalColor = mix(_lightColor1, _lightColor2, att);
    mediump float finalIntensity = mix(_attenuation.x, _attenuation.y, att);

    finalColor *= insideAABB;

    result.rgb += finalColor.rgb * finalIntensity;
    result.a += finalColor.a;

    return result;
}

mediump float computeBoxFogLighting(lowp vec3 _lightDir, mediump vec4 _lightColor1, mediump vec4 _lightColor2, mediump vec2 _attenuation, highp vec3 _lightPos, mediump vec3 _aabbSize, mediump vec2 _scaleOffset, highp vec3 _vertexPos)
{	
    mediump vec4 result = computeBoxLighting(_lightDir, _lightColor1, _lightColor2, _attenuation, _lightPos, _aabbSize, _scaleOffset, _vertexPos);
    return length(result);
}

lowp float computeFogLighting(mediump vec2 _nearFar, highp vec3 _camPos, highp vec3 _vertexPos)
{
	highp float dist = length(_vertexPos - _camPos);
	highp float fog = (dist - _nearFar.x) / (_nearFar.y - _nearFar.x);
	return clamp(fog, 0., 1.);
}

DirectSpecularLightStruct computePointLighting(highp vec3 _lightPos, mediump vec4 _lightColor, mediump vec2 _attenuation, mediump vec3 _worldNormal, highp vec3 _vertexPos, lowp float _ambientFactor)
{
	DirectSpecularLightStruct resultingLight;	
	
	resultingLight.directLight = vec4(0.0);
	resultingLight.specularLight = vec3(0.0);
	
	highp vec3 lightPosYCorrected = _lightPos.xyz;

	lowp float Attenuation = computeStartEndAttenuation(_vertexPos, lightPosYCorrected, _attenuation);
	
	/// DIRECT LIGHT   
	resultingLight.directLight = Attenuation * computeDirectLighting(lightPosYCorrected, _lightColor, _worldNormal, _vertexPos, _ambientFactor);
	
	return resultingLight;
}

DirectSpecularLightStruct computePointLightingWithSpecular(highp vec3 _eyePos, highp vec3 _lightPos, mediump vec4 _lightColor, mediump vec4 _specularColor, mediump vec2 _attenuation, mediump vec3 _worldNormal, highp vec3 _vertexPos, lowp float _useSpec, lowp float _ambientFactor)
{
	DirectSpecularLightStruct resultingLight;	
	
	resultingLight.directLight = vec4(0.0);
	resultingLight.specularLight = vec3(0.0);
	
	highp vec3 lightPosYCorrected = _lightPos.xyz;

	lowp float Attenuation = computeStartEndAttenuation(_vertexPos, lightPosYCorrected, _attenuation);
	
	/// DIRECT LIGHT   
	resultingLight.directLight = Attenuation * computeDirectLighting(lightPosYCorrected, _lightColor, _worldNormal, _vertexPos, _ambientFactor);
	
	
	/// SPECULAR LIGHT
	#ifdef SPECULAR
			resultingLight.specularLight = Attenuation * computeSpecularLighting(_eyePos, _specularColor.xyz, _specularColor.w, lightPosYCorrected, _worldNormal, _vertexPos) * _useSpec;
	#endif
	
	return resultingLight;
}

// Simplified Omni calculation for particles and vfx
DirectSpecularLightStruct computePointLightingNoNormal(highp vec3 _lightPos, mediump vec4 _lightColor, mediump vec2 _attenuation, highp vec3 _vertexPos)
{
	DirectSpecularLightStruct resultingLight;	
	
	resultingLight.directLight = vec4(0.0);
	resultingLight.specularLight = vec3(0.0);
	
	highp vec3 lightPosYCorrected = _lightPos.xyz;

	lowp float Attenuation = computeStartEndAttenuation(_vertexPos, lightPosYCorrected, _attenuation);
	
	/// DIRECT LIGHT   
	resultingLight.directLight = Attenuation * _lightColor;
	
	return resultingLight;
}

highp vec3 computeRimLight(highp vec3 color, lowp float size, mediump float intensity, highp vec3 worldNormal, highp vec3 worldEyePosition, highp vec3 worldPosition)
{
    highp vec3 c = color * (1. - max(0., dot(worldNormal, normalize(worldEyePosition - worldPosition))));
    return pow(max(vec3(.0), c), vec3(size)) * intensity;    
}


#ifdef USE_MATCAP
lowp vec2 getMatcapUV(lowp vec3 normal)
{
	normal = (matView * normal);
	//normal = (mat3(matviewprojection) * normal) * 0.5 + 0.5;
	
	//if (normal.z < 0.0) { normal.xy = normalize(normal.xy); } // IF TOO HEAVY, REMOVE. LEL
	
	normal.xy = normal.xy * 0.5 +0.5;
	
	normal.y = 1. - normal.y; // texfetch correction
	
	return normal.xy;
}
#endif



varying lowp  vec2   vUV;


uniform sampler2D texture0;     // albedo, discard
uniform sampler2D texture1;     // position WS
uniform sampler2D texture2;     // normal WS
uniform sampler2D texture3;     // emissive / spec
uniform sampler2D depthSampler; // depth

#ifdef USE_SSAO
uniform sampler2D ssaoSampler;  // ssao
uniform lowp float ssaoStrength;
#endif

// LIGHTS MATRIX
uniform lowp int boxFogLightMatricesSize;
uniform lowp int boxLightMatricesSize;
uniform lowp int omniSpecLightMatricesSize;
uniform lowp int omniLightMatricesSize;

uniform highp float boxFogLightMatrices[NB_BOX_LIGHTS * BOX_LIGHT_ARRAY_SIZE]; // Size MUST be a factor of BOX_LIGHT_ARRAY_SIZE and must match the size in the code
uniform highp float boxLightMatrices[NB_BOX_LIGHTS * BOX_LIGHT_ARRAY_SIZE]; // Size MUST be a factor of BOX_LIGHT_ARRAY_SIZE and must match the size in the code
uniform highp mat4  omniSpecLightMatrices[5]; // The size MUST match the matrix size in the code
uniform highp mat4  omniLightMatrices[20]; // The size MUST match the matrix size in the code

// SPECULAR
uniform highp vec3  SpecularColor;
uniform highp float SpecularGloss;

// SUN
uniform highp vec4 global_SunColor;
uniform highp vec3 global_SunDirection;

// AMBIENT
uniform lowp vec4 global_AmbientColorA;
uniform lowp vec4 global_AmbientColorB;
uniform lowp vec4 global_AmbientColorC;

// RIM
uniform lowp vec4 global_RimLightColor;
uniform highp float global_RimLightIntensity;
uniform highp float global_RimLightSize;

// FOG
uniform mediump vec2 global_FogNearFar;
uniform mediump vec4 global_FogLightColor;

uniform highp vec3 camPosition;
uniform lowp vec3 global_ShadowColor;

#ifdef USE_SHADOW_MAP

#ifndef ES3
  #define sampler2DShadow sampler2D
#endif

#define SHADOW_MAP_CASCADE_COUNT 2
//varying highp float vCascadeIdx;
#endif

#ifdef USE_SHADOW_MAP
uniform highp sampler2DShadow global_ShadowMapSampler;
uniform highp mat4 global_ShadowMapViewProjs[SHADOW_MAP_CASCADE_COUNT];
uniform highp vec2 global_ShadowMapScale;
uniform highp vec3 global_ShadowDir;
uniform highp vec2 global_ShadowMapOffsets;

highp float rand(highp vec2 seed2)
{
    return fract(sin(dot(seed2, vec2(12.9898,78.233))) * 43758.5453);
}

// highp float DirtyShadowMapSampling(mediump vec3 smCoord)
// {
    // mediump vec3 P = smCoord;
    // mediump vec3 Q = smCoord;
// 
    // mediump vec2 offset = vec2(rand(gl_FragCoord.xy), rand(gl_FragCoord.yx)) * global_ShadowMapScale * 1.41;
    // Q.xy += offset;
    // P.xy -= offset;
    // return (texture2D(global_ShadowMapSampler, P) + texture(global_ShadowMapSampler, Q)) * 0.5f;
// }

highp float ShadowMapSampling(mediump vec3 smCoord)
{	
  #ifdef ES3
    return texture2D(global_ShadowMapSampler, smCoord);
  #else   
    return texture2D(global_ShadowMapSampler, smCoord);texture2D(global_ShadowMapSampler, smCoord.xy).r - smCoord.z < 0.0 ? 1.0 : 0.0;
  #endif
}

highp float ReceiveShadow(mediump vec4 worldPos, highp vec3 worldNormal)
{
	  // Compute offset world vertex pos
    highp vec3 constantOffset = global_ShadowDir * global_ShadowMapOffsets[0];
    highp float normalOffsetSlopeScale = clamp(1. - dot(worldNormal, global_ShadowDir), 0., 1.);
    highp vec3 normalOffset = worldNormal * normalOffsetSlopeScale * global_ShadowMapOffsets[1];
    
    highp vec3 SMOffsetPosition = worldPos.xyz + normalOffset + constantOffset;
    highp float cascadeIdx = 0.; //clamp(floor(vCascadeIdx), 0., 2.); //introduces a branching
    highp vec3 smCoord = (global_ShadowMapViewProjs[int(cascadeIdx)] * vec4(SMOffsetPosition.xyz, 1.) ).xyz;
	
    highp float realShadow = ShadowMapSampling(smCoord);
        
    return clamp(realShadow, 0., 1.);
    
    //color *= mix(vec3(1., 1., 1.), global_ShadowColor.rgb, coefficient);
    
#ifdef DEBUG_SHADOW_MAP_CASCADE
    if(cascadeIdx == 0.)
        color *= vec3(1.0, 0.0, 0.0);
    else if(cascadeIdx == 1.)
        color *= vec3(0.0, 1.0, 0.0);
    else if(cascadeIdx == 2.)  
        color *= vec3(0.0, 0.0, 1.0);
    else if(cascadeIdx == 3.)
        color *= vec3(1.0, 1.0, 0.0);
    else
        color *= vec3(1.0, 0.0, 1.0);
#endif
}
#endif

///////////////////////////////// DYNAMIC LIGHTING ////////////////////////////////////
void addForwardLighting(inout highp vec3 ld, inout highp vec3 ls, inout highp float fog, highp vec3 normalWS, highp vec3 positionWS)
{
    // Omnis without Spec
    for(int i = 0; i < omniLightMatricesSize; i++)
    {
	      // get values from matrix
	      PointLightStruct omniLight = MatToStructOmni(omniLightMatrices[i]);

        DirectSpecularLightStruct p;
        p =	computePointLighting(omniLight.s_lightPos, omniLight.s_lightColor, omniLight.s_attenuation, normalWS, positionWS, omniLight.s_ambientFactor);
        ld += p.directLight.rgb;
    }

    // Omnis with Spec
    for(int i = 0; i < omniSpecLightMatricesSize; i++)
    {
	      // get values from matrix
	      PointLightStruct omniLight = MatToStructOmni(omniSpecLightMatrices[i]);

        DirectSpecularLightStruct p;
        p =	computePointLightingWithSpecular(camPosition.xyz, omniLight.s_lightPos, omniLight.s_lightColor, vec4(SpecularColor, SpecularGloss), omniLight.s_attenuation, normalWS, positionWS, 1.0, omniLight.s_ambientFactor);
        ld += p.directLight.rgb;
        ls += p.specularLight * omniLight.s_specIntensity;
    }

    // Boxlights
    for(int i = 0; i < boxLightMatricesSize; i++)
    {
	      // get values from matrix
	      BoxLightStruct boxLight = MatToStructBox(boxLightMatrices, i * BOX_LIGHT_ARRAY_SIZE);

        ld += computeBoxLighting(boxLight.s_lightDir, boxLight.s_lightColor1, boxLight.s_lightColor2, boxLight.s_attenuation, boxLight.s_lightPos, boxLight.s_aabbSize, boxLight.s_scaleOffset, positionWS).rgb;
    }

    // BoxFoglights
    #ifdef USE_FOGLIGHT
    for(int i = 0; i < boxFogLightMatricesSize; i++)
    {
	      // get values from matrix
	      BoxLightStruct boxLight = MatToStructBox(boxFogLightMatrices, i * BOX_LIGHT_ARRAY_SIZE);

        fog += computeBoxFogLighting(boxLight.s_lightDir, boxLight.s_lightColor1, boxLight.s_lightColor2, boxLight.s_attenuation, boxLight.s_lightPos, boxLight.s_aabbSize, boxLight.s_scaleOffset, positionWS);
    }
    #endif
}




void main(void)
{          
    highp vec4 albedo = vec4(0.);
    highp vec3 positionWS = vec3(0.);
    highp vec3 normalWS = vec3(0.);
    highp vec4 multi = vec4(0.);
    
#undef USE_LAST_FRAG_DATA
    /////////////////////////////////////////////////////////
    #ifdef USE_LAST_FRAG_DATA
		albedo = frag0;
		positionWS = frag1.rgb;
		normalWS = normalize(frag2.rgb);
		multi = frag3;
	#else
		albedo = texture2D(texture0, vUV);
		positionWS = texture2D(texture1, vUV).rgb;
		normalWS = normalize(texture2D(texture2, vUV).rgb);
		multi = texture2D(texture3, vUV);
	#endif
	
	//highp vec3 emissive = multi.rgb;
	highp float specStrength = multi.x;
	highp float reflectStrength = multi.y;
	highp vec2 roughness = multi.zw;
	
	#ifdef USE_SSAO
		//highp float depth = texture2D(depthSampler, vUV).r;
		highp float ssao = texture2D(ssaoSampler, vUV).r;
		
		ssao = smoothstep(0., 1., ssao);
		ssao = pow(ssao, ssaoStrength);
		
		#ifdef SSAO_DEBUG
			gl_FragColor = vec4(mix(global_ShadowColor, vec3(1.0), vec3(ssao)), 1.0);//vec4(vec3(ssao), 1.0);
			
			return;
		#endif
	#endif
    /////////////////////////////////////////////////////////

    // Non deferred pixel (requires alpha channel in albedo texture)
    //if(albedo.a == 0.)
    //{
    //    gl_FragColor = albedo;
    //    return;
    //}

    highp vec3 c = albedo.rgb;
    highp vec3 ld = vec3(0.);
    highp vec3 ls = vec3(0.);
	highp vec3 ls2 = vec3(0.);
	
    highp vec3 viewDir = normalize(camPosition - positionWS);
    highp vec3 reflectDir = -normalize( reflect(viewDir, normalWS) );
    highp float ndotv = max(dot(normalWS, viewDir), 0.00001);

    // Ambient
    //highp vec3 ambientLight = computeAmbientLighting(normalWS, global_AmbientColorA , global_AmbientColorB, global_AmbientColorC).rgb * IndirectLight(normalWS, reflectDir, ndotv, reflectStrength, ls);
	highp vec3 ambientLight = global_AmbientColorA.rgb * IndirectLight(normalWS, reflectDir, ndotv, reflectStrength, ls);

    

    // Sun
    highp vec3 sunLight = global_SunColor.rgb * DirectLight(normalWS, global_SunDirection, reflectDir, specStrength, roughness, ls2);
	
    // Sun shadows
    #ifdef USE_SHADOW_MAP
        mediump float shadowCoefficient = 1.0 - ReceiveShadow(vec4(positionWS, 1.), normalWS);
        sunLight = mix(global_ShadowColor, sunLight, shadowCoefficient);
    #endif

    ld += sunLight; //dLight
    ld += ambientLight; //iLight
	
	// Occlusion
    #ifdef USE_SSAO
		ld = mix(ld*global_ShadowColor, ld, ssao);
    #endif	

    // Fog
    //highp float fog = computeFogLighting(global_FogNearFar, camPosition, positionWS);

    // Brute force lighting
    //addForwardLighting(ld, ls, fog, normalWS, positionWS);

    // Rim light
    //ls += computeRimLight(global_RimLightColor.rgb, global_RimLightSize, global_RimLightIntensity, normalWS, camPosition, positionWS);

    // Composite
    c *= ld;
	
	//TODO: Fix this
    c += global_AmbientColorA.rgb * ls + global_SunColor.rgb * ls2;

    // Emissive
    //c += emissive;

    // Fog
    //c = mix(c, global_FogLightColor.rgb, global_FogLightColor.a * fog);
	
    c = pow( c, vec3(1.0/2.2) );
    ////////////////////////////////////////////////////
    gl_FragColor = min(vec4(c, 1.), vec4(1.0));
}


