#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
  #define ES3
#endif

#if defined(GL_OES_standard_derivatives)
  #extension GL_OES_standard_derivatives : enable
  #define USE_DERIVATIVES
#else
#endif

#if defined(GL_EXT_shader_framebuffer_fetch)
  #extension GL_EXT_shader_framebuffer_fetch : enable
  #define USE_LAST_FRAG_DATA
#endif

#ifdef ES3
  #define varying in
  #define texture2D texture
  #define gl_FragColor fragData0
  #define frag0 fragData0
  #define frag1 fragData1
  #define frag2 fragData2
  #define frag3 fragData3
  #if (defined USE_LAST_FRAG_DATA && defined USE_SOFT)
    layout (location = 0) inout highp vec4 fragData0;
    layout (location = 1) inout highp vec4 fragData1;
    layout (location = 2) inout highp vec4 fragData2;
    layout (location = 3) inout highp vec4 fragData3;
  #else
    layout (location = 0) out highp vec4 fragData0;
    layout (location = 1) out highp vec4 fragData1;
    layout (location = 2) out highp vec4 fragData2;
    layout (location = 3) out highp vec4 fragData3;
  #endif
  #define shadowmapSampler sampler2Dshadow
#else
  #define shadowmapSampler sampler2D
  #define frag0 gl_FragColor[0]
  #define frag1 gl_FragColor[1]
  #define frag2 gl_FragColor[2]
  #define frag3 gl_FragColor[3]
#endif




varying lowp vec2 vUV;

#if !(defined GLITCH_HLSL_VS_4_0_LEVEL_9_3 || defined GLITCH_HLSL_PS_4_0_LEVEL_9_3)
#ifndef GAMMA_CORRECT
varying lowp float vExposure;
#endif
#endif // not (defined GLITCH_HLSL_VS_4_0_LEVEL_9_3 || defined GLITCH_HLSL_PS_4_0_LEVEL_9_3)

uniform lowp int exposureEnabled;

uniform sampler2D texture0;
uniform highp vec2 texture0TexelSize;


// Gamma value
#define GAMMA 2.2
highp vec3 linearToScreen(highp vec3 t){ return pow(t, vec3(1.0/GAMMA)); }

#ifndef GAMMA_CORRECT

uniform sampler2D blur0;
uniform sampler2D blur1;
uniform sampler2D blur2;
uniform sampler2D depthTexture;
uniform sampler2D dofTexture;
uniform highp float bloomIntensity;
uniform highp float bloomClamp;
uniform highp vec3 colorCorrectScale;
uniform highp vec3 colorCorrectOffset;
uniform highp float colorCorrectSaturation;
uniform highp float vignetteIntensity;
uniform highp float vignetteContrast;
uniform highp vec4 vignetteColor;
uniform highp float dofIntensity;
uniform highp float dofDistance;
uniform highp float dofWidth;
uniform highp vec2 camNearFar;
uniform lowp int viewRange;
uniform highp float time;
uniform lowp int bloomEnabled;



// Filmic ALU, Includes gamma correction
highp vec3 FilmicALU(highp vec3 t)
{
    t = max(vec3(0.), t - .004);
    return (t * (6.2 * t + .5)) / (t * (6.2 * t + 1.7) + .6);
}

// Filmic by John Hable, DOES NOT includes gamma correction
highp vec3 Hable(highp vec3 t)
{
	  highp float A = .22; // Shoulder strength
	  highp float B = .30; // Linear strength
	  highp float C = .10; // Linear angle
	  highp float D = .20; // Toe strength 
	  highp float E = .01; // Toe numerator
	  highp float F = .30; // Toe denominator 
	  
    return ((t * (A * t + C * B) + D * E) / (t * (A * t + B) + D * F)) - E / F;
}

// Simplified filmic version
highp vec3 simpleFTM(highp vec3 t)
{
    highp float A = 10.; /// Mid
    highp float B = 0.3; /// Toe
    highp float C = 0.5; /// Shoulder
    highp float D = 1.5; /// Mid

    return (t * (A * t + B)) / (t * (A * t + C) + D);
}

// Reinhard ton mapping
highp vec3 Reinhard(highp vec3 t, highp float whitePoint)
{
    highp float lum = dot(t, vec3(.2126, .7152, .0722)) + .00001;
    highp float nl = (lum * (1. + lum / whitePoint)) / (1. + lum);
    return nl * t / lum;
}

highp vec2 cubicUV(highp vec2 uv, highp vec2 texSize)
{
	uv = uv * texSize + .5;
	highp vec2 iuv = floor( uv );
	highp vec2 fuv = fract( uv );
	uv = iuv + fuv * fuv * (3. - 2. * fuv);
	uv = (uv - .5) / texSize;
	return uv;
}

highp vec2 n(highp vec2 uv, highp float f)
{
	return f *(uv - .5) + .5;
}

highp vec3 AddBloom(highp vec3 t)
{
    if(bloomIntensity == 0.)
      return t;
    
    highp vec4 bloom = vec4(0.0);
    
    bloom += texture2D(blur0, vUV) * 0.33;
    bloom += texture2D(blur1, vUV) * 0.33;
    bloom += texture2D(blur2, vUV) * 0.33;
    
    //TEMP FIX FOR FLICKERING IN MENUS
    bloom = clamp(bloom, vec4(0.), vec4(bloomClamp));
    
    return t + bloom.rgb * bloomIntensity;
}

highp vec3 ApplyToneMap(highp vec3 t, highp float wp)
{	
	#if defined TONEMAP_LINEAR
		t.rgb /= wp;
	#elif defined TONEMAP_FILMIC
		t.rgb = FilmicALU(t.rgb) / FilmicALU(vec3(wp));
	#elif defined TONEMAP_HABLE
		t.rgb = Hable(t.rgb) / Hable(vec3(wp));
	#elif defined TONEMAP_REINHARD
		t.rgb = Reinhard(t.rgb, wp);
	#elif defined TONEMAP_COMPARE
		if(vUV.y < .5)
		{
			if(vUV.x < .5)
			{
				t.rgb /= wp;
			}
		  	else
		  	{
		  		t.rgb = FilmicALU(t.rgb) / FilmicALU(vec3(wp));
		  	}
		}
		else
		{
			if(vUV.x < .5)
			{
				t.rgb = Hable(t.rgb) / Hable(vec3(wp));
			}
			else
			{
				t.rgb = Reinhard(t.rgb, wp);
			}
		}
	#endif
	
	return t;
}

highp vec3 ApplyColorCorrection(highp vec3 t)
{
	t = t * colorCorrectScale + colorCorrectOffset;
	
	if(colorCorrectSaturation != 1.)
	{
    highp float desturated = max(0., dot(t, vec3(.3, .59, .11)));
    t = mix(vec3(desturated), t, colorCorrectSaturation);
	}
	
	return t;
}

highp float LinearizeDepth(mediump float z_b, highp vec2 nearFar)
{
    highp float z_n = 2.0 * z_b - 1.0;
    highp float z_e = 2.0 * nearFar.x * nearFar.y / (nearFar.y + nearFar.x - z_n * (nearFar.y - nearFar.x));
    return (z_e - nearFar.x) / (nearFar.y - nearFar.x);
}

highp float DepthToWorld(highp vec2 uv, highp vec2 nearFar)
{
    highp float d = LinearizeDepth(texture2D(depthTexture, uv).r, camNearFar);
    return d * (nearFar.y - nearFar.x) + nearFar.x;
}

highp float DOFrange(highp float d)
{
    return max(0., abs(d - dofDistance) - dofWidth);
}

const mediump int steps = 12;
highp vec4 ComputeDepthOfField(highp vec2 uv)
{
    if(dofIntensity <= 0.)
      return texture2D(texture0, uv);

    highp vec4 c = vec4(.0);
    highp vec2 offset[12];
    offset[0] = vec2(-1.5,  -.5);
    offset[1] = vec2(  .5, -1.5);
    offset[2] = vec2( 1.5,   .5);
    offset[3] = vec2( -.5,  1.5);  
      
    offset[4] = vec2(-1.5, -2.5);
    offset[5] = vec2( 2.5, -1.5);
    offset[6] = vec2( 1.5,  2.5);
    offset[7] = vec2(-2.5,  1.5); 
     
    offset[8]  = vec2(-3.5,  -.5);
    offset[9]  = vec2(  .5, -3.5);
    offset[10] = vec2( 3.5,   .5);
    offset[11] = vec2( -.5,  3.5);
    
    for(int i = 0; i < steps; i++)
    {
        c += texture2D(texture0, uv + offset[i] * texture0TexelSize);
    }
    c /= float(steps);
    
    return c;
}

// #undef USE_LAST_FRAG_DATA
highp vec3 ApplyDepthOfField(highp vec3 t, highp vec2 uv)
{
    if(dofIntensity <= 0.)
      return t;
      
    // #ifdef USE_LAST_FRAG_DATA
    // highp float depth = frag0.a;
    // #else
    highp float depth = DepthToWorld(uv, camNearFar);
    // #endif
    highp float dof = DOFrange(depth);
    dof = clamp(dofIntensity * dof, 0., 1.);
    
    if(viewRange == 1)
    {
        return mix(t, vec3(dof), .5);
    }
    
    highp vec4 c = texture2D(dofTexture, uv);
    return mix(t, c.rgb, dof);
}

highp vec3 ApplyAutoExposure(highp vec3 t)
{	  
   if(exposureEnabled == 0)
    return t;
  
  #ifndef GLITCH_HLSL_PS_4_0_LEVEL_9_3  
	return t * vExposure;
  #endif // not defined GLITCH_HLSL_PS_4_0_LEVEL_9_3
}

highp vec3 AddVignette(highp vec3 t, highp vec2 uv)
{
  if(vignetteIntensity == 0.)
    return t;

  highp vec2 cuv = uv;
  highp float vign = 1. - (((dot(1. - cuv, cuv) * 2. - .5) * vignetteContrast) * .5 + .5);
  vign = clamp((vign * vignetteIntensity), 0., 1.);
  return t * (1. - (vign * (1. - vignetteColor.rgb)));
}

#endif


highp vec4 getFrameBuffer(highp vec2 uv)
{
	// #ifdef USE_LAST_FRAG_DATA
  // return frag0;
  // #else
  return texture2D(texture0, uv);
 // #endif
}

void main()
{	   
  #ifdef GAMMA_CORRECT
  
  // Apply gamma
   highp vec4 t = texture2D(texture0, vUV);
   t.rgb = linearToScreen(t.rgb);
   
	gl_FragColor = t;
	return;
	
  #else
  
  #ifdef DOF
  
  gl_FragColor = ComputeDepthOfField(vUV);
	return;
  
  #else
    
    
	highp vec4 c = vec4(1.);
	  
	// Input   
  c.rgb = getFrameBuffer(vUV).rgb;

  // Apply depth-of-field
  c.rgb = ApplyDepthOfField(c.rgb, vUV);
    	  
  // Auto exposure
  #ifndef GLITCH_HLSL_PS_4_0_LEVEL_9_3
  c.rgb = ApplyAutoExposure(c.rgb);
  #endif // not GLITCH_HLSL_PS_4_0_LEVEL_9_3
  
  // Apply tone map
  c.rgb = ApplyToneMap(c.rgb, 2.0);
  
  // Add bloom
  c.rgb = AddBloom(c.rgb);
  
  // Apply color correction
  c.rgb = ApplyColorCorrection(c.rgb);
  
  // Apply vignette
  c.rgb = AddVignette(c.rgb, vUV);
  
  //c.rgb = vec3(0.);
  //c.rgb = AddBloom(c.rgb);
	
	gl_FragColor = c;
	
	#endif
	#endif
}



