#ifdef DCC_MAX
	#define DCC(x) x
#else
	#define DCC(x)
#endif

#ifdef ANIMFX
	uniform highp sampler2D AnimFXPos1;
	uniform highp sampler2D AnimFXPos2;
	uniform highp sampler2D AnimFXRot;
	uniform highp sampler2D AnimFXCol;
	uniform highp float time;
	uniform highp float Speed;
	uniform highp vec3 BoundsShift;
	uniform highp vec3 BoundingBox;    
    
    attribute mediump vec2 TexCoord0 DCC(: TEXCOORD0); 
    uniform mediump vec3 eyepos;
#endif

attribute highp vec4 Vertex DCC(: POSITION);

// Values that stay constant for the whole mesh.
//uniform highp mat4 ShadowMatrix;
uniform highp mat4 matWorld;
uniform highp mat4 matviewprojection;

void main()
{
#ifdef ANIMFX
    highp vec2 PanCoords = vec2(TexCoord0.x, (TexCoord0.y + Speed - 0.5));
    
    highp vec4 AnimRot = texture2D(AnimFXRot,  PanCoords);
    highp vec4 AnimPos1 = texture2D(AnimFXPos1, PanCoords);
    highp vec4 AnimPos2 = texture2D(AnimFXPos2, PanCoords);

    highp vec4 AnimPos = vec4(
        dot(vec2(AnimPos1.x, AnimPos2.x), vec2(1.0, 1.0/255.0)), 
        dot(vec2(AnimPos1.y, AnimPos2.y), vec2(1.0, 1.0/255.0)), 
        dot(vec2(AnimPos1.z, AnimPos2.z), vec2(1.0, 1.0/255.0)), 
        AnimPos1.w);

    highp vec4 axis_angle = normalize((AnimRot - 0.5) * 2.0);
    
    highp vec4 worldPos = mix (vec4(eyepos, 1.0),// Hidden Vert Location behind camera
        matWorld * //World Matrix
        (vec4((Vertex.xyz + 2.0 * cross(axis_angle.xyz, cross(axis_angle.xyz, Vertex.xyz) + axis_angle.w * Vertex.xyz)), 1.0)  //Rotation at zero point
        + 
        vec4( // Position within bounding box
            mix((BoundingBox.x/2.0)*-1.0, (BoundingBox.x/2.0) ,AnimPos.x) + BoundsShift.x, //X value Calculation
            mix((BoundingBox.y/2.0)*-1.0, (BoundingBox.y/2.0) ,AnimPos.y) + BoundsShift.y, //Y value Calculation 
            mix((BoundingBox.z/2.0), (BoundingBox.z/2.0)*-1.0 ,AnimPos.z) + BoundsShift.z, //Z value Calculation 
            0.0)
        ),
        floor(AnimPos.a)// Mask for Hidden Verts
    ); 
    
    gl_Position = matviewprojection * worldPos;
#else    
    gl_Position = matviewprojection * matWorld * Vertex;
#endif
 }