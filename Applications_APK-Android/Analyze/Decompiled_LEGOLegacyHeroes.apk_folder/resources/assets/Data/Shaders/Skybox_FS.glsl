#version 300 es
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif



#define PI 3.141592

// varyings
varying highp vec3 vNormal;

#ifdef CUBEMAP
    uniform highp samplerCube global_reflMap;
#endif

#ifdef PANORAMA
    uniform highp sampler2D skyboxPan;
#endif

highp vec2 DirectionToPanoramaUV( highp vec3 dir )
{
    highp vec2 uv;
    highp float r = length(dir.xz);
    highp float theta = (dir.x < 0.0) ? asin(dir.z/r) : (-asin(dir.z/r) + PI);
    uv.x = theta / (2.0*PI);
    uv.y = 0.5 * (-dir.y + 1.0);
    return uv;
}

highp vec3 DecodeRGBM( highp vec4 rgbm )
{
    return 6.0 * rgbm.a * rgbm.rgb;
}

void main(void)
{
    highp vec3 n = normalize(vNormal);
    
    highp vec4 env = vec4(0.0);
    
 #ifdef CUBEMAP
    env = textureCubeLod(global_reflMap, n, 1.0);
 #endif   
 
 #ifdef PANORAMA
    highp vec2 uv = DirectionToPanoramaUV(n);
    env = texture2D(skyboxPan, uv);
 #endif
      
    gl_FragColor = vec4( DecodeRGBM( env ), 1.0 );
}


