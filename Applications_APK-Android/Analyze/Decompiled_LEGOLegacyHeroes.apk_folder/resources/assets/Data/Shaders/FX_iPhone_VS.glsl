#version 300 es

#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif


#ifdef DCC_MAX
    #define DCC(x) x
#else
    #define DCC(x)
#endif


varying	mediump vec2 vCoord0 DCC(: TEXCOORD4);
varying	mediump vec2 vCoord1 DCC(: TEXCOORD5);

#ifndef IGNORE_VERT_COLOR
varying mediump vec4 vColor  DCC(: TEXCOORD6);
#endif


attribute highp vec4 Vertex DCC(: POSITION);
attribute highp vec2 TexCoord0 DCC(: TEXCOORD0);

#ifndef IGNORE_VERT_COLOR
    #ifdef DCC_MAX
        attribute lowp vec4  Color : TEXCOORD1;
        attribute lowp float Alpha : TEXCOORD2;
    #else
        attribute lowp vec4  Color;  
    #endif
#endif
  


uniform highp mat4 WorldViewProjectionMatrix;
uniform   mediump	vec2 UVTiling; 
uniform   mediump	vec2 UVShearing;
uniform   mediump	vec2 UVOffset;
uniform   mediump   vec2 UVAOffset;
uniform   mediump   vec4 DiffuseColor;
uniform   mediump   float BurnFactor;

void main() 
{
	mediump vec2 tCoord = vec2(TexCoord0.y, TexCoord0.x);

	vCoord0        = TexCoord0 * UVTiling + tCoord * UVShearing;
	vCoord0.y	   = vCoord0.y + 1.0 - UVTiling.y;
	vCoord1		   = vCoord0 + UVAOffset;
	vCoord0		   = vCoord0 + UVOffset;
    #ifndef IGNORE_VERT_COLOR
        #ifdef DCC_MAX
            vColor         = vec4(Color.rgb, Alpha) * DiffuseColor * BurnFactor;
        #else
            vColor         = Color * DiffuseColor * BurnFactor;
        #endif
    #endif
	gl_Position    = WorldViewProjectionMatrix * Vertex;
}

