#ifdef DCC_MAX
	#define DCC(x) x
#else
	#define DCC(x)
#endif

attribute highp   vec4 Vertex DCC(: POSITION);
attribute mediump vec2 TexCoord0 DCC(: TEXCOORD0);
attribute mediump vec3 Normal DCC(: NORMAL);

#ifdef DCC_MAX
	attribute lowp vec3  Color : TEXCOORD3;
	attribute lowp float Alpha : TEXCOORD4;
#else
	attribute lowp vec4  Color;
#endif

#ifdef OUTLINE
	uniform lowp float OutlineThickness;
#endif

uniform highp   mat4 WorldViewProjectionMatrix;
uniform highp   mat4 WorldViewMatrix;
uniform highp   mat4 MatProjection;
uniform highp   vec4 eyeposos;

varying mediump vec4 vColor DCC(:TEXCOORD0);
varying mediump vec2 vCoord0 DCC(:TEXCOORD1);
varying mediump vec3 vNormal DCC(:TEXCOORD2);

void main()
{	

	#ifdef OUTLINE
		highp vec4 projPos = WorldViewProjectionMatrix * (Vertex + (vec4(Normal.xyz, 0.0) * OutlineThickness));
	#else
		highp vec4 projPos = WorldViewProjectionMatrix * Vertex;
	#endif
	
	vCoord0 = TexCoord0;
	vNormal = Normal;
	
	#ifdef DCC_MAX
		vColor.rgb = Color;
		vColor.a = Alpha;
	#elif defined(GLITCH_EDITOR)
		vColor = Color;
	#else
		vColor = Color;
	#endif
	
	gl_Position = projPos;
	
} // main end