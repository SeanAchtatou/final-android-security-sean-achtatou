#extension GL_EXT_shadow_samplers : enable
#define REFLECTIONS
#ifdef GL_ES    
    #extension GL_EXT_shader_texture_lod :enable
	#if defined(GL_EXT_shader_texture_lod)
		#define textureCubeLod textureCubeLodEXT
	#else	
		#undef REFLECTIONS
	#endif
#endif


#ifdef DCC_MAX
	#define DCC(x) x
    #undef SHADOWS
	#extension GL_EXT_shadow_samplers : disable
#else
	#define DCC(x)
#endif

#define PI 3.14159265359
#define _F0 0.0082644628099174 // equal to an IOR of 1.2
#define SPEC_EXP 76.125 // Roughness 0.4
#define SPEC_MUL 12.433979929054323106944044013478 //Roughness 0.4

#ifdef ALBEDO_MAP
	uniform lowp sampler2D AlbedoMap;
	
	#ifdef ALBEDO_TWO 
		uniform lowp sampler2D AlbedoMapTwo;	
	#endif

	#ifdef SEL_FACE
		uniform lowp float SimpleFaceIndex;
	#endif		
#endif

#ifndef DEFERRED

	uniform mediump sampler2D global_brdfTable;
	uniform mediump samplerCube global_reflMap;
	uniform mediump float global_reflStrength;
	uniform mediump float global_numMipMaps;

	uniform mediump vec3 global_cC;
	uniform mediump vec3 global_cAr;
	uniform mediump vec3 global_cAg;
	uniform mediump vec3 global_cAb;
		
	uniform mediump vec3 global_SunDirection;
	uniform mediump vec4 global_SunColor;
	uniform mediump vec4 global_AmbientColorA;
#endif

	uniform mediump vec2  Roughness;

#ifdef ANIM_FACE
	uniform lowp sampler2D FaceAtlas;
	uniform lowp sampler2D FaceAtlas_Alpha;		
	uniform lowp vec3 HairAlbedo;
	uniform lowp vec3 EyeBrowSelection;	
	uniform	highp vec4 EyeXatt;	
	uniform	highp vec4 EyeYatt;		
	uniform	highp vec4 MouthAtt;		
	uniform	highp vec4 BrowAtt;				
	uniform	highp vec4 ScaleAtt;

#endif

#ifdef ALBEDO_MAP_ALPHA	
	uniform lowp sampler2D AlbedoMapAlpha;	
#endif


#ifdef DEBUG_MATERIAL
	uniform lowp vec4 DebugColor;
#endif

#ifdef LIGHT_MAP
	uniform lowp sampler2D LightMap;
#endif

varying	mediump vec4 vCoord01      DCC(:TEXCOORD0);
varying mediump vec2 vCoord03	   DCC(:TEXCOORD1);
varying mediump vec4 vColor	       DCC(:TEXCOORD2);
varying mediump vec3 vWNormal 	   DCC(:TEXCOORD3);
varying mediump vec3 vWView  	   DCC(:TEXCOORD4);
#ifdef DEFERRED
	varying highp   vec3 vWPosition	   DCC(:TEXCOORD4);
#endif

uniform mediump float global_ACES_slope;
uniform mediump float global_ACES_toe;
uniform mediump float global_ACES_shoulder;
uniform mediump float global_ACES_blackClip;
uniform mediump float global_ACES_whiteClip;

#if defined(SHADOWS)
    #ifdef GL_EXT_shadow_samplers
        #define SHADOW_MAP_CASCADE_COUNT 2
        varying highp vec3 vSMCoord0;
        
        uniform highp sampler2DShadow global_ShadowMapSampler;
        uniform highp vec2 global_ShadowMapScale;
        uniform mediump vec3 global_ShadowColor;

        //uniform highp vec4 global_SMSplits;
        //uniform highp float global_SMIntersticeInvSize;

        mediump float SampleShadowMap(mediump vec2 baseUV, mediump vec2 offset, highp float depth)
        {
            return shadow2DEXT(global_ShadowMapSampler, vec3(baseUV + offset * global_ShadowMapScale, depth));
        }

        mediump float SampleShadowMapPCF3x3(highp vec2 smCoord, highp float depth)
        {
            highp vec2 uv = smCoord.xy / global_ShadowMapScale; 

            mediump vec2 baseUV;
            baseUV.x = floor(uv.x + 0.5);
            baseUV.y = floor(uv.y + 0.5);

            mediump float s = (uv.x + 0.5 - baseUV.x);
            mediump float t = (uv.y + 0.5 - baseUV.y);
            
            baseUV -= vec2(0.5, 0.5);
            baseUV *= global_ShadowMapScale;
            
            mediump float sum = 0.0;

            mediump float uw0 = (3. - 2. * s);
            mediump float uw1 = (1. + 2. * s);

            mediump float u0 = (2. - s) / uw0 - 1.;
            mediump float u1 = s / uw1 + 1.;

            mediump float vw0 = (3. - 2. * t);
            mediump float vw1 = (1. + 2. * t);

            mediump float v0 = (2. - t) / vw0 - 1.;
            mediump float v1 = t / vw1 + 1.;

            sum += uw0 * vw0 * SampleShadowMap(baseUV, vec2(u0, v0), depth);
            sum += uw1 * vw0 * SampleShadowMap(baseUV, vec2(u1, v0), depth);
            sum += uw0 * vw1 * SampleShadowMap(baseUV, vec2(u0, v1), depth);
            sum += uw1 * vw1 * SampleShadowMap(baseUV, vec2(u1, v1), depth);

            return sum * 1.0 / 16.0;
        }

        mediump float GetShadowCoefficient()
        {
            return SampleShadowMapPCF3x3(vSMCoord0.xy, clamp(vSMCoord0.z, 0.0, 1.0));
        }
    #else
        uniform highp sampler2D global_ShadowMapSampler; 
		lowp float GetShadowCoefficient() 
		{ 
			highp vec3 projCoords = shadowPos.xyz / shadowPos.w;
			projCoords = projCoords * 0.5 + 0.5; 
	
			highp float sceneDepth = texture2D(global_ShadowMapSampler, projCoords.xy).r; 
			highp float fragDepth = min(projCoords.z, 1.0); 
			//return float( fragDepth < sceneDepth ); //standard says float(true) = 1.0 

			#ifdef BIAS
				lowp float bias = max(ShadowBias, 0.002);
			#else
				lowp float bias = 0.002;
			#endif
		
			
			#ifdef OS_ANDROID
                if ( sceneDepth < fragDepth - bias)
                {
                    return 0.0;
                }
                else
                {
                    return 1.0;
                }
			#else
				return ( sceneDepth < fragDepth - bias? 0.0 : 1.0);
			#endif
		} 
    #endif
#endif

mediump vec3 sRGB_To_Linear(lowp vec4 sRGB)
{
	// cubic is a faster approximation of the pow function
	return sRGB.rgb * (sRGB.rgb * (sRGB.rgb * 0.305306011 + 0.682171111) + 0.012522878);
}

mediump vec3 Linear_To_sRGB(mediump vec3 lin)
{
    return pow( lin, vec3(0.45) );
}

const mediump float WhiteLevel = 2.0;
mediump vec3 WhitePreservingReinhard(mediump vec3 color)
{
    mediump float luma = dot(color, vec3(0.2126, 0.7152, 0.0722));
    mediump float toneMappedLuma = luma * (1. + luma / (WhiteLevel*WhiteLevel)) / (1. + luma);
    color *= toneMappedLuma / luma;
    color = pow(color, vec3(1.0/2.2));
    return color;
}

mediump vec3 ACESTonemapping( mediump vec3 x )
{
    mediump float a = global_ACES_slope;
    mediump float b = global_ACES_toe;
    mediump float c = global_ACES_shoulder;
    mediump float d = global_ACES_blackClip;
    mediump float e = global_ACES_whiteClip;
    
    mediump vec3 aces_val = (x*(a*x+b))/(x*(c*x+d)+e);
    
    return clamp(
        Linear_To_sRGB( aces_val ),
        vec3(0), vec3(1)
    );
}

#ifndef DEFERRED
mediump float NDF_GGX( mediump float NoH, mediump float r )
{
    mediump float a2 = r*r; 
    mediump float ndf = a2 / max((NoH*NoH*(a2-1.0) + 1.0), 0.0001);
    ndf *= ndf;
    ndf *= 1.0 / max((a2*a2*PI), 0.0001);
    return ndf;
}

mediump vec3 Fresnel_Schlick(mediump float VoH, mediump vec3 F0)
{
    return F0 + (vec3(1.0) - F0)*pow(1.0-VoH, 5.0);
}

mediump float GGX_GeometryTerm( mediump float VoN, mediump float VoH, mediump float roughness )
{
    mediump float alpha = roughness;
    mediump float VoH2 = VoH*VoH;
    mediump float tan2 = (1.0 - VoH2) / VoH2;
    mediump float chi = ( (VoH2/VoN) > 0.0 ) ? 1.0 : 0.0;
    return (chi * 2.0) / (1.0 + sqrt( 1.0 + alpha * alpha * tan2 ));
}

mediump vec3 EvaluateL1SH(mediump vec3 dir, mediump vec3 cAr, mediump vec3 cAg, mediump vec3 cAb, mediump vec3 cC)
{
    mediump float x1 = dot(dir.yzx, cAr);
    mediump float x2 = dot(dir.yzx, cAg);
    mediump float x3 = dot(dir.yzx, cAb);
    
    return (0.886227*cC) + (1.023328*vec3(x1, x2, x3));
}

mediump vec3 DirectLight( mediump vec3 albedo, mediump float NoL, mediump float NoH, mediump float NoV, mediump float VoH, mediump float LoH )
{
	mediump float rough = 0.05;
	mediump vec3 specColor = vec3(0.04);
    mediump vec3 NDF = specColor * NDF_GGX( NoH, rough );
    mediump vec3 F = Fresnel_Schlick(LoH, specColor);
    mediump vec3 NDF_F = NDF * F;
	mediump float G = clamp( GGX_GeometryTerm( NoV, VoH, rough ) * GGX_GeometryTerm( NoL, LoH, rough ), 0.0, 1.0);
	
	mediump float recip = 4.0 * NoL * NoV;
    mediump vec3 spec = (NDF_F * G)/recip;
	
    mediump float grazingReduction = smoothstep(0.001, 0.01, recip);
    spec *= grazingReduction;
	spec = max(spec, vec3(0.0));
	
	mediump vec3 diffuse = (vec3(1.0)-spec) * albedo; 
	
	mediump vec3 retVal = NoL * (diffuse+spec);
		
	return retVal;
}

mediump vec3 IndirectLight( mediump vec3 albedo, mediump vec3 normal, mediump vec3 reflectDir, mediump float NoV )
{
	#ifdef LIGHT_MAP
		mediump vec3 ambientColor = texture2D(LightMap, vCoord03).rgb;
	#else
		mediump vec3 ambientColor = EvaluateL1SH(normal, global_cAr, global_cAg, global_cAb, global_cC);
	#endif

#ifdef REFLECTIONS
	mediump vec4 envRGBM = textureCubeLod( global_reflMap, reflectDir, 1.0 );
	mediump vec3 envColor = 6.0 * envRGBM.a * envRGBM.rgb;
	mediump vec2 envBrdf = texture2D( global_brdfTable, vec2(0.8, NoV) ).rg; 
	mediump vec3 env = envColor * ((envBrdf.x * 0.04) + envBrdf.y);	
#else
	mediump vec3 env = vec3(0.0);
#endif
	
	mediump vec3 ambient = max( vec3(1.0)-env, vec3(0.001) ) * albedo * ambientColor;
	
	mediump vec3 retVal = ambient+env;
	
	
	return retVal;
}
#endif

void main()
{
	#ifdef OVERDRAW
	    gl_FragColor = vec4(0.4, 0.1,0.0,1.0);
	    return;
	#else
	
	#ifdef SHOW_NORMALS
		gl_FragColor = vec4(vWNormal*0.5 + 0.5, 1.0);
		return;
	#endif

		mediump vec4 finalColor = vec4(0.0, 0.0, 0.0, 1.0);
		
		#ifdef ALBEDO_MAP
			#ifdef SEL_FACE				
				mediump vec4 texAlbedo = texture2D(AlbedoMap, (clamp( (vec2(vCoord01.y, (vCoord01.x * -1.0))*0.5) , vec2(0.0, -0.50), vec2(0.25, 0.0) ) + vec2(SimpleFaceIndex, 0.0)));
			#else			
				#ifdef ALBEDO_MAP_ALPHA
					mediump vec4 texAlbedo = vec4((texture2D(AlbedoMap, vCoord01.xy)).rgb, (texture2D(AlbedoMapAlpha, vCoord01.xy)).r);
				#else			
					mediump vec4 texAlbedo = texture2D(AlbedoMap, vCoord01.xy);	
				#endif
				
				#ifdef ALBEDO_TWO
					mediump vec4 texAlbedoTwo = texture2D(AlbedoMapTwo, vCoord01.xy);
				#endif
			#endif
			
			
			#ifdef MFBODY
				mediump vec3 albedo = sRGB_To_Linear( mix (texAlbedoTwo, texAlbedo, vColor.r) )/PI;
				mediump float alpha = mix (texAlbedoTwo.a, texAlbedo.a,  vColor.r);
			#else	
				mediump vec3 albedo = sRGB_To_Linear(texAlbedo)/PI;
				mediump float alpha = texAlbedo.a;
			#endif
		#elif defined(DEBUG_MATERIAL)
			mediump vec3 albedo = sRGB_To_Linear(DebugColor)/PI;
			mediump float alpha = DebugColor.a;
		#else
			#ifdef SETUNBUILT
				mediump vec3 albedo = vec3(0.01);
				mediump float alpha = 1.0;
			#else	
				mediump vec3 albedo = vColor.rgb;
				mediump float alpha = vColor.a;
			#endif
		#endif
		
		
		//Facial Animation Section
		#ifdef ANIM_FACE
			highp vec2 DCoord = ((vCoord01.xy) + vec2(0.0,0.165))  * vec2(ScaleAtt.x, ScaleAtt.y); // Scaled and shifted coordinates for the Face Calculations
			highp float EyeMask = clamp(floor(vCoord01.x * 2.0), 0.0, 1.0); // This is a vertical mask to seperate the eyes
			
			highp vec2 FaceAtlasCoord = //These are the calculations to generate the Face Atlas UV Coordinates
			mix(// This MIX Combine EyesComb and Mouth
				
				// Below is the Eyes UV calculations
					vec2 ( // Below Composes the Eyes in the X and Y access
							max( // Below combines the eyes in the X access
									(clamp((DCoord.x * -1.0)+EyeXatt.y-EyeXatt.z, 0.00, EyeXatt.x)), // Left Eye Calculations
									(clamp(DCoord.x-EyeXatt.y-(EyeXatt.z*-1.0), 0.00, EyeXatt.x)) // Right Eye Calculations
								) + EyeXatt.a // This is to center the eyes
							, // Below chooses the index of the eyes in the Y access
								clamp(DCoord.y - EyeYatt.x, 0.00, EyeYatt.y) + mix(EyeYatt.z, EyeYatt.a, EyeMask) // Chooses the Index
					)//^ End of Vec2 //End of Eye Calculations
				
				
				, // Below is the Mouth UV calculations
					vec2 ( // Below Composes the Mouth in the X and Y access
							clamp(DCoord.x + MouthAtt.x, MouthAtt.y, MouthAtt.y + EyeXatt.x), //X access of the Mouth Calculations
							clamp(DCoord.y-MouthAtt.z, 0.0, EyeYatt.y) + MouthAtt.a  //Y access of the Mouth Calculations
					)//^ End of Vec2 // End of Mouth Calculatins
					
					
				,// Below is the EyesComb and Mouth MIX mask
					clamp(floor(vCoord01.y * 2.0 + 0.395), 0.0, 1.0) // This is the Mouth/Eye Mask
					
				); //^This Is the End of MIX Combine EyesComb and Mouth
		
			
			lowp vec4 EyesAndMouth = texture2D(FaceAtlas, FaceAtlasCoord);
			EyesAndMouth.a = texture2D(FaceAtlas_Alpha, FaceAtlasCoord).r;		
			
			lowp vec3 EyebrowsAllChan = texture2D(FaceAtlas, 
				// Below Composes the Eye brows in the X and Y access
				vec2 ( //Below Composes the eyeborows in the X access
						max( 
							clamp((DCoord.x * -1.0) + EyeXatt.y + ScaleAtt.z, EyeXatt.x, EyeXatt.x*2.0), // Left Brow Eye Calculations
							clamp((DCoord.x) - EyeXatt.y + ScaleAtt.z, EyeXatt.x, EyeXatt.x*2.0) // Right Brow Eye Calculations
						)//^ end of max
						
					, // Below is the calulations for EyeBrow the Y access
						clamp(DCoord.y-(mix(BrowAtt.z, BrowAtt.a, EyeMask)), 0.00, EyeYatt.y) + mix(BrowAtt.x, BrowAtt.y, EyeMask)// Chooses the Index
						
				)  //^ end of vec2
			
			).rgb  * EyeBrowSelection;  // This isolates the selected channel for use
			
			lowp float Eyebrows = EyebrowsAllChan.r + EyebrowsAllChan.g + EyebrowsAllChan.b;
			
			#ifdef ALBEDO_MAP
				#ifdef ALBEDO_MAP_ALPHA
					albedo = mix (albedo.rgb, (sRGB_To_Linear(vec4 ((mix (EyesAndMouth.rgb, HairAlbedo, Eyebrows)), 1.0))/PI) * 2.0, (((max (Eyebrows, EyesAndMouth.a))*((texture2D(AlbedoMapAlpha, vCoord01.xy).r)*-1.0+1.0))) );
				#else
					albedo = mix (albedo.rgb, (sRGB_To_Linear(vec4 ((mix (EyesAndMouth.rgb, HairAlbedo, Eyebrows)), 1.0))/PI) * 2.0, (max (Eyebrows, EyesAndMouth.a)) );
				#endif
			#endif						
		#endif
		
		#ifdef DCC_MAX	
			lowp vec3 finalColorOveride = albedo.rgb;
			
	//		#ifdef ANIM_FACE
	//			lowp vec3 finalColorOveride = (albedo.rgb * 0.0001) + vec4 (EyesAndMouth.a, EyesAndMouth.a, EyesAndMouth.a, EyesAndMouth.a); // This Sections Bipasses the Build Mode and Lighting Sections for Max		
	//		#else
	//			lowp vec3 finalColorOveride = albedo.rgb; // This Sections Bipasses the Build Mode and Lighting Sections for Max	
	//		#endif	
		#endif


		#ifndef DEFERRED
			
			mediump vec3 normal  = normalize(vWNormal);
			mediump vec3 viewDir = normalize(vWView);
			mediump vec3 halfVec = normalize(viewDir+global_SunDirection.xyz);
			mediump vec3 reflectDir = normalize( reflect(-viewDir, normal) );
			
			mediump float ndotl = max(dot(normal, global_SunDirection),0.00001);
			mediump float ndotv = max(dot(normal, viewDir), 0.00001);
			mediump float ndoth = max(dot(normal, halfVec), 0.00001);
			mediump float vdoth = max(dot(viewDir, halfVec), 0.00001);
			mediump float ldoth = max(dot(global_SunDirection, halfVec), 0.00001);
			
			mediump vec3 dLight = global_SunColor.rgb * DirectLight( albedo, ndotl, ndoth, ndotv, vdoth, ldoth );
			mediump vec3 iLight = global_reflStrength * IndirectLight( albedo, normal, reflectDir, ndotv );
			
			
			#if defined(SHADOWS) //&& !defined(GLITCH_EDITOR)
				mediump float lit = 1.0-GetShadowCoefficient(); 
				dLight *= lit;
			#endif

			finalColor.rgb = dLight+iLight;
			
		#else //DEFERRED
		
			finalColor.rgb = albedo;	
			finalColor.a = alpha;
			
			gl_FragData[0] = finalColor;
			gl_FragData[1] = vec4(vWPosition, 1.0);
			gl_FragData[2] = vec4(vWNormal, 1.0);
			//gl_FragData[3] = vec4(SpecStrength, ReflectStrength, Roughness.x, Roughness.y); 
			
			return;
		#endif
		
		//Glitch Effect Edditor Lighting Override 
		#ifdef EFFECT_EDITOR
			mediump vec3 fakeLight =  normalize(vec3(1.0,1.0,1.0));	
			mediump float faceLightCalc = max(dot(normal, fakeLight),0.02);
			mediump float fresnelCalc = clamp(pow(max(dot(normal, viewDir),0.00001)*-1.0 + 1.0, 5.0), 0.0, 1.0);
			finalColor.rgb = (((finalColor.rgb * 0.0001) + albedo ) * (faceLightCalc*3.0)) + (vec3(0.2, 0.5, 0.6) * fresnelCalc);	
			
		#endif
		
		#ifdef SETUNBUILT
			//Red = Fresnel
			//Green = Lighting
			//
			
			mediump vec4 CompColor = vec4(
				dot(normalize(vWNormal), normalize(vWView)),
				finalColor.r,
				max(dot(normalize(vWNormal), vec3(0.0, 1.0, 0.0)), 0.0),
				1.0
			);
			finalColor = CompColor;
		#else
			
			finalColor.a = alpha;	
			finalColor.rgb = ACESTonemapping( finalColor.rgb );
		#endif
	
		//Max Lighting Override 
		#ifdef DCC_MAX
		

			#ifdef ANIM_FACE
				finalColor.rgb = (finalColor.rgb * 0.0001) + Linear_To_sRGB(finalColorOveride) ;
			#else
				finalColor.rgb = (finalColor.rgb * 0.0001) + Linear_To_sRGB(albedo);	
			#endif
		#endif
		
		gl_FragColor = clamp(finalColor, vec4(0.0), vec4(0.9999)); // 0.9999 instead of 1.0 because of a silly 3DS Max shader compiler error
    #endif
} // main end
