#extension GL_EXT_shadow_samplers : enable

#ifdef DCC_MAX
	#define DCC(x) x
#else
	#define DCC(x)
#endif

//#define ALBEDO_MAP
//#define PANORAMA
//#define SHADOWS
//#define BUILD_MODE
//#define ANIM_FACE
//#define SEL_FACE

#define PI 3.14159265359
#define _F0 0.0082644628099174 // equal to an IOR of 1.2
#define SPEC_EXP 76.125 // Roughness 0.4
#define SPEC_MUL 12.433979929054323106944044013478 //Roughness 0.4

#ifdef ALBEDO_MAP
	uniform lowp sampler2D AlbedoMap;

	#ifdef SEL_FACE
		uniform lowp float SimpleFaceIndex;
	#endif		
#endif

uniform mediump float SpecStrength;
uniform mediump vec2  Roughness;

varying	mediump vec4 vCoord01      DCC(:TEXCOORD0);
varying mediump vec4 vColor	       DCC(:TEXCOORD1);
varying mediump vec3 vWNormal 	   DCC(:TEXCOORD2);
varying mediump vec3 vWView  	   DCC(:TEXCOORD3);
varying mediump vec3 vIrradiance   DCC(:TEXCOORD4);


#if defined(SHADOWS) //&& !defined (GLITCH_EDITOR)
	varying highp vec4 shadowPos		DCC(:TEXCOORD7);
	
	#ifdef BIAS
		uniform lowp float ShadowBias;
	#endif
	
	#ifdef GL_EXT_shadow_samplers 
		uniform highp sampler2DShadow ShadowMapSampler; 
		//configured to do shadowPos <= shadowMap.  note shadow2DEXT internally clamps. 
		lowp float percentageLit() 
		{  
			#ifdef BIAS
				lowp float bias = ShadowBias;
			#else
				lowp float bias = 0.0005;
			#endif

			return shadow2DEXT(ShadowMapSampler, vec3(shadowPos.xy, shadowPos.z - bias));
		}  
	#else 
		uniform highp sampler2D ShadowMapSampler; 
		lowp float percentageLit() 
		{ 
			highp vec3 projCoords = shadowPos.xyz / shadowPos.w;
			projCoords = projCoords * 0.5 + 0.5; 
	
			highp float sceneDepth = texture2D(ShadowMapSampler, projCoords.xy).r; 
			highp float fragDepth = min(projCoords.z, 1.0); 
			//return float( fragDepth < sceneDepth ); //standard says float(true) = 1.0 

			#ifdef BIAS
				lowp float bias = ShadowBias;
			#else
				lowp float bias = 0.001;
			#endif
		
			
			#ifdef OS_ANDROID
			if ( sceneDepth < fragDepth - bias)
			{
				return 0.0;
			}
			else
			{
				return 1.0;
			}
			#else
				return ( sceneDepth < fragDepth - bias? 0.0 : 1.0);
			#endif
		} 
	#endif
#endif

mediump vec3 sRGB_To_Linear(lowp vec4 sRGB)
{
	// cubic is a faster approximation of the pow function
	return sRGB.rgb * (sRGB.rgb * (sRGB.rgb * 0.305306011 + 0.682171111) + 0.012522878);
}


mediump vec3 DirectLight( mediump vec3 albedo, mediump float NoL, mediump float RoL, mediump float NoV )
{
	mediump float spec = SpecStrength * Roughness.x * pow(2.0, Roughness.y * RoL - Roughness.y );
	
//	mediump float Roughness = 0.4;
//	mediump float alpha = Roughness*Roughness;
//	mediump float a2 = alpha*alpha;
//	mediump float rcp_a2 = 1.0/a2;
//	mediump float c = 0.72134752 * rcp_a2 + 0.39674113;
//	mediump float spec = rcp_a2 * exp2(c * RoL - c); 
	
	mediump vec3 diffuse = max( (1.0-spec), 0.001) * NoL * albedo; 
	
	mediump vec3 retVal = diffuse+spec;
	
//	retVal *= 0.0001;
	
	return retVal;
}

mediump vec3 IndirectLight( mediump vec3 albedo, mediump vec3 normal, mediump vec3 reflectDir, mediump float NoV )
{
	mediump vec4 ambientRGBM = textureCube( IrradianceCube, normal );
	mediump vec3 ambientColor = 8.0 * ambientRGBM.a * ambientRGBM.rgb;
	mediump vec4 envRGBM = textureCube( ReflectionCube, reflectDir );
	mediump vec3 envColor = 8.0 * envRGBM.a * envRGBM.rgb;
	
	mediump float refl = ReflectStrength * (min( 0.49, pow(2.0, -9.28 * NoV) ) * 0.7 + 0.015); 
	
	mediump vec3 ambient = max( (1.0-refl), 0.001 ) * albedo * ambientColor;
	mediump vec3 env = refl*envColor;
	
	mediump vec3 retVal = ambient+env;
	
	return retVal;
}

void main()
{
	mediump vec4 finalColor = vec4(0.0, 0.0, 0.0, 1.0);
	
	mediump vec3 normal  = normalize(vWNormal);
	mediump vec3 viewDir = normalize(vWView);
	mediump vec3 reflectDir = normalize( reflect(-viewDir, normal) );
	
	mediump float ndotv = max(dot(normal, viewDir), 0.00001);
	
	finalColor.rgb = vIrradiance;
	finalColor.a = 1.0;

	finalColor.rgb = pow( finalColor.rgb, vec3(1.0/2.2) );	
    gl_FragColor = min(finalColor, vec4(1.0));
    
} // main end
