#version 300 es


// attributes
in highp vec4 Vertex;

out highp vec3 vDir;

uniform highp mat4 WorldViewProjectionMatrix;

uniform highp vec3 dir0;
uniform highp vec3 dir1;
uniform highp vec3 dir2;
uniform highp vec3 dir3;

void main() 
{
    // Passing in the cube directions via shader parameter 
    // and then indexing them based on the vertex ID 
    // allows us to skip a bunch of annoying vertex setup 
    // for the quads used to render the filtered map.
    highp vec3 dirs[4];
    dirs[0] = dir0;
    dirs[1] = dir1;
    dirs[2] = dir2;
    dirs[3] = dir3;
    
    vDir = dirs[ gl_VertexID ];
    
	gl_Position = WorldViewProjectionMatrix * Vertex;	
}
