#version 300 es

#define PI 3.14159265359

// varyings
in highp vec3 vDir;
out highp vec4 fragmentColor;

// uniforms
#ifdef PANORAMA 
    uniform highp sampler2D envMapSrc;
#else 
    uniform highp samplerCube envMapSrc; 
#endif

uniform highp float roughness;
uniform highp float texRes;
uniform highp float mipLevel;

highp vec2 DirectionToPanoramaUV( highp vec3 dir )
{
    highp vec2 uv;
    highp float r = length(dir.xz);
    highp float theta = (dir.x < 0.0) ? asin(dir.z/r) : (-asin(dir.z/r) + PI);
    uv.x = theta / (2.0*PI);
    uv.y = 0.5 * (-dir.y + 1.0);
    return uv;
}

// The final result is converted to sRGB and RGBM encoded.
highp vec4 EncodeToRGBM( highp vec3 col )
{
    col = pow(col, vec3(1.0/2.2));
        
    highp vec4 rgbm;
    col *= 1.0 / 6.0;
    rgbm.a = clamp( max( max( col.r, col.g ), max( col.b, 1e-6 ) ), 0.0, 1.0 );
    rgbm.a = ceil( rgbm.a * 255.0 ) / 255.0;
    rgbm.rgb = col / rgbm.a;
    return rgbm;
}
    
highp vec3 SampleEnvMap(highp vec3 dir, highp float mip)
{
    highp vec4 env;
#ifdef PANORAMA
    highp vec2 uv = DirectionToPanoramaUV(dir);
    env = textureLod(envMapSrc, uv, mip);
#else
    env = textureLod(envMapSrc, dir, mip);
#endif
    
#ifdef SRGB
    env.rgb = pow(env.rgb, vec3(2.2));
#endif
    
    return env.rgb;
}

// This uses UE4's split-sum approximation method for reflection map filtering. 
// see: http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
#ifdef FILTER
    highp float GGX_Trowbridge_Reitz(highp float ndoth, highp float rough2)
    {
        highp float ndf = rough2 / (ndoth*ndoth*(rough2 - 1.0) + 1.0);
        ndf *= ndf;
        ndf *= 1.0 / (rough2*rough2*PI);
        return ndf;
    }

    highp vec3 ImportanceSampleGGX(highp vec2 Xi, highp float rough2, highp vec3 N)
    {
        highp float a = rough2;
        highp float Phi = 2.0 * PI * Xi.x;
        highp float CosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
        highp float SinTheta = sqrt(1.0 - CosTheta * CosTheta);
        highp vec3 H;
        H.x = SinTheta * cos(Phi);
        H.y = SinTheta * sin(Phi);
        H.z = CosTheta;
        highp vec3 UpVector = abs(N.z) < 0.999 ? vec3(0, 0, 1) : vec3(1, 0, 0);
        highp vec3 TangentX = normalize(cross(UpVector, N));
        highp vec3 TangentY = cross(N, TangentX);
        // Tangent to world space
        return TangentX * H.x + TangentY * H.y + N * H.z;
    }

    highp float radicalInverse_VdC(highp uint bits)
    {
        bits = (bits << 16u) | (bits >> 16u);
        bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
        bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
        bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
        bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
        return float(bits) * 2.3283064365386963e-10; // / 0x100000000
    }

    highp vec2 Hammersley(highp uint i, highp uint N)
    {
        return vec2(float(i) / float(N), radicalInverse_VdC(i));
    }
    
    highp vec3 PrefilterEnvMap(highp vec3 R)
    {
        highp vec3 N = R;
        highp vec3 V = R;
        highp vec3 PrefilteredColor = vec3(0.0);
        highp uint NumSamples = 2048u; // yeah, it's a lot. 
        highp float TotalWeight = 0.0;
        highp float alpha = pow(roughness, 2.0);
        highp float sampleCount = 2048.0;
        
        for (highp uint i = 0u; i < NumSamples; i++)
        {
            highp vec2 Xi = Hammersley(i, NumSamples);
            highp vec3 H = ImportanceSampleGGX(Xi, alpha, N);
            highp vec3 L = normalize( 2.0 * dot(V, H) * H - V );
            highp float NoL = clamp(dot(N, L), 0.0, 1.0);
            highp float NoH = clamp(dot(N, H), 0.0, 1.0);
            highp float HoV = clamp(dot(H, V), 0.0, 1.0);
                        
            if (NoL > 0.0)
            {
                highp float D = GGX_Trowbridge_Reitz(NoH, alpha);
                highp float pdf = (D * NoH / (4.0 * HoV)) + 0.0001;
                highp float res = texRes;
                highp float saTexel = 4.0 * PI / (6.0 * res * res);
                highp float saSample = 1.0 / (sampleCount * pdf + 0.0001);
                highp float mip = min( log2(saSample / saTexel), 4.0);
                
                PrefilteredColor += SampleEnvMap(L, mip) * NoL; // Using mip-naps prevents undersampling errors.
                TotalWeight += NoL;
            }
        }
        return PrefilteredColor / TotalWeight;
    }
#endif

void main(void)
{
    highp vec3 D = normalize(vDir);

#ifdef FILTER    
    highp vec3 col = PrefilterEnvMap(D);
#else
    highp vec3 col = SampleEnvMap(D, mipLevel);
#endif
    
#ifdef ENCODE_RGBM 
    fragmentColor = EncodeToRGBM( col );
#else
    fragmentColor = vec4( col, 1.0 );
#endif
}


