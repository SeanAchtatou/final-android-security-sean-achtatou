// varyings
varying mediump vec4 vTexCoord;

// attributes
attribute highp vec4 position;
attribute mediump vec2 TexCoord0;

// uniforms
uniform highp mat4 worldviewprojection;

void main(void)
{
    vTexCoord.xy = TexCoord0;
	gl_Position = worldviewprojection * position;
}

