#version 300 es

//#if defined(MESHWRAP) && !defined(DISABLE_DOF)
//    #extension GL_EXT_frag_depth : enable
//#endif

#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

#ifdef SHADOWS
#define SHADOW_MAP_CASCADE_COUNT 2
varying highp vec3 vSMCoord0;
//varying highp vec3 vSMCoord1;
//varying highp float vViewZ;
#endif

#ifdef SHADOWS
#ifdef GLSL2METAL
GLITCH_UNIFORM_PROPERTIES(global_ShadowMapSampler, (samplerCompareFunc = greater, samplerFilter = linear))
#endif
uniform highp sampler2DShadow global_ShadowMapSampler;
uniform highp vec2 global_ShadowMapScale;

//uniform highp vec4 global_SMSplits;
//uniform highp float global_SMIntersticeInvSize;

mediump float SampleShadowMap(mediump vec2 baseUV, mediump vec2 offset, highp float depth)
{
	// TODO: investigate shadow2DProj as it might be able to do hardware accelerated PCF
	return float(shadow2D(global_ShadowMapSampler, vec3(baseUV + offset * global_ShadowMapScale, depth)));
}

mediump float SampleShadowMapPCF3x3(highp vec2 smCoord, highp float depth)
{
	highp vec2 uv = smCoord.xy / global_ShadowMapScale; 

	mediump vec2 baseUV;
    baseUV.x = floor(uv.x + 0.5);
    baseUV.y = floor(uv.y + 0.5);

    mediump float s = (uv.x + 0.5 - baseUV.x);
    mediump float t = (uv.y + 0.5 - baseUV.y);
	
 	baseUV -= vec2(0.5, 0.5);
    baseUV *= global_ShadowMapScale;
	
	mediump float sum = 0.0;

	mediump float uw0 = (3. - 2. * s);
    mediump float uw1 = (1. + 2. * s);

    mediump float u0 = (2. - s) / uw0 - 1.;
    mediump float u1 = s / uw1 + 1.;

    mediump float vw0 = (3. - 2. * t);
    mediump float vw1 = (1. + 2. * t);

    mediump float v0 = (2. - t) / vw0 - 1.;
    mediump float v1 = t / vw1 + 1.;

    sum += uw0 * vw0 * SampleShadowMap(baseUV, vec2(u0, v0), depth);
    sum += uw1 * vw0 * SampleShadowMap(baseUV, vec2(u1, v0), depth);
    sum += uw0 * vw1 * SampleShadowMap(baseUV, vec2(u0, v1), depth);
    sum += uw1 * vw1 * SampleShadowMap(baseUV, vec2(u1, v1), depth);

	return sum * 1.0 / 16.0;
}

mediump float GetShadowCoefficient()
{
    return SampleShadowMapPCF3x3(vSMCoord0.xy, clamp(vSMCoord0.z, 0.0, 1.0));
}
#endif

#if (!defined(ALLOW_SHADOW_BLOBS) || defined(SHADOWS)) && defined(SHADOW_BLOB)
	#undef SHADOW_BLOB
#endif


#ifdef SHADOW_BLOB
	#define MAX_SHADOW_BLOB_CASTERS 16
	#define MIN_SHADOWING 0.2
	uniform mediump vec2 global_shadowBlobPos[MAX_SHADOW_BLOB_CASTERS];
#endif


#ifdef SHADOW_BLOB

mediump float CalculateShadowBlob(mediump vec3 worldPos)
{
	mediump vec2 worldPos2d = vec2(worldPos.x, worldPos.z);
	
	//duy.daoba calculate closest caster only
	mediump float closestDist = length(global_shadowBlobPos[0] - worldPos2d);
	for (int i = 1; i < MAX_SHADOW_BLOB_CASTERS; i++)
	{
		closestDist = min(length(global_shadowBlobPos[i] - worldPos2d), closestDist);
	}
	
	return max(min(1.0, closestDist * closestDist * 36.0), MIN_SHADOWING);	
}

#endif


#ifdef DISABLE_LIGHTMAP
	#undef LIGHTMAP
#endif

// Extensions

#if defined(DISABLE_REFLECTIONS) && defined(REFLECTIONS)
	#undef REFLECTIONS
#endif

#if !defined(GL_OES_standard_derivatives) || defined(LOW_END)
    #ifdef CURVSMOOTH
        #undef CURVSMOOTH
    #endif
#endif

#ifdef DCC_MAX
    #define DCC(x) x
#else
    #define DCC(x)
#endif

#define PI 3.14159265359
#define ONE_OVER_PI 0.3183098862



// uniforms 
uniform highp mat4 omniLightMatrices[1];

#ifdef ALTERNATIVE_LIGHT_SOURCE
	uniform mediump vec3 SunDirection;
	uniform mediump vec4 SunColor;
	uniform mediump vec3 ShadowColor;

	#define global_SunDirection		SunDirection
	#define global_SunColor			SunColor
	#define global_ShadowColor	    ShadowColor
#else
	uniform mediump vec3 global_SunDirection;
	uniform mediump vec4 global_SunColor;
	uniform mediump vec3 global_ShadowColor;
#endif

uniform highp vec4 eyepos;
uniform highp vec4 eyeposos;

// varyings
varying mediump vec4 vTexCoord;
varying mediump vec3 vWNormal;
varying mediump vec3 vWTangent;
varying mediump vec3 vWBinormal;
varying mediump vec3 vWLightDir;
varying mediump vec3 vWorldPos;
varying mediump vec4 vColor;

//#ifdef GL_EXT_frag_depth
//    varying mediump float vDepth;
//#endif



#ifdef PIPNORMAL	
    varying mediump float PipMask;
#endif

#ifdef TRIPLANAR
    varying mediump vec2 vTriPlanUV_X;
    varying mediump vec2 vTriPlanUV_Y;
    varying mediump vec2 vTriPlanUV_Z;
#endif

// Gamma 2.2 to 1.0
mediump vec3 sRGB_To_Linear(mediump vec3 sRGB)
{
	// cubic is a faster approximation of the pow function
	return sRGB.rgb * (sRGB.rgb * (sRGB.rgb * 0.305306011 + 0.682171111) + 0.012522878);
}

// Gamma 1.0 to 2.2
mediump vec3 Linear_To_sRGB(mediump vec3 lin)
{
    return pow( lin, vec3(0.45) );
}

// Calculates the L1 spherical harmonics value for a specified directional vector.
// cC corresponds to the L0 coefficients.
// cAr, cAg, cAb are the L1 coefficients organized by color channel.
mediump vec3 EvaluateL1SH(mediump vec3 dir, mediump vec3 cAr, mediump vec3 cAg, mediump vec3 cAb, mediump vec3 cC)
{
    mediump float x1 = dot(dir.yzx, cAr);
    mediump float x2 = dot(dir.yzx, cAg);
    mediump float x3 = dot(dir.yzx, cAb);
    return ((0.886227*cC) + (1.023328*vec3(x1, x2, x3)));
}

// Returns the luma (brightness) of a color.
mediump float Luma(mediump vec3 color)
{
    return dot(color, vec3(0.2126, 0.7152, 0.0722));
}


#if defined(ANIMFX) || defined(MESHWRAPEFFECT)
    uniform highp float Speed;
#endif


uniform lowp sampler2D AlbedoMap;
uniform lowp sampler2D ARMMap;
uniform lowp sampler2D NormalMap;

//uniform lowp float GrazingReduction_A;
//uniform lowp float GrazingReduction_B;

#ifdef MESHWRAPEFFECT
    uniform highp sampler2D PanTexture;
    uniform lowp float PanTilt;
    uniform lowp vec4 PanColor;
#endif

#ifdef PIPNORMAL	
    uniform lowp sampler2D global_PipNormalMap;
#endif

#ifdef SLICE
    uniform lowp sampler2D AlphaSliceMap;
#endif

#ifdef SHADOWS
    uniform lowp float ShadowBias;
    uniform mediump float ShadowStrength;
#endif

#ifdef LIGHTMAP
    uniform lowp sampler2D LightMap;
#endif

#ifdef ALPHA
    uniform mediump float AlphaVal;
#endif

#ifdef UNIFORM_ROUGH
    uniform mediump float RoughVal;
#endif

//#ifdef CUTOUT
//    uniform mediump float AlphaCutoff;
//#endif

#ifdef SEL_FACE
    uniform lowp float SimpleFaceIndex;
#endif	

#ifdef ANIM_FACE
    uniform lowp sampler2D FaceAtlas;
    uniform lowp sampler2D FaceAtlasAlpha;		
    uniform lowp vec3 HairAlbedo;
    uniform lowp vec3 EyeBrowSelection;	
    uniform	highp vec4 EyeXatt;	
    uniform	highp vec4 EyeYatt;		
    uniform	highp vec4 MouthAtt;		
    uniform	highp vec4 BrowAtt;	

    uniform	highp vec4 ScaleAtt;
    //If we need to reduce uniforms for some devices ScaleAtt can be replaced with hard coded values
        //uniform	highp vec4 ScaleAtt;
        //This is commented out to reduce the amount of uniforms that are in the shader for some devices
        //This will limit how many assets are on a face altlas to 4 rows as apposed to the posible 16 this technique supports.
#endif

uniform mediump sampler2D global_brdfTable;

#ifdef ALTERNATIVE_LIGHT_SOURCE
	#define REFLECTION_MAP				ReflMap
	#define REFLECTION_MAP_NUM_MIPMAPS	NumMipMaps
	#define REFLECTION_MAP_STRENGTH		ReflStrength
	#define REFLECTION_MAP_BOXMIN		ReflBoxMin
	#define REFLECTION_MAP_BOXMAX		ReflBoxMax
	#define REFLECTION_MAP_PROBE_POS	ReflProbePos
	#define REFLECTION_MAP_C_C			CC
	#define REFLECTION_MAP_CA_R			CAr
	#define REFLECTION_MAP_CA_G			CAg
	#define REFLECTION_MAP_CA_B			CAb
	
	#define TONEMAP_ACES_SLOPE			ACES_slope
	#define TONEMAP_ACES_TOE			ACES_toe
	#define TONEMAP_ACES_SHOULDER		ACES_shoulder
	#define TONEMAP_ACES_BLACKCLIP		ACES_blackClip
	#define TONEMAP_ACES_WHITECLIP		ACES_whiteClip
    #define TONEMAP_ACES_AMBMULT        ACES_ambMult
#else
	#ifdef MINIFIG
		#define REFLECTION_MAP					global_reflMap
		#define REFLECTION_MAP_MINIFIG			global_reflMap_aux
		#define REFLECTION_MAP_NUM_MIPMAPS		global_numMipMaps_aux

        #define TONEMAP_ACES_SLOPE			    global_ACES_slope_Minifig
	    #define TONEMAP_ACES_TOE			    global_ACES_toe_Minifig
	    #define TONEMAP_ACES_SHOULDER		    global_ACES_shoulder_Minifig
	    #define TONEMAP_ACES_BLACKCLIP		    global_ACES_blackClip_Minifig
	    #define TONEMAP_ACES_WHITECLIP		    global_ACES_whiteClip_Minifig
        #define TONEMAP_ACES_AMBMULT            global_ACES_ambMult_Minifig
        #define TONEMAP_MINIFIG_GRAD            global_Grad_Minifig
	#else		
		#define REFLECTION_MAP					global_reflMap
		#define REFLECTION_MAP_NUM_MIPMAPS		global_numMipMaps  
        
        #define TONEMAP_ACES_SLOPE			    global_ACES_slope
	    #define TONEMAP_ACES_TOE			    global_ACES_toe
	    #define TONEMAP_ACES_SHOULDER		    global_ACES_shoulder
	    #define TONEMAP_ACES_BLACKCLIP		    global_ACES_blackClip
	    #define TONEMAP_ACES_WHITECLIP		    global_ACES_whiteClip
        #define TONEMAP_ACES_AMBMULT            global_ACES_ambMult
	#endif
	
	#define REFLECTION_MAP_STRENGTH		global_reflStrength
	#define REFLECTION_MAP_BOXMIN		global_reflBoxMin
	#define REFLECTION_MAP_BOXMAX		global_reflBoxMax
	#define REFLECTION_MAP_PROBE_POS	global_reflProbePos
	#define REFLECTION_MAP_C_C			global_cC
	#define REFLECTION_MAP_CA_R			global_cAr
	#define REFLECTION_MAP_CA_G			global_cAg
	#define REFLECTION_MAP_CA_B			global_cAb
#endif

uniform mediump samplerCube REFLECTION_MAP;

#ifdef MINIFIG
    uniform mediump samplerCube REFLECTION_MAP_MINIFIG;
#endif

uniform mediump float REFLECTION_MAP_NUM_MIPMAPS;

uniform mediump float REFLECTION_MAP_STRENGTH;
uniform mediump vec3 REFLECTION_MAP_BOXMIN;
uniform mediump vec3 REFLECTION_MAP_BOXMAX;
uniform mediump vec3 REFLECTION_MAP_PROBE_POS;
uniform mediump vec3 REFLECTION_MAP_C_C;
uniform mediump vec3 REFLECTION_MAP_CA_R;
uniform mediump vec3 REFLECTION_MAP_CA_G;
uniform mediump vec3 REFLECTION_MAP_CA_B;

uniform mediump float TONEMAP_ACES_SLOPE;
uniform mediump float TONEMAP_ACES_TOE;
uniform mediump float TONEMAP_ACES_SHOULDER;
uniform mediump float TONEMAP_ACES_BLACKCLIP;
uniform mediump float TONEMAP_ACES_WHITECLIP;
uniform mediump vec3 TONEMAP_ACES_AMBMULT;

#ifdef MINIFIG
    uniform mediump vec3 TONEMAP_MINIFIG_GRAD;
#endif

// Specular functions used for physically-based shading. 
// See http://graphicrants.blogspot.ca/2013/08/specular-brdf-reference.html
// for explanation and the formulas.

// Trowbridge-Reitz (GGX) normal distribution function.
highp float NDF_GGX( mediump float NoH, mediump float r )
{
    highp float a2 = r*r; 
    highp float ndf = a2 / max((NoH*NoH*(a2-1.0) + 1.0), 0.0001);
    ndf *= ndf;
    ndf *= 1.0 / max((a2*a2*PI), 0.0001);
    return ndf;
}

// UE4's optimized normal distribution function used for mobile. 
// https://www.unrealengine.com/en-US/blog/physically-based-shading-on-mobile
mediump float NDF_Gaussian( mediump float NoL, mediump float RoL, mediump float roughness )
{
    mediump float a2 = roughness;
    mediump float rcp_a2 = 1.0 / a2;
    mediump float c = 0.72134752 * rcp_a2 + 0.39674113;
    
    mediump float DF = rcp_a2 * pow(2.0, c * RoL - c );
    return DF;
}

// Standard Schlick Fresnel. 
mediump vec3 Fresnel_Schlick(mediump float VoH, mediump vec3 F0)
{
    return F0 + (vec3(1.0) - F0)*pow(1.0-VoH, 5.0);
}

// GGX Geometric Shadowing
mediump float GGX_GeometryTerm( mediump float VoN, mediump float VoH, mediump float roughness )
{
 
    mediump float a = pow (roughness, 4.0);
    return (2.0 * VoN) / (VoN + sqrt(a + (1.0 - a) * pow (VoN, 2.0)   ));
   
    //mediump float alpha = roughness;
    //mediump float VoH2 = VoH*VoH;
    //mediump float tan2 = (1.0 - VoH2) / VoH2;
    //mediump float chi = ( (VoH2/VoN) > 0.0 ) ? 1.0 : 0.0;
    //return (chi * 2.0) / (1.0 + sqrt( 1.0 + alpha * alpha * tan2 ));
    

}



// Contrast-preserving Reinhard tonemapping operator. 
// https://imdoingitwrong.wordpress.com/2010/08/19/why-reinhard-desaturates-my-blacks-3/
// Also does Linear->sRGB conversion.
const mediump float WhiteLevel = 2.0;
mediump vec3 WhitePreservingReinhard(mediump vec3 color)
{
    mediump float luma = dot(color, vec3(0.2126, 0.7152, 0.0722));
    mediump float toneMappedLuma = luma * (1. + luma / (WhiteLevel*WhiteLevel)) / (1. + luma);
    color *= toneMappedLuma / luma;
    color = pow(color, vec3(1.0/2.2));
    return color;
}

// Controllable filmic tone mapping operator. 
// (Actually might not be ACES? Like I think ACES uses the same
// formula but has specific parameters. I don't know). 
// Also does Linear->sRGB conversion.
highp vec3 ACESTonemapping( highp vec3 x )
{
    mediump float a = TONEMAP_ACES_SLOPE;
    mediump float b = TONEMAP_ACES_TOE;
    mediump float c = TONEMAP_ACES_SHOULDER;
    mediump float d = TONEMAP_ACES_BLACKCLIP;
    mediump float e = TONEMAP_ACES_WHITECLIP;
    
    highp vec3 aces_val = (x*(a*x+b))/(x*(c*x+d)+e);
    
    return clamp(
        Linear_To_sRGB( aces_val ),
        vec3(0), vec3(1)
    );
}



// Unpacks an RGBM-encoded value (max range is [0.0 - 6.0]).
// Also converts from sRGB->Linear. RGBM values should always
// be in RGBM.
// http://graphicrants.blogspot.ca/2009/04/rgbm-color-encoding.html
// And for some info on how it could be improved:
// http://www.ludicon.com/castano/blog/2016/09/lightmap-compression-in-the-witness/
mediump vec3 DecodeRGBM( mediump vec4 rgbm )
{
    mediump vec3 col = 6.0 * rgbm.a * rgbm.rgb;
    return sRGB_To_Linear(col);
}

// Converts texture-packed normal to a normalized vector.
mediump vec3 UnpackNormal( mediump vec4 n )
{
    return normalize( 2.0 * (n.xyz - 0.5) );
}

// Returns the intersection point of a ray and an axis-aligned box. 
mediump vec3 BoxIntersect( mediump vec3 rayStart, mediump vec3 rayDir, mediump vec3 boxMin, mediump vec3 boxMax )
{
    mediump vec3 invRay = 1.0/rayDir;
    
    mediump vec3 t0 = invRay*(boxMin-rayStart);
    mediump vec3 t1 = invRay*(boxMax-rayStart);
    
    mediump vec3 maxVal = max(t0, t1);
    mediump float tMax = min(maxVal.x, min(maxVal.y, maxVal.z));
    
    return rayStart + tMax*rayDir; // intersect pos
}

// Given a world position and a world-space reflection direction, returns a 
// parallax-corrected UV for the cubemap sample.
// https://seblagarde.wordpress.com/2012/11/28/siggraph-2012-talk/
mediump vec3 ReflectDirToCubeUV( mediump vec3 rayStart, mediump vec3 rayDir )
{
    #ifdef MINIFIG
        mediump vec3 intersectPos = BoxIntersect(rayStart, rayDir, vec3(-10.0, -10.0, -10.0), vec3(10.0, 10.0, 10.0) );
    #else
        mediump vec3 intersectPos = BoxIntersect(rayStart, rayDir, REFLECTION_MAP_BOXMIN, REFLECTION_MAP_BOXMAX );
    #endif
    return normalize( intersectPos - REFLECTION_MAP_PROBE_POS );
}

// Tri-planar mapping for objects that don't have UVs. Probably shouldn't use
// this in the final game, as it's extremely wasteful. Make the artists do their
// UV unwraps!
// https://medium.com/@bgolus/normal-mapping-for-a-triplanar-shader-10bf39dca05a
#ifdef TRIPLANAR
    lowp vec4 TriplanarSample(lowp sampler2D tex, mediump vec3 blend)
    {
        lowp vec4 retVal = blend.x * texture2D(tex, vTriPlanUV_X);
        retVal += blend.y * texture2D(ARMMap, vTriPlanUV_Y);
        retVal += blend.z * texture2D(ARMMap, vTriPlanUV_Z);
        return retVal;
    }
    
    // Uses UDN blending.
    lowp vec3 TriplanarSampleNormal(lowp sampler2D normalMap, mediump vec3 vertNormal, mediump vec3 blend)
    {
        mediump vec3 n_x = UnpackNormal(texture2D(normalMap, vTriPlanUV_X));
        mediump vec3 n_y = UnpackNormal(texture2D(normalMap, vTriPlanUV_Y));
        mediump vec3 n_z = UnpackNormal(texture2D(normalMap, vTriPlanUV_Z));
              
        n_x = vec3( n_x.xy + vertNormal.zy, vertNormal.x );
        n_y = vec3( n_y.xy + vertNormal.xz, vertNormal.y );
        n_z = vec3( n_z.xy + vertNormal.xy, vertNormal.z );
        
        return normalize(
            n_x.zyx * blend.x 
         +  n_y.xzy * blend.y 
         +  n_z.xyz * blend.z
        );
    }
#endif

#ifdef ANIM_FACE
    mediump vec4 FacialAnimation( mediump vec3 albedo, mediump float alpha, mediump vec2 Target_AlbedoCoords )
    {
        // mediump vec4 ScaleAtt = vec4(0.92, 0.92, 0.28, 0.175);
        // This is a hard coded variable to reduce the amount of uniforms for some devices
        // This will limit how many assets are on a face altlas to 4 rows as apposed to the posible 16 this technique supports.

        highp vec2 DCoord = ((Target_AlbedoCoords.xy) + vec2(0.0,0.165))  * vec2(ScaleAtt.x, ScaleAtt.y); // Scaled and shifted coordinates for the Face Calculations
        highp float EyeMask = clamp(floor(Target_AlbedoCoords.x * 2.0), 0.0, 1.0); // This is a vertical mask to seperate the eyes
			
        highp vec2 FaceAtlasCoord = //These are the calculations to generate the Face Atlas UV Coordinates
            mix(// This MIX Combine EyesComb and Mouth
				
                    // Below is the Eyes UV calculations
                        vec2 ( // Below Composes the Eyes in the X and Y access
                                max( // Below combines the eyes in the X access
                                        (clamp((DCoord.x * -1.0)+EyeXatt.y-EyeXatt.z, 0.00, EyeXatt.x)), // Left Eye Calculations
                                        (clamp(DCoord.x-EyeXatt.y-(EyeXatt.z*-1.0), 0.00, EyeXatt.x)) // Right Eye Calculations
                                    ) + EyeXatt.a // This is to center the eyes
                                , // Below chooses the index of the eyes in the Y access
                                    clamp(DCoord.y - EyeYatt.x, 0.00, EyeYatt.y) + mix(EyeYatt.z, EyeYatt.a, EyeMask) // Chooses the Index
                        )//^ End of Vec2 //End of Eye Calculations
				
				
                    , // Below is the Mouth UV calculations
                        vec2 ( // Below Composes the Mouth in the X and Y access
                                clamp(DCoord.x + MouthAtt.x, MouthAtt.y, MouthAtt.y + EyeXatt.x), //X access of the Mouth Calculations
                                clamp(DCoord.y-MouthAtt.z, 0.0, EyeYatt.y) + MouthAtt.a  //Y access of the Mouth Calculations
                        )//^ End of Vec2 // End of Mouth Calculations
					
					
				            ,// Below is the EyesComb and Mouth MIX mask
                   clamp(floor(Target_AlbedoCoords.y * 2.0 + 0.395), 0.0, 1.0) // This is the Mouth/Eye Mask
					
				); //^This Is the End of MIX Combine EyesComb and Mouth
		
			
			lowp vec4 EyesAndMouth = texture2D(FaceAtlas, FaceAtlasCoord);
			EyesAndMouth.a = texture2D(FaceAtlasAlpha, FaceAtlasCoord).r;		
			
			
        lowp vec3 EyebrowsAllChan = texture2D(FaceAtlas, 
            // Below Composes the Eye brows in the X and Y access
            vec2 ( //Below Composes the eyeborows in the X access
                max( 
                    clamp((DCoord.x * -1.0) + EyeXatt.y + ScaleAtt.z, EyeXatt.x, EyeXatt.x*2.0), // Left Brow Eye Calculations
                    clamp((DCoord.x) - EyeXatt.y + ScaleAtt.z, EyeXatt.x, EyeXatt.x*2.0) // Right Brow Eye Calculations
						      )//^ end of max
                , // Below is the calulations for EyeBrow the Y access
                clamp(DCoord.y-(mix(BrowAtt.z, BrowAtt.a, EyeMask)), 0.00, EyeYatt.y) + mix(BrowAtt.x, BrowAtt.y, EyeMask)// Chooses the Index
						
            )  //^ end of vec2
			
			    ).rgb  * EyeBrowSelection;  // This isolates the selected channel for use
			
        lowp float Eyebrows = EyebrowsAllChan.r + EyebrowsAllChan.g + EyebrowsAllChan.b;
			
        mediump vec3 face_albedo = mix (EyesAndMouth.rgb, HairAlbedo, Eyebrows);
        face_albedo = ONE_OVER_PI * sRGB_To_Linear(face_albedo);

        #ifdef ALBEDO_MAP_ALPHA
            return vec4 ( mix (albedo.rgb, face_albedo, max (Eyebrows, EyesAndMouth.a) * (alpha * -1.0 + 1.0) ), max ( max (Eyebrows, EyesAndMouth.a),alpha) );
        #else 
            return vec4 ( mix (albedo.rgb, face_albedo, max (Eyebrows, EyesAndMouth.a) ), max (Eyebrows, EyesAndMouth.a) );
        #endif
    }
#endif



void PhysicallyBasedShading(void)
{
    mediump vec3 vertNormal = normalize(vWNormal.xyz);
    mediump vec3 vertTangent = normalize(vWTangent.xyz);
    mediump vec3 vertBinormal = normalize(vWBinormal.xyz);
    
 
    #ifdef AUX_MAPS_COORDTWO
        mediump vec2 Target_AlbedoCoords = vTexCoord.zw;
    #else 
        mediump vec2 Target_AlbedoCoords = vTexCoord.xy;
    #endif
 
 
// *********** ALBEDO & ALPHA ***********        

    mediump vec3 albedo = vec3(0.0);
    mediump float alpha = 0.0;

#ifdef VERTCOLOR
    albedo = vColor.rgb;
    alpha = vColor.a;
    
#elif defined(SEL_FACE)    
    mediump vec4 face_tex = texture2D(AlbedoMap, (clamp( (vec2(Target_AlbedoCoords.y, (Target_AlbedoCoords.x * -1.0))*0.5) , vec2(0.0, -0.50), vec2(0.25, 0.0) ) + vec2(SimpleFaceIndex, 0.0)));
    albedo = ONE_OVER_PI * sRGB_To_Linear( face_tex.rgb );
    alpha = face_tex.a;
    
#else

    lowp vec4 albedo_tex = texture2D(AlbedoMap, Target_AlbedoCoords);
    albedo = ONE_OVER_PI * sRGB_To_Linear( albedo_tex.rgb );
    alpha = albedo_tex.a;
#endif

#ifdef ALPHA
    alpha = AlphaVal;
#endif

// Discards for materials that are opaque, but partially see-through.
// (e.g. foliage billboards)
//#ifdef CUTOUT
    //if( alpha < AlphaCutoff )
    //{
       // discard;
    //}
//#endif  


#ifdef ANIM_FACE
    mediump vec4 RawFace = FacialAnimation( albedo, alpha, Target_AlbedoCoords);
    albedo = RawFace.rgb;
#endif
    
// *********** NORMALS & SHADING PARAMETERS ***********
#ifdef TRIPLANAR
    mediump vec3 blend = abs(vertNormal);
    blend = max(blend - 0.2, 0.0);
    blend /= dot(blend, vec3(1));
    
    lowp vec4 ARM = TriplanarSample(ARMMap, blend);
    mediump vec3 normal = TriplanarSampleNormal(NormalMap, vertNormal, blend);
    
    lowp float ao = ARM.r;
    mediump float roughness = ARM.g;
    lowp float metalness = ARM.b;  
#else
    //This changes the target coordinate of the normal and ARM maps for non
    //triplanar techniques. It will be primarily used for minifig parts which
    //use texture coordinate two for the normal and ARM.

    // Uniform roughness will provide a shader parameter for 
    // adjusting roughnes and metalness for the entire object.
    // Otherwise, they're sampled from an ARM texture.
    #ifdef UNIFORM_ROUGH
        lowp float ao = 1.0;
        lowp float metalness = 0.0;
        mediump float roughness = RoughVal;
    #elif defined(LEGO_ROUGH)
        lowp float ao = 1.0;
        lowp float metalness = 0.0;
        mediump float roughness = 0.106;
    #else 
        lowp vec4 ARM = texture2D(ARMMap, vTexCoord.xy);
        lowp float ao = ARM.r;
        mediump float roughness = ARM.g;
        lowp float metalness = ARM.b;
        
        #ifdef ANIM_FACE
           roughness = mix (roughness,  min (1.0, roughness + 0.2) , RawFace.a); // min (1.0, roughness + 0.5)
           //metalness = mix (metalness, 0.0, RawFace.a);
        #endif
    #endif
    


    #ifdef PIPNORMAL
        lowp vec4 normal_pip =  texture2D(global_PipNormalMap, vTexCoord.xy);
        albedo *= mix ( normal_pip.z, 1.0, PipMask);

        #ifndef VERTNORM
            #ifdef ANIMFX
                lowp vec4 nmap = vec4(0.5, 0.5, 1.0, 0.0);
            #else 
                lowp vec4 nmap = texture2D(NormalMap, vTexCoord.xy);
            #endif
            lowp vec4 normal_tex = mix (
                normal_pip,
                nmap,
                PipMask
            );
        #endif

    #else 
        #ifndef VERTNORM
            lowp vec4 normal_tex = texture2D(NormalMap, vTexCoord.xy);
        #endif
    #endif

 
    #ifdef VERTNORM
        mediump vec3 normal = normalize( vertNormal );
    #else   
        mediump vec3 tangent_normal = UnpackNormal(normal_tex);
        
        // Tangent normal conversion
        mediump vec3 normal = normalize(
                tangent_normal.x * vertTangent
            +   tangent_normal.y * vertBinormal
            +   tangent_normal.z * vertNormal
        );
    #endif
#endif




    // *********** LIGHTING SETUP ***********
#ifdef CURVSMOOTH
    // Use the derivative of the normal to get the surface curvature,
    // then use that to limit the roughness (prevents glowing edges 
    // from specular aliasing and the environment map).
    mediump vec3 ddxN = dFdx( normal );
    mediump vec3 ddyN = dFdy( normal );
    mediump float curv2 = max( dot(ddxN, ddxN), dot(ddyN, ddyN) );  
    curv2 = clamp( curv2, 0.0, 1.0 );
    roughness = mix( roughness, 1.0, curv2 );
#endif
    
    // Vectors
    mediump vec3 lightDir = normalize(vWLightDir);
    mediump vec3 viewDir = normalize( eyepos.xyz - vWorldPos.xyz );
    mediump vec3 halfVec = normalize(lightDir + viewDir);
    mediump vec3 reflectDir = normalize( reflect(-viewDir, normal) );
    
    // Dot products
    mediump float NoL = max(dot(normal, lightDir),      0.001);
    mediump float NoH = max(dot(normal, halfVec),       0.001);
    mediump float VoH = max(dot(viewDir, halfVec),      0.001);
    mediump float NoV = max(dot(normal, viewDir),       0.001);
    mediump float LoH = max(dot(lightDir, halfVec),     0.001);
    mediump float RoL = max(dot(reflectDir, lightDir),  0.001);
    
    // Albedo and F0 (reflectance at normal incidence)
    //metalness = (metalness>0.1) ? 1.0 : metalness;
    mediump float f0 = mix(0.04, 0.9, metalness);
    mediump vec3 specColor = mix( vec3(f0), albedo, metalness );
    albedo = mix( albedo, vec3(0.0), metalness );
    
    
    
    
    
    // *********** DIRECT LIGHT ***********
#if !defined(NODIRECT)    
    mediump vec3 NDF = specColor * NDF_GGX( NoH, roughness );
    mediump vec3 F = Fresnel_Schlick(LoH, specColor);
    mediump vec3 NDF_F = NDF * F;
    mediump float G = clamp( GGX_GeometryTerm( NoV, VoH, roughness ) * GGX_GeometryTerm( NoL, LoH, roughness ), 0.0, 1.0);
    
    mediump float recip = max (0.001, 4.0 * NoL * NoV);

    #ifdef BUILDROOM
        mediump vec3 spec = (NDF_F * G);
    #else
        mediump vec3 spec = (NDF_F * G)/recip; 
    #endif
 
    // Kill albedo for direct light. Useful for surfaces where you want to prevent wash-out but still get direct specular.
    #ifdef SPECONLY    
        mediump vec3 diffuse = 0.001*albedo.rgb; 
    #else
        mediump vec3 diffuse = albedo.rgb;
    #endif

    //mediump float grazingReduction = smoothstep(0.001, 0.001, recip);
    //mediump float grazingReduction = smoothstep(GrazingReduction_A, GrazingReduction_B, recip) * NoL;

    
    #if defined(PIPNORMAL) || defined(MINIFIG)
        mediump float grazingReduction = smoothstep(0.001, 0.01, recip) * NoL;
    #else
        mediump float grazingReduction = smoothstep(-5.0, 1.0, recip) * NoL;
    #endif
    
    spec *= grazingReduction;
    
    
    mediump vec3 directLight = clamp(vec3(1.0) - spec, 0.0, 1.0) * diffuse + spec;
    directLight *= NoL;
    directLight *= global_SunColor.rgb;
#else 
    mediump vec3 directLight = vec3(0.0);    
#endif
     
     
     
     
     
    // *********** INDIRECT LIGHT *********** 
    
// Reflections use UE4's split sum approximation method.
// http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
// It's important for the global_brdfTable to be present for this to work. Also the reflection
// cubemap must be pre-filtered with their algorithm.
#ifdef REFLECTIONS           
    mediump vec2 envBrdf = texture2D( global_brdfTable, vec2(roughness, NoV) ).rg; 
    mediump vec3 cubeUV = ReflectDirToCubeUV( vWorldPos, reflectDir );
    mediump vec3 env = DecodeRGBM( textureCubeLod(  REFLECTION_MAP, cubeUV, roughness*REFLECTION_MAP_NUM_MIPMAPS) );
    #ifdef MINIFIG
        mediump vec3 env2 = DecodeRGBM( textureCubeLod(  REFLECTION_MAP_MINIFIG, cubeUV, roughness*REFLECTION_MAP_NUM_MIPMAPS) );
        env = mix (env2, env, metalness);
    #endif

    mediump vec3 indirectSpec = env * ((envBrdf.x * specColor) + envBrdf.y);
    indirectSpec = clamp( indirectSpec, vec3(0.0), specColor );
#else
    mediump vec3 indirectSpec = vec3(0.0);
#endif   

// Lightmaps for ambient (i.e. indirect lighting or bounce lighting), spherical harmonics for dynamic objects. 
#ifdef LIGHTMAP
    mediump vec3 ambient = DecodeRGBM( texture2D(LightMap, vTexCoord.zw) ) * TONEMAP_ACES_AMBMULT;
    indirectSpec *= Luma(ambient);
#else    
    mediump vec3 ambient = EvaluateL1SH(-normal, REFLECTION_MAP_CA_R, REFLECTION_MAP_CA_G, REFLECTION_MAP_CA_B, REFLECTION_MAP_C_C) * TONEMAP_ACES_AMBMULT;
#endif    
    
    mediump vec3 indirectDiffuse = albedo.rgb * ambient;  
    
    mediump vec3 indirectLight = (clamp(vec3(1.0) - indirectSpec, 0.0, 1.0) * indirectDiffuse + indirectSpec);
    indirectLight *= REFLECTION_MAP_STRENGTH;
    indirectLight *= ao; // ambient occlusion only gets applied to ambient light
    
    

       
    
    // *********** ASSEMBLY & TONE MAPPING ***********
    mediump vec3 totalLight = directLight + indirectLight;
  
#ifdef SHADOWS
    lowp float shadowing = clamp(1.0 - (ShadowStrength*GetShadowCoefficient()), 0.0, 1.0);
    //totalLight *= shadowing;
    totalLight *= max (global_ShadowColor, shadowing);
#endif  

#ifdef SHADOW_BLOB
	totalLight *= CalculateShadowBlob(vWorldPos);
#endif

#ifdef MINIFIG
    totalLight *= max (TONEMAP_MINIFIG_GRAD, clamp(vWorldPos.y * 2.6 + 0.2, 0.2, 1.0));
#endif

// Everything under the DEBUG_SHADING define is meant to hook into the technique maps used
// by the Phantom editor's debug views.


//#define DEBUG_SHADING
//#define SHOW_ROUGH

#ifdef DEBUG_SHADING
    #ifdef SHOW_ALBEDO
        totalLight = Linear_To_sRGB(albedo);
    #endif
    #ifdef SHOW_NORMALS
        totalLight = normal;
    #endif
    #ifdef SHOW_AO
        totalLight = vec3(ao);
    #endif
    #ifdef SHOW_ROUGH
        totalLight = vec3(roughness);
    #endif
    #ifdef SHOW_METAL
        totalLight = vec3(metalness);
    #endif
    #ifdef SHOW_SPEC
        #ifdef NODIRECT
            totalLight = vec3(0.0);
        #else
            mediump vec3 debug_spec = global_SunColor.rgb * NoL * spec;
            totalLight = ACESTonemapping( debug_spec );
        #endif
    #endif
    #ifdef SHOW_AMBIENT
        mediump vec3 debug_ambient = REFLECTION_MAP_STRENGTH * ambient; 
        totalLight = ACESTonemapping( debug_ambient );
    #endif
    #ifdef SHOW_REFLECT
        totalLight = ACESTonemapping( indirectSpec );
    #endif
#else 
    totalLight = ACESTonemapping( totalLight );
    totalLight = clamp(totalLight, vec3(0.0), vec3(1.0));

    #ifdef VFOG
        totalLight.rgb = (1.0 - vColor.a)*totalLight.rgb + vColor.a*vColor.rgb;
    #endif
#endif




#if defined(EFFECT_EDITOR) || defined(GLITCH_EDITOR)
    mediump vec3 fakeLight =  normalize(vec3(1.0,1.0,1.0));	
    mediump vec3 fakeLight2 =  normalize(vec3(-1.0,1.0,-1.0));	
    mediump float faceLightCalc = max( max (dot(normal, fakeLight), dot(normal, fakeLight2)),0.2);
    //mediump float fresnelCalc = clamp(pow(max(dot(normal, viewDir),0.00001)*-1.0 + 1.0, 5.0), 0.0, 1.0);
    totalLight.rgb = (Linear_To_sRGB(albedo) * (faceLightCalc*3.0));// + (vec3(0.2, 0.5, 0.6) * fresnelCalc);

#endif



#if  defined(ALPHA) || defined(ALBEDO_MAP_ALPHA)
    #ifndef ANIM_FACE
        mediump vec3 totalSpec = ao*(REFLECTION_MAP_STRENGTH*indirectSpec + NoL*global_SunColor.rgb*spec);
        // Adding the spec luma to the alpha allows for reflections to be visible on transparent objects
        // (Think of how light is reflected on glass).
        alpha = min(alpha + Luma(totalSpec), 1.0); 
        gl_FragColor = vec4( totalLight, alpha );
    #else
        gl_FragColor = vec4( totalLight, 1.0 );
    #endif
#elif defined(SLICE)

    lowp float alphaSlice = texture2D(AlphaSliceMap, vTexCoord.zw).r;
    #ifdef SLICE_REV
        alphaSlice = 1.0 - alphaSlice;
    #endif
    gl_FragColor = vec4( totalLight, alphaSlice );

#else
     gl_FragColor =vec4( totalLight, 1.0 );
#endif
}

void GhostShading()
{
    #ifdef GHOST
    /*
    mediump vec3 normal = normalize(vWNormal);
    mediump vec3 lightDir = normalize(vWLightDir);
    mediump vec3 viewDir = normalize( eyepos.xyz - vWorldPos.xyz );
    mediump vec3 halfVec = normalize(lightDir + viewDir);
       
    mediump float NoL = max(dot(normal, vec3(0.0, 1.0, 0.0)), 0.0);
    mediump float NoH = max(dot(normal, halfVec), 0.001);
    mediump float VoH = max(dot(viewDir, halfVec), 0.001);
    mediump float NoV = max(dot(normal, viewDir), 0.001);
    mediump float LoH = max(dot(lightDir, halfVec), 0.001);       
    
    mediump float roughness = 0.05;
    mediump float NDF = 0.04 * NDF_GGX( NoH, roughness );
    mediump float F = Fresnel_Schlick(LoH, vec3(0.04) ).r;
    mediump float NDF_F = NDF * F;
    mediump float G = clamp( GGX_GeometryTerm( NoV, VoH, roughness ) * GGX_GeometryTerm( NoL, LoH, roughness ), 0.0, 1.0);
    
    mediump float recip = 4.0 * NoL * NoV;
    mediump float spec = (NDF_F * G)/recip; //clamp(vec3(1.0) - spec, 0.0, 1.0) * vec3 (1.0, 0.0, 0.0) + spec;    
    //mediump vec4 CompColor = vec4( NoV, spec, NoL, 1.0);

    
    mediump vec4 CompColor = vec4(vec3(spec), 1.0); //(vec3 (1.0, 0.0, 0.0) * max (0.1, (1.0 - NoV))).rgb
    */

   
    mediump vec3 normal = normalize( vWNormal.xyz );
    mediump vec3 lightDir = normalize(vWLightDir);
    mediump vec3 viewDir = normalize( eyepos.xyz - vWorldPos.xyz );
    mediump vec3 halfVec = normalize(lightDir + viewDir);
     
    mediump float NoH = max(dot(normal, halfVec),       0.001);
    mediump float NoV = max(dot(normal, viewDir),       0.001);

    mediump float NDF = 0.05 * NDF_GGX( NoH, 0.05 ); // First Float is spec color, and last float is roughness
    //mediump float spec = max (NDF, 1.0 - NoV); // This has a specular highlight
    mediump float spec = 1.0 - NoV; // This has a specular highlight
    lowp float alpha = 1.0;



    #ifdef ANIMFX
        #if  defined(ANIMCOL) || defined(VERTCOLOR)
            mediump vec3 diffuse = Linear_To_sRGB( vColor.rgb ) / ONE_OVER_PI;// vec3(1.0, 0.0, 0.0); // vColor.rgb;
            alpha = max (0.1, vColor.a);
        #endif
    #else
        #ifdef VERTCOLOR 
            mediump vec3 diffuse = Linear_To_sRGB( vColor.rgb ) / ONE_OVER_PI;// vec3(1.0, 0.0, 0.0); // vColor.rgb;
            alpha = max (0.1, vColor.a);
        #else
            mediump vec3 diffuse = vec3(0.462, 0.615, 1.0);
        #endif
    #endif
	    gl_FragColor = vec4(mix (diffuse * 0.3, max (vec3(0.5), diffuse) ,min (pow (spec, 3.0), 1.0) ), alpha);
    #endif
}
void ShadowLOD()
{
    gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
}

#ifdef MESHWRAP
void MeshWrapAlpha()
{
    #ifdef ALBEDO_MAP_ALPHA
        lowp vec4 MeshWrapAlbedo = texture2D(AlbedoMap, vTexCoord.xy).rgba;
        //mediump float MeshWrapAlpha = texture2D(AlbedoMap, vTexCoord.xy).a;//texture2D(AlbedoMapAlpha, vTexCoord.xy).r;
        //mediump float MeshWrapAlpha = texture2D(AlbedoMap, vTexCoord.xy).w;
        //lowp vec3 LineEffect = MeshWrapAlbedo.rgb * vec3(1.0, 0.0, 0.0);

        //Fog
        lowp vec4 finalColor = MeshWrapAlbedo;
        finalColor = vec4(ACESTonemapping(sRGB_To_Linear(finalColor.rgb)), finalColor.a);

        
        #ifdef VFOG
            finalColor.rgb = (1.0 - vColor.a)*MeshWrapAlbedo.rgb + vColor.a*vColor.rgb;
        #endif
        
        #ifndef DCC_MAX
            #if !defined(DISABLE_DOF)
                gl_FragDepth = mix(1.0, gl_FragCoord.z, ceil(finalColor.a - 0.1));
            #endif
        #endif


        #ifdef MESHWRAPEFFECT
            lowp vec3 LineEffect = pow (finalColor.rgb + PanColor.a, vec3(2.0)) * PanColor.rgb;
            lowp float LineMask = texture2D(PanTexture, vec2( vTexCoord.x, clamp (vTexCoord.y + ((Speed * 2.0) -1.0) - (vTexCoord.x * PanTilt), 0.0, 1.0) ) ).r;
            gl_FragColor = vec4 ( mix (finalColor.rgb, LineEffect, floor(finalColor.a + 0.001) * LineMask), finalColor.a) ;
        #else
            gl_FragColor = vec4 (finalColor.rgb, finalColor.a);
        #endif
    #endif
}
#endif

void main(void)
{
    #ifdef GHOST
        GhostShading();
    #elif defined(SHADOWLOD)
        ShadowLOD();
    #elif defined(MESHWRAP)
        MeshWrapAlpha();
    #else
        PhysicallyBasedShading();
    #endif
}
