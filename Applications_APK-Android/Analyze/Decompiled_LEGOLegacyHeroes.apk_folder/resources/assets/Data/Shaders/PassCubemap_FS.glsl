#version 300 es


in highp vec3 vDir;
out highp vec4 fragmentColor;

uniform highp samplerCube envMapSrc;

void main()
{
    highp vec3 D = normalize(vDir);

    highp vec4 col = texture(envMapSrc, D);

	fragmentColor = col;
}
