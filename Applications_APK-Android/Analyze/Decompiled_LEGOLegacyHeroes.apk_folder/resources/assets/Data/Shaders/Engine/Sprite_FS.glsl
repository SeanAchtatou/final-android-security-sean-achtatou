#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

varying lowp vec4 vColor;
varying highp vec2 vUV;

uniform lowp sampler2D BaseColor;
#ifdef SPLIT_ALPHA
uniform lowp sampler2D BaseColor_alpha;
#endif

uniform lowp vec3 ColorAdd;

void main()
{
	lowp vec4 color = texture2D(BaseColor, vUV);
    
    #ifdef SPLIT_ALPHA
        color.a = texture2D(BaseColor_alpha, vUV).r;
    #endif

	gl_FragColor = vColor * color + vec4(ColorAdd, 0.0);
}
