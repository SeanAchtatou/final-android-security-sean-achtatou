#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

varying lowp vec2 vUV;

uniform lowp sampler2D BaseColor;
#ifdef SPLIT_ALPHA
    uniform lowp sampler2D BaseColor_alpha;
#endif

uniform highp float global_EnvironmentIntensity;

void main()
{
	lowp vec4 bc = texture2D(BaseColor,vUV);
	#ifdef SPLIT_ALPHA
	    bc.a = texture2D(BaseColor_alpha, vUV).r;
	#endif

	mediump vec3 color = bc.rgb / max(1. / 256., bc.a);
	#ifdef ENVIRONMENT_INTENSITY
    	color *= global_EnvironmentIntensity;
	#endif

    #ifdef EDITOR_DEPTH
        color = vec3(1.0);  
    #endif
	gl_FragColor = vec4(color, 1.0);
}
