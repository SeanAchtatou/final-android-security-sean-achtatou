
varying lowp vec2 vUV;
varying mediump vec2 vUV1;
varying mediump vec2 vUV2;
varying mediump vec2 vUV3;
varying mediump vec2 vUV4;

attribute highp vec4 position;
attribute highp vec2 texcoord0;

uniform highp mat4 WorldViewProjection;

uniform highp float mistBaseTime;
uniform highp vec2 mistPanTime;


void main()
{	   
	// UVs
	vUV = texcoord0;

	// Position
    highp vec4 pos = WorldViewProjection * position;
	
	highp float baseTime = mistBaseTime + 5.0; 
	highp vec2 panTime = mistPanTime;

	highp vec2 base1 = vec2( 0.0190, -0.0200) * baseTime;
	highp vec2 base2 = vec2(-0.0210,  0.0250) * baseTime;
	highp vec2 base3 = vec2( 0.0215,  0.0105) * (baseTime+2.0);
	highp vec2 base4 = vec2(-0.0150, -0.0280) * (baseTime+2.0);

	highp vec2 panScale1 = vec2(0.85);
	highp vec2 panScale2 = vec2(1.10);
	highp vec2 panScale3 = vec2(1.15);
	highp vec2 panScale4 = vec2(0.90);

	highp vec2 offset1 = vec2(fract(base1 + panScale1 * panTime));
	highp vec2 offset2 = vec2(fract(base2 + panScale2 * panTime));
	highp vec2 offset3 = vec2(fract(base3 + panScale3 * panTime));
	highp vec2 offset4 = vec2(fract(base4 + panScale4 * panTime));

	vUV1 =  texcoord0.xy + offset1;
	vUV2 =  texcoord0.xy + offset2;
	vUV3 =  texcoord0.xy + offset3;
	vUV4 =  texcoord0.xy + offset4;
	
	
    gl_Position = pos; 
}
