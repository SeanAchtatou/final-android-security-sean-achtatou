#version 300 es
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
#endif
uniform mediump float irradianceArray [27];
mediump vec3 irradiance(mediump vec3 normal3)
{

    mediump vec4 cAr = vec4(irradianceArray[0],  irradianceArray[1],  irradianceArray[2],  irradianceArray[3] );
    mediump vec4 cAg = vec4(irradianceArray[4],  irradianceArray[5],  irradianceArray[6],  irradianceArray[7] );
    mediump vec4 cAb = vec4(irradianceArray[8],  irradianceArray[9],  irradianceArray[10], irradianceArray[11]);
    mediump vec4 cBr = vec4(irradianceArray[12], irradianceArray[13], irradianceArray[14], irradianceArray[15]);
    mediump vec4 cBg = vec4(irradianceArray[16], irradianceArray[17], irradianceArray[18], irradianceArray[19]);
    mediump vec4 cBb = vec4(irradianceArray[20], irradianceArray[21], irradianceArray[22], irradianceArray[23]);
    mediump vec3 cC =  vec3(irradianceArray[24], irradianceArray[25], irradianceArray[26]);

    mediump vec4 normal = vec4(normal3, 1.0);
    //normal.z = -normal.z;

    mediump vec3 x1, x2, x3;
    
    // Linear + constant polynomial terms
    x1.r = dot(cAr,normal);
    x1.g = dot(cAg,normal); 
    x1.b = dot(cAb,normal);
    
    // 4 of the quadratic polynomials
    mediump vec4 vB = normal.xyzz * normal.yzzx;   
    x2.r = dot(cBr,vB);
    x2.g = dot(cBg,vB);
    x2.b = dot(cBb,vB);
    
    // Final quadratic polynomial
    mediump float vC = normal.x*normal.x - normal.y*normal.y;
    x3 = cC.rgb * vC;    
    mediump vec3 irr = x1 + x2 + x3;
    
    #if defined(EFFECT_EDITOR) || defined(GLITCH_EDITOR)
        irr = irr * 0.0001 + vec3(1.);        		
    #endif
    
    return irr;
}

mediump vec3 irradianceL0()
{
    return vec3(
        irradianceArray[3] + irradianceArray[14] * 1.0 / 3.0,
        irradianceArray[7] + irradianceArray[18] * 1.0 / 3.0,
        irradianceArray[11] + irradianceArray[22] * 1.0 / 3.0);
}

varying highp vec3 vNormal;
varying highp vec3 vPos;

attribute highp vec4 Vertex;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 World;
uniform highp mat4 Worldit;

void main() 
{
	vNormal = (Worldit * vec4(Vertex.xyz, 0.0)).xyz;
	vPos = (World * Vertex).xyz;
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
