#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define attribute in
#    define varying out
#endif

varying lowp vec2 vUV;

attribute highp vec4 Vertex;
attribute highp vec2 TexCoord0;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 matWorld;

void main() 
{
	vUV = TexCoord0;
	highp vec4 proj = WorldViewProjectionMatrix * Vertex;
	proj.z = proj.w;
	gl_Position = proj;	
}
