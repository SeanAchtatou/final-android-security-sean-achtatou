varying mediump float vExposure;

mediump vec3 CancelExposure(mediump vec3 color)
{	  
	return color / vExposure;
}

mediump vec3 ApplyExposure(mediump vec3 color)
{	  
	return color * vExposure;
}


varying mediump vec2 vUV; 


uniform mediump sampler2D texture0;
//uniform mediump sampler2D texture1;
uniform mediump vec2 texture0TexelSize;
uniform mediump float whitePoint;
uniform mediump float bloomThreshold;
uniform mediump float bloomClamp;

mediump vec3 brightPass(mediump vec3 c)
{
    highp float L = max(dot(c, vec3(0.3,0.6,0.1)), 0.0001);
    highp float L2 = L - bloomThreshold;

	c *= clamp(L2/L, 0.0, bloomClamp); 
    
	return c;
}

void main()
{	   
	mediump vec3 c = vec3(0.);
	mediump vec2 halfTex = texture0TexelSize * 0.5; 
	
	#if defined BRIGHTPASS

    	c = texture2D(texture0, vUV + halfTex).rgb;
        c = ApplyExposure(c);
		c = brightPass(c);

    #endif

	gl_FragColor = vec4(c, 1.0);
}
