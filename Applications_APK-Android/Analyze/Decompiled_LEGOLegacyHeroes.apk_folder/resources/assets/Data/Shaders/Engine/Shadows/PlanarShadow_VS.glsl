
uniform highp mat4 World;

//========================================================================================
//     ____ ___  ______  ______   _  __
//    |_  // _ \/ __/  |/  / _ | | |/_/
//   _/_ </ // /\ \/ /|_/ / __ |_>  <  
//  /____/____/___/_/  /_/_/ |_/_/|_| 

#ifdef DCC_MAX
	#define SEM(x) : x
	#define VERTEX_COLOR_TYPE vec3
#else
	#define SEM(x)
	#define VERTEX_COLOR_TYPE vec4
#endif


//========================================================================================
//    _________  __  _____  _______  _  __     _______________
//   / ___/ __ \/  |/  /  |/  / __ \/ |/ /    / __/ ___/_  __/
//  / /__/ /_/ / /|_/ / /|_/ / /_/ /    /    / _// /__  / /   
//  \___/\____/_/  /_/_/  /_/\____/_/|_/    /_/  \___/ /_/  

lowp mat3 OS2TS(lowp vec3 t, lowp vec3 b, lowp vec3 n)
{	
	#ifdef DCC_MAX
	// 3DSMax TBN is in a left-hand coord.sys, so we interchange T and B and flip B to get a right-hand coord.sys
	// This practice makes the TB match the UV direction and Glitch output at the same time
	lowp vec3 temp = b;
	b = -t;
	t = temp;
	#endif

	return mat3(t.x, b.x, n.x,
				t.y, b.y, n.y,
				t.z, b.z, n.z);
}


mediump vec3 pack(mediump vec3 v){ return v * .5 + .5; }
mediump vec4 pack(mediump vec4 v){ return v * .5 + .5; }
mediump vec3 unpack(mediump vec3 v){ return v * 2. - 1.; }
mediump vec4 unpack(mediump vec4 v){ return v * 2. - 1.; }

//========================================================================================
//    _________  _  _______________   _  ____________
//   / ___/ __ \/ |/ / __/_  __/ _ | / |/ /_  __/ __/
//  / /__/ /_/ /    /\ \  / / / __ |/    / / / _\ \  
//  \___/\____/_/|_/___/ /_/ /_/ |_/_/|_/ /_/ /___/  

#define TWOPI	6.283185307179586476925286766559
#define PI		3.1415926535897932384626433832795
#define HLFPI	1.5707963267948966192313216916398


attribute highp vec4 Vertex;
uniform highp mat4 ViewProjection;
uniform highp mat4 FlattenerShadowProjection;

void main()
{
	gl_Position = ViewProjection * (FlattenerShadowProjection * (World * Vertex));
}
