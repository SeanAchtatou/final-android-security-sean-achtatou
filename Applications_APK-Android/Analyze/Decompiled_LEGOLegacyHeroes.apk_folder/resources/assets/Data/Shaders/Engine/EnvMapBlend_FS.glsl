#version 300 es
#if defined POSITIVE_FACES && defined NEGATIVE_FACES
    #define MAX_DRAW_BUFFERS 6
#else
    #define MAX_DRAW_BUFFERS 4
#endif
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

// Varyings
#ifdef POSITIVE_FACES
    varying highp vec3 vPosXDir;
    varying highp vec3 vPosYDir;
    varying highp vec3 vPosZDir;
#endif

#ifdef NEGATIVE_FACES
    varying highp vec3 vNegXDir;
    varying highp vec3 vNegYDir;
    varying highp vec3 vNegZDir;
#endif

uniform samplerCube envMap0;
uniform mediump float weight0;
#ifdef ENV_MAP_1
uniform samplerCube envMap1;
uniform mediump float weight1;
#endif
#ifdef ENV_MAP_2
uniform samplerCube envMap2;
uniform mediump float weight2;
#endif

uniform mediump float lod;

mediump vec4 sampleEnvMap(samplerCube envMap, highp vec3 direction, mediump float lod)
{
	mediump vec4 c = textureCubeLod(envMap, direction, lod);
    // Decode RGBD
    c.rgb /= max(1. / 256., c.a);
    return c;
}

mediump vec4 blend(highp vec3 dir)
{
    mediump vec4 accum = vec4(0.);
    accum += sampleEnvMap(envMap0, dir, lod) * weight0;
#ifdef ENV_MAP_1
	accum += sampleEnvMap(envMap1, dir, lod) * weight1;
#endif
#ifdef ENV_MAP_2
	accum += sampleEnvMap(envMap2, dir, lod) * weight2;
#endif

    // Encode in RGBDiv8
    mediump float maxValue = max(max(1., accum.r), max(accum.g, accum.b));
    accum.rgb /= maxValue;
    accum.a = 1. / maxValue;

    return accum;
}

void main()
{
#if defined POSITIVE_FACES && defined NEGATIVE_FACES
    fragData[0] = blend(vPosXDir);
    fragData[1] = blend(vPosYDir);
    fragData[2] = blend(vPosZDir);
    fragData[3] = blend(vNegXDir);
    fragData[4] = blend(vNegYDir);
    fragData[5] = blend(vNegZDir);
#elif defined POSITIVE_FACES
    fragData[0] = blend(vPosXDir);
    fragData[1] = blend(vPosYDir);
    fragData[2] = blend(vPosZDir);
#elif defined NEGATIVE_FACES
    fragData[0] = blend(vNegXDir);
    fragData[1] = blend(vNegYDir);
    fragData[2] = blend(vNegZDir);
#else
    fragData[0] = vec4(0.0);
#endif
}
