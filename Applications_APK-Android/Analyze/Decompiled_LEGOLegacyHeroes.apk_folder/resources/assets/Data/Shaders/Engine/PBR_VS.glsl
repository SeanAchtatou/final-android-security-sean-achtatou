#version 300 es

#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif
varying mediump float vExposure;

mediump vec3 CancelExposure(mediump vec3 color)
{	  
	return color / vExposure;
}

mediump vec3 ApplyExposure(mediump vec3 color)
{	  
	return color * vExposure;
}


uniform mediump sampler2D global_exposureTexture;
uniform mediump float global_exposureCompensation;

void PrepareExposure()
{	   
	mediump vec2 exposureTexture = texture2D(global_exposureTexture, vec2(0,0)).rg;
	vExposure = 0.18 / exp2(exposureTexture.r - global_exposureCompensation);
	//vExposure.y = exp2(exposureTexture.g) * vExposure.x;
}

// Force disable texture array for lightmaps
#ifdef USE_TEXTURE_ARRAY
    #undef USE_TEXTURE_ARRAY
    #define sampler2DArray sampler2D
#endif

#if defined SUPERMIX && !defined MIX
	#define MIX
#endif

#if defined VERTEX_COLOR_MASK && !defined ROME_MASK
	#define ROME_MASK
#endif 

#if defined DECAL && !defined ROME_MASK
	#define ROME_MASK
#endif

#if defined DECAL_MASK && defined CHROMAFLAIR
	#error DECAL_MASK and CHROMAFLAIR are mutually exclusive.
#endif 

#ifndef GAMMA_UNIFORM
	#define colorp lowp
#else 
	#define colorp mediump
#endif

#ifdef LOW_END_1TEXTURE_EMUL
    #define LOW_END_1TEXTURE
#endif

#ifdef LOW_END_1TEXTURE
    #define LOW_END
#endif

#ifdef LOW_END
    #undef IBL_MANUAL_ENV_MAP
    #undef SUPERMIX
    #undef MIX
    #undef ROME_MASK
    #undef CHROMAFLAIR
    #undef USE_SHADOW_MAP
    #undef SPECULAR_AA_HACK
    #ifndef LOW_END_SPECULAR
        #ifdef IRRADIANCE
            #undef NORMAL_MAPPING
        #else
            #define TRIVIAL_VS
        #endif
    #endif
    #ifdef IRRADIANCE
        #define VIEW_DEPENDENT_OUTLINE
    #endif
#else
    #undef LOW_END_SPECULAR
    #ifndef NORMAL_MAPPING
        #define NORMAL_MAPPING
    #endif
#endif

#if defined TRIVIAL_VS || defined OUTLINE
    #undef NORMAL_MAPPING
    #undef IRRADIANCE
#endif


varying lowp vec4 vColor;
varying highp vec3 vPosition;
varying mediump vec3 vNormal;
#ifdef NORMAL_MAPPING
varying mediump vec3 vTangent;
varying mediump vec3 vBinormal;
#endif
varying highp vec2 vTexBase;
#ifdef LIGHTMAP
#ifndef LOW_END
//centroid
#endif
#ifdef USE_TEXTURE_ARRAY
varying highp vec3 vTexLightMap;
#else
varying highp vec2 vTexLightMap;
#endif
#endif
#ifdef DECAL
varying highp vec2 vTexDecal;
#endif
#ifdef MIX
varying highp vec2 vTexMix;
#endif
#ifdef SUPERMIX
varying mediump vec2 vBlendEdges;
#endif
#ifdef FOG
varying mediump vec4 vFogColor;
#endif
#if defined LOW_END && defined IRRADIANCE
varying mediump vec3 vDiffuseLight;
#endif

#if defined OUTLINE || defined VIEW_DEPENDENT_OUTLINE
uniform mediump vec4 outlineColor;
varying mediump vec4 vOutlineColor;
#endif

#ifdef IRRADIANCE
	uniform mediump float irradianceArray [27];
	mediump vec3 irradiance(mediump vec3 normal3)
	{

		mediump vec4 cAr = vec4(irradianceArray[0],  irradianceArray[1],  irradianceArray[2],  irradianceArray[3] );
		mediump vec4 cAg = vec4(irradianceArray[4],  irradianceArray[5],  irradianceArray[6],  irradianceArray[7] );
		mediump vec4 cAb = vec4(irradianceArray[8],  irradianceArray[9],  irradianceArray[10], irradianceArray[11]);
		mediump vec4 cBr = vec4(irradianceArray[12], irradianceArray[13], irradianceArray[14], irradianceArray[15]);
		mediump vec4 cBg = vec4(irradianceArray[16], irradianceArray[17], irradianceArray[18], irradianceArray[19]);
		mediump vec4 cBb = vec4(irradianceArray[20], irradianceArray[21], irradianceArray[22], irradianceArray[23]);
		mediump vec3 cC =  vec3(irradianceArray[24], irradianceArray[25], irradianceArray[26]);
	
		mediump vec4 normal = vec4(normal3, 1.0);
		//normal.z = -normal.z;
	
	    mediump vec3 x1, x2, x3;
	    
	    // Linear + constant polynomial terms
	    x1.r = dot(cAr,normal);
	    x1.g = dot(cAg,normal); 
	    x1.b = dot(cAb,normal);
	    
	    // 4 of the quadratic polynomials
	    mediump vec4 vB = normal.xyzz * normal.yzzx;   
	    x2.r = dot(cBr,vB);
	    x2.g = dot(cBg,vB);
	    x2.b = dot(cBb,vB);
	   
	    // Final quadratic polynomial
	    mediump float vC = normal.x*normal.x - normal.y*normal.y;
	    x3 = cC.rgb * vC;    
	    mediump vec3 irr = x1 + x2 + x3;
	    
      	#if defined(EFFECT_EDITOR) || defined(GLITCH_EDITOR)
        	irr = irr * 0.0001 + vec3(1.);        		
      	#endif
        
      	return irr;
	}
	
    mediump vec3 irradianceL0()
    {
        return vec3(
            irradianceArray[3] + irradianceArray[14] * 1.0 / 3.0,
            irradianceArray[7] + irradianceArray[18] * 1.0 / 3.0,
            irradianceArray[11] + irradianceArray[22] * 1.0 / 3.0);
    }
#endif


#ifdef USE_SHADOW_MAP
#define SHADOW_MAP_CASCADE_COUNT 2
varying highp vec3 vSMCoord0;
//varying highp vec3 vSMCoord1;
//varying highp float vViewZ;
#endif

#ifdef USE_SHADOW_MAP
//uniform highp mat4 global_ShadowMapView;

#if defined SHADOW_MAP_OFFSET_POSITION || defined EDITOR
uniform mediump vec3 global_ShadowDir;
uniform mediump vec2 global_ShadowMapOffsets;
#endif

//uniform highp mat4 global_ShadowMapViewProjs[SHADOW_MAP_CASCADE_COUNT];
uniform highp mat4 global_ShadowMapViewProj;

void ReceiveShadow(highp vec3 worldPos, mediump vec3 worldNormal)
{
    #if defined SHADOW_MAP_OFFSET_POSITION || defined EDITOR
        // Compute offset world vertex pos
        mediump vec3 constantOffset = global_ShadowDir * global_ShadowMapOffsets.x;
        mediump float normalOffsetSlopeScale = clamp(1.0 - dot(worldNormal, global_ShadowDir), 0., 1.);
        mediump vec3 normalOffset = worldNormal * normalOffsetSlopeScale * global_ShadowMapOffsets.y;
        
        highp vec3 offsetPosition = worldPos + normalOffset + constantOffset;
    #else
        highp vec3 offsetPosition = worldPos;
    #endif

    vSMCoord0 = (global_ShadowMapViewProj * vec4(offsetPosition, 1.)).xyz;

    //vSMCoord0 = (global_ShadowMapViewProjs[0] * vec4(offsetPosition, 1.)).xyz;
    //vSMCoord1 = (global_ShadowMapViewProjs[1] * vec4(offsetPosition, 1.)).xyz;

    //vViewZ = -(global_ShadowMapView * vec4(worldPos, 1.)).z;
}
#endif


 


const vec2 defaultDetailFrequencies = vec2(1.975, 0.793);
const vec2 defaultDetailFrequencies2 = vec2(1.762, -0.697);
const vec2 defaultBranchFrequencies = vec2(0.375, 0.193);
const vec2 defaultTrunkFrequencies = vec2(0.123, 0.0877);
	
uniform float windSpeed;

uniform float windDetailAmplitude;
uniform float windBranchAmplitude;
uniform float windTrunkAmplitude;

uniform float windDetailPhaseSize;
uniform float windBranchPhaseShift;
	
uniform float windDetailFrequency;
uniform float windBranchFrequency;
uniform float windTrunkFrequency;

uniform vec3 windBranchDirection;
uniform vec3 windTrunkDirection;
uniform vec3 windDetailDirection;

uniform sampler2D perlinTexture;

//const highp vec4 windParams = vec4(10.0, 8.0, 4.0, 0.5); //(wind Strength, Branch Amplitude, Detail Amplitude, Speed)
//const highp vec4 windParams2 = vec4(1.0, 0.0, 1.0, 1.0); //wind direction (xyz), bendFactor
uniform highp float global_gameTime;

vec3 project(vec3 vector, vec3 planeNormal)
{
	return vector - dot(vector, planeNormal) * planeNormal;
}

vec3 ApplyWind(vec3 position, vec3 normal, vec4 color, float localSpeed)
{
	vec2 detailFreqs = defaultDetailFrequencies * windDetailFrequency;
	vec2 detailFreqs2 = defaultDetailFrequencies2 * windDetailFrequency;
	vec2 branchFreqs = defaultBranchFrequencies * windBranchFrequency;
	vec2 trunkFreqs = defaultTrunkFrequencies * windTrunkFrequency;
	
	float vertexDetailAmplitude = color.b;
	float vertexBranchAmplitude = color.r;
	float vertexTrunkAmplitude = color.a;
	float vertexBranchPhase = color.g;
	
	float detailAmplitude = windDetailAmplitude * vertexDetailAmplitude * vertexBranchAmplitude;
	float branchAmplitude = windBranchAmplitude * vertexBranchAmplitude;
	float trunkAmplitude = windTrunkAmplitude * vertexTrunkAmplitude;
	
	// Phases
	float vertexPhase = dot(position.xyz * windDetailPhaseSize, vec3(vertexBranchPhase)); 	
	float mainPhase = global_gameTime * windSpeed + localSpeed;
	vec2 phases = mainPhase + vec2(vertexPhase, vertexBranchPhase);
	float trunkPhase = mainPhase - windBranchPhaseShift * vertexBranchAmplitude;
	
	// Waves
	vec4 waves = (fract(phases.xxyy * vec4(detailFreqs, branchFreqs)) * 2.0 - 1.0);
	vec2 waves2 = (fract(trunkPhase * trunkFreqs) * 2.0 - 1.0);
	
	// + Smoothstep
	//   + Convert from saw to triangle waves
	vec4 triangleWave = abs(waves);
	vec2 triangleWave2 = abs(waves2);
	//   + Cubic hermite
	waves = triangleWave * triangleWave *( 3.0 - 2.0 * triangleWave );  
	waves = waves * 2.0 - 1.0;
	waves2 = triangleWave2 * triangleWave2 *( 3.0 - 2.0 * triangleWave2 );  
	waves2 = waves2 * 2.0 - 1.0;

	// + Final waves results
	vec2 wavesSum = waves.xz + waves.yw;  
	float wavesSum2 = waves2.x + waves2.y;
	
	// Displacement
	vec3 trunkDisplacement = windTrunkDirection * wavesSum2 * trunkAmplitude;
	vec3 branchDisplacement = windBranchDirection * wavesSum.y * branchAmplitude;
	
	#if defined WIND_NOISE
		mat3 m = mat3(windTrunkDirection, cross(windDetailDirection, windTrunkDirection), windDetailDirection);
		vec2 uv = (position * m).xy;
		uv *= windDetailPhaseSize;
		
		
		float perlin1 = texture2D(perlinTexture, uv - detailFreqs * global_gameTime ).r;
		float perlin2 = texture2D(perlinTexture, uv - detailFreqs2 * global_gameTime).g;
		float perlin = (perlin1 + perlin2) * 0.5;
		vec3 detailDisplacement = detailAmplitude * 10.0 * (perlin-0.5) * windDetailDirection;
	#else
		#if defined WIND_DETAIL_NORMAL_PROJECTED
			vec3 projectedNormal = project(normal, windBranchDirection);
			vec3 detailDisplacement = detailAmplitude * wavesSum.x * projectedNormal;
		#elif defined WIND_DETAIL_NORMAL
			vec3 detailDisplacement = detailAmplitude * wavesSum.x * normal;
		#else // defined WIND_DETAIL_DIRECTION
			vec3 detailDisplacement = detailAmplitude * wavesSum.x * windDetailDirection;
		#endif
	#endif
	
	vec3 displacement = trunkDisplacement + branchDisplacement + detailDisplacement;
		
	position.xyz += displacement;
	return position;
}

attribute highp vec4 Vertex;
attribute highp vec2 TexCoord0;
#if defined LIGHTMAP || defined UV1_MAP
attribute highp vec2 TexCoord1;
#endif
#ifdef DECAL
attribute highp vec2 TexCoord2;
#endif
#ifdef MIX
attribute highp vec2 TexCoord3;
#endif
#if defined VERTEX_COLOR || defined VERTEX_COLOR_MASK || defined VERTEX_COLOR_ALPHA || defined WIND
attribute mediump vec4 Color;
#endif
attribute mediump vec3 Normal;
#ifdef NORMAL_MAPPING
attribute mediump vec3 Tangent;
attribute mediump vec3 Binormal;
#endif

uniform highp mat4 ViewProjectionMatrix;
#ifndef BAKED_GEOMETRY
uniform highp mat4 worldIT;
uniform highp mat4 World;
#endif

#ifdef UV_SCALEOFFSET
uniform highp vec4 TexCoord0_scaleoffset;
uniform highp vec4 texCoord0ScaleOffset;
#if defined LIGHTMAP || defined UV1_MAP
uniform highp vec4 TexCoord1_scaleoffset;
#endif
#ifdef DECAL
uniform highp vec4 TexCoord2_scaleoffset;
#endif
#ifdef MIX
uniform highp vec4 TexCoord3_scaleoffset;
#endif
#endif

#ifdef SUPERMIX
uniform mediump float blendThreshold;
#endif

uniform mediump float opacity;
#ifdef DECAL_TILING
uniform mediump vec2 decalTiling;
#endif

#ifdef FOG
uniform mediump vec4 global_fogNearColor;
uniform mediump vec4 global_fogFarColor;
uniform highp float global_fogNearDistance;
uniform highp float global_fogFarDistance;
uniform mediump float global_fogIntensity;
#endif

uniform highp vec3 eyepos;

#ifdef OUTLINE
uniform highp vec2 outlineOffsetScale;
#endif
#ifdef VIEW_DEPENDENT_OUTLINE
uniform mediump float global_viewDependentOutlineScale;
#endif


#ifdef SKINNED
uniform highp mat4 BoneMatrices[64];
uniform highp vec4 WeightMask;

attribute highp vec4 SkinWeights;
attribute mediump vec4 SkinIndices;
#endif

#if !defined SKIN_NO_LEGACY
    #ifndef SKIN_NORMAL
		#define SKIN_NORMAL
    #endif
    #if defined NORMAL_MAPPING && !defined SKIN_TANGENTS
		#define SKIN_TANGENTS
    #endif
#endif

#if defined MX_REQUIRE_NORMAL_VERTEX && !defined SKIN_NORMAL
    #define SKIN_NORMAL
#endif
#if defined MX_REQUIRE_TANGENTS_VERTEX && !defined SKIN_TANGENTS
    #define SKIN_TANGENTS
#endif

struct SkinResult 
{
    highp vec3 Position;
    highp vec3 Normal;
    highp vec3 Tangent;
    highp vec3 Binormal;
};


SkinResult SkinAll()
{
    SkinResult result;
#ifdef SKINNED
	highp vec4 weights = SkinWeights * WeightMask;
	highp vec3 position = vec3(0.0);

    mediump int idx0 = int(SkinIndices[0]);

    position += weights[0] * (BoneMatrices[idx0] * vec4(Vertex.xyz, 1.0)).xyz;
    position += weights[1] * (BoneMatrices[int(SkinIndices[1])] * vec4(Vertex.xyz, 1.0)).xyz;
    position += weights[2] * (BoneMatrices[int(SkinIndices[2])] * vec4(Vertex.xyz, 1.0)).xyz;

	#ifndef TRIVIAL_VS
        mediump mat3 boneMatrix = mat3(BoneMatrices[idx0]);

        #ifdef SKIN_NORMAL
    	    mediump vec3 netNormal = vec3(0.0);
            netNormal = boneMatrix * Normal;
            result.Normal = normalize(netNormal);
        #endif
        
        #ifdef SKIN_TANGENTS
	 	    mediump vec3 netTangent = vec3(0.0);
		    mediump vec3 netBinormal = vec3(0.0);
		    netTangent = boneMatrix * Tangent;
		    netBinormal = boneMatrix * Binormal;
            result.Tangent = normalize(netTangent);
            result.Binormal = normalize(netBinormal);
        #endif
    #endif

    result.Position = position;
#else
    #ifdef BAKED_GEOMETRY
	    result.Position = Vertex.xyz;
    #else
        result.Position = (World*vec4(Vertex.xyz, 1.0)).xyz;
    #endif
		
	#ifndef TRIVIAL_VS
        #ifdef BAKED_GEOMETRY
            #ifdef SKIN_NORMAL
                result.Normal = normalize(Normal);
            #endif
            #ifdef SKIN_TANGENTS
                result.Tangent = normalize(Tangent);
                result.Binormal = normalize(Binormal);
            #endif
        #else
            #ifdef SKIN_NORMAL
                result.Normal = normalize((worldIT*vec4(Normal,0.0)).xyz);
            #endif
            #ifdef SKIN_TANGENTS
                result.Tangent = normalize((worldIT*vec4(Tangent,0.0)).xyz);
                result.Binormal = normalize((worldIT*vec4(Binormal,0.0)).xyz);
            #endif
        #endif
    #endif
#endif
#ifdef TRIVIAL_VS
    result.Normal = vec3(0.0);
#endif
	return result;
}

#if !defined SKIN_NO_LEGACY
highp vec3 Skin() // return worldPos
{
    SkinResult result = SkinAll();
    vPosition = result.Position;
    vNormal = result.Normal;
    #ifdef SKIN_TANGENTS
    vTangent = result.Tangent;
    vBinormal = result.Binormal;
    #endif
	return result.Position;
}
#endif

void main() 
{
	highp vec3 worldPos = Skin();
	
	#ifdef WIND
		worldPos = ApplyWind(worldPos, vNormal, Color, 1.0);
	#endif	

    #if defined VERTEX_COLOR || defined VERTEX_COLOR_MASK
	    vColor = vec4(pow(Color.rgb, vec3(2.2)), opacity * Color.a);
    #elif defined VERTEX_COLOR_ALPHA
        vColor = vec4(vec3(1.0), opacity * Color.a);
    #else
        vColor = vec4(vec3(1.0), opacity);
    #endif

	// UV 0
	vTexBase = TexCoord0;
	#ifdef UV_SCALEOFFSET
		vTexBase = (vTexBase * TexCoord0_scaleoffset.xy + TexCoord0_scaleoffset.zw) * texCoord0ScaleOffset.xy + texCoord0ScaleOffset.zw;
	#endif
		
	// UV 1
	#ifdef LIGHTMAP
        highp vec2 texLightMap = TexCoord1;
		#ifdef UV_SCALEOFFSET
			texLightMap = texLightMap * TexCoord1_scaleoffset.xy + TexCoord1_scaleoffset.zw;
		#endif
        #ifdef USE_TEXTURE_ARRAY
            vTexLightMap = vec3(texLightMap, floor(texLightMap.x));
        #else
            vTexLightMap = texLightMap;
        #endif
	#endif
		
	// UV 2
	#ifdef DECAL
		vTexDecal = TexCoord2;
	 	#ifdef UV_SCALEOFFSET
	 		vTexDecal = vTexDecal * TexCoord2_scaleoffset.xy + TexCoord2_scaleoffset.zw;
	 	#endif
	 	#ifdef DECAL_TILING
	 		vTexDecal *= decalTiling;
	 	#endif
	#endif
		
	// UV 3
	#ifdef MIX
		vTexMix = TexCoord3;
	 	#ifdef UV_SCALEOFFSET
			vTexMix = vTexMix * TexCoord3_scaleoffset.xy + TexCoord3_scaleoffset.zw;
	 	#endif
	#endif
	
	// Mix
	#ifdef SUPERMIX
		vBlendEdges.x = Color.a - blendThreshold * 0.5;
		vBlendEdges.y = 1.0 / blendThreshold;
	#endif
		
	#ifdef USE_SHADOW_MAP
		ReceiveShadow(worldPos, vNormal);
	#endif
	
	#ifdef UV1_MAP
		highp vec2 ndc = TexCoord1;
		#ifdef UV_SCALEOFFSET
			ndc = ndc * TexCoord1_scaleoffset.xy + TexCoord1_scaleoffset.zw;
		#endif	
		ndc = ndc * 2.0 - 1.0;
		gl_Position = vec4(ndc.x, ndc.y, 0., 1.);
	#elif defined OUTLINE
		highp vec2 ndcOffset = normalize((ViewProjectionMatrix * vec4(vNormal, 0.0)).xy);
		highp vec4 ndcPos = ViewProjectionMatrix * vec4(worldPos, 1.0);
		gl_Position = ndcPos + vec4(ndcOffset * outlineOffsetScale * ndcPos.w, 0.0, 0.0);
	#else
        gl_Position = ViewProjectionMatrix * vec4(worldPos, 1.);
	#endif
	
	#ifdef FOG
		highp float d = length(eyepos - worldPos);
		mediump float fog = (d - global_fogNearDistance) / (global_fogFarDistance - global_fogNearDistance + 0.0001);
		fog = smoothstep(0.0, 1.0, fog);
		vFogColor = mix(global_fogNearColor, global_fogFarColor, fog) * global_fogIntensity;
	#endif
	
	#if defined CANCEL_EXPOSURE || defined OUTLINE || defined VIEW_DEPENDENT_OUTLINE 
		PrepareExposure();
	#endif

    #if defined OUTLINE || defined VIEW_DEPENDENT_OUTLINE 
        vOutlineColor.rgb = CancelExposure(outlineColor.rgb);
        #ifdef VIEW_DEPENDENT_OUTLINE
            mediump float VoN = clamp(dot(vNormal, normalize(eyepos - worldPos)), 0.0, 1.0);
            vOutlineColor.a = exp(-8.0 * global_viewDependentOutlineScale * VoN * VoN) * outlineColor.a;
        #else
            vOutlineColor.a = outlineColor.a;
        #endif
    #endif

    #if defined LOW_END && defined IRRADIANCE
        vDiffuseLight = irradiance(vNormal);
    #endif
}
