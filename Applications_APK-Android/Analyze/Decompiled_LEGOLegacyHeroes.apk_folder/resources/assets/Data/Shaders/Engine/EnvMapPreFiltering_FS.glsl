#version 300 es
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	out lowp vec4 glitch_FragColor;
	#define gl_FragColor glitch_FragColor
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

#define PI 3.1415926535897932384626433832795

// Varyings
varying mediump vec2 vUV;
varying highp vec3 vDirection;

uniform highp vec3 faceFront;
uniform highp vec3 faceUp;



#define SAMPLE_COUNT 16384

uniform samplerCube texture0;

uniform sampler2D vanDerCorputTexture;
uniform highp float vanDerCorputWidth;
uniform highp float roughness;
uniform highp float lod;

highp float VanDerCorput(int i)
{
    highp float u = (float(i) + 0.5) / vanDerCorputWidth;
    return texture2D(vanDerCorputTexture, vec2(u, 0.5)).x;
}

highp vec2 Hammersley(int i, int sampleCount)
{
    return vec2(float(i) / float(sampleCount), VanDerCorput(i));
}

highp vec3 ImportanceSampleGGX(highp vec2 Xi, highp float Roughness, highp vec3 N)
{
    highp float a = Roughness * Roughness;
    highp float Phi = 2. * PI * Xi.x;
    highp float CosTheta = sqrt( (1. - Xi.y) / ( 1. + (a*a - 1.) * Xi.y ) );
    highp float SinTheta = sqrt( 1. - CosTheta * CosTheta );
    highp vec3 H;
    H.x = SinTheta * cos( Phi );
    H.y = SinTheta * sin( Phi );
    H.z = CosTheta;

    highp vec3 UpVector = abs(N.y) < 0.9999 ? vec3(0.,1.,0.) : vec3(1.,0.,0.);
    highp vec3 TangentX = normalize( cross( UpVector, N ) );
    highp vec3 TangentY = cross( N, TangentX );
    // Tangent to world space
    return TangentX * H.x + TangentY * H.y + N * H.z;
}

highp float D_GGX(highp float NDotH, highp float m)
{
    highp float m2 = m * m;
    highp float f = (NDotH * m2 - NDotH) * NDotH + 1.;
    return m2 / (f * f);
}

highp vec3 PrefilterEnvMap(highp float Roughness, highp vec3 R)
{
    highp float TotalWeight = 0.001;
    highp vec3 N = R;
    highp vec3 V = R;
    highp vec3 PrefilteredColor = vec3(0.);

    for(int i = 0; i < SAMPLE_COUNT; i++ )
    {
        highp vec2 Xi = Hammersley(i, SAMPLE_COUNT);
        highp vec3 H = ImportanceSampleGGX(Xi, Roughness, N);
        highp vec3 L = 2. * dot( V, H ) * H - V;
        highp float NoL = clamp( dot( N, L ), 0., 1. );
        if( NoL > 0. )
        {
            PrefilteredColor += textureCubeLod(texture0, L, 0.).rgb * NoL;
            TotalWeight += NoL;
        }
    }
    return PrefilteredColor * (1. / TotalWeight);
}

void main()
{
#ifdef PASS_THROUGH
	highp vec4 c = textureCubeLod(texture0, vDirection, 0.);
#elif defined LOW_END
    highp vec4 c = textureCubeLod(texture0, vDirection, lod);
    c.rgb /= max(1. / 256., c.a);
    c = clamp(c, 0., 1.);
#else
	highp vec4 c = vec4(PrefilterEnvMap(roughness, normalize(vDirection)), 1.);
#endif

    // Encode in RGBDiv8 
    highp float maxValue = max(max(1., c.r), max(c.g, c.b));
    c.rgb /= maxValue;
    c.a = 1. / maxValue;

    gl_FragColor = c;
}
