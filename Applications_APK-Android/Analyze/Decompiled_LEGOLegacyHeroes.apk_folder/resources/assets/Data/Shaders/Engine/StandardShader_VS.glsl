#version 300 es

#ifndef FLAT
    #define REQUIRE_TANGENT_TO_WORLD
    #define REQUIRE_UV0
#endif

#define REQUIRE_COLOR

#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif

#ifndef MX_REQUIRE_UV0
#define MX_REQUIRE_UV0
#endif

#if defined LIGHTMAP
    #ifndef MX_REQUIRE_UV1
        #define MX_REQUIRE_UV1
    #endif
#endif

#ifndef MX_REQUIRE_POSITION_FRAGMENT
#define MX_REQUIRE_POSITION_FRAGMENT
#endif

#ifndef MX_REQUIRE_EYE_FRAGMENT
#define MX_REQUIRE_EYE_FRAGMENT
#endif

#ifndef MX_REQUIRE_NORMAL_FRAGMENT
#define MX_REQUIRE_NORMAL_FRAGMENT
#endif

// Disable ShadowMap because GL_EXT_shadow_samplers
// is not supported on every Android devices, and produce
// a crash (Ex: Galaxy S5 G900F)
#if !defined(GL_EXT_shadow_samplers)
    #ifdef USE_SHADOW_MAP
        #undef USE_SHADOW_MAP
    #endif
#endif

struct ModelInput 
{
    mediump vec3  BaseColor;
    mediump vec3  Normal;
    mediump float Roughness;
    mediump float Occlusion;
    mediump float Metalness;
    mediump vec3  Emissivity;
    mediump float Opacity;
};


#ifndef MX_MODEL_INPUTS
#define MX_MODEL_INPUTS
#endif

// Public requires
/*
#if defined REQUIRE__FRAGMENT && !defined MX_REQUIRE__FRAGMENT
#define MX_REQUIRE__FRAGMENT
#endif

#if defined REQUIRE__VERTEX && !defined MX_REQUIRE__VERTEX
#define MX_REQUIRE__VERTEX
#endif

#if defined REQUIRE_ && !defined MX_REQUIRE_
#define MX_REQUIRE_
#endif
*/


#if defined REQUIRE_TANGENT_TO_WORLD && !defined MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT
#define MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT
#endif

#if defined REQUIRE_TANGENT_TO_WORLD_VERTEX && !defined MX_REQUIRE_TANGENT_TO_WORLD_VERTEX
#define MX_REQUIRE_TANGENT_TO_WORLD_VERTEX
#endif

#if defined REQUIRE_NORMAL && !defined MX_REQUIRE_NORMAL_FRAGMENT
#define MX_REQUIRE_NORMAL_FRAGMENT
#endif

#if defined REQUIRE_NORMAL_VERTEX && !defined MX_REQUIRE_NORMAL_VERTEX
#define MX_REQUIRE_NORMAL_VERTEX
#endif

#if defined REQUIRE_TANGENTS && !defined MX_REQUIRE_TANGENTS_FRAGMENT
#define MX_REQUIRE_TANGENTS_FRAGMENT
#endif

#if defined REQUIRE_TANGENTS_VERTEX && !defined MX_REQUIRE_TANGENTS_VERTEX
#define MX_REQUIRE_TANGENTS_VERTEX
#endif

#if defined REQUIRE_COLOR && !defined MX_REQUIRE_COLOR_FRAGMENT
#define MX_REQUIRE_COLOR_FRAGMENT
#endif

#if defined REQUIRE_COLOR_VERTEX && !defined MX_REQUIRE_COLOR_VERTEX
#define MX_REQUIRE_COLOR_VERTEX
#endif

#if defined REQUIRE_POSITION && !defined MX_REQUIRE_POSITION_FRAGMENT
#define MX_REQUIRE_POSITION_FRAGMENT
#endif

#if defined REQUIRE_POSITION_VERTEX && !defined MX_REQUIRE_POSITION_VERTEX
#define MX_REQUIRE_POSITION_VERTEX
#endif

#if defined REQUIRE_EYE && !defined MX_REQUIRE_EYE_FRAGMENT
#define MX_REQUIRE_EYE_FRAGMENT
#endif

#if defined REQUIRE_EYE_VERTEX && !defined MX_REQUIRE_EYE_VERTEX
#define MX_REQUIRE_EYE_VERTEX
#endif

#if defined REQUIRE_TIME && !defined MX_REQUIRE_TIME_FRAGMENT
#define MX_REQUIRE_TIME_FRAGMENT
#endif

#if defined REQUIRE_TIME_VERTEX && !defined MX_REQUIRE_TIME_VERTEX
#define MX_REQUIRE_TIME_VERTEX
#endif


#if defined REQUIRE_UV0 && !defined MX_REQUIRE_UV0
#define MX_REQUIRE_UV0
#endif

#if defined REQUIRE_UV0_TYPE && !defined MX_UV0_TYPE
#define MX_UV0_TYPE REQUIRE_UV0_TYPE
#endif

#if defined REQUIRE_UV1 && !defined MX_REQUIRE_UV1
#define MX_REQUIRE_UV1
#endif

#if defined REQUIRE_UV1_TYPE && !defined MX_UV1_TYPE
#define MX_UV1_TYPE REQUIRE_UV1_TYPE
#endif

#if defined REQUIRE_UV2 && !defined MX_REQUIRE_UV2
#define MX_REQUIRE_UV2
#endif

#if defined REQUIRE_UV2_TYPE && !defined MX_UV2_TYPE
#define MX_UV2_TYPE UV2_TYPE
#endif

#if defined REQUIRE_UV3 && !defined MX_REQUIRE_UV3
#define MX_REQUIRE_UV3
#endif

#if defined REQUIRE_UV3_TYPE && !defined MX_UV3_TYPE
#define MX_UV3_TYPE REQUIRE_UV3_TYPE
#endif


// Tangents to world
#if defined MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT && !defined(MX_REQUIRE_TANGENT_TO_WORLD_VERTEX)
#define MX_REQUIRE_TANGENT_TO_WORLD_VERTEX
#endif

#if defined MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT && !defined MX_REQUIRE_NORMAL_FRAGMENT
#define MX_REQUIRE_NORMAL_FRAGMENT
#endif

#if defined MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT && !defined MX_REQUIRE_TANGENTS_FRAGMENT
#define MX_REQUIRE_TANGENTS_FRAGMENT
#endif

#if defined MX_REQUIRE_TANGENT_TO_WORLD_VERTEX && !defined MX_REQUIRE_NORMAL_VERTEX
#define MX_REQUIRE_NORMAL_VERTEX
#endif

#if defined MX_REQUIRE_TANGENT_TO_WORLD_VERTEX && !defined MX_REQUIRE_TANGENTS_VERTEX
#define MX_REQUIRE_TANGENTS_VERTEX
#endif

// Normals
#if defined MX_REQUIRE_NORMAL_FRAGMENT && !defined(MX_REQUIRE_NORMAL_VERTEX)
#define MX_REQUIRE_NORMAL_VERTEX
#endif

// Tangents
#if defined MX_REQUIRE_TANGENTS_FRAGMENT && !defined(MX_REQUIRE_TANGENTS_VERTEX)
#define MX_REQUIRE_TANGENTS_VERTEX
#endif

// Color
#if defined MX_REQUIRE_COLOR_FRAGMENT && !defined(MX_REQUIRE_COLOR_VERTEX)
#define MX_REQUIRE_COLOR_VERTEX
#endif

// Position
#if defined MX_REQUIRE_POSITION_FRAGMENT && !defined(MX_REQUIRE_POSITION_VERTEX)
#define MX_REQUIRE_POSITION_VERTEX
#endif

// UV
#if defined MX_REQUIRE_UV0 && !defined MX_UV0_TYPE
#define MX_UV0_TYPE vec2
#endif

#if defined MX_UV0_TYPE && !defined MX_REQUIRE_UV0
#define MX_REQUIRE_UV0
#endif

#if defined MX_REQUIRE_UV1 && !defined MX_UV1_TYPE
#define MX_UV1_TYPE vec2
#endif

#if defined MX_UV1_TYPE && !defined MX_REQUIRE_UV1
#define MX_REQUIRE_UV1
#endif

#if defined MX_REQUIRE_UV2 && !defined MX_UV2_TYPE
#define MX_UV2_TYPE vec2
#endif

#if defined MX_UV2_TYPE && !defined MX_REQUIRE_UV2
#define MX_REQUIRE_UV2
#endif

#if defined MX_REQUIRE_UV3 && !defined MX_UV3_TYPE
#define MX_UV3_TYPE vec2
#endif

#if defined MX_UV3_TYPE && !defined MX_REQUIRE_UV3
#define MX_REQUIRE_UV3
#endif


// For debuging purpose

#if defined MX_SHOW_LIMITS && !defined MX_REQUIRE_TIME_FRAGMENT
#define MX_REQUIRE_TIME_FRAGMENT
#endif

attribute highp vec4 Vertex;

// Color

#ifdef MX_REQUIRE_COLOR_VERTEX
attribute highp vec4 Color;
lowp vec4 mx_Color;
#endif

#ifdef MX_REQUIRE_NORMAL_VERTEX
attribute highp vec3 Normal;
lowp vec3 mx_Normal;
#endif

#ifdef MX_REQUIRE_TANGENTS_VERTEX
attribute mediump vec3 Tangent;
attribute mediump vec3 Binormal;
mediump vec3 mx_Tangent;
mediump vec3 mx_Binormal;
#endif

#ifdef MX_REQUIRE_TANGENT_TO_WORLD_VERTEX
mediump mat3 mx_TangentToWorld;
#endif

#ifdef MX_REQUIRE_COLOR_FRAGMENT
varying lowp vec4 vColor;
#endif

#ifdef MX_REQUIRE_POSITION_VERTEX
highp vec3 mx_Position;
#endif

#ifdef MX_REQUIRE_POSITION_FRAGMENT
varying highp vec3 vPosition;
#endif

#ifdef MX_REQUIRE_NORMAL_FRAGMENT
varying mediump vec3 vNormal;
#endif

#ifdef MX_REQUIRE_TANGENTS_FRAGMENT
varying mediump vec3 vTangent;
varying mediump vec3 vBinormal;
#endif


// Eye
#ifdef MX_REQUIRE_EYE_VERTEX
uniform highp vec3 eyePos;
highp vec3 mx_EyePosition;
#endif

// Time
#ifdef MX_REQUIRE_TIME_VERTEX
uniform highp float time;
highp float mx_Time;
#endif

// UV
#ifdef MX_REQUIRE_UV0
attribute highp MX_UV0_TYPE TexCoord0;
varying highp MX_UV0_TYPE vUV0;
#ifdef UV_SCALEOFFSET
uniform highp vec4 TexCoord0_scaleoffset;
#endif
#endif
#ifdef MX_REQUIRE_UV1
attribute highp MX_UV1_TYPE TexCoord1;
varying highp MX_UV1_TYPE vUV1;
#ifdef UV_SCALEOFFSET
uniform highp vec4 TexCoord1_scaleoffset;
#endif
#endif
#ifdef MX_REQUIRE_UV2
attribute highp MX_UV2_TYPE TexCoord2;
varying highp MX_UV2_TYPE vUV2;
#ifdef UV_SCALEOFFSET
uniform highp vec4 TexCoord2_scaleoffset;
#endif
#endif

#ifdef MX_REQUIRE_UV3
attribute highp MX_UV3_TYPE TexCoord3;
varying highp MX_UV3_TYPE vUV3;
#ifdef UV_SCALEOFFSET
uniform highp vec4 TexCoord3_scaleoffset;
#endif
#endif

highp vec4 mx_Debug;
#ifdef MX_DEBUG
varying vec4 vDebug;
#endif

uniform highp mat4 World;
uniform highp mat4 ViewProjectionMatrix;
uniform highp mat4 WorldIT;

#define SKIN_NO_LEGACY
uniform highp mat4 worldIT;

const mediump float Pi = 3.14159;
const mediump float Tau = Pi * 2.0;

const mediump vec3 cLumWeights = vec3(0.2126, 0.7152, 0.0722);

mediump float ToksvigSpecularAA(mediump vec3 tangentNormal, mediump float roughness)
{
	mediump float len = length(tangentNormal);
    mediump float a = max(roughness, 0.05);
    mediump float s = 2.0 / (a * a) - 2.0;
    mediump float factor = len / mix(s, 1.0, len);
    return inversesqrt((s * max(factor, 0.01) + 2.0) * 0.5);
}

#ifdef REQUIRE_TANGENT_TO_WORLD
mediump vec3 TransformTangentNormal(mediump vec3 tangentNormal)
{
    mediump vec3 wNormal = mx_TangentToWorld * tangentNormal;
    mediump float len2 = dot(wNormal, wNormal);
    return len2 > 0.0 ? wNormal * inversesqrt(len2) : mx_Normal;
}
#endif

#ifdef SKINNED
uniform highp mat4 BoneMatrices[64];
uniform highp vec4 WeightMask;

attribute highp vec4 SkinWeights;
attribute mediump vec4 SkinIndices;
#endif

#if !defined SKIN_NO_LEGACY
    #ifndef SKIN_NORMAL
		#define SKIN_NORMAL
    #endif
    #if defined NORMAL_MAPPING && !defined SKIN_TANGENTS
		#define SKIN_TANGENTS
    #endif
#endif

#if defined MX_REQUIRE_NORMAL_VERTEX && !defined SKIN_NORMAL
    #define SKIN_NORMAL
#endif
#if defined MX_REQUIRE_TANGENTS_VERTEX && !defined SKIN_TANGENTS
    #define SKIN_TANGENTS
#endif

struct SkinResult 
{
    highp vec3 Position;
    mediump vec3 Normal;
    mediump vec3 Tangent;
    mediump vec3 Binormal;
};

SkinResult SkinAll()
{
    SkinResult result;
#ifdef SKINNED
	highp vec4 weights = SkinWeights * WeightMask;

    // Skin position
    highp vec3 position =
        weights[0] * (BoneMatrices[int(SkinIndices[0])] * vec4(Vertex.xyz, 1.0)).xyz + 
        weights[1] * (BoneMatrices[int(SkinIndices[1])] * vec4(Vertex.xyz, 1.0)).xyz +
        weights[2] * (BoneMatrices[int(SkinIndices[2])] * vec4(Vertex.xyz, 1.0)).xyz +
        weights[3] * (BoneMatrices[int(SkinIndices[3])] * vec4(Vertex.xyz, 1.0)).xyz;
    result.Position = position;

    // Skin normals
    #ifndef TRIVIAL_VS
        #ifdef SKIN_NORMAL
            mediump vec3 netNormal = vec3(0.0);
        #endif
        #ifdef SKIN_TANGENTS
            mediump vec3 netTangent = vec3(0.0);
            mediump vec3 netBinormal = vec3(0.0);
        #endif

        #ifdef SKIN_NORMALS_ONE_INFLUENCE
            mediump mat3 boneMatrix = mat3(BoneMatrices[int(SkinIndices[0])]);
            #ifdef SKIN_NORMAL
                netNormal = boneMatrix * Normal;
            #endif
            #ifdef SKIN_TANGENTS
                netTangent = boneMatrix * Tangent;
                netBinormal = boneMatrix * Binormal;
            #endif
        #else
            mediump vec4 normalsWeights = weights;
            mediump mat3 boneMatrix0 = mat3(BoneMatrices[int(SkinIndices[0])]);
            mediump mat3 boneMatrix1 = mat3(BoneMatrices[int(SkinIndices[1])]);
            mediump mat3 boneMatrix2 = mat3(BoneMatrices[int(SkinIndices[2])]);
            mediump mat3 boneMatrix3 = mat3(BoneMatrices[int(SkinIndices[3])]);
            #ifdef SKIN_NORMAL
                netNormal += normalsWeights[0] * (boneMatrix0 * Normal);
                netNormal += normalsWeights[1] * (boneMatrix1 * Normal);
                netNormal += normalsWeights[2] * (boneMatrix2 * Normal);
                netNormal += normalsWeights[3] * (boneMatrix3 * Normal);
            #endif
            #ifdef SKIN_TANGENTS
                netTangent  += normalsWeights[0] * (boneMatrix0 * Tangent);
                netTangent  += normalsWeights[1] * (boneMatrix1 * Tangent);
                netTangent  += normalsWeights[2] * (boneMatrix2 * Tangent);
                netTangent  += normalsWeights[3] * (boneMatrix3 * Tangent);
                netBinormal += normalsWeights[0] * (boneMatrix0 * Binormal);
                netBinormal += normalsWeights[1] * (boneMatrix1 * Binormal);
                netBinormal += normalsWeights[2] * (boneMatrix2 * Binormal);
                netBinormal += normalsWeights[3] * (boneMatrix3 * Binormal);
            #endif
        #endif

        #ifdef SKIN_NORMAL
            result.Normal = normalize(netNormal);
        #endif
        #ifdef SKIN_TANGENTS
            result.Tangent = normalize(netTangent);
            result.Binormal = normalize(netBinormal);
        #endif
    #endif
#else
    result.Position = (World * vec4(Vertex.xyz, 1.0)).xyz;
		
	#ifndef TRIVIAL_VS
        #ifdef SKIN_NORMAL
            result.Normal = normalize((worldIT * vec4(Normal, 0.0)).xyz);
        #endif
        #ifdef SKIN_TANGENTS
            result.Tangent = normalize((worldIT * vec4(Tangent, 0.0)).xyz);
            result.Binormal = normalize((worldIT * vec4(Binormal, 0.0)).xyz);
        #endif
    #endif
#endif
#ifdef TRIVIAL_VS
    result.Normal = vec3(0.0);
#endif
	return result;
}

#if !defined SKIN_NO_LEGACY
highp vec3 Skin() // return worldPos
{
    SkinResult result = SkinAll();
    vPosition = result.Position;
    vNormal = result.Normal;
    #ifdef SKIN_TANGENTS
    vTangent = result.Tangent;
    vBinormal = result.Binormal;
    #endif
	return result.Position;
}
#endif

#ifdef USE_SHADOW_MAP
#define SHADOW_MAP_CASCADE_COUNT 2
varying highp vec3 vSMCoord0;
//varying highp vec3 vSMCoord1;
//varying highp float vViewZ;
#endif

#ifdef USE_SHADOW_MAP
//uniform highp mat4 global_ShadowMapView;

#if !defined SHADOW_MAP_DONT_OFFSET_POSITION
uniform mediump vec3 global_ShadowDir;
uniform mediump vec2 global_ShadowMapOffsets;
#endif

//uniform highp mat4 global_ShadowMapViewProjs[SHADOW_MAP_CASCADE_COUNT];
uniform highp mat4 global_ShadowMapViewProj;

void ReceiveShadow(highp vec3 worldPos, mediump vec3 worldNormal)
{
    #if !defined SHADOW_MAP_DONT_OFFSET_POSITION
        // Compute offset world vertex pos
        mediump vec3 constantOffset = global_ShadowDir * global_ShadowMapOffsets.x;
        mediump float normalOffsetSlopeScale = clamp(1.0 - dot(worldNormal, global_ShadowDir), 0., 1.);
        mediump vec3 normalOffset = worldNormal * normalOffsetSlopeScale * global_ShadowMapOffsets.y;
        
        highp vec3 offsetPosition = worldPos + normalOffset + constantOffset;
    #else
        highp vec3 offsetPosition = worldPos;
    #endif

    vSMCoord0 = (global_ShadowMapViewProj * vec4(offsetPosition, 1.)).xyz;

    //vSMCoord0 = (global_ShadowMapViewProjs[0] * vec4(offsetPosition, 1.)).xyz;
    //vSMCoord1 = (global_ShadowMapViewProjs[1] * vec4(offsetPosition, 1.)).xyz;

    //vViewZ = -(global_ShadowMapView * vec4(worldPos, 1.)).z;
}
#endif



#ifdef MX_EMPTY_MODEL
struct ModelInput
{
highp float reserved;
};
#endif


void graph(inout ModelInput model);
void model(ModelInput inputs);

#ifdef MX_EMPTY_GRAPH
void graph(inout ModelInput model) { }
#endif

#ifdef MX_EMPTY_MODEL
void model(ModelInput inputs) { }
#endif


void main() 
{

    SkinResult skin = SkinAll();

    #ifdef MX_REQUIRE_POSITION_VERTEX 
        mx_Position = skin.Position;
    #endif
    #ifdef MX_REQUIRE_POSITION_FRAGMENT
        vPosition = mx_Position;
    #endif

    #ifdef MX_REQUIRE_NORMAL_VERTEX
        mx_Normal = skin.Normal;
    #endif
    #ifdef MX_REQUIRE_NORMAL_FRAGMENT
        vNormal = mx_Normal;
    #endif

    #ifdef MX_REQUIRE_TANGENTS_VERTEX
        mx_Tangent = skin.Tangent;
        mx_Binormal = skin.Binormal;
    #endif
    #ifdef MX_REQUIRE_TANGENTS_FRAGMENT
        vTangent = mx_Tangent;
        vBinormal = mx_Binormal;
    #endif

    #ifdef MX_REQUIRE_TANGENT_TO_WORLD_VERTEX
        mx_TangentToWorld = mat3(mx_Tangent, mx_Binormal, mx_Normal);
    #endif

    #ifdef MX_REQUIRE_COLOR_VERTEX
        mx_Color = Color;
        mx_Color.rgb = pow(mx_Color.rgb,vec3(2.2));
    #endif
    #ifdef MX_REQUIRE_COLOR_FRAGMENT
        vColor = mx_Color;
    #endif

    #ifdef MX_REQUIRE_TIME_VERTEX
        mx_Time = time;
    #endif

    #ifdef MX_REQUIRE_EYE_VERTEX
        mx_EyePosition = eyePos;
    #endif


    #ifdef MX_REQUIRE_UV0
        vUV0 = TexCoord0;
		#ifdef UV_SCALEOFFSET
			vUV0 = vUV0 * TexCoord0_scaleoffset.xy + TexCoord0_scaleoffset.zw;
		#endif        
    #endif
    #ifdef MX_REQUIRE_UV1
        vUV1 = TexCoord1;
		#ifdef UV_SCALEOFFSET
			vUV1 = vUV1 * TexCoord1_scaleoffset.xy + TexCoord1_scaleoffset.zw;
		#endif        
    #endif
    #ifdef MX_REQUIRE_UV2
        vUV2 = TexCoord2;
        #ifdef UV_SCALEOFFSET
			vUV2 = vUV2 * TexCoord2_scaleoffset.xy + TexCoord2_scaleoffset.zw;
		#endif        
    #endif
    #ifdef MX_REQUIRE_UV3
        vUV3 = TexCoord3;
        #ifdef UV_SCALEOFFSET
			vUV3 = vUV3 * TexCoord3_scaleoffset.xy + TexCoord3_scaleoffset.zw;
		#endif        
    #endif

    #ifdef USE_SHADOW_MAP
        ReceiveShadow(mx_Position, mx_Normal);
    #endif

    ModelInput inputs;
    graph(inputs);

    model(inputs);

    #ifdef MX_DEBUG
        vDebug = mx_Debug;
    #endif    

	#ifdef UV1_MAP
		highp vec2 ndc = TexCoord1;
        #ifdef UV_SCALEOFFSET
            ndc = ndc * TexCoord1_scaleoffset.xy + TexCoord1_scaleoffset.zw;
        #endif	
		ndc = ndc * 2.0 - 1.0;
		gl_Position = vec4(ndc.x, ndc.y, 0., 1.);
	#elif defined OUTLINE
		highp vec2 ndcOffset = normalize((ViewProjectionMatrix * vec4(mx_Normal, 0.0)).xy);
		highp vec4 ndcPos = ViewProjectionMatrix * vec4(mx_Position, 1.0);
		gl_Position = ndcPos + vec4(ndcOffset * outlineOffsetScale * ndcPos.w, 0.0, 0.0);
	#else
        gl_Position = ViewProjectionMatrix * vec4(mx_Position, 1.);
	#endif
}


#ifdef FOG
uniform mediump vec4 global_fogNearColor;
uniform mediump vec4 global_fogFarColor;
uniform highp float global_fogNearDistance;
uniform highp float global_fogFarDistance;
uniform mediump float global_fogIntensity;

mediump vec4 ComputeFogColor(highp float dist)
{
	mediump float fog = (dist - global_fogNearDistance) / (global_fogFarDistance - global_fogNearDistance + 0.0001);
	fog = smoothstep(0.0, 1.0, fog);
	mediump vec4 result = mix(global_fogNearColor, global_fogFarColor, fog);
    result.rgb *= global_fogIntensity;
    return result;
}
#endif



void model(ModelInput inputs)
{
}





void graph(inout ModelInput model) 
{
}

