#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif


varying mediump float vExposure;

mediump vec3 CancelExposure(mediump vec3 color)
{	  
	return color / vExposure;
}

mediump vec3 ApplyExposure(mediump vec3 color)
{	  
	return color * vExposure;
}


uniform mediump sampler2D global_exposureTexture;
uniform mediump float global_exposureCompensation;

void PrepareExposure()
{	   
	mediump vec2 exposureTexture = texture2D(global_exposureTexture, vec2(0,0)).rg;
	vExposure = 0.18 / exp2(exposureTexture.r - global_exposureCompensation);
	//vExposure.y = exp2(exposureTexture.g) * vExposure.x;
}


varying highp vec2 vUV;


attribute highp vec4 position;
attribute highp vec2 texcoord0;

uniform highp mat4 WorldViewProjection;

// UV scale offset
uniform highp vec2 uvScale;
uniform highp vec2 uvOffset;


// Common vertex shader
void main()
{	   
	// UVs
	vUV = texcoord0;
	  
	// Auto exposure
	PrepareExposure();
	
	// Position
    highp vec4 pos = WorldViewProjection * position;

    gl_Position = pos; 
}
