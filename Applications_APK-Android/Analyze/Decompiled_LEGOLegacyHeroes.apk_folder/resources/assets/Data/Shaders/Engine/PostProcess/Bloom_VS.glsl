varying mediump float vExposure;

mediump vec3 CancelExposure(mediump vec3 color)
{	  
	return color / vExposure;
}

mediump vec3 ApplyExposure(mediump vec3 color)
{	  
	return color * vExposure;
}


uniform mediump sampler2D global_exposureTexture;
uniform mediump float global_exposureCompensation;

void PrepareExposure()
{	   
	mediump vec2 exposureTexture = texture2D(global_exposureTexture, vec2(0,0)).rg;
	vExposure = 0.18 / exp2(exposureTexture.r - global_exposureCompensation);
	//vExposure.y = exp2(exposureTexture.g) * vExposure.x;
}

varying mediump vec2 vUV; 


attribute highp vec4 Vertex;
attribute highp vec2 TexCoord0;

uniform highp mat4 WorldViewProjection;

// UV scale offset
uniform highp vec2 uvScale;
uniform highp vec2 uvOffset;

// Common vertex shader
void main()
{	   
    // UVs
    vUV = TexCoord0 * uvScale + uvOffset;
	  
	// Auto exposure
    PrepareExposure();	  
	
	  // Position
    highp vec4 pos = WorldViewProjection * Vertex;
    gl_Position = pos;
}
