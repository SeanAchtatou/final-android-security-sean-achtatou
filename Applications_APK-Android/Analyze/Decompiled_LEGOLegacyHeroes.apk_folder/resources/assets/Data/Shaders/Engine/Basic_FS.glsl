varying highp vec2 vExposure;



highp vec3 CancelExposure(highp vec3 color)
{	  
	return color / vExposure.x;
}

highp vec3 ApplyExposure(highp vec3 color)
{	  
	return color * vExposure.x;
}

varying lowp vec4 vColor;
varying lowp vec3 vNormal;
varying lowp vec3 vEyePosition;
varying lowp vec3 vPosition;



uniform highp vec4  ambiantLightColor;
uniform highp vec3  l0Direction;
uniform highp vec4  l0DiffuseColor;
uniform highp vec4  l0SpecularColor;
uniform highp vec3  l1Direction;
uniform highp vec4  l1DiffuseColor;
uniform highp vec4  l1SpecularColor;
uniform highp vec3  l2Direction;
uniform highp vec4  l2DiffuseColor;
uniform highp vec4  l2SpecularColor;
uniform highp float specPower;
uniform highp float specAmount;
uniform highp vec4  specColor;
uniform highp vec4  diffuseColor;
uniform highp float whiteAmount;
uniform highp float opacity;
uniform lowp float cancelExposure;

highp vec4 screen(highp vec4 a, highp vec4 b)
{
	return 1.0 - (1.0 - a) * (1.0 - b);
}

highp vec3 screen(highp vec3 a, highp vec3 b)
{
	return 1.0 - (1.0 - a) * (1.0 - b);
}

highp float screen(highp float a, highp float b)
{
	return 1.0 - (1.0 - a) * (1.0 - b);
}



void main()
{
	highp vec3 normal = normalize(vNormal);
	highp vec3 l0 = normalize(l0Direction);
	highp vec3 l1 = normalize(l1Direction);
	highp vec3 l2 = normalize(l2Direction);
	highp vec3 eye = normalize(vEyePosition-vPosition);
	highp float facing = dot(eye, normal);


	highp vec3 light = ambiantLightColor.rgb;
	highp vec3 spec = vec3(0.0);

	
	// + Diffuse
	highp float l0dot = clamp(-dot(l0, normal),0.0,1.0);
	light = screen(light, l0dot * l0DiffuseColor.rgb);
	highp float l1dot = clamp(-dot(l1, normal),0.0,1.0);
	light = screen(light, l1dot * l1DiffuseColor.rgb);
	highp float l2dot = clamp(-dot(l2, normal),0.0,1.0);
	light = screen(light, l2dot * l2DiffuseColor.rgb);
	//light += l0dot * light0DiffuseColor;
	
	// + Specular
	highp vec3 l0ref = reflect(l0, normal);
	highp float l0spec = clamp(dot(l0ref, eye),0.0,1.0);
	l0spec = pow(l0spec, specPower+.0001);
	l0spec *= specAmount;
	spec = l0spec * l0SpecularColor.rgb * pow(l0dot,0.5);

	// + Specular
	highp vec3 l1ref = reflect(l1, normal);
	highp float l1spec = clamp(dot(l1ref, eye),0.0,1.0);
	l1spec = pow(l1spec, specPower+.0001);
	l1spec *= specAmount;
	spec = screen(spec, l1spec * l1SpecularColor.rgb * pow(l1dot,0.5));
	
	// + Specular
	highp vec3 l2ref = reflect(l2, normal);
	highp float l2spec = clamp(dot(l2ref, eye),0.0,1.0);
	l2spec = pow(l2spec, specPower+.0001);
	l2spec *= specAmount;
	spec = screen(spec, l2spec * l2SpecularColor.rgb * pow(l2dot,0.5));
	
	
	highp vec4 color = mix(vColor.rgba, vec4(1), whiteAmount);
	color.rgb *= light * diffuseColor.rgb;
	color.rgb = screen(color.rgb, spec * specColor.rgb);
	//color.rgb *= facing;
	
	color.a *= opacity * diffuseColor.a;

	color.rgb = mix(color.rgb, CancelExposure(color.rgb), cancelExposure);
	
	gl_FragColor = color.rgba;
}
