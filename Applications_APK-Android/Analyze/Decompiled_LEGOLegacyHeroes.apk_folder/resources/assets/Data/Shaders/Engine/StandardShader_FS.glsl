#version 300 es

#ifndef FLAT
    #define REQUIRE_TANGENT_TO_WORLD
    #define REQUIRE_UV0
#endif

#define REQUIRE_COLOR

#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

#ifndef MX_REQUIRE_UV0
#define MX_REQUIRE_UV0
#endif

#if defined LIGHTMAP
    #ifndef MX_REQUIRE_UV1
        #define MX_REQUIRE_UV1
    #endif
#endif

#ifndef MX_REQUIRE_POSITION_FRAGMENT
#define MX_REQUIRE_POSITION_FRAGMENT
#endif

#ifndef MX_REQUIRE_EYE_FRAGMENT
#define MX_REQUIRE_EYE_FRAGMENT
#endif

#ifndef MX_REQUIRE_NORMAL_FRAGMENT
#define MX_REQUIRE_NORMAL_FRAGMENT
#endif

// Disable ShadowMap because GL_EXT_shadow_samplers
// is not supported on every Android devices, and produce
// a crash (Ex: Galaxy S5 G900F)
#if !defined(GL_EXT_shadow_samplers)
    #ifdef USE_SHADOW_MAP
        #undef USE_SHADOW_MAP
    #endif
#endif

struct ModelInput 
{
    mediump vec3  BaseColor;
    mediump vec3  Normal;
    mediump float Roughness;
    mediump float Occlusion;
    mediump float Metalness;
    mediump vec3  Emissivity;
    mediump float Opacity;
};


#ifndef MX_MODEL_INPUTS
#define MX_MODEL_INPUTS
#endif

// Public requires
/*
#if defined REQUIRE__FRAGMENT && !defined MX_REQUIRE__FRAGMENT
#define MX_REQUIRE__FRAGMENT
#endif

#if defined REQUIRE__VERTEX && !defined MX_REQUIRE__VERTEX
#define MX_REQUIRE__VERTEX
#endif

#if defined REQUIRE_ && !defined MX_REQUIRE_
#define MX_REQUIRE_
#endif
*/


#if defined REQUIRE_TANGENT_TO_WORLD && !defined MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT
#define MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT
#endif

#if defined REQUIRE_TANGENT_TO_WORLD_VERTEX && !defined MX_REQUIRE_TANGENT_TO_WORLD_VERTEX
#define MX_REQUIRE_TANGENT_TO_WORLD_VERTEX
#endif

#if defined REQUIRE_NORMAL && !defined MX_REQUIRE_NORMAL_FRAGMENT
#define MX_REQUIRE_NORMAL_FRAGMENT
#endif

#if defined REQUIRE_NORMAL_VERTEX && !defined MX_REQUIRE_NORMAL_VERTEX
#define MX_REQUIRE_NORMAL_VERTEX
#endif

#if defined REQUIRE_TANGENTS && !defined MX_REQUIRE_TANGENTS_FRAGMENT
#define MX_REQUIRE_TANGENTS_FRAGMENT
#endif

#if defined REQUIRE_TANGENTS_VERTEX && !defined MX_REQUIRE_TANGENTS_VERTEX
#define MX_REQUIRE_TANGENTS_VERTEX
#endif

#if defined REQUIRE_COLOR && !defined MX_REQUIRE_COLOR_FRAGMENT
#define MX_REQUIRE_COLOR_FRAGMENT
#endif

#if defined REQUIRE_COLOR_VERTEX && !defined MX_REQUIRE_COLOR_VERTEX
#define MX_REQUIRE_COLOR_VERTEX
#endif

#if defined REQUIRE_POSITION && !defined MX_REQUIRE_POSITION_FRAGMENT
#define MX_REQUIRE_POSITION_FRAGMENT
#endif

#if defined REQUIRE_POSITION_VERTEX && !defined MX_REQUIRE_POSITION_VERTEX
#define MX_REQUIRE_POSITION_VERTEX
#endif

#if defined REQUIRE_EYE && !defined MX_REQUIRE_EYE_FRAGMENT
#define MX_REQUIRE_EYE_FRAGMENT
#endif

#if defined REQUIRE_EYE_VERTEX && !defined MX_REQUIRE_EYE_VERTEX
#define MX_REQUIRE_EYE_VERTEX
#endif

#if defined REQUIRE_TIME && !defined MX_REQUIRE_TIME_FRAGMENT
#define MX_REQUIRE_TIME_FRAGMENT
#endif

#if defined REQUIRE_TIME_VERTEX && !defined MX_REQUIRE_TIME_VERTEX
#define MX_REQUIRE_TIME_VERTEX
#endif


#if defined REQUIRE_UV0 && !defined MX_REQUIRE_UV0
#define MX_REQUIRE_UV0
#endif

#if defined REQUIRE_UV0_TYPE && !defined MX_UV0_TYPE
#define MX_UV0_TYPE REQUIRE_UV0_TYPE
#endif

#if defined REQUIRE_UV1 && !defined MX_REQUIRE_UV1
#define MX_REQUIRE_UV1
#endif

#if defined REQUIRE_UV1_TYPE && !defined MX_UV1_TYPE
#define MX_UV1_TYPE REQUIRE_UV1_TYPE
#endif

#if defined REQUIRE_UV2 && !defined MX_REQUIRE_UV2
#define MX_REQUIRE_UV2
#endif

#if defined REQUIRE_UV2_TYPE && !defined MX_UV2_TYPE
#define MX_UV2_TYPE UV2_TYPE
#endif

#if defined REQUIRE_UV3 && !defined MX_REQUIRE_UV3
#define MX_REQUIRE_UV3
#endif

#if defined REQUIRE_UV3_TYPE && !defined MX_UV3_TYPE
#define MX_UV3_TYPE REQUIRE_UV3_TYPE
#endif


// Tangents to world
#if defined MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT && !defined(MX_REQUIRE_TANGENT_TO_WORLD_VERTEX)
#define MX_REQUIRE_TANGENT_TO_WORLD_VERTEX
#endif

#if defined MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT && !defined MX_REQUIRE_NORMAL_FRAGMENT
#define MX_REQUIRE_NORMAL_FRAGMENT
#endif

#if defined MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT && !defined MX_REQUIRE_TANGENTS_FRAGMENT
#define MX_REQUIRE_TANGENTS_FRAGMENT
#endif

#if defined MX_REQUIRE_TANGENT_TO_WORLD_VERTEX && !defined MX_REQUIRE_NORMAL_VERTEX
#define MX_REQUIRE_NORMAL_VERTEX
#endif

#if defined MX_REQUIRE_TANGENT_TO_WORLD_VERTEX && !defined MX_REQUIRE_TANGENTS_VERTEX
#define MX_REQUIRE_TANGENTS_VERTEX
#endif

// Normals
#if defined MX_REQUIRE_NORMAL_FRAGMENT && !defined(MX_REQUIRE_NORMAL_VERTEX)
#define MX_REQUIRE_NORMAL_VERTEX
#endif

// Tangents
#if defined MX_REQUIRE_TANGENTS_FRAGMENT && !defined(MX_REQUIRE_TANGENTS_VERTEX)
#define MX_REQUIRE_TANGENTS_VERTEX
#endif

// Color
#if defined MX_REQUIRE_COLOR_FRAGMENT && !defined(MX_REQUIRE_COLOR_VERTEX)
#define MX_REQUIRE_COLOR_VERTEX
#endif

// Position
#if defined MX_REQUIRE_POSITION_FRAGMENT && !defined(MX_REQUIRE_POSITION_VERTEX)
#define MX_REQUIRE_POSITION_VERTEX
#endif

// UV
#if defined MX_REQUIRE_UV0 && !defined MX_UV0_TYPE
#define MX_UV0_TYPE vec2
#endif

#if defined MX_UV0_TYPE && !defined MX_REQUIRE_UV0
#define MX_REQUIRE_UV0
#endif

#if defined MX_REQUIRE_UV1 && !defined MX_UV1_TYPE
#define MX_UV1_TYPE vec2
#endif

#if defined MX_UV1_TYPE && !defined MX_REQUIRE_UV1
#define MX_REQUIRE_UV1
#endif

#if defined MX_REQUIRE_UV2 && !defined MX_UV2_TYPE
#define MX_UV2_TYPE vec2
#endif

#if defined MX_UV2_TYPE && !defined MX_REQUIRE_UV2
#define MX_REQUIRE_UV2
#endif

#if defined MX_REQUIRE_UV3 && !defined MX_UV3_TYPE
#define MX_UV3_TYPE vec2
#endif

#if defined MX_UV3_TYPE && !defined MX_REQUIRE_UV3
#define MX_REQUIRE_UV3
#endif


// For debuging purpose

#if defined MX_SHOW_LIMITS && !defined MX_REQUIRE_TIME_FRAGMENT
#define MX_REQUIRE_TIME_FRAGMENT
#endif

// Color
#ifdef MX_REQUIRE_COLOR_FRAGMENT
varying lowp vec4 vColor;
lowp vec4 mx_Color;
#endif

#ifdef MX_REQUIRE_POSITION_FRAGMENT
varying highp vec3 vPosition;
highp vec3 mx_Position;
#endif

#ifdef MX_REQUIRE_NORMAL_FRAGMENT
varying mediump vec3 vNormal;
lowp vec3 mx_Normal;
#endif

#ifdef MX_REQUIRE_TANGENTS_FRAGMENT
varying mediump vec3 vTangent;
varying mediump vec3 vBinormal;
mediump vec3 mx_Tangent;
mediump vec3 mx_Binormal;
#endif

#ifdef MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT
mediump mat3 mx_TangentToWorld;
#endif


// Eye
#ifdef MX_REQUIRE_EYE_FRAGMENT
uniform highp vec3 eyePos;
highp vec3 mx_EyePosition;
#endif

// Time
#ifdef MX_REQUIRE_TIME_FRAGMENT
uniform highp float time;
highp float mx_Time;
#endif

// UV
#ifdef MX_REQUIRE_UV0
varying highp MX_UV0_TYPE vUV0;
highp MX_UV0_TYPE mx_UV0;
#endif

#ifdef MX_REQUIRE_UV1
varying highp MX_UV1_TYPE vUV1;
highp MX_UV1_TYPE mx_UV1;
#endif

#ifdef MX_REQUIRE_UV2
varying highp MX_UV2_TYPE vUV2;
highp MX_UV2_TYPE mx_UV2;
#endif

#ifdef MX_REQUIRE_UV3
varying highp MX_UV3_TYPE vUV3;
highp MX_UV3_TYPE mx_UV3;
#endif

#ifdef MX_DEBUG
varying highp vec4 vDebug;
#endif
highp vec4 mx_Debug;


#define SKIN_NO_LEGACY
uniform highp mat4 worldIT;

const mediump float Pi = 3.14159;
const mediump float Tau = Pi * 2.0;

const mediump vec3 cLumWeights = vec3(0.2126, 0.7152, 0.0722);

mediump float ToksvigSpecularAA(mediump vec3 tangentNormal, mediump float roughness)
{
	mediump float len = length(tangentNormal);
    mediump float a = max(roughness, 0.05);
    mediump float s = 2.0 / (a * a) - 2.0;
    mediump float factor = len / mix(s, 1.0, len);
    return inversesqrt((s * max(factor, 0.01) + 2.0) * 0.5);
}

#ifdef REQUIRE_TANGENT_TO_WORLD
mediump vec3 TransformTangentNormal(mediump vec3 tangentNormal)
{
    mediump vec3 wNormal = mx_TangentToWorld * tangentNormal;
    mediump float len2 = dot(wNormal, wNormal);
    return len2 > 0.0 ? wNormal * inversesqrt(len2) : mx_Normal;
}
#endif

#ifdef USE_SHADOW_MAP
#define SHADOW_MAP_CASCADE_COUNT 2
varying highp vec3 vSMCoord0;
//varying highp vec3 vSMCoord1;
//varying highp float vViewZ;
#endif

#ifdef USE_SHADOW_MAP
#ifdef GLSL2METAL
GLITCH_UNIFORM_PROPERTIES(global_ShadowMapSampler, (samplerCompareFunc = greater, samplerFilter = linear))
#endif
uniform highp sampler2DShadow global_ShadowMapSampler;
uniform highp vec2 global_ShadowMapScale;
uniform mediump vec3 global_ShadowColor;

//uniform highp vec4 global_SMSplits;
//uniform highp float global_SMIntersticeInvSize;

mediump float SampleShadowMap(mediump vec2 baseUV, mediump vec2 offset, highp float depth)
{
#if !defined(GL_ES) && __VERSION__ < 130
    return shadow2D(global_ShadowMapSampler, vec3(baseUV + offset * global_ShadowMapScale, depth)).r;
#else
	return shadow2D(global_ShadowMapSampler, vec3(baseUV + offset * global_ShadowMapScale, depth));
#endif	
}

mediump float SampleShadowMapPCF3x3(highp vec2 smCoord, highp float depth)
{
	highp vec2 uv = smCoord.xy / global_ShadowMapScale; 

	mediump vec2 baseUV;
    baseUV.x = floor(uv.x + 0.5);
    baseUV.y = floor(uv.y + 0.5);

    mediump float s = (uv.x + 0.5 - baseUV.x);
    mediump float t = (uv.y + 0.5 - baseUV.y);
	
 	baseUV -= vec2(0.5, 0.5);
    baseUV *= global_ShadowMapScale;
	
	mediump float sum = 0.0;

	mediump float uw0 = (3. - 2. * s);
    mediump float uw1 = (1. + 2. * s);

    mediump float u0 = (2. - s) / uw0 - 1.;
    mediump float u1 = s / uw1 + 1.;

    mediump float vw0 = (3. - 2. * t);
    mediump float vw1 = (1. + 2. * t);

    mediump float v0 = (2. - t) / vw0 - 1.;
    mediump float v1 = t / vw1 + 1.;

    sum += uw0 * vw0 * SampleShadowMap(baseUV, vec2(u0, v0), depth);
    sum += uw1 * vw0 * SampleShadowMap(baseUV, vec2(u1, v0), depth);
    sum += uw0 * vw1 * SampleShadowMap(baseUV, vec2(u0, v1), depth);
    sum += uw1 * vw1 * SampleShadowMap(baseUV, vec2(u1, v1), depth);

	return sum * 1.0 / 16.0;
}

mediump float GetShadowCoefficient()
{
    return SampleShadowMapPCF3x3(vSMCoord0.xy, clamp(vSMCoord0.z, 0.0, 1.0));
}
#endif


#ifdef MX_EMPTY_MODEL
struct ModelInput
{
highp float reserved;
};
#endif



void graph(inout ModelInput inputs);
mediump vec4 model(ModelInput inputs);



//ModelInput inputs;

#ifdef MX_EMPTY_MODEL
mediump vec4 model(ModelInput inputs) { return vec4(0,0,0,1); }
#endif

#ifdef MX_EMPTY_GRAPH
void graph(inout ModelInput model) { }
#endif

bool mx_isNaN(highp float val)
{
    return ( val < 0.0 || 0.0 < val || val == 0.0 ) ? false : true;
}

bool mx_isInf(highp float val)
{
    return val > 65504.0;
}

bool mx_isDenormal(highp float val)
{
    return (val > 0.0 && val < 6.10352E-5) || (val < 0.0 && val > -6.10352E-5);
}

void main()
{
    #ifdef MX_REQUIRE_COLOR_FRAGMENT
        mx_Color = vColor;
    #endif

    #ifdef MX_REQUIRE_POSITION_FRAGMENT
        mx_Position = vPosition;
    #endif

    #ifdef MX_REQUIRE_EYE_FRAGMENT
        mx_EyePosition = eyePos;
    #endif

    #ifdef MX_REQUIRE_NORMAL_FRAGMENT
        mx_Normal = normalize(vNormal);
    #endif
    #ifdef MX_REQUIRE_TANGENTS_FRAGMENT
        mx_Tangent = normalize(vTangent);
        mx_Binormal = normalize(vBinormal);
    #endif

    #ifdef MX_REQUIRE_TANGENT_TO_WORLD_FRAGMENT
        mx_TangentToWorld = mat3(vTangent, vBinormal, vNormal);
    #endif

    #ifdef MX_REQUIRE_TIME_FRAGMENT
        mx_Time = time;
    #endif

    #ifdef MX_REQUIRE_UV0
        mx_UV0 = vUV0;
    #endif
    #ifdef MX_REQUIRE_UV1
        mx_UV1 = vUV1;
    #endif
    #ifdef MX_REQUIRE_UV2
        mx_UV2 = vUV2;
    #endif
    #ifdef MX_REQUIRE_UV3
        mx_UV3 = vUV3;
    #endif

    #ifdef MX_DEBUG
        mx_Debug = vDebug;
    #endif    
    ModelInput inputs;
    graph(inputs);

    mediump vec4 color = model(inputs);

    #ifdef MX_DEBUG
    color = color * 0.0001 + mx_Debug;
    #endif

    #ifdef MX_SHOW_LIMITS
    if(mx_isNaN(color.r) || mx_isNaN(color.g) || mx_isNaN(color.b) || mx_isNaN(color.a))
    {
        mediump float blink = (int(time*2.0) % 2) == 0 ? 1.0 : 0.0;
        color = vec4(blink, 0.0, blink, 1.0);
    }
    if(mx_isInf(color.r) || mx_isInf(color.g) || mx_isInf(color.b) || mx_isInf(color.a))
    {
        mediump float blink = (int(time*2.0) % 2) == 0 ? 1.0 : 0.0;
        color = vec4(vec3(blink),1.0);
    }
    //if(mx_isDenormal(color.r) || mx_isDenormal(color.g) || mx_isDenormal(color.b) || mx_isDenormal(color.a))
    //{
    //    mediump float blink = (int(time*2.0) % 2) == 0 ? 1.0 : 0.0;
    //    color = vec4(0.0, 0.0, blink, 1.0);
    //}
    #endif
    

	gl_FragColor = color;
}



#ifdef FOG
uniform mediump vec4 global_fogNearColor;
uniform mediump vec4 global_fogFarColor;
uniform highp float global_fogNearDistance;
uniform highp float global_fogFarDistance;
uniform mediump float global_fogIntensity;

mediump vec4 ComputeFogColor(highp float dist)
{
	mediump float fog = (dist - global_fogNearDistance) / (global_fogFarDistance - global_fogNearDistance + 0.0001);
	fog = smoothstep(0.0, 1.0, fog);
	mediump vec4 result = mix(global_fogNearColor, global_fogFarColor, fog);
    result.rgb *= global_fogIntensity;
    return result;
}
#endif

#ifdef FOG
mediump vec3 ApplyFog(mediump vec3 color, mediump vec4 fogColor)
{
    return mix(color, fogColor.rgb, clamp(fogColor.a, 0.0, 1.0));
}
#endif


uniform mediump float irradianceArray [27];
mediump vec3 irradiance(mediump vec3 normal3)
{

    mediump vec4 cAr = vec4(irradianceArray[0],  irradianceArray[1],  irradianceArray[2],  irradianceArray[3] );
    mediump vec4 cAg = vec4(irradianceArray[4],  irradianceArray[5],  irradianceArray[6],  irradianceArray[7] );
    mediump vec4 cAb = vec4(irradianceArray[8],  irradianceArray[9],  irradianceArray[10], irradianceArray[11]);
    mediump vec4 cBr = vec4(irradianceArray[12], irradianceArray[13], irradianceArray[14], irradianceArray[15]);
    mediump vec4 cBg = vec4(irradianceArray[16], irradianceArray[17], irradianceArray[18], irradianceArray[19]);
    mediump vec4 cBb = vec4(irradianceArray[20], irradianceArray[21], irradianceArray[22], irradianceArray[23]);
    mediump vec3 cC =  vec3(irradianceArray[24], irradianceArray[25], irradianceArray[26]);

    mediump vec4 normal = vec4(normal3, 1.0);
    //normal.z = -normal.z;

    mediump vec3 x1, x2, x3;
    
    // Linear + constant polynomial terms
    x1.r = dot(cAr,normal);
    x1.g = dot(cAg,normal); 
    x1.b = dot(cAb,normal);
    
    // 4 of the quadratic polynomials
    mediump vec4 vB = normal.xyzz * normal.yzzx;   
    x2.r = dot(cBr,vB);
    x2.g = dot(cBg,vB);
    x2.b = dot(cBb,vB);
    
    // Final quadratic polynomial
    mediump float vC = normal.x*normal.x - normal.y*normal.y;
    x3 = cC.rgb * vC;    
    mediump vec3 irr = x1 + x2 + x3;
    
    #if defined(EFFECT_EDITOR) || defined(GLITCH_EDITOR)
        irr = irr * 0.0001 + vec3(1.);        		
    #endif
    
    return irr;
}

mediump vec3 irradianceL0()
{
    return vec3(
        irradianceArray[3] + irradianceArray[14] * 1.0 / 3.0,
        irradianceArray[7] + irradianceArray[18] * 1.0 / 3.0,
        irradianceArray[11] + irradianceArray[22] * 1.0 / 3.0);
}

uniform mediump sampler2D global_brdfTable;

#ifdef EDITOR
uniform mediump float global_envMapGen;
#endif

#ifdef IBL_LOCAL_ENV_MAP
	uniform highp mat4 IBLWorldToProxyBox;
	uniform highp vec3 IBLEnvMapPos;
	
	uniform mediump samplerCube IBLEnvMap;
	//uniform mediump float IBLEnvMapMaxMipLevel;
    uniform mediump vec3 IBLEnvMapNormFactor;
	
	uniform lowp int IBLBoxProjection;
	
	highp vec3 BoxProjection(mediump vec3 reflVec)
	{
		highp vec3 RayLS = mat3(IBLWorldToProxyBox) * reflVec;
		highp vec3 PositionLS = (IBLWorldToProxyBox * vec4(vPosition, 1.)).xyz;
			
		highp vec3 Unitary = vec3(1.0, 1.0, 1.0);
		highp vec3 FirstPlaneIntersect  = (Unitary - PositionLS) / RayLS;
		highp vec3 SecondPlaneIntersect = (-Unitary - PositionLS) / RayLS;
		highp vec3 FurthestPlane = max(FirstPlaneIntersect, SecondPlaneIntersect);
		highp float Distance = min(FurthestPlane.x, min(FurthestPlane.y, FurthestPlane.z));
			
		// Use Distance in WS directly to recover intersection
		highp vec3 IntersectPositionWS = vPosition + reflVec * Distance;
		return IntersectPositionWS - IBLEnvMapPos;
	}
#else
	uniform mediump samplerCube global_envMap;
	//uniform mediump float global_envMapMaxMipLevel;
    uniform mediump vec3 global_envMapNormFactor;
#endif

mediump float RoughnessToMipLevel(mediump float roughness, mediump float mipcount)
{
    return log2(roughness) + mipcount;
}

mediump vec3 SampleEnvMap_RGBDiv8(mediump samplerCube envMap, mediump vec3 dir, mediump float level)
{
    mediump vec4 f = textureCubeLod(envMap, dir, level);
    f.rgb = f.rgb / max(1. / 256., f.a);
    return f.rgb;
}

// Forward declaration for the environment map sample function. The model must provide the body. See below for built-in implementations.
mediump vec3 IBLSampleEnvMap(mediump samplerCube envMap, mediump vec3 dir, mediump float roughness);

mediump vec3 IBLSampleEnvMap_Standard(mediump samplerCube envMap, mediump vec3 dir, mediump float roughness)
{
    return SampleEnvMap_RGBDiv8(envMap, dir, RoughnessToMipLevel(roughness, 4.0));
}

mediump vec3 IBLReflectivity(mediump float VoN, mediump float roughness, mediump vec3 f0)
{
    mediump vec2 brdf = texture2D(global_brdfTable, vec2(VoN, roughness)).rg;
    mediump vec3 reflectivity = brdf.r * f0 + vec3(brdf.g);
    return reflectivity;
}

mediump vec3 SampleEnvMap(mediump vec3 R, mediump float roughness)
{
    #ifdef IBL_LOCAL_ENV_MAP
        return IBLSampleEnvMap(IBLEnvMap, IBLBoxProjection != 0 ? BoxProjection(R) : R, roughness);
    #else
        return IBLSampleEnvMap(global_envMap, R, roughness);
    #endif
}

mediump vec3 IBLSample(mediump vec3 R, mediump float roughness)
{
    #ifdef EDITOR
        if(global_envMapGen != 0.0)
        {
            return vec3(1.0);
        }
    #endif
    return SampleEnvMap(R, roughness);
}

mediump vec3 IBLNormalizedSample(mediump vec3 R, mediump float roughness)
{
    #ifdef EDITOR
        if(global_envMapGen != 0.0)
        {
            return vec3(1.0);
		}
    #endif
    return SampleEnvMap(R, roughness) * dot(global_envMapNormFactor, cLumWeights);
}
#ifdef LIGHTMAP
    uniform mediump sampler2D lightMap;
    #ifdef SPLIT_ALPHA
        uniform mediump sampler2D lightMap_alpha;
    #endif
    #ifdef EDITOR_LIGHTMAP_UV
        uniform highp float lightMap_width;
    #endif
    #ifdef EDITOR
        uniform bool global_realtimeRadiosity;
        uniform mediump vec3 global_dominantLightDir;
    #endif
    uniform mediump vec3 global_dominantLightIntensity;
#endif
#ifdef LIGHTMAP_DIR
    uniform mediump sampler2D lightMapDir;
    #ifdef SPLIT_ALPHA
        uniform mediump sampler2D lightMapDir_alpha;
    #endif
#endif

#ifdef LIGHTMAP_DIR
    mediump float RelightSurface(mediump vec3 direction, mediump vec3 radiosityNormal, mediump vec3 normal)
    {
        direction += direction - radiosityNormal * dot(radiosityNormal, direction);
        direction *= inversesqrt(dot(direction, direction) + 0.0001);

        // ----- relighting normal
        // compute dot product with computed centres of distributions
        mediump float radNormDotVI = dot(radiosityNormal, direction);
        mediump float lambda = min(radNormDotVI + 1.0, 1.0);
        mediump float q = radNormDotVI * 0.5 + 0.5;
        mediump float q2 = q * q;

        const mediump float c = 10.0 / 3.0;
        mediump float radNormVal = lambda * (c * q2 * (2.0 * q - q2) - 1.0) + 1.0;

        // compute dot product with computed centres of distributions
        mediump float shadingNormDotVI = dot(normal, direction);
        q = shadingNormDotVI * 0.5 + 0.5;
        q2 = q * q;
        mediump float shadingNormVal = lambda * (c * q2 * (2.0 * q - q2) - 1.0) + 1.0;
	    
        return shadingNormVal / radNormVal;
    }
#endif

mediump vec3 LightmapIrradiance()
{
    #ifdef LIGHTMAP
        mediump vec4 lm = texture2D(lightMap, vUV1);

        #ifdef SPLIT_ALPHA
            lm.a = texture2D(lightMap_alpha, vUV1).r;
        #endif
        
        return lm.rgb / max(1. / 256., lm.a);
    #else
        return vec3(1.0);
    #endif
}

mediump vec4 LightmapDirectional()
{
    #if defined LIGHTMAP_DIR
        mediump vec4 lmDir = texture2D(lightMapDir, vUV1);

        #ifdef SPLIT_ALPHA
            lmDir.a = texture2D(lightMapDir_alpha, vUV1).r;
        #endif

        return lmDir;
    #else
        return vec4(0.0);
    #endif
}

mediump vec3 LightmapDiffuse(mediump vec3 normal, mediump vec3 radiosityNormal, mediump vec3 diffuseColor)
{
    mediump vec3 diffuseLight = LightmapIrradiance();

    #if defined LIGHTMAP_DIR && !defined LOW_END
        mediump vec4 lmDir = LightmapDirectional();

        #ifdef USE_SHADOW_MAP
            lmDir.a *= 1.0 - GetShadowCoefficient();
        #endif

        diffuseLight += lmDir.a * global_dominantLightIntensity;

        mediump vec3 localLightDir = lmDir.xyz * 2.0 - 1.0;

        mediump float H = 0.5 - dot(radiosityNormal, normal) * 0.5;
        diffuseLight *= (H * diffuseColor + (1. - H) * RelightSurface(localLightDir, radiosityNormal, normal) * vec3(1.0));
    #endif

    return diffuseLight;
}

#ifndef Pi 
    #define Pi 3.1415926
#endif

mediump float D_Blinn(mediump float roughness, mediump float NoH)
{
    mediump float a = roughness;
    mediump float a2 = a * a;
    mediump float power = 2.0 / a2 - 2.0;
    return (power + 2.0) / (2.0 * Pi) * pow(NoH, power);
}

mediump float D_Beckmann(mediump float roughness, mediump float NoH)
{
    mediump float a = roughness;
    mediump float a2 = a * a;
    mediump float NoH2 = NoH * NoH;
    return exp((NoH2 - 1.0) / (a2 * NoH2)) / (Pi * a2 * NoH2 * NoH2);
}

mediump float D_GGX(mediump float roughness, mediump float NoH)
{
    mediump float a = roughness;
    mediump float a2 = a * a;
    highp float d = (NoH * a2 - NoH) * NoH + 1.0;
    d =  max(d, 0.0001);
    return a2 / (Pi * d * d);
}

mediump float D_GGX_Clamped(mediump float roughness, mediump float NoH)
{
    mediump float a = roughness;
    mediump float a2 = a * a;
    highp float d = (NoH * NoH) * (a2 - 1.0) + 1.0;
    d =  max(d, 0.0001);
    return min(a2 / (Pi * d * d), 65000.0);
}

mediump float D_GGX_Cross(mediump float roughness, mediump float NoH, mediump vec3 NxH)
{
    mediump float oneMinusNoHSquared = dot(NxH, NxH);

    mediump float a = NoH * roughness ;
    mediump float k = roughness / (oneMinusNoHSquared + a * a);
    mediump float d = k * k * (1.0 / Pi);
    return min(d, 500.0);
}


mediump float G_Schlick(mediump float roughness, mediump float NoV, mediump float NoL)
{
    mediump float a = roughness;
    mediump float k = a * 0.8;//a * a;
    mediump float Gv = NoV / ( NoV * (1.0 - k) + k );
    mediump float Gl = NoL / ( NoL * (1.0 - k) + k );
    return Gv * Gl;
}

mediump float G_Schlick_Unreal(mediump float roughness, mediump float NoV, mediump float NoL)
{
    mediump float a = (sqrt(roughness)+1.0);
    mediump float k = (a * a) / 8.0;
    mediump float Gv = NoV / ( NoV * (1.0 - k) + k );
    mediump float Gl = NoL / ( NoL * (1.0 - k) + k );
    return Gv * Gl;
}

mediump float G_Schlick_Normalized(mediump float roughness, mediump float NoV, mediump float NoL)
{
    mediump float a = roughness * 0.5;
    mediump float k = a;// * a;
    mediump float Gv = ( NoV * (1.0 - k) + k );
    mediump float Gl = ( NoL * (1.0 - k) + k );
    return 0.25 / (Gv * Gl);
}

mediump float G_Implicit(mediump float roughness, mediump float NoV, mediump float NoL)
{
    return NoV * NoL;
}

mediump vec3 F_Schlick_Gauss(mediump vec3 f0, mediump float VoH)
{
	return f0 + (1.0 - f0) * exp2( (-5.55473 * VoH - 6.98316) * VoH);
}

mediump vec3 F_Schlick(mediump vec3 f0, mediump float product)
{
    mediump float x = 1.0001 - product;
    mediump float x2 = x * x;
    return f0 + (1.0 - f0) * x2 * x2 * x;
}



mediump vec3 SpecularBrdf(mediump vec3 F, mediump float NoL, mediump float NoV, mediump float NoH, mediump float VoH, mediump float roughness, mediump vec3 normal, mediump vec3 H)
{ 
    mediump vec3 NxH = cross(normal, H);
    mediump float D = D_GGX_Clamped(roughness, NoH);
    mediump float G = G_Implicit(roughness, NoV, NoL);

    //mediump float rim = mix(1.0 - roughness * 0.9, 1.0, NoV);
    return vec3( G * D * F) / (4.0 * NoV); 
}

mediump float SpecularPhong(mediump vec3 V, mediump vec3 L, mediump vec3 N, mediump float roughness)
{
    mediump vec3 R = reflect(-L, N);
    mediump float spec = max(0.0, dot(V, R));

    mediump float k = 1.999 / (roughness * roughness);

    return min(1.0, 3.0 * 0.0398 * k) * pow(spec, min(10000.0, k));
}

mediump float SpecularBlinn(mediump float NoH, mediump float roughness)
{
    mediump float k = 1.999 / (roughness * roughness);
    
    return min(1.0, 3.0 * 0.0398 * k) * pow(NoH, min(10000.0, k));
}

/*
#if defined LIGHT_COUNT_8 && !defined LIGHT_COUNT_7
#define LIGHT_COUNT_7
#endif

#if defined LIGHT_COUNT_7 && !defined LIGHT_COUNT_6
#define LIGHT_COUNT_6
#endif

#if defined LIGHT_COUNT_6 && !defined LIGHT_COUNT_5
#define LIGHT_COUNT_5
#endif

#if defined LIGHT_COUNT_5 && !defined LIGHT_COUNT_4
#define LIGHT_COUNT_4
#endif*/

#if defined LIGHT_COUNT_4 && !defined LIGHT_COUNT_3
#define LIGHT_COUNT_3
#endif

#if defined LIGHT_COUNT_3 && !defined LIGHT_COUNT_2
#define LIGHT_COUNT_2
#endif

#if defined LIGHT_COUNT_2 && !defined LIGHT_COUNT_1
#define LIGHT_COUNT_1
#endif

#if defined LIGHT_COUNT_1
#define DYNAMIC_LIGHTING
#endif

struct LightParameters
{
    mediump float Type;
    mediump vec3  Color;
    highp   vec3  Position;
    mediump vec3  Direction;
    mediump float PointRadius;
    mediump vec2  Cone;
    mediump float FalloffRadius;
};

struct LightResult
{
    mediump vec3 Diffuse;
    mediump vec3 Specular;
};

struct LightSurface 
{
    mediump vec3  Position;
    mediump vec3  F0;
    mediump float Roughness;
    mediump vec3  Normal;
};

// Forward declaration for light function. The model must provide the body. See below for built-in implementations.
LightResult LightFunction(LightSurface surface, mediump vec3 V, mediump float VoN, mediump vec3 L, LightParameters light);

LightResult StandardLightFunction(LightSurface surface, mediump vec3 V, mediump float VoN, mediump vec3 L, LightParameters light)
{
    mediump float NoL = max(0.001, dot(surface.Normal, L));

    mediump vec3  H   = normalize(L + V);
    mediump float NoH = max(0.0, dot(surface.Normal, H));
    mediump float VoH = max(0.0, dot(V, H));

    mediump vec3 F = F_Schlick_Gauss(surface.F0, VoH);
    mediump vec3 specular = Pi * SpecularBrdf(F, NoL, VoN, NoH, VoH, surface.Roughness, surface.Normal, H) * light.Color;
    mediump vec3 diffuse = NoL * light.Color * (1.0 - F);// * (1.0 / Pi); 

    LightResult result;
    result.Specular = specular;
    result.Diffuse = diffuse;
    return result;
}

LightResult BasicLightFunction(LightSurface surface, mediump vec3 V, mediump float VoN, mediump vec3 L, LightParameters light)
{
    mediump float NoL = max(0.001, dot(surface.Normal, L));
    mediump vec3 diffuse = NoL * light.Color; // * (1.0 / Pi); 

    LightResult result;
    result.Specular = vec3(0.0);
    result.Diffuse = diffuse;
    return result;
}

#ifdef DYNAMIC_LIGHTING

#define LIGHT_TYPE_POINT 1.0
#define LIGHT_TYPE_SPOT 2.0
#define LIGHT_TYPE_DIRECTIONAL 3.0

#define LIGHT_COUNT 4

uniform mediump float global_lightType[LIGHT_COUNT];
uniform mediump float global_lightIntensity[LIGHT_COUNT];
uniform mediump vec3  global_lightColor[LIGHT_COUNT];
uniform highp   vec3  global_lightPosition[LIGHT_COUNT];
uniform mediump vec3  global_lightDirection[LIGHT_COUNT];
uniform mediump float global_lightPointRadius[LIGHT_COUNT];
uniform mediump vec2  global_lightCone[LIGHT_COUNT];
uniform mediump float global_lightFalloffRadius[LIGHT_COUNT];

LightResult DirectionalLight(LightSurface surface, mediump vec3 V, mediump float VoN, mediump vec3 L, LightParameters light)
{
    return LightFunction(surface, V, VoN, L, light);
}

LightResult PointLight(LightSurface surface, mediump vec3 V, mediump float VoN, LightParameters light)
{
    highp vec3 D = light.Position - surface.Position;
    mediump vec3 L = normalize(D);

    highp float D2 = dot(D, D);
    mediump float falloff = clamp((1.0 - D2 / (light.FalloffRadius * light.FalloffRadius)) / (1.0 + D2), 0.0, 1.0);

    #ifdef LIGHT_RADIUS
        highp vec3 reflected = reflect(V,surface.Normal);
        highp vec3 centerToRay = D - dot(D, reflected) * reflected;
        highp vec3 closest = D - centerToRay * clamp(light.PointRadius/length(centerToRay),0.0,1.0);
        L = normalize(closest);
    #endif

    light.Color *= falloff;

	return LightFunction(surface, V, VoN, L, light);
}

LightResult SpotLight(LightSurface surface, mediump vec3 V, mediump float VoN, LightParameters light)
{
    highp vec3 D = light.Position - surface.Position;
    mediump vec3 L = normalize(D);

    highp float D2 = dot(D, D);
    mediump float falloff = clamp((1.0 - D2 / (light.FalloffRadius * light.FalloffRadius)) / (1.0 + D2), 0.0, 1.0);

    mediump float LoD = dot(L,-light.Direction);
    mediump float coneFalloff = (LoD-light.Cone.y)/(light.Cone.x-light.Cone.y);  
    falloff *= smoothstep(0.0,1.0,coneFalloff);

    light.Color *= falloff;

	return LightFunction(surface, V, VoN, L, light);
}

LightResult AnyLight(LightSurface surface, mediump vec3 V, mediump float VoN, LightParameters light)
{
    if(light.Type == LIGHT_TYPE_POINT)
    {
        return PointLight(surface, V, VoN, light);
    }
    else if(light.Type == LIGHT_TYPE_SPOT)
    {
        return SpotLight(surface, V, VoN, light);
    }
    else if(light.Type == LIGHT_TYPE_DIRECTIONAL)
    {
        return DirectionalLight(surface, V, VoN, -light.Direction, light);
    }
    else
    {
        LightResult result;
        result.Diffuse = vec3(0.0);
        result.Specular = vec3(0.0);
        return result;
    }
}

void IndexedLight(LightSurface surface, mediump vec3 V, mediump float VoN, int index, inout LightResult result)
{
    LightParameters light;
    light.Type = global_lightType[index];
    light.Position = global_lightPosition[index];
    light.Direction = global_lightDirection[index];
    light.Color = global_lightColor[index] * global_lightIntensity[index];
    light.Cone = global_lightCone[index];
    light.PointRadius = global_lightPointRadius[index];
    light.FalloffRadius = global_lightFalloffRadius[index];

    LightResult lightResult = AnyLight(surface, V, VoN, light);


    #ifdef USE_SHADOW_MAP
        if(index==0)
        {
            mediump float shadow = 1.0 - GetShadowCoefficient();
            lightResult.Diffuse *= shadow;
            lightResult.Specular *= shadow;
        }
    #endif

    result.Diffuse += lightResult.Diffuse;
    result.Specular += lightResult.Specular;
}

LightResult AllLights(LightSurface surface, mediump vec3 V, mediump float VoN)
{
    LightResult result;
    result.Diffuse = vec3(0.0);
    result.Specular = vec3(0.0);

#ifdef LIGHT_COUNT_1
    IndexedLight(surface, V, VoN, 0, result);
#endif
#ifdef LIGHT_COUNT_2
    IndexedLight(surface, V, VoN, 1, result);
#endif
#ifdef LIGHT_COUNT_3
    IndexedLight(surface, V, VoN, 2, result);
#endif
#ifdef LIGHT_COUNT_4
    IndexedLight(surface, V, VoN, 3, result);
#endif

    return result;
}

#endif // DYNAMIC_LIGHTING
// sRGB transfert function
// https://fr.wikipedia.org/wiki/SRGB
// http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
mediump vec3 SRGBToLinear(mediump vec3 c)
{
#if defined USE_REFERENCE_SRGB
    mediump vec3 linearRGBLo = c / 12.92;
    mediump vec3 linearRGBHi = pow((c + 0.055) / 1.055, vec3(2.4));
    mediump vec3 linearRGB = vec3(
        (c.x <= 0.04045) ? linearRGBLo.x : linearRGBHi.x,
        (c.y <= 0.04045) ? linearRGBLo.y : linearRGBHi.y,
        (c.z <= 0.04045) ? linearRGBLo.z : linearRGBHi.z);
    return linearRGB;
#else
    return c * (c * (c * 0.305306011 + 0.682171111) + 0.012522878);
#endif
}

mediump vec3 LinearToSRGB(mediump vec3 c)
{
#if defined USE_REFERENCE_SRGB
    mediump vec3 sRGBLo = c * 12.92;
    mediump vec3 sRGBHi = (pow(c, vec3(1.0 / 2.4)) * 1.055) - 0.055;
    mediump vec3 sRGB = vec3(
        (c.x <= 0.0031308) ? sRGBLo.x : sRGBHi.x,
        (c.y <= 0.0031308) ? sRGBLo.y : sRGBHi.y,
        (c.z <= 0.0031308) ? sRGBLo.z : sRGBHi.z);
    return sRGB;
#else
    return max(1.055 * pow(c, vec3(0.416666667)) - 0.055, 0.0);
#endif
}

// Relative luminance: https://en.wikipedia.org/wiki/Relative_luminance
mediump float Luminance(mediump vec3 linearRgb)
{
    return dot(linearRgb, vec3(0.2126729, 0.7151522, 0.0721750));
}

LightResult LightFunction(LightSurface surface, mediump vec3 V, mediump float VoN, mediump vec3 L, LightParameters light)
{
#if !defined LOW_END
    return StandardLightFunction(surface, V, VoN, L, light);
#else
    return BasicLightFunction(surface, V, VoN, L, light);
#endif
}

mediump vec3 IBLSampleEnvMap(mediump samplerCube envMap, mediump vec3 dir, mediump float roughness)
{
#if !defined LOW_END
    return IBLSampleEnvMap_Standard(envMap, dir, roughness);
#else
    // Use hardcoded roughness to avoid performance issue with textureCubeLod on some PowerVR architectures (e.g. A8 and below).
    return IBLSampleEnvMap_Standard(envMap, dir, 0.5);
#endif
}

#ifdef BASE_REFLECTIVITY
    uniform mediump vec3 baseReflectivity;
#else
    const mediump vec3 baseReflectivity = vec3(0.04, 0.04, 0.04);
#endif

mediump vec4 model(ModelInput inputs)
{

    mediump vec3 baseColor = inputs.BaseColor;
    mediump vec3 diffuseBaseColor = baseColor;
    mediump vec3 normal = inputs.Normal;

    mediump vec3 V = normalize(mx_EyePosition - mx_Position);
    mediump float VoN = clamp(dot(normal, V), 0.001, 1.0);

    mediump float roughness = inputs.Roughness;
    mediump float occlusion = inputs.Occlusion;
    mediump float metalness = inputs.Metalness;
    mediump vec3 emissivity = inputs.Emissivity;
    mediump float opacity = inputs.Opacity;

    // Reflectivity
	mediump vec3 f0 = mix(baseReflectivity, baseColor, metalness);

    mediump vec3 diffuseColor = diffuseBaseColor * (1.0 - metalness);

    // Diffuse
    mediump vec3 diffuseLight = vec3(0.0);
	#if defined LIGHTMAP
        diffuseLight += LightmapDiffuse(normal, mx_Normal, diffuseBaseColor);
	#elif defined IRRADIANCE
		diffuseLight += irradiance(normal);
	#endif

	// Specular
	mediump vec3 specular = vec3(0.0);

    #ifdef SPECULAR_IBL
        mediump vec3 reflectivity = IBLReflectivity(VoN, roughness, f0);
        mediump vec3 R = reflect(-V, normal);

        mediump vec3 ambient;
        #ifdef IRRADIANCE
            ambient = irradianceL0();
        #else
            ambient = diffuseLight;
        #endif

        // Energy conservation between diffuse and specular BRDFs
        diffuseLight *= (1.0 - reflectivity);

        specular += IBLNormalizedSample(R, roughness) * reflectivity * dot(ambient, cLumWeights);
	#endif
    // Dynamic lighting
    #ifdef DYNAMIC_LIGHTING
        roughness = max(0.001, roughness);

	    LightSurface surface;
        surface.Normal = normal;
        surface.F0 = f0;
        surface.Roughness = roughness * roughness;
        surface.Position = vPosition;

        LightResult lightResult = AllLights(surface, V, VoN);
        diffuseLight += lightResult.Diffuse;
        specular += lightResult.Specular;
    #endif

	#ifdef TRANSPARENT
		diffuseLight *= opacity;
	#endif
	
	// Combining diffuse and specular terms
	mediump vec3 color = diffuseColor.rgb * diffuseLight + specular;

	#if defined USE_SHADOW_MAP
		color *= mix(vec3(1.0), global_ShadowColor, GetShadowCoefficient());
	#endif
	
    // Occlusion
    color *= occlusion;
	
    // Emissivity
    color += emissivity;

    #ifdef FOG
		color = ApplyFog(color, ComputeFogColor(length(mx_EyePosition - mx_Position)));
	#endif

	#ifdef OUTLINE
		color = vOutlineColor.rgb;
		opacity = vOutlineColor.a;
	#endif

	#ifdef CANCEL_EXPOSURE
		color = CancelExposure(color);
	#endif

#ifdef EDITOR
#if defined EDITOR_NORMAL_NORMALIZED
	color = normal * 0.5 + 0.5;
	opacity = 1.0;
#elif defined EDITOR_NORMAL_POSITIVE
	color = normal;
	color = clamp(color, 0.0, 1.0);
	opacity = 1.0;
#elif defined EDITOR_NORMAL_NEGATIVE
	color = -normal;
	color = clamp(color, 0.0, 1.0);
	opacity = 1.0;
#elif defined EDITOR_BASECOLOR
	color = diffuseBaseColor;
	opacity = 1.0;
#elif defined EDITOR_ROUGHNESS
	color = vec3(roughness);
	opacity = 1.0;
#elif defined EDITOR_OCCLUSION
	color = vec3(occlusion);
	opacity = 1.0;
#elif defined EDITOR_DIFFUSE
	color = vec3(diffuseLight * diffuseColor);
	opacity = 1.0;
#elif defined EDITOR_SPECULAR
	color = vec3(specular);
	opacity = 1.0;
#elif defined EDITOR_METALNESS
	color = vec3(metalness);
	opacity = 1.0;
#elif defined EDITOR_VERTEX_COLOR
    color = mx_Color.rgb;
    opacity = 1.0;
#elif defined EDITOR_VERTEX_COLOR_ALPHA
    color = vec3(mx_Color.a);
    opacity = 1.0;
#elif defined EDITOR_BASE_UV
	color =  vec3(fract(vUV0),0.0);
	opacity = 1.0;
#elif defined EDITOR_LIGHTMAP_UV
	highp float _f = mod(dot(floor(vUV1.xy * lightMap_width), vec2(1.0)),2.0);
	lowp vec3 _tileColor = (_f * 0.65 + (1.0 - _f)) * vec3(fract(vUV1.xy), 0.0);
	color = (LightmapIrradiance() * 0.75 + vec3(0.25)) * _tileColor;
	opacity = 1.0;
#elif defined EDITOR_LIGHTMAP
	color = LightmapIrradiance();
	opacity = 1.0;
#elif defined EDITOR_LIGHTMAP_DIRECTIONAL
    color = LightmapDirectional().xyz;
	opacity = 1.0;
#elif defined TRIVIAL_FS
	color = vec3(0.0);
	opacity = 1.0;
#elif defined ALBEDO_OUT
    color = diffuseBaseColor.rgb;
    opacity = 1.0;
#elif defined EMISSIVE_OUT
	color = emissivity;
	opacity= 1.0;
#elif defined EDITOR_WIREFRAME
	highp float _d = clamp(length(eyePos - vPosition) / 30.0, 0.0, 1.0);
	color = vec3(0.8,0.8,0.8);
	opacity= mix(0.9, 0.1, _d);
#elif defined EDITOR_MIP_BASE
#elif defined EDITOR_MIP_BASE_LINEAR
#elif defined EDITOR_DENSITY_WORLD
#elif defined EDITOR_DENSITY_SCREEN
#elif defined EDITOR_DEPTH    
#endif
#endif

	#ifdef GAMMA_OUT
		color = LinearToSRGB(color);
	#endif

    return vec4(color, opacity);
}




#ifdef FLAT

    uniform lowp vec4 baseColor;
    uniform mediump sampler2D NormalMap;
    uniform mediump float roughness;
    uniform mediump float Metalness;
    uniform mediump vec3  Emissivity;

#else

    uniform lowp sampler2D BaseColor;
    uniform mediump sampler2D NormalMap;
    uniform lowp sampler2D Rome; 
    uniform highp float EmissiveIntensity;

#endif

void graph(inout ModelInput model) 
{

    #ifdef FLAT

        model.Normal = normalize(vNormal);

        model.BaseColor = baseColor.rgb * mx_Color.rgb;
        model.Opacity = baseColor.a * mx_Color.a;       

        model.Roughness = roughness; 
        model.Occlusion = 1.0; 
        model.Metalness = Metalness; 
        model.Emissivity = Emissivity;

    #else

        lowp vec3 normalMap = texture(NormalMap, vUV0).rgb;
        model.Normal = normalize(mx_TangentToWorld * (normalMap * 2.0 - 1.0));

        lowp vec4 baseColor = texture(BaseColor, vUV0);
        model.BaseColor = baseColor.rgb * mx_Color.rgb;
        model.Opacity = baseColor.a * mx_Color.a;

        lowp vec4 rome = texture(Rome, vUV0);
        model.Roughness = rome.r; 
        model.Occlusion = rome.g; 
        model.Metalness = rome.b; 
        model.Emissivity = rome.a * EmissiveIntensity * baseColor.rgb;

    #endif
}
