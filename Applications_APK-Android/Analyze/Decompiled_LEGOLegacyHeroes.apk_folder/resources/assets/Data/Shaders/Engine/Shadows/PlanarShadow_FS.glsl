
uniform highp mat4 World;

//========================================================================================
//     ____ ___  ______  ______   _  __
//    |_  // _ \/ __/  |/  / _ | | |/_/
//   _/_ </ // /\ \/ /|_/ / __ |_>  <  
//  /____/____/___/_/  /_/_/ |_/_/|_| 

#ifdef DCC_MAX
	#define SEM(x) : x
	#define VERTEX_COLOR_TYPE vec3
#else
	#define SEM(x)
	#define VERTEX_COLOR_TYPE vec4
#endif


//========================================================================================
//    _________  __  _____  _______  _  __     _______________
//   / ___/ __ \/  |/  /  |/  / __ \/ |/ /    / __/ ___/_  __/
//  / /__/ /_/ / /|_/ / /|_/ / /_/ /    /    / _// /__  / /   
//  \___/\____/_/  /_/_/  /_/\____/_/|_/    /_/  \___/ /_/  

lowp mat3 OS2TS(lowp vec3 t, lowp vec3 b, lowp vec3 n)
{	
	#ifdef DCC_MAX
	// 3DSMax TBN is in a left-hand coord.sys, so we interchange T and B and flip B to get a right-hand coord.sys
	// This practice makes the TB match the UV direction and Glitch output at the same time
	lowp vec3 temp = b;
	b = -t;
	t = temp;
	#endif

	return mat3(t.x, b.x, n.x,
				t.y, b.y, n.y,
				t.z, b.z, n.z);
}

lowp mat3 TS2OS(lowp vec3 t, lowp vec3 b, lowp vec3 n)
{	
	#ifdef DCC_MAX
	lowp vec3 temp = b;
	b = -t;
	t = temp;
	#endif

	return mat3(t.x, t.y, t.z,
				b.x, b.y, b.z,
				n.x, n.y, n.z);
}


mediump vec3 pack(mediump vec3 v){ return v * .5 + .5; }
mediump vec4 pack(mediump vec4 v){ return v * .5 + .5; }
mediump vec3 unpack(mediump vec3 v){ return v * 2. - 1.; }
mediump vec4 unpack(mediump vec4 v){ return v * 2. - 1.; }

//========================================================================================
//    _________  _  _______________   _  ____________
//   / ___/ __ \/ |/ / __/_  __/ _ | / |/ /_  __/ __/
//  / /__/ /_/ /    /\ \  / / / __ |/    / / / _\ \  
//  \___/\____/_/|_/___/ /_/ /_/ |_/_/|_/ /_/ /___/  

#define TWOPI	6.283185307179586476925286766559
#define PI		3.1415926535897932384626433832795
#define HLFPI	1.5707963267948966192313216916398

const lowp float GlobalLightmapFactor = 1.5;


#ifdef USE_MATCAP
//========================================================================================
//     __  ______ ______________   ___ 
//    /  |/  / _ /_  __/ ___/ _ | / _ \
//   / /|_/ / __ |/ / / /__/ __ |/ ___/
//  /_/  /_/_/ |_/_/  \___/_/ |_/_/ 

// Matcap
lowp vec2 getMatcapUV(highp mat4 WorldView, lowp vec3 normal)
{
	normal = normalize((WorldView * vec4(normal,0.)).xyz);
	normal = pack(normal);
	normal.y = 1. - normal.y; // texfetch correction	
	return normal.xy;
}

#ifdef USE_NORMALMAP
// Matcap with normal map
lowp vec2 getMatcapUVWithNormalMap(highp mat4 WorldView, lowp vec3 normal, lowp mat3 mTS2OS, lowp vec4 tNormal)
{
	tNormal = unpack(tNormal);
	lowp vec3 norm = normal.xyz + mTS2OS * tNormal.xyz;	
	return getMatcapUV(WorldView, norm);
}	
#endif
#endif


//========================================================================================
//     ___  ______   _  _____  _____  _______
//    / _ )/ __/ /  / |/ / _ \/  _/ |/ / ___/
//   / _  / _// /__/    / // // //    / (_ / 
//  /____/___/____/_/|_/____/___/_/|_/\___/  
lowp vec3 overlay( lowp vec3 s, lowp vec3 d )
{
	lowp vec3 s0 = step(0.5, d);
	lowp vec3 s1 = 1.0 - s0;	
	return s1 * (2.0 * s * d) + s0 * (1.0 - 2.0 * (1.0 - s) * (1.0 - d));
}

lowp float overlay( lowp float s, lowp float d )
{
	lowp float s0 = step(0.5, d);
	lowp float s1 = 1.0 - s0;	
	return s1 * (2.0 * s * d) + s0 * (1.0 - 2.0 * (1.0 - s) * (1.0 - d));
}

lowp float screen(lowp float s, lowp float d)
{
    return 1. - (1. - s) * (1. - d);
}


   
//========================================================================================
//     _______  __ 
//    /  _/ _ )/ / 
//   _/ // _  / /__
//  /___/____/____/
               

lowp vec2 getEnvUV(mediump vec3 v)
{	
	highp vec2 envUV = vec2(0.);		
	#ifdef DCC_MAX	
	envUV.x = 1. - (atan(v.y/PI,v.x/PI) + PI)/TWOPI;
	envUV.y = 1. - (v.z * .5 + .5);
	#else 
	envUV.x = 1. - (atan(-v.z/PI,v.x/PI) + PI)/TWOPI;
	envUV.y = 1. - (v.y * .5 + .5);
	#endif
	return envUV;
}

uniform lowp vec4 ShadowColor;

void main()
{
	gl_FragColor = ShadowColor;
}
