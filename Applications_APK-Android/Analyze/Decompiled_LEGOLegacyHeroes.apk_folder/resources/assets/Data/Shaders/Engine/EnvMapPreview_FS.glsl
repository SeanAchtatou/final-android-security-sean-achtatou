#version 300 es
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	out lowp vec4 glitch_FragColor;
	#define gl_FragColor glitch_FragColor
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

highp vec2 octWrap(highp vec2 v)
{
    return (1.0 - abs(v.yx)) * sign(v);
}

highp vec2 dirToOct(highp vec3 d)
{
    d /= (abs(d.x) + abs(d.y) + abs(d.z));
    d.xz = d.y >= 0.0 ? d.xz : octWrap(d.xz);
    return d.xz * 0.5 + 0.5;
}

highp vec3 octToDir(highp vec2 v)
{
    v = v * 2.0 - 1.0;
    highp vec3 d;
    d.y = 1.0 - abs(v.x) - abs(v.y);
    d.xz = d.y >= 0.0 ? v : octWrap(v);
    return normalize(d);
}
uniform mediump float irradianceArray [27];
mediump vec3 irradiance(mediump vec3 normal3)
{

    mediump vec4 cAr = vec4(irradianceArray[0],  irradianceArray[1],  irradianceArray[2],  irradianceArray[3] );
    mediump vec4 cAg = vec4(irradianceArray[4],  irradianceArray[5],  irradianceArray[6],  irradianceArray[7] );
    mediump vec4 cAb = vec4(irradianceArray[8],  irradianceArray[9],  irradianceArray[10], irradianceArray[11]);
    mediump vec4 cBr = vec4(irradianceArray[12], irradianceArray[13], irradianceArray[14], irradianceArray[15]);
    mediump vec4 cBg = vec4(irradianceArray[16], irradianceArray[17], irradianceArray[18], irradianceArray[19]);
    mediump vec4 cBb = vec4(irradianceArray[20], irradianceArray[21], irradianceArray[22], irradianceArray[23]);
    mediump vec3 cC =  vec3(irradianceArray[24], irradianceArray[25], irradianceArray[26]);

    mediump vec4 normal = vec4(normal3, 1.0);
    //normal.z = -normal.z;

    mediump vec3 x1, x2, x3;
    
    // Linear + constant polynomial terms
    x1.r = dot(cAr,normal);
    x1.g = dot(cAg,normal); 
    x1.b = dot(cAb,normal);
    
    // 4 of the quadratic polynomials
    mediump vec4 vB = normal.xyzz * normal.yzzx;   
    x2.r = dot(cBr,vB);
    x2.g = dot(cBg,vB);
    x2.b = dot(cBb,vB);
    
    // Final quadratic polynomial
    mediump float vC = normal.x*normal.x - normal.y*normal.y;
    x3 = cC.rgb * vC;    
    mediump vec3 irr = x1 + x2 + x3;
    
    #if defined(EFFECT_EDITOR) || defined(GLITCH_EDITOR)
        irr = irr * 0.0001 + vec3(1.);        		
    #endif
    
    return irr;
}

mediump vec3 irradianceL0()
{
    return vec3(
        irradianceArray[3] + irradianceArray[14] * 1.0 / 3.0,
        irradianceArray[7] + irradianceArray[18] * 1.0 / 3.0,
        irradianceArray[11] + irradianceArray[22] * 1.0 / 3.0);
}

varying highp vec3 vNormal;
varying highp vec3 vPos;

// sRGB transfert function
// https://fr.wikipedia.org/wiki/SRGB
// http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
mediump vec3 SRGBToLinear(mediump vec3 c)
{
#if defined USE_REFERENCE_SRGB
    mediump vec3 linearRGBLo = c / 12.92;
    mediump vec3 linearRGBHi = pow((c + 0.055) / 1.055, vec3(2.4));
    mediump vec3 linearRGB = vec3(
        (c.x <= 0.04045) ? linearRGBLo.x : linearRGBHi.x,
        (c.y <= 0.04045) ? linearRGBLo.y : linearRGBHi.y,
        (c.z <= 0.04045) ? linearRGBLo.z : linearRGBHi.z);
    return linearRGB;
#else
    return c * (c * (c * 0.305306011 + 0.682171111) + 0.012522878);
#endif
}

mediump vec3 LinearToSRGB(mediump vec3 c)
{
#if defined USE_REFERENCE_SRGB
    mediump vec3 sRGBLo = c * 12.92;
    mediump vec3 sRGBHi = (pow(c, vec3(1.0 / 2.4)) * 1.055) - 0.055;
    mediump vec3 sRGB = vec3(
        (c.x <= 0.0031308) ? sRGBLo.x : sRGBHi.x,
        (c.y <= 0.0031308) ? sRGBLo.y : sRGBHi.y,
        (c.z <= 0.0031308) ? sRGBLo.z : sRGBHi.z);
    return sRGB;
#else
    return max(1.055 * pow(c, vec3(0.416666667)) - 0.055, 0.0);
#endif
}

// Relative luminance: https://en.wikipedia.org/wiki/Relative_luminance
mediump float Luminance(mediump vec3 linearRgb)
{
    return dot(linearRgb, vec3(0.2126729, 0.7151522, 0.0721750));
}

uniform mediump samplerCube Cubemap;
uniform highp sampler2D EquirectangularMap;
uniform mediump sampler2D OctohedronMap;

uniform highp float EnvMapLod;

uniform highp vec3 eyepos;

uniform bool Linearize;

const highp float PI = 3.141592653589793238462643383;

void main()
{
	highp vec4 color;

#ifdef CUBEMAP
	highp vec3 reflected = reflect(vPos - eyepos, vNormal);
	color = textureCubeLod(Cubemap, reflected, EnvMapLod);
#endif

#ifdef PANORAMA
	highp vec3 n = normalize(vNormal);
	
	highp float inclination = acos(n.y);
	highp float azimuth = atan(n.x, -n.z);
	
	highp float u = (azimuth / PI + 1.0) / 2.0;
	highp float v = inclination / PI;
	
	color = texture2D(EquirectangularMap, vec2(u, v));
#endif

#ifdef OCT_MAP
    highp vec3 reflected = reflect(vPos - eyepos, vNormal);
	color = textureLod(OctohedronMap, (dirToOct(normalize(vNormal)) + 0.125 / 2.0) / 1.125, EnvMapLod);
    //color = textureLod(OctohedronMap, dirToOct(normalize(vNormal)), EnvMapLod);
#endif

#ifdef SH
    color = vec4(irradiance(normalize(vNormal)), 1.0);
#endif

#if defined DECODE_RGBD
	if(Linearize)
	{
		color.rgb = SRGBToLinear(color.rgb);
	}
	color.rgb /= max(1. / 256., color.a);
#endif

	gl_FragColor = vec4(color.rgb, 1.0);
}
