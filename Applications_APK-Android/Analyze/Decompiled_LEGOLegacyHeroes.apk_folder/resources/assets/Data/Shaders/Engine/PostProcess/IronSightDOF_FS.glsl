#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

varying mediump vec2 vUV0;


uniform sampler2D texture0;
GLITCH_UNIFORM_PROPERTIES(texture1, (depthtexture = 1))
uniform sampler2D texture1;
uniform highp vec2 texelSize;
//uniform highp vec2 screenSpaceAim;

uniform highp float Intensity;
uniform highp float MaxDistance;

highp float LinearizeDepth(mediump float z_b, highp vec2 nearFar)
{
    highp float z_n = 2.0 * z_b - 1.0;
    highp float z_e = 2.0 * nearFar.x * nearFar.y / (nearFar.y + nearFar.x - z_n * (nearFar.y - nearFar.x));
    // z_e = -A / (z_n + B);  // A, B precomputed
    
    return (z_e - nearFar.x) / (nearFar.y - nearFar.x);
}

highp float LinearizeDepth01(mediump float z_b)
{
    highp float z_n = 2.0 * z_b - 1.0;
    highp float z_e = 0.02 / (1. + .01 - z_n * .99);
    
    return (z_e - .01) / .99;
}

void main()
{	
	  // Assuming iron sight is aiming at the center of the screen
	  highp vec2 screenSpaceAim = vec2(.5);
	  
	  // Get depth values 
	  highp float d = LinearizeDepth01(texture2D(texture1, vUV0.xy).r);
	  highp float aim = LinearizeDepth01(texture2D(texture1, screenSpaceAim).r);
	  
	  // Limit effect to a distance
	  aim = min(aim, MaxDistance);
	  
	  // Calculate DOF amount
	  highp float dof = smoothstep(aim, aim - .05, d) * step(.1, d);
	  
	  // Make blend
	  highp vec4 c = vec4(0.);
	  highp vec2 offset = (Intensity + .5)  * texelSize * dof;
	  highp vec2 halfTexel = 0.5 * texelSize;
	  c += texture2D(texture0, vUV0.xy + vec2( offset.x,  halfTexel.y));
	  c += texture2D(texture0, vUV0.xy + vec2( halfTexel.x, -offset.y));
	  c += texture2D(texture0, vUV0.xy + vec2(-offset.x, -halfTexel.y));
	  c += texture2D(texture0, vUV0.xy + vec2(-halfTexel.x,  offset.y));
	  c /= 4.;
	  
	  //c = vec4(d);
	  
	  gl_FragColor = c;
}
