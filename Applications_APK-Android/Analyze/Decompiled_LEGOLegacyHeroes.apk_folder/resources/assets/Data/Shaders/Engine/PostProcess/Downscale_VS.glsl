#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
#endif

varying mediump vec2 vUV; 


attribute highp vec4 Vertex;
attribute highp vec2 TexCoord0;

uniform highp mat4 WorldViewProjection;

uniform highp vec2 uvScale;
uniform highp vec2 uvOffset;

void main()
{	   
    vUV = TexCoord0 * uvScale + uvOffset;
    highp vec4 pos = WorldViewProjection * Vertex;
    gl_Position = pos; 
}
