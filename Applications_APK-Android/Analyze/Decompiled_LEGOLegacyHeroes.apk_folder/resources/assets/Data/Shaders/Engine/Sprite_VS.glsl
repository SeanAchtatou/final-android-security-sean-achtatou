#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define attribute in
#    define varying out
#endif

varying lowp vec4 vColor;
varying highp vec2 vUV;

attribute highp vec4 Vertex;
attribute highp vec4 Color;
attribute highp vec2 TexCoord0;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp float flareScale;

void main() 
{
	vColor = Color;
	vUV = TexCoord0;
	highp vec4 position = WorldViewProjectionMatrix * Vertex;
	position.xy *= flareScale;
	gl_Position = position;	
}
