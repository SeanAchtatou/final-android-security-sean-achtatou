#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

// Varyings
varying mediump vec2 vScreen;


uniform mediump sampler2D texture0;
uniform mediump sampler2D texture1;
uniform mediump float amount0;
uniform mediump float amount1;

void main()
{
	mediump vec4 color0 = texture2D(texture0, vScreen);
	mediump vec4 color1 = texture2D(texture1, vScreen);
	#if defined(PREMULTIPLIED)
		mediump vec4 color = color0 * (1.0-color1.a) + color1;
	#elif defined(ADD)
		mediump vec4 color = color0 * amount0 + color1 * amount1;
    #else
		mediump vec4 color = mix(color0, vec4(color1.rgb, 1.0), color1.a);
	#endif
	gl_FragColor = color;	
}
