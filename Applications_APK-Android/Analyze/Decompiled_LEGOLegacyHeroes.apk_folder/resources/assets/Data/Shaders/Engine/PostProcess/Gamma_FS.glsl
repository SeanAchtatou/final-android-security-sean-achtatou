#version 300 es
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	out lowp vec4 glitch_FragColor;
	#define gl_FragColor glitch_FragColor
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif
// Varyings
varying mediump vec2 vScreen;

// sRGB transfert function
// https://fr.wikipedia.org/wiki/SRGB
// http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
mediump vec3 SRGBToLinear(mediump vec3 c)
{
#if defined USE_REFERENCE_SRGB
    mediump vec3 linearRGBLo = c / 12.92;
    mediump vec3 linearRGBHi = pow((c + 0.055) / 1.055, vec3(2.4));
    mediump vec3 linearRGB = vec3(
        (c.x <= 0.04045) ? linearRGBLo.x : linearRGBHi.x,
        (c.y <= 0.04045) ? linearRGBLo.y : linearRGBHi.y,
        (c.z <= 0.04045) ? linearRGBLo.z : linearRGBHi.z);
    return linearRGB;
#else
    return c * (c * (c * 0.305306011 + 0.682171111) + 0.012522878);
#endif
}

mediump vec3 LinearToSRGB(mediump vec3 c)
{
#if defined USE_REFERENCE_SRGB
    mediump vec3 sRGBLo = c * 12.92;
    mediump vec3 sRGBHi = (pow(c, vec3(1.0 / 2.4)) * 1.055) - 0.055;
    mediump vec3 sRGB = vec3(
        (c.x <= 0.0031308) ? sRGBLo.x : sRGBHi.x,
        (c.y <= 0.0031308) ? sRGBLo.y : sRGBHi.y,
        (c.z <= 0.0031308) ? sRGBLo.z : sRGBHi.z);
    return sRGB;
#else
    return max(1.055 * pow(c, vec3(0.416666667)) - 0.055, 0.0);
#endif
}

// Relative luminance: https://en.wikipedia.org/wiki/Relative_luminance
mediump float Luminance(mediump vec3 linearRgb)
{
    return dot(linearRgb, vec3(0.2126729, 0.7151522, 0.0721750));
}

uniform mediump sampler2D texture0;

void main()
{
    mediump vec4 color = texture2D(texture0, vScreen);

    #ifndef DISABLED
        color.rgb = LinearToSRGB(color.rgb);
    #endif

	gl_FragColor = color;
}
