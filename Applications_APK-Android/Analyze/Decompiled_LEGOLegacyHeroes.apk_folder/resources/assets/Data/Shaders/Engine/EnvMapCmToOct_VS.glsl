#version 300 es
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
#endif

// Max defines
#ifdef DCC_MAX
	#define SEM(x) : x
	#define COLOR_TYPE vec3
#else
	#define SEM(x)
	#define COLOR_TYPE vec4
#endif

#define PI 3.1415926535897932384626433832795

// Varyings
varying highp vec2 vUV;


attribute highp vec4 position				SEM(POSITION);
attribute highp vec2 texcoord0				SEM(TEXCOORD0);
uniform highp mat4 ViewProjection;

void main() 
{
    vUV = texcoord0 * 1.125 - 0.125 / 2.0;
    gl_Position = ViewProjection * position;
}
