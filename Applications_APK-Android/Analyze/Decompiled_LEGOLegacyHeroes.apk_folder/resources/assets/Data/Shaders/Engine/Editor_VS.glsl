#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
#endif
varying highp vec2 vExposure;


uniform highp sampler2D global_exposureTexture;
uniform highp float global_exposureCompensation;

void PrepareExposure()
{	   
	highp vec2 exposureTexture = texture2D(global_exposureTexture, vec2(0,0)).rg;
	vExposure.x = 0.18 / exp2(exposureTexture.r - global_exposureCompensation);
	vExposure.y = exp2(exposureTexture.g) * vExposure.x;
}
	  

varying highp vec3 vPosition;
varying lowp vec3 vColor;
attribute highp vec4 Vertex;
attribute lowp vec4 Color;
uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 matWorld;

void main()
{
    PrepareExposure();
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
