#version 300 es
#if defined POSITIVE_FACES && defined NEGATIVE_FACES
    #define MAX_DRAW_BUFFERS 6
#else
    #define MAX_DRAW_BUFFERS 4
#endif
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
#endif

// Varyings
#ifdef POSITIVE_FACES
    varying highp vec3 vPosXDir;
    varying highp vec3 vPosYDir;
    varying highp vec3 vPosZDir;
#endif

#ifdef NEGATIVE_FACES
    varying highp vec3 vNegXDir;
    varying highp vec3 vNegYDir;
    varying highp vec3 vNegZDir;
#endif


attribute highp vec4 position;
attribute highp vec2 texcoord0;
uniform highp mat4 ViewProjection;

#ifdef POSITIVE_FACES
    const highp vec3 posFront0 = vec3(1.0, 0.0, 0.0);
    const highp vec3 posFront1 = vec3(0.0, 1.0, 0.0);
    const highp vec3 posFront2 = vec3(0.0, 0.0, 1.0);
    const highp vec3 posUp0 = vec3(0.0, 1.0, 0.0);
    const highp vec3 posUp1 = vec3(0.0, 0.0, -1.0);
    const highp vec3 posUp2 = vec3(0.0, 1.0, 0.0);
#endif

#ifdef NEGATIVE_FACES
    const highp vec3 negFront0 = vec3(-1.0, 0.0, 0.0);
    const highp vec3 negFront1 = vec3(0.0, -1.0, 0.0);
    const highp vec3 negFront2 = vec3(0.0, 0.0, -1.0);
    const highp vec3 negUp0 = vec3(0.0, 1.0, 0.0);
    const highp vec3 negUp1 = vec3(0.0, 0.0, 1.0);
    const highp vec3 negUp2 = vec3(0.0, 1.0, 0.0);
#endif

highp vec3 computeDir(highp vec3 faceFront, highp vec3 faceUp, highp vec2 biasedUV)
{
    highp vec3 faceRight = normalize(cross(faceFront, faceUp));
    return normalize(faceFront - faceRight * biasedUV.x - faceUp * biasedUV.y);
}

void main() 
{
    highp vec2 biasedUV = (texcoord0 - vec2(0.5)) * 2.0;
#ifdef POSITIVE_FACES
    vPosXDir = computeDir(posFront0, posUp0, biasedUV);
    vPosYDir = computeDir(posFront1, posUp1, biasedUV);
    vPosZDir = computeDir(posFront2, posUp2, biasedUV);
#endif
#ifdef NEGATIVE_FACES
    vNegXDir = computeDir(negFront0, negUp0, biasedUV);
    vNegYDir = computeDir(negFront1, negUp1, biasedUV);
    vNegZDir = computeDir(negFront2, negUp2, biasedUV);
#endif
    gl_Position = ViewProjection * position;
}
