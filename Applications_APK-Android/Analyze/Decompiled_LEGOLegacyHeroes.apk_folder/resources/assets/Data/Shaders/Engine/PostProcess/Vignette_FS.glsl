#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	out lowp vec4 glitch_FragColor;
	#define gl_FragColor glitch_FragColor
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif
varying highp vec2 vUV;

uniform mediump sampler2D texture0;

uniform mediump int VignetteMode;

uniform mediump vec4 vignetteColor;
uniform mediump vec2 vignetteCenter;
uniform mediump float vignetteIntensity;
uniform mediump float vignetteSmoothness;

uniform mediump sampler2D VignetteMask;
uniform mediump float vignetteOpacity;

void main()
{
    mediump vec4 color = texture2D(texture0, vUV);

    if(VignetteMode == 0)
    {
        mediump vec2 d = clamp(abs(vUV - vignetteCenter) * vignetteIntensity, 0.0, 1.0);
        mediump float vF = pow(clamp(1.0 - dot(d, d), 0.0, 1.0), vignetteSmoothness);
        color.rgb *= mix(vignetteColor.rgb, vec3(1.0), vF);
        color.a = mix(1.0, color.a, vF);
    }
    else
    {
        mediump float vF = texture2D(VignetteMask, vUV).a;

        mediump vec3 newColor = color.rgb * mix(vignetteColor.rgb, vec3(1.0), vF);
        color.rgb = mix(color.rgb, newColor, vignetteOpacity);
        color.a = mix(1.0, color.a, vF);
    }

    gl_FragColor = color;
}

