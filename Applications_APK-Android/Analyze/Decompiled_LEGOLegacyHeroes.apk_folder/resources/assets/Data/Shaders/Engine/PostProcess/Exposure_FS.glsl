//#version 300 es
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	out lowp vec4 glitch_FragColor;
	#define gl_FragColor glitch_FragColor
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

varying highp vec2 vUV0;

//#extension GL_EXT_shader_texture_lod :require

uniform lowp sampler2D texture0;
uniform lowp sampler2D texture1;
uniform lowp sampler2D vanDerCorput;

#define BIN_COUNT 64
#define SAMPLE_COUNT 1024

uniform highp float lightenHalfLife;
uniform highp float darkenHalfLife;
uniform highp float deltaTime;
uniform highp float darkIgnore;
uniform highp float lightIgnore;
uniform highp float minExposure;
uniform highp float maxExposure;
uniform highp vec2 resetValue;

uniform highp float histogramMinValue;
uniform highp float histogramMaxValue;

highp float gauss(highp float x, highp float a, highp float b, highp float c)
{
	highp float xb = (x-b);
	return a * exp(-xb*xb/(2.0*c*c));
}


highp float smoothStep(highp float x)
{
	highp float x2 = x*x;
	return -2.0 * x2 * x + 3.0 * x2;
}

highp float smoothClamp(highp float x)
{
	return smoothStep(clamp((x+(1.5/2.0)-0.5)/1.5,0.0,1.0));
}

highp float smoothSlopeClamp(highp float x)
{
	return minExposure + (maxExposure - minExposure) * smoothClamp((x-minExposure)/(maxExposure-minExposure));
}



void main()
{
	highp float invBin = 1.0/float(BIN_COUNT);
	highp float invSample = 1.0/float(SAMPLE_COUNT);

	mediump vec3 color = vec3(0.0);
	#ifdef SAMPLE
		highp vec2 samplePos = vec2(texture2D(vanDerCorput, vUV0).r, vUV0.x);
		highp vec3 sample = texture2D(texture0, samplePos).rgb;
		highp float lum = dot(sample, vec3(0.2126, 0.7152, 0.0722));
		color.rgb = vec3(max(-100.0,log2(lum)));
	#endif
	
	
	#ifdef HISTOGRAM
		int binIndex = int(vUV0.x * float(BIN_COUNT));
		highp float bin = 0.0;
		highp float binWidth = (histogramMaxValue-histogramMinValue)/float(BIN_COUNT);
		highp float binMin = histogramMinValue + binWidth * float(binIndex);
		highp float binMax = binMin + binWidth;
		if(binMin < histogramMinValue+0.1)
			binMin = -1000.0;
		if(binMax > histogramMaxValue-0.1)
			binMax = 1000.0;
		
		for(int i=0; i<SAMPLE_COUNT; i++)
		{
			highp float v;
			highp float sample = texture2D(texture0, vec2(float(i)*invSample,0.0)).r;
			if(sample >= binMin && sample < binMax)
				v=invSample;
			else
				v=0.0;
			//highp float v = 1.0 - step(0.5 * binWidth, abs(sample - a));
			
			bin += v;
		}
		
		color = vec3(bin);
	#endif
	
	
	#ifdef LUMINANCE
	
		highp vec2 targetExposure;
		highp float dark = darkIgnore;
		highp float light = 1.0-lightIgnore;
		highp float binWidth = (histogramMaxValue-histogramMinValue)/float(BIN_COUNT);
		
		
		highp float darkValue = 0.0;
		highp float lightValue = 1.0;
		for(int i=0; i<BIN_COUNT; i++)
		{
			highp float b = texture2D(texture0, vec2(float(i) * invBin,0)).r;
			highp float binValue = histogramMinValue + binWidth * (float(i) + 0.5);
			if(dark > 0.0)
				darkValue = binValue;
			if(light > 0.0)
				lightValue = binValue;
			dark-=b;
			light-=b;
		}
		
		targetExposure = vec2((darkValue + lightValue) *0.5, lightValue);
		//targetExposure = vec2((darkValue + lightValue) *0.5, 1.0);
		
	/*
		highp float bins [BIN_COUNT];
		highp float binWidth = (histogramMaxValue-histogramMinValue)/float(BIN_COUNT);
		highp float dd = dark;

		for(int i=0; i<BIN_COUNT; i++)
		{
			highp float b = texture2D(texture0, vec2(float(i) * invBin,0)).r;
			bins[i] = b;
		}
		// Cull dark
		for(int i=0; i<BIN_COUNT; i++)
		{
			highp float b = texture2D(texture0, vec2(float(i) * invBin,0)).r;
			highp float bd = max(0.0, b - dark);
			dark -= b - bd;
			bins[i] = bd;
		}

		// Cull light
		for(int i=0; i<BIN_COUNT; i++)
		{
			highp float b = bins[BIN_COUNT-i-1];
			highp float bl = max(0.0, b - light);
			light -= b - bl;
			bins[BIN_COUNT-i-1] = bl;
		}
		
		// Average the remaining bins
		highp float totalWeight = 0.0;
		highp float totalExposure = 0.0;
		for(int i=0; i<BIN_COUNT; i++)
		{
			highp float binValue = histogramMinValue + binWidth * (float(i) + 0.5);
			highp float b = bins[i];
			totalExposure += binValue * b;
			totalWeight += b;
		}
		
		targetExposure = totalExposure / totalWeight;*/
		#ifndef MIX
			color = vec3(targetExposure, darkValue);
		#endif
	#endif
	#ifdef MIX
			highp vec2 lastExposure = texture2D(texture1, vec2(0.0,0.0)).rg;
		#ifndef LUMINANCE
			highp vec2 luminanceTexture = texture2D(texture0, vec2(0.0, 0.0)).rg;
			highp vec2 targetExposure = luminanceTexture.rg;
		#endif

		// NaN detector
		if(!(targetExposure.r > 0.0 || targetExposure.r < 0.0 || targetExposure.r == 0.0))
		{
			targetExposure.r = (maxExposure + minExposure) * 0.5;
		} 

		// Clamp
		highp float r = smoothSlopeClamp(targetExposure.r);
		//targetExposure.g -= r - targetExposure.r;
		targetExposure.r = r;

		// NaN detector
		if(!(lastExposure.r > 0.0 || lastExposure.r < 0.0 || lastExposure.r == 0.0))
		{
			lastExposure = targetExposure;
		}
		
		// Decay
		const highp float ln2 = 0.69315;
		highp float halfLife = targetExposure.r > lastExposure.r ? darkenHalfLife : lightenHalfLife;
		highp vec2 exposure = targetExposure + (lastExposure-targetExposure) * exp(-deltaTime * ln2 / halfLife);
		
		color.rg = exposure;
	#endif
	
	#ifdef RESET
		color.rgb = vec3(resetValue, 0.0);
	#endif
	
	gl_FragColor = vec4(color, 1.0);
}
