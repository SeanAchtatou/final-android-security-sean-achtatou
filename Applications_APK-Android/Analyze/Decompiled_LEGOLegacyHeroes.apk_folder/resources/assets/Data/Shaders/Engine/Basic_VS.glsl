varying highp vec2 vExposure;


uniform highp sampler2D global_exposureTexture;
uniform highp float global_exposureCompensation;

void PrepareExposure()
{	   
	highp vec2 exposureTexture = texture2D(global_exposureTexture, vec2(0,0)).rg;
	vExposure.x = 0.18 / exp2(exposureTexture.r - global_exposureCompensation);
	vExposure.y = exp2(exposureTexture.g) * vExposure.x;
}
	  
varying lowp vec4 vColor;
varying lowp vec3 vNormal;
varying lowp vec3 vEyePosition;
varying lowp vec3 vPosition;


attribute highp vec3 Vertex;
attribute highp vec4 Color;
attribute highp vec3 Normal;

uniform highp mat4 World;
uniform highp mat4 WorldViewProjectionMatrix;
uniform highp vec3 eyePos;


#ifdef SKINNED
//GLITCH_UNIFORM_BLOCK(Skeleton, (WeightMask = highp vec4, BoneMatrices = highp mat4[57]))

uniform highp mat4 BoneMatrices[64];
uniform highp vec4 WeightMask;

attribute highp vec4 SkinWeights;
attribute mediump vec4 SkinIndices;
#endif

highp vec3 Skin()
{
#ifdef SKINNED
	highp vec4 weights = SkinWeights * WeightMask;
	highp vec3 position = vec3(0.0);
	highp vec4 indices = SkinIndices;

	#ifdef TRIVIAL_VS
        for (int i = 0; i < 4; i++)
        {
            int index = int(indices[i]);
            //position += weights[i] * (GLITCH_IN_BLOCK(Skeleton, BoneMatrices)[index] * vec4(Vertex.xyz, 1.0)).xyz;
            position += weights[i] * (BoneMatrices[index] * vec4(Vertex.xyz, 1.0)).xyz;
        }
	#else
        for (int i = 0; i < 4; i++)
        {
            int index = int(indices[i]);
            //position += weights[i] * (GLITCH_IN_BLOCK(Skeleton, BoneMatrices)[index] * vec4(Vertex.xyz, 1.0)).xyz;
            position += weights[i] * (BoneMatrices[index] * vec4(Vertex.xyz, 1.0)).xyz;
        }

	    highp vec3 netNormal = vec3(0.0);
	 	highp vec3 netTangent = vec3(0.0);
		highp vec3 netBinormal = vec3(0.0);
			
		for(int i = 0; i < 2; i++)
		{
			int index = int(indices[i]);
			//netNormal += weights[i] * (GLITCH_IN_BLOCK(Skeleton, BoneMatrices)[index] * vec4(Normal.xyz, 0.0)).xyz;
		    //netTangent += weights[i] * (GLITCH_IN_BLOCK(Skeleton, BoneMatrices)[index] * vec4(Tangent.xyz, 0.0)).xyz;
		    //netBinormal += weights[i] * (GLITCH_IN_BLOCK(Skeleton, BoneMatrices)[index] * vec4(Binormal.xyz, 0.0)).xyz;
		    netNormal += weights[i] * (BoneMatrices[index] * vec4(Normal.xyz, 0.0)).xyz;
		    netTangent += weights[i] * (BoneMatrices[index] * vec4(Tangent.xyz, 0.0)).xyz;
		    netBinormal += weights[i] * (BoneMatrices[index] * vec4(Binormal.xyz, 0.0)).xyz;
		}
	    
		vNormal = normalize((worldIT*vec4(netNormal,0.0)).xyz);
		vTangent = (worldIT*vec4(netTangent,0.0)).xyz;
		vBinormal = (worldIT*vec4(netBinormal,0.0)).xyz;
    #endif

    vPosition = position;
#else
	highp vec3 position = Vertex.xyz;
	vPosition = (World*vec4(position, 1.0)).xyz;
		
	#ifndef TRIVIAL_VS
		vNormal = (worldIT*vec4(Normal,0.0)).xyz;
		vTangent = (worldIT*vec4(Tangent,0.0)).xyz;
		vBinormal = (worldIT*vec4(Binormal,0.0)).xyz;
	#endif
#endif
	return position;
}

void main() 
{
	highp vec3 position = Skin();
	
	vColor = vec4(pow(Color.rgb,vec3(2.2)),Color.a);
	vEyePosition = eyePos;
	vNormal = Normal;
	vPosition = (World * vec4(position,1)).xyz;
	PrepareExposure();
	gl_Position = WorldViewProjectionMatrix * vec4(position,1);
}

