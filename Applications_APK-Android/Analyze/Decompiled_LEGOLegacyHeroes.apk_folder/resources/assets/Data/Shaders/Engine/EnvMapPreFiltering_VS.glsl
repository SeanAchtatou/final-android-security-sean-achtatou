#version 300 es
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
#endif

#define PI 3.1415926535897932384626433832795

// Varyings
varying mediump vec2 vUV;
varying highp vec3 vDirection;

uniform highp vec3 faceFront;
uniform highp vec3 faceUp;



attribute highp vec4 position;
attribute highp vec2 texcoord0;
uniform highp mat4 WorldViewProjection;
uniform highp mat4 WorldView;

void main() 
{
    vUV = texcoord0;
    
    highp vec3 faceRight = normalize(cross(faceFront, faceUp));
    highp vec2 biasedUV = (texcoord0 - vec2(0.5)) * 2.0;
    vDirection = normalize(faceFront - faceRight * biasedUV.x - faceUp * biasedUV.y);

    gl_Position = WorldViewProjection * position;
}
