#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

varying mediump float vExposure;

mediump vec3 CancelExposure(mediump vec3 color)
{	  
	return color / vExposure;
}

mediump vec3 ApplyExposure(mediump vec3 color)
{	  
	return color * vExposure;
}



varying highp vec2 vUV;

// sRGB transfert function
// https://fr.wikipedia.org/wiki/SRGB
// http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
mediump vec3 SRGBToLinear(mediump vec3 c)
{
#if defined USE_REFERENCE_SRGB
    mediump vec3 linearRGBLo = c / 12.92;
    mediump vec3 linearRGBHi = pow((c + 0.055) / 1.055, vec3(2.4));
    mediump vec3 linearRGB = vec3(
        (c.x <= 0.04045) ? linearRGBLo.x : linearRGBHi.x,
        (c.y <= 0.04045) ? linearRGBLo.y : linearRGBHi.y,
        (c.z <= 0.04045) ? linearRGBLo.z : linearRGBHi.z);
    return linearRGB;
#else
    return c * (c * (c * 0.305306011 + 0.682171111) + 0.012522878);
#endif
}

mediump vec3 LinearToSRGB(mediump vec3 c)
{
#if defined USE_REFERENCE_SRGB
    mediump vec3 sRGBLo = c * 12.92;
    mediump vec3 sRGBHi = (pow(c, vec3(1.0 / 2.4)) * 1.055) - 0.055;
    mediump vec3 sRGB = vec3(
        (c.x <= 0.0031308) ? sRGBLo.x : sRGBHi.x,
        (c.y <= 0.0031308) ? sRGBLo.y : sRGBHi.y,
        (c.z <= 0.0031308) ? sRGBLo.z : sRGBHi.z);
    return sRGB;
#else
    return max(1.055 * pow(c, vec3(0.416666667)) - 0.055, 0.0);
#endif
}

// Relative luminance: https://en.wikipedia.org/wiki/Relative_luminance
mediump float Luminance(mediump vec3 linearRgb)
{
    return dot(linearRgb, vec3(0.2126729, 0.7151522, 0.0721750));
}

uniform mediump sampler2D texture0;

uniform mediump sampler2D blur0;
uniform mediump sampler2D blur1;
uniform mediump sampler2D blur2;
//uniform mediump sampler2D blur3;
//uniform mediump sampler2D blur4;

uniform mediump float bloomIntensity;
uniform mediump sampler2D dirtLensTexture;
uniform mediump float dirtLensIntensity;
uniform mediump vec3 colorCorrectScale;
uniform mediump vec3 colorCorrectOffset;
uniform mediump float colorCorrectSaturation;

// Gamma value
#define GAMMA 2.2

mediump vec3 linearToScreen(mediump vec3 t){ return pow(t, vec3(1.0/GAMMA)); }

// Filmic ALU, Includes gamma correction
mediump vec3 FilmicALU(mediump vec3 t)
{
    t = max(vec3(0.), t - .004);
    return (t * (6.2 * t + .5)) / (t * (6.2 * t + 1.7) + .6);
} 

// Filmic by John Hable, DOES NOT includes gamma correction
mediump vec3 Hable(mediump vec3 t)
{
    mediump float A = 0.22; // Shoulder strength
	mediump float B = 0.30; // Linear strength
    mediump float C = 0.10; // Linear angle
    mediump float D = 0.20; // Toe strength 
    mediump float E = 0.01; // Toe numerator
    mediump float F = 0.30; // Toe denominator 
	  
    return ((t * (A * t + C * B) + D * E) / (t * (A * t + B) + D * F)) - E / F;
}

// Simplified filmic version
mediump vec3 simpleFTM(mediump vec3 t)
{
    mediump float A = 10.; /// Mid
    mediump float B = 0.3; /// Toe
    mediump float C = 0.5; /// Shoulder
    mediump float D = 1.5; /// Mid

    return (t * (A * t + B)) / (t * (A * t + C) + D);
}

// Reinhard ton mapping
mediump vec3 Reinhard(mediump vec3 t, mediump float whitePoint)
{
    mediump float lum = dot(t, vec3(.2126, .7152, .0722)) + .00001;
    mediump float nl = (lum * (1. + lum / whitePoint)) / (1. + lum);
    return nl * t / lum;
}

highp vec2 cubicUV(highp vec2 uv, highp vec2 texSize)
{
	uv = uv * texSize + .5;
	highp vec2 iuv = floor( uv );
	highp vec2 fuv = fract( uv );
	uv = iuv + fuv * fuv * (3. - 2. * fuv);
	uv = (uv - .5) / texSize;
	return uv;
}

highp vec2 n(highp vec2 uv, highp float f)
{
	return f *(uv - .5) + .5;
}

mediump vec3 AddBloom(mediump vec3 t)
{
    mediump vec4 bloom = vec4(0.0);
    
    bloom += texture2D(blur0, vUV);
    bloom += texture2D(blur1, vUV);
    bloom += texture2D(blur2, vUV);
    //bloom += texture2D(blur3, vUV);
    //bloom += texture2D(blur4, vUV);
    
    return t + bloom.rgb * (bloomIntensity * 1.0 / 3.0);
}

highp vec3 AddDirtLens(highp vec3 t)
{
    highp vec4 flare = vec4(0.0);
    
    //flare += texture2D(blur3, vUV);
   // flare += texture2D(blur4, vUV);
    
    highp vec4 dirt = texture2D(dirtLensTexture, vUV);
    flare *= dirt * dirtLensIntensity;
    
    t = min(t, vec3(1.0));
    
    return 1.0 - (1.0 - t) * (1.0 - flare.rgb);
}

mediump vec3 ApplyToneMap(mediump vec3 t, mediump float wp)
{	
	#if defined TONEMAP_LINEAR
		t.rgb /= wp;
	#elif defined TONEMAP_FILMIC
		t.rgb = FilmicALU(t.rgb) / FilmicALU(vec3(wp));
	#elif defined TONEMAP_HABLE
		t.rgb = Hable(t.rgb) / Hable(vec3(wp));
	#elif defined TONEMAP_REINHARD
		t.rgb = Reinhard(t.rgb, wp);
	#elif defined TONEMAP_COMPARE
		if(vUV.y < .5)
		{
			if(vUV.x < .5)
			{
				t.rgb /= wp;
			}
		  	else
		  	{
		  		t.rgb = FilmicALU(t.rgb) / FilmicALU(vec3(wp));
		  	}
		}
		else
		{
			if(vUV.x < .5)
			{
				t.rgb = Hable(t.rgb) / Hable(vec3(wp));
			}
			else
			{
				t.rgb = Reinhard(t.rgb, wp);
			}
		}
	#endif
	
	return t;
}

mediump vec3 ApplyColorCorrection(mediump vec3 t)
{
	t = t * colorCorrectScale + colorCorrectOffset;
	mediump float desturated = max(0., dot(t, vec3(0.2126, 0.7152, 0.0722)));
	return mix(vec3(desturated), t, colorCorrectSaturation);
}

void main()
{   
    mediump vec4 c = texture2D(texture0, vUV);
   
    #ifndef DISABLED

        c.rgb = ApplyExposure(c.rgb);

        c.rgb = ApplyToneMap(c.rgb, 1.0);
    
        #ifndef NO_BLOOM

            c.rgb = AddBloom(c.rgb);
    
        #endif
    
    c.rgb = ApplyColorCorrection(c.rgb);

    #endif

    // to sRGB
    c.rgb = LinearToSRGB(c.rgb);
 
    gl_FragColor = c;
}


