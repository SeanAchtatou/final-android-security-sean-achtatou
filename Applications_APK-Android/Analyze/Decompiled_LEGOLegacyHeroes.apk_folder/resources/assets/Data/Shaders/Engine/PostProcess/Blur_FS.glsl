#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

varying lowp vec4 vColor;
varying highp  vec2 vUV;


uniform sampler2D texture0;
uniform sampler2D texture1;
uniform highp float exposure;

uniform highp vec2 texture0TexelSize;
uniform highp vec2 texture1TexelSize;

// Gaussian factors
// Using gaussian values [ gE=0.00390625	 gD=0.03125]	[gC=0.109375	 gB=0.21875]	[gA=0.2734375]	[0.21875	 0.109375]	[0.03125	 0.00390625]
// Using filtering to combine fetches by pairs (offsets in half texels size), then using combined weight for factors (g0, g1, g2)

/*
const highp float g0 = 0.261843383; //0.27343750; // gA // 0.261843383
const highp float g1 = 0.322516382; //0.32812500; // gC + gB // 0.322516382
const highp float g2 = 0.04656193; //0.03515625; // gE + gD // 0.04656193

const highp float off0 = 0.0; 
const highp float off1 = -1.5;
const highp float off2 = -3.5;
const highp float off3 = 1.5;
const highp float off4 = 3.5;
*/
/*
const highp float Weight0 = 0.262 / 0.9052;
const highp float Weight1 = 0.2108 / 0.9052;
const highp float Weight2 = 0.1108 / 0.9052;
const highp float Offset0 = 1.0 * 0.5;
const highp float Offset1 = 4.8899430740038 * 0.5;
const highp float Offset2 = -2.8899430740038 * 0.5;
const highp float Offset3 = 8.78339350180505 * 0.5;
const highp float Offset4 = -6.78339350180505 * 0.5;
*/
/*
const highp float g0 = 0.382;
const highp float g1 = 0.2418;
const highp float g2 = 0.061;
const highp float off0 = 1.0 * 0.5;
const highp float off1 = 4.55737704918033  * 0.5;
const highp float off2 = 8.75930521091811 * 0.5;
const highp float off3 = -2.55737704918033 * 0.5;
const highp float off4 = -6.75930521091811 * 0.5;*/
/*
const highp float Weight0 = 0.382;
const highp float Weight1 = 0.2418;
const highp float Weight2 = 0.061;
const highp float Offset0 = 1.0 * 0.5;
const highp float Offset1 = 4.55737704918033 * 0.5;
const highp float Offset2 = -2.55737704918033 * 0.5;
const highp float Offset3 = 8.75930521091811 * 0.5;
const highp float Offset4 = -6.75930521091811 * 0.5;
*/


uniform mediump float Weight0;
uniform mediump float Weight1;
uniform mediump float Weight2;
uniform mediump float Weight3;
uniform highp float Offset0;
uniform highp float Offset1;
uniform highp float Offset2;
uniform highp float Offset3;
uniform highp float Offset4;
uniform highp float Offset5;
uniform highp float Offset6;


// Texture fetch
mediump vec4 t(highp vec2 offset, mediump float factor)
{
	return texture2D(texture0, vUV + offset) * factor;
}

#if defined SAMPLE_7 && !defined SAMPLE_5
    #define SAMPLE_5
#endif
#if defined SAMPLE_5 && !defined SAMPLE_3
    #define SAMPLE_3
#endif

void main()
{	   
    mediump vec4 c = vec4(0.);

    #if defined HORIZONTAL 	
        // Blur horizontal
        c += t(vec2(Offset0, 0.0) * texture0TexelSize, Weight0);	  
        c += t(vec2(Offset1, 0.0) * texture0TexelSize, Weight1);	  
        c += t(vec2(Offset2, 0.0) * texture0TexelSize, Weight1);	  
        #if defined SAMPLE_5 	
            c += t(vec2(Offset3, 0.0) * texture0TexelSize, Weight2);	  
            c += t(vec2(Offset4, 0.0) * texture0TexelSize, Weight2);
            #if defined SAMPLE_7
                c += t(vec2(Offset5, 0.0) * texture0TexelSize, Weight3);	  
                c += t(vec2(Offset6, 0.0) * texture0TexelSize, Weight3);
            #endif	 
        #endif

    #elif defined VERTICAL

        // Blur vertical
        
        c += t(vec2(0.0,Offset0) * texture0TexelSize, Weight0);	  
        c += t(vec2(0.0,Offset1) * texture0TexelSize, Weight1);	  
        c += t(vec2(0.0,Offset2) * texture0TexelSize, Weight1);	  
        #if defined SAMPLE_5 	
            c += t(vec2(0.0,Offset3) * texture0TexelSize, Weight2);	  
            c += t(vec2(0.0,Offset4) * texture0TexelSize, Weight2);
                #if defined SAMPLE_7
                    c += t(vec2(0.0, Offset5) * texture0TexelSize, Weight3);	  
                    c += t(vec2(0.0, Offset6) * texture0TexelSize, Weight3);
                #endif	 
        #endif	  	

    #endif

    gl_FragColor = c; 
}
