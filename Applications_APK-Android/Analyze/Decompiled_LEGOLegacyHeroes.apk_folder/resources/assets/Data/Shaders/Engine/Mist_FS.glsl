
varying lowp vec2 vUV;
varying mediump vec2 vUV1;
varying mediump vec2 vUV2;
varying mediump vec2 vUV3;
varying mediump vec2 vUV4;

uniform sampler2D texture0;
uniform sampler2D texture1;

uniform lowp sampler2D perlinTexture;
uniform lowp vec4 mistColor;
uniform highp float mistIntensity;

void main()
{	   
	highp vec4 c = vec4(0.);
	  
	// Input
	c = texture2D(texture0, vUV);

	lowp float perlin1 = texture2D(perlinTexture, vUV1).g;
	lowp float perlin2 = texture2D(perlinTexture, vUV2).b;
	lowp float perlin3 = texture2D(perlinTexture, vUV3).g;
	lowp float perlin4 = texture2D(perlinTexture, vUV4).b;
	
	lowp float perlin = max(0.0 ,(perlin1 + perlin2 + perlin3 + perlin4) * 1.5 / 4.0 - 0.5);
	c.rgb = mix(c.rgb, mistColor.rgb * mistIntensity, mistColor.a * perlin);
	gl_FragColor = c; 
}

