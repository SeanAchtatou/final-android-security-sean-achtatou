
//#version 300 es
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	out lowp vec4 glitch_FragColor;
	#define gl_FragColor glitch_FragColor
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

varying mediump vec2 vScreen;


uniform lowp sampler2D texture0;
#ifdef TEXTURE3D
    uniform sampler3D texture1;
#else
    uniform sampler2D texture1;
#endif
uniform mediump float amount;

void main()
{
    #ifdef MIX
        lowp vec3 color1 = texture2D(texture0, vScreen).rgb; 
        lowp vec3 color2 = texture2D(texture1, vScreen).rgb;
        lowp vec3 color = mix(color1, color2, amount); 
    #else
        lowp vec3 color = clamp(texture2D(texture0, vScreen),0.0,1.0).rgb;
        #ifndef TEXTURE3D 

            const mediump float cellSize = 16.0;
            const mediump float cellCount = 32.0;
            
            const mediump float texWidth = cellSize * cellCount; 
            const mediump float pixelSizeX = 1.0 / texWidth;
            const mediump float pixelSizeY = 1.0 / cellSize;
            const mediump float pixelHalfSizeX = 0.5 * pixelSizeX;
            const mediump float pixelHalfSizeY = 0.5 * pixelSizeY;
            
            mediump float r = color.r * (pixelSizeX * (cellSize - 1.0)) + pixelHalfSizeX;
            mediump float g = color.g * (pixelSizeY * (cellSize - 1.0)) + pixelHalfSizeY;
            #ifdef LINEAR_BLUE
                mediump float s = cellSize * pixelSizeX;
                mediump float f = color.b * (cellCount - 1.0);
                mediump float x = fract(f);
                mediump float b1 = floor(f) * s;
                mediump float b2 = b1 + s;
                mediump vec2 luv1 = vec2(r + b1, g);
                mediump vec2 luv2 = vec2(r + b2, g);
                lowp vec3 corrected1 = texture2D(texture1, luv1).rgb;
                lowp vec3 corrected2 = texture2D(texture1, luv2).rgb;
                lowp vec3 corrected = mix(corrected1, corrected2, x).rgb;
                
            #else
                mediump float b = floor(color.b * (cellCount - 1.0)+0.5) * (cellSize * pixelSizeX);
                mediump vec2 luv = vec2(r + b,g);
                luv = clamp(luv,0.0,1.0);
                lowp vec3 corrected = texture2D(texture1, luv).rgb;
            #endif
            
        #else
            lowp vec3 corrected = texture3D(texture1, color.rgb).rgb;
        #endif	
        color = mix(color, corrected, amount);
    #endif
    //color = color * 0.000001 + vec3(r*32.0);
	gl_FragColor = vec4( color,1.0);	

	
}
