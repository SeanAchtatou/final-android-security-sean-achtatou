#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
#endif

// Varyings
varying mediump vec2 vScreen;


attribute highp vec4 position;
attribute highp vec2 texcoord0;
uniform highp mat4 WorldViewProjection;
uniform highp vec2 texelSize;

void main() 
{

	vScreen = texcoord0;//-0.1;//0.5;// -0.1;
	//vScreen = texcoord0 + texelSize * 0.38;//-0.1;//0.5;// -0.1;
	gl_Position = WorldViewProjection * position;
}
