#version 300 es
#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	out lowp vec4 glitch_FragColor;
	#define gl_FragColor glitch_FragColor
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif

highp vec2 octWrap(highp vec2 v)
{
    return (1.0 - abs(v.yx)) * sign(v);
}

highp vec2 dirToOct(highp vec3 d)
{
    d /= (abs(d.x) + abs(d.y) + abs(d.z));
    d.xz = d.y >= 0.0 ? d.xz : octWrap(d.xz);
    return d.xz * 0.5 + 0.5;
}

highp vec3 octToDir(highp vec2 v)
{
    v = v * 2.0 - 1.0;
    highp vec3 d;
    d.y = 1.0 - abs(v.x) - abs(v.y);
    d.xz = d.y >= 0.0 ? v : octWrap(v);
    return normalize(d);
}

// Max defines
#ifdef DCC_MAX
	#define SEM(x) : x
	#define COLOR_TYPE vec3
#else
	#define SEM(x)
	#define COLOR_TYPE vec4
#endif

#define PI 3.1415926535897932384626433832795

// Varyings
varying highp vec2 vUV;


uniform lowp samplerCube envMap;
uniform highp float envMapLod;
uniform highp vec2 viewportsize;
uniform highp float envMap_width;

highp vec2 borderWrap(highp vec2 v)
{
    if(v.x >= 0.0 && v.x <= 1.0)
    {
        if(v.y < 0.0)
        {
            v.x = 1.0 - v.x;
            v.y = abs(v.y);
        }
        else if(v.y > 1.0)
        {
            v.x = 1.0 - v.x;
            v.y = 2.0 - v.y;
        }
    }
    else if(v.y >= 0.0 && v.y <= 1.0)
    {
        if(v.x < 0.0)
        {
            v.y = 1.0 - v.y;
            v.x = abs(v.x);
        }
        else if(v.x > 1.0)
        {
            v.y = 1.0 - v.y;
            v.x = 2.0 - v.x;
        }
    }
    else
    {
        if(v.x < 0.0)
            v.x += 1.0;
        else if(v.x > 1.0)
            v.x -= 1.0;
        
        if(v.y < 0.0)
            v.y += 1.0;
        else if(v.y > 1.0)
            v.y -= 1.0;
    }

    return v;
}

highp vec4 sampleEnvMap(samplerCube envMap, highp vec3 d, highp float lod)
{
    highp vec4 color =  textureLod(envMap, d, lod);
    color.rgb /= max(1. / 256., color.a);
    return color;
}

highp vec2 cartesianToSpherical(highp vec3 v)
{
    highp float inclination = acos(v.y);
	highp float azimuth = atan(v.x, v.z);
    return vec2(inclination, azimuth);
}

highp vec3 sphericalToCartesian(highp vec2 v)
{
    highp float z = sin(v.x) * cos(v.y);
    highp float x = sin(v.x) * sin(v.y);
    highp float y = cos(v.x);
    return vec3(x, y, z);
}

highp vec2 octWrapBW(highp vec2 v)
{
    return (1.0 - abs(v.yx)) * sign(v);
}

highp vec3 octToDirBW(highp vec2 v)
{
    highp vec3 d;
    d.y = 1.0 - abs(v.x) - abs(v.y);
    d.xz = d.x < 0.0 ? v : octWrapBW(v);
    return normalize(d);
}


highp vec3 octToDirBorderWrap(highp vec2 v)
{

    highp vec2 p = v * 2.0 - 1.0;

    if(p.x < 0.0)
    {
        if(p.y < 0.0)
        {
            return octToDirBW(p);
        }
    }

    return octToDir(v);
}

void main()
{

    highp float offset = 0.5 / viewportsize.x;

    highp vec4 accum = vec4(0.0);
    //highp vec3 dir = octToDirBorderWrap(vUV);

    accum += sampleEnvMap(envMap, octToDir(borderWrap(vUV + vec2(-offset, -offset))), envMapLod);
    accum += sampleEnvMap(envMap, octToDir(borderWrap(vUV + vec2(offset, -offset))), envMapLod);
    accum += sampleEnvMap(envMap, octToDir(borderWrap(vUV + vec2(-offset, offset))), envMapLod);
    accum += sampleEnvMap(envMap, octToDir(borderWrap(vUV + vec2(offset, offset))), envMapLod);
    accum /= 4.0;

    // Encode in RGBDiv8
    highp float maxValue = max(max(1., accum.r), max(accum.g, accum.b));
    accum.rgb /= maxValue;
    accum.a = 1. / maxValue;

    gl_FragColor = accum;
}
