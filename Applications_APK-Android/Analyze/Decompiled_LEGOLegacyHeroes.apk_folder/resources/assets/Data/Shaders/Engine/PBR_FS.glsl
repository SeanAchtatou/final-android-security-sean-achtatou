#version 300 es


#ifdef USE_SHADER_FRAMEBUFFER_FETCH
    #if defined(GL_EXT_shader_framebuffer_fetch)
        #extension GL_EXT_shader_framebuffer_fetch : enable
        #define USE_LAST_FRAG_DATA
    #elif defined GLSL2METAL
        #define USE_LAST_FRAG_DATA
    #endif
#endif

#if defined GL_ES && __VERSION__ >= 300
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture3D texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif defined GL_ES // Must be OpenGL ES 2.0 (__VERSION__ == 100). See https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions
	#extension GL_EXT_shader_texture_lod :enable
	#define texture2DLod texture2DLodEXT
	#define textureCubeLod textureCubeLodEXT
	#extension GL_EXT_shadow_samplers :enable
	#define shadow2D shadow2DEXT
	#extension GL_OES_texture_3D :enable
    #define sampler2DArray sampler2D
#elif !defined(GL_ES) && __VERSION__ > 120
    #ifndef MAX_DRAW_BUFFERS
        #if defined USE_SHADER_FRAMEBUFFER_FETCH
            #define MAX_DRAW_BUFFERS 1        
        #elif defined WIN32 || defined GLSL2METAL
            #define MAX_DRAW_BUFFERS 8
        #else
            #define MAX_DRAW_BUFFERS 4
        #endif
    #endif
	#define varying in
	#define texture2D texture
	#define texture2DLod textureLod
	#define textureCubeLod textureLod
	#define shadow2D texture
	#define textureCube texture
	#ifdef USE_LAST_FRAG_DATA
        #if MAX_DRAW_BUFFERS == 1
            inout mediump vec4 fragData;
            #define gl_FragColor fragData
        #else
            inout mediump vec4 fragData[MAX_DRAW_BUFFERS];
            #define gl_FragColor fragData[0]
        #endif
	#else
		out mediump vec4 fragData[MAX_DRAW_BUFFERS];
        #define gl_FragColor fragData[0]
	#endif
    #ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif __VERSION__ < 130
	#extension GL_ARB_shader_texture_lod :enable
#else
	#define shadow2D texture
	#define texture3D texture
#endif
varying mediump float vExposure;

mediump vec3 CancelExposure(mediump vec3 color)
{	  
	return color / vExposure;
}

mediump vec3 ApplyExposure(mediump vec3 color)
{	  
	return color * vExposure;
}


// Force disable texture array for lightmaps
#ifdef USE_TEXTURE_ARRAY
    #undef USE_TEXTURE_ARRAY
    #define sampler2DArray sampler2D
#endif

#if defined SUPERMIX && !defined MIX
	#define MIX
#endif

#if defined VERTEX_COLOR_MASK && !defined ROME_MASK
	#define ROME_MASK
#endif 

#if defined DECAL && !defined ROME_MASK
	#define ROME_MASK
#endif

#if defined DECAL_MASK && defined CHROMAFLAIR
	#error DECAL_MASK and CHROMAFLAIR are mutually exclusive.
#endif 

#ifndef GAMMA_UNIFORM
	#define colorp lowp
#else 
	#define colorp mediump
#endif

#ifdef LOW_END_1TEXTURE_EMUL
    #define LOW_END_1TEXTURE
#endif

#ifdef LOW_END_1TEXTURE
    #define LOW_END
#endif

#ifdef LOW_END
    #undef IBL_MANUAL_ENV_MAP
    #undef SUPERMIX
    #undef MIX
    #undef ROME_MASK
    #undef CHROMAFLAIR
    #undef USE_SHADOW_MAP
    #undef SPECULAR_AA_HACK
    #ifndef LOW_END_SPECULAR
        #ifdef IRRADIANCE
            #undef NORMAL_MAPPING
        #else
            #define TRIVIAL_VS
        #endif
    #endif
    #ifdef IRRADIANCE
        #define VIEW_DEPENDENT_OUTLINE
    #endif
#else
    #undef LOW_END_SPECULAR
    #ifndef NORMAL_MAPPING
        #define NORMAL_MAPPING
    #endif
#endif

#if defined TRIVIAL_VS || defined OUTLINE
    #undef NORMAL_MAPPING
    #undef IRRADIANCE
#endif


varying lowp vec4 vColor;
varying highp vec3 vPosition;
varying mediump vec3 vNormal;
#ifdef NORMAL_MAPPING
varying mediump vec3 vTangent;
varying mediump vec3 vBinormal;
#endif
varying highp vec2 vTexBase;
#ifdef LIGHTMAP
#ifndef LOW_END
//centroid
#endif
#ifdef USE_TEXTURE_ARRAY
varying highp vec3 vTexLightMap;
#else
varying highp vec2 vTexLightMap;
#endif
#endif
#ifdef DECAL
varying highp vec2 vTexDecal;
#endif
#ifdef MIX
varying highp vec2 vTexMix;
#endif
#ifdef SUPERMIX
varying mediump vec2 vBlendEdges;
#endif
#ifdef FOG
varying mediump vec4 vFogColor;
#endif
#if defined LOW_END && defined IRRADIANCE
varying mediump vec3 vDiffuseLight;
#endif

#if defined OUTLINE || defined VIEW_DEPENDENT_OUTLINE
uniform mediump vec4 outlineColor;
varying mediump vec4 vOutlineColor;
#endif

#ifdef IRRADIANCE
	uniform mediump float irradianceArray [27];
	mediump vec3 irradiance(mediump vec3 normal3)
	{

		mediump vec4 cAr = vec4(irradianceArray[0],  irradianceArray[1],  irradianceArray[2],  irradianceArray[3] );
		mediump vec4 cAg = vec4(irradianceArray[4],  irradianceArray[5],  irradianceArray[6],  irradianceArray[7] );
		mediump vec4 cAb = vec4(irradianceArray[8],  irradianceArray[9],  irradianceArray[10], irradianceArray[11]);
		mediump vec4 cBr = vec4(irradianceArray[12], irradianceArray[13], irradianceArray[14], irradianceArray[15]);
		mediump vec4 cBg = vec4(irradianceArray[16], irradianceArray[17], irradianceArray[18], irradianceArray[19]);
		mediump vec4 cBb = vec4(irradianceArray[20], irradianceArray[21], irradianceArray[22], irradianceArray[23]);
		mediump vec3 cC =  vec3(irradianceArray[24], irradianceArray[25], irradianceArray[26]);
	
		mediump vec4 normal = vec4(normal3, 1.0);
		//normal.z = -normal.z;
	
	    mediump vec3 x1, x2, x3;
	    
	    // Linear + constant polynomial terms
	    x1.r = dot(cAr,normal);
	    x1.g = dot(cAg,normal); 
	    x1.b = dot(cAb,normal);
	    
	    // 4 of the quadratic polynomials
	    mediump vec4 vB = normal.xyzz * normal.yzzx;   
	    x2.r = dot(cBr,vB);
	    x2.g = dot(cBg,vB);
	    x2.b = dot(cBb,vB);
	   
	    // Final quadratic polynomial
	    mediump float vC = normal.x*normal.x - normal.y*normal.y;
	    x3 = cC.rgb * vC;    
	    mediump vec3 irr = x1 + x2 + x3;
	    
      	#if defined(EFFECT_EDITOR) || defined(GLITCH_EDITOR)
        	irr = irr * 0.0001 + vec3(1.);        		
      	#endif
        
      	return irr;
	}
	
    mediump vec3 irradianceL0()
    {
        return vec3(
            irradianceArray[3] + irradianceArray[14] * 1.0 / 3.0,
            irradianceArray[7] + irradianceArray[18] * 1.0 / 3.0,
            irradianceArray[11] + irradianceArray[22] * 1.0 / 3.0);
    }
#endif


#ifdef USE_SHADOW_MAP
#define SHADOW_MAP_CASCADE_COUNT 2
varying highp vec3 vSMCoord0;
//varying highp vec3 vSMCoord1;
//varying highp float vViewZ;
#endif

#ifdef USE_SHADOW_MAP
#ifdef GLSL2METAL
GLITCH_UNIFORM_PROPERTIES(global_ShadowMapSampler, (samplerCompareFunc = greater, samplerFilter = linear))
#endif
uniform highp sampler2DShadow global_ShadowMapSampler;
uniform highp vec2 global_ShadowMapScale;
uniform mediump vec3 global_ShadowColor;

//uniform highp vec4 global_SMSplits;
//uniform highp float global_SMIntersticeInvSize;

mediump float SampleShadowMap(mediump vec2 baseUV, mediump vec2 offset, highp float depth)
{
	return shadow2D(global_ShadowMapSampler, vec3(baseUV + offset * global_ShadowMapScale, depth));
}

mediump float SampleShadowMapPCF3x3(highp vec2 smCoord, highp float depth)
{
	highp vec2 uv = smCoord.xy / global_ShadowMapScale; 

	mediump vec2 baseUV;
    baseUV.x = floor(uv.x + 0.5);
    baseUV.y = floor(uv.y + 0.5);

    mediump float s = (uv.x + 0.5 - baseUV.x);
    mediump float t = (uv.y + 0.5 - baseUV.y);
	
 	baseUV -= vec2(0.5, 0.5);
    baseUV *= global_ShadowMapScale;
	
	mediump float sum = 0.0;

	mediump float uw0 = (3. - 2. * s);
    mediump float uw1 = (1. + 2. * s);

    mediump float u0 = (2. - s) / uw0 - 1.;
    mediump float u1 = s / uw1 + 1.;

    mediump float vw0 = (3. - 2. * t);
    mediump float vw1 = (1. + 2. * t);

    mediump float v0 = (2. - t) / vw0 - 1.;
    mediump float v1 = t / vw1 + 1.;

    sum += uw0 * vw0 * SampleShadowMap(baseUV, vec2(u0, v0), depth);
    sum += uw1 * vw0 * SampleShadowMap(baseUV, vec2(u1, v0), depth);
    sum += uw0 * vw1 * SampleShadowMap(baseUV, vec2(u0, v1), depth);
    sum += uw1 * vw1 * SampleShadowMap(baseUV, vec2(u1, v1), depth);

	return sum * 1.0 / 16.0;
}

mediump float GetShadowCoefficient()
{
    return SampleShadowMapPCF3x3(vSMCoord0.xy, clamp(vSMCoord0.z, 0.0, 1.0));
}
#endif

//#extension GL_EXT_shader_texture_lod :require

#ifdef ADRENO_SAMPLER_OPTIMITATION
#undef SUPERMIX
#endif

uniform lowp sampler2D Rome;
uniform lowp sampler2D BaseColor;
#ifdef SPLIT_ALPHA
uniform lowp sampler2D Rome_alpha;
uniform lowp sampler2D BaseColor_alpha;
#endif
uniform lowp sampler2D NormalMap;

#if defined SUPERMIX || defined MIX
uniform lowp sampler2D RomeMix;
uniform lowp sampler2D BaseColorMix;
uniform lowp sampler2D NormalMapMix;
#endif
#ifdef DECAL
uniform lowp sampler2D BaseColorDecal;
#ifdef SPLIT_ALPHA
uniform lowp sampler2D BaseColorDecal_alpha;
#endif
#endif
#ifdef SUPERMIX
uniform lowp sampler2D BlendHeight;
uniform mediump vec2 blendHeightTiling;
#endif


#ifdef LIGHTMAP
    uniform lowp sampler2DArray lightMap;
    #ifdef SPLIT_ALPHA
    uniform lowp sampler2DArray lightMap_alpha;
    #endif
    #ifdef EDITOR_LIGHTMAP_UV
        uniform highp float lightMap_width;
    #endif
#endif
#ifdef LIGHTMAP_DIR
    uniform mediump vec3 global_dominantLightIntensity;
    #ifdef EDITOR
        uniform bool global_realtimeRadiosity;
        uniform mediump vec3 global_dominantLightDir;
    #endif
    uniform lowp sampler2DArray lightMapDir;
    #ifdef SPLIT_ALPHA
    uniform lowp sampler2DArray lightMapDir_alpha;
    #endif
#endif

#ifdef EDITOR
uniform highp float BaseColor_width; 

uniform sampler2D global_qualitativeMapTexture; 
uniform sampler2D global_sequentialMapTexture; 
uniform sampler2D global_divergingMapTexture; 

uniform highp float global_minDepthRenderDistance; 
uniform highp float global_maxDepthRenderDistance; 
uniform highp float global_worldTexelDensity; 
uniform highp float global_worldTexelDensityScale;
#endif

#if defined TINT || defined CHROMAFLAIR || defined DECAL_MASK
uniform lowp sampler2D TintMap;
#ifdef SPLIT_ALPHA
uniform lowp sampler2D TintMap_alpha;
#endif
#endif

#ifdef BASE_REFLECTIVITY
uniform lowp vec3 baseReflectivity;
#else
const lowp vec3 baseReflectivity = vec3(0.04, 0.04, 0.04);
#endif
uniform mediump sampler2D global_brdfTable;

const mediump float Pi = 3.14159;
const mediump float Tau = Pi * 2.0;

#ifdef CHROMAFLAIR
uniform colorp vec3 ChromaColor;
uniform mediump float chromaPower;
#endif

#ifdef TINT
uniform colorp vec3 tintColor0;
uniform colorp vec3 tintColor1;
uniform colorp vec3 tintColor2;
#endif

uniform highp vec3 eyepos;

#ifdef EDITOR
uniform mediump float global_envMapGen;
uniform mediump float global_envMapEmissivityScale;
#endif

#ifdef IBL_MANUAL_ENV_MAP
	uniform highp mat4 IBLWorldToProxyBox;
	uniform highp vec3 IBLEnvMapPos;
	
	uniform lowp samplerCube IBLEnvMap;
	//uniform mediump float IBLEnvMapMaxMipLevel;
    uniform mediump vec3 IBLEnvMapNormFactor;
	
	uniform mediump float IBLBoxProjection;
	
	highp vec3 boxProjection(mediump vec3 reflVec)
	{
		highp vec3 RayLS = mat3(IBLWorldToProxyBox) * reflVec;
		highp vec3 PositionLS = (IBLWorldToProxyBox * vec4(vPosition, 1.)).xyz;
			
		highp vec3 Unitary = vec3(1.0, 1.0, 1.0);
		highp vec3 FirstPlaneIntersect  = (Unitary - PositionLS) / RayLS;
		highp vec3 SecondPlaneIntersect = (-Unitary - PositionLS) / RayLS;
		highp vec3 FurthestPlane = max(FirstPlaneIntersect, SecondPlaneIntersect);
		highp float Distance = min(FurthestPlane.x, min(FurthestPlane.y, FurthestPlane.z));
			
		// Use Distance in WS directly to recover intersection
		highp vec3 IntersectPositionWS = vPosition + reflVec * Distance;
		return IntersectPositionWS - IBLEnvMapPos;
	}
#else
	uniform lowp samplerCube global_envMap;
	//uniform mediump float global_envMapMaxMipLevel;
    uniform mediump vec3 global_envMapNormFactor;
#endif

mediump float roughnessToMipLevel(lowp float roughness, mediump float mipcount)
{
    return log2(roughness) + mipcount;
}

#ifndef LOW_END
mediump vec3 sampleEnvMap(lowp samplerCube envMap, mediump vec3 dir, mediump float level)
{
	mediump vec4 f = textureCubeLod(envMap, dir, level);
	f.rgb = f.rgb / max(1. / 256., f.a);
	return f.rgb;
}
#endif

mediump vec3 diffuseLight(mediump vec3 dir)
{
#ifdef IRRADIANCE
    #ifdef LOW_END
	    return vDiffuseLight;
    #else
        return irradiance(dir);
    #endif
#else
    return vec3(0.0);
#endif
}

#ifdef ROME_MASK
uniform mediump float roughnessMaskScale;
uniform mediump float roughnessMaskBias;
uniform mediump float metalnessMaskScale;
uniform mediump float metalnessMaskBias;
#endif

#ifdef EMISSIVE
uniform colorp vec3 EmissiveColor;
uniform mediump float EmissiveIntensity;
#endif

#ifndef LOW_END
mediump float G_Smith(mediump float roughness, mediump float VoN, mediump float NoL)
{
	mediump float k = ((roughness + 1.0) * (roughness + 1.0)) / 8.0;
    //highp float k = (roughness * roughness)/2.0;//*GameMath.Sqrt(2.0f/GameMath.Pi);
    mediump float Gv = VoN / (VoN * (1.0 - k) + k);
    mediump float Gl = NoL / (NoL * (1.0 - k) + k);
    return Gv * Gl;
}

mediump vec3 F_Schlick_Gauss(mediump vec3 F0, mediump float VoH)
{
	 return F0 + (1.0 - F0) * exp2((-5.55473*VoH - 6.98316)*VoH);
}

uniform mediump float global_light1Intensity;
uniform colorp vec3 global_light1Color;
uniform highp vec3 global_light1Position;
uniform mediump float global_light1Radius;
uniform mediump float global_light1Enable;
#endif

#ifdef LIGHTMAP_DIR
	mediump float RelightSurface(mediump vec3 direction, mediump vec3 radiosityNormal, mediump vec3 normal)
	{
		direction += direction - radiosityNormal * dot(radiosityNormal, direction);
		direction *= inversesqrt(dot(direction, direction) + 0.0001);

	    // ----- relighting normal
	    // compute dot product with computed centres of distributions
	    mediump float radNormDotVI = dot(radiosityNormal, direction);
	    mediump float lambda = min(radNormDotVI + 1.0, 1.0);
	    mediump float q = radNormDotVI * 0.5 + 0.5;
        mediump float q2 = q * q;

        const mediump float c = 10.0 / 3.0;
	    mediump float radNormVal = lambda * (c * q2 * (2.0 * q - q2) - 1.0) + 1.0;

	    // compute dot product with computed centres of distributions
	    mediump float shadingNormDotVI = dot(normal, direction);
	    q = shadingNormDotVI * 0.5 + 0.5;
        q2 = q * q;
	    mediump float shadingNormVal = lambda * (c * q2 * (2.0 * q - q2) - 1.0) + 1.0;
	    
	    return shadingNormVal / radNormVal;
	 }
#endif

#ifdef IRRADIANCE_DIRECTIONAL_LIGHT
uniform mediump vec3 irradianceDomLightIntensity;
uniform mediump vec3 irradianceDomLightDir;
#endif

void main()
{
#ifdef TRIVIAL_FS
    gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
#elif defined OUTLINE
    gl_FragColor = vOutlineColor;
#else
	#if defined TINT || defined CHROMAFLAIR || defined DECAL_MASK
		lowp vec4 tintMap = texture2D(TintMap, vTexBase);
        #ifdef SPLIT_ALPHA
        tintMap.a = texture2D(TintMap_alpha, vTexBase).r;
        #endif
	#endif

	// Mix
	#ifdef SUPERMIX
		#ifdef BLENDMAP_UV_MIX
			highp vec2 texBlendMap = vTexMix * blendHeightTiling;
		#else 
			highp vec2 texBlendMap = vTexBase * blendHeightTiling;
		#endif
		
		lowp float blendMap = texture2D(BlendHeight, texBlendMap).r;
		lowp float blend = clamp((blendMap - vBlendEdges.x) * vBlendEdges.y, 0.0, 1.0);
	#elif defined MIX
		lowp float blend = vColor.a;
	#endif

 	// Base Color
    lowp vec4 bc1 = texture2D(BaseColor, vTexBase);
    #if !defined LOW_END_1TEXTURE || defined LOW_END_1TEXTURE_EMUL
    lowp vec4 rome = texture2D(Rome, vTexBase).rgba;
    #endif

    #ifdef SPLIT_ALPHA
    bc1.a = texture2D(BaseColor_alpha, vTexBase).r;
    #if !defined LOW_END_1TEXTURE || defined LOW_END_1TEXTURE_EMUL
    rome.a = texture2D(Rome_alpha, vTexBase).r;
    #endif
    #endif

	lowp vec4 bc = bc1;
    
    #ifdef LOW_END_1TEXTURE_EMUL
        bc.rgb *= rome.g;
        bc.a *= rome.a;
    #endif

	#ifdef DECAL
		lowp vec4 bcDecal = texture2D(BaseColorDecal, vTexDecal);
        #ifdef SPLIT_ALPHA
        bcDecal.a = texture2D(BaseColorDecal_alpha, vTexDecal).r;
        #endif
		lowp float decalAlpha = bcDecal.a;
		#ifdef DECAL_MASK
			decalAlpha *= tintMap.a;
		#endif
		bc = mix(bc1, bcDecal, decalAlpha);
	#endif
	
	#if defined SUPERMIX || defined MIX
		lowp vec4 bcMix = texture2D(BaseColorMix, vTexMix);
		bc = mix(bc, bcMix, blend);
	#endif	
	
	lowp vec3 baseColor = bc.rgb;
	
	#if defined MIX 
		lowp float opacity = 1.0;
    #else
        lowp float opacity = bc.a * vColor.a;
	#endif
    
	#ifdef VERTEX_COLOR
		baseColor *= vColor.rgb;
	#endif

    // Normal
    #ifdef NORMAL_MAPPING
        #if defined PACKED_NORMAL_MAP
            lowp vec2 normalSample1 = texture2D(NormalMap, vTexBase).ag;
            #ifdef SUPERMIX
                lowp vec2 normalSampleMix = texture2D(NormalMapMix, vTexMix).ag;
                mediump vec2 normalXY = mix(normalSample1, normalSampleMix, blend);
            #else
                mediump vec2 normalXY = normalSample1;
            #endif
            normalXY = normalXY * 2.0 - 1.0;
            mediump vec3 normalSample = vec3(normalXY, sqrt(max(1.0 - dot(normalXY, normalXY), 0.0)));
        #else
            lowp vec3 normalSample1 = texture2D(NormalMap, vTexBase).rgb;
            #ifdef SUPERMIX
                lowp vec3 normalSampleMix = texture2D(NormalMapMix, vTexMix).rgb;
                mediump vec3 normalSample = mix(normalSample1, normalSampleMix, blend);
            #else
                mediump vec3 normalSample = normalSample1;
            #endif
            normalSample = normalSample * 2.0 - 1.0;
        #endif

        mediump mat3 tangentToWorld = mat3(vTangent, vBinormal, vNormal);
        mediump vec3 normal = tangentToWorld * normalSample;

        mediump float normalLen2 = dot(normal, normal);
        mediump vec3 bNormal = normalize(vNormal);
        normal = normalLen2 > 0.0 ? normal * inversesqrt(normalLen2) : bNormal;
    #else
        mediump vec3 normal = normalize(vNormal);
    #endif

    #if !defined LOW_END || defined LOW_END_SPECULAR
        mediump vec3 V = normalize(eyepos - vPosition);
        mediump float VoN = clamp(dot(normal, V), 0.0001, 1.0);
    #endif

    #ifndef LOW_END_1TEXTURE
        // Rome
		#ifdef SUPERMIX
			lowp vec4 romeMix = texture2D(RomeMix, vTexMix).rgba;
			rome = mix(rome, romeMix, blend);
		#endif
		lowp float roughness = rome.r;
		lowp float occlusion = rome.g;
		lowp float metalness = rome.b;
		lowp float emissivity = rome.a;
    #else
        lowp float occlusion = 1.0; // Already combined in bc
        lowp float emissivity = bc.a;
        lowp float metalness = 0.0;
        lowp float roughness = 0.25;
    #endif

	lowp float romeMask = 0.0;
	#ifdef VERTEX_COLOR_MASK
		romeMask = bc.a;
		
		lowp vec3 vc_b2 = baseColor * 2.0;
		lowp vec3 vc_b2_1 = vc_b2 - 1.0;
		lowp vec3 vc_black = mix(vec3(0,0,0), vColor.rgb, vc_b2);
		lowp vec3 vc_white = mix(vColor.rgb, vec3(1,1,1), vc_b2_1);
		lowp vec3 vc_overlay = mix(vc_black, vc_white, step(0.5, baseColor));
		
		baseColor = mix(baseColor, vc_overlay.rgb, bc.a);
	#endif
	
	#ifdef DECAL
		romeMask = decalAlpha;
	
		#ifdef SUPERMIX
			romeMask *= 1.0-blend;
		#endif
	#endif
	
	#ifdef ROME_MASK
		roughness = clamp(mix(roughness, roughness * roughnessMaskScale + roughnessMaskBias, romeMask), 0.0, 1.0);
		metalness = clamp(mix(metalness, metalness * metalnessMaskScale + metalnessMaskBias, romeMask), 0.0, 1.0);
	#endif

	// Tint
	#ifdef TINT
		lowp vec3 tintAmount = tintMap.rgb;
		#ifdef DECAL_MASK
		 tintAmount.rgb *= (1.0-tintMap.a);
		#endif
		lowp vec3 tintColor = vec3(0.5,0.5,0.5);
		colorp vec3 tint0 = tintColor0;
		colorp vec3 tint1 = tintColor1;
		colorp vec3 tint2 = tintColor2;
		#ifdef GAMMA_UNIFORM
			tint0 = pow(tint0.rgb, vec3(2.2));
			tint1 = pow(tint1.rgb, vec3(2.2));
			tint2 = pow(tint2.rgb, vec3(2.2));
		#endif
		tintColor = mix(tintColor, tint0.rgb, tintAmount.r);
		tintColor = mix(tintColor, tint1.rgb, tintAmount.g);
		tintColor = mix(tintColor, tint2.rgb, tintAmount.b);

		colorp vec3 b2 = baseColor * 2.0;
		colorp vec3 b2_1 = b2 - 1.0;
		colorp vec3 black = mix(vec3(0,0,0), tintColor.rgb, b2);
		colorp vec3 white = mix(tintColor.rgb, vec3(1,1,1), b2_1);
		colorp vec3 overlay = mix(black, white, step(0.5, baseColor));
		baseColor = overlay;
	#endif


	// Chroma flair
	#ifdef CHROMAFLAIR
		colorp vec3 chromaColor = ChromaColor;
		#ifdef GAMMA_UNIFORM
			chromaColor = pow(chromaColor, vec3(2.2));
		#endif
		colorp vec3 chromaAmount = tintMap.a * pow(VoN,chromaPower) * chromaColor;
		
		lowp vec3 diffuseBaseColor = clamp(baseColor + chromaAmount,0.0,1.0);
	#else
		lowp vec3 diffuseBaseColor = baseColor;
	#endif

    // Diffuse
	#ifdef LIGHTMAP
        mediump vec4 lm = texture2D(lightMap, vTexLightMap);
        #ifdef LOW_END
            mediump float luma = dot(lm.rgb, vec3(0.2126, 0.7152, 0.0722));
            mediump vec3 lmIrr = lm.rgb / (1.0 - luma * (1.0 - 1.0 / 4.0));
            mediump vec3 diffuseLight = lmIrr;
        #else
            #ifdef SPLIT_ALPHA
                lm.a = texture2D(lightMap_alpha, vTexLightMap).r;
            #endif

            mediump vec3 lmIrr = lm.rgb / max(1. / 256., lm.a);
            mediump vec3 diffuseLight = lmIrr;
		
		    #ifdef LIGHTMAP_DIR
                mediump vec4 lmDir = texture2D(lightMapDir, vTexLightMap);
                #ifdef SPLIT_ALPHA
                    lmDir.a = texture2D(lightMapDir_alpha, vTexLightMap).r;
                #endif

                #ifdef EDITOR
                    if(global_realtimeRadiosity)
                    {
                        mediump float NoL = clamp(dot(normal, -global_dominantLightDir), 0.0, 1.0);
                        #ifdef USE_SHADOW_MAP
                            NoL *= 1.0 - GetShadowCoefficient();
                        #endif
                        diffuseLight += NoL * global_dominantLightIntensity;
                    }
                    else
                #endif
                {
                    #ifdef USE_SHADOW_MAP
                        if(lmDir.a > 0.0)
                        {
                            lmDir.a *= 1.0 - GetShadowCoefficient();
                        }
                    #endif
                    diffuseLight += lmDir.a * global_dominantLightIntensity;
                }

                mediump vec3 localLightDir = lmDir.xyz * 2.0 - 1.0;
                mediump float H = 0.5 - dot(bNormal, normal) * 0.5;
                diffuseLight *= (H * diffuseBaseColor + (1. - H) * RelightSurface(localLightDir, bNormal, normal) * vec3(1.0));
             #endif
        #endif
	#else
		mediump vec3 diffuseLight = diffuseLight(normal);
	#endif

    #if !defined LOW_END || defined LOW_END_SPECULAR
		// Reflectivity
		mediump vec3 f0 = mix(baseReflectivity, baseColor, metalness);
	    
		#if defined EDITOR_PLASTIC
			metalness = 0.0;
			roughness = 0.3;
			baseColor = vec3(0.20);
			diffuseBaseColor = vec3(0.20);
			occlusion = 1.0;
			f0 = vec3(0.1);
		#endif

		// IBL: Compute second sum for pre-filtered environment map using LUT
		mediump vec2 brdf = texture2D(global_brdfTable, vec2(VoN, roughness)).rg;
		mediump vec3 reflectivity = brdf.r * f0 + vec3(brdf.g);

        #if defined SPECULAR_AA_HACK && defined NORMAL_MAPPING
            // Hack : To reduce aliasing when normal varies a lot.
            mediump float perturb = clamp(1.0 - dot(normalSample, normalSample), 0.0, 1.0);
            roughness = mix(roughness, 1.0, perturb);
        #endif

        // + Reflected
        mediump vec3 reflected = reflect(-V, normal);

        mediump vec3 diffuseColor = diffuseBaseColor * (1.0 - reflectivity) * (1.0 - metalness);
	#else
        mediump vec3 diffuseColor = diffuseBaseColor;
    #endif

	// Specular
	mediump vec3 specular = vec3(0.0);

    mediump vec3 diffuseAmbient;
	#ifdef IRRADIANCE
		diffuseAmbient = irradianceL0();
	#else
		diffuseAmbient = diffuseLight;
	#endif

	#ifndef LOW_END
	#ifdef EDITOR
		if(global_envMapGen != 0.0)
		{
			specular = diffuseAmbient * reflectivity;
		}
		else
	#endif
        {
            mediump vec3 prefilter;
        #ifdef IBL_MANUAL_ENV_MAP
            if(IBLBoxProjection != 0.0)
                prefilter = sampleEnvMap(IBLEnvMap, boxProjection(reflected), roughnessToMipLevel(roughness, 4.0));
            else
                prefilter = sampleEnvMap(IBLEnvMap, reflected, roughnessToMipLevel(roughness, 4.0));
            prefilter *= diffuseAmbient * IBLEnvMapNormFactor;
        #else
            prefilter = sampleEnvMap(global_envMap, reflected, roughnessToMipLevel(roughness, 4.0));
            prefilter *= diffuseAmbient * global_envMapNormFactor;
        #endif
            specular = prefilter * reflectivity;
        }
    #elif defined LOW_END_SPECULAR
        specular += textureCube(global_envMap, reflected).rgb * global_envMapNormFactor * reflectivity * diffuseAmbient;
    #endif

    #ifndef LOW_END
        if(global_light1Enable != 0.0)
        {
            mediump vec3 light1Specular = vec3(0.0);
            
            highp vec3 L2 = global_light1Position - vPosition;
            mediump vec3 L = normalize(L2);
            
            mediump vec3 l1Color = global_light1Color * global_light1Intensity / dot(L2, L2);
            mediump float NoL = clamp(dot(normal, L), 0.0, 1.0);
            mediump float alpha = max(0.001, roughness);
            mediump vec3 light1Diffuse = NoL * l1Color;
            
            #ifdef LIGHT1_RADIUS
                highp vec3 centerToRay = L2 - dot(L2, reflected) * reflected;
                highp vec3 closest = L2 - centerToRay * clamp(global_light1Radius/length(centerToRay),0.0,1.0);
                L = normalize(closest);
                NoL = clamp(dot(normal, L), 0.0, 1.0);
                highp float alpha_ = clamp(alpha + (global_light1Radius/3.0 * length(L2)),0.0,1.0);
            #endif
            
            mediump vec3 h = normalize(L + V);
            
            mediump float NoH = clamp(dot(normal, h),0.0,1.0);
            mediump float VoH = clamp(dot(V, h),0.0,1.0);

            //highp float D = D_GGX(roughness, NoH);
            
            mediump float alpha2 = alpha * alpha;
            mediump float d = NoH * NoH * (alpha2 - 1.0) + 1.0;
            mediump float D = alpha2 / max(0.0001, (Pi * d * d));
            
            mediump float G = G_Smith(alpha, VoN, NoL);
            mediump vec3 F = F_Schlick_Gauss(f0, VoH);
            
            //highp float den = max(0.00001, 4.0 * NoL * VoN);
            light1Specular = (D * G * F) * l1Color;
            //light1Specular = vec3(D);
            //light1Specular *= 0.1;
            //l0Specular = pow(NoH,20.0/roughness) * l1Color;
        
            specular += light1Specular;
            diffuseLight += light1Diffuse;
        }

        #ifdef IRRADIANCE_DIRECTIONAL_LIGHT

            mediump vec3 light2Specular = vec3(0.0);
            
            mediump vec3 L = irradianceDomLightDir;
            
            mediump vec3 l2Color = irradianceDomLightIntensity;
            mediump float NoL = clamp(dot(normal, L), 0.0, 1.0);
            mediump float alpha = max(0.001, roughness);
            mediump vec3 light2Diffuse = NoL * l2Color;
            
            mediump vec3 h = normalize(L + V);
            
            mediump float NoH = clamp(dot(normal, h),0.0,1.0);
            mediump float VoH = clamp(dot(V, h),0.0,1.0);

            mediump float alpha2 = alpha * alpha;
            mediump float d = NoH * NoH * (alpha2 - 1.0) + 1.0;
            mediump float D = alpha2 / max(0.0001, (Pi * d * d));
            
            mediump float G = G_Smith(alpha, VoN, NoL);
            mediump vec3 F = F_Schlick_Gauss(f0, VoH);
            
            light2Specular = (D * G * F) * l2Color;
        
            specular += light2Specular;
        
        #endif

	#endif // LOW_END

	#ifdef TRANSPARENT
		diffuseLight *= opacity;
	#endif
	
	// Combining diffuse and specular terms
	mediump vec3 color = diffuseColor.rgb * diffuseLight + specular;

	#if defined USE_SHADOW_MAP && !defined LIGHTMAP_DIR
		color *= mix(vec3(1.0), global_ShadowColor, GetShadowCoefficient());
	#endif
	
	#ifdef EMISSIVE
		mediump vec3 emissiveColor = EmissiveColor;
		#ifdef GAMMA_UNIFORM
			emissiveColor = pow(emissiveColor, vec3(2.2));
		#endif
		mediump vec3 baseEmissive = emissivity * EmissiveIntensity * emissiveColor;
		#ifdef EMISSIVE_MONO
			mediump vec3 emissive = baseEmissive;
		#else
			mediump vec3 emissive = baseColor * baseEmissive;
		#endif
	
		#ifdef EDITOR
			if(global_envMapGen != 0.0)
			{
				emissive *= global_envMapEmissivityScale;
			}
		#endif

		#ifdef EMISSIVE_PURE
			color = emissive;
		#else
			color += emissive;
		#endif
	#endif
	
	color *= occlusion;
	
	#ifdef FOG
		color = mix(color, vFogColor.rgb, vFogColor.a);
	#endif
	
	#ifdef CANCEL_EXPOSURE
		color = CancelExposure(color);
	#endif
#ifdef EDITOR
#if defined EDITOR_NORMAL_NORMALIZED
	color = normal*0.5+0.5;
	color = clamp(color, 0.0, 1.0);
	opacity = bc.a;
#elif defined EDITOR_NORMAL_POSITIVE
	color = normal;
	color = clamp(color, 0.0, 1.0);
	opacity = 1.0;
#elif defined EDITOR_NORMAL_NEGATIVE
	color = -normal;
	color = clamp(color, 0.0, 1.0);
	opacity = 1.0;
#elif defined EDITOR_BASECOLOR
	color = baseColor.rgb;
	opacity = bc.a;
#elif defined EDITOR_ROUGHNESS
	color = vec3(roughness);
	opacity = bc.a;
#elif defined EDITOR_OCCLUSION
	color = vec3(occlusion);
	opacity = 1.0;
#elif defined EDITOR_DIFFUSE
	color = vec3(diffuseLight);
	opacity = 1.0;
#elif defined EDITOR_SPECULAR
	color = vec3(specular);
	opacity = 1.0;
#elif defined EDITOR_METALNESS
	color = vec3(metalness);
	opacity = bc.a;
#elif defined EDITOR_VERTEX_COLOR
	color =  vColor.rgb;
	opacity = 1.0;
#elif defined EDITOR_VERTEX_COLOR_ALPHA
	color =  vColor.aaa;
	opacity = 1.0;
#elif defined EDITOR_BASE_UV
	color =  vec3(fract(vTexBase),0.0);
	opacity = 1.0;
#elif defined EDITOR_LIGHTMAP_UV
	highp float _f = mod(dot(floor(vTexLightMap.xy * lightMap_width), vec2(1.0)),2.0);
	lowp vec3 _tileColor = (_f * 0.65 + (1.0 - _f)) * vec3(fract(vTexLightMap.xy), 0.0);
	color = (lmIrr*0.75 + vec3(0.25)) * _tileColor;
	opacity = 1.0;
#elif defined EDITOR_LIGHTMAP
	color = lmIrr;
	opacity = 1.0;
#elif defined EDITOR_LIGHTMAP_DIRECTIONAL
	color = lmDir.xyz;
	opacity = 1.0;
#elif defined TRIVIAL_FS
	color = vec3(0.0);
	opacity = 1.0;
#elif defined ALBEDO_OUT
    color = baseColor.rgb;
    opacity = 1.0;
#elif defined EMISSIVE_OUT
	color = emissive;
	opacity= 1.0;
#elif defined EDITOR_WIREFRAME
	highp float _d = clamp(length(eyepos - vPosition)/30.0,0.0,1.0);
	color = vec3(0.8,0.8,0.8);
	opacity= mix(0.9, 0.1, _d);
	
#elif defined EDITOR_MIP_BASE
	highp float _mipCount = log2(BaseColor_width)+1.0;
	highp vec2 _dx = dFdx( vTexBase * BaseColor_width ); 
	highp vec2 _dy = dFdy( vTexBase * BaseColor_width );
	highp float _d = max( dot( _dx, _dx ), dot( _dy, _dy ) );
	highp float _mipLevel = clamp(log2(sqrt(_d)), 0.0, _mipCount);

	_mipLevel = floor(_mipLevel+0.5);   
	highp vec3 _mapColor = texture2D(global_sequentialMapTexture, vec2(_mipLevel/(_mipCount)+0.5/10.0,0)).rgb;
	
	highp float _levelSize = pow(2.0, _mipCount - _mipLevel - 1.0);
	highp float _f = mod(dot(floor(vTexBase * 0.25 * _levelSize), vec2(1.0)),2.0);
	highp float _tileColor = (_f * 0.65 + (1.0 - _f));
	
	color = texture2D(BaseColor, vec2(0,0)).rgb * 0.0001 + _mapColor * _tileColor;
	opacity= 1.0;
#elif defined EDITOR_MIP_BASE_LINEAR
	highp float _mipCount = log2(BaseColor_width)+1.0;
	highp vec2 _dx = dFdx( vTexBase * BaseColor_width ); 
	highp vec2 _dy = dFdy( vTexBase * BaseColor_width );
	highp float _d = max( dot( _dx, _dx ), dot( _dy, _dy ) );
	highp float _mipLevel = clamp(log2(sqrt(_d)), 0.0, _mipCount);

	highp float _mipLevel1 = floor(_mipLevel);   
	highp float _mipLevel2 = ceil(_mipLevel);
	highp float _mipLevelX = clamp(_mipLevel - _mipLevel1,0.0,1.0);
	
	highp vec3 _mapColor = texture2D(global_sequentialMapTexture, vec2(_mipLevel/(_mipCount)+0.5/10.0,0)).rgb;
	
	
	highp float _levelSize1 = pow(2.0, _mipCount - _mipLevel1 - 1.0);
	highp float _f1 = mod(dot(floor(vTexBase * 0.25 * _levelSize1), vec2(1.0)),2.0);
	highp float _tileColor1 = mix(0.6,1.0,_f1);

	highp float _levelSize2 = pow(2.0, _mipCount - _mipLevel2 - 1.0);
	highp float _f2 = mod(dot(floor(vTexBase * 0.25 * _levelSize2), vec2(1.0)),2.0);
	highp float _tileColor2 = mix(0.6,1.0,_f2);

	highp float _tileColor = mix(_tileColor1, _tileColor2, _mipLevelX);
	
	color = texture2D(BaseColor, vec2(0,0)).rgb * 0.0001 + _mapColor * _tileColor;
	//color = texture2D(BaseColor, vec2(0,0)).rgb * 0.0001 + texture2D(global_sequentialMapTexture, vTexBase).rgb;
	opacity= 1.0;
#elif defined EDITOR_DENSITY_WORLD
	highp float _dx = length(dFdx( vTexBase * BaseColor_width )); 
	highp float _dy = length(dFdy( vTexBase * BaseColor_width ));
	highp float _dpdx = length(dFdx( vPosition ))*100.0; 
	highp float _dpdy = length(dFdy( vPosition ))*100.0;
	
	highp float _Ap = _dpdx * _dpdy;
	highp float _At = _dx * _dy;
	highp float _d = log2(sqrt(_At/_Ap));
	
	//highp float mipLevel = clamp(log2(d), 0.0, mipCount);

	//highp float mipLevel1 = floor(mipLevel);   
	//highp float mipLevel2 = ceil(mipLevel);
	//highp float mipLevelX = clamp(mipLevel-mipLevel1,0.0,1.0);
	
	highp vec3 _mapColor = texture2D(global_divergingMapTexture, vec2(1.0,0.0)-vec2(0.5 + _d/10.0,0)).rgb;
	
	highp float _levelSize1 = BaseColor_width;
	highp float _f1 = mod(dot(floor(vec3(0.00183727)+vPosition * 10.0), vec3(1.0)),2.0);
	highp float _tileColor1 = mix(0.6,1.0,_f1);

//	highp float levelSize2 = pow(2.0, mipCount - mipLevel2 - 1.0);
//	highp float f2 = mod(dot(floor(vTexBase * 0.25 * levelSize2), vec2(1.0)),2.0);
//	highp float tileColor2 = mix(0.6,1.0,f2);

	highp float _tileColor = _tileColor1;
	
	color = texture2D(BaseColor, vec2(0,0)).rgb * 0.0001 + _mapColor * _tileColor;
	//color = texture2D(BaseColor, vec2(0,0)).rgb * 0.0001 + texture2D(global_sequentialMapTexture, vTexBase).rgb;
	opacity= 1.0;
#elif defined EDITOR_DENSITY_SCREEN
	highp float _mipCount = log2(BaseColor_width)+1.0;
	highp vec2 _dx = dFdx( vTexBase * BaseColor_width ); 
	highp vec2 _dy = dFdy( vTexBase * BaseColor_width );
	highp float _d = max( dot( _dx, _dx ), dot( _dy, _dy ) );
	highp float _mipLevel = clamp(log2(sqrt(_d)), 0.0, _mipCount);

	highp float _mipLevel1 = floor(_mipLevel);   
	highp float _mipLevel2 = ceil(_mipLevel);
	highp float _mipLevelX = clamp(_mipLevel - _mipLevel1,0.0,1.0);
	
	highp vec3 _mapColor = texture2D(global_divergingMapTexture, vec2(1.0,0.0)-vec2(0.5 + _d/10.0,0)).rgb;
	
	
	highp float _levelSize1 = pow(2.0, _mipCount - _mipLevel1 - 1.0);
	highp float _f1 = mod(dot(floor(vTexBase * 0.25 * _levelSize1), vec2(1.0)),2.0);
	highp float _tileColor1 = mix(0.6,1.0,_f1);

	highp float _levelSize2 = pow(2.0, _mipCount - _mipLevel2 - 1.0);
	highp float _f2 = mod(dot(floor(vTexBase * 0.25 * _levelSize2), vec2(1.0)),2.0);
	highp float _tileColor2 = mix(0.6,1.0,_f2);

	highp float _tileColor = mix(_tileColor1, _tileColor2, _mipLevelX);
	
	color = texture2D(BaseColor, vec2(0,0)).rgb * 0.0001 + _mapColor * _tileColor;
	//color = texture2D(BaseColor, vec2(0,0)).rgb * 0.0001 + texture2D(global_sequentialMapTexture, vTexBase).rgb;
	opacity= 1.0;
#elif defined EDITOR_DEPTH
    highp float _depth = length(vPosition-eyepos);

    color = vec3(clamp((_depth - global_minDepthRenderDistance) / (global_maxDepthRenderDistance - global_minDepthRenderDistance),0.0,1.0));    
#endif
#endif


    #ifdef VIEW_DEPENDENT_OUTLINE
        color = mix(color, vOutlineColor.rgb, vOutlineColor.a);
    #endif

	#ifdef GAMMA_OUT
		color = pow(color, vec3(0.45454));
	#endif

	gl_FragColor = vec4(color, opacity);
#endif // TRIVIAL_FS
}

