#ifdef DCC_MAX
	#define DCC(x) x
#else
	#define DCC(x)
#endif

#ifdef TEXTURE
	uniform lowp sampler2D AlbedoTex;
#endif

#ifdef OUTLINE
	uniform lowp vec4 OutlineColor;
#endif

varying mediump vec4 vColor DCC(:TEXCOORD0);
varying mediump vec2 vCoord0 DCC(:TEXCOORD1);
varying mediump vec3 vNormal DCC(:TEXCOORD2);

void main()
{
	#ifdef TEXTURE
		lowp vec4 color = texture2D(AlbedoTex, vCoord0);
	#else
   		lowp vec4 color = vColor;  
   	#endif
   	
   	#ifdef OUTLINE
   		color = OutlineColor;
   	#endif
      	
    #ifdef DIFFUSE_WRAP
		mediump float ndotl = dot( normalize(vNormal), vec3(0,1,0) );
		ndotl = ndotl * 0.5 + 0.5;
		color += ndotl;
	#endif
	
   	gl_FragColor = color;
   	  
} // main end
