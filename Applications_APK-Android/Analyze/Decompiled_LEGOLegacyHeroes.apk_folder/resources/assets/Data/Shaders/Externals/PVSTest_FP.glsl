#ifdef ALPHATEST
uniform lowp sampler2D texture;
#endif

#ifdef ALPHATEST
varying mediump vec2 uv;
#endif					    

void main() 
{
#ifdef ALPHATEST
	float alpha = texture2D(texture, uv);
	if (alpha < 0.5)
	{
		discard;
	}
#endif	

    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}