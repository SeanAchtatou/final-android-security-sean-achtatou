// Parameters
uniform lowp float TexelSize;

// Attributes
attribute highp vec4 Vertex;
attribute lowp vec2 TexCoord0;

// Varyings
varying lowp vec2 vCoord0;
varying lowp vec2 vCoord1;
varying lowp vec2 vCoord2;
varying lowp vec2 vCoord3;

void main(void) 
{
	gl_Position = Vertex * vec4(1.0, -1.0, 1.0, 1.0);
	// 3x3 gaussian filter
	lowp float offset = 0.5 * TexelSize;
	vCoord0 = TexCoord0 + vec2(-offset, -offset);
	vCoord1 = TexCoord0 + vec2( offset, -offset);
	vCoord2 = TexCoord0 + vec2( offset,  offset);
	vCoord3 = TexCoord0 + vec2(-offset,  offset);
}