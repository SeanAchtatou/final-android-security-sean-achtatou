#version 430

#define GROUP_SIZE 16
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE) in;

GLITCH_UNIFORM_PROPERTIES(stencilOut, (access = w))

layout(r8) uniform image2D stencilOut;
uniform usampler2D stencilIn;

void main()
{
    const ivec2 local_xy = ivec2(gl_LocalInvocationID);
    const ivec2 image_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE + local_xy;
    const ivec2 image_size = imageSize(stencilOut);
    
    const float value = texture(stencilIn, (image_xy + 0.5) / image_size).r;
    imageStore(stencilOut, image_xy, vec4(value));
}
