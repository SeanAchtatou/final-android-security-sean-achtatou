#version 430

#define GROUP_SIZE 4
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE, local_size_z = 6) in;

GLITCH_UNIFORM_PROPERTIES(colorTex, (access = rw))

layout(rgba8) uniform imageCube colorTex;

uniform int steps;
uniform int startScale;

void main()
{
    const ivec2 local_xy = ivec2(gl_LocalInvocationID);
    const ivec2 group_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE * 2;
    const ivec2 image_size = imageSize(colorTex);
    
    int scale = startScale;
    //Phase 1: in place reduction for the number of steps defined in uniform steps
    
    for(int i = 0; i < steps; ++i)
    {   
        const ivec3 origin = ivec3(group_xy + local_xy * scale * 2, gl_LocalInvocationID.z);
    
        if(all(lessThan(local_xy * scale, ivec2(GROUP_SIZE))) && all(lessThan(origin.xy, image_size)))
        {
            vec4 c = imageLoad(colorTex, origin);
            ivec3 o1 = ivec3(0, scale, 0);
            ivec3 o2 = ivec3(scale, 0, 0);
            ivec3 o3 = ivec3(scale, scale, 0);
            
            c += imageLoad(colorTex, origin + o1);
            c += imageLoad(colorTex, origin + o2);
            c += imageLoad(colorTex, origin + o3);
           
            imageStore(colorTex, origin, c * 0.25);
        }
   
        memoryBarrierImage();
        barrier();
        scale = scale * 2;
    }
}
