#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define attribute in
#    define varying out
#endif

attribute highp   vec4 Vertex;
attribute lowp    vec4 Color0;

uniform   highp   mat4 WorldViewProjectionMatrix;

varying	  lowp    vec4 vColor0;

void main(void)
{
	vColor0 = Color0;
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
