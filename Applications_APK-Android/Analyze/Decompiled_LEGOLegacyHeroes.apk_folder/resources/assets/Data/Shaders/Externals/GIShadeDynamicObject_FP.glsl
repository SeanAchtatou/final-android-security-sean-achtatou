// Compiler options	
//#define GI_EDITOR
//#define USE_ALPHA_MAP
//#define USE_NORMAL_MAP
//#define USE_BLURRED_SHADOW

// Shader options
#define USE_ALBEDO
#define USE_TEXTURE_NORMALIZATION
#define USE_SHADOW
#if !defined(GL_ES)
#	define USE_TWEAKERS
#endif

// Shadow options
#if defined(USE_SHADOW)
#	if !defined(USE_HW_SHADOW)
#		if defined(HIDE_GLSL_DEFINES)
#			define USE_HW_SHADOW 0 // Workaround to allow this shader to be compiled from the debugger
#		else
#			define USE_HW_SHADOW 1
#		endif
#	endif // !USE_HW_SHADOW
#	define USE_LISP_SHADOW 1
#endif // USE_SHADOW

#define optional_normalize

// Shadow sampler
#if defined(USE_SHADOW)
#	if USE_HW_SHADOW
#		if defined(GL_ES)
#			extension GL_EXT_shadow_samplers : require
#			define _shadow2D(sm, sv) shadow2DEXT(sm, sv)
#		else // defined(GL_ES)
#			define _shadow2D(sm, sv) shadow2D(sm, sv).r
#		endif // defined(GL_ES)
#if defined(GLSL2METAL)
GLITCH_UNIFORM_PROPERTIES(ShadowMap, (samplerCompareFunc = lessEqual, samplerAddress = clampToEdge, samplerFilter = linear))
#endif
uniform highp sampler2DShadow ShadowMap;
#	else // USE_HW_SHADOW
#		define _shadow2D(sm, sv) float(texture2D(sm, sv.xy).r > sv.z)
uniform sampler2D ShadowMap;
#	endif // USE_HW_SHADOW
#endif // USE_SHADOW

// Shading parameters
uniform highp vec4 CameraPosition;
uniform lowp vec3 Light0Direction;

// Diffuse sampler
uniform sampler2D diffuse;
#if defined(USE_NORMAL_MAP)
// Normal sampler
uniform sampler2D bump;
#endif // USE_NORMAL_MAP
#if defined(USE_ALPHA_MAP)
// Alpha sampler
uniform sampler2D alpha;
#endif // USE_ALPHA_MAP
#if defined(USE_TEXTURE_NORMALIZATION)
uniform samplerCube NormalizationCubeTex;
#endif // USE_TEXTURE_NORMALIZATION

// Tweakers
uniform lowp float DiffusemapBlendTweaker;
#if defined(USE_NORMAL_MAP)
uniform lowp vec3 SunColorTweaker;
uniform lowp float SunPowerTweaker;
uniform lowp float DiffuseScaleTweaker;
uniform lowp float SpecularScaleTweaker;
uniform lowp float SpecularPowerTweaker;
#endif // USE_NORMAL_MAP

// Varyings
#if defined(USE_NORMAL_MAP)
varying highp vec4 vWorldVertex;
varying lowp vec4 vWorldNormal;
varying lowp vec3 vWorldTangent;
varying lowp vec3 vWorldBinormal;
#endif // USE_NORMAL_MAP
varying lowp vec4 vCoord0and1;
#if defined(USE_SHADOW)
varying highp vec4 vShadowVertex;
#	if defined(USE_NORMAL_MAP)
#		define vShadowOpacity vWorldNormal.w
#	else // USE_NORMAL_MAP
#		define vShadowOpacity vDirectColor.w
#	endif // USE_NORMAL_MAP
#endif // USE_SHADOW
#if defined(USE_NORMAL_MAP)
varying lowp vec4 vBasisGI0;
varying lowp vec4 vBasisGI1;
varying lowp vec4 vBasisGI2;
#else // USE_NORMAL_MAP
varying lowp vec4 vDirectColor;
#endif // USE_NORMAL_MAP
varying lowp vec3 vPrecomputedColor;

// main
void main(void)
{  	
	// Get diffusemap uv set
	lowp vec2 vCoordDiffusemap = vCoord0and1.st;
#if defined(USE_ALBEDO)
	// Fetch albedo
	lowp vec3 albedo = texture2D(diffuse, vCoordDiffusemap).rgb;
#else
	const lowp vec3 albedo = vec3(1.0, 1.0, 1.0);
#endif // USE_ALBEDO
#if defined(USE_NORMAL_MAP)
	// Fetch bump
	lowp vec3 normalmap = texture2D(bump, vCoordDiffusemap).rgb;
	lowp vec3 normalTS = (normalmap - vec3(0.5, 0.5, 0.5));
#endif // USE_NORMAL_MAP
// Fetch alpha
#if defined(USE_ALPHA_MAP)
	lowp float alphaBlend = texture2D(alpha, vCoordDiffusemap).r;
#else
	const lowp float alphaBlend = 1.0;
#endif // USE_ALPHA_MAP
#if defined(USE_SHADOW)
	// Fetch shadow map
#	if defined(USE_LISP_SHADOW)
	// Compute vertex position in LS
	highp vec3 shadowVertex = vShadowVertex.xyz / vShadowVertex.w;
#	else // USE_LISP_SHADOW
	highp vec3 shadowVertex = vShadowVertex.xyz;
#	endif // USE_LISP_SHADOW	
#	if defined(USE_BLURRED_SHADOW)
	const highp float shadowOffset = 0.5 / 1024.0;
	highp vec3 shadowVertex0 = shadowVertex + vec3(-shadowOffset, -shadowOffset, 0.0);
	highp vec3 shadowVertex1 = shadowVertex + vec3( shadowOffset, -shadowOffset, 0.0);
	highp vec3 shadowVertex2 = shadowVertex + vec3( shadowOffset,  shadowOffset, 0.0);
	highp vec3 shadowVertex3 = shadowVertex + vec3(-shadowOffset,  shadowOffset, 0.0);
	lowp float visibility = 
		_shadow2D(ShadowMap, shadowVertex0) +
		_shadow2D(ShadowMap, shadowVertex1) +
		_shadow2D(ShadowMap, shadowVertex2) + 
		_shadow2D(ShadowMap, shadowVertex3);
	visibility *= 0.25;
#	else // USE_BLURRED_SHADOW
	lowp float visibility = _shadow2D(ShadowMap, shadowVertex);
#	endif // USE_BLURRED_SHADOW
	visibility *= vShadowOpacity;
#else // USE_SHADOW
	const lowp float visibility = 1.0;
#endif // USE_SHADOW
	// Get lightmap uv set
	lowp vec2 vCoordLightmap = vCoord0and1.pq;	
#if defined(USE_NORMAL_MAP)
	// Compute bumped normal
	lowp vec3 worldNormal = optional_normalize(vWorldNormal.xyz);
	lowp vec3 worldTangent = optional_normalize(vWorldTangent);
	lowp vec3 worldBinormal = optional_normalize(vWorldBinormal);	
	lowp vec3 bumpedNormal = normalize(normalTS.x * worldTangent + normalTS.y * worldBinormal + normalTS.z * worldNormal);
	// Basis weights
#	if defined(USE_TEXTURE_NORMALIZATION)
	lowp vec4 basisWeights = textureCube(NormalizationCubeTex, bumpedNormal);
#	else // USE_TEXTURE_NORMALIZATION	
	// Basis vectors
	const lowp vec3 basisUP1 = vec3(-1.0 / sqrt(6.0), -1.0 / sqrt(2.0),  1.0 / sqrt(3.0));
	const lowp vec3 basisUP2 = vec3(-1.0 / sqrt(6.0),  1.0 / sqrt(2.0),  1.0 / sqrt(3.0));
	const lowp vec3 basisUP3 = vec3( sqrt(2.0 / 3.0),              0.0,  1.0 / sqrt(3.0));	
	const lowp vec3 basisDW1 = vec3(             0.0,              0.0, 		    -1.0);
	const lowp vec4 basisScale = vec4(1.0, 1.0, 1.0, 0.5);
	const lowp vec4 basisBias = vec4(0.0, 0.0, 0.0, 0.5);	
	lowp vec4 basisWeights = vec4(
		dot(bumpedNormal, basisUP1),
		dot(bumpedNormal, basisUP2),
		dot(bumpedNormal, basisUP3),
		dot(bumpedNormal, basisDW1));
	basisWeights = clamp(basisWeights * basisScale + basisBias, 0.0, 1.0);
	basisWeights = optional_normalize(basisWeights);
#	endif // USE_TEXTURE_NORMALIZATION
	// Unpack basis colors
	lowp vec3 basisColor0 = vBasisGI0.rgb;
	lowp vec3 basisColor1 = vBasisGI1.rgb;
	lowp vec3 basisColor2 = vBasisGI2.rgb;
	lowp vec3 basisColor3 = vec3(vBasisGI0.a, vBasisGI1.a, vBasisGI2.a);
	lowp vec3 precomputedColor =
		basisColor0 * basisWeights[0] +
		basisColor1 * basisWeights[1] +
		basisColor2 * basisWeights[2] +
		basisColor3 * basisWeights[3];
	precomputedColor += vPrecomputedColor; 
	// Diffuse factor
	lowp float diffuseFactor = DiffuseScaleTweaker * max(0.0, dot(bumpedNormal, Light0Direction));
	// Specular factor
	lowp vec3 worldView = normalize(vWorldVertex.xyz - CameraPosition.xyz);
	lowp vec3 reflectedWorldView = reflect(worldView, bumpedNormal);
	lowp float specularFactor = SpecularScaleTweaker * pow(max(0.0, dot(reflectedWorldView, Light0Direction)), SpecularPowerTweaker);  
	// Direct color
	lowp vec3 directColor = (diffuseFactor + specularFactor) * SunColorTweaker * SunPowerTweaker;
#else // USE_NORMAL_MAP
	lowp vec3 directColor = vDirectColor.xyz;
	lowp vec3 precomputedColor = vPrecomputedColor;
#endif // USE_NORMAL_MAP
#if defined(USE_ALBEDO)
	// Fade albedo
#	if defined(USE_TWEAKERS)
	albedo = mix(vec3(1.0), albedo, DiffusemapBlendTweaker);
#	endif // USE_TWEAKERS
#endif //USE_ALBEDO
	// Compute final color
	lowp vec3 color = albedo * (visibility * directColor + precomputedColor);
	gl_FragColor = vec4(color, alphaBlend);
#if 0
	gl_FragColor = mix(gl_FragColor, vec4(albedo, 1.0), 0.999);
#endif
}
