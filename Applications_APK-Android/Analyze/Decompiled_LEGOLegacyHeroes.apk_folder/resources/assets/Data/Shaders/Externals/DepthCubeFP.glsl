#ifdef DCC_MAX
#define DMAX(x) x
#else
#define DMAX(x)
#endif

#if defined(GLSL2HLSL)
#	if GLITCH_PROFILE_HLSL <= GLITCH_PROF_HLSL_PS_4_0_LEVEL_9_3
#		define EMUL_FRAG_COORD
#	endif
#endif

#if defined(GLSL2HLSL)
#	if GLITCH_PROFILE_HLSL < GLITCH_PROF_HLSL_PS_4_0_LEVEL_9_3
#		define PACK_DEPTH_ON_RG8
//#		define PACK_DEPTH_ON_RGB8
#	endif
#endif

#ifdef EMUL_FRAG_COORD
varying highp vec2 fragCoordEmul DMAX( : TEXCOORD0 );
#	define FragCoordZ (fragCoordEmul.x / fragCoordEmul.y)
#else
#	define FragCoordZ gl_FragCoord.z
#endif

void main()
{	
#ifdef PACK_DEPTH_ON_RG8
	// 2 component	
	highp int z = int(FragCoordZ * 65535.0);
	highp float zu = float(z / 256) / 255.0;
	highp float zd = float(mod(z, 256)) / 255.0;
	gl_FragColor = vec4(zu, zd, 0.0, 1.0);
#elif defined(PACK_DEPTH_ON_RGB8)
	// 3 component	
	highp int z = int(FragCoordZ * 16777215.0);
	highp float z2 = float(z / 65536) / 255.0;	
	highp float z1 = float(int(mod(z, 65536)) / 256) / 255.0;
	highp float z0 = float(mod(z, 256)) / 255.0;
	gl_FragColor = vec4(z2, z1, z0, 1.0);
#else
	// 1 component
	gl_FragColor = vec4(vec3(FragCoordZ), 1.0);
#endif
}
