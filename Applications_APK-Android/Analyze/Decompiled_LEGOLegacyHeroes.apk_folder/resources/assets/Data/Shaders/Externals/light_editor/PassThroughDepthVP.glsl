in vec4 Vertex;

uniform highp mat4 WorldViewProjectionMatrix;

void main()
{
    gl_Position = WorldViewProjectionMatrix * Vertex;
}
