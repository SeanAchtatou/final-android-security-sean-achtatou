// Compiler options	
//#define USE_BLURRED_SHADOW

// Transformation parameters
uniform highp mat4 World;
uniform highp mat4 WorldIT;

// Shadow parameters
uniform highp mat4 ShadowMatrix;
uniform lowp float ShadowOpacityPowerTweaker;
#if defined(USE_BLURRED_SHADOW)
uniform lowp float ShadowTexelSize;
#endif // USE_BLURRED_SHADOW

// Shading parameters
uniform lowp vec3 Light0Direction;

// Attributes
attribute highp vec4 Vertex;
attribute lowp vec3 Normal;
attribute lowp vec2 TexCoord1;

// Varyings
#if defined(USE_BLURRED_SHADOW)
varying highp vec3 vShadowVertex0;
varying highp vec3 vShadowVertex1;
varying highp vec3 vShadowVertex2;
varying highp vec3 vShadowVertex3;
#else
varying highp vec3 vShadowVertex;
#endif // USE_BLURRED_SHADOW
varying lowp float vShadowOpacity;

void main(void) 
{
	// Compute world space tranforms
	highp vec4 worldVertex = World * Vertex;
	lowp vec3 worldNormal = normalize((WorldIT * vec4(Normal, 0.0)).xyz); 
	// Compute shadow opacity
	lowp float visibilityFactor = max(0.0, dot(worldNormal, Light0Direction));
	vShadowOpacity = 1.0 - pow(1.0 - visibilityFactor, ShadowOpacityPowerTweaker);	
	// Compute light space tranforms
	highp vec3 shadowVertex = (ShadowMatrix * worldVertex).xyz;
#	if defined(USE_BLURRED_SHADOW)
	// 3x3 gaussian filter
	highp float shadowOffset = 0.5 * ShadowTexelSize;
	vShadowVertex0 = shadowVertex + vec3(-shadowOffset, -shadowOffset, 0.0);
	vShadowVertex1 = shadowVertex + vec3( shadowOffset, -shadowOffset, 0.0);
	vShadowVertex2 = shadowVertex + vec3( shadowOffset,  shadowOffset, 0.0);
	vShadowVertex3 = shadowVertex + vec3(-shadowOffset,  shadowOffset, 0.0);
#else
	vShadowVertex = shadowVertex;
#endif
	// Output in lightmap space
	gl_Position = vec4(-1.0 + 2.0 * TexCoord1.s, -1.0 + 2.0 * TexCoord1.t, 1.0, 1.0);
}
