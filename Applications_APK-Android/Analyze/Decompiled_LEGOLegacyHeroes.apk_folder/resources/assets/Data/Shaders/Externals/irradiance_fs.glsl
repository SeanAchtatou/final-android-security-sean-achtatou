uniform sampler2D DiffuseMapSampler;

varying	  mediump vec3 vDiffuse;

void main()
{
	gl_FragColor = vec4(sqrt(1. - exp(-1.5 * pow(vDiffuse, vec3(1.3, 1.3, 1.3)))), 1.);
}

