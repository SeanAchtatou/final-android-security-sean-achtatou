#ifdef DCC_MAX
#define DM( x ) x
#else
#define DM( x )
#endif

// attributes
attribute vec4 position DM( : POSITION ); 
attribute vec2 texcoord0 DM( : TEXCOORD0 );
attribute vec3 normal DM( : NORMAL );
attribute vec3 tangent DM( : TANGENT );
attribute vec3 binormal DM( : BINORMAL );

#ifdef DCC_MAX
attribute vec3 color : TEXCOORD1;
#else
attribute vec4 color;
#endif

// varying
varying vec2 vTexcoord DM( : TEXCOORD0 );

#ifdef TANGENTS
varying vec3 vTangent DM( : TEXCOORD1 );
#elif defined( BINORMALS )
varying vec3 vBinormal DM( : TEXCOORD1 );
#elif defined( NORMALS )
varying vec3 vNormal DM( : TEXCOORD1 );
#elif defined( VERTEX_COLORS )
varying vec3 vColor DM( : TEXCOORD1 );
#endif

// uniforms
uniform mat4 wvp;

// functions
vec3 ToColor( vec3 v )
{
	return ( normalize( v ) + vec3( 1., 1., 1. ) ) / 2.;
}

void main() 
{ 
	gl_Position = wvp * position;
	
	vec2 texCoordTmp = texcoord0;
	DM( ++texCoordTmp.y; )
	vTexcoord = texCoordTmp;
	
#ifdef TANGENTS
#ifdef DCC_MAX
	vTangent = ToColor( binormal );
#else
	vTangent = ToColor( tangent );
#endif
#elif defined( BINORMALS )
#ifdef DCC_MAX
	vBinormal = ToColor( -tangent );
#else
	vBinormal = ToColor( binormal );
#endif
#elif defined( NORMALS )
	vNormal = ToColor( normal );
#elif defined( VERTEX_COLORS )
#ifdef DCC_MAX
	vColor = color;
#else
	vColor = color.xyz;
#endif
#endif
}
