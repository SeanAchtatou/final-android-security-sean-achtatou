uniform lowp sampler2D Direct;
uniform lowp sampler2D Indirect;

varying lowp vec2 vTexCoord0;

void main()
{
        lowp vec4 direct = texture2D(Direct, vTexCoord0);
        lowp vec4 indirect = texture2D(Indirect, vTexCoord0);
        gl_FragColor = direct + indirect;
}