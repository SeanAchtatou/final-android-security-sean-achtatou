#version 400

uniform sampler2D DebugTex;

in vec2 vTexCoord1;

out vec4 DebugColor;

void main()
{
	DebugColor = texture2D(DebugTex, vTexCoord1);
}
