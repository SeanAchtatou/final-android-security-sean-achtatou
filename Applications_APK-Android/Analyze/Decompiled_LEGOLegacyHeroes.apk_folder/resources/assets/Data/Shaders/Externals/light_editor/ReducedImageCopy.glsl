#version 430

#define GROUP_SIZE 4
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE) in;

GLITCH_UNIFORM_PROPERTIES(colorTex, (access = r))
GLITCH_UNIFORM_PROPERTIES(imgOut, (access = w))

layout(rgba8) uniform image2D colorTex;
layout(rgba8) uniform image2D imgOut;

uniform int scale;

void main()
{
    const ivec2 local_xy = ivec2(gl_LocalInvocationID);
    const ivec2 image_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE + local_xy;
    const ivec2 image_size = imageSize(imgOut);
    
    if(all(lessThan(image_xy, image_size)))
    {
        vec4 c = imageLoad(colorTex, image_xy * scale);
        imageStore(imgOut, image_xy, c);
    }
}
