#extension GL_EXT_shader_framebuffer_fetch : require

#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
inout lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#    define gl_LastFragData0 gl_FragColor
#else
#    define gl_LastFragData0 gl_LastFragData[0]
#endif

uniform lowp sampler2D TextureSampler;
uniform lowp vec4 DiffuseColor;
uniform lowp vec4 AdditiveColor;

#ifdef SPLIT_ALPHA
uniform lowp sampler2D TextureSampler_alpha;
#endif

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

void main()
{
#ifdef SPLIT_ALPHA
	lowp vec4 color = vec4(texture2D(TextureSampler, vTexCoord0).rgb,
						   texture2D(TextureSampler_alpha, vTexCoord0).g);
#else
	lowp vec4 color = texture2D(TextureSampler, vTexCoord0);
#endif

	lowp vec4 dstColor = gl_LastFragData0;

	color = (color + DiffuseColor) * vColor0 + AdditiveColor;
	color.rgb *= color.a;

	lowp vec4 result;

	if (0.333*dstColor.r + 0.333*dstColor.g + 0.333*dstColor.b < 0.5)
		result = 2.0 * dstColor * color;
	else
		result = 1.0 - 2.0*(1.0 - dstColor)*(1.0 - color);

	result.a = color.a;
	gl_FragColor = result;
}
