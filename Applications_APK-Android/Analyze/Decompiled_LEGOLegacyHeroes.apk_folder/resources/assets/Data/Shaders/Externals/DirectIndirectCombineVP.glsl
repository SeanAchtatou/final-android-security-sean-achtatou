attribute highp vec2 Vertex;
attribute lowp vec2 TexCoord0;

varying lowp vec2 vTexCoord0;

//uniform highp mat4 ProjectionMatrix;

void main()
{
        gl_Position = vec4(Vertex * 2. - 1., 0.0, 1.0); //map to [-1, 1]
        vTexCoord0 = TexCoord0;
}