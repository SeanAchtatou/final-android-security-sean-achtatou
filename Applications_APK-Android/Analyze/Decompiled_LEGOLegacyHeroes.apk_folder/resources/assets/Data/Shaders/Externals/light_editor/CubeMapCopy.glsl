#version 430

#define GROUP_SIZE 4
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE, local_size_z = 6) in;

GLITCH_UNIFORM_PROPERTIES(texOut, (access = w))

uniform samplerCube texIn;
layout(rgba8) uniform imageCube texOut;

vec3 generateCubemapCoord(in vec2 txc, in uint face)
{
    vec3 v;

    switch(face)
    {
        case 0: v = vec3( 1.0, -txc.y, -txc.x); break; // +X
        case 1: v = vec3(-1.0, -txc.y, txc.x); break; // -X
        case 2: v = vec3(txc.x,  1.0, txc.y); break; // +Y
        case 3: v = vec3(txc.x, -1.0, -txc.y); break; // -Y
        case 4: v = vec3(txc.x, -txc.y,  1.0); break; // +Z
        case 5: v = vec3(-txc.x, -txc.y, -1.0); break; // -Z
    }
    
    return normalize(v);
}

void main()
{
    const uvec2 local_xy = gl_LocalInvocationID.xy;
    const uvec2 image_xy = gl_WorkGroupID.xy * GROUP_SIZE + local_xy;
    const ivec2 image_size = imageSize(texOut);
    
    vec2 faceCoord = ((vec2(image_xy) + 0.5) / vec2(image_size)) * 2.0 - 1.0;
    vec3 cubeCoord = generateCubemapCoord(faceCoord, gl_LocalInvocationID.z);
    
    vec4 c = texture(texIn, cubeCoord);
    imageStore(texOut, ivec3(image_xy, gl_LocalInvocationID.z), c);
}
