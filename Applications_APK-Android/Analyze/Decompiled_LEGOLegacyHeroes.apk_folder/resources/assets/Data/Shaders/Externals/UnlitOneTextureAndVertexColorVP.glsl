#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define attribute in
#    define varying out
#endif

#define TEXTURE_2D 0
#define TEXTURE_2D_ARRAY 1
#define TEXTURE_3D 2
#define TEXTURE_CUBE_MAP 3
#define TEXTURE_CUBE_MAP_ARRAY 4

#ifndef TEXTURE_TYPE
#    define TEXTURE_TYPE TEXTURE_2D
#endif

#define TEXCOORD_NONE 0
#define TEXCOORD_SCALE_OFFSET 1
#define TEXCOORD_TRANSFORM 2
#define TEXCOORD_TRANSFORM_3 3
#define TEXCOORD_SCALE_OFFSET_SEPARATE 4

#if !defined(TEXCOORD_MODIFIER) && defined(TEXTURE_MATRIX)
// legacy support for old TEXTURE_MATRIX
#    define TEXCOORD_MODIFIER TEXCOORD_TRANSFORM
#elif !defined(TEXCOORD_MODIFIER)
#    define TEXCOORD_MODIFIER TEXCOORD_NONE
#endif

attribute highp vec4 Vertex;

#if TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && defined(TEXCOORD_ARRAY_SLICE)
attribute highp vec4 TexCoord0;
#elif TEXTURE_TYPE == TEXTURE_3D || TEXTURE_TYPE == TEXTURE_CUBE_MAP || (TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && !defined(TEXCOORD_ARRAY_SLICE)) || (TEXTURE_TYPE == TEXTURE_2D_ARRAY && defined(TEXCOORD_ARRAY_SLICE))
attribute highp vec3 TexCoord0;
#else
attribute highp vec2 TexCoord0;
#endif

attribute lowp vec4 Color0;

uniform highp mat4 WorldViewProjectionMatrix;

#if (TEXTURE_TYPE == TEXTURE_2D_ARRAY || TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY) && !defined(TEXCOORD_ARRAY_SLICE)
uniform highp int Index;
#endif

#if TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY
varying highp vec4 vCoord0;
#elif TEXTURE_TYPE == TEXTURE_3D || TEXTURE_TYPE == TEXTURE_CUBE_MAP || TEXTURE_TYPE == TEXTURE_2D_ARRAY
varying highp vec3 vCoord0;
#else
varying highp vec2 vCoord0;
#endif
varying lowp vec4 vColor0;

#if (TEXCOORD_MODIFIER == TEXCOORD_TRANSFORM || TEXCOORD_MODIFIER == TEXCOORD_TRANSFORM_3) && (TEXTURE_TYPE == TEXTURE_3D || TEXTURE_TYPE == TEXTURE_CUBE_MAP || TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY)
uniform highp mat4 TexCoord0_transform;
#elif TEXCOORD_MODIFIER == TEXCOORD_TRANSFORM_3
uniform highp mat3 TexCoord0_transform;
#elif (TEXTURE_TYPE == TEXTURE_3D || TEXTURE_TYPE == TEXTURE_CUBE_MAP || TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY) && (TEXCOORD_MODIFIER == TEXCOORD_SCALE_OFFSET || TEXCOORD_MODIFIER == TEXCOORD_SCALE_OFFSET_SEPARATE)
uniform highp vec3 TexCoord0_scale;
uniform highp vec3 TexCoord0_offset;
#elif TEXCOORD_MODIFIER == TEXCOORD_SCALE_OFFSET
uniform highp vec4 TexCoord0_scaleoffset;
#elif TEXCOORD_MODIFIER == TEXCOORD_SCALE_OFFSET_SEPARATE
uniform highp vec2 TexCoord0_scale;
uniform highp vec2 TexCoord0_offset;
#endif

void main(void)
{
#if TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && TEXCOORD_MODIFIER != TEXCOORD_NONE && defined(TEXCOORD_ARRAY_SLICE)
	vCoord0.w = TexCoord0.w;
#elif TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && TEXCOORD_MODIFIER != TEXCOORD_NONE && !defined(TEXCOORD_ARRAY_SLICE)
	vCoord0.w = float(Index);
#elif TEXTURE_TYPE == TEXTURE_2D_ARRAY && TEXCOORD_MODIFIER != TEXCOORD_NONE && defined(TEXCOORD_ARRAY_SLICE)
	vCoord0.z = TexCoord0.z;
#elif TEXTURE_TYPE == TEXTURE_2D_ARRAY && TEXCOORD_MODIFIER != TEXCOORD_NONE && !defined(TEXCOORD_ARRAY_SLICE)
	vCoord0.z = float(Index);
#endif
#if (TEXTURE_TYPE == TEXTURE_3D || TEXTURE_TYPE == TEXTURE_CUBE_MAP || TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY) && (TEXCOORD_MODIFIER == TEXCOORD_TRANSFORM || TEXCOORD_MODIFIER == TEXCOORD_TRANSFORM_3)
	vCoord0.xyz = (TexCoord0_transform * vec4(TexCoord0.xyz, 1.0)).xyz;
#elif (TEXTURE_TYPE == TEXTURE_3D || TEXTURE_TYPE == TEXTURE_CUBE_MAP || TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY) && (TEXCOORD_MODIFIER == TEXCOORD_SCALE_OFFSET || TEXCOORD_MODIFIER == TEXCOORD_SCALE_OFFSET_SEPARATE)
	vCoord0.xyz = TexCoord0.xyz * TexCoord0_scale + TexCoord0_offset;
#elif (TEXTURE_TYPE == TEXTURE_2D || TEXTURE_TYPE == TEXTURE_2D_ARRAY) && TEXCOORD_MODIFIER == TEXCOORD_TRANSFORM_3
	vCoord0 = (TexCoord0_transform * vec3(TexCoord0.xy, 1.0)).xy;
#elif (TEXTURE_TYPE == TEXTURE_2D || TEXTURE_TYPE == TEXTURE_2D_ARRAY) && TEXCOORD_MODIFIER == TEXCOORD_SCALE_OFFSET
	vCoord0 = TexCoord0 * TexCoord0_scaleoffset.xy + TexCoord0_scaleoffset.zw;
#elif (TEXTURE_TYPE == TEXTURE_2D || TEXTURE_TYPE == TEXTURE_2D_ARRAY) && TEXCOORD_MODIFIER == TEXCOORD_SCALE_OFFSET_SEPARATE
	vCoord0.xy = TexCoord0.xy * TexCoord0_scale + TexCoord0_offset;
#elif TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && !defined(TEXCOORD_ARRAY_SLICE)
	vCoord0.xyz = TexCoord0;
	vCoord0.w = float(Index);
#elif TEXTURE_TYPE == TEXTURE_2D_ARRAY && !defined(TEXCOORD_ARRAY_SLICE)
	vCoord0.xy = TexCoord0;
	vCoord0.z = float(Index);
#else
	vCoord0 = TexCoord0;
#endif
	vColor0 = Color0;
#if defined(ON_FAR)
	highp vec4 pos = (WorldViewProjectionMatrix * Vertex).xyww;
	// some devices do not interpolate between 1.0 and 1.0 without floating
	// point errors (most notably the Mali-400 MP), making sometimes the
	// resulting value slightly greater than 1.0, so we scale the "z" down to
	// make it slithly less than 1.0 (after perspective division)
	pos.z *= 0.999999;
	gl_Position = pos;
#else
	gl_Position = WorldViewProjectionMatrix * Vertex;
#endif
}
