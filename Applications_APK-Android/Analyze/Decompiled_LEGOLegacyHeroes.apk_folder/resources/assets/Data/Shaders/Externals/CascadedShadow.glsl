//#define VERTEX_SHADER
//#define FRAGMENT_SHADER
//#define RENDER_CASCADE
//#define LIGHT_AND_SHADOW
//#define SHADOW_HW
//#define DEBUG
//#define WRITE_DEPTH_TO_COLOR_ATT

#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#ifdef VERTEX_SHADER
#define attribute in
#define varying out
#endif
#ifdef FRAGMENT_SHADER
#define varying in
out lowp vec4 glitch_FragColor;
#define gl_FragColor glitch_FragColor
#define texture2D texture
#endif
#endif

#ifdef DCC_MAX
#define DMAX(x) x
#else
#define DMAX(x)
#endif

#if defined(FRAGMENT_SHADER) && defined(GLSL2HLSL)
#	if GLITCH_PROFILE_HLSL < GLITCH_PROF_HLSL_PS_4_0_LEVEL_9_3
#		define PACK_DEPTH_ON_RG8
#	endif
#endif

// ------------------------------------------------------------
// ----                RENDER_CASCADE                      ----
// ----               Fill shadow map                      ----
// ------------------------------------------------------------
#ifdef RENDER_CASCADE

#if defined(WRITE_DEPTH_TO_COLOR_ATT) && defined(GLSL2HLSL)
// Note: GLSL Converter doesn't allow undefined macros in preprocessor expressions
//  ( i.e. Macro <= PotentiallyUndefinedMacro in this case )
//  GLITCH_PROF_HLSL_VS_* macros are only defined when compiling a vertex shader
//  GLITCH_PROF_HLSL_PS_* macros are only defined when compiling a fragment shader
#	if defined(VERTEX_SHADER)
#		if GLITCH_PROFILE_HLSL <= GLITCH_PROF_HLSL_VS_4_0_LEVEL_9_3
#			define EMUL_FRAG_COORD
#		endif
#	elif GLITCH_PROFILE_HLSL <= GLITCH_PROF_HLSL_PS_4_0_LEVEL_9_3
#		define EMUL_FRAG_COORD
#	endif
#endif

#ifdef VERTEX_SHADER

attribute highp 	vec4 Vertex DMAX( : POSITION );

#ifdef EMUL_FRAG_COORD
varying highp vec2 fragCoordEmul DMAX( : TEXCOORD0 );
#endif

uniform highp mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	
#ifdef EMUL_FRAG_COORD
	fragCoordEmul = gl_Position.zw;
#endif
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

#if defined(EMUL_FRAG_COORD)
varying highp vec2 fragCoordEmul DMAX( : TEXCOORD0 );
#	define FragCoordZ (fragCoordEmul.x / fragCoordEmul.y)
#else
#	define FragCoordZ gl_FragCoord.z
#endif

#ifdef DEBUG
uniform lowp int cascadeIndex;
#endif

void main()
{
#ifdef DEBUG
	const lowp vec4 red = vec4(1.0, 0.0, 0.0, 1.0);
	const lowp vec4 blue = vec4(0.0, 0.0, 1.0, 1.0);
	const lowp vec4 green = vec4(0.0, 1.0, 0.0, 1.0);
	const lowp vec4 purple = vec4(1.0, 0.0, 1.0, 1.0);
	const lowp vec4 yellow = vec4(1.0, 1.0, 0.0, 1.0);

	// Ugly unoptimal if..else.. but don't care since it is just for debug
	lowp vec4 debugColor;

	if (cascadeIndex == 0)
		debugColor = red;
	else if (cascadeIndex == 1)
		debugColor = green;
	else if (cascadeIndex == 2)
		debugColor = blue;
	else // Shouldn't happen
		debugColor = yellow;

	gl_FragColor = debugColor;
#else

#ifdef WRITE_DEPTH_TO_COLOR_ATT

#ifdef PACK_DEPTH_ON_RG8
	// 2 component	
	highp int z = int(clamp(FragCoordZ, 0.0, 1.0) * 65535.0);
	highp float zu = float(z / 256) / 255.0;
	highp float zd = float(mod(z, 256)) / 255.0;
	gl_FragColor = vec4(zu, zd, 0.0, 1.0);
#elif defined(PACK_DEPTH_ON_RGB8)
	// 3 component	
	highp int z = int(clamp(FragCoordZ, 0.0, 1.0) * 16777215.0);
	highp float z2 = float(z / 65536) / 255.0;	
	highp float z1 = float(int(mod(z, 65536)) / 256) / 255.0;
	highp float z0 = float(mod(z, 256)) / 255.0;
	gl_FragColor = vec4(z2, z1, z0, 1.0);
#else
	// 1 component
	gl_FragColor = vec4(vec3(FragCoordZ), 1.0);
#endif

#else
	gl_FragColor = vec4(1.0,0.0,0.0,1.0);
#endif
#endif
}
#endif
// FRAGMENT_SHADER

#endif
// RENDER_CASCADE

// ------------------------------------------------------------
// ----               LIGHT_AND_SHADOW                     ----
// ----  Render objects with basic diffuse lighting        ----
// ----  and shadows from cascade                          ----
// ------------------------------------------------------------
#ifdef LIGHT_AND_SHADOW

#ifdef VERTEX_SHADER
attribute highp 	vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute highp 	vec3 Normal
#ifdef DCC_MAX
: NORMAL
#endif
;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 WorldView;
uniform highp mat4 WorldIT;
uniform highp mat4 World;

varying mediump		vec3	vNormalWS;	// World Space
varying highp		vec3	vPosVS;		// View Space position
varying highp		vec4	vPosWS;		// World Space position

void main()
{
	vPosWS = World * Vertex;
	vNormalWS = (WorldIT * vec4(Normal, 1.0)).xyz;
	vPosVS = (WorldView * Vertex).xyz;
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

uniform mediump vec2 camnearandfar;
uniform mediump vec3 cascadeFar;

uniform mediump vec3 sunLightDir; // WS normalized

uniform highp mat4 ViewToLightMatrix0;
uniform highp mat4 LightProjMatrix0;

uniform highp mat4 ViewToLightMatrix1;
uniform highp mat4 LightProjMatrix1;

uniform highp mat4 ViewToLightMatrix2;
uniform highp mat4 LightProjMatrix2;

#ifdef SHADOW_HW
#if defined(GL_ES) && __VERSION__ < 300
#extension GL_EXT_shadow_samplers : require
#endif
#if defined(GLSL2METAL)
GLITCH_UNIFORM_PROPERTIES(CascadedShadowMap, (samplerCompareFunc = lessEqual, samplerAddress = clampToEdge, samplerFilter = linear))
#endif
uniform highp sampler2DShadow CascadedShadowMap;
#else
GLITCH_UNIFORM_PROPERTIES(CascadedShadowMap, (depthtexture = 1))
uniform highp sampler2D CascadedShadowMap;
#endif

varying mediump		vec3	vNormalWS;	// World space normal
varying highp		vec3	vPosVS;		// View Space position
varying highp		vec4    vPosWS;		// World Space position

mediump float computeLighting()
{
	mediump vec3 normal = normalize(vNormalWS);
	return max(0.0, dot(normal,sunLightDir));
}

// TODO: optimize ?
lowp int computeCascadeIndex()
{
	lowp vec2 comp = step(cascadeFar.xy,vec2(abs(vPosVS.z)));
	return int(dot(vec2(1.0),comp));
}

// Return 1.0 if visible, 0.0 if occluded
// TODO: optim: precompute matrix product
mediump float computeVisibility(int index)
{
	highp float bias = 0.001 * float(index + 1);
	const highp float atlasScale = 0.5;

	highp vec2 atlasShift = vec2(0.0,0.0);
	highp mat4 LightProjMatrix = LightProjMatrix0;
	highp mat4 ViewToLightMatrix = ViewToLightMatrix0;

	if (index == 1) {
		atlasShift = vec2(0.0,0.5);
		LightProjMatrix = LightProjMatrix1;
		ViewToLightMatrix = ViewToLightMatrix1;
	}
	else if (index == 2) {
		atlasShift = vec2(0.5,0.0);
		LightProjMatrix = LightProjMatrix2;
		ViewToLightMatrix = ViewToLightMatrix2;
	}

	// Compute vertex position in LS
	highp vec4 posLS = LightProjMatrix * (ViewToLightMatrix * vPosWS);

	posLS.xyz = posLS.xyz * 0.5 + 0.5;
	posLS.y = 1.0 - posLS.y;
	posLS.xy = posLS.xy * atlasScale + atlasShift;

	posLS.z -= bias;

	// Fetch proper cascade
#ifdef SHADOW_HW
#if defined(GL_ES) && __VERSION__ < 300
	return shadow2DEXT(CascadedShadowMap, posLS.xyz);
#elif (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
	return texture(CascadedShadowMap, posLS.xyz);
#else
	return shadow2D(CascadedShadowMap, posLS.xyz).r;
#endif
#else
#ifdef PACK_DEPTH_ON_RG8	
	// 2 components
	highp vec2 depthQuantized = texture2D(CascadedShadowMap, posLS.xy).rg;
	highp float currentDepth = float((int(depthQuantized.r * 255.0) * 256) + int(depthQuantized.g * 255.0)) / 65535.0;
#elif defined(PACK_DEPTH_ON_RGB8)
	// 3 components
	highp vec3 depthQuantized = texture2D(CascadedShadowMap, posLS.xy).rgb;
	highp float currentDepth = float((int(depthQuantized.r * 255.0) * 65536) + (int(depthQuantized.g * 255.0) * 256) + int(depthQuantized.b * 255.0)) / 16777215.0;
#else
	// 1 component
	highp float currentDepth = texture2D(CascadedShadowMap, posLS.xy).r;
#endif
	return float(currentDepth > posLS.z);
#endif
}

void main()
{
	mediump float lightIntensity = 0.0;
	lowp int cascadeIndex = 0;

	lightIntensity = computeLighting();
	cascadeIndex = computeCascadeIndex();

	highp float visibility = computeVisibility(cascadeIndex);
	
#ifdef DEBUG
	lowp vec3 diffuseColor;
	
	const lowp vec3 red = vec3(1.0, 0.3, 0.3);
	const lowp vec3 green = vec3(0.3, 1.0, 0.3);
	const lowp vec3 blue = vec3(0.3, 0.3, 1.0);
	const lowp vec3 yellow = vec3(1.0, 1.0, 0.3);

	// Ugly unoptimal if..else.. but don't care since it is just for debug
	if (cascadeIndex == 0)
		diffuseColor = red;
	else if (cascadeIndex == 1)
		diffuseColor = green;
	else if (cascadeIndex == 2)
		diffuseColor = blue;
	else // Shouldn't happen
		diffuseColor = yellow;
#else
	const lowp vec3 diffuseColor = vec3(0.8,0.8,0.8);
#endif

	const lowp vec4 shadowColor = vec4(0.0,0.0,0.0,0.8);
	mediump float shadowIntensity = min(1.0 - visibility, shadowColor.a);
	//shadowIntensity = min(0.0,shadowIntensity);
	lowp vec3 finalColor = mix(diffuseColor * lightIntensity, shadowColor.rgb, shadowIntensity);

	gl_FragColor = vec4(finalColor,1.0);
}
#endif
// FRAGMENT_SHADER

#endif
// LIGHT_AND_SHADOW
