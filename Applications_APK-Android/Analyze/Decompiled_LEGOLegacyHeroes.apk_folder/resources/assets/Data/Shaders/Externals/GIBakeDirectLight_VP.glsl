// Attributes
attribute highp vec4 Vertex;
attribute lowp vec2 TexCoord0;

// Varyings
varying lowp vec2 vCoord1;
varying lowp vec2 vCoord2;
varying lowp vec2 vCoord3;

void main(void) 
{
	gl_Position = Vertex * vec4(1.0, -1.0, 1.0, 1.0);
	vCoord1 = TexCoord0;
	vCoord2 = vCoord1 * vec2(1.0, 0.5);
	vCoord3 = vCoord2 + vec2(0.0, 0.5);
}
