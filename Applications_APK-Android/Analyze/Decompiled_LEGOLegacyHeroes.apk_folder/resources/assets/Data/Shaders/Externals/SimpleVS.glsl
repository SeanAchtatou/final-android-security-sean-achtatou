attribute highp vec4 position;
attribute highp vec2 texcoord0;

uniform highp mat4 WorldViewProjection;

varying lowp vec2 v_texcoord0;	
varying highp vec4 v_position;

void main() 
{
	v_texcoord0 = texcoord0;
	v_position = WorldViewProjection * position;
	gl_Position = v_position;
}

