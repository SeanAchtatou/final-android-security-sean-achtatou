#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp sampler2D TextureSampler;
uniform lowp sampler2D TextureSamplerMap;

uniform lowp ivec2 Component;
uniform lowp vec2 Scale;
uniform lowp vec2 MapPoint;
uniform lowp vec2 MapScale;

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

void main()
{
	lowp vec2 uvMap = (vTexCoord0 - MapPoint) * MapScale;
	lowp vec4 pixel = texture2D(TextureSamplerMap, uvMap);
	if(uvMap.x < 0.0 || uvMap.y < 0.0 || uvMap.x > 1.0 || uvMap.y > 1.0)
	{
		pixel = vec4(0.5, 0.5, 0.5, 0.5);
	}
	lowp vec2 comp = vec2(pixel[Component.x], pixel[Component.y]);
	lowp vec2 displacement = (comp - vec2(0.5, 0.5)) * Scale;
	gl_FragColor = texture2D(TextureSampler, vTexCoord0 + displacement);
}
