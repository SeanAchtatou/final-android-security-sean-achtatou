// Parameters
uniform sampler2D Lightmap;

// Varyings
varying lowp vec2 vCoord0;
varying lowp vec2 vCoord1;
varying lowp vec2 vCoord2;
varying lowp vec2 vCoord3;

// main
void main(void)
{  		
	lowp float visibility = 0.0;
	visibility += texture2D(Lightmap, vCoord0).r;
	visibility += texture2D(Lightmap, vCoord1).r;
	visibility += texture2D(Lightmap, vCoord2).r;
	visibility += texture2D(Lightmap, vCoord3).r;
	visibility *= 0.25;
	gl_FragColor = vec4(visibility, 0.0, 0.0, 1.0);
}