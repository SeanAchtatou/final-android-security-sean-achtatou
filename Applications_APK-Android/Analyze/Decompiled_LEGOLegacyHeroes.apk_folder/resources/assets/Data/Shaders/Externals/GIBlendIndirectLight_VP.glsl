// Attributes
attribute highp vec4 Vertex;
attribute lowp vec2 TexCoord0;

// Varyings
varying lowp vec2 vCoord0;

void main(void) 
{
	gl_Position = Vertex * vec4(1.0, -1.0, 1.0, 1.0);
	vCoord0 = TexCoord0;
}