attribute highp   vec4 Vertex;
attribute highp   vec3 Normal;

uniform   highp   mat4 WorldViewProjectionMatrix;
uniform   highp   mat4 matWorldIT;

varying	  mediump vec3 vDiffuse;

uniform mediump vec4 ird_cAr; 
uniform mediump vec4 ird_cAg;
uniform mediump vec4 ird_cAb;
uniform mediump vec4 ird_cBr;
uniform mediump vec4 ird_cBg;
uniform mediump vec4 ird_cBb;
uniform mediump vec3 ird_cC;

void main()
{

	vec4 normal = vec4((matWorldIT * vec4(Normal,0)).xyz,1.0);
	//normal.z = -normal.z;
	normal.xyz = normalize(normal.xyz);
	normal.yz = normal.zy;
	normal.y *= -1.0;

	gl_Position = WorldViewProjectionMatrix * Vertex;

    mediump vec3 x1, x2, x3;
    
    // Linear + constant polynomial terms
    x1.r = dot(ird_cAr,normal);
    x1.g = dot(ird_cAg,normal);
    x1.b = dot(ird_cAb,normal);
    
    // 4 of the quadratic polynomials
    mediump vec4 vB = normal.xyzz * normal.yzzx;   
    x2.r = dot(ird_cBr,vB);
    x2.g = dot(ird_cBg,vB);
    x2.b = dot(ird_cBb,vB);
   
    // Final quadratic polynomial
    mediump float vC = normal.x*normal.x - normal.y*normal.y;
    x3 = ird_cC.rgb * vC;    
    
    vDiffuse = x1 + x2 + x3;	
}

