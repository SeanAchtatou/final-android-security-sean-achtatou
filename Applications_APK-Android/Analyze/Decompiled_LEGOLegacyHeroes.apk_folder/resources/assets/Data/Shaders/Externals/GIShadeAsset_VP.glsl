// Transformation parameters
uniform highp mat4 World;
uniform highp mat4 WorldIT;
uniform highp mat4 WorldViewProjection;

// Attributes
attribute highp vec4 Vertex;
attribute lowp vec3 Normal;
attribute lowp vec3 Tangent;
attribute lowp vec3 Binormal;
attribute lowp vec2 TexCoord0;
attribute lowp vec2 TexCoord1;

// Varyings
varying lowp vec4 vWorldVertex;
varying lowp vec3 vWorldNormal;
varying lowp vec3 vWorldTangent;
varying lowp vec3 vWorldBinormal;
varying lowp vec2 vCoord0;
varying lowp vec2 vCoord1;

// main
void main(void)
{
	vWorldVertex = World * Vertex;
	vWorldNormal = (WorldIT * vec4(Normal, 0.0)).xyz; 
	vWorldTangent = (WorldIT * vec4(Tangent, 0.0)).xyz; 
	vWorldBinormal  = (WorldIT * vec4(Binormal, 0.0)).xyz; 
	vCoord0 = TexCoord0;
	vCoord1 = TexCoord1;
	gl_Position = WorldViewProjection * Vertex;
}
