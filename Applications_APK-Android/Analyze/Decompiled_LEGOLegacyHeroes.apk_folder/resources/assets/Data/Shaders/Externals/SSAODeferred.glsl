//#define VERTEX_SHADER
//#define FRAGMENT_SHADER

//#define SSAO
//#define SSAO_BLUR
//#define SSAO_BLUR_H
//#define SSAO_BLUR_V
//#define SSAO_CUMULATIVE

// WARNING !! To be changed in code too !
#define SSAO_RESOLUTION_DIVIDER		4 

GLITCH_UNIFORM_PROPERTIES(DepthBuffer, (depthtexture = 1))

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef SSAO

// Common
varying	mediump vec2 vCoord0;

#ifdef VERTEX_SHADER
attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute highp vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

// Vertex specific
uniform highp  mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

uniform highp sampler2D	DepthBuffer;
uniform lowp sampler2D NormalBuffer;

uniform mediump sampler2D SSAORandomSamples;

uniform highp mat4 CameraViewProjectionI;
uniform highp vec3 MyCameraPosition;
uniform mediump vec2 MyCameraNearAndFar;

uniform mediump vec2 viewportdimension;
uniform mediump vec2 viewportdimensioninv;

#define NB_SAMPLES	16

// Function for converting depth to world-space position
// in deferred pixel shader pass.  coord is a texture
// coordinate for a full-screen quad, such that x=0 is the
// left of the screen, and y=0 is the top of the screen.
highp vec3 getPositionFromDepth(mediump vec2 coord,highp float depth)
{
    highp vec3 pos = vec3(coord, depth) * 2.0 - 1.0;
	pos.y *= -1.0;
    
    // Transform by the inverse view projection matrix
    highp vec4 pos4 =  CameraViewProjectionI * vec4(pos,1.0);
  
    // Divide by w to get the world-space position
    return  pos4.xyz / pos4.w;
}

highp float getDepth(mediump vec2 coord)
{
	return texture2D(DepthBuffer, coord).r;
}


lowp vec3 getNormal(mediump vec2 coord)
{
	return texture2D(NormalBuffer, coord).rgb;
}

void main()
{
	
	const mediump mat4 samplesSet1 = mat4(
	-0.286199, 0.331545,		 0.464995, -0.110527,
	 0.243161, 0.116612,		-0.170724, -0.155669,
	 0.340982, 0.263809,		 0.343742, -0.183868,
	-0.498398, 0.000477,		-0.101363, -0.288459
	);

	const mediump mat4 samplesSet2 = mat4(
	 0.376492, 0.508996,		-0.917514, -0.068567,
	 0.659013, 0.197366,		-0.825559, -0.147333,
	-0.488068, 0.127293,		-0.417507, -0.482348,
	-0.448707, 0.655000,		 0.599988, -0.695858
	);

	const lowp float bias = 0.1;
	const lowp float epsilon = 1.0;
	const lowp float distCoef = 1.0;
	
	const mediump float minRadius = 20.0;
	const mediump float maxRadius = 40.0;
	
	const mediump float globalScale = 20.0 * pow(2.0,float(SSAO_RESOLUTION_DIVIDER)-1.0);
	const mediump float minScale = 5.0 * globalScale / float(NB_SAMPLES);
	const mediump float maxScale = 60.0 * globalScale / float(NB_SAMPLES);
	
	const mediump vec2 randomTexSizeInv = 1.0 / vec2(32.0,32.0);
	mediump vec4 rotVector = texture2D(SSAORandomSamples,vCoord0 * viewportdimension * randomTexSizeInv) * 2.0 -1.0;
	
	mediump vec3 currentNormal = getNormal(vCoord0);
	highp float currentDepth = getDepth(vCoord0);
	
	currentNormal = normalize(currentNormal * 2.0 - 1.0);
	
	highp vec3 currentPos = getPositionFromDepth(vCoord0,currentDepth) + 0.1 * currentNormal;
	
	mediump mat2 rotMatrix = mat2(normalize(rotVector.rg),normalize(rotVector.ba));
	
	mediump float depthCoef = (abs(MyCameraPosition.z - currentPos.z) - MyCameraNearAndFar.x) / (MyCameraNearAndFar.y - MyCameraNearAndFar.x);
	mediump float radius = mix(maxRadius,minRadius,depthCoef);
	mediump float scale = mix(minScale,maxScale,depthCoef);
	
	mediump vec2 shiftScale = radius * viewportdimensioninv;// / float(SSAO_RESOLUTION_DIVIDER);
	
	// First sample set => 8 samples
	highp float aoFactor = 0.0;
	
	for(int i=0; i < 4; ++i) {
		
		mediump vec4 samples = samplesSet1[i];
		mediump vec2 tmpShift1 = rotMatrix * samples.xy;
		mediump vec2 tmpShift2 = rotMatrix * samples.zw;
		
		mediump vec2 tmpCoord1 = tmpShift1 * shiftScale + vCoord0;
		highp float sampleDepth1 = getDepth(tmpCoord1);
		highp vec3 currentToSample1 = getPositionFromDepth(tmpCoord1,sampleDepth1) - currentPos;
		
		mediump float squareDistance1 = distCoef * dot(currentToSample1,currentToSample1);
		aoFactor += max(0.0,dot(normalize(currentToSample1),currentNormal) - bias) / (squareDistance1 + epsilon);
		
		mediump vec2 tmpCoord2 = tmpShift2 * shiftScale + vCoord0;
		highp float sampleDepth2 = getDepth(tmpCoord2);
		highp vec3 currentToSample2 = getPositionFromDepth(tmpCoord2,sampleDepth2) - currentPos;
		
		mediump float squareDistance2 = distCoef * dot(currentToSample2,currentToSample2);
		aoFactor += max(0.0,dot(normalize(currentToSample2),currentNormal) - bias) / (squareDistance2 + epsilon);
	}
	
	// Second sample set => 8 samples
	for(int i=0; i < 4; ++i) {
		
		mediump vec4 samples = samplesSet2[i];
		mediump vec2 tmpShift1 = rotMatrix * samples.xy;
		mediump vec2 tmpShift2 = rotMatrix * samples.zw;
		
		mediump vec2 tmpCoord1 = tmpShift1 * shiftScale + vCoord0;
		highp float sampleDepth1 = getDepth(tmpCoord1);
		highp vec3 currentToSample1 = getPositionFromDepth(tmpCoord1,sampleDepth1) - currentPos;
		
		mediump float squareDistance1 = distCoef * dot(currentToSample1,currentToSample1);
		aoFactor += max(0.0,dot(normalize(currentToSample1),currentNormal) - bias) / (squareDistance1 + epsilon);
		
		
		mediump vec2 tmpCoord2 = tmpShift2 * shiftScale + vCoord0;
		highp float sampleDepth2 = getDepth(tmpCoord2);
		highp vec3 currentToSample2 = getPositionFromDepth(tmpCoord2,sampleDepth2) - currentPos;
		
		mediump float squareDistance2 = distCoef * dot(currentToSample2,currentToSample2);
		aoFactor += max(0.0,dot(normalize(currentToSample2),currentNormal) - bias) / (squareDistance2 + epsilon);
	}
	
	aoFactor = 1.0 - clamp(aoFactor * scale,0.0,1.0);
	gl_FragColor = vec4(aoFactor,aoFactor,aoFactor,1.0);
	
	//gl_FragColor = vec4(depthCoef,depthCoef,depthCoef,1.0) + 0.0001 * aoFactor;

}
#endif
#endif 
// SSAO

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// 5x5 Separable Gaussian blur
#ifdef SSAO_BLUR

// Common
varying	mediump vec2 vCoord0;

#ifdef VERTEX_SHADER

attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute highp vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

// Vertex specific
uniform highp  mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

uniform lowp sampler2D SSAOBuffer;
uniform highp vec2 viewportdimensioninv;

void main()
{
	const lowp float centralWeight = 7.0 / 17.0;
	const lowp vec2 neighbourWeight = vec2(4.0/17.0, 1.0/17.0);

	mediump float aoFactor = centralWeight * texture2D(SSAOBuffer,vCoord0).r;
	
	// Look left and right (up and bottom) samples at once
			
#ifdef SSAO_BLUR_H

	mediump vec2 sampleCoord1 = vec2(viewportdimensioninv.x,0.0) + vCoord0;
	aoFactor += neighbourWeight[0] * texture2D(SSAOBuffer,sampleCoord1).r;
		
	mediump vec2 sampleCoord2 = vec2(-viewportdimensioninv.x,0.0) + vCoord0;
	aoFactor += neighbourWeight[0] * texture2D(SSAOBuffer,sampleCoord2).r;

	mediump float shiftH = 2.0 * viewportdimensioninv.x;
		
	sampleCoord1 = vec2(shiftH,0.0) + vCoord0;
	aoFactor += neighbourWeight[1] * texture2D(SSAOBuffer,sampleCoord1).r;
		
	sampleCoord2 = vec2(-shiftH,0.0) + vCoord0;
	aoFactor += neighbourWeight[1] * texture2D(SSAOBuffer,sampleCoord2).r;

#elif defined(SSAO_BLUR_V)
		
	mediump vec2 sampleCoord1 = vec2(0.0,viewportdimensioninv.y) + vCoord0;
	aoFactor += neighbourWeight[0] * texture2D(SSAOBuffer,sampleCoord1).r;
		
	mediump vec2 sampleCoord2 = vec2(0.0,-viewportdimensioninv.y) + vCoord0;
	aoFactor += neighbourWeight[0] * texture2D(SSAOBuffer,sampleCoord2).r;

	mediump float shiftV = 2.0 * viewportdimensioninv.y;
		
	sampleCoord1 = vec2(0.0,shiftV) + vCoord0;
	aoFactor += neighbourWeight[1] * texture2D(SSAOBuffer,sampleCoord1).r;
		
	sampleCoord2 = vec2(0.0,-shiftV) + vCoord0;
	aoFactor += neighbourWeight[1] * texture2D(SSAOBuffer,sampleCoord2).r;
		
#else
#    error ! You must define either SSAO_BLUR_H or SSAO_BLUR_V
#endif
		
	gl_FragColor = vec4(aoFactor,aoFactor,aoFactor,1.0);
	
}
#endif 
// FRAGMENT_SHADER
#endif  
// SSAO_BLUR

#ifdef SSAO_CUMULATIVE

// Common
varying	mediump vec2 vCoord0;

#ifdef VERTEX_SHADER
attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute highp vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

// Vertex specific
uniform highp  mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

uniform lowp sampler2D 		AlbedoBuffer;
uniform lowp sampler2D 		LightIntensityBuffer;
uniform lowp sampler2D 		SpecularIntensityBuffer;
uniform lowp sampler2D		SSAOBuffer;

void main()
{
	gl_FragColor = vec4( texture2D(AlbedoBuffer, vCoord0).rgb
					   * texture2D(LightIntensityBuffer, vCoord0).rgb
					   * texture2D(SSAOBuffer, vCoord0).rgb
					   + texture2D(SpecularIntensityBuffer, vCoord0).rgb
					   ,1.0);
}
#endif
// FRAGMENT_SHADER

#endif
// SSAO_CUMULATIVE
