#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp sampler2D TextureSampler;
uniform lowp vec4 DiffuseColor;
uniform lowp vec2 MapPoint;

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

void main()
{
	lowp vec4 color = DiffuseColor;
	color.a *= texture2D(TextureSampler, vTexCoord0 - MapPoint).a;
	gl_FragColor = color;
}
