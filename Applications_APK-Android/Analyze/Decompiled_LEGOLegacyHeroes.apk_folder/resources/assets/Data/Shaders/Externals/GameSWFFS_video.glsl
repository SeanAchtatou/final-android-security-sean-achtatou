#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp sampler2D TextureSampler;
uniform lowp sampler2D TextureSamplerCb;
uniform lowp sampler2D TextureSamplerCr;
uniform lowp sampler2D TextureSamplerAlpha;
uniform lowp vec4 AdditiveColor;

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

//lowp float R = 1.164 * (YCbCr.x - 0.0627)                           + 1.596 * (YCbCr.z - 0.5);
//lowp float G = 1.164 * (YCbCr.x - 0.0627) - 0.392 * (YCbCr.y - 0.5) - 0.813 * (YCbCr.z - 0.5);
//lowp float B = 1.164 * (YCbCr.x - 0.0627) + 2.017 * (YCbCr.y - 0.5);
									
const lowp mat4 YCbCrToRGB = mat4(	vec4(1.164,      1.164,      1.164,     0.0),
									vec4(0.0,       -0.392,      2.017,     0.0),
									vec4(1.596,     -0.813,      0.0,       0.0),
									vec4(-0.8709828, 0.5295172, -1.0814828, 0.0)
								);

void main()
{
	lowp vec4 YCbCr = vec4(texture2D(TextureSampler, vTexCoord0).a, texture2D(TextureSamplerCb, vTexCoord0).a, texture2D(TextureSamplerCr, vTexCoord0).a, 1.0);
	lowp vec4 rgb = YCbCrToRGB * YCbCr;
	lowp vec4 color = vec4(rgb.r, rgb.g, rgb.b, texture2D(TextureSamplerAlpha, vTexCoord0).a);
	
	color *= vColor0;

	gl_FragColor = color + AdditiveColor;
}
