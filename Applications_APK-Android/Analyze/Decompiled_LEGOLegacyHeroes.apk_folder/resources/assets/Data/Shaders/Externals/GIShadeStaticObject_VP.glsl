// Compiler options
//#define GI_EDITOR
//#define USE_NORMAL_MAP
//#define USE_BAKED_SHADOW
//#define USE_HIGHRES_LIGHTS
//#define USE_HOTSPOT

#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define attribute in
#    define varying out
#endif

// Shader options
#if !defined(USE_BAKED_SHADOW)
#	define USE_SHADOW
#endif // USE_BAKED_SHADOW

// Shadow options
#if defined(USE_SHADOW)
#	define USE_LISP_SHADOW 1
#endif // USE_SHADOW

// Transformation parameters
uniform highp mat4 World;
uniform highp mat4 WorldIT;
uniform highp mat4 WorldViewProjection;

// Attributes
attribute highp vec4 Vertex;
attribute lowp vec3 Normal;
#if defined(USE_NORMAL_MAP)
attribute lowp vec3 Tangent;
attribute lowp vec3 Binormal;
#endif // USE_NORMAL_MAP
attribute lowp vec2 TexCoord0;
attribute lowp vec2 TexCoord1;

// Shading parameters
uniform highp vec4 CameraPosition;
uniform lowp vec3 Light0Direction;

#if defined(USE_SHADOW)
// Shadow parameters
uniform highp mat4 ShadowMatrix;
uniform lowp float ShadowOpacityPowerTweaker;
uniform lowp float ShadowOffsetFactorTweaker;
uniform lowp float ShadowOffsetMinTweaker;
uniform lowp float ShadowOffsetMaxTweaker;
#endif // USE_SHADOW

#if !defined(USE_NORMAL_MAP)
// Tweakers
uniform lowp vec3 SunColorTweaker;
uniform lowp float SunPowerTweaker;
uniform lowp float DiffuseScaleTweaker;
uniform lowp float SpecularScaleTweaker;
uniform lowp float SpecularPowerTweaker;
#endif // !USE_NORMAL_MAP

// Varyings
#if defined(USE_NORMAL_MAP) || defined(USE_HOTSPOT)
varying highp vec4 vWorldVertex;
varying lowp vec4 vWorldNormal;
#endif // USE_NORMAL_MAP || USE_HOTSPOT
#if defined(USE_NORMAL_MAP)
varying lowp vec3 vWorldTangent;
varying lowp vec3 vWorldBinormal;
#endif // USE_NORMAL_MAP
varying lowp vec4 vCoord0and1;
#if defined(USE_HIGHRES_LIGHTS)
varying lowp vec4 vCoord2and3;
#endif // USE_HIGHRES_LIGHTS
varying lowp vec4 vCoord4and5;
varying lowp vec4 vCoord6and7;
#if defined(USE_SHADOW)
varying highp vec4 vShadowVertex;
#	if defined(USE_NORMAL_MAP)
#		define vShadowOpacity vWorldNormal.w
#	else
#		define vShadowOpacity vDirectColor.w
#	endif
#endif // USE_SHADOW
#if defined(USE_NORMAL_MAP)
#else // USE_NORMAL_MAP
varying lowp vec4 vDirectColor;
varying lowp vec4 vBasisWeights;
#endif // !USE_NORMAL_MAP

// main
void main(void)
{
	highp vec4 worldVertex = World * Vertex;
	lowp vec3 worldNormal = normalize((WorldIT * vec4(Normal, 0.0)).xyz); 
#if defined(USE_NORMAL_MAP) || defined(USE_HOTSPOT)
	vWorldVertex = worldVertex;
	vWorldNormal.xyz = worldNormal;
#endif // USE_NORMAL_MAP || USE_HOTSPOT
#if defined(USE_NORMAL_MAP)
	vWorldTangent = (WorldIT * vec4(Tangent, 0.0)).xyz; 
	vWorldBinormal  = (WorldIT * vec4(Binormal, 0.0)).xyz; 
#endif // USE_NORMAL_MAP	
	vCoord0and1.st = TexCoord0;
	vCoord0and1.pq = TexCoord1;
#if defined(USE_HIGHRES_LIGHTS)
	vCoord2and3.st = vCoord0and1.pq * vec2(1.0, 0.5);
	vCoord2and3.pq = vCoord2and3.st + vec2(0.0, 0.5);
#endif // USE_HIGHRES_LIGHTS
	vCoord4and5.st = vCoord0and1.pq * vec2(0.5, 0.5);
	vCoord4and5.pq = vCoord4and5.st + vec2(0.5, 0.0);
	vCoord6and7.pq = vCoord4and5.st + vec2(0.0, 0.5);
#if defined(USE_HOTSPOT)
	vCoord6and7.st = vCoord4and5.st + vec2(0.5, 0.5);
#endif // USE_HOTSPOT
	// Visibility factor
	lowp float NdotL = max(0.0, dot(worldNormal, Light0Direction));
#if defined(USE_SHADOW)
	// Compute shadow opacity
	vShadowOpacity = 1.0 - pow(1.0 - NdotL, ShadowOpacityPowerTweaker);	
	// Compute vertex position in LS
	vShadowVertex = ShadowMatrix * worldVertex;
#	if !defined(USE_LISP_SHADOW)
	// Compute depth bias
	highp float zBias = clamp(ShadowOffsetFactorTweaker * sqrt(1.0 - NdotL*NdotL) / NdotL, ShadowOffsetMinTweaker, ShadowOffsetMaxTweaker) * 0.0001;
	vShadowVertex.z -= zBias;
#	endif // USE_LISP_SHADOW
#endif // USE_SHADOW
#if defined(USE_NORMAL_MAP)
#else  // USE_NORMAL_MAP
	// Diffuse factor
	lowp float diffuseFactor = DiffuseScaleTweaker * NdotL;
	// Specular factor
	lowp vec3 view = normalize(CameraPosition.xyz - worldVertex.xyz); 
	lowp vec3 h = normalize(Light0Direction + view);
	lowp float specularFactor = SpecularScaleTweaker * pow(max(0.0, dot(worldNormal, h)), SpecularPowerTweaker);  
	// Direct color
	vDirectColor.xyz = (diffuseFactor + specularFactor) * SunColorTweaker * SunPowerTweaker;
	// Basis vectors
	const lowp vec3 basisUP1 = vec3(-1.0 / sqrt(6.0), -1.0 / sqrt(2.0),  1.0 / sqrt(3.0)	);
	const lowp vec3 basisUP2 = vec3(-1.0 / sqrt(6.0),  1.0 / sqrt(2.0),  1.0 / sqrt(3.0)	);
	const lowp vec3 basisUP3 = vec3( sqrt(2.0 / 3.0),              0.0,  1.0 / sqrt(3.0)	);	
	const lowp vec3 basisDW1 = vec3(             0.0,              0.0, -1.0		);
	const lowp vec4 basisScale = vec4(1.0, 1.0, 1.0, 0.5);
	const lowp vec4 basisBias = vec4(0.0, 0.0, 0.0, 0.5); 
	// Basis weights	
	lowp vec4 basisWeights = vec4(
		dot(worldNormal, basisUP1),
		dot(worldNormal, basisUP2),
		dot(worldNormal, basisUP3),
		dot(worldNormal, basisDW1));		
	basisWeights = clamp(basisWeights * basisScale + basisBias, 0.0, 1.0);
	basisWeights = normalize(basisWeights);
	vBasisWeights = basisWeights;	
#endif // USE_NORMAL_MAP
	gl_Position = WorldViewProjection * Vertex;
}
