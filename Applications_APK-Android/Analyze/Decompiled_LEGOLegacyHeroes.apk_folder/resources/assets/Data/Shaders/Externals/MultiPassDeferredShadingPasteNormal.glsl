//#define USE_SCEEN_SPACE_COORD
//#define VERTEX_SHADER
//#define FRAGMENT_SHADER
//#define CUMULATIVE
//#define ALBEDO
//#define NORMAL_PASTING
//#define LIGHT_ACCUMULATION
//#define SPECULAR_ACCUMULATION

//#define SSAO

//#define SSAO_BLUR_SEPARABLE
//#define SSAO_BLUR_H

// WARNING !! To be changed in code too !
#define SSAO_RESOLUTION_DIVIDER		4 

GLITCH_UNIFORM_PROPERTIES(DepthBuffer, (depthtexture = 1))

#ifdef CUMULATIVE

#	ifdef VERTEX_SHADER
attribute highp 	vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute mediump   vec2 TexCoord0; // DiffuseMap Coord
#	endif // VERTEX_SHADER

varying	mediump 	vec2 			vCoord0;
varying	mediump 	vec4 			vCoord1;

uniform highp mat4 WorldViewProjectionMatrix;

#ifdef VERTEX_SHADER
void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
	vCoord1 = gl_Position;
}
#endif

uniform lowp 		sampler2D 		DiffuseTexture;
uniform lowp 		sampler2D 		LightIntensityBuffer;
uniform lowp 		sampler2D 		SpecularIntensityBuffer;
uniform lowp		sampler2D		SSAOBuffer;

#ifdef FRAGMENT_SHADER
void main()
{
	mediump vec2 vCoordtmp = (vCoord1.xy / vCoord1.w) * 0.5 + 0.5;
	vCoordtmp.y = 1.0 - vCoordtmp.y;
	
	lowp vec3 albedo = texture2D(DiffuseTexture, vCoord0).rgb;
	
	gl_FragColor = vec4( albedo
						* texture2D(SSAOBuffer, vCoordtmp).rgb
					  * (texture2D(LightIntensityBuffer, vCoordtmp).rgb
					  + texture2D(SpecularIntensityBuffer, vCoordtmp).rgb)
					  ,1.0);
}
#endif

#endif 
// CUMULATIVE

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef NORMAL_PASTING

#	ifdef VERTEX_SHADER
attribute highp 	vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;
attribute highp 	vec3 Normal
#ifdef DCC_MAX
: NORMAL
#endif
;
attribute mediump   vec2 TexCoord0; // NormalMap Coord
#	endif

varying mediump		vec3 			vNormal;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 WorldViewIT;
uniform highp mat4 WorldIT;
uniform highp mat4 World;


#ifdef VERTEX_SHADER
void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	
	// calculate lighting
	vNormal = (WorldIT * vec4(Normal, 1.0)).xyz;
}
#endif

#ifdef FRAGMENT_SHADER
void main()
{
	gl_FragColor.rgb = normalize(vNormal.rgb) * 0.5 + 0.5;
	gl_FragColor.a = 1.0;
}
#endif
#endif 
// NORMAL_PASTING

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef LIGHT_ACCUMULATION
#	ifdef VERTEX_SHADER

attribute highp 	vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;
#	endif

// Common
varying	mediump 	vec4 			vCoord0;
varying	mediump 	vec4 			vCoord1;
// Vertex specific
uniform highp		mat4 			WorldViewProjectionMatrix;
uniform highp		mat4 			WorldViewMatrix;

#ifdef VERTEX_SHADER
void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = gl_Position;
}
#endif

// Fragment specific
uniform lowp 		sampler2D  		NormalBuffer;
uniform highp		sampler2D 		DepthBuffer;
uniform highp		vec4			Light0Position;
uniform lowp		vec4			Light0Color;
uniform mediump		vec3			Light0Attenuation;

uniform highp		mat4 			ViewProjectionI;

// Function for converting depth to world-space position
// in deferred pixel shader pass.  coord is a texture
// coordinate for a full-screen quad, such that x=0 is the
// left of the screen, and y=0 is the top of the screen.
highp vec3 getPositionFromDepth(mediump vec2 coord)
{
    
    highp vec3 pos = vec3(coord, texture2D(DepthBuffer, coord).r) * 2.0 - 1.0;
	
    // Transform by the inverse view projection matrix
    highp vec4 pos4 =  ViewProjectionI * vec4(pos,1.0);
  
    // Divide by w to get the world-space position
    return  pos4.xyz / pos4.w;
}

#ifdef FRAGMENT_SHADER
void main()
{
	// Read current pixel info
	mediump vec2 vCoordtmp = (vCoord0.xy / vCoord0.w) * 0.5 + 0.5;
	
	highp vec3 position = Light0Position.xyz - getPositionFromDepth(vCoordtmp);
	
	mediump vec3 normal = normalize(texture2D(NormalBuffer, vCoordtmp).rgb * 2.0 - 1.0);
	
	highp float distance = length(position);
	position /= distance;
	mediump float ndotl = max(0.0, dot(normal, position));
	//highp float attn = Light0Attenuation.x + Light0Attenuation.y * distance + Light0Attenuation.z * distance * distance;
	mediump float attn = Light0Attenuation.x + distance * (Light0Attenuation.y + Light0Attenuation.z * distance);
	
	gl_FragColor  = vec4(Light0Color.rgb * ndotl / attn,1.0);
}
#endif
#endif 
// LIGHT_ACCUMULATION


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef SPECULAR_ACCUMULATION
#	ifdef VERTEX_SHADER

attribute highp 	vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;
#	endif

// Common
varying	mediump 	vec4 			vCoord0;
// Vertex specific
uniform highp		mat4 			WorldViewProjectionMatrix;

#ifdef VERTEX_SHADER
void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = gl_Position;
}
#endif

// Fragment specific
uniform lowp 		sampler2D 		NormalBuffer;
uniform highp		sampler2D		DepthBuffer;
uniform highp		vec4			Light0Position;
uniform highp		vec3			Light0Direction;
uniform lowp		vec4			Light0Color;
uniform mediump		vec3			Light0Attenuation;
uniform highp		vec3			EyePosition;
uniform highp		mat4 			ViewProjectionI;

// Function for converting depth to world-space position
// in deferred pixel shader pass.  coord is a texture
// coordinate for a full-screen quad, such that x=0 is the
// left of the screen, and y=0 is the top of the screen.
highp vec3 getPositionFromDepth(mediump vec2 coord)
{
    
    highp vec3 pos = vec3(coord, texture2D(DepthBuffer, coord).r) * 2.0 - 1.0;
	
    // Transform by the inverse view projection matrix
    highp vec4 pos4 =  ViewProjectionI * vec4(pos,1.0);
  
    // Divide by w to get the world-space position
    return  pos4.xyz / pos4.w;
}

#ifdef FRAGMENT_SHADER
void main()
{

	// Read current pixel info
	mediump vec2 vCoordtmp = (vCoord0.xy / vCoord0.w) * 0.5 + 0.5;

	highp vec3 position = getPositionFromDepth(vCoordtmp);
	mediump vec3 normal = normalize(texture2D(NormalBuffer, vCoordtmp).rgb * 2.0 - 1.0);
	
	highp vec3 light0Direction = Light0Position.xyz - position;
	highp float distance = length(light0Direction);
	light0Direction /= distance;
		
	mediump vec3 halfVector = normalize(light0Direction + normalize(EyePosition - position));
	// No need to test ndotl value ??
	mediump float intensity = pow(max(0.0, dot(normal, halfVector)), 16.0);
		
	mediump float attn = Light0Attenuation.x + distance * (Light0Attenuation.y + Light0Attenuation.z * distance);
	
	gl_FragColor = vec4(Light0Color.rgb * intensity / attn,1.0);
}
#endif
#endif 

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef SSAO

#ifdef VERTEX_SHADER
attribute highp 	vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;
attribute highp 	vec3 Normal
#ifdef DCC_MAX
: NORMAL
#endif
;

#endif

varying	highp vec4 vCoord0;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 ViewProjectionI;

uniform highp sampler2D	DepthBuffer;
uniform mediump sampler2D NormalBuffer;

uniform mediump sampler2D SSAORandomSamples;

uniform mediump vec2 camnearandfar;
uniform mediump vec2 viewportdimension;
uniform mediump vec2 viewportdimensioninv;

#ifdef VERTEX_SHADER
void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = gl_Position;
}
#endif

#ifdef FRAGMENT_SHADER


#define NB_SAMPLES	16

highp vec3 getPositionFromDepth(mediump vec2 coord)
{
    
    highp vec3 pos = vec3(coord, texture2D(DepthBuffer, coord).r) * 2.0 - 1.0;
	
    // Transform by the inverse view projection matrix
    highp vec4 pos4 =  ViewProjectionI * vec4(pos,1.0);
  
	
    // Divide by w to get the world-space position
    return  pos4.xyz / pos4.w;
}


mediump vec3 getNormal(mediump vec2 coord)
{
	return normalize(texture2D(NormalBuffer, coord).rgb * 2.0 - 1.0);
}

void main()
{
	
	const mediump mat4 samplesSet1 = mat4(
	-0.286199, 0.331545,		 0.464995, -0.110527,
	 0.243161, 0.116612,		-0.170724, -0.155669,
	 0.340982, 0.263809,		 0.343742, -0.183868,
	-0.498398, 0.000477,		-0.101363, -0.288459
	);

	const mediump mat4 samplesSet2 = mat4(
	 0.376492, 0.508996,		-0.917514, -0.068567,
	 0.659013, 0.197366,		-0.825559, -0.147333,
	-0.488068, 0.127293,		-0.417507, -0.482348,
	-0.448707, 0.655000,		 0.599988, -0.695858
	);

	const lowp float bias = 0.1;
	const lowp float epsilon = 1.0;
	const lowp float distCoef = 1.0;
	
	const mediump float minRadius = 10.0;
	const mediump float maxRadius = 40.0;
	
	const mediump float globalScale = 2.0 * pow(2.0,float(SSAO_RESOLUTION_DIVIDER)-1.0);
	const mediump float minScale = 2.0 * globalScale / float(NB_SAMPLES);
	const mediump float maxScale = 50.0 * globalScale / float(NB_SAMPLES);
	
	// Read current pixel info
	mediump vec2 vCoordtmp = (vCoord0.xy / vCoord0.w) * 0.5 + 0.5;
	
	const mediump vec2 randomTexSizeInv = 1.0 / vec2(32.0,32.0);
	mediump vec4 rotVector = texture2D(SSAORandomSamples,vCoordtmp * viewportdimension * randomTexSizeInv) * 2.0 -1.0;
	
	mediump vec3 currentNormal = getNormal(vCoordtmp);
	highp vec3 currentPos = getPositionFromDepth(vCoordtmp) + 0.1 * currentNormal;
	
	mediump mat2 rotMatrix = mat2(normalize(rotVector.rg),normalize(rotVector.ba));
	
	mediump float depthCoef = (vCoord0.z - camnearandfar.x) / (camnearandfar.y - camnearandfar.x);
	mediump float radius = mix(maxRadius,minRadius,depthCoef);
	mediump float scale = mix(minScale,maxScale,depthCoef);
	
	
	
	mediump vec2 shiftScale = radius * viewportdimensioninv;// / float(SSAO_RESOLUTION_DIVIDER);
	
	// First sample set => 8 samples
	highp float aoFactor = 0.0;
	
	for(int i=0; i < 4; ++i) {
		
		mediump vec4 samples = samplesSet1[i];
		mediump vec2 tmpShift1 = rotMatrix * samples.xy;
		mediump vec2 tmpShift2 = rotMatrix * samples.zw;
		
		mediump vec2 tmpCoord1 = tmpShift1 * shiftScale + vCoordtmp;
		highp vec3 currentToSample1 = getPositionFromDepth(tmpCoord1) - currentPos;
		
		
		mediump float squareDistance1 = distCoef * dot(currentToSample1,currentToSample1);
		aoFactor += max(0.0,dot(normalize(currentToSample1),currentNormal) - bias) / (squareDistance1 + epsilon);
		
		mediump vec2 tmpCoord2 = tmpShift2 * shiftScale + vCoordtmp;
		highp vec3 currentToSample2 = getPositionFromDepth(tmpCoord2) - currentPos;
		
		mediump float squareDistance2 = distCoef * dot(currentToSample2,currentToSample2);
		aoFactor += max(0.0,dot(normalize(currentToSample2),currentNormal) - bias) / (squareDistance2 + epsilon);
	}
	
	// Second sample set => 8 samples
	for(int i=0; i < 4; ++i) {
		
		mediump vec4 samples = samplesSet2[i];
		mediump vec2 tmpShift1 = rotMatrix * samples.xy;
		mediump vec2 tmpShift2 = rotMatrix * samples.zw;
		
		mediump vec2 tmpCoord1 = tmpShift1 * shiftScale + vCoordtmp;
		highp vec3 currentToSample1 = getPositionFromDepth(tmpCoord1) - currentPos;
		
		mediump float squareDistance1 = distCoef * dot(currentToSample1,currentToSample1);
		aoFactor += max(0.0,dot(normalize(currentToSample1),currentNormal) - bias) / (squareDistance1 + epsilon);
		
		
		mediump vec2 tmpCoord2 = tmpShift2 * shiftScale + vCoordtmp;
		highp vec3 currentToSample2 = getPositionFromDepth(tmpCoord2) - currentPos;
		
		mediump float squareDistance2 = distCoef * dot(currentToSample2,currentToSample2);
		aoFactor += max(0.0,dot(normalize(currentToSample2),currentNormal) - bias) / (squareDistance2 + epsilon);
	}
	
	aoFactor = 1.0 - clamp(aoFactor * scale,0.0,1.0);
	gl_FragColor = vec4(aoFactor,aoFactor,aoFactor,1.0);

}
#endif
#endif 
// SSAO

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef SSAO_BLUR

#ifdef VERTEX_SHADER
attribute highp 	vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;
attribute highp 	vec3 Normal
#ifdef DCC_MAX
: NORMAL
#endif
;

#endif

varying	highp vec4 vCoord0;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 ViewProjectionI;

//uniform highp sampler2D	DepthBuffer;
//uniform highp sampler2D NormalBuffer;

uniform lowp sampler2D SSAOBuffer;

#ifdef VERTEX_SHADER
void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = gl_Position;
}
#endif

#ifdef FRAGMENT_SHADER


uniform highp vec2 viewportdimensioninv;

void main()
{
	lowp float aoFactor = 0.0;
	
	const lowp mat3 gaussianWeight = mat3(
		1.0/16.0, 2.0/16.0, 1.0/16.0,
		2.0/16.0, 4.0/16.0, 2.0/16.0,
		1.0/16.0, 2.0/16.0, 1.0/16.0
	);

	// Read current pixel info
	mediump vec2 vCoordtmp = (vCoord0.xy / vCoord0.w) * 0.5 + 0.5;
	
	//highp vec3 currentPos = getPositionFromDepth(vCoordtmp);
	
	for(int i=-1; i <= 1; ++i) {
		for(int j=-1; j <= 1; ++j)  {
			
			highp vec2 sampleCoord = vec2(float(i),float(j)) * viewportdimensioninv + vCoordtmp;
			aoFactor += gaussianWeight[i+1][j+1] * texture2D(SSAOBuffer,sampleCoord).r;
			
		}
	}
		
	gl_FragColor = vec4(aoFactor,aoFactor,aoFactor,1.0);
	
}
#endif
#endif 
// SSAO_BLUR

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// 5x5 Separable Gaussian blur
#ifdef SSAO_BLUR_SEPARABLE

#ifdef VERTEX_SHADER
attribute highp 	vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;
attribute highp 	vec3 Normal
#ifdef DCC_MAX
: NORMAL
#endif
;

#endif

uniform highp mat4 WorldViewProjectionMatrix;
uniform lowp sampler2D SSAOBuffer;

varying	highp vec4 vCoord0;

#ifdef VERTEX_SHADER
void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = gl_Position;
}
#endif

#ifdef FRAGMENT_SHADER

uniform highp vec2 viewportdimensioninv;

void main()
{
	const lowp float centralWeight = 7.0 / 17.0;
	const lowp vec2 neighbourWeight = vec2(4.0/17.0, 1.0/17.0);

	// Read current pixel info
	mediump vec2 vCoordtmp = (vCoord0.xy / vCoord0.w) * 0.5 + 0.5;
	
	mediump float aoFactor = centralWeight * texture2D(SSAOBuffer,vCoordtmp).r;
	
	// Look left and right (up and bottom) samples at once
			
#ifdef SSAO_BLUR_H

	mediump vec2 sampleCoord1 = vec2(viewportdimensioninv.x,0.0) + vCoordtmp;
	aoFactor += neighbourWeight[0] * texture2D(SSAOBuffer,sampleCoord1).r;
		
	mediump vec2 sampleCoord2 = vec2(-viewportdimensioninv.x,0.0) + vCoordtmp;
	aoFactor += neighbourWeight[0] * texture2D(SSAOBuffer,sampleCoord2).r;

	sampleCoord1 = vec2(2.0 * viewportdimensioninv.x,0.0) + vCoordtmp;
	aoFactor += neighbourWeight[1] * texture2D(SSAOBuffer,sampleCoord1).r;
		
	sampleCoord2 = vec2(-2.0 * viewportdimensioninv.x,0.0) + vCoordtmp;
	aoFactor += neighbourWeight[1] * texture2D(SSAOBuffer,sampleCoord2).r;
		
#elif defined(SSAO_BLUR_V)
		
	mediump vec2 sampleCoord1 = vec2(0.0,viewportdimensioninv.y) + vCoordtmp;
	aoFactor += neighbourWeight[0] * texture2D(SSAOBuffer,sampleCoord1).r;
		
	mediump vec2 sampleCoord2 = vec2(0.0,-viewportdimensioninv.y) + vCoordtmp;
	aoFactor += neighbourWeight[0] * texture2D(SSAOBuffer,sampleCoord2).r;

	sampleCoord1 = vec2(0.0,2.0 * viewportdimensioninv.y) + vCoordtmp;
	aoFactor += neighbourWeight[1] * texture2D(SSAOBuffer,sampleCoord1).r;
		
	sampleCoord2 = vec2(0.0,-2.0 * viewportdimensioninv.y) + vCoordtmp;
	aoFactor += neighbourWeight[1] * texture2D(SSAOBuffer,sampleCoord2).r;
		
#else
#    error ! You must define either SSAO_BLUR_H or SSAO_BLUR_V
#endif
		
	gl_FragColor = vec4(aoFactor,aoFactor,aoFactor,1.0);
	
}
#endif
#endif  // SSAO_BLURH
