attribute highp   vec4 Vertex;

#ifdef ALPHATEST
attribute mediump vec2 TexCoord0;
varying	  lowp    vec2 uv;
#endif

uniform   highp   mat4 WorldViewProjectionMatrix;

void main(void)
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	
#ifdef ALPHATEST
	uv = TexCoord0;
#endif
}