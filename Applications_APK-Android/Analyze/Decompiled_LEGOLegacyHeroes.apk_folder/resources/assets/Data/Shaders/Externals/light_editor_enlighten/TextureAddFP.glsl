uniform lowp sampler2D texture0;
uniform lowp sampler2D texture1;
uniform lowp sampler2D texture2;
uniform lowp sampler2D texture3;
uniform lowp sampler2D texture4;
uniform lowp sampler2D texture5;
uniform lowp sampler2D texture6;
uniform lowp sampler2D texture7;

varying mediump vec2 vCoord0;

void main()
{
    lowp vec4 tc0 = texture2D(texture0, vCoord0);
    lowp vec4 tc1 = texture2D(texture1, vCoord0);
    lowp vec4 tc2 = texture2D(texture2, vCoord0);
    lowp vec4 tc3 = texture2D(texture3, vCoord0);
    lowp vec4 tc4 = texture2D(texture4, vCoord0);
    lowp vec4 tc5 = texture2D(texture5, vCoord0);
    lowp vec4 tc6 = texture2D(texture6, vCoord0);
    lowp vec4 tc7 = texture2D(texture7, vCoord0);

    gl_FragColor = tc0 + tc1 + tc2 + tc3 + tc4 + tc5 + tc6 + tc7;
}