#version 400

noperspective in highp float vScreenSpaceDistance;

uniform float TotalSegmentSize;
uniform float SolidSegmentSize;
uniform vec4 LineColor;

out lowp vec4 fragColor;

void main()
{
    float t = mod(vScreenSpaceDistance, TotalSegmentSize);
	
	if(t <= SolidSegmentSize)
	{
		fragColor = LineColor;
	}
	else
	{
		discard;
	}
}
