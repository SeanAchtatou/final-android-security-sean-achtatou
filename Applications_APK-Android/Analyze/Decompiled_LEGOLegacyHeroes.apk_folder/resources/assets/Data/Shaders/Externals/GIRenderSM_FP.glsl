// Compiler options	
//#define USE_LISP

void main(void) 
{
#if defined(USE_LISP)
	gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);
#else
	gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
#endif
}