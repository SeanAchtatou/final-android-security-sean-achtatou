#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

void main(void) 
{
	gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}
