// when writing a fragment shader, precision for vecs & float has to be specified
// in vertex shader, precision highp float is implicit, so I report it here for this shader to compile properly
precision highp float;

#ifdef DCC_MAX
#define DM( x ) x
#else
#define DM( x )
#endif

varying vec2 vTexcoord DM( : TEXCOORD0 );

#ifdef TANGENTS
varying vec3 vTangent DM( : TEXCOORD1 );
#elif defined( BINORMALS )
varying vec3 vBinormal DM( : TEXCOORD1 );
#elif defined( NORMALS )
varying vec3 vNormal DM( : TEXCOORD1 );
#elif defined( VERTEX_COLORS )
varying vec3 vColor DM( : TEXCOORD1 );
#endif

void main()
{ 
#ifdef TEXCOORDS
	vec4 ret = vec4( fract( vTexcoord ), 0., 1. );
#elif defined( TANGENTS )
	vec4 ret = vec4( vTangent, 1. );
#elif defined( BINORMALS )
	vec4 ret = vec4( vBinormal, 1. );
#elif defined( NORMALS )
	vec4 ret = vec4( vNormal, 1. );
#elif defined( VERTEX_COLORS )
	vec4 ret = vec4( vColor, 1. );
#endif

	gl_FragColor = ret;
}

