#version 430

#define GROUP_SIZE 16
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE) in;

GLITCH_UNIFORM_PROPERTIES(NormalAndDepthTex, (access = r))
GLITCH_UNIFORM_PROPERTIES(TargetTex, (access = rw))

layout(rgba16f) uniform image2D NormalTex;
layout(rgba8) uniform image2D TargetTex;
			  
#ifndef MAX_LIGHTS
#define MAX_LIGHTS 5
#endif

#define SHADOW_MAP
			  
uniform sampler2D DepthTex;
uniform sampler2D ShadowMapCache;
uniform mat4 ViewProjectionI;
uniform mat4 TestViewProjectionI;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform int InitFromLast;
uniform int NbLights;

#ifdef POINT_LIGHT

struct Light
{
	vec4 PositionAndRadius;
	vec4 AttenuationAndIntensity;
	vec4 Color; //Shadow mapping if alpha is 1.
	mat4 ProjectionMat; //Bias included
	vec4 ScaleOffset[6];
	int Channels[6];
};

#elif defined(SPOT_LIGHT)

struct Light
{
	vec4 PositionAndRadius;
	vec4 AttenuationAndIntensity;
	vec4 Color; //Shadow mapping if alpha is 1.
	mat4 ViewProjectionMat; //Bias included
	vec4 ScaleOffset;
	vec4 ConeAnglesSpotExponentChannel;
	vec3 Direction;
};

#else //DIRECTIONAL_LIGHT

struct Light
{
	vec4 DirectionAndIntensity;
	vec4 Color; //Channel + 1 is a, if a is 0, no shadow map.
	mat4 ViewProjectionMat; //Bias included
	vec4 ScaleOffset;
};

#endif

uniform LightBlock { Light Lights[MAX_LIGHTS]; };

#if defined(POINT_LIGHT) && defined(SHADOW_MAP)

const mat4 VIEW_TEMPLATE[6] = { mat4(0.000000000, 0.000000000, -1.00000000, 0.000000000,
									 -0.000000000, 1.00000000, 0.000000000, 0.000000000,
									 1.00000000, 0.000000000, 0.000000000, 0.000000000,
									 -0.000000000, -0.000000000, -0.000000000, 1.00000000),
									 
								mat4(0.000000000, -0.000000000,	1.00000000, 0.000000000,
									 0.000000000, 1.00000000, 0.000000000, 0.000000000,
									 -1.00000000, 0.000000000, 0.000000000, 0.000000000,
									 -0.000000000, -0.000000000, -0.000000000, 1.00000000),	 
									 
								mat4(-1.00000000, 0.000000000, 0.000000000, 0.000000000,
									 -0.000000000, 0.000000000, -1.00000000, 0.000000000,
									 -0.000000000, -1.00000000, 0.000000000, 0.000000000,
									 0.000000000, -0.000000000, -0.000000000, 1.00000000),
									 
								mat4(-1.00000000, 0.000000000, 0.000000000, 0.000000000,
									 0.000000000, -0.000000000, 1.00000000, 0.000000000,
									 0.000000000, 1.00000000, 0.000000000, 0.000000000,
									 -0.000000000, -0.000000000, -0.000000000, 1.00000000),
									 
								mat4(-1.00000000, 0.000000000, 0.000000000, 0.000000000,
									 0.000000000, 1.00000000, 0.000000000, 0.000000000,
									 0.000000000, 0.000000000, -1.00000000, 0.000000000,
									 -0.000000000, -0.000000000, -0.000000000, 1.00000000),
									 
								mat4(1.00000000, 0.000000000, 0.000000000, 0.000000000,
									 0.000000000, 1.00000000, 0.000000000, 0.000000000,
									 0.000000000, 0.000000000, 1.00000000, 0.000000000,
									 -0.000000000, -0.000000000, -0.000000000, 1.00000000) };
									 
const vec3 LIGHT_DIRS[6] = { vec3(-1, 0, 0),
							 vec3(1, 0, 0),
							 vec3(0, -1, 0),
							 vec3(0, 1, 0),
							 vec3(0, 0, -1),
							 vec3(0, 0, 1) };	 

int getCubeFace(vec3 dir)
{
	vec3 absDir = abs(dir);

	if(absDir.x > absDir.y)
	{
		if(absDir.x > absDir.z)
		{
			return dir.x > 0 ? 0 : 1;
		}
		else
		{
			return dir.z > 0 ? 4 : 5;
		}
	}
	else if(absDir.y > absDir.z)
	{
		return dir.y > 0 ? 2 : 3;
	}
	else
	{
		return dir.z > 0 ? 4 : 5;
	}
}

#endif

vec3 getPositionFromDepth(vec2 coord, float depth)
{
    const vec4 pos = vec4(vec3(coord, depth) * 2.0 - 1.0, 1.0);
	
    const vec4 pos4 = ViewProjectionI * pos;
  
    return pos4.xyz / pos4.w;
}


shared uint CurrentLightCount;
shared uint VisibleLightIndices[MAX_LIGHTS];

#ifndef DIRECTIONAL_LIGHT 

shared uint MinZ;
shared uint MaxZ;

vec4 FrustumPlanes[6];

bool inFrustum(uint li)
{
	const vec4 pos = ViewMatrix * vec4(Lights[li].PositionAndRadius.xyz, 1.0); //Light position in view space

	for(uint i = 3; i >= 0; --i)
	{
		const float dist = dot(FrustumPlanes[i], pos);
		if(-Lights[li].PositionAndRadius.w > dist)
		{
			return false;
		}
	}
	
	return true;
}

#endif

void main()
{
	vec3 bidon = vec3(2812.934);
	vec3 test = bidon;
	
	CurrentLightCount = 0;
	
#ifndef DIRECTIONAL_LIGHT 

	MinZ = 0xFFFFFFFF;
	MaxZ = 0;
	
#endif
	
	barrier();
	
	const ivec2 local_xy = ivec2(gl_LocalInvocationID);
	const ivec2 image_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE + local_xy;
	const ivec2 image_size = imageSize(NormalTex);
	const vec2 pos_xy = (vec2(image_xy) + 0.5) / image_size;
	
	const vec3 normal = imageLoad(NormalTex, image_xy).xyz;
	const float depth = texture(DepthTex, pos_xy).r;
	const vec3 pos = getPositionFromDepth(pos_xy, depth);
	
	// Culling lights ---------------------------------------------------------
	
#ifndef DIRECTIONAL_LIGHT
	
	const uint udepth = uint(depth * 0xFFFFFFFF);
	
	atomicMin(MinZ, udepth);
	atomicMax(MaxZ, udepth);
	
	barrier();
	
	const float minDepth = float(MinZ / float(0xFFFFFFFF));
	const float maxDepth = float(MaxZ / float(0xFFFFFFFF));
	
	vec2 tileScale = vec2(image_size) / float(2 * GROUP_SIZE);
	vec2 tileBias = tileScale - vec2(gl_WorkGroupID.xy);
	
	vec4 c1 = vec4(-ProjectionMatrix[0][0] * tileScale.x, 0.0f, tileBias.x, 0.0f);
	vec4 c2 = vec4(0.0f, -ProjectionMatrix[1][1] * tileScale.y, tileBias.y, 0.0f);
	vec4 c4 = vec4(0.0f, 0.0f, 1.0f, 0.0f);
	
	// Sides
	//right
	FrustumPlanes[0] = c4 - c1;
	//left
	FrustumPlanes[1] = c4 + c1;
	//bottom
	FrustumPlanes[2] = c4 - c2;
	//top
	FrustumPlanes[3] = c4 + c2;
	// Near/far
	FrustumPlanes[4] = vec4(0.0f, 0.0f,  1.0f, -minDepth);
	FrustumPlanes[5] = vec4(0.0f, 0.0f, -1.0f,  maxDepth);
	
	for(int i = 0; i < 4; ++i)
	{
		FrustumPlanes[i] /= length(FrustumPlanes[i].xyz);
	}
	
	//Light culling
	
	for(uint li = gl_LocalInvocationIndex; li < NbLights; li += GROUP_SIZE)
	{	
		//test = vec3(1.0);
		if(inFrustum(li) || true)
		{	
			const uint id = atomicAdd(CurrentLightCount, 1);
			VisibleLightIndices[id] = li;
		}
	}
	
#else

	// Directional lights, just tag all lights as visible
	
	for(uint li = gl_LocalInvocationIndex; li < NbLights; li += GROUP_SIZE)
	{		
		const uint id = atomicAdd(CurrentLightCount, 1);
		VisibleLightIndices[id] = li;
	}

#endif
	
	barrier();	
	
	vec3 accum = mix(vec3(0.0), imageLoad(TargetTex, image_xy).rgb, bvec3(InitFromLast));
	
	//test = Lights[0].Color;
	
	for(int i = 0; i < CurrentLightCount; ++i)
	{	
		const uint lId = VisibleLightIndices[i];
		
#ifdef DIRECTIONAL_LIGHT
		vec3 dir = -Lights[lId].DirectionAndIntensity.xyz;
#else
		//const vec3 lPos = vec3(0.0, 0.0, 100.0);
		const vec3 lPos = Lights[lId].PositionAndRadius.xyz;
		vec3 dir = lPos - pos;
		const float distance = length(dir);
		//test = dir;
		dir = normalize(dir);
#endif
		
		float shadowFactor = 1.0;

		if(Lights[lId].Color.a > 0.0)
		{		
#if defined(POINT_LIGHT) && defined(SHADOW_MAP)
			const vec3 posPermutations[6] = { vec3(-lPos.z, -lPos.y, lPos.x),
											  vec3(lPos.z, -lPos.y, -lPos.x),
											  vec3(lPos.x, lPos.z, lPos.y),
											  vec3(lPos.x, -lPos.z, -lPos.y),
											  vec3(lPos.x, -lPos.y, lPos.z),
											  vec3(-lPos.x, -lPos.y, -lPos.z) };
			//Shadow Map
			
			int f = getCubeFace(-dir);
			//int f = int(dir.x);
			
			mat4 lightView = VIEW_TEMPLATE[f];
			lightView[3][0] = posPermutations[f][0];
			lightView[3][1] = posPermutations[f][1];
			lightView[3][2] = posPermutations[f][2];
			const mat4 lightMatrix = Lights[lId].ProjectionMat * lightView;
			const vec4 lScaleOffset = Lights[lId].ScaleOffset[f];
			const int ch = Lights[lId].Channels[f];
#elif defined(SPOT_LIGHT) && defined(SHADOW_MAP)
			const int ch = int(Lights[lId].ConeAnglesSpotExponentChannel.w);
			const mat4 lightMatrix = Lights[lId].ViewProjectionMat;
			const vec4 lScaleOffset = Lights[lId].ScaleOffset;
#elif defined(SHADOW_MAP)
			const int ch = int(Lights[lId].Color.a - 1.0);
			const mat4 lightMatrix = Lights[lId].ViewProjectionMat;
			const vec4 lScaleOffset = Lights[lId].ScaleOffset;
#endif

#ifdef SHADOW_MAP
			const vec4 posInDepthMap = lightMatrix * vec4(pos, 1.0);
			const vec3 posTest = posInDepthMap.xyz / posInDepthMap.w;
				
			vec2 coord = posTest.xy * lScaleOffset.xy + lScaleOffset.zw;	
			const vec4 zCompare = texture(ShadowMapCache, coord);
			//const vec4 zCompare = vec4(1.0);
			
			shadowFactor = float(zCompare[ch] >= posTest.z);
			
			//test = vec3(posTest.z);
			//test = vec3(coord.xy, 0.0);
			//test = vec3(shadowFactor);
#endif
		}
		
#ifdef POINT_LIGHT

		if(shadowFactor > 0.0)
		{
			const float hardCutOff = 1.0 - step(Lights[lId].PositionAndRadius.w, distance);
		
			const float ndotl = max(0.0, dot(normal, dir));
			const vec3 lAttn = Lights[lId].AttenuationAndIntensity.xyz;
			const float attn = lAttn.x + distance * (lAttn.y + lAttn.z * distance);
		
			accum += Lights[lId].Color.rgb * ndotl * hardCutOff * Lights[lId].AttenuationAndIntensity.w * shadowFactor / attn;
			
			//test = vec3((depth - 0.95) * 15);
			//test = vec3(distance);
		}
		
#elif defined(SPOT_LIGHT)
		if(shadowFactor > 0.0)
		{
			const vec3 lAttn = Lights[lId].AttenuationAndIntensity.xyz;
			const float att1 = lAttn.x + distance * (lAttn.y + lAttn.z * distance);

            //this code suppose the half angle of the spot light is not greater than 90.
            //Yes, this is a quite heavy, but it is for baking lightmaps on a PC, so it doesn't matter much

            float inAngleRad = radians(Lights[lId].ConeAnglesSpotExponentChannel.x * 0.5);
            float outAngleRad = radians(Lights[lId].ConeAnglesSpotExponentChannel.y * 0.5);
            float lightCosAngle = max(dot(-Lights[lId].Direction, dir), 0.);
            float lightAngle = acos(lightCosAngle);

            float weight = step(lightAngle, outAngleRad);

            lightAngle = max(lightAngle - inAngleRad, 0.);
            outAngleRad -= inAngleRad;

            lightCosAngle = cos(lightAngle);
            float outCosAngle = cos(outAngleRad);

            float stepper = (lightCosAngle - outCosAngle) / (1. - outCosAngle);
                
            float att2 = pow(smoothstep(0., 1., stepper), Lights[lId].ConeAnglesSpotExponentChannel.z);
                
            accum += Lights[lId].Color.rgb * max(dot(normal, dir), 0.) / att1 * att2 * weight * Lights[lId].AttenuationAndIntensity.w * shadowFactor;
			//test = Lights[lId].Color.rgb * max(dot(normal, dir), 0.) / att1 * att2 * weight * Lights[lId].AttenuationAndIntensity.w * shadowFactor;
		}
#else
		accum += Lights[lId].Color.rgb * max(0.0, dot(normal, dir)) * Lights[lId].DirectionAndIntensity.w * shadowFactor;
		//test = vec3(shadowFactor);
#endif
	}
	
	imageStore(TargetTex, image_xy, vec4(accum, 1.0));
	
	if(bidon != test)
	{
		imageStore(TargetTex, image_xy, vec4(test, 1.0));
	}
	
	memoryBarrierImage();
}
