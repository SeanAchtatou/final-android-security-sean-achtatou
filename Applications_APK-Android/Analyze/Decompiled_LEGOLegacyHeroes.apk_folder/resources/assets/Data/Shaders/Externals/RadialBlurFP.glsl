uniform mediump sampler2D framebufferColor;

varying mediump vec2 uv;

// some const, tweak for best look
const mediump float sampleDist = 0.5;
const mediump float sampleStrength = 10.0; 

void main()
{
    // 0.5,0.5 is the center of the screen
    // so substracting uv from it will result in
    // a vector pointing to the middle of the screen
    mediump vec2 dir = 0.5 - uv; 
 
    // calculate the distance to the center of the screen
    mediump float dist = length(dir); 
 
    // normalize the direction (reuse the distance)
    dir = dir/dist; 
 
    // this is the original colour of this fragment
    // using only this would result in a nonblurred version
    mediump vec4 color = texture2D(framebufferColor, uv); 
 
    mediump vec4 sum = color;
 
    // take 10 additional blur samples in the direction towards
    // the center of the screen
    sum += texture2D(framebufferColor, uv + dir * -0.08 * sampleDist);
    sum += texture2D(framebufferColor, uv + dir * -0.05 * sampleDist); 
	sum += texture2D(framebufferColor, uv + dir * -0.03 * sampleDist); 
    sum += texture2D(framebufferColor, uv + dir * -0.02 * sampleDist);
    sum += texture2D(framebufferColor, uv + dir * -0.01 * sampleDist);
    sum += texture2D(framebufferColor, uv + dir *  0.01 * sampleDist);
    sum += texture2D(framebufferColor, uv + dir *  0.02 * sampleDist);
    sum += texture2D(framebufferColor, uv + dir *  0.03 * sampleDist);
    sum += texture2D(framebufferColor, uv + dir *  0.05 * sampleDist);
    sum += texture2D(framebufferColor, uv + dir *  0.08 * sampleDist);

    // we have taken eleven samples
    sum *= 1.0/11.0;
 
    // weighten the blur effect with the distance to the
    // center of the screen ( further out is blurred more)
    mediump float t = clamp(dist * sampleStrength ,0.0,1.0); //0 &lt;= t &lt;= 1
 
    //Blend the original color with the averaged pixels
    gl_FragColor = mix(color, sum, t);
}

