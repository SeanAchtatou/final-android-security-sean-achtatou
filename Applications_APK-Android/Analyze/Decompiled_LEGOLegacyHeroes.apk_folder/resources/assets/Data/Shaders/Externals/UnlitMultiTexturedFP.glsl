#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp    sampler2D texture1;
uniform lowp    sampler2D texture2;

#if defined(SPLIT_ALPHA1) || defined(SPLIT_ALPHA)
uniform lowp    sampler2D texture1_alpha;
uniform lowp    int       texture1_alphachannel;
#endif

#if defined(SPLIT_ALPHA2) || defined(SPLIT_ALPHA)
uniform lowp    sampler2D texture2_alpha;
uniform lowp    int       texture2_alphachannel;
#endif

varying mediump vec2 vCoord0;
varying	lowp vec4 vColor0;

void main()
{
	lowp vec4 color1 =
#if defined(SPLIT_ALPHA1) || defined(SPLIT_ALPHA)
		vec4(texture2D(texture1, vCoord0).rgb,
			 texture2D(texture1_alpha, vCoord0)[texture1_alphachannel]);
#else
		texture2D(texture1, vCoord0);
#endif
	lowp vec4 color2 =
#if defined(SPLIT_ALPHA2) || defined(SPLIT_ALPHA)
		vec4(texture2D(texture2, vCoord0).rgb,
			 texture2D(texture2_alpha, vCoord0)[texture2_alphachannel]);
#else
		texture2D(texture2, vCoord0);
#endif
    gl_FragColor = color1 * color2 * vColor0;
}
