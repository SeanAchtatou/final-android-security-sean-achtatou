#version 400

uniform sampler2D Diffuse;
uniform sampler2D Indirect;
uniform vec4 DiffuseColor;

in vec2 vTexCoord0;
in vec2 vTexCoord1;
in vec3 NormalizedNormal;
in float Depth;

layout(location = 0) out vec4 NormalAndDepth;
layout(location = 1) out vec4 DiffuseColorOut;
layout(location = 2) out vec4 IndirectColorOut;

void main()
{
	DiffuseColorOut = texture2D(Diffuse, vTexCoord0) * DiffuseColor;
	IndirectColorOut = texture2D(Indirect, vTexCoord1);
	NormalAndDepth = vec4(normalize(NormalizedNormal), gl_FragCoord.z / gl_FragCoord.w);
}
