// options
//#define USE_ALPHA_MAP

// Samplers
uniform sampler2D diffuse;
uniform sampler2D bump;
#if defined(USE_ALPHA_MAP)
uniform sampler2D alpha;
#endif // USE_ALPHA_MAP

// Shading parameters
uniform highp vec4 CameraPosition;
uniform lowp vec3 Light0Direction;

// Varyings
varying lowp vec4 vWorldVertex;
varying lowp vec3 vWorldNormal;
varying lowp vec3 vWorldTangent;
varying lowp vec3 vWorldBinormal;
varying lowp vec2 vCoord0;
varying lowp vec2 vCoord1;

lowp float getUVBlend(lowp vec2 coord, int lightmapSize)
{
	lowp vec2 uv = abs(fract(coord * float(lightmapSize)) * 2.0 - 1.0);
	lowp float uvBlend = max(uv.r, uv.g);
	uvBlend *= uvBlend; //^2
	uvBlend *= uvBlend; //^4
	uvBlend *= uvBlend; //^8
	uvBlend *= uvBlend; //^16
	return uvBlend;
}

// main
void main(void)
{  	
	// Fetch albedo
	lowp vec3 albedo = texture2D(diffuse, vCoord0).rgb;
	// Fetch bump
	lowp vec3 normalmap = texture2D(bump, vCoord0).rgb;
	lowp vec3 normalTS = normalmap - vec3(0.5, 0.5, 0.5);
	// Fetch alpha
#if defined(USE_ALPHA_MAP)
	lowp float alphaBlend = texture2D(alpha, vCoord0).r;
#else
	const lowp float alphaBlend = 1.0;
#endif // USE_ALPHA_MAP
	// Compute bumped normal
	lowp vec3 worldNormal = normalize(vWorldNormal);
	lowp vec3 worldTangent = normalize(vWorldTangent);
	lowp vec3 worldBinormal = normalize(vWorldBinormal);	
	lowp vec3 bumpedNormal = normalize(normalTS.x * worldTangent + normalTS.y * worldBinormal + normalTS.z * worldNormal);
	// Visibility factor
	lowp float NdotL = max(0.0, dot(bumpedNormal, Light0Direction));
	// Diffuse factor
	lowp float diffuseFactor = NdotL;
	// Specular factor
	lowp vec3 view = normalize(CameraPosition.xyz - vWorldVertex.xyz); 
	lowp vec3 halfLight = normalize(Light0Direction + view);
	lowp float specularFactor = pow(max(0.0, dot(bumpedNormal, halfLight)), 32.0);  
	// Compute final color
	lowp vec3 color = albedo * (0.25 + 0.75 * (diffuseFactor + specularFactor));
	// Compute uv grid color
	lowp float uvBlend0 = getUVBlend(vCoord0, 32);
	lowp float uvBlend1 = getUVBlend(vCoord1, 32);
	lowp vec3 uvColor = uvBlend1 > uvBlend0 ? vec3(0.5, 0.8, 0.5) : vec3(0.8, 0.5, 0.5);
	color = mix(color, uvColor, 0.5 * max(uvBlend0, uvBlend1));
	gl_FragColor = vec4(color, alphaBlend);
}