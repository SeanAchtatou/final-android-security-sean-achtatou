#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp sampler2D TextureSampler;
uniform lowp sampler2D TextureSamplerMap;
uniform lowp float Premultiply;

varying highp vec2 vTexCoord0;
varying highp vec2 vTexCoord1;

void main()
{
	lowp vec4 color = texture2D(TextureSampler, vTexCoord0);
	lowp float alpha = texture2D(TextureSamplerMap, vTexCoord1).a;
	
	if(vTexCoord1.x < 0.0 || vTexCoord1.y < 0.0 || vTexCoord1.x > 1.0 || vTexCoord1.y > 1.0)
	{
		alpha = 0.0;
	}
	
	color.a *= alpha;
	
	if(Premultiply != 0.0) // render target
		color.rgb *= color.a;
	
	gl_FragColor = color;
}
