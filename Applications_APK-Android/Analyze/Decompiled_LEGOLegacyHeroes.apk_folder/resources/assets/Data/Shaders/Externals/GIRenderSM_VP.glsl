// Compiler options	
//#define USE_LISP

attribute highp vec4 Vertex;
uniform highp mat4 WorldViewProjectionMatrix;

void main(void) 
{
#if defined(USE_LISP)
	// Snap objects that are in light frustrum but not in lisp
	highp vec4 transformedVertex = WorldViewProjectionMatrix * Vertex;
	transformedVertex.z = max(-1.0, transformedVertex.z / transformedVertex.w) * transformedVertex.w;	
	gl_Position = transformedVertex;
#else
	gl_Position = WorldViewProjectionMatrix * Vertex;
#endif
}
