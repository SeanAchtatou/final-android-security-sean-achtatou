#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define attribute in
#    define varying out
#endif

attribute highp vec4 Position;

attribute mediump vec2 TexCoord0;
varying mediump vec2 vTexCoord0;
varying mediump vec2 vTexCoord1;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 UVMatrix;


void main(void)
{
	gl_Position = WorldViewProjectionMatrix * Position;

	vTexCoord0 = TexCoord0;
	vTexCoord1 = (UVMatrix * vec4(TexCoord0.x, TexCoord0.y, 0.0, 1.0)).xy;
}
