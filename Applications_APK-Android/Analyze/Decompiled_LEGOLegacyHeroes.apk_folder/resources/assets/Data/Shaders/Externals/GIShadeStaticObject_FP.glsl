// Compiler options	
//#define GI_EDITOR
//#define USE_NORMAL_MAP
//#define USE_BLURRED_SHADOW
//#define USE_BAKED_SHADOW
//#define USE_HIGHRES_LIGHTS
//#define USE_HOTSPOT

// Shader options
#define USE_ALBEDO
#define USE_AMBIENT_OCCLUSION
#define USE_TEXTURE_NORMALIZATION
#if! defined(USE_BAKED_SHADOW)
#	define USE_SHADOW
#endif
#if !defined(GL_ES)
#	define USE_TWEAKERS
#endif

#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
#    define textureCube texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

// Shadow options
#if defined(USE_SHADOW)
#	if !defined(USE_HW_SHADOW)
#		if defined(HIDE_GLSL_DEFINES)
#			define USE_HW_SHADOW 0 // Workaround to allow this shader to be compiled from the debugger
#		else
#			define USE_HW_SHADOW 1
#		endif
#	endif
#	define USE_LISP_SHADOW 1
#endif

#define optional_normalize

// Lights options
#if defined(USE_HIGHRES_LIGHTS)
#	if defined(GI_EDITOR)
#		define USE_LIGHT_DYNAMIC_COLORS			1
#	else
#		define USE_LIGHT_DYNAMIC_COLORS			0
#	endif // GI_EDITOR
#	define MAX_LIGHT_SWITCH_COUNT_PER_TEXEL		4
#	define MAX_LIGHT_SWITCH_COUNT				16
#endif

// Shadow sampler
#if defined(USE_SHADOW)
#	if USE_HW_SHADOW
#		if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#			define _shadow2D(sm, sv) texture(sm, sv)
#		elif defined(GL_ES)
#			extension GL_EXT_shadow_samplers : require
#			define _shadow2D(sm, sv) shadow2DEXT(sm, sv)
#		else // defined(GL_ES)
#			define _shadow2D(sm, sv) shadow2D(sm, sv).r
#		endif // defined(GL_ES)
#if defined(GLSL2METAL)
GLITCH_UNIFORM_PROPERTIES(ShadowMap, (samplerCompareFunc = lessEqual, samplerAddress = clampToEdge, samplerFilter = linear))
#endif
uniform lowp sampler2DShadow ShadowMap;
#	else // USE_HW_SHADOW
#		define _shadow2D(sm, sv) float(texture2D(sm, sv.xy).r > sv.z)
uniform lowp sampler2D ShadowMap;
#	endif // USE_HW_SHADOW
#endif // USE_SHADOW

// Shading parameters
uniform highp vec4 CameraPosition;
uniform lowp vec3 Light0Direction;

// Diffuse sampler
uniform lowp sampler2D diffuse;

#if defined(USE_NORMAL_MAP)
// Normal sampler
uniform lowp sampler2D bump;
#endif // USE_NORMAL_MAP

#if defined(USE_TEXTURE_NORMALIZATION)
uniform lowp samplerCube NormalizationCubeTex;
#endif // USE_TEXTURE_NORMALIZATION

// Ambient Occlusion sample
#if !defined(USE_HIGHRES_LIGHTS) && defined(USE_AMBIENT_OCCLUSION)
uniform lowp sampler2D AmbientOcclusionTex;
#endif // USE_AMBIENT_OCCLUSION

// Baked Shadow sampler
#if defined(USE_BAKED_SHADOW)
uniform lowp sampler2D BakedShadowTex;
#endif

// Lightmap lights samplers
#if defined(USE_HIGHRES_LIGHTS)
#	if USE_LIGHT_DYNAMIC_COLORS
uniform lowp sampler2D LightmapDynamicColors;
#		if defined(GI_EDITOR)
uniform lowp vec3 LightSwitchColors[MAX_LIGHT_SWITCH_COUNT];
#		else // GI_EDITOR
uniform lowp sampler2D LightSwitchColors;
#		endif // GI_EDITOR
#		define LightmapColors 					LightmapDynamicColors
#		define LightmapLightSwitchIntensities 	LightmapDynamicColors
uniform lowp sampler2D LightmapLightSwitchIds;
#	else
uniform lowp sampler2D LightmapStaticColors;
#		define LightmapColors 					LightmapStaticColors
#	endif // USE_LIGHT_DYNAMIC_COLORS
#endif // USE_HIGHRES_LIGHTS

// Lightmap environment samplers
uniform lowp sampler2D LightmapGITex;
#if defined(USE_HOTSPOT)
#define LightmapHotSpotDirectionTex LightmapGITex
uniform lowp sampler2D LightmapHotSpotColorTex;
#endif // USE_HOTSPOT

// Tweakers
uniform lowp float AmbientOcclusionTweaker;
uniform lowp float DiffusemapBlendTweaker;
#if defined(USE_NORMAL_MAP)
uniform lowp vec3 SunColorTweaker;
uniform lowp float SunPowerTweaker;
uniform lowp float DiffuseScaleTweaker;
uniform lowp float SpecularScaleTweaker;
uniform lowp float SpecularPowerTweaker;
#endif // USE_NORMAL_MAP
#if defined(USE_HOTSPOT)
uniform lowp float HotSpotSpecularScaleTweaker;
uniform lowp float HotSpotSpecularPowerTweaker;
#endif // USE_HOTSPOT

// Varyings
#if defined(USE_NORMAL_MAP) || defined(USE_HOTSPOT)
varying highp vec4 vWorldVertex;
varying lowp vec4 vWorldNormal;
#endif // USE_NORMAL_MAP || USE_HOTSPOT
#if defined(USE_NORMAL_MAP)
varying lowp vec3 vWorldTangent;
varying lowp vec3 vWorldBinormal;
#endif // USE_NORMAL_MAP
varying lowp vec4 vCoord0and1;
#if defined(USE_HIGHRES_LIGHTS)
varying lowp vec4 vCoord2and3;
#endif
varying lowp vec4 vCoord4and5;
varying lowp vec4 vCoord6and7;
#if defined(USE_SHADOW)
varying highp vec4 vShadowVertex;
#	if defined(USE_NORMAL_MAP)
#		define vShadowOpacity vWorldNormal.w
#	else
#		define vShadowOpacity vDirectColor.w
#	endif
#endif // USE_SHADOW
#if defined(USE_NORMAL_MAP)
#else // USE_NORMAL_MAP
varying lowp vec4 vDirectColor;
varying lowp vec4 vBasisWeights;
#endif // USE_NORMAL_MAP

// main
void main(void)
{  	
	// Get diffusemap uv set
	lowp vec2 vCoordDiffusemap = vCoord0and1.st;
	// Get lightmap uv set
	lowp vec2 vCoordLightmap = vCoord0and1.pq;
#if defined(USE_ALBEDO)
	// Fetch albedo
	lowp vec3 albedo = texture2D(diffuse, vCoordDiffusemap).rgb;
#else
	const lowp vec3 albedo = vec3(1.0, 1.0, 1.0);
#endif // USE_ALBEDO
#if defined(USE_NORMAL_MAP)
	// Fetch bump
	lowp vec3 normalmap = texture2D(bump, vCoordDiffusemap).rgb;
	lowp vec3 normalTS = (normalmap - vec3(0.5, 0.5, 0.5));
#endif // USE_NORMAL_MAP
#if defined(USE_SHADOW)
	// Fetch shadow map
#	if defined(USE_LISP_SHADOW)
	// Compute vertex position in LS
	highp vec3 shadowVertex = vShadowVertex.xyz / vShadowVertex.w;
#	else // USE_LISP_SHADOW
	highp vec3 shadowVertex = vShadowVertex.xyz;
#	endif // USE_LISP_SHADOW	
#	if defined(USE_BLURRED_SHADOW)
	const highp float shadowOffset = 0.5 / 1024.0;
	highp vec3 shadowVertex0 = shadowVertex + vec3(-shadowOffset, -shadowOffset, 0.0);
	highp vec3 shadowVertex1 = shadowVertex + vec3( shadowOffset, -shadowOffset, 0.0);
	highp vec3 shadowVertex2 = shadowVertex + vec3( shadowOffset,  shadowOffset, 0.0);
	highp vec3 shadowVertex3 = shadowVertex + vec3(-shadowOffset,  shadowOffset, 0.0);
	lowp float visibility = 
		_shadow2D(ShadowMap, shadowVertex0) +
		_shadow2D(ShadowMap, shadowVertex1) +
		_shadow2D(ShadowMap, shadowVertex2) + 
		_shadow2D(ShadowMap, shadowVertex3);
	visibility *= 0.25;
#	else // USE_BLURRED_SHADOW
	lowp float visibility = _shadow2D(ShadowMap, shadowVertex);
#	endif // USE_BLURRED_SHADOW
	visibility *= vShadowOpacity;
#elif defined(USE_BAKED_SHADOW) // USE_SHADOW
	// Fetch baked shadow map
	lowp float visibility = texture2D(BakedShadowTex, vCoordLightmap).r;
#else // USE_BAKED_SHADOW
	const lowp float visibility = 1.0;
#endif // USE_SHADOW		
#if defined(USE_HIGHRES_LIGHTS)
	lowp vec2 vCoordLightmapSwitchId = vCoord0and1.pq;
#	if USE_LIGHT_DYNAMIC_COLORS
	lowp vec2 vCoordLightmapColor = vCoord2and3.st;
	lowp vec2 vCoordLightmapSwitchIntensity = vCoord2and3.pq;	
#	else // USE_LIGHT_DYNAMIC_COLORS
	lowp vec2 vCoordLightmapColor = vCoord0and1.pq;
#	endif // USE_LIGHT_DYNAMIC_COLORS
	// Fetch static direct color
	lowp vec4 staticColor = texture2D(LightmapColors, vCoordLightmapColor);
	// Initialize direct light color with scaled static color
	lowp vec3 precomputedDirectColor = staticColor.rgb;
#	if USE_LIGHT_DYNAMIC_COLORS && MAX_LIGHT_SWITCH_COUNT_PER_TEXEL > 0
	// Scale static color with switch #0
#		if defined(GI_EDITOR)
	lowp vec3 switchColorStatic = LightSwitchColors[0];
#		else // GI_EDITOR
	const lowp vec2 switchCoordStatic = vec2(0.0, 0.0);
	lowp vec3 switchColorStatic = texture2D(LightSwitchColors, switchCoordStatic).rgb;
#		endif // GI_EDITOR
	precomputedDirectColor *= switchColorStatic;
	// Fetch intensities
	lowp vec4 lightSwitchIntensities = texture2D(LightmapLightSwitchIntensities, vCoordLightmapSwitchIntensity);	
	// Fetch switch ids
	lowp vec4 lightSwitchIds = texture2D(LightmapLightSwitchIds, vCoordLightmapSwitchId);	
	// Cumulate scaled switch colors in direct light color
#		if MAX_LIGHT_SWITCH_COUNT_PER_TEXEL == 4
#			if defined(GI_EDITOR)
	for (int lightSwitchChannel = 0; lightSwitchChannel < MAX_LIGHT_SWITCH_COUNT_PER_TEXEL; ++lightSwitchChannel)
	{
		int switchId = int(lightSwitchIds[lightSwitchChannel] * 255.0);
		precomputedDirectColor += lightSwitchIntensities[lightSwitchChannel] * LightSwitchColors[switchId];
	}
#			else // GI_EDITOR
	lowp vec2 switchCoordChannel0 = vec2(lightSwitchIds[0], 0.0);
	lowp vec2 switchCoordChannel1 = vec2(lightSwitchIds[1], 0.0);
	lowp vec2 switchCoordChannel2 = vec2(lightSwitchIds[2], 0.0);
	lowp vec2 switchCoordChannel3 = vec2(lightSwitchIds[3], 0.0);
	lowp vec3 switchColorChannel0 = texture2D(LightSwitchColors, switchCoordChannel0).rgb;
	lowp vec3 switchColorChannel1 = texture2D(LightSwitchColors, switchCoordChannel1).rgb;
	lowp vec3 switchColorChannel2 = texture2D(LightSwitchColors, switchCoordChannel2).rgb;
	lowp vec3 switchColorChannel3 = texture2D(LightSwitchColors, switchCoordChannel3).rgb;
	precomputedDirectColor += lightSwitchIntensities[0] * switchColorChannel0;
	precomputedDirectColor += lightSwitchIntensities[1] * switchColorChannel1;
	precomputedDirectColor += lightSwitchIntensities[2] * switchColorChannel2;
	precomputedDirectColor += lightSwitchIntensities[3] * switchColorChannel3;
#			endif // GI_EDITOR
#		endif // MAX_LIGHT_SWITCH_COUNT_PER_TEXEL
#	endif // USE_LIGHT_DYNAMIC_COLORS
#	if defined(USE_AMBIENT_OCCLUSION)
	// Unpack ambient occlusion
#		if defined(USE_TWEAKERS)
	lowp float precomputedAO = mix(1.0, staticColor.a, AmbientOcclusionTweaker);
#		else // USE_TWEAKERS
	lowp float precomputedAO =  staticColor.a;
#		endif // USE_TWEAKERS
#	else // USE_AMBIENT_OCCLUSION
	const lowp float precomputedAO = 1.0;
#	endif // USE_AMBIENT_OCCLUSION
#else // USE_HIGHRES_LIGHTS
	lowp vec3 precomputedDirectColor = vec3(0.0);
#	if defined(USE_AMBIENT_OCCLUSION)
#		if defined(USE_TWEAKERS)
	lowp float precomputedAO = mix(1.0, texture2D(AmbientOcclusionTex, vCoordLightmap).r, AmbientOcclusionTweaker);
#		else // USE_TWEAKERS
	lowp float precomputedAO = texture2D(AmbientOcclusionTex, vCoordLightmap).r;
#		endif // USE_TWEAKERS
#	else // USE_AMBIENT_OCCLUSION
	const lowp float precomputedAO = 1.0;
#	endif // USE_AMBIENT_OCCLUSION
#endif // USE_HIGHRES_LIGHTS
#if defined(USE_NORMAL_MAP)
	// Compute bumped normal
	lowp vec3 worldNormal = optional_normalize(vWorldNormal.xyz);
	lowp vec3 worldTangent = optional_normalize(vWorldTangent);
	lowp vec3 worldBinormal = optional_normalize(vWorldBinormal);	
	lowp vec3 bumpedNormal = normalize(normalTS.x * worldTangent + normalTS.y * worldBinormal + normalTS.z * worldNormal);
#endif // USE_NORMAL_MAP
	// Fetch indirect lightmaps
	lowp vec4 lightmapGI0 = texture2D(LightmapGITex, vCoord4and5.st);
	lowp vec4 lightmapGI1 = texture2D(LightmapGITex, vCoord4and5.pq);
	lowp vec4 lightmapGI2 = texture2D(LightmapGITex, vCoord6and7.pq);
#	if defined(USE_HOTSPOT)
	// Fetch hot spot direction and color
	lowp vec3 hotSpotDir = texture2D(LightmapHotSpotDirectionTex, vCoord6and7.st).rgb * 2.0 - 1.0;
	lowp vec3 hotSpotColor = texture2D(LightmapHotSpotColorTex, vCoordLightmap).rgb;
#	endif // USE_HOTSPOT
	// Unpack basis colors
	lowp vec3 basisColor0 = lightmapGI0.rgb;
	lowp vec3 basisColor1 = lightmapGI1.rgb;
	lowp vec3 basisColor2 = lightmapGI2.rgb;
	lowp vec3 basisColor3 = vec3(lightmapGI0.a, lightmapGI1.a, lightmapGI2.a);
	// Compute pixel indirect color
#if defined(USE_NORMAL_MAP)	
	// Basis weights
#	if defined(USE_TEXTURE_NORMALIZATION)
	lowp vec4 basisWeights = textureCube(NormalizationCubeTex, bumpedNormal);
#	else // USE_TEXTURE_NORMALIZATION
	// Basis vectors
	const lowp vec3 basisUP1 = vec3(-1.0 / sqrt(6.0), -1.0 / sqrt(2.0),  1.0 / sqrt(3.0));
	const lowp vec3 basisUP2 = vec3(-1.0 / sqrt(6.0),  1.0 / sqrt(2.0),  1.0 / sqrt(3.0));
	const lowp vec3 basisUP3 = vec3( sqrt(2.0 / 3.0),              0.0,  1.0 / sqrt(3.0));	
	const lowp vec3 basisDW1 = vec3(             0.0,              0.0, 		    -1.0);
	const lowp vec4 basisScale = vec4(1.0, 1.0, 1.0, 0.5);
	const lowp vec4 basisBias = vec4(0.0, 0.0, 0.0, 0.5);	
	lowp vec4 basisWeights = vec4(
		dot(bumpedNormal, basisUP1),
		dot(bumpedNormal, basisUP2),
		dot(bumpedNormal, basisUP3),
		dot(bumpedNormal, basisDW1));
	basisWeights = clamp(basisWeights * basisScale + basisBias, 0.0, 1.0);
	basisWeights = optional_normalize(basisWeights);
#	endif // USE_TEXTURE_NORMALIZATION
	// Diffuse factor
	lowp float diffuseFactor = DiffuseScaleTweaker * max(0.0, dot(bumpedNormal, Light0Direction));
	// Specular factor
	lowp vec3 worldView = normalize(vWorldVertex.xyz - CameraPosition.xyz);
	lowp vec3 reflectedWorldView = reflect(worldView, bumpedNormal);
	lowp float specularFactor = SpecularScaleTweaker * pow(max(0.0, dot(reflectedWorldView, Light0Direction)), SpecularPowerTweaker);  
	// Direct color
	lowp vec3 directColor = (diffuseFactor + specularFactor) * SunColorTweaker * SunPowerTweaker;
#	if defined(USE_HOTSPOT)
	// Hot spot Specular factor	
	lowp float hotSpotSpecularFactor = HotSpotSpecularScaleTweaker * pow(max(0.0, dot(reflectedWorldView, hotSpotDir)), HotSpotSpecularPowerTweaker);  
	// Add hot spot specular color	
	precomputedDirectColor += hotSpotSpecularFactor * hotSpotColor;
#	endif // USE_HOTSPOT
#else // USE_NORMAL_MAP
	lowp vec4 basisWeights = vBasisWeights;
	lowp vec3 directColor = vDirectColor.xyz;
#	if defined(USE_HOTSPOT)
	// Hot spot Specular factor		
	lowp vec3 worldView = normalize(vWorldVertex.xyz - CameraPosition.xyz);
	lowp vec3 reflectedWorldView = reflect(worldView, vWorldNormal.xyz);
	lowp float hotSpotSpecularFactor = HotSpotSpecularScaleTweaker * pow(max(0.0, dot(reflectedWorldView, hotSpotDir)), HotSpotSpecularPowerTweaker);  
	// Add hot spot specular color	
	precomputedDirectColor += hotSpotSpecularFactor * hotSpotColor;
#	endif // USE_HOTSPOT
#endif // USE_NORMAL_MAP
#if defined(USE_ALBEDO)
	// Fade albedo
#	if defined(USE_TWEAKERS)
	albedo = mix(vec3(1.0), albedo, DiffusemapBlendTweaker);
#	endif // USE_TWEAKERS
#endif
	lowp vec3 precomputedIndirectColor =
		basisColor0 * basisWeights[0] +
		basisColor1 * basisWeights[1] +
		basisColor2 * basisWeights[2] +
		basisColor3 * basisWeights[3];
	// Compute final color
	lowp vec3 color = albedo * precomputedAO * (visibility * directColor + precomputedDirectColor + precomputedIndirectColor);
	gl_FragColor = vec4(color, 1.0);
#if 0
	gl_FragColor = mix(gl_FragColor, vec4(visibility, visibility, visibility, 1.0), 0.999);
#endif
}
