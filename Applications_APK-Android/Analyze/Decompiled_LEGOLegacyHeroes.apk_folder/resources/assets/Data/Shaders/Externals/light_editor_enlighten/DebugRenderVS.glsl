#version 400

in vec4 Vertex;
in vec2 TexCoord1;

out vec2 vTexCoord1;

uniform mat4 ViewProjectionMatrix;

void main()
{
	gl_Position = ViewProjectionMatrix * Vertex;
	vTexCoord1 = TexCoord1;
}
