//#define VERTEX_SHADER
//#define FRAGMENT_SHADER
//#define RENDER_SM
//#define LIGHT_AND_SHADOW
//#define LISPSHADOW
//#define SHADOW_HW
//#define WRITE_DEPTH_TO_COLOR_ATT

#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#ifdef VERTEX_SHADER
#define attribute in
#define varying out
#endif
#ifdef FRAGMENT_SHADER
#define varying in
out lowp vec4 glitch_FragColor;
#define gl_FragColor glitch_FragColor
#define texture2D texture
#endif
#endif

#ifdef DCC_MAX
#define DMAX(x) x
#else
#define DMAX(x)
#endif

#if defined(FRAGMENT_SHADER) && defined(GLSL2HLSL)
#	if GLITCH_PROFILE_HLSL < GLITCH_PROF_HLSL_PS_4_0_LEVEL_9_3
#		define PACK_DEPTH_ON_RG8
#	endif
#endif

// ------------------------------------------------------------
// ----                   RENDER_SM                        ----
// ----  Fill shadow map with no specific color            ----
// ------------------------------------------------------------
#ifdef RENDER_SM

#if defined(WRITE_DEPTH_TO_COLOR_ATT) && defined(GLSL2HLSL)
// Note: GLSL Converter doesn't allow undefined macros in preprocessor expressions
//  ( i.e. Macro <= PotentiallyUndefinedMacro in this case )
//  GLITCH_PROF_HLSL_VS_* macros are only defined when compiling a vertex shader
//  GLITCH_PROF_HLSL_PS_* macros are only defined when compiling a fragment shader
#	if defined(VERTEX_SHADER)
#		if GLITCH_PROFILE_HLSL <= GLITCH_PROF_HLSL_VS_4_0_LEVEL_9_3
#			define EMUL_FRAG_COORD
#		endif
#	elif GLITCH_PROFILE_HLSL <= GLITCH_PROF_HLSL_PS_4_0_LEVEL_9_3
#		define EMUL_FRAG_COORD
#	endif
#endif

#ifdef VERTEX_SHADER

attribute highp 	vec4 Vertex DMAX( : POSITION );

#ifdef EMUL_FRAG_COORD
varying highp vec2 fragCoordEmul DMAX( : TEXCOORD0 );
#endif

uniform highp mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	
#ifdef EMUL_FRAG_COORD
	fragCoordEmul = gl_Position.zw;
#endif
}
#endif 
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

#if defined(EMUL_FRAG_COORD)
varying highp vec2 fragCoordEmul DMAX( : TEXCOORD0 );
#	define FragCoordZ (fragCoordEmul.x / fragCoordEmul.y)
#else
#	define FragCoordZ gl_FragCoord.z
#endif

void main()
{
#ifdef WRITE_DEPTH_TO_COLOR_ATT

#ifdef PACK_DEPTH_ON_RG8
	// 2 component	
	highp int z = int(FragCoordZ * 65535.0);
	highp float zu = float(z / 256) / 255.0;
	highp float zd = float(mod(z, 256)) / 255.0;
	gl_FragColor = vec4(zu, zd, 0.0, 1.0);
#elif defined(PACK_DEPTH_ON_RGB8)	
	// 3 component	
	highp int z = int(FragCoordZ * 16777215.0);
	highp float z2 = float(z / 65536) / 255.0;	
	highp float z1 = float(int(mod(z, 65536)) / 256) / 255.0;
	highp float z0 = float(mod(z, 256)) / 255.0;
	gl_FragColor = vec4(z2, z1, z0, 1.0);
#else
	// 1 component
	gl_FragColor = vec4(FragCoordZ, 0.0, 0.0, 1.0);
#endif
#else
	gl_FragColor = vec4(1.0,0.0,0.0,1.0);
#endif
}
#endif
// FRAGMENT_SHADER

#endif 
// RENDER_SM

// ------------------------------------------------------------
// ----               LIGHT_AND_SHADOW                     ----
// ----  Render objects with basic diffuse lighting        ----
// ------------------------------------------------------------

#if defined(LIGHT_AND_SHADOW)

varying mediump		vec3	vNormalWS DMAX( : TEXCOORD0 );	// World Space
varying highp		vec4	vPosWS    DMAX( : TEXCOORD01 );	// World Space position

#ifdef VERTEX_SHADER

attribute highp 	vec4 Vertex DMAX( : POSITION );
attribute highp 	vec3 Normal DMAX( : NORMAL );

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 WorldIT;
uniform highp mat4 World;

void main()
{
	vPosWS = World * Vertex;
	vNormalWS = (WorldIT * vec4(Normal, 1.0)).xyz;
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
#endif 
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

uniform mediump vec3 sunLightDir; // WS normalized

#if !defined(LISPSHADOW)
uniform highp mat4 ViewToLightMatrix;
#endif
uniform highp mat4 LightProjMatrix;

#if defined(SHADOW_HW)
#if defined(GL_ES) && __VERSION__ < 300
#extension GL_EXT_shadow_samplers : require
#endif
#if defined(GLSL2METAL)
GLITCH_UNIFORM_PROPERTIES(ShadowMap, (samplerCompareFunc = lessEqual, samplerAddress = clampToEdge, samplerFilter = linear))
#endif
uniform highp sampler2DShadow ShadowMap;
#else
GLITCH_UNIFORM_PROPERTIES(ShadowMap, (depthtexture = 1))
uniform highp sampler2D ShadowMap;
#endif

mediump float computeLighting() 
{
	mediump vec3 normal = normalize(vNormalWS);
	return max(0.0, dot(normal,sunLightDir));
}

// Return 1.0 if visible, 0.0 if occluded
mediump float computeVisibility(mediump float NdotL)
{
	//highp float bias = 0.001;
	highp float bias = 0.005 * sqrt(1.0 - NdotL*NdotL) / NdotL;
	
	// Compute vertex position in LS
#if defined(LISPSHADOW)
	highp vec4 posLS = LightProjMatrix * vPosWS;
	posLS /= posLS.w;
#else
	highp vec4 posLS = LightProjMatrix * (ViewToLightMatrix * vPosWS);
#endif
	
	posLS.xyz = posLS.xyz * 0.5 + 0.5;
	posLS.y = 1.0 - posLS.y;
	posLS.z -= bias;
	
#if defined(SHADOW_HW)
#if defined(GL_ES) && __VERSION__ < 300
	return shadow2DEXT(ShadowMap, posLS.xyz);
#elif (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
	return texture(ShadowMap, posLS.xyz);
#else
	return shadow2D(ShadowMap, posLS.xyz).r;
#endif
#else
	posLS.z = min(posLS.z, 1.0);
#ifdef PACK_DEPTH_ON_RG8	
	// 2 components
	highp vec2 depthQuantized = texture2D(ShadowMap, posLS.xy).rg;
	highp float currentDepth = float((int(depthQuantized.r * 255.0) * 256) + int(depthQuantized.g * 255.0)) / 65535.0;
#elif defined(PACK_DEPTH_ON_RGB8)
	// 3 components
	highp vec3 depthQuantized = texture2D(ShadowMap, posLS.xy).rgb;
	highp float currentDepth = float((int(depthQuantized.r * 255.0) * 65536) + (int(depthQuantized.g * 255.0) * 256) + int(depthQuantized.b * 255.0)) / 16777215.0;
#else
	// 1 component
	highp float currentDepth = texture2D(ShadowMap, posLS.xy).r;
#endif
	return float(currentDepth >= posLS.z);
#endif
}

void main()
{
	mediump float lightIntensity = 0.0;
	
	lightIntensity = computeLighting();
	
	highp float visibility = 0.0;
	
	if(lightIntensity > 0.0)
	{
		visibility = computeVisibility(lightIntensity);
	}

#if defined(LISPSHADOW)
	const lowp vec3 diffuseColor = vec3(0.84,0.94,0.19);
	const lowp vec4 shadowColor = vec4(60.0 / 255.0,  7.0 / 255.0, 6.0 / 255.0, 0.8);
#else
	const lowp vec3 diffuseColor = vec3(0.8,0.8,0.8);
	const lowp vec4 shadowColor = vec4(0.0,0.0,0.0,0.8);
#endif
	mediump float shadowIntensity = min(1.0 - visibility, shadowColor.a);
	//lowp vec3 finalColor = mix(diffuseColor * lightIntensity, shadowColor.rgb, shadowIntensity);
	lowp vec3 finalColor = mix(mix(shadowColor.rgb,diffuseColor,lightIntensity), shadowColor.rgb, shadowIntensity);
	
	gl_FragColor = vec4(finalColor,1.0);
}
#endif
// FRAGMENT_SHADER

#endif 
// LIGHT_AND_SHADOW