#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

#define TEXTURE_2D 0
#define TEXTURE_2D_ARRAY 1
#define TEXTURE_3D 2
#define TEXTURE_CUBE_MAP 3
#define TEXTURE_CUBE_MAP_ARRAY 4

#ifndef TEXTURE_TYPE
#    define TEXTURE_TYPE TEXTURE_2D
#endif

#if (defined(GL_ES) && TEXTURE_TYPE == TEXTURE_2D_ARRAY && !defined(GL_NV_texture_array) && __VERSION__ < 300) || (!defined(GL_ES) && TEXTURE_TYPE == TEXTURE_2D_ARRAY && !defined(GL_EXT_texture_array) && __VERSION__ < 130)
#    undef TEXTURE_TYPE
#    define TEXTURE_TYPE TEXTURE_2D
#elif defined(GL_ES) && TEXTURE_TYPE == TEXTURE_2D_ARRAY && __VERSION__ < 300
#    extension GL_NV_texture_array : enable
#    define texture2DArray(s, t) texture2DArrayNV(s, t)
#    define sampler2DArray sampler2DArrayNV
#elif !defined(GL_ES) && TEXTURE_TYPE == TEXTURE_2D_ARRAY && __VERSION__ < 130
#    extension GL_EXT_texture_array : enable
#elif defined(GL_ES) && TEXTURE_TYPE == TEXTURE_3D && !defined(GL_OES_texture_3D) && __VERSION__ < 300
#    undef TEXTURE_TYPE
#    define TEXTURE_TYPE TEXTURE_2D
#elif defined(GL_ES) && TEXTURE_TYPE == TEXTURE_3D && __VERSION__ < 300
#    extension GL_OES_texture_3D : enable
#elif ((defined(GL_ES) && TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && ((!defined(GL_OES_texture_cube_map_array) && !defined(GL_EXT_texture_cube_map_array) && __VERSION__ < 320) || __VERSION__ < 310)) || (!defined(GL_ES) && TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && ((!defined(GL_ARB_texture_cube_map_array) && __VERSION__ < 400) || __VERSION__ < 130)))
#    undef TEXTURE_TYPE
#    define TEXTURE_TYPE TEXTURE_CUBE_MAP
#elif defined(GL_ES) && TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && __VERSION__ < 320
#    if defined(GL_OES_texture_cube_map_array)
#        extension GL_OES_texture_cube_map_array : enable
#    else
#        extension GL_EXT_texture_cube_map_array : enable
#    endif
#elif !defined(GL_ES) && TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && __VERSION__ < 400
#    extension GL_ARB_texture_cube_map_array : enable
#endif

#if (defined(GL_ES) && __VERSION__ < 300) || (!defined(GL_ES) && __VERSION__ < 130)
#    if TEXTURE_TYPE == TEXTURE_2D
#        define texture(s, t) texture2D(s, t)
#    elif TEXTURE_TYPE == TEXTURE_2D_ARRAY
#        define texture(s, t) texture2DArray(s, t)
#    elif TEXTURE_TYPE == TEXTURE_3D
#        define texture(s, t) texture3D(s, t)
#    elif TEXTURE_TYPE == TEXTURE_CUBE_MAP
#        define texture(s, t) textureCube(s, t)
#    endif
#endif

GLITCH_UNIFORM_PROPERTIES(sampler, (texcoord = TexCoord0, semantic = texture))

#if TEXTURE_TYPE == TEXTURE_2D_ARRAY
uniform lowp sampler2DArray Texture;
#elif TEXTURE_TYPE == TEXTURE_3D
uniform lowp sampler3D Texture;
#elif TEXTURE_TYPE == TEXTURE_CUBE_MAP
uniform lowp samplerCube Texture;
#elif TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY
uniform lowp samplerCubeArray Texture;
#else
uniform lowp sampler2D Texture;
#endif

#if TEXTURE_TYPE == TEXTURE_2D_ARRAY && defined(SPLIT_ALPHA)
uniform lowp sampler2DArray Texture_alpha;
#elif TEXTURE_TYPE == TEXTURE_3D && defined(SPLIT_ALPHA)
uniform lowp sampler3D Texture_alpha;
#elif TEXTURE_TYPE == TEXTURE_CUBE_MAP && defined(SPLIT_ALPHA)
uniform lowp samplerCube Texture_alpha;
#elif TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY && defined(SPLIT_ALPHA)
uniform lowp samplerCubeArray Texture_alpha;
#elif defined(SPLIT_ALPHA)
uniform lowp sampler2D Texture_alpha;
#endif

#if defined(SPLIT_ALPHA)
uniform lowp int Texture_alphachannel;
#endif

GLITCH_ALPHA_REF_UNIFORM

#if TEXTURE_TYPE == TEXTURE_CUBE_MAP_ARRAY
varying highp vec4 vCoord0;
#elif TEXTURE_TYPE == TEXTURE_3D || TEXTURE_TYPE == TEXTURE_CUBE_MAP || TEXTURE_TYPE == TEXTURE_2D_ARRAY
varying highp vec3 vCoord0;
#else
varying highp vec2 vCoord0;
#endif
varying	lowp vec4 vColor0;

void main()
{
	lowp vec4 color =
#if defined(SPLIT_ALPHA)
		vec4(texture(Texture, vCoord0).rgb * vColor0.rgb,
			 texture(Texture_alpha, vCoord0)[Texture_alphachannel] * vColor0.a);
#else
		texture(Texture, vCoord0) * vColor0;
#endif
	GLITCH_ALPHA_TEST(color.a);
	gl_FragColor = color;
}
