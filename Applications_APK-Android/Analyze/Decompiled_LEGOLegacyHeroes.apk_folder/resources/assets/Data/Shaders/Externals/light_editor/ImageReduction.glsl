#version 430

#define GROUP_SIZE 4
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE) in;

GLITCH_UNIFORM_PROPERTIES(colorTex, (access = rw))
GLITCH_UNIFORM_PROPERTIES(stencilTex, (access = rw))

layout(rgba8) uniform image2D colorTex;
layout(r8) uniform image2D stencilTex;

uniform int steps;
uniform int startScale;

void main()
{
    const ivec2 local_xy = ivec2(gl_LocalInvocationID);
    const ivec2 group_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE * 2;
    const ivec2 image_size = imageSize(colorTex);
    
    int scale = startScale;
    //Phase 1: in place reduction for the number of steps defined in uniform steps
    
    for(int i = 0; i < steps; ++i)
    {   
        const ivec2 origin = group_xy + local_xy * scale * 2;
    
        if(all(lessThan(local_xy * scale, ivec2(GROUP_SIZE))) && all(lessThan(origin, image_size)))
        {
            mat4 cAll;
            cAll[0] = imageLoad(colorTex, origin);
            
            vec4 sAll;
            
            sAll[0] = imageLoad(stencilTex, origin).r;
            
            ivec2 o1 = ivec2(0, scale);
            ivec2 o2 = ivec2(scale, 0);
            ivec2 o3 = ivec2(scale, scale);
            
            cAll[1] = imageLoad(colorTex, origin + o1);
            sAll[1] = imageLoad(stencilTex, origin + o1).r;
            
            cAll[2] = imageLoad(colorTex, origin + o2);
            sAll[2] = imageLoad(stencilTex, origin + o2).r;
            
            cAll[3] = imageLoad(colorTex, origin + o3);
            sAll[3] = imageLoad(stencilTex, origin + o3).r;
                        
            vec4 c = cAll * sAll;
            float factor = dot(sAll, sAll);
            imageStore(colorTex, origin, c / factor);
            
            imageStore(stencilTex, origin, vec4(sign(factor)));
        }
   
        memoryBarrierImage();
        barrier();
        scale = scale * 2;
    }
}
