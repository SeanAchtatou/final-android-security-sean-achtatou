attribute highp vec4 Vertex;
attribute highp vec3 Normal;

#ifdef VERTEXCOLOR
attribute lowp vec4 Color0;
varying lowp vec4 vColor0;
#endif

#ifdef TEXTURED
attribute mediump vec2 TexCoord0;
varying mediump vec2 vCoord0;
#endif

varying mediump vec3 vCoord1;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 WorldMatrix;
uniform highp vec3 EyePositionOS;

void main(void)
{
#ifdef VERTEXCOLOR
	vColor0 = Color0;
#endif
#ifdef TEXTURED
	vCoord0 = TexCoord0;
#endif

	// Note: intuitively, we want to reflect the vertex-to-eye vector and sample
	// with that, however, GLSL's reflect computes the reflection about the
	// plane perpendicular to the provided normal, so the reflection we want is
	// the negation of that, that is, we want:
	// WorldMatrix * -reflect(EyePositionOS - Vertex.xyz, Normal), which is equivalent to:
	vCoord1 = (WorldMatrix * vec4(reflect(Vertex.xyz - EyePositionOS, Normal), 0)).xyz;

	gl_Position = WorldViewProjectionMatrix * Vertex;
}
