// Uniforms
uniform sampler2D TextureA;
uniform sampler2D TextureB;
uniform lowp float InterpolationFactor;

// Varyings
varying lowp vec2 vCoord0;

// main
void main(void)
{  		
	lowp vec4 colorA = texture2D(TextureA, vCoord0);
	lowp vec4 colorB = texture2D(TextureB, vCoord0);
	gl_FragColor = mix(colorA, colorB, InterpolationFactor);
}