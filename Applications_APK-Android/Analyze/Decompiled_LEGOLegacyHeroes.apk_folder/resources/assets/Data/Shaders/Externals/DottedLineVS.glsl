#version 400

in highp vec4 Vertex;
in highp float Custom;

out highp float vScreenSpaceDistance;

uniform highp mat4 ViewProjectionMatrix;

void main()
{
	gl_Position = ViewProjectionMatrix * Vertex;
	vScreenSpaceDistance = Custom;
}
