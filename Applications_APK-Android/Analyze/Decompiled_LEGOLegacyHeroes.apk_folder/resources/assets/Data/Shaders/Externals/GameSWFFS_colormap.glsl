#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp sampler2D TextureSampler;
uniform lowp vec4 DiffuseColor;
uniform lowp sampler2D TextureSamplerMap;
uniform lowp ivec2 Component;

#ifdef SPLIT_ALPHA
uniform lowp sampler2D TextureSampler_alpha;
#endif

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

void main()
{

#ifdef SPLIT_ALPHA
	lowp vec4 color = vec4(texture2D(TextureSampler, vTexCoord0).rgb,
						   texture2D(TextureSampler_alpha, vTexCoord0).g);
#else
	lowp vec4 color = texture2D(TextureSampler, vTexCoord0);
#endif

	//lowp vec3 hsi = vec3(0.0, 0.0, 0.0);
	//hsi.r = (color.r + color.g + color.b) / 3.0; // i
	//hsi.g = (color.r + color.g + color.b) / 3.0; // h
	//hsi.b = (color.r + color.g + color.b) / 3.0; // s
  lowp float index = (color.r + color.g + color.b) / 3.0; 
	
	lowp vec4 mapped = texture2D(TextureSamplerMap, vec2(/*hsi[Component.x], hsi[Component.y]*/index, index));
	mapped = (mapped + DiffuseColor) * vColor0;
	mapped.a = color.a;
	gl_FragColor = mapped;
}
