#version 300 es

uniform highp mat4 WorldMatrix;
uniform highp mat4 WorldIT;

#ifdef GLITCH_POSITION
in highp vec4 Position;
out highp vec3 position;
#endif

#ifdef GLITCH_TEXCOORD0
uniform highp vec4 TexCoord0_scaleoffset;
in highp vec2 TexCoord0;
out highp vec2 texcoord0;
#endif

#ifdef GLITCH_TEXCOORD1
in highp vec2 TexCoord1;
out highp vec2 texcoord1;
#endif

#ifdef GLITCH_TEXCOORD2
in highp vec2 TexCoord2;
out highp vec2 texcoord2;
#endif

#ifdef GLITCH_TEXCOORD3
in highp vec2 TexCoord3;
out highp vec2 texcoord3;
#endif

#ifdef GLITCH_TEXCOORD4
in highp vec2 TexCoord4;
out highp vec2 texcoord4;
#endif

#ifdef GLITCH_TEXCOORD5
in highp vec2 TexCoord5;
out highp vec2 texcoord5;
#endif

#ifdef GLITCH_TEXCOORD6
in highp vec2 TexCoord6;
out highp vec2 texcoord6;
#endif

#ifdef GLITCH_TEXCOORD7
in highp vec2 TexCoord7;
out highp vec2 texcoord7;
#endif

#ifdef GLITCH_TEXCOORD8
in highp vec2 TexCoord8;
out highp vec2 texcoord8;
#endif

#ifdef GLITCH_TEXCOORD9
in highp vec2 TexCoord9;
out highp vec2 texcoord9;
#endif

#ifdef GLITCH_TEXCOORD10
in highp vec2 TexCoord10;
out highp vec2 texcoord10;
#endif

#ifdef GLITCH_TEXCOORD11
in highp vec2 TexCoord11;
out highp vec2 texcoord11;
#endif

#ifdef GLITCH_TEXCOORD12
in highp vec2 TexCoord12;
out highp vec2 texcoord12;
#endif

#ifdef GLITCH_TEXCOORD13
in highp vec2 TexCoord13;
out highp vec2 texcoord13;
#endif

#ifdef GLITCH_NORMAL
in highp vec3 Normal;
out highp vec3 normal;
#endif

#ifdef GLITCH_COLOR0
in highp vec4 Color0;
flat out uint color0;
#endif

#ifdef GLITCH_COLOR1
in highp vec4 Color1;
flat out uint color1;
#endif

#ifdef GLITCH_TANGENT0
in highp vec3 Tangent0;
out highp vec3 tangent0;
#endif

#ifdef GLITCH_TANGENT1
in highp vec3 Tangent1;
out highp vec3 tangent1;
#endif

#ifdef GLITCH_TANGENT2
in highp vec3 Tangent2;
out highp vec3 tangent2;
#endif

#ifdef GLITCH_TANGENT3
in highp vec3 Tangent3;
out highp vec3 tangent3;
#endif

#ifdef GLITCH_BINORMAL0
in highp vec3 Binormal0;
out highp vec3 binormal0;
#endif

#ifdef GLITCH_BINORMAL1
in highp vec3 Binormal1;
out highp vec3 binormal1;
#endif

#ifdef GLITCH_BINORMAL2
in highp vec3 Binormal2;
out highp vec3 binormal2;
#endif

#ifdef GLITCH_BINORMAL3
in highp vec3 Binormal3;
out highp vec3 binormal3;
#endif

#if defined(GLITCH_IS_SKINNED) || defined(GLITCH_SKIN_WEIGHTS)
in highp vec4 SkinWeights;
# ifdef GLITCH_SKIN_WEIGHTS
out highp vec4 skin_weights;
# endif
#endif

#if defined(GLITCH_IS_SKINNED) || defined(GLITCH_SKIN_INDICES)
in mediump vec4 SkinIndices;
# ifdef GLITCH_SKIN_INDICES
flat out mediump uint skin_indices;
# endif
#endif

#ifdef GLITCH_CUSTOM
in highp vec4 Custom;
out highp vec4 custom;
#endif

#ifdef GLITCH_IS_SKINNED
# if defined(GLSL2METAL)
uniform highp mat4 BoneMatrices[48];
uniform highp vec4 WeightMask;
# else
GLITCH_BEGIN_UNIFORM_BLOCK(Skeleton, skel)
    highp vec4 WeightMask;
    highp mat4 BoneMatrices[48];
GLITCH_END_UNIFORM_BLOCK
# endif
#endif

void main(void)
{
#ifdef GLITCH_IS_SKINNED

# ifdef GLITCH_POSITION
    vec3 netPosition = vec3(0.0, 0.0, 0.0);
#endif

# ifdef GLITCH_NORMAL
    vec3 netNormal = vec3(0.0, 0.0, 0.0);
# endif

# if defined(GLSL2METAL)
    vec4 weights = SkinWeights * WeightMask;
# else
    vec4 weights = SkinWeights * skel.WeightMask;
# endif
	vec4 indices = SkinIndices;
    for (int i = 0; i < 4; i++)
    {
        int index = int(indices[i]);
	
# ifdef GLITCH_POSITION
#  if defined(GLSL2METAL)
        netPosition += weights[i] * (BoneMatrices[index] * vec4(Position.xyz, 1)).xyz;
#  else
        netPosition += weights[i] * (skel.BoneMatrices[index] * vec4(Position.xyz, 1)).xyz;
#  endif
# endif

# ifdef GLITCH_NORMAL
#  if defined(GLSL2METAL)
        netNormal += weights[i] * (BoneMatrices[index] * vec4(Normal.xyz, 0)).xyz;
#  else
        netNormal += weights[i] * (skel.BoneMatrices[index] * vec4(Normal.xyz, 0)).xyz;
#  endif
# endif
	}

# ifdef GLITCH_POSITION
	position = (WorldMatrix * vec4(netPosition, 1.0)).xyz;
# endif	

# ifdef GLITCH_NORMAL
	normal = normalize((WorldIT * vec4(netNormal, 0.0)).xyz);
# endif

#else // GLITCH_IS_SKINNED

# ifdef GLITCH_POSITION
	position = (WorldMatrix * Position).xyz;
# endif

# ifdef GLITCH_NORMAL
	normal = normalize((WorldIT * vec4(Normal, 0.0)).xyz);
# endif

#endif // GLITCH_IS_SKINNED

#ifdef GLITCH_TEXCOORD0
	texcoord0 = TexCoord0 * TexCoord0_scaleoffset.xy + TexCoord0_scaleoffset.zw;
#endif

#ifdef GLITCH_TEXCOORD1
	texcoord1 = TexCoord1;
#endif

#ifdef GLITCH_TEXCOORD2
	texcoord2 = TexCoord2;
#endif

#ifdef GLITCH_TEXCOORD3
	texcoord3 = TexCoord3;
#endif

#ifdef GLITCH_TEXCOORD4
	texcoord4 = TexCoord4;
#endif

#ifdef GLITCH_TEXCOORD5
	texcoord5 = TexCoord5;
#endif

#ifdef GLITCH_TEXCOORD6
	texcoord6 = TexCoord6;
#endif

#ifdef GLITCH_TEXCOORD7
	texcoord7 = TexCoord7;
#endif

#ifdef GLITCH_TEXCOORD8
	texcoord8 = TexCoord8;
#endif

#ifdef GLITCH_TEXCOORD9
	texcoord9 = TexCoord9;
#endif

#ifdef GLITCH_TEXCOORD10
	texcoord10 = TexCoord10;
#endif

#ifdef GLITCH_TEXCOORD11
	texcoord11 = TexCoord11;
#endif

#ifdef GLITCH_TEXCOORD12
	texcoord12 = TexCoord12;
#endif

#ifdef GLITCH_TEXCOORD13
	texcoord13 = TexCoord13;
#endif

#ifdef GLITCH_COLOR0
	// Better, but only supported in es 3.1 :(
	// color0 = packUnorm4x8(Color0);

	// *** NOTE: casting from float to uint using a large float (> 255.0) doesn't work:
	//				+ uint(.5 * 255. * 256.)
	//			 cause the mesh to display yellowish instead of green. There seems to be some
	//			 case that works tho: value of 0. or 1. and some value of gray.
	color0 = uint(Color0.a * 255.) * 256u * 256u * 256u 
		   + uint(Color0.b * 255.) * 256u * 256u
		   + uint(Color0.g * 255.) * 256u
	       + uint(Color0.r * 255.);
#endif

#ifdef GLITCH_COLOR1
	color1 = uint(Color1.a * 255.) * 256u * 256u * 256u 
		   + uint(Color1.b * 255.) * 256u * 256u
		   + uint(Color1.g * 255.) * 256u
	       + uint(Color1.r * 255.);
#endif

#ifdef GLITCH_TANGENT0
	tangent0 = normalize((WorldIT * vec4(Tangent0, 0.0)).xyz);
#endif

#ifdef GLITCH_TANGENT1
	tangent1 = normalize((WorldIT * vec4(Tangent1, 0.0)).xyz);
#endif

#ifdef GLITCH_TANGENT2
	tangent2 = normalize((WorldIT * vec4(Tangent2, 0.0)).xyz);
#endif

#ifdef GLITCH_TANGENT3
	tangent3 = normalize((WorldIT * vec4(Tangent3, 0.0)).xyz);
#endif

#ifdef GLITCH_BINORMAL0
	binormal0 = normalize((WorldIT * vec4(Binormal0, 0.0)).xyz);
#endif

#ifdef GLITCH_BINORMAL1
	binormal1 = normalize((WorldIT * vec4(Binormal1, 0.0)).xyz);
#endif

#ifdef GLITCH_BINORMAL2
	binormal2 = normalize((WorldIT * vec4(Binormal2, 0.0)).xyz);
#endif

#ifdef GLITCH_BINORMAL3
	binormal3 = normalize((WorldIT * vec4(Binormal3, 0.0)).xyz);
#endif

#ifdef GLITCH_SKIN_WEIGHTS
	skin_weights = SkinWeights;
#endif

#ifdef GLITCH_SKIN_INDICES
	skin_indices = uint(SkinIndices.a * 255.0 * 256.0 * 256.0 * 256.0) 
				 + uint(SkinIndices.b * 255.0 * 256.0 * 256.0)
				 + uint(SkinIndices.g * 255.0 * 256.0)
				 + uint(SkinIndices.r * 255.0);
#endif

#ifdef GLITCH_CUSTOM
	custom = Custom;
#endif

	gl_Position = vec4(0.0); // quell errors/warnings on some driver versions/implementations
}

