#version 430

#define GROUP_SIZE 16
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE) in;

GLITCH_UNIFORM_PROPERTIES(Texture, (access = w))

layout(rgba8) uniform image2D Texture;
uniform vec4 Color;

void main()
{
	const ivec2 local_xy = ivec2(gl_LocalInvocationID);
    const ivec2 image_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE + local_xy; 
	
	imageStore(Texture, image_xy, Color);
}
