#if defined(GLSL2HLSL)
#	if GLITCH_PROFILE_HLSL < GLITCH_PROF_HLSL_PS_4_0_LEVEL_9_3
#		define PACK_DEPTH_ON_RG8
//#		define PACK_DEPTH_ON_RGB8
#	endif
#endif

#if defined(VERTEXCOLOR)
varying lowp    vec4        vColor0;
#endif

#if defined(TEXTURED)
varying mediump vec2        vCoord0;
uniform lowp    sampler2D   Texture;
#endif

#if defined(LIGHTING)
// MaterialColor
uniform lowp vec4 DiffuseColor;
uniform lowp vec4 emissioncolor;
uniform lowp vec4 ambientcolor;
uniform lowp vec4 SceneAmbientLight;

varying lowp vec4 vAmbient;
varying lowp vec4 vDiffuse;
#endif

varying mediump vec3        vVertexWorld;

GLITCH_UNIFORM_PROPERTIES(ShadowTexture0, (depthtexture = 1))

uniform mediump vec3        LightPosition0;
uniform mediump vec2        ShadowNearFar0;
uniform lowp    samplerCube ShadowTexture0;
uniform lowp    float       ShadowOpacity0; 

void main(void)
{
    // Do these calc in mediump as the values can be outside lowp's [-2,2]
    mediump vec3 vertexDistanceVec =  vVertexWorld - LightPosition0;
    mediump vec3  vCoord1 = normalize(vertexDistanceVec);
    
    // take only the longest component as the distance the the light's projection plan
    mediump float vertexDistance =  max(max(abs(vertexDistanceVec.x), abs(vertexDistanceVec.y)), abs(vertexDistanceVec.z)); 
	
#ifdef PACK_DEPTH_ON_RG8	
	// 2 components
	highp vec2 depthQuantized = textureCube(ShadowTexture0, vCoord1).rg;
	mediump float lightDepth = float((int(depthQuantized.r * 255.0) * 256) + int(depthQuantized.g * 255.0)) / 65535.0;
#elif defined(PACK_DEPTH_ON_RGB8)
	// 3 components
	highp vec3 depthQuantized = textureCube(ShadowTexture0, vCoord1).rgb;
	mediump float lightDepth = float((int(depthQuantized.r * 255.0) * 65536) + (int(depthQuantized.g * 255.0) * 256) + int(depthQuantized.b * 255.0)) / 16777215.0;
#else
	// 1 component
	mediump float lightDepth = textureCube(ShadowTexture0, vCoord1).r;
#endif	

	// There are mysterious differences in shadow map values when emulating gl_FragCoord.z
	// that cause shadow acne.
	// Adding a small bias to compensate
#if defined(GLSL2HLSL)
#	if GLITCH_PROFILE_HLSL <= GLITCH_PROF_HLSL_PS_4_0_LEVEL_9_3
	lightDepth += 0.001;
#	endif
#endif
    
    // Project the distance in the light's depth projection
    // new.z = old.z * far / ( far - near ) -  old.w * far * near / (far - near)
    // new.w = old.z
    // dist = new.z / new.w
    mediump float transformedVertexDistance = (vertexDistance * ShadowNearFar0.y / (ShadowNearFar0.y-ShadowNearFar0.x) - (ShadowNearFar0.x*ShadowNearFar0.y/(ShadowNearFar0.y-ShadowNearFar0.x))) / vertexDistance;
    
    // Final color calc will be in lowp
    lowp float percentShadowed = 0.0;
    // compare the distance to the light with the light's "distance map"
    if( vertexDistance < ShadowNearFar0.y && transformedVertexDistance > lightDepth )
    {
        percentShadowed = ShadowOpacity0;
    }
   
    lowp vec4 color = vec4(1.0);
#if defined(LIGHTING)
    lowp vec4 Diffuse = vec4(vDiffuse.rgb * (1.0 - percentShadowed), vDiffuse.a);
#if defined(TEXTURED)
    Diffuse *= texture2D(Texture, vCoord0);
#endif
    color = emissioncolor + (SceneAmbientLight + vAmbient) * ambientcolor + Diffuse * DiffuseColor;    
    gl_FragColor = clamp( color, 0.0, 1.0 );
#else
#if defined(VERTEXCOLOR) || defined(TEXTURED)
#    if defined(VERTEXCOLOR) && defined(TEXTURED)
    color *= texture2D(Texture, vCoord0) * vColor0;
#    elif defined(VERTEXCOLOR)
    color *= vColor0;
#    else //defined(TEXTURED)
    color *= texture2D(Texture, vCoord0);
#    endif
    gl_FragColor = vec4(color.rgb * (1.0 - percentShadowed), color.a);
#else
    gl_FragColor = vec4((1.0 - percentShadowed), 1.0);
#endif
#endif
}
