#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp sampler2D TextureSampler;

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

void main()
{
	mediump vec4 color = texture2D(TextureSampler, vTexCoord0);
	color *= vColor0;
	
	if (color.a > 0.0)
		color.rgb /= color.a;

	gl_FragColor = color;
}
