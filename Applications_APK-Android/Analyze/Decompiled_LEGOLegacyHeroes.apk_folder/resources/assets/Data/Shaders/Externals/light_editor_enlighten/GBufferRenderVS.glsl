#version 400

in vec4 Vertex;
in vec3 Normal;
in vec2 TexCoord0;
in vec2 TexCoord1;

out vec2 vTexCoord0;
out vec2 vTexCoord1;
out vec3 NormalizedNormal;
out float Depth;

uniform mat4 ViewProjectionMatrix;

void main()
{
	gl_Position = ViewProjectionMatrix * Vertex;
	vTexCoord0 = TexCoord0;
	vTexCoord1 = TexCoord1;
	NormalizedNormal = Normal;
}
