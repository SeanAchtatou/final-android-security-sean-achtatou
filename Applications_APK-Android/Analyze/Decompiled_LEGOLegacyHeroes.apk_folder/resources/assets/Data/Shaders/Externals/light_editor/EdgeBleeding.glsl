#version 430

#define GROUP_SIZE 16
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE) in;

GLITCH_UNIFORM_PROPERTIES(colorTex, (access = rw))

layout(rgba8) uniform image2D colorTex;
uniform usampler2D stencilTex;

void main()
{
    const ivec2 local_xy = ivec2(gl_LocalInvocationID);
    const ivec2 image_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE + local_xy;
    const ivec2 image_size = imageSize(colorTex);
    
    float s = float(texture(stencilTex, (vec2(image_xy) + 0.5) / image_size).r);
    
    const ivec2 direct_offsets[4] = {ivec2(1, 0), ivec2(-1, 0), ivec2(0, 1), ivec2(0, -1)};
    const ivec2 indirect_offsets[4] = {ivec2(1, 1), ivec2(-1, -1), ivec2(-1, 1), ivec2(1, -1)};
    
    const float direct_weight = 1;
    const float indirect_weight = 0.5;
    
    if(s == 0.0)
    {
    #if 1
        mat4 values;
        vec4 weightVector;
        
        for(int i = 0; i < 4; ++i)
        {
            values[i] = imageLoad(colorTex, image_xy + direct_offsets[i]);
            weightVector[i] = float(texture(stencilTex, (image_xy + direct_offsets[i] + 0.5) / image_size).r);
        }
        
        vec4 color = values * (weightVector * direct_weight);
        float totalWeight = dot(weightVector, vec4(direct_weight));
        
        for(int i = 0; i < 4; ++i)
        {
            values[i] = imageLoad(colorTex, image_xy + indirect_offsets[i]);
            weightVector[i] = float(texture(stencilTex, (image_xy + indirect_offsets[i] + 0.5) / image_size).r);
        }
        
        color += values * (weightVector * indirect_weight);
        totalWeight += dot(weightVector, vec4(indirect_weight));
        
        color /= totalWeight;
        
        imageStore(colorTex, image_xy, color);
    #else
        vec4 weightVector;
        
        for(int i = 0; i < 4; ++i)
        {
            weightVector[i] = float(texture(stencilTex, (image_xy + direct_offsets[i] + 0.5) / image_size).r);
        }
        
        float totalWeight = dot(weightVector, vec4(direct_weight));
        
        for(int i = 0; i < 4; ++i)
        {
            weightVector[i] = float(texture(stencilTex, (image_xy + indirect_offsets[i] + 0.5) / image_size).r);
        }
        
        totalWeight += dot(weightVector, vec4(indirect_weight));
        
        if(totalWeight > 0)
        {
            imageStore(colorTex, image_xy, vec4(0, 1, 0, 1));
        }
    #endif
        //imageStore(colorTex, image_xy, vec4(0, 1, 0, 1));
    }
}
