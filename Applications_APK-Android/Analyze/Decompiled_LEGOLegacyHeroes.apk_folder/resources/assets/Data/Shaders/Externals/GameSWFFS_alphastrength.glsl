#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp sampler2D TextureSampler;
uniform lowp vec4 DiffuseColor;
uniform mediump vec2 Scale;

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

void main()
{
	lowp vec4 color = texture2D(TextureSampler, vTexCoord0);
	color.a = clamp(color.a * Scale.x, 0.0, 1.0);
	gl_FragColor = color;
}
