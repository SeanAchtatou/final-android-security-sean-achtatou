#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define attribute in
#    define varying out
#endif

attribute highp vec4 Vertex;
uniform highp mat4 WorldViewProjectionMatrix;

#if defined(GL_ES) && defined(USE_POINTS)
uniform mediump float PointSize;
#endif

void main(void) 
{
#if defined(GL_ES) && defined(USE_POINTS)
	gl_PointSize = PointSize;
#endif
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
