
#ifdef FOUR
#define THREE
#endif
#ifdef FIVE
#define THREE
#define FOUR
#endif
#ifdef SIX
#define THREE
#define FOUR
#define FIVE
#endif
#ifdef SEVEN
#define THREE
#define FOUR
#define FIVE
#define SIX
#endif
#ifdef EIGHT
#define THREE
#define FOUR
#define FIVE
#define SIX
#define SEVEN
#endif

uniform lowp sampler2D texture0;
uniform lowp sampler2D texture1;
#if defined(THREE)
uniform lowp sampler2D texture2;
#endif
#if defined(FOUR)
uniform lowp sampler2D texture3;
#endif
#if defined(FIVE)
uniform lowp sampler2D texture4;
#endif
#if defined(SIX)
uniform lowp sampler2D texture5;
#endif
#if defined(SEVEN)
uniform lowp sampler2D texture6;
#endif
#if defined(EIGHT)
uniform lowp sampler2D texture7;
#endif

varying mediump vec2 vCoord0;
varying lowp vec2 vColor0;

void main()
{
    lowp vec4 tc0 = texture2D(texture0, vCoord0);
    lowp vec4 tc1 = texture2D(texture1, vCoord0);
#if defined(THREE)
    lowp vec4 tc2 = texture2D(texture2, vCoord0);
#endif
#if defined(FOUR)
    lowp vec4 tc3 = texture2D(texture3, vCoord0);
#endif
#if defined(FIVE)
    lowp vec4 tc4 = texture2D(texture4, vCoord0);
#endif
#if defined(SIX)
    lowp vec4 tc5 = texture2D(texture5, vCoord0);
#endif
#if defined(SEVEN)
    lowp vec4 tc6 = texture2D(texture6, vCoord0);
#endif
#if defined(EIGHT)
    lowp vec4 tc7 = texture2D(texture7, vCoord0);
#endif

    gl_FragColor = tc0 + tc1
#if defined(THREE)
					+ tc2
#endif
#if defined(FOUR)
					+ tc3
#endif
#if defined(FIVE)
					+ tc4
#endif
#if defined(SIX)
					+ tc5
#endif
#if defined(SEVEN)
					+ tc6
#endif
#if defined(EIGHT)
					+ tc7
#endif
						;
}