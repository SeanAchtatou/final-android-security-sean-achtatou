#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define attribute in
#    define varying out
#endif

#ifndef DCC_MAX
attribute highp vec4 Vertex;
#else
attribute highp vec4 Vertex : POSITION;
#endif

uniform   lowp  vec4 DiffuseColor;
uniform   highp mat4 WorldViewProjectionMatrix;

varying	  lowp  vec4 vColor0;

void main(void)
{
	vColor0 = DiffuseColor;
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
