// Compiler options
//#define GI_EDITOR
//#define USE_NORMAL_MAP
//#define USE_HOTSPOT

// Shader options
#define USE_SHADOW

// Shadow options
#if defined(USE_SHADOW)
#	define USE_LISP_SHADOW 1
#endif // USE_SHADOW

#if defined(GI_EDITOR)
#	define BASIS_COLORS			BasisColors_Editor
#	define HOTSPOT_POSITION		HotSpotPosition_Editor
#	define HOTSPOT_COLOR		HotSpotColor_Editor
#else // GI_EDITOR
#	define BASIS_COLORS			BasisColors
#	define HOTSPOT_POSITION		HotSpotPosition
#	define HOTSPOT_COLOR		HotSpotColor
#endif

// Basis colors
uniform lowp vec3 BASIS_COLORS[4];
#if defined(USE_HOTSPOT)
uniform highp vec3 HOTSPOT_POSITION;
uniform lowp vec3 HOTSPOT_COLOR;
#endif // USE_HOTSPOT

// Transformation parameters
uniform highp mat4 World;
uniform highp mat4 WorldIT;
uniform highp mat4 WorldViewProjection;

// Attributes
attribute highp vec4 Vertex;
attribute lowp vec3 Normal;
#if defined(USE_NORMAL_MAP)
attribute lowp vec3 Tangent;
attribute lowp vec3 Binormal;
#endif // USE_NORMAL_MAP
attribute lowp vec2 TexCoord0;
attribute lowp vec2 TexCoord1;

// Shading parameters
uniform highp vec4 CameraPosition;
uniform lowp vec3 Light0Direction;

#if defined(USE_SHADOW)
// Shadow parameters
uniform highp mat4 ShadowMatrix;
uniform lowp float ShadowOpacityPowerTweaker;
uniform lowp float ShadowOffsetFactorTweaker;
uniform lowp float ShadowOffsetMinTweaker;
uniform lowp float ShadowOffsetMaxTweaker;
#endif // USE_SHADOW

#if !defined(USE_NORMAL_MAP)
// Tweakers
uniform lowp vec3 SunColorTweaker;
uniform lowp float SunPowerTweaker;
uniform lowp float DiffuseScaleTweaker;
uniform lowp float SpecularScaleTweaker;
uniform lowp float SpecularPowerTweaker;
#endif // !USE_NORMAL_MAP
#if defined(USE_HOTSPOT)
uniform lowp float HotSpotSpecularScaleTweaker;
uniform lowp float HotSpotSpecularPowerTweaker;
#endif // USE_HOTSPOT

// Varyings
#if defined(USE_NORMAL_MAP)
varying highp vec4 vWorldVertex;
varying lowp vec4 vWorldNormal;
varying lowp vec3 vWorldTangent;
varying lowp vec3 vWorldBinormal;
#endif // USE_NORMAL_MAP
varying lowp vec4 vCoord0and1;
#if defined(USE_SHADOW)
varying highp vec4 vShadowVertex;
#	if defined(USE_NORMAL_MAP)
#		define vShadowOpacity vWorldNormal.w
#	else
#		define vShadowOpacity vDirectColor.w
#	endif
#endif // USE_SHADOW
#if defined(USE_NORMAL_MAP)
varying lowp vec4 vBasisGI0;
varying lowp vec4 vBasisGI1;
varying lowp vec4 vBasisGI2;
#else // USE_NORMAL_MAP
varying lowp vec4 vDirectColor;
#endif // USE_NORMAL_MAP
varying lowp vec3 vPrecomputedColor;

// main
void main(void)
{
	highp vec4 worldVertex = World * Vertex;
	lowp vec3 worldNormal = normalize((WorldIT * vec4(Normal, 0.0)).xyz); 	
#if defined(USE_NORMAL_MAP)
	vWorldVertex = worldVertex;
	vWorldNormal.xyz = worldNormal;
	vWorldTangent = (WorldIT * vec4(Tangent, 0.0)).xyz; 
	vWorldBinormal  = (WorldIT * vec4(Binormal, 0.0)).xyz; 
#endif // USE_NORMAL_MAP	
	vCoord0and1.st = TexCoord0;
	vCoord0and1.pq = TexCoord1;
	// Visibility factor
	lowp float NdotL = max(0.0, dot(worldNormal, Light0Direction));
#if defined(USE_SHADOW)
	// Compute shadow opacity
	vShadowOpacity = 1.0 - pow(1.0 - NdotL, ShadowOpacityPowerTweaker);	
	// Compute vertex position in LS
	vShadowVertex = ShadowMatrix * worldVertex;
#	if !defined(USE_LISP_SHADOW)
	// Compute depth bias
	highp float zBias = clamp(ShadowOffsetFactorTweaker * sqrt(1.0 - NdotL*NdotL) / NdotL, ShadowOffsetMinTweaker, ShadowOffsetMaxTweaker) * 0.0001;
	vShadowVertex.z -= zBias;
#	endif // USE_LISP_SHADOW
#endif // USE_SHADOW	
	lowp vec3 view = normalize(CameraPosition.xyz - worldVertex.xyz); 
#if defined(USE_HOTSPOT)
	// Hot spot Specular factor	
	lowp vec3 hotSpotDir = normalize(HOTSPOT_POSITION.xyz - worldVertex.xyz);
	lowp vec3 halfHotSpot = normalize(hotSpotDir + view);
	lowp float hotSpotSpecularFactor = HotSpotSpecularScaleTweaker * pow(max(0.0, dot(worldNormal, halfHotSpot)), HotSpotSpecularPowerTweaker);  
	// Add hot spot specular color	
	vPrecomputedColor = hotSpotSpecularFactor * HOTSPOT_COLOR;
#else
	vPrecomputedColor = vec3(0.0, 0.0, 0.0);
#endif // USE_HOTSPOT
#if defined(USE_NORMAL_MAP)
	vBasisGI0.rgb = BASIS_COLORS[0];	
	vBasisGI1.rgb = BASIS_COLORS[1];	
	vBasisGI2.rgb = BASIS_COLORS[2];
	vBasisGI0.a = BASIS_COLORS[3].r;
	vBasisGI1.a = BASIS_COLORS[3].g;
	vBasisGI2.a = BASIS_COLORS[3].b;	
#else  // USE_NORMAL_MAP
	// Diffuse factor
	lowp float diffuseFactor = DiffuseScaleTweaker * NdotL;
	// Specular factor	
	lowp vec3 h = normalize(Light0Direction + view);
	lowp float specularFactor = SpecularScaleTweaker * pow(max(0.0, dot(worldNormal, h)), SpecularPowerTweaker);  
	// Direct color
	vDirectColor.xyz = (diffuseFactor + specularFactor) * SunColorTweaker * SunPowerTweaker;
	// Basis vectors
	const lowp vec3 basisUP1 = vec3(-1.0 / sqrt(6.0), -1.0 / sqrt(2.0),  1.0 / sqrt(3.0)	);
	const lowp vec3 basisUP2 = vec3(-1.0 / sqrt(6.0),  1.0 / sqrt(2.0),  1.0 / sqrt(3.0)	);
	const lowp vec3 basisUP3 = vec3( sqrt(2.0 / 3.0),              0.0,  1.0 / sqrt(3.0)	);	
	const lowp vec3 basisDW1 = vec3(             0.0,              0.0, -1.0		    	);
	const lowp vec4 basisScale = vec4(1.0, 1.0, 1.0, 0.5);
	const lowp vec4 basisBias = vec4(0.0, 0.0, 0.0, 0.5); 
	// Basis weights	
	lowp vec4 basisWeights = vec4(
		dot(worldNormal, basisUP1),
		dot(worldNormal, basisUP2),
		dot(worldNormal, basisUP3),
		dot(worldNormal, basisDW1));		
	basisWeights = clamp(basisWeights * basisScale + basisBias, 0.0, 1.0);
	basisWeights = normalize(basisWeights);
	// Indirect color
	vPrecomputedColor += 
		BASIS_COLORS[0] * basisWeights[0] +
		BASIS_COLORS[1] * basisWeights[1] +
		BASIS_COLORS[2] * basisWeights[2] +
		BASIS_COLORS[3] * basisWeights[3];	
#endif // USE_NORMAL_MAP
	gl_Position = WorldViewProjection * Vertex;
}
