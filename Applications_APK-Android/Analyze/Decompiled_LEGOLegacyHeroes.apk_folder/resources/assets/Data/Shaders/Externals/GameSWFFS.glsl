#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

uniform lowp sampler2D TextureSampler;
uniform lowp vec4 DiffuseColor;
uniform lowp vec4 AdditiveColor;

#ifdef SPLIT_ALPHA
uniform lowp sampler2D TextureSampler_alpha;
#endif

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

void main()
{
#ifdef SPLIT_ALPHA
	lowp vec4 color = vec4(texture2D(TextureSampler, vTexCoord0).rgb,
						   texture2D(TextureSampler_alpha, vTexCoord0).g);
#else
	lowp vec4 color = texture2D(TextureSampler, vTexCoord0);
#endif
	color = (color + DiffuseColor) * vColor0;

	gl_FragColor = color + AdditiveColor;
}
