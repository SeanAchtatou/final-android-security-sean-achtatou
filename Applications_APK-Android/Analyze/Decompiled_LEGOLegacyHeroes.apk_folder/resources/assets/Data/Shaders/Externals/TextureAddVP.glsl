attribute mediump vec2 Vertex;

varying mediump vec2 vCoord0;

void main()
{
        vCoord0 = (Vertex + 1.) * 0.5; //Map to [0, 1]
        gl_Position = vec4(Vertex, 0., 1.);
}