//#define VERTEX_SHADER
//#define FRAGMENT_SHADER

//#define ALBEDO
//#define NORMAL_ENCODE
//#define LIGHT_PROJECTION
//#define LIGHT_ACCUMULATION
//#define SPECULAR_ACCUMULATION
//#define CUMULATIVE
//#define GEOMETRY_PASS
//#define VISUAL_DEPTH

GLITCH_UNIFORM_PROPERTIES(DepthBuffer, (depthtexture = 1))

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef ALBEDO

varying mediump	vec2 vCoord0;

#ifdef VERTEX_SHADER

attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute mediump vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

uniform highp mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

uniform lowp sampler2D DiffuseTex;

void main()
{
	gl_FragColor = texture2D(DiffuseTex,vCoord0);
}
#endif
// FRAGMENT_SHADER

#endif 
// ALBEDO

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef NORMAL_ENCODE

varying mediump	vec3 vNormal;

#ifdef VERTEX_SHADER

attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;
attribute mediump vec4 Normal
#ifdef DCC_MAX
: NORMAL
#endif
;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 WorldIT;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vNormal = (WorldIT * Normal).xyz;
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER
void main()
{
	gl_FragColor = vec4(normalize(vNormal.rgb) * 0.5 + 0.5,1.0);
}
#endif
// FRAGMENT_SHADER

#endif 
// NORMAL_ENCODE

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef LIGHT_PROJECTION

#ifdef VERTEX_SHADER
attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

// Vertex specific
uniform highp  mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
}
#endif
// VERTEX_SHADER


#ifdef FRAGMENT_SHADER

void main()
{
	gl_FragColor  = vec4(0.0,0.0,0.0,0.0);
}
#endif
// FRAGMENT_SHADER
#endif 
// LIGHT_PROJECTION


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef LIGHT_ACCUMULATION

// Common
varying	mediump vec2 vCoord0;

#ifdef VERTEX_SHADER
attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute highp vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

// Vertex specific
uniform highp  mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
}
#endif
// VERTEX_SHADER


#ifdef FRAGMENT_SHADER

// Fragment specific
uniform lowp 		sampler2D  		NormalBuffer;
uniform highp		sampler2D 		DepthBuffer;

uniform highp		vec3			MyLightPosition;
uniform lowp		vec3			MyLightDiffuseColor;
uniform lowp		vec3			MyLightAttenuation;

uniform highp		mat4 			CameraViewProjectionI;

// Function for converting depth to world-space position
// in deferred pixel shader pass.  coord is a texture
// coordinate for a full-screen quad, such that x=0 is the
// left of the screen, and y=0 is the top of the screen.
highp vec3 getPositionFromDepth(mediump vec2 coord,highp float depth)
{
    highp vec3 pos = vec3(coord, depth) * 2.0 - 1.0;
	pos.y *= -1.0;
    
    // Transform by the inverse view projection matrix
    highp vec4 pos4 =  CameraViewProjectionI * vec4(pos,1.0);
  
    // Divide by w to get the world-space position
    return  pos4.xyz / pos4.w;
}

highp float getDepth(mediump vec2 coord)
{
	return texture2D(DepthBuffer, coord).r;
}


lowp vec3 getNormal(mediump vec2 coord)
{
	return texture2D(NormalBuffer, coord).rgb;
}

void main()
{
	highp float depth = getDepth(vCoord0);
	mediump vec3 normal = getNormal(vCoord0);
	
	highp vec3 position = MyLightPosition - getPositionFromDepth(vCoord0,depth);
	normal = normalize(normal * 2.0 -1.0);
	
	
	mediump float ndotl = max(0.0, dot(normal, normalize(position)));
	highp float dist = length(position);
	
	// Warning: Light0Attenuation is set to (1,0,0) by maxGlitch
	mediump float attn = 1.0 / (MyLightAttenuation.x + dist * (MyLightAttenuation.y + MyLightAttenuation.z * dist));
	
	gl_FragColor  = vec4(ndotl * attn * MyLightDiffuseColor,1.0);
}
#endif
// FRAGMENT_SHADER
#endif 
// LIGHT_ACCUMULATION


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef SPECULAR_ACCUMULATION
// Common
varying	mediump vec2 vCoord0;

#ifdef VERTEX_SHADER
attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute highp vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

// Vertex specific
uniform highp  mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

// Fragment specific
uniform lowp 		sampler2D 		NormalBuffer;
uniform highp		sampler2D		DepthBuffer;

uniform highp		vec3			MyLightPosition;
uniform lowp		vec3			MyLightSpecularColor;
uniform lowp		vec3			MyLightAttenuation;

uniform highp		vec3			MyCameraPosition;
uniform highp		mat4 			CameraViewProjectionI;

// Function for converting depth to world-space position
// in deferred pixel shader pass.  coord is a texture
// coordinate for a full-screen quad, such that x=0 is the
// left of the screen, and y=0 is the top of the screen.

highp vec3 getPositionFromDepth(mediump vec2 coord,highp float depth)
{
    highp vec3 pos = vec3(coord, depth) * 2.0 - 1.0;
	pos.y *= -1.0;
    
    // Transform by the inverse view projection matrix
    highp vec4 pos4 =  CameraViewProjectionI * vec4(pos,1.0);
  
    // Divide by w to get the world-space position
    return  pos4.xyz / pos4.w;
}

highp float getDepth(mediump vec2 coord)
{
	return texture2D(DepthBuffer, coord).r;
}


lowp vec3 getNormal(mediump vec2 coord)
{
	return texture2D(NormalBuffer, coord).rgb;
}

void main()
{

	highp float depth = getDepth(vCoord0);
	mediump vec3 normal = getNormal(vCoord0);
	
	highp vec3 position = getPositionFromDepth(vCoord0,depth);
	normal = normalize(normal * 2.0 -1.0);
	
	highp vec3 MyLightDirection = MyLightPosition - position;
		
	mediump vec3 halfVector = normalize(normalize(MyLightDirection) + normalize(MyCameraPosition - position));
	
	// No need to test ndotl value ??
	mediump float intensity = pow(max(0.0, dot(normal, halfVector)), 16.0);
	
	highp float dist = length(MyLightDirection);
	mediump float attn = 1.0 / (MyLightAttenuation.x + dist * (MyLightAttenuation.y + MyLightAttenuation.z * dist));
		
	gl_FragColor = vec4(intensity * attn * MyLightSpecularColor, 1);
}
#endif
// FRAGMENT_SHADER
#endif
// SPECULAR_ACCUMULATION

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#ifdef CUMULATIVE

// Common
varying	mediump vec2 vCoord0;

#ifdef VERTEX_SHADER
attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute highp vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

// Vertex specific
uniform highp  mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
}
#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

uniform lowp sampler2D 		AlbedoBuffer;
uniform lowp sampler2D 		LightIntensityBuffer;
uniform lowp sampler2D 		SpecularIntensityBuffer;

void main()
{
	gl_FragColor = vec4( texture2D(AlbedoBuffer, vCoord0).rgb
					   * texture2D(LightIntensityBuffer, vCoord0).rgb
					   + texture2D(SpecularIntensityBuffer, vCoord0).rgb
					   ,1.0);
}
#endif
// FRAGMENT_SHADER

#endif
// CUMULATIVE

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#ifdef GEOMETRY_PASS

#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) &&  __VERSION__ >= 130)
#    ifdef VERTEX_SHADER
#        define attribute in
#        define varying out
#    elif defined(FRAGMENT_SHADER)
#        define varying in
#    endif
#endif

varying mediump	vec2 vCoord0;
varying mediump	vec3 vNormal;

#ifdef VERTEX_SHADER

attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute mediump vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

attribute mediump vec4 Normal
#ifdef DCC_MAX
: NORMAL
#endif
;

uniform highp mat4 WorldViewProjectionMatrix;
uniform highp mat4 WorldIT;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
	vNormal = (WorldIT * Normal).xyz;	
}

#endif
// VERTEX_SHADER

#ifdef FRAGMENT_SHADER

#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) &&  __VERSION__ >= 130)
layout(location = 0) out lowp vec4 glitch_FragData[2];
#    define gl_FragData glitch_FragData
#    define texture2D texture
#endif

uniform lowp sampler2D DiffuseTex;

void main()
{
	gl_FragData[0] = texture2D(DiffuseTex,vCoord0);					// albedo
	gl_FragData[1] = vec4(normalize(vNormal.rgb) * 0.5 + 0.5,1.0);  // normal		
}
#endif
// FRAGMENT_SHADER

#endif
// GEOMETRY_PASS

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef VISUAL_DEPTH

// Common
varying	mediump vec2 vCoord0;

#ifdef VERTEX_SHADER
attribute highp vec4 Vertex
#ifdef DCC_MAX
: POSITION
#endif
;

attribute highp vec4 TexCoord0
#ifdef DCC_MAX
: TEXCOORD0
#endif
;

// Vertex specific
uniform highp  mat4 WorldViewProjectionMatrix;

void main()
{
	gl_Position = WorldViewProjectionMatrix * Vertex;
	vCoord0 = TexCoord0.xy;
}
#endif

// VERTEX_SHADER


#ifdef FRAGMENT_SHADER

uniform highp		sampler2D 		DepthBuffer;

highp float getDepth(mediump vec2 coord)
{
	return texture2D(DepthBuffer, coord).r;
}

void main()
{
	highp float depth = getDepth(vCoord0);		
	depth = pow(depth,200.0);	
	gl_FragColor = vec4(depth,depth,depth,1.0);	
}

#endif
// FRAGMENT_SHADER
#endif 
// VISUAL_DEPTH
