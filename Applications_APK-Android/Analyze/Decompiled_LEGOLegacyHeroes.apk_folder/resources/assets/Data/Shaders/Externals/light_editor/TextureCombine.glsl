#version 430

#define GROUP_SIZE 16
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE) in;

GLITCH_UNIFORM_PROPERTIES(texOut, (access = w))
//GLITCH_UNIFORM_PROPERTIES(texIn, (access = r))

uniform sampler2D texIn[COMBINE_ARY];
layout(rgba8) uniform image2D texOut;
//layout(rgba8) uniform image2D texIn;
uniform int combineCount;
//uniform vec3 colorDebug;

void main()
{
    const ivec2 local_xy = ivec2(gl_LocalInvocationID);
    const ivec2 image_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE + local_xy;
    const ivec2 image_size = imageSize(texOut);
    const vec2 texCoord = (vec2(image_xy) + 0.5) / image_size;
    
    vec3 color = vec3(0);
    
    const int loop = min(combineCount, COMBINE_ARY);
    
    for(int i = 0; i < loop; ++i)
    {
        color += texture(texIn[i], texCoord).rgb;
    }
    
    //color += imageLoad(texIn, image_xy).rgb;
    
    imageStore(texOut, image_xy, vec4(color, 1));
}
