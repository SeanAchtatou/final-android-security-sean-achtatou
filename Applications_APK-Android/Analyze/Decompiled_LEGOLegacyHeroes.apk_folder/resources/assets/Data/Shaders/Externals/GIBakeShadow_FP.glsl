// Compiler options	
//#define USE_BLURRED_SHADOW

// Shadow options
#if !defined(USE_HW_SHADOW)
#	if defined(HIDE_GLSL_DEFINES)
#		define USE_HW_SHADOW 0 // Workaround to allow this shader to be compiled from the debugger
#	else
#		define USE_HW_SHADOW 1
#	endif
#endif

// Shadow sampler
#if USE_HW_SHADOW
#	if defined(GL_ES)
#		extension GL_EXT_shadow_samplers : require
#		define _shadow2D(sm, sv) shadow2DEXT(sm, sv)
#	else // defined(GL_ES)
#		define _shadow2D(sm, sv) shadow2D(sm, sv).r
#	endif // defined(GL_ES)
#if defined(GLSL2METAL)
GLITCH_UNIFORM_PROPERTIES(ShadowMap, (samplerCompareFunc = lessEqual, samplerAddress = clampToEdge, samplerFilter = linear))
#endif
uniform highp sampler2DShadow ShadowMap;
#else // USE_HW_SHADOW
#	define _shadow2D(sm, sv) (texture2D(sm, sv.xy).r > sv.z ? 1.0 : 0.0)
uniform sampler2D ShadowMap;
#endif // USE_HW_SHADOW

// Tweakers

// Varyings
#if defined(USE_BLURRED_SHADOW)
varying highp vec3 vShadowVertex0;
varying highp vec3 vShadowVertex1;
varying highp vec3 vShadowVertex2;
varying highp vec3 vShadowVertex3;
#else
varying highp vec3 vShadowVertex;
#endif // USE_BLURRED_SHADOW
varying lowp float vShadowOpacity;

// main
void main(void)
{  	
	lowp float visibility = 0.0;
#	if defined(USE_BLURRED_SHADOW)	
	visibility += _shadow2D(ShadowMap, vShadowVertex0);
	visibility += _shadow2D(ShadowMap, vShadowVertex1);
	visibility += _shadow2D(ShadowMap, vShadowVertex2);
	visibility += _shadow2D(ShadowMap, vShadowVertex3);
	visibility *= 0.25;
#	else // USE_BLURRED_SHADOW
	visibility += _shadow2D(ShadowMap, vShadowVertex);
#	endif // USE_BLURRED_SHADOW
	visibility *= vShadowOpacity;
	gl_FragColor = vec4(visibility, 0.0, 0.0, 1.0);
}
