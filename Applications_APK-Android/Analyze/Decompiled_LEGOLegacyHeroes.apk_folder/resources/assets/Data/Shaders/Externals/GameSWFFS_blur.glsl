#if (defined(GL_ES) && __VERSION__ >= 300) || (!defined(GL_ES) && __VERSION__ > 120)
#    define varying in
#    define texture2D texture
out lowp vec4 glitch_FragColor;
#    define gl_FragColor glitch_FragColor
#endif

// http://callumhay.blogspot.com/2010/09/gaussian-blur-shader-glsl.html

uniform lowp sampler2D TextureSampler;
uniform lowp float BlurSize;

varying highp vec2 vTexCoord0;
varying lowp vec4 vColor0;

const mediump float pi = 3.14159265;
const mediump float sigma = 3.5;
//const mediump float BlurSize = 1.0 / 512.0;

#if defined(HORIZONTAL_BLUR_5)
const mediump float numBlurPixelsPerSide = 3.0;
const mediump vec2  blurMultiplyVec      = vec2(1.0, 0.0);
#else
const mediump float numBlurPixelsPerSide = 3.0;
const mediump vec2  blurMultiplyVec      = vec2(0.0, 1.0);
#endif

void main()
{
	// Incremental Gaussian Coefficent Calculation (See GPU Gems 3 pp. 877 - 889)
	mediump vec3 incrementalGaussian;
	incrementalGaussian.x = 1.0 / (sqrt(2.0 * pi) * sigma);
	incrementalGaussian.y = exp(-0.5 / (sigma * sigma));
	incrementalGaussian.z = incrementalGaussian.y * incrementalGaussian.y;
	
	mediump vec4 avgValue = vec4(0.0, 0.0, 0.0, 0.0);
	mediump float coefficientSum = 0.0;
	
	// Take the central sample first...
	avgValue += texture2D(TextureSampler, vTexCoord0.xy) * incrementalGaussian.x;
	coefficientSum += incrementalGaussian.x;
	incrementalGaussian.xy *= incrementalGaussian.yz;
	
	mediump vec4 texel = vec4(0.0, 0.0, 0.0, 0.0);
	
	// Go through the remaining 8 vertical samples (3 on each side of the center)
	for (mediump float i = 1.0; i <= numBlurPixelsPerSide; i++)
	{
		avgValue += texture2D(TextureSampler, vTexCoord0.xy - i * BlurSize * blurMultiplyVec) * incrementalGaussian.x;
		coefficientSum += incrementalGaussian.x;
		
		avgValue += texture2D(TextureSampler, vTexCoord0.xy + i * BlurSize * blurMultiplyVec) * incrementalGaussian.x;
		coefficientSum += incrementalGaussian.x;
		
		incrementalGaussian.xy *= incrementalGaussian.yz;
	}
	
	gl_FragColor = avgValue * vColor0 / coefficientSum;
}
