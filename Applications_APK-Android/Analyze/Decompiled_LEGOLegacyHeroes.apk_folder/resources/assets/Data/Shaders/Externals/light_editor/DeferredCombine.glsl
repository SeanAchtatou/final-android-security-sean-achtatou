#version 430

#define GROUP_SIZE 16
layout(local_size_x = GROUP_SIZE, local_size_y = GROUP_SIZE) in;

GLITCH_UNIFORM_PROPERTIES(DiffuseTex, (access = r))
GLITCH_UNIFORM_PROPERTIES(IndirectTex, (access = r))
GLITCH_UNIFORM_PROPERTIES(TargetTex, (access = rw))

layout(rgba16f) uniform image2D DiffuseTex;
layout(rgba16f) uniform image2D IndirectTex;
layout(rgba8) uniform image2D TargetTex;

void main()
{
	const ivec2 local_xy = ivec2(gl_LocalInvocationID);
    const ivec2 image_xy = ivec2(gl_WorkGroupID) * GROUP_SIZE + local_xy; 
	
	const vec4 diffuse = imageLoad(DiffuseTex, image_xy);
	const vec4 indirect = imageLoad(IndirectTex, image_xy);
	const vec4 direct = imageLoad(TargetTex, image_xy);
	
	vec3 bidon = vec3(3729.37);
	vec3 test = bidon;
	
	vec4 fullLight = diffuse * (direct + indirect);
	//vec4 fullLight = diffuse;
	
	//test = diffuse.xyz * (direct.xyz + indirect.xyz);
	//test = vec4(1.0);
	
	imageStore(TargetTex, image_xy, vec4(fullLight.rgb, 1.0));
	
	if(test != bidon)
	{
		imageStore(TargetTex, image_xy, vec4(test, 1.0));
	}
}
