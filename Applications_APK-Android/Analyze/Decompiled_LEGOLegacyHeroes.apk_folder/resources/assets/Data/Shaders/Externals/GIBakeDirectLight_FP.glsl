uniform sampler2D LightSwitchColors;
uniform sampler2D LightmapDynamicColors;
#define LightmapColors 					LightmapDynamicColors
#define LightmapLightSwitchIntensities 	LightmapDynamicColors
uniform sampler2D LightmapLightSwitchIds;

// Varyings
varying lowp vec2 vCoord1;
varying lowp vec2 vCoord2;
varying lowp vec2 vCoord3;

// main
void main(void)
{  		
	lowp vec2 vCoordLightmapSwitchId = vCoord1;
	lowp vec2 vCoordLightmapColor = vCoord2;
	lowp vec2 vCoordLightmapSwitchIntensity = vCoord3;	
	// Fetch static direct color
	lowp vec4 staticColor = texture2D(LightmapColors, vCoordLightmapColor);
	// Initialize direct light color with static color
	lowp vec3 precomputedDirectColor = staticColor.rgb;
	// Scale static color with switch #0
	const lowp vec2 switchCoordStatic = vec2(0.0, 0.0);
	lowp vec3 switchColorStatic = texture2D(LightSwitchColors, switchCoordStatic).rgb;
	precomputedDirectColor *= switchColorStatic;
	// Fetch intensities
	lowp vec4 lightSwitchIntensities = texture2D(LightmapLightSwitchIntensities, vCoordLightmapSwitchIntensity);	
	// Fetch switch ids
	lowp vec4 lightSwitchIds = texture2D(LightmapLightSwitchIds, vCoordLightmapSwitchId);	
	// Cumulate scaled switch colors in direct light color
	lowp vec2 switchCoordChannel0 = vec2(lightSwitchIds[0], 0.0);
	lowp vec2 switchCoordChannel1 = vec2(lightSwitchIds[1], 0.0);
	lowp vec2 switchCoordChannel2 = vec2(lightSwitchIds[2], 0.0);
	lowp vec2 switchCoordChannel3 = vec2(lightSwitchIds[3], 0.0);
	lowp vec3 switchColorChannel0 = texture2D(LightSwitchColors, switchCoordChannel0).rgb;
	lowp vec3 switchColorChannel1 = texture2D(LightSwitchColors, switchCoordChannel1).rgb;
	lowp vec3 switchColorChannel2 = texture2D(LightSwitchColors, switchCoordChannel2).rgb;
	lowp vec3 switchColorChannel3 = texture2D(LightSwitchColors, switchCoordChannel3).rgb;
	precomputedDirectColor += lightSwitchIntensities[0] * switchColorChannel0;
	precomputedDirectColor += lightSwitchIntensities[1] * switchColorChannel1;
	precomputedDirectColor += lightSwitchIntensities[2] * switchColorChannel2;
	precomputedDirectColor += lightSwitchIntensities[3] * switchColorChannel3;
	gl_FragColor = vec4(precomputedDirectColor, staticColor.a);
#if 0
	gl_FragColor = mix(gl_FragColor, vec4(staticColor.rgb, 1.0), 0.999);
#endif
}