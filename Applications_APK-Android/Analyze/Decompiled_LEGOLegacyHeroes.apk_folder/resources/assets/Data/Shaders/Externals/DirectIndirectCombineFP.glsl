uniform lowp sampler2D Direct;
uniform lowp sampler2D Indirect;

varying lowp vec2 vTexCoord0;

void main()
{
        lowp vec3 direct = texture2D(Direct, vTexCoord0).rgb;
        lowp vec3 indirect = texture2D(Indirect, vTexCoord0).rgb;
        gl_FragColor = vec4(direct + indirect, 1.);
}