#ifdef DCC_MAX
#define DMAX(x) x
#else
#define DMAX(x)
#endif

#if defined(GLSL2HLSL)
#if GLITCH_PROFILE_HLSL <= GLITCH_PROF_HLSL_VS_4_0_LEVEL_9_3
#define EMUL_FRAG_COORD
#endif
#endif

attribute highp vec4 Vertex DMAX( : POSITION );

#ifdef EMUL_FRAG_COORD
varying highp vec2 fragCoordEmul DMAX( : TEXCOORD0 );
#endif

uniform highp mat4 WorldViewProjectionMatrix;

void main()
{
    gl_Position = WorldViewProjectionMatrix * Vertex;
	
#ifdef EMUL_FRAG_COORD
	fragCoordEmul = gl_Position.zw;
#endif
}
