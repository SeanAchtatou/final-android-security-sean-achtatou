#version 300 es
#if defined(GL_ES) && __VERSION__ >= 300
	#define varying out
	#define attribute in
	#define texture2D texture
	#define textureCube texture
	#ifdef DISABLE_USE_TEXTURE_ARRAY
        #define sampler2DArray sampler2D
    #else
        #define USE_TEXTURE_ARRAY
    #endif
#elif !defined(GL_ES) && __VERSION__ > 120
	#define attribute in
	#define varying out
	#define texture2D texture
	#define textureCube texture
#endif


// attributes
attribute highp vec4 position;

// uniforms
uniform highp mat4 worldviewprojection;
uniform highp vec3 dir0;
uniform highp vec3 dir1;
uniform highp vec3 dir2;
uniform highp vec3 dir3;

varying highp vec3 vDir;


void main(void)
{
    highp vec3 dirs[4];
    dirs[0] = dir0;
    dirs[1] = dir1;
    dirs[2] = dir2;
    dirs[3] = dir3;
    
    vDir = dirs[ gl_VertexID ];
    gl_Position = worldviewprojection * position;
}

