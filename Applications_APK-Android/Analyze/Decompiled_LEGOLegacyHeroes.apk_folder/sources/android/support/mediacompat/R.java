package android.support.mediacompat;

public final class R {

    public static final class attr {
        public static final int font = 2130837647;
        public static final int fontProviderAuthority = 2130837649;
        public static final int fontProviderCerts = 2130837650;
        public static final int fontProviderFetchStrategy = 2130837651;
        public static final int fontProviderFetchTimeout = 2130837652;
        public static final int fontProviderPackage = 2130837653;
        public static final int fontProviderQuery = 2130837654;
        public static final int fontStyle = 2130837655;
        public static final int fontWeight = 2130837656;
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2130903040;
    }

    public static final class color {
        public static final int notification_action_color_filter = 2130968680;
        public static final int notification_icon_bg_color = 2130968681;
        public static final int notification_material_background_media_default_color = 2130968682;
        public static final int primary_text_default_material_dark = 2130968687;
        public static final int ripple_material_light = 2130968692;
        public static final int secondary_text_default_material_dark = 2130968693;
        public static final int secondary_text_default_material_light = 2130968694;
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131034208;
        public static final int compat_button_inset_vertical_material = 2131034209;
        public static final int compat_button_padding_horizontal_material = 2131034210;
        public static final int compat_button_padding_vertical_material = 2131034211;
        public static final int compat_control_corner_material = 2131034212;
        public static final int notification_action_icon_size = 2131034239;
        public static final int notification_action_text_size = 2131034240;
        public static final int notification_big_circle_margin = 2131034241;
        public static final int notification_content_margin_start = 2131034242;
        public static final int notification_large_icon_height = 2131034243;
        public static final int notification_large_icon_width = 2131034244;
        public static final int notification_main_column_padding_top = 2131034245;
        public static final int notification_media_narrow_margin = 2131034246;
        public static final int notification_right_icon_size = 2131034247;
        public static final int notification_right_side_padding_top = 2131034248;
        public static final int notification_small_icon_background_padding = 2131034249;
        public static final int notification_small_icon_size_as_large = 2131034250;
        public static final int notification_subtext_size = 2131034251;
        public static final int notification_top_pad = 2131034252;
        public static final int notification_top_pad_large_text = 2131034253;
    }

    public static final class drawable {
        public static final int notification_action_background = 2131099806;
        public static final int notification_bg = 2131099807;
        public static final int notification_bg_low = 2131099808;
        public static final int notification_bg_low_normal = 2131099809;
        public static final int notification_bg_low_pressed = 2131099810;
        public static final int notification_bg_normal = 2131099811;
        public static final int notification_bg_normal_pressed = 2131099812;
        public static final int notification_icon_background = 2131099813;
        public static final int notification_template_icon_bg = 2131099814;
        public static final int notification_template_icon_low_bg = 2131099815;
        public static final int notification_tile_bg = 2131099816;
        public static final int notify_panel_notification_icon_bg = 2131099817;
    }

    public static final class id {
        public static final int action0 = 2131165193;
        public static final int action_container = 2131165201;
        public static final int action_divider = 2131165203;
        public static final int action_image = 2131165204;
        public static final int action_text = 2131165210;
        public static final int actions = 2131165211;
        public static final int async = 2131165219;
        public static final int blocking = 2131165223;
        public static final int cancel_action = 2131165236;
        public static final int chronometer = 2131165242;
        public static final int end_padder = 2131165269;
        public static final int forever = 2131165275;
        public static final int icon = 2131165281;
        public static final int icon_group = 2131165282;
        public static final int info = 2131165286;
        public static final int italic = 2131165294;
        public static final int line1 = 2131165298;
        public static final int line3 = 2131165299;
        public static final int media_actions = 2131165315;
        public static final int normal = 2131165322;
        public static final int notification_background = 2131165323;
        public static final int notification_main_column = 2131165324;
        public static final int notification_main_column_container = 2131165325;
        public static final int right_icon = 2131165337;
        public static final int right_side = 2131165338;
        public static final int status_bar_latest_event_content = 2131165367;
        public static final int tag_transition_group = 2131165371;
        public static final int text = 2131165372;
        public static final int text2 = 2131165373;
        public static final int time = 2131165377;
        public static final int title = 2131165378;
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131230722;
        public static final int status_bar_notification_info_maxnum = 2131230730;
    }

    public static final class layout {
        public static final int notification_action = 2131296304;
        public static final int notification_action_tombstone = 2131296305;
        public static final int notification_media_action = 2131296306;
        public static final int notification_media_cancel_action = 2131296307;
        public static final int notification_template_big_media = 2131296308;
        public static final int notification_template_big_media_custom = 2131296309;
        public static final int notification_template_big_media_narrow = 2131296310;
        public static final int notification_template_big_media_narrow_custom = 2131296311;
        public static final int notification_template_custom_big = 2131296312;
        public static final int notification_template_icon_group = 2131296313;
        public static final int notification_template_lines_media = 2131296314;
        public static final int notification_template_media = 2131296315;
        public static final int notification_template_media_custom = 2131296316;
        public static final int notification_template_part_chronometer = 2131296317;
        public static final int notification_template_part_time = 2131296318;
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131493190;
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131558635;
        public static final int TextAppearance_Compat_Notification_Info = 2131558636;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131558637;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131558638;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131558639;
        public static final int TextAppearance_Compat_Notification_Media = 2131558640;
        public static final int TextAppearance_Compat_Notification_Time = 2131558641;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131558642;
        public static final int TextAppearance_Compat_Notification_Title = 2131558643;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131558644;
        public static final int Widget_Compat_NotificationActionContainer = 2131558758;
        public static final int Widget_Compat_NotificationActionText = 2131558759;
    }

    public static final class styleable {
        public static final int[] FontFamily = {com.gameloft.anmp.lego.heroes.R.attr.fontProviderAuthority, com.gameloft.anmp.lego.heroes.R.attr.fontProviderCerts, com.gameloft.anmp.lego.heroes.R.attr.fontProviderFetchStrategy, com.gameloft.anmp.lego.heroes.R.attr.fontProviderFetchTimeout, com.gameloft.anmp.lego.heroes.R.attr.fontProviderPackage, com.gameloft.anmp.lego.heroes.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, com.gameloft.anmp.lego.heroes.R.attr.font, com.gameloft.anmp.lego.heroes.R.attr.fontStyle, com.gameloft.anmp.lego.heroes.R.attr.fontWeight};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_font = 3;
        public static final int FontFamilyFont_fontStyle = 4;
        public static final int FontFamilyFont_fontWeight = 5;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
    }
}
