package com.millennialmedia;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.millennialmedia.internal.ActivityListenerManager;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementMetadata;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.ErrorStatus;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.AdAdapter;
import com.millennialmedia.internal.adadapters.InlineAdapter;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.Utils;
import com.millennialmedia.internal.utils.ViewUtils;
import java.lang.ref.WeakReference;
import java.util.Map;

public class InlineAd extends AdPlacement {
    protected static final String STATE_ATTACHING = "attaching";
    protected static final String STATE_LOAD_ABORTED = "aborted";
    /* access modifiers changed from: private */
    public static final String TAG = "InlineAd";
    /* access modifiers changed from: private */
    public volatile boolean aborting = false;
    private ThreadUtils.ScheduledRunnable adAdapterRequestTimeoutRunnable;
    /* access modifiers changed from: private */
    public final WeakReference<ViewGroup> adContainerRef;
    /* access modifiers changed from: private */
    public volatile InlineAdapter currentInlineAdAdapter;
    private AdPlacement.DisplayOptions displayOptions;
    private volatile boolean hasRequested = false;
    /* access modifiers changed from: private */
    public volatile ImpressionListener impressionListener;
    private InlineAbortListener inlineAbortListener;
    /* access modifiers changed from: private */
    public InlineAdMetadata inlineAdMetadata;
    /* access modifiers changed from: private */
    public InlineListener inlineListener;
    /* access modifiers changed from: private */
    public volatile boolean isExpanded = false;
    /* access modifiers changed from: private */
    public volatile boolean isResized = false;
    private long lastRequestTime;
    /* access modifiers changed from: private */
    public RelativeLayout mmAdContainer;
    /* access modifiers changed from: private */
    public volatile InlineAdapter nextInlineAdAdapter;
    private ThreadUtils.ScheduledRunnable placementRequestTimeoutRunnable;
    /* access modifiers changed from: private */
    public ThreadUtils.ScheduledRunnable refreshRunnable;
    private Integer requestedRefreshInterval;

    public interface InlineAbortListener {
        void onAbortFailed(InlineAd inlineAd);

        void onAborted(InlineAd inlineAd);
    }

    public interface InlineListener {
        void onAdLeftApplication(InlineAd inlineAd);

        void onClicked(InlineAd inlineAd);

        void onCollapsed(InlineAd inlineAd);

        void onExpanded(InlineAd inlineAd);

        void onRequestFailed(InlineAd inlineAd, InlineErrorStatus inlineErrorStatus);

        void onRequestSucceeded(InlineAd inlineAd);

        void onResize(InlineAd inlineAd, int i, int i2);

        void onResized(InlineAd inlineAd, int i, int i2, boolean z);
    }

    public static class InlineErrorStatus extends ErrorStatus {
        public InlineErrorStatus(int i) {
            super(i);
        }

        public InlineErrorStatus(int i, String str) {
            super(i, str);
        }
    }

    public static class InlineAdMetadata extends AdPlacementMetadata<InlineAdMetadata> {
        private static final String PLACEMENT_TYPE_INLINE = "inline";
        private AdSize adSize;

        public InlineAdMetadata() {
            super("inline");
        }

        public InlineAdMetadata setAdSize(AdSize adSize2) {
            if (adSize2 == null) {
                MMLog.e(InlineAd.TAG, "Provided AdSize cannot be null");
            } else {
                this.adSize = adSize2;
            }
            return this;
        }

        public AdSize getAdSize() {
            return this.adSize;
        }

        public boolean hasValidAdSize() {
            return this.adSize != null && this.adSize.width >= 0 && this.adSize.height >= 0;
        }

        /* access modifiers changed from: package-private */
        public int getWidth(InlineAd inlineAd) {
            ViewGroup viewGroup;
            if (this.adSize != null && this.adSize.width != 0) {
                return this.adSize.width;
            }
            if (inlineAd == null || (viewGroup = (ViewGroup) inlineAd.adContainerRef.get()) == null) {
                return 0;
            }
            return ViewUtils.convertPixelsToDips(viewGroup.getWidth());
        }

        /* access modifiers changed from: package-private */
        public int getHeight(InlineAd inlineAd) {
            ViewGroup viewGroup;
            if (this.adSize != null && this.adSize.height != 0) {
                return this.adSize.height;
            }
            if (inlineAd == null || (viewGroup = (ViewGroup) inlineAd.adContainerRef.get()) == null) {
                return 0;
            }
            return ViewUtils.convertPixelsToDips(viewGroup.getHeight());
        }

        /* access modifiers changed from: package-private */
        public Map<String, Object> toMap(InlineAd inlineAd) {
            return addElementsToMap(super.toMap((AdPlacement) inlineAd), inlineAd);
        }

        public Map<String, Object> toMap(String str) {
            return addElementsToMap(super.toMap(str), null);
        }

        private Map<String, Object> addElementsToMap(Map<String, Object> map, InlineAd inlineAd) {
            Utils.injectIfNotNull(map, "width", Integer.valueOf(getWidth(inlineAd)));
            Utils.injectIfNotNull(map, "height", Integer.valueOf(getHeight(inlineAd)));
            if (inlineAd != null) {
                Utils.injectIfNotNull(map, "refreshRate", inlineAd.getRefreshInterval());
            }
            return map;
        }
    }

    private static class ImpressionListener {
        volatile boolean impressionFound = false;
        volatile ThreadUtils.ScheduledRunnable impressionTimerRunnable;
        WeakReference<InlineAd> inlineAdRef;
        long minImpressionDelay;
        int minViewabilityPercentage;
        ViewUtils.ViewabilityWatcher viewabilityWatcher;

        ImpressionListener(InlineAd inlineAd, View view, long j, int i) {
            this.minViewabilityPercentage = i;
            this.minImpressionDelay = i == 0 ? 0 : j;
            this.inlineAdRef = new WeakReference<>(inlineAd);
            this.viewabilityWatcher = new ViewUtils.ViewabilityWatcher(view, new ViewUtils.ViewabilityListener() {
                public void onViewableChanged(boolean z) {
                    synchronized (ImpressionListener.this) {
                        if (z) {
                            try {
                                if (ImpressionListener.this.impressionTimerRunnable == null && !ImpressionListener.this.impressionFound) {
                                    ImpressionListener.this.impressionTimerRunnable = ThreadUtils.runOnWorkerThreadDelayed(new Runnable() {
                                        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0046, code lost:
                                            if (r7.this$1.this$0.minImpressionDelay != 0) goto L_0x0049;
                                         */
                                        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
                                            r3 = 0;
                                         */
                                        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
                                            com.millennialmedia.InlineAd.access$200(r0, r3);
                                         */
                                        /* JADX WARNING: Code restructure failed: missing block: B:18:0x004c, code lost:
                                            return;
                                         */
                                        /* Code decompiled incorrectly, please refer to instructions dump. */
                                        public void run() {
                                            /*
                                                r7 = this;
                                                com.millennialmedia.InlineAd$ImpressionListener$1 r0 = com.millennialmedia.InlineAd.ImpressionListener.AnonymousClass1.this
                                                com.millennialmedia.InlineAd$ImpressionListener r0 = com.millennialmedia.InlineAd.ImpressionListener.this
                                                java.lang.ref.WeakReference<com.millennialmedia.InlineAd> r0 = r0.inlineAdRef
                                                java.lang.Object r0 = r0.get()
                                                com.millennialmedia.InlineAd r0 = (com.millennialmedia.InlineAd) r0
                                                if (r0 == 0) goto L_0x0052
                                                boolean r1 = r0.isDestroyed()
                                                if (r1 == 0) goto L_0x0015
                                                goto L_0x0052
                                            L_0x0015:
                                                com.millennialmedia.InlineAd$ImpressionListener$1 r1 = com.millennialmedia.InlineAd.ImpressionListener.AnonymousClass1.this
                                                com.millennialmedia.InlineAd$ImpressionListener r1 = com.millennialmedia.InlineAd.ImpressionListener.this
                                                monitor-enter(r1)
                                                com.millennialmedia.InlineAd$ImpressionListener$1 r2 = com.millennialmedia.InlineAd.ImpressionListener.AnonymousClass1.this     // Catch:{ all -> 0x004f }
                                                com.millennialmedia.InlineAd$ImpressionListener r2 = com.millennialmedia.InlineAd.ImpressionListener.this     // Catch:{ all -> 0x004f }
                                                r3 = 0
                                                r2.impressionTimerRunnable = r3     // Catch:{ all -> 0x004f }
                                                com.millennialmedia.InlineAd$ImpressionListener$1 r2 = com.millennialmedia.InlineAd.ImpressionListener.AnonymousClass1.this     // Catch:{ all -> 0x004f }
                                                com.millennialmedia.InlineAd$ImpressionListener r2 = com.millennialmedia.InlineAd.ImpressionListener.this     // Catch:{ all -> 0x004f }
                                                com.millennialmedia.internal.utils.ViewUtils$ViewabilityWatcher r2 = r2.viewabilityWatcher     // Catch:{ all -> 0x004f }
                                                boolean r2 = r2.viewable     // Catch:{ all -> 0x004f }
                                                if (r2 == 0) goto L_0x004d
                                                com.millennialmedia.InlineAd$ImpressionListener$1 r2 = com.millennialmedia.InlineAd.ImpressionListener.AnonymousClass1.this     // Catch:{ all -> 0x004f }
                                                com.millennialmedia.InlineAd$ImpressionListener r2 = com.millennialmedia.InlineAd.ImpressionListener.this     // Catch:{ all -> 0x004f }
                                                boolean r2 = r2.impressionFound     // Catch:{ all -> 0x004f }
                                                if (r2 == 0) goto L_0x0034
                                                goto L_0x004d
                                            L_0x0034:
                                                com.millennialmedia.InlineAd$ImpressionListener$1 r2 = com.millennialmedia.InlineAd.ImpressionListener.AnonymousClass1.this     // Catch:{ all -> 0x004f }
                                                com.millennialmedia.InlineAd$ImpressionListener r2 = com.millennialmedia.InlineAd.ImpressionListener.this     // Catch:{ all -> 0x004f }
                                                r3 = 1
                                                r2.impressionFound = r3     // Catch:{ all -> 0x004f }
                                                monitor-exit(r1)     // Catch:{ all -> 0x004f }
                                                com.millennialmedia.InlineAd$ImpressionListener$1 r1 = com.millennialmedia.InlineAd.ImpressionListener.AnonymousClass1.this
                                                com.millennialmedia.InlineAd$ImpressionListener r1 = com.millennialmedia.InlineAd.ImpressionListener.this
                                                long r1 = r1.minImpressionDelay
                                                r4 = 0
                                                int r6 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
                                                if (r6 != 0) goto L_0x0049
                                                r3 = 0
                                            L_0x0049:
                                                r0.reportImpression(r3)
                                                return
                                            L_0x004d:
                                                monitor-exit(r1)     // Catch:{ all -> 0x004f }
                                                return
                                            L_0x004f:
                                                r0 = move-exception
                                                monitor-exit(r1)     // Catch:{ all -> 0x004f }
                                                throw r0
                                            L_0x0052:
                                                return
                                            */
                                            throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.ImpressionListener.AnonymousClass1.AnonymousClass1.run():void");
                                        }
                                    }, ImpressionListener.this.minImpressionDelay);
                                }
                            } catch (Throwable th) {
                                throw th;
                            }
                        }
                        if (!z && ImpressionListener.this.impressionTimerRunnable != null) {
                            ImpressionListener.this.impressionTimerRunnable.cancel();
                            ImpressionListener.this.impressionTimerRunnable = null;
                        }
                    }
                }
            });
        }

        /* access modifiers changed from: protected */
        public void listen() {
            if (this.viewabilityWatcher != null) {
                this.viewabilityWatcher.setMinViewabilityPercent(this.minViewabilityPercentage);
                this.viewabilityWatcher.startWatching();
            }
        }

        public void cancel() {
            synchronized (this) {
                this.viewabilityWatcher.stopWatching();
                if (this.impressionTimerRunnable != null) {
                    this.impressionTimerRunnable.cancel();
                    this.impressionTimerRunnable = null;
                }
            }
        }
    }

    public static class AdSize {
        public static final int AUTO_HEIGHT = 0;
        public static final int AUTO_WIDTH = 0;
        public static final AdSize BANNER = new AdSize(320, 50);
        public static final AdSize FULL_BANNER = new AdSize(468, 60);
        public static final AdSize LARGE_BANNER = new AdSize(320, 100);
        public static final AdSize LEADERBOARD = new AdSize(728, 90);
        public static final AdSize MEDIUM_RECTANGLE = new AdSize(300, 250);
        public static final AdSize SMART_BANNER = new AdSize(0, 0);
        public final int height;
        public final int width;

        public AdSize(int i, int i2) {
            this.width = i <= 0 ? 0 : i;
            this.height = i2 <= 0 ? 0 : i2;
        }

        public boolean isAuto() {
            return this.width == 0 || this.height == 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass() || !(obj instanceof AdSize)) {
                return false;
            }
            AdSize adSize = (AdSize) obj;
            if (this.width == adSize.width && this.height == adSize.height) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (31 * this.width) + this.height;
        }

        public String toString() {
            return "Inline ad of size " + this.width + " by " + this.height;
        }
    }

    private static class RefreshRunnable implements Runnable {
        WeakReference<InlineAd> inlineAdRef;

        RefreshRunnable(InlineAd inlineAd) {
            this.inlineAdRef = new WeakReference<>(inlineAd);
        }

        public void run() {
            final InlineAd inlineAd = this.inlineAdRef.get();
            if (inlineAd == null) {
                MMLog.e(InlineAd.TAG, "InlineAd instance has been destroyed, shutting down refresh behavior");
                return;
            }
            ViewGroup viewGroup = (ViewGroup) inlineAd.adContainerRef.get();
            if (viewGroup == null) {
                MMLog.e(InlineAd.TAG, "InlineAd container has been destroyed, shutting down refresh behavior");
            } else if (!inlineAd.isRefreshEnabled()) {
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(InlineAd.TAG, "Inline refresh disabled, aborting refresh behavior");
                }
                ThreadUtils.ScheduledRunnable unused = inlineAd.refreshRunnable = null;
            } else {
                Activity activityForView = ViewUtils.getActivityForView(viewGroup);
                if (activityForView == null) {
                    MMLog.e(InlineAd.TAG, "Unable to find valid activity context for placement container, aborting refresh");
                    return;
                }
                boolean z = false;
                boolean z2 = ActivityListenerManager.getLifecycleState(activityForView) == ActivityListenerManager.LifecycleState.RESUMED;
                if (inlineAd.impressionListener == null || inlineAd.impressionListener.impressionFound) {
                    z = true;
                }
                if (viewGroup.isShown() && !inlineAd.isResized && !inlineAd.isExpanded && z2 && z) {
                    ThreadUtils.runOnWorkerThread(new Runnable() {
                        public void run() {
                            inlineAd.loadPlayList();
                        }
                    });
                }
                ThreadUtils.ScheduledRunnable unused2 = inlineAd.refreshRunnable = ThreadUtils.runOnWorkerThreadDelayed(this, (long) inlineAd.getRefreshInterval().intValue());
            }
        }
    }

    public static InlineAd createInstance(String str, ViewGroup viewGroup) throws MMException {
        if (!MMSDK.isInitialized()) {
            throw new MMInitializationException("Unable to create instance, SDK must be initialized first");
        } else if (viewGroup == null) {
            throw new MMException("Unable to create instance, ad container cannot be null");
        } else if (viewGroup.getContext() != null) {
            return new InlineAd(str, viewGroup);
        } else {
            throw new MMException("Unable to create instance, ad container must have an associated context");
        }
    }

    private InlineAd(String str, ViewGroup viewGroup) throws MMException {
        super(str);
        this.adContainerRef = new WeakReference<>(viewGroup);
    }

    public void request(InlineAdMetadata inlineAdMetadata2) {
        if (!isDestroyed()) {
            String str = TAG;
            MMLog.i(str, "Requesting playlist for placement ID: " + this.placementId);
            this.inlineAdMetadata = inlineAdMetadata2;
            this.hasRequested = true;
            loadPlayList();
            startRefresh();
        }
    }

    public static void requestBid(String str, InlineAdMetadata inlineAdMetadata2, BidRequestListener bidRequestListener) throws MMException {
        if (inlineAdMetadata2 == null) {
            throw new MMException("Metadata must not be null");
        }
        AdSize adSize = inlineAdMetadata2.getAdSize();
        if (!inlineAdMetadata2.hasValidAdSize() || adSize.isAuto()) {
            throw new MMException("Invalid AdSize <" + adSize + ">");
        }
        AdPlacement.requestBid(str, inlineAdMetadata2, bidRequestListener);
    }

    public void setDisplayOptions(AdPlacement.DisplayOptions displayOptions2) {
        this.displayOptions = displayOptions2;
    }

    /* access modifiers changed from: private */
    public void setCurrentAdAdapter(InlineAdapter inlineAdapter) {
        if (!(this.currentInlineAdAdapter == null || this.currentInlineAdAdapter == inlineAdapter)) {
            this.currentInlineAdAdapter.release();
        }
        this.currentInlineAdAdapter = inlineAdapter;
    }

    public void abort(InlineAbortListener inlineAbortListener2) {
        if (!isDestroyed()) {
            String str = TAG;
            MMLog.i(str, "Attempting to abort playlist request for placement ID: " + this.placementId);
            this.inlineAbortListener = inlineAbortListener2;
            synchronized (this) {
                if (isLoading()) {
                    if (MMLog.isDebugEnabled()) {
                        String str2 = TAG;
                        MMLog.d(str2, "Aborting playlist request for placement ID: " + this.placementId);
                    }
                    this.aborting = true;
                    return;
                }
                onAbortFailed();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
        r8.playList = null;
        r8.lastRequestTime = java.lang.System.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        if (r8.inlineAdMetadata != null) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004b, code lost:
        r8.inlineAdMetadata = new com.millennialmedia.InlineAd.InlineAdMetadata();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0054, code lost:
        if (r8.displayOptions != null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0056, code lost:
        r8.displayOptions = new com.millennialmedia.internal.AdPlacement.DisplayOptions();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005d, code lost:
        r0 = getRequestState();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0063, code lost:
        if (r8.placementRequestTimeoutRunnable == null) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0065, code lost:
        r8.placementRequestTimeoutRunnable.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006a, code lost:
        r1 = com.millennialmedia.internal.Handshake.getInlineTimeout();
        r8.placementRequestTimeoutRunnable = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(new com.millennialmedia.InlineAd.AnonymousClass1(r8), (long) r1);
        r2 = r8.inlineAdMetadata.getImpressionGroup();
        com.millennialmedia.internal.playlistserver.PlayListServer.loadPlayList(r8.inlineAdMetadata.toMap(r8), new com.millennialmedia.InlineAd.AnonymousClass2(r8), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadPlayList() {
        /*
            r8 = this;
            boolean r0 = r8.isResized
            if (r0 != 0) goto L_0x0092
            boolean r0 = r8.isExpanded
            if (r0 == 0) goto L_0x000a
            goto L_0x0092
        L_0x000a:
            long r0 = java.lang.System.currentTimeMillis()
            long r2 = r8.lastRequestTime
            int r4 = com.millennialmedia.internal.Handshake.getMinInlineRefreshRate()
            long r4 = (long) r4
            long r6 = r2 + r4
            int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r2 >= 0) goto L_0x0023
            java.lang.String r0 = com.millennialmedia.InlineAd.TAG
            java.lang.String r1 = "Too soon since last inline ad request, unable to request ad"
            com.millennialmedia.MMLog.e(r0, r1)
            return
        L_0x0023:
            monitor-enter(r8)
            boolean r0 = r8.isDestroyed()     // Catch:{ all -> 0x008f }
            if (r0 == 0) goto L_0x002c
            monitor-exit(r8)     // Catch:{ all -> 0x008f }
            return
        L_0x002c:
            boolean r0 = r8.isLoading()     // Catch:{ all -> 0x008f }
            if (r0 == 0) goto L_0x0034
            monitor-exit(r8)     // Catch:{ all -> 0x008f }
            return
        L_0x0034:
            r0 = 0
            r8.aborting = r0     // Catch:{ all -> 0x008f }
            r0 = 0
            r8.inlineAbortListener = r0     // Catch:{ all -> 0x008f }
            java.lang.String r1 = "loading_play_list"
            r8.placementState = r1     // Catch:{ all -> 0x008f }
            monitor-exit(r8)     // Catch:{ all -> 0x008f }
            r8.playList = r0
            long r0 = java.lang.System.currentTimeMillis()
            r8.lastRequestTime = r0
            com.millennialmedia.InlineAd$InlineAdMetadata r0 = r8.inlineAdMetadata
            if (r0 != 0) goto L_0x0052
            com.millennialmedia.InlineAd$InlineAdMetadata r0 = new com.millennialmedia.InlineAd$InlineAdMetadata
            r0.<init>()
            r8.inlineAdMetadata = r0
        L_0x0052:
            com.millennialmedia.internal.AdPlacement$DisplayOptions r0 = r8.displayOptions
            if (r0 != 0) goto L_0x005d
            com.millennialmedia.internal.AdPlacement$DisplayOptions r0 = new com.millennialmedia.internal.AdPlacement$DisplayOptions
            r0.<init>()
            r8.displayOptions = r0
        L_0x005d:
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r8.getRequestState()
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r1 = r8.placementRequestTimeoutRunnable
            if (r1 == 0) goto L_0x006a
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r1 = r8.placementRequestTimeoutRunnable
            r1.cancel()
        L_0x006a:
            int r1 = com.millennialmedia.internal.Handshake.getInlineTimeout()
            com.millennialmedia.InlineAd$1 r2 = new com.millennialmedia.InlineAd$1
            r2.<init>(r0)
            long r3 = (long) r1
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r2 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r2, r3)
            r8.placementRequestTimeoutRunnable = r2
            com.millennialmedia.InlineAd$InlineAdMetadata r2 = r8.inlineAdMetadata
            java.lang.String r2 = r2.getImpressionGroup()
            com.millennialmedia.InlineAd$InlineAdMetadata r3 = r8.inlineAdMetadata
            java.util.Map r3 = r3.toMap(r8)
            com.millennialmedia.InlineAd$2 r4 = new com.millennialmedia.InlineAd$2
            r4.<init>(r0, r2)
            com.millennialmedia.internal.playlistserver.PlayListServer.loadPlayList(r3, r4, r1)
            return
        L_0x008f:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x008f }
            throw r0
        L_0x0092:
            java.lang.String r0 = com.millennialmedia.InlineAd.TAG
            java.lang.String r1 = "Inline ad is resized or expanded, unable to request new ad"
            com.millennialmedia.MMLog.w(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.loadPlayList():void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003b, code lost:
        if (r6.playList.hasNext() != false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0043, code lost:
        com.millennialmedia.MMLog.d(com.millennialmedia.InlineAd.TAG, "Unable to find ad adapter in play list");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004a, code lost:
        onRequestFailed(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004d, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
        if (r6.aborting == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        onAborted(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0055, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0056, code lost:
        r7 = com.millennialmedia.internal.AdPlacementReporter.getPlayListItemReporter(r7.getAdPlacementReporter());
        r6.nextInlineAdAdapter = (com.millennialmedia.internal.adadapters.InlineAdapter) r6.playList.getNextAdAdapter(r6, r7);
        r1 = r6.adContainerRef.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0072, code lost:
        if (r6.nextInlineAdAdapter == null) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0074, code lost:
        if (r1 == null) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0076, code lost:
        r2 = r6.nextInlineAdAdapter.requestTimeout;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007a, code lost:
        if (r2 <= 0) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007e, code lost:
        if (r6.adAdapterRequestTimeoutRunnable == null) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0080, code lost:
        r6.adAdapterRequestTimeoutRunnable.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0085, code lost:
        r6.adAdapterRequestTimeoutRunnable = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(new com.millennialmedia.InlineAd.AnonymousClass3(r6), (long) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0091, code lost:
        r6.nextInlineAdAdapter.init(r1.getContext(), new com.millennialmedia.InlineAd.AnonymousClass4(r6), r6.displayOptions);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a2, code lost:
        com.millennialmedia.internal.AdPlacementReporter.reportPlayListItem(r0.getAdPlacementReporter(), r7);
        onAdAdapterLoadFailed(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadAdAdapter(com.millennialmedia.internal.AdPlacement.RequestState r7) {
        /*
            r6 = this;
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r7.copy()
            monitor-enter(r6)
            boolean r1 = r6.doPendingDestroy()     // Catch:{ all -> 0x00af }
            if (r1 == 0) goto L_0x000d
            monitor-exit(r6)     // Catch:{ all -> 0x00af }
            return
        L_0x000d:
            com.millennialmedia.internal.AdPlacement$RequestState r1 = r6.currentRequestState     // Catch:{ all -> 0x00af }
            boolean r1 = r1.compareRequest(r0)     // Catch:{ all -> 0x00af }
            if (r1 == 0) goto L_0x00ad
            java.lang.String r1 = r6.placementState     // Catch:{ all -> 0x00af }
            java.lang.String r2 = "play_list_loaded"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x00af }
            if (r1 != 0) goto L_0x002b
            java.lang.String r1 = r6.placementState     // Catch:{ all -> 0x00af }
            java.lang.String r2 = "ad_adapter_load_failed"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x00af }
            if (r1 != 0) goto L_0x002b
            goto L_0x00ad
        L_0x002b:
            java.lang.String r1 = "loading_ad_adapter"
            r6.placementState = r1     // Catch:{ all -> 0x00af }
            r0.getItemHash()     // Catch:{ all -> 0x00af }
            r6.currentRequestState = r0     // Catch:{ all -> 0x00af }
            monitor-exit(r6)     // Catch:{ all -> 0x00af }
            com.millennialmedia.internal.PlayList r1 = r6.playList
            boolean r1 = r1.hasNext()
            if (r1 != 0) goto L_0x004e
            boolean r7 = com.millennialmedia.MMLog.isDebugEnabled()
            if (r7 == 0) goto L_0x004a
            java.lang.String r7 = com.millennialmedia.InlineAd.TAG
            java.lang.String r1 = "Unable to find ad adapter in play list"
            com.millennialmedia.MMLog.d(r7, r1)
        L_0x004a:
            r6.onRequestFailed(r0)
            return
        L_0x004e:
            boolean r1 = r6.aborting
            if (r1 == 0) goto L_0x0056
            r6.onAborted(r0)
            return
        L_0x0056:
            com.millennialmedia.internal.AdPlacementReporter r7 = r7.getAdPlacementReporter()
            com.millennialmedia.internal.AdPlacementReporter$PlayListItemReporter r7 = com.millennialmedia.internal.AdPlacementReporter.getPlayListItemReporter(r7)
            com.millennialmedia.internal.PlayList r1 = r6.playList
            com.millennialmedia.internal.adadapters.AdAdapter r1 = r1.getNextAdAdapter(r6, r7)
            com.millennialmedia.internal.adadapters.InlineAdapter r1 = (com.millennialmedia.internal.adadapters.InlineAdapter) r1
            r6.nextInlineAdAdapter = r1
            java.lang.ref.WeakReference<android.view.ViewGroup> r1 = r6.adContainerRef
            java.lang.Object r1 = r1.get()
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            com.millennialmedia.internal.adadapters.InlineAdapter r2 = r6.nextInlineAdAdapter
            if (r2 == 0) goto L_0x00a2
            if (r1 == 0) goto L_0x00a2
            com.millennialmedia.internal.adadapters.InlineAdapter r2 = r6.nextInlineAdAdapter
            int r2 = r2.requestTimeout
            if (r2 <= 0) goto L_0x0091
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = r6.adAdapterRequestTimeoutRunnable
            if (r3 == 0) goto L_0x0085
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = r6.adAdapterRequestTimeoutRunnable
            r3.cancel()
        L_0x0085:
            com.millennialmedia.InlineAd$3 r3 = new com.millennialmedia.InlineAd$3
            r3.<init>(r0, r7)
            long r4 = (long) r2
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r2 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r3, r4)
            r6.adAdapterRequestTimeoutRunnable = r2
        L_0x0091:
            com.millennialmedia.internal.adadapters.InlineAdapter r2 = r6.nextInlineAdAdapter
            android.content.Context r1 = r1.getContext()
            com.millennialmedia.InlineAd$4 r3 = new com.millennialmedia.InlineAd$4
            r3.<init>(r0, r7)
            com.millennialmedia.internal.AdPlacement$DisplayOptions r7 = r6.displayOptions
            r2.init(r1, r3, r7)
            goto L_0x00ac
        L_0x00a2:
            com.millennialmedia.internal.AdPlacementReporter r1 = r0.getAdPlacementReporter()
            com.millennialmedia.internal.AdPlacementReporter.reportPlayListItem(r1, r7)
            r6.onAdAdapterLoadFailed(r0)
        L_0x00ac:
            return
        L_0x00ad:
            monitor-exit(r6)     // Catch:{ all -> 0x00af }
            return
        L_0x00af:
            r7 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00af }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.loadAdAdapter(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    public void setListener(InlineListener inlineListener2) {
        if (!isDestroyed()) {
            this.inlineListener = inlineListener2;
        }
    }

    public void setRefreshInterval(int i) {
        if (!isDestroyed()) {
            this.requestedRefreshInterval = Integer.valueOf(Math.max(0, i));
            if (this.hasRequested) {
                startRefresh();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Integer getRefreshInterval() {
        if (isDestroyed()) {
            return null;
        }
        if (isRefreshEnabled()) {
            return Integer.valueOf(Math.max(this.requestedRefreshInterval.intValue(), Handshake.getMinInlineRefreshRate()));
        }
        return this.requestedRefreshInterval;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void startRefresh() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.isRefreshEnabled()     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x0022
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r0 = r3.refreshRunnable     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x000c
            goto L_0x0022
        L_0x000c:
            com.millennialmedia.InlineAd$RefreshRunnable r0 = new com.millennialmedia.InlineAd$RefreshRunnable     // Catch:{ all -> 0x0031 }
            r0.<init>(r3)     // Catch:{ all -> 0x0031 }
            java.lang.Integer r1 = r3.getRefreshInterval()     // Catch:{ all -> 0x0031 }
            int r1 = r1.intValue()     // Catch:{ all -> 0x0031 }
            long r1 = (long) r1     // Catch:{ all -> 0x0031 }
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r0 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r0, r1)     // Catch:{ all -> 0x0031 }
            r3.refreshRunnable = r0     // Catch:{ all -> 0x0031 }
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
            return
        L_0x0022:
            boolean r0 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x002f
            java.lang.String r0 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x0031 }
            java.lang.String r1 = "Refresh disabled or already started, returning"
            com.millennialmedia.MMLog.d(r0, r1)     // Catch:{ all -> 0x0031 }
        L_0x002f:
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
            return
        L_0x0031:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.startRefresh():void");
    }

    /* access modifiers changed from: package-private */
    public boolean isRefreshEnabled() {
        if (!isDestroyed() && this.requestedRefreshInterval != null && this.requestedRefreshInterval.intValue() > 0) {
            return true;
        }
        return false;
    }

    public CreativeInfo getCreativeInfo() {
        if (this.currentInlineAdAdapter != null) {
            return this.currentInlineAdAdapter.getCreativeInfo();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void stopRequestTimeoutTimers() {
        if (this.placementRequestTimeoutRunnable != null) {
            this.placementRequestTimeoutRunnable.cancel();
            this.placementRequestTimeoutRunnable = null;
        }
        if (this.adAdapterRequestTimeoutRunnable != null) {
            this.adAdapterRequestTimeoutRunnable.cancel();
            this.adAdapterRequestTimeoutRunnable = null;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r1.impressionListener == null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1.impressionListener.cancel();
        r1.impressionListener = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0025, code lost:
        stopRequestTimeoutTimers();
        releaseAdapters();
        com.millennialmedia.internal.utils.ThreadUtils.postOnUiThread(new com.millennialmedia.InlineAd.AnonymousClass5(r1));
        r1.placementState = "idle";
        r1.mmAdContainer = null;
        r1.playList = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onUnload(com.millennialmedia.internal.AdPlacement.RequestState r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x003c }
            boolean r2 = r0.compare(r2)     // Catch:{ all -> 0x003c }
            if (r2 != 0) goto L_0x0018
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x003c }
            if (r2 == 0) goto L_0x0016
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x003c }
            java.lang.String r0 = "unload called but request state is not valid"
            com.millennialmedia.MMLog.d(r2, r0)     // Catch:{ all -> 0x003c }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x003c }
            return
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x003c }
            com.millennialmedia.InlineAd$ImpressionListener r2 = r1.impressionListener
            r0 = 0
            if (r2 == 0) goto L_0x0025
            com.millennialmedia.InlineAd$ImpressionListener r2 = r1.impressionListener
            r2.cancel()
            r1.impressionListener = r0
        L_0x0025:
            r1.stopRequestTimeoutTimers()
            r1.releaseAdapters()
            com.millennialmedia.InlineAd$5 r2 = new com.millennialmedia.InlineAd$5
            r2.<init>()
            com.millennialmedia.internal.utils.ThreadUtils.postOnUiThread(r2)
            java.lang.String r2 = "idle"
            r1.placementState = r2
            r1.mmAdContainer = r0
            r1.playList = r0
            return
        L_0x003c:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003c }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onUnload(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    public void reportImpression(int i) {
        AdPlacementReporter.setDisplayed(this.currentRequestState.getAdPlacementReporter(), i);
        if (this.impressionListener != null) {
            this.impressionListener.cancel();
            this.impressionListener = null;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAdAdapterLoadFailed(com.millennialmedia.internal.AdPlacement.RequestState r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x0053 }
            boolean r0 = r0.compare(r3)     // Catch:{ all -> 0x0053 }
            if (r0 != 0) goto L_0x0018
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0053 }
            if (r3 == 0) goto L_0x0016
            java.lang.String r3 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x0053 }
            java.lang.String r0 = "onAdAdapterLoadFailed called but request state is not valid"
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0053 }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x0018:
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0053 }
            if (r0 != 0) goto L_0x0042
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0053 }
            if (r3 == 0) goto L_0x0040
            java.lang.String r3 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x0053 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0053 }
            r0.<init>()     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = "onAdAdapterLoadFailed called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = r2.placementState     // Catch:{ all -> 0x0053 }
            r0.append(r1)     // Catch:{ all -> 0x0053 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0053 }
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0053 }
        L_0x0040:
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x0042:
            boolean r0 = r2.doPendingDestroy()     // Catch:{ all -> 0x0053 }
            if (r0 == 0) goto L_0x004a
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x004a:
            java.lang.String r0 = "ad_adapter_load_failed"
            r2.placementState = r0     // Catch:{ all -> 0x0053 }
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            r2.loadAdAdapter(r3)
            return
        L_0x0053:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onAdAdapterLoadFailed(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0057, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRequestSucceeded(final android.view.ViewGroup r3, final com.millennialmedia.internal.AdPlacement.RequestState r4, final com.millennialmedia.internal.adadapters.InlineAdapter r5) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x007b }
            boolean r0 = r0.compare(r4)     // Catch:{ all -> 0x007b }
            if (r0 != 0) goto L_0x0018
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x007b }
            if (r3 == 0) goto L_0x0016
            java.lang.String r3 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x007b }
            java.lang.String r4 = "onRequestSucceeded called but request state is not valid"
            com.millennialmedia.MMLog.d(r3, r4)     // Catch:{ all -> 0x007b }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x007b }
            return
        L_0x0018:
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x007b }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x007b }
            if (r0 != 0) goto L_0x0058
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x007b }
            if (r3 == 0) goto L_0x0040
            java.lang.String r3 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x007b }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x007b }
            r4.<init>()     // Catch:{ all -> 0x007b }
            java.lang.String r5 = "onRequestSucceeded called but placement state is not valid: "
            r4.append(r5)     // Catch:{ all -> 0x007b }
            java.lang.String r5 = r2.placementState     // Catch:{ all -> 0x007b }
            r4.append(r5)     // Catch:{ all -> 0x007b }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x007b }
            com.millennialmedia.MMLog.d(r3, r4)     // Catch:{ all -> 0x007b }
        L_0x0040:
            com.millennialmedia.InlineAd$InlineListener r3 = r2.inlineListener     // Catch:{ all -> 0x007b }
            if (r3 == 0) goto L_0x0056
            java.lang.String r4 = r2.placementState     // Catch:{ all -> 0x007b }
            java.lang.String r5 = "idle"
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x007b }
            if (r4 == 0) goto L_0x0056
            com.millennialmedia.InlineAd$6 r4 = new com.millennialmedia.InlineAd$6     // Catch:{ all -> 0x007b }
            r4.<init>(r3)     // Catch:{ all -> 0x007b }
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r4)     // Catch:{ all -> 0x007b }
        L_0x0056:
            monitor-exit(r2)     // Catch:{ all -> 0x007b }
            return
        L_0x0058:
            boolean r0 = r2.doPendingDestroy()     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x0060
            monitor-exit(r2)     // Catch:{ all -> 0x007b }
            return
        L_0x0060:
            boolean r0 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x006d
            java.lang.String r0 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x007b }
            java.lang.String r1 = "InlineAd in attaching state."
            com.millennialmedia.MMLog.d(r0, r1)     // Catch:{ all -> 0x007b }
        L_0x006d:
            java.lang.String r0 = "attaching"
            r2.placementState = r0     // Catch:{ all -> 0x007b }
            monitor-exit(r2)     // Catch:{ all -> 0x007b }
            com.millennialmedia.InlineAd$7 r0 = new com.millennialmedia.InlineAd$7
            r0.<init>(r3, r4, r5)
            com.millennialmedia.internal.utils.ThreadUtils.postOnUiThread(r0)
            return
        L_0x007b:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x007b }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onRequestSucceeded(android.view.ViewGroup, com.millennialmedia.internal.AdPlacement$RequestState, com.millennialmedia.internal.adadapters.InlineAdapter):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0053, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0080, code lost:
        r4 = r3.inlineListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0082, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0084, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InlineAd.AnonymousClass8(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRequestFailed(com.millennialmedia.internal.AdPlacement.RequestState r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.doPendingDestroy()     // Catch:{ all -> 0x008d }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            return
        L_0x0009:
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r3.currentRequestState     // Catch:{ all -> 0x008d }
            boolean r0 = r0.compareRequest(r4)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0020
            boolean r4 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x008d }
            if (r4 == 0) goto L_0x001e
            java.lang.String r4 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x008d }
            java.lang.String r0 = "onRequestFailed called but request state is not valid"
            com.millennialmedia.MMLog.d(r4, r0)     // Catch:{ all -> 0x008d }
        L_0x001e:
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            return
        L_0x0020:
            java.lang.String r0 = r3.placementState     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0054
            java.lang.String r0 = r3.placementState     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "loading_play_list"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0054
            boolean r4 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x008d }
            if (r4 == 0) goto L_0x0052
            java.lang.String r4 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r0.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "onRequestFailed called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r1 = r3.placementState     // Catch:{ all -> 0x008d }
            r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008d }
            com.millennialmedia.MMLog.d(r4, r0)     // Catch:{ all -> 0x008d }
        L_0x0052:
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            return
        L_0x0054:
            java.lang.String r0 = "load_failed"
            r3.placementState = r0     // Catch:{ all -> 0x008d }
            java.lang.String r0 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r1.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r2 = "Request failed for placement ID: "
            r1.append(r2)     // Catch:{ all -> 0x008d }
            java.lang.String r2 = r3.placementId     // Catch:{ all -> 0x008d }
            r1.append(r2)     // Catch:{ all -> 0x008d }
            java.lang.String r2 = ". If this warning persists please check your placement configuration."
            r1.append(r2)     // Catch:{ all -> 0x008d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008d }
            com.millennialmedia.MMLog.w(r0, r1)     // Catch:{ all -> 0x008d }
            r3.stopRequestTimeoutTimers()     // Catch:{ all -> 0x008d }
            com.millennialmedia.internal.AdPlacementReporter r4 = r4.getAdPlacementReporter()     // Catch:{ all -> 0x008d }
            com.millennialmedia.internal.AdPlacementReporter.reportPlayList(r4)     // Catch:{ all -> 0x008d }
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            com.millennialmedia.InlineAd$InlineListener r4 = r3.inlineListener
            if (r4 == 0) goto L_0x008c
            com.millennialmedia.InlineAd$8 r0 = new com.millennialmedia.InlineAd$8
            r0.<init>(r4)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x008c:
            return
        L_0x008d:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onRequestFailed(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    public void onClicked(AdPlacement.RequestState requestState) {
        MMLog.i(TAG, "Ad clicked");
        reportImpression(2);
        AdPlacementReporter.setClicked(requestState.getAdPlacementReporter());
        final InlineListener inlineListener2 = this.inlineListener;
        if (inlineListener2 != null) {
            ThreadUtils.runOffUiThread(new Runnable() {
                public void run() {
                    inlineListener2.onClicked(InlineAd.this);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InlineAd.TAG, "Ad resizing");
        r1.isResized = true;
        r2 = r1.inlineListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InlineAd.AnonymousClass10(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResize(com.millennialmedia.internal.AdPlacement.RequestState r2, final int r3, final int r4) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x0030 }
            boolean r2 = r0.compare(r2)     // Catch:{ all -> 0x0030 }
            if (r2 != 0) goto L_0x0018
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x0016
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = "onResize called but request state is not valid"
            com.millennialmedia.MMLog.d(r2, r3)     // Catch:{ all -> 0x0030 }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            return
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG
            java.lang.String r0 = "Ad resizing"
            com.millennialmedia.MMLog.i(r2, r0)
            r2 = 1
            r1.isResized = r2
            com.millennialmedia.InlineAd$InlineListener r2 = r1.inlineListener
            if (r2 == 0) goto L_0x002f
            com.millennialmedia.InlineAd$10 r0 = new com.millennialmedia.InlineAd$10
            r0.<init>(r2, r3, r4)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x002f:
            return
        L_0x0030:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onResize(com.millennialmedia.internal.AdPlacement$RequestState, int, int):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        r7 = com.millennialmedia.InlineAd.TAG;
        com.millennialmedia.MMLog.i(r7, "Ad resized, is closed: " + r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002f, code lost:
        if (r10 == false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        r6.isResized = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0034, code lost:
        r2 = r6.inlineListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0038, code lost:
        r3 = r8;
        r4 = r9;
        r5 = r10;
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InlineAd.AnonymousClass11(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResized(com.millennialmedia.internal.AdPlacement.RequestState r7, int r8, int r9, boolean r10) {
        /*
            r6 = this;
            monitor-enter(r6)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r6.currentRequestState     // Catch:{ all -> 0x0046 }
            boolean r7 = r0.compare(r7)     // Catch:{ all -> 0x0046 }
            if (r7 != 0) goto L_0x0018
            boolean r7 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0046 }
            if (r7 == 0) goto L_0x0016
            java.lang.String r7 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x0046 }
            java.lang.String r8 = "onResized called but request state is not valid"
            com.millennialmedia.MMLog.d(r7, r8)     // Catch:{ all -> 0x0046 }
        L_0x0016:
            monitor-exit(r6)     // Catch:{ all -> 0x0046 }
            return
        L_0x0018:
            monitor-exit(r6)     // Catch:{ all -> 0x0046 }
            java.lang.String r7 = com.millennialmedia.InlineAd.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Ad resized, is closed: "
            r0.append(r1)
            r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.millennialmedia.MMLog.i(r7, r0)
            if (r10 == 0) goto L_0x0034
            r7 = 0
            r6.isResized = r7
        L_0x0034:
            com.millennialmedia.InlineAd$InlineListener r2 = r6.inlineListener
            if (r2 == 0) goto L_0x0045
            com.millennialmedia.InlineAd$11 r7 = new com.millennialmedia.InlineAd$11
            r0 = r7
            r1 = r6
            r3 = r8
            r4 = r9
            r5 = r10
            r0.<init>(r2, r3, r4, r5)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r7)
        L_0x0045:
            return
        L_0x0046:
            r7 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0046 }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onResized(com.millennialmedia.internal.AdPlacement$RequestState, int, int, boolean):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InlineAd.TAG, "Ad expanded");
        r1.isExpanded = true;
        r1.isResized = false;
        r2 = r1.inlineListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InlineAd.AnonymousClass12(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onExpanded(com.millennialmedia.internal.AdPlacement.RequestState r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x0033 }
            boolean r2 = r0.compare(r2)     // Catch:{ all -> 0x0033 }
            if (r2 != 0) goto L_0x0018
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0033 }
            if (r2 == 0) goto L_0x0016
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x0033 }
            java.lang.String r0 = "onExpanded called but request state is not valid"
            com.millennialmedia.MMLog.d(r2, r0)     // Catch:{ all -> 0x0033 }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            return
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG
            java.lang.String r0 = "Ad expanded"
            com.millennialmedia.MMLog.i(r2, r0)
            r2 = 1
            r1.isExpanded = r2
            r2 = 0
            r1.isResized = r2
            com.millennialmedia.InlineAd$InlineListener r2 = r1.inlineListener
            if (r2 == 0) goto L_0x0032
            com.millennialmedia.InlineAd$12 r0 = new com.millennialmedia.InlineAd$12
            r0.<init>(r2)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x0032:
            return
        L_0x0033:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onExpanded(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InlineAd.TAG, "Ad collapsed");
        r1.isExpanded = false;
        r2 = r1.inlineListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InlineAd.AnonymousClass13(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCollapsed(com.millennialmedia.internal.AdPlacement.RequestState r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x0030 }
            boolean r2 = r0.compare(r2)     // Catch:{ all -> 0x0030 }
            if (r2 != 0) goto L_0x0018
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x0016
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x0030 }
            java.lang.String r0 = "onCollapsed called but request state is not valid"
            com.millennialmedia.MMLog.d(r2, r0)     // Catch:{ all -> 0x0030 }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            return
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG
            java.lang.String r0 = "Ad collapsed"
            com.millennialmedia.MMLog.i(r2, r0)
            r2 = 0
            r1.isExpanded = r2
            com.millennialmedia.InlineAd$InlineListener r2 = r1.inlineListener
            if (r2 == 0) goto L_0x002f
            com.millennialmedia.InlineAd$13 r0 = new com.millennialmedia.InlineAd$13
            r0.<init>(r2)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x002f:
            return
        L_0x0030:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onCollapsed(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InlineAd.TAG, "Ad left application");
        r2 = r1.inlineListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0022, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InlineAd.AnonymousClass14(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAdLeftApplication(com.millennialmedia.internal.AdPlacement.RequestState r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x002d }
            boolean r2 = r0.compare(r2)     // Catch:{ all -> 0x002d }
            if (r2 != 0) goto L_0x0018
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x002d }
            if (r2 == 0) goto L_0x0016
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x002d }
            java.lang.String r0 = "onAdLeftApplication called but request state is not valid"
            com.millennialmedia.MMLog.d(r2, r0)     // Catch:{ all -> 0x002d }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
            return
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
            java.lang.String r2 = com.millennialmedia.InlineAd.TAG
            java.lang.String r0 = "Ad left application"
            com.millennialmedia.MMLog.i(r2, r0)
            com.millennialmedia.InlineAd$InlineListener r2 = r1.inlineListener
            if (r2 == 0) goto L_0x002c
            com.millennialmedia.InlineAd$14 r0 = new com.millennialmedia.InlineAd$14
            r0.<init>(r2)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x002c:
            return
        L_0x002d:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onAdLeftApplication(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004f, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InlineAd.TAG, "Ad aborted");
        com.millennialmedia.internal.AdPlacementReporter.reportPlayList(r3.getAdPlacementReporter());
        r3 = r2.inlineAbortListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005f, code lost:
        if (r3 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0061, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InlineAd.AnonymousClass15(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void onAborted(com.millennialmedia.internal.AdPlacement.RequestState r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.doPendingDestroy()     // Catch:{ all -> 0x006a }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r2)     // Catch:{ all -> 0x006a }
            return
        L_0x0009:
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x006a }
            boolean r0 = r0.compare(r3)     // Catch:{ all -> 0x006a }
            if (r0 != 0) goto L_0x0020
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x006a }
            if (r3 == 0) goto L_0x001e
            java.lang.String r3 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x006a }
            java.lang.String r0 = "onAborted called but request state is not valid"
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x006a }
        L_0x001e:
            monitor-exit(r2)     // Catch:{ all -> 0x006a }
            return
        L_0x0020:
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x006a }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x006a }
            if (r0 != 0) goto L_0x004a
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x006a }
            if (r3 == 0) goto L_0x0048
            java.lang.String r3 = com.millennialmedia.InlineAd.TAG     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x006a }
            r0.<init>()     // Catch:{ all -> 0x006a }
            java.lang.String r1 = "onAborted called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = r2.placementState     // Catch:{ all -> 0x006a }
            r0.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x006a }
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x006a }
        L_0x0048:
            monitor-exit(r2)     // Catch:{ all -> 0x006a }
            return
        L_0x004a:
            java.lang.String r0 = "aborted"
            r2.placementState = r0     // Catch:{ all -> 0x006a }
            monitor-exit(r2)     // Catch:{ all -> 0x006a }
            java.lang.String r0 = com.millennialmedia.InlineAd.TAG
            java.lang.String r1 = "Ad aborted"
            com.millennialmedia.MMLog.i(r0, r1)
            com.millennialmedia.internal.AdPlacementReporter r3 = r3.getAdPlacementReporter()
            com.millennialmedia.internal.AdPlacementReporter.reportPlayList(r3)
            com.millennialmedia.InlineAd$InlineAbortListener r3 = r2.inlineAbortListener
            if (r3 == 0) goto L_0x0069
            com.millennialmedia.InlineAd$15 r0 = new com.millennialmedia.InlineAd$15
            r0.<init>(r3)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x0069:
            return
        L_0x006a:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x006a }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InlineAd.onAborted(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    public void onAbortFailed() {
        MMLog.i(TAG, "Ad abort failed");
        final InlineAbortListener inlineAbortListener2 = this.inlineAbortListener;
        if (inlineAbortListener2 != null) {
            ThreadUtils.runOffUiThread(new Runnable() {
                public void run() {
                    inlineAbortListener2.onAbortFailed(InlineAd.this);
                }
            });
        }
    }

    private boolean isLoading() {
        return !this.placementState.equals("idle") && !this.placementState.equals("load_failed") && !this.placementState.equals("loaded") && !this.placementState.equals(STATE_LOAD_ABORTED) && !this.placementState.equals("destroyed");
    }

    /* access modifiers changed from: private */
    public static long getImpressionDelay(AdAdapter adAdapter) {
        return adAdapter.getImpressionDelay();
    }

    /* access modifiers changed from: private */
    public static int getMinViewabilityPercentage(AdAdapter adAdapter) {
        return adAdapter.getMinImpressionViewabilityPercentage();
    }

    public Context getContext() {
        if (this.adContainerRef == null || this.adContainerRef.get() == null) {
            return null;
        }
        return this.adContainerRef.get().getContext();
    }

    public Map<String, Object> getAdPlacementMetaDataMap() {
        if (this.inlineAdMetadata == null) {
            return null;
        }
        return this.inlineAdMetadata.toMap(this);
    }

    /* access modifiers changed from: protected */
    public boolean canDestroyNow() {
        return !isLoading();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.inlineListener = null;
        this.inlineAbortListener = null;
        this.incentivizedEventListener = null;
        if (this.impressionListener != null) {
            this.impressionListener.cancel();
            this.impressionListener = null;
        }
        stopRequestTimeoutTimers();
        if (this.refreshRunnable != null) {
            this.refreshRunnable.cancel();
            this.refreshRunnable = null;
        }
        releaseAdapters();
        final WeakReference<ViewGroup> weakReference = this.adContainerRef;
        if (weakReference.get() != null) {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    ViewGroup viewGroup = (ViewGroup) weakReference.get();
                    if (viewGroup != null) {
                        viewGroup.removeAllViews();
                    }
                }
            });
        }
        this.mmAdContainer = null;
        this.inlineAdMetadata = null;
        this.playList = null;
    }

    private void releaseAdapters() {
        if (this.currentInlineAdAdapter != null) {
            this.currentInlineAdAdapter.release();
            this.currentInlineAdAdapter = null;
        }
        if (this.nextInlineAdAdapter != null) {
            this.nextInlineAdAdapter.release();
            this.nextInlineAdAdapter = null;
        }
    }
}
