package com.millennialmedia;

public final class R {

    public static final class attr {
        public static final int adSize = 2130837504;
        public static final int adSizes = 2130837505;
        public static final int adUnitId = 2130837506;
        public static final int circleCrop = 2130837508;
        public static final int font = 2130837515;
        public static final int fontProviderAuthority = 2130837516;
        public static final int fontProviderCerts = 2130837517;
        public static final int fontProviderFetchStrategy = 2130837518;
        public static final int fontProviderFetchTimeout = 2130837519;
        public static final int fontProviderPackage = 2130837520;
        public static final int fontProviderQuery = 2130837521;
        public static final int fontStyle = 2130837522;
        public static final int fontWeight = 2130837523;
        public static final int imageAspectRatio = 2130837524;
        public static final int imageAspectRatioAdjust = 2130837525;
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2130903040;
    }

    public static final class color {
        public static final int mmadsdk_inline_video_controls_background = 2130968593;
        public static final int mmadsdk_lightbox_curtain_background = 2130968594;
        public static final int notification_action_color_filter = 2130968595;
        public static final int notification_icon_bg_color = 2130968596;
        public static final int notification_material_background_media_default_color = 2130968597;
        public static final int primary_text_default_material_dark = 2130968598;
        public static final int ripple_material_light = 2130968599;
        public static final int secondary_text_default_material_dark = 2130968601;
        public static final int secondary_text_default_material_light = 2130968602;
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131034124;
        public static final int compat_button_inset_vertical_material = 2131034125;
        public static final int compat_button_padding_horizontal_material = 2131034126;
        public static final int compat_button_padding_vertical_material = 2131034127;
        public static final int compat_control_corner_material = 2131034128;
        public static final int mmadsdk_ad_button_height = 2131034129;
        public static final int mmadsdk_ad_button_padding_left = 2131034130;
        public static final int mmadsdk_ad_button_width = 2131034131;
        public static final int mmadsdk_adchoices_icon_height = 2131034132;
        public static final int mmadsdk_control_button_height = 2131034133;
        public static final int mmadsdk_control_button_max_width_height = 2131034134;
        public static final int mmadsdk_control_button_min_width_height = 2131034135;
        public static final int mmadsdk_control_button_width = 2131034136;
        public static final int mmadsdk_lightbox_bottom_margin = 2131034137;
        public static final int mmadsdk_lightbox_fullscreen_companion_top_margin = 2131034138;
        public static final int mmadsdk_lightbox_height = 2131034139;
        public static final int mmadsdk_lightbox_minimize_button_height = 2131034140;
        public static final int mmadsdk_lightbox_minimize_button_right_margin = 2131034141;
        public static final int mmadsdk_lightbox_minimize_button_top_margin = 2131034142;
        public static final int mmadsdk_lightbox_minimize_button_width = 2131034143;
        public static final int mmadsdk_lightbox_replay_button_height = 2131034144;
        public static final int mmadsdk_lightbox_replay_button_width = 2131034145;
        public static final int mmadsdk_lightbox_right_margin = 2131034146;
        public static final int mmadsdk_lightbox_top_margin = 2131034147;
        public static final int mmadsdk_lightbox_width = 2131034148;
        public static final int mmadsdk_mraid_resize_close_area_size = 2131034149;
        public static final int notification_action_icon_size = 2131034179;
        public static final int notification_action_text_size = 2131034180;
        public static final int notification_big_circle_margin = 2131034181;
        public static final int notification_content_margin_start = 2131034182;
        public static final int notification_large_icon_height = 2131034183;
        public static final int notification_large_icon_width = 2131034184;
        public static final int notification_main_column_padding_top = 2131034185;
        public static final int notification_media_narrow_margin = 2131034186;
        public static final int notification_right_icon_size = 2131034187;
        public static final int notification_right_side_padding_top = 2131034188;
        public static final int notification_small_icon_background_padding = 2131034189;
        public static final int notification_small_icon_size_as_large = 2131034190;
        public static final int notification_subtext_size = 2131034191;
        public static final int notification_top_pad = 2131034192;
        public static final int notification_top_pad_large_text = 2131034193;
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131099685;
        public static final int mmadsdk_close = 2131099714;
        public static final int mmadsdk_expand_collapse = 2131099715;
        public static final int mmadsdk_fullscreen = 2131099716;
        public static final int mmadsdk_inline_video_progress_bar = 2131099717;
        public static final int mmadsdk_lightbox_down = 2131099718;
        public static final int mmadsdk_lightbox_replay = 2131099719;
        public static final int mmadsdk_mute_unmute = 2131099720;
        public static final int mmadsdk_no_sound = 2131099721;
        public static final int mmadsdk_pause = 2131099722;
        public static final int mmadsdk_play = 2131099723;
        public static final int mmadsdk_play_pause = 2131099724;
        public static final int mmadsdk_sound = 2131099725;
        public static final int mmadsdk_unfullscreen = 2131099726;
        public static final int mmadsdk_vast_close = 2131099727;
        public static final int mmadsdk_vast_opacity = 2131099728;
        public static final int mmadsdk_vast_replay = 2131099729;
        public static final int mmadsdk_vast_skip = 2131099730;
        public static final int notification_action_background = 2131099766;
        public static final int notification_bg = 2131099767;
        public static final int notification_bg_low = 2131099768;
        public static final int notification_bg_low_normal = 2131099769;
        public static final int notification_bg_low_pressed = 2131099770;
        public static final int notification_bg_normal = 2131099771;
        public static final int notification_bg_normal_pressed = 2131099772;
        public static final int notification_icon_background = 2131099773;
        public static final int notification_template_icon_bg = 2131099774;
        public static final int notification_template_icon_low_bg = 2131099775;
        public static final int notification_tile_bg = 2131099776;
        public static final int notify_panel_notification_icon_bg = 2131099777;
    }

    public static final class id {
        public static final int action0 = 2131165187;
        public static final int action_container = 2131165188;
        public static final int action_divider = 2131165189;
        public static final int action_image = 2131165190;
        public static final int action_text = 2131165191;
        public static final int actions = 2131165192;
        public static final int adjust_height = 2131165193;
        public static final int adjust_width = 2131165194;
        public static final int async = 2131165195;
        public static final int blocking = 2131165197;
        public static final int cancel_action = 2131165198;
        public static final int chronometer = 2131165199;
        public static final int end_padder = 2131165222;
        public static final int forever = 2131165223;
        public static final int icon = 2131165224;
        public static final int icon_group = 2131165225;
        public static final int info = 2131165227;
        public static final int italic = 2131165228;
        public static final int line1 = 2131165231;
        public static final int line3 = 2131165232;
        public static final int media_actions = 2131165234;
        public static final int mmadsdk_inline_video_mute_unmute_button = 2131165236;
        public static final int mmadsdk_inline_video_play_pause_button = 2131165237;
        public static final int mmadsdk_light_box_video_view = 2131165238;
        public static final int mmadsdk_vast_video_control_buttons = 2131165239;
        public static final int mmadsdk_vast_video_view = 2131165240;
        public static final int none = 2131165274;
        public static final int normal = 2131165275;
        public static final int notification_background = 2131165276;
        public static final int notification_main_column = 2131165277;
        public static final int notification_main_column_container = 2131165278;
        public static final int right_icon = 2131165285;
        public static final int right_side = 2131165286;
        public static final int status_bar_latest_event_content = 2131165290;
        public static final int text = 2131165291;
        public static final int text2 = 2131165292;
        public static final int time = 2131165293;
        public static final int title = 2131165294;
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131230720;
        public static final int google_play_services_version = 2131230721;
        public static final int status_bar_notification_info_maxnum = 2131230722;
    }

    public static final class layout {
        public static final int notification_action = 2131296276;
        public static final int notification_action_tombstone = 2131296277;
        public static final int notification_media_action = 2131296278;
        public static final int notification_media_cancel_action = 2131296279;
        public static final int notification_template_big_media = 2131296280;
        public static final int notification_template_big_media_custom = 2131296281;
        public static final int notification_template_big_media_narrow = 2131296282;
        public static final int notification_template_big_media_narrow_custom = 2131296283;
        public static final int notification_template_custom_big = 2131296284;
        public static final int notification_template_icon_group = 2131296285;
        public static final int notification_template_lines_media = 2131296286;
        public static final int notification_template_media = 2131296287;
        public static final int notification_template_media_custom = 2131296288;
        public static final int notification_template_part_chronometer = 2131296289;
        public static final int notification_template_part_time = 2131296290;
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131427365;
        public static final int common_google_play_services_enable_text = 2131427366;
        public static final int common_google_play_services_enable_title = 2131427367;
        public static final int common_google_play_services_install_button = 2131427368;
        public static final int common_google_play_services_install_title = 2131427370;
        public static final int common_google_play_services_notification_ticker = 2131427371;
        public static final int common_google_play_services_unknown_issue = 2131427372;
        public static final int common_google_play_services_unsupported_text = 2131427373;
        public static final int common_google_play_services_update_button = 2131427374;
        public static final int common_google_play_services_update_text = 2131427375;
        public static final int common_google_play_services_update_title = 2131427376;
        public static final int common_google_play_services_updating_text = 2131427377;
        public static final int common_open_on_phone = 2131427379;
        public static final int common_signin_button_text = 2131427380;
        public static final int common_signin_button_text_long = 2131427381;
        public static final int mmadsdk_app_name = 2131427401;
        public static final int status_bar_notification_info_overflow = 2131427428;
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131492867;
        public static final int TextAppearance_Compat_Notification_Info = 2131492868;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131492869;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131492870;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131492871;
        public static final int TextAppearance_Compat_Notification_Media = 2131492872;
        public static final int TextAppearance_Compat_Notification_Time = 2131492873;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131492874;
        public static final int TextAppearance_Compat_Notification_Title = 2131492875;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131492876;
        public static final int Theme_IAPTheme = 2131492877;
        public static final int Theme_MMTransparent = 2131492878;
        public static final int Widget_Compat_NotificationActionContainer = 2131492879;
        public static final int Widget_Compat_NotificationActionText = 2131492880;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.miniclip.plagueinc.R.attr.adSize, com.miniclip.plagueinc.R.attr.adSizes, com.miniclip.plagueinc.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] FontFamily = {com.miniclip.plagueinc.R.attr.fontProviderAuthority, com.miniclip.plagueinc.R.attr.fontProviderCerts, com.miniclip.plagueinc.R.attr.fontProviderFetchStrategy, com.miniclip.plagueinc.R.attr.fontProviderFetchTimeout, com.miniclip.plagueinc.R.attr.fontProviderPackage, com.miniclip.plagueinc.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {com.miniclip.plagueinc.R.attr.font, com.miniclip.plagueinc.R.attr.fontStyle, com.miniclip.plagueinc.R.attr.fontWeight};
        public static final int FontFamilyFont_font = 0;
        public static final int FontFamilyFont_fontStyle = 1;
        public static final int FontFamilyFont_fontWeight = 2;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] LoadingImageView = {com.miniclip.plagueinc.R.attr.circleCrop, com.miniclip.plagueinc.R.attr.imageAspectRatio, com.miniclip.plagueinc.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
    }
}
