package com.millennialmedia;

import android.app.Activity;
import android.app.Application;
import android.text.TextUtils;
import com.millennialmedia.internal.ActivityListenerManager;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.UserPrivacy;
import com.millennialmedia.internal.VolumeChangeManager;
import com.millennialmedia.internal.adadapters.AdAdapter;
import com.millennialmedia.internal.adadapters.InlineMediatedAdAdapter;
import com.millennialmedia.internal.adadapters.InterstitialMediatedAdAdapter;
import com.millennialmedia.internal.adadapters.MediatedAdAdapter;
import com.millennialmedia.internal.adadapters.NativeMediatedAdAdapter;
import com.millennialmedia.internal.adcontrollers.AdController;
import com.millennialmedia.internal.playlistserver.PlayListServer;
import com.millennialmedia.internal.playlistserver.PlayListServerAdapter;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.mediation.CustomEventRegistry;
import com.moat.analytics.mobile.aol.MoatAnalytics;
import com.moat.analytics.mobile.aol.MoatOptions;
import java.util.HashMap;
import java.util.Map;

public class MMSDK {
    public static final String IAB_CONSENT_KEY = "iab";
    private static final String TAG = "MMSDK";
    public static final String VERSION = "6.8.1-72925a6";
    private static AppInfo appInfo = null;
    static boolean initialized = false;
    public static boolean locationEnabled = true;
    public static final Map<String, String> registeredPlugins = new HashMap();
    private static TestInfo testInfo;
    private static UserData userData;

    public static void initialize(Application application) throws MMException {
        long currentTimeMillis = System.currentTimeMillis();
        if (application == null) {
            throw new MMInitializationException("Unable to initialize SDK. Please provide a valid Application instance.");
        } else if (initialized) {
            MMLog.i(TAG, "Millennial Media SDK already initialized");
        } else {
            ThreadUtils.initialize();
            EnvironmentUtils.init(application);
            Handshake.initialize();
            VolumeChangeManager.initialize(application);
            ActivityListenerManager.init();
            PlayListServerAdapter.registerPackagedAdapters();
            AdAdapter.registerPackagedAdapters();
            AdController.registerPackagedControllers();
            registerKnownMediationAdapters();
            PlayListServerAdapter.registerPackagedPlayListItemTypes();
            initializeMoat(application);
            Handshake.request(true);
            AdPlacementReporter.init();
            UserPrivacy.geoIpCheck(true);
            initialized = true;
            if (MMLog.isDebugEnabled()) {
                String str = TAG;
                MMLog.d(str, "SDK Initialization completed in " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
            }
        }
    }

    private static void initializeMoat(Application application) {
        MoatOptions moatOptions = new MoatOptions();
        if (MMLog.isDebugEnabled()) {
            moatOptions.loggingEnabled = true;
            MMLog.d(TAG, "Moat logging enabled");
        }
        MoatAnalytics.getInstance().start(moatOptions, application);
    }

    private static void registerKnownMediationAdapters() {
        registerMediatedAdAdapter(InlineAd.class, InlineMediatedAdAdapter.class);
        registerMediatedAdAdapter(InterstitialAd.class, InterstitialMediatedAdAdapter.class);
        registerMediatedAdAdapter(NativeAd.class, NativeMediatedAdAdapter.class);
        CustomEventRegistry.register("ADCOLONY", InterstitialAd.class, "com.millennialmedia.mediation.AdColonyCustomEventInterstitial");
        CustomEventRegistry.register("ADMOB", InlineAd.class, "com.millennialmedia.mediation.AdMobCustomEventBanner");
        CustomEventRegistry.register("ADMOB", InterstitialAd.class, "com.millennialmedia.mediation.AdMobCustomEventInterstitial");
        CustomEventRegistry.register("FACEBOOK", InlineAd.class, "com.millennialmedia.mediation.FacebookCustomEventBanner");
        CustomEventRegistry.register("FACEBOOK", InterstitialAd.class, "com.millennialmedia.mediation.FacebookCustomEventInterstitial");
        CustomEventRegistry.register("FACEBOOK", NativeAd.class, "com.millennialmedia.mediation.FacebookCustomEventNative");
        CustomEventRegistry.register("INMOBI", InlineAd.class, "com.millennialmedia.mediation.InMobiCustomEventBanner");
        CustomEventRegistry.register("INMOBI", InterstitialAd.class, "com.millennialmedia.mediation.InMobiCustomEventInterstitial");
        CustomEventRegistry.register("MOPUB", InlineAd.class, "com.millennialmedia.mediation.MoPubCustomEventBanner");
        CustomEventRegistry.register("MOPUB", NativeAd.class, "com.millennialmedia.mediation.MoPubCustomEventNative");
        CustomEventRegistry.register("MOPUB", InterstitialAd.class, "com.millennialmedia.mediation.MoPubCustomEventInterstitial");
        CustomEventRegistry.register("CHARTBOOST", InterstitialAd.class, "com.millennialmedia.mediation.ChartboostCustomEventInterstitial");
    }

    @Deprecated
    public static void initialize(Activity activity) {
        if (activity == null) {
            throw new IllegalStateException("Unable to initialize SDK, specified activity is null");
        }
        try {
            initialize(activity.getApplication());
        } catch (MMException e) {
            throw new IllegalStateException(e);
        }
    }

    @Deprecated
    public static void initialize(Activity activity, ActivityListenerManager.LifecycleState lifecycleState) {
        initialize(activity);
        ActivityListenerManager.setInitialStateForUnknownActivity(activity.hashCode(), lifecycleState);
    }

    public static boolean isInitialized() {
        return initialized;
    }

    public static void registerPlayListServerAdapter(PlayListServerAdapter playListServerAdapter) {
        PlayListServerAdapter.registerAdapter(playListServerAdapter);
    }

    public static void setActiveAdServerAdapter(Class<? extends PlayListServerAdapter> cls) {
        PlayListServer.setActivePlayListServerAdapter(cls);
    }

    public static void registerAdAdapter(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        AdAdapter.registerAdapter(cls, cls2, cls3);
    }

    public static void registerAdController(AdController adController) {
        AdController.registerController(adController);
    }

    public static void registerMediatedAdAdapter(Class<? extends AdPlacement> cls, Class<? extends MediatedAdAdapter> cls2) {
        AdAdapter.registerMediatedAdAdapter(cls, cls2);
    }

    public static boolean registerPlugin(String str, String str2) throws MMException {
        if (!initialized) {
            throw new MMInitializationException("Unable to register plugin, SDK must be initialized first");
        } else if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            MMLog.e(TAG, "Unable to register plugin, neither id or version can be null or empty");
            return false;
        } else {
            registeredPlugins.put(str, str2);
            if (!MMLog.isDebugEnabled()) {
                return true;
            }
            String str3 = TAG;
            MMLog.d(str3, "Registered plugin with ID <" + str + "> and version <" + str2 + ">");
            return true;
        }
    }

    public static void setUserData(UserData userData2) throws MMException {
        if (!initialized) {
            throw new MMInitializationException("Unable to set user data, SDK must be initialized first");
        }
        userData = userData2;
    }

    public static UserData getUserData() {
        return userData;
    }

    public static void setAppInfo(AppInfo appInfo2) throws MMException {
        if (!initialized) {
            throw new MMInitializationException("Unable to set app info, SDK must be initialized first");
        }
        appInfo = appInfo2;
    }

    public static AppInfo getAppInfo() {
        return appInfo;
    }

    public static void setLocationEnabled(boolean z) throws MMException {
        if (!initialized) {
            throw new MMInitializationException("Unable to set location state, SDK must be initialized first");
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Setting location enabled: " + z);
        }
        locationEnabled = z;
    }

    public static void setConsentRequired(boolean z) {
        UserPrivacy.setConsentRequired(z);
    }

    public static void setConsentData(String str, String str2) {
        UserPrivacy.setConsentData(str, str2);
    }

    public static void clearConsentData() {
        UserPrivacy.clearConsentData();
    }

    public static void setTestInfo(TestInfo testInfo2) throws MMException {
        if (!initialized) {
            throw new MMInitializationException("Unable to set test info, SDK must be initialized first");
        }
        testInfo = testInfo2;
    }

    public static TestInfo getTestInfo() {
        return testInfo;
    }
}
