package com.millennialmedia;

public class MMException extends Exception {
    public MMException(String str) {
        super(str);
    }

    public MMException(String str, Throwable th) {
        super(str, th);
    }
}
