package com.millennialmedia;

import java.util.Date;

public class UserData {
    private static final String TAG = "com.millennialmedia.UserData";
    private Integer age;
    private Integer children;
    private String country;
    private String dma;
    private Date dob;
    private String education;
    private String ethnicity;
    private String gender;
    private Integer income;
    private String keywords;
    private String marital;
    private String politics;
    private String postalCode;
    private String state;

    public enum Education {
        HIGH_SCHOOL("highschool"),
        SOME_COLLEGE("somecollege"),
        ASSOCIATE("associate"),
        BACHELOR("bachelor"),
        MASTERS("masters"),
        PHD("phd"),
        PROFESSIONAL("professional");
        
        public final String value;

        private Education(String str) {
            this.value = str;
        }
    }

    public enum Ethnicity {
        HISPANIC("hispanic"),
        BLACK("africanamerican"),
        ASIAN("asian"),
        INDIAN("indian"),
        MIDDLE_EASTERN("middleeastern"),
        NATIVE_AMERICAN("nativeamerican"),
        PACIFIC_ISLANDER("pacificislander"),
        WHITE("white"),
        OTHER("other");
        
        public final String value;

        private Ethnicity(String str) {
            this.value = str;
        }
    }

    public enum Gender {
        MALE("M"),
        FEMALE("F"),
        UNKNOWN("O");
        
        public final String value;

        private Gender(String str) {
            this.value = str;
        }
    }

    public enum Marital {
        SINGLE("S"),
        MARRIED("M"),
        DIVORCED("D"),
        RELATIONSHIP("O");
        
        public final String value;

        private Marital(String str) {
            this.value = str;
        }
    }

    public enum Politics {
        REPUBLICAN("republican"),
        DEMOCRAT("democrat"),
        CONSERVATIVE("conservative"),
        MODERATE("moderate"),
        LIBERAL("liberal"),
        INDEPENDENT("independent"),
        OTHER("other");
        
        public final String value;

        private Politics(String str) {
            this.value = str;
        }
    }

    public UserData setAge(int i) {
        if (i < 0 || i > 150) {
            MMLog.e(TAG, "Age must be at least 0 and no greater than 150");
        } else {
            this.age = Integer.valueOf(i);
        }
        return this;
    }

    public Integer getAge() {
        return this.age;
    }

    public UserData setChildren(int i) {
        if (i < 0) {
            MMLog.e(TAG, "Number of children must be greater than or equal to zero");
        } else {
            this.children = Integer.valueOf(i);
        }
        return this;
    }

    public Integer getChildren() {
        return this.children;
    }

    public UserData setEducation(Education education2) {
        this.education = education2.value;
        return this;
    }

    public String getEducation() {
        return this.education;
    }

    public UserData setEthnicity(Ethnicity ethnicity2) {
        this.ethnicity = ethnicity2.value;
        return this;
    }

    public String getEthnicity() {
        return this.ethnicity;
    }

    public UserData setGender(Gender gender2) {
        this.gender = gender2.value;
        return this;
    }

    public String getGender() {
        return this.gender;
    }

    public UserData setIncome(int i) {
        this.income = Integer.valueOf(i);
        return this;
    }

    public Integer getIncome() {
        return this.income;
    }

    public UserData setKeywords(String str) {
        this.keywords = str;
        return this;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public UserData setMarital(Marital marital2) {
        this.marital = marital2.value;
        return this;
    }

    public String getMarital() {
        return this.marital;
    }

    public UserData setPolitics(Politics politics2) {
        this.politics = politics2.value;
        return this;
    }

    public String getPolitics() {
        return this.politics;
    }

    public UserData setPostalCode(String str) {
        this.postalCode = str;
        return this;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public UserData setDob(Date date) {
        this.dob = date;
        return this;
    }

    public Date getDob() {
        return this.dob;
    }

    public UserData setState(String str) {
        this.state = str;
        return this;
    }

    public String getState() {
        return this.state;
    }

    public UserData setCountry(String str) {
        this.country = str;
        return this;
    }

    public String getCountry() {
        return this.country;
    }

    public UserData setDma(String str) {
        this.dma = str;
        return this;
    }

    public String getDma() {
        return this.dma;
    }
}
