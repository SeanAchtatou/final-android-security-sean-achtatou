package com.millennialmedia.mediation;

import android.content.Context;
import android.os.Bundle;

public interface CustomEventInterstitial extends CustomEvent {

    public interface CustomEventInterstitialListener {
        void onAdLeftApplication();

        void onClicked();

        void onClosed();

        void onExpired();

        void onLoadFailed(ErrorCode errorCode);

        void onLoaded();

        void onShowFailed(ErrorCode errorCode);

        void onShown();
    }

    void destroy();

    void loadInterstitial(Context context, CustomEventInterstitialListener customEventInterstitialListener, Bundle bundle);

    void showInterstitial(Context context, Bundle bundle);
}
