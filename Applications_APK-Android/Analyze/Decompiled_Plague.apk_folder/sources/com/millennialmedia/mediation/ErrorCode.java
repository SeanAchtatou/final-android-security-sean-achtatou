package com.millennialmedia.mediation;

public enum ErrorCode {
    CONFIGURATION_ERROR,
    INTERNAL_ERROR,
    NO_FILL,
    NETWORK_ERROR,
    SERVER_ERROR,
    VIDEO_DOWNLOAD_ERROR,
    VIDEO_PLAYBACK_ERROR,
    UNKNOWN_ERROR
}
