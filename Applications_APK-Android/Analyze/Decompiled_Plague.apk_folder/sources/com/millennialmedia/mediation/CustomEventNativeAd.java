package com.millennialmedia.mediation;

import java.util.List;

public class CustomEventNativeAd {
    private TextComponent body;
    private TextComponent callToAction;
    private TextComponent disclaimer;
    private ImageComponent iconUrl;
    private ImageComponent mainImageUrl;
    private TextComponent rating;
    private TextComponent title;

    public static abstract class Component {
        public String clickThroughUrl;
        public List<String> clickTrackingUrls;
    }

    public static class ImageComponent extends Component {
        public String imageUrl;
    }

    public static class TextComponent extends Component {
        public String value;
    }

    public TextComponent getRating() {
        return this.rating;
    }

    public void setRating(TextComponent textComponent) {
        this.rating = textComponent;
    }

    public TextComponent getCallToAction() {
        return this.callToAction;
    }

    public void setCallToAction(TextComponent textComponent) {
        this.callToAction = textComponent;
    }

    public ImageComponent getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(ImageComponent imageComponent) {
        this.iconUrl = imageComponent;
    }

    public ImageComponent getMainImageUrl() {
        return this.mainImageUrl;
    }

    public void setMainImageUrl(ImageComponent imageComponent) {
        this.mainImageUrl = imageComponent;
    }

    public TextComponent getTitle() {
        return this.title;
    }

    public void setTitle(TextComponent textComponent) {
        this.title = textComponent;
    }

    public TextComponent getDisclaimer() {
        return this.disclaimer;
    }

    public void setDisclaimer(TextComponent textComponent) {
        this.disclaimer = textComponent;
    }

    public TextComponent getBody() {
        return this.body;
    }

    public void setBody(TextComponent textComponent) {
        this.body = textComponent;
    }
}
