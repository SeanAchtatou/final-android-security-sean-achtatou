package com.millennialmedia.mediation;

public interface CustomEvent {
    public static final String PLACEMENT_ID_KEY = "PLACEMENT_ID";
    public static final String SITE_ID_KEY = "SITE_ID";
}
