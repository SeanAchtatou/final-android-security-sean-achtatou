package com.millennialmedia;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.millennialmedia";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "production";
    public static final String MMSDK_VERSION = "6.8.1-72925a6";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "6.8.1";
}
