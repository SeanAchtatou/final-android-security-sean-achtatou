package com.millennialmedia;

public interface BidRequestListener {
    void onRequestFailed(BidRequestErrorStatus bidRequestErrorStatus);

    void onRequestSucceeded(String str);
}
