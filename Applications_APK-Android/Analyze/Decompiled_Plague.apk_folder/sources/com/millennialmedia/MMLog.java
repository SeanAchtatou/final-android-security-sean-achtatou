package com.millennialmedia;

import android.util.Log;

public class MMLog {
    private static final String TAG_PREFIX = "MMSDK-";
    public static int logLevel = 4;
    private static LogListener logListener;

    public interface LogListener {
        void onLogMessage(int i, String str, String str2);
    }

    public static void setLogLevel(int i) {
        logLevel = i;
    }

    public static boolean isVerboseEnabled() {
        return logLevel <= 2;
    }

    public static void v(String str, String str2) {
        if (logLevel <= 2) {
            vInternal(str, str2);
        }
    }

    public static void v(String str, String str2, Throwable th) {
        if (logLevel <= 2) {
            vInternal(str, str2 + ": " + Log.getStackTraceString(th));
        }
    }

    private static void vInternal(String str, String str2) {
        Log.v(getFullTag(str), str2);
        if (logListener != null) {
            logListener.onLogMessage(2, str, str2);
        }
    }

    public static boolean isDebugEnabled() {
        return logLevel <= 3;
    }

    public static void d(String str, String str2) {
        if (logLevel <= 3) {
            dInternal(str, str2);
        }
    }

    public static void d(String str, String str2, Throwable th) {
        if (logLevel <= 3) {
            dInternal(str, str2 + ": " + Log.getStackTraceString(th));
        }
    }

    private static void dInternal(String str, String str2) {
        Log.d(getFullTag(str), str2);
        if (logListener != null) {
            logListener.onLogMessage(3, str, str2);
        }
    }

    public static void i(String str, String str2) {
        if (logLevel <= 4) {
            iInternal(str, str2);
        }
    }

    public static void i(String str, String str2, Throwable th) {
        if (logLevel <= 4) {
            iInternal(str, str2 + ": " + Log.getStackTraceString(th));
        }
    }

    private static void iInternal(String str, String str2) {
        Log.i(getFullTag(str), str2);
        if (logListener != null) {
            logListener.onLogMessage(4, str, str2);
        }
    }

    public static void w(String str, String str2) {
        if (logLevel <= 5) {
            wInternal(str, str2);
        }
    }

    public static void w(String str, String str2, Throwable th) {
        if (logLevel <= 5) {
            wInternal(str, str2 + ": " + Log.getStackTraceString(th));
        }
    }

    private static void wInternal(String str, String str2) {
        Log.w(getFullTag(str), str2);
        if (logListener != null) {
            logListener.onLogMessage(5, str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (logLevel <= 6) {
            eInternal(str, str2);
        }
    }

    public static void e(String str, String str2, Throwable th) {
        if (logLevel <= 6) {
            eInternal(str, str2 + ": " + Log.getStackTraceString(th));
        }
    }

    private static void eInternal(String str, String str2) {
        Log.e(getFullTag(str), str2);
        if (logListener != null) {
            logListener.onLogMessage(6, str, str2);
        }
    }

    private static String getFullTag(String str) {
        return TAG_PREFIX + str + " <" + Thread.currentThread().getId() + ":" + System.currentTimeMillis() + ">";
    }

    public static void setListener(LogListener logListener2) {
        logListener = logListener2;
    }
}
