package com.millennialmedia;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementMetadata;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.ErrorStatus;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.AdAdapter;
import com.millennialmedia.internal.adadapters.NativeAdapter;
import com.millennialmedia.internal.adadapters.NativeMediatedAdAdapter;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.TrackingEvent;
import com.millennialmedia.internal.utils.Utils;
import com.millennialmedia.internal.utils.ViewUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class NativeAd extends AdPlacement {
    public static final String COMPONENT_ID_BODY = "body";
    public static final String COMPONENT_ID_CALL_TO_ACTION = "callToAction";
    public static final String COMPONENT_ID_DISCLAIMER = "disclaimer";
    public static final String COMPONENT_ID_ICON_IMAGE = "iconImage";
    public static final String COMPONENT_ID_MAIN_IMAGE = "mainImage";
    public static final String COMPONENT_ID_RATING = "rating";
    public static final String COMPONENT_ID_TITLE = "title";
    private static final String DEFAULT_DISCLAIMER_TEXT = "Sponsored";
    private static final int MAX_COMP_INSTANCE_ID = 900;
    private static final int MIN_COMP_INSTANCE_ID = 1;
    public static final String NATIVE_TYPE_INLINE = "inline";
    protected static final String STATE_EXPIRED = "expired";
    /* access modifiers changed from: private */
    public static final String TAG = "NativeAd";
    private Map<String, Set<Integer>> accessedComponentIndices = new HashMap();
    private ThreadUtils.ScheduledRunnable adAdapterRequestTimeoutRunnable;
    private List<NativeAdapter.TextComponentInfo> bodyInfoList;
    private List<NativeAdapter.TextComponentInfo> callToActionInfoList;
    private WeakReference<Context> contextRef;
    /* access modifiers changed from: private */
    public volatile NativeAdapter currentNativeAdAdapter;
    private List<NativeAdapter.TextComponentInfo> disclaimerInfoList;
    /* access modifiers changed from: private */
    public ThreadUtils.ScheduledRunnable expirationRunnable;
    private List<NativeAdapter.ImageComponentInfo> iconImageInfoList;
    /* access modifiers changed from: private */
    public boolean impressionReported = false;
    private ImpressionReporter impressionReporter;
    public Map<String, List<Object>> loadedComponents = new HashMap();
    private List<NativeAdapter.ImageComponentInfo> mainImageInfoList;
    private NativeAdMetadata nativeAdMetadata;
    /* access modifiers changed from: private */
    public NativeListener nativeListener;
    private Handshake.NativeTypeDefinition nativeTypeDefinition;
    /* access modifiers changed from: private */
    public volatile NativeAdapter nextNativeAdAdapter;
    private ThreadUtils.ScheduledRunnable placementRequestTimeoutRunnable;
    private List<NativeAdapter.TextComponentInfo> ratingInfoList;
    /* access modifiers changed from: private */
    public List<String> requestedNativeTypes;
    private List<NativeAdapter.TextComponentInfo> titleInfoList;
    private boolean usingManagedLayout = false;

    public enum ComponentName {
        TITLE,
        BODY,
        ICON_IMAGE,
        MAIN_IMAGE,
        CALL_TO_ACTION,
        RATING,
        DISCLAIMER
    }

    public interface NativeListener {
        void onAdLeftApplication(NativeAd nativeAd);

        void onClicked(NativeAd nativeAd, ComponentName componentName, int i);

        void onExpired(NativeAd nativeAd);

        void onLoadFailed(NativeAd nativeAd, NativeErrorStatus nativeErrorStatus);

        void onLoaded(NativeAd nativeAd);
    }

    public static class NativeErrorStatus extends ErrorStatus {
        public static final int EXPIRED = 301;

        static {
            errorCodes.put(Integer.valueOf((int) EXPIRED), "EXPIRED");
        }

        public NativeErrorStatus(int i) {
            super(i);
        }

        public NativeErrorStatus(int i, String str, Throwable th) {
            super(i, str, th);
        }
    }

    public static class NativeAdMetadata extends AdPlacementMetadata<NativeAdMetadata> {
        private static final String PLACEMENT_TYPE_NATIVE = "native";

        public NativeAdMetadata() {
            super("native");
        }

        /* access modifiers changed from: package-private */
        public Map<String, Object> toMap(NativeAd nativeAd) {
            Map<String, Object> map = super.toMap((AdPlacement) nativeAd);
            Utils.injectIfNotNull(map, AdPlacementMetadata.METADATA_KEY_NATIVE_TYPES, nativeAd.requestedNativeTypes);
            return map;
        }
    }

    static class ImpressionReporter implements ViewUtils.ViewabilityListener {
        final long impressionDelay;
        volatile ThreadUtils.ScheduledRunnable impressionTimerRunnable;
        final View layout;
        final NativeAd nativeAd;
        ViewUtils.ViewabilityWatcher viewabilityWatcher;

        ImpressionReporter(NativeAd nativeAd2, View view, long j) {
            this.nativeAd = nativeAd2;
            this.layout = view;
            this.impressionDelay = j;
        }

        /* access modifiers changed from: protected */
        public void start() {
            if (this.viewabilityWatcher == null) {
                this.viewabilityWatcher = new ViewUtils.ViewabilityWatcher(this.layout, this);
            }
            this.viewabilityWatcher.setMinViewabilityPercent(50);
            this.viewabilityWatcher.startWatching();
        }

        public void cancel() {
            synchronized (this) {
                cancelImpressionTimer();
                if (this.viewabilityWatcher != null) {
                    MMLog.d(NativeAd.TAG, "Stopping previous impression viewability watcher");
                    this.viewabilityWatcher.stopWatching();
                }
            }
        }

        public void onViewableChanged(boolean z) {
            synchronized (this) {
                if (z) {
                    try {
                        if (!wasImpressionTimerPrepared() && !this.nativeAd.impressionReported) {
                            prepareImpressionTimer(this.nativeAd);
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                if (!z) {
                    cancelImpressionTimer();
                }
            }
        }

        private void prepareImpressionTimer(final NativeAd nativeAd2) {
            this.impressionTimerRunnable = ThreadUtils.runOnWorkerThreadDelayed(new Runnable() {
                public void run() {
                    MMLog.d(NativeAd.TAG, "Preparing impression timer runnable");
                    synchronized (ImpressionReporter.this) {
                        if (ImpressionReporter.this.viewabilityWatcher.viewable && !nativeAd2.isDestroyed()) {
                            int i = 1;
                            if (ImpressionReporter.this.impressionDelay == 0) {
                                i = 0;
                            }
                            nativeAd2.reportImpression(nativeAd2.currentRequestState.getAdPlacementReporter(), i);
                        }
                        ImpressionReporter.this.cancel();
                    }
                }
            }, this.impressionDelay);
        }

        private void cancelImpressionTimer() {
            if (this.impressionTimerRunnable != null) {
                MMLog.d(NativeAd.TAG, "Cancelling previous impression timer runnable");
                this.impressionTimerRunnable.cancel();
                this.impressionTimerRunnable = null;
            }
        }

        private boolean wasImpressionTimerPrepared() {
            return this.impressionTimerRunnable != null;
        }
    }

    private static class ExpirationRunnable implements Runnable {
        WeakReference<NativeAd> nativeAdRef;
        WeakReference<AdPlacement.RequestState> requestStateRef;

        ExpirationRunnable(NativeAd nativeAd, AdPlacement.RequestState requestState) {
            this.nativeAdRef = new WeakReference<>(nativeAd);
            this.requestStateRef = new WeakReference<>(requestState);
        }

        public void run() {
            NativeAd nativeAd = this.nativeAdRef.get();
            if (nativeAd == null) {
                MMLog.e(NativeAd.TAG, "NativeAd instance has been destroyed, aborting expiration state change");
                return;
            }
            ThreadUtils.ScheduledRunnable unused = nativeAd.expirationRunnable = null;
            AdPlacement.RequestState requestState = this.requestStateRef.get();
            if (requestState == null) {
                MMLog.e(NativeAd.TAG, "No valid RequestStateComponents is available, unable to trigger expired state change");
            } else {
                nativeAd.onExpired(requestState);
            }
        }
    }

    public static NativeAd createInstance(String str, String str2) throws MMException {
        return createInstance(str, new String[]{str2});
    }

    public static NativeAd createInstance(String str, String[] strArr) throws MMException {
        if (MMSDK.isInitialized()) {
            return new NativeAd(str, strArr);
        }
        throw new MMInitializationException("Unable to create instance, SDK must be initialized first");
    }

    private NativeAd(String str, String[] strArr) throws MMException {
        super(str);
        int i = 0;
        if (strArr == null || strArr.length == 0 || strArr[0] == null || strArr[0].isEmpty()) {
            throw new MMException("Unable to create native ad, nativeTypes is required");
        }
        ArrayList arrayList = new ArrayList();
        Map<String, Handshake.NativeTypeDefinition> nativeTypeDefinitions = Handshake.getNativeTypeDefinitions();
        int length = strArr.length;
        while (i < length) {
            String str2 = strArr[i];
            String str3 = null;
            Iterator<Map.Entry<String, Handshake.NativeTypeDefinition>> it = nativeTypeDefinitions.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                String str4 = (String) next.getKey();
                if (str2.equals(((Handshake.NativeTypeDefinition) next.getValue()).typeName)) {
                    str3 = str4;
                    break;
                }
            }
            if (str3 != null) {
                arrayList.add(str3);
                i++;
            } else {
                throw new MMException("Unable to load native ad, specified native type <" + str2 + "> is not recognized");
            }
        }
        this.requestedNativeTypes = arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0062, code lost:
        r4.playList = null;
        r4.accessedComponentIndices.clear();
        r4.loadedComponents.clear();
        r4.usingManagedLayout = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
        if (r6 != null) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0074, code lost:
        r6 = new com.millennialmedia.NativeAd.NativeAdMetadata();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0079, code lost:
        r5 = getRequestState();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007f, code lost:
        if (r4.placementRequestTimeoutRunnable == null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0081, code lost:
        r4.placementRequestTimeoutRunnable.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0086, code lost:
        r0 = com.millennialmedia.internal.Handshake.getNativeTimeout();
        r4.placementRequestTimeoutRunnable = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(new com.millennialmedia.NativeAd.AnonymousClass1(r4), (long) r0);
        r1 = r6.getImpressionGroup();
        com.millennialmedia.internal.playlistserver.PlayListServer.loadPlayList(r6.toMap(r4), new com.millennialmedia.NativeAd.AnonymousClass2(r4), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a6, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void load(android.content.Context r5, com.millennialmedia.NativeAd.NativeAdMetadata r6) throws com.millennialmedia.MMException {
        /*
            r4 = this;
            boolean r0 = r4.isDestroyed()
            if (r0 == 0) goto L_0x0007
            return
        L_0x0007:
            java.lang.String r0 = com.millennialmedia.NativeAd.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Loading playlist for placement ID: "
            r1.append(r2)
            java.lang.String r2 = r4.placementId
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.millennialmedia.MMLog.i(r0, r1)
            if (r5 != 0) goto L_0x0029
            com.millennialmedia.MMException r5 = new com.millennialmedia.MMException
            java.lang.String r6 = "Unable to load native, specified context cannot be null"
            r5.<init>(r6)
            throw r5
        L_0x0029:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r5)
            r4.contextRef = r0
            r4.nativeAdMetadata = r6
            monitor-enter(r4)
            java.lang.String r5 = r4.placementState     // Catch:{ all -> 0x00a7 }
            java.lang.String r0 = "idle"
            boolean r5 = r5.equals(r0)     // Catch:{ all -> 0x00a7 }
            if (r5 != 0) goto L_0x005d
            java.lang.String r5 = r4.placementState     // Catch:{ all -> 0x00a7 }
            java.lang.String r0 = "load_failed"
            boolean r5 = r5.equals(r0)     // Catch:{ all -> 0x00a7 }
            if (r5 != 0) goto L_0x005d
            java.lang.String r5 = r4.placementState     // Catch:{ all -> 0x00a7 }
            java.lang.String r0 = "loaded"
            boolean r5 = r5.equals(r0)     // Catch:{ all -> 0x00a7 }
            if (r5 != 0) goto L_0x005d
            java.lang.String r5 = r4.placementState     // Catch:{ all -> 0x00a7 }
            java.lang.String r0 = "expired"
            boolean r5 = r5.equals(r0)     // Catch:{ all -> 0x00a7 }
            if (r5 != 0) goto L_0x005d
            monitor-exit(r4)     // Catch:{ all -> 0x00a7 }
            return
        L_0x005d:
            java.lang.String r5 = "loading_play_list"
            r4.placementState = r5     // Catch:{ all -> 0x00a7 }
            monitor-exit(r4)     // Catch:{ all -> 0x00a7 }
            r5 = 0
            r4.playList = r5
            java.util.Map<java.lang.String, java.util.Set<java.lang.Integer>> r5 = r4.accessedComponentIndices
            r5.clear()
            java.util.Map<java.lang.String, java.util.List<java.lang.Object>> r5 = r4.loadedComponents
            r5.clear()
            r5 = 0
            r4.usingManagedLayout = r5
            if (r6 != 0) goto L_0x0079
            com.millennialmedia.NativeAd$NativeAdMetadata r6 = new com.millennialmedia.NativeAd$NativeAdMetadata
            r6.<init>()
        L_0x0079:
            com.millennialmedia.internal.AdPlacement$RequestState r5 = r4.getRequestState()
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r0 = r4.placementRequestTimeoutRunnable
            if (r0 == 0) goto L_0x0086
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r0 = r4.placementRequestTimeoutRunnable
            r0.cancel()
        L_0x0086:
            int r0 = com.millennialmedia.internal.Handshake.getNativeTimeout()
            com.millennialmedia.NativeAd$1 r1 = new com.millennialmedia.NativeAd$1
            r1.<init>(r5)
            long r2 = (long) r0
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r1 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r1, r2)
            r4.placementRequestTimeoutRunnable = r1
            java.lang.String r1 = r6.getImpressionGroup()
            java.util.Map r6 = r6.toMap(r4)
            com.millennialmedia.NativeAd$2 r2 = new com.millennialmedia.NativeAd$2
            r2.<init>(r5, r1)
            com.millennialmedia.internal.playlistserver.PlayListServer.loadPlayList(r6, r2, r0)
            return
        L_0x00a7:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00a7 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.NativeAd.load(android.content.Context, com.millennialmedia.NativeAd$NativeAdMetadata):void");
    }

    public CreativeInfo getCreativeInfo() {
        if (this.currentNativeAdAdapter != null) {
            return this.currentNativeAdAdapter.getCreativeInfo();
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003a, code lost:
        if (r5.playList.hasNext() != false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
        if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0042, code lost:
        com.millennialmedia.MMLog.d(com.millennialmedia.NativeAd.TAG, "Unable to find ad adapter in play list");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        onLoadFailed(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
        r6 = com.millennialmedia.internal.AdPlacementReporter.getPlayListItemReporter(r6.getAdPlacementReporter());
        r5.nextNativeAdAdapter = (com.millennialmedia.internal.adadapters.NativeAdapter) r5.playList.getNextAdAdapter(r5, r6);
        r1 = r5.contextRef.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0069, code lost:
        if (r5.nextNativeAdAdapter == null) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006b, code lost:
        if (r1 == null) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006d, code lost:
        r1 = r5.nextNativeAdAdapter.requestTimeout;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0071, code lost:
        if (r1 <= 0) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0075, code lost:
        if (r5.adAdapterRequestTimeoutRunnable == null) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0077, code lost:
        r5.adAdapterRequestTimeoutRunnable.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007c, code lost:
        r5.adAdapterRequestTimeoutRunnable = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(new com.millennialmedia.NativeAd.AnonymousClass3(r5), (long) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0088, code lost:
        r5.nextNativeAdAdapter.init(new com.millennialmedia.NativeAd.AnonymousClass4(r5));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0093, code lost:
        com.millennialmedia.internal.AdPlacementReporter.reportPlayListItem(r0.getAdPlacementReporter(), r6);
        onAdAdapterLoadFailed(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadAdAdapter(com.millennialmedia.internal.AdPlacement.RequestState r6) {
        /*
            r5 = this;
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r6.copy()
            monitor-enter(r5)
            boolean r1 = r5.doPendingDestroy()     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x000d
            monitor-exit(r5)     // Catch:{ all -> 0x00a0 }
            return
        L_0x000d:
            com.millennialmedia.internal.AdPlacement$RequestState r1 = r5.currentRequestState     // Catch:{ all -> 0x00a0 }
            boolean r1 = r1.compareRequest(r0)     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x009e
            java.lang.String r1 = r5.placementState     // Catch:{ all -> 0x00a0 }
            java.lang.String r2 = "play_list_loaded"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x00a0 }
            if (r1 != 0) goto L_0x002a
            java.lang.String r1 = r5.placementState     // Catch:{ all -> 0x00a0 }
            java.lang.String r2 = "ad_adapter_load_failed"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x00a0 }
            if (r1 != 0) goto L_0x002a
            goto L_0x009e
        L_0x002a:
            java.lang.String r1 = "loading_ad_adapter"
            r5.placementState = r1     // Catch:{ all -> 0x00a0 }
            r0.getItemHash()     // Catch:{ all -> 0x00a0 }
            r5.currentRequestState = r0     // Catch:{ all -> 0x00a0 }
            monitor-exit(r5)     // Catch:{ all -> 0x00a0 }
            com.millennialmedia.internal.PlayList r1 = r5.playList
            boolean r1 = r1.hasNext()
            if (r1 != 0) goto L_0x004d
            boolean r6 = com.millennialmedia.MMLog.isDebugEnabled()
            if (r6 == 0) goto L_0x0049
            java.lang.String r6 = com.millennialmedia.NativeAd.TAG
            java.lang.String r1 = "Unable to find ad adapter in play list"
            com.millennialmedia.MMLog.d(r6, r1)
        L_0x0049:
            r5.onLoadFailed(r0)
            return
        L_0x004d:
            com.millennialmedia.internal.AdPlacementReporter r6 = r6.getAdPlacementReporter()
            com.millennialmedia.internal.AdPlacementReporter$PlayListItemReporter r6 = com.millennialmedia.internal.AdPlacementReporter.getPlayListItemReporter(r6)
            com.millennialmedia.internal.PlayList r1 = r5.playList
            com.millennialmedia.internal.adadapters.AdAdapter r1 = r1.getNextAdAdapter(r5, r6)
            com.millennialmedia.internal.adadapters.NativeAdapter r1 = (com.millennialmedia.internal.adadapters.NativeAdapter) r1
            r5.nextNativeAdAdapter = r1
            java.lang.ref.WeakReference<android.content.Context> r1 = r5.contextRef
            java.lang.Object r1 = r1.get()
            android.content.Context r1 = (android.content.Context) r1
            com.millennialmedia.internal.adadapters.NativeAdapter r2 = r5.nextNativeAdAdapter
            if (r2 == 0) goto L_0x0093
            if (r1 == 0) goto L_0x0093
            com.millennialmedia.internal.adadapters.NativeAdapter r1 = r5.nextNativeAdAdapter
            int r1 = r1.requestTimeout
            if (r1 <= 0) goto L_0x0088
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r2 = r5.adAdapterRequestTimeoutRunnable
            if (r2 == 0) goto L_0x007c
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r2 = r5.adAdapterRequestTimeoutRunnable
            r2.cancel()
        L_0x007c:
            com.millennialmedia.NativeAd$3 r2 = new com.millennialmedia.NativeAd$3
            r2.<init>(r0, r6)
            long r3 = (long) r1
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r1 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r2, r3)
            r5.adAdapterRequestTimeoutRunnable = r1
        L_0x0088:
            com.millennialmedia.internal.adadapters.NativeAdapter r1 = r5.nextNativeAdAdapter
            com.millennialmedia.NativeAd$4 r2 = new com.millennialmedia.NativeAd$4
            r2.<init>(r0, r6)
            r1.init(r2)
            goto L_0x009d
        L_0x0093:
            com.millennialmedia.internal.AdPlacementReporter r1 = r0.getAdPlacementReporter()
            com.millennialmedia.internal.AdPlacementReporter.reportPlayListItem(r1, r6)
            r5.onAdAdapterLoadFailed(r0)
        L_0x009d:
            return
        L_0x009e:
            monitor-exit(r5)     // Catch:{ all -> 0x00a0 }
            return
        L_0x00a0:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00a0 }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.NativeAd.loadAdAdapter(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    public boolean loadComponents(NativeAdapter nativeAdapter) {
        String type = nativeAdapter.getType();
        if (type == null) {
            MMLog.e(TAG, "Unable to load components, native type is not set");
            return false;
        } else if (!this.requestedNativeTypes.contains(type)) {
            String str = TAG;
            MMLog.e(str, "Unable to load components, native type <" + type + "> is not a requested native type");
            return false;
        } else {
            this.nativeTypeDefinition = Handshake.getNativeTypeDefinition(type);
            if (this.nativeTypeDefinition == null) {
                String str2 = TAG;
                MMLog.e(str2, "Unable to load components, unable to find list of required components for native type <" + type + ">");
                return false;
            }
            this.titleInfoList = nativeAdapter.getTitleList();
            loadTextComponentArray("title", ComponentName.TITLE, this.titleInfoList);
            this.bodyInfoList = nativeAdapter.getBodyList();
            loadTextComponentArray(COMPONENT_ID_BODY, ComponentName.BODY, this.bodyInfoList);
            this.iconImageInfoList = nativeAdapter.getIconImageList();
            loadImageComponentArray(COMPONENT_ID_ICON_IMAGE, ComponentName.ICON_IMAGE, this.iconImageInfoList);
            this.mainImageInfoList = nativeAdapter.getMainImageList();
            loadImageComponentArray(COMPONENT_ID_MAIN_IMAGE, ComponentName.MAIN_IMAGE, this.mainImageInfoList);
            this.callToActionInfoList = nativeAdapter.getCallToActionList();
            loadButtonComponentArray(COMPONENT_ID_CALL_TO_ACTION, ComponentName.CALL_TO_ACTION, this.callToActionInfoList);
            this.ratingInfoList = nativeAdapter.getRatingList();
            loadTextComponentArray(COMPONENT_ID_RATING, ComponentName.RATING, this.ratingInfoList);
            this.disclaimerInfoList = nativeAdapter.getDisclaimerList();
            if (this.disclaimerInfoList.isEmpty()) {
                NativeAdapter.TextComponentInfo textComponentInfo = new NativeAdapter.TextComponentInfo();
                textComponentInfo.value = DEFAULT_DISCLAIMER_TEXT;
                this.disclaimerInfoList.add(textComponentInfo);
            }
            loadTextComponentArray(COMPONENT_ID_DISCLAIMER, ComponentName.DISCLAIMER, this.disclaimerInfoList);
            return validateLoadedComponents(type);
        }
    }

    private boolean validateLoadedComponents(String str) {
        ArrayList arrayList = new ArrayList();
        for (Handshake.NativeTypeDefinition.ComponentDefinition next : this.nativeTypeDefinition.componentDefinitions) {
            if (next != null) {
                int i = next.adverstiserRequired;
                List list = this.loadedComponents.get(next.componentId);
                if (list == null || list.size() < i) {
                    arrayList.add(next.componentId);
                }
            } else {
                MMLog.e(TAG, String.format("Missing configuration data for native type: %s.", str));
                return false;
            }
        }
        if (arrayList.size() <= 0) {
            return true;
        }
        String str2 = TAG;
        MMLog.e(str2, "Unable to load required components <" + TextUtils.join(", ", arrayList) + "> for native type <" + str + ">");
        return false;
    }

    private void loadTextComponentArray(String str, ComponentName componentName, List<NativeAdapter.TextComponentInfo> list) {
        ArrayList arrayList = new ArrayList();
        Context context = this.contextRef.get();
        if (context != null) {
            for (int i = 0; i < list.size(); i++) {
                NativeAdapter.TextComponentInfo textComponentInfo = list.get(i);
                if (textComponentInfo != null) {
                    TextView textView = new TextView(context);
                    textView.setText(textComponentInfo.value);
                    setComponentClickListener(textView, componentName, i, textComponentInfo);
                    arrayList.add(textView);
                }
            }
        }
        this.loadedComponents.put(str, arrayList);
    }

    private void loadImageComponentArray(String str, ComponentName componentName, List<NativeAdapter.ImageComponentInfo> list) {
        ArrayList arrayList = new ArrayList();
        Context context = this.contextRef.get();
        if (context != null) {
            for (int i = 0; i < list.size(); i++) {
                NativeAdapter.ImageComponentInfo imageComponentInfo = list.get(i);
                if (imageComponentInfo != null) {
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(imageComponentInfo.bitmap);
                    ImageView imageView = new ImageView(context);
                    imageView.setImageDrawable(bitmapDrawable);
                    setComponentClickListener(imageView, componentName, i, imageComponentInfo);
                    arrayList.add(imageView);
                }
            }
        }
        this.loadedComponents.put(str, arrayList);
    }

    private void loadButtonComponentArray(String str, ComponentName componentName, List<NativeAdapter.TextComponentInfo> list) {
        ArrayList arrayList = new ArrayList();
        Context context = this.contextRef.get();
        if (context != null) {
            for (int i = 0; i < list.size(); i++) {
                NativeAdapter.TextComponentInfo textComponentInfo = list.get(i);
                if (textComponentInfo != null) {
                    Button button = new Button(context);
                    button.setText(textComponentInfo.value);
                    setComponentClickListener(button, componentName, i, textComponentInfo);
                    arrayList.add(button);
                }
            }
        }
        this.loadedComponents.put(str, arrayList);
    }

    public void setListener(NativeListener nativeListener2) {
        if (!isDestroyed()) {
            this.nativeListener = nativeListener2;
        }
    }

    public boolean isReady() {
        if (isDestroyed()) {
            return false;
        }
        return this.placementState.equals("loaded");
    }

    /* access modifiers changed from: private */
    public void setCurrentAdAdapter(NativeAdapter nativeAdapter) {
        if (!(this.currentNativeAdAdapter == null || this.currentNativeAdAdapter == nativeAdapter)) {
            this.currentNativeAdAdapter.release();
        }
        this.currentNativeAdAdapter = nativeAdapter;
    }

    public boolean hasExpired() {
        if (isDestroyed()) {
            return false;
        }
        return this.placementState.equals(STATE_EXPIRED);
    }

    public void invokeDefaultAction() {
        if (!isDestroyed()) {
            if (!isReady()) {
                MMLog.e(TAG, "Unable to invoke default action, ad not loaded");
            } else if (this.currentNativeAdAdapter != null) {
                reportClick(this.currentRequestState.getAdPlacementReporter(), null);
                invokeAction("default", this.currentNativeAdAdapter.getDefaultAction());
            }
        }
    }

    /* access modifiers changed from: private */
    public void invokeAction(String str, String str2) {
        if (str2 == null) {
            String str3 = TAG;
            MMLog.e(str3, "Unable to invoke " + str + " action, url is null");
            return;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str2));
        Context context = this.contextRef.get();
        if (context != null && Utils.startActivity(context, intent)) {
            onAdLeftApplication();
        }
    }

    public void fireImpression() throws MMException {
        if (!isDestroyed()) {
            if (!isReady()) {
                Utils.logAndFireMMException(TAG, "Native ad is not in a loaded state, you must load before showing");
            } else if (this.usingManagedLayout) {
                MMLog.w(TAG, "Impression firing is disabled when using a managed layout.");
            } else {
                validateRequiredComponentAccess();
                MMLog.i(TAG, "All required components have been accessed, firing impression");
                reportImpression(this.currentRequestState.getAdPlacementReporter(), -1);
            }
        }
    }

    /* access modifiers changed from: private */
    public void reportImpression(AdPlacementReporter adPlacementReporter, int i) {
        if (!this.impressionReported) {
            this.impressionReported = true;
            AdPlacementReporter.setDisplayed(adPlacementReporter, i);
            TrackingEvent.fireUrls(this.currentNativeAdAdapter.getImpressionTrackers(), "impression tracker");
            stopExpirationTimer();
        }
    }

    /* access modifiers changed from: private */
    public void reportClick(AdPlacementReporter adPlacementReporter, NativeAdapter.ComponentInfo componentInfo) {
        List<String> list;
        reportImpression(adPlacementReporter, 2);
        AdPlacementReporter.setClicked(adPlacementReporter);
        if (componentInfo == null || componentInfo.clickTrackerUrls == null) {
            list = this.currentNativeAdAdapter.getClickTrackers();
        } else {
            list = componentInfo.clickTrackerUrls;
        }
        TrackingEvent.fireUrls(list, "click tracker");
    }

    public String getNativeType() {
        if (isDestroyed()) {
            return null;
        }
        if (!isReady()) {
            MMLog.e(TAG, "Unable to get native type, ad not loaded");
            return null;
        } else if (this.currentNativeAdAdapter == null) {
            return null;
        } else {
            return this.nativeTypeDefinition.typeName;
        }
    }

    public TextView getTitle() {
        return getTitle(1);
    }

    public TextView getTitle(int i) {
        if (isDestroyed()) {
            return null;
        }
        if (isReady()) {
            return (TextView) getComponentInstance(i, "title", "title");
        }
        MMLog.e(TAG, "Unable to get title, ad not loaded");
        return null;
    }

    public TextView getBody() {
        return getBody(1);
    }

    public TextView getBody(int i) {
        if (isDestroyed()) {
            return null;
        }
        if (isReady()) {
            return (TextView) getComponentInstance(i, COMPONENT_ID_BODY, COMPONENT_ID_BODY);
        }
        MMLog.e(TAG, "Unable to get body, ad not loaded");
        return null;
    }

    public ImageView getIconImage() {
        return getIconImage(1);
    }

    public ImageView getIconImage(int i) {
        if (isDestroyed()) {
            return null;
        }
        if (isReady()) {
            return (ImageView) getComponentInstance(i, COMPONENT_ID_ICON_IMAGE, "icon image");
        }
        MMLog.e(TAG, "Unable to get icon image, ad not loaded");
        return null;
    }

    public ImageView getMainImage() {
        return getMainImage(1);
    }

    public ImageView getMainImage(int i) {
        if (isDestroyed()) {
            return null;
        }
        if (isReady()) {
            return (ImageView) getComponentInstance(i, COMPONENT_ID_MAIN_IMAGE, "main image");
        }
        MMLog.e(TAG, "Unable to get main image, ad not loaded");
        return null;
    }

    public Button getCallToActionButton() {
        return getCallToActionButton(1);
    }

    public Button getCallToActionButton(int i) {
        if (isDestroyed()) {
            return null;
        }
        if (isReady()) {
            return (Button) getComponentInstance(i, COMPONENT_ID_CALL_TO_ACTION, "call to action");
        }
        MMLog.e(TAG, "Unable to get call to action button, ad not loaded");
        return null;
    }

    public TextView getRating() {
        return getRating(1);
    }

    public TextView getRating(int i) {
        if (isDestroyed()) {
            return null;
        }
        if (isReady()) {
            return (TextView) getComponentInstance(i, COMPONENT_ID_RATING, COMPONENT_ID_RATING);
        }
        MMLog.e(TAG, "Unable to get rating, ad not loaded");
        return null;
    }

    public TextView getDisclaimer() {
        return getDisclaimer(1);
    }

    public TextView getDisclaimer(int i) {
        if (isDestroyed()) {
            return null;
        }
        if (isReady()) {
            return (TextView) getComponentInstance(i, COMPONENT_ID_DISCLAIMER, COMPONENT_ID_DISCLAIMER);
        }
        MMLog.e(TAG, "Unable to get disclaimer, ad not loaded");
        return null;
    }

    public void fireCallToActionClicked() {
        if (!isDestroyed()) {
            NativeAdapter.ComponentInfo componentInfo = getComponentInfo(ComponentName.CALL_TO_ACTION, 1);
            if (componentInfo == null) {
                MMLog.e(TAG, "Unable to fire clicked, found component info is null");
            } else {
                reportClick(this.currentRequestState.getAdPlacementReporter(), componentInfo);
            }
        }
    }

    public String getCallToActionUrl() {
        if (isDestroyed()) {
            return null;
        }
        NativeAdapter.ComponentInfo componentInfo = getComponentInfo(ComponentName.CALL_TO_ACTION, 1);
        if (componentInfo != null) {
            return componentInfo.clickUrl;
        }
        MMLog.e(TAG, "Unable to get call to action url, found component info is not for a call to action component");
        return null;
    }

    public String getImageUrl(ComponentName componentName, int i) {
        if (isDestroyed()) {
            return null;
        }
        NativeAdapter.ComponentInfo componentInfo = getComponentInfo(componentName, i);
        if (componentInfo instanceof NativeAdapter.ImageComponentInfo) {
            return ((NativeAdapter.ImageComponentInfo) componentInfo).bitmapUrl;
        }
        MMLog.e(TAG, "Unable to get image url, found component info is not for a image component");
        return null;
    }

    private NativeAdapter.ComponentInfo getComponentInfo(ComponentName componentName, int i) {
        List list;
        if (componentName == ComponentName.CALL_TO_ACTION) {
            list = this.callToActionInfoList;
        } else if (componentName == ComponentName.ICON_IMAGE) {
            list = this.iconImageInfoList;
        } else {
            list = componentName == ComponentName.MAIN_IMAGE ? this.mainImageInfoList : null;
        }
        if (list == null) {
            MMLog.e(TAG, String.format(Locale.getDefault(), "Unable to get component info for component name <%s> and instance id <%d>, did not find component info list", componentName, Integer.valueOf(i)));
            return null;
        } else if (i < 1) {
            MMLog.e(TAG, "Unable to get component info for component name <" + componentName + "> and instance id <" + i + ">, instance id must be greater than 0");
            return null;
        } else if (list.size() < i) {
            MMLog.e(TAG, "Unable to get component info for component name <" + componentName + "> and instance id <" + i + ">, only <" + list.size() + "> instances found");
            return null;
        } else {
            int i2 = i - 1;
            NativeAdapter.ComponentInfo componentInfo = (NativeAdapter.ComponentInfo) list.get(i2);
            if (componentInfo != null) {
                return componentInfo;
            }
            MMLog.e(TAG, "Unable to get component info for component name <" + componentName + "> and instance id <" + i2 + ">, found value is null");
            return null;
        }
    }

    private Object getComponentInstance(int i, String str, String str2) {
        if (i < 1) {
            String str3 = TAG;
            MMLog.e(str3, "Unable to retrieve the requested <" + str2 + "> instance, instance value must be 1 or greater");
            return null;
        }
        List list = this.loadedComponents.get(str);
        if (list.size() < i) {
            String str4 = TAG;
            MMLog.e(str4, "Unable to retrieve the requested <" + str2 + "> instance <" + i + ">, only <" + list.size() + "> instances available");
            return null;
        }
        markComponentAsAccessed(str, i);
        return list.get(i - 1);
    }

    private void setComponentClickListener(View view, ComponentName componentName, int i, NativeAdapter.ComponentInfo componentInfo) {
        final AdPlacementReporter adPlacementReporter = this.currentRequestState.getAdPlacementReporter();
        final NativeAdapter.ComponentInfo componentInfo2 = componentInfo;
        final ComponentName componentName2 = componentName;
        final int i2 = i;
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!NativeAd.this.isDestroyed()) {
                    MMLog.i(NativeAd.TAG, "Ad clicked");
                    NativeAd.this.reportClick(adPlacementReporter, componentInfo2);
                    if (NativeAd.this.currentNativeAdAdapter instanceof NativeMediatedAdAdapter) {
                        ((NativeMediatedAdAdapter) NativeAd.this.currentNativeAdAdapter).onAdClicked(NativeAd.this);
                    }
                    final NativeListener access$2600 = NativeAd.this.nativeListener;
                    if (access$2600 != null) {
                        ThreadUtils.runOffUiThread(new Runnable() {
                            public void run() {
                                access$2600.onClicked(NativeAd.this, componentName2, i2);
                            }
                        });
                    }
                    NativeAd.this.invokeAction("click", componentInfo2.clickUrl);
                }
            }
        });
    }

    private void markComponentAsAccessed(String str, int i) {
        Set set = this.accessedComponentIndices.get(str);
        if (set == null) {
            set = new HashSet();
            this.accessedComponentIndices.put(str, set);
        }
        set.add(Integer.valueOf(i));
    }

    @SuppressLint({"DefaultLocale"})
    private void validateRequiredComponentAccess() throws MMException {
        ArrayList arrayList = new ArrayList();
        for (Handshake.NativeTypeDefinition.ComponentDefinition next : this.nativeTypeDefinition.componentDefinitions) {
            Set set = this.accessedComponentIndices.get(next.componentId);
            int size = set != null ? set.size() : 0;
            if (size < next.publisherRequired) {
                arrayList.add(String.format("Component: %s, required: %d, accessed: %d", next.componentId, Integer.valueOf(next.publisherRequired), Integer.valueOf(size)));
            }
        }
        if (!arrayList.isEmpty()) {
            String str = "Unable to validate that all required native components have been accessed:\n" + arrayList.toString();
            MMLog.e(TAG, str);
            throw new MMException(str);
        }
    }

    public void updateLayout(View view) throws MMException {
        if (!isDestroyed()) {
            if (!ThreadUtils.isUiThread()) {
                MMLog.e(TAG, "NativeAd.updateLayout must be called on the UI thread.");
            } else if (view == null) {
                MMLog.e(TAG, "Unable to updated; the provided layout was null.");
            } else if (!isReady()) {
                MMLog.e(TAG, "Cannot update the layout. The NativeAd is not loaded.");
            } else {
                internalUpdateLayout(view, false, true);
            }
        }
    }

    public View inflateLayout(Context context, int[] iArr) throws MMException {
        if (isDestroyed()) {
            return null;
        }
        if (!ThreadUtils.isUiThread()) {
            MMLog.e(TAG, "NativeAd.inflateLayout must be called on the UI thread.");
            return null;
        } else if (context == null) {
            MMLog.e(TAG, "Unable to inflate a layout because the provided Context is null.");
            return null;
        } else if (iArr == null || iArr.length == 0) {
            MMLog.e(TAG, "Unable to inflate a layout because the layoutIds are null or empty.");
            return null;
        } else if (!isReady()) {
            MMLog.e(TAG, "Cannot inflate a layout. The NativeAd is not loaded.");
            return null;
        } else {
            LayoutInflater from = LayoutInflater.from(context);
            for (int i = 0; i < iArr.length; i++) {
                View inflate = from.inflate(iArr[i], (ViewGroup) null);
                boolean z = true;
                if (i >= iArr.length - 1) {
                    z = false;
                }
                if (internalUpdateLayout(inflate, z, false)) {
                    return inflate;
                }
            }
            return null;
        }
    }

    private boolean internalUpdateLayout(View view, boolean z, boolean z2) throws MMException {
        boolean z3;
        View view2 = view;
        HashMap hashMap = new HashMap();
        for (Handshake.NativeTypeDefinition.ComponentDefinition next : this.nativeTypeDefinition.componentDefinitions) {
            hashMap.put(next.componentId, next);
        }
        List<TextView> findTextViewsByComponentId = findTextViewsByComponentId(view2, COMPONENT_ID_BODY);
        Handshake.NativeTypeDefinition.ComponentDefinition componentDefinition = (Handshake.NativeTypeDefinition.ComponentDefinition) hashMap.get(COMPONENT_ID_BODY);
        if (componentDefinition == null || findTextViewsByComponentId.size() >= componentDefinition.publisherRequired) {
            z3 = true;
        } else {
            MMLog.e(TAG, "Layout does not contain the required number of Views for the body component.");
            z3 = false;
        }
        List<TextView> findTextViewsByComponentId2 = findTextViewsByComponentId(view2, COMPONENT_ID_CALL_TO_ACTION);
        Handshake.NativeTypeDefinition.ComponentDefinition componentDefinition2 = (Handshake.NativeTypeDefinition.ComponentDefinition) hashMap.get(COMPONENT_ID_CALL_TO_ACTION);
        if (componentDefinition2 != null && findTextViewsByComponentId2.size() < componentDefinition2.publisherRequired) {
            MMLog.e(TAG, "Layout does not contain the required number of Views for the 'Call To Action' component.");
            z3 = false;
        }
        List<TextView> findTextViewsByComponentId3 = findTextViewsByComponentId(view2, COMPONENT_ID_DISCLAIMER);
        Handshake.NativeTypeDefinition.ComponentDefinition componentDefinition3 = (Handshake.NativeTypeDefinition.ComponentDefinition) hashMap.get(COMPONENT_ID_DISCLAIMER);
        if (componentDefinition3 != null && findTextViewsByComponentId3.size() < componentDefinition3.publisherRequired) {
            MMLog.e(TAG, "Layout does not contain the required number of Views for the Disclaimer component.");
            z3 = false;
        }
        List<ImageView> findImageViewsByComponentId = findImageViewsByComponentId(view2, COMPONENT_ID_ICON_IMAGE);
        Handshake.NativeTypeDefinition.ComponentDefinition componentDefinition4 = (Handshake.NativeTypeDefinition.ComponentDefinition) hashMap.get(COMPONENT_ID_ICON_IMAGE);
        if (componentDefinition4 != null && findImageViewsByComponentId.size() < componentDefinition4.publisherRequired) {
            MMLog.e(TAG, "Layout does not contain the required number of Views for the 'Icon Image' component.");
            z3 = false;
        }
        List<ImageView> findImageViewsByComponentId2 = findImageViewsByComponentId(view2, COMPONENT_ID_MAIN_IMAGE);
        Handshake.NativeTypeDefinition.ComponentDefinition componentDefinition5 = (Handshake.NativeTypeDefinition.ComponentDefinition) hashMap.get(COMPONENT_ID_MAIN_IMAGE);
        if (componentDefinition5 != null && findImageViewsByComponentId2.size() < componentDefinition5.publisherRequired) {
            MMLog.e(TAG, "Layout does not contain the required number of Views for the 'Main Image' component.");
            z3 = false;
        }
        List<TextView> findTextViewsByComponentId4 = findTextViewsByComponentId(view2, COMPONENT_ID_RATING);
        Handshake.NativeTypeDefinition.ComponentDefinition componentDefinition6 = (Handshake.NativeTypeDefinition.ComponentDefinition) hashMap.get(COMPONENT_ID_RATING);
        if (componentDefinition6 != null && findTextViewsByComponentId4.size() < componentDefinition6.publisherRequired) {
            MMLog.e(TAG, "Layout does not contain the required number of Views for the Rating component.");
            z3 = false;
        }
        List<TextView> findTextViewsByComponentId5 = findTextViewsByComponentId(view2, "title");
        Handshake.NativeTypeDefinition.ComponentDefinition componentDefinition7 = (Handshake.NativeTypeDefinition.ComponentDefinition) hashMap.get("title");
        if (componentDefinition7 != null && findTextViewsByComponentId5.size() < componentDefinition7.publisherRequired) {
            MMLog.e(TAG, "Layout does not contain the required number of Views for the Title component.");
            z3 = false;
        }
        if (z3) {
            if (!(findTextViewsByComponentId.size() <= this.bodyInfoList.size() && findTextViewsByComponentId3.size() <= this.disclaimerInfoList.size() && findTextViewsByComponentId4.size() <= this.ratingInfoList.size() && findTextViewsByComponentId5.size() <= this.titleInfoList.size() && findTextViewsByComponentId2.size() <= this.callToActionInfoList.size() && findImageViewsByComponentId.size() <= this.iconImageInfoList.size() && findImageViewsByComponentId2.size() <= this.mainImageInfoList.size()) && z) {
                return false;
            }
            boolean z4 = z2;
            fillTextViews(findTextViewsByComponentId, COMPONENT_ID_BODY, ComponentName.BODY, this.bodyInfoList, z4);
            fillTextViews(findTextViewsByComponentId3, COMPONENT_ID_DISCLAIMER, ComponentName.DISCLAIMER, this.disclaimerInfoList, z4);
            fillTextViews(findTextViewsByComponentId4, COMPONENT_ID_RATING, ComponentName.RATING, this.ratingInfoList, z4);
            fillTextViews(findTextViewsByComponentId5, "title", ComponentName.TITLE, this.titleInfoList, z4);
            fillTextViews(findTextViewsByComponentId2, COMPONENT_ID_CALL_TO_ACTION, ComponentName.CALL_TO_ACTION, this.callToActionInfoList, z4);
            fillImageViews(findImageViewsByComponentId, COMPONENT_ID_ICON_IMAGE, ComponentName.ICON_IMAGE, this.iconImageInfoList, z4);
            fillImageViews(findImageViewsByComponentId2, COMPONENT_ID_MAIN_IMAGE, ComponentName.MAIN_IMAGE, this.mainImageInfoList, z4);
            this.usingManagedLayout = true;
            if (this.impressionReporter != null) {
                this.impressionReporter.cancel();
            }
            this.impressionReporter = new ImpressionReporter(this, view2, getImpressionDelay(this.currentNativeAdAdapter));
            this.impressionReporter.start();
            return true;
        }
        MMLog.e(TAG, "Layout was not updated because it did not contain the required Views.");
        return false;
    }

    private void fillImageViews(List<ImageView> list, String str, ComponentName componentName, List<NativeAdapter.ImageComponentInfo> list2, boolean z) throws MMException {
        int i;
        if (list != null) {
            List list3 = this.loadedComponents.get(str);
            if (z) {
                i = list.size();
            } else {
                i = Math.min(list.size(), list3.size());
            }
            for (int i2 = 0; i2 < i; i2++) {
                ImageView imageView = list.get(i2);
                if (i2 < list3.size()) {
                    imageView.setImageDrawable(((ImageView) list3.get(i2)).getDrawable());
                    setComponentClickListener(imageView, componentName, i2, list2.get(i2));
                } else {
                    imageView.setImageDrawable(null);
                    imageView.setOnClickListener(null);
                }
            }
        }
    }

    private void fillTextViews(List<TextView> list, String str, ComponentName componentName, List<NativeAdapter.TextComponentInfo> list2, boolean z) throws MMException {
        int i;
        if (list != null) {
            List list3 = this.loadedComponents.get(str);
            if (z) {
                i = list.size();
            } else {
                i = Math.min(list.size(), list3.size());
            }
            for (int i2 = 0; i2 < i; i2++) {
                TextView textView = list.get(i2);
                if (i2 < list3.size()) {
                    textView.setText(((TextView) list3.get(i2)).getText());
                    setComponentClickListener(textView, componentName, i2, list2.get(i2));
                } else {
                    textView.setText("");
                    textView.setOnClickListener(null);
                }
            }
        }
    }

    private List<TextView> findTextViewsByComponentId(View view, String str) throws MMException {
        ArrayList arrayList = new ArrayList();
        int i = 1;
        while (i <= MAX_COMP_INSTANCE_ID) {
            View findViewWithTag = view.findViewWithTag(str + "_" + i);
            if (findViewWithTag == null) {
                break;
            } else if (findViewWithTag instanceof TextView) {
                arrayList.add((TextView) findViewWithTag);
                i++;
            } else {
                throw new MMException("Expected View with tag = " + findViewWithTag.getTag() + " to be a TextView.");
            }
        }
        return arrayList;
    }

    private List<ImageView> findImageViewsByComponentId(View view, String str) throws MMException {
        ArrayList arrayList = new ArrayList();
        int i = 1;
        while (i <= MAX_COMP_INSTANCE_ID) {
            View findViewWithTag = view.findViewWithTag(str + "_" + i);
            if (findViewWithTag == null) {
                break;
            } else if (findViewWithTag instanceof ImageView) {
                arrayList.add((ImageView) findViewWithTag);
                i++;
            } else {
                throw new MMException("Expected View with tag = " + findViewWithTag.getTag() + " to be a ImageView.");
            }
        }
        return arrayList;
    }

    private void stopRequestTimeoutTimers() {
        if (this.placementRequestTimeoutRunnable != null) {
            this.placementRequestTimeoutRunnable.cancel();
            this.placementRequestTimeoutRunnable = null;
        }
        if (this.adAdapterRequestTimeoutRunnable != null) {
            this.adAdapterRequestTimeoutRunnable.cancel();
            this.adAdapterRequestTimeoutRunnable = null;
        }
    }

    private void startExpirationTimer(AdPlacement.RequestState requestState) {
        stopExpirationTimer();
        int nativeExpirationDuration = Handshake.getNativeExpirationDuration();
        if (nativeExpirationDuration > 0) {
            this.expirationRunnable = ThreadUtils.runOnWorkerThreadDelayed(new ExpirationRunnable(this, requestState), (long) nativeExpirationDuration);
        }
    }

    private void stopExpirationTimer() {
        if (this.expirationRunnable != null) {
            this.expirationRunnable.cancel();
            this.expirationRunnable = null;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAdAdapterLoadFailed(com.millennialmedia.internal.AdPlacement.RequestState r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x0053 }
            boolean r0 = r0.compare(r3)     // Catch:{ all -> 0x0053 }
            if (r0 != 0) goto L_0x0018
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0053 }
            if (r3 == 0) goto L_0x0016
            java.lang.String r3 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x0053 }
            java.lang.String r0 = "onAdAdapterLoadFailed called but load state is not valid"
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0053 }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x0018:
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0053 }
            if (r0 != 0) goto L_0x0042
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0053 }
            if (r3 == 0) goto L_0x0040
            java.lang.String r3 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x0053 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0053 }
            r0.<init>()     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = "onAdAdapterLoadFailed called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = r2.placementState     // Catch:{ all -> 0x0053 }
            r0.append(r1)     // Catch:{ all -> 0x0053 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0053 }
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0053 }
        L_0x0040:
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x0042:
            boolean r0 = r2.doPendingDestroy()     // Catch:{ all -> 0x0053 }
            if (r0 == 0) goto L_0x004a
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x004a:
            java.lang.String r0 = "ad_adapter_load_failed"
            r2.placementState = r0     // Catch:{ all -> 0x0053 }
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            r2.loadAdAdapter(r3)
            return
        L_0x0053:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.NativeAd.onAdAdapterLoadFailed(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005a, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0080, code lost:
        if ((r3.currentNativeAdAdapter instanceof com.millennialmedia.internal.adadapters.NativeMediatedAdAdapter) == false) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0082, code lost:
        ((com.millennialmedia.internal.adadapters.NativeMediatedAdAdapter) r3.currentNativeAdAdapter).onAdLoaded(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0089, code lost:
        r4 = r3.nativeListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x008b, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008d, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.NativeAd.AnonymousClass6(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0030, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLoadSucceeded(com.millennialmedia.internal.AdPlacement.RequestState r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r3.currentRequestState     // Catch:{ all -> 0x0096 }
            boolean r0 = r0.compare(r4)     // Catch:{ all -> 0x0096 }
            if (r0 != 0) goto L_0x0031
            boolean r0 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0096 }
            if (r0 == 0) goto L_0x002f
            java.lang.String r0 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x0096 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0096 }
            r1.<init>()     // Catch:{ all -> 0x0096 }
            java.lang.String r2 = "onLoadSucceeded called but load state is not valid; current = "
            r1.append(r2)     // Catch:{ all -> 0x0096 }
            com.millennialmedia.internal.AdPlacement$RequestState r2 = r3.currentRequestState     // Catch:{ all -> 0x0096 }
            r1.append(r2)     // Catch:{ all -> 0x0096 }
            java.lang.String r2 = ", caller = "
            r1.append(r2)     // Catch:{ all -> 0x0096 }
            r1.append(r4)     // Catch:{ all -> 0x0096 }
            java.lang.String r4 = r1.toString()     // Catch:{ all -> 0x0096 }
            com.millennialmedia.MMLog.d(r0, r4)     // Catch:{ all -> 0x0096 }
        L_0x002f:
            monitor-exit(r3)     // Catch:{ all -> 0x0096 }
            return
        L_0x0031:
            java.lang.String r0 = r3.placementState     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0096 }
            if (r0 != 0) goto L_0x005b
            boolean r4 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0096 }
            if (r4 == 0) goto L_0x0059
            java.lang.String r4 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x0096 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0096 }
            r0.<init>()     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = "onLoadSucceeded called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = r3.placementState     // Catch:{ all -> 0x0096 }
            r0.append(r1)     // Catch:{ all -> 0x0096 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0096 }
            com.millennialmedia.MMLog.d(r4, r0)     // Catch:{ all -> 0x0096 }
        L_0x0059:
            monitor-exit(r3)     // Catch:{ all -> 0x0096 }
            return
        L_0x005b:
            boolean r0 = r3.doPendingDestroy()     // Catch:{ all -> 0x0096 }
            if (r0 == 0) goto L_0x0063
            monitor-exit(r3)     // Catch:{ all -> 0x0096 }
            return
        L_0x0063:
            java.lang.String r0 = "loaded"
            r3.placementState = r0     // Catch:{ all -> 0x0096 }
            java.lang.String r0 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = "Load succeeded"
            com.millennialmedia.MMLog.i(r0, r1)     // Catch:{ all -> 0x0096 }
            r3.stopRequestTimeoutTimers()     // Catch:{ all -> 0x0096 }
            r3.startExpirationTimer(r4)     // Catch:{ all -> 0x0096 }
            com.millennialmedia.internal.AdPlacementReporter r4 = r4.getAdPlacementReporter()     // Catch:{ all -> 0x0096 }
            com.millennialmedia.internal.AdPlacementReporter.reportPlayList(r4)     // Catch:{ all -> 0x0096 }
            monitor-exit(r3)     // Catch:{ all -> 0x0096 }
            com.millennialmedia.internal.adadapters.NativeAdapter r4 = r3.currentNativeAdAdapter
            boolean r4 = r4 instanceof com.millennialmedia.internal.adadapters.NativeMediatedAdAdapter
            if (r4 == 0) goto L_0x0089
            com.millennialmedia.internal.adadapters.NativeAdapter r4 = r3.currentNativeAdAdapter
            com.millennialmedia.internal.adadapters.NativeMediatedAdAdapter r4 = (com.millennialmedia.internal.adadapters.NativeMediatedAdAdapter) r4
            r4.onAdLoaded(r3)
        L_0x0089:
            com.millennialmedia.NativeAd$NativeListener r4 = r3.nativeListener
            if (r4 == 0) goto L_0x0095
            com.millennialmedia.NativeAd$6 r0 = new com.millennialmedia.NativeAd$6
            r0.<init>(r4)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x0095:
            return
        L_0x0096:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0096 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.NativeAd.onLoadSucceeded(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0053, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0080, code lost:
        r4 = r3.nativeListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0082, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0084, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.NativeAd.AnonymousClass7(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLoadFailed(com.millennialmedia.internal.AdPlacement.RequestState r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.doPendingDestroy()     // Catch:{ all -> 0x008d }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            return
        L_0x0009:
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r3.currentRequestState     // Catch:{ all -> 0x008d }
            boolean r0 = r0.compareRequest(r4)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0020
            boolean r4 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x008d }
            if (r4 == 0) goto L_0x001e
            java.lang.String r4 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x008d }
            java.lang.String r0 = "onLoadFailed called but load state is not valid"
            com.millennialmedia.MMLog.d(r4, r0)     // Catch:{ all -> 0x008d }
        L_0x001e:
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            return
        L_0x0020:
            java.lang.String r0 = r3.placementState     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0054
            java.lang.String r0 = r3.placementState     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "loading_play_list"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0054
            boolean r4 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x008d }
            if (r4 == 0) goto L_0x0052
            java.lang.String r4 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r0.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "onLoadFailed called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r1 = r3.placementState     // Catch:{ all -> 0x008d }
            r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008d }
            com.millennialmedia.MMLog.d(r4, r0)     // Catch:{ all -> 0x008d }
        L_0x0052:
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            return
        L_0x0054:
            java.lang.String r0 = "load_failed"
            r3.placementState = r0     // Catch:{ all -> 0x008d }
            java.lang.String r0 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r1.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r2 = "Load failed for placement ID: "
            r1.append(r2)     // Catch:{ all -> 0x008d }
            java.lang.String r2 = r3.placementId     // Catch:{ all -> 0x008d }
            r1.append(r2)     // Catch:{ all -> 0x008d }
            java.lang.String r2 = ". If this warning persists please check your placement configuration."
            r1.append(r2)     // Catch:{ all -> 0x008d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008d }
            com.millennialmedia.MMLog.w(r0, r1)     // Catch:{ all -> 0x008d }
            r3.stopRequestTimeoutTimers()     // Catch:{ all -> 0x008d }
            com.millennialmedia.internal.AdPlacementReporter r4 = r4.getAdPlacementReporter()     // Catch:{ all -> 0x008d }
            com.millennialmedia.internal.AdPlacementReporter.reportPlayList(r4)     // Catch:{ all -> 0x008d }
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            com.millennialmedia.NativeAd$NativeListener r4 = r3.nativeListener
            if (r4 == 0) goto L_0x008c
            com.millennialmedia.NativeAd$7 r0 = new com.millennialmedia.NativeAd$7
            r0.<init>(r4)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x008c:
            return
        L_0x008d:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x008d }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.NativeAd.onLoadFailed(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    private void onAdLeftApplication() {
        MMLog.i(TAG, "Ad left application");
        final NativeListener nativeListener2 = this.nativeListener;
        if (nativeListener2 != null) {
            ThreadUtils.runOffUiThread(new Runnable() {
                public void run() {
                    nativeListener2.onAdLeftApplication(NativeAd.this);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0047, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.NativeAd.TAG, "Ad expired");
        r3 = r2.nativeListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0050, code lost:
        if (r3 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0052, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.NativeAd.AnonymousClass9(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onExpired(com.millennialmedia.internal.AdPlacement.RequestState r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x005b }
            boolean r3 = r0.compare(r3)     // Catch:{ all -> 0x005b }
            if (r3 != 0) goto L_0x0018
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x005b }
            if (r3 == 0) goto L_0x0016
            java.lang.String r3 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x005b }
            java.lang.String r0 = "onExpired called but load state is not valid"
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x005b }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            return
        L_0x0018:
            java.lang.String r3 = r2.placementState     // Catch:{ all -> 0x005b }
            java.lang.String r0 = "loaded"
            boolean r3 = r3.equals(r0)     // Catch:{ all -> 0x005b }
            if (r3 != 0) goto L_0x0042
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x005b }
            if (r3 == 0) goto L_0x0040
            java.lang.String r3 = com.millennialmedia.NativeAd.TAG     // Catch:{ all -> 0x005b }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x005b }
            r0.<init>()     // Catch:{ all -> 0x005b }
            java.lang.String r1 = "onExpired called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x005b }
            java.lang.String r1 = r2.placementState     // Catch:{ all -> 0x005b }
            r0.append(r1)     // Catch:{ all -> 0x005b }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x005b }
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x005b }
        L_0x0040:
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            return
        L_0x0042:
            java.lang.String r3 = "expired"
            r2.placementState = r3     // Catch:{ all -> 0x005b }
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            java.lang.String r3 = com.millennialmedia.NativeAd.TAG
            java.lang.String r0 = "Ad expired"
            com.millennialmedia.MMLog.i(r3, r0)
            com.millennialmedia.NativeAd$NativeListener r3 = r2.nativeListener
            if (r3 == 0) goto L_0x005a
            com.millennialmedia.NativeAd$9 r0 = new com.millennialmedia.NativeAd$9
            r0.<init>(r3)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x005a:
            return
        L_0x005b:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.NativeAd.onExpired(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    private boolean isLoading() {
        return !this.placementState.equals("idle") && !this.placementState.equals("load_failed") && !this.placementState.equals("loaded") && !this.placementState.equals(STATE_EXPIRED) && !this.placementState.equals("destroyed");
    }

    private static long getImpressionDelay(AdAdapter adAdapter) {
        return adAdapter.getImpressionDelay();
    }

    public Context getContext() {
        if (this.contextRef == null) {
            return null;
        }
        return this.contextRef.get();
    }

    public Map<String, Object> getAdPlacementMetaDataMap() {
        if (this.nativeAdMetadata == null) {
            return null;
        }
        return this.nativeAdMetadata.toMap(this);
    }

    /* access modifiers changed from: protected */
    public boolean canDestroyNow() {
        return !isLoading();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.nativeListener = null;
        this.incentivizedEventListener = null;
        this.nativeAdMetadata = null;
        stopRequestTimeoutTimers();
        stopExpirationTimer();
        if (this.impressionReporter != null) {
            this.impressionReporter.cancel();
            this.impressionReporter = null;
        }
        releaseAdapters();
        this.nativeTypeDefinition = null;
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                for (Map.Entry<String, List<Object>> value : NativeAd.this.loadedComponents.entrySet()) {
                    List list = (List) value.getValue();
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i) != null) {
                            ViewUtils.removeFromParent((View) list.get(i));
                        }
                    }
                }
                NativeAd.this.loadedComponents.clear();
            }
        });
        this.accessedComponentIndices.clear();
        if (this.requestedNativeTypes != null) {
            this.requestedNativeTypes.clear();
            this.requestedNativeTypes = null;
        }
        this.bodyInfoList = null;
        this.disclaimerInfoList = null;
        this.ratingInfoList = null;
        this.titleInfoList = null;
        this.callToActionInfoList = null;
        this.iconImageInfoList = null;
        this.mainImageInfoList = null;
        this.playList = null;
    }

    private void releaseAdapters() {
        if (this.currentNativeAdAdapter != null) {
            this.currentNativeAdAdapter.release();
            this.currentNativeAdAdapter = null;
        }
        if (this.nextNativeAdAdapter != null) {
            this.nextNativeAdAdapter.release();
            this.nextNativeAdAdapter = null;
        }
    }
}
