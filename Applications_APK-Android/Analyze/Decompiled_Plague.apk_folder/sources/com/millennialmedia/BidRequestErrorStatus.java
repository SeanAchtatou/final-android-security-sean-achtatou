package com.millennialmedia;

import com.millennialmedia.internal.ErrorStatus;

public class BidRequestErrorStatus extends ErrorStatus {
    public static final int INVALID_BID_PRICE = 401;

    static {
        errorCodes.put(Integer.valueOf((int) INVALID_BID_PRICE), "INVALID_BID_PRICE");
    }

    public BidRequestErrorStatus(int i) {
        super(i);
    }

    public BidRequestErrorStatus(int i, String str) {
        super(i, str);
    }
}
