package com.millennialmedia;

import android.content.Context;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementMetadata;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.ErrorStatus;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.InterstitialAdapter;
import com.millennialmedia.internal.utils.ThreadUtils;
import java.lang.ref.WeakReference;
import java.util.Map;

public class InterstitialAd extends AdPlacement {
    protected static final String STATE_EXPIRED = "expired";
    protected static final String STATE_SHOWING = "showing";
    protected static final String STATE_SHOWN = "shown";
    protected static final String STATE_SHOW_FAILED = "show_failed";
    /* access modifiers changed from: private */
    public static final String TAG = "InterstitialAd";
    private ThreadUtils.ScheduledRunnable adAdapterRequestTimeoutRunnable;
    private WeakReference<Context> contextRef;
    private volatile InterstitialAdapter currentInterstitialAdAdapter;
    /* access modifiers changed from: private */
    public ThreadUtils.ScheduledRunnable expirationRunnable;
    private InterstitialAdMetadata interstitialAdMetadata;
    private InterstitialListener interstitialListener;
    /* access modifiers changed from: private */
    public volatile InterstitialAdapter nextInterstitialAdAdapter;
    private ThreadUtils.ScheduledRunnable placementRequestTimeoutRunnable;

    public interface InterstitialListener {
        void onAdLeftApplication(InterstitialAd interstitialAd);

        void onClicked(InterstitialAd interstitialAd);

        void onClosed(InterstitialAd interstitialAd);

        void onExpired(InterstitialAd interstitialAd);

        void onLoadFailed(InterstitialAd interstitialAd, InterstitialErrorStatus interstitialErrorStatus);

        void onLoaded(InterstitialAd interstitialAd);

        void onShowFailed(InterstitialAd interstitialAd, InterstitialErrorStatus interstitialErrorStatus);

        void onShown(InterstitialAd interstitialAd);
    }

    public static class InterstitialErrorStatus extends ErrorStatus {
        public static final int ALREADY_LOADED = 203;
        public static final int EXPIRED = 201;
        public static final int NOT_LOADED = 202;

        static {
            errorCodes.put(Integer.valueOf((int) EXPIRED), "EXPIRED");
            errorCodes.put(Integer.valueOf((int) NOT_LOADED), "NOT_LOADED");
            errorCodes.put(Integer.valueOf((int) ALREADY_LOADED), "ALREADY_LOADED");
        }

        public InterstitialErrorStatus(int i, String str) {
            super(i, str);
        }

        public InterstitialErrorStatus(int i) {
            super(i);
        }
    }

    public static class InterstitialAdMetadata extends AdPlacementMetadata<InterstitialAdMetadata> {
        private static final String PLACEMENT_TYPE_INTERSTITIAL = "interstitial";

        public InterstitialAdMetadata() {
            super("interstitial");
        }
    }

    private static class ExpirationRunnable implements Runnable {
        WeakReference<InterstitialAd> interstitialAdRef;
        WeakReference<AdPlacement.RequestState> requestStateRef;

        ExpirationRunnable(InterstitialAd interstitialAd, AdPlacement.RequestState requestState) {
            this.interstitialAdRef = new WeakReference<>(interstitialAd);
            this.requestStateRef = new WeakReference<>(requestState);
        }

        public void run() {
            InterstitialAd interstitialAd = this.interstitialAdRef.get();
            if (interstitialAd == null) {
                MMLog.e(InterstitialAd.TAG, "InterstitialAd instance has been destroyed, aborting expiration state change");
                return;
            }
            ThreadUtils.ScheduledRunnable unused = interstitialAd.expirationRunnable = null;
            AdPlacement.RequestState requestState = this.requestStateRef.get();
            if (requestState == null) {
                MMLog.e(InterstitialAd.TAG, "No valid RequestState is available, unable to trigger expired state change");
            } else {
                interstitialAd.onExpired(requestState);
            }
        }
    }

    public static InterstitialAd createInstance(String str) throws MMException {
        if (MMSDK.isInitialized()) {
            return new InterstitialAd(str);
        }
        throw new MMInitializationException("Unable to create instance, SDK must be initialized first");
    }

    private InterstitialAd(String str) throws MMException {
        super(str);
    }

    public Context getContext() {
        if (this.contextRef == null) {
            return null;
        }
        return this.contextRef.get();
    }

    public Map<String, Object> getAdPlacementMetaDataMap() {
        if (this.interstitialAdMetadata == null) {
            return null;
        }
        return this.interstitialAdMetadata.toMap(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0070, code lost:
        r4.playList = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0073, code lost:
        if (r6 != null) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0075, code lost:
        r6 = new com.millennialmedia.InterstitialAd.InterstitialAdMetadata();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007a, code lost:
        r5 = getRequestState();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0080, code lost:
        if (r4.placementRequestTimeoutRunnable == null) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0082, code lost:
        r4.placementRequestTimeoutRunnable.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0087, code lost:
        r0 = com.millennialmedia.internal.Handshake.getInterstitialTimeout();
        r4.placementRequestTimeoutRunnable = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(new com.millennialmedia.InterstitialAd.AnonymousClass1(r4), (long) r0);
        r1 = r6.getImpressionGroup();
        com.millennialmedia.internal.playlistserver.PlayListServer.loadPlayList(r6.toMap(r4), new com.millennialmedia.InterstitialAd.AnonymousClass2(r4), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a7, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void load(android.content.Context r5, com.millennialmedia.InterstitialAd.InterstitialAdMetadata r6) {
        /*
            r4 = this;
            boolean r0 = r4.isDestroyed()
            if (r0 == 0) goto L_0x0007
            return
        L_0x0007:
            java.lang.String r0 = com.millennialmedia.InterstitialAd.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Loading playlist for placement ID: "
            r1.append(r2)
            java.lang.String r2 = r4.placementId
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.millennialmedia.MMLog.i(r0, r1)
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r5)
            r4.contextRef = r0
            r4.interstitialAdMetadata = r6
            monitor-enter(r4)
            java.lang.String r5 = r4.placementState     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = "idle"
            boolean r5 = r5.equals(r0)     // Catch:{ all -> 0x00a8 }
            if (r5 != 0) goto L_0x006b
            java.lang.String r5 = r4.placementState     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = "load_failed"
            boolean r5 = r5.equals(r0)     // Catch:{ all -> 0x00a8 }
            if (r5 != 0) goto L_0x006b
            java.lang.String r5 = r4.placementState     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = "expired"
            boolean r5 = r5.equals(r0)     // Catch:{ all -> 0x00a8 }
            if (r5 != 0) goto L_0x006b
            java.lang.String r5 = r4.placementState     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = "show_failed"
            boolean r5 = r5.equals(r0)     // Catch:{ all -> 0x00a8 }
            if (r5 != 0) goto L_0x006b
            java.lang.String r5 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            r6.<init>()     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = "Unable to load interstitial ad, state is invalid: "
            r6.append(r0)     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = r4.placementState     // Catch:{ all -> 0x00a8 }
            r6.append(r0)     // Catch:{ all -> 0x00a8 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x00a8 }
            com.millennialmedia.MMLog.w(r5, r6)     // Catch:{ all -> 0x00a8 }
            monitor-exit(r4)     // Catch:{ all -> 0x00a8 }
            return
        L_0x006b:
            java.lang.String r5 = "loading_play_list"
            r4.placementState = r5     // Catch:{ all -> 0x00a8 }
            monitor-exit(r4)     // Catch:{ all -> 0x00a8 }
            r5 = 0
            r4.playList = r5
            if (r6 != 0) goto L_0x007a
            com.millennialmedia.InterstitialAd$InterstitialAdMetadata r6 = new com.millennialmedia.InterstitialAd$InterstitialAdMetadata
            r6.<init>()
        L_0x007a:
            com.millennialmedia.internal.AdPlacement$RequestState r5 = r4.getRequestState()
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r0 = r4.placementRequestTimeoutRunnable
            if (r0 == 0) goto L_0x0087
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r0 = r4.placementRequestTimeoutRunnable
            r0.cancel()
        L_0x0087:
            int r0 = com.millennialmedia.internal.Handshake.getInterstitialTimeout()
            com.millennialmedia.InterstitialAd$1 r1 = new com.millennialmedia.InterstitialAd$1
            r1.<init>(r5)
            long r2 = (long) r0
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r1 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r1, r2)
            r4.placementRequestTimeoutRunnable = r1
            java.lang.String r1 = r6.getImpressionGroup()
            java.util.Map r6 = r6.toMap(r4)
            com.millennialmedia.InterstitialAd$2 r2 = new com.millennialmedia.InterstitialAd$2
            r2.<init>(r5, r1)
            com.millennialmedia.internal.playlistserver.PlayListServer.loadPlayList(r6, r2, r0)
            return
        L_0x00a8:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00a8 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.load(android.content.Context, com.millennialmedia.InterstitialAd$InterstitialAdMetadata):void");
    }

    public static void requestBid(String str, InterstitialAdMetadata interstitialAdMetadata2, BidRequestListener bidRequestListener) throws MMException {
        AdPlacement.requestBid(str, interstitialAdMetadata2, bidRequestListener);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003a, code lost:
        if (r6.playList.hasNext() != false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
        if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0042, code lost:
        com.millennialmedia.MMLog.d(com.millennialmedia.InterstitialAd.TAG, "Unable to find ad adapter in play list");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        onLoadFailed(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
        r7 = com.millennialmedia.internal.AdPlacementReporter.getPlayListItemReporter(r7.getAdPlacementReporter());
        r6.nextInterstitialAdAdapter = (com.millennialmedia.internal.adadapters.InterstitialAdapter) r6.playList.getNextAdAdapter(r6, r7);
        r1 = r6.contextRef.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0069, code lost:
        if (r6.nextInterstitialAdAdapter == null) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006b, code lost:
        if (r1 == null) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006d, code lost:
        r2 = r6.nextInterstitialAdAdapter.requestTimeout;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0071, code lost:
        if (r2 <= 0) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0075, code lost:
        if (r6.adAdapterRequestTimeoutRunnable == null) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0077, code lost:
        r6.adAdapterRequestTimeoutRunnable.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007c, code lost:
        r6.adAdapterRequestTimeoutRunnable = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(new com.millennialmedia.InterstitialAd.AnonymousClass3(r6), (long) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0088, code lost:
        r6.nextInterstitialAdAdapter.init(r1, new com.millennialmedia.InterstitialAd.AnonymousClass4(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0093, code lost:
        com.millennialmedia.internal.AdPlacementReporter.reportPlayListItem(r0.getAdPlacementReporter(), r7);
        onAdAdapterLoadFailed(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadAdAdapter(com.millennialmedia.internal.AdPlacement.RequestState r7) {
        /*
            r6 = this;
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r7.copy()
            monitor-enter(r6)
            boolean r1 = r6.doPendingDestroy()     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x000d
            monitor-exit(r6)     // Catch:{ all -> 0x00a0 }
            return
        L_0x000d:
            com.millennialmedia.internal.AdPlacement$RequestState r1 = r6.currentRequestState     // Catch:{ all -> 0x00a0 }
            boolean r1 = r1.compareRequest(r0)     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x009e
            java.lang.String r1 = r6.placementState     // Catch:{ all -> 0x00a0 }
            java.lang.String r2 = "play_list_loaded"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x00a0 }
            if (r1 != 0) goto L_0x002a
            java.lang.String r1 = r6.placementState     // Catch:{ all -> 0x00a0 }
            java.lang.String r2 = "ad_adapter_load_failed"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x00a0 }
            if (r1 != 0) goto L_0x002a
            goto L_0x009e
        L_0x002a:
            java.lang.String r1 = "loading_ad_adapter"
            r6.placementState = r1     // Catch:{ all -> 0x00a0 }
            r0.getItemHash()     // Catch:{ all -> 0x00a0 }
            r6.currentRequestState = r0     // Catch:{ all -> 0x00a0 }
            monitor-exit(r6)     // Catch:{ all -> 0x00a0 }
            com.millennialmedia.internal.PlayList r1 = r6.playList
            boolean r1 = r1.hasNext()
            if (r1 != 0) goto L_0x004d
            boolean r7 = com.millennialmedia.MMLog.isDebugEnabled()
            if (r7 == 0) goto L_0x0049
            java.lang.String r7 = com.millennialmedia.InterstitialAd.TAG
            java.lang.String r1 = "Unable to find ad adapter in play list"
            com.millennialmedia.MMLog.d(r7, r1)
        L_0x0049:
            r6.onLoadFailed(r0)
            return
        L_0x004d:
            com.millennialmedia.internal.AdPlacementReporter r7 = r7.getAdPlacementReporter()
            com.millennialmedia.internal.AdPlacementReporter$PlayListItemReporter r7 = com.millennialmedia.internal.AdPlacementReporter.getPlayListItemReporter(r7)
            com.millennialmedia.internal.PlayList r1 = r6.playList
            com.millennialmedia.internal.adadapters.AdAdapter r1 = r1.getNextAdAdapter(r6, r7)
            com.millennialmedia.internal.adadapters.InterstitialAdapter r1 = (com.millennialmedia.internal.adadapters.InterstitialAdapter) r1
            r6.nextInterstitialAdAdapter = r1
            java.lang.ref.WeakReference<android.content.Context> r1 = r6.contextRef
            java.lang.Object r1 = r1.get()
            android.content.Context r1 = (android.content.Context) r1
            com.millennialmedia.internal.adadapters.InterstitialAdapter r2 = r6.nextInterstitialAdAdapter
            if (r2 == 0) goto L_0x0093
            if (r1 == 0) goto L_0x0093
            com.millennialmedia.internal.adadapters.InterstitialAdapter r2 = r6.nextInterstitialAdAdapter
            int r2 = r2.requestTimeout
            if (r2 <= 0) goto L_0x0088
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = r6.adAdapterRequestTimeoutRunnable
            if (r3 == 0) goto L_0x007c
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = r6.adAdapterRequestTimeoutRunnable
            r3.cancel()
        L_0x007c:
            com.millennialmedia.InterstitialAd$3 r3 = new com.millennialmedia.InterstitialAd$3
            r3.<init>(r0, r7)
            long r4 = (long) r2
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r2 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r3, r4)
            r6.adAdapterRequestTimeoutRunnable = r2
        L_0x0088:
            com.millennialmedia.internal.adadapters.InterstitialAdapter r2 = r6.nextInterstitialAdAdapter
            com.millennialmedia.InterstitialAd$4 r3 = new com.millennialmedia.InterstitialAd$4
            r3.<init>(r0, r7)
            r2.init(r1, r3)
            goto L_0x009d
        L_0x0093:
            com.millennialmedia.internal.AdPlacementReporter r1 = r0.getAdPlacementReporter()
            com.millennialmedia.internal.AdPlacementReporter.reportPlayListItem(r1, r7)
            r6.onAdAdapterLoadFailed(r0)
        L_0x009d:
            return
        L_0x009e:
            monitor-exit(r6)     // Catch:{ all -> 0x00a0 }
            return
        L_0x00a0:
            r7 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00a0 }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.loadAdAdapter(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    public void show(Context context) throws MMException {
        show(context, null);
    }

    public void show(Context context, AdPlacement.DisplayOptions displayOptions) throws MMException {
        if (!isDestroyed()) {
            String str = null;
            if (context == null) {
                throw new MMException("Unable to show interstitial, specified context cannot be null");
            }
            synchronized (this) {
                if (!this.placementState.equals("loaded")) {
                    str = "Unable to show interstitial ad, state is not valid: " + this.placementState;
                } else {
                    this.placementState = STATE_SHOWING;
                }
            }
            if (str != null) {
                onShowFailed(new InterstitialErrorStatus(4, str));
                return;
            }
            stopExpirationTimer();
            this.currentInterstitialAdAdapter.show(context, displayOptions);
        }
    }

    public void setListener(InterstitialListener interstitialListener2) {
        if (!isDestroyed()) {
            this.interstitialListener = interstitialListener2;
        }
    }

    public boolean isReady() {
        if (isDestroyed()) {
            return false;
        }
        return this.placementState.equals("loaded");
    }

    public boolean hasExpired() {
        if (isDestroyed()) {
            return false;
        }
        return this.placementState.equals(STATE_EXPIRED);
    }

    public CreativeInfo getCreativeInfo() {
        if (this.currentInterstitialAdAdapter != null) {
            return this.currentInterstitialAdAdapter.getCreativeInfo();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void setCurrentAdAdapter(InterstitialAdapter interstitialAdapter) {
        if (!(this.currentInterstitialAdAdapter == null || this.currentInterstitialAdAdapter == interstitialAdapter)) {
            this.currentInterstitialAdAdapter.release();
        }
        this.currentInterstitialAdAdapter = interstitialAdapter;
    }

    private void stopRequestTimeoutTimers() {
        if (this.placementRequestTimeoutRunnable != null) {
            this.placementRequestTimeoutRunnable.cancel();
            this.placementRequestTimeoutRunnable = null;
        }
        if (this.adAdapterRequestTimeoutRunnable != null) {
            this.adAdapterRequestTimeoutRunnable.cancel();
            this.adAdapterRequestTimeoutRunnable = null;
        }
    }

    private void startExpirationTimer(AdPlacement.RequestState requestState) {
        stopExpirationTimer();
        int interstitialExpirationDuration = Handshake.getInterstitialExpirationDuration();
        if (interstitialExpirationDuration > 0) {
            this.expirationRunnable = ThreadUtils.runOnWorkerThreadDelayed(new ExpirationRunnable(this, requestState), (long) interstitialExpirationDuration);
        }
    }

    private void stopExpirationTimer() {
        if (this.expirationRunnable != null) {
            this.expirationRunnable.cancel();
            this.expirationRunnable = null;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAdAdapterLoadFailed(com.millennialmedia.internal.AdPlacement.RequestState r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x0053 }
            boolean r0 = r0.compare(r3)     // Catch:{ all -> 0x0053 }
            if (r0 != 0) goto L_0x0018
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0053 }
            if (r3 == 0) goto L_0x0016
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0053 }
            java.lang.String r0 = "onAdAdapterLoadFailed called but load state is not valid"
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0053 }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x0018:
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0053 }
            if (r0 != 0) goto L_0x0042
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0053 }
            if (r3 == 0) goto L_0x0040
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0053 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0053 }
            r0.<init>()     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = "onAdAdapterLoadFailed called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = r2.placementState     // Catch:{ all -> 0x0053 }
            r0.append(r1)     // Catch:{ all -> 0x0053 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0053 }
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0053 }
        L_0x0040:
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x0042:
            boolean r0 = r2.doPendingDestroy()     // Catch:{ all -> 0x0053 }
            if (r0 == 0) goto L_0x004a
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            return
        L_0x004a:
            java.lang.String r0 = "ad_adapter_load_failed"
            r2.placementState = r0     // Catch:{ all -> 0x0053 }
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            r2.loadAdAdapter(r3)
            return
        L_0x0053:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onAdAdapterLoadFailed(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004f, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InterstitialAd.TAG, "Load succeeded");
        stopRequestTimeoutTimers();
        startExpirationTimer(r3);
        com.millennialmedia.internal.AdPlacementReporter.reportPlayList(r3.getAdPlacementReporter());
        r3 = r2.interstitialListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0065, code lost:
        if (r3 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0067, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InterstitialAd.AnonymousClass5(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLoadSucceeded(com.millennialmedia.internal.AdPlacement.RequestState r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x0070 }
            boolean r0 = r0.compare(r3)     // Catch:{ all -> 0x0070 }
            if (r0 != 0) goto L_0x0018
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0070 }
            if (r3 == 0) goto L_0x0016
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0070 }
            java.lang.String r0 = "onLoadSucceeded called but load state is not valid"
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0070 }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x0070 }
            return
        L_0x0018:
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x0070 }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0070 }
            if (r0 != 0) goto L_0x0042
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0070 }
            if (r3 == 0) goto L_0x0040
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0070 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0070 }
            r0.<init>()     // Catch:{ all -> 0x0070 }
            java.lang.String r1 = "onLoadSucceeded called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x0070 }
            java.lang.String r1 = r2.placementState     // Catch:{ all -> 0x0070 }
            r0.append(r1)     // Catch:{ all -> 0x0070 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0070 }
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0070 }
        L_0x0040:
            monitor-exit(r2)     // Catch:{ all -> 0x0070 }
            return
        L_0x0042:
            boolean r0 = r2.doPendingDestroy()     // Catch:{ all -> 0x0070 }
            if (r0 == 0) goto L_0x004a
            monitor-exit(r2)     // Catch:{ all -> 0x0070 }
            return
        L_0x004a:
            java.lang.String r0 = "loaded"
            r2.placementState = r0     // Catch:{ all -> 0x0070 }
            monitor-exit(r2)     // Catch:{ all -> 0x0070 }
            java.lang.String r0 = com.millennialmedia.InterstitialAd.TAG
            java.lang.String r1 = "Load succeeded"
            com.millennialmedia.MMLog.i(r0, r1)
            r2.stopRequestTimeoutTimers()
            r2.startExpirationTimer(r3)
            com.millennialmedia.internal.AdPlacementReporter r3 = r3.getAdPlacementReporter()
            com.millennialmedia.internal.AdPlacementReporter.reportPlayList(r3)
            com.millennialmedia.InterstitialAd$InterstitialListener r3 = r2.interstitialListener
            if (r3 == 0) goto L_0x006f
            com.millennialmedia.InterstitialAd$5 r0 = new com.millennialmedia.InterstitialAd$5
            r0.<init>(r3)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x006f:
            return
        L_0x0070:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0070 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onLoadSucceeded(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0053, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0063, code lost:
        r3 = com.millennialmedia.InterstitialAd.TAG;
        com.millennialmedia.MMLog.w(r3, "Load failed for placement ID: " + r2.placementId + ". If this warning persists please check your placement configuration.");
        r3 = r2.interstitialListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0082, code lost:
        if (r3 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0084, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InterstitialAd.AnonymousClass6(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLoadFailed(com.millennialmedia.internal.AdPlacement.RequestState r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.doPendingDestroy()     // Catch:{ all -> 0x008d }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r2)     // Catch:{ all -> 0x008d }
            return
        L_0x0009:
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x008d }
            boolean r0 = r0.compareRequest(r3)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0020
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x008d }
            if (r3 == 0) goto L_0x001e
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x008d }
            java.lang.String r0 = "onLoadFailed called but load state is not valid"
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x008d }
        L_0x001e:
            monitor-exit(r2)     // Catch:{ all -> 0x008d }
            return
        L_0x0020:
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "loading_ad_adapter"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0054
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "loading_play_list"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0054
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x008d }
            if (r3 == 0) goto L_0x0052
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r0.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "onLoadFailed called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r1 = r2.placementState     // Catch:{ all -> 0x008d }
            r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008d }
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x008d }
        L_0x0052:
            monitor-exit(r2)     // Catch:{ all -> 0x008d }
            return
        L_0x0054:
            java.lang.String r0 = "load_failed"
            r2.placementState = r0     // Catch:{ all -> 0x008d }
            r2.stopRequestTimeoutTimers()     // Catch:{ all -> 0x008d }
            com.millennialmedia.internal.AdPlacementReporter r3 = r3.getAdPlacementReporter()     // Catch:{ all -> 0x008d }
            com.millennialmedia.internal.AdPlacementReporter.reportPlayList(r3)     // Catch:{ all -> 0x008d }
            monitor-exit(r2)     // Catch:{ all -> 0x008d }
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Load failed for placement ID: "
            r0.append(r1)
            java.lang.String r1 = r2.placementId
            r0.append(r1)
            java.lang.String r1 = ". If this warning persists please check your placement configuration."
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.millennialmedia.MMLog.w(r3, r0)
            com.millennialmedia.InterstitialAd$InterstitialListener r3 = r2.interstitialListener
            if (r3 == 0) goto L_0x008c
            com.millennialmedia.InterstitialAd$6 r0 = new com.millennialmedia.InterstitialAd$6
            r0.<init>(r3)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x008c:
            return
        L_0x008d:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x008d }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onLoadFailed(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InterstitialAd.TAG, "Ad shown");
        r2 = r1.interstitialListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InterstitialAd.AnonymousClass7(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onShown(com.millennialmedia.internal.AdPlacement.RequestState r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r1.doPendingDestroy()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x0041 }
            return
        L_0x0009:
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x0041 }
            boolean r0 = r0.compare(r2)     // Catch:{ all -> 0x0041 }
            if (r0 != 0) goto L_0x0020
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x001e
            java.lang.String r2 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0041 }
            java.lang.String r0 = "onShown called but load state is not valid"
            com.millennialmedia.MMLog.d(r2, r0)     // Catch:{ all -> 0x0041 }
        L_0x001e:
            monitor-exit(r1)     // Catch:{ all -> 0x0041 }
            return
        L_0x0020:
            java.lang.String r0 = "shown"
            r1.placementState = r0     // Catch:{ all -> 0x0041 }
            com.millennialmedia.internal.AdPlacementReporter r2 = r2.getAdPlacementReporter()     // Catch:{ all -> 0x0041 }
            r0 = 0
            com.millennialmedia.internal.AdPlacementReporter.setDisplayed(r2, r0)     // Catch:{ all -> 0x0041 }
            monitor-exit(r1)     // Catch:{ all -> 0x0041 }
            java.lang.String r2 = com.millennialmedia.InterstitialAd.TAG
            java.lang.String r0 = "Ad shown"
            com.millennialmedia.MMLog.i(r2, r0)
            com.millennialmedia.InterstitialAd$InterstitialListener r2 = r1.interstitialListener
            if (r2 == 0) goto L_0x0040
            com.millennialmedia.InterstitialAd$7 r0 = new com.millennialmedia.InterstitialAd$7
            r0.<init>(r2)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x0040:
            return
        L_0x0041:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0041 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onShown(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0014, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InterstitialAd.TAG, "Ad show failed");
        r0 = r2.interstitialListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InterstitialAd.AnonymousClass8(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onShowFailed(final com.millennialmedia.InterstitialAd.InterstitialErrorStatus r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.doPendingDestroy()     // Catch:{ all -> 0x0028 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r2)     // Catch:{ all -> 0x0028 }
            return
        L_0x0009:
            java.lang.String r0 = r2.placementState     // Catch:{ all -> 0x0028 }
            java.lang.String r1 = "showing"
            if (r0 != r1) goto L_0x0013
            java.lang.String r0 = "show_failed"
            r2.placementState = r0     // Catch:{ all -> 0x0028 }
        L_0x0013:
            monitor-exit(r2)     // Catch:{ all -> 0x0028 }
            java.lang.String r0 = com.millennialmedia.InterstitialAd.TAG
            java.lang.String r1 = "Ad show failed"
            com.millennialmedia.MMLog.i(r0, r1)
            com.millennialmedia.InterstitialAd$InterstitialListener r0 = r2.interstitialListener
            if (r0 == 0) goto L_0x0027
            com.millennialmedia.InterstitialAd$8 r1 = new com.millennialmedia.InterstitialAd$8
            r1.<init>(r0, r3)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r1)
        L_0x0027:
            return
        L_0x0028:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0028 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onShowFailed(com.millennialmedia.InterstitialAd$InterstitialErrorStatus):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InterstitialAd.TAG, "Ad closed");
        r2 = r1.interstitialListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
        if (r2 == null) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InterstitialAd.AnonymousClass9(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        setCurrentAdAdapter(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClosed(com.millennialmedia.internal.AdPlacement.RequestState r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x0035 }
            boolean r2 = r0.compare(r2)     // Catch:{ all -> 0x0035 }
            if (r2 != 0) goto L_0x0018
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0035 }
            if (r2 == 0) goto L_0x0016
            java.lang.String r2 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0035 }
            java.lang.String r0 = "onClosed called but load state is not valid"
            com.millennialmedia.MMLog.d(r2, r0)     // Catch:{ all -> 0x0035 }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            return
        L_0x0018:
            java.lang.String r2 = "idle"
            r1.placementState = r2     // Catch:{ all -> 0x0035 }
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            java.lang.String r2 = com.millennialmedia.InterstitialAd.TAG
            java.lang.String r0 = "Ad closed"
            com.millennialmedia.MMLog.i(r2, r0)
            com.millennialmedia.InterstitialAd$InterstitialListener r2 = r1.interstitialListener
            if (r2 == 0) goto L_0x0030
            com.millennialmedia.InterstitialAd$9 r0 = new com.millennialmedia.InterstitialAd$9
            r0.<init>(r2)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x0030:
            r2 = 0
            r1.setCurrentAdAdapter(r2)
            return
        L_0x0035:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onClosed(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    public void onClicked(AdPlacement.RequestState requestState) {
        MMLog.i(TAG, "Ad clicked");
        AdPlacementReporter.setClicked(requestState.getAdPlacementReporter());
        final InterstitialListener interstitialListener2 = this.interstitialListener;
        if (interstitialListener2 != null) {
            ThreadUtils.runOffUiThread(new Runnable() {
                public void run() {
                    interstitialListener2.onClicked(InterstitialAd.this);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InterstitialAd.TAG, "Ad left application");
        r2 = r1.interstitialListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0022, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InterstitialAd.AnonymousClass11(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAdLeftApplication(com.millennialmedia.internal.AdPlacement.RequestState r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x002d }
            boolean r2 = r0.compare(r2)     // Catch:{ all -> 0x002d }
            if (r2 != 0) goto L_0x0018
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x002d }
            if (r2 == 0) goto L_0x0016
            java.lang.String r2 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x002d }
            java.lang.String r0 = "onAdLeftApplication called but load state is not valid"
            com.millennialmedia.MMLog.d(r2, r0)     // Catch:{ all -> 0x002d }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
            return
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
            java.lang.String r2 = com.millennialmedia.InterstitialAd.TAG
            java.lang.String r0 = "Ad left application"
            com.millennialmedia.MMLog.i(r2, r0)
            com.millennialmedia.InterstitialAd$InterstitialListener r2 = r1.interstitialListener
            if (r2 == 0) goto L_0x002c
            com.millennialmedia.InterstitialAd$11 r0 = new com.millennialmedia.InterstitialAd$11
            r0.<init>(r2)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x002c:
            return
        L_0x002d:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onAdLeftApplication(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0051, code lost:
        com.millennialmedia.MMLog.i(com.millennialmedia.InterstitialAd.TAG, "Ad expired");
        releaseAdapters();
        r3 = r2.interstitialListener;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005d, code lost:
        if (r3 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005f, code lost:
        com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(new com.millennialmedia.InterstitialAd.AnonymousClass12(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onExpired(com.millennialmedia.internal.AdPlacement.RequestState r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r2.currentRequestState     // Catch:{ all -> 0x0068 }
            boolean r3 = r0.compare(r3)     // Catch:{ all -> 0x0068 }
            if (r3 != 0) goto L_0x0018
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0068 }
            if (r3 == 0) goto L_0x0016
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0068 }
            java.lang.String r0 = "onExpired called but load state is not valid"
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0068 }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x0068 }
            return
        L_0x0018:
            java.lang.String r3 = r2.placementState     // Catch:{ all -> 0x0068 }
            java.lang.String r0 = "loaded"
            boolean r3 = r3.equals(r0)     // Catch:{ all -> 0x0068 }
            if (r3 != 0) goto L_0x004c
            java.lang.String r3 = r2.placementState     // Catch:{ all -> 0x0068 }
            java.lang.String r0 = "show_failed"
            boolean r3 = r3.equals(r0)     // Catch:{ all -> 0x0068 }
            if (r3 != 0) goto L_0x004c
            boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0068 }
            if (r3 == 0) goto L_0x004a
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0068 }
            r0.<init>()     // Catch:{ all -> 0x0068 }
            java.lang.String r1 = "onExpired called but placement state is not valid: "
            r0.append(r1)     // Catch:{ all -> 0x0068 }
            java.lang.String r1 = r2.placementState     // Catch:{ all -> 0x0068 }
            r0.append(r1)     // Catch:{ all -> 0x0068 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0068 }
            com.millennialmedia.MMLog.d(r3, r0)     // Catch:{ all -> 0x0068 }
        L_0x004a:
            monitor-exit(r2)     // Catch:{ all -> 0x0068 }
            return
        L_0x004c:
            java.lang.String r3 = "expired"
            r2.placementState = r3     // Catch:{ all -> 0x0068 }
            monitor-exit(r2)     // Catch:{ all -> 0x0068 }
            java.lang.String r3 = com.millennialmedia.InterstitialAd.TAG
            java.lang.String r0 = "Ad expired"
            com.millennialmedia.MMLog.i(r3, r0)
            r2.releaseAdapters()
            com.millennialmedia.InterstitialAd$InterstitialListener r3 = r2.interstitialListener
            if (r3 == 0) goto L_0x0067
            com.millennialmedia.InterstitialAd$12 r0 = new com.millennialmedia.InterstitialAd$12
            r0.<init>(r3)
            com.millennialmedia.internal.utils.ThreadUtils.runOffUiThread(r0)
        L_0x0067:
            return
        L_0x0068:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0068 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onExpired(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }

    private boolean isLoading() {
        return !this.placementState.equals("idle") && !this.placementState.equals("load_failed") && !this.placementState.equals("loaded") && !this.placementState.equals(STATE_EXPIRED) && !this.placementState.equals("destroyed") && !this.placementState.equals(STATE_SHOW_FAILED) && !this.placementState.equals(STATE_SHOWN);
    }

    /* access modifiers changed from: protected */
    public boolean canDestroyNow() {
        return !isLoading() && !this.placementState.equals(STATE_SHOWING);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.interstitialListener = null;
        this.incentivizedEventListener = null;
        this.interstitialAdMetadata = null;
        stopRequestTimeoutTimers();
        if (this.expirationRunnable != null) {
            this.expirationRunnable.cancel();
            this.expirationRunnable = null;
        }
        releaseAdapters();
        this.playList = null;
    }

    private void releaseAdapters() {
        if (this.currentInterstitialAdAdapter != null) {
            this.currentInterstitialAdAdapter.release();
            this.currentInterstitialAdAdapter = null;
        }
        if (this.nextInterstitialAdAdapter != null) {
            this.nextInterstitialAdAdapter.release();
            this.nextInterstitialAdAdapter = null;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        stopRequestTimeoutTimers();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        if (r1.expirationRunnable == null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0024, code lost:
        r1.expirationRunnable.cancel();
        r1.expirationRunnable = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        releaseAdapters();
        r1.playList = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0030, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onUnload(com.millennialmedia.internal.AdPlacement.RequestState r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.millennialmedia.internal.AdPlacement$RequestState r0 = r1.currentRequestState     // Catch:{ all -> 0x0031 }
            boolean r0 = r0.compare(r2)     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x0018
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0031 }
            if (r2 == 0) goto L_0x0016
            java.lang.String r2 = com.millennialmedia.InterstitialAd.TAG     // Catch:{ all -> 0x0031 }
            java.lang.String r0 = "onAdLeftApplication called but load state is not valid"
            com.millennialmedia.MMLog.d(r2, r0)     // Catch:{ all -> 0x0031 }
        L_0x0016:
            monitor-exit(r1)     // Catch:{ all -> 0x0031 }
            return
        L_0x0018:
            r1.onClosed(r2)     // Catch:{ all -> 0x0031 }
            monitor-exit(r1)     // Catch:{ all -> 0x0031 }
            r1.stopRequestTimeoutTimers()
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r2 = r1.expirationRunnable
            r0 = 0
            if (r2 == 0) goto L_0x002b
            com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r2 = r1.expirationRunnable
            r2.cancel()
            r1.expirationRunnable = r0
        L_0x002b:
            r1.releaseAdapters()
            r1.playList = r0
            return
        L_0x0031:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0031 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.InterstitialAd.onUnload(com.millennialmedia.internal.AdPlacement$RequestState):void");
    }
}
