package com.millennialmedia;

public class TestInfo {
    public String bidder;
    public String creativeId;

    public void setBidder(String str) {
        this.bidder = str;
    }

    public void setCreativeId(String str) {
        this.creativeId = str;
    }
}
