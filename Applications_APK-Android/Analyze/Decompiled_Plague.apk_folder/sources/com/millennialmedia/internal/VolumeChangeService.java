package com.millennialmedia.internal;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.Settings;
import android.support.annotation.Nullable;
import com.millennialmedia.MMLog;

public class VolumeChangeService extends IntentService {
    /* access modifiers changed from: private */
    public static final String TAG = "VolumeChangeService";
    private VolumeChangeContentObserver contentObserver;
    /* access modifiers changed from: private */
    public int maxVolume;
    /* access modifiers changed from: private */
    public int volume;

    /* access modifiers changed from: protected */
    public void onHandleIntent(@Nullable Intent intent) {
    }

    public int onStartCommand(@Nullable Intent intent, int i, int i2) {
        return 1;
    }

    private class VolumeChangeContentObserver extends ContentObserver {
        private Handler handler;
        private HandlerThread handlerThread = new HandlerThread("VolumeChangeDispatcher");

        VolumeChangeContentObserver() {
            super(null);
            this.handlerThread.start();
            this.handler = new Handler(this.handlerThread.getLooper());
        }

        @SuppressLint({"DefaultLocale"})
        public void onChange(boolean z, Uri uri) {
            if (uri != null) {
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(VolumeChangeService.TAG, String.format("Setting changed for URI = %s", uri));
                }
                if (uri.toString().startsWith("content://settings/system/volume_music_")) {
                    dispatchVolumeChange(false);
                }
            }
        }

        /* access modifiers changed from: package-private */
        @SuppressLint({"DefaultLocale"})
        public void dispatchVolumeChange(boolean z) {
            final int streamVolume = ((AudioManager) VolumeChangeService.this.getSystemService("audio")).getStreamVolume(3);
            if (z) {
                int unused = VolumeChangeService.this.volume = -1;
            }
            if (MMLog.isDebugEnabled()) {
                MMLog.d(VolumeChangeService.TAG, String.format("Current volume vol = %d, last vol = %d", Integer.valueOf(streamVolume), Integer.valueOf(VolumeChangeService.this.volume)));
            }
            if (streamVolume != VolumeChangeService.this.volume) {
                final int access$100 = VolumeChangeService.this.volume;
                int unused2 = VolumeChangeService.this.volume = streamVolume;
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(VolumeChangeService.TAG, String.format("Dispatching volume change old vol = %d, new vol = %d", Integer.valueOf(access$100), Integer.valueOf(streamVolume)));
                }
                this.handler.post(new Runnable() {
                    public void run() {
                        VolumeChangeManager.updateVolume(access$100, streamVolume, VolumeChangeService.this.maxVolume);
                    }
                });
            }
        }

        /* access modifiers changed from: package-private */
        public void stopDispatchThread() {
            if (this.handlerThread != null) {
                this.handlerThread.quit();
            }
        }
    }

    public VolumeChangeService() {
        super(TAG);
    }

    @SuppressLint({"DefaultLocale"})
    public void onCreate() {
        super.onCreate();
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "VolumeChangeService created.");
        }
        if (VolumeChangeManager.isStarted()) {
            AudioManager audioManager = (AudioManager) getSystemService("audio");
            this.volume = audioManager.getStreamVolume(3);
            this.maxVolume = audioManager.getStreamMaxVolume(3);
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, String.format("VolumeChangeService created with initial vol = %d, max vol = %d", Integer.valueOf(this.volume), Integer.valueOf(this.maxVolume)));
            }
            this.contentObserver = new VolumeChangeContentObserver();
            this.contentObserver.dispatchVolumeChange(true);
            getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this.contentObserver);
            return;
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "VolumeChangeManager has not been started; shutting down until needed.");
        }
        stopSelf();
    }

    public void onDestroy() {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "VolumeChangeService destroyed.");
        }
        if (this.contentObserver != null) {
            this.contentObserver.stopDispatchThread();
            getContentResolver().unregisterContentObserver(this.contentObserver);
        }
        super.onDestroy();
    }
}
