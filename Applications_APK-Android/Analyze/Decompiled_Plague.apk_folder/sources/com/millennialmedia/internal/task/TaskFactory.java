package com.millennialmedia.internal.task;

import android.os.Build;
import com.millennialmedia.internal.task.geoipcheck.GeoIpCheckRequestJobSchedulerTask;
import com.millennialmedia.internal.task.geoipcheck.GeoIpCheckRequestTask;
import com.millennialmedia.internal.task.handshake.HandshakeRequestJobSchedulerTask;
import com.millennialmedia.internal.task.handshake.HandshakeRequestTask;
import com.millennialmedia.internal.task.reporting.PlacementReportingJobSchedulerTask;
import com.millennialmedia.internal.task.reporting.PlacementReportingTask;

public final class TaskFactory {
    private TaskFactory() {
    }

    public static Task createHandshakeRequestTask() {
        if (isJobSchedulerSupported()) {
            return new HandshakeRequestJobSchedulerTask();
        }
        return new HandshakeRequestTask();
    }

    public static Task createGeoIpCheckRequestTask() {
        if (isJobSchedulerSupported()) {
            return new GeoIpCheckRequestJobSchedulerTask();
        }
        return new GeoIpCheckRequestTask();
    }

    public static Task createAdPlacementReporterTask() {
        if (isJobSchedulerSupported()) {
            return new PlacementReportingJobSchedulerTask();
        }
        return new PlacementReportingTask();
    }

    private static boolean isJobSchedulerSupported() {
        return Build.VERSION.SDK_INT >= 23;
    }
}
