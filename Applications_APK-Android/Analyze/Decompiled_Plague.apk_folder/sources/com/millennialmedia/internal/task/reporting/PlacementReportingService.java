package com.millennialmedia.internal.task.reporting;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import com.millennialmedia.MMSDK;
import com.millennialmedia.internal.AdPlacementReporter;

@TargetApi(23)
public class PlacementReportingService extends JobService {
    /* access modifiers changed from: private */
    public ReportUploader reportUploader;

    private static class ReportUploader extends AsyncTask<Void, Void, Void> {
        private ReportUploader() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            AdPlacementReporter.uploadNow();
            return null;
        }
    }

    @SuppressLint({"StaticFieldLeak"})
    public boolean onStartJob(final JobParameters jobParameters) {
        if (!MMSDK.isInitialized()) {
            return false;
        }
        this.reportUploader = new ReportUploader() {
            /* access modifiers changed from: protected */
            public void onPostExecute(Void voidR) {
                PlacementReportingService.this.jobFinished(jobParameters, false);
                ReportUploader unused = PlacementReportingService.this.reportUploader = null;
            }
        };
        this.reportUploader.execute(new Void[0]);
        return true;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        if (this.reportUploader != null) {
            this.reportUploader.cancel(true);
        }
        return true;
    }
}
