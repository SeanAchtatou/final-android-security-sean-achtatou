package com.millennialmedia.internal.task;

public abstract class Task {
    public abstract void execute(long j);

    public void execute() {
        execute(0);
    }
}
