package com.millennialmedia.internal.task.geoipcheck;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import com.millennialmedia.MMSDK;
import com.millennialmedia.internal.UserPrivacy;

@TargetApi(23)
public class GeoIpCheckRequestService extends JobService {
    private GeoIpCheckRequester geoIpCheckRequester;

    private static class GeoIpCheckRequester extends AsyncTask<Void, Void, Void> {
        private GeoIpCheckRequester() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            UserPrivacy.geoIpCheck(false);
            return null;
        }
    }

    @SuppressLint({"StaticFieldLeak"})
    public boolean onStartJob(final JobParameters jobParameters) {
        if (!MMSDK.isInitialized()) {
            return false;
        }
        this.geoIpCheckRequester = new GeoIpCheckRequester() {
            /* access modifiers changed from: protected */
            public void onPostExecute(Void voidR) {
                GeoIpCheckRequestService.this.jobFinished(jobParameters, false);
            }
        };
        this.geoIpCheckRequester.execute(new Void[0]);
        return true;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        if (this.geoIpCheckRequester != null) {
            this.geoIpCheckRequester.cancel(true);
        }
        return true;
    }
}
