package com.millennialmedia.internal.task.geoipcheck;

import com.millennialmedia.internal.UserPrivacy;
import com.millennialmedia.internal.task.ThreadTask;
import com.millennialmedia.internal.utils.ThreadUtils;

public class GeoIpCheckRequestTask extends ThreadTask {
    private static final String TAG = "GeoIpCheckRequestTask";
    private static volatile ThreadUtils.ScheduledRunnable scheduledRunnable;

    /* access modifiers changed from: protected */
    public void executeCommand() {
        UserPrivacy.geoIpCheck(false);
    }

    /* access modifiers changed from: protected */
    public String getTagName() {
        return TAG;
    }

    /* access modifiers changed from: protected */
    public ThreadUtils.ScheduledRunnable getScheduledRunnable() {
        return scheduledRunnable;
    }

    /* access modifiers changed from: protected */
    public void setScheduledRunnable(ThreadUtils.ScheduledRunnable scheduledRunnable2) {
        scheduledRunnable = scheduledRunnable2;
    }
}
