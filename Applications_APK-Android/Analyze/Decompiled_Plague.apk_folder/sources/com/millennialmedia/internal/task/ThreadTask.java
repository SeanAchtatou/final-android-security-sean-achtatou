package com.millennialmedia.internal.task;

import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.ThreadUtils;

public abstract class ThreadTask extends Task {
    /* access modifiers changed from: protected */
    public abstract void executeCommand();

    /* access modifiers changed from: protected */
    public abstract ThreadUtils.ScheduledRunnable getScheduledRunnable();

    /* access modifiers changed from: protected */
    public abstract String getTagName();

    /* access modifiers changed from: protected */
    public abstract void setScheduledRunnable(ThreadUtils.ScheduledRunnable scheduledRunnable);

    public void execute(long j) {
        cancel();
        setScheduledRunnable(ThreadUtils.runOnWorkerThreadDelayed(new Runnable() {
            public void run() {
                ThreadTask.this.executeCommand();
                ThreadTask.this.setScheduledRunnable(null);
            }
        }, j));
    }

    private void cancel() {
        ThreadUtils.ScheduledRunnable scheduledRunnable = getScheduledRunnable();
        if (scheduledRunnable != null) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(getTagName(), "Cancelling task.");
            }
            scheduledRunnable.cancel();
            setScheduledRunnable(null);
        }
    }
}
