package com.millennialmedia.internal.task.geoipcheck;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import com.millennialmedia.internal.task.JobSchedulerTask;
import com.millennialmedia.internal.utils.EnvironmentUtils;

@TargetApi(23)
public class GeoIpCheckRequestJobSchedulerTask extends JobSchedulerTask {
    private static final int DEFAULT_JOB_ID = 19;
    private static final String JOB_ID_RESOURCE_NAME = "geoipcheck_job_id";
    private static final String JOB_ID_RESOURCE_TYPE = "integer";
    private static final String TAG = "GeoIpCheckRequestJobSchedulerTask";

    /* access modifiers changed from: protected */
    public int getDefaultJobId() {
        return 19;
    }

    /* access modifiers changed from: protected */
    public String getJobIdResourceName() {
        return JOB_ID_RESOURCE_NAME;
    }

    /* access modifiers changed from: protected */
    public String getJobIdResourceType() {
        return JOB_ID_RESOURCE_TYPE;
    }

    public void execute(long j) {
        Context applicationContext = EnvironmentUtils.getApplicationContext();
        JobScheduler jobScheduler = (JobScheduler) applicationContext.getSystemService("jobscheduler");
        JobInfo.Builder requiredNetworkType = new JobInfo.Builder(retrieveJobId(), new ComponentName(applicationContext, GeoIpCheckRequestService.class)).setRequiredNetworkType(1);
        if (j > 0) {
            requiredNetworkType.setMinimumLatency(j);
        }
        jobScheduler.schedule(requiredNetworkType.build());
    }

    /* access modifiers changed from: protected */
    public String getTagName() {
        return TAG;
    }
}
