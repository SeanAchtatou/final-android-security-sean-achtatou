package com.millennialmedia.internal.task.reporting;

import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.task.ThreadTask;
import com.millennialmedia.internal.utils.ThreadUtils;

public class PlacementReportingTask extends ThreadTask {
    private static final String TAG = "PlacementReportingTask";
    private static volatile ThreadUtils.ScheduledRunnable scheduledRunnable;

    /* access modifiers changed from: protected */
    public void executeCommand() {
        AdPlacementReporter.uploadNow();
    }

    /* access modifiers changed from: protected */
    public String getTagName() {
        return TAG;
    }

    /* access modifiers changed from: protected */
    public ThreadUtils.ScheduledRunnable getScheduledRunnable() {
        return scheduledRunnable;
    }

    /* access modifiers changed from: protected */
    public void setScheduledRunnable(ThreadUtils.ScheduledRunnable scheduledRunnable2) {
        scheduledRunnable = scheduledRunnable2;
    }
}
