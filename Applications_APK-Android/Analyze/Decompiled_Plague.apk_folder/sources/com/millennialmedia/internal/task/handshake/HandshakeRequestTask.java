package com.millennialmedia.internal.task.handshake;

import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.task.ThreadTask;
import com.millennialmedia.internal.utils.ThreadUtils;

public class HandshakeRequestTask extends ThreadTask {
    private static final String TAG = "HandshakeRequestTask";
    private static volatile ThreadUtils.ScheduledRunnable scheduledRunnable;

    /* access modifiers changed from: protected */
    public void executeCommand() {
        Handshake.request(false);
    }

    /* access modifiers changed from: protected */
    public String getTagName() {
        return TAG;
    }

    /* access modifiers changed from: protected */
    public ThreadUtils.ScheduledRunnable getScheduledRunnable() {
        return scheduledRunnable;
    }

    /* access modifiers changed from: protected */
    public void setScheduledRunnable(ThreadUtils.ScheduledRunnable scheduledRunnable2) {
        scheduledRunnable = scheduledRunnable2;
    }
}
