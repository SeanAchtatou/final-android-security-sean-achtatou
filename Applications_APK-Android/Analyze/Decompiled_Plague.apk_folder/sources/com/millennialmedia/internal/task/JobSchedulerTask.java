package com.millennialmedia.internal.task;

import android.util.TypedValue;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.EnvironmentUtils;

public abstract class JobSchedulerTask extends Task {
    /* access modifiers changed from: protected */
    public abstract int getDefaultJobId();

    /* access modifiers changed from: protected */
    public abstract String getJobIdResourceName();

    /* access modifiers changed from: protected */
    public abstract String getJobIdResourceType();

    /* access modifiers changed from: protected */
    public abstract String getTagName();

    /* access modifiers changed from: protected */
    public int retrieveJobId() {
        int defaultJobId = getDefaultJobId();
        if (EnvironmentUtils.resourceExists(getJobIdResourceName(), getJobIdResourceType())) {
            TypedValue typedValue = new TypedValue();
            EnvironmentUtils.getResourceValueFrom(getJobIdResourceName(), getJobIdResourceType(), typedValue, true);
            defaultJobId = typedValue.data;
        }
        if (MMLog.isDebugEnabled()) {
            String tagName = getTagName();
            MMLog.d(tagName, "The selected jobId for " + getJobIdResourceName() + " is: " + defaultJobId);
        }
        return defaultJobId;
    }
}
