package com.millennialmedia.internal;

import com.millennialmedia.MMLog;
import com.millennialmedia.internal.adwrapper.AdWrapper;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class PlayList {
    private static final String TAG = "PlayList";
    public static final String VERSION = "2";
    private int currentPlayListPosition = 0;
    public String handshakeConfig;
    public String placementId;
    public String placementName;
    private final List<AdWrapper> playListItems = new ArrayList();
    public String playListVersion;
    public boolean reportingEnabled = false;
    public String responseId;
    public String siteId;

    public void enableReporting() {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Enabling reporting for placement id <" + this.placementId + "> and playlist <" + this + ">");
        }
        this.reportingEnabled = true;
    }

    public void addItem(AdWrapper adWrapper) throws InvalidParameterException {
        if (adWrapper != null) {
            if (MMLog.isDebugEnabled()) {
                String str = TAG;
                MMLog.d(str, "Adding playlist item.\n\tPlaylist: " + this + "\n\tPlaylist item: " + adWrapper + "\n\tPlaylist item ID: " + adWrapper.itemId);
            }
            this.playListItems.add(adWrapper);
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Unable to add null playlist item");
        }
    }

    public AdWrapper getItem(int i) {
        if (this.playListItems.size() > i) {
            return this.playListItems.get(i);
        }
        return null;
    }

    public boolean hasNext() {
        return this.currentPlayListPosition < this.playListItems.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004d, code lost:
        if (r7 == null) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004f, code lost:
        r7.itemId = r1.itemId;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0057, code lost:
        if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0059, code lost:
        r2 = com.millennialmedia.internal.PlayList.TAG;
        com.millennialmedia.MMLog.d(r2, "Processing playlist item ID: " + r1.itemId);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0071, code lost:
        r6 = r1.getAdAdapter(r6, r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0075, code lost:
        if (r6 == null) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0077, code lost:
        r0.set(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007b, code lost:
        if (r1 == null) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007d, code lost:
        r6.setAdMetadata(r1.adMetadata);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0082, code lost:
        if (r7 == null) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0084, code lost:
        r7.status = r0.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008a, code lost:
        return r6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.millennialmedia.internal.adadapters.AdAdapter getNextAdAdapter(com.millennialmedia.internal.AdPlacement r6, com.millennialmedia.internal.AdPlacementReporter.PlayListItemReporter r7) {
        /*
            r5 = this;
            boolean r0 = com.millennialmedia.MMLog.isDebugEnabled()
            if (r0 == 0) goto L_0x0026
            java.lang.String r0 = com.millennialmedia.internal.PlayList.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Attempting to get ad adapter for placement.\n\tPlacement: "
            r1.append(r2)
            r1.append(r6)
            java.lang.String r2 = "\n\tPlacement ID: "
            r1.append(r2)
            java.lang.String r2 = r6.placementId
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.millennialmedia.MMLog.d(r0, r1)
        L_0x0026:
            java.util.concurrent.atomic.AtomicInteger r0 = new java.util.concurrent.atomic.AtomicInteger
            r1 = -3
            r0.<init>(r1)
            monitor-enter(r5)
            int r2 = r5.currentPlayListPosition     // Catch:{ all -> 0x008b }
            java.util.List<com.millennialmedia.internal.adwrapper.AdWrapper> r3 = r5.playListItems     // Catch:{ all -> 0x008b }
            int r3 = r3.size()     // Catch:{ all -> 0x008b }
            if (r2 < r3) goto L_0x003e
            if (r7 == 0) goto L_0x003b
            r7.status = r1     // Catch:{ all -> 0x008b }
        L_0x003b:
            r6 = 0
            monitor-exit(r5)     // Catch:{ all -> 0x008b }
            return r6
        L_0x003e:
            java.util.List<com.millennialmedia.internal.adwrapper.AdWrapper> r1 = r5.playListItems     // Catch:{ all -> 0x008b }
            int r2 = r5.currentPlayListPosition     // Catch:{ all -> 0x008b }
            int r3 = r2 + 1
            r5.currentPlayListPosition = r3     // Catch:{ all -> 0x008b }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x008b }
            com.millennialmedia.internal.adwrapper.AdWrapper r1 = (com.millennialmedia.internal.adwrapper.AdWrapper) r1     // Catch:{ all -> 0x008b }
            monitor-exit(r5)     // Catch:{ all -> 0x008b }
            if (r7 == 0) goto L_0x0053
            java.lang.String r2 = r1.itemId
            r7.itemId = r2
        L_0x0053:
            boolean r2 = com.millennialmedia.MMLog.isDebugEnabled()
            if (r2 == 0) goto L_0x0071
            java.lang.String r2 = com.millennialmedia.internal.PlayList.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Processing playlist item ID: "
            r3.append(r4)
            java.lang.String r4 = r1.itemId
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            com.millennialmedia.MMLog.d(r2, r3)
        L_0x0071:
            com.millennialmedia.internal.adadapters.AdAdapter r6 = r1.getAdAdapter(r6, r7, r0)
            if (r6 == 0) goto L_0x0082
            r2 = 1
            r0.set(r2)
            if (r1 == 0) goto L_0x0082
            com.millennialmedia.internal.AdMetadata r1 = r1.adMetadata
            r6.setAdMetadata(r1)
        L_0x0082:
            if (r7 == 0) goto L_0x008a
            int r0 = r0.get()
            r7.status = r0
        L_0x008a:
            return r6
        L_0x008b:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x008b }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.PlayList.getNextAdAdapter(com.millennialmedia.internal.AdPlacement, com.millennialmedia.internal.AdPlacementReporter$PlayListItemReporter):com.millennialmedia.internal.adadapters.AdAdapter");
    }
}
