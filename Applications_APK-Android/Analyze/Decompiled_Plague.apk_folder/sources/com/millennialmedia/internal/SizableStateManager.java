package com.millennialmedia.internal;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.millennialmedia.MMLog;
import com.millennialmedia.R;
import com.millennialmedia.internal.MMActivity;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.ViewUtils;
import java.lang.ref.WeakReference;

public class SizableStateManager {
    /* access modifiers changed from: private */
    public static final String TAG = "SizableStateManager";
    static final int closeAreaSize = EnvironmentUtils.getApplicationContext().getResources().getDimensionPixelSize(R.dimen.mmadsdk_mraid_resize_close_area_size);
    private ExpandStateManager expandStateManager;
    private ResizeContainer resizeContainer;
    /* access modifiers changed from: private */
    public RestoreState restoreState;
    /* access modifiers changed from: private */
    public SizableListener sizableListener;

    public static class ExpandParams {
        public int height;
        public boolean immersive;
        public int orientation;
        public boolean showLoadingSpinner;
        public boolean transparent;
        public String url;
        public int width;
    }

    public static class ResizeParams {
        boolean allowOffScreen;
        String customClosePosition;
        int height;
        int offsetX;
        int offsetY;
        int width;
    }

    public interface SizableListener {
        void onCollapsed();

        void onCollapsing();

        void onExpandFailed();

        void onExpanded();

        void onExpanding();

        void onResized(int i, int i2);

        void onResizing(int i, int i2);

        void onUnresized(int i, int i2);

        void onUnresizing(int i, int i2);
    }

    private enum SizableState {
        STATE_RESIZED,
        STATE_UNRESIZED,
        STATE_EXPANDED,
        STATE_COLLAPSED
    }

    private class RestoreState {
        /* access modifiers changed from: private */
        public ViewGroup.LayoutParams layoutParams;
        /* access modifiers changed from: private */
        public WeakReference<ViewGroup> parentContainerRef;
        /* access modifiers changed from: private */
        public Point pos;
        /* access modifiers changed from: private */
        public Point size;
        /* access modifiers changed from: private */
        public View view;

        private RestoreState() {
        }
    }

    public class ResizeContainer extends RelativeLayout {
        private View closeControl;
        private Rect contentRect;

        public ResizeContainer(Context context) {
            super(context);
        }

        public void attachCloseControl(Context context, String str) {
            if (this.closeControl == null) {
                this.closeControl = new View(context);
                this.closeControl.setBackgroundColor(0);
                this.closeControl.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        ResizeContainer.this.close();
                    }
                });
                ViewUtils.attachView(this, this.closeControl);
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(SizableStateManager.closeAreaSize, SizableStateManager.closeAreaSize);
            if (str.equals("top-right")) {
                layoutParams.addRule(11);
            } else if (str.equals("top-center")) {
                layoutParams.addRule(14);
            } else if (str.equals("bottom-left")) {
                layoutParams.addRule(12);
            } else if (str.equals("bottom-center")) {
                layoutParams.addRule(12);
                layoutParams.addRule(14);
            } else if (str.equals("bottom-right")) {
                layoutParams.addRule(12);
                layoutParams.addRule(11);
            } else if (str.equals("center")) {
                layoutParams.addRule(13);
            }
            this.closeControl.setLayoutParams(layoutParams);
            this.closeControl.bringToFront();
        }

        private Rect calculateCloseAreaRect(String str, Rect rect) {
            Rect rect2 = new Rect();
            if (str.contains("center")) {
                rect2.left = ((rect.left + rect.right) - SizableStateManager.closeAreaSize) / 2;
                if (str.equals("center")) {
                    rect2.top = ((rect.top + rect.bottom) - SizableStateManager.closeAreaSize) / 2;
                }
            }
            if (str.startsWith("top")) {
                rect2.top = rect.top;
            } else if (str.startsWith("bottom")) {
                rect2.top = rect.bottom - SizableStateManager.closeAreaSize;
            }
            if (str.endsWith("left")) {
                rect2.left = rect.left;
            } else if (str.endsWith("right")) {
                rect2.left = rect.right - SizableStateManager.closeAreaSize;
            }
            rect2.right = rect2.left + SizableStateManager.closeAreaSize;
            rect2.bottom = rect2.top + SizableStateManager.closeAreaSize;
            return rect2;
        }

        /* access modifiers changed from: package-private */
        public void shiftRectOnScreen(Rect rect, Rect rect2) {
            int i;
            int i2 = 0;
            if (rect.left < rect2.left) {
                i = rect2.left - rect.left;
            } else {
                i = rect.right > rect2.right ? rect2.right - rect.right : 0;
            }
            if (rect.top < rect2.top) {
                i2 = rect2.top - rect.top;
            } else if (rect.bottom > rect2.bottom) {
                i2 = rect2.bottom - rect.bottom;
            }
            rect.offset(i, i2);
        }

        public boolean resize(View view, ResizeParams resizeParams) {
            if (!ThreadUtils.isUiThread()) {
                MMLog.e(SizableStateManager.TAG, "resize must be called on the UI thread");
                return false;
            }
            if (this.contentRect == null) {
                this.contentRect = ViewUtils.getContentDimensions(view, null);
            }
            Rect rect = new Rect();
            if (SizableStateManager.this.restoreState == null) {
                Point viewPositionOnScreen = ViewUtils.getViewPositionOnScreen(view);
                rect.left = viewPositionOnScreen.x + resizeParams.offsetX;
                rect.top = viewPositionOnScreen.y + resizeParams.offsetY;
            } else {
                rect.left = SizableStateManager.this.restoreState.pos.x + resizeParams.offsetX;
                rect.top = SizableStateManager.this.restoreState.pos.y + resizeParams.offsetY;
            }
            rect.right = rect.left + resizeParams.width;
            rect.bottom = rect.top + resizeParams.height;
            if (!resizeParams.allowOffScreen) {
                shiftRectOnScreen(rect, this.contentRect);
                if (!this.contentRect.contains(rect)) {
                    MMLog.e(SizableStateManager.TAG, "Resized view would not appear on screen");
                    return false;
                }
            }
            if (!this.contentRect.contains(calculateCloseAreaRect(resizeParams.customClosePosition, rect))) {
                MMLog.e(SizableStateManager.TAG, "Close area would not appear on screen");
                return false;
            }
            SizableStateManager.this.sizableListener.onResizing(resizeParams.width, resizeParams.height);
            if (SizableStateManager.this.restoreState == null) {
                SizableStateManager.this.saveDefaultState(view);
                ViewGroup decorView = ViewUtils.getDecorView(view);
                if (decorView == null) {
                    MMLog.e(SizableStateManager.TAG, "Unable to resize to root view");
                    return false;
                }
                ViewUtils.attachView(this, view);
                ViewUtils.attachView(decorView, this);
                ViewGroup viewGroup = (ViewGroup) SizableStateManager.this.restoreState.parentContainerRef.get();
                if (viewGroup != null) {
                    viewGroup.setVisibility(4);
                }
            }
            view.setLayoutParams(new RelativeLayout.LayoutParams(resizeParams.width, resizeParams.height));
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            layoutParams.width = resizeParams.width;
            layoutParams.height = resizeParams.height;
            setLayoutParams(layoutParams);
            setTranslationX((float) rect.left);
            setTranslationY((float) rect.top);
            attachCloseControl(getContext(), resizeParams.customClosePosition);
            SizableStateManager.this.notifyStateWhenReady(view, SizableState.STATE_RESIZED);
            return true;
        }

        public void close() {
            if (!ThreadUtils.isUiThread()) {
                MMLog.e(SizableStateManager.TAG, "close must be called on the UI thread");
                return;
            }
            SizableStateManager.this.restoreDefaultState(true);
            ViewUtils.removeFromParent(this);
        }
    }

    public class ExpandStateManager {
        private static final long CLOSE_BUTTON_APPEARANCE_DELAY = 1100;
        /* access modifiers changed from: private */
        public ThreadUtils.ScheduledRunnable closeButtonShowRunnable = null;
        private ImageView closeControl;
        private ProgressBar loadingSpinner;
        /* access modifiers changed from: private */
        public MMActivity mmExpandActivity;

        public ExpandStateManager() {
        }

        /* access modifiers changed from: package-private */
        public void showLoadingSpinner() {
            if (this.mmExpandActivity != null) {
                if (this.loadingSpinner == null) {
                    this.loadingSpinner = new ProgressBar(this.mmExpandActivity.getRootView().getContext());
                    this.loadingSpinner.setIndeterminate(true);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams.addRule(13);
                    this.loadingSpinner.setLayoutParams(layoutParams);
                }
                ViewUtils.attachView(this.mmExpandActivity.getRootView(), this.loadingSpinner);
                this.loadingSpinner.bringToFront();
            }
        }

        /* access modifiers changed from: package-private */
        public void hideLoadingSpinner() {
            if (this.loadingSpinner != null) {
                this.loadingSpinner.setVisibility(8);
                ViewUtils.removeFromParent(this.loadingSpinner);
                this.loadingSpinner = null;
            }
        }

        /* access modifiers changed from: package-private */
        public void showCloseIndicatorDelay() {
            if (this.closeButtonShowRunnable == null) {
                this.closeButtonShowRunnable = ThreadUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        ThreadUtils.ScheduledRunnable unused = ExpandStateManager.this.closeButtonShowRunnable = null;
                        ExpandStateManager.this.showCloseIndicator();
                    }
                }, CLOSE_BUTTON_APPEARANCE_DELAY);
            }
        }

        /* access modifiers changed from: private */
        public void showCloseIndicator() {
            if (this.closeButtonShowRunnable == null && this.closeControl != null) {
                this.closeControl.setImageDrawable(this.closeControl.getResources().getDrawable(R.drawable.mmadsdk_close));
                this.closeControl.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        }

        /* access modifiers changed from: package-private */
        public void attachCloseControl() {
            if (this.closeControl == null) {
                this.closeControl = new ImageView(this.mmExpandActivity.getRootView().getContext());
                this.closeControl.setBackgroundColor(0);
                this.closeControl.setTag("mm_close_control");
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(SizableStateManager.closeAreaSize, SizableStateManager.closeAreaSize);
                layoutParams.addRule(11);
                this.closeControl.setLayoutParams(layoutParams);
                this.closeControl.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        ExpandStateManager.this.close();
                    }
                });
            }
            ViewUtils.attachView(this.mmExpandActivity.getRootView(), this.closeControl);
            this.closeControl.bringToFront();
        }

        /* access modifiers changed from: package-private */
        public void setOrientation(int i) {
            this.mmExpandActivity.setOrientation(i);
        }

        public boolean expand(final View view, final ExpandParams expandParams, MMActivity.MMActivityConfig mMActivityConfig) {
            if (!ThreadUtils.isUiThread()) {
                MMLog.e(SizableStateManager.TAG, "expand must be called on the UI thread");
                return false;
            }
            MMActivity.launch(view.getContext(), mMActivityConfig, new MMActivity.MMActivityListener() {
                public void onCreate(MMActivity mMActivity) {
                    boolean z;
                    if (ExpandStateManager.this.mmExpandActivity == null) {
                        SizableStateManager.this.sizableListener.onExpanding();
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(expandParams.width, expandParams.height);
                        layoutParams.addRule(13);
                        view.setLayoutParams(layoutParams);
                        z = true;
                        SizableStateManager.this.notifyStateWhenReady(view, SizableState.STATE_EXPANDED);
                    } else {
                        z = false;
                    }
                    MMActivity unused = ExpandStateManager.this.mmExpandActivity = mMActivity;
                    ViewUtils.attachView(mMActivity.getRootView(), view);
                    ExpandStateManager.this.attachCloseControl();
                    if (z) {
                        ExpandStateManager.this.showCloseIndicatorDelay();
                    } else {
                        ExpandStateManager.this.showCloseIndicator();
                    }
                    if (expandParams.showLoadingSpinner) {
                        ExpandStateManager.this.showLoadingSpinner();
                    }
                }

                public void onDestroy(MMActivity mMActivity) {
                    if (mMActivity.isFinishing()) {
                        SizableStateManager.this.restoreDefaultState(true);
                        MMActivity unused = ExpandStateManager.this.mmExpandActivity = null;
                    }
                }

                public void onLaunchFailed() {
                    SizableStateManager.this.sizableListener.onExpandFailed();
                }
            });
            return true;
        }

        public void close() {
            if (!ThreadUtils.isUiThread()) {
                MMLog.e(SizableStateManager.TAG, "close must be called on the UI thread");
            } else if (this.mmExpandActivity != null) {
                this.mmExpandActivity.finish();
            }
        }
    }

    public SizableStateManager(SizableListener sizableListener2) {
        this.sizableListener = sizableListener2;
    }

    public boolean isExpanded() {
        return this.expandStateManager != null;
    }

    public boolean isResized() {
        return this.resizeContainer != null;
    }

    public void saveDefaultState(View view) {
        if (this.restoreState == null) {
            this.restoreState = new RestoreState();
            View unused = this.restoreState.view = view;
            Point unused2 = this.restoreState.pos = ViewUtils.getViewPositionOnScreen(view);
            ViewGroup.LayoutParams unused3 = this.restoreState.layoutParams = view.getLayoutParams();
            Point unused4 = this.restoreState.size = new Point(view.getWidth(), view.getHeight());
            WeakReference unused5 = this.restoreState.parentContainerRef = new WeakReference(ViewUtils.getParentContainer(view));
        }
    }

    public void restoreDefaultState(boolean z) {
        if (this.restoreState != null) {
            ViewGroup viewGroup = (ViewGroup) this.restoreState.parentContainerRef.get();
            if (viewGroup != null) {
                if (z) {
                    if (this.expandStateManager != null) {
                        this.sizableListener.onCollapsing();
                        notifyStateWhenReady(this.restoreState.view, SizableState.STATE_COLLAPSED);
                    } else {
                        this.sizableListener.onUnresizing(this.restoreState.size.x, this.restoreState.size.y);
                        notifyStateWhenReady(this.restoreState.view, SizableState.STATE_UNRESIZED);
                    }
                }
                viewGroup.setVisibility(0);
                if (this.restoreState.layoutParams == null) {
                    if (MMLog.isDebugEnabled()) {
                        String str = TAG;
                        MMLog.d(str, "No layout params found for view being restored, creating new layout params based on original size: x<" + this.restoreState.size.x + ">, y<" + this.restoreState.size.y + ">");
                    }
                    ViewGroup.LayoutParams unused = this.restoreState.layoutParams = new ViewGroup.LayoutParams(this.restoreState.size.x, this.restoreState.size.y);
                }
                ViewUtils.attachView(viewGroup, this.restoreState.view, this.restoreState.layoutParams);
            }
            View unused2 = this.restoreState.view = null;
            this.restoreState = null;
        } else if (z) {
            this.sizableListener.onCollapsing();
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    SizableStateManager.this.sizableListener.onCollapsed();
                }
            });
        }
        this.resizeContainer = null;
        this.expandStateManager = null;
    }

    /* access modifiers changed from: private */
    public void notifyStateWhenReady(final View view, final SizableState sizableState) {
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                final int i9 = i3 - i;
                final int i10 = i4 - i2;
                if (i9 > 0 && i10 > 0) {
                    view.removeOnLayoutChangeListener(this);
                    ThreadUtils.postOnUiThread(new Runnable() {
                        public void run() {
                            if (sizableState == SizableState.STATE_RESIZED) {
                                SizableStateManager.this.sizableListener.onResized(i9, i10);
                            } else if (sizableState == SizableState.STATE_EXPANDED) {
                                SizableStateManager.this.sizableListener.onExpanded();
                            } else if (sizableState == SizableState.STATE_UNRESIZED) {
                                SizableStateManager.this.sizableListener.onUnresized(i9, i10);
                            } else if (sizableState == SizableState.STATE_COLLAPSED) {
                                SizableStateManager.this.sizableListener.onCollapsed();
                            }
                        }
                    });
                }
            }
        });
    }

    public void showLoadingSpinner(ExpandParams expandParams) {
        expandParams.showLoadingSpinner = true;
        if (this.expandStateManager != null) {
            this.expandStateManager.showLoadingSpinner();
        }
    }

    public void hideLoadingSpinner(ExpandParams expandParams) {
        expandParams.showLoadingSpinner = false;
        if (this.expandStateManager != null) {
            this.expandStateManager.hideLoadingSpinner();
        }
    }

    public void setOrientation(int i) {
        if (this.expandStateManager != null) {
            this.expandStateManager.setOrientation(i);
        }
    }

    public boolean resize(View view, ResizeParams resizeParams) {
        if (this.expandStateManager != null) {
            MMLog.e(TAG, "Cannot resize while expanded");
            return false;
        }
        boolean z = this.resizeContainer == null;
        if (z) {
            this.resizeContainer = new ResizeContainer(view.getContext());
        }
        if (this.resizeContainer.resize(view, resizeParams)) {
            return true;
        }
        if (z) {
            this.resizeContainer = null;
        }
        return false;
    }

    public boolean expand(View view, ExpandParams expandParams, boolean z) {
        if (this.expandStateManager != null) {
            MMLog.e(TAG, "Cannot expand while expanded");
            return false;
        }
        if (z) {
            saveDefaultState(view);
        } else {
            restoreDefaultState(false);
        }
        MMActivity.MMActivityConfig transparent = new MMActivity.MMActivityConfig().setImmersive(expandParams.immersive).setOrientation(expandParams.orientation).setTransparent(expandParams.transparent);
        this.expandStateManager = new ExpandStateManager();
        if (this.expandStateManager.expand(view, expandParams, transparent)) {
            return true;
        }
        this.expandStateManager = null;
        return false;
    }

    public boolean expand(View view, ExpandParams expandParams, MMActivity.MMActivityConfig mMActivityConfig) {
        this.expandStateManager = new ExpandStateManager();
        if (this.expandStateManager.expand(view, expandParams, mMActivityConfig)) {
            return true;
        }
        this.expandStateManager = null;
        return false;
    }

    public void close() {
        if (this.expandStateManager != null) {
            this.expandStateManager.close();
        } else if (this.resizeContainer != null) {
            this.resizeContainer.close();
        }
    }
}
