package com.millennialmedia.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.TimedMemoryCache;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MMIntentWrapperActivity extends Activity {
    private static final String INTENT_WRAPPER_STATE_KEY = "intent_wrapper_state_id";
    private static final long ON_CREATE_TIMEOUT = 5000;
    private static final String TAG = "MMIntentWrapperActivity";
    private static TimedMemoryCache<ActivityState> timedMemoryCache = new TimedMemoryCache<>();
    private ActivityState activityState;

    public interface MMIntentWrapperListener {
        void onData(Intent intent);

        void onError(String str);
    }

    private static class ActivityState {
        MMIntentWrapperListener intentWrapperListener;
        CountDownLatch onCreateLatch;
        Intent wrappedIntent;

        private ActivityState(MMIntentWrapperListener mMIntentWrapperListener, Intent intent) {
            this.onCreateLatch = new CountDownLatch(1);
            this.intentWrapperListener = mMIntentWrapperListener;
            this.wrappedIntent = intent;
        }
    }

    public static void launch(Context context, Intent intent, MMIntentWrapperListener mMIntentWrapperListener) {
        if (mMIntentWrapperListener == null) {
            MMLog.e(TAG, "Unable to launch MMIntentWrapperActivity, provided MMIntentWrapperListener instance is null");
            return;
        }
        final ActivityState activityState2 = new ActivityState(mMIntentWrapperListener, intent);
        String add = timedMemoryCache.add(activityState2, null);
        if (add == null) {
            mMIntentWrapperListener.onError("Unable to launch MMIntentWrapperActivity, failed to cache activity state");
            return;
        }
        Intent intent2 = new Intent(context, MMIntentWrapperActivity.class);
        intent2.putExtra(INTENT_WRAPPER_STATE_KEY, add);
        context.startActivity(intent2);
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                try {
                    if (!activityState2.onCreateLatch.await(MMIntentWrapperActivity.ON_CREATE_TIMEOUT, TimeUnit.MILLISECONDS)) {
                        activityState2.intentWrapperListener.onError("Failed to start activity");
                    }
                } catch (InterruptedException unused) {
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!loadActivityState()) {
            String str = TAG;
            MMLog.e(str, "Failed to load activity state, aborting activity launch <" + this + ">");
            finish();
        } else if (bundle == null) {
            this.activityState.onCreateLatch.countDown();
            if (this.activityState.wrappedIntent == null || this.activityState.wrappedIntent.resolveActivity(getPackageManager()) == null) {
                MMIntentWrapperListener mMIntentWrapperListener = this.activityState.intentWrapperListener;
                mMIntentWrapperListener.onError("Failed to start activity, aborting activity launch <" + this + ">");
                finish();
                return;
            }
            startActivityForResult(this.activityState.wrappedIntent, 0);
        }
    }

    public void onDestroy() {
        if (!isFinishing() && !saveActivityState()) {
            String str = TAG;
            MMLog.e(str, "Failed to save activity state <" + this + ">");
        }
        super.onDestroy();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (!(this.activityState == null || this.activityState.intentWrapperListener == null)) {
            if (i2 == -1) {
                this.activityState.intentWrapperListener.onData(intent);
            } else {
                MMIntentWrapperListener mMIntentWrapperListener = this.activityState.intentWrapperListener;
                mMIntentWrapperListener.onError("Activity failed with result code <" + i2 + ">");
            }
        }
        finish();
    }

    private boolean loadActivityState() {
        ActivityState activityState2 = timedMemoryCache.get(getIntent().getStringExtra(INTENT_WRAPPER_STATE_KEY));
        if (activityState2 == null) {
            return false;
        }
        this.activityState = activityState2;
        return true;
    }

    private boolean saveActivityState() {
        Intent intent = getIntent();
        intent.removeExtra(INTENT_WRAPPER_STATE_KEY);
        String add = timedMemoryCache.add(this.activityState, null);
        if (add == null) {
            return false;
        }
        intent.putExtra(INTENT_WRAPPER_STATE_KEY, add);
        return true;
    }
}
