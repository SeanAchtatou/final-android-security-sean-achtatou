package com.millennialmedia.internal;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.graphics.Rect;
import android.os.Build;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.JSBridge;
import com.millennialmedia.internal.SizableStateManager;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.Utils;
import com.millennialmedia.internal.utils.ViewUtils;
import com.moat.analytics.mobile.aol.MoatFactory;
import com.moat.analytics.mobile.aol.WebAdTracker;
import com.mopub.common.Constants;
import java.lang.ref.WeakReference;

@SuppressLint({"ViewConstructor"})
public class MMWebView extends WebView implements ViewTreeObserver.OnScrollChangedListener {
    private static final int EXPOSURE_POLLING_INTERVAL_MILLIS = 200;
    /* access modifiers changed from: private */
    public static final String TAG = "MMWebView";
    public static boolean googleSecurityPatchEnabled = true;
    private int[] currentPosition = new int[2];
    String currentUrl;
    private volatile boolean destroyed = false;
    /* access modifiers changed from: private */
    public boolean dispatchExposureUpdates = false;
    private GestureDetector gestureDetector;
    JSBridge jsBridge;
    /* access modifiers changed from: private */
    public boolean jsScriptsInjected = false;
    /* access modifiers changed from: private */
    public float lastExposedPercentage;
    /* access modifiers changed from: private */
    public Rect lastMinimumBoundingRectangle;
    private int[] lastPosition = new int[2];
    private MoatFactory moatFactory;
    private final MMWebViewOptions options;
    /* access modifiers changed from: private */
    public final ViewUtils.ViewabilityWatcher viewabilityWatcher;
    private WebAdTracker webAdTracker;
    protected final MMWebViewListener webViewListener;

    public interface MMWebViewListener {
        void close();

        boolean expand(SizableStateManager.ExpandParams expandParams);

        void onAdLeftApplication();

        void onClicked();

        void onFailed();

        void onLoaded();

        void onReady();

        void onUnload();

        boolean resize(SizableStateManager.ResizeParams resizeParams);

        void setOrientation(int i);
    }

    /* access modifiers changed from: protected */
    public String getExtraScriptToInject() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void injectExtraAPIs() {
    }

    public static class MMWebViewOptions {
        public final boolean enableEnhancedAdControl;
        public final boolean enableMoat;
        public final boolean interstitial;
        public final boolean transparent;

        public static MMWebViewOptions getDefault() {
            return new MMWebViewOptions(false, false, false, false);
        }

        public MMWebViewOptions(boolean z, boolean z2, boolean z3, boolean z4) {
            this.interstitial = z;
            this.transparent = z2;
            this.enableMoat = z3;
            this.enableEnhancedAdControl = z4;
        }
    }

    static class MMWebViewClient extends WebViewClient {
        MMWebViewClient() {
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            if (webView instanceof MMWebView) {
                ((MMWebView) webView).webViewListener.onFailed();
            }
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!(webView instanceof MMWebView)) {
                return false;
            }
            MMWebView mMWebView = (MMWebView) webView;
            if (!mMWebView.isOriginalUrl(str) && mMWebView.jsBridge.areApiCallsEnabled() && Utils.startActivityFromUrl(str)) {
                mMWebView.webViewListener.onAdLeftApplication();
            }
            return true;
        }
    }

    static class MMWebChromeClient extends WebChromeClient {
        MMWebChromeClient() {
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            if (webView.getContext() != EnvironmentUtils.getApplicationContext()) {
                return super.onJsAlert(webView, str, str2, jsResult);
            }
            Toast.makeText(webView.getContext(), str2, 0).show();
            jsResult.confirm();
            return true;
        }

        public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
            if (webView.getContext() != EnvironmentUtils.getApplicationContext()) {
                return super.onJsConfirm(webView, str, str2, jsResult);
            }
            Toast.makeText(webView.getContext(), str2, 0).show();
            jsResult.confirm();
            return true;
        }

        public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
            if (webView.getContext() != EnvironmentUtils.getApplicationContext()) {
                return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
            }
            Toast.makeText(webView.getContext(), str2, 0).show();
            jsPromptResult.confirm(str3);
            return true;
        }

        public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
            if (webView.getContext() != EnvironmentUtils.getApplicationContext()) {
                return super.onJsBeforeUnload(webView, str, str2, jsResult);
            }
            Toast.makeText(webView.getContext(), str2, 0).show();
            jsResult.confirm();
            return true;
        }
    }

    static class MMWebViewViewabilityListener implements ViewUtils.ViewabilityListener {
        WeakReference<MMWebView> mmWebViewRef;

        MMWebViewViewabilityListener(MMWebView mMWebView) {
            this.mmWebViewRef = new WeakReference<>(mMWebView);
        }

        public void onViewableChanged(boolean z) {
            MMWebView mMWebView = this.mmWebViewRef.get();
            if (mMWebView != null && mMWebView.jsBridge != null && !mMWebView.isDestroyed()) {
                mMWebView.jsBridge.setViewable(z);
            }
        }
    }

    static class MMWebViewJSBridgeListener implements JSBridge.JSBridgeListener {
        WeakReference<MMWebView> mmWebViewRef;

        MMWebViewJSBridgeListener(MMWebView mMWebView) {
            this.mmWebViewRef = new WeakReference<>(mMWebView);
        }

        private MMWebView getWebView() {
            MMWebView mMWebView = this.mmWebViewRef.get();
            if (mMWebView == null || mMWebView.isDestroyed()) {
                return null;
            }
            return mMWebView;
        }

        public void onInjectedScriptsLoaded() {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(MMWebView.TAG, "Injected scripts have been loaded");
            }
            MMWebView webView = getWebView();
            if (webView != null) {
                boolean unused = webView.jsScriptsInjected = true;
                webView.jsBridge.setLogLevel(MMLog.logLevel);
                webView.setLoaded();
                return;
            }
            MMLog.e(MMWebView.TAG, "MMWebView reference no longer points to a valid object");
        }

        public void onJSBridgeReady() {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(MMWebView.TAG, "JSBridge is ready");
            }
            MMWebView webView = getWebView();
            if (webView != null) {
                webView.webViewListener.onReady();
            }
        }

        public void close() {
            MMWebView mMWebView = this.mmWebViewRef.get();
            if (mMWebView != null) {
                mMWebView.webViewListener.close();
            }
        }

        public boolean expand(SizableStateManager.ExpandParams expandParams) {
            MMWebView webView = getWebView();
            if (webView == null) {
                return false;
            }
            return webView.webViewListener.expand(expandParams);
        }

        public boolean resize(SizableStateManager.ResizeParams resizeParams) {
            MMWebView webView = getWebView();
            if (webView == null) {
                return false;
            }
            return webView.webViewListener.resize(resizeParams);
        }

        public void onAdLeftApplication() {
            MMWebView webView = getWebView();
            if (webView != null) {
                webView.webViewListener.onAdLeftApplication();
            }
        }

        public void setOrientation(int i) {
            MMWebView webView = getWebView();
            if (webView != null) {
                webView.webViewListener.setOrientation(i);
            }
        }

        public void unload() {
            MMWebView webView = getWebView();
            if (webView != null) {
                webView.webViewListener.onUnload();
            }
        }
    }

    static class MMWebViewGestureListener extends GestureDetector.SimpleOnGestureListener {
        MMWebViewListener webViewListener;

        MMWebViewGestureListener(MMWebViewListener mMWebViewListener) {
            this.webViewListener = mMWebViewListener;
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            this.webViewListener.onClicked();
            return true;
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    @TargetApi(17)
    public MMWebView(Context context, MMWebViewOptions mMWebViewOptions, MMWebViewListener mMWebViewListener) {
        super(new MutableContextWrapper(context));
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Creating webview " + hashCode());
        }
        this.options = mMWebViewOptions == null ? MMWebViewOptions.getDefault() : mMWebViewOptions;
        setTag(TAG);
        if (mMWebViewListener == null) {
            throw new IllegalArgumentException("Unable to create MMWebView instance, specified listener is null");
        }
        this.webViewListener = mMWebViewListener;
        if (this.options.transparent) {
            setBackgroundColor(0);
        }
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        this.gestureDetector = new GestureDetector(context.getApplicationContext(), new MMWebViewGestureListener(mMWebViewListener));
        setWebViewClient(new MMWebViewClient());
        setWebChromeClient(new MMWebChromeClient());
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(-1);
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setLoadWithOverviewMode(true);
        settings.setGeolocationEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= 17) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Disabling user gesture requirement for media playback");
            }
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Google security patch is ");
            sb.append(googleSecurityPatchEnabled ? "enabled" : "disabled");
            MMLog.d(str2, sb.toString());
        }
        settings.setAllowFileAccess(!googleSecurityPatchEnabled);
        settings.setAllowContentAccess(!googleSecurityPatchEnabled);
        settings.setAllowFileAccessFromFileURLs(!googleSecurityPatchEnabled);
        settings.setAllowUniversalAccessFromFileURLs(true ^ googleSecurityPatchEnabled);
        this.jsBridge = new JSBridge(this, this.options.interstitial, new MMWebViewJSBridgeListener(this));
        if (this.options.enableEnhancedAdControl) {
            this.jsBridge.enableApiCalls();
        }
        VolumeChangeManager.registerListener(this.jsBridge);
        VolumeChangeManager.start();
        this.viewabilityWatcher = new ViewUtils.ViewabilityWatcher(this, new MMWebViewViewabilityListener(this));
        this.viewabilityWatcher.startWatching();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.options.enableMoat) {
            this.moatFactory = MoatFactory.create();
            if (this.webAdTracker == null) {
                this.webAdTracker = this.moatFactory.createWebAdTracker((WebView) this);
                this.webAdTracker.startTracking();
                MMLog.v(TAG, "Moat tracking enabled for MMWebView.");
            }
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Moat is not enabled for this MMWebView.");
        }
        getLocationOnScreen(this.lastPosition);
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnScrollChangedListener(this);
        }
        this.dispatchExposureUpdates = true;
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (MMWebView.this.viewabilityWatcher != null && (!Utils.isEqual(MMWebView.this.viewabilityWatcher.mbr, MMWebView.this.lastMinimumBoundingRectangle) || MMWebView.this.lastExposedPercentage != MMWebView.this.viewabilityWatcher.exposedPercentage)) {
                    float unused = MMWebView.this.lastExposedPercentage = MMWebView.this.viewabilityWatcher.exposedPercentage;
                    Rect unused2 = MMWebView.this.lastMinimumBoundingRectangle = MMWebView.this.viewabilityWatcher.mbr;
                    MMWebView.this.sendExposureChange();
                }
                if (MMWebView.this.dispatchExposureUpdates) {
                    ThreadUtils.postOnUiThread(this, 200);
                } else if (MMLog.isDebugEnabled()) {
                    MMLog.d(MMWebView.TAG, "Stopping exposureChange notifications.");
                }
            }
        });
        this.jsBridge.startLocationUpdates();
    }

    /* access modifiers changed from: package-private */
    public void sendExposureChange() {
        if (this.jsBridge != null) {
            this.jsBridge.sendExposureChange(this.viewabilityWatcher.exposedPercentage, this.viewabilityWatcher.mbr);
        }
    }

    public void onDetachedFromWindow() {
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnScrollChangedListener(this);
        }
        this.dispatchExposureUpdates = false;
        this.jsBridge.stopLocationUpdates();
        super.onDetachedFromWindow();
    }

    public void onScrollChanged() {
        getLocationOnScreen(this.currentPosition);
        if (this.currentPosition[0] != this.lastPosition[0] || this.currentPosition[1] != this.lastPosition[1]) {
            this.lastPosition[0] = this.currentPosition[0];
            this.lastPosition[1] = this.currentPosition[1];
            if (this.jsBridge != null) {
                this.jsBridge.setScrolledPosition(this);
            }
        }
    }

    public void setContent(String str) {
        if (TextUtils.isEmpty(str)) {
            this.webViewListener.onFailed();
            return;
        }
        this.jsScriptsInjected = false;
        final String activePlaylistServerBaseUrl = Handshake.getActivePlaylistServerBaseUrl();
        final String injectJSBridge = this.jsBridge.injectJSBridge(str, URLUtil.isHttpsUrl(activePlaylistServerBaseUrl));
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                MMWebView.this.loadDataWithBaseURL(activePlaylistServerBaseUrl, injectJSBridge, "text/html", "UTF-8", "mmadsdk");
            }
        });
    }

    public String getUrl() {
        if (this.destroyed) {
            return null;
        }
        return super.getUrl();
    }

    public void release() {
        if (!ThreadUtils.isUiThread()) {
            MMLog.e(TAG, "release must be called on the UI thread");
            return;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Releasing webview " + hashCode());
        }
        if (this.webAdTracker != null) {
            this.webAdTracker.stopTracking();
        }
        if (getParent() != null) {
            ViewUtils.removeFromParent(this);
        }
        VolumeChangeManager.removeListener(this.jsBridge);
        super.loadUrl("about:blank");
        stopLoading();
        setWebChromeClient(null);
        setWebViewClient(null);
        try {
            destroy();
        } catch (Exception e) {
            MMLog.e(TAG, "An error occurred destroying the webview.", e);
        }
        this.gestureDetector = null;
        this.destroyed = true;
    }

    public boolean isDestroyed() {
        return this.destroyed;
    }

    /* access modifiers changed from: private */
    public void setLoaded() {
        if (this.jsScriptsInjected) {
            onLoaded();
        }
    }

    /* access modifiers changed from: protected */
    public void onLoaded() {
        this.webViewListener.onLoaded();
    }

    public boolean isJSBridgeReady() {
        return this.jsBridge.isReady();
    }

    public boolean isEnhancedAdControlEnabled() {
        return this.jsBridge.areApiCallsEnabled();
    }

    public void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        this.currentUrl = str;
        try {
            super.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } catch (Exception e) {
            MMLog.e(TAG, "Error hit when calling through to loadDataWithBaseUrl", e);
        }
    }

    /* access modifiers changed from: private */
    public void loadUrlOnUiThread(String str) {
        if (!this.destroyed) {
            try {
                super.loadUrl(str);
            } catch (Exception e) {
                MMLog.e(TAG, "Error loading url", e);
            }
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Attempt to loadUrlOnUiThread after webview has been destroyed");
        }
    }

    public void loadUrl(final String str) {
        if (TextUtils.isEmpty(str)) {
            MMLog.e(TAG, "Url is null or empty");
        } else if (!this.destroyed) {
            if (str.startsWith(Constants.HTTP)) {
                this.currentUrl = str;
            }
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MMWebView.this.loadUrlOnUiThread(str);
                }
            });
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Attempt to load url after webview has been destroyed");
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.jsBridge != null) {
            this.jsBridge.setCurrentPosition(this);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.jsBridge.enableApiCalls();
        }
        if (this.gestureDetector != null) {
            this.gestureDetector.onTouchEvent(motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: private */
    public boolean isOriginalUrl(String str) {
        if (!TextUtils.isEmpty(this.currentUrl)) {
            if (!str.startsWith(this.currentUrl + "?")) {
                if (str.startsWith(this.currentUrl + "#")) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }

    public void invokeCallback(String str, Object... objArr) {
        if (this.jsBridge != null) {
            this.jsBridge.invokeCallback(str, objArr);
        }
    }

    public void callJavascript(String str, Object... objArr) {
        if (this.jsBridge != null) {
            this.jsBridge.callJavascript(str, objArr);
        }
    }

    public void setStateResized() {
        if (this.jsBridge != null) {
            this.jsBridge.setStateResized();
        }
    }

    public void setStateUnresized() {
        if (this.jsBridge != null) {
            this.jsBridge.setStateUnresized();
        }
    }

    public void setStateExpanded() {
        if (this.jsBridge != null) {
            this.jsBridge.setStateExpanded();
        }
    }

    public void setStateCollapsed() {
        if (this.jsBridge != null) {
            this.jsBridge.setStateCollapsed();
        }
    }

    public void setTwoPartExpand() {
        if (this.jsBridge != null) {
            this.jsBridge.setTwoPartExpand();
        }
    }

    public void setStateResizing() {
        if (this.jsBridge != null) {
            this.jsBridge.setStateResizing();
        }
    }

    public void onNotifyClicked() {
        this.webViewListener.onClicked();
    }
}
