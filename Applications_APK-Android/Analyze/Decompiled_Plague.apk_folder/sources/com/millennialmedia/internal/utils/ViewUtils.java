package com.millennialmedia.internal.utils;

import android.app.Activity;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.ActivityListenerManager;
import java.lang.ref.WeakReference;
import java.util.Locale;

public class ViewUtils {
    public static final int AT_LEAST_ONE_PIXEL_VIEWABLE = -1;
    /* access modifiers changed from: private */
    public static final String TAG = "ViewUtils";

    public interface ViewabilityListener {
        void onViewableChanged(boolean z);
    }

    public static class ViewabilityWatcher implements ViewTreeObserver.OnPreDrawListener, View.OnLayoutChangeListener, View.OnAttachStateChangeListener, Runnable {
        volatile ActivityListenerManager.ActivityListener activityListener;
        Rect clipRect = new Rect();
        public float exposedPercentage;
        volatile ActivityListenerManager.LifecycleState lifecycleState;
        volatile ViewabilityListener listener;
        volatile boolean listeningToActivity = false;
        public Rect mbr;
        int minViewabilityPercent = -1;
        volatile boolean observingViewTree = false;
        volatile WeakReference<View> viewRef;
        public volatile boolean viewable = false;
        volatile boolean watching = false;

        public ViewabilityWatcher(View view, ViewabilityListener viewabilityListener) {
            if (MMLog.isDebugEnabled()) {
                String access$000 = ViewUtils.TAG;
                MMLog.d(access$000, "Creating viewability watcher <" + this + "> for view <" + view + ">");
            }
            this.viewRef = new WeakReference<>(view);
            this.listener = viewabilityListener;
            this.activityListener = new ActivityListenerManager.ActivityListener() {
                public void onResumed(Activity activity) {
                    ViewabilityWatcher.this.lifecycleState = ActivityListenerManager.LifecycleState.RESUMED;
                    ViewabilityWatcher.this.checkViewable();
                }

                public void onPaused(Activity activity) {
                    ViewabilityWatcher.this.lifecycleState = ActivityListenerManager.LifecycleState.PAUSED;
                    ViewabilityWatcher.this.checkViewable();
                }
            };
        }

        public void setMinViewabilityPercent(int i) {
            if (MMLog.isDebugEnabled()) {
                String access$000 = ViewUtils.TAG;
                MMLog.d(access$000, "Setting the viewability percentage.\n\tViewability watcher: " + this + "\n\tPercentage: " + i);
            }
            this.minViewabilityPercent = i;
        }

        public void startWatching() {
            if (MMLog.isDebugEnabled()) {
                String access$000 = ViewUtils.TAG;
                MMLog.d(access$000, "Starting watcher.\n\tViewability watcher: " + this + "\n\tView: " + this.viewRef.get());
            }
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    View view = ViewabilityWatcher.this.viewRef.get();
                    if (view != null && !ViewabilityWatcher.this.watching) {
                        view.addOnAttachStateChangeListener(ViewabilityWatcher.this);
                        view.addOnLayoutChangeListener(ViewabilityWatcher.this);
                        ViewabilityWatcher.this.watching = true;
                        if (view.getWindowToken() != null) {
                            ViewabilityWatcher.this.addObserver(view);
                            ViewabilityWatcher.this.listenToActivity(view, true);
                        }
                        ViewabilityWatcher.this.checkViewable();
                    }
                }
            });
        }

        public void stopWatching() {
            if (MMLog.isDebugEnabled()) {
                String access$000 = ViewUtils.TAG;
                MMLog.d(access$000, "Stopping watcher.\n\tViewability watcher: " + this + "\n\tView: " + this.viewRef.get());
            }
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    View view = ViewabilityWatcher.this.viewRef.get();
                    if (view != null && ViewabilityWatcher.this.watching) {
                        ViewabilityWatcher.this.removeObserver(view);
                        view.removeOnAttachStateChangeListener(ViewabilityWatcher.this);
                        view.removeOnLayoutChangeListener(ViewabilityWatcher.this);
                        ViewabilityWatcher.this.watching = false;
                        ViewabilityWatcher.this.listenToActivity(view, false);
                    }
                }
            });
        }

        public void onViewAttachedToWindow(View view) {
            if (MMLog.isDebugEnabled()) {
                String access$000 = ViewUtils.TAG;
                MMLog.d(access$000, "onViewAttachedToWindow called.\n\tViewability watcher: " + this + "\n\tView: " + view);
            }
            if (this.watching) {
                addObserver(view);
                listenToActivity(view, true);
                checkViewable();
            }
        }

        public void onViewDetachedFromWindow(View view) {
            if (MMLog.isDebugEnabled()) {
                String access$000 = ViewUtils.TAG;
                MMLog.d(access$000, "onViewDetachedFromWindow called.\n\tViewability watcher: " + this + "\n\tView: " + view);
            }
            if (this.watching) {
                removeObserver(view);
                listenToActivity(view, false);
                checkViewable();
            }
        }

        /* access modifiers changed from: private */
        public void addObserver(View view) {
            if (!this.observingViewTree) {
                ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
                if (viewTreeObserver.isAlive()) {
                    if (MMLog.isDebugEnabled()) {
                        String access$000 = ViewUtils.TAG;
                        MMLog.d(access$000, "Adding ViewTreeObserver.\n\tViewability watcher: " + this + "\n\tViewTreeObserver: " + viewTreeObserver + "\n\tView: " + view);
                    }
                    viewTreeObserver.addOnPreDrawListener(this);
                    this.observingViewTree = true;
                }
            } else if (MMLog.isDebugEnabled()) {
                MMLog.d(ViewUtils.TAG, "Trying to set view tree observer when already set");
            }
        }

        /* access modifiers changed from: private */
        public void removeObserver(View view) {
            if (this.observingViewTree) {
                ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
                if (viewTreeObserver.isAlive()) {
                    if (MMLog.isDebugEnabled()) {
                        String access$000 = ViewUtils.TAG;
                        MMLog.d(access$000, "Removing ViewTreeObserver.\n\tViewability watcher: " + this + "\n\tViewTreeObserver: " + viewTreeObserver + "\n\tView: " + view);
                    }
                    viewTreeObserver.removeOnPreDrawListener(this);
                }
                this.observingViewTree = false;
            } else if (MMLog.isDebugEnabled()) {
                MMLog.d(ViewUtils.TAG, "Trying to remove view tree observer when not set");
            }
        }

        /* access modifiers changed from: private */
        public void listenToActivity(View view, boolean z) {
            Activity activityForView = ViewUtils.getActivityForView(view);
            if (activityForView != null) {
                if (z && !this.listeningToActivity) {
                    ActivityListenerManager.registerListener(activityForView.hashCode(), this.activityListener);
                    this.lifecycleState = ActivityListenerManager.getLifecycleState(activityForView.hashCode());
                } else if (!z && this.listeningToActivity) {
                    ActivityListenerManager.unregisterListener(activityForView.hashCode(), this.activityListener);
                }
                this.listeningToActivity = z;
            }
        }

        public boolean onPreDraw() {
            if (!this.watching) {
                return true;
            }
            checkViewable();
            return true;
        }

        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            if (this.watching) {
                checkViewable();
            }
        }

        /* access modifiers changed from: private */
        public void checkViewable() {
            ThreadUtils.postOnUiThread(this);
        }

        private boolean isViewViewable(View view) {
            if (view == null) {
                return false;
            }
            if (this.minViewabilityPercent == 0) {
                return true;
            }
            if (this.lifecycleState != ActivityListenerManager.LifecycleState.RESUMED || !view.isShown() || ((double) view.getAlpha()) <= 0.0d || !view.getGlobalVisibleRect(this.clipRect)) {
                this.exposedPercentage = 0.0f;
                this.mbr = null;
            } else {
                long height = (long) (this.clipRect.height() * this.clipRect.width());
                long height2 = (long) (view.getHeight() * view.getWidth());
                this.exposedPercentage = 100.0f * (((float) height) / ((float) height2));
                this.mbr = new Rect(this.clipRect);
                if (height > 0) {
                    if (this.minViewabilityPercent == -1) {
                        return true;
                    }
                    if (height2 <= 0 || (100 * height) / height2 < ((long) this.minViewabilityPercent)) {
                        return false;
                    }
                    return true;
                }
            }
            return false;
        }

        public void run() {
            View view = this.viewRef.get();
            boolean isViewViewable = isViewViewable(view);
            if (this.viewable != isViewViewable) {
                this.viewable = isViewViewable;
                if (this.watching && this.listener != null) {
                    if (MMLog.isDebugEnabled()) {
                        String access$000 = ViewUtils.TAG;
                        MMLog.d(access$000, "Notifying listener of viewability change.\n\tViewability watcher: " + this + "\n\tView: " + view + "\n\tViewable: " + this.viewable);
                    }
                    this.listener.onViewableChanged(this.viewable);
                }
            }
        }
    }

    public static void attachView(ViewGroup viewGroup, View view) {
        attachView(viewGroup, view, null);
    }

    public static void attachView(ViewGroup viewGroup, View view, ViewGroup.LayoutParams layoutParams) {
        Context context;
        if (view.getParent() != null) {
            removeFromParent(view);
        }
        Context context2 = view.getContext();
        if ((context2 instanceof MutableContextWrapper) && context2 != (context = viewGroup.getContext())) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Changing view context to match parent context");
            }
            ((MutableContextWrapper) context2).setBaseContext(context);
        }
        if (layoutParams != null) {
            viewGroup.addView(view, layoutParams);
        } else {
            viewGroup.addView(view);
        }
    }

    public static void removeFromParent(View view) {
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(view);
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Unable to remove view from parent, no valid parent view found");
        }
    }

    public static boolean isChild(ViewGroup viewGroup, View view) {
        ViewParent parent = view.getParent();
        if (!(parent instanceof ViewGroup) || ((ViewGroup) parent) != viewGroup) {
            return false;
        }
        return true;
    }

    public static ViewGroup getParentContainer(View view) {
        ViewParent parent = view.getParent();
        if (!(parent instanceof ViewGroup)) {
            return null;
        }
        return (ViewGroup) parent;
    }

    public static int getActivityHashForView(View view) {
        int i;
        Activity activityForView = getActivityForView(view);
        if (activityForView == null) {
            MMLog.e(TAG, "Unable to get activity hash");
            i = -1;
        } else {
            i = activityForView.hashCode();
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format(Locale.getDefault(), "Found activity hash code <%d> for view <%s>", Integer.valueOf(i), view.toString()));
        }
        return i;
    }

    public static ViewGroup getDecorView(View view) {
        KeyEvent.Callback callback;
        Activity activityForView = getActivityForView(view);
        ViewGroup viewGroup = null;
        if (activityForView != null) {
            callback = activityForView.getWindow().getDecorView();
        } else {
            callback = view != null ? view.getRootView() : null;
        }
        if (callback instanceof ViewGroup) {
            viewGroup = (ViewGroup) callback;
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("Found decor view <%s> for view <%s>", viewGroup, view));
        }
        return viewGroup;
    }

    public static Point getViewPositionOnScreen(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        return new Point(iArr[0], iArr[1]);
    }

    public static Rect getViewDimensionsOnScreen(View view, Rect rect) {
        if (view == null) {
            MMLog.e(TAG, "Unable to calculate view dimensions for null view");
            return null;
        }
        if (rect == null) {
            rect = new Rect();
        }
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        rect.left = iArr[0];
        rect.top = iArr[1];
        rect.right = rect.left + view.getWidth();
        rect.bottom = rect.top + view.getHeight();
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("On screen dimensions for View <%s>: %s", view, rect.flattenToString()));
        }
        return rect;
    }

    public static Rect getContentDimensions(View view, Rect rect) {
        if (view == null) {
            MMLog.e(TAG, "Unable to calculate content dimensions for null view");
            return null;
        }
        ViewGroup decorView = getDecorView(view);
        if (decorView == null) {
            MMLog.e(TAG, "Unable to calculate content for null root view");
            return null;
        }
        if (rect == null) {
            rect = new Rect();
        }
        decorView.getWindowVisibleDisplayFrame(rect);
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("Content dimensions for View <%s>: %s", view, rect.flattenToString()));
        }
        return rect;
    }

    public static Rect getViewDimensionsRelativeToContent(View view, Rect rect) {
        Rect viewDimensionsOnScreen = getViewDimensionsOnScreen(view, rect);
        if (viewDimensionsOnScreen != null) {
            ViewGroup decorView = getDecorView(view);
            if (decorView == null) {
                MMLog.e(TAG, "Unable to calculate dimensions for null root view");
                return null;
            }
            Rect rect2 = new Rect();
            decorView.getWindowVisibleDisplayFrame(rect2);
            viewDimensionsOnScreen.top -= rect2.top;
            viewDimensionsOnScreen.bottom -= rect2.top;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            Object[] objArr = new Object[2];
            objArr[0] = view;
            objArr[1] = viewDimensionsOnScreen != null ? viewDimensionsOnScreen.flattenToString() : "null";
            MMLog.d(str, String.format("Dimensions relative to content for View <%s>: %s", objArr));
        }
        return viewDimensionsOnScreen;
    }

    public static int convertPixelsToDips(int i) {
        return (int) (((float) i) / EnvironmentUtils.getDisplayDensity());
    }

    public static void convertPixelsToDips(Rect rect) {
        if (rect == null) {
            MMLog.e(TAG, "Unable to convert for null dimensions");
            return;
        }
        float displayDensity = EnvironmentUtils.getDisplayDensity();
        rect.left = (int) (((float) rect.left) / displayDensity);
        rect.top = (int) (((float) rect.top) / displayDensity);
        rect.right = rect.left + ((int) (((float) (rect.right - rect.left)) / displayDensity));
        rect.bottom = rect.top + ((int) (((float) (rect.bottom - rect.top)) / displayDensity));
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Converted dimensions from pixels to dips: " + rect.flattenToString());
        }
    }

    public static int convertDipsToPixels(int i) {
        int ceil = (int) Math.ceil((double) TypedValue.applyDimension(1, (float) i, EnvironmentUtils.getApplicationContext().getResources().getDisplayMetrics()));
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("Converted dips: %d, to pixels: %d", Integer.valueOf(i), Integer.valueOf(ceil)));
        }
        return ceil;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.app.Activity getActivityForView(android.view.View r4) {
        /*
            if (r4 == 0) goto L_0x0018
            android.content.Context r0 = r4.getContext()
        L_0x0006:
            boolean r1 = r0 instanceof android.content.MutableContextWrapper
            if (r1 == 0) goto L_0x0011
            android.content.MutableContextWrapper r0 = (android.content.MutableContextWrapper) r0
            android.content.Context r0 = r0.getBaseContext()
            goto L_0x0006
        L_0x0011:
            boolean r1 = r0 instanceof android.app.Activity
            if (r1 == 0) goto L_0x0018
            android.app.Activity r0 = (android.app.Activity) r0
            goto L_0x0019
        L_0x0018:
            r0 = 0
        L_0x0019:
            boolean r1 = com.millennialmedia.MMLog.isDebugEnabled()
            if (r1 == 0) goto L_0x0042
            java.lang.String r1 = com.millennialmedia.internal.utils.ViewUtils.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Found activity <"
            r2.append(r3)
            r2.append(r0)
            java.lang.String r3 = "> for view <"
            r2.append(r3)
            r2.append(r4)
            java.lang.String r4 = ">"
            r2.append(r4)
            java.lang.String r4 = r2.toString()
            com.millennialmedia.MMLog.d(r1, r4)
        L_0x0042:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.utils.ViewUtils.getActivityForView(android.view.View):android.app.Activity");
    }
}
