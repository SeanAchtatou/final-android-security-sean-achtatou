package com.millennialmedia.internal.utils;

import android.graphics.Bitmap;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdMetadata;
import com.millennialmedia.internal.utils.IOUtils;
import com.mopub.common.AdType;
import java.io.File;
import java.io.InputStream;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class HttpUtils {
    private static final int DEFAULT_TIMEOUT = 15000;
    /* access modifiers changed from: private */
    public static final String TAG = "HttpUtils";
    private static HttpInterceptor httpInterceptor;

    public interface HttpInterceptor {
        void onRequest(String str, HttpRequestRunner httpRequestRunner);

        void onResponse(String str, Response response);
    }

    public interface HttpRequestListener {
        void onResponse(Response response);
    }

    public interface ResponseStreamer {
        void streamContent(InputStream inputStream, Response response);
    }

    public static class Response {
        public AdMetadata adMetadata;
        public Bitmap bitmap;
        public int code;
        public String content;
        public String contentType;
        public File file;

        public Response() {
        }

        public Response(int i) {
            this.code = i;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format(Locale.getDefault(), "code: %d", Integer.valueOf(this.code)));
            if (this.contentType != null) {
                sb.append(String.format(Locale.getDefault(), "\n\tcontent-type: %s", this.contentType));
            }
            if (this.content != null) {
                if (this.contentType == null || this.contentType.contains("text") || this.contentType.contains(AdType.STATIC_NATIVE)) {
                    sb.append(String.format(Locale.getDefault(), "\n\tcontent: %s", this.content));
                } else {
                    sb.append("\n\tcontent: <non-text-content>");
                }
            } else if (this.bitmap != null) {
                sb.append(String.format(Locale.getDefault(), "\n\tbitmap: dimensions: %d x %d\n\tbitmap size: %d", Integer.valueOf(this.bitmap.getWidth()), Integer.valueOf(this.bitmap.getHeight()), Integer.valueOf(this.bitmap.getByteCount())));
            } else if (this.file != null) {
                sb.append(String.format(Locale.getDefault(), "\n\tfile: %s", this.file.getAbsolutePath()));
            }
            return sb.toString();
        }
    }

    public static class HttpRequestRunner implements Runnable {
        private String contentType;
        public Map<String, String> headers;
        private CountDownLatch latch = new CountDownLatch(1);
        public String postData;
        private long requestId;
        public Response response;
        private ResponseStreamer responseStreamer;
        public int timeout;
        public String url;

        HttpRequestRunner(long j, String str, String str2, String str3, int i, ResponseStreamer responseStreamer2) {
            this.requestId = j;
            this.url = str;
            this.postData = str2;
            this.contentType = str3;
            this.timeout = i;
            this.responseStreamer = responseStreamer2;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format(Locale.getDefault(), "requestId: %d\n\turl: %s\n\ttimeout: %d", Long.valueOf(this.requestId), this.url, Integer.valueOf(this.timeout)));
            if (this.contentType != null) {
                sb.append(String.format(Locale.getDefault(), "\n\tcontent type: %s", this.contentType));
            }
            if (this.postData != null) {
                sb.append(String.format(Locale.getDefault(), "\n\tpost data: %s", this.postData));
            }
            return sb.toString();
        }

        /* access modifiers changed from: package-private */
        public Response getResponse() {
            return this.response;
        }

        /* access modifiers changed from: package-private */
        public Response waitForResponse(long j) {
            try {
                if (this.latch.await(j, TimeUnit.MILLISECONDS)) {
                    return this.response;
                }
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(HttpUtils.TAG, String.format(Locale.getDefault(), "HTTP request timed out.\n\trequestId: %d\n\twait time: %d", Long.valueOf(this.requestId), Long.valueOf(j)));
                }
                return new Response(408);
            } catch (InterruptedException unused) {
                MMLog.e(HttpUtils.TAG, String.format(Locale.getDefault(), "Http request was interrupted.\n\trequestId: %d", Long.valueOf(this.requestId)));
                return new Response(400);
            }
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX INFO: additional move instructions added (2) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v7, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v20, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v29, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v51, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v52, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v53, resolved type: java.io.InputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v54, resolved type: java.io.InputStream} */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x0280, code lost:
            if (r5 == null) goto L_0x0285;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x0282, code lost:
            r5.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x01ce, code lost:
            if (r5 != null) goto L_0x0282;
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:104:0x025c A[Catch:{ all -> 0x028b }] */
        /* JADX WARNING: Removed duplicated region for block: B:114:0x0299  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x0221 A[Catch:{ all -> 0x024b }] */
        /* JADX WARNING: Removed duplicated region for block: B:96:0x0247  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r14 = this;
                com.millennialmedia.internal.utils.HttpUtils$Response r0 = new com.millennialmedia.internal.utils.HttpUtils$Response
                r0.<init>()
                r14.response = r0
                r0 = 2
                r1 = 400(0x190, float:5.6E-43)
                r2 = 0
                r3 = 0
                r4 = 1
                java.net.URL r5 = new java.net.URL     // Catch:{ SocketTimeoutException -> 0x024d, Exception -> 0x01f2, all -> 0x01ec }
                java.lang.String r6 = r14.url     // Catch:{ SocketTimeoutException -> 0x024d, Exception -> 0x01f2, all -> 0x01ec }
                r5.<init>(r6)     // Catch:{ SocketTimeoutException -> 0x024d, Exception -> 0x01f2, all -> 0x01ec }
                java.net.URLConnection r5 = r5.openConnection()     // Catch:{ SocketTimeoutException -> 0x024d, Exception -> 0x01f2, all -> 0x01ec }
                java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ SocketTimeoutException -> 0x024d, Exception -> 0x01f2, all -> 0x01ec }
                int r6 = r14.timeout     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setReadTimeout(r6)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                int r6 = r14.timeout     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setConnectTimeout(r6)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setInstanceFollowRedirects(r4)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setUseCaches(r2)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setDoInput(r4)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r6 = "User-Agent"
                java.lang.String r7 = com.millennialmedia.internal.utils.EnvironmentUtils.getUserAgent()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setRequestProperty(r6, r7)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.Map<java.lang.String, java.lang.String> r6 = r14.headers     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r6 == 0) goto L_0x0060
                java.util.Map<java.lang.String, java.lang.String> r6 = r14.headers     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.Set r6 = r6.entrySet()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.Iterator r6 = r6.iterator()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
            L_0x0044:
                boolean r7 = r6.hasNext()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r7 == 0) goto L_0x0060
                java.lang.Object r7 = r6.next()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.Map$Entry r7 = (java.util.Map.Entry) r7     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.Object r8 = r7.getKey()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r8 = (java.lang.String) r8     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.Object r7 = r7.getValue()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r7 = (java.lang.String) r7     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setRequestProperty(r8, r7)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                goto L_0x0044
            L_0x0060:
                boolean r6 = r5 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r6 == 0) goto L_0x007f
                boolean r6 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r6 == 0) goto L_0x0073
                java.lang.String r6 = com.millennialmedia.internal.utils.HttpUtils.TAG     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r7 = "HttpsURLConnection created"
                com.millennialmedia.MMLog.d(r6, r7)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
            L_0x0073:
                com.millennialmedia.internal.utils.MMSSLSocketFactory r6 = com.millennialmedia.internal.utils.MMSSLSocketFactory.getInstance()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r6 == 0) goto L_0x007f
                r7 = r5
                javax.net.ssl.HttpsURLConnection r7 = (javax.net.ssl.HttpsURLConnection) r7     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r7.setSSLSocketFactory(r6)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
            L_0x007f:
                java.lang.String r6 = r14.postData     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r6 != 0) goto L_0x008c
                java.lang.String r6 = "GET"
                r5.setRequestMethod(r6)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.connect()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                goto L_0x00c9
            L_0x008c:
                r5.setDoOutput(r4)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r6 = "POST"
                r5.setRequestMethod(r6)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r6 = "Content-Type"
                java.lang.String r7 = r14.contentType     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setRequestProperty(r6, r7)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r6 = "Content-Length"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r7.<init>()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r8 = ""
                r7.append(r8)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r8 = r14.postData     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r9 = "UTF-8"
                byte[] r8 = r8.getBytes(r9)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                int r8 = r8.length     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r7.append(r8)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r7 = r7.toString()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r5.setRequestProperty(r6, r7)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.io.OutputStream r6 = r5.getOutputStream()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r7 = r14.postData     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x01d6, all -> 0x01d2 }
                com.millennialmedia.internal.utils.IOUtils.write(r6, r7)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x01d6, all -> 0x01d2 }
                r6.flush()     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x01d6, all -> 0x01d2 }
                com.millennialmedia.internal.utils.IOUtils.closeStream(r6)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x01d6, all -> 0x01d2 }
            L_0x00c9:
                boolean r6 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r6 == 0) goto L_0x00f2
                boolean r6 = r5 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r6 == 0) goto L_0x00f2
                r6 = r5
                javax.net.ssl.HttpsURLConnection r6 = (javax.net.ssl.HttpsURLConnection) r6     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r7 = com.millennialmedia.internal.utils.HttpUtils.TAG     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r8.<init>()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r9 = "Negotiated Cipher Suite: "
                r8.append(r9)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r6 = r6.getCipherSuite()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r8.append(r6)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r6 = r8.toString()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                com.millennialmedia.MMLog.d(r7, r6)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
            L_0x00f2:
                java.util.Map r6 = r5.getHeaderFields()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r6 == 0) goto L_0x0149
                boolean r7 = r6.isEmpty()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r7 != 0) goto L_0x0149
                com.millennialmedia.internal.AdMetadata r7 = new com.millennialmedia.internal.AdMetadata     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r7.<init>()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.Set r6 = r6.entrySet()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.Iterator r6 = r6.iterator()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
            L_0x010b:
                boolean r8 = r6.hasNext()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r8 == 0) goto L_0x0145
                java.lang.Object r8 = r6.next()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.Map$Entry r8 = (java.util.Map.Entry) r8     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.Object r9 = r8.getKey()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r9 == 0) goto L_0x010b
                java.lang.Object r9 = r8.getValue()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r9 == 0) goto L_0x013c
                java.lang.Object r9 = r8.getValue()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.List r9 = (java.util.List) r9     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                boolean r9 = r9.isEmpty()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                if (r9 != 0) goto L_0x013c
                java.lang.Object r9 = r8.getValue()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.util.List r9 = (java.util.List) r9     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.Object r9 = r9.get(r2)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r9 = (java.lang.String) r9     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                goto L_0x013d
            L_0x013c:
                r9 = r3
            L_0x013d:
                java.lang.Object r8 = r8.getKey()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r7.put(r8, r9)     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                goto L_0x010b
            L_0x0145:
                com.millennialmedia.internal.utils.HttpUtils$Response r6 = r14.response     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r6.adMetadata = r7     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
            L_0x0149:
                int r6 = r5.getResponseCode()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                com.millennialmedia.internal.utils.HttpUtils$Response r7 = r14.response     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r7.code = r6     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r7 = 200(0xc8, float:2.8E-43)
                if (r6 != r7) goto L_0x017f
                com.millennialmedia.internal.utils.HttpUtils$Response r6 = r14.response     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.lang.String r7 = r5.getContentType()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                r6.contentType = r7     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                java.io.InputStream r6 = r5.getInputStream()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                com.millennialmedia.internal.utils.HttpUtils$ResponseStreamer r7 = r14.responseStreamer     // Catch:{ SocketTimeoutException -> 0x017a, Exception -> 0x0172, all -> 0x016b }
                com.millennialmedia.internal.utils.HttpUtils$Response r8 = r14.response     // Catch:{ SocketTimeoutException -> 0x017a, Exception -> 0x0172, all -> 0x016b }
                r7.streamContent(r6, r8)     // Catch:{ SocketTimeoutException -> 0x017a, Exception -> 0x0172, all -> 0x016b }
                r7 = r3
                goto L_0x01c5
            L_0x016b:
                r0 = move-exception
                r7 = r3
                r8 = r5
                r5 = r7
                r3 = r6
                goto L_0x028e
            L_0x0172:
                r7 = move-exception
                r8 = r5
                r5 = r3
                r3 = r6
                r6 = r7
                r7 = r5
                goto L_0x01f6
            L_0x017a:
                r7 = r3
                r3 = r6
                r6 = r7
                goto L_0x0250
            L_0x017f:
                if (r6 < r1) goto L_0x01c3
                java.io.InputStream r7 = r5.getErrorStream()     // Catch:{ SocketTimeoutException -> 0x01ea, Exception -> 0x01e5, all -> 0x01df }
                com.millennialmedia.internal.utils.HttpUtils$Response r8 = r14.response     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                java.lang.String r9 = com.millennialmedia.internal.utils.IOUtils.convertStreamToString(r7)     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                r8.content = r9     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                java.lang.String r8 = com.millennialmedia.internal.utils.HttpUtils.TAG     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                java.util.Locale r9 = java.util.Locale.getDefault()     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                java.lang.String r10 = "HTTP ERROR.\n\trequestId: %d\n\tcode: %d\n\tmessage: %s"
                r11 = 3
                java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                long r12 = r14.requestId     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                java.lang.Long r12 = java.lang.Long.valueOf(r12)     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                r11[r2] = r12     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                r11[r4] = r6     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                com.millennialmedia.internal.utils.HttpUtils$Response r6 = r14.response     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                java.lang.String r6 = r6.content     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                r11[r0] = r6     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                java.lang.String r6 = java.lang.String.format(r9, r10, r11)     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                com.millennialmedia.MMLog.e(r8, r6)     // Catch:{ SocketTimeoutException -> 0x01c0, Exception -> 0x01bc, all -> 0x01b7 }
                r6 = r3
                goto L_0x01c5
            L_0x01b7:
                r0 = move-exception
                r8 = r5
                r5 = r3
                goto L_0x028e
            L_0x01bc:
                r6 = move-exception
                r8 = r5
                r5 = r3
                goto L_0x01f6
            L_0x01c0:
                r6 = r3
                goto L_0x0250
            L_0x01c3:
                r6 = r3
                r7 = r6
            L_0x01c5:
                com.millennialmedia.internal.utils.IOUtils.closeStream(r6)
                com.millennialmedia.internal.utils.IOUtils.closeStream(r3)
                com.millennialmedia.internal.utils.IOUtils.closeStream(r7)
                if (r5 == 0) goto L_0x0285
                goto L_0x0282
            L_0x01d2:
                r0 = move-exception
                r7 = r3
                goto L_0x028c
            L_0x01d6:
                r7 = move-exception
                r8 = r5
                r5 = r6
                r6 = r7
                r7 = r3
                goto L_0x01f6
            L_0x01dc:
                r7 = r3
                goto L_0x0250
            L_0x01df:
                r0 = move-exception
                r7 = r3
                r8 = r5
                r5 = r7
                goto L_0x028e
            L_0x01e5:
                r6 = move-exception
                r7 = r3
                r8 = r5
                r5 = r7
                goto L_0x01f6
            L_0x01ea:
                r6 = r3
                goto L_0x024f
            L_0x01ec:
                r0 = move-exception
                r5 = r3
                r7 = r5
                r8 = r7
                goto L_0x028e
            L_0x01f2:
                r6 = move-exception
                r5 = r3
                r7 = r5
                r8 = r7
            L_0x01f6:
                com.millennialmedia.internal.utils.HttpUtils$Response r9 = r14.response     // Catch:{ all -> 0x024b }
                r9.code = r1     // Catch:{ all -> 0x024b }
                java.lang.String r1 = com.millennialmedia.internal.utils.HttpUtils.TAG     // Catch:{ all -> 0x024b }
                java.util.Locale r9 = java.util.Locale.getDefault()     // Catch:{ all -> 0x024b }
                java.lang.String r10 = "Error occurred when trying to get response content.\n\trequestId: %d\n\texception: %s"
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x024b }
                long r11 = r14.requestId     // Catch:{ all -> 0x024b }
                java.lang.Long r11 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x024b }
                r0[r2] = r11     // Catch:{ all -> 0x024b }
                java.lang.String r11 = r6.toString()     // Catch:{ all -> 0x024b }
                r0[r4] = r11     // Catch:{ all -> 0x024b }
                java.lang.String r0 = java.lang.String.format(r9, r10, r0)     // Catch:{ all -> 0x024b }
                com.millennialmedia.MMLog.e(r1, r0)     // Catch:{ all -> 0x024b }
                boolean r0 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x024b }
                if (r0 == 0) goto L_0x023c
                java.lang.String r0 = com.millennialmedia.internal.utils.HttpUtils.TAG     // Catch:{ all -> 0x024b }
                java.util.Locale r1 = java.util.Locale.getDefault()     // Catch:{ all -> 0x024b }
                java.lang.String r9 = "Debug info for requestId: %d"
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x024b }
                long r10 = r14.requestId     // Catch:{ all -> 0x024b }
                java.lang.Long r10 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x024b }
                r4[r2] = r10     // Catch:{ all -> 0x024b }
                java.lang.String r1 = java.lang.String.format(r1, r9, r4)     // Catch:{ all -> 0x024b }
                com.millennialmedia.MMLog.d(r0, r1, r6)     // Catch:{ all -> 0x024b }
            L_0x023c:
                com.millennialmedia.internal.utils.IOUtils.closeStream(r3)
                com.millennialmedia.internal.utils.IOUtils.closeStream(r5)
                com.millennialmedia.internal.utils.IOUtils.closeStream(r7)
                if (r8 == 0) goto L_0x0285
                r8.disconnect()
                goto L_0x0285
            L_0x024b:
                r0 = move-exception
                goto L_0x028e
            L_0x024d:
                r5 = r3
                r6 = r5
            L_0x024f:
                r7 = r6
            L_0x0250:
                com.millennialmedia.internal.utils.HttpUtils$Response r0 = r14.response     // Catch:{ all -> 0x028b }
                r1 = 408(0x198, float:5.72E-43)
                r0.code = r1     // Catch:{ all -> 0x028b }
                boolean r0 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x028b }
                if (r0 == 0) goto L_0x0277
                java.lang.String r0 = com.millennialmedia.internal.utils.HttpUtils.TAG     // Catch:{ all -> 0x028b }
                java.util.Locale r1 = java.util.Locale.getDefault()     // Catch:{ all -> 0x028b }
                java.lang.String r8 = "HTTP request socket timed out.\n\trequestId: %d"
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x028b }
                long r9 = r14.requestId     // Catch:{ all -> 0x028b }
                java.lang.Long r9 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x028b }
                r4[r2] = r9     // Catch:{ all -> 0x028b }
                java.lang.String r1 = java.lang.String.format(r1, r8, r4)     // Catch:{ all -> 0x028b }
                com.millennialmedia.MMLog.d(r0, r1)     // Catch:{ all -> 0x028b }
            L_0x0277:
                com.millennialmedia.internal.utils.IOUtils.closeStream(r3)
                com.millennialmedia.internal.utils.IOUtils.closeStream(r6)
                com.millennialmedia.internal.utils.IOUtils.closeStream(r7)
                if (r5 == 0) goto L_0x0285
            L_0x0282:
                r5.disconnect()
            L_0x0285:
                java.util.concurrent.CountDownLatch r0 = r14.latch
                r0.countDown()
                return
            L_0x028b:
                r0 = move-exception
            L_0x028c:
                r8 = r5
                r5 = r6
            L_0x028e:
                com.millennialmedia.internal.utils.IOUtils.closeStream(r3)
                com.millennialmedia.internal.utils.IOUtils.closeStream(r5)
                com.millennialmedia.internal.utils.IOUtils.closeStream(r7)
                if (r8 == 0) goto L_0x029c
                r8.disconnect()
            L_0x029c:
                java.util.concurrent.CountDownLatch r1 = r14.latch
                r1.countDown()
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.utils.HttpUtils.HttpRequestRunner.run():void");
        }
    }

    public static Response sendHttpRequest(String str, String str2, String str3, Integer num, ResponseStreamer responseStreamer) {
        int i;
        long currentTimeMillis = System.currentTimeMillis();
        if (num == null) {
            i = DEFAULT_TIMEOUT;
        } else {
            i = num.intValue();
        }
        HttpRequestRunner httpRequestRunner = new HttpRequestRunner(currentTimeMillis, str, str2, str3, i, responseStreamer);
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format(Locale.getDefault(), "Sending Http request.\n\t%s", httpRequestRunner.toString()));
        }
        if (httpInterceptor != null) {
            httpInterceptor.onRequest(str, httpRequestRunner);
        }
        ThreadUtils.runOnWorkerThread(httpRequestRunner);
        Response waitForResponse = httpRequestRunner.waitForResponse((long) i);
        if (httpInterceptor != null) {
            httpInterceptor.onResponse(str, waitForResponse);
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format(Locale.getDefault(), "Http response.\n\trequestId: %d\n\t%s", Long.valueOf(currentTimeMillis), waitForResponse.toString()));
        }
        return waitForResponse;
    }

    public static Response getContentFromGetRequest(String str) {
        return sendHttpRequest(str, null, null, null, new IOUtils.StringStreamer());
    }

    public static Response getContentFromGetRequest(String str, int i) {
        return sendHttpRequest(str, null, null, Integer.valueOf(i), new IOUtils.StringStreamer());
    }

    public static Response getContentFromPostRequest(String str, String str2, String str3) {
        return sendHttpRequest(str, str2, str3, null, new IOUtils.StringStreamer());
    }

    public static Response getContentFromPostRequest(String str, String str2, String str3, int i) {
        return sendHttpRequest(str, str2, str3, Integer.valueOf(i), new IOUtils.StringStreamer());
    }

    public static Response getBitmapFromGetRequest(String str) {
        return sendHttpRequest(str, null, null, null, new IOUtils.BitmapStreamer());
    }

    public static Response getContentFromPostRequest(String str, int i) {
        return sendHttpRequest(str, null, null, Integer.valueOf(i), new IOUtils.StringStreamer());
    }

    public static void setInterceptor(HttpInterceptor httpInterceptor2) {
        httpInterceptor = httpInterceptor2;
    }

    public static void getBitmapFromGetRequestAsync(final String str, final HttpRequestListener httpRequestListener) {
        if (httpRequestListener != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    httpRequestListener.onResponse(HttpUtils.getBitmapFromGetRequest(str));
                }
            });
        }
    }
}
