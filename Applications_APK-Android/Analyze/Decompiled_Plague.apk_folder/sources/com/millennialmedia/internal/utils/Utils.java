package com.millennialmedia.internal.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Vibrator;
import android.text.TextUtils;
import android.widget.Toast;
import com.google.android.gms.drive.DriveFile;
import com.millennialmedia.MMException;
import com.millennialmedia.MMLog;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Utils {
    private static final String TAG = "Utils";

    public interface VibrateListener {
        void onError();

        void onFinished();

        void onStarted();
    }

    static String byteArrayToHex(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%02X", Byte.valueOf(bArr[i])));
        }
        return sb.toString();
    }

    public static void injectIfNotNull(Map<String, Object> map, String str, Object obj) {
        if (str == null) {
            MMLog.e(TAG, "Unable to inject value, specified key is null");
        } else if (obj != null) {
            map.put(str, obj);
        }
    }

    public static List<String> splitCommaSeparateString(String str) {
        if (str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String trim : str.split(",")) {
            String trim2 = trim.trim();
            if (!arrayList.contains(trim2)) {
                arrayList.add(trim2);
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return arrayList;
    }

    public static void showToast(final Context context, final String str) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(context, str, 0).show();
            }
        });
    }

    @SuppressLint({"MissingPermission"})
    public static void vibrate(long[] jArr, int i, final VibrateListener vibrateListener) {
        if (vibrateListener == null) {
            MMLog.e(TAG, "Unable to call vibrate, provided listener cannot be null");
        } else if (!EnvironmentUtils.hasVibratePermission()) {
            MMLog.e(TAG, "Unable to call vibrate, permission is not available");
            vibrateListener.onError();
        } else {
            ((Vibrator) EnvironmentUtils.getApplicationContext().getSystemService("vibrator")).vibrate(jArr, -1);
            vibrateListener.onStarted();
            long j = 0;
            int i2 = 0;
            while (i2 < jArr.length) {
                i2++;
                j += jArr[i2];
            }
            ThreadUtils.runOnWorkerThreadDelayed(new Runnable() {
                public void run() {
                    vibrateListener.onFinished();
                }
            }, j);
        }
    }

    public static void logAndFireMMException(String str, String str2) throws MMException {
        MMLog.e(str, str2);
        throw new MMException(str2);
    }

    public static boolean startActivity(Context context, Intent intent) {
        if (!(context instanceof Activity)) {
            intent.addFlags(DriveFile.MODE_READ_ONLY);
        }
        try {
            context.startActivity(intent);
            return true;
        } catch (Exception e) {
            String str = TAG;
            MMLog.e(str, "Unable to start activity for intent: " + e.getMessage());
            return false;
        }
    }

    public static boolean startActivityFromUrl(String str) {
        if (TextUtils.isEmpty(str)) {
            MMLog.e(TAG, "Unable to start activity for null url");
            return false;
        }
        return startActivity(EnvironmentUtils.getApplicationContext(), new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    public static boolean isSchemeAvailable(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (!str.contains(":")) {
            str = str + ":";
        }
        List<ResolveInfo> queryIntentActivities = EnvironmentUtils.getApplicationContext().getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(str)), 65536);
        if (queryIntentActivities == null || queryIntentActivities.size() <= 0) {
            return false;
        }
        return true;
    }

    public static boolean isPackageAvailable(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            EnvironmentUtils.getApplicationContext().getPackageManager().getPackageInfo(str, 128);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        }
        return str.trim().isEmpty();
    }

    public static String trimStringNotEmpty(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (!trim.isEmpty()) {
            return trim;
        }
        return null;
    }

    public static boolean isActivityContext(Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            return true;
        }
        while (context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();
            if (context instanceof Activity) {
                return true;
            }
        }
        return false;
    }

    public static void putIfNotNull(Map map, Object obj, Object obj2) {
        if (obj2 != null) {
            map.put(obj, obj2);
        }
    }

    public static boolean isEqual(Rect rect, Rect rect2) {
        if (rect == null) {
            return rect2 == null;
        }
        return rect.equals(rect2);
    }
}
