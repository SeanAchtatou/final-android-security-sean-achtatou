package com.millennialmedia.internal.utils;

import android.os.Handler;
import android.os.Looper;
import com.millennialmedia.MMException;
import com.millennialmedia.MMLog;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadUtils {
    private static final String TAG = "ThreadUtils";
    /* access modifiers changed from: private */
    public static Handler uiHandler;
    /* access modifiers changed from: private */
    public static ExecutorService workerExecutor;
    /* access modifiers changed from: private */
    public static Handler workerHandler;

    public interface ScheduledRunnable extends Runnable {
        void cancel();
    }

    public static void initialize() throws MMException {
        boolean z;
        if (uiHandler != null) {
            MMLog.w(TAG, "ThreadUtils already initialized");
            return;
        }
        uiHandler = new Handler(Looper.getMainLooper());
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        new Thread() {
            public void run() {
                Looper.prepare();
                Handler unused = ThreadUtils.workerHandler = new Handler();
                countDownLatch.countDown();
                Looper.loop();
            }
        }.start();
        workerExecutor = Executors.newCachedThreadPool();
        try {
            z = countDownLatch.await(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            MMLog.e(TAG, "Failed to initialize latch", e);
            z = false;
        }
        if (!z) {
            throw new MMException("Failed to initialize ThreadUtils");
        }
    }

    public static void postOnUiThread(Runnable runnable) {
        uiHandler.post(runnable);
    }

    public static void postOnUiThread(Runnable runnable, long j) {
        uiHandler.postDelayed(runnable, j);
    }

    @Deprecated
    public static void runOnUiThread(Runnable runnable) {
        postOnUiThread(runnable);
    }

    public static void runOffUiThread(Runnable runnable) {
        if (isUiThread()) {
            workerExecutor.execute(runnable);
        } else {
            runnable.run();
        }
    }

    public static void runOnWorkerThread(Runnable runnable) {
        workerExecutor.execute(runnable);
    }

    public static ScheduledRunnable runOnUiThreadDelayed(final Runnable runnable, long j) {
        AnonymousClass2 r0 = new ScheduledRunnable() {
            public void run() {
                runnable.run();
            }

            public void cancel() {
                ThreadUtils.uiHandler.removeCallbacks(this);
            }
        };
        uiHandler.postDelayed(r0, j);
        return r0;
    }

    public static ScheduledRunnable runOnWorkerThreadDelayed(final Runnable runnable, long j) {
        AnonymousClass3 r0 = new ScheduledRunnable() {
            public void run() {
                ThreadUtils.workerExecutor.execute(runnable);
            }

            public void cancel() {
                ThreadUtils.workerHandler.removeCallbacks(this);
            }
        };
        workerHandler.postDelayed(r0, j);
        return r0;
    }

    public static boolean isUiThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static int getActiveWorkerThreadCount() {
        return ((ThreadPoolExecutor) workerExecutor).getActiveCount();
    }
}
