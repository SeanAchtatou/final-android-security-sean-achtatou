package com.millennialmedia.internal.utils;

import com.millennialmedia.MMLog;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashSet;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class MMSSLSocketFactory extends SSLSocketFactory {
    private static final String[] SUPPORTED_CIPHER_SUITES = {"TLS_RSA_WITH_AES_128_CBC_SHA", "TLS_RSA_WITH_AES_128_GCM_SHA256", "TLS_RSA_WITH_AES_256_CBC_SHA", "TLS_RSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"};
    private static final String[] SUPPORTED_PROTOCOLS = {"TLSv1.1", "TLSv1.2"};
    private static final String TAG = "MMSSLSocketFactory";
    private static volatile MMSSLSocketFactory instance = new MMSSLSocketFactory().init();
    private static String sslContextFactoryClass;
    private String[] enabledCipherSuites = null;
    private String[] enabledProtocols = null;
    private SSLSocketFactory socketFactory;

    public interface MMSSLContextFactory {
        SSLContext getInstance(String str) throws NoSuchAlgorithmException, KeyManagementException;
    }

    private class DefaultMMSSLContextFactory implements MMSSLContextFactory {
        private DefaultMMSSLContextFactory() {
        }

        public SSLContext getInstance(String str) throws NoSuchAlgorithmException, KeyManagementException {
            SSLContext instance = SSLContext.getInstance(str);
            instance.init(null, null, null);
            return instance;
        }
    }

    static void setSSLContextFactoryClass(String str) {
        sslContextFactoryClass = str;
        instance.init();
    }

    private MMSSLSocketFactory() {
    }

    private MMSSLSocketFactory init() {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Initializing MMSSLSocketFactory");
        }
        try {
            SSLContext sSLContext = getSSLContext();
            if (sSLContext != null) {
                this.socketFactory = sSLContext.getSocketFactory();
                SSLSocket sSLSocket = (SSLSocket) this.socketFactory.createSocket();
                this.enabledProtocols = getEnabledEntries(sSLSocket.getSupportedProtocols(), SUPPORTED_PROTOCOLS);
                this.enabledCipherSuites = getEnabledEntries(sSLSocket.getSupportedCipherSuites(), SUPPORTED_CIPHER_SUITES);
                sSLSocket.close();
                if (MMLog.isDebugEnabled()) {
                    String str = TAG;
                    MMLog.d(str, "Protocols enabled: " + Arrays.toString(this.enabledProtocols));
                    String str2 = TAG;
                    MMLog.d(str2, "Cipher suites enabled: " + Arrays.toString(this.enabledCipherSuites));
                }
                return this;
            }
            MMLog.e(TAG, "Failed to instantiate a valid SSLContext.");
            return null;
        } catch (Exception e) {
            MMLog.e(TAG, "Failed to initialize MMSSLSocketFactory", e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private javax.net.ssl.SSLContext getSSLContext() throws java.security.NoSuchAlgorithmException, java.security.KeyManagementException {
        /*
            r4 = this;
            java.lang.String r0 = com.millennialmedia.internal.utils.MMSSLSocketFactory.sslContextFactoryClass
            r1 = 0
            if (r0 == 0) goto L_0x002f
            java.lang.String r0 = com.millennialmedia.internal.utils.MMSSLSocketFactory.sslContextFactoryClass     // Catch:{ Exception -> 0x0012 }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0012 }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Exception -> 0x0012 }
            com.millennialmedia.internal.utils.MMSSLSocketFactory$MMSSLContextFactory r0 = (com.millennialmedia.internal.utils.MMSSLSocketFactory.MMSSLContextFactory) r0     // Catch:{ Exception -> 0x0012 }
            goto L_0x0030
        L_0x0012:
            java.lang.String r0 = com.millennialmedia.internal.utils.MMSSLSocketFactory.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not instantiate custom MMSSLContextFactory using class = "
            r2.append(r3)
            java.lang.String r3 = com.millennialmedia.internal.utils.MMSSLSocketFactory.sslContextFactoryClass
            r2.append(r3)
            java.lang.String r3 = ", reverting to default."
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.millennialmedia.MMLog.e(r0, r2)
        L_0x002f:
            r0 = r1
        L_0x0030:
            if (r0 != 0) goto L_0x0037
            com.millennialmedia.internal.utils.MMSSLSocketFactory$DefaultMMSSLContextFactory r0 = new com.millennialmedia.internal.utils.MMSSLSocketFactory$DefaultMMSSLContextFactory
            r0.<init>()
        L_0x0037:
            java.lang.String r1 = "TLS"
            javax.net.ssl.SSLContext r0 = r0.getInstance(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.utils.MMSSLSocketFactory.getSSLContext():javax.net.ssl.SSLContext");
    }

    public static MMSSLSocketFactory getInstance() {
        return instance;
    }

    public String[] getDefaultCipherSuites() {
        return this.enabledCipherSuites;
    }

    public String[] getSupportedCipherSuites() {
        return this.enabledCipherSuites;
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return secureSocket(this.socketFactory.createSocket(socket, str, i, z));
    }

    public Socket createSocket(String str, int i) throws IOException {
        return secureSocket(this.socketFactory.createSocket(str, i));
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        return secureSocket(this.socketFactory.createSocket(str, i, inetAddress, i2));
    }

    public Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return secureSocket(this.socketFactory.createSocket(inetAddress, i));
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return secureSocket(this.socketFactory.createSocket(inetAddress, i, inetAddress2, i2));
    }

    private Socket secureSocket(Socket socket) {
        if (socket instanceof SSLSocket) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Setting enabled protocols and cipher suites on secure socket");
            }
            try {
                ((SSLSocket) socket).setEnabledProtocols(this.enabledProtocols);
                ((SSLSocket) socket).setEnabledCipherSuites(this.enabledCipherSuites);
            } catch (Exception e) {
                String str = TAG;
                MMLog.e(str, "Failed to set secure socket properties - " + e.getMessage());
            }
        }
        return socket;
    }

    private static String[] getEnabledEntries(String[] strArr, String[] strArr2) {
        HashSet hashSet = new HashSet(Arrays.asList(strArr));
        HashSet hashSet2 = new HashSet(Arrays.asList(strArr2));
        hashSet2.retainAll(hashSet);
        return (String[]) hashSet2.toArray(new String[0]);
    }
}
