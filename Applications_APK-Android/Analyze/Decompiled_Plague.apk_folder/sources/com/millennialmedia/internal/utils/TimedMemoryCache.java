package com.millennialmedia.internal.utils;

import android.text.TextUtils;
import com.millennialmedia.MMLog;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class TimedMemoryCache<O> {
    private static final long CACHE_TIMEOUT = 60000;
    private static final long FORCE_REMOVE_TIME = -1;
    /* access modifiers changed from: private */
    public static final String TAG = "TimedMemoryCache";
    /* access modifiers changed from: private */
    public static long cleanerDelay = 10000;
    /* access modifiers changed from: private */
    public Map<String, CacheItem> cache = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public AtomicBoolean cleanerRunning = new AtomicBoolean();
    private AtomicInteger lastCacheId = new AtomicInteger(0);

    /* access modifiers changed from: protected */
    public void onExpired(String str, Object obj) {
    }

    /* access modifiers changed from: protected */
    public void onOverwritten(String str, Object obj) {
    }

    private static class CacheItem<T> {
        T cachedObject;
        long itemTimeout;

        CacheItem(T t, Long l) {
            if (l == null) {
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(TimedMemoryCache.TAG, "Cached item timeout is null, setting to default: 60000");
                }
                l = Long.valueOf((long) TimedMemoryCache.CACHE_TIMEOUT);
            }
            this.cachedObject = t;
            this.itemTimeout = System.currentTimeMillis() + l.longValue();
        }

        public String toString() {
            return "cachedObject: " + ((Object) this.cachedObject) + ", itemTimeout: " + this.itemTimeout;
        }
    }

    public static void setCleanerDelay(long j) {
        cleanerDelay = j;
    }

    public String add(O o, Long l) {
        return add(null, o, l);
    }

    public String add(String str, O o, Long l) {
        if (o == null) {
            MMLog.e(TAG, "Nothing to cache, object provided is null");
            return null;
        }
        if (TextUtils.isEmpty(str)) {
            str = String.valueOf(this.lastCacheId.incrementAndGet());
        }
        CacheItem cacheItem = this.cache.get(str);
        if (cacheItem != null) {
            onOverwritten(str, cacheItem.cachedObject);
        }
        CacheItem cacheItem2 = new CacheItem(o, l);
        this.cache.put(str, cacheItem2);
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Add CacheItem\n\tID: " + str + "\n\tItem: " + cacheItem2);
        }
        startCleaner();
        return str;
    }

    public O get(String str) {
        CacheItem cachedItem = getCachedItem(str);
        if (cachedItem != null) {
            this.cache.remove(str);
            return cachedItem.cachedObject;
        } else if (!MMLog.isDebugEnabled()) {
            return null;
        } else {
            String str2 = TAG;
            MMLog.d(str2, "No item in cache for ID <" + str + ">");
            return null;
        }
    }

    private CacheItem getCachedItem(String str) {
        if (str == null) {
            return null;
        }
        CacheItem cacheItem = this.cache.get(str);
        if (cacheItem == null) {
            this.cache.remove(str);
            return null;
        } else if (removeItemIfExpired(str, cacheItem, System.currentTimeMillis())) {
            return null;
        } else {
            return cacheItem;
        }
    }

    public boolean containsKey(String str) {
        return this.cache.containsKey(str);
    }

    private void startCleaner() {
        if (this.cleanerRunning.compareAndSet(false, true)) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    do {
                        try {
                            Thread.sleep(TimedMemoryCache.cleanerDelay);
                            TimedMemoryCache.this.runCleaner(System.currentTimeMillis());
                        } catch (InterruptedException e) {
                            MMLog.e(TimedMemoryCache.TAG, "Error occurred while cleaner was sleeping", e);
                        }
                    } while (TimedMemoryCache.this.cache.size() > 0);
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(TimedMemoryCache.TAG, "Stopping cleaner");
                    }
                    TimedMemoryCache.this.cleanerRunning.set(false);
                }
            });
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Cleaner already running");
        }
    }

    /* access modifiers changed from: private */
    public void runCleaner(long j) {
        for (Map.Entry next : this.cache.entrySet()) {
            String str = (String) next.getKey();
            CacheItem cacheItem = (CacheItem) next.getValue();
            if (cacheItem != null) {
                removeItemIfExpired(str, cacheItem, j);
            } else if (MMLog.isDebugEnabled()) {
                String str2 = TAG;
                MMLog.d(str2, "Attempted to remove CacheItem with ID <" + str + "> but item was null");
            }
        }
    }

    public void expireAll() {
        runCleaner(-1);
    }

    private boolean removeItemIfExpired(String str, CacheItem cacheItem, long j) {
        if (j <= cacheItem.itemTimeout && j != -1) {
            return false;
        }
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Removed CacheItem\n\t:Checked time: " + j + "\n\tID: " + str + "\n\tItem: " + cacheItem);
        }
        this.cache.remove(str);
        onExpired(str, cacheItem.cachedObject);
        return true;
    }
}
