package com.millennialmedia.internal.utils;

public class VideoTrackingEvent extends TrackingEvent {
    private static final String TAG = "VideoTrackingEvent";
    public int position;

    public VideoTrackingEvent(String str, String str2, int i) {
        super(str, str2);
        this.position = i;
    }

    public VideoTrackingEvent(TrackingEvent trackingEvent, int i) {
        this(trackingEvent.name, trackingEvent.url, i);
    }

    public String toString() {
        return super.toString() + "(position:" + this.position + ")";
    }
}
