package com.millennialmedia.internal.utils;

final class AmazonAdvertisingIdInfo implements AdvertisingIdInfo {
    private String id;
    private boolean limitAdTracking;

    AmazonAdvertisingIdInfo(String str, int i) {
        this.id = str;
        this.limitAdTracking = i != 0;
    }

    public boolean isLimitAdTrackingEnabled() {
        return this.limitAdTracking;
    }

    public String getId() {
        return this.id;
    }

    public String toString() {
        return "AmazonAdvertisingIdInfo{id='" + this.id + '\'' + ", limitAdTracking=" + this.limitAdTracking + '}';
    }
}
