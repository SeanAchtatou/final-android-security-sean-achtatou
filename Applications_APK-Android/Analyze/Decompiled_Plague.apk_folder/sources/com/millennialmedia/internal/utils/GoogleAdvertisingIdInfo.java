package com.millennialmedia.internal.utils;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;

final class GoogleAdvertisingIdInfo implements AdvertisingIdInfo {
    private AdvertisingIdClient.Info info;

    GoogleAdvertisingIdInfo(AdvertisingIdClient.Info info2) {
        this.info = info2;
    }

    public boolean isLimitAdTrackingEnabled() {
        return this.info != null && this.info.isLimitAdTrackingEnabled();
    }

    public String getId() {
        if (this.info != null) {
            return this.info.getId();
        }
        return null;
    }

    public String toString() {
        return "GoogleAdvertisingIdInfo{id='" + getId() + '\'' + ", limitAdTracking=" + isLimitAdTrackingEnabled() + '}';
    }
}
