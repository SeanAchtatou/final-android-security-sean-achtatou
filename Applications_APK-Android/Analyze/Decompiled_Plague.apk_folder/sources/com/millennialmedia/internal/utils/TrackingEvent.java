package com.millennialmedia.internal.utils;

import com.millennialmedia.MMLog;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TrackingEvent {
    /* access modifiers changed from: private */
    public static final String TAG = "TrackingEvent";
    /* access modifiers changed from: private */
    public static AtomicInteger activeCount = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public static TrackingEventListener trackingEventListener;
    public String name;
    public String url;

    public interface TrackingEventListener {
        void onTrackingEventFired(TrackingEvent trackingEvent);
    }

    public TrackingEvent(String str, String str2) {
        this.name = str;
        this.url = str2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackingEvent)) {
            return false;
        }
        TrackingEvent trackingEvent = (TrackingEvent) obj;
        return this.name.equals(trackingEvent.name) && this.url.equals(trackingEvent.url);
    }

    public int hashCode() {
        return (31 * this.url.hashCode()) + this.name.hashCode();
    }

    public String toString() {
        return (("Event:[" + "name:" + this.name + ";") + "url:" + this.url) + "]";
    }

    public static void setTrackingEventListener(TrackingEventListener trackingEventListener2) {
        trackingEventListener = trackingEventListener2;
    }

    public static void fireUrls(List<String> list, String str) {
        if (list != null && !list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (String next : list) {
                if (!Utils.isEmpty(next)) {
                    arrayList.add(new TrackingEvent(str, next));
                }
            }
            fireEvents(arrayList);
        }
    }

    public static void fireEvents(final List<TrackingEvent> list) {
        if (list != null && !list.isEmpty()) {
            activeCount.incrementAndGet();
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    for (TrackingEvent trackingEvent : list) {
                        if (trackingEvent != null && !Utils.isEmpty(trackingEvent.url)) {
                            if (MMLog.isDebugEnabled()) {
                                String access$000 = TrackingEvent.TAG;
                                MMLog.d(access$000, "Firing event " + trackingEvent.toString());
                            }
                            HttpUtils.getContentFromGetRequest(trackingEvent.url);
                            if (TrackingEvent.trackingEventListener != null) {
                                TrackingEvent.trackingEventListener.onTrackingEventFired(trackingEvent);
                            }
                        }
                    }
                    TrackingEvent.activeCount.decrementAndGet();
                }
            });
        }
    }

    public static boolean isIdle() {
        return activeCount.intValue() == 0;
    }
}
