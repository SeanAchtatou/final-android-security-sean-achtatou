package com.millennialmedia.internal.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.os.StatFs;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.EnvironmentCompat;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.millennialmedia.AppInfo;
import com.millennialmedia.MMInitializationException;
import com.millennialmedia.MMLog;
import com.millennialmedia.MMSDK;
import com.millennialmedia.internal.UserPrivacy;
import com.moat.analytics.mobile.aol.BuildConfig;
import com.mopub.mobileads.GooglePlayServicesInterstitial;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;

public class EnvironmentUtils {
    private static final String MILLENNIAL_DIRECTORY = "/.com.millennialmedia/";
    public static final String ORIENTATION_LANDSCAPE = "landscape";
    public static final String ORIENTATION_PORTRAIT = "portrait";
    /* access modifiers changed from: private */
    public static final String TAG = "EnvironmentUtils";
    /* access modifiers changed from: private */
    public static volatile AdvertisingIdInfo advertisingIdInfo;
    @SuppressLint({"StaticFieldLeak"})
    private static Application application;
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static Context applicationContext;
    /* access modifiers changed from: private */
    public static AvailableCameras availableCameras;
    /* access modifiers changed from: private */
    public static boolean bluetoothPermissionGranted;
    /* access modifiers changed from: private */
    public static Integer cellSignalDbm;
    private static String deviceId;
    /* access modifiers changed from: private */
    public static boolean fineLocationPermissionGranted;
    /* access modifiers changed from: private */
    public static boolean microphonePermissionGranted;
    /* access modifiers changed from: private */
    public static boolean nfcPermissionGranted;
    /* access modifiers changed from: private */
    public static boolean readExternalStoragePermissionGranted;
    /* access modifiers changed from: private */
    public static volatile String userAgent;
    /* access modifiers changed from: private */
    public static boolean vibratePermissionGranted;
    /* access modifiers changed from: private */
    public static boolean wifiStatePermissionGranted;
    /* access modifiers changed from: private */
    public static boolean writeExternalStoragePermissionGranted;

    public static class AvailableCameras {
        public boolean backCamera;
        public boolean frontCamera;
    }

    public static void init(Application application2) throws MMInitializationException {
        application = application2;
        applicationContext = application2.getApplicationContext();
        registerWebViewUsageWithAndroid();
        if (Build.VERSION.SDK_INT < 17) {
            extractUserAgentFromWebView();
        }
        extractPermissions();
        registerSignalStrengthListener();
        availableCameras = getCameraInfo();
        MediaContentProvider.verifyMediaContentProvider(applicationContext);
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                EnvironmentUtils.loadAdvertisingInfo();
                String access$100 = EnvironmentUtils.TAG;
                MMLog.i(access$100, "Environment initialized with the following data.\n\tMillennial Media Ad SDK version: 6.8.1-72925a6 (release)\n\tAndroid SDK version: " + Build.VERSION.SDK_INT + "\n\tApplication name: " + EnvironmentUtils.getApplicationName() + "\n\tApplication id: " + EnvironmentUtils.getAppId() + "\n\tLocale country " + EnvironmentUtils.getLocaleCountry() + "\n\tLocale language: " + EnvironmentUtils.getLocaleLanguage() + "\n\tExternal storage available: " + EnvironmentUtils.isExternalStorageReadable() + "\n\tDisplay width: " + EnvironmentUtils.getDisplayWidth() + "\n\tDisplay height: " + EnvironmentUtils.getDisplayHeight() + "\n\tDisplay density: " + EnvironmentUtils.getDisplayDensity() + "\n\tDisplay dpi: " + EnvironmentUtils.getDisplayDensityDpi() + "\n\tNatural screen orientation: " + EnvironmentUtils.getNaturalConfigOrientationString() + "\n\tREAD_EXTERNAL_STORAGE permission available: " + EnvironmentUtils.readExternalStoragePermissionGranted + "\n\tWRITE_EXTERNAL_STORAGE permission available: " + EnvironmentUtils.writeExternalStoragePermissionGranted + "\n\tACCESS_WIFI_STATE permission available: " + EnvironmentUtils.wifiStatePermissionGranted + "\n\tACCESS_FINE_LOCATION permission available: " + EnvironmentUtils.fineLocationPermissionGranted + "\n\tVIBRATE permission available: " + EnvironmentUtils.vibratePermissionGranted + "\n\tBLUETOOTH permission available: " + EnvironmentUtils.bluetoothPermissionGranted + "\n\tNFC permission available: " + EnvironmentUtils.nfcPermissionGranted + "\n\tRECORD_AUDIO permission available: " + EnvironmentUtils.microphonePermissionGranted + "\n\tFront camera available: " + EnvironmentUtils.availableCameras.frontCamera + "\n\tBack camera available: " + EnvironmentUtils.availableCameras.backCamera + "\n\tAdvertising ID: " + EnvironmentUtils.getAdvertisingId(EnvironmentUtils.advertisingIdInfo) + "\n\tLimit ad tracking enabled: " + EnvironmentUtils.isLimitAdTrackingEnabled(EnvironmentUtils.advertisingIdInfo) + "\n\n");
            }
        });
    }

    private static void registerWebViewUsageWithAndroid() throws MMInitializationException {
        try {
            Class.forName("android.webkit.WebView");
        } catch (Exception e) {
            MMLog.e(TAG, "Unable to find system webview class", e);
            throw new MMInitializationException("Unable to initialize SDK. The system webview class is currently unavailable, most likely due to a system update in progress. Reinitialize the SDK after the system webview update has finished.");
        }
    }

    private static void registerSignalStrengthListener() {
        ((TelephonyManager) application.getSystemService("phone")).listen(new PhoneStateListener() {
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                super.onSignalStrengthsChanged(signalStrength);
                if (signalStrength.isGsm()) {
                    Integer unused = EnvironmentUtils.cellSignalDbm = Integer.valueOf((signalStrength.getGsmSignalStrength() * 2) - 113);
                } else {
                    Integer unused2 = EnvironmentUtils.cellSignalDbm = Integer.valueOf(signalStrength.getCdmaDbm());
                }
                if (MMLog.isDebugEnabled()) {
                    String access$100 = EnvironmentUtils.TAG;
                    MMLog.d(access$100, "Cell signal DBM updated to: " + EnvironmentUtils.cellSignalDbm);
                }
            }
        }, 256);
    }

    private static void extractPermissions() {
        writeExternalStoragePermissionGranted = Build.VERSION.SDK_INT > 22 || checkPermissionGranted("android.permission.WRITE_EXTERNAL_STORAGE");
        readExternalStoragePermissionGranted = checkPermissionGranted("android.permission.READ_EXTERNAL_STORAGE");
        wifiStatePermissionGranted = checkPermissionGranted("android.permission.ACCESS_WIFI_STATE");
        fineLocationPermissionGranted = checkPermissionGranted("android.permission.ACCESS_FINE_LOCATION");
        vibratePermissionGranted = checkPermissionGranted("android.permission.VIBRATE");
        bluetoothPermissionGranted = checkPermissionGranted("android.permission.BLUETOOTH");
        nfcPermissionGranted = checkPermissionGranted("android.permission.NFC");
        microphonePermissionGranted = checkPermissionGranted("android.permission.RECORD_AUDIO");
    }

    @TargetApi(21)
    private static AvailableCameras getCameraInfo() {
        availableCameras = new AvailableCameras();
        int i = 0;
        availableCameras.frontCamera = false;
        availableCameras.backCamera = false;
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                CameraManager cameraManager = (CameraManager) applicationContext.getSystemService("camera");
                String[] cameraIdList = cameraManager.getCameraIdList();
                int length = cameraIdList.length;
                while (i < length) {
                    int intValue = ((Integer) cameraManager.getCameraCharacteristics(cameraIdList[i]).get(CameraCharacteristics.LENS_FACING)).intValue();
                    if (intValue == 0) {
                        availableCameras.frontCamera = true;
                    } else if (intValue == 1) {
                        availableCameras.backCamera = true;
                    }
                    i++;
                }
            } else {
                Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                int numberOfCameras = Camera.getNumberOfCameras();
                while (i < numberOfCameras) {
                    Camera.getCameraInfo(i, cameraInfo);
                    if (cameraInfo.facing == 1) {
                        availableCameras.frontCamera = true;
                    } else if (cameraInfo.facing == 0) {
                        availableCameras.backCamera = true;
                    }
                    i++;
                }
            }
        } catch (Exception e) {
            MMLog.e(TAG, "Error retrieving camera information for device", e);
        } catch (AssertionError e2) {
            String str = TAG;
            MMLog.e(str, "AssertionError while retrieving camera info <" + e2.getMessage() + "> -- ignored");
        }
        return availableCameras;
    }

    public static Application getApplication() {
        return application;
    }

    public static Context getApplicationContext() {
        return applicationContext;
    }

    public static float getDisplayDensity() {
        return applicationContext.getResources().getDisplayMetrics().density;
    }

    public static int getDisplayDensityDpi() {
        return applicationContext.getResources().getDisplayMetrics().densityDpi;
    }

    @TargetApi(17)
    public static int getDisplayHeight() {
        if (Build.VERSION.SDK_INT < 17) {
            return applicationContext.getResources().getDisplayMetrics().heightPixels;
        }
        Display defaultDisplay = ((WindowManager) applicationContext.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getRealSize(point);
        return point.y;
    }

    @TargetApi(17)
    public static int getDisplayWidth() {
        if (Build.VERSION.SDK_INT < 17) {
            return applicationContext.getResources().getDisplayMetrics().widthPixels;
        }
        Display defaultDisplay = ((WindowManager) applicationContext.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getRealSize(point);
        return point.x;
    }

    public static AdvertisingIdInfo getAdInfo() {
        return advertisingIdInfo;
    }

    public static String getAdvertisingId(AdvertisingIdInfo advertisingIdInfo2) {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        if (advertisingIdInfo2 != null) {
            return advertisingIdInfo2.getId();
        }
        MMLog.e(TAG, "Unable to get advertisering id value");
        return null;
    }

    public static boolean isLimitAdTrackingEnabled(AdvertisingIdInfo advertisingIdInfo2) {
        if (advertisingIdInfo2 != null) {
            return advertisingIdInfo2.isLimitAdTrackingEnabled();
        }
        MMLog.e(TAG, "Unable to get limit ad tracking value, ad info is null");
        return false;
    }

    @SuppressLint({"HardwareIds"})
    public static synchronized String getHashedDeviceId(String str) {
        synchronized (EnvironmentUtils.class) {
            if (!UserPrivacy.canPassUserData()) {
                return null;
            }
            String string = Settings.Secure.getString(applicationContext.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
            if (string == null) {
                return null;
            }
            try {
                String byteArrayToHex = Utils.byteArrayToHex(MessageDigest.getInstance(str).digest(string.getBytes("UTF-8")));
                return byteArrayToHex;
            } catch (Exception e) {
                String str2 = TAG;
                MMLog.e(str2, "Exception calculating <" + str + "> hashed device id with ANDROID_ID <" + string + ">", e);
                return null;
            }
        }
    }

    @SuppressLint({"HardwareIds"})
    static synchronized String getDeviceId() {
        synchronized (EnvironmentUtils.class) {
            if (deviceId != null) {
                String str = deviceId;
                return str;
            }
            String string = Settings.Secure.getString(applicationContext.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
            if (string == null) {
                return null;
            }
            StringBuilder sb = new StringBuilder("mmh_");
            try {
                sb.append(Utils.byteArrayToHex(MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"))));
                sb.append('_');
                sb.append(Utils.byteArrayToHex(MessageDigest.getInstance("SHA1").digest(string.getBytes("UTF-8"))));
                deviceId = sb.toString();
                String str2 = deviceId;
                return str2;
            } catch (Exception e) {
                String str3 = TAG;
                MMLog.e(str3, "Exception calculating device id hash with ANDROID_ID <" + string + ">", e);
                return null;
            }
        }
    }

    public static Integer getMcc() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        Configuration configuration = applicationContext.getResources().getConfiguration();
        if (configuration.mcc != 0) {
            return Integer.valueOf(configuration.mcc);
        }
        String networkOperator = getNetworkOperator();
        if (networkOperator != null && networkOperator.length() >= 6) {
            try {
                return Integer.valueOf(Integer.parseInt(networkOperator.substring(0, 3)));
            } catch (NumberFormatException e) {
                MMLog.w(TAG, "Unable to parse mcc from network operator", e);
            }
        }
        MMLog.w(TAG, "Unable to retrieve mcc");
        return null;
    }

    public static Integer getMnc() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        Configuration configuration = applicationContext.getResources().getConfiguration();
        if (configuration.mnc != 0) {
            return Integer.valueOf(configuration.mnc);
        }
        String networkOperator = getNetworkOperator();
        if (networkOperator != null && networkOperator.length() >= 6) {
            try {
                return Integer.valueOf(Integer.parseInt(networkOperator.substring(3)));
            } catch (NumberFormatException e) {
                MMLog.w(TAG, "Unable to parse mnc from network operator", e);
            }
        }
        MMLog.w(TAG, "Unable to retrieve mnc");
        return null;
    }

    public static String getNetworkOperator() {
        return ((TelephonyManager) applicationContext.getSystemService("phone")).getNetworkOperator();
    }

    public static String getNetworkOperatorName() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return ((TelephonyManager) applicationContext.getSystemService("phone")).getNetworkOperatorName();
    }

    public static String getLocaleLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public static String getLocaleCountry() {
        return Locale.getDefault().getCountry();
    }

    public static String getApplicationName() {
        try {
            PackageManager packageManager = applicationContext.getPackageManager();
            return packageManager.getApplicationLabel(packageManager.getApplicationInfo(applicationContext.getPackageName(), 0)).toString();
        } catch (Throwable th) {
            MMLog.e(TAG, "Unable to determine package name", th);
            return null;
        }
    }

    public static String getAppId() {
        return applicationContext.getPackageName();
    }

    @TargetApi(17)
    public static String getUserAgent() {
        if (userAgent == null) {
            extractUserAgentFromWebSettings();
            if (userAgent == null) {
                extractUserAgentFromWebView();
            }
        }
        return userAgent;
    }

    private static void extractUserAgentFromWebSettings() {
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                userAgent = WebSettings.getDefaultUserAgent(applicationContext);
                String str = TAG;
                MMLog.i(str, "User agent: " + userAgent);
            } catch (Exception unused) {
                MMLog.w(TAG, "Unable to get user agent from call to getDefaultUserAgent");
            }
        }
    }

    private static void extractUserAgentFromWebView() {
        userAgent = "Android " + Build.VERSION.RELEASE;
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                try {
                    String unused = EnvironmentUtils.userAgent = new WebView(EnvironmentUtils.applicationContext).getSettings().getUserAgentString();
                    String access$100 = EnvironmentUtils.TAG;
                    MMLog.i(access$100, "User agent: " + EnvironmentUtils.userAgent);
                } catch (Exception e) {
                    MMLog.e(EnvironmentUtils.TAG, "Unable to get user agent from call to getUserAgentString", e);
                }
            }
        });
    }

    public static Integer getBatteryLevel() {
        Intent batteryIntent;
        if (!UserPrivacy.canPassUserData() || (batteryIntent = getBatteryIntent()) == null) {
            return null;
        }
        int intExtra = batteryIntent.getIntExtra("scale", -1);
        int intExtra2 = batteryIntent.getIntExtra("level", -1);
        if (intExtra == -1 || intExtra2 == -1) {
            return null;
        }
        return Integer.valueOf(Math.round((((float) intExtra2) / ((float) intExtra)) * 100.0f));
    }

    public static Boolean isDevicePlugged() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        Intent batteryIntent = getBatteryIntent();
        if (batteryIntent == null) {
            return false;
        }
        if (batteryIntent.getIntExtra("plugged", 0) == 0) {
            return false;
        }
        return true;
    }

    private static Intent getBatteryIntent() {
        return applicationContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    public static Long getAvailableInternalStorageSize() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return Long.valueOf(getAvailableSize(Environment.getRootDirectory().getAbsolutePath()));
    }

    public static Long getAvailableExternalStorageSize() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        if (isExternalStorageReadable()) {
            return Long.valueOf(getAvailableSize(Environment.getExternalStorageDirectory().getAbsolutePath()));
        }
        return 0L;
    }

    public static Long getAvailableStorageSize() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return Long.valueOf(0 + getAvailableInternalStorageSize().longValue() + getAvailableExternalStorageSize().longValue());
    }

    private static long getAvailableSize(String str) {
        StatFs statFs = new StatFs(str);
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    public static boolean checkPermissionGranted(String str) {
        try {
            return ContextCompat.checkSelfPermission(applicationContext, str) == 0;
        } catch (Exception e) {
            String str2 = TAG;
            MMLog.e(str2, "Unable to check permission " + str, e);
            return false;
        }
    }

    public static boolean isExternalStorageReadable() {
        String externalStorageState = Environment.getExternalStorageState();
        return readExternalStoragePermissionGranted && ("mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState));
    }

    public static boolean isExternalStorageWritable() {
        return writeExternalStoragePermissionGranted && "mounted".equals(Environment.getExternalStorageState());
    }

    public static boolean isExternalStorageSupported() {
        return writeExternalStoragePermissionGranted;
    }

    public static boolean isNetworkAvailable() {
        NetworkInfo networkInfo = getNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static String getNetworkConnectionType() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        NetworkInfo networkInfo = getNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected()) {
            return "offline";
        }
        int type = networkInfo.getType();
        if (type == 1) {
            return TapjoyConstants.TJC_CONNECTION_TYPE_WIFI;
        }
        if (type != 0) {
            return EnvironmentCompat.MEDIA_UNKNOWN;
        }
        switch (networkInfo.getSubtype()) {
            case 1:
                return "gprs";
            case 2:
                return "edge";
            case 3:
                return "umts";
            case 4:
                return "cdma";
            case 5:
                return "evdo_0";
            case 6:
                return "evdo_a";
            case 7:
                return "1xrtt";
            case 8:
                return "hsdpa";
            case 9:
                return "hsupa";
            case 10:
                return "hspa";
            case 11:
                return "iden";
            case 12:
                return "evdo_b";
            case 13:
                return "lte";
            case 14:
                return "ehrpd";
            case 15:
                return "hspap";
            default:
                return EnvironmentCompat.MEDIA_UNKNOWN;
        }
    }

    @SuppressLint({"MissingPermission"})
    private static NetworkInfo getNetworkInfo() {
        ConnectivityManager connectivityManager = (ConnectivityManager) applicationContext.getSystemService("connectivity");
        if (connectivityManager == null) {
            return null;
        }
        return connectivityManager.getActiveNetworkInfo();
    }

    public static String getCellSignalDbm() {
        if (UserPrivacy.canPassUserData() && cellSignalDbm != null) {
            return cellSignalDbm.toString();
        }
        return null;
    }

    public static String getIpAddress() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        try {
            for (NetworkInterface inetAddresses : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                Iterator it = Collections.list(inetAddresses.getInetAddresses()).iterator();
                while (true) {
                    if (it.hasNext()) {
                        InetAddress inetAddress = (InetAddress) it.next();
                        if (!inetAddress.isLoopbackAddress()) {
                            String upperCase = inetAddress.getHostAddress().toUpperCase();
                            if (inetAddress instanceof Inet4Address) {
                                return upperCase;
                            }
                            int indexOf = upperCase.indexOf(37);
                            return indexOf < 0 ? upperCase : upperCase.substring(0, indexOf);
                        }
                    }
                }
            }
        } catch (Exception e) {
            MMLog.e(TAG, "Unable to determine IP address for device", e);
        }
        return null;
    }

    public static int getCurrentConfigOrientation() {
        return applicationContext.getResources().getConfiguration().orientation;
    }

    public static String getCurrentConfigOrientationString() {
        switch (applicationContext.getResources().getConfiguration().orientation) {
            case 1:
                return ORIENTATION_PORTRAIT;
            case 2:
                return "landscape";
            default:
                return getNaturalConfigOrientationString();
        }
    }

    public static int getNaturalConfigOrientation() {
        Configuration configuration = applicationContext.getResources().getConfiguration();
        int rotation = ((WindowManager) applicationContext.getSystemService("window")).getDefaultDisplay().getRotation();
        if (configuration.orientation == 2 && (rotation == 0 || rotation == 2)) {
            return 2;
        }
        if (configuration.orientation == 1 && (rotation == 1 || rotation == 3)) {
            return 2;
        }
        return 1;
    }

    public static String getNaturalConfigOrientationString() {
        return getNaturalConfigOrientation() == 2 ? "landscape" : ORIENTATION_PORTRAIT;
    }

    public static int getConfigOrientationFromRequestedOrientation(int i) {
        switch (i) {
            case -1:
                return 0;
            case 0:
            case 6:
            case 8:
            case 11:
                return 2;
            case 1:
            case 7:
            case 9:
            case 12:
                return 1;
            case 2:
            case 3:
            case 4:
            case 5:
            case 10:
            default:
                return getCurrentConfigOrientation();
        }
    }

    @SuppressLint({"MissingPermission"})
    public static Location getLocation() {
        LocationManager locationManager;
        if (UserPrivacy.canPassUserData() && fineLocationPermissionGranted && (locationManager = (LocationManager) applicationContext.getSystemService((String) GooglePlayServicesInterstitial.LOCATION_KEY)) != null) {
            return locationManager.getLastKnownLocation("passive");
        }
        return null;
    }

    public static boolean hasGps() {
        return applicationContext.getPackageManager().hasSystemFeature("android.hardware.location.gps");
    }

    public static Boolean hasFineLocationPermission() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return Boolean.valueOf(fineLocationPermissionGranted);
    }

    public static boolean hasMicrophone() {
        return applicationContext.getPackageManager().hasSystemFeature("android.hardware.microphone");
    }

    public static Boolean hasMicrophonePermission() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return Boolean.valueOf(microphonePermissionGranted);
    }

    public static boolean hasCamera() {
        return availableCameras.backCamera || availableCameras.frontCamera;
    }

    public static AvailableCameras getAvailableCameras() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return availableCameras;
    }

    public static boolean hasVibratePermission() {
        return vibratePermissionGranted;
    }

    public static boolean hasBluetooth() {
        return applicationContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth");
    }

    public static Boolean hasBluetoothPermission() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return Boolean.valueOf(bluetoothPermissionGranted);
    }

    public static boolean hasNfc() {
        return applicationContext.getPackageManager().hasSystemFeature("android.hardware.nfc");
    }

    public static Boolean hasNfcPermission() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return Boolean.valueOf(nfcPermissionGranted);
    }

    public static Integer getVolume(int i) {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        AudioManager audioManager = (AudioManager) applicationContext.getSystemService("audio");
        int streamMaxVolume = audioManager.getStreamMaxVolume(i);
        if (streamMaxVolume < 1) {
            return 0;
        }
        return Integer.valueOf((int) (((float) audioManager.getStreamVolume(i)) * (100.0f / ((float) streamMaxVolume))));
    }

    public static Boolean areHeadphonesPluggedIn() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return Boolean.valueOf(((AudioManager) applicationContext.getSystemService("audio")).isWiredHeadsetOn());
    }

    public static File getMillennialDir() {
        File file = new File(applicationContext.getFilesDir() + MILLENNIAL_DIRECTORY);
        file.mkdirs();
        return file;
    }

    public static File getPicturesDirectory() {
        File file;
        if (Build.VERSION.SDK_INT >= 23) {
            File[] externalMediaDirs = applicationContext.getExternalMediaDirs();
            file = externalMediaDirs.length > 0 ? externalMediaDirs[0] : null;
        } else {
            file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        }
        if (file == null) {
            return file;
        }
        File file2 = new File(file.getAbsolutePath() + File.separator + BuildConfig.NAMESPACE + File.separator + getApplicationName());
        file2.mkdirs();
        return file2;
    }

    public static boolean isSmsSupported() {
        return applicationContext.getPackageManager().hasSystemFeature("android.hardware.telephony");
    }

    public static boolean isTelSupported() {
        return applicationContext.getPackageManager().hasSystemFeature("android.hardware.telephony");
    }

    @TargetApi(23)
    public static boolean isDeviceIdle() {
        if (Build.VERSION.SDK_INT >= 23) {
            return ((PowerManager) application.getSystemService("power")).isDeviceIdleMode();
        }
        return false;
    }

    public static String getModel() {
        if (!UserPrivacy.canPassUserData()) {
            return null;
        }
        return Build.MODEL;
    }

    public static boolean resourceExists(String str, String str2) {
        return resourceExists(str, str2, applicationContext.getPackageName());
    }

    public static boolean resourceExists(String str, String str2, String str3) {
        return applicationContext.getResources().getIdentifier(str, str2, str3) != 0;
    }

    public static void getResourceValueFrom(String str, String str2, TypedValue typedValue, boolean z) {
        Resources resources = applicationContext.getResources();
        resources.getValue(applicationContext.getPackageName() + ":" + str2 + "/" + str, typedValue, z);
    }

    public static boolean isCoppaEnabled() {
        AppInfo appInfo = MMSDK.getAppInfo();
        if (appInfo == null || appInfo.getCoppa() == null) {
            return false;
        }
        return appInfo.getCoppa().booleanValue();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:20|21|22|23|(1:25)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0069 */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006f A[Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void loadAdvertisingInfo() {
        /*
            java.lang.Class<com.millennialmedia.internal.utils.EnvironmentUtils> r0 = com.millennialmedia.internal.utils.EnvironmentUtils.class
            monitor-enter(r0)
            com.google.android.gms.common.GoogleApiAvailability r1 = com.google.android.gms.common.GoogleApiAvailability.getInstance()     // Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }
            android.content.Context r2 = com.millennialmedia.internal.utils.EnvironmentUtils.applicationContext     // Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }
            int r1 = r1.isGooglePlayServicesAvailable(r2)     // Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }
            if (r1 != 0) goto L_0x004b
            android.content.Context r1 = com.millennialmedia.internal.utils.EnvironmentUtils.applicationContext     // Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r1 = com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo(r1)     // Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }
            com.millennialmedia.internal.utils.GoogleAdvertisingIdInfo r2 = new com.millennialmedia.internal.utils.GoogleAdvertisingIdInfo     // Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }
            com.millennialmedia.internal.utils.EnvironmentUtils.advertisingIdInfo = r2     // Catch:{ IOException -> 0x0043, GooglePlayServicesNotAvailableException -> 0x003a, IllegalStateException -> 0x0031, GooglePlayServicesRepairableException -> 0x0028, Throwable -> 0x001f }
            goto L_0x004b
        L_0x001d:
            r1 = move-exception
            goto L_0x008d
        L_0x001f:
            r1 = move-exception
            java.lang.String r2 = com.millennialmedia.internal.utils.EnvironmentUtils.TAG     // Catch:{ all -> 0x001d }
            java.lang.String r3 = "Unable to get google play services advertising info, error obtaining advertising info from google play services"
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x001d }
            goto L_0x004b
        L_0x0028:
            r1 = move-exception
            java.lang.String r2 = com.millennialmedia.internal.utils.EnvironmentUtils.TAG     // Catch:{ all -> 0x001d }
            java.lang.String r3 = "Unable to get google play services advertising info, google play services is not installed, up-to-date, or enabled"
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x001d }
            goto L_0x004b
        L_0x0031:
            r1 = move-exception
            java.lang.String r2 = com.millennialmedia.internal.utils.EnvironmentUtils.TAG     // Catch:{ all -> 0x001d }
            java.lang.String r3 = "Unable to get google play services advertising info, illegal state"
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x001d }
            goto L_0x004b
        L_0x003a:
            r1 = move-exception
            java.lang.String r2 = com.millennialmedia.internal.utils.EnvironmentUtils.TAG     // Catch:{ all -> 0x001d }
            java.lang.String r3 = "Unable to get google play services advertising info, google play services is not available"
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x001d }
            goto L_0x004b
        L_0x0043:
            r1 = move-exception
            java.lang.String r2 = com.millennialmedia.internal.utils.EnvironmentUtils.TAG     // Catch:{ all -> 0x001d }
            java.lang.String r3 = "Unable to get google play services advertising info, google play services (e.g., the old version of the service doesn't support getting advertising ID)"
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x001d }
        L_0x004b:
            com.millennialmedia.internal.utils.AdvertisingIdInfo r1 = com.millennialmedia.internal.utils.EnvironmentUtils.advertisingIdInfo     // Catch:{ all -> 0x001d }
            if (r1 != 0) goto L_0x0076
            android.content.Context r1 = com.millennialmedia.internal.utils.EnvironmentUtils.applicationContext     // Catch:{ SettingNotFoundException -> 0x0069 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ SettingNotFoundException -> 0x0069 }
            java.lang.String r2 = "limit_ad_tracking"
            int r2 = android.provider.Settings.Secure.getInt(r1, r2)     // Catch:{ SettingNotFoundException -> 0x0069 }
            java.lang.String r3 = "advertising_id"
            java.lang.String r1 = android.provider.Settings.Secure.getString(r1, r3)     // Catch:{ SettingNotFoundException -> 0x0069 }
            com.millennialmedia.internal.utils.AmazonAdvertisingIdInfo r3 = new com.millennialmedia.internal.utils.AmazonAdvertisingIdInfo     // Catch:{ SettingNotFoundException -> 0x0069 }
            r3.<init>(r1, r2)     // Catch:{ SettingNotFoundException -> 0x0069 }
            com.millennialmedia.internal.utils.EnvironmentUtils.advertisingIdInfo = r3     // Catch:{ SettingNotFoundException -> 0x0069 }
            goto L_0x0076
        L_0x0069:
            boolean r1 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x001d }
            if (r1 == 0) goto L_0x0076
            java.lang.String r1 = com.millennialmedia.internal.utils.EnvironmentUtils.TAG     // Catch:{ all -> 0x001d }
            java.lang.String r2 = "Amazon advertiser info not available."
            com.millennialmedia.MMLog.d(r1, r2)     // Catch:{ all -> 0x001d }
        L_0x0076:
            boolean r1 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x001d }
            if (r1 == 0) goto L_0x008b
            com.millennialmedia.internal.utils.AdvertisingIdInfo r1 = com.millennialmedia.internal.utils.EnvironmentUtils.advertisingIdInfo     // Catch:{ all -> 0x001d }
            if (r1 == 0) goto L_0x008b
            java.lang.String r1 = com.millennialmedia.internal.utils.EnvironmentUtils.TAG     // Catch:{ all -> 0x001d }
            com.millennialmedia.internal.utils.AdvertisingIdInfo r2 = com.millennialmedia.internal.utils.EnvironmentUtils.advertisingIdInfo     // Catch:{ all -> 0x001d }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x001d }
            com.millennialmedia.MMLog.d(r1, r2)     // Catch:{ all -> 0x001d }
        L_0x008b:
            monitor-exit(r0)
            return
        L_0x008d:
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.utils.EnvironmentUtils.loadAdvertisingInfo():void");
    }
}
