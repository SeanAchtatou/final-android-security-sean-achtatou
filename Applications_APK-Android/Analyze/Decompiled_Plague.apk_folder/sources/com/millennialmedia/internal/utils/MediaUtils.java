package com.millennialmedia.internal.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Base64;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.MMIntentWrapperActivity;
import com.millennialmedia.internal.utils.IOUtils;
import com.mopub.common.Constants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MediaUtils {
    /* access modifiers changed from: private */
    public static final String TAG = "MediaUtils";

    public interface PhotoListener {
        void onError(String str);

        void onPhoto(Uri uri);
    }

    public interface PlayVideoListener {
        void onError(String str);

        void onVideoStarted(Uri uri);
    }

    public interface SavePictureListener {
        void onError(String str);

        void onPictureSaved(File file);
    }

    public static void savePicture(final Context context, String str, String str2, final SavePictureListener savePictureListener) {
        File file;
        if (savePictureListener == null) {
            MMLog.e(TAG, "PictureListener is required");
        } else if (!EnvironmentUtils.isExternalStorageWritable()) {
            savePictureListener.onError("Storage not mounted, cannot add image to photo library");
        } else if (str == null) {
            savePictureListener.onError("url is required");
        } else {
            Uri parse = Uri.parse(str);
            if (parse.getScheme().startsWith(Constants.HTTP)) {
                File picturesDirectory = EnvironmentUtils.getPicturesDirectory();
                if (picturesDirectory == null) {
                    savePictureListener.onError("Cannot access pictures directory");
                    return;
                }
                if (str2 == null) {
                    file = IOUtils.getUniqueFileName(picturesDirectory, parse.getLastPathSegment());
                } else {
                    file = IOUtils.getUniqueFileName(picturesDirectory, str2);
                }
                if (file == null) {
                    savePictureListener.onError("Unable to store photo");
                } else {
                    IOUtils.downloadFile(str, null, file, new IOUtils.DownloadListener() {
                        public void onDownloadSucceeded(File file) {
                            if (MMLog.isDebugEnabled()) {
                                String access$000 = MediaUtils.TAG;
                                MMLog.d(access$000, "Picture downloaded to: " + file.getAbsolutePath());
                            }
                            MediaUtils.scanPicture(context, file, savePictureListener, true);
                        }

                        public void onDownloadFailed(Throwable th) {
                            savePictureListener.onError("Unable to download file");
                        }
                    });
                }
            } else {
                File file2 = new File(parse.getPath());
                if (!file2.exists()) {
                    savePictureListener.onError("No file found at " + file2.getAbsolutePath());
                    return;
                }
                scanPicture(context, file2, savePictureListener, false);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void scanPicture(Context context, final File file, final SavePictureListener savePictureListener, final boolean z) {
        MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
            public void onScanCompleted(String str, Uri uri) {
                if (uri == null) {
                    if (z) {
                        file.delete();
                    }
                    SavePictureListener savePictureListener = savePictureListener;
                    savePictureListener.onError("Failed to scan file " + str);
                    return;
                }
                savePictureListener.onPictureSaved(file);
            }
        });
    }

    public static void startVideoPlayer(Context context, String str, PlayVideoListener playVideoListener) {
        if (playVideoListener == null) {
            MMLog.e(TAG, "VideoListener is required");
        } else if (str == null) {
            playVideoListener.onError("url is required");
        } else {
            Uri parse = Uri.parse(str);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(parse, "video/*");
            if (Utils.startActivity(context, intent)) {
                playVideoListener.onVideoStarted(parse);
            } else {
                playVideoListener.onError("No video application installed");
            }
        }
    }

    public static void getPhotoFromCamera(Context context, final PhotoListener photoListener) {
        Uri uri;
        if (photoListener == null) {
            MMLog.e(TAG, "PhotoListener is required");
        } else if (!EnvironmentUtils.hasCamera()) {
            photoListener.onError("This device does not have a camera");
        } else {
            File picturesDirectory = EnvironmentUtils.getPicturesDirectory();
            if (picturesDirectory == null) {
                photoListener.onError("Cannot access pictures directory");
                return;
            }
            try {
                final File createTempFile = File.createTempFile("CAMERA_", ".tmp", picturesDirectory);
                if (Build.VERSION.SDK_INT >= 24) {
                    uri = MediaContentProvider.getUriForMediaContentProvider(EnvironmentUtils.getApplicationContext(), createTempFile);
                } else {
                    uri = Uri.fromFile(createTempFile);
                }
                if (uri == null) {
                    photoListener.onError("Unable to get URI for temporary file for picture");
                    return;
                }
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra("output", uri);
                MMIntentWrapperActivity.launch(context, intent, new MMIntentWrapperActivity.MMIntentWrapperListener() {
                    public void onData(Intent intent) {
                        if (createTempFile == null || !createTempFile.exists()) {
                            photoListener.onError("Unable to get image from camera");
                            return;
                        }
                        photoListener.onPhoto(Uri.fromFile(createTempFile));
                        createTempFile.delete();
                    }

                    public void onError(String str) {
                        if (createTempFile != null && createTempFile.exists()) {
                            createTempFile.delete();
                        }
                        photoListener.onError(str);
                    }
                });
            } catch (IOException unused) {
                photoListener.onError("Unable to create temporary file for picture");
            }
        }
    }

    public static void getPhotoFromGallery(Context context, final PhotoListener photoListener) {
        if (photoListener == null) {
            MMLog.e(TAG, "PhotoListener is required");
        } else {
            MMIntentWrapperActivity.launch(context, getPictureChooserIntent(), new MMIntentWrapperActivity.MMIntentWrapperListener() {
                public void onData(Intent intent) {
                    Uri data;
                    if (intent == null || (data = intent.getData()) == null) {
                        photoListener.onError("Unable to get image from gallery");
                    } else {
                        photoListener.onPhoto(data);
                    }
                }

                public void onError(String str) {
                    photoListener.onError(str);
                }
            });
        }
    }

    private static Intent getPictureChooserIntent() {
        return new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    }

    public static boolean isPictureChooserAvailable() {
        List<ResolveInfo> queryIntentActivities;
        if (!EnvironmentUtils.isExternalStorageReadable() || (queryIntentActivities = EnvironmentUtils.getApplicationContext().getPackageManager().queryIntentActivities(getPictureChooserIntent(), 65536)) == null || queryIntentActivities.size() <= 0) {
            return false;
        }
        return true;
    }

    private static Bitmap decodeBitmapUri(Context context, Uri uri, BitmapFactory.Options options) throws Exception {
        try {
            InputStream openInputStream = context.getContentResolver().openInputStream(uri);
            Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream, null, options);
            IOUtils.closeStream(openInputStream);
            return decodeStream;
        } catch (Exception e) {
            String str = TAG;
            MMLog.e(str, "Bitmap file not found <" + uri.toString() + ">", e);
            throw e;
        }
    }

    public static String getMimeTypeFromUri(Context context, Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            decodeBitmapUri(context, uri, options);
            return options.outMimeType;
        } catch (Exception unused) {
            return null;
        }
    }

    private static int getRotationAngle(Context context, Uri uri) {
        int i;
        String path = uri.getPath();
        Cursor query = context.getContentResolver().query(uri, new String[]{"_data"}, null, null, null);
        if (query != null) {
            int columnIndex = query.getColumnIndex("_data");
            if (query.moveToFirst()) {
                path = query.getString(columnIndex);
            }
            query.close();
        }
        if (path == null) {
            return 0;
        }
        try {
            int attributeInt = new ExifInterface(new File(path).getCanonicalPath()).getAttributeInt("Orientation", 1);
            if (attributeInt == 3) {
                i = 180;
            } else if (attributeInt == 6) {
                i = 90;
            } else if (attributeInt != 8) {
                return 0;
            } else {
                i = 270;
            }
            return i;
        } catch (IOException unused) {
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getScaledBitmapFromUri(Context context, Uri uri, int i, int i2, boolean z, boolean z2) {
        int i3;
        int i4;
        int i5;
        Context context2 = context;
        Uri uri2 = uri;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            decodeBitmapUri(context2, uri2, options);
            if (z2) {
                int rotationAngle = getRotationAngle(context, uri);
                if (rotationAngle == 90 || rotationAngle == 270) {
                    i4 = i;
                    i3 = rotationAngle;
                    i5 = i2;
                } else {
                    i4 = i2;
                    i3 = rotationAngle;
                    i5 = i;
                }
            } else {
                i5 = i;
                i4 = i2;
                i3 = 0;
            }
            options.inSampleSize = 1;
            if (options.outWidth > i5 || options.outHeight > i4) {
                int i6 = options.outWidth / 2;
                int i7 = options.outHeight / 2;
                while (i6 / options.inSampleSize > i5 && i7 / options.inSampleSize > i4) {
                    options.inSampleSize *= 2;
                }
            }
            options.inJustDecodeBounds = false;
            try {
                Bitmap decodeBitmapUri = decodeBitmapUri(context2, uri2, options);
                if (decodeBitmapUri == null || options.outWidth == 0 || options.outHeight == 0) {
                    MMLog.e(TAG, "Failed to load bitmap <" + uri.toString() + ">");
                    return null;
                }
                float min = Math.min(1.0f, ((float) i5) / ((float) options.outWidth));
                float min2 = Math.min(1.0f, ((float) i4) / ((float) options.outHeight));
                if (z) {
                    min = Math.min(min, min2);
                    min2 = min;
                }
                if (min == 1.0f && min2 == 1.0f && i3 == 0) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(TAG, "Unscaled and unrotated bitmap: " + decodeBitmapUri.getWidth() + " x " + decodeBitmapUri.getHeight());
                    }
                    return decodeBitmapUri;
                }
                Matrix matrix = new Matrix();
                matrix.postScale(min, min2);
                if (i3 > 0) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(TAG, "Rotating image " + i3 + " degrees");
                    }
                    matrix.postRotate((float) i3);
                }
                Bitmap createBitmap = Bitmap.createBitmap(decodeBitmapUri, 0, 0, options.outWidth, options.outHeight, matrix, true);
                if (createBitmap == null) {
                    MMLog.e(TAG, "Unable to create scaled bitmap");
                } else if (MMLog.isDebugEnabled()) {
                    MMLog.d(TAG, "Scaled and rotated bitmap: " + createBitmap.getWidth() + " x " + createBitmap.getHeight());
                }
                decodeBitmapUri.recycle();
                return createBitmap;
            } catch (Exception unused) {
                return null;
            }
        } catch (Exception unused2) {
            return null;
        }
    }

    public static String base64EncodeBitmap(Bitmap bitmap, String str) {
        Bitmap.CompressFormat compressFormat;
        if ("image/jpg".equalsIgnoreCase(str) || "image/jpeg".equalsIgnoreCase(str)) {
            compressFormat = Bitmap.CompressFormat.JPEG;
        } else if ("image/webp".equalsIgnoreCase(str)) {
            compressFormat = Bitmap.CompressFormat.WEBP;
        } else {
            str = "image/png";
            compressFormat = Bitmap.CompressFormat.PNG;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (!bitmap.compress(compressFormat, 100, byteArrayOutputStream)) {
            MMLog.e(TAG, "Unable to compress bitmap for encoding");
            return null;
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return "data:" + str + ";base64," + Base64.encodeToString(byteArray, 0);
    }

    public static void setFileDescription(File file, String str) {
        if (str != null) {
            try {
                ExifInterface exifInterface = new ExifInterface(file.getCanonicalPath());
                exifInterface.setAttribute("UserComment", str);
                exifInterface.saveAttributes();
            } catch (IOException unused) {
                if (MMLog.isDebugEnabled()) {
                    String str2 = TAG;
                    MMLog.d(str2, "Cannot set description on media file <" + file.getAbsolutePath() + ">");
                }
            }
        }
    }
}
