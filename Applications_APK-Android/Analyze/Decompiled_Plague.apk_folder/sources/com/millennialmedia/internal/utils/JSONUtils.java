package com.millennialmedia.internal.utils;

import com.millennialmedia.MMLog;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtils {
    private static final String TAG = "com.millennialmedia.internal.utils.JSONUtils";

    public static JSONArray buildFromList(List list) {
        if (list == null) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (Object buildFromObject : list) {
            jSONArray.put(buildFromObject(buildFromObject));
        }
        return jSONArray;
    }

    public static JSONObject buildFromMap(Map<String, ? extends Object> map) {
        if (map == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            for (Map.Entry next : map.entrySet()) {
                jSONObject.put((String) next.getKey(), buildFromObject(next.getValue()));
            }
        } catch (Exception unused) {
            MMLog.e(TAG, "Error building JSON from Map");
        }
        return jSONObject;
    }

    public static Object buildFromObject(Object obj) {
        if (obj instanceof Map) {
            return buildFromMap((Map) obj);
        }
        return obj instanceof List ? buildFromList((List) obj) : obj;
    }

    public static String[] convertToStringArray(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null) {
            return new String[0];
        }
        String[] strArr = new String[jSONArray.length()];
        for (int i = 0; i < jSONArray.length(); i++) {
            strArr[i] = jSONArray.getString(i);
        }
        return strArr;
    }

    public static String[] getStringArray(JSONObject jSONObject, String str) throws JSONException {
        if (jSONObject != null) {
            return convertToStringArray(jSONObject.getJSONArray(str));
        }
        return new String[0];
    }

    public static int optInt(JSONObject jSONObject, String str, int i) {
        return jSONObject != null ? jSONObject.optInt(str, i) : i;
    }

    public static String getNonEmptyString(JSONObject jSONObject, String str) throws JSONException {
        if (jSONObject == null) {
            throw new JSONException("The passed jsonObject is null.");
        }
        String string = jSONObject.getString(str);
        if (!Utils.isEmpty(string)) {
            return string;
        }
        throw new JSONException("The value for key '" + str + "' is null or empty.");
    }

    public static boolean isEmpty(JSONArray jSONArray) {
        return jSONArray == null || jSONArray.length() <= 0;
    }

    public static void injectIfNotNull(JSONObject jSONObject, String str, Object obj) {
        if (str == null) {
            MMLog.e(TAG, "Unable to inject value, specified key is null");
        } else if (obj != null) {
            try {
                jSONObject.put(str, obj);
            } catch (Exception unused) {
                String str2 = TAG;
                MMLog.e(str2, "Error adding " + str + ":" + obj + " to JSON");
            }
        }
    }

    public static void injectIfTrue(JSONObject jSONObject, String str, Object obj, Boolean bool) {
        if (Boolean.TRUE.equals(bool)) {
            injectIfNotNull(jSONObject, str, obj);
        }
    }

    public static void injectAsStringIfNotNull(JSONObject jSONObject, String str, Object obj) {
        if (obj != null) {
            injectIfNotNull(jSONObject, str, String.valueOf(obj));
        }
    }
}
