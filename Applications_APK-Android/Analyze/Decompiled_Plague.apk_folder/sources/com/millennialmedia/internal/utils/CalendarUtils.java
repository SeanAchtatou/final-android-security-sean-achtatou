package com.millennialmedia.internal.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.text.TextUtils;
import com.millennialmedia.MMLog;
import com.mopub.mobileads.GooglePlayServicesInterstitial;
import com.tapjoy.TJAdUnitConstants;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

public class CalendarUtils {
    private static final String[] DaysInWeekArray = {"SU", "MO", "TU", "WE", "TH", "FR", "SA", "SU"};
    private static final String TAG = "CalendarUtils";
    private static final String[] calendarEventDateFormats = {"yyyy-MM-dd'T'HH:mmZZZ", "yyyy-MM-dd'T'HH:mm:ssZZZ"};
    @SuppressLint({"SimpleDateFormat"})
    private static final SimpleDateFormat rruleUntilDateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");

    public interface CalendarListener {
        void onError(String str);

        void onUIDisplayed();
    }

    public static void addEvent(Context context, JSONObject jSONObject, CalendarListener calendarListener) {
        if (calendarListener == null) {
            MMLog.e(TAG, "CalendarListener is required");
            return;
        }
        String optString = jSONObject.optString("description", null);
        String optString2 = jSONObject.optString("summary", null);
        String optString3 = jSONObject.optString(GooglePlayServicesInterstitial.LOCATION_KEY, null);
        String recurrenceRule = getRecurrenceRule(jSONObject.optJSONObject("recurrence"));
        Date parseDate = parseDate(jSONObject.optString(TJAdUnitConstants.String.VIDEO_START, null));
        Date parseDate2 = parseDate(jSONObject.optString("end", null));
        Integer transparency = getTransparency(jSONObject.optString("transparency", null));
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("Creating calendar event: title: %s, location: %s, start: %s, end: %s, description: %s, rrule: %s, transparency: %s", optString, optString3, parseDate, parseDate2, optString2, recurrenceRule, transparency));
        }
        if (optString == null || parseDate == null) {
            calendarListener.onError("Description and start are required");
            return;
        }
        Intent data = new Intent("android.intent.action.INSERT").setData(CalendarContract.Events.CONTENT_URI);
        data.putExtra("title", optString);
        data.putExtra("beginTime", parseDate.getTime());
        if (parseDate2 != null) {
            data.putExtra("endTime", parseDate2.getTime());
        }
        if (optString2 != null) {
            data.putExtra("description", optString2);
        }
        if (optString3 != null) {
            data.putExtra("eventLocation", optString3);
        }
        if (recurrenceRule != null) {
            data.putExtra("rrule", recurrenceRule);
        }
        if (transparency != null) {
            data.putExtra("availability", transparency);
        }
        if (Utils.startActivity(context, data)) {
            calendarListener.onUIDisplayed();
        } else {
            calendarListener.onError("No calendar application installed");
        }
    }

    public static String getRecurrenceRule(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        String optString = jSONObject.optString("frequency", null);
        if (optString == null) {
            MMLog.e(TAG, "frequency is required for recurrence rule");
            return null;
        }
        sb.append("FREQ=");
        sb.append(optString);
        sb.append(';');
        Date parseDate = parseDate(jSONObject.optString("expires", null));
        if (parseDate != null) {
            String format = rruleUntilDateFormat.format(parseDate);
            sb.append("UNTIL=");
            sb.append(format);
            sb.append(';');
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("daysInWeek");
        if (optJSONArray != null) {
            ArrayList<String> convertDaysToRRuleDays = convertDaysToRRuleDays(optJSONArray);
            if (convertDaysToRRuleDays.size() > 0) {
                sb.append("BYDAY=");
                sb.append(TextUtils.join(",", convertDaysToRRuleDays));
                sb.append(';');
            }
        }
        String optString2 = jSONObject.optString("daysInMonth", null);
        if (optString2 != null) {
            String replaceAll = optString2.replaceAll("\\[", "").replaceAll("\\]", "");
            sb.append("BYMONTHDAY=");
            sb.append(replaceAll);
            sb.append(';');
        }
        String optString3 = jSONObject.optString("monthsInYear", null);
        if (optString3 != null) {
            String replaceAll2 = optString3.replaceAll("\\[", "").replaceAll("\\]", "");
            sb.append("BYMONTH=");
            sb.append(replaceAll2);
            sb.append(';');
        }
        String optString4 = jSONObject.optString("daysInYear", null);
        if (optString4 != null) {
            sb.append("BYYEARDAY=");
            sb.append(optString4);
            sb.append(';');
        }
        return sb.toString().toUpperCase();
    }

    public static Date parseDate(String str) {
        Date date = null;
        if (str == null) {
            return null;
        }
        String[] strArr = calendarEventDateFormats;
        int length = strArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            String str2 = strArr[i];
            try {
                Date parse = new SimpleDateFormat(str2).parse(str);
                if (parse != null) {
                    date = parse;
                    break;
                }
                date = parse;
                i++;
            } catch (ParseException unused) {
                if (MMLog.isDebugEnabled()) {
                    String str3 = TAG;
                    MMLog.d(str3, "Parsing exception for value = " + str + " with pattern = " + str2);
                }
            }
        }
        if (date == null) {
            String str4 = TAG;
            MMLog.e(str4, "Error parsing calendar event date <" + str + ">");
        }
        return date;
    }

    public static ArrayList<String> convertDaysToRRuleDays(JSONArray jSONArray) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < jSONArray.length(); i++) {
            int optInt = jSONArray.optInt(i, 0);
            if (optInt < 0 || optInt >= DaysInWeekArray.length) {
                MMLog.e(TAG, "Invalid index for day of week <" + optInt + ">");
            } else {
                arrayList.add(DaysInWeekArray[optInt]);
            }
        }
        return arrayList;
    }

    public static Integer getTransparency(String str) {
        if (TJAdUnitConstants.String.TRANSPARENT.equals(str)) {
            return 1;
        }
        return "opaque".equals(str) ? 0 : null;
    }
}
