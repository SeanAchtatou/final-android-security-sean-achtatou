package com.millennialmedia.internal.utils;

public interface AdvertisingIdInfo {
    String getId();

    boolean isLimitAdTrackingEnabled();
}
