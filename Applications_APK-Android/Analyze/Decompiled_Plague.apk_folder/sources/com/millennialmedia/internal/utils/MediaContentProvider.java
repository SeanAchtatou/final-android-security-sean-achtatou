package com.millennialmedia.internal.utils;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.drive.DriveFile;
import com.millennialmedia.MMLog;
import java.io.File;
import java.io.FileNotFoundException;

public class MediaContentProvider extends ContentProvider {
    private static final int PHOTOS = 1;
    private static final String PROVIDER_NAME = ".MediaContentProvider";
    private static final String TAG = "MediaContentProvider";
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1);

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    @Nullable
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Nullable
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    @Nullable
    public ParcelFileDescriptor openFile(Uri uri, String str) throws FileNotFoundException {
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Request to resolve uri = " + uri.toString() + ", with mode = " + str);
        }
        if (URI_MATCHER.match(uri) != 1) {
            throw new FileNotFoundException("Requested uri is not supported by MediaContentProvider");
        }
        File file = new File(EnvironmentUtils.getPicturesDirectory(), uri.getLastPathSegment());
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("Resolved: \n%s, to:\n%s", uri, file.getPath()));
        }
        if (file.exists()) {
            return ParcelFileDescriptor.open(file, parseMode(str));
        }
        throw new FileNotFoundException(String.format("MediaContentProvider cannot find file %s", file.toString()));
    }

    public boolean onCreate() {
        URI_MATCHER.addURI(getAuthority(getContext()), "*", 1);
        return true;
    }

    public static Uri getUriForMediaContentProvider(Context context, File file) {
        if (file == null) {
            return null;
        }
        return Uri.parse("content://" + getAuthority(context) + "/" + file.getName());
    }

    @NonNull
    public static String getAuthority(Context context) {
        return context.getPackageName() + PROVIDER_NAME;
    }

    public static void verifyMediaContentProvider(Context context) {
        try {
            ProviderInfo providerInfo = context.getPackageManager().getProviderInfo(new ComponentName(context, MediaContentProvider.class), 128);
            String authority = getAuthority(context);
            if (!authority.equals(providerInfo.authority)) {
                String str = TAG;
                MMLog.e(str, "MediaContentProvider authority is not set properly in your AndroidManifest.xml. Currently set to '" + providerInfo.authority + "' but should be '" + authority + "'");
            }
        } catch (PackageManager.NameNotFoundException e) {
            MMLog.e(TAG, "MediaContentProvider not found. Please verify your AndroidManifest.xml", e);
        }
    }

    @TargetApi(19)
    private static int parseMode(String str) {
        if (Build.VERSION.SDK_INT >= 19) {
            return ParcelFileDescriptor.parseMode(str);
        }
        if ("r".equals(str)) {
            return DriveFile.MODE_READ_ONLY;
        }
        if ("w".equals(str) || "wt".equals(str)) {
            return 738197504;
        }
        if ("wa".equals(str)) {
            return 704643072;
        }
        if ("rw".equals(str)) {
            return 939524096;
        }
        if ("rwt".equals(str)) {
            return 1006632960;
        }
        throw new IllegalArgumentException("Bad mode '" + str + "'");
    }
}
