package com.millennialmedia.internal.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.HttpUtils;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class IOUtils {
    private static final int BUFFER_SIZE = 4096;
    private static final String ENCODING_UTF_8 = "UTF-8";
    /* access modifiers changed from: private */
    public static final String TAG = "IOUtils";

    public interface DownloadListener {
        void onDownloadFailed(Throwable th);

        void onDownloadSucceeded(File file);
    }

    public static byte[] read(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        read(inputStream, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static void read(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr, 0, 4096);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static String read(InputStream inputStream, String str) throws IOException {
        if (str == null) {
            str = ENCODING_UTF_8;
        }
        return new String(read(inputStream), str);
    }

    public static void write(OutputStream outputStream, String str) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, ENCODING_UTF_8);
        try {
            outputStreamWriter.write(str);
        } finally {
            if (outputStreamWriter != null) {
                outputStreamWriter.close();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0050 A[SYNTHETIC, Splitter:B:25:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0061 A[SYNTHETIC, Splitter:B:32:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String convertStreamToString(java.io.InputStream r4) {
        /*
            r0 = 0
            if (r4 != 0) goto L_0x000b
            java.lang.String r4 = com.millennialmedia.internal.utils.IOUtils.TAG
            java.lang.String r1 = "Unable to convert to string, input stream is null"
            com.millennialmedia.MMLog.e(r4, r1)
            return r0
        L_0x000b:
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0045, all -> 0x0042 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0045, all -> 0x0042 }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r4, r3)     // Catch:{ IOException -> 0x0045, all -> 0x0042 }
            r4 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r2, r4)     // Catch:{ IOException -> 0x0045, all -> 0x0042 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0040 }
            r4.<init>()     // Catch:{ IOException -> 0x0040 }
        L_0x001e:
            java.lang.String r2 = r1.readLine()     // Catch:{ IOException -> 0x0040 }
            if (r2 == 0) goto L_0x002d
            r4.append(r2)     // Catch:{ IOException -> 0x0040 }
            r2 = 10
            r4.append(r2)     // Catch:{ IOException -> 0x0040 }
            goto L_0x001e
        L_0x002d:
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0040 }
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ IOException -> 0x0037 }
            goto L_0x005d
        L_0x0037:
            r0 = move-exception
            java.lang.String r1 = com.millennialmedia.internal.utils.IOUtils.TAG
            java.lang.String r2 = "Error closing input stream reader"
            com.millennialmedia.MMLog.e(r1, r2, r0)
            goto L_0x005d
        L_0x0040:
            r4 = move-exception
            goto L_0x0047
        L_0x0042:
            r4 = move-exception
            r1 = r0
            goto L_0x005f
        L_0x0045:
            r4 = move-exception
            r1 = r0
        L_0x0047:
            java.lang.String r2 = com.millennialmedia.internal.utils.IOUtils.TAG     // Catch:{ all -> 0x005e }
            java.lang.String r3 = "Error occurred when converting stream to string"
            com.millennialmedia.MMLog.e(r2, r3, r4)     // Catch:{ all -> 0x005e }
            if (r1 == 0) goto L_0x005c
            r1.close()     // Catch:{ IOException -> 0x0054 }
            goto L_0x005c
        L_0x0054:
            r4 = move-exception
            java.lang.String r1 = com.millennialmedia.internal.utils.IOUtils.TAG
            java.lang.String r2 = "Error closing input stream reader"
            com.millennialmedia.MMLog.e(r1, r2, r4)
        L_0x005c:
            r4 = r0
        L_0x005d:
            return r4
        L_0x005e:
            r4 = move-exception
        L_0x005f:
            if (r1 == 0) goto L_0x006d
            r1.close()     // Catch:{ IOException -> 0x0065 }
            goto L_0x006d
        L_0x0065:
            r0 = move-exception
            java.lang.String r1 = com.millennialmedia.internal.utils.IOUtils.TAG
            java.lang.String r2 = "Error closing input stream reader"
            com.millennialmedia.MMLog.e(r1, r2, r0)
        L_0x006d:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.utils.IOUtils.convertStreamToString(java.io.InputStream):java.lang.String");
    }

    public static Bitmap convertStreamToBitmap(InputStream inputStream) {
        Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
        if (decodeStream == null) {
            MMLog.e(TAG, "Unable to create bitmap from input stream");
        }
        return decodeStream;
    }

    public static class StringStreamer implements HttpUtils.ResponseStreamer {
        public void streamContent(InputStream inputStream, HttpUtils.Response response) {
            response.content = IOUtils.convertStreamToString(inputStream);
        }
    }

    public static class BitmapStreamer implements HttpUtils.ResponseStreamer {
        public void streamContent(InputStream inputStream, HttpUtils.Response response) {
            response.bitmap = IOUtils.convertStreamToBitmap(inputStream);
        }
    }

    public static class FileStreamer implements HttpUtils.ResponseStreamer {
        private File outputFile;

        public FileStreamer(File file) {
            this.outputFile = file;
        }

        public void streamContent(InputStream inputStream, HttpUtils.Response response) {
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(this.outputFile);
                try {
                    IOUtils.read(inputStream, fileOutputStream2);
                    response.file = this.outputFile;
                    IOUtils.closeStream(fileOutputStream2);
                } catch (IOException e) {
                    e = e;
                    fileOutputStream = fileOutputStream2;
                    try {
                        MMLog.e(IOUtils.TAG, "Unable to create file from input stream", e);
                        IOUtils.closeStream(fileOutputStream);
                    } catch (Throwable th) {
                        th = th;
                        IOUtils.closeStream(fileOutputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    IOUtils.closeStream(fileOutputStream);
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                MMLog.e(IOUtils.TAG, "Unable to create file from input stream", e);
                IOUtils.closeStream(fileOutputStream);
            }
        }
    }

    public static void downloadFile(final String str, final Integer num, final File file, final DownloadListener downloadListener) {
        if (str == null || file == null || downloadListener == null) {
            throw new IllegalArgumentException("url, file, and download listener are required");
        }
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                File file = file;
                try {
                    if (file.isDirectory()) {
                        file = File.createTempFile("_mm_", null, file);
                    }
                    HttpUtils.Response sendHttpRequest = HttpUtils.sendHttpRequest(str, null, null, num, new FileStreamer(file));
                    if (sendHttpRequest.file != null) {
                        downloadListener.onDownloadSucceeded(sendHttpRequest.file);
                    } else {
                        downloadListener.onDownloadFailed(new Throwable("Error creating file"));
                    }
                } catch (IOException e) {
                    String access$000 = IOUtils.TAG;
                    MMLog.e(access$000, "An error occurred downloading file from url = " + str, e);
                    downloadListener.onDownloadFailed(e);
                }
            }
        });
    }

    public static File getUniqueFileName(File file, String str) {
        file.mkdirs();
        File file2 = new File(file, str);
        if (!file2.exists()) {
            return file2;
        }
        String[] split = str.split("\\.(?=[^\\.]+$)");
        String str2 = split[0];
        String str3 = split.length > 1 ? split[1] : "";
        for (int i = 1; i < 1000; i++) {
            File file3 = new File(file, str2 + "(" + i + ")." + str3);
            if (!file3.exists()) {
                return file3;
            }
        }
        return null;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0021 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String readAssetContents(java.lang.String r5) {
        /*
            java.lang.String r0 = ""
            r1 = 0
            android.content.Context r2 = com.millennialmedia.internal.utils.EnvironmentUtils.getApplicationContext()     // Catch:{ IOException -> 0x0021 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ IOException -> 0x0021 }
            java.io.InputStream r2 = r2.open(r5)     // Catch:{ IOException -> 0x0021 }
            java.lang.String r1 = "UTF-8"
            java.lang.String r1 = read(r2, r1)     // Catch:{ IOException -> 0x001d, all -> 0x001a }
            closeStream(r2)
            r0 = r1
            goto L_0x003f
        L_0x001a:
            r5 = move-exception
            r1 = r2
            goto L_0x0040
        L_0x001d:
            r1 = r2
            goto L_0x0021
        L_0x001f:
            r5 = move-exception
            goto L_0x0040
        L_0x0021:
            java.lang.String r2 = com.millennialmedia.internal.utils.IOUtils.TAG     // Catch:{ all -> 0x001f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x001f }
            r3.<init>()     // Catch:{ all -> 0x001f }
            java.lang.String r4 = "Could not read contents of '"
            r3.append(r4)     // Catch:{ all -> 0x001f }
            r3.append(r5)     // Catch:{ all -> 0x001f }
            java.lang.String r5 = "' asset."
            r3.append(r5)     // Catch:{ all -> 0x001f }
            java.lang.String r5 = r3.toString()     // Catch:{ all -> 0x001f }
            com.millennialmedia.MMLog.e(r2, r5)     // Catch:{ all -> 0x001f }
            closeStream(r1)
        L_0x003f:
            return r0
        L_0x0040:
            closeStream(r1)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.utils.IOUtils.readAssetContents(java.lang.String):java.lang.String");
    }

    public static boolean closeStream(Closeable closeable) {
        if (closeable == null) {
            return false;
        }
        try {
            closeable.close();
            return true;
        } catch (IOException e) {
            MMLog.e(TAG, "Error closing stream", e);
            return false;
        }
    }
}
