package com.millennialmedia.internal;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class VolumeChangeManager {
    private static final Set<VolumeChangeListener> listeners = Collections.synchronizedSet(new HashSet());
    private static volatile boolean started = false;

    interface VolumeChangeListener {
        void onVolumeChange(int i, int i2, int i3);
    }

    public static void initialize(Application application) {
        application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            public void onActivityCreated(Activity activity, Bundle bundle) {
            }

            public void onActivityDestroyed(Activity activity) {
            }

            public void onActivityPaused(Activity activity) {
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            }

            public void onActivityStarted(Activity activity) {
            }

            public void onActivityStopped(Activity activity) {
            }

            public void onActivityResumed(Activity activity) {
                activity.startService(new Intent(activity, VolumeChangeService.class));
            }
        });
    }

    public static void start() {
        started = true;
        Application application = EnvironmentUtils.getApplication();
        application.startService(new Intent(application, VolumeChangeService.class));
    }

    public static boolean isStarted() {
        return started;
    }

    static void registerListener(VolumeChangeListener volumeChangeListener) {
        if (volumeChangeListener != null) {
            listeners.add(volumeChangeListener);
        }
    }

    static void removeListener(VolumeChangeListener volumeChangeListener) {
        if (volumeChangeListener != null) {
            listeners.remove(volumeChangeListener);
        }
    }

    public static Float getCurrentVolume(Context context) {
        if (context == null) {
            return null;
        }
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        return Float.valueOf((((float) audioManager.getStreamVolume(3)) / ((float) audioManager.getStreamMaxVolume(3))) * 100.0f);
    }

    static void updateVolume(int i, int i2, int i3) {
        for (VolumeChangeListener onVolumeChange : listeners) {
            onVolumeChange.onVolumeChange(i, i2, i3);
        }
    }
}
