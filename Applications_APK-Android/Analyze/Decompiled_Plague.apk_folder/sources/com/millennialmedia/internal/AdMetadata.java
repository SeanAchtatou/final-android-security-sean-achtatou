package com.millennialmedia.internal;

import java.util.HashMap;
import java.util.Map;

public class AdMetadata extends HashMap<String, String> {
    public static final String ENHANCED_AD_CONTROL_ENABLED = "enhancedAdControlEnabled";
    public static final String TRANSPARENT = "x-mm-transparent";

    public boolean isTransparent() {
        String str = (String) get(TRANSPARENT);
        return str != null && Boolean.parseBoolean(str);
    }

    public void addAll(AdMetadata adMetadata) {
        if (adMetadata != null) {
            for (Map.Entry entry : adMetadata.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    public boolean getBoolean(String str, boolean z) {
        Boolean valueOf;
        return (!containsKey(str) || (valueOf = Boolean.valueOf(Boolean.parseBoolean((String) get(str)))) == null) ? z : valueOf.booleanValue();
    }
}
