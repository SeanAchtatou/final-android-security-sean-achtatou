package com.millennialmedia.internal.adadapters;

import android.content.Context;
import android.widget.RelativeLayout;
import com.millennialmedia.InlineAd;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.InlineAdapter;
import com.millennialmedia.internal.adcontrollers.LightboxController;
import com.millennialmedia.internal.utils.ViewUtils;

public class InlineLightboxAdapter extends InlineAdapter {
    /* access modifiers changed from: private */
    public InlineAdapter.InlineAdapterListener inlineAdapterListener;
    private LightboxController lightboxController;
    private LightboxController.LightboxControllerListener lightboxControllerListener = new LightboxController.LightboxControllerListener() {
        public void initSucceeded() {
            InlineLightboxAdapter.this.inlineAdapterListener.initSucceeded();
        }

        public void initFailed() {
            InlineLightboxAdapter.this.inlineAdapterListener.initFailed();
        }

        public void attachSucceeded() {
            InlineLightboxAdapter.this.inlineAdapterListener.displaySucceeded();
        }

        public void attachFailed() {
            InlineLightboxAdapter.this.inlineAdapterListener.displayFailed();
        }

        public void onClicked() {
            InlineLightboxAdapter.this.inlineAdapterListener.onClicked();
        }

        public void onAdLeftApplication() {
            InlineLightboxAdapter.this.inlineAdapterListener.onAdLeftApplication();
        }

        public void onExpanded() {
            InlineLightboxAdapter.this.inlineAdapterListener.onExpanded();
        }

        public void onCollapsed() {
            InlineLightboxAdapter.this.inlineAdapterListener.onCollapsed();
        }
    };

    public void init(Context context, InlineAdapter.InlineAdapterListener inlineAdapterListener2, AdPlacement.DisplayOptions displayOptions) {
        this.inlineAdapterListener = inlineAdapterListener2;
        this.lightboxController = new LightboxController(this.lightboxControllerListener);
        this.lightboxController.init(context, this.adContent);
    }

    public long getImpressionDelay() {
        return (long) Handshake.getMinImpressionDuration();
    }

    public int getMinImpressionViewabilityPercentage() {
        return Handshake.getMinImpressionViewabilityPercent();
    }

    public void release() {
        if (this.lightboxController != null) {
            this.lightboxController.close();
            this.lightboxController.release();
            this.lightboxController = null;
        }
    }

    public void display(RelativeLayout relativeLayout, InlineAd.AdSize adSize) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewUtils.convertDipsToPixels(adSize.width), ViewUtils.convertDipsToPixels(adSize.height));
        layoutParams.addRule(13);
        if (this.lightboxController != null) {
            this.lightboxController.attach(relativeLayout, layoutParams);
        }
    }
}
