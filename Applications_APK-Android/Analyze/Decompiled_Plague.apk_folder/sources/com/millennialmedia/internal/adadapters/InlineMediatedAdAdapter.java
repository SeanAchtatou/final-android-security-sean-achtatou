package com.millennialmedia.internal.adadapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import com.millennialmedia.InlineAd;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.ActivityListenerManager;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.InlineAdapter;
import com.millennialmedia.internal.adadapters.MediatedAdAdapter;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.ViewUtils;
import com.millennialmedia.mediation.CustomEvent;
import com.millennialmedia.mediation.CustomEventBanner;
import com.millennialmedia.mediation.CustomEventRegistry;
import com.millennialmedia.mediation.ErrorCode;

@SuppressLint({"DefaultLocale"})
public class InlineMediatedAdAdapter extends InlineAdapter implements MediatedAdAdapter {
    private static final String TAG = "InlineMediatedAdAdapter";
    /* access modifiers changed from: private */
    public int activityHashCode = -1;
    /* access modifiers changed from: private */
    public InlineActivityListener activityListener;
    private Context context;
    /* access modifiers changed from: private */
    public volatile CustomEventBanner customEventBanner;
    private CustomEventBannerListenerImpl customEventBannerListener;
    private InlineAdapter.InlineAdapterListener inlineAdapterListener;
    private MediatedAdAdapter.MediationInfo mediationInfo;

    private final class InlineActivityListener extends ActivityListenerManager.ActivityListener {
        private InlineActivityListener() {
        }

        public void onResumed(Activity activity) {
            if (InlineMediatedAdAdapter.this.customEventBanner != null) {
                InlineMediatedAdAdapter.this.customEventBanner.onResume();
            }
        }

        public void onPaused(Activity activity) {
            if (InlineMediatedAdAdapter.this.customEventBanner != null) {
                InlineMediatedAdAdapter.this.customEventBanner.onPause();
            }
        }
    }

    private static class CustomEventBannerListenerImpl implements CustomEventBanner.CustomEventBannerListener {
        private RelativeLayout containerLayout;
        private InlineAdapter.InlineAdapterListener inlineAdapterListener;

        CustomEventBannerListenerImpl(InlineAdapter.InlineAdapterListener inlineAdapterListener2, RelativeLayout relativeLayout) {
            this.inlineAdapterListener = inlineAdapterListener2;
            this.containerLayout = relativeLayout;
        }

        public void onRequestSucceeded(View view) {
            this.containerLayout.addView(view, new RelativeLayout.LayoutParams(-2, -2));
            this.inlineAdapterListener.displaySucceeded();
        }

        public void onRequestFailed(ErrorCode errorCode) {
            this.inlineAdapterListener.displayFailed();
        }

        public void onClicked() {
            this.inlineAdapterListener.onClicked();
        }

        public void onExpanded() {
            this.inlineAdapterListener.onExpanded();
        }

        public void onCollapsed() {
            this.inlineAdapterListener.onCollapsed();
        }

        public void onAdLeftApplication() {
            this.inlineAdapterListener.onAdLeftApplication();
        }
    }

    public void init(Context context2, InlineAdapter.InlineAdapterListener inlineAdapterListener2, AdPlacement.DisplayOptions displayOptions) {
        if (context2 == null) {
            inlineAdapterListener2.initFailed();
        } else if (this.mediationInfo == null) {
            inlineAdapterListener2.initFailed();
        } else if (getCustomEventBanner() == null) {
            inlineAdapterListener2.initFailed();
        } else {
            this.context = context2;
            this.inlineAdapterListener = inlineAdapterListener2;
            this.activityListener = new InlineActivityListener();
            inlineAdapterListener2.initSucceeded();
        }
    }

    public void display(RelativeLayout relativeLayout, InlineAd.AdSize adSize) {
        try {
            this.activityHashCode = ViewUtils.getActivityHashForView(relativeLayout);
            if (this.activityHashCode != -1) {
                ActivityListenerManager.registerListener(this.activityHashCode, this.activityListener);
            }
            relativeLayout.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                public void onViewAttachedToWindow(View view) {
                    if (InlineMediatedAdAdapter.this.activityHashCode == -1) {
                        int unused = InlineMediatedAdAdapter.this.activityHashCode = ViewUtils.getActivityHashForView(view);
                        ActivityListenerManager.registerListener(InlineMediatedAdAdapter.this.activityHashCode, InlineMediatedAdAdapter.this.activityListener);
                    }
                }

                public void onViewDetachedFromWindow(View view) {
                    ActivityListenerManager.unregisterListener(InlineMediatedAdAdapter.this.activityHashCode, InlineMediatedAdAdapter.this.activityListener);
                }
            });
            this.customEventBannerListener = new CustomEventBannerListenerImpl(this.inlineAdapterListener, relativeLayout);
            Bundle bundle = new Bundle();
            bundle.putString(CustomEvent.PLACEMENT_ID_KEY, this.mediationInfo.spaceId);
            bundle.putString(CustomEvent.SITE_ID_KEY, this.mediationInfo.siteId);
            this.customEventBanner.requestBanner(this.context, this.customEventBannerListener, adSize, bundle);
        } catch (Throwable th) {
            MMLog.e(TAG, String.format("Error requesting banner for mediation Id: %s", this.mediationInfo.networkId), th);
            if (this.inlineAdapterListener != null) {
                this.inlineAdapterListener.displayFailed();
            } else {
                MMLog.w(TAG, "display called but inlineAdapterListener was null. Possibly display called without first calling init?");
            }
        }
    }

    public void setMediationInfo(MediatedAdAdapter.MediationInfo mediationInfo2) {
        this.mediationInfo = mediationInfo2;
    }

    public void release() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (InlineMediatedAdAdapter.this.customEventBanner != null) {
                    InlineMediatedAdAdapter.this.customEventBanner.destroy();
                    CustomEventBanner unused = InlineMediatedAdAdapter.this.customEventBanner = null;
                }
            }
        });
    }

    private synchronized CustomEventBanner getCustomEventBanner() {
        if (this.customEventBanner == null) {
            this.customEventBanner = (CustomEventBanner) CustomEventRegistry.getCustomEvent(InlineAd.class, this.mediationInfo.networkId, CustomEventBanner.class);
        }
        return this.customEventBanner;
    }

    public long getImpressionDelay() {
        return (long) Handshake.getMinImpressionDuration();
    }

    public int getMinImpressionViewabilityPercentage() {
        return Handshake.getMinImpressionViewabilityPercent();
    }
}
