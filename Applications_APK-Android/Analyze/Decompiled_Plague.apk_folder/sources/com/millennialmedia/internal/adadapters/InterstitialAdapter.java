package com.millennialmedia.internal.adadapters;

import android.content.Context;
import com.millennialmedia.InterstitialAd;
import com.millennialmedia.XIncentivizedEventListener;
import com.millennialmedia.internal.AdPlacement;

public abstract class InterstitialAdapter extends AdAdapter {
    protected InterstitialAdapterListener adapterListener;

    public interface InterstitialAdapterListener {
        void initFailed();

        void initSucceeded();

        void onAdLeftApplication();

        void onClicked();

        void onClosed();

        void onExpired();

        void onIncentiveEarned(XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent);

        void onUnload();

        void showFailed(InterstitialAd.InterstitialErrorStatus interstitialErrorStatus);

        void shown();
    }

    public abstract void init(Context context, InterstitialAdapterListener interstitialAdapterListener);

    public abstract void show(Context context, AdPlacement.DisplayOptions displayOptions);
}
