package com.millennialmedia.internal.adadapters;

import android.content.Context;
import com.millennialmedia.InterstitialAd;
import com.millennialmedia.MMLog;
import com.millennialmedia.XIncentivizedEventListener;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.MMActivity;
import com.millennialmedia.internal.adadapters.InterstitialAdapter;
import com.millennialmedia.internal.adcontrollers.VASTVideoController;

public class InterstitialVASTVideoAdapter extends InterstitialAdapter {
    private static final String TAG = "InterstitialVASTVideoAdapter";
    /* access modifiers changed from: private */
    public volatile boolean attached;
    /* access modifiers changed from: private */
    public MMActivity mmVastActivity;
    /* access modifiers changed from: private */
    public VASTVideoController vastVideoController;
    VASTVideoController.VASTVideoControllerListener vastVideoControllerListener = new VASTVideoController.VASTVideoControllerListener() {
        public void initSucceeded() {
            InterstitialVASTVideoAdapter.this.adapterListener.initSucceeded();
        }

        public void initFailed() {
            InterstitialVASTVideoAdapter.this.adapterListener.initFailed();
        }

        public void attachSucceeded() {
            if (!InterstitialVASTVideoAdapter.this.attached) {
                boolean unused = InterstitialVASTVideoAdapter.this.attached = true;
                InterstitialVASTVideoAdapter.this.adapterListener.shown();
            }
        }

        public void attachFailed() {
            if (!InterstitialVASTVideoAdapter.this.attached) {
                InterstitialVASTVideoAdapter.this.adapterListener.showFailed(new InterstitialAd.InterstitialErrorStatus(7));
            }
        }

        public void onClick() {
            InterstitialVASTVideoAdapter.this.adapterListener.onClicked();
        }

        public void onIncentiveEarned(XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent) {
            InterstitialVASTVideoAdapter.this.adapterListener.onIncentiveEarned(xIncentiveEvent);
        }

        public void onAdLeftApplication() {
            InterstitialVASTVideoAdapter.this.adapterListener.onAdLeftApplication();
        }

        public void close() {
            if (InterstitialVASTVideoAdapter.this.mmVastActivity != null) {
                InterstitialVASTVideoAdapter.this.mmVastActivity.finish();
            }
        }

        public void onUnload() {
            InterstitialVASTVideoAdapter.this.adapterListener.onUnload();
        }
    };

    public void init(Context context, InterstitialAdapter.InterstitialAdapterListener interstitialAdapterListener) {
        this.adapterListener = interstitialAdapterListener;
        this.vastVideoController = new VASTVideoController(this.vastVideoControllerListener);
        this.vastVideoController.init(context, this.adContent);
    }

    public void show(Context context, AdPlacement.DisplayOptions displayOptions) {
        if (displayOptions == null) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Display options not specified, using defaults.");
            }
            displayOptions = new AdPlacement.DisplayOptions().setImmersive(true);
        }
        MMActivity.launch(context, new MMActivity.MMActivityConfig().setImmersive(displayOptions.isImmersive()), new MMActivity.MMActivityListener() {
            public void onCreate(MMActivity mMActivity) {
                MMActivity unused = InterstitialVASTVideoAdapter.this.mmVastActivity = mMActivity;
                if (InterstitialVASTVideoAdapter.this.vastVideoController != null) {
                    InterstitialVASTVideoAdapter.this.vastVideoController.attach(mMActivity);
                }
            }

            public void onDestroy(MMActivity mMActivity) {
                if (mMActivity.isFinishing()) {
                    InterstitialVASTVideoAdapter.this.adapterListener.onClosed();
                    MMActivity unused = InterstitialVASTVideoAdapter.this.mmVastActivity = null;
                }
            }

            public boolean onBackPressed() {
                if (InterstitialVASTVideoAdapter.this.vastVideoController == null) {
                    return true;
                }
                return InterstitialVASTVideoAdapter.this.vastVideoController.onBackPressed();
            }
        });
    }

    public void release() {
        if (this.vastVideoController != null) {
            this.vastVideoController.close();
            this.vastVideoController.release();
            this.vastVideoController = null;
        }
    }
}
