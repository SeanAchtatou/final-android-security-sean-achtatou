package com.millennialmedia.internal.adadapters;

import com.millennialmedia.MMLog;
import com.millennialmedia.internal.adadapters.NativeAdapter;
import com.millennialmedia.internal.adcontrollers.NativeController;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class NativeNativeAdapter extends NativeAdapter {
    private static final String ASSET_COMPONENT_TYPE_ID_BODY = "101";
    private static final String ASSET_COMPONENT_TYPE_ID_CALL_TO_ACTION = "104";
    private static final String ASSET_COMPONENT_TYPE_ID_DISCLAIMER = "106";
    private static final String ASSET_COMPONENT_TYPE_ID_ICON_IMAGE = "102";
    private static final String ASSET_COMPONENT_TYPE_ID_MAIN_IMAGE = "103";
    private static final String ASSET_COMPONENT_TYPE_ID_RATING = "105";
    private static final String ASSET_COMPONENT_TYPE_ID_TITLE = "100";
    private static final String TAG = "com.millennialmedia.internal.adadapters.NativeNativeAdapter";
    private List<NativeAdapter.TextComponentInfo> bodies = new CopyOnWriteArrayList();
    private List<NativeAdapter.TextComponentInfo> callToActions = new CopyOnWriteArrayList();
    private List<NativeAdapter.TextComponentInfo> disclaimers = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */
    public List<NativeAdapter.ImageComponentInfo> iconImages = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */
    public List<NativeAdapter.ImageComponentInfo> mainImages = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */
    public NativeAdapter.NativeAdapterListener nativeAdapterListener;
    private volatile NativeController nativeController;
    NativeController.NativeControllerListener nativeControllerListener = new NativeController.NativeControllerListener() {
        public void initSucceeded() {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    if (NativeNativeAdapter.this.loadComponentsInfo()) {
                        NativeNativeAdapter.this.nativeAdapterListener.initSucceeded();
                    } else {
                        NativeNativeAdapter.this.nativeAdapterListener.initFailed(new RuntimeException("Component info not loaded"));
                    }
                }
            });
        }

        public void initFailed(Throwable th) {
            NativeNativeAdapter.this.nativeAdapterListener.initFailed(th);
        }
    };
    private String nativeType;
    private List<NativeAdapter.TextComponentInfo> ratings = new CopyOnWriteArrayList();
    private List<NativeAdapter.TextComponentInfo> titles = new CopyOnWriteArrayList();

    interface ImageLoadedCallback {
        void imageLoaded(NativeAdapter.ImageComponentInfo imageComponentInfo);
    }

    public void init(NativeAdapter.NativeAdapterListener nativeAdapterListener2) {
        this.nativeAdapterListener = nativeAdapterListener2;
        this.nativeController = new NativeController(this.nativeControllerListener);
        this.nativeController.init(this.adContent);
    }

    public void release() {
        if (this.nativeController != null) {
            this.nativeController.close();
            this.nativeController.release();
            this.nativeController = null;
        }
        this.titles.clear();
        this.bodies.clear();
        this.iconImages.clear();
        this.mainImages.clear();
        this.callToActions.clear();
        this.ratings.clear();
        this.disclaimers.clear();
    }

    /* access modifiers changed from: private */
    public boolean loadComponentsInfo() {
        if (this.nativeController == null) {
            return false;
        }
        final CountDownLatch countDownLatch = new CountDownLatch(this.nativeController.assets.size());
        for (int i = 0; i < this.nativeController.assets.size(); i++) {
            final NativeController.Asset asset = this.nativeController.assets.get(i);
            if (asset == null) {
                MMLog.w(TAG, "Unable to load component, asset is null");
                countDownLatch.countDown();
            } else {
                String num = Integer.toString(asset.id);
                if (num.length() != 9) {
                    MMLog.e(TAG, "error when processing native asset, asset ID is not the correct length");
                    countDownLatch.countDown();
                } else {
                    this.nativeType = num.substring(0, 3);
                    String substring = num.substring(3, 6);
                    if (substring.equals("100")) {
                        if (asset.type == NativeController.Asset.Type.TITLE) {
                            NativeAdapter.TextComponentInfo textComponentInfo = new NativeAdapter.TextComponentInfo();
                            textComponentInfo.value = asset.title.value;
                            setLink(textComponentInfo, asset);
                            this.titles.add(textComponentInfo);
                        } else {
                            MMLog.w(TAG, "Unable to load title component, asset not the expected type");
                        }
                        countDownLatch.countDown();
                    } else if (substring.equals(ASSET_COMPONENT_TYPE_ID_BODY)) {
                        if (asset.type == NativeController.Asset.Type.DATA) {
                            NativeAdapter.TextComponentInfo textComponentInfo2 = new NativeAdapter.TextComponentInfo();
                            textComponentInfo2.value = asset.data.value;
                            setLink(textComponentInfo2, asset);
                            this.bodies.add(textComponentInfo2);
                        } else {
                            MMLog.w(TAG, "Unable to load body component, asset not the expected type");
                        }
                        countDownLatch.countDown();
                    } else if (substring.equals(ASSET_COMPONENT_TYPE_ID_ICON_IMAGE)) {
                        if (asset.type == NativeController.Asset.Type.IMAGE) {
                            loadImageComponent(asset.image.url, new ImageLoadedCallback() {
                                public void imageLoaded(NativeAdapter.ImageComponentInfo imageComponentInfo) {
                                    if (imageComponentInfo != null) {
                                        NativeNativeAdapter.this.setLink(imageComponentInfo, asset);
                                        NativeNativeAdapter.this.iconImages.add(imageComponentInfo);
                                    }
                                    countDownLatch.countDown();
                                }
                            });
                        } else {
                            MMLog.w(TAG, "Unable to load icon image component, asset not the expected type");
                            countDownLatch.countDown();
                        }
                    } else if (substring.equals(ASSET_COMPONENT_TYPE_ID_MAIN_IMAGE)) {
                        if (asset.type == NativeController.Asset.Type.IMAGE) {
                            loadImageComponent(asset.image.url, new ImageLoadedCallback() {
                                public void imageLoaded(NativeAdapter.ImageComponentInfo imageComponentInfo) {
                                    if (imageComponentInfo != null) {
                                        NativeNativeAdapter.this.setLink(imageComponentInfo, asset);
                                        NativeNativeAdapter.this.mainImages.add(imageComponentInfo);
                                    }
                                    countDownLatch.countDown();
                                }
                            });
                        } else {
                            MMLog.w(TAG, "Unable to load main image component, asset not the expected type");
                            countDownLatch.countDown();
                        }
                    } else if (substring.equals(ASSET_COMPONENT_TYPE_ID_CALL_TO_ACTION)) {
                        if (asset.type == NativeController.Asset.Type.DATA) {
                            NativeAdapter.TextComponentInfo textComponentInfo3 = new NativeAdapter.TextComponentInfo();
                            textComponentInfo3.value = asset.data.value;
                            setLink(textComponentInfo3, asset);
                            this.callToActions.add(textComponentInfo3);
                        } else {
                            MMLog.w(TAG, "Unable to load call to action text component, asset not the expected type");
                        }
                        countDownLatch.countDown();
                    } else if (substring.equals(ASSET_COMPONENT_TYPE_ID_RATING)) {
                        if (asset.type == NativeController.Asset.Type.DATA) {
                            NativeAdapter.TextComponentInfo textComponentInfo4 = new NativeAdapter.TextComponentInfo();
                            textComponentInfo4.value = asset.data.value;
                            setLink(textComponentInfo4, asset);
                            this.ratings.add(textComponentInfo4);
                        } else {
                            MMLog.w(TAG, "Unable to load rating component, asset not the expected type");
                        }
                        countDownLatch.countDown();
                    } else if (substring.equals(ASSET_COMPONENT_TYPE_ID_DISCLAIMER)) {
                        if (asset.type == NativeController.Asset.Type.DATA) {
                            NativeAdapter.TextComponentInfo textComponentInfo5 = new NativeAdapter.TextComponentInfo();
                            textComponentInfo5.value = asset.data.value;
                            setLink(textComponentInfo5, asset);
                            this.disclaimers.add(textComponentInfo5);
                        } else {
                            MMLog.w(TAG, "Unable to load disclaimer component, asset not the expected type");
                        }
                        countDownLatch.countDown();
                    } else {
                        MMLog.e(TAG, "Unable to load component from asset, asset type is unrecognized <" + substring + ">");
                        countDownLatch.countDown();
                    }
                }
            }
        }
        try {
            return countDownLatch.await(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException unused) {
            MMLog.e(TAG, "Error occurred when loading native component info");
            return false;
        }
    }

    private void loadImageComponent(String str, ImageLoadedCallback imageLoadedCallback) {
        HttpUtils.Response bitmapFromGetRequest = HttpUtils.getBitmapFromGetRequest(str);
        if (bitmapFromGetRequest.code != 200 || bitmapFromGetRequest.bitmap == null) {
            imageLoadedCallback.imageLoaded(null);
            return;
        }
        NativeAdapter.ImageComponentInfo imageComponentInfo = new NativeAdapter.ImageComponentInfo();
        imageComponentInfo.bitmapUrl = str;
        imageComponentInfo.bitmap = bitmapFromGetRequest.bitmap;
        imageComponentInfo.width = bitmapFromGetRequest.bitmap.getWidth();
        imageComponentInfo.height = bitmapFromGetRequest.bitmap.getHeight();
        imageLoadedCallback.imageLoaded(imageComponentInfo);
    }

    /* access modifiers changed from: private */
    public void setLink(NativeAdapter.ComponentInfo componentInfo, NativeController.Asset asset) {
        if (asset.link != null) {
            componentInfo.clickUrl = asset.link.url;
            componentInfo.clickTrackerUrls = asset.link.clickTrackerUrls;
        } else if (this.nativeController.link != null) {
            componentInfo.clickUrl = this.nativeController.link.url;
            componentInfo.clickTrackerUrls = this.nativeController.link.clickTrackerUrls;
        }
    }

    public String getType() {
        return this.nativeType;
    }

    public List<NativeAdapter.TextComponentInfo> getTitleList() {
        return this.titles;
    }

    public List<NativeAdapter.TextComponentInfo> getBodyList() {
        return this.bodies;
    }

    public List<NativeAdapter.ImageComponentInfo> getIconImageList() {
        return this.iconImages;
    }

    public List<NativeAdapter.ImageComponentInfo> getMainImageList() {
        return this.mainImages;
    }

    public List<NativeAdapter.TextComponentInfo> getCallToActionList() {
        return this.callToActions;
    }

    public List<NativeAdapter.TextComponentInfo> getRatingList() {
        return this.ratings;
    }

    public List<NativeAdapter.TextComponentInfo> getDisclaimerList() {
        return this.disclaimers;
    }

    public String getDefaultAction() {
        if (this.nativeController.link != null) {
            return this.nativeController.link.url;
        }
        return null;
    }

    public List<String> getImpressionTrackers() {
        if (this.nativeController.impTrackers != null) {
            return this.nativeController.impTrackers;
        }
        return Collections.emptyList();
    }

    public List<String> getClickTrackers() {
        if (this.nativeController.link != null) {
            return this.nativeController.link.clickTrackerUrls;
        }
        return Collections.emptyList();
    }
}
