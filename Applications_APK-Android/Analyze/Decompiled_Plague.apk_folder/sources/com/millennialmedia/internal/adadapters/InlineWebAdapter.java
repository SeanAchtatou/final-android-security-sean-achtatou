package com.millennialmedia.internal.adadapters;

import android.content.Context;
import android.widget.RelativeLayout;
import com.millennialmedia.InlineAd;
import com.millennialmedia.internal.AdMetadata;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.InlineAdapter;
import com.millennialmedia.internal.adcontrollers.WebController;
import com.millennialmedia.internal.utils.ViewUtils;

public class InlineWebAdapter extends InlineAdapter {
    InlineAdapter.InlineAdapterListener adapterListener;
    WebController.WebControllerListener controllerListener = new WebController.WebControllerListener() {
        public void initSucceeded() {
            InlineWebAdapter.this.adapterListener.initSucceeded();
        }

        public void initFailed() {
            InlineWebAdapter.this.adapterListener.initFailed();
        }

        public void attachSucceeded() {
            InlineWebAdapter.this.adapterListener.displaySucceeded();
        }

        public void attachFailed() {
            InlineWebAdapter.this.adapterListener.displayFailed();
        }

        public void onClicked() {
            InlineWebAdapter.this.adapterListener.onClicked();
        }

        public void onAdLeftApplication() {
            InlineWebAdapter.this.adapterListener.onAdLeftApplication();
        }

        public void onResize(int i, int i2) {
            InlineWebAdapter.this.adapterListener.onResize(i, i2);
        }

        public void onResized(int i, int i2, boolean z) {
            InlineWebAdapter.this.adapterListener.onResized(i, i2, z);
        }

        public void onExpanded() {
            InlineWebAdapter.this.adapterListener.onExpanded();
        }

        public void onCollapsed() {
            InlineWebAdapter.this.adapterListener.onCollapsed();
        }

        public void onUnload() {
            InlineWebAdapter.this.adapterListener.onUnload();
        }
    };
    WebController webController;

    public void init(Context context, InlineAdapter.InlineAdapterListener inlineAdapterListener, AdPlacement.DisplayOptions displayOptions) {
        this.adapterListener = inlineAdapterListener;
        WebController.WebControllerOptions webControllerOptions = new WebController.WebControllerOptions(false, Handshake.isMoatEnabled(), isEnhancedAdControlEnabled(), false, displayOptions.isImmersive());
        this.webController = new WebController(this.controllerListener);
        this.webController.init(context, this.adContent, this.adMetadata, webControllerOptions);
    }

    public void display(RelativeLayout relativeLayout, InlineAd.AdSize adSize) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewUtils.convertDipsToPixels(adSize.width), ViewUtils.convertDipsToPixels(adSize.height));
        layoutParams.addRule(13);
        if (this.webController != null) {
            this.webController.attach(relativeLayout, layoutParams);
        }
    }

    public long getImpressionDelay() {
        if (isEnhancedAdControlEnabled()) {
            return 0;
        }
        return (long) Handshake.getMinImpressionDuration();
    }

    public int getMinImpressionViewabilityPercentage() {
        if (isEnhancedAdControlEnabled()) {
            return -1;
        }
        return Handshake.getMinImpressionViewabilityPercent();
    }

    public void release() {
        if (this.webController != null) {
            this.webController.close();
            this.webController.release();
            this.webController = null;
        }
    }

    private boolean isEnhancedAdControlEnabled() {
        return this.adMetadata.getBoolean(AdMetadata.ENHANCED_AD_CONTROL_ENABLED, false);
    }
}
