package com.millennialmedia.internal.adadapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.millennialmedia.InterstitialAd;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.adadapters.InterstitialAdapter;
import com.millennialmedia.internal.adadapters.MediatedAdAdapter;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.mediation.CustomEvent;
import com.millennialmedia.mediation.CustomEventInterstitial;
import com.millennialmedia.mediation.CustomEventRegistry;
import com.millennialmedia.mediation.ErrorCode;

@SuppressLint({"DefaultLocale"})
public class InterstitialMediatedAdAdapter extends InterstitialAdapter implements MediatedAdAdapter {
    private static final String TAG = "InterstitialMediatedAdAdapter";
    /* access modifiers changed from: private */
    public InterstitialAdapter.InterstitialAdapterListener adapterListener;
    /* access modifiers changed from: private */
    public volatile CustomEventInterstitial customEventInterstitial;
    /* access modifiers changed from: private */
    public CustomEventInterstitialListenerImpl customEventInterstitialListener;
    private MediatedAdAdapter.MediationInfo mediationInfo;

    public long getImpressionDelay() {
        return 0;
    }

    public int getMinImpressionViewabilityPercentage() {
        return -1;
    }

    private class CustomEventInterstitialListenerImpl implements CustomEventInterstitial.CustomEventInterstitialListener {
        private CustomEventInterstitialListenerImpl() {
        }

        public void onLoaded() {
            InterstitialMediatedAdAdapter.this.adapterListener.initSucceeded();
        }

        public void onLoadFailed(ErrorCode errorCode) {
            InterstitialMediatedAdAdapter.this.adapterListener.initFailed();
        }

        public void onShown() {
            InterstitialMediatedAdAdapter.this.adapterListener.shown();
        }

        public void onShowFailed(ErrorCode errorCode) {
            InterstitialMediatedAdAdapter.this.adapterListener.showFailed(null);
        }

        public void onClicked() {
            InterstitialMediatedAdAdapter.this.adapterListener.onClicked();
        }

        public void onClosed() {
            InterstitialMediatedAdAdapter.this.adapterListener.onClosed();
        }

        public void onAdLeftApplication() {
            InterstitialMediatedAdAdapter.this.adapterListener.onAdLeftApplication();
        }

        public void onExpired() {
            InterstitialMediatedAdAdapter.this.adapterListener.onExpired();
        }
    }

    public void setMediationInfo(MediatedAdAdapter.MediationInfo mediationInfo2) {
        this.mediationInfo = mediationInfo2;
    }

    public void init(final Context context, InterstitialAdapter.InterstitialAdapterListener interstitialAdapterListener) {
        this.adapterListener = interstitialAdapterListener;
        if (this.mediationInfo == null) {
            interstitialAdapterListener.initFailed();
            return;
        }
        this.customEventInterstitial = getCustomEventInterstitial();
        if (this.customEventInterstitial == null) {
            interstitialAdapterListener.initFailed();
        } else {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    CustomEventInterstitialListenerImpl unused = InterstitialMediatedAdAdapter.this.customEventInterstitialListener = new CustomEventInterstitialListenerImpl();
                    InterstitialMediatedAdAdapter.this.customEventInterstitial.loadInterstitial(context, InterstitialMediatedAdAdapter.this.customEventInterstitialListener, InterstitialMediatedAdAdapter.this.buildMediationExtras());
                }
            });
        }
    }

    public void show(Context context, AdPlacement.DisplayOptions displayOptions) {
        this.customEventInterstitial.showInterstitial(context, buildMediationExtras());
    }

    /* access modifiers changed from: private */
    @NonNull
    public Bundle buildMediationExtras() {
        Bundle bundle = new Bundle();
        bundle.putString(CustomEvent.PLACEMENT_ID_KEY, this.mediationInfo.spaceId);
        bundle.putString(CustomEvent.SITE_ID_KEY, this.mediationInfo.siteId);
        return bundle;
    }

    public void release() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (InterstitialMediatedAdAdapter.this.customEventInterstitial != null) {
                    InterstitialMediatedAdAdapter.this.customEventInterstitial.destroy();
                    CustomEventInterstitial unused = InterstitialMediatedAdAdapter.this.customEventInterstitial = null;
                }
            }
        });
    }

    private synchronized CustomEventInterstitial getCustomEventInterstitial() {
        if (this.customEventInterstitial == null) {
            this.customEventInterstitial = (CustomEventInterstitial) CustomEventRegistry.getCustomEvent(InterstitialAd.class, this.mediationInfo.networkId, CustomEventInterstitial.class);
        }
        return this.customEventInterstitial;
    }
}
