package com.millennialmedia.internal.adadapters;

import android.annotation.SuppressLint;
import android.os.Bundle;
import com.millennialmedia.MMException;
import com.millennialmedia.MMLog;
import com.millennialmedia.NativeAd;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.MediatedAdAdapter;
import com.millennialmedia.internal.adadapters.NativeAdapter;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.Utils;
import com.millennialmedia.mediation.CustomEvent;
import com.millennialmedia.mediation.CustomEventNative;
import com.millennialmedia.mediation.CustomEventNativeAd;
import com.millennialmedia.mediation.CustomEventRegistry;
import com.millennialmedia.mediation.ErrorCode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@SuppressLint({"DefaultLocale"})
public class NativeMediatedAdAdapter extends NativeAdapter implements MediatedAdAdapter {
    /* access modifiers changed from: private */
    public static final String TAG = "NativeMediatedAdAdapter";
    private List<NativeAdapter.TextComponentInfo> body = new ArrayList(1);
    private List<NativeAdapter.TextComponentInfo> callToAction = new ArrayList(1);
    /* access modifiers changed from: private */
    public CustomEventNative customEventNative;
    private List<NativeAdapter.TextComponentInfo> disclaimer = new ArrayList(1);
    private List<NativeAdapter.ImageComponentInfo> iconImage = new ArrayList(1);
    private volatile int imagesDownloaded = 0;
    private List<NativeAdapter.ImageComponentInfo> mainImage = new ArrayList(1);
    private MediatedAdAdapter.MediationInfo mediationInfo;
    /* access modifiers changed from: private */
    public NativeAdapter.NativeAdapterListener nativeAdapterListener;
    private List<NativeAdapter.TextComponentInfo> rating = new ArrayList(1);
    private List<NativeAdapter.TextComponentInfo> title = new ArrayList(1);

    public List<String> getClickTrackers() {
        return null;
    }

    public String getDefaultAction() {
        return null;
    }

    public List<String> getImpressionTrackers() {
        return null;
    }

    static /* synthetic */ int access$508(NativeMediatedAdAdapter nativeMediatedAdAdapter) {
        int i = nativeMediatedAdAdapter.imagesDownloaded;
        nativeMediatedAdAdapter.imagesDownloaded = i + 1;
        return i;
    }

    private class CustomEventNativeListenerImpl implements CustomEventNative.CustomEventNativeListener {
        private CustomEventNativeListenerImpl() {
        }

        public void onLoaded(CustomEventNativeAd customEventNativeAd) {
            try {
                NativeMediatedAdAdapter.this.loadContent(customEventNativeAd);
                NativeMediatedAdAdapter.this.nativeAdapterListener.initSucceeded();
            } catch (Exception e) {
                MMLog.e(NativeMediatedAdAdapter.TAG, "An exception was thrown while loading custom event native ad.", e);
                NativeMediatedAdAdapter.this.nativeAdapterListener.initFailed(e);
            }
        }

        public void onLoadFailed(ErrorCode errorCode) {
            String access$200 = NativeMediatedAdAdapter.TAG;
            MMLog.d(access$200, "onLoadFailed called with error code = " + errorCode);
            NativeMediatedAdAdapter.this.nativeAdapterListener.initFailed(null);
        }
    }

    public void setMediationInfo(MediatedAdAdapter.MediationInfo mediationInfo2) {
        this.mediationInfo = mediationInfo2;
    }

    public void init(NativeAdapter.NativeAdapterListener nativeAdapterListener2) {
        if (this.mediationInfo == null) {
            MMLog.d(TAG, "calling initFailed, mediationInfo was null.");
            nativeAdapterListener2.initFailed(null);
            return;
        }
        this.customEventNative = (CustomEventNative) CustomEventRegistry.getCustomEvent(NativeAd.class, this.mediationInfo.networkId, CustomEventNative.class);
        if (this.customEventNative == null) {
            MMLog.d(TAG, "calling initFailed, customEventNative was null.");
            nativeAdapterListener2.initFailed(null);
            return;
        }
        this.nativeAdapterListener = nativeAdapterListener2;
        final Bundle bundle = new Bundle();
        bundle.putString(CustomEvent.PLACEMENT_ID_KEY, this.mediationInfo.spaceId);
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                NativeMediatedAdAdapter.this.customEventNative.loadNativeAd(EnvironmentUtils.getApplicationContext(), new CustomEventNativeListenerImpl(), bundle);
            }
        });
    }

    public String getType() {
        String type = this.customEventNative != null ? this.customEventNative.getType() : null;
        return type == null ? CustomEventNative.DEFAULT_TYPE : type;
    }

    public List<NativeAdapter.TextComponentInfo> getTitleList() {
        return this.title;
    }

    public List<NativeAdapter.TextComponentInfo> getBodyList() {
        return this.body;
    }

    public List<NativeAdapter.ImageComponentInfo> getIconImageList() {
        return this.iconImage;
    }

    public List<NativeAdapter.ImageComponentInfo> getMainImageList() {
        return this.mainImage;
    }

    public List<NativeAdapter.TextComponentInfo> getCallToActionList() {
        return this.callToAction;
    }

    public List<NativeAdapter.TextComponentInfo> getRatingList() {
        return this.rating;
    }

    public List<NativeAdapter.TextComponentInfo> getDisclaimerList() {
        return this.disclaimer;
    }

    public void onAdLoaded(NativeAd nativeAd) {
        this.customEventNative.onAdLoaded(nativeAd);
    }

    public void onAdClicked(NativeAd nativeAd) {
        this.customEventNative.onAdClicked(nativeAd);
    }

    /* access modifiers changed from: private */
    public void loadContent(CustomEventNativeAd customEventNativeAd) throws InterruptedException, MMException {
        addTextComponentInfo(this.title, customEventNativeAd.getTitle());
        addTextComponentInfo(this.body, customEventNativeAd.getBody());
        addTextComponentInfo(this.disclaimer, customEventNativeAd.getDisclaimer());
        addTextComponentInfo(this.callToAction, customEventNativeAd.getCallToAction());
        addTextComponentInfo(this.rating, customEventNativeAd.getRating());
        addImageComponentInfo(this.mainImage, customEventNativeAd.getMainImageUrl());
        addImageComponentInfo(this.iconImage, customEventNativeAd.getIconUrl());
        downloadImages();
    }

    private void addTextComponentInfo(List<NativeAdapter.TextComponentInfo> list, CustomEventNativeAd.TextComponent textComponent) {
        if (textComponent != null) {
            NativeAdapter.TextComponentInfo textComponentInfo = new NativeAdapter.TextComponentInfo();
            textComponentInfo.value = textComponent.value;
            textComponentInfo.clickTrackerUrls = textComponent.clickTrackingUrls;
            textComponentInfo.clickUrl = textComponent.clickThroughUrl;
            list.add(textComponentInfo);
        }
    }

    private void addImageComponentInfo(List<NativeAdapter.ImageComponentInfo> list, CustomEventNativeAd.ImageComponent imageComponent) {
        if (imageComponent != null) {
            NativeAdapter.ImageComponentInfo imageComponentInfo = new NativeAdapter.ImageComponentInfo();
            imageComponentInfo.bitmapUrl = imageComponent.imageUrl;
            imageComponentInfo.clickTrackerUrls = imageComponent.clickTrackingUrls;
            imageComponentInfo.clickUrl = imageComponent.clickThroughUrl;
            list.add(imageComponentInfo);
        }
    }

    private void downloadImages() throws InterruptedException, MMException {
        int i = this.mainImage.size() == 1 ? 1 : 0;
        if (this.iconImage.size() == 1) {
            i++;
        }
        CountDownLatch countDownLatch = new CountDownLatch(i);
        downloadImage(countDownLatch, this.mainImage.get(0));
        downloadImage(countDownLatch, this.iconImage.get(0));
        countDownLatch.await((long) Handshake.getNativeTimeout(), TimeUnit.MILLISECONDS);
        if (this.imagesDownloaded < i) {
            throw new MMException(String.format("Failed to retrieve all image assets for custom event native ad. Excepted %d, downloaded %d.", Integer.valueOf(i), Integer.valueOf(this.imagesDownloaded)));
        }
    }

    private void downloadImage(final CountDownLatch countDownLatch, final NativeAdapter.ImageComponentInfo imageComponentInfo) {
        if (imageComponentInfo == null || Utils.isEmpty(imageComponentInfo.bitmapUrl)) {
            countDownLatch.countDown();
        } else {
            HttpUtils.getBitmapFromGetRequestAsync(imageComponentInfo.bitmapUrl, new HttpUtils.HttpRequestListener() {
                public void onResponse(HttpUtils.Response response) {
                    if (response.code == 200) {
                        imageComponentInfo.bitmap = response.bitmap;
                        imageComponentInfo.width = response.bitmap.getWidth();
                        imageComponentInfo.height = response.bitmap.getHeight();
                        NativeMediatedAdAdapter.access$508(NativeMediatedAdAdapter.this);
                    } else {
                        MMLog.e(NativeMediatedAdAdapter.TAG, "An HTTP error occurred downloading custom native ad event image.");
                    }
                    countDownLatch.countDown();
                }
            });
        }
    }

    public void release() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (NativeMediatedAdAdapter.this.customEventNative != null) {
                    NativeMediatedAdAdapter.this.customEventNative.destroy();
                    CustomEventNative unused = NativeMediatedAdAdapter.this.customEventNative = null;
                }
            }
        });
    }
}
