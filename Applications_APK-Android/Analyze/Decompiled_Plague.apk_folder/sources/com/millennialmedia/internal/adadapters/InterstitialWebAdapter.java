package com.millennialmedia.internal.adadapters;

import android.content.Context;
import com.millennialmedia.InterstitialAd;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdMetadata;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.MMActivity;
import com.millennialmedia.internal.adadapters.InterstitialAdapter;
import com.millennialmedia.internal.adcontrollers.WebController;

public class InterstitialWebAdapter extends InterstitialAdapter {
    private static final String TAG = "InterstitialWebAdapter";
    WebController.WebControllerListener controllerListener = new WebController.WebControllerListener() {
        public void attachSucceeded() {
        }

        public void onResize(int i, int i2) {
        }

        public void initSucceeded() {
            InterstitialWebAdapter.this.adapterListener.initSucceeded();
        }

        public void initFailed() {
            InterstitialWebAdapter.this.adapterListener.initFailed();
        }

        public void attachFailed() {
            InterstitialWebAdapter.this.adapterListener.showFailed(new InterstitialAd.InterstitialErrorStatus(7, "Unable to start interstitial activity"));
        }

        public void onClicked() {
            InterstitialWebAdapter.this.adapterListener.onClicked();
        }

        public void onAdLeftApplication() {
            InterstitialWebAdapter.this.adapterListener.onAdLeftApplication();
        }

        public void onResized(int i, int i2, boolean z) {
            if (z) {
                InterstitialWebAdapter.this.adapterListener.onClosed();
            }
        }

        public void onExpanded() {
            InterstitialWebAdapter.this.adapterListener.shown();
        }

        public void onCollapsed() {
            InterstitialWebAdapter.this.adapterListener.onClosed();
        }

        public void onUnload() {
            InterstitialWebAdapter.this.adapterListener.onUnload();
        }
    };
    WebController webController;

    public void init(Context context, InterstitialAdapter.InterstitialAdapterListener interstitialAdapterListener) {
        this.adapterListener = interstitialAdapterListener;
        WebController.WebControllerOptions webControllerOptions = new WebController.WebControllerOptions(true, Handshake.isMoatEnabled(), isEnhancedAdControlEnabled());
        this.webController = new WebController(this.controllerListener);
        this.webController.init(context, this.adContent, this.adMetadata, webControllerOptions);
    }

    public void show(Context context, AdPlacement.DisplayOptions displayOptions) {
        if (this.webController != null) {
            if (displayOptions == null) {
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(TAG, "Display options not specified, using defaults.");
                }
                displayOptions = new AdPlacement.DisplayOptions();
            }
            this.webController.showExpanded(new MMActivity.MMActivityConfig().setImmersive(displayOptions.isImmersive()).setTransitionAnimation(displayOptions.getEnterAnimationId(), displayOptions.getExitAnimationId()).setTransparent(this.adMetadata != null && this.adMetadata.isTransparent()));
        }
    }

    public long getImpressionDelay() {
        return isEnhancedAdControlEnabled() ? 0 : 1000;
    }

    public int getMinImpressionViewabilityPercentage() {
        return isEnhancedAdControlEnabled() ? -1 : 50;
    }

    public void release() {
        if (this.webController != null) {
            this.webController.close();
            this.webController.release();
            this.webController = null;
        }
    }

    private boolean isEnhancedAdControlEnabled() {
        return this.adMetadata.getBoolean(AdMetadata.ENHANCED_AD_CONTROL_ENABLED, false);
    }
}
