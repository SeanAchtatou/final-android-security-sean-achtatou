package com.millennialmedia.internal.adadapters;

import com.millennialmedia.CreativeInfo;
import com.millennialmedia.InlineAd;
import com.millennialmedia.InterstitialAd;
import com.millennialmedia.MMLog;
import com.millennialmedia.NativeAd;
import com.millennialmedia.internal.AdMetadata;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.adcontrollers.AdController;
import com.millennialmedia.internal.adcontrollers.LightboxController;
import com.millennialmedia.internal.adcontrollers.NativeController;
import com.millennialmedia.internal.adcontrollers.VASTVideoController;
import com.millennialmedia.internal.adcontrollers.WebController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class AdAdapter {
    public static final int DEFAULT_MIN_IMPRESSION_DELAY = 1000;
    public static final int DEFAULT_MIN_IMPRESSION_VIEWABILITY_PERCENTAGE = 50;
    public static final int NO_IMPRESSION_DELAY = 0;
    public static final int NO_MIN_IMPRESSION_VIEWABILITY_PERCENTAGE = -1;
    private static final String TAG = "AdAdapter";
    private static List<AdapterRegistration> registeredAdapters = new ArrayList();
    private static Map<Class<? extends AdPlacement>, Class<? extends MediatedAdAdapter>> registeredMediatedAdAdapters = new HashMap();
    protected String adContent;
    protected AdMetadata adMetadata = new AdMetadata();
    private CreativeInfo creativeInfo;
    public int requestTimeout = 0;

    public long getImpressionDelay() {
        return 1000;
    }

    public int getMinImpressionViewabilityPercentage() {
        return 50;
    }

    public abstract void release();

    public CreativeInfo getCreativeInfo() {
        return this.creativeInfo;
    }

    public void setCreativeInfo(CreativeInfo creativeInfo2) {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "CreativeInfo " + creativeInfo2);
        }
        this.creativeInfo = creativeInfo2;
    }

    private static class AdapterRegistration {
        Class<?> adAdapterClass;
        Class<?> adControllerClass;
        Class<?> adPlacementClass;

        AdapterRegistration(Class<?> cls, Class<?> cls2, Class<?> cls3) {
            this.adPlacementClass = cls;
            this.adAdapterClass = cls2;
            this.adControllerClass = cls3;
        }
    }

    public static void registerPackagedAdapters() {
        registerAdapter(InlineAd.class, InlineLightboxAdapter.class, LightboxController.class);
        registerAdapter(InterstitialAd.class, InterstitialVASTVideoAdapter.class, VASTVideoController.class);
        registerAdapter(InlineAd.class, InlineWebAdapter.class, WebController.class);
        registerAdapter(InterstitialAd.class, InterstitialWebAdapter.class, WebController.class);
        registerAdapter(NativeAd.class, NativeNativeAdapter.class, NativeController.class);
    }

    public static void registerAdapter(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        if (!AdPlacement.class.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Unable to register ad adapter, specified placement class is not an instance of AdPlacement");
        } else if (!AdAdapter.class.isAssignableFrom(cls2)) {
            throw new IllegalArgumentException("Unable to register ad adapter, specified adapter class is not an instance of AdAdapter");
        } else if (!AdController.class.isAssignableFrom(cls3)) {
            throw new IllegalArgumentException("Unable to register ad adapter, specified controller class is not an instance of AdController");
        } else {
            if (MMLog.isDebugEnabled()) {
                String str = TAG;
                MMLog.d(str, "Registering ad adapter <" + cls2 + "> for ad placement <" + cls + "> and ad controller <" + cls3 + ">");
            }
            Iterator<AdapterRegistration> it = registeredAdapters.iterator();
            while (it.hasNext()) {
                AdapterRegistration next = it.next();
                if (next.adPlacementClass == cls && next.adAdapterClass == cls2 && next.adControllerClass == cls3) {
                    it.remove();
                }
            }
            registeredAdapters.add(new AdapterRegistration(cls, cls2, cls3));
        }
    }

    public static void registerMediatedAdAdapter(Class<? extends AdPlacement> cls, Class<? extends MediatedAdAdapter> cls2) {
        if (cls == null) {
            throw new IllegalArgumentException("Unable to register mediated ad adapter, specified placement class must not be null.");
        } else if (cls2 == null) {
            throw new IllegalArgumentException("Unable to register mediated ad adapter, specified mediated ad adapter class must not be null.");
        } else {
            if (MMLog.isDebugEnabled()) {
                String str = TAG;
                MMLog.d(str, "Registering mediated ad adapter <" + cls2 + "> for ad placement <" + cls + ">");
            }
            registeredMediatedAdAdapters.put(cls, cls2);
        }
    }

    public static AdAdapter getAdapterInstance(Class<?> cls, Class<?> cls2) {
        Class<?> cls3;
        Iterator<AdapterRegistration> it = registeredAdapters.iterator();
        while (true) {
            if (!it.hasNext()) {
                cls3 = null;
                break;
            }
            AdapterRegistration next = it.next();
            boolean equals = next.adPlacementClass.equals(cls);
            boolean equals2 = next.adControllerClass.equals(cls2);
            if (equals && equals2) {
                cls3 = next.adAdapterClass;
                break;
            }
        }
        if (cls3 != null) {
            return (AdAdapter) cls3.getConstructor(new Class[0]).newInstance(new Object[0]);
        }
        try {
            throw new Exception("Unable to find adapter class");
        } catch (Exception e) {
            MMLog.e(TAG, "Unable to create ad adapter instance for the placement type <" + cls + "> and ad controller type <" + cls2 + ">", e);
            return null;
        }
    }

    public static MediatedAdAdapter getMediatedAdapterInstance(Class<? extends AdPlacement> cls) {
        if (cls == null) {
            MMLog.e(TAG, "Ad placement class cannot be null.");
            return null;
        }
        Class cls2 = registeredMediatedAdAdapters.get(cls);
        if (cls2 == null) {
            MMLog.e(TAG, String.format("No mediated ad adapters registered for placement type: %s", cls));
            return null;
        }
        try {
            return (MediatedAdAdapter) cls2.newInstance();
        } catch (Throwable th) {
            MMLog.e(TAG, String.format("Unable to instantiate mediated ad adapter class %s for placement %s.", cls2, cls), th);
            return null;
        }
    }

    public void setContent(String str) {
        this.adContent = str;
    }

    public void setAdMetadata(AdMetadata adMetadata2) {
        this.adMetadata.addAll(adMetadata2);
    }
}
