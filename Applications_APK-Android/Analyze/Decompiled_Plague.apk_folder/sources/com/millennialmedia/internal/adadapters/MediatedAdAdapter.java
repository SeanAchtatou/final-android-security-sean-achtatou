package com.millennialmedia.internal.adadapters;

public interface MediatedAdAdapter {
    void setMediationInfo(MediationInfo mediationInfo);

    public static class MediationInfo {
        public String networkId;
        public String siteId;
        public String spaceId;

        public MediationInfo(String str, String str2, String str3) {
            this.networkId = str;
            this.siteId = str2;
            this.spaceId = str3;
        }
    }
}
