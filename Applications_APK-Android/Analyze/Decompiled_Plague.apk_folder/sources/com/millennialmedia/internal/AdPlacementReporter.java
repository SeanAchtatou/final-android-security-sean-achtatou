package com.millennialmedia.internal;

import android.os.SystemClock;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.adwrapper.AdWrapper;
import com.millennialmedia.internal.task.TaskFactory;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.IOUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdPlacementReporter {
    public static final int BID_PRICE_MISSING = 110;
    public static final int BID_SUBMITTED = 1;
    public static final int DISPLAY_TYPE_AUTO = 0;
    public static final int DISPLAY_TYPE_CLICK = 2;
    public static final int DISPLAY_TYPE_UNKNOWN = -1;
    public static final int DISPLAY_TYPE_VISIBILITY = 1;
    private static final String EVENT_CLICK = "click_";
    private static final String EVENT_DISPLAY = "display_";
    private static final String EVENT_REQUEST = "request_";
    private static final String EXTENSION_JSON = ".json";
    private static final String EXTENSION_TEMP = ".tmp";
    public static final int PLAYLIST_LOADED_FROM_CACHE_DEMAND_ITEM_FAILED = 112;
    public static final int PLAYLIST_LOADED_FROM_CACHE_DEMAND_ITEM_SUCCEEDED = 111;
    public static final int PLAYLIST_REPLACED_IN_CACHE = 114;
    public static final int PLAYLIST_TIMED_OUT_IN_CACHE = 113;
    public static final String REPORTING_DIR = "/.reporting/";
    private static final String REPORT_KEY_ADNET = "adnet";
    private static final String REPORT_KEY_BUYER = "buyer";
    private static final String REPORT_KEY_CLICK = "click";
    private static final String REPORT_KEY_DISPLAY = "display";
    private static final String REPORT_KEY_IMPRESSION_GROUP = "grp";
    private static final String REPORT_KEY_ITEM_ID = "tag";
    private static final String REPORT_KEY_PLACEMENT_NAME = "zone";
    private static final String REPORT_KEY_PRICE = "price";
    private static final String REPORT_KEY_PRU = "pru";
    private static final String REPORT_KEY_REQUEST = "req";
    private static final String REPORT_KEY_RESPONSE_ID = "a";
    private static final String REPORT_KEY_RESPONSE_TIME = "resp";
    private static final String REPORT_KEY_STATUS = "status";
    private static final String REPORT_KEY_SUPER_AUCTION = "superAuction";
    private static final String REPORT_KEY_SUPER_AUCTION_BIDDERS = "bidders";
    private static final String REPORT_KEY_SUPER_AUCTION_DEMAND_SOURCES = "demandSources";
    private static final String REPORT_KEY_TIMESTAMP = "ts";
    private static final String REPORT_KEY_TYPE = "type";
    public static final String SITEID_FILENAME = "siteid";
    public static final String SSP_REPORTING_PATH = "/admax/sdk/report/4";
    public static final String SSP_SITE_ID_PARAMETER = "?dcn=";
    private static final int STARTUP_DELAY_IN_SECONDS = 5;
    public static final int STATUS_AD_SERVED = 1;
    public static final int STATUS_NO_AD = -1;
    public static final int STATUS_NO_AD_ERROR = -3;
    public static final int STATUS_NO_AD_TIMEOUT = -2;
    /* access modifiers changed from: private */
    public static final String TAG = "AdPlacementReporter";
    /* access modifiers changed from: private */
    public static volatile AtomicInteger numQueuedEvents = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public static volatile File reportingDir;
    /* access modifiers changed from: private */
    public static final Object stateLock = new Object();
    /* access modifiers changed from: private */
    public static volatile UploadState uploadState = UploadState.IDLE;
    private volatile PlayListItemReporter activePlayListItemReporter;
    private volatile String buyer;
    private boolean clickReported = false;
    private boolean displayReported = false;
    private volatile String eventId;
    private volatile String impressionGroup;
    private volatile String itemId;
    private volatile String placementName;
    private volatile ElapsedTimer playlistProcessingElapsedTimer;
    private volatile JSONObject playlistReportJson;
    private volatile String pru;
    private volatile String responseId;

    public static class Bidder {
        public String price;
        public int status;
        public String type;
    }

    public static class SuperAuction {
        public List<Bidder> bidders = new CopyOnWriteArrayList();
        public List<DemandSource> demandSources = new CopyOnWriteArrayList();
        public String itemId;
        public int status;
    }

    enum UploadState {
        IDLE,
        UPLOADING,
        ERROR_NETWORK_UNAVAILABLE,
        ERROR_SENDING_TO_SERVER,
        CLEARING
    }

    static String getDisplayTypeName(int i) {
        return i == 1 ? "visibility" : i == 2 ? REPORT_KEY_CLICK : i == 0 ? "auto" : EnvironmentCompat.MEDIA_UNKNOWN;
    }

    public static class ElapsedTimer {
        private long endTime;
        private long startTime;

        public void start() {
            this.startTime = SystemClock.elapsedRealtime();
            this.endTime = 0;
        }

        public void stop() {
            this.endTime = SystemClock.elapsedRealtime();
        }

        public long getElapsedTime() {
            if (this.endTime == 0) {
                stop();
            }
            return this.endTime - this.startTime;
        }
    }

    public static class DemandSource {
        /* access modifiers changed from: private */
        public ElapsedTimer elapsedTimer = new ElapsedTimer();
        public int status;
        public String tag;
        public String type;

        public DemandSource() {
            this.elapsedTimer.start();
        }
    }

    public class PlayListItemReporter {
        public String buyer;
        /* access modifiers changed from: private */
        public ElapsedTimer elapsedTimer = new ElapsedTimer();
        public String itemId;
        public String pru;
        public int status;
        /* access modifiers changed from: private */
        public SuperAuction superAuction;

        PlayListItemReporter() {
            this.elapsedTimer.start();
        }

        public SuperAuction getSuperAuction() {
            if (this.superAuction == null) {
                this.superAuction = new SuperAuction();
            }
            return this.superAuction;
        }
    }

    static class Uploader {
        private static volatile ThreadUtils.ScheduledRunnable scheduledRunnable;

        Uploader() {
        }

        private static File[] getEventsToUpload() {
            File[] listFiles = AdPlacementReporter.reportingDir.listFiles(new FilenameFilter() {
                public boolean accept(File file, String str) {
                    return str.endsWith(AdPlacementReporter.EXTENSION_JSON);
                }
            });
            return listFiles == null ? new File[0] : listFiles;
        }

        private static void deleteUploadedEvents(File[] fileArr) {
            int i = 0;
            for (File file : fileArr) {
                if (!file.delete()) {
                    MMLog.e(AdPlacementReporter.TAG, "Failed to delete reporting file <" + file.getName() + ">");
                } else {
                    i--;
                }
            }
            AdPlacementReporter.numQueuedEvents.addAndGet(i);
        }

        private static String buildRequest(File[] fileArr) {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            JSONArray jSONArray2 = new JSONArray();
            JSONArray jSONArray3 = new JSONArray();
            try {
                for (File file : fileArr) {
                    JSONObject retrieveEvent = retrieveEvent(file);
                    if (retrieveEvent != null) {
                        String name = file.getName();
                        if (name.startsWith(AdPlacementReporter.EVENT_REQUEST)) {
                            jSONArray.put(retrieveEvent);
                        } else if (name.startsWith(AdPlacementReporter.EVENT_DISPLAY)) {
                            jSONArray2.put(retrieveEvent);
                        } else if (name.startsWith(AdPlacementReporter.EVENT_CLICK)) {
                            jSONArray3.put(retrieveEvent);
                        }
                    }
                }
                if (jSONArray.length() > 0) {
                    jSONObject.put("req", jSONArray);
                }
                if (jSONArray2.length() > 0) {
                    jSONObject.put("display", jSONArray2);
                }
                if (jSONArray3.length() > 0) {
                    jSONObject.put(AdPlacementReporter.REPORT_KEY_CLICK, jSONArray3);
                }
                if (jSONObject.length() == 0) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(AdPlacementReporter.TAG, "No reporting events added to the request");
                    }
                    return null;
                }
                if (MMLog.isDebugEnabled()) {
                    try {
                        MMLog.d(AdPlacementReporter.TAG, "Generated report.\n" + jSONObject.toString(2));
                    } catch (JSONException unused) {
                        MMLog.d(AdPlacementReporter.TAG, "Unable to format report with indentation");
                    }
                }
                return jSONObject.toString();
            } catch (Exception e) {
                MMLog.e(AdPlacementReporter.TAG, "Error creating SSP reporting request", e);
                return null;
            }
        }

        public static boolean saveToFile(File file, String str) {
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                try {
                    IOUtils.write(fileOutputStream2, str);
                    return IOUtils.closeStream(fileOutputStream2);
                } catch (Exception e) {
                    e = e;
                    fileOutputStream = fileOutputStream2;
                    try {
                        String access$100 = AdPlacementReporter.TAG;
                        MMLog.e(access$100, "Error writing to file <" + file.getName() + ">", e);
                        return IOUtils.closeStream(fileOutputStream);
                    } catch (Throwable th) {
                        th = th;
                        IOUtils.closeStream(fileOutputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    IOUtils.closeStream(fileOutputStream);
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                String access$1002 = AdPlacementReporter.TAG;
                MMLog.e(access$1002, "Error writing to file <" + file.getName() + ">", e);
                return IOUtils.closeStream(fileOutputStream);
            }
        }

        public static String readFromFile(File file) {
            FileInputStream fileInputStream;
            if (!file.exists()) {
                return null;
            }
            try {
                fileInputStream = new FileInputStream(file);
                try {
                    String read = IOUtils.read(fileInputStream, "UTF-8");
                    IOUtils.closeStream(fileInputStream);
                    return read;
                } catch (IOException e) {
                    e = e;
                    try {
                        MMLog.e(AdPlacementReporter.TAG, "Error opening file <" + file.getName() + ">", e);
                        IOUtils.closeStream(fileInputStream);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        IOUtils.closeStream(fileInputStream);
                        throw th;
                    }
                }
            } catch (IOException e2) {
                e = e2;
                fileInputStream = null;
                MMLog.e(AdPlacementReporter.TAG, "Error opening file <" + file.getName() + ">", e);
                IOUtils.closeStream(fileInputStream);
                return null;
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = null;
                IOUtils.closeStream(fileInputStream);
                throw th;
            }
        }

        private static String retrieveSiteId() {
            return readFromFile(new File(AdPlacementReporter.reportingDir, AdPlacementReporter.SITEID_FILENAME));
        }

        /* access modifiers changed from: private */
        public static void storeSiteId(String str) {
            saveToFile(new File(AdPlacementReporter.reportingDir, AdPlacementReporter.SITEID_FILENAME), str);
        }

        /* access modifiers changed from: private */
        public static File storeEvent(String str, String str2, JSONObject jSONObject, boolean z) {
            if (str2 == null) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            sb.append(z ? AdPlacementReporter.EXTENSION_JSON : AdPlacementReporter.EXTENSION_TEMP);
            File file = new File(AdPlacementReporter.reportingDir, sb.toString());
            if (saveToFile(file, jSONObject.toString()) && z) {
                incrementQueuedEventsCount();
            }
            return file;
        }

        private static JSONObject retrieveEvent(File file) {
            if (file.exists()) {
                try {
                    return new JSONObject(readFromFile(file));
                } catch (JSONException e) {
                    String access$100 = AdPlacementReporter.TAG;
                    MMLog.e(access$100, "Error parsing reporting file <" + file.getName() + ">", e);
                }
            }
            return null;
        }

        private static void countQueuedEvents() {
            int i = 0;
            for (File file : AdPlacementReporter.reportingDir.listFiles()) {
                if (file.getName().endsWith(AdPlacementReporter.EXTENSION_TEMP)) {
                    if (setEventAsCompleted(file, false)) {
                        i++;
                    }
                } else if (file.getName().endsWith(AdPlacementReporter.EXTENSION_JSON)) {
                    i++;
                }
            }
            AdPlacementReporter.numQueuedEvents.set(i);
        }

        private static void incrementQueuedEventsCount() {
            synchronized (AdPlacementReporter.stateLock) {
                int incrementAndGet = AdPlacementReporter.numQueuedEvents.incrementAndGet();
                if (AdPlacementReporter.uploadState == UploadState.IDLE && incrementAndGet >= Handshake.getReportingBatchSize()) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(AdPlacementReporter.TAG, "Reporting batch size limit detected -- requesting upload");
                    }
                    setUploadState(UploadState.UPLOADING);
                }
            }
        }

        /* access modifiers changed from: private */
        public static boolean setEventAsCompleted(File file, boolean z) {
            File file2 = new File(file.getPath().replace(AdPlacementReporter.EXTENSION_TEMP, AdPlacementReporter.EXTENSION_JSON));
            if (file2.exists()) {
                String access$100 = AdPlacementReporter.TAG;
                MMLog.w(access$100, "Target file already exists + <" + file2.getName() + ">");
            } else if (file.renameTo(file2)) {
                if (!z) {
                    return true;
                }
                incrementQueuedEventsCount();
                return true;
            }
            String access$1002 = AdPlacementReporter.TAG;
            MMLog.w(access$1002, "Unable to rename temp file <" + file.getName() + ">");
            if (file.delete()) {
                return false;
            }
            String access$1003 = AdPlacementReporter.TAG;
            MMLog.e(access$1003, "Error deleting temp file <" + file.getName() + ">");
            return false;
        }

        /* access modifiers changed from: private */
        public static void start() {
            File unused = AdPlacementReporter.reportingDir = new File(EnvironmentUtils.getMillennialDir() + AdPlacementReporter.REPORTING_DIR);
            AdPlacementReporter.reportingDir.mkdirs();
            if (!AdPlacementReporter.reportingDir.isDirectory()) {
                MMLog.e(AdPlacementReporter.TAG, "Unable to creating reporting directory");
                return;
            }
            countQueuedEvents();
            scheduledRunnable = ThreadUtils.runOnWorkerThreadDelayed(new Runnable() {
                public void run() {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(AdPlacementReporter.TAG, "Reporting startup -- requesting upload");
                    }
                    Uploader.setUploadState(UploadState.UPLOADING);
                }
            }, 5000);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
            switch(com.millennialmedia.internal.AdPlacementReporter.access$400()) {
                case com.millennialmedia.internal.AdPlacementReporter.UploadState.IDLE :com.millennialmedia.internal.AdPlacementReporter$UploadState: goto L_0x00a2;
                case com.millennialmedia.internal.AdPlacementReporter.UploadState.UPLOADING :com.millennialmedia.internal.AdPlacementReporter$UploadState: goto L_0x0082;
                case com.millennialmedia.internal.AdPlacementReporter.UploadState.ERROR_NETWORK_UNAVAILABLE :com.millennialmedia.internal.AdPlacementReporter$UploadState: goto L_0x005f;
                case com.millennialmedia.internal.AdPlacementReporter.UploadState.ERROR_SENDING_TO_SERVER :com.millennialmedia.internal.AdPlacementReporter$UploadState: goto L_0x003f;
                case com.millennialmedia.internal.AdPlacementReporter.UploadState.CLEARING :com.millennialmedia.internal.AdPlacementReporter$UploadState: goto L_0x0022;
                default: goto L_0x0020;
            };
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
            if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x0031;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
            com.millennialmedia.MMLog.d(com.millennialmedia.internal.AdPlacementReporter.access$100(), "Reporting upload state set to CLEARING");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
            if (com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable == null) goto L_0x003a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
            com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable.cancel();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x003a, code lost:
            clearNow();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0043, code lost:
            if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x004e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0045, code lost:
            com.millennialmedia.MMLog.d(com.millennialmedia.internal.AdPlacementReporter.access$100(), "Reporting upload state set to ERROR_SENDING_TO_SERVER");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x004e, code lost:
            com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(new com.millennialmedia.internal.AdPlacementReporter.Uploader.AnonymousClass5(), (long) com.millennialmedia.internal.Handshake.getReportingBatchFrequency());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0063, code lost:
            if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x006e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0065, code lost:
            com.millennialmedia.MMLog.d(com.millennialmedia.internal.AdPlacementReporter.access$100(), "Reporting upload state set to ERROR_NETWORK_UNAVAILABLE");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x006e, code lost:
            com.millennialmedia.internal.utils.EnvironmentUtils.getApplicationContext().registerReceiver(new com.millennialmedia.internal.AdPlacementReporter.Uploader.AnonymousClass4(), new android.content.IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0086, code lost:
            if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x0091;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0088, code lost:
            com.millennialmedia.MMLog.d(com.millennialmedia.internal.AdPlacementReporter.access$100(), "Reporting upload state set to UPLOADING");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0093, code lost:
            if (com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable == null) goto L_0x009a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0095, code lost:
            com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable.cancel();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x009a, code lost:
            com.millennialmedia.internal.task.TaskFactory.createAdPlacementReporterTask().execute();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a6, code lost:
            if (com.millennialmedia.MMLog.isDebugEnabled() == false) goto L_0x00b1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a8, code lost:
            com.millennialmedia.MMLog.d(com.millennialmedia.internal.AdPlacementReporter.access$100(), "Reporting upload state set to IDLE");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b1, code lost:
            com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(new com.millennialmedia.internal.AdPlacementReporter.Uploader.AnonymousClass3(), (long) com.millennialmedia.internal.Handshake.getReportingBatchFrequency());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static void setUploadState(com.millennialmedia.internal.AdPlacementReporter.UploadState r3) {
            /*
                java.lang.Object r0 = com.millennialmedia.internal.AdPlacementReporter.stateLock
                monitor-enter(r0)
                com.millennialmedia.internal.AdPlacementReporter$UploadState r1 = com.millennialmedia.internal.AdPlacementReporter.uploadState     // Catch:{ all -> 0x00c2 }
                if (r3 != r1) goto L_0x000d
                monitor-exit(r0)     // Catch:{ all -> 0x00c2 }
                return
            L_0x000d:
                com.millennialmedia.internal.AdPlacementReporter.UploadState unused = com.millennialmedia.internal.AdPlacementReporter.uploadState = r3     // Catch:{ all -> 0x00c2 }
                monitor-exit(r0)     // Catch:{ all -> 0x00c2 }
                int[] r3 = com.millennialmedia.internal.AdPlacementReporter.AnonymousClass1.$SwitchMap$com$millennialmedia$internal$AdPlacementReporter$UploadState
                com.millennialmedia.internal.AdPlacementReporter$UploadState r0 = com.millennialmedia.internal.AdPlacementReporter.uploadState
                int r0 = r0.ordinal()
                r3 = r3[r0]
                switch(r3) {
                    case 1: goto L_0x00a2;
                    case 2: goto L_0x0082;
                    case 3: goto L_0x005f;
                    case 4: goto L_0x003f;
                    case 5: goto L_0x0022;
                    default: goto L_0x0020;
                }
            L_0x0020:
                goto L_0x00c1
            L_0x0022:
                boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()
                if (r3 == 0) goto L_0x0031
                java.lang.String r3 = com.millennialmedia.internal.AdPlacementReporter.TAG
                java.lang.String r0 = "Reporting upload state set to CLEARING"
                com.millennialmedia.MMLog.d(r3, r0)
            L_0x0031:
                com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable
                if (r3 == 0) goto L_0x003a
                com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable
                r3.cancel()
            L_0x003a:
                clearNow()
                goto L_0x00c1
            L_0x003f:
                boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()
                if (r3 == 0) goto L_0x004e
                java.lang.String r3 = com.millennialmedia.internal.AdPlacementReporter.TAG
                java.lang.String r0 = "Reporting upload state set to ERROR_SENDING_TO_SERVER"
                com.millennialmedia.MMLog.d(r3, r0)
            L_0x004e:
                com.millennialmedia.internal.AdPlacementReporter$Uploader$5 r3 = new com.millennialmedia.internal.AdPlacementReporter$Uploader$5
                r3.<init>()
                int r0 = com.millennialmedia.internal.Handshake.getReportingBatchFrequency()
                long r0 = (long) r0
                com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r3, r0)
                com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable = r3
                goto L_0x00c1
            L_0x005f:
                boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()
                if (r3 == 0) goto L_0x006e
                java.lang.String r3 = com.millennialmedia.internal.AdPlacementReporter.TAG
                java.lang.String r0 = "Reporting upload state set to ERROR_NETWORK_UNAVAILABLE"
                com.millennialmedia.MMLog.d(r3, r0)
            L_0x006e:
                android.content.Context r3 = com.millennialmedia.internal.utils.EnvironmentUtils.getApplicationContext()
                com.millennialmedia.internal.AdPlacementReporter$Uploader$4 r0 = new com.millennialmedia.internal.AdPlacementReporter$Uploader$4
                r0.<init>()
                android.content.IntentFilter r1 = new android.content.IntentFilter
                java.lang.String r2 = "android.net.conn.CONNECTIVITY_CHANGE"
                r1.<init>(r2)
                r3.registerReceiver(r0, r1)
                goto L_0x00c1
            L_0x0082:
                boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()
                if (r3 == 0) goto L_0x0091
                java.lang.String r3 = com.millennialmedia.internal.AdPlacementReporter.TAG
                java.lang.String r0 = "Reporting upload state set to UPLOADING"
                com.millennialmedia.MMLog.d(r3, r0)
            L_0x0091:
                com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable
                if (r3 == 0) goto L_0x009a
                com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable
                r3.cancel()
            L_0x009a:
                com.millennialmedia.internal.task.Task r3 = com.millennialmedia.internal.task.TaskFactory.createAdPlacementReporterTask()
                r3.execute()
                goto L_0x00c1
            L_0x00a2:
                boolean r3 = com.millennialmedia.MMLog.isDebugEnabled()
                if (r3 == 0) goto L_0x00b1
                java.lang.String r3 = com.millennialmedia.internal.AdPlacementReporter.TAG
                java.lang.String r0 = "Reporting upload state set to IDLE"
                com.millennialmedia.MMLog.d(r3, r0)
            L_0x00b1:
                com.millennialmedia.internal.AdPlacementReporter$Uploader$3 r3 = new com.millennialmedia.internal.AdPlacementReporter$Uploader$3
                r3.<init>()
                int r0 = com.millennialmedia.internal.Handshake.getReportingBatchFrequency()
                long r0 = (long) r0
                com.millennialmedia.internal.utils.ThreadUtils$ScheduledRunnable r3 = com.millennialmedia.internal.utils.ThreadUtils.runOnWorkerThreadDelayed(r3, r0)
                com.millennialmedia.internal.AdPlacementReporter.Uploader.scheduledRunnable = r3
            L_0x00c1:
                return
            L_0x00c2:
                r3 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x00c2 }
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.AdPlacementReporter.Uploader.setUploadState(com.millennialmedia.internal.AdPlacementReporter$UploadState):void");
        }

        static void uploadNow() {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(AdPlacementReporter.TAG, "Reporting is starting upload");
            }
            File[] eventsToUpload = getEventsToUpload();
            if (eventsToUpload.length == 0) {
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(AdPlacementReporter.TAG, "Reporting found no events to upload");
                }
                setUploadState(UploadState.IDLE);
                return;
            }
            String reportingBaseUrl = Handshake.getReportingBaseUrl();
            if (reportingBaseUrl == null) {
                MMLog.e(AdPlacementReporter.TAG, "Unable to determine base url for request");
                setUploadState(UploadState.ERROR_SENDING_TO_SERVER);
                return;
            }
            String concat = reportingBaseUrl.concat(AdPlacementReporter.SSP_REPORTING_PATH);
            String retrieveSiteId = retrieveSiteId();
            if (TextUtils.isEmpty(retrieveSiteId)) {
                MMLog.e(AdPlacementReporter.TAG, "Unable to upload report -- siteId has not been set");
                setUploadState(UploadState.ERROR_SENDING_TO_SERVER);
                return;
            }
            String buildRequest = buildRequest(eventsToUpload);
            if (buildRequest != null) {
                HttpUtils.Response contentFromPostRequest = HttpUtils.getContentFromPostRequest(concat + AdPlacementReporter.SSP_SITE_ID_PARAMETER + retrieveSiteId, buildRequest, "application/json");
                if (contentFromPostRequest.code != 200) {
                    if (EnvironmentUtils.isDeviceIdle()) {
                        String access$100 = AdPlacementReporter.TAG;
                        MMLog.e(access$100, "Reporting failed to upload with response code <" + contentFromPostRequest.code + "> while in Doze mode");
                        setUploadState(UploadState.ERROR_SENDING_TO_SERVER);
                        return;
                    } else if (!EnvironmentUtils.isNetworkAvailable()) {
                        MMLog.e(AdPlacementReporter.TAG, "Reporting failed to upload because network is unavailable");
                        setUploadState(UploadState.ERROR_NETWORK_UNAVAILABLE);
                        return;
                    } else {
                        String access$1002 = AdPlacementReporter.TAG;
                        MMLog.e(access$1002, "Reporting failed to upload with response code <" + contentFromPostRequest.code + ">");
                        setUploadState(UploadState.ERROR_SENDING_TO_SERVER);
                        return;
                    }
                } else if (MMLog.isDebugEnabled()) {
                    MMLog.d(AdPlacementReporter.TAG, "Report successfully uploaded");
                }
            }
            deleteUploadedEvents(eventsToUpload);
            if (AdPlacementReporter.numQueuedEvents.get() >= Handshake.getReportingBatchSize()) {
                TaskFactory.createAdPlacementReporterTask().execute();
            } else {
                setUploadState(UploadState.IDLE);
            }
        }

        static void clearNow() {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(AdPlacementReporter.TAG, "Reporting is clearing events");
            }
            File[] eventsToUpload = getEventsToUpload();
            if (eventsToUpload.length > 0) {
                deleteUploadedEvents(eventsToUpload);
            }
            setUploadState(UploadState.IDLE);
        }
    }

    public static UploadState getUploadState() {
        return uploadState;
    }

    public static void init() {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Initializing");
        }
        Uploader.start();
    }

    private AdPlacementReporter(PlayList playList, String str) throws Exception {
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Creating new reporting instance for responseId: " + playList.responseId);
        }
        Uploader.storeSiteId(playList.siteId);
        if (!TextUtils.isEmpty(playList.responseId)) {
            this.eventId = UUID.randomUUID().toString();
        }
        this.responseId = playList.responseId;
        this.placementName = playList.placementName;
        this.impressionGroup = str;
        this.playlistReportJson = new JSONObject();
        this.playlistReportJson.put(REPORT_KEY_TIMESTAMP, System.currentTimeMillis());
        this.playlistReportJson.put(REPORT_KEY_ADNET, new JSONArray());
        this.playlistReportJson.put(REPORT_KEY_RESPONSE_ID, this.responseId);
        this.playlistReportJson.put(REPORT_KEY_PLACEMENT_NAME, this.placementName);
        this.playlistReportJson.put(REPORT_KEY_IMPRESSION_GROUP, str);
        File unused = Uploader.storeEvent(EVENT_REQUEST, this.eventId, this.playlistReportJson, false);
        this.playlistProcessingElapsedTimer = new ElapsedTimer();
        this.playlistProcessingElapsedTimer.start();
    }

    /* access modifiers changed from: package-private */
    public void reportPlayList() {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Reporting playlist stop for responseId: " + this.responseId);
        }
        try {
            this.playlistReportJson.put(REPORT_KEY_RESPONSE_TIME, this.playlistProcessingElapsedTimer.getElapsedTime());
            File access$700 = Uploader.storeEvent(EVENT_REQUEST, this.eventId, this.playlistReportJson, false);
            if (access$700 != null) {
                boolean unused = Uploader.setEventAsCompleted(access$700, true);
            }
            this.playlistReportJson = null;
        } catch (Exception unused2) {
            MMLog.e(TAG, "Error stopping playlist reporting");
        }
    }

    /* access modifiers changed from: package-private */
    public PlayListItemReporter getPlayListItemReporter() {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Reporting playlist item start for responseId: " + this.responseId);
        }
        return new PlayListItemReporter();
    }

    /* access modifiers changed from: package-private */
    public void reportPlayListItem(PlayListItemReporter playListItemReporter) {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Reporting playlist item stop for responseId: " + this.responseId);
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(REPORT_KEY_ITEM_ID, playListItemReporter.itemId);
            jSONObject.put("status", playListItemReporter.status);
            jSONObject.put(REPORT_KEY_RESPONSE_TIME, playListItemReporter.elapsedTimer.getElapsedTime());
            reportSuperAuction(playListItemReporter, jSONObject);
            if (playListItemReporter.status == 1) {
                this.buyer = playListItemReporter.buyer;
                this.pru = playListItemReporter.pru;
                if (playListItemReporter.superAuction == null || playListItemReporter.superAuction.itemId == null) {
                    this.itemId = playListItemReporter.itemId;
                    this.playlistReportJson.put(REPORT_KEY_BUYER, this.buyer);
                    this.playlistReportJson.put(REPORT_KEY_PRU, this.pru);
                } else {
                    this.itemId = playListItemReporter.superAuction.itemId;
                }
            }
            this.playlistReportJson.getJSONArray(REPORT_KEY_ADNET).put(jSONObject);
            File unused = Uploader.storeEvent(EVENT_REQUEST, this.eventId, this.playlistReportJson, false);
        } catch (Exception unused2) {
            MMLog.e(TAG, "Error adding playlist item");
        }
    }

    private void reportSuperAuction(PlayListItemReporter playListItemReporter, JSONObject jSONObject) throws JSONException {
        if (playListItemReporter.superAuction != null) {
            jSONObject.put("status", playListItemReporter.superAuction.status);
            JSONObject jSONObject2 = new JSONObject();
            if (!playListItemReporter.superAuction.bidders.isEmpty()) {
                JSONArray jSONArray = new JSONArray();
                for (Bidder next : playListItemReporter.superAuction.bidders) {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("type", next.type);
                    jSONObject3.put(REPORT_KEY_PRICE, next.price);
                    jSONObject3.put("status", next.status);
                    jSONArray.put(jSONObject3);
                }
                jSONObject2.put(REPORT_KEY_SUPER_AUCTION_BIDDERS, jSONArray);
            }
            if (!playListItemReporter.superAuction.demandSources.isEmpty()) {
                JSONArray jSONArray2 = new JSONArray();
                for (DemandSource next2 : playListItemReporter.superAuction.demandSources) {
                    JSONObject jSONObject4 = new JSONObject();
                    jSONObject4.put("status", next2.status);
                    jSONObject4.put(REPORT_KEY_TIMESTAMP, System.currentTimeMillis());
                    jSONObject4.put(REPORT_KEY_ITEM_ID, next2.tag);
                    jSONObject4.put(REPORT_KEY_RESPONSE_TIME, next2.elapsedTimer.getElapsedTime());
                    jSONArray2.put(jSONObject4);
                }
                jSONObject2.put(REPORT_KEY_SUPER_AUCTION_DEMAND_SOURCES, jSONArray2);
            }
            jSONObject.put(REPORT_KEY_SUPER_AUCTION, jSONObject2);
        }
    }

    public static Bidder reportBidItem(JSONObject jSONObject, PlayListItemReporter playListItemReporter, int i) {
        if (playListItemReporter == null) {
            return null;
        }
        Bidder bidder = new Bidder();
        bidder.price = jSONObject.optString("bidPrice");
        bidder.type = jSONObject.optString("type");
        bidder.status = i;
        playListItemReporter.getSuperAuction().bidders.add(bidder);
        return bidder;
    }

    public static DemandSource reportDemandSource(PlayListItemReporter playListItemReporter, String str, AdWrapper adWrapper) {
        if (playListItemReporter == null) {
            return null;
        }
        DemandSource demandSource = new DemandSource();
        demandSource.tag = adWrapper.itemId;
        demandSource.type = str;
        playListItemReporter.getSuperAuction().demandSources.add(demandSource);
        return demandSource;
    }

    /* access modifiers changed from: package-private */
    public void setDisplayed(int i) {
        if (!this.displayReported) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, String.format("Reporting ad displayed for responseId: %s, %s", this.responseId, getDisplayTypeName(i)));
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(REPORT_KEY_RESPONSE_ID, this.responseId);
                jSONObject.put(REPORT_KEY_TIMESTAMP, System.currentTimeMillis());
                jSONObject.put(REPORT_KEY_PLACEMENT_NAME, this.placementName);
                jSONObject.put(REPORT_KEY_ITEM_ID, this.itemId);
                jSONObject.put(REPORT_KEY_BUYER, this.buyer);
                jSONObject.put(REPORT_KEY_PRU, this.pru);
                jSONObject.put(REPORT_KEY_IMPRESSION_GROUP, this.impressionGroup);
                File unused = Uploader.storeEvent(EVENT_DISPLAY, this.eventId, jSONObject, true);
            } catch (Exception unused2) {
                MMLog.e(TAG, "Error recording display");
            }
            this.displayReported = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void setClicked() {
        if (!this.clickReported) {
            if (MMLog.isDebugEnabled()) {
                String str = TAG;
                MMLog.d(str, "Reporting ad clicked for responseId: " + this.responseId);
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(REPORT_KEY_RESPONSE_ID, this.responseId);
                jSONObject.put(REPORT_KEY_TIMESTAMP, System.currentTimeMillis());
                jSONObject.put(REPORT_KEY_PLACEMENT_NAME, this.placementName);
                jSONObject.put(REPORT_KEY_ITEM_ID, this.itemId);
                jSONObject.put(REPORT_KEY_IMPRESSION_GROUP, this.impressionGroup);
                File unused = Uploader.storeEvent(EVENT_CLICK, this.eventId, jSONObject, true);
            } catch (Exception unused2) {
                MMLog.e(TAG, "Error recording click");
            }
            this.clickReported = true;
        }
    }

    public static AdPlacementReporter getPlayListReporter(PlayList playList, String str) {
        if (!playList.reportingEnabled) {
            return null;
        }
        try {
            return new AdPlacementReporter(playList, str);
        } catch (Exception unused) {
            MMLog.e(TAG, "Error starting ad placement reporting");
            return null;
        }
    }

    public static PlayListItemReporter getPlayListItemReporter(AdPlacementReporter adPlacementReporter) {
        if (adPlacementReporter == null) {
            return null;
        }
        adPlacementReporter.activePlayListItemReporter = adPlacementReporter.getPlayListItemReporter();
        return adPlacementReporter.activePlayListItemReporter;
    }

    public static void reportPlayListItem(AdPlacementReporter adPlacementReporter, PlayListItemReporter playListItemReporter, int i) {
        if (playListItemReporter != null) {
            playListItemReporter.status = i;
            reportPlayListItem(adPlacementReporter, playListItemReporter);
        }
    }

    public static void reportPlayListItem(AdPlacementReporter adPlacementReporter, PlayListItemReporter playListItemReporter) {
        if (adPlacementReporter != null) {
            if (adPlacementReporter.activePlayListItemReporter == playListItemReporter) {
                adPlacementReporter.reportPlayListItem(playListItemReporter);
                adPlacementReporter.activePlayListItemReporter = null;
            } else if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "reportPlayListItem called but item is not the active item");
            }
        }
    }

    public static void reportPlayList(AdPlacementReporter adPlacementReporter) {
        if (adPlacementReporter != null) {
            if (adPlacementReporter.activePlayListItemReporter != null) {
                adPlacementReporter.activePlayListItemReporter.status = -2;
                reportPlayListItem(adPlacementReporter, adPlacementReporter.activePlayListItemReporter);
            }
            adPlacementReporter.reportPlayList();
        }
    }

    public static void setDisplayed(AdPlacementReporter adPlacementReporter) {
        setDisplayed(adPlacementReporter, -1);
    }

    public static void setDisplayed(AdPlacementReporter adPlacementReporter, int i) {
        if (adPlacementReporter != null) {
            adPlacementReporter.setDisplayed(i);
        }
    }

    public static void setClicked(AdPlacementReporter adPlacementReporter) {
        if (adPlacementReporter != null) {
            adPlacementReporter.setClicked();
        }
    }

    static void clear() {
        if (uploadState != UploadState.UPLOADING) {
            Uploader.setUploadState(UploadState.CLEARING);
        }
    }

    public static void uploadNow() {
        Uploader.uploadNow();
    }
}
