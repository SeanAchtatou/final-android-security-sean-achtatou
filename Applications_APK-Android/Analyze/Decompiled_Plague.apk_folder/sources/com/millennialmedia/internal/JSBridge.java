package com.millennialmedia.internal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.webkit.JavascriptInterface;
import com.millennialmedia.AppInfo;
import com.millennialmedia.MMLog;
import com.millennialmedia.MMSDK;
import com.millennialmedia.internal.SizableStateManager;
import com.millennialmedia.internal.VolumeChangeManager;
import com.millennialmedia.internal.utils.AdvertisingIdInfo;
import com.millennialmedia.internal.utils.CalendarUtils;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.IOUtils;
import com.millennialmedia.internal.utils.JSONUtils;
import com.millennialmedia.internal.utils.MediaUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.Utils;
import com.millennialmedia.internal.utils.ViewUtils;
import com.millennialmedia.internal.video.InlineWebVideoView;
import com.millennialmedia.internal.video.VASTVideoView;
import com.miniclip.inapppurchases.MCInAppPurchases;
import com.miniclip.videoads.ProviderConfig;
import com.mopub.common.AdType;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSBridge implements VolumeChangeManager.VolumeChangeListener {
    private static final String AD_NOT_CLICKED_MESSAGE = "Ad has not been clicked";
    private static final String CALLBACK_ID_KEY = "callbackId";
    private static final String CANNOT_EXPAND_INTERSTITIAL_MESSAGE = "Cannot expand interstitial";
    private static final String CANNOT_EXPAND_STATE_MESSAGE = "Cannot expand in current state<%s>";
    private static final String CANNOT_RESIZE_INTERSTITIAL_MESSAGE = "Cannot resize interstitial";
    private static final String CANNOT_RESIZE_STATE_MESSAGE = "Cannot resize in current state<%s>";
    private static final String CREATE_CALENDAR_EVENT_MRAID_OP = "createCalendarEvent";
    private static final String EXPAND_FAILED_MESSAGE = "Unable to expand";
    private static final String EXPAND_MRAID_OP = "expand";
    private static final String HEIGHT_KEY = "height";
    private static final int HTTP_GET_TIMEOUT = 15000;
    private static final String INVALID_ORIENTATION_MESSAGE = "Invalid orientation specified <%s>";
    private static final String JS_MRAID_NAMESPACE = "MmJsBridge.mraid";
    private static final String JS_SET_EXPOSURE_CHANGE = "MmJsBridge.mraid.setExposureChange";
    private static final String JS_SET_LOCATION = "MmJsBridge.mraid.setLocation";
    private static final String JS_SET_PLACEMENT_TYPE = "MmJsBridge.mraid.setPlacementType";
    private static final String JS_SET_POSITIONS = "MmJsBridge.mraid.setPositions";
    private static final String JS_SET_STATE = "MmJsBridge.mraid.setState";
    private static final String JS_SET_SUPPORTS = "MmJsBridge.mraid.setSupports";
    private static final String JS_SET_VIEWABLE = "MmJsBridge.mraid.setViewable";
    private static final String JS_SET_VOLUME = "MmJsBridge.mraid.setVolume";
    private static final String JS_THROW_MRAID_ERROR = "MmJsBridge.mraid.throwMraidError";
    private static final int LOCATION_UPDATE_DELAY = 5000;
    private static final String MM_JS_BRIDGE_CALL_CALLBACK = "MmJsBridge.callbackManager.callCallback";
    private static final String MM_JS_BRIDGE_SET_LOG_LEVEL = "MmJsBridge.logging.setLogLevel";
    private static final String NOT_SUPPORTED_MESSAGE = "Not supported";
    private static final String NO_PICTURE_PATH_MESSAGE = "No path specified for picture";
    private static final String NO_VIDEO_PATH_MESSAGE = "No path specified for video";
    private static final String PARAMETERS_NOT_PROVIDED_MESSAGE = "No parameters provided";
    private static final String PLAY_VIDEO_MRAID_OP = "playVideo";
    private static final String RESIZE_FAILED_MESSAGE = "Unable to resize";
    private static final String RESIZE_MRAID_OP = "resize";
    private static final int SCROLL_IDLE_TIMEOUT = 450;
    private static final int SCROLL_UPDATE_INTERVAL = 100;
    private static final String STATE_DEFAULT = "default";
    private static final String STATE_EXPANDED = "expanded";
    private static final String STATE_HIDDEN = "hidden";
    private static final String STATE_LOADING = "loading";
    private static final String STATE_RESIZED = "resized";
    private static final String SUPPORTS_CALENDAR = "calendar";
    private static final String SUPPORTS_INLINE_VIDEO = "inlineVideo";
    private static final String SUPPORTS_LOCATION = "location";
    private static final String SUPPORTS_SMS = "sms";
    private static final String SUPPORTS_STORE_PICTURE = "storePicture";
    private static final String SUPPORTS_TEL = "tel";
    private static final String SUPPORTS_VPAID = "vpaid";
    /* access modifiers changed from: private */
    public static final String TAG = "JSBridge";
    private static final String URL_KEY = "url";
    private static final String URL_OPEN_FAILED_MESSAGE = "Unable to open url <%s>";
    private static final String VIDEO_ID_KEY = "videoId";
    private static final String WEBVIEW_NOT_VALID_MESSAGE = "Webview is no longer valid";
    private static final String WIDTH_KEY = "width";
    private static final Pattern bodyStartPattern = Pattern.compile("<body[^>]*>", 2);
    static final String bodyStyling = "<style>body {margin:0;padding:0;}</style>";
    private static final Pattern headStartPattern = Pattern.compile("<head[^>]*>", 2);
    private static final Pattern htmlStartPattern = Pattern.compile("<html[^>]*>", 2);
    private static final Pattern mraidJsReplacePattern = Pattern.compile("<script\\s+[^>]*\\bsrc\\s*=\\s*([\\\"\\'])mraid\\.js\\1[^>]*>\\s*</script\\s*>", 2);
    private static final Pattern nonMetaPattern = Pattern.compile("<(?!meta)[^>]*>", 2);
    static final Map<String, String> scriptFilesCache = new HashMap();
    static final String secureContentMeta = "<meta http-equiv=\"Content-Security-Policy\" content=\"upgrade-insecure-requests\">";
    static final boolean useActionsQueue = (Build.VERSION.SDK_INT < 19);
    /* access modifiers changed from: private */
    public volatile JSONArray actionsQueue;
    /* access modifiers changed from: private */
    public volatile boolean apiCallsEnabled = false;
    String currentState = STATE_LOADING;
    boolean hasSize = false;
    final boolean isInterstitial;
    boolean isResizing = true;
    boolean isTwoPartExpand = false;
    boolean isViewable = false;
    /* access modifiers changed from: private */
    public volatile JSBridgeListener jsBridgeListener;
    boolean jsInjected = false;
    Location lastLocation;
    int lastOrientation = EnvironmentUtils.getCurrentConfigOrientation();
    /* access modifiers changed from: private */
    public volatile boolean locationUpdatesEnabled = false;
    /* access modifiers changed from: private */
    public volatile WeakReference<MMWebView> mmWebViewRef;
    boolean ready = false;
    int requestedOrientation = -1;
    List<String> scriptsAwaitingLoad = new LinkedList();
    /* access modifiers changed from: private */
    public volatile long scrollIdleTimeout;
    /* access modifiers changed from: private */
    public volatile AtomicBoolean scrollThrottling = new AtomicBoolean(false);
    UpdateLocationTask updateLocationTask;

    public interface JSBridgeListener {
        void close();

        boolean expand(SizableStateManager.ExpandParams expandParams);

        void onAdLeftApplication();

        void onInjectedScriptsLoaded();

        void onJSBridgeReady();

        boolean resize(SizableStateManager.ResizeParams resizeParams);

        void setOrientation(int i);

        void unload();
    }

    @SuppressLint({"StaticFieldLeak"})
    private class UpdateLocationTask extends AsyncTask<Void, Void, Location> {
        private UpdateLocationTask() {
        }

        /* access modifiers changed from: protected */
        public Location doInBackground(Void... voidArr) {
            return EnvironmentUtils.getLocation();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Location location) {
            if (!JSBridge.isLocationSupported()) {
                JSBridge.this.sendLocation(null);
                return;
            }
            if (location != null && (JSBridge.this.lastLocation == null || JSBridge.this.lastLocation.distanceTo(location) > 10.0f)) {
                JSBridge.this.sendLocation(location);
            }
            if (!isCancelled() && JSBridge.this.locationUpdatesEnabled) {
                ThreadUtils.postOnUiThread(new Runnable() {
                    public void run() {
                        JSBridge.this.updateLocationTask = new UpdateLocationTask();
                        JSBridge.this.updateLocationTask.execute(new Void[0]);
                    }
                }, 5000);
            } else if (MMLog.isDebugEnabled()) {
                MMLog.d(JSBridge.TAG, "Shutting down update location task.");
            }
        }
    }

    /* access modifiers changed from: private */
    public void sendLocation(Location location) {
        if (location == null || !MMSDK.locationEnabled) {
            callJavascript(JS_SET_LOCATION, -1);
            return;
        }
        this.lastLocation = location;
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("lat", location.getLatitude());
            jSONObject.put("lon", location.getLongitude());
            jSONObject.put("type", 1);
            if (location.hasAccuracy()) {
                jSONObject.putOpt("accuracy", Float.valueOf(location.getAccuracy()));
            }
            jSONObject.put("lastfix", location.getTime() / 1000);
            callJavascript(JS_SET_LOCATION, jSONObject);
        } catch (JSONException e) {
            MMLog.e(TAG, "Error converting location to json.", e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    static JSONObject getSupportedFeatures() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(SUPPORTS_SMS, EnvironmentUtils.isSmsSupported());
            jSONObject.put(SUPPORTS_TEL, EnvironmentUtils.isTelSupported());
            jSONObject.put(SUPPORTS_CALENDAR, true);
            jSONObject.put(SUPPORTS_STORE_PICTURE, EnvironmentUtils.isExternalStorageSupported());
            jSONObject.put(SUPPORTS_INLINE_VIDEO, true);
            jSONObject.put(SUPPORTS_VPAID, false);
            jSONObject.put("location", isLocationSupported());
        } catch (JSONException e) {
            MMLog.e(TAG, "Error creating supports dictionary", e);
        }
        return jSONObject;
    }

    /* access modifiers changed from: private */
    public static boolean isLocationSupported() {
        return Boolean.TRUE.equals(EnvironmentUtils.hasFineLocationPermission()) && MMSDK.locationEnabled;
    }

    JSBridge(MMWebView mMWebView, boolean z, JSBridgeListener jSBridgeListener) {
        this.mmWebViewRef = new WeakReference<>(mMWebView);
        this.jsBridgeListener = jSBridgeListener;
        this.isInterstitial = z;
        if (mMWebView != null) {
            mMWebView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    int currentConfigOrientation;
                    if ((view instanceof MMWebView) && JSBridge.this.lastOrientation != (currentConfigOrientation = EnvironmentUtils.getCurrentConfigOrientation())) {
                        if (MMLog.isDebugEnabled()) {
                            String access$400 = JSBridge.TAG;
                            MMLog.d(access$400, "Detected change in orientation to " + EnvironmentUtils.getCurrentConfigOrientationString());
                        }
                        JSBridge.this.lastOrientation = currentConfigOrientation;
                        JSBridge.this.sendPositions((MMWebView) view);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"AddJavascriptInterface"})
    public String injectJSBridge(String str, boolean z) {
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (!this.jsInjected) {
            if (mMWebView != null) {
                mMWebView.addJavascriptInterface(new JSBridgeCommon(), "MmInjectedFunctions");
                mMWebView.addJavascriptInterface(new JSBridgeMRAID(), "MmInjectedFunctionsMraid");
                mMWebView.addJavascriptInterface(new JSBridgeInlineVideo(), "MmInjectedFunctionsInlineVideo");
                mMWebView.addJavascriptInterface(new JSBridgeMMJS(), "MmInjectedFunctionsMmjs");
                mMWebView.addJavascriptInterface(new JSBridgeVastVideo(), "MmInjectedFunctionsVast");
                mMWebView.injectExtraAPIs();
            }
            this.jsInjected = true;
        }
        this.scriptsAwaitingLoad = createScriptsAwaitingLoadList(mMWebView != null ? mMWebView.getExtraScriptToInject() : null);
        String str2 = (z ? "<meta http-equiv=\"Content-Security-Policy\" content=\"upgrade-insecure-requests\"><style>body {margin:0;padding:0;}</style>" : bodyStyling) + buildScriptStatements(this.scriptsAwaitingLoad);
        Matcher matcher = mraidJsReplacePattern.matcher(str);
        if (matcher.find(0)) {
            str = matcher.replaceAll("");
        }
        StringBuffer stringBuffer = new StringBuffer(str.length() + str2.length() + 64);
        Matcher matcher2 = htmlStartPattern.matcher(str);
        boolean find = matcher2.find(0);
        if (!find) {
            stringBuffer.append("<html>");
        }
        matcher2.usePattern(headStartPattern);
        if (matcher2.find()) {
            int end = matcher2.end(0);
            matcher2.usePattern(nonMetaPattern);
            matcher2.region(end, matcher2.regionEnd());
            if (matcher2.find()) {
                matcher2.appendReplacement(stringBuffer, "");
                stringBuffer.append(str2 + matcher2.group(0));
            }
            matcher2.appendTail(stringBuffer);
        } else {
            matcher2.usePattern(bodyStartPattern);
            if (matcher2.find()) {
                matcher2.appendReplacement(stringBuffer, "");
                stringBuffer.append("<head>" + str2 + "</head>" + matcher2.group(0));
                matcher2.appendTail(stringBuffer);
            } else if (!find) {
                stringBuffer.append("<head>" + str2 + "</head><body>" + str + "</body>");
            }
        }
        if (!find) {
            stringBuffer.append("</html>");
        }
        this.ready = false;
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public List<String> createScriptsAwaitingLoadList(String str) {
        LinkedList linkedList = new LinkedList();
        if (useActionsQueue) {
            linkedList.add("actionsQueue.js");
        }
        linkedList.add("mm.js");
        linkedList.add("mraid.js");
        if (!Utils.isEmpty(str)) {
            linkedList.add(str);
        }
        return linkedList;
    }

    static String buildScriptStatements(List<String> list) {
        StringBuilder sb = new StringBuilder();
        sb.append("<script>window.mmSdkVersion=\"");
        sb.append("6.8.1-72925a6");
        sb.append("\";</script>");
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(ProviderConfig.MCVideoAdsVersionKey, "3.0");
            jSONObject.put(TapjoyConstants.TJC_SDK_PLACEMENT, "Millennial Media Ad SDK");
            jSONObject.put("sdkVersion", "6.8.1-72925a6");
            AppInfo appInfo = MMSDK.getAppInfo();
            if (appInfo != null) {
                if (appInfo.isShareApplicationIdEnabled()) {
                    jSONObject.put(ProviderConfig.MCVideoAdsAppIdKey, EnvironmentUtils.getAppId());
                }
                if (appInfo.isShareAdvertiserIdEnabled()) {
                    AdvertisingIdInfo adInfo = EnvironmentUtils.getAdInfo();
                    jSONObject.put("ifa", EnvironmentUtils.getAdvertisingId(adInfo));
                    jSONObject.put("limitAdTracking", EnvironmentUtils.isLimitAdTrackingEnabled(adInfo));
                }
            }
            jSONObject.put("coppa", EnvironmentUtils.isCoppaEnabled());
            sb.append("<script>\nwindow.MRAID_ENV = " + jSONObject.toString(4) + "\n</script>");
        } catch (JSONException e) {
            MMLog.e(TAG, "MRAID_ENV could not be configured.", e);
        }
        for (String assetContents : list) {
            sb.append("<script>");
            sb.append(getAssetContents(assetContents));
            sb.append("</script>");
        }
        return sb.toString();
    }

    private static String getAssetContents(String str) {
        if (!scriptFilesCache.containsKey(str)) {
            Map<String, String> map = scriptFilesCache;
            map.put(str, IOUtils.readAssetContents("mmadsdk/" + str));
        }
        return scriptFilesCache.get(str);
    }

    public void invokeCallback(String str, Object... objArr) {
        if (str != null) {
            if (objArr == null) {
                objArr = new Object[1];
            }
            int i = 0;
            Object[] objArr2 = new Object[(objArr.length + 1)];
            objArr2[0] = str;
            while (i < objArr.length) {
                int i2 = i + 1;
                objArr2[i2] = objArr[i];
                i = i2;
            }
            callJavascript(MM_JS_BRIDGE_CALL_CALLBACK, objArr2);
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "No callbackId provided");
        }
    }

    public void setLogLevel(int i) {
        String str = "DEBUG";
        if (i >= 6) {
            str = "ERROR";
        } else if (i >= 4) {
            str = "INFO";
        }
        callJavascript(MM_JS_BRIDGE_SET_LOG_LEVEL, str);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    @android.annotation.TargetApi(19)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callJavascript(java.lang.String r5, java.lang.Object... r6) {
        /*
            r4 = this;
            org.json.JSONArray r0 = new org.json.JSONArray
            java.util.List r6 = java.util.Arrays.asList(r6)
            r0.<init>(r6)
            boolean r6 = r4.isJavascriptReady()     // Catch:{ JSONException -> 0x00b8 }
            if (r6 != 0) goto L_0x003f
            boolean r6 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ JSONException -> 0x00b8 }
            if (r6 == 0) goto L_0x003e
            java.lang.String r6 = com.millennialmedia.internal.JSBridge.TAG     // Catch:{ JSONException -> 0x00b8 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00b8 }
            r1.<init>()     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r2 = "jsBridge scripts are not loaded: "
            r1.append(r2)     // Catch:{ JSONException -> 0x00b8 }
            r1.append(r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r5 = "("
            r1.append(r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r5 = ","
            java.lang.String r5 = r0.join(r5)     // Catch:{ JSONException -> 0x00b8 }
            r1.append(r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r5 = ")"
            r1.append(r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r5 = r1.toString()     // Catch:{ JSONException -> 0x00b8 }
            com.millennialmedia.MMLog.d(r6, r5)     // Catch:{ JSONException -> 0x00b8 }
        L_0x003e:
            return
        L_0x003f:
            boolean r6 = com.millennialmedia.internal.JSBridge.useActionsQueue     // Catch:{ JSONException -> 0x00b8 }
            if (r6 == 0) goto L_0x0090
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00b8 }
            r6.<init>()     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r1 = "functionName"
            r6.put(r1, r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r1 = "args"
            r6.put(r1, r0)     // Catch:{ JSONException -> 0x00b8 }
            monitor-enter(r4)     // Catch:{ JSONException -> 0x00b8 }
            boolean r1 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x008d }
            if (r1 == 0) goto L_0x007b
            java.lang.String r1 = com.millennialmedia.internal.JSBridge.TAG     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r2.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r3 = "Queuing js: "
            r2.append(r3)     // Catch:{ all -> 0x008d }
            r2.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = " args: "
            r2.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x008d }
            r2.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x008d }
            com.millennialmedia.MMLog.d(r1, r5)     // Catch:{ all -> 0x008d }
        L_0x007b:
            org.json.JSONArray r5 = r4.actionsQueue     // Catch:{ all -> 0x008d }
            if (r5 != 0) goto L_0x0086
            org.json.JSONArray r5 = new org.json.JSONArray     // Catch:{ all -> 0x008d }
            r5.<init>()     // Catch:{ all -> 0x008d }
            r4.actionsQueue = r5     // Catch:{ all -> 0x008d }
        L_0x0086:
            org.json.JSONArray r5 = r4.actionsQueue     // Catch:{ all -> 0x008d }
            r5.put(r6)     // Catch:{ all -> 0x008d }
            monitor-exit(r4)     // Catch:{ all -> 0x008d }
            goto L_0x00c0
        L_0x008d:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x008d }
            throw r5     // Catch:{ JSONException -> 0x00b8 }
        L_0x0090:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00b8 }
            r6.<init>()     // Catch:{ JSONException -> 0x00b8 }
            r6.append(r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r5 = "("
            r6.append(r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r5 = ","
            java.lang.String r5 = r0.join(r5)     // Catch:{ JSONException -> 0x00b8 }
            r6.append(r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r5 = ")"
            r6.append(r5)     // Catch:{ JSONException -> 0x00b8 }
            java.lang.String r5 = r6.toString()     // Catch:{ JSONException -> 0x00b8 }
            com.millennialmedia.internal.JSBridge$2 r6 = new com.millennialmedia.internal.JSBridge$2     // Catch:{ JSONException -> 0x00b8 }
            r6.<init>(r5)     // Catch:{ JSONException -> 0x00b8 }
            com.millennialmedia.internal.utils.ThreadUtils.postOnUiThread(r6)     // Catch:{ JSONException -> 0x00b8 }
            goto L_0x00c0
        L_0x00b8:
            r5 = move-exception
            java.lang.String r6 = com.millennialmedia.internal.JSBridge.TAG
            java.lang.String r0 = "Unable to execute javascript function"
            com.millennialmedia.MMLog.e(r6, r0, r5)
        L_0x00c0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.JSBridge.callJavascript(java.lang.String, java.lang.Object[]):void");
    }

    class JSBridgeCommon {
        JSBridgeCommon() {
        }

        @JavascriptInterface
        public Boolean useActionsQueue() {
            return Boolean.valueOf(JSBridge.useActionsQueue);
        }

        @JavascriptInterface
        public String getActionsQueue() {
            synchronized (JSBridge.this) {
                if (JSBridge.this.actionsQueue == null) {
                    return null;
                }
                String jSONArray = JSBridge.this.actionsQueue.toString();
                JSONArray unused = JSBridge.this.actionsQueue = null;
                return jSONArray;
            }
        }

        @JavascriptInterface
        public void fileLoaded(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "fileLoaded: " + str);
            }
            JSBridge.this.scriptsAwaitingLoad.remove(new JSONObject(str).getString("filename"));
            if (JSBridge.this.scriptsAwaitingLoad.size() == 0) {
                if (JSBridge.this.jsBridgeListener != null) {
                    JSBridge.this.jsBridgeListener.onInjectedScriptsLoaded();
                }
                JSBridge.this.setReadyState();
            }
        }
    }

    class JSBridgeMRAID {
        JSBridgeMRAID() {
        }

        @JavascriptInterface
        public void open(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: open(" + str + ")");
            }
            if (!JSBridge.this.apiCallsEnabled) {
                JSBridge.this.throwMraidError(JSBridge.AD_NOT_CLICKED_MESSAGE, "open");
                return;
            }
            String string = new JSONObject(str).getString("url");
            if (Utils.startActivityFromUrl(string)) {
                JSBridge.this.jsBridgeListener.onAdLeftApplication();
                return;
            }
            JSBridge.this.throwMraidError(String.format(JSBridge.URL_OPEN_FAILED_MESSAGE, string), "open");
        }

        @JavascriptInterface
        public void close(String str) {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: close(" + str + ")");
            }
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    JSBridge.this.jsBridgeListener.close();
                }
            });
        }

        @JavascriptInterface
        public void unload(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: unload(" + str + ")");
            }
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    JSBridge.this.jsBridgeListener.unload();
                }
            });
        }

        @JavascriptInterface
        public void expand(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: expand(" + str + ")");
            }
            if (!JSBridge.this.apiCallsEnabled) {
                JSBridge.this.throwMraidError(JSBridge.AD_NOT_CLICKED_MESSAGE, JSBridge.EXPAND_MRAID_OP);
                return;
            }
            JSONObject jSONObject = new JSONObject(str);
            if (JSBridge.this.isInterstitial) {
                JSBridge.this.throwMraidError(JSBridge.CANNOT_EXPAND_INTERSTITIAL_MESSAGE, JSBridge.EXPAND_MRAID_OP);
                return;
            }
            DisplayMetrics displayMetrics = EnvironmentUtils.getApplicationContext().getResources().getDisplayMetrics();
            final SizableStateManager.ExpandParams expandParams = new SizableStateManager.ExpandParams();
            if (jSONObject.has("width")) {
                expandParams.width = Math.min((int) TypedValue.applyDimension(1, (float) jSONObject.getInt("width"), displayMetrics), EnvironmentUtils.getDisplayWidth());
            } else {
                expandParams.width = -1;
            }
            if (jSONObject.has("height")) {
                expandParams.height = Math.min((int) TypedValue.applyDimension(1, (float) jSONObject.getInt("height"), displayMetrics), EnvironmentUtils.getDisplayHeight());
            } else {
                expandParams.height = -1;
            }
            expandParams.orientation = JSBridge.this.requestedOrientation;
            expandParams.url = jSONObject.optString("url", null);
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    if (TextUtils.equals(JSBridge.this.currentState, JSBridge.STATE_EXPANDED) || TextUtils.equals(JSBridge.this.currentState, JSBridge.STATE_HIDDEN) || TextUtils.equals(JSBridge.this.currentState, JSBridge.STATE_LOADING)) {
                        JSBridge.this.throwMraidError(String.format(JSBridge.CANNOT_EXPAND_STATE_MESSAGE, JSBridge.this.currentState), JSBridge.EXPAND_MRAID_OP);
                    } else if (!JSBridge.this.jsBridgeListener.expand(expandParams)) {
                        JSBridge.this.throwMraidError(JSBridge.EXPAND_FAILED_MESSAGE, JSBridge.EXPAND_MRAID_OP);
                    }
                }
            });
        }

        @JavascriptInterface
        public void resize(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: resize(" + str + ")");
            }
            if (!JSBridge.this.apiCallsEnabled) {
                JSBridge.this.throwMraidError(JSBridge.AD_NOT_CLICKED_MESSAGE, JSBridge.RESIZE_MRAID_OP);
                return;
            }
            JSONObject jSONObject = new JSONObject(str);
            if (JSBridge.this.isInterstitial) {
                JSBridge.this.throwMraidError(JSBridge.CANNOT_RESIZE_INTERSTITIAL_MESSAGE, JSBridge.RESIZE_MRAID_OP);
                return;
            }
            DisplayMetrics displayMetrics = EnvironmentUtils.getApplicationContext().getResources().getDisplayMetrics();
            final SizableStateManager.ResizeParams resizeParams = new SizableStateManager.ResizeParams();
            resizeParams.width = (int) TypedValue.applyDimension(1, (float) jSONObject.getInt("width"), displayMetrics);
            resizeParams.height = (int) TypedValue.applyDimension(1, (float) jSONObject.getInt("height"), displayMetrics);
            resizeParams.offsetX = (int) TypedValue.applyDimension(1, (float) jSONObject.optInt("offsetX", 0), displayMetrics);
            resizeParams.offsetY = (int) TypedValue.applyDimension(1, (float) jSONObject.optInt("offsetY", 0), displayMetrics);
            resizeParams.customClosePosition = jSONObject.optString("customClosePosition", "top-right");
            resizeParams.allowOffScreen = jSONObject.optBoolean("allowOffscreen", true);
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    if (TextUtils.equals(JSBridge.this.currentState, JSBridge.STATE_EXPANDED) || TextUtils.equals(JSBridge.this.currentState, JSBridge.STATE_HIDDEN) || TextUtils.equals(JSBridge.this.currentState, JSBridge.STATE_LOADING)) {
                        JSBridge.this.throwMraidError(String.format(JSBridge.CANNOT_RESIZE_STATE_MESSAGE, JSBridge.this.currentState), JSBridge.RESIZE_MRAID_OP);
                    } else if (!JSBridge.this.jsBridgeListener.resize(resizeParams)) {
                        JSBridge.this.throwMraidError(JSBridge.RESIZE_FAILED_MESSAGE, JSBridge.RESIZE_MRAID_OP);
                    }
                }
            });
        }

        @JavascriptInterface
        public void useCustomClose(String str) throws JSONException {
            MMLog.w(JSBridge.TAG, "MRAID: useCustomClose is deprecated. Ignoring per spec.");
        }

        @JavascriptInterface
        public void setOrientationProperties(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: setOrientationProperties(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            boolean optBoolean = jSONObject.optBoolean("allowOrientationChange", true);
            String optString = jSONObject.optString("forceOrientation", "none");
            if ("none".equals(optString)) {
                if (optBoolean) {
                    JSBridge.this.requestedOrientation = -1;
                } else if (EnvironmentUtils.getCurrentConfigOrientation() == 2) {
                    JSBridge.this.requestedOrientation = 6;
                } else {
                    JSBridge.this.requestedOrientation = 7;
                }
            } else if (EnvironmentUtils.ORIENTATION_PORTRAIT.equals(optString)) {
                JSBridge.this.requestedOrientation = 7;
            } else if ("landscape".equals(optString)) {
                JSBridge.this.requestedOrientation = 6;
            } else {
                JSBridge.this.throwMraidError(String.format(JSBridge.INVALID_ORIENTATION_MESSAGE, optString), "setOrientationProperties");
                return;
            }
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    if (JSBridge.this.isInterstitial || JSBridge.this.currentState.equals(JSBridge.STATE_EXPANDED)) {
                        JSBridge.this.jsBridgeListener.setOrientation(JSBridge.this.requestedOrientation);
                    }
                }
            });
        }

        @JavascriptInterface
        public void storePicture(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: storePicture(" + str + ")");
            }
            if (!JSBridge.this.apiCallsEnabled) {
                JSBridge.this.throwMraidError(JSBridge.AD_NOT_CLICKED_MESSAGE, JSBridge.SUPPORTS_STORE_PICTURE);
            } else if (!EnvironmentUtils.isExternalStorageSupported()) {
                JSBridge.this.throwMraidError(JSBridge.NOT_SUPPORTED_MESSAGE, JSBridge.SUPPORTS_STORE_PICTURE);
            } else {
                String optString = new JSONObject(str).optString("url", null);
                if (TextUtils.isEmpty(optString)) {
                    JSBridge.this.throwMraidError(JSBridge.NO_PICTURE_PATH_MESSAGE, JSBridge.SUPPORTS_STORE_PICTURE);
                    return;
                }
                final MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                if (mMWebView == null) {
                    JSBridge.this.throwMraidError(JSBridge.WEBVIEW_NOT_VALID_MESSAGE, JSBridge.SUPPORTS_STORE_PICTURE);
                } else {
                    MediaUtils.savePicture(mMWebView.getContext(), optString, null, new MediaUtils.SavePictureListener() {
                        public void onPictureSaved(File file) {
                            Context context = mMWebView.getContext();
                            Utils.showToast(context, file.getName() + " stored in gallery");
                        }

                        public void onError(String str) {
                            JSBridge.this.throwMraidError(str, JSBridge.SUPPORTS_STORE_PICTURE);
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void createCalendarEvent(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: createCalendarEvent(" + str + ")");
            }
            if (!JSBridge.this.apiCallsEnabled) {
                JSBridge.this.throwMraidError(JSBridge.AD_NOT_CLICKED_MESSAGE, JSBridge.CREATE_CALENDAR_EVENT_MRAID_OP);
                return;
            }
            JSONObject jSONObject = new JSONObject(str).getJSONObject("parameters");
            if (jSONObject == null) {
                JSBridge.this.throwMraidError(JSBridge.PARAMETERS_NOT_PROVIDED_MESSAGE, JSBridge.CREATE_CALENDAR_EVENT_MRAID_OP);
                return;
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView == null) {
                JSBridge.this.throwMraidError(JSBridge.WEBVIEW_NOT_VALID_MESSAGE, JSBridge.CREATE_CALENDAR_EVENT_MRAID_OP);
            } else {
                CalendarUtils.addEvent(mMWebView.getContext(), jSONObject, new CalendarUtils.CalendarListener() {
                    public void onUIDisplayed() {
                        if (MMLog.isDebugEnabled()) {
                            MMLog.d(JSBridge.TAG, "Calendar activity started");
                        }
                    }

                    public void onError(String str) {
                        JSBridge.this.throwMraidError(str, JSBridge.CREATE_CALENDAR_EVENT_MRAID_OP);
                    }
                });
            }
        }

        @JavascriptInterface
        public void playVideo(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MRAID: playVideo(" + str + ")");
            }
            if (!JSBridge.this.apiCallsEnabled) {
                JSBridge.this.throwMraidError(JSBridge.AD_NOT_CLICKED_MESSAGE, JSBridge.PLAY_VIDEO_MRAID_OP);
                return;
            }
            String optString = new JSONObject(str).optString("url", null);
            if (TextUtils.isEmpty(optString)) {
                JSBridge.this.throwMraidError(JSBridge.NO_VIDEO_PATH_MESSAGE, JSBridge.PLAY_VIDEO_MRAID_OP);
                return;
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView == null) {
                JSBridge.this.throwMraidError(JSBridge.WEBVIEW_NOT_VALID_MESSAGE, JSBridge.PLAY_VIDEO_MRAID_OP);
            } else {
                MediaUtils.startVideoPlayer(mMWebView.getContext(), optString, new MediaUtils.PlayVideoListener() {
                    public void onVideoStarted(Uri uri) {
                        if (MMLog.isDebugEnabled()) {
                            String access$400 = JSBridge.TAG;
                            MMLog.d(access$400, "Video activity started for <" + uri.toString() + ">");
                        }
                    }

                    public void onError(String str) {
                        JSBridge.this.throwMraidError(str, JSBridge.PLAY_VIDEO_MRAID_OP);
                    }
                });
            }
        }
    }

    class JSBridgeMMJS {
        JSBridgeMMJS() {
        }

        @JavascriptInterface
        public void addCalendarEvent(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: addCalendarEvent(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            final String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("options");
            if (optJSONObject == null) {
                MMLog.e(JSBridge.TAG, "No options provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView == null) {
                MMLog.e(JSBridge.TAG, JSBridge.WEBVIEW_NOT_VALID_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            CalendarUtils.addEvent(mMWebView.getContext(), optJSONObject, new CalendarUtils.CalendarListener() {
                public void onUIDisplayed() {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(JSBridge.TAG, "Calendar activity started");
                    }
                    JSBridge.this.jsBridgeListener.onAdLeftApplication();
                    JSBridge.this.invokeCallback(optString, true);
                }

                public void onError(String str) {
                    MMLog.e(JSBridge.TAG, str);
                    JSBridge.this.invokeCallback(optString, false);
                }
            });
        }

        @JavascriptInterface
        public void openInBrowser(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: openInBrowser(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            boolean startActivityFromUrl = Utils.startActivityFromUrl(jSONObject.optString("url", null));
            if (startActivityFromUrl) {
                JSBridge.this.jsBridgeListener.onAdLeftApplication();
            }
            JSBridge.this.invokeCallback(optString, Boolean.valueOf(startActivityFromUrl));
        }

        @JavascriptInterface
        public void isSourceTypeAvailable(String str) throws JSONException {
            boolean z;
            if (MMLog.isDebugEnabled()) {
                MMLog.d(JSBridge.TAG, "MMJS: isSourceTypeAvailable(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            String optString2 = jSONObject.optString("sourceType", null);
            if (optString2 == null) {
                MMLog.e(JSBridge.TAG, "sourceType was not provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            if ("Photo Library".equals(optString2)) {
                z = MediaUtils.isPictureChooserAvailable();
            } else {
                EnvironmentUtils.AvailableCameras availableCameras = EnvironmentUtils.getAvailableCameras();
                if (availableCameras != null) {
                    if ("Camera".equals(optString2)) {
                        if (availableCameras.frontCamera || availableCameras.backCamera) {
                            z = true;
                        }
                    } else if ("Rear Camera".equals(optString2)) {
                        z = availableCameras.backCamera;
                    } else if ("Front Camera".equals(optString2)) {
                        z = availableCameras.frontCamera;
                    }
                }
                z = false;
            }
            JSBridge.this.invokeCallback(optString, Boolean.valueOf(z));
        }

        @JavascriptInterface
        public void getAvailableSourceTypes(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: getAvailableSourceTypes(" + str + ")");
            }
            String optString = new JSONObject(str).optString("callbackId", null);
            JSONArray jSONArray = new JSONArray();
            EnvironmentUtils.AvailableCameras availableCameras = EnvironmentUtils.getAvailableCameras();
            if (availableCameras != null) {
                if (availableCameras.backCamera) {
                    jSONArray.put("Rear Camera");
                }
                if (availableCameras.frontCamera) {
                    jSONArray.put("Front Camera");
                }
                if (availableCameras.frontCamera || availableCameras.backCamera) {
                    jSONArray.put("Camera");
                }
            }
            if (MediaUtils.isPictureChooserAvailable()) {
                jSONArray.put("Photo Library");
            }
            JSBridge.this.invokeCallback(optString, jSONArray);
        }

        @JavascriptInterface
        public void getPictureFromPhotoLibrary(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: getPictureFromPhotoLibrary(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            final String optString = jSONObject.optString("callbackId", null);
            if (!EnvironmentUtils.isExternalStorageReadable()) {
                MMLog.e(JSBridge.TAG, "Cannot read external storage");
                JSBridge.this.invokeCallback(optString, null);
            } else if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, null);
            } else {
                JSONObject optJSONObject = jSONObject.optJSONObject("size");
                if (optJSONObject == null) {
                    MMLog.e(JSBridge.TAG, "No size parameters provided");
                    JSBridge.this.invokeCallback(optString, null);
                    return;
                }
                DisplayMetrics displayMetrics = EnvironmentUtils.getApplicationContext().getResources().getDisplayMetrics();
                final int applyDimension = (int) TypedValue.applyDimension(1, (float) optJSONObject.optInt("maxWidth", 0), displayMetrics);
                final int applyDimension2 = (int) TypedValue.applyDimension(1, (float) optJSONObject.optInt("maxHeight", 0), displayMetrics);
                final boolean optBoolean = optJSONObject.optBoolean("maintainAspectRatio", true);
                if (applyDimension <= 0 || applyDimension2 <= 0) {
                    MMLog.e(JSBridge.TAG, "maxWidth and maxHeight must be > 0");
                    JSBridge.this.invokeCallback(optString, null);
                    return;
                }
                final MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                if (mMWebView == null) {
                    MMLog.e(JSBridge.TAG, JSBridge.WEBVIEW_NOT_VALID_MESSAGE);
                    JSBridge.this.invokeCallback(optString, null);
                    return;
                }
                MediaUtils.getPhotoFromGallery(mMWebView.getContext(), new MediaUtils.PhotoListener() {
                    public void onPhoto(Uri uri) {
                        String str;
                        String mimeTypeFromUri = MediaUtils.getMimeTypeFromUri(mMWebView.getContext(), uri);
                        Bitmap scaledBitmapFromUri = MediaUtils.getScaledBitmapFromUri(mMWebView.getContext(), uri, applyDimension, applyDimension2, optBoolean, true);
                        if (scaledBitmapFromUri != null) {
                            str = MediaUtils.base64EncodeBitmap(scaledBitmapFromUri, mimeTypeFromUri);
                            scaledBitmapFromUri.recycle();
                        } else {
                            str = null;
                        }
                        JSBridge.this.invokeCallback(optString, str);
                    }

                    public void onError(String str) {
                        MMLog.e(JSBridge.TAG, str);
                        JSBridge.this.invokeCallback(optString, null);
                    }
                });
            }
        }

        @JavascriptInterface
        public void openCamera(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: openCamera(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            final String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, null);
                return;
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("size");
            if (optJSONObject == null) {
                MMLog.e(JSBridge.TAG, "No size parameters provided");
                JSBridge.this.invokeCallback(optString, null);
                return;
            }
            DisplayMetrics displayMetrics = EnvironmentUtils.getApplicationContext().getResources().getDisplayMetrics();
            final int applyDimension = (int) TypedValue.applyDimension(1, (float) optJSONObject.optInt("maxWidth", 0), displayMetrics);
            final int applyDimension2 = (int) TypedValue.applyDimension(1, (float) optJSONObject.optInt("maxHeight", 0), displayMetrics);
            final boolean optBoolean = optJSONObject.optBoolean("maintainAspectRatio", true);
            if (applyDimension <= 0 || applyDimension2 <= 0) {
                MMLog.e(JSBridge.TAG, "maxWidth and maxHeight must be > 0");
                JSBridge.this.invokeCallback(optString, null);
                return;
            }
            final MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView == null) {
                MMLog.e(JSBridge.TAG, JSBridge.WEBVIEW_NOT_VALID_MESSAGE);
                JSBridge.this.invokeCallback(optString, null);
                return;
            }
            MediaUtils.getPhotoFromCamera(mMWebView.getContext(), new MediaUtils.PhotoListener() {
                public void onPhoto(Uri uri) {
                    String str;
                    String mimeTypeFromUri = MediaUtils.getMimeTypeFromUri(mMWebView.getContext(), uri);
                    Bitmap scaledBitmapFromUri = MediaUtils.getScaledBitmapFromUri(mMWebView.getContext(), uri, applyDimension, applyDimension2, optBoolean, true);
                    if (scaledBitmapFromUri != null) {
                        str = MediaUtils.base64EncodeBitmap(scaledBitmapFromUri, mimeTypeFromUri);
                        scaledBitmapFromUri.recycle();
                    } else {
                        str = null;
                    }
                    JSBridge.this.invokeCallback(optString, str);
                }

                public void onError(String str) {
                    MMLog.e(JSBridge.TAG, str);
                    JSBridge.this.invokeCallback(optString, null);
                }
            });
        }

        @JavascriptInterface
        public void savePictureToPhotoLibrary(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: savePictureToPhotoLibrary(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            final String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            String optString2 = jSONObject.optString(TJAdUnitConstants.String.USAGE_TRACKER_NAME, null);
            String optString3 = jSONObject.optString("url", null);
            if (TextUtils.isEmpty(optString3)) {
                MMLog.e(JSBridge.TAG, "No path specified for photo");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            final MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView == null) {
                MMLog.e(JSBridge.TAG, JSBridge.WEBVIEW_NOT_VALID_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            MediaUtils.savePicture(mMWebView.getContext(), optString3, optString2, new MediaUtils.SavePictureListener() {
                public void onPictureSaved(File file) {
                    Context context = mMWebView.getContext();
                    Utils.showToast(context, file.getName() + " stored in gallery");
                    JSBridge.this.invokeCallback(optString, true);
                }

                public void onError(String str) {
                    MMLog.e(JSBridge.TAG, str);
                    JSBridge.this.invokeCallback(optString, false);
                }
            });
        }

        @JavascriptInterface
        public void call(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: call(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            String optString2 = jSONObject.optString("number", null);
            if (optString2 == null) {
                MMLog.e(JSBridge.TAG, "No number provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            boolean startActivity = Utils.startActivity(EnvironmentUtils.getApplicationContext(), new Intent("android.intent.action.VIEW", Uri.parse("tel:" + optString2)));
            if (startActivity) {
                JSBridge.this.jsBridgeListener.onAdLeftApplication();
            }
            JSBridge.this.invokeCallback(optString, Boolean.valueOf(startActivity));
        }

        @JavascriptInterface
        public void sms(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: sms(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("recipients");
            if (optJSONArray == null || optJSONArray.length() == 0) {
                MMLog.e(JSBridge.TAG, "No recipients provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            String optString2 = jSONObject.optString("message", null);
            if (optString2 == null) {
                MMLog.e(JSBridge.TAG, "No message provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            String join = TextUtils.join(",", JSONUtils.convertToStringArray(optJSONArray));
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("sms:" + join));
            intent.putExtra("sms_body", optString2);
            boolean startActivity = Utils.startActivity(EnvironmentUtils.getApplicationContext(), intent);
            if (startActivity) {
                JSBridge.this.jsBridgeListener.onAdLeftApplication();
            }
            JSBridge.this.invokeCallback(optString, Boolean.valueOf(startActivity));
        }

        @JavascriptInterface
        public void email(String str) throws JSONException {
            JSONArray jSONArray;
            JSONArray jSONArray2;
            JSONArray jSONArray3;
            if (MMLog.isDebugEnabled()) {
                MMLog.d(JSBridge.TAG, "MMJS: email(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("recipients");
            if (optJSONObject != null) {
                jSONArray2 = optJSONObject.optJSONArray("to");
                jSONArray = optJSONObject.optJSONArray("cc");
                jSONArray3 = optJSONObject.optJSONArray("bcc");
            } else {
                jSONArray3 = null;
                jSONArray2 = null;
                jSONArray = null;
            }
            String optString2 = jSONObject.optString("subject", null);
            if (optString2 == null) {
                MMLog.e(JSBridge.TAG, "No subject provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            String optString3 = jSONObject.optString("message", null);
            if (optString3 == null) {
                MMLog.e(JSBridge.TAG, "No message provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType(jSONObject.optString("type", "text/plain"));
            if (jSONArray2 != null) {
                intent.putExtra("android.intent.extra.EMAIL", JSONUtils.convertToStringArray(jSONArray2));
            }
            if (jSONArray != null) {
                intent.putExtra("android.intent.extra.CC", JSONUtils.convertToStringArray(jSONArray));
            }
            if (jSONArray3 != null) {
                intent.putExtra("android.intent.extra.BCC", JSONUtils.convertToStringArray(jSONArray3));
            }
            intent.putExtra("android.intent.extra.SUBJECT", optString2);
            intent.putExtra("android.intent.extra.TEXT", optString3);
            boolean startActivity = Utils.startActivity(EnvironmentUtils.getApplicationContext(), intent);
            if (startActivity) {
                JSBridge.this.jsBridgeListener.onAdLeftApplication();
            }
            JSBridge.this.invokeCallback(optString, Boolean.valueOf(startActivity));
        }

        @JavascriptInterface
        public void vibrate(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: vibrate(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            final String optString = jSONObject.optString("onStartCallbackId", null);
            final String optString2 = jSONObject.optString("onFinishCallbackId", null);
            JSONArray optJSONArray = jSONObject.optJSONArray("pattern");
            int i = 0;
            if (optJSONArray == null) {
                MMLog.e(JSBridge.TAG, "No pattern provided");
                JSBridge.this.invokeCallback(optString2, false);
                return;
            }
            long[] jArr = new long[(optJSONArray.length() + 1)];
            jArr[0] = 0;
            while (i < optJSONArray.length()) {
                int i2 = i + 1;
                jArr[i2] = optJSONArray.getLong(i);
                i = i2;
            }
            Utils.vibrate(jArr, -1, new Utils.VibrateListener() {
                public void onStarted() {
                    JSBridge.this.invokeCallback(optString, new Object[0]);
                }

                public void onFinished() {
                    JSBridge.this.invokeCallback(optString2, true);
                }

                public void onError() {
                    JSBridge.this.invokeCallback(optString2, false);
                }
            });
        }

        @JavascriptInterface
        public void isSchemeAvailable(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: isSchemeAvailable(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            String optString2 = jSONObject.optString(TJAdUnitConstants.String.USAGE_TRACKER_NAME, null);
            if (optString2 == null) {
                MMLog.e(JSBridge.TAG, "name was not provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            boolean isSchemeAvailable = Utils.isSchemeAvailable(optString2);
            JSBridge.this.invokeCallback(optString, Boolean.valueOf(isSchemeAvailable));
        }

        @JavascriptInterface
        public void isPackageAvailable(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: isPackageAvailable(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            String optString2 = jSONObject.optString(TJAdUnitConstants.String.USAGE_TRACKER_NAME, null);
            if (optString2 == null) {
                MMLog.e(JSBridge.TAG, "name was not provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            boolean isPackageAvailable = Utils.isPackageAvailable(optString2);
            JSBridge.this.invokeCallback(optString, Boolean.valueOf(isPackageAvailable));
        }

        @JavascriptInterface
        @SuppressLint({"DefaultLocale"})
        public void openMap(String str) throws JSONException {
            String str2;
            if (MMLog.isDebugEnabled()) {
                MMLog.d(JSBridge.TAG, "MMJS: openMap(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            if (jSONObject.has("address")) {
                String optString2 = jSONObject.optString("address");
                try {
                    str2 = "geo:0,0?q=" + URLEncoder.encode(optString2, "utf-8");
                } catch (UnsupportedEncodingException unused) {
                    JSBridge.this.invokeCallback(optString, false, "Unable to encode address");
                    return;
                }
            } else if (!jSONObject.has("longitude") || !jSONObject.has("latitude")) {
                JSBridge.this.invokeCallback(optString, false, "address or latitude and longitude must be specified");
                return;
            } else {
                Double valueOf = Double.valueOf(jSONObject.optDouble("latitude"));
                Double valueOf2 = Double.valueOf(jSONObject.optDouble("longitude"));
                str2 = String.format("geo:%f,%f?q=%f,%f", valueOf, valueOf2, valueOf, valueOf2);
            }
            if (Utils.startActivityFromUrl(str2)) {
                JSBridge.this.jsBridgeListener.onAdLeftApplication();
                JSBridge.this.invokeCallback(optString, true);
                return;
            }
            JSBridge.this.invokeCallback(optString, false, "Unable to open map");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException} */
        @JavascriptInterface
        public void location(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: location(" + str + ")");
            }
            String optString = new JSONObject(str).optString("callbackId", null);
            Location location = EnvironmentUtils.getLocation();
            if (location == null) {
                JSBridge.this.invokeCallback(optString, null);
                return;
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("latitude", location.getLatitude());
            jSONObject.put("longitude", location.getLongitude());
            jSONObject.put("altitude", location.getAltitude());
            jSONObject.put("accuracy", (double) location.getAccuracy());
            jSONObject.put("altitudeAccuracy", 0.0d);
            jSONObject.put("heading", (double) location.getBearing());
            jSONObject.put("speed", (double) location.getSpeed());
            JSBridge.this.invokeCallback(optString, jSONObject);
        }

        @JavascriptInterface
        public void openAppStore(String str) throws JSONException {
            String str2;
            if (MMLog.isDebugEnabled()) {
                MMLog.d(JSBridge.TAG, "MMJS: openAppStore(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("callbackId", null);
            if (!JSBridge.this.apiCallsEnabled) {
                MMLog.e(JSBridge.TAG, JSBridge.AD_NOT_CLICKED_MESSAGE);
                JSBridge.this.invokeCallback(optString, false, JSBridge.AD_NOT_CLICKED_MESSAGE);
                return;
            }
            String optString2 = jSONObject.optString(ProviderConfig.MCVideoAdsAppIdKey, null);
            if (optString2 == null) {
                JSBridge.this.invokeCallback(optString, false, "appId was not provided");
                return;
            }
            if (Build.MANUFACTURER.equals(MCInAppPurchases.AMAZON_NAME)) {
                str2 = "amzn://apps/android?p=" + optString2;
            } else {
                str2 = "market://details?id=" + optString2;
            }
            if (Utils.startActivityFromUrl(str2)) {
                JSBridge.this.jsBridgeListener.onAdLeftApplication();
                JSBridge.this.invokeCallback(optString, true);
                return;
            }
            JSBridge.this.invokeCallback(optString, false, "Unable to open app store");
        }

        @JavascriptInterface
        public void httpGet(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "MMJS: httpGet(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            final String optString = jSONObject.optString("callbackId", null);
            final String optString2 = jSONObject.optString("url", null);
            if (TextUtils.isEmpty(optString2)) {
                MMLog.e(JSBridge.TAG, "url was not provided");
                JSBridge.this.invokeCallback(optString, false);
                return;
            }
            final int optInt = jSONObject.optInt("timeout", JSBridge.HTTP_GET_TIMEOUT);
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    HttpUtils.Response contentFromGetRequest = HttpUtils.getContentFromGetRequest(optString2, optInt);
                    if (contentFromGetRequest.code == 200) {
                        JSBridge.this.invokeCallback(optString, contentFromGetRequest.content);
                        return;
                    }
                    JSBridge jSBridge = JSBridge.this;
                    String str = optString;
                    jSBridge.invokeCallback(str, false, "http request failed with response code: " + contentFromGetRequest.code);
                }
            });
        }
    }

    class JSBridgeInlineVideo {
        /* access modifiers changed from: private */
        public FiredEvents firedEvents = new FiredEvents();
        /* access modifiers changed from: private */
        public Map<Object, InlineWebVideoView> inlineWebVideoViews = new HashMap();

        public JSBridgeInlineVideo() {
        }

        @JavascriptInterface
        public void insert(String str) throws JSONException {
            String str2 = str;
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: insert(" + str2 + ")");
            }
            JSONObject jSONObject = new JSONObject(str2);
            final String string = jSONObject.getString("url");
            final int i = jSONObject.getInt("width");
            final int i2 = jSONObject.getInt("height");
            final int i3 = jSONObject.getInt("x");
            final int i4 = jSONObject.getInt("y");
            final boolean optBoolean = jSONObject.optBoolean("autoPlay", false);
            final boolean optBoolean2 = jSONObject.optBoolean("showMediaControls", false);
            final boolean optBoolean3 = jSONObject.optBoolean("showExpandControls", false);
            final String optString = jSONObject.optString("placeholder", null);
            final boolean optBoolean4 = jSONObject.optBoolean("muted", false);
            final int optInt = jSONObject.optInt("timeUpdateInterval", -1);
            final String optString2 = jSONObject.optString("callbackId");
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    final MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                    if (mMWebView != null) {
                        InlineWebVideoView inlineWebVideoView = new InlineWebVideoView(mMWebView.getContext(), optBoolean, optBoolean4, optBoolean2, optBoolean3, optInt, optString2, new InlineWebVideoView.InlineWebVideoViewListener() {
                            public void onClicked() {
                                mMWebView.onNotifyClicked();
                            }
                        });
                        JSBridgeInlineVideo.this.inlineWebVideoViews.put(inlineWebVideoView.getTag(), inlineWebVideoView);
                        DisplayMetrics displayMetrics = mMWebView.getContext().getResources().getDisplayMetrics();
                        inlineWebVideoView.setAnchorView(mMWebView, JSBridgeInlineVideo.this.toPixels(displayMetrics, (float) i3), JSBridgeInlineVideo.this.toPixels(displayMetrics, (float) i4), JSBridgeInlineVideo.this.toPixels(displayMetrics, (float) i), JSBridgeInlineVideo.this.toPixels(displayMetrics, (float) i2), new InlineWebVideoView.InlineWebVideoViewAttachListener() {
                            public void attachSucceeded(InlineWebVideoView inlineWebVideoView) {
                                JSBridgeInlineVideo.this.inlineWebVideoViews.remove(inlineWebVideoView.getTag());
                            }

                            public void attachFailed(InlineWebVideoView inlineWebVideoView) {
                                JSBridgeInlineVideo.this.inlineWebVideoViews.remove(inlineWebVideoView.getTag());
                            }
                        });
                        if (optString != null) {
                            inlineWebVideoView.setPlaceholder(Uri.parse(optString));
                        }
                        inlineWebVideoView.setVideoURI(Uri.parse(string), JSBridgeInlineVideo.this.firedEvents);
                    }
                }
            });
        }

        @JavascriptInterface
        public void play(String str) throws JSONException {
            InlineWebVideoView inlineWebVideoView;
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: play(" + str + ")");
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView != null && (inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(new JSONObject(str).getString(JSBridge.VIDEO_ID_KEY))) != null) {
                inlineWebVideoView.start();
            }
        }

        @JavascriptInterface
        public void stop(String str) throws JSONException {
            InlineWebVideoView inlineWebVideoView;
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: stop(" + str + ")");
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView != null && (inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(new JSONObject(str).getString(JSBridge.VIDEO_ID_KEY))) != null) {
                inlineWebVideoView.stop();
            }
        }

        @JavascriptInterface
        public void pause(String str) throws JSONException {
            InlineWebVideoView inlineWebVideoView;
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: pause(" + str + ")");
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView != null && (inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(new JSONObject(str).getString(JSBridge.VIDEO_ID_KEY))) != null) {
                inlineWebVideoView.pause();
            }
        }

        @JavascriptInterface
        public void seek(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: seek(" + str + ")");
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView != null) {
                JSONObject jSONObject = new JSONObject(str);
                InlineWebVideoView inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(jSONObject.getString(JSBridge.VIDEO_ID_KEY));
                if (inlineWebVideoView != null) {
                    inlineWebVideoView.seekTo(jSONObject.getInt("time"));
                }
            }
        }

        @JavascriptInterface
        public void setMuted(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: setMuted(" + str + ")");
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView != null) {
                JSONObject jSONObject = new JSONObject(str);
                InlineWebVideoView inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(jSONObject.getString(JSBridge.VIDEO_ID_KEY));
                if (inlineWebVideoView == null) {
                    return;
                }
                if (jSONObject.getBoolean("mute")) {
                    inlineWebVideoView.mute();
                } else {
                    inlineWebVideoView.unmute();
                }
            }
        }

        @JavascriptInterface
        public void updateVideoURL(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: updateVideoURL(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            final String string = jSONObject.getString(JSBridge.VIDEO_ID_KEY);
            final String string2 = jSONObject.getString("url");
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineWebVideoView inlineWebVideoView;
                    MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                    if (mMWebView != null && (inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(string)) != null) {
                        inlineWebVideoView.setVideoURI(Uri.parse(string2), JSBridgeInlineVideo.this.firedEvents);
                    }
                }
            });
        }

        @JavascriptInterface
        public void reposition(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: reposition(" + str + ")");
            }
            JSONObject jSONObject = new JSONObject(str);
            final String string = jSONObject.getString(JSBridge.VIDEO_ID_KEY);
            final int i = jSONObject.getInt("width");
            final int i2 = jSONObject.getInt("height");
            final int i3 = jSONObject.getInt("x");
            final int i4 = jSONObject.getInt("y");
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                    if (mMWebView != null) {
                        DisplayMetrics displayMetrics = mMWebView.getContext().getResources().getDisplayMetrics();
                        InlineWebVideoView inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(string);
                        if (inlineWebVideoView != null) {
                            inlineWebVideoView.reposition(JSBridgeInlineVideo.this.toPixels(displayMetrics, (float) i3), JSBridgeInlineVideo.this.toPixels(displayMetrics, (float) i4), JSBridgeInlineVideo.this.toPixels(displayMetrics, (float) i), JSBridgeInlineVideo.this.toPixels(displayMetrics, (float) i2));
                        }
                    }
                }
            });
        }

        @JavascriptInterface
        public void remove(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: remove(" + str + ")");
            }
            final String string = new JSONObject(str).getString(JSBridge.VIDEO_ID_KEY);
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineWebVideoView inlineWebVideoView;
                    MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                    if (mMWebView != null && (inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(string)) != null) {
                        inlineWebVideoView.remove();
                    }
                }
            });
        }

        @JavascriptInterface
        public void expandToFullScreen(String str) throws JSONException {
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: expandToFullScreen(" + str + ")");
            }
            final String string = new JSONObject(str).getString(JSBridge.VIDEO_ID_KEY);
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineWebVideoView inlineWebVideoView;
                    MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                    if (mMWebView != null && (inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(string)) != null) {
                        inlineWebVideoView.expandToFullScreen();
                    }
                }
            });
        }

        @JavascriptInterface
        public void triggerTimeUpdate(String str) throws JSONException {
            InlineWebVideoView inlineWebVideoView;
            if (MMLog.isDebugEnabled()) {
                String access$400 = JSBridge.TAG;
                MMLog.d(access$400, "InlineVideo: triggerTimeUpdate(" + str + ")");
            }
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView != null && (inlineWebVideoView = (InlineWebVideoView) mMWebView.findViewWithTag(new JSONObject(str).getString(JSBridge.VIDEO_ID_KEY))) != null) {
                inlineWebVideoView.triggerTimeUpdate();
            }
        }

        /* access modifiers changed from: private */
        public int toPixels(DisplayMetrics displayMetrics, float f) {
            return (int) TypedValue.applyDimension(1, f, displayMetrics);
        }
    }

    class JSBridgeVastVideo {
        JSBridgeVastVideo() {
        }

        @JavascriptInterface
        public void play(String str) {
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView instanceof VASTVideoView.VASTVideoWebView) {
                ((VASTVideoView.VASTVideoWebView) mMWebView).play();
            } else {
                MMLog.e(JSBridge.TAG, "Play cannot be called on a WebView that is not part of a VAST Video creative.");
            }
        }

        @JavascriptInterface
        public void pause(String str) {
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView instanceof VASTVideoView.VASTVideoWebView) {
                ((VASTVideoView.VASTVideoWebView) mMWebView).pause();
            } else {
                MMLog.e(JSBridge.TAG, "Pause cannot be called on a WebView that is not part of a VAST Video creative.");
            }
        }

        @JavascriptInterface
        public void close(String str) {
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView instanceof VASTVideoView.VASTVideoWebView) {
                ((VASTVideoView.VASTVideoWebView) mMWebView).close();
            } else {
                MMLog.e(JSBridge.TAG, "Close cannot be called on a WebView that is not part of a VAST Video creative.");
            }
        }

        @JavascriptInterface
        public void skip(String str) {
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView instanceof VASTVideoView.VASTVideoWebView) {
                ((VASTVideoView.VASTVideoWebView) mMWebView).skip();
            }
        }

        @JavascriptInterface
        public void restart(String str) {
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView instanceof VASTVideoView.VASTVideoWebView) {
                ((VASTVideoView.VASTVideoWebView) mMWebView).restart();
            } else {
                MMLog.e(JSBridge.TAG, "Restart cannot be called on a WebView that is not part of a VAST Video creative.");
            }
        }

        @JavascriptInterface
        public void seek(String str) throws JSONException {
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView instanceof VASTVideoView.VASTVideoWebView) {
                ((VASTVideoView.VASTVideoWebView) mMWebView).seek(new JSONObject(str).getInt("seekTime"));
                return;
            }
            MMLog.e(JSBridge.TAG, "Seek cannot be called on a WebView that is not part of a VAST Video creative.");
        }

        @JavascriptInterface
        public void triggerTimeUpdate(String str) {
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView instanceof VASTVideoView.VASTVideoWebView) {
                ((VASTVideoView.VASTVideoWebView) mMWebView).triggerTimeUpdate();
            } else {
                MMLog.e(JSBridge.TAG, "TriggerTimeUpdate can't be called on a WebView that is not part of a VAST Video creative.");
            }
        }

        @JavascriptInterface
        public void setTimeInterval(String str) throws JSONException {
            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
            if (mMWebView instanceof VASTVideoView.VASTVideoWebView) {
                ((VASTVideoView.VASTVideoWebView) mMWebView).setTimeInterval(new JSONObject(str).optInt("timeInterval", -1));
                return;
            }
            MMLog.e(JSBridge.TAG, "SetTimeInterval can't be called on a WebView that is not part of a VAST Video creative.");
        }
    }

    public void enableApiCalls() {
        this.apiCallsEnabled = true;
    }

    public void setTwoPartExpand() {
        this.isTwoPartExpand = true;
    }

    public void setStateResized() {
        setState(STATE_RESIZED);
    }

    public void setStateUnresized() {
        setState(STATE_DEFAULT);
    }

    public void setStateExpanded() {
        if (this.isInterstitial) {
            setState(STATE_DEFAULT);
        } else {
            setState(STATE_EXPANDED);
        }
    }

    public void setStateCollapsed() {
        if (this.isInterstitial) {
            setState(STATE_HIDDEN);
        } else {
            setState(STATE_DEFAULT);
        }
    }

    public void setStateResizing() {
        this.isResizing = true;
    }

    /* access modifiers changed from: package-private */
    public void setState(final String str) {
        if (this.ready) {
            this.isResizing = false;
            if (!TextUtils.equals(str, this.currentState) || TextUtils.equals(str, STATE_RESIZED)) {
                this.currentState = str;
                ThreadUtils.postOnUiThread(new Runnable() {
                    public void run() {
                        MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                        if (mMWebView != null) {
                            JSONObject jsonCurrentPosition = JSBridge.this.getJsonCurrentPosition(mMWebView);
                            JSBridge.this.callJavascript(JSBridge.JS_SET_STATE, str, jsonCurrentPosition);
                        }
                    }
                });
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setViewable(boolean z) {
        if (z != this.isViewable) {
            this.isViewable = z;
            if (this.ready) {
                callJavascript(JS_SET_VIEWABLE, Boolean.valueOf(z));
                return;
            }
            setReadyState();
        }
    }

    /* access modifiers changed from: package-private */
    public JSONObject getJsonCurrentPosition(MMWebView mMWebView) {
        Rect viewDimensionsRelativeToContent = ViewUtils.getViewDimensionsRelativeToContent(mMWebView, null);
        if (viewDimensionsRelativeToContent == null) {
            return null;
        }
        ViewUtils.convertPixelsToDips(viewDimensionsRelativeToContent);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("x", viewDimensionsRelativeToContent.left);
            jSONObject.put("y", viewDimensionsRelativeToContent.top);
            jSONObject.put("width", viewDimensionsRelativeToContent.width());
            jSONObject.put("height", viewDimensionsRelativeToContent.height());
        } catch (JSONException unused) {
            MMLog.e(TAG, "Error creating json object");
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public void setScrolledPosition(MMWebView mMWebView) {
        this.scrollIdleTimeout = System.currentTimeMillis() + 450;
        if (this.scrollThrottling.compareAndSet(false, true)) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    long j = 0;
                    do {
                        try {
                            Thread.sleep(100);
                            MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                            if (mMWebView == null) {
                                break;
                            } else if (JSBridge.this.scrollIdleTimeout > j) {
                                j = JSBridge.this.scrollIdleTimeout;
                                JSBridge.this.setCurrentPosition(mMWebView);
                            }
                        } catch (InterruptedException unused) {
                        }
                    } while (System.currentTimeMillis() < JSBridge.this.scrollIdleTimeout);
                    JSBridge.this.scrollThrottling.set(false);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void setCurrentPosition(final MMWebView mMWebView) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                JSONObject jsonCurrentPosition = JSBridge.this.getJsonCurrentPosition(mMWebView);
                if (jsonCurrentPosition != null) {
                    if (!JSBridge.this.ready) {
                        int optInt = jsonCurrentPosition.optInt("width", 0);
                        int optInt2 = jsonCurrentPosition.optInt("height", 0);
                        if (optInt > 0 && optInt2 > 0) {
                            JSBridge.this.hasSize = true;
                            JSBridge.this.setReadyState();
                        }
                    } else if (!JSBridge.this.isResizing) {
                        try {
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("currentPosition", jsonCurrentPosition);
                            JSBridge.this.callJavascript(JSBridge.JS_SET_POSITIONS, jSONObject);
                        } catch (JSONException unused) {
                            MMLog.e(JSBridge.TAG, "Error creating json object in setCurrentPosition");
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void throwMraidError(String str, String str2) {
        String str3 = TAG;
        MMLog.e(str3, "MRAID error - action: " + str2 + " message: " + str);
        callJavascript(JS_THROW_MRAID_ERROR, str, str2);
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"SwitchIntDef"})
    public void sendPositions(MMWebView mMWebView) {
        Activity activityForView;
        boolean z;
        if (this.ready && (activityForView = ViewUtils.getActivityForView(mMWebView)) != null) {
            float displayDensity = EnvironmentUtils.getDisplayDensity();
            int displayWidth = (int) (((float) EnvironmentUtils.getDisplayWidth()) / displayDensity);
            int displayHeight = (int) (((float) EnvironmentUtils.getDisplayHeight()) / displayDensity);
            Rect contentDimensions = ViewUtils.getContentDimensions(mMWebView, null);
            try {
                JSONObject jsonCurrentPosition = getJsonCurrentPosition(mMWebView);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("width", displayWidth);
                jSONObject.put("height", displayHeight);
                JSONObject jSONObject2 = new JSONObject();
                if (contentDimensions != null) {
                    ViewUtils.convertPixelsToDips(contentDimensions);
                    jSONObject2.put("width", contentDimensions.width());
                    jSONObject2.put("height", contentDimensions.height());
                }
                int requestedOrientation2 = activityForView.getRequestedOrientation();
                if (!(requestedOrientation2 == -1 || requestedOrientation2 == 4 || requestedOrientation2 == 10)) {
                    switch (requestedOrientation2) {
                        case 6:
                        case 7:
                            break;
                        default:
                            z = true;
                            break;
                    }
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("currentPosition", jsonCurrentPosition);
                    jSONObject3.put("screenSize", jSONObject);
                    jSONObject3.put("maxSize", jSONObject2);
                    jSONObject3.put("currentAppOrientation", EnvironmentUtils.getCurrentConfigOrientationString());
                    jSONObject3.put("orientationLocked", z);
                    callJavascript(JS_SET_POSITIONS, jSONObject3);
                }
                z = false;
                JSONObject jSONObject32 = new JSONObject();
                jSONObject32.put("currentPosition", jsonCurrentPosition);
                jSONObject32.put("screenSize", jSONObject);
                jSONObject32.put("maxSize", jSONObject2);
                jSONObject32.put("currentAppOrientation", EnvironmentUtils.getCurrentConfigOrientationString());
                jSONObject32.put("orientationLocked", z);
                callJavascript(JS_SET_POSITIONS, jSONObject32);
            } catch (JSONException unused) {
                MMLog.e(TAG, "Error creating json object in setCurrentPosition");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setReadyState() {
        if (!this.ready && this.hasSize && this.isViewable && isJavascriptReady()) {
            this.ready = true;
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MMWebView mMWebView = (MMWebView) JSBridge.this.mmWebViewRef.get();
                    if (mMWebView != null) {
                        JSBridge jSBridge = JSBridge.this;
                        Object[] objArr = new Object[1];
                        objArr[0] = JSBridge.this.isInterstitial ? AdType.INTERSTITIAL : "inline";
                        jSBridge.callJavascript(JSBridge.JS_SET_PLACEMENT_TYPE, objArr);
                        JSBridge.this.callJavascript(JSBridge.JS_SET_SUPPORTS, JSBridge.getSupportedFeatures());
                        JSBridge.this.sendPositions(mMWebView);
                        JSBridge.this.callJavascript(JSBridge.JS_SET_VIEWABLE, Boolean.valueOf(JSBridge.this.isViewable));
                        JSBridge.this.callJavascript(JSBridge.JS_SET_VOLUME, VolumeChangeManager.getCurrentVolume(mMWebView.getContext()));
                        mMWebView.sendExposureChange();
                        JSBridge.this.sendLocation(EnvironmentUtils.getLocation());
                        JSBridge.this.setState(JSBridge.this.isTwoPartExpand ? JSBridge.STATE_EXPANDED : JSBridge.STATE_DEFAULT);
                        if (JSBridge.this.jsBridgeListener != null) {
                            JSBridge.this.jsBridgeListener.onJSBridgeReady();
                        }
                    }
                }
            });
        }
    }

    public boolean isReady() {
        return this.ready;
    }

    private boolean isJavascriptReady() {
        return this.jsInjected && this.scriptsAwaitingLoad.size() == 0;
    }

    public boolean areApiCallsEnabled() {
        return this.apiCallsEnabled;
    }

    public void startLocationUpdates() {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Starting location updates for mmjs.");
        }
        if (this.updateLocationTask != null) {
            this.updateLocationTask.cancel(true);
        }
        if (isLocationSupported()) {
            this.locationUpdatesEnabled = true;
            this.updateLocationTask = new UpdateLocationTask();
            this.updateLocationTask.execute(new Void[0]);
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Location access is disabled. Not starting location updates.");
        }
    }

    public void stopLocationUpdates() {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Stopping location updates for mmjs.");
        }
        this.locationUpdatesEnabled = false;
        if (this.updateLocationTask != null) {
            this.updateLocationTask.cancel(true);
            this.updateLocationTask = null;
        }
    }

    @SuppressLint({"DefaultLocale"})
    public void onVolumeChange(int i, int i2, int i3) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("onVolumeChange old = %d, new = %d, max = %d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)));
        }
        callJavascript(JS_SET_VOLUME, Float.valueOf((((float) i2) / ((float) i3)) * 100.0f));
    }

    @SuppressLint({"DefaultLocale"})
    public void sendExposureChange(float f, Rect rect) {
        JSONObject jSONObject = null;
        if (rect != null) {
            try {
                jSONObject = new JSONObject();
                jSONObject.put("x", rect.left);
                jSONObject.put("y", rect.top);
                jSONObject.put("width", rect.width());
                jSONObject.put("height", rect.height());
            } catch (JSONException e) {
                MMLog.e(TAG, "Error creating minimumBoundingRectangle object for exposure change.", e);
                return;
            }
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("Sending exposure change percentage: %f, minimumBoundingRectangle: %s", Float.valueOf(f), rect));
        }
        callJavascript(JS_SET_EXPOSURE_CHANGE, Float.valueOf(f), jSONObject);
    }
}
