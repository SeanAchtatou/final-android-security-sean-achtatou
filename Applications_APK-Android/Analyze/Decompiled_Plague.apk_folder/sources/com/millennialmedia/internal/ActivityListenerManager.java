package com.millennialmedia.internal;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.SparseArray;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class ActivityListenerManager {
    /* access modifiers changed from: private */
    public static final String TAG = "ActivityListenerManager";
    /* access modifiers changed from: private */
    public static volatile SparseArray<ActivityState> activities = new SparseArray<>();

    public enum LifecycleState {
        UNKNOWN,
        CREATED,
        STARTED,
        RESUMED,
        PAUSED,
        STOPPED,
        DESTROYED
    }

    private static class ActivityState {
        private List<WeakReference<ActivityListener>> activityListenerRefs;
        /* access modifiers changed from: private */
        public LifecycleState lifecycleState = LifecycleState.UNKNOWN;

        ActivityState() {
        }

        /* access modifiers changed from: package-private */
        public List<ActivityListener> getListeners() {
            ArrayList arrayList = new ArrayList();
            if (this.activityListenerRefs != null) {
                synchronized (this.activityListenerRefs) {
                    Iterator<WeakReference<ActivityListener>> it = this.activityListenerRefs.iterator();
                    while (it.hasNext()) {
                        ActivityListener activityListener = (ActivityListener) it.next().get();
                        if (activityListener == null) {
                            it.remove();
                        } else {
                            arrayList.add(activityListener);
                        }
                    }
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        public void registerListener(ActivityListener activityListener) {
            if (this.activityListenerRefs == null) {
                this.activityListenerRefs = new ArrayList();
            }
            synchronized (this.activityListenerRefs) {
                this.activityListenerRefs.add(new WeakReference(activityListener));
            }
            if (MMLog.isDebugEnabled()) {
                String access$000 = ActivityListenerManager.TAG;
                MMLog.d(access$000, "Registered activity listener: " + activityListener);
            }
        }

        /* access modifiers changed from: package-private */
        public void unregisterListener(ActivityListener activityListener) {
            if (this.activityListenerRefs != null) {
                synchronized (this.activityListenerRefs) {
                    Iterator<WeakReference<ActivityListener>> it = this.activityListenerRefs.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        } else if (activityListener == it.next().get()) {
                            if (MMLog.isDebugEnabled()) {
                                String access$000 = ActivityListenerManager.TAG;
                                MMLog.d(access$000, "Unregistered activity listener: " + activityListener);
                            }
                            it.remove();
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public LifecycleState getLifecycleState() {
            return this.lifecycleState;
        }
    }

    public static class ActivityListener {
        public void onCreated(Activity activity) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(ActivityListenerManager.TAG, "Activity created");
            }
        }

        public void onDestroyed(Activity activity) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(ActivityListenerManager.TAG, "Activity destroyed");
            }
        }

        public void onResumed(Activity activity) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(ActivityListenerManager.TAG, "Activity resumed");
            }
        }

        public void onPaused(Activity activity) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(ActivityListenerManager.TAG, "Activity paused");
            }
        }

        public void onStarted(Activity activity) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(ActivityListenerManager.TAG, "Activity started");
            }
        }

        public void onStopped(Activity activity) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(ActivityListenerManager.TAG, "Activity stopped");
            }
        }
    }

    public static void init() {
        EnvironmentUtils.getApplication().registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            public void onActivityCreated(Activity activity, Bundle bundle) {
                if (MMLog.isDebugEnabled()) {
                    String access$000 = ActivityListenerManager.TAG;
                    MMLog.d(access$000, "Activity onCreate called for activity ID: " + activity.hashCode());
                }
                ActivityState access$100 = ActivityListenerManager.getActivityState(activity.hashCode(), true);
                LifecycleState unused = access$100.lifecycleState = LifecycleState.CREATED;
                for (ActivityListener next : access$100.getListeners()) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(ActivityListenerManager.TAG, String.format(Locale.getDefault(), "Calling onCreated of activity listener <%s> for activity ID <%d>", next, Integer.valueOf(activity.hashCode())));
                    }
                    next.onCreated(activity);
                }
            }

            public void onActivityStarted(Activity activity) {
                if (MMLog.isDebugEnabled()) {
                    String access$000 = ActivityListenerManager.TAG;
                    MMLog.d(access$000, "Activity onStart called for activity ID: " + activity.hashCode());
                }
                ActivityState access$100 = ActivityListenerManager.getActivityState(activity.hashCode(), true);
                LifecycleState unused = access$100.lifecycleState = LifecycleState.STARTED;
                for (ActivityListener next : access$100.getListeners()) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(ActivityListenerManager.TAG, String.format(Locale.getDefault(), "Calling onStarted of activity listener <%s> for activity ID <%d>", next, Integer.valueOf(activity.hashCode())));
                    }
                    next.onStarted(activity);
                }
            }

            public void onActivityResumed(Activity activity) {
                if (MMLog.isDebugEnabled()) {
                    String access$000 = ActivityListenerManager.TAG;
                    MMLog.d(access$000, "Activity onResume called for activity ID: " + activity.hashCode());
                }
                ActivityState access$100 = ActivityListenerManager.getActivityState(activity.hashCode(), true);
                LifecycleState unused = access$100.lifecycleState = LifecycleState.RESUMED;
                for (ActivityListener next : access$100.getListeners()) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(ActivityListenerManager.TAG, String.format(Locale.getDefault(), "Calling onResumed of activity listener <%s> for activity ID <%d>", next, Integer.valueOf(activity.hashCode())));
                    }
                    next.onResumed(activity);
                }
            }

            public void onActivityPaused(Activity activity) {
                if (MMLog.isDebugEnabled()) {
                    String access$000 = ActivityListenerManager.TAG;
                    MMLog.d(access$000, "Activity onPause called for activity ID: " + activity.hashCode());
                }
                ActivityState access$100 = ActivityListenerManager.getActivityState(activity.hashCode(), true);
                LifecycleState unused = access$100.lifecycleState = LifecycleState.PAUSED;
                for (ActivityListener next : access$100.getListeners()) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(ActivityListenerManager.TAG, String.format(Locale.getDefault(), "Calling onPaused of activity listener <%s> for activity ID <%d>", next, Integer.valueOf(activity.hashCode())));
                    }
                    next.onPaused(activity);
                }
            }

            public void onActivityStopped(Activity activity) {
                if (MMLog.isDebugEnabled()) {
                    String access$000 = ActivityListenerManager.TAG;
                    MMLog.d(access$000, "Activity onStop called for activity ID: " + activity.hashCode());
                }
                ActivityState access$100 = ActivityListenerManager.getActivityState(activity.hashCode(), true);
                LifecycleState unused = access$100.lifecycleState = LifecycleState.STOPPED;
                for (ActivityListener next : access$100.getListeners()) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(ActivityListenerManager.TAG, String.format(Locale.getDefault(), "Calling onStopped of activity listener <%s> for activity ID <%d>", next, Integer.valueOf(activity.hashCode())));
                    }
                    next.onStopped(activity);
                }
            }

            public void onActivityDestroyed(Activity activity) {
                if (MMLog.isDebugEnabled()) {
                    String access$000 = ActivityListenerManager.TAG;
                    MMLog.d(access$000, "Activity destroy called for activity ID: " + activity.hashCode());
                }
                ActivityState access$100 = ActivityListenerManager.getActivityState(activity.hashCode(), false);
                if (access$100 != null) {
                    LifecycleState unused = access$100.lifecycleState = LifecycleState.DESTROYED;
                    for (ActivityListener next : access$100.getListeners()) {
                        if (MMLog.isDebugEnabled()) {
                            MMLog.d(ActivityListenerManager.TAG, String.format(Locale.getDefault(), "Calling onDestroyed of activity listener <%s> for activity ID <%d>", next, Integer.valueOf(activity.hashCode())));
                        }
                        next.onDestroyed(activity);
                    }
                    ActivityListenerManager.activities.remove(activity.hashCode());
                } else if (MMLog.isDebugEnabled()) {
                    String access$0002 = ActivityListenerManager.TAG;
                    MMLog.d(access$0002, "Unable to find activity state for activity ID: " + activity.hashCode());
                }
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                if (MMLog.isDebugEnabled()) {
                    String access$000 = ActivityListenerManager.TAG;
                    MMLog.d(access$000, "Activity onSaveInstanceState called for activity ID: " + activity.hashCode());
                }
            }
        });
    }

    public static void registerListener(int i, ActivityListener activityListener) {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Attempting to register activity listener.\n\tactivity ID: " + i + "\n\tactivity listener: " + activityListener);
        }
        if (activityListener == null) {
            MMLog.e(TAG, "Unable to register activity listener, provided instance is null");
        } else {
            getActivityState(i, true).registerListener(activityListener);
        }
    }

    public static void unregisterListener(int i, ActivityListener activityListener) {
        ActivityState activityState;
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Attempting to unregister activity listener.\n\tactivity ID: " + i + "\n\tactivity listener: " + activityListener);
        }
        if (activityListener != null && (activityState = getActivityState(i, false)) != null) {
            activityState.unregisterListener(activityListener);
        }
    }

    public static void setInitialStateForUnknownActivity(int i, LifecycleState lifecycleState) {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Attempting to set lifecycle state for unknown activity.\n\tactivity ID: " + i + "\n\tlifecycle state: " + lifecycleState);
        }
        ActivityState activityState = getActivityState(i, true);
        if (activityState.lifecycleState != LifecycleState.UNKNOWN) {
            String str2 = TAG;
            MMLog.e(str2, "Failed to set lifecycle state. Activity already exists with state <" + activityState.lifecycleState + ">");
            return;
        }
        LifecycleState unused = activityState.lifecycleState = lifecycleState;
    }

    public static LifecycleState getLifecycleState(Activity activity) {
        if (activity != null) {
            return getLifecycleState(activity.hashCode());
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Lifecycle state <UNKNOWN> for null activity");
        }
        return LifecycleState.UNKNOWN;
    }

    public static LifecycleState getLifecycleState(int i) {
        LifecycleState lifecycleState = LifecycleState.UNKNOWN;
        ActivityState activityState = getActivityState(i, false);
        if (activityState != null) {
            lifecycleState = activityState.getLifecycleState();
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Lifecycle state <" + lifecycleState + "> for activity ID <" + i + ">");
        }
        return lifecycleState;
    }

    /* access modifiers changed from: private */
    public static ActivityState getActivityState(int i, boolean z) {
        ActivityState activityState = activities.get(i);
        if (activityState != null || !z) {
            return activityState;
        }
        ActivityState activityState2 = new ActivityState();
        activities.put(i, activityState2);
        return activityState2;
    }
}
