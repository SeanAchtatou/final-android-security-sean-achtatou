package com.millennialmedia.internal;

import android.content.Context;
import android.text.TextUtils;
import com.millennialmedia.BidRequestErrorStatus;
import com.millennialmedia.BidRequestListener;
import com.millennialmedia.CreativeInfo;
import com.millennialmedia.MMException;
import com.millennialmedia.MMLog;
import com.millennialmedia.XIncentivizedEventListener;
import com.millennialmedia.internal.adwrapper.AdWrapper;
import com.millennialmedia.internal.adwrapper.SuperAuctionAdWrapperType;
import com.millennialmedia.internal.playlistserver.PlayListServer;
import com.millennialmedia.internal.utils.ThreadUtils;
import java.util.Map;

public abstract class AdPlacement {
    protected static final String STATE_AD_ADAPTER_LOAD_FAILED = "ad_adapter_load_failed";
    protected static final String STATE_DESTROYED = "destroyed";
    protected static final String STATE_IDLE = "idle";
    protected static final String STATE_LOADED = "loaded";
    protected static final String STATE_LOADING_AD_ADAPTER = "loading_ad_adapter";
    protected static final String STATE_LOADING_PLAY_LIST = "loading_play_list";
    protected static final String STATE_LOAD_FAILED = "load_failed";
    protected static final String STATE_PLAY_LIST_LOADED = "play_list_loaded";
    /* access modifiers changed from: private */
    public static final String TAG = "AdPlacement";
    protected Context context;
    /* access modifiers changed from: protected */
    public volatile RequestState currentRequestState;
    protected XIncentivizedEventListener incentivizedEventListener;
    private volatile boolean pendingDestroy = false;
    public String placementId;
    /* access modifiers changed from: protected */
    public volatile String placementState = STATE_IDLE;
    /* access modifiers changed from: protected */
    public volatile PlayList playList;

    /* access modifiers changed from: protected */
    public abstract boolean canDestroyNow();

    public abstract Map<String, Object> getAdPlacementMetaDataMap();

    public abstract Context getContext();

    public abstract CreativeInfo getCreativeInfo();

    /* access modifiers changed from: protected */
    public abstract void onDestroy();

    public static class RequestState {
        private AdPlacementReporter adPlacementReporter;
        private int itemHash;
        private int requestHash = new Object().hashCode();

        public int getItemHash() {
            this.itemHash = new Object().hashCode();
            return this.itemHash;
        }

        public void setAdPlacementReporter(AdPlacementReporter adPlacementReporter2) {
            this.adPlacementReporter = adPlacementReporter2;
        }

        public AdPlacementReporter getAdPlacementReporter() {
            return this.adPlacementReporter;
        }

        public boolean compareRequest(RequestState requestState) {
            return this.requestHash == requestState.requestHash;
        }

        public boolean compare(RequestState requestState) {
            if (this.requestHash == requestState.requestHash && this.itemHash == requestState.itemHash) {
                return true;
            }
            return false;
        }

        public RequestState copy() {
            RequestState requestState = new RequestState();
            requestState.requestHash = this.requestHash;
            requestState.itemHash = this.itemHash;
            requestState.adPlacementReporter = this.adPlacementReporter;
            return requestState;
        }

        public String toString() {
            return "RequestState{requestHash=" + this.requestHash + ", itemHash=" + this.itemHash + "}";
        }
    }

    public RequestState getRequestState() {
        this.currentRequestState = new RequestState();
        return this.currentRequestState;
    }

    protected AdPlacement(String str) throws MMException {
        this.placementId = cleanPlacementId(str);
    }

    public void xSetIncentivizedListener(XIncentivizedEventListener xIncentivizedEventListener) {
        this.incentivizedEventListener = xIncentivizedEventListener;
    }

    public XIncentivizedEventListener xGetIncentivizedListener() {
        return this.incentivizedEventListener;
    }

    /* access modifiers changed from: protected */
    public void onIncentiveEarned(final XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent) {
        if (xIncentiveEvent != null) {
            if (MMLog.isDebugEnabled()) {
                String str = TAG;
                MMLog.d(str, "Incentive earned <" + xIncentiveEvent.eventId + ">");
            }
            final XIncentivizedEventListener xIncentivizedEventListener = this.incentivizedEventListener;
            if (xIncentivizedEventListener != null) {
                ThreadUtils.runOffUiThread(new Runnable() {
                    public void run() {
                        if (XIncentivizedEventListener.XIncentiveEvent.INCENTIVE_VIDEO_COMPLETE.equalsIgnoreCase(xIncentiveEvent.eventId)) {
                            xIncentivizedEventListener.onVideoComplete();
                        } else {
                            xIncentivizedEventListener.onCustomEvent(xIncentiveEvent);
                        }
                    }
                });
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void destroy() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.isDestroyed()     // Catch:{ all -> 0x0038 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r3)
            return
        L_0x0009:
            boolean r0 = r3.canDestroyNow()     // Catch:{ all -> 0x0038 }
            if (r0 == 0) goto L_0x0013
            r3.doDestroy()     // Catch:{ all -> 0x0038 }
            goto L_0x0036
        L_0x0013:
            boolean r0 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ all -> 0x0038 }
            if (r0 == 0) goto L_0x0033
            java.lang.String r0 = com.millennialmedia.internal.AdPlacement.TAG     // Catch:{ all -> 0x0038 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0038 }
            r1.<init>()     // Catch:{ all -> 0x0038 }
            java.lang.String r2 = "Destroy is pending "
            r1.append(r2)     // Catch:{ all -> 0x0038 }
            int r2 = r3.hashCode()     // Catch:{ all -> 0x0038 }
            r1.append(r2)     // Catch:{ all -> 0x0038 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0038 }
            com.millennialmedia.MMLog.e(r0, r1)     // Catch:{ all -> 0x0038 }
        L_0x0033:
            r0 = 1
            r3.pendingDestroy = r0     // Catch:{ all -> 0x0038 }
        L_0x0036:
            monitor-exit(r3)
            return
        L_0x0038:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.AdPlacement.destroy():void");
    }

    /* access modifiers changed from: protected */
    public synchronized boolean doPendingDestroy() {
        if (this.placementState.equals(STATE_DESTROYED)) {
            return true;
        }
        if (!this.pendingDestroy) {
            return false;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.e(str, "Processing pending destroy " + hashCode());
        }
        doDestroy();
        return true;
    }

    private void doDestroy() {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.i(str, "Destroying ad " + hashCode());
        }
        this.placementState = STATE_DESTROYED;
        this.pendingDestroy = false;
        onDestroy();
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Ad destroyed");
        }
    }

    public boolean isDestroyed() {
        if (!this.placementState.equals(STATE_DESTROYED) && !this.pendingDestroy) {
            return false;
        }
        MMLog.e(TAG, "Placement has been destroyed");
        return true;
    }

    public static class DisplayOptions {
        private Integer enterAnimationId;
        private Integer exitAnimationId;
        private boolean immersive = true;

        public DisplayOptions setImmersive(boolean z) {
            this.immersive = z;
            return this;
        }

        public DisplayOptions setTransitionAnimation(int i, int i2) {
            this.enterAnimationId = Integer.valueOf(i);
            this.exitAnimationId = Integer.valueOf(i2);
            return this;
        }

        public boolean isImmersive() {
            return this.immersive;
        }

        public Integer getEnterAnimationId() {
            return this.enterAnimationId;
        }

        public Integer getExitAnimationId() {
            return this.exitAnimationId;
        }
    }

    protected static void requestBid(String str, final AdPlacementMetadata<?> adPlacementMetadata, final BidRequestListener bidRequestListener) throws MMException {
        final String cleanPlacementId = cleanPlacementId(str);
        if (bidRequestListener == null) {
            throw new MMException("BidRequestListener must not be null");
        } else if (adPlacementMetadata == null) {
            throw new MMException("Metadata must not be null");
        } else {
            PlayListServer.loadPlayList(adPlacementMetadata.toMap(cleanPlacementId), new PlayListServer.PlayListLoadListener() {
                public void onLoaded(PlayList playList) {
                    AdWrapper item = playList.getItem(0);
                    if (item instanceof SuperAuctionAdWrapperType.SuperAuctionAdWrapper) {
                        SuperAuctionAdWrapperType.SuperAuctionAdWrapper superAuctionAdWrapper = (SuperAuctionAdWrapperType.SuperAuctionAdWrapper) item;
                        String bidPrice = superAuctionAdWrapper.getBidPrice();
                        if (TextUtils.isEmpty(bidPrice)) {
                            AdPlacement.onBidRequestFailed(bidRequestListener, new BidRequestErrorStatus(BidRequestErrorStatus.INVALID_BID_PRICE));
                            SuperAuctionAdWrapperType.reportBidFailed(playList, superAuctionAdWrapper, adPlacementMetadata.getImpressionGroup(), 110);
                            return;
                        }
                        PlayListServer.addPlaylistToCache(cleanPlacementId, playList, adPlacementMetadata.getImpressionGroup(), (long) Handshake.getSuperAuctionCacheTimeout());
                        if (MMLog.isDebugEnabled()) {
                            String access$000 = AdPlacement.TAG;
                            MMLog.d(access$000, "Added Playlist to cache for placement id: <" + cleanPlacementId + ">");
                        }
                        AdPlacement.onBidRequestSucceeded(bidRequestListener, bidPrice);
                        return;
                    }
                    String access$0002 = AdPlacement.TAG;
                    MMLog.e(access$0002, "Invalid playlist item <" + item + ">. Playlist item must be of type super_auction.");
                    AdPlacement.onBidRequestFailed(bidRequestListener, new BidRequestErrorStatus(5));
                    SuperAuctionAdWrapperType.reportBidFailed(playList, null, adPlacementMetadata.getImpressionGroup(), 110);
                }

                public void onLoadFailed(Throwable th) {
                    MMLog.e(AdPlacement.TAG, "Play list load failed", th);
                    AdPlacement.onBidRequestFailed(bidRequestListener, new BidRequestErrorStatus(5));
                }
            }, Handshake.getInlineTimeout(), false);
        }
    }

    protected static void onBidRequestSucceeded(final BidRequestListener bidRequestListener, final String str) {
        if (bidRequestListener != null) {
            ThreadUtils.runOffUiThread(new Runnable() {
                public void run() {
                    bidRequestListener.onRequestSucceeded(str);
                }
            });
        }
    }

    protected static void onBidRequestFailed(final BidRequestListener bidRequestListener, final BidRequestErrorStatus bidRequestErrorStatus) {
        if (bidRequestListener != null) {
            ThreadUtils.runOffUiThread(new Runnable() {
                public void run() {
                    bidRequestListener.onRequestFailed(bidRequestErrorStatus);
                }
            });
        }
    }

    private static String cleanPlacementId(String str) throws MMException {
        if (str == null) {
            throw new MMException("PlacementId must be a non null.");
        }
        String trim = str.trim();
        if (!trim.isEmpty()) {
            return trim;
        }
        throw new MMException("PlacementId cannot be an empty string.");
    }
}
