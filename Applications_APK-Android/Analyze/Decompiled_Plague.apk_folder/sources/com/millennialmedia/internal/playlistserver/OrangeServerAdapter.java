package com.millennialmedia.internal.playlistserver;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.millennialmedia.AppInfo;
import com.millennialmedia.MMLog;
import com.millennialmedia.MMSDK;
import com.millennialmedia.TestInfo;
import com.millennialmedia.UserData;
import com.millennialmedia.internal.AdPlacementMetadata;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.PlayList;
import com.millennialmedia.internal.UserPrivacy;
import com.millennialmedia.internal.playlistserver.PlayListServerAdapter;
import com.millennialmedia.internal.utils.AdvertisingIdInfo;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.JSONUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.Utils;
import com.miniclip.videoads.ProviderConfig;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrangeServerAdapter extends PlayListServerAdapter {
    public static final String PLAYLIST_REQUEST_PATH = "/admax/sdk/playlist/2";
    private static final String REQ_KEY = "req";
    /* access modifiers changed from: private */
    public static final String TAG = "OrangeServerAdapter";

    private static JSONObject buildAppInfoJSON() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(ProviderConfig.MCVideoAdsAppIdKey, EnvironmentUtils.getAppId());
        jSONObject.put(TJAdUnitConstants.String.USAGE_TRACKER_NAME, EnvironmentUtils.getApplicationName());
        return jSONObject;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private static JSONObject buildEnvironmentInfoJSON(boolean z) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("os", TapjoyConstants.TJC_DEVICE_PLATFORM_TYPE);
        jSONObject.put("osv", Build.VERSION.RELEASE);
        JSONUtils.injectIfNotNull(jSONObject, "model", EnvironmentUtils.getModel());
        jSONObject.put("sdkVer", "6.8.1-72925a6");
        if (!MMSDK.registeredPlugins.isEmpty()) {
            jSONObject.put("sdkPlugins", JSONUtils.buildFromMap(MMSDK.registeredPlugins));
        }
        JSONUtils.injectIfNotNull(jSONObject, "mcc", EnvironmentUtils.getMcc());
        JSONUtils.injectIfNotNull(jSONObject, "mnc", EnvironmentUtils.getMnc());
        jSONObject.put("lang", EnvironmentUtils.getLocaleLanguage());
        jSONObject.put("country", EnvironmentUtils.getLocaleCountry());
        jSONObject.put("ua", EnvironmentUtils.getUserAgent());
        if (z) {
            jSONObject.put("secureContent", true);
        }
        AdvertisingIdInfo adInfo = EnvironmentUtils.getAdInfo();
        String advertisingId = EnvironmentUtils.getAdvertisingId(adInfo);
        if (advertisingId != null) {
            jSONObject.put("ifa", advertisingId);
            jSONObject.put("lmt", EnvironmentUtils.isLimitAdTrackingEnabled(adInfo));
        } else {
            JSONUtils.injectIfNotNull(jSONObject, "dpidmd5", EnvironmentUtils.getHashedDeviceId("MD5"));
            JSONUtils.injectIfNotNull(jSONObject, "dpidsha1", EnvironmentUtils.getHashedDeviceId("SHA1"));
        }
        jSONObject.put("w", EnvironmentUtils.getDisplayWidth());
        jSONObject.put("h", EnvironmentUtils.getDisplayHeight());
        jSONObject.put("screenScale", (double) EnvironmentUtils.getDisplayDensity());
        jSONObject.put("ppi", EnvironmentUtils.getDisplayDensityDpi());
        jSONObject.put("natOrient", EnvironmentUtils.getNaturalConfigOrientationString());
        JSONUtils.injectIfNotNull(jSONObject, "storage", EnvironmentUtils.getAvailableStorageSize());
        JSONUtils.injectIfNotNull(jSONObject, "vol", EnvironmentUtils.getVolume(3));
        JSONUtils.injectIfNotNull(jSONObject, "headphones", EnvironmentUtils.areHeadphonesPluggedIn());
        JSONUtils.injectIfNotNull(jSONObject, "charging", EnvironmentUtils.isDevicePlugged());
        JSONUtils.injectIfNotNull(jSONObject, "charge", EnvironmentUtils.getBatteryLevel());
        JSONUtils.injectIfNotNull(jSONObject, "connectionType", EnvironmentUtils.getNetworkConnectionType());
        JSONUtils.injectIfNotNull(jSONObject, "cellSignalDbm", EnvironmentUtils.getCellSignalDbm());
        JSONUtils.injectIfNotNull(jSONObject, "carrier", EnvironmentUtils.getNetworkOperatorName());
        JSONUtils.injectIfNotNull(jSONObject, "ip", EnvironmentUtils.getIpAddress());
        Location location = EnvironmentUtils.getLocation();
        if (location != null && MMSDK.locationEnabled) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("lat", location.getLatitude());
            jSONObject2.put("lon", location.getLongitude());
            jSONObject2.put("src", location.getProvider());
            jSONObject2.put("ts", location.getTime() / 1000);
            if (location.hasAccuracy()) {
                jSONObject2.put("horizAcc", (double) location.getAccuracy());
            }
            if (location.hasSpeed()) {
                jSONObject2.put("speed", (double) location.getSpeed());
            }
            if (location.hasBearing()) {
                jSONObject2.put("bearing", (double) location.getBearing());
            }
            if (location.hasAltitude()) {
                jSONObject2.put("alt", location.getAltitude());
            }
            jSONObject.put("loc", jSONObject2);
        }
        JSONObject jSONObject3 = new JSONObject();
        EnvironmentUtils.AvailableCameras availableCameras = EnvironmentUtils.getAvailableCameras();
        if (availableCameras != null) {
            if (availableCameras.frontCamera) {
                jSONObject3.put("cameraFront", "true");
            }
            if (availableCameras.backCamera) {
                jSONObject3.put("cameraRear", "true");
            }
        }
        if (EnvironmentUtils.hasNfc()) {
            JSONUtils.injectAsStringIfNotNull(jSONObject3, "nfc", EnvironmentUtils.hasNfcPermission());
        }
        if (EnvironmentUtils.hasBluetooth()) {
            JSONUtils.injectAsStringIfNotNull(jSONObject3, "bt", EnvironmentUtils.hasBluetoothPermission());
        }
        if (EnvironmentUtils.hasMicrophone()) {
            JSONUtils.injectAsStringIfNotNull(jSONObject3, "mic", EnvironmentUtils.hasMicrophonePermission());
        }
        if (EnvironmentUtils.hasGps()) {
            JSONUtils.injectAsStringIfNotNull(jSONObject3, "gps", EnvironmentUtils.hasFineLocationPermission());
        }
        JSONUtils.injectIfTrue(jSONObject, "deviceFeatures", jSONObject3, Boolean.valueOf(UserPrivacy.canPassUserData()));
        List<String> existingIds = Handshake.getExistingIds();
        if (!existingIds.isEmpty()) {
            JSONUtils.injectIfTrue(jSONObject, "existIds", JSONUtils.buildFromList(existingIds), Boolean.valueOf(UserPrivacy.canPassUserData()));
        }
        return jSONObject;
    }

    private static JSONObject buildRequestInfoJSON(Map<String, Object> map) throws JSONException {
        JSONObject buildFromMap;
        JSONObject jSONObject = new JSONObject();
        JSONUtils.injectIfNotNull(jSONObject, "gdpr", UserPrivacy.isCoveredByGDPR());
        AppInfo appInfo = MMSDK.getAppInfo();
        if (appInfo != null) {
            jSONObject.put("coppa", appInfo.getCoppa());
            jSONObject.put("dcn", appInfo.getSiteId());
            jSONObject.put("mediator", appInfo.getMediator());
        }
        if (map != null) {
            jSONObject.put("posType", map.get(AdPlacementMetadata.METADATA_KEY_PLACEMENT_TYPE));
            Object obj = map.get(AdPlacementMetadata.METADATA_KEY_IMPRESSION_GROUP);
            if ((obj instanceof String) && !Utils.isEmpty((String) obj)) {
                jSONObject.put("grp", obj);
            }
            Object obj2 = map.get(AdPlacementMetadata.METADATA_KEY_CUSTOM_TARGETING);
            if (obj2 instanceof Map) {
                Map map2 = (Map) obj2;
                if (!map2.isEmpty() && (buildFromMap = JSONUtils.buildFromMap(map2)) != null && buildFromMap.length() > 0) {
                    jSONObject.put("targeting", buildFromMap);
                }
            }
            JSONObject buildFromMap2 = JSONUtils.buildFromMap(UserPrivacy.getConsentDataMap());
            if (buildFromMap2 != null && buildFromMap2.length() > 0) {
                jSONObject.put("consentstrings", buildFromMap2);
            }
            jSONObject.put("orients", JSONUtils.buildFromList((List) map.get(AdPlacementMetadata.METADATA_KEY_SUPPORTED_ORIENTATIONS)));
            jSONObject.put(AdPlacementMetadata.METADATA_KEY_KEYWORDS, JSONUtils.buildFromList(Utils.splitCommaSeparateString((String) map.get(AdPlacementMetadata.METADATA_KEY_KEYWORDS))));
            jSONObject.put("posId", map.get(AdPlacementMetadata.METADATA_KEY_PLACEMENT_ID));
            Object obj3 = map.get("width");
            if ((obj3 instanceof Integer) && ((Integer) obj3).intValue() > 0) {
                jSONObject.put("w", obj3);
            }
            Object obj4 = map.get("height");
            if ((obj4 instanceof Integer) && ((Integer) obj4).intValue() > 0) {
                jSONObject.put("h", obj4);
            }
            jSONObject.put("refreshRate", map.get("refreshRate"));
            if (map.containsKey(AdPlacementMetadata.METADATA_KEY_NATIVE_TYPES)) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("nativeType", JSONUtils.buildFromList((List) map.get(AdPlacementMetadata.METADATA_KEY_NATIVE_TYPES)));
                jSONObject.put("posTypeAttrs", jSONObject2);
            }
        }
        jSONObject.put("curOrient", EnvironmentUtils.getCurrentConfigOrientationString());
        return jSONObject;
    }

    @SuppressLint({"SimpleDateFormat"})
    private static JSONObject buildUserInfoJSON() throws JSONException {
        UserData userData;
        if (!UserPrivacy.canPassUserData() || (userData = MMSDK.getUserData()) == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("age", userData.getAge());
        jSONObject.put("kids", userData.getChildren());
        jSONObject.put("hhi", userData.getIncome());
        jSONObject.put("edu", userData.getEducation());
        jSONObject.put("eth", userData.getEthnicity());
        jSONObject.put("gender", userData.getGender());
        jSONObject.put(AdPlacementMetadata.METADATA_KEY_KEYWORDS, JSONUtils.buildFromList(Utils.splitCommaSeparateString(userData.getKeywords())));
        jSONObject.put("marital", userData.getMarital());
        jSONObject.put("politics", userData.getPolitics());
        jSONObject.put("zip", userData.getPostalCode());
        Date dob = userData.getDob();
        if (dob != null) {
            jSONObject.put("dob", new SimpleDateFormat("yyyyMMdd").format(dob));
        }
        jSONObject.put("state", userData.getState());
        jSONObject.put("country", userData.getCountry());
        jSONObject.put("dma", userData.getDma());
        return jSONObject;
    }

    private static JSONObject buildTestingJSON() throws JSONException {
        TestInfo testInfo = MMSDK.getTestInfo();
        if (testInfo == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("bidder", testInfo.bidder);
        jSONObject.put("creativeId", testInfo.creativeId);
        return jSONObject;
    }

    static String buildAdRequest(Map<String, Object> map, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("ver", "2");
            jSONObject.put("app", buildAppInfoJSON());
            jSONObject.put("env", buildEnvironmentInfoJSON(z));
            jSONObject.put("req", buildRequestInfoJSON(map));
            jSONObject.put("user", buildUserInfoJSON());
            jSONObject.put("testing", buildTestingJSON());
            return jSONObject.toString();
        } catch (Exception e) {
            MMLog.e(TAG, "Error creating JSON request", e);
            return null;
        }
    }

    public void loadPlayList(final Map<String, Object> map, final PlayListServerAdapter.AdapterLoadListener adapterLoadListener, final int i) {
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                String activePlaylistServerBaseUrl = Handshake.getActivePlaylistServerBaseUrl();
                if (activePlaylistServerBaseUrl == null) {
                    adapterLoadListener.loadFailed(new RuntimeException("Unable to determine base url for request"));
                    return;
                }
                String concat = activePlaylistServerBaseUrl.concat(OrangeServerAdapter.PLAYLIST_REQUEST_PATH);
                String buildAdRequest = OrangeServerAdapter.buildAdRequest(map, URLUtil.isHttpsUrl(concat));
                if (buildAdRequest == null) {
                    adapterLoadListener.loadFailed(new RuntimeException("Unable to create post request data"));
                    return;
                }
                if (MMLog.isDebugEnabled()) {
                    String access$000 = OrangeServerAdapter.TAG;
                    MMLog.d(access$000, "Request\n\turl: " + concat + "\n\tpost data: " + buildAdRequest);
                }
                HttpUtils.Response contentFromPostRequest = HttpUtils.getContentFromPostRequest(concat, buildAdRequest, "application/json", i);
                if (contentFromPostRequest.code != 200 || TextUtils.isEmpty(contentFromPostRequest.content)) {
                    adapterLoadListener.loadFailed(new RuntimeException("Post request failed to get ad"));
                    return;
                }
                if (MMLog.isDebugEnabled()) {
                    String access$0002 = OrangeServerAdapter.TAG;
                    MMLog.d(access$0002, "Response content:\n" + contentFromPostRequest.content);
                }
                PlayList parsePlayListResponse = OrangeServerAdapter.parsePlayListResponse(contentFromPostRequest.content);
                if (parsePlayListResponse == null) {
                    adapterLoadListener.loadFailed(new RuntimeException("Unable to get valid playlist"));
                } else {
                    adapterLoadListener.loadSucceeded(parsePlayListResponse);
                }
            }
        });
    }

    static PlayList parsePlayListResponse(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (MMLog.isDebugEnabled()) {
                String str2 = TAG;
                MMLog.d(str2, "playlist = \n" + jSONObject.toString(2));
            }
            PlayList playList = new PlayList();
            playList.playListVersion = jSONObject.getString("ver");
            if (!playList.playListVersion.equals("2")) {
                MMLog.e(TAG, "Playlist response does not match requested version");
                return null;
            }
            playList.handshakeConfig = jSONObject.optString("config", null);
            if (playList.handshakeConfig != null && !playList.handshakeConfig.equals(Handshake.getConfig())) {
                Handshake.request(true);
            }
            playList.responseId = JSONUtils.getNonEmptyString(jSONObject, "id");
            playList.placementId = JSONUtils.getNonEmptyString(jSONObject, "posId");
            playList.placementName = JSONUtils.getNonEmptyString(jSONObject, "pos");
            playList.siteId = JSONUtils.getNonEmptyString(jSONObject, "dcn");
            if (!"DoNotReport".equals(playList.siteId)) {
                playList.enableReporting();
            } else if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Playlist dcn is <DoNotReport> -- reporting disabled");
            }
            JSONArray jSONArray = jSONObject.getJSONArray("playlist");
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    playList.addItem(getPlayListItemType(jSONObject2.getString("type")).createAdWrapperFromJSON(jSONObject2, playList.responseId));
                } catch (Exception e) {
                    String str3 = TAG;
                    MMLog.e(str3, "Unable to parse play list item<" + i + ">", e);
                }
            }
            return playList;
        } catch (JSONException e2) {
            MMLog.e(TAG, "Unable to parse play list", e2);
            return null;
        }
    }
}
