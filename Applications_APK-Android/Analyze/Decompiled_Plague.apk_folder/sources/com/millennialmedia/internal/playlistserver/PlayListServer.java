package com.millennialmedia.internal.playlistserver;

import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdPlacementMetadata;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.PlayList;
import com.millennialmedia.internal.adwrapper.SuperAuctionAdWrapperType;
import com.millennialmedia.internal.playlistserver.PlayListServerAdapter;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.TimedMemoryCache;
import com.millennialmedia.internal.utils.Utils;
import com.tapjoy.TapjoyConstants;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PlayListServer {
    private static final String TAG = "PlayListServer";
    private static Class<? extends PlayListServerAdapter> activePlayListServerAdapterClass;
    public static final PlayListTimedMemoryCache playListCache = new PlayListTimedMemoryCache();
    public static final List<String> supportedAdFormats = Arrays.asList("web", TapjoyConstants.TJC_PLUGIN_NATIVE);

    public interface PlayListLoadListener {
        void onLoadFailed(Throwable th);

        void onLoaded(PlayList playList);
    }

    public static class PlayListCacheItem {
        String impressionGroup;
        PlayList playList;

        PlayListCacheItem(PlayList playList2, String str) {
            this.playList = playList2;
            this.impressionGroup = str;
        }
    }

    public static class PlayListTimedMemoryCache extends TimedMemoryCache<PlayListCacheItem> {
        /* access modifiers changed from: protected */
        public void onExpired(String str, PlayListCacheItem playListCacheItem) {
            SuperAuctionAdWrapperType.reportBidFailed(playListCacheItem.playList, playListCacheItem.impressionGroup, AdPlacementReporter.PLAYLIST_TIMED_OUT_IN_CACHE);
        }

        /* access modifiers changed from: protected */
        public void onOverwritten(String str, PlayListCacheItem playListCacheItem) {
            SuperAuctionAdWrapperType.reportBidFailed(playListCacheItem.playList, playListCacheItem.impressionGroup, AdPlacementReporter.PLAYLIST_REPLACED_IN_CACHE);
        }
    }

    public static void loadPlayList(Map<String, Object> map, PlayListLoadListener playListLoadListener, int i) {
        loadPlayList(map, playListLoadListener, i, true);
    }

    public static void loadPlayList(Map<String, Object> map, final PlayListLoadListener playListLoadListener, int i, boolean z) {
        String str;
        PlayList cachedPlaylist;
        if (!Handshake.isSdkEnabled()) {
            MMLog.e(TAG, "Unable to request ad, SDK is disabled.  Please contact Millennial Media.");
            playListLoadListener.onLoadFailed(new RuntimeException("SDK disabled"));
        } else if (!EnvironmentUtils.isNetworkAvailable()) {
            MMLog.e(TAG, "Unable to request ad, no network connection found");
            playListLoadListener.onLoadFailed(new RuntimeException("Network not available"));
        } else {
            try {
                PlayListServerAdapter activePlayListServerAdapter = getActivePlayListServerAdapter();
                if (!z || (cachedPlaylist = getCachedPlaylist((str = (String) map.get(AdPlacementMetadata.METADATA_KEY_PLACEMENT_ID)))) == null) {
                    activePlayListServerAdapter.loadPlayList(map, new PlayListServerAdapter.AdapterLoadListener() {
                        public void loadSucceeded(PlayList playList) {
                            if (playList != null) {
                                playListLoadListener.onLoaded(playList);
                            } else {
                                playListLoadListener.onLoadFailed(new RuntimeException("Playlist provided by adapter is null"));
                            }
                        }

                        public void loadFailed(Throwable th) {
                            playListLoadListener.onLoadFailed(th);
                        }
                    }, i);
                    return;
                }
                if (MMLog.isDebugEnabled()) {
                    String str2 = TAG;
                    MMLog.d(str2, "Using playlist from cache for id <" + str + ">");
                }
                playListLoadListener.onLoaded(cachedPlaylist);
            } catch (Exception e) {
                playListLoadListener.onLoadFailed(e);
            }
        }
    }

    public static void setActivePlayListServerAdapter(Class<? extends PlayListServerAdapter> cls) {
        activePlayListServerAdapterClass = cls;
    }

    private static PlayListServerAdapter getActivePlayListServerAdapter() throws Exception {
        Class<? extends PlayListServerAdapter> cls = activePlayListServerAdapterClass;
        if (cls == null) {
            cls = Handshake.getActivePlayListServerAdapterClass();
        }
        return PlayListServerAdapter.getAdapter(cls);
    }

    private static PlayList getCachedPlaylist(String str) {
        PlayListCacheItem playListCacheItem;
        if (!Utils.isEmpty(str) && (playListCacheItem = (PlayListCacheItem) playListCache.get(str)) != null) {
            return playListCacheItem.playList;
        }
        return null;
    }

    public static void addPlaylistToCache(String str, PlayList playList, String str2, long j) {
        playListCache.add(str, new PlayListCacheItem(playList, str2), Long.valueOf(j));
    }

    public static void clearPlaylistCache() {
        playListCache.expireAll();
    }
}
