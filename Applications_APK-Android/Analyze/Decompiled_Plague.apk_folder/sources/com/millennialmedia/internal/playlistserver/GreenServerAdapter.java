package com.millennialmedia.internal.playlistserver;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.millennialmedia.AppInfo;
import com.millennialmedia.MMLog;
import com.millennialmedia.MMSDK;
import com.millennialmedia.TestInfo;
import com.millennialmedia.UserData;
import com.millennialmedia.internal.AdMetadata;
import com.millennialmedia.internal.AdPlacementMetadata;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.PlayList;
import com.millennialmedia.internal.adwrapper.ContentAdWrapperType;
import com.millennialmedia.internal.playlistserver.PlayListServerAdapter;
import com.millennialmedia.internal.utils.AdvertisingIdInfo;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.mopub.common.AdType;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class GreenServerAdapter extends PlayListServerAdapter {
    private static final String FALSE = "false";
    /* access modifiers changed from: private */
    public static final String TAG = "GreenServerAdapter";
    private static final String TRUE = "true";

    @SuppressLint({"SimpleDateFormat"})
    static String buildAdRequestParameters(Map<String, Object> map, boolean z) {
        StringBuilder sb = new StringBuilder();
        addParameter(sb, "dm", Build.MODEL);
        addParameter(sb, "dv", "Android" + Build.VERSION.RELEASE);
        addParameter(sb, "ua", EnvironmentUtils.getUserAgent());
        AdvertisingIdInfo adInfo = EnvironmentUtils.getAdInfo();
        String advertisingId = EnvironmentUtils.getAdvertisingId(adInfo);
        if (advertisingId != null) {
            addParameter(sb, "aaid", advertisingId);
            addParameter(sb, "ate", Boolean.toString(!EnvironmentUtils.isLimitAdTrackingEnabled(adInfo)));
        } else {
            String hashedDeviceId = EnvironmentUtils.getHashedDeviceId("MD5");
            String hashedDeviceId2 = EnvironmentUtils.getHashedDeviceId("SHA1");
            if (!(hashedDeviceId == null || hashedDeviceId2 == null)) {
                addParameter(sb, "mmdid", "mmh_" + hashedDeviceId + "_" + hashedDeviceId2);
            }
        }
        addParameter(sb, "density", Float.toString(EnvironmentUtils.getDisplayDensity()));
        addParameter(sb, "hpx", Integer.toString(EnvironmentUtils.getDisplayHeight()));
        addParameter(sb, "wpx", Integer.toString(EnvironmentUtils.getDisplayWidth()));
        addParameter(sb, "do", EnvironmentUtils.getCurrentConfigOrientationString());
        addParameter(sb, "olock", "false");
        addParameter(sb, "sk", "false");
        addParameter(sb, "vol", EnvironmentUtils.getVolume(3));
        addParameter(sb, "headphones", EnvironmentUtils.areHeadphonesPluggedIn());
        if (EnvironmentUtils.hasMicrophone()) {
            addParameter(sb, "mic", EnvironmentUtils.hasMicrophonePermission());
        }
        addParameter(sb, "language", EnvironmentUtils.getLocaleLanguage());
        addParameter(sb, "country", EnvironmentUtils.getLocaleCountry());
        addParameter(sb, "pkid", EnvironmentUtils.getAppId());
        addParameter(sb, "pknm", EnvironmentUtils.getApplicationName());
        addParameter(sb, "bl", EnvironmentUtils.getBatteryLevel());
        addParameter(sb, "plugged", EnvironmentUtils.isDevicePlugged());
        addParameter(sb, "space", EnvironmentUtils.getAvailableStorageSize());
        addParameter(sb, "conn", EnvironmentUtils.getNetworkConnectionType());
        addParameter(sb, "celldbm", EnvironmentUtils.getCellSignalDbm());
        Integer mcc = EnvironmentUtils.getMcc();
        if (mcc != null) {
            addParameter(sb, "mcc", Integer.toString(mcc.intValue()));
        }
        Integer mnc = EnvironmentUtils.getMnc();
        if (mnc != null) {
            addParameter(sb, "mnc", Integer.toString(mnc.intValue()));
        }
        addParameter(sb, "pip", EnvironmentUtils.getIpAddress());
        String networkOperatorName = EnvironmentUtils.getNetworkOperatorName();
        if (!TextUtils.isEmpty(networkOperatorName)) {
            addParameter(sb, "cn", networkOperatorName);
        }
        Location location = EnvironmentUtils.getLocation();
        if (location == null || !MMSDK.locationEnabled) {
            addParameter(sb, "loc", "false");
        } else {
            addParameter(sb, "lat", Double.toString(location.getLatitude()));
            addParameter(sb, "long", Double.toString(location.getLongitude()));
            if (location.hasAccuracy()) {
                addParameter(sb, "ha", Float.toString(location.getAccuracy()));
            }
            if (location.hasSpeed()) {
                addParameter(sb, "spd", Float.toString(location.getSpeed()));
            }
            if (location.hasBearing()) {
                addParameter(sb, "brg", Float.toString(location.getBearing()));
            }
            if (location.hasAltitude()) {
                addParameter(sb, "alt", Double.toString(location.getAltitude()));
            }
            addParameter(sb, "tslr", Long.toString(location.getTime() / 1000));
            addParameter(sb, "loc", "true");
            addParameter(sb, "lsrc", location.getProvider());
        }
        addParameter(sb, "sdkversion", "6.8.1-72925a6.a");
        addParameter(sb, "video", "true");
        addParameter(sb, "cachedvideo", "true");
        AppInfo appInfo = MMSDK.getAppInfo();
        if (appInfo != null) {
            addParameter(sb, VastExtensionXmlManager.VENDOR, appInfo.getMediator());
            addParameter(sb, "coppa", appInfo.getCoppa());
        }
        Object obj = map.get(AdPlacementMetadata.METADATA_KEY_PLACEMENT_ID);
        if (obj instanceof String) {
            addParameter(sb, "apid", (String) obj);
        }
        Object obj2 = map.get(AdPlacementMetadata.METADATA_KEY_PLACEMENT_TYPE);
        if (!(obj2 instanceof String) || !((String) obj2).equals(AdType.INTERSTITIAL)) {
            addParameter(sb, "at", "b");
            addParameter(sb, "reqtype", "getad");
        } else {
            addParameter(sb, "at", "i");
            addParameter(sb, "reqtype", "fetch");
        }
        Object obj3 = map.get("width");
        if ((obj3 instanceof Integer) && ((Integer) obj3).intValue() > 0) {
            addParameter(sb, "hswd", obj3);
        }
        Object obj4 = map.get("height");
        if ((obj4 instanceof Integer) && ((Integer) obj4).intValue() > 0) {
            addParameter(sb, "hsht", obj4);
        }
        addParameter(sb, "refreshrate", map.get("refreshRate"));
        ArrayList arrayList = new ArrayList();
        Object obj5 = map.get(AdPlacementMetadata.METADATA_KEY_KEYWORDS);
        if (obj5 instanceof String) {
            String str = (String) obj5;
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(str);
            }
        }
        UserData userData = MMSDK.getUserData();
        if (userData != null) {
            addParameter(sb, "age", userData.getAge());
            addParameter(sb, "children", userData.getChildren());
            addParameter(sb, "education", userData.getEducation());
            addParameter(sb, "ethnicity", userData.getEthnicity());
            addParameter(sb, "gender", userData.getGender());
            addParameter(sb, "income", userData.getIncome());
            addParameter(sb, "marital", userData.getMarital());
            addParameter(sb, "politics", userData.getPolitics());
            addParameter(sb, "zip", userData.getPostalCode());
            addParameter(sb, "state", userData.getState());
            if (!TextUtils.isEmpty(userData.getKeywords())) {
                arrayList.add(userData.getKeywords());
            }
            Date dob = userData.getDob();
            if (dob != null) {
                addParameter(sb, "dob", new SimpleDateFormat("yyyyMMdd").format(dob));
            }
            addParameter(sb, "dma", userData.getDma());
        }
        if (arrayList.size() > 0) {
            addParameter(sb, AdPlacementMetadata.METADATA_KEY_KEYWORDS, TextUtils.join(",", arrayList));
        }
        addParameter(sb, "appsids", TextUtils.join(",", Handshake.getExistingIds()));
        TestInfo testInfo = MMSDK.getTestInfo();
        if (testInfo != null) {
            addParameter(sb, "acid", testInfo.creativeId);
        }
        if (z) {
            addParameter(sb, "securecontent", "true");
        }
        String sb2 = sb.toString();
        if (TextUtils.isEmpty(sb2)) {
            return null;
        }
        return sb2;
    }

    private static void addParameter(StringBuilder sb, String str, Object obj) {
        String obj2 = obj != null ? obj.toString() : null;
        if (obj != null && !TextUtils.isEmpty(obj2)) {
            try {
                String format = String.format("%s=%s", str, URLEncoder.encode(obj2, "UTF-8"));
                if (sb.length() > 0) {
                    sb.append('&');
                }
                sb.append(format);
            } catch (UnsupportedEncodingException e) {
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(TAG, "Error occurred when trying to inject ad url request parameter", e);
                }
            }
        } else if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Unable to add parameter due to empty value for key <" + str + "> and value <" + obj + ">");
        }
    }

    public void loadPlayList(final Map<String, Object> map, final PlayListServerAdapter.AdapterLoadListener adapterLoadListener, final int i) {
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                String activePlaylistServerBaseUrl = Handshake.getActivePlaylistServerBaseUrl();
                if (activePlaylistServerBaseUrl == null) {
                    adapterLoadListener.loadFailed(new RuntimeException("Unable to determine base url for request"));
                    return;
                }
                String buildAdRequestParameters = GreenServerAdapter.buildAdRequestParameters(map, URLUtil.isHttpsUrl(activePlaylistServerBaseUrl));
                if (buildAdRequestParameters == null) {
                    adapterLoadListener.loadFailed(new RuntimeException("Unable to create request parameters"));
                    return;
                }
                String str = activePlaylistServerBaseUrl + buildAdRequestParameters;
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(GreenServerAdapter.TAG, "Ad request url: " + str);
                }
                HttpUtils.Response contentFromGetRequest = HttpUtils.getContentFromGetRequest(str, i);
                if (contentFromGetRequest.code != 200 || TextUtils.isEmpty(contentFromGetRequest.content)) {
                    adapterLoadListener.loadFailed(new RuntimeException("Get request failed to get ad"));
                    return;
                }
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(GreenServerAdapter.TAG, "Ad response content: " + contentFromGetRequest.content);
                }
                PlayList parsePlayListResponse = GreenServerAdapter.parsePlayListResponse(contentFromGetRequest.content, contentFromGetRequest.adMetadata);
                if (parsePlayListResponse == null) {
                    adapterLoadListener.loadFailed(new RuntimeException("Unable to get valid playlist"));
                } else {
                    adapterLoadListener.loadSucceeded(parsePlayListResponse);
                }
            }
        });
    }

    static PlayList parsePlayListResponse(String str, AdMetadata adMetadata) {
        String l = Long.toString(System.currentTimeMillis() / 1000);
        PlayList playList = new PlayList();
        playList.playListVersion = "2";
        playList.handshakeConfig = "handshakeId_" + l;
        playList.responseId = "response_" + l;
        playList.placementId = "placementId_" + l;
        playList.placementName = "placementName_" + l;
        playList.siteId = "siteId_" + l;
        ContentAdWrapperType.ContentAdWrapper contentAdWrapper = new ContentAdWrapperType.ContentAdWrapper("itemId", str, adMetadata);
        if (adMetadata != null) {
            contentAdWrapper.creativeId = (String) adMetadata.get("X-MM-Acid");
        }
        contentAdWrapper.adnet = "mydas";
        playList.addItem(contentAdWrapper);
        return playList;
    }
}
