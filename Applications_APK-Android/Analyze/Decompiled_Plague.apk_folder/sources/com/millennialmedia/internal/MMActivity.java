package com.millennialmedia.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.google.android.gms.drive.DriveFile;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.TimedMemoryCache;
import com.millennialmedia.internal.utils.Utils;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MMActivity extends Activity {
    private static final String ACTIVITY_STATE_ID_KEY = "activity_state_id";
    private static final long ON_CREATE_TIMEOUT = 5000;
    private static final String TAG = "MMActivity";
    private static TimedMemoryCache<ActivityState> timedMemoryCache = new TimedMemoryCache<>();
    private ActivityState activityState;
    private RelativeLayout rootView;

    public static abstract class MMActivityListener {
        public boolean onBackPressed() {
            return true;
        }

        public void onCreate(MMActivity mMActivity) {
        }

        public void onDestroy(MMActivity mMActivity) {
        }

        public void onLaunchFailed() {
        }

        public void onPause(MMActivity mMActivity) {
        }

        public void onResume(MMActivity mMActivity) {
        }
    }

    private static class ActivityState {
        MMActivityListener activityListener;
        MMActivityConfig configuration;
        CountDownLatch onCreateLatch;

        private ActivityState(MMActivityListener mMActivityListener, MMActivityConfig mMActivityConfig) {
            this.onCreateLatch = new CountDownLatch(1);
            this.activityListener = mMActivityListener;
            this.configuration = mMActivityConfig;
        }
    }

    public static class MMActivityConfig {
        /* access modifiers changed from: private */
        public int audioSource = -1;
        /* access modifiers changed from: private */
        public Integer enterAnimationId;
        /* access modifiers changed from: private */
        public Integer exitAnimationId;
        /* access modifiers changed from: private */
        public boolean immersive;
        /* access modifiers changed from: private */
        public int orientation = -1;
        /* access modifiers changed from: private */
        public boolean transparent;

        public MMActivityConfig setImmersive(boolean z) {
            this.immersive = z;
            return this;
        }

        public MMActivityConfig setOrientation(int i) {
            this.orientation = i;
            return this;
        }

        public MMActivityConfig setAudioSource(int i) {
            this.audioSource = i;
            return this;
        }

        public MMActivityConfig setTransitionAnimation(Integer num, Integer num2) {
            this.enterAnimationId = num;
            this.exitAnimationId = num2;
            return this;
        }

        public MMActivityConfig setTransparent(boolean z) {
            this.transparent = z;
            return this;
        }
    }

    public static void launch(Context context, MMActivityConfig mMActivityConfig, MMActivityListener mMActivityListener) {
        if (mMActivityListener == null) {
            MMLog.e(TAG, "Unable to launch MMActivity, provided MMActivityListener instance is null");
            return;
        }
        if (mMActivityConfig == null) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "No MMActivity Configuration specified, creating default activity Configuration.");
            }
            mMActivityConfig = new MMActivityConfig();
        }
        final ActivityState activityState2 = new ActivityState(mMActivityListener, mMActivityConfig);
        String add = timedMemoryCache.add(activityState2, Long.valueOf((long) ON_CREATE_TIMEOUT));
        if (add == null) {
            MMLog.e(TAG, "Unable to launch MMActivity, failed to cache activity state");
            mMActivityListener.onLaunchFailed();
            return;
        }
        Intent intent = new Intent(context, MMActivity.class);
        intent.putExtra(ACTIVITY_STATE_ID_KEY, add);
        if (!Utils.isActivityContext(context)) {
            intent.addFlags(DriveFile.MODE_READ_ONLY);
        }
        if (mMActivityConfig.enterAnimationId == null && mMActivityConfig.exitAnimationId == null) {
            context.startActivity(intent);
        } else {
            int i = 0;
            int intValue = mMActivityConfig.enterAnimationId != null ? mMActivityConfig.enterAnimationId.intValue() : 0;
            if (mMActivityConfig.exitAnimationId != null) {
                i = mMActivityConfig.exitAnimationId.intValue();
            }
            context.startActivity(intent, ActivityOptions.makeCustomAnimation(context, intValue, i).toBundle());
        }
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                try {
                    if (!activityState2.onCreateLatch.await(MMActivity.ON_CREATE_TIMEOUT, TimeUnit.MILLISECONDS) && activityState2.activityListener != null) {
                        activityState2.activityListener.onLaunchFailed();
                    }
                } catch (InterruptedException unused) {
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!loadActivityState()) {
            String str = TAG;
            MMLog.e(str, "Failed to load activity state, aborting activity launch <" + this + ">");
            finish();
            return;
        }
        int i = 0;
        boolean z = this.activityState.onCreateLatch.getCount() > 0;
        if (z) {
            this.activityState.onCreateLatch.countDown();
        }
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "New activity created with orientation " + EnvironmentUtils.getCurrentConfigOrientationString());
        }
        if (this.activityState.configuration.audioSource != -1) {
            setVolumeControlStream(this.activityState.configuration.audioSource);
        }
        if (Build.VERSION.SDK_INT >= 19 && this.activityState.configuration.immersive) {
            enableImmersiveMode();
            getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                public void onSystemUiVisibilityChange(int i) {
                    if ((i & 4) == 0) {
                        MMActivity.this.enableImmersiveMode();
                    }
                }
            });
        } else if (this.activityState.configuration.immersive) {
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
        }
        this.rootView = new RelativeLayout(this);
        this.rootView.setTag("mmactivity_root_view");
        if (!this.activityState.configuration.transparent) {
            i = 160;
        }
        ColorDrawable colorDrawable = new ColorDrawable(-16777216);
        colorDrawable.setAlpha(i);
        this.rootView.setBackground(colorDrawable);
        this.rootView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        setContentView(this.rootView);
        if (this.activityState.activityListener != null) {
            this.activityState.activityListener.onCreate(this);
        }
        if (z && getRequestedOrientation() != this.activityState.configuration.orientation) {
            if (MMLog.isDebugEnabled()) {
                String str3 = TAG;
                MMLog.d(str3, "Setting requested orientation on activity:\n\tCurrent requested orientation: " + getRequestedOrientation() + "\n\tDesired requested orientation: " + this.activityState.configuration.orientation);
            }
            setRequestedOrientation(this.activityState.configuration.orientation);
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    public void enableImmersiveMode() {
        View decorView = getWindow().getDecorView();
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Enabling immersive mode:\ndecorView = " + decorView + "\nActivity = " + this);
        }
        decorView.setSystemUiVisibility(5894);
    }

    public void setOrientation(int i) {
        if (i != getRequestedOrientation()) {
            int unused = this.activityState.configuration.orientation = i;
            setRequestedOrientation(i);
        }
    }

    public void onResume() {
        super.onResume();
        if (this.activityState.activityListener != null) {
            this.activityState.activityListener.onResume(this);
        }
    }

    public void onPause() {
        if (this.activityState.activityListener != null) {
            this.activityState.activityListener.onPause(this);
        }
        super.onPause();
    }

    public void onDestroy() {
        if (this.activityState != null) {
            if (!isFinishing() && !saveActivityState()) {
                String str = TAG;
                MMLog.e(str, "Failed to save activity state <" + this + ">");
            }
            if (this.activityState.activityListener != null) {
                this.activityState.activityListener.onDestroy(this);
                if (isFinishing()) {
                    this.activityState.activityListener = null;
                }
            }
        }
        super.onDestroy();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "onWindowFocusChanged: hasFocus = " + z);
            if (this.activityState != null) {
                String str2 = TAG;
                MMLog.d(str2, "activityState.configuration.immersive = " + this.activityState.configuration.immersive);
            }
        }
        if (Build.VERSION.SDK_INT >= 19 && this.activityState != null && this.activityState.configuration.immersive && z) {
            enableImmersiveMode();
        }
    }

    public void finish() {
        if (!(this.activityState == null || this.activityState.configuration == null || (this.activityState.configuration.enterAnimationId == null && this.activityState.configuration.exitAnimationId == null))) {
            overridePendingTransition(this.activityState.configuration.enterAnimationId.intValue(), this.activityState.configuration.exitAnimationId.intValue());
        }
        super.finish();
    }

    private boolean loadActivityState() {
        ActivityState activityState2 = timedMemoryCache.get(getIntent().getStringExtra(ACTIVITY_STATE_ID_KEY));
        if (activityState2 == null) {
            return false;
        }
        this.activityState = activityState2;
        return true;
    }

    private boolean saveActivityState() {
        Intent intent = getIntent();
        intent.removeExtra(ACTIVITY_STATE_ID_KEY);
        String add = timedMemoryCache.add(this.activityState, null);
        if (add == null) {
            return false;
        }
        intent.putExtra(ACTIVITY_STATE_ID_KEY, add);
        return true;
    }

    public ViewGroup getRootView() {
        return this.rootView;
    }

    public void onBackPressed() {
        if (this.activityState.activityListener == null || this.activityState.activityListener.onBackPressed()) {
            super.onBackPressed();
        }
    }
}
