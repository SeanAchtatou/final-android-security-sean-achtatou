package com.millennialmedia.internal;

import java.util.HashMap;
import java.util.Map;

public abstract class ErrorStatus {
    public static final int ADAPTER_NOT_FOUND = 1;
    public static final int DISPLAY_FAILED = 4;
    public static final int INIT_FAILED = 3;
    public static final int LOAD_FAILED = 5;
    public static final int LOAD_TIMED_OUT = 6;
    public static final int NO_NETWORK = 2;
    public static final int UNKNOWN = 7;
    protected static final Map<Integer, String> errorCodes = new HashMap();
    private Throwable cause;
    private String description;
    private int errorCode;

    static {
        errorCodes.put(1, "ADAPTER_NOT_FOUND");
        errorCodes.put(2, "NO_NETWORK");
        errorCodes.put(3, "INIT_FAILED");
        errorCodes.put(4, "DISPLAY_FAILED");
        errorCodes.put(5, "LOAD_FAILED");
        errorCodes.put(6, "LOAD_TIMED_OUT");
        errorCodes.put(7, "UNKNOWN");
    }

    protected ErrorStatus(int i) {
        this(i, null);
    }

    protected ErrorStatus(int i, String str) {
        this(i, str, null);
    }

    protected ErrorStatus(int i, String str, Throwable th) {
        this.errorCode = i;
        this.description = str;
        this.cause = th;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getDescription() {
        return this.description;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.errorCode);
        sb.append("]: [");
        sb.append(errorCodes.get(Integer.valueOf(this.errorCode)));
        sb.append("] ");
        sb.append(this.description != null ? this.description : "No additional details available.");
        if (this.cause != null) {
            str = " caused by " + this.cause.getMessage();
        } else {
            str = "";
        }
        sb.append(str);
        return sb.toString();
    }
}
