package com.millennialmedia.internal;

import android.net.Uri;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FiredEvents {
    private Map<Uri, Set<String>> uriToEvents = new HashMap();

    public boolean isEventFiredForUri(Uri uri, String str) {
        Set set = this.uriToEvents.get(uri);
        return set != null && set.contains(str);
    }

    public void recordForUri(Uri uri, String str) {
        Set set = this.uriToEvents.get(uri);
        if (set == null) {
            set = new HashSet();
            this.uriToEvents.put(uri, set);
        }
        set.add(str);
    }
}
