package com.millennialmedia.internal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.widget.RelativeLayout;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.ActivityListenerManager;
import com.millennialmedia.internal.utils.ViewUtils;

@SuppressLint({"ViewConstructor"})
public class AdContainer extends RelativeLayout {
    private static final String TAG = "AdContainer";
    private ActivityListenerManager.ActivityListener activityListener;

    public AdContainer(Activity activity, ActivityListenerManager.ActivityListener activityListener2) {
        super(activity);
        this.activityListener = activityListener2;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.activityListener != null) {
            int activityHashForView = ViewUtils.getActivityHashForView(this);
            if (activityHashForView == -1) {
                MMLog.e(TAG, "Unable to register activity lifecycle listener for AdContainer, no valid activity hash");
            } else {
                ActivityListenerManager.registerListener(activityHashForView, this.activityListener);
            }
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "AdContainer not listening for activity lifecycle events, skipping activity lifecycle dispatcher registration");
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.activityListener != null) {
            int activityHashForView = ViewUtils.getActivityHashForView(this);
            if (activityHashForView == -1) {
                MMLog.e(TAG, "Unable to unregister activity lifecycle listener for AdContainer, no valid activity hash");
            } else {
                ActivityListenerManager.unregisterListener(activityHashForView, this.activityListener);
            }
        }
    }
}
