package com.millennialmedia.internal;

import android.os.Build;
import com.google.android.gms.games.GamesStatusCodes;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.playlistserver.PlayListServerAdapter;
import com.millennialmedia.internal.task.TaskFactory;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.IOUtils;
import com.millennialmedia.internal.utils.JSONUtils;
import com.millennialmedia.internal.utils.Utils;
import com.miniclip.videoads.ProviderConfig;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Handshake {
    public static final String HANDSHAKE_JSON = "handshake.json";
    public static final String HANDSHAKE_PATH = "/admax/sdk/handshake/1";
    public static final int HANDSHAKE_VERSION = 1;
    private static final String TAG = "Handshake";
    private static Map<String, Class<? extends PlayListServerAdapter>> availableHandshakePlayListServerAdapters = null;
    private static HandshakeInfo currentHandshakeInfo = null;
    private static HandshakeInfo defaultHandshakeInfo = null;
    private static int handshakeAttempts = 0;
    private static boolean initialized = false;
    private static AtomicBoolean requestInProgress = new AtomicBoolean(false);

    public static class HandshakeInfo {
        public volatile String activePlaylistServerBaseUrl;
        public volatile String activePlaylistServerName;
        public volatile int clientMediationTimeout;
        public volatile String config;
        public volatile int exchangeTimeout;
        public volatile Map<String, String> existingPackages = new HashMap();
        public volatile int geoIpCheckTtl;
        public volatile String geoIpCheckUrl;
        public volatile String handshakeBaseUrl;
        public volatile int handshakeTtl;
        public volatile int inlineTimeout;
        public volatile int interstitialExpirationDuration;
        public volatile int interstitialTimeout;
        public volatile int minImpressionDuration;
        public volatile int minImpressionViewabilityPercent;
        public volatile int minInlineRefreshRate;
        public volatile boolean moatEnabled = true;
        public volatile int nativeExpirationDuration;
        public volatile int nativeTimeout;
        public volatile Map<String, NativeTypeDefinition> nativeTypeDefinitions;
        public volatile String reportingBaseUrl;
        public volatile int reportingBatchFrequency;
        public volatile int reportingBatchSize;
        public volatile boolean sdkEnabled = true;
        public volatile int serverToServerTimeout;
        public volatile int superAuctionCacheTimeout;
        public volatile int vastVideoSkipOffsetMax;
        public volatile int vastVideoSkipOffsetMin;
        public volatile String version;
        public volatile int vpaidAdUnitTimeout = 5000;
        public volatile int vpaidHtmlEndCardTimeout = 5000;
        public volatile int vpaidMaxBackButtonDelay = GamesStatusCodes.STATUS_REQUEST_UPDATE_PARTIAL_SUCCESS;
        public volatile int vpaidSkipAdTimeout = TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL;
        public volatile int vpaidStartAdTimeout = 5000;
    }

    static class Defaults {
        static final int CLIENT_MEDIATION_TIMEOUT_FLOOR = 1000;
        static final String DEFAULT_GEO_IP_CHECK_URL = "https://service.cmp.oath.com/cmp/v0/location/eu";
        static final String DEFAULT_HANDSHAKE_BASE_URL = "https://ads.nexage.com";
        static final String DEFAULT_HANDSHAKE_JSON = "mmadsdk/default_handshake.json";
        static final int EXCHANGE_TIMEOUT_FLOOR = 1000;
        static final int GEO_IP_CHECK_TTL_CEILING = 604800000;
        static final int GEO_IP_CHECK_TTL_DEFAULT = 86400000;
        static final int GEO_IP_CHECK_TTL_FLOOR = 1800000;
        static final int HANDSHAKE_REQUEST_TIMEOUT = 15000;
        static final int HANDSHAKE_TTL_FLOOR = 60000;
        static final int INLINE_TIMEOUT_FLOOR = 3000;
        static final int INTERSTITIAL_TIMEOUT_FLOOR = 3000;
        static final int MAX_HANDSHAKE_ATTEMPTS = 10;
        static final int MIN_IMPRESSION_DURATION_CEILING = 60000;
        static final int MIN_IMPRESSION_DURATION_DEFAULT = 0;
        static final int MIN_IMPRESSION_DURATION_FLOOR = 0;
        static final int MIN_IMPRESSION_VIEWABILITY_PERCENT_CEILING = 100;
        static final int MIN_IMPRESSION_VIEWABILITY_PERCENT_DEFAULT = 0;
        static final int MIN_IMPRESSION_VIEWABILITY_PERCENT_FLOOR = 0;
        static final int MIN_INLINE_REFRESH_RATE_FLOOR = 10000;
        static final int NATIVE_TIMEOUT_FLOOR = 3000;
        static final int REPORTING_BATCH_FREQUENCY_FLOOR = 120000;
        static final int REPORTING_BATCH_SIZE_FLOOR = 1;
        static final String SERVER_ADAPTER_KEY_GREEN = "green";
        static final String SERVER_ADAPTER_KEY_ORANGE = "orange";
        static final int SERVER_TO_SERVER_TIMEOUT_FLOOR = 1000;
        static final int SUPER_AUCTION_CACHE_TIMEOUT_CEILING = 86400000;
        static final int SUPER_AUCTION_CACHE_TIMEOUT_DEFAULT = 600000;
        static final int SUPER_AUCTION_CACHE_TIMEOUT_FLOOR = 1000;
        static final int VAST_VIDEO_SKIP_OFFSET_FLOOR = 0;
        static final int VPAID_AD_UNIT_TIMEOUT_FLOOR = 1000;
        static final int VPAID_HTML_END_CARD_TIMEOUT_FLOOR = 0;
        static final int VPAID_MAX_BACK_BUTTON_DELAY_FLOOR = 0;
        static final int VPAID_SKIP_AD_TIMEOUT_FLOOR = 500;
        static final int VPAID_START_AD_TIMEOUT_FLOOR = 1000;

        Defaults() {
        }
    }

    public static class NativeTypeDefinition {
        public List<ComponentDefinition> componentDefinitions = new ArrayList();
        public String typeName;

        public static class ComponentDefinition {
            public int adverstiserRequired;
            public String componentId;
            public int publisherRequired;

            public ComponentDefinition(String str, int i, int i2) {
                this.componentId = str;
                this.publisherRequired = i;
                this.adverstiserRequired = i2;
            }
        }

        public NativeTypeDefinition(String str) {
            this.typeName = str;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0088 A[Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7, all -> 0x00c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00af A[Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c0, JSONException -> 0x00bd, all -> 0x00ba }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void initialize() {
        /*
            boolean r0 = com.millennialmedia.internal.Handshake.initialized
            if (r0 == 0) goto L_0x0012
            boolean r0 = com.millennialmedia.MMLog.isDebugEnabled()
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = com.millennialmedia.internal.Handshake.TAG
            java.lang.String r1 = "Handshake already initialized"
            com.millennialmedia.MMLog.d(r0, r1)
        L_0x0011:
            return
        L_0x0012:
            r0 = 1
            com.millennialmedia.internal.Handshake.initialized = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            com.millennialmedia.internal.Handshake.availableHandshakePlayListServerAdapters = r0
            java.util.Map<java.lang.String, java.lang.Class<? extends com.millennialmedia.internal.playlistserver.PlayListServerAdapter>> r0 = com.millennialmedia.internal.Handshake.availableHandshakePlayListServerAdapters
            java.lang.String r1 = "green"
            java.lang.Class<com.millennialmedia.internal.playlistserver.GreenServerAdapter> r2 = com.millennialmedia.internal.playlistserver.GreenServerAdapter.class
            r0.put(r1, r2)
            java.util.Map<java.lang.String, java.lang.Class<? extends com.millennialmedia.internal.playlistserver.PlayListServerAdapter>> r0 = com.millennialmedia.internal.Handshake.availableHandshakePlayListServerAdapters
            java.lang.String r1 = "orange"
            java.lang.Class<com.millennialmedia.internal.playlistserver.OrangeServerAdapter> r2 = com.millennialmedia.internal.playlistserver.OrangeServerAdapter.class
            r0.put(r1, r2)
            r0 = 0
            boolean r1 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ IOException -> 0x0077, JSONException -> 0x006e }
            if (r1 == 0) goto L_0x003c
            java.lang.String r1 = com.millennialmedia.internal.Handshake.TAG     // Catch:{ IOException -> 0x0077, JSONException -> 0x006e }
            java.lang.String r2 = "Loading packaged default handshake"
            com.millennialmedia.MMLog.d(r1, r2)     // Catch:{ IOException -> 0x0077, JSONException -> 0x006e }
        L_0x003c:
            android.content.Context r1 = com.millennialmedia.internal.utils.EnvironmentUtils.getApplicationContext()     // Catch:{ IOException -> 0x0077, JSONException -> 0x006e }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ IOException -> 0x0077, JSONException -> 0x006e }
            java.lang.String r2 = "mmadsdk/default_handshake.json"
            java.io.InputStream r1 = r1.open(r2)     // Catch:{ IOException -> 0x0077, JSONException -> 0x006e }
            java.lang.String r0 = "UTF-8"
            java.lang.String r0 = com.millennialmedia.internal.utils.IOUtils.read(r1, r0)     // Catch:{ IOException -> 0x0066, JSONException -> 0x0061, all -> 0x005b }
            com.millennialmedia.internal.Handshake$HandshakeInfo r0 = parseHandshake(r0)     // Catch:{ IOException -> 0x0066, JSONException -> 0x0061, all -> 0x005b }
            com.millennialmedia.internal.Handshake.defaultHandshakeInfo = r0     // Catch:{ IOException -> 0x0066, JSONException -> 0x0061, all -> 0x005b }
            com.millennialmedia.internal.utils.IOUtils.closeStream(r1)
            r0 = r1
            goto L_0x0082
        L_0x005b:
            r0 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x00e8
        L_0x0061:
            r0 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x006f
        L_0x0066:
            r0 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0078
        L_0x006b:
            r1 = move-exception
            goto L_0x00e8
        L_0x006e:
            r1 = move-exception
        L_0x006f:
            java.lang.String r2 = com.millennialmedia.internal.Handshake.TAG     // Catch:{ all -> 0x006b }
            java.lang.String r3 = "Could not parse the default handshake."
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x006b }
            goto L_0x007f
        L_0x0077:
            r1 = move-exception
        L_0x0078:
            java.lang.String r2 = com.millennialmedia.internal.Handshake.TAG     // Catch:{ all -> 0x006b }
            java.lang.String r3 = "Could not read default handshake."
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x006b }
        L_0x007f:
            com.millennialmedia.internal.utils.IOUtils.closeStream(r0)
        L_0x0082:
            boolean r1 = com.millennialmedia.MMLog.isDebugEnabled()     // Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7 }
            if (r1 == 0) goto L_0x008f
            java.lang.String r1 = com.millennialmedia.internal.Handshake.TAG     // Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7 }
            java.lang.String r2 = "Loading previously stored handshake"
            com.millennialmedia.MMLog.d(r1, r2)     // Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7 }
        L_0x008f:
            java.io.File r1 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7 }
            java.io.File r2 = com.millennialmedia.internal.utils.EnvironmentUtils.getMillennialDir()     // Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7 }
            java.lang.String r3 = "handshake.json"
            r1.<init>(r2, r3)     // Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7 }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00d9, IOException -> 0x00d0, JSONException -> 0x00c7 }
            java.lang.String r0 = "UTF-8"
            java.lang.String r0 = com.millennialmedia.internal.utils.IOUtils.read(r2, r0)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c0, JSONException -> 0x00bd, all -> 0x00ba }
            com.millennialmedia.internal.Handshake$HandshakeInfo r0 = parseHandshake(r0)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c0, JSONException -> 0x00bd, all -> 0x00ba }
            com.millennialmedia.internal.Handshake.currentHandshakeInfo = r0     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c0, JSONException -> 0x00bd, all -> 0x00ba }
            com.millennialmedia.internal.Handshake$HandshakeInfo r0 = com.millennialmedia.internal.Handshake.currentHandshakeInfo     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c0, JSONException -> 0x00bd, all -> 0x00ba }
            if (r0 != 0) goto L_0x00b6
            java.lang.String r0 = com.millennialmedia.internal.Handshake.TAG     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c0, JSONException -> 0x00bd, all -> 0x00ba }
            java.lang.String r1 = "Unable to create handshake info object"
            com.millennialmedia.MMLog.e(r0, r1)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c0, JSONException -> 0x00bd, all -> 0x00ba }
        L_0x00b6:
            com.millennialmedia.internal.utils.IOUtils.closeStream(r2)
            goto L_0x00e3
        L_0x00ba:
            r1 = move-exception
            r0 = r2
            goto L_0x00e4
        L_0x00bd:
            r1 = move-exception
            r0 = r2
            goto L_0x00c8
        L_0x00c0:
            r1 = move-exception
            r0 = r2
            goto L_0x00d1
        L_0x00c3:
            r0 = r2
            goto L_0x00d9
        L_0x00c5:
            r1 = move-exception
            goto L_0x00e4
        L_0x00c7:
            r1 = move-exception
        L_0x00c8:
            java.lang.String r2 = com.millennialmedia.internal.Handshake.TAG     // Catch:{ all -> 0x00c5 }
            java.lang.String r3 = "Could not parse handshake.json"
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x00c5 }
            goto L_0x00e0
        L_0x00d0:
            r1 = move-exception
        L_0x00d1:
            java.lang.String r2 = com.millennialmedia.internal.Handshake.TAG     // Catch:{ all -> 0x00c5 }
            java.lang.String r3 = "Could not read handshake.json"
            com.millennialmedia.MMLog.e(r2, r3, r1)     // Catch:{ all -> 0x00c5 }
            goto L_0x00e0
        L_0x00d9:
            java.lang.String r1 = com.millennialmedia.internal.Handshake.TAG     // Catch:{ all -> 0x00c5 }
            java.lang.String r2 = "No handshake.json exists."
            com.millennialmedia.MMLog.i(r1, r2)     // Catch:{ all -> 0x00c5 }
        L_0x00e0:
            com.millennialmedia.internal.utils.IOUtils.closeStream(r0)
        L_0x00e3:
            return
        L_0x00e4:
            com.millennialmedia.internal.utils.IOUtils.closeStream(r0)
            throw r1
        L_0x00e8:
            com.millennialmedia.internal.utils.IOUtils.closeStream(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.Handshake.initialize():void");
    }

    public static boolean isRequestInProgress() {
        return requestInProgress.get();
    }

    public static void request(boolean z) {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Requesting handshake, async mode <" + z + ">");
        }
        if (z) {
            TaskFactory.createHandshakeRequestTask().execute();
        } else {
            requestInternal();
        }
    }

    private static void requestInternal() {
        FileOutputStream fileOutputStream;
        if (requestInProgress.compareAndSet(false, true)) {
            int i = 60000;
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("ver", 1);
                jSONObject.put("sdkVer", "6.8.1-72925a6");
                jSONObject.put("os", TapjoyConstants.TJC_DEVICE_PLATFORM_TYPE);
                jSONObject.put("osv", Build.VERSION.RELEASE);
                jSONObject.put(ProviderConfig.MCVideoAdsAppIdKey, EnvironmentUtils.getApplicationContext().getPackageName());
                String str = "https://ads.nexage.com";
                if (currentHandshakeInfo != null && handshakeAttempts < 10) {
                    str = currentHandshakeInfo.handshakeBaseUrl;
                }
                String concat = str.concat(HANDSHAKE_PATH);
                handshakeAttempts++;
                String jSONObject2 = jSONObject.toString();
                if (MMLog.isDebugEnabled()) {
                    MMLog.d(TAG, "Executing handshake request.\n\tattempt: " + handshakeAttempts + "\n\turl: " + concat + "\n\tpost data: " + jSONObject2);
                }
                HttpUtils.Response contentFromPostRequest = HttpUtils.getContentFromPostRequest(concat, jSONObject2, "application/json", 15000);
                if (contentFromPostRequest.code != 200 || contentFromPostRequest.content == null) {
                    MMLog.e(TAG, "Handshake request failed with HTTP response code: " + contentFromPostRequest.code);
                    requestInProgress.set(false);
                    TaskFactory.createHandshakeRequestTask().execute((long) i);
                }
                try {
                    HandshakeInfo parseHandshake = parseHandshake(contentFromPostRequest.content);
                    if (parseHandshake == null) {
                        throw new Exception("Unable to create handshake info object");
                    }
                    currentHandshakeInfo = parseHandshake;
                    fileOutputStream = new FileOutputStream(new File(EnvironmentUtils.getMillennialDir(), HANDSHAKE_JSON));
                    try {
                        IOUtils.write(fileOutputStream, contentFromPostRequest.content);
                    } catch (IOException e) {
                        MMLog.e(TAG, "Error storing handshake response", e);
                    }
                    IOUtils.closeStream(fileOutputStream);
                    int handshakeTtl = getHandshakeTtl();
                    try {
                        handshakeAttempts = 0;
                        i = handshakeTtl;
                    } catch (JSONException e2) {
                        int i2 = handshakeTtl;
                        e = e2;
                        i = i2;
                        MMLog.e(TAG, "An error occurred parsing the handshake response.  Reverting to last known good copy.", e);
                        requestInProgress.set(false);
                        TaskFactory.createHandshakeRequestTask().execute((long) i);
                    } catch (FileNotFoundException e3) {
                        int i3 = handshakeTtl;
                        e = e3;
                        i = i3;
                        MMLog.e(TAG, "Unable to open a file to store the handshake response.", e);
                        requestInProgress.set(false);
                        TaskFactory.createHandshakeRequestTask().execute((long) i);
                    } catch (Exception e4) {
                        int i4 = handshakeTtl;
                        e = e4;
                        i = i4;
                        MMLog.e(TAG, "Exception occurred when trying to load handshake.", e);
                        requestInProgress.set(false);
                        TaskFactory.createHandshakeRequestTask().execute((long) i);
                    }
                    requestInProgress.set(false);
                    TaskFactory.createHandshakeRequestTask().execute((long) i);
                } catch (JSONException e5) {
                    e = e5;
                } catch (FileNotFoundException e6) {
                    e = e6;
                    MMLog.e(TAG, "Unable to open a file to store the handshake response.", e);
                    requestInProgress.set(false);
                    TaskFactory.createHandshakeRequestTask().execute((long) i);
                } catch (Exception e7) {
                    e = e7;
                    MMLog.e(TAG, "Exception occurred when trying to load handshake.", e);
                    requestInProgress.set(false);
                    TaskFactory.createHandshakeRequestTask().execute((long) i);
                } catch (Throwable th) {
                    IOUtils.closeStream(fileOutputStream);
                    throw th;
                }
            } catch (JSONException e8) {
                MMLog.e(TAG, "Cannot build the handshake request data", e8);
            }
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Handshake request already in progress");
        }
    }

    private static HandshakeInfo parseHandshake(String str) throws JSONException {
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Parsing handshake:\n" + str);
        }
        if (str == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        HandshakeInfo handshakeInfo = new HandshakeInfo();
        handshakeInfo.version = jSONObject.getString("ver");
        try {
            int parseInt = Integer.parseInt(handshakeInfo.version);
            if (parseInt > 1) {
                String str3 = TAG;
                MMLog.e(str3, "Handshake response did not contain a compatible version. Received version, " + parseInt + " expected max version of " + 1);
                return null;
            }
            handshakeInfo.config = jSONObject.getString("config");
            JSONObject jSONObject2 = jSONObject.getJSONObject("playlistServer");
            handshakeInfo.activePlaylistServerName = jSONObject2.getString(TJAdUnitConstants.String.USAGE_TRACKER_NAME);
            handshakeInfo.activePlaylistServerBaseUrl = jSONObject2.getString("baseUrl");
            handshakeInfo.handshakeBaseUrl = jSONObject.getString("handshakeBaseUrl");
            handshakeInfo.reportingBaseUrl = jSONObject.getString("rptBaseUrl");
            handshakeInfo.geoIpCheckUrl = jSONObject.optString("geoIpCheckUrl", "https://service.cmp.oath.com/cmp/v0/location/eu");
            handshakeInfo.geoIpCheckTtl = jSONObject.optInt("geoIpCheckTtl", 86400000);
            handshakeInfo.handshakeTtl = jSONObject.getInt("ttl");
            handshakeInfo.sdkEnabled = jSONObject.optBoolean("sdkEnabled", true);
            handshakeInfo.moatEnabled = jSONObject.optBoolean("moatEnabled", true);
            handshakeInfo.reportingBatchSize = jSONObject.getInt("rptBatchSize");
            handshakeInfo.reportingBatchFrequency = jSONObject.getInt("rptFreq");
            handshakeInfo.inlineTimeout = jSONObject.getInt("inlineTmax");
            handshakeInfo.interstitialTimeout = jSONObject.getInt("instlTmax");
            handshakeInfo.nativeTimeout = jSONObject.getInt("nativeTmax");
            handshakeInfo.clientMediationTimeout = jSONObject.getInt("clientAdTmax");
            handshakeInfo.serverToServerTimeout = jSONObject.getInt("serverAdTmax");
            handshakeInfo.exchangeTimeout = jSONObject.getInt("exTmax");
            handshakeInfo.minInlineRefreshRate = jSONObject.getInt("minInlineRefresh");
            handshakeInfo.interstitialExpirationDuration = jSONObject.getInt("instlExpDur");
            handshakeInfo.nativeExpirationDuration = jSONObject.getInt("nativeExpDur");
            handshakeInfo.vastVideoSkipOffsetMax = jSONObject.getInt("vastSkipOffsetMax");
            handshakeInfo.vastVideoSkipOffsetMin = jSONObject.getInt("vastSkipOffsetMin");
            handshakeInfo.nativeTypeDefinitions = loadNativeAdConfig(jSONObject);
            JSONObject optJSONObject = jSONObject.optJSONObject("vpaid");
            handshakeInfo.vpaidStartAdTimeout = JSONUtils.optInt(optJSONObject, "startAdTimeout", 5000);
            handshakeInfo.vpaidSkipAdTimeout = JSONUtils.optInt(optJSONObject, "skipAdTimeout", TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
            handshakeInfo.vpaidAdUnitTimeout = JSONUtils.optInt(optJSONObject, "adUnitTimeout", 5000);
            handshakeInfo.vpaidHtmlEndCardTimeout = JSONUtils.optInt(optJSONObject, "htmlEndCardTimeout", 5000);
            handshakeInfo.vpaidMaxBackButtonDelay = JSONUtils.optInt(optJSONObject, "maxBackButtonDelay", GamesStatusCodes.STATUS_REQUEST_UPDATE_PARTIAL_SUCCESS);
            handshakeInfo.minImpressionDuration = jSONObject.optInt("minImpressionDuration", 0);
            handshakeInfo.minImpressionViewabilityPercent = jSONObject.optInt("minImpressionViewabilityPercent", 0);
            handshakeInfo.superAuctionCacheTimeout = jSONObject.optInt("saCacheTimeout", 600000);
            JSONArray optJSONArray = jSONObject.optJSONArray("exists");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                    if (optJSONObject2 != null) {
                        try {
                            handshakeInfo.existingPackages.put(optJSONObject2.getString("id"), optJSONObject2.getString("pkg"));
                        } catch (JSONException unused) {
                        }
                    }
                }
            }
            if (!MMLog.isDebugEnabled()) {
                return handshakeInfo;
            }
            MMLog.d(TAG, "Handshake successfully parsed");
            return handshakeInfo;
        } catch (NumberFormatException unused2) {
            String str4 = TAG;
            MMLog.e(str4, "Handshake version is not a valid integer, " + handshakeInfo.version);
            return null;
        }
    }

    private static Map<String, NativeTypeDefinition> loadNativeAdConfig(JSONObject jSONObject) throws JSONException {
        JSONObject optJSONObject = jSONObject.getJSONObject("nativeConfig").optJSONObject("typeDefs");
        if (optJSONObject == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        Iterator<String> keys = optJSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            JSONObject jSONObject2 = optJSONObject.getJSONObject(next);
            NativeTypeDefinition nativeTypeDefinition = new NativeTypeDefinition(jSONObject2.getString(TJAdUnitConstants.String.USAGE_TRACKER_NAME));
            JSONObject jSONObject3 = jSONObject2.getJSONObject("components");
            Iterator<String> keys2 = jSONObject3.keys();
            while (keys2.hasNext()) {
                String next2 = keys2.next();
                JSONObject jSONObject4 = jSONObject3.getJSONObject(next2);
                nativeTypeDefinition.componentDefinitions.add(new NativeTypeDefinition.ComponentDefinition(next2, jSONObject4.getInt("publisherRequired"), jSONObject4.getInt("advertiserRequired")));
            }
            hashMap.put(next, nativeTypeDefinition);
        }
        return hashMap;
    }

    public static HandshakeInfo getCurrentHandshakeInfo() {
        if (currentHandshakeInfo != null) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Returning current handshake info");
            }
            return currentHandshakeInfo;
        } else if (defaultHandshakeInfo == null) {
            return new HandshakeInfo();
        } else {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Returning default handshake info");
            }
            return defaultHandshakeInfo;
        }
    }

    public static String getVersion() {
        String str = getCurrentHandshakeInfo().version;
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Handshake version: " + str);
        }
        return str;
    }

    public static String getConfig() {
        String str = getCurrentHandshakeInfo().config;
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Handshake config: " + str);
        }
        return str;
    }

    public static String getActivePlaylistServerName() {
        String str = getCurrentHandshakeInfo().activePlaylistServerName;
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Handshake playlist server name: " + str);
        }
        return str;
    }

    public static String getActivePlaylistServerBaseUrl() {
        String str = getCurrentHandshakeInfo().activePlaylistServerBaseUrl;
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Handshake active playlist server base url: " + str);
        }
        return str;
    }

    public static String getReportingBaseUrl() {
        String str = getCurrentHandshakeInfo().reportingBaseUrl;
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Handshake reporting base url: " + str);
        }
        return str;
    }

    public static String getGeoIpCheckUrl() {
        String str = getCurrentHandshakeInfo().geoIpCheckUrl;
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Handshake geo ip check url: " + str);
        }
        return str;
    }

    public static int getGeoIpCheckTtl() {
        int i = getCurrentHandshakeInfo().geoIpCheckTtl;
        if (i < 1800000 || i > 604800000) {
            i = 86400000;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake geo ip check ttl: " + i);
        }
        return i;
    }

    public static int getHandshakeTtl() {
        int max = Math.max(getCurrentHandshakeInfo().handshakeTtl, 60000);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake handshake ttl: " + max);
        }
        return max;
    }

    public static boolean isSdkEnabled() {
        boolean z = getCurrentHandshakeInfo().sdkEnabled;
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake sdk enabled: " + z);
        }
        return z;
    }

    public static boolean isMoatEnabled() {
        boolean z = getCurrentHandshakeInfo().moatEnabled;
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake moat enabled: " + z);
        }
        return z;
    }

    public static int getReportingBatchSize() {
        int max = Math.max(getCurrentHandshakeInfo().reportingBatchSize, 1);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake reportingBatchSize: " + max);
        }
        return max;
    }

    public static int getReportingBatchFrequency() {
        int max = Math.max(getCurrentHandshakeInfo().reportingBatchFrequency, 120000);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake reporting batch frequency: " + max);
        }
        return max;
    }

    public static int getInlineTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().inlineTimeout, (int) GamesStatusCodes.STATUS_ACHIEVEMENT_UNLOCK_FAILURE);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake inline timeout: " + max);
        }
        return max;
    }

    public static int getInterstitialTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().interstitialTimeout, (int) GamesStatusCodes.STATUS_ACHIEVEMENT_UNLOCK_FAILURE);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake interstitial timeout: " + max);
        }
        return max;
    }

    public static int getNativeTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().nativeTimeout, (int) GamesStatusCodes.STATUS_ACHIEVEMENT_UNLOCK_FAILURE);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake native timeout: " + max);
        }
        return max;
    }

    public static int getClientMediationTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().clientMediationTimeout, 1000);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake client mediation timeout: " + max);
        }
        return max;
    }

    public static int getSuperAuctionCacheTimeout() {
        int i = getCurrentHandshakeInfo().superAuctionCacheTimeout;
        if (i < 1000 || i > 86400000) {
            i = 600000;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake super auction cache timeout: " + i);
        }
        return i;
    }

    public static int getServerToServerTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().serverToServerTimeout, 1000);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake server to server timeout: " + max);
        }
        return max;
    }

    public static int getExchangeTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().exchangeTimeout, 1000);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake exchange timeout: " + max);
        }
        return max;
    }

    public static int getMinInlineRefreshRate() {
        int max = Math.max(getCurrentHandshakeInfo().minInlineRefreshRate, 10000);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake min inline refresh rate: " + max);
        }
        return max;
    }

    public static int getInterstitialExpirationDuration() {
        int max = Math.max(getCurrentHandshakeInfo().interstitialExpirationDuration, 0);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake interstitial expiration: " + max);
        }
        return max;
    }

    public static int getNativeExpirationDuration() {
        int max = Math.max(getCurrentHandshakeInfo().nativeExpirationDuration, 0);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake native expiration duration: " + max);
        }
        return max;
    }

    public static int getMinImpressionViewabilityPercent() {
        int i = getCurrentHandshakeInfo().minImpressionViewabilityPercent;
        if (i < 0 || i > 100) {
            i = 0;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake minimum impression viewability percentage: " + i);
        }
        return i;
    }

    public static int getMinImpressionDuration() {
        int i = getCurrentHandshakeInfo().minImpressionDuration;
        if (i < 0 || i > 60000) {
            i = 0;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake minimum impression duration: " + i);
        }
        return i;
    }

    public static int getVASTVideoSkipOffsetMax() {
        int i = getCurrentHandshakeInfo().vastVideoSkipOffsetMax;
        if (i < 0) {
            i = 0;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake VAST video max skip offset: " + i);
        }
        return i;
    }

    public static int getVASTVideoSkipOffsetMin() {
        int i = getCurrentHandshakeInfo().vastVideoSkipOffsetMin;
        if (i < 0) {
            i = 0;
        }
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake VAST video min skip offset: " + i);
        }
        return i;
    }

    public static int getVPAIDStartAdTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().vpaidStartAdTimeout, 1000);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake VPAID start ad timeout: " + max);
        }
        return max;
    }

    public static int getVPAIDSkipAdTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().vpaidSkipAdTimeout, (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake VPAID skip ad timeout: " + max);
        }
        return max;
    }

    public static int getVPAIDAdUnitTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().vpaidAdUnitTimeout, 1000);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake VPAID ad unit timeout: " + max);
        }
        return max;
    }

    public static int getVPAIDHTMLEndCardTimeout() {
        int max = Math.max(getCurrentHandshakeInfo().vpaidHtmlEndCardTimeout, 0);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake VPAID html end card timeout: " + max);
        }
        return max;
    }

    public static int getVPAIDMaxBackButtonDelay() {
        int max = Math.max(getCurrentHandshakeInfo().vpaidMaxBackButtonDelay, 0);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake VPAID max back button delay: " + max);
        }
        return max;
    }

    public static NativeTypeDefinition getNativeTypeDefinition(String str) {
        NativeTypeDefinition nativeTypeDefinition = getCurrentHandshakeInfo().nativeTypeDefinitions != null ? getCurrentHandshakeInfo().nativeTypeDefinitions.get(str) : null;
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Handshake native type definition: " + nativeTypeDefinition);
        }
        return nativeTypeDefinition;
    }

    public static Map<String, NativeTypeDefinition> getNativeTypeDefinitions() {
        Map<String, NativeTypeDefinition> map = getCurrentHandshakeInfo().nativeTypeDefinitions;
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake native type definitions: " + map);
        }
        return map;
    }

    public static Class<? extends PlayListServerAdapter> getActivePlayListServerAdapterClass() {
        Class<? extends PlayListServerAdapter> cls = availableHandshakePlayListServerAdapters.get(getActivePlaylistServerName());
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake active playlist server adapter class: " + cls);
        }
        return cls;
    }

    public static List<String> getExistingIds() {
        ArrayList arrayList = new ArrayList();
        Map<String, String> map = getCurrentHandshakeInfo().existingPackages;
        for (String next : map.keySet()) {
            if (Utils.isPackageAvailable(map.get(next))) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public static Map<String, String> getExistingPackages() {
        HashMap hashMap = new HashMap(getCurrentHandshakeInfo().existingPackages);
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Handshake existing packages: " + hashMap);
        }
        return hashMap;
    }
}
