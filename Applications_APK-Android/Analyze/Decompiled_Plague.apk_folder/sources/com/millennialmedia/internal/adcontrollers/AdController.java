package com.millennialmedia.internal.adcontrollers;

import com.millennialmedia.MMLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AdController {
    private static final String TAG = "AdController";
    private static List<AdController> registeredControllers = new ArrayList();

    public abstract boolean canHandleContent(String str);

    public abstract void close();

    public abstract void release();

    public static void registerPackagedControllers() {
        registerController(new LightboxController());
        registerController(new VASTVideoController());
        registerController(new NativeController());
        registerController(new WebController());
    }

    public static void registerController(AdController adController) {
        if (adController == null) {
            throw new IllegalArgumentException("Unable to register ad controller, specified controller cannot be null");
        } else if (registeredControllers.contains(adController)) {
            String str = TAG;
            MMLog.w(str, "Ad controller <" + adController.getClass() + "> already registered");
        } else {
            if (MMLog.isDebugEnabled()) {
                String str2 = TAG;
                MMLog.d(str2, "Registering ad controller <" + adController.getClass() + ">");
            }
            registeredControllers.add(adController);
        }
    }

    public static Class<?> getControllerClassForContent(String str) {
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Attempting to get controller class for ad content.\n" + str);
        }
        Class<?> cls = null;
        Iterator<AdController> it = registeredControllers.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AdController next = it.next();
            if (next.canHandleContent(str)) {
                cls = next.getClass();
                if (MMLog.isDebugEnabled()) {
                    String str3 = TAG;
                    MMLog.d(str3, "Found controller class <" + cls.getName() + "> for ad content");
                }
            }
        }
        if (cls == null) {
            String str4 = TAG;
            MMLog.e(str4, "Unable to find AdController for content <" + str + ">");
        }
        return cls;
    }
}
