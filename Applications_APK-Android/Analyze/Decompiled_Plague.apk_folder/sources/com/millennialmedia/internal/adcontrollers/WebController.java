package com.millennialmedia.internal.adcontrollers;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdMetadata;
import com.millennialmedia.internal.MMActivity;
import com.millennialmedia.internal.MMWebView;
import com.millennialmedia.internal.SizableStateManager;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.ViewUtils;
import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class WebController extends AdController {
    /* access modifiers changed from: private */
    public static final String TAG = "WebController";
    private static final Pattern patternHeadOrBody = Pattern.compile("<HEAD|<BODY", 2);
    private static final Pattern patternHtml = Pattern.compile("<HTML", 2);
    private static final Pattern patternOtherHtmlTags = Pattern.compile("<SCRIPT|<IMG|<A|<DIV|<SPAN|<P|<H1|<H2|<H3|<H4|<H5|<H6|<IFRAME", 2);
    /* access modifiers changed from: private */
    public volatile MMWebView mmWebView;
    /* access modifiers changed from: private */
    public volatile SizableStateManager sizableStateManager;
    /* access modifiers changed from: private */
    public volatile MMWebView twoPartWebView;
    /* access modifiers changed from: private */
    public WebControllerListener webControllerListener;

    public interface WebControllerListener {
        void attachFailed();

        void attachSucceeded();

        void initFailed();

        void initSucceeded();

        void onAdLeftApplication();

        void onClicked();

        void onCollapsed();

        void onExpanded();

        void onResize(int i, int i2);

        void onResized(int i, int i2, boolean z);

        void onUnload();
    }

    public static class WebControllerOptions {
        public final boolean enableEnhancedAdControl;
        public final boolean enableMoat;
        public final boolean immersive;
        public final boolean interstitial;
        public final boolean twoPart;

        public WebControllerOptions(boolean z, boolean z2, boolean z3) {
            this(z, z2, z3, false);
        }

        public WebControllerOptions(boolean z, boolean z2, boolean z3, boolean z4) {
            this(z, z2, z3, z4, true);
        }

        public WebControllerOptions(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
            this.interstitial = z;
            this.enableMoat = z2;
            this.enableEnhancedAdControl = z3;
            this.twoPart = z4;
            this.immersive = z5;
        }
    }

    public WebController() {
    }

    public WebController(WebControllerListener webControllerListener2) {
        this.webControllerListener = webControllerListener2;
    }

    public void init(Context context, String str, AdMetadata adMetadata, WebControllerOptions webControllerOptions) {
        final Context context2 = context;
        final WebControllerOptions webControllerOptions2 = webControllerOptions;
        final AdMetadata adMetadata2 = adMetadata;
        final String str2 = str;
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                MMWebView unused = WebController.this.mmWebView = WebController.this.createWebView(context2, webControllerOptions2, adMetadata2, WebController.this.webControllerListener);
                WebController.this.mmWebView.setContent(str2);
            }
        });
    }

    /* access modifiers changed from: private */
    public MMWebView getWebView() {
        if (this.mmWebView != null) {
            return this.mmWebView;
        }
        if (!MMLog.isDebugEnabled()) {
            return null;
        }
        MMLog.e(TAG, "MMWebView has not been created or has been released.");
        return null;
    }

    public boolean canHandleContent(String str) {
        if (str == null) {
            return false;
        }
        try {
            new JSONObject(str);
            return false;
        } catch (JSONException unused) {
            Matcher matcher = patternHeadOrBody.matcher(str);
            if (matcher.find()) {
                return true;
            }
            matcher.usePattern(patternHtml);
            if (matcher.find()) {
                return false;
            }
            matcher.usePattern(patternOtherHtmlTags);
            if (matcher.find()) {
                return true;
            }
            return false;
        }
    }

    public void attach(final RelativeLayout relativeLayout, final RelativeLayout.LayoutParams layoutParams) {
        if (relativeLayout == null) {
            this.webControllerListener.attachFailed();
            return;
        }
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WebController.this.webControllerListener.onClicked();
            }
        });
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                MMWebView access$200 = WebController.this.getWebView();
                if (access$200 == null) {
                    MMLog.e(WebController.TAG, "MMWebView instance is null, unable to attach");
                    WebController.this.webControllerListener.attachFailed();
                    return;
                }
                ViewUtils.attachView(relativeLayout, access$200, layoutParams);
                WebController.this.webControllerListener.attachSucceeded();
            }
        });
    }

    public void showExpanded(final MMActivity.MMActivityConfig mMActivityConfig) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                MMWebView access$200 = WebController.this.getWebView();
                if (access$200 == null) {
                    MMLog.e(WebController.TAG, "MMWebView instance is null, unable to expand");
                    WebController.this.webControllerListener.attachFailed();
                    return;
                }
                SizableStateManager sizableStateManager = WebController.this.getSizableStateManager();
                SizableStateManager.ExpandParams expandParams = new SizableStateManager.ExpandParams();
                expandParams.width = -1;
                expandParams.height = -1;
                expandParams.orientation = -1;
                if (!sizableStateManager.expand(access$200, expandParams, mMActivityConfig)) {
                    WebController.this.webControllerListener.attachFailed();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public SizableStateManager getSizableStateManager() {
        if (this.sizableStateManager == null) {
            this.sizableStateManager = new SizableStateManager(new SizableStateManager.SizableListener() {
                public void onResizing(int i, int i2) {
                    MMWebView access$200 = WebController.this.getWebView();
                    if (access$200 != null) {
                        access$200.setStateResizing();
                        WebController.this.webControllerListener.onResize(i, i2);
                    }
                }

                public void onResized(int i, int i2) {
                    MMWebView access$200 = WebController.this.getWebView();
                    if (access$200 != null) {
                        access$200.setStateResized();
                        WebController.this.webControllerListener.onResized(i, i2, false);
                    }
                }

                public void onUnresizing(int i, int i2) {
                    MMWebView access$200 = WebController.this.getWebView();
                    if (access$200 != null) {
                        access$200.setStateResizing();
                    }
                }

                public void onUnresized(int i, int i2) {
                    MMWebView access$200 = WebController.this.getWebView();
                    if (access$200 != null) {
                        access$200.setStateUnresized();
                        WebController.this.webControllerListener.onResized(i, i2, true);
                        WebController.this.releaseFromSizableState();
                    }
                }

                public void onExpanding() {
                    MMWebView access$200 = WebController.this.getWebView();
                    if (access$200 != null) {
                        access$200.setStateResizing();
                    }
                }

                public void onExpanded() {
                    MMWebView access$200 = WebController.this.getWebView();
                    if (access$200 != null) {
                        access$200.setStateExpanded();
                        WebController.this.webControllerListener.onExpanded();
                    }
                }

                public void onCollapsing() {
                    MMWebView access$200 = WebController.this.getWebView();
                    if (access$200 != null) {
                        access$200.setStateResizing();
                    }
                }

                public void onCollapsed() {
                    MMWebView access$200 = WebController.this.getWebView();
                    if (access$200 != null) {
                        access$200.setStateCollapsed();
                        WebController.this.webControllerListener.onCollapsed();
                        WebController.this.releaseFromSizableState();
                    }
                }

                public void onExpandFailed() {
                    WebController.this.webControllerListener.attachFailed();
                    WebController.this.releaseFromSizableState();
                }
            });
        }
        return this.sizableStateManager;
    }

    /* access modifiers changed from: package-private */
    public MMWebView createWebView(Context context, WebControllerOptions webControllerOptions, AdMetadata adMetadata, WebControllerListener webControllerListener2) {
        boolean z = adMetadata != null && adMetadata.isTransparent() && webControllerOptions.interstitial;
        final WeakReference weakReference = new WeakReference(context);
        final WebControllerOptions webControllerOptions2 = webControllerOptions;
        final WebControllerListener webControllerListener3 = webControllerListener2;
        final AdMetadata adMetadata2 = adMetadata;
        return new MMWebView(context, new MMWebView.MMWebViewOptions(webControllerOptions.interstitial, z, webControllerOptions.enableMoat, webControllerOptions.enableEnhancedAdControl), new MMWebView.MMWebViewListener() {
            public void onReady() {
            }

            public void onLoaded() {
                if (!webControllerOptions2.twoPart) {
                    webControllerListener3.initSucceeded();
                }
            }

            public void onFailed() {
                if (!webControllerOptions2.twoPart) {
                    webControllerListener3.initFailed();
                }
            }

            public void onClicked() {
                webControllerListener3.onClicked();
            }

            public void onAdLeftApplication() {
                webControllerListener3.onAdLeftApplication();
            }

            public void close() {
                WebController.this.close();
            }

            public boolean expand(SizableStateManager.ExpandParams expandParams) {
                boolean z;
                MMWebView access$200 = WebController.this.getWebView();
                if (access$200 == null) {
                    MMLog.e(WebController.TAG, "MMWebView instance is null, unable to expand");
                    return false;
                }
                SizableStateManager sizableStateManager = WebController.this.getSizableStateManager();
                expandParams.immersive = webControllerOptions2.immersive;
                if (TextUtils.isEmpty(expandParams.url)) {
                    z = !webControllerOptions2.interstitial;
                } else {
                    Context context = (Context) weakReference.get();
                    if (context == null) {
                        MMLog.e(WebController.TAG, "Context is no longer valid, unable to expand");
                        return false;
                    }
                    MMWebView unused = WebController.this.twoPartWebView = WebController.this.createWebView(context, new WebControllerOptions(false, webControllerOptions2.enableMoat, webControllerOptions2.enableEnhancedAdControl, true), adMetadata2, webControllerListener3);
                    WebController.this.twoPartWebView.setTwoPartExpand();
                    WebController.this.twoPartWebView.setVisibility(4);
                    WebController.this.loadTwoPartContentAsync(WebController.this.twoPartWebView, expandParams);
                    access$200 = WebController.this.twoPartWebView;
                    z = false;
                }
                if (adMetadata2 != null && adMetadata2.isTransparent()) {
                    access$200.setBackgroundColor(0);
                    expandParams.transparent = true;
                }
                return sizableStateManager.expand(access$200, expandParams, z);
            }

            public boolean resize(SizableStateManager.ResizeParams resizeParams) {
                MMWebView access$200 = WebController.this.getWebView();
                if (access$200 != null) {
                    return WebController.this.getSizableStateManager().resize(access$200, resizeParams);
                }
                MMLog.e(WebController.TAG, "MMWebView instance is null, unable to resize");
                return false;
            }

            public void setOrientation(int i) {
                if (WebController.this.sizableStateManager != null) {
                    WebController.this.sizableStateManager.setOrientation(i);
                }
            }

            public void onUnload() {
                if (WebController.this.sizableStateManager != null && WebController.this.sizableStateManager.isExpanded()) {
                    WebController.this.sizableStateManager.close();
                }
                webControllerListener3.onUnload();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void loadTwoPartContentAsync(MMWebView mMWebView, final SizableStateManager.ExpandParams expandParams) {
        final WeakReference weakReference = new WeakReference(this.sizableStateManager);
        final WeakReference weakReference2 = new WeakReference(mMWebView);
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                WebController.this.sizableStateManager.showLoadingSpinner(expandParams);
                final HttpUtils.Response contentFromGetRequest = HttpUtils.getContentFromGetRequest(expandParams.url);
                ThreadUtils.postOnUiThread(new Runnable() {
                    public void run() {
                        MMWebView mMWebView = (MMWebView) weakReference2.get();
                        if (mMWebView != null) {
                            SizableStateManager sizableStateManager = (SizableStateManager) weakReference.get();
                            if (sizableStateManager == null) {
                                if (MMLog.isDebugEnabled()) {
                                    MMLog.d(WebController.TAG, "Sizing container is no longer valid");
                                }
                            } else if (sizableStateManager.isExpanded()) {
                                sizableStateManager.hideLoadingSpinner(expandParams);
                                if (contentFromGetRequest == null || contentFromGetRequest.code != 200 || contentFromGetRequest.content == null) {
                                    MMLog.e(WebController.TAG, "Unable to retrieve expanded content");
                                } else {
                                    mMWebView.setContent(contentFromGetRequest.content);
                                }
                                mMWebView.setVisibility(0);
                            } else if (MMLog.isDebugEnabled()) {
                                MMLog.d(WebController.TAG, "Sizing container has been collapsed");
                            }
                        } else if (MMLog.isDebugEnabled()) {
                            MMLog.d(WebController.TAG, "Expanded web view is no longer valid");
                        }
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public void releaseFromSizableState() {
        if (this.twoPartWebView != null) {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    WebController.this.twoPartWebView.release();
                    MMWebView unused = WebController.this.twoPartWebView = null;
                }
            });
        }
        this.sizableStateManager = null;
    }

    public void release() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (WebController.this.mmWebView != null) {
                    WebController.this.mmWebView.release();
                    MMWebView unused = WebController.this.mmWebView = null;
                }
            }
        });
    }

    public void close() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (WebController.this.sizableStateManager != null) {
                    WebController.this.sizableStateManager.close();
                }
                MMWebView access$200 = WebController.this.getWebView();
                if (access$200 != null) {
                    access$200.setBackgroundColor(-1);
                }
            }
        });
    }
}
