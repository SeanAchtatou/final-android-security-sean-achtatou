package com.millennialmedia.internal.adcontrollers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.view.View;
import android.view.ViewGroup;
import com.millennialmedia.MMLog;
import com.millennialmedia.XIncentivizedEventListener;
import com.millennialmedia.internal.AdContainer;
import com.millennialmedia.internal.MMActivity;
import com.millennialmedia.internal.SizableStateManager;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.TrackingEvent;
import com.millennialmedia.internal.utils.Utils;
import com.millennialmedia.internal.utils.ViewUtils;
import com.millennialmedia.internal.video.VASTParser;
import com.millennialmedia.internal.video.VASTVideoView;
import com.millennialmedia.internal.video.VPAIDWebView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

public class VASTVideoController extends AdController {
    private static final int MAX_WRAPPERS = 3;
    /* access modifiers changed from: private */
    public static final String TAG = "VASTVideoController";
    /* access modifiers changed from: private */
    public VASTParser.InLineAd inLineAd;
    /* access modifiers changed from: private */
    public VASTVideoControllerListener listener;
    /* access modifiers changed from: private */
    public List<String> vastDocuments;
    /* access modifiers changed from: private */
    public ViewGroup videoView;
    /* access modifiers changed from: private */
    public List<VASTParser.WrapperAd> wrapperAds;

    public interface VASTVideoControllerListener {
        void attachFailed();

        void attachSucceeded();

        void close();

        void initFailed();

        void initSucceeded();

        void onAdLeftApplication();

        void onClick();

        void onIncentiveEarned(XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent);

        void onUnload();
    }

    public interface VideoViewActions {
        boolean onBackPressed();

        void release();

        void updateLayout();
    }

    public VASTVideoController() {
    }

    public VASTVideoController(VASTVideoControllerListener vASTVideoControllerListener) {
        this.listener = vASTVideoControllerListener;
    }

    public void init(final Context context, final String str) {
        this.wrapperAds = new ArrayList();
        this.vastDocuments = new ArrayList();
        if (!EnvironmentUtils.isExternalStorageWritable()) {
            MMLog.w(TAG, "External storage is not writeable.  Unable to load VAST video interstitial.");
            this.listener.initFailed();
            return;
        }
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                try {
                    VASTVideoController.this.loadAd(str);
                    if (VASTVideoController.this.inLineAd == null) {
                        MMLog.e(VASTVideoController.TAG, "VAST content did not produce a valid InLineAd instance.");
                        VASTVideoController.this.fireErrorUrls();
                        VASTVideoController.this.listener.initFailed();
                        return;
                    }
                    if (VASTVideoController.this.wrapperAds != null) {
                        for (VASTParser.WrapperAd wrapperAd : VASTVideoController.this.wrapperAds) {
                            if (wrapperAd.impressions.isEmpty()) {
                                MMLog.e(VASTVideoController.TAG, "WrapperAd must contain at least one Impression URL.");
                                VASTVideoController.this.fireErrorUrls();
                                VASTVideoController.this.listener.initFailed();
                                return;
                            }
                        }
                    }
                    if (VASTVideoController.isVPAIDOnly(VASTVideoController.this.inLineAd)) {
                        VASTVideoController.this.createVPAIDWebView(context);
                    } else {
                        VASTVideoController.this.createVASTVideoView(context);
                    }
                } catch (XmlPullParserException e) {
                    MMLog.e(VASTVideoController.TAG, "VAST XML Parsing error.", e);
                    VASTVideoController.this.fireErrorUrls();
                    VASTVideoController.this.listener.initFailed();
                } catch (IOException e2) {
                    MMLog.e(VASTVideoController.TAG, "VAST XML I/O error.", e2);
                    VASTVideoController.this.fireErrorUrls();
                    VASTVideoController.this.listener.initFailed();
                }
            }
        });
    }

    public void createVPAIDWebView(final Context context) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                VPAIDWebView vPAIDWebView = new VPAIDWebView(new MutableContextWrapper(context), false, new VPAIDWebView.VPAIDVideoViewListener() {
                    public boolean expand(SizableStateManager.ExpandParams expandParams) {
                        return false;
                    }

                    public void onReady() {
                    }

                    public boolean resize(SizableStateManager.ResizeParams resizeParams) {
                        return false;
                    }

                    public void setOrientation(int i) {
                    }

                    public void onIncentiveEarned(XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent) {
                        VASTVideoController.this.listener.onIncentiveEarned(xIncentiveEvent);
                    }

                    public void onLoaded() {
                        VASTVideoController.this.listener.initSucceeded();
                    }

                    public void onFailed() {
                        VASTVideoController.this.listener.initFailed();
                    }

                    public void onClicked() {
                        VASTVideoController.this.listener.onClick();
                    }

                    public void onAdLeftApplication() {
                        VASTVideoController.this.listener.onAdLeftApplication();
                    }

                    public void close() {
                        VASTVideoController.this.close();
                    }

                    public void onUnload() {
                        VASTVideoController.this.listener.onUnload();
                    }
                });
                vPAIDWebView.setTag("mmVpaidWebView");
                ViewGroup unused = VASTVideoController.this.videoView = vPAIDWebView;
                vPAIDWebView.setVastDocuments(VASTVideoController.this.vastDocuments);
            }
        });
    }

    public void createVASTVideoView(final Context context) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                ViewGroup unused = VASTVideoController.this.videoView = new VASTVideoView(new MutableContextWrapper(context), VASTVideoController.this.inLineAd, VASTVideoController.this.wrapperAds, new VASTVideoView.VASTVideoViewListener() {
                    public void onLoaded() {
                        VASTVideoController.this.listener.initSucceeded();
                    }

                    public void onFailed() {
                        VASTVideoController.this.fireErrorUrls();
                        VASTVideoController.this.listener.initFailed();
                    }

                    public void onClicked() {
                        VASTVideoController.this.listener.onClick();
                    }

                    public void onIncentiveEarned(XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent) {
                        VASTVideoController.this.listener.onIncentiveEarned(xIncentiveEvent);
                    }

                    public void onAdLeftApplication() {
                        VASTVideoController.this.listener.onAdLeftApplication();
                    }

                    public void close() {
                        VASTVideoController.this.close();
                    }
                });
                VASTVideoController.this.videoView.setTag("mmVastVideoView");
            }
        });
    }

    @SuppressLint({"DefaultLocale"})
    public boolean canHandleContent(String str) {
        if (Utils.isEmpty(str)) {
            return false;
        }
        String upperCase = str.toUpperCase();
        int indexOf = upperCase.indexOf("<VAST");
        int indexOf2 = upperCase.indexOf("<AD");
        int indexOf3 = upperCase.indexOf("</VAST>");
        if (indexOf < 0 || indexOf >= indexOf2 || indexOf2 >= indexOf3) {
            return false;
        }
        return true;
    }

    public void attach(MMActivity mMActivity) {
        ViewGroup rootView = mMActivity.getRootView();
        if (rootView == null) {
            this.listener.attachFailed();
            return;
        }
        Context context = rootView.getContext();
        if (!(context instanceof Activity)) {
            this.listener.attachFailed();
            return;
        }
        final AdContainer adContainer = new AdContainer((Activity) context, null);
        adContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VASTVideoController.this.listener.onClick();
            }
        });
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (VASTVideoController.this.videoView == null) {
                    MMLog.e(VASTVideoController.TAG, "videoView instance is null, unable to attach");
                    VASTVideoController.this.listener.attachFailed();
                    return;
                }
                VASTVideoController.this.videoView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                ViewUtils.attachView(adContainer, VASTVideoController.this.videoView);
                if (VASTVideoController.this.videoView instanceof VideoViewActions) {
                    ((VideoViewActions) VASTVideoController.this.videoView).updateLayout();
                }
                VASTVideoController.this.listener.attachSucceeded();
            }
        });
        ViewUtils.attachView(rootView, adContainer);
    }

    /* access modifiers changed from: private */
    public void loadAd(String str) throws XmlPullParserException, IOException {
        this.vastDocuments.add(str);
        VASTParser.Ad parse = VASTParser.parse(str);
        if (parse == null) {
            fireErrorUrls();
            this.listener.initFailed();
        } else if (parse instanceof VASTParser.InLineAd) {
            this.inLineAd = (VASTParser.InLineAd) parse;
        } else if (parse instanceof VASTParser.WrapperAd) {
            VASTParser.WrapperAd wrapperAd = (VASTParser.WrapperAd) parse;
            this.wrapperAds.add(wrapperAd);
            if (this.wrapperAds.size() > 3 || wrapperAd.adTagURI == null || wrapperAd.adTagURI.isEmpty()) {
                MMLog.e(TAG, "VAST wrapper did not contain a valid ad tag URI or MAX VAST Redirects exceeded.");
                return;
            }
            if (MMLog.isDebugEnabled()) {
                String str2 = TAG;
                MMLog.d(str2, "Requesting VAST tag URI = " + wrapperAd.adTagURI);
            }
            HttpUtils.Response contentFromGetRequest = HttpUtils.getContentFromGetRequest(wrapperAd.adTagURI);
            if (contentFromGetRequest.code == 200) {
                loadAd(contentFromGetRequest.content);
                return;
            }
            String str3 = TAG;
            MMLog.e(str3, "Received HTTP status code = " + contentFromGetRequest.code + " when processing ad tag URI = " + wrapperAd.adTagURI);
        }
    }

    /* access modifiers changed from: private */
    public void fireErrorUrls() {
        ArrayList arrayList = new ArrayList();
        if (this.inLineAd != null && !Utils.isEmpty(this.inLineAd.error)) {
            arrayList.add(new TrackingEvent("error", this.inLineAd.error));
        }
        if (this.wrapperAds != null) {
            for (VASTParser.WrapperAd next : this.wrapperAds) {
                if (!Utils.isEmpty(next.error)) {
                    arrayList.add(new TrackingEvent("error", next.error));
                }
            }
        }
        TrackingEvent.fireEvents(arrayList);
    }

    public void release() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (VASTVideoController.this.videoView instanceof VideoViewActions) {
                    ((VideoViewActions) VASTVideoController.this.videoView).release();
                    ViewGroup unused = VASTVideoController.this.videoView = null;
                }
            }
        });
    }

    public void close() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (VASTVideoController.this.listener != null) {
                    VASTVideoController.this.listener.close();
                }
            }
        });
    }

    public boolean onBackPressed() {
        if (this.videoView instanceof VideoViewActions) {
            return ((VideoViewActions) this.videoView).onBackPressed();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public ViewGroup getVideoView() {
        return this.videoView;
    }

    private static boolean isJavascript(String str) {
        return "application/javascript".equalsIgnoreCase(str) || "application/x-javascript".equalsIgnoreCase(str) || "text/javascript".equalsIgnoreCase(str);
    }

    public static boolean isVPAIDOnly(VASTParser.InLineAd inLineAd2) {
        if (inLineAd2.creatives == null) {
            return false;
        }
        boolean z = false;
        for (VASTParser.Creative creative : inLineAd2.creatives) {
            if (!(creative.linearAd == null || creative.linearAd.mediaFiles == null)) {
                for (VASTParser.MediaFile next : creative.linearAd.mediaFiles) {
                    if (!"VPAID".equalsIgnoreCase(next.apiFramework) || !isJavascript(next.contentType)) {
                        if (MMLog.isDebugEnabled()) {
                            MMLog.d(TAG, "Detected non-VPAID video content");
                        }
                        return false;
                    }
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(TAG, "Detected VPAID video content");
                    }
                    z = true;
                }
                continue;
            }
        }
        return z;
    }
}
