package com.millennialmedia.internal.adcontrollers;

import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.JSONUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NativeController extends AdController {
    private static final String TAG = "NativeController";
    public List<Asset> assets;
    public List<String> impTrackers;
    public String jsTracker;
    public Link link;
    private NativeControllerListener nativeControllerListener;
    public int version = 1;

    public static class Link {
        public List<String> clickTrackerUrls;
        public String fallback;
        public String url;
    }

    public interface NativeControllerListener {
        void initFailed(Throwable th);

        void initSucceeded();
    }

    public void close() {
    }

    public static class Asset {
        public Data data;
        public int id;
        public Image image;
        public Link link;
        public boolean required = false;
        public Title title;
        public Type type;
        public Video video;

        public static class Data {
            public String label;
            public String value;
        }

        public static class Image {
            public Integer height;
            public String url;
            public Integer width;
        }

        public static class Title {
            public String value;
        }

        public enum Type {
            TITLE,
            IMAGE,
            VIDEO,
            DATA,
            UNKNOWN
        }

        public static class Video {
            public String vastTag;
        }

        Asset(Type type2, int i, boolean z) {
            this.type = type2;
            this.id = i;
            this.required = z;
        }
    }

    public NativeController() {
    }

    public NativeController(NativeControllerListener nativeControllerListener2) {
        this.nativeControllerListener = nativeControllerListener2;
    }

    public void init(String str) {
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Loading native assets " + hashCode());
        }
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject(TapjoyConstants.TJC_PLUGIN_NATIVE);
            this.version = jSONObject.optInt("ver", this.version);
            loadAssets(jSONObject.getJSONArray("assets"));
            this.link = loadLink(jSONObject.getJSONObject("link"));
            JSONArray optJSONArray = jSONObject.optJSONArray("imptrackers");
            if (optJSONArray != null) {
                this.impTrackers = new CopyOnWriteArrayList(JSONUtils.convertToStringArray(optJSONArray));
            }
            this.jsTracker = jSONObject.optString("jstracker", null);
            this.nativeControllerListener.initSucceeded();
        } catch (JSONException e) {
            MMLog.e(TAG, "Initialization of the native controller instance failed", e);
            this.nativeControllerListener.initFailed(e);
        }
    }

    public void release() {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Releasing native assets " + hashCode());
        }
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (NativeController.this.assets != null) {
                    NativeController.this.assets.clear();
                }
                if (NativeController.this.impTrackers != null) {
                    NativeController.this.impTrackers.clear();
                }
            }
        });
    }

    public boolean canHandleContent(String str) {
        try {
            new JSONObject(str).getJSONObject(TapjoyConstants.TJC_PLUGIN_NATIVE);
            return true;
        } catch (JSONException unused) {
            return false;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:16|17|18|19|20|21|22) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0080 */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f9 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadAssets(org.json.JSONArray r11) throws org.json.JSONException {
        /*
            r10 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            r2 = r1
        L_0x0007:
            int r3 = r11.length()
            if (r2 >= r3) goto L_0x00fd
            org.json.JSONObject r3 = r11.getJSONObject(r2)
            java.lang.String r4 = "id"
            int r4 = r3.getInt(r4)
            java.lang.String r5 = "required"
            int r5 = r3.optInt(r5)
            if (r5 <= 0) goto L_0x0021
            r5 = 1
            goto L_0x0022
        L_0x0021:
            r5 = r1
        L_0x0022:
            java.lang.String r6 = "title"
            boolean r6 = r3.has(r6)
            r7 = 0
            if (r6 == 0) goto L_0x004c
            java.lang.String r6 = "title"
            org.json.JSONObject r6 = r3.getJSONObject(r6)     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset r8 = new com.millennialmedia.internal.adcontrollers.NativeController$Asset     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Type r9 = com.millennialmedia.internal.adcontrollers.NativeController.Asset.Type.TITLE     // Catch:{ JSONException -> 0x00e8 }
            r8.<init>(r9, r4, r5)     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Title r4 = new com.millennialmedia.internal.adcontrollers.NativeController$Asset$Title     // Catch:{ JSONException -> 0x00e8 }
            r4.<init>()     // Catch:{ JSONException -> 0x00e8 }
            r8.title = r4     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Title r4 = r8.title     // Catch:{ JSONException -> 0x00e8 }
            java.lang.String r5 = "text"
            java.lang.String r5 = r6.getString(r5)     // Catch:{ JSONException -> 0x00e8 }
            r4.value = r5     // Catch:{ JSONException -> 0x00e8 }
        L_0x0049:
            r7 = r8
            goto L_0x00e8
        L_0x004c:
            java.lang.String r6 = "img"
            boolean r6 = r3.has(r6)
            if (r6 == 0) goto L_0x008f
            java.lang.String r6 = "img"
            org.json.JSONObject r6 = r3.getJSONObject(r6)     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset r8 = new com.millennialmedia.internal.adcontrollers.NativeController$Asset     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Type r9 = com.millennialmedia.internal.adcontrollers.NativeController.Asset.Type.IMAGE     // Catch:{ JSONException -> 0x00e8 }
            r8.<init>(r9, r4, r5)     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Image r4 = new com.millennialmedia.internal.adcontrollers.NativeController$Asset$Image     // Catch:{ JSONException -> 0x00e8 }
            r4.<init>()     // Catch:{ JSONException -> 0x00e8 }
            r8.image = r4     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Image r4 = r8.image     // Catch:{ JSONException -> 0x00e8 }
            java.lang.String r5 = "url"
            java.lang.String r5 = r6.getString(r5)     // Catch:{ JSONException -> 0x00e8 }
            r4.url = r5     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Image r4 = r8.image     // Catch:{ JSONException -> 0x0080 }
            java.lang.String r5 = "w"
            int r5 = r6.getInt(r5)     // Catch:{ JSONException -> 0x0080 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ JSONException -> 0x0080 }
            r4.width = r5     // Catch:{ JSONException -> 0x0080 }
        L_0x0080:
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Image r4 = r8.image     // Catch:{ JSONException -> 0x0049 }
            java.lang.String r5 = "h"
            int r5 = r6.getInt(r5)     // Catch:{ JSONException -> 0x0049 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ JSONException -> 0x0049 }
            r4.height = r5     // Catch:{ JSONException -> 0x0049 }
            goto L_0x0049
        L_0x008f:
            java.lang.String r6 = "video"
            boolean r6 = r3.has(r6)
            if (r6 == 0) goto L_0x00b6
            java.lang.String r6 = "video"
            org.json.JSONObject r6 = r3.getJSONObject(r6)     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset r8 = new com.millennialmedia.internal.adcontrollers.NativeController$Asset     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Type r9 = com.millennialmedia.internal.adcontrollers.NativeController.Asset.Type.VIDEO     // Catch:{ JSONException -> 0x00e8 }
            r8.<init>(r9, r4, r5)     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Video r4 = new com.millennialmedia.internal.adcontrollers.NativeController$Asset$Video     // Catch:{ JSONException -> 0x00e8 }
            r4.<init>()     // Catch:{ JSONException -> 0x00e8 }
            r8.video = r4     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Video r4 = r8.video     // Catch:{ JSONException -> 0x00e8 }
            java.lang.String r5 = "vasttag"
            java.lang.String r5 = r6.getString(r5)     // Catch:{ JSONException -> 0x00e8 }
            r4.vastTag = r5     // Catch:{ JSONException -> 0x00e8 }
            goto L_0x0049
        L_0x00b6:
            java.lang.String r6 = "data"
            boolean r6 = r3.has(r6)
            if (r6 == 0) goto L_0x00e8
            java.lang.String r6 = "data"
            org.json.JSONObject r6 = r3.getJSONObject(r6)     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset r8 = new com.millennialmedia.internal.adcontrollers.NativeController$Asset     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Type r9 = com.millennialmedia.internal.adcontrollers.NativeController.Asset.Type.DATA     // Catch:{ JSONException -> 0x00e8 }
            r8.<init>(r9, r4, r5)     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Data r4 = new com.millennialmedia.internal.adcontrollers.NativeController$Asset$Data     // Catch:{ JSONException -> 0x00e8 }
            r4.<init>()     // Catch:{ JSONException -> 0x00e8 }
            r8.data = r4     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Data r4 = r8.data     // Catch:{ JSONException -> 0x00e8 }
            java.lang.String r5 = "value"
            java.lang.String r5 = r6.getString(r5)     // Catch:{ JSONException -> 0x00e8 }
            r4.value = r5     // Catch:{ JSONException -> 0x00e8 }
            com.millennialmedia.internal.adcontrollers.NativeController$Asset$Data r4 = r8.data     // Catch:{ JSONException -> 0x00e8 }
            java.lang.String r5 = "label"
            java.lang.String r5 = r6.optString(r5, r7)     // Catch:{ JSONException -> 0x00e8 }
            r4.label = r5     // Catch:{ JSONException -> 0x00e8 }
            goto L_0x0049
        L_0x00e8:
            if (r7 == 0) goto L_0x00f9
            java.lang.String r4 = "link"
            org.json.JSONObject r3 = r3.getJSONObject(r4)     // Catch:{ JSONException -> 0x00f6 }
            com.millennialmedia.internal.adcontrollers.NativeController$Link r3 = r10.loadLink(r3)     // Catch:{ JSONException -> 0x00f6 }
            r7.link = r3     // Catch:{ JSONException -> 0x00f6 }
        L_0x00f6:
            r0.add(r7)
        L_0x00f9:
            int r2 = r2 + 1
            goto L_0x0007
        L_0x00fd:
            java.util.concurrent.CopyOnWriteArrayList r11 = new java.util.concurrent.CopyOnWriteArrayList
            r11.<init>(r0)
            r10.assets = r11
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.adcontrollers.NativeController.loadAssets(org.json.JSONArray):void");
    }

    private Link loadLink(JSONObject jSONObject) throws JSONException {
        Link link2 = new Link();
        link2.url = jSONObject.getString(TJAdUnitConstants.String.URL);
        if (jSONObject.has("clicktrackers")) {
            try {
                link2.clickTrackerUrls = new CopyOnWriteArrayList(JSONUtils.convertToStringArray(jSONObject.getJSONArray("clicktrackers")));
            } catch (JSONException unused) {
            }
        }
        link2.fallback = jSONObject.optString("fallback", null);
        return link2;
    }
}
