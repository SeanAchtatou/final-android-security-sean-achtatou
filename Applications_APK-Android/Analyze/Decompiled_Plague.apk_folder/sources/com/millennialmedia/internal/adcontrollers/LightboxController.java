package com.millennialmedia.internal.adcontrollers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.MMWebView;
import com.millennialmedia.internal.SizableStateManager;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.TrackingEvent;
import com.millennialmedia.internal.utils.ViewUtils;
import com.millennialmedia.internal.video.LightboxView;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.mraid.controller.Abstract;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LightboxController extends AdController {
    /* access modifiers changed from: private */
    public static final String TAG = "LightboxController";
    /* access modifiers changed from: private */
    public volatile ViewGroup adContainer;
    /* access modifiers changed from: private */
    public LightboxAd lightboxAd;
    /* access modifiers changed from: private */
    public LightboxControllerListener lightboxControllerListener;
    /* access modifiers changed from: private */
    public LightboxView lightboxView;
    /* access modifiers changed from: private */
    public MMWebView mmWebView;

    public interface LightboxControllerListener {
        void attachFailed();

        void attachSucceeded();

        void initFailed();

        void initSucceeded();

        void onAdLeftApplication();

        void onClicked();

        void onCollapsed();

        void onExpanded();
    }

    public enum TrackableEvent {
        loaded,
        start,
        firstQuartile,
        midpoint,
        thirdQuartile,
        complete,
        videoExpand,
        videoCollapse,
        videoClose
    }

    public static class LightboxAd {
        public Fullscreen fullscreen;
        public Inline inline;
        public Video video;

        LightboxAd(Inline inline2, Video video2, Fullscreen fullscreen2) {
            this.inline = inline2;
            this.video = video2;
            this.fullscreen = fullscreen2;
        }
    }

    public static class Inline {
        public String content;
        public List<TrackingEvent> trackingEvents;

        Inline(String str, List<TrackingEvent> list) {
            this.content = str;
            this.trackingEvents = list;
        }
    }

    public static class Video {
        public Map<TrackableEvent, List<TrackingEvent>> trackingEvents;
        public String uri;

        Video(String str, Map<TrackableEvent, List<TrackingEvent>> map) {
            this.uri = str;
            this.trackingEvents = map;
        }
    }

    public static class Fullscreen {
        public String imageUri;
        public List<TrackingEvent> trackingEvents;
        public String webContent;

        Fullscreen(String str, String str2, List<TrackingEvent> list) {
            this.webContent = str;
            this.imageUri = str2;
            this.trackingEvents = list;
        }
    }

    LightboxController() {
    }

    public LightboxController(LightboxControllerListener lightboxControllerListener2) {
        this.lightboxControllerListener = lightboxControllerListener2;
    }

    public void init(final Context context, String str) {
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject("ad");
            JSONObject jSONObject2 = jSONObject.getJSONObject("inline");
            Inline inline = new Inline(jSONObject2.getString("content"), fromJSONArray(TrackableEvent.loaded, jSONObject2.getJSONArray("loadTracking")));
            JSONObject jSONObject3 = jSONObject.getJSONObject("video");
            HashMap hashMap = new HashMap();
            hashMap.put(TrackableEvent.start, fromJSONArray(TrackableEvent.start, jSONObject3.getJSONArray(TJAdUnitConstants.String.VIDEO_START)));
            hashMap.put(TrackableEvent.firstQuartile, fromJSONArray(TrackableEvent.firstQuartile, jSONObject3.getJSONArray(TJAdUnitConstants.String.VIDEO_FIRST_QUARTILE)));
            hashMap.put(TrackableEvent.midpoint, fromJSONArray(TrackableEvent.midpoint, jSONObject3.getJSONArray(TJAdUnitConstants.String.VIDEO_MIDPOINT)));
            hashMap.put(TrackableEvent.thirdQuartile, fromJSONArray(TrackableEvent.thirdQuartile, jSONObject3.getJSONArray(TJAdUnitConstants.String.VIDEO_THIRD_QUARTILE)));
            hashMap.put(TrackableEvent.complete, fromJSONArray(TrackableEvent.complete, jSONObject3.getJSONArray(TJAdUnitConstants.String.VIDEO_COMPLETE)));
            hashMap.put(TrackableEvent.videoExpand, fromJSONArray(TrackableEvent.videoExpand, jSONObject3.getJSONArray("videoExpand")));
            hashMap.put(TrackableEvent.videoCollapse, fromJSONArray(TrackableEvent.videoCollapse, jSONObject3.getJSONArray("videoCollapse")));
            hashMap.put(TrackableEvent.videoClose, fromJSONArray(TrackableEvent.videoClose, jSONObject3.getJSONArray("videoClose")));
            Video video = new Video(jSONObject3.getString("uri"), hashMap);
            JSONObject jSONObject4 = jSONObject.getJSONObject(Abstract.FULL_SCREEN);
            this.lightboxAd = new LightboxAd(inline, video, new Fullscreen(jSONObject4.getString("webContent"), jSONObject4.getString("imageUri"), fromJSONArray(TrackableEvent.loaded, jSONObject4.getJSONArray("loadTracking"))));
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    LightboxView unused = LightboxController.this.lightboxView = new LightboxView(context, LightboxController.this.lightboxAd, new LightboxView.LightboxViewListener() {
                        public void onFailed() {
                        }

                        public void onPrepared() {
                            if (MMLog.isDebugEnabled()) {
                                MMLog.d(LightboxController.TAG, "Lightbox prepared.");
                            }
                            ThreadUtils.postOnUiThread(new Runnable() {
                                public void run() {
                                    if (LightboxController.this.adContainer != null && LightboxController.this.lightboxView.getParent() == null) {
                                        if (MMLog.isDebugEnabled()) {
                                            MMLog.d(LightboxController.TAG, "Attaching Lightbox in onPrepared.");
                                        }
                                        LightboxController.this.attachLightboxView();
                                    }
                                }
                            });
                        }

                        public void onReadyToStart() {
                            if (MMLog.isDebugEnabled()) {
                                MMLog.d(LightboxController.TAG, "lightbox is ready to start playback");
                            }
                            LightboxController.this.lightboxView.start();
                        }

                        public void onExpanded() {
                            LightboxController.this.lightboxControllerListener.onExpanded();
                        }

                        public void onCollapsed() {
                            LightboxController.this.lightboxControllerListener.onCollapsed();
                        }

                        public void onClicked() {
                            LightboxController.this.lightboxControllerListener.onClicked();
                        }

                        public void onAdLeftApplication() {
                            LightboxController.this.lightboxControllerListener.onAdLeftApplication();
                        }
                    });
                    MMWebView unused2 = LightboxController.this.mmWebView = new MMWebView(context, MMWebView.MMWebViewOptions.getDefault(), LightboxController.this.createMMWebViewListener(LightboxController.this.lightboxControllerListener));
                    LightboxController.this.mmWebView.setContent(LightboxController.this.lightboxAd.inline.content);
                    LightboxController.this.mmWebView.addOnAttachStateChangeListener(LightboxController.this.createAttachListener(LightboxController.this.lightboxView));
                }
            });
        } catch (JSONException e) {
            MMLog.e(TAG, "Lightbox ad content is malformed.", e);
            this.lightboxControllerListener.initFailed();
        }
    }

    public void release() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (LightboxController.this.mmWebView != null) {
                    LightboxController.this.mmWebView.release();
                    MMWebView unused = LightboxController.this.mmWebView = null;
                }
                if (LightboxController.this.lightboxView != null) {
                    LightboxController.this.lightboxView.release();
                }
            }
        });
    }

    public void close() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (LightboxController.this.adContainer != null) {
                    LightboxController.this.adContainer.removeAllViews();
                    ViewGroup unused = LightboxController.this.adContainer = null;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public MMWebView.MMWebViewListener createMMWebViewListener(final LightboxControllerListener lightboxControllerListener2) {
        return new MMWebView.MMWebViewListener() {
            public void close() {
            }

            public boolean expand(SizableStateManager.ExpandParams expandParams) {
                return false;
            }

            public void onReady() {
            }

            public void onUnload() {
            }

            public boolean resize(SizableStateManager.ResizeParams resizeParams) {
                return false;
            }

            public void setOrientation(int i) {
            }

            public void onLoaded() {
                lightboxControllerListener2.initSucceeded();
            }

            public void onFailed() {
                lightboxControllerListener2.initFailed();
            }

            public void onClicked() {
                lightboxControllerListener2.onClicked();
            }

            public void onAdLeftApplication() {
                lightboxControllerListener2.onAdLeftApplication();
            }
        };
    }

    /* access modifiers changed from: private */
    public View.OnAttachStateChangeListener createAttachListener(final LightboxView lightboxView2) {
        return new View.OnAttachStateChangeListener() {
            public void onViewAttachedToWindow(View view) {
            }

            public void onViewDetachedFromWindow(View view) {
                lightboxView2.animateToGone(false);
            }
        };
    }

    private List<TrackingEvent> fromJSONArray(TrackableEvent trackableEvent, JSONArray jSONArray) throws JSONException {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(new TrackingEvent(trackableEvent.name(), jSONArray.getString(i)));
        }
        return arrayList;
    }

    public boolean canHandleContent(String str) {
        try {
            return "lightbox".equalsIgnoreCase(new JSONObject(str).getString("mmAdFormat"));
        } catch (JSONException unused) {
            return false;
        }
    }

    public void attach(final ViewGroup viewGroup, final ViewGroup.LayoutParams layoutParams) {
        if (viewGroup == null) {
            this.lightboxControllerListener.attachFailed();
            return;
        }
        this.adContainer = viewGroup;
        if (!(viewGroup.getContext() instanceof Activity)) {
            this.lightboxControllerListener.attachFailed();
        } else {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    ViewUtils.attachView(viewGroup, LightboxController.this.mmWebView, layoutParams);
                    if (LightboxController.this.lightboxView.isPrepared() && LightboxController.this.lightboxView.getParent() == null) {
                        if (MMLog.isDebugEnabled()) {
                            MMLog.d(LightboxController.TAG, "attaching lightbox is attach.");
                        }
                        LightboxController.this.attachLightboxView();
                    }
                    LightboxController.this.lightboxControllerListener.attachSucceeded();
                    TrackingEvent.fireEvents(LightboxController.this.lightboxAd.inline.trackingEvents);
                }
            });
        }
    }

    public int getDuration() {
        if (this.lightboxView == null) {
            return -1;
        }
        return this.lightboxView.getDuration();
    }

    public int getCurrentPosition() {
        if (this.lightboxView == null) {
            return -1;
        }
        return this.lightboxView.getCurrentPosition();
    }

    /* access modifiers changed from: private */
    public void attachLightboxView() {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "attaching lightbox view");
        }
        Display defaultDisplay = ((WindowManager) this.adContainer.getContext().getSystemService("window")).getDefaultDisplay();
        final Point point = new Point();
        defaultDisplay.getSize(point);
        Point defaultPosition = this.lightboxView.getDefaultPosition();
        Point defaultDimensions = this.lightboxView.getDefaultDimensions();
        this.lightboxView.setTranslationX((float) defaultPosition.x);
        this.lightboxView.setTranslationY((float) point.y);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(defaultDimensions.x, defaultDimensions.y);
        ViewGroup decorView = ViewUtils.getDecorView(this.adContainer);
        if (decorView != null) {
            ViewUtils.attachView(decorView, this.lightboxView, layoutParams);
            final int i = point.y - defaultPosition.y;
            AnonymousClass7 r0 = new Animation() {
                /* access modifiers changed from: protected */
                public void applyTransformation(float f, Transformation transformation) {
                    LightboxController.this.lightboxView.setTranslationY(f == 1.0f ? (float) (point.y - i) : ((float) point.y) - (f * ((float) i)));
                }
            };
            r0.setDuration((long) (((float) point.y) / this.adContainer.getContext().getResources().getDisplayMetrics().density));
            this.lightboxView.startAnimation(r0);
            return;
        }
        MMLog.e(TAG, "Unable to determine the root view; cannot attach Lightbox view.");
    }
}
