package com.millennialmedia.internal.video;

import android.annotation.SuppressLint;
import android.content.Context;
import android.webkit.JavascriptInterface;
import com.millennialmedia.MMLog;
import com.millennialmedia.XIncentivizedEventListener;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.MMWebView;
import com.millennialmedia.internal.adcontrollers.VASTVideoController;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.IOUtils;
import com.millennialmedia.internal.utils.JSONUtils;
import com.tapjoy.TapjoyConstants;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"ViewConstructor"})
public class VPAIDWebView extends MMWebView implements VASTVideoController.VideoViewActions {
    private static final int DEFAULT_BITRATE = 800;
    private static final int LTE_BITRATE = 800;
    private static final String MM_JS_BRIDGE_VPAID_INIT = "MmJsBridge.vpaid.init";
    private static final String TAG = "VPAIDWebView";
    private static final String VPAID_CLOSE_BUTTON = "vpaid_close_button.txt";
    private static final String VPAID_SCRIPT = "vpaid.js";
    private static final String VPAID_SKIP_BUTTON = "vpaid_skip_button.txt";
    private static final String VPAID_SPINNER = "vpaid_spinner.txt";
    private static final int WIFI_BITRATE = 1200;
    private long backButtonEnableTime = Long.MAX_VALUE;
    private boolean incentVideoCompleteEarned = false;
    private boolean isSkippable = false;
    private List vastDocuments;

    public interface VPAIDVideoViewListener extends MMWebView.MMWebViewListener {
        void onIncentiveEarned(XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent);
    }

    /* access modifiers changed from: protected */
    public String getExtraScriptToInject() {
        return VPAID_SCRIPT;
    }

    public void updateLayout() {
    }

    public VPAIDWebView(Context context, boolean z, VPAIDVideoViewListener vPAIDVideoViewListener) {
        super(context, new MMWebView.MMWebViewOptions(true, z, false, false), vPAIDVideoViewListener);
        setTag(TAG);
        setBackgroundColor(-16777216);
    }

    public void setVastDocuments(List list) {
        this.vastDocuments = list;
        super.setContent("<HTML><HEAD><meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' /></HEAD><BODY></BODY></HTML>");
    }

    /* access modifiers changed from: protected */
    public void onLoaded() {
        if (this.vastDocuments != null) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("vastDocs", JSONUtils.buildFromList(this.vastDocuments));
                jSONObject.put("minSkipOffset", Handshake.getVASTVideoSkipOffsetMin());
                jSONObject.put("maxSkipOffset", Handshake.getVASTVideoSkipOffsetMax());
                jSONObject.put("desiredBitrate", getDesiredBitrate());
                jSONObject.put("startAdTimeout", Handshake.getVPAIDStartAdTimeout());
                jSONObject.put("skipAdTimeout", Handshake.getVPAIDSkipAdTimeout());
                jSONObject.put("adUnitTimeout", Handshake.getVPAIDAdUnitTimeout());
                jSONObject.put("htmlEndCardTimeout", Handshake.getVPAIDHTMLEndCardTimeout());
                jSONObject.put("spinnerImage", IOUtils.readAssetContents("mmadsdk/vpaid_spinner.txt"));
                jSONObject.put("closeButtonImage", IOUtils.readAssetContents("mmadsdk/vpaid_close_button.txt"));
                jSONObject.put("skipButtonImage", IOUtils.readAssetContents("mmadsdk/vpaid_skip_button.txt"));
                callJavascript(MM_JS_BRIDGE_VPAID_INIT, jSONObject);
            } catch (JSONException e) {
                MMLog.e(TAG, "Unable to create JSON arguments for vpaid init", e);
                if (this.webViewListener != null) {
                    this.webViewListener.onFailed();
                }
            }
            this.vastDocuments = null;
        } else if (this.webViewListener != null) {
            this.webViewListener.onFailed();
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"AddJavascriptInterface"})
    public void injectExtraAPIs() {
        addJavascriptInterface(this, "MmInjectedFunctionsVpaid");
    }

    public boolean onBackPressed() {
        if (!this.isSkippable && System.currentTimeMillis() >= this.backButtonEnableTime) {
            this.isSkippable = true;
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Back button enabled due to delay timeout");
            }
        }
        return this.isSkippable;
    }

    public void release() {
        super.release();
    }

    @JavascriptInterface
    public void adLoadSucceeded(String str) throws JSONException {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Received adLoadSucceeded notification from VPAID");
        }
        if (this.webViewListener != null) {
            this.webViewListener.onLoaded();
        }
        this.backButtonEnableTime = System.currentTimeMillis() + ((long) Handshake.getVPAIDMaxBackButtonDelay());
    }

    @JavascriptInterface
    public void onVideoComplete(String str) throws JSONException {
        if (!this.incentVideoCompleteEarned) {
            this.incentVideoCompleteEarned = true;
            if (this.webViewListener != null && (this.webViewListener instanceof VPAIDVideoViewListener)) {
                ((VPAIDVideoViewListener) this.webViewListener).onIncentiveEarned(new XIncentivizedEventListener.XIncentiveEvent(XIncentivizedEventListener.XIncentiveEvent.INCENTIVE_VIDEO_COMPLETE, null));
            }
        }
    }

    @JavascriptInterface
    public void adLoadFailed(String str) throws JSONException {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Received adLoadFailed notification from VPAID");
        }
        if (this.webViewListener != null) {
            this.webViewListener.onFailed();
        }
    }

    @JavascriptInterface
    public void adSkippable(String str) throws JSONException {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Received adSkippable notification from VPAID");
        }
        this.isSkippable = true;
    }

    private static int getDesiredBitrate() {
        String networkConnectionType = EnvironmentUtils.getNetworkConnectionType();
        if (TapjoyConstants.TJC_CONNECTION_TYPE_WIFI.equalsIgnoreCase(networkConnectionType)) {
            return 1200;
        }
        boolean equalsIgnoreCase = "lte".equalsIgnoreCase(networkConnectionType);
        return 800;
    }
}
