package com.millennialmedia.internal.video;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.RelativeLayout;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.moat.analytics.mobile.aol.MoatAdEvent;
import com.moat.analytics.mobile.aol.MoatAdEventType;
import com.moat.analytics.mobile.aol.MoatFactory;
import com.moat.analytics.mobile.aol.NativeVideoTracker;
import com.moat.analytics.mobile.aol.VideoTrackerListener;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@SuppressLint({"ViewConstructor"})
public class MMVideoView extends RelativeLayout implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnInfoListener, MediaPlayer.OnVideoSizeChangedListener, MediaPlayer.OnSeekCompleteListener {
    private static final int COMPLETED = 6;
    private static final int ERROR = 7;
    private static final int IDLE = 0;
    private static final int MEDIA_ERROR_EXTRA_AUDIO_NO_INIT = -19;
    private static final int MEDIA_ERROR_STATE_EXCEPTION = -38;
    private static final String PARTNER_CODE = "millennialmedianativeapp775281030677";
    private static final int PAUSED = 5;
    private static final int PLAYING = 4;
    private static final int PREPARED = 2;
    private static final int PREPARING = 1;
    private static final int PROGRESS_POLLING_INTERVAL = 100;
    private static final int READY_TO_PLAY = 3;
    /* access modifiers changed from: private */
    public static final String TAG = "MMVideoView";
    /* access modifiers changed from: private */
    public volatile int checkedIncrement = 0;
    /* access modifiers changed from: private */
    public volatile int currentState = 0;
    /* access modifiers changed from: private */
    public MediaController mediaController;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer;
    private MoatFactory moatFactory;
    private Map<String, String> moatIdentifiers;
    private boolean muted;
    private NativeVideoTracker nativeVideoTracker;
    private ProgressRunnable progressRunnable;
    private int seekToMilliseconds = 0;
    /* access modifiers changed from: private */
    public SurfaceHolder surfaceHolder;
    private SurfaceHolder.Callback surfaceHolderCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            SurfaceHolder unused = MMVideoView.this.surfaceHolder = surfaceHolder;
            if (!MMVideoView.this.surfaceHolder.getSurface().isValid()) {
                int unused2 = MMVideoView.this.currentState = 7;
                int unused3 = MMVideoView.this.targetState = 7;
                if (MMVideoView.this.videoViewListener != null) {
                    ThreadUtils.runOnWorkerThread(new Runnable() {
                        public void run() {
                            MMVideoView.this.videoViewListener.onError(MMVideoView.this);
                        }
                    });
                    return;
                }
                return;
            }
            if (MMVideoView.this.mediaPlayer != null) {
                MMVideoView.this.mediaPlayer.setDisplay(MMVideoView.this.surfaceHolder);
            }
            if (MMVideoView.this.currentState == 2) {
                MMVideoView.this.setAudioFocus();
                int unused4 = MMVideoView.this.currentState = 3;
                if (!(MMVideoView.this.videoWidth == 0 || MMVideoView.this.videoHeight == 0)) {
                    MMVideoView.this.surfaceHolder.setFixedSize(MMVideoView.this.videoWidth, MMVideoView.this.videoHeight);
                }
                if (!(MMVideoView.this.videoViewListener == null || MMVideoView.this.targetState == 4)) {
                    ThreadUtils.runOnWorkerThread(new Runnable() {
                        public void run() {
                            MMVideoView.this.videoViewListener.onReadyToStart(MMVideoView.this);
                        }
                    });
                }
                if (MMVideoView.this.targetState == 4) {
                    MMVideoView.this.start();
                }
            }
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            if (MMVideoView.this.mediaPlayer != null && MMVideoView.this.targetState == 4) {
                MMVideoView.this.start();
            }
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            SurfaceHolder unused = MMVideoView.this.surfaceHolder = null;
            if (MMVideoView.this.mediaPlayer != null) {
                MMVideoView.this.mediaPlayer.setDisplay(null);
            }
        }
    };
    private VideoSurfaceView surfaceView;
    /* access modifiers changed from: private */
    public volatile int targetState;
    private Uri uri;
    /* access modifiers changed from: private */
    public int videoHeight;
    private VideoTrackerListener videoTrackerListener;
    /* access modifiers changed from: private */
    public MMVideoViewListener videoViewListener;
    /* access modifiers changed from: private */
    public int videoWidth;

    public interface MMVideoViewListener {
        void onBufferingUpdate(MMVideoView mMVideoView, int i);

        void onComplete(MMVideoView mMVideoView);

        void onError(MMVideoView mMVideoView);

        void onMuted(MMVideoView mMVideoView);

        void onPause(MMVideoView mMVideoView);

        void onPrepared(MMVideoView mMVideoView);

        void onProgress(MMVideoView mMVideoView, int i);

        void onReadyToStart(MMVideoView mMVideoView);

        void onSeek(MMVideoView mMVideoView);

        void onStart(MMVideoView mMVideoView);

        void onStop(MMVideoView mMVideoView);

        void onUnmuted(MMVideoView mMVideoView);
    }

    public interface MediaController {
        void onComplete();

        void onMuted();

        void onPause();

        void onProgress(int i);

        void onStart();

        void onUnmuted();

        void setDuration(int i);
    }

    public boolean onInfo(MediaPlayer mediaPlayer2, int i, int i2) {
        return false;
    }

    private static class MMVideoViewInfo extends View.BaseSavedState {
        public static final Parcelable.Creator<MMVideoViewInfo> CREATOR = new Parcelable.Creator<MMVideoViewInfo>() {
            public MMVideoViewInfo createFromParcel(Parcel parcel) {
                return new MMVideoViewInfo(parcel);
            }

            public MMVideoViewInfo[] newArray(int i) {
                return new MMVideoViewInfo[i];
            }
        };
        int currentPosition;
        int currentState;
        boolean muted;
        int targetState;
        String uri;

        public int describeContents() {
            return 0;
        }

        public MMVideoViewInfo(Parcelable parcelable) {
            super(parcelable);
        }

        private MMVideoViewInfo(Parcel parcel) {
            super(parcel);
            this.currentState = parcel.readInt();
            this.targetState = parcel.readInt();
            this.currentPosition = parcel.readInt();
            this.muted = parcel.readInt() != 1 ? false : true;
            this.uri = parcel.readString();
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.currentState);
            parcel.writeInt(this.targetState);
            parcel.writeInt(this.currentPosition);
            parcel.writeInt(this.muted ? 1 : 0);
            parcel.writeString(this.uri);
        }
    }

    private class VideoSurfaceView extends SurfaceView {
        VideoSurfaceView(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00a8, code lost:
            if (r1 > r6) goto L_0x00e4;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onMeasure(int r6, int r7) {
            /*
                r5 = this;
                com.millennialmedia.internal.video.MMVideoView r0 = com.millennialmedia.internal.video.MMVideoView.this
                int r0 = r0.videoWidth
                int r0 = getDefaultSize(r0, r6)
                com.millennialmedia.internal.video.MMVideoView r1 = com.millennialmedia.internal.video.MMVideoView.this
                int r1 = r1.videoHeight
                int r1 = getDefaultSize(r1, r7)
                com.millennialmedia.internal.video.MMVideoView r2 = com.millennialmedia.internal.video.MMVideoView.this
                int r2 = r2.videoWidth
                if (r2 <= 0) goto L_0x00e2
                com.millennialmedia.internal.video.MMVideoView r2 = com.millennialmedia.internal.video.MMVideoView.this
                int r2 = r2.videoHeight
                if (r2 <= 0) goto L_0x00e2
                int r0 = android.view.View.MeasureSpec.getMode(r6)
                int r6 = android.view.View.MeasureSpec.getSize(r6)
                int r1 = android.view.View.MeasureSpec.getMode(r7)
                int r7 = android.view.View.MeasureSpec.getSize(r7)
                r2 = 1073741824(0x40000000, float:2.0)
                if (r0 != r2) goto L_0x007d
                if (r1 != r2) goto L_0x007d
                com.millennialmedia.internal.video.MMVideoView r0 = com.millennialmedia.internal.video.MMVideoView.this
                int r0 = r0.videoWidth
                int r0 = r0 * r7
                com.millennialmedia.internal.video.MMVideoView r1 = com.millennialmedia.internal.video.MMVideoView.this
                int r1 = r1.videoHeight
                int r1 = r1 * r6
                if (r0 >= r1) goto L_0x005c
                com.millennialmedia.internal.video.MMVideoView r6 = com.millennialmedia.internal.video.MMVideoView.this
                int r6 = r6.videoWidth
                int r6 = r6 * r7
                com.millennialmedia.internal.video.MMVideoView r0 = com.millennialmedia.internal.video.MMVideoView.this
                int r0 = r0.videoHeight
                int r0 = r6 / r0
                r6 = r0
                goto L_0x00e4
            L_0x005c:
                com.millennialmedia.internal.video.MMVideoView r0 = com.millennialmedia.internal.video.MMVideoView.this
                int r0 = r0.videoWidth
                int r0 = r0 * r7
                com.millennialmedia.internal.video.MMVideoView r1 = com.millennialmedia.internal.video.MMVideoView.this
                int r1 = r1.videoHeight
                int r1 = r1 * r6
                if (r0 <= r1) goto L_0x00e4
                com.millennialmedia.internal.video.MMVideoView r7 = com.millennialmedia.internal.video.MMVideoView.this
                int r7 = r7.videoHeight
                int r7 = r7 * r6
                com.millennialmedia.internal.video.MMVideoView r0 = com.millennialmedia.internal.video.MMVideoView.this
                int r0 = r0.videoWidth
                int r1 = r7 / r0
                goto L_0x00e3
            L_0x007d:
                r3 = -2147483648(0xffffffff80000000, float:-0.0)
                if (r0 != r2) goto L_0x0096
                com.millennialmedia.internal.video.MMVideoView r0 = com.millennialmedia.internal.video.MMVideoView.this
                int r0 = r0.videoHeight
                int r0 = r0 * r6
                com.millennialmedia.internal.video.MMVideoView r2 = com.millennialmedia.internal.video.MMVideoView.this
                int r2 = r2.videoWidth
                int r0 = r0 / r2
                if (r1 != r3) goto L_0x0094
                if (r0 <= r7) goto L_0x0094
                goto L_0x00e4
            L_0x0094:
                r7 = r0
                goto L_0x00e4
            L_0x0096:
                if (r1 != r2) goto L_0x00ad
                com.millennialmedia.internal.video.MMVideoView r1 = com.millennialmedia.internal.video.MMVideoView.this
                int r1 = r1.videoWidth
                int r1 = r1 * r7
                com.millennialmedia.internal.video.MMVideoView r2 = com.millennialmedia.internal.video.MMVideoView.this
                int r2 = r2.videoHeight
                int r1 = r1 / r2
                if (r0 != r3) goto L_0x00ab
                if (r1 <= r6) goto L_0x00ab
                goto L_0x00e4
            L_0x00ab:
                r6 = r1
                goto L_0x00e4
            L_0x00ad:
                com.millennialmedia.internal.video.MMVideoView r2 = com.millennialmedia.internal.video.MMVideoView.this
                int r2 = r2.videoWidth
                com.millennialmedia.internal.video.MMVideoView r4 = com.millennialmedia.internal.video.MMVideoView.this
                int r4 = r4.videoHeight
                if (r1 != r3) goto L_0x00cc
                if (r4 <= r7) goto L_0x00cc
                com.millennialmedia.internal.video.MMVideoView r1 = com.millennialmedia.internal.video.MMVideoView.this
                int r1 = r1.videoWidth
                int r1 = r1 * r7
                com.millennialmedia.internal.video.MMVideoView r2 = com.millennialmedia.internal.video.MMVideoView.this
                int r2 = r2.videoHeight
                int r1 = r1 / r2
                goto L_0x00ce
            L_0x00cc:
                r1 = r2
                r7 = r4
            L_0x00ce:
                if (r0 != r3) goto L_0x00ab
                if (r1 <= r6) goto L_0x00ab
                com.millennialmedia.internal.video.MMVideoView r7 = com.millennialmedia.internal.video.MMVideoView.this
                int r7 = r7.videoHeight
                int r7 = r7 * r6
                com.millennialmedia.internal.video.MMVideoView r0 = com.millennialmedia.internal.video.MMVideoView.this
                int r0 = r0.videoWidth
                int r1 = r7 / r0
                goto L_0x00e3
            L_0x00e2:
                r6 = r0
            L_0x00e3:
                r7 = r1
            L_0x00e4:
                r5.setMeasuredDimension(r6, r7)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.video.MMVideoView.VideoSurfaceView.onMeasure(int, int):void");
        }
    }

    private void startProgressRunnable() {
        if (this.progressRunnable == null) {
            this.progressRunnable = new ProgressRunnable();
        }
        this.progressRunnable.start();
    }

    private void stopProgressRunnable() {
        if (this.progressRunnable != null) {
            this.progressRunnable.stop();
            this.progressRunnable = null;
        }
    }

    private void resetProgressRunnable() {
        if (this.progressRunnable != null) {
            this.progressRunnable.resetCheckedPosition();
        }
    }

    private class ProgressRunnable implements Runnable {
        int checkedPosition;
        ThreadUtils.ScheduledRunnable scheduledRunnable;

        private ProgressRunnable() {
            this.scheduledRunnable = null;
            this.checkedPosition = 0;
        }

        public void run() {
            synchronized (this) {
                if (this.scheduledRunnable != null) {
                    if (MMVideoView.this.currentState != 4) {
                        this.scheduledRunnable = null;
                        return;
                    }
                    int checkedCurrentPosition = getCheckedCurrentPosition();
                    if (MMVideoView.this.videoViewListener != null) {
                        MMVideoView.this.videoViewListener.onProgress(MMVideoView.this, checkedCurrentPosition);
                    }
                    if (MMVideoView.this.mediaController != null) {
                        MMVideoView.this.mediaController.onProgress(checkedCurrentPosition);
                    }
                    this.scheduledRunnable = ThreadUtils.runOnWorkerThreadDelayed(this, 100);
                }
            }
        }

        public void start() {
            synchronized (this) {
                resetCheckedPosition();
                if (this.scheduledRunnable == null) {
                    this.scheduledRunnable = ThreadUtils.runOnWorkerThreadDelayed(this, 100);
                }
            }
        }

        public void stop() {
            synchronized (this) {
                if (this.scheduledRunnable != null) {
                    this.scheduledRunnable.cancel();
                    this.scheduledRunnable = null;
                }
            }
        }

        public void resetCheckedPosition() {
            this.checkedPosition = 0;
            int unused = MMVideoView.this.checkedIncrement = 0;
        }

        private int getCheckedCurrentPosition() {
            int currentPosition = MMVideoView.this.mediaPlayer.getCurrentPosition();
            if (this.checkedPosition == currentPosition) {
                if (MMVideoView.this.checkedIncrement == 0 && MMLog.isDebugEnabled()) {
                    MMLog.d(MMVideoView.TAG, "Current position frozen -- activating auto-correction");
                }
                int unused = MMVideoView.this.checkedIncrement = MMVideoView.this.checkedIncrement + 100;
                return currentPosition + MMVideoView.this.checkedIncrement;
            }
            if (MMVideoView.this.checkedIncrement > 0 && MMLog.isDebugEnabled()) {
                MMLog.d(MMVideoView.TAG, "Current position unfrozen -- deactivating auto-correction");
            }
            this.checkedPosition = currentPosition;
            int unused2 = MMVideoView.this.checkedIncrement = 0;
            return currentPosition;
        }
    }

    public MMVideoView(Context context, boolean z, boolean z2, Map<String, String> map, MMVideoViewListener mMVideoViewListener) {
        super(new MutableContextWrapper(context));
        if (map == null) {
            this.moatIdentifiers = Collections.emptyMap();
        } else {
            this.moatIdentifiers = map;
        }
        MutableContextWrapper mutableContextWrapper = (MutableContextWrapper) getContext();
        this.muted = z2;
        this.videoViewListener = mMVideoViewListener;
        if (z) {
            this.targetState = 4;
        }
        setBackgroundColor(getResources().getColor(17170444));
        this.surfaceView = new VideoSurfaceView(mutableContextWrapper);
        this.surfaceView.getHolder().addCallback(this.surfaceHolderCallback);
        this.surfaceView.getHolder().setType(3);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        addView(this.surfaceView, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        MMVideoViewInfo mMVideoViewInfo = (MMVideoViewInfo) parcelable;
        super.onRestoreInstanceState(mMVideoViewInfo.getSuperState());
        this.targetState = mMVideoViewInfo.targetState;
        this.seekToMilliseconds = mMVideoViewInfo.currentPosition;
        this.muted = mMVideoViewInfo.muted;
        if (mMVideoViewInfo.currentState == 4 || mMVideoViewInfo.targetState == 4) {
            start();
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        MMVideoViewInfo mMVideoViewInfo = new MMVideoViewInfo(super.onSaveInstanceState());
        mMVideoViewInfo.currentState = this.currentState;
        mMVideoViewInfo.targetState = this.targetState;
        mMVideoViewInfo.currentPosition = getCurrentPosition();
        mMVideoViewInfo.muted = this.muted;
        mMVideoViewInfo.uri = this.uri.toString();
        return mMVideoViewInfo;
    }

    public void release() {
        if (this.nativeVideoTracker != null) {
            this.nativeVideoTracker.stopTracking();
        }
        stopProgressRunnable();
        if (this.mediaPlayer != null) {
            this.mediaPlayer.setDisplay(null);
            this.mediaPlayer.reset();
            this.mediaPlayer.release();
            this.mediaPlayer = null;
            this.currentState = 0;
        }
    }

    public void setVideoURI(Uri uri2) {
        this.uri = uri2;
        if (uri2 != null) {
            release();
            this.mediaPlayer = new MediaPlayer();
            if (this.surfaceHolder != null) {
                this.mediaPlayer.setDisplay(this.surfaceHolder);
            }
            this.mediaPlayer.setOnPreparedListener(this);
            this.mediaPlayer.setOnCompletionListener(this);
            this.mediaPlayer.setOnErrorListener(this);
            this.mediaPlayer.setOnBufferingUpdateListener(this);
            this.mediaPlayer.setOnSeekCompleteListener(this);
            this.mediaPlayer.setOnInfoListener(this);
            this.mediaPlayer.setOnVideoSizeChangedListener(this);
            try {
                this.mediaPlayer.setDataSource(getContext(), uri2, (Map<String, String>) null);
                this.currentState = 1;
                this.mediaPlayer.prepareAsync();
            } catch (IOException e) {
                MMLog.e(TAG, "An error occurred preparing the VideoPlayer.", e);
                this.currentState = 7;
                this.targetState = 7;
                if (this.videoViewListener != null) {
                    ThreadUtils.runOnWorkerThread(new Runnable() {
                        public void run() {
                            MMVideoView.this.videoViewListener.onError(MMVideoView.this);
                        }
                    });
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void setAudioFocus() {
        AudioManager audioManager = (AudioManager) getContext().getSystemService("audio");
        if (!this.muted) {
            audioManager.requestAudioFocus(null, 3, 3);
        } else {
            audioManager.abandonAudioFocus(null);
        }
    }

    private void releaseAudioFocus() {
        ((AudioManager) getContext().getSystemService("audio")).abandonAudioFocus(null);
    }

    public void setVideoPath(String str) {
        setVideoURI(Uri.parse(str));
    }

    public void setMediaController(MediaController mediaController2) {
        this.mediaController = mediaController2;
    }

    public void restart() {
        if (Build.VERSION.SDK_INT > 21) {
            seekTo(0);
        } else if (this.uri != null) {
            setVideoURI(this.uri);
        } else {
            return;
        }
        start();
    }

    public void start() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                MMVideoView.this.startOnUiThread();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void setMoatVideoTrackerListener(VideoTrackerListener videoTrackerListener2) {
        this.videoTrackerListener = videoTrackerListener2;
        setMoatVideoTrackerListener(this.nativeVideoTracker, this.videoTrackerListener);
    }

    private void setMoatVideoTrackerListener(NativeVideoTracker nativeVideoTracker2, VideoTrackerListener videoTrackerListener2) {
        if (nativeVideoTracker2 != null && videoTrackerListener2 != null) {
            nativeVideoTracker2.setVideoListener(videoTrackerListener2);
        }
    }

    /* access modifiers changed from: private */
    public void startOnUiThread() {
        if (!isInPlaybackState() || this.currentState == 4) {
            this.targetState = 4;
            return;
        }
        if (this.muted) {
            mute();
        }
        if (this.seekToMilliseconds != 0) {
            this.mediaPlayer.seekTo(this.seekToMilliseconds);
            this.seekToMilliseconds = 0;
        }
        if (!this.moatIdentifiers.isEmpty() && this.moatFactory != null && this.nativeVideoTracker == null) {
            this.nativeVideoTracker = this.moatFactory.createNativeVideoTracker(PARTNER_CODE);
            this.nativeVideoTracker.trackVideoAd(this.moatIdentifiers, this.mediaPlayer, this);
            setMoatVideoTrackerListener(this.nativeVideoTracker, this.videoTrackerListener);
            MMLog.v(TAG, "Moat video tracking enabled.");
        }
        this.mediaPlayer.start();
        this.currentState = 4;
        this.targetState = 4;
        if (this.videoViewListener != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.videoViewListener.onStart(MMVideoView.this);
                }
            });
        }
        if (this.mediaController != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.mediaController.onStart();
                }
            });
        }
        startProgressRunnable();
    }

    public void stop() {
        releaseAudioFocus();
        if (!isInPlaybackState()) {
            return;
        }
        if (this.mediaPlayer.isPlaying() || this.currentState == 5) {
            this.mediaPlayer.stop();
            if (this.videoViewListener != null) {
                ThreadUtils.runOnWorkerThread(new Runnable() {
                    public void run() {
                        MMVideoView.this.videoViewListener.onStop(MMVideoView.this);
                    }
                });
            }
            this.currentState = 0;
            this.targetState = 0;
        }
    }

    public void pause() {
        if (isInPlaybackState() && this.mediaPlayer.isPlaying()) {
            this.mediaPlayer.pause();
            if (this.videoViewListener != null) {
                ThreadUtils.runOnWorkerThread(new Runnable() {
                    public void run() {
                        MMVideoView.this.videoViewListener.onPause(MMVideoView.this);
                    }
                });
            }
            if (this.mediaController != null) {
                ThreadUtils.runOnWorkerThread(new Runnable() {
                    public void run() {
                        MMVideoView.this.mediaController.onPause();
                    }
                });
            }
            this.currentState = 5;
            this.targetState = 5;
        }
    }

    public void seekTo(int i) {
        if (isInPlaybackState()) {
            resetProgressRunnable();
            this.mediaPlayer.seekTo(i);
            this.seekToMilliseconds = 0;
            return;
        }
        this.seekToMilliseconds = i;
    }

    public void mute() {
        this.muted = true;
        if (this.mediaPlayer != null) {
            this.mediaPlayer.setVolume(0.0f, 0.0f);
        }
        setAudioFocus();
        if (this.videoViewListener != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.videoViewListener.onMuted(MMVideoView.this);
                }
            });
        }
        if (this.mediaController != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.mediaController.onMuted();
                }
            });
        }
    }

    public void unmute() {
        this.muted = false;
        if (this.mediaPlayer != null) {
            this.mediaPlayer.setVolume(1.0f, 1.0f);
        }
        setAudioFocus();
        if (this.videoViewListener != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.videoViewListener.onUnmuted(MMVideoView.this);
                }
            });
        }
        if (this.mediaController != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.mediaController.onUnmuted();
                }
            });
        }
    }

    public int getCurrentPosition() {
        if (isInPlaybackState()) {
            return this.mediaPlayer.getCurrentPosition() + this.checkedIncrement;
        }
        return -1;
    }

    public int getDuration() {
        if (isInPlaybackState() || this.currentState == 2) {
            return this.mediaPlayer.getDuration();
        }
        return -1;
    }

    public void setVideoViewListener(MMVideoViewListener mMVideoViewListener) {
        this.videoViewListener = mMVideoViewListener;
    }

    public boolean isPlaying() {
        return isInPlaybackState() && this.mediaPlayer.isPlaying();
    }

    public void videoSkipped() {
        if (this.nativeVideoTracker != null) {
            this.nativeVideoTracker.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_SKIPPED));
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer2) {
        this.currentState = 6;
        this.targetState = 6;
        if (this.nativeVideoTracker != null) {
            this.nativeVideoTracker.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
        }
        stopProgressRunnable();
        if (this.videoViewListener != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.videoViewListener.onProgress(MMVideoView.this, MMVideoView.this.getDuration());
                    MMVideoView.this.videoViewListener.onComplete(MMVideoView.this);
                }
            });
        }
        if (this.mediaController != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.mediaController.onComplete();
                }
            });
        }
    }

    public boolean onError(MediaPlayer mediaPlayer2, int i, int i2) {
        if ((i == 1 && i2 == MEDIA_ERROR_EXTRA_AUDIO_NO_INIT) || i == MEDIA_ERROR_STATE_EXCEPTION) {
            if (MMLog.isDebugEnabled()) {
                String str = TAG;
                MMLog.d(str, "Ignoring acceptable media error: (" + i + "," + i2 + ")");
            }
            return true;
        }
        this.currentState = 7;
        if (this.videoViewListener != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.videoViewListener.onError(MMVideoView.this);
                }
            });
        }
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer2) {
        if (this.surfaceHolder != null) {
            setAudioFocus();
            this.currentState = 3;
            if (this.targetState == 4) {
                if (this.videoViewListener != null) {
                    ThreadUtils.runOnWorkerThread(new Runnable() {
                        public void run() {
                            MMVideoView.this.videoViewListener.onPrepared(MMVideoView.this);
                        }
                    });
                }
                start();
            } else if (this.videoViewListener != null) {
                ThreadUtils.runOnWorkerThread(new Runnable() {
                    public void run() {
                        MMVideoView.this.videoViewListener.onReadyToStart(MMVideoView.this);
                    }
                });
            }
        } else {
            this.currentState = 2;
            if (this.videoViewListener != null) {
                ThreadUtils.runOnWorkerThread(new Runnable() {
                    public void run() {
                        MMVideoView.this.videoViewListener.onPrepared(MMVideoView.this);
                    }
                });
            }
        }
        if (this.mediaController != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.mediaController.setDuration(MMVideoView.this.getDuration());
                }
            });
        }
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer2, final int i) {
        if (this.videoViewListener != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.videoViewListener.onBufferingUpdate(MMVideoView.this, i);
                }
            });
        }
    }

    public void onSeekComplete(MediaPlayer mediaPlayer2) {
        if (this.videoViewListener != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.videoViewListener.onSeek(MMVideoView.this);
                }
            });
        }
        if (this.mediaController != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    MMVideoView.this.mediaController.onProgress(MMVideoView.this.getCurrentPosition());
                }
            });
        }
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer2, int i, int i2) {
        if (i2 != 0 && i != 0) {
            this.videoWidth = i;
            this.videoHeight = i2;
            if (this.surfaceHolder != null) {
                this.surfaceHolder.setFixedSize(this.videoWidth, this.videoHeight);
                requestLayout();
            }
        }
    }

    private boolean isInPlaybackState() {
        return (this.currentState == 0 || this.currentState == 1 || this.currentState == 2 || this.currentState == 7) ? false : true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setAudioFocus();
        if (!this.moatIdentifiers.isEmpty()) {
            this.moatFactory = MoatFactory.create();
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Moat ad identifiers were not provided. Moat video tracking disabled.");
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        releaseAudioFocus();
        super.onDetachedFromWindow();
    }
}
