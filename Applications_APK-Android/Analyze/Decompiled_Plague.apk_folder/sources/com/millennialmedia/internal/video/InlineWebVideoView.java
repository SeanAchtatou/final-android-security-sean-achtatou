package com.millennialmedia.internal.video;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.graphics.Rect;
import android.media.AudioManager;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;
import com.millennialmedia.MMLog;
import com.millennialmedia.R;
import com.millennialmedia.internal.FiredEvents;
import com.millennialmedia.internal.MMActivity;
import com.millennialmedia.internal.MMWebView;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.ViewUtils;
import com.millennialmedia.internal.video.MMVideoView;
import com.tapjoy.TJAdUnitConstants;
import java.lang.ref.WeakReference;

@SuppressLint({"ViewConstructor"})
public class InlineWebVideoView extends RelativeLayout implements MMVideoView.MMVideoViewListener {
    private static final String BASE_TAG = "MMInlineWebVideoView_";
    private static final int HIDE_CONTROLS_DELAY = 2500;
    public static final int PROGRESS_UPDATES_DISABLED = -1;
    private static final String STATE_CHANGE = "stateChange";
    /* access modifiers changed from: private */
    public static final String TAG = "InlineWebVideoView";
    private static final String TRACKING = "tracking";
    private static volatile int lastTagID = 0;
    private static volatile int nextTagID = 100;
    private InlineWebVideoViewAttachListener attachListener;
    /* access modifiers changed from: private */
    public String callbackId;
    private boolean error = false;
    /* access modifiers changed from: private */
    public ToggleButton expandCollapseToggleButton;
    private FiredEvents firedEvents;
    /* access modifiers changed from: private */
    public int height;
    private ThreadUtils.ScheduledRunnable hideControlsRunnable;
    /* access modifiers changed from: private */
    public InlineVideoControls inlineVideoControls;
    /* access modifiers changed from: private */
    public InlineWebVideoViewListener inlineWebVideoViewListener;
    private long lastUpdateTime = 0;
    /* access modifiers changed from: private */
    public MMVideoView mmVideoView;
    /* access modifiers changed from: private */
    public WeakReference<MMWebView> mmWebViewRef;
    /* access modifiers changed from: private */
    public ImageView placeholderView;
    private boolean showExpandControls;
    private boolean showMediaControls;
    private int timeUpdateInterval = -1;
    /* access modifiers changed from: private */
    public Uri uri;
    /* access modifiers changed from: private */
    public FrameLayout videoContainer;
    private ViewUtils.ViewabilityWatcher viewabilityWatcher;
    /* access modifiers changed from: private */
    public int width;
    /* access modifiers changed from: private */
    public int x;
    /* access modifiers changed from: private */
    public int y;

    public interface InlineWebVideoViewAttachListener {
        void attachFailed(InlineWebVideoView inlineWebVideoView);

        void attachSucceeded(InlineWebVideoView inlineWebVideoView);
    }

    public interface InlineWebVideoViewListener {
        void onClicked();
    }

    public void onBufferingUpdate(MMVideoView mMVideoView, int i) {
    }

    public class InlineVideoControls extends RelativeLayout implements MMVideoView.MediaController {
        /* access modifiers changed from: private */
        public ToggleButton muteUnmuteButton;
        /* access modifiers changed from: private */
        public ToggleButton playPauseButton;
        /* access modifiers changed from: private */
        public ProgressBar progressBar;

        public void onMuted() {
        }

        public void onPause() {
        }

        public void onStart() {
        }

        public void onUnmuted() {
        }

        public InlineVideoControls(Context context, final MMVideoView mMVideoView, boolean z, boolean z2) {
            super(context);
            setBackgroundColor(getResources().getColor(R.color.mmadsdk_inline_video_controls_background));
            setOnClickListener(new View.OnClickListener(InlineWebVideoView.this) {
                public void onClick(View view) {
                }
            });
            this.playPauseButton = new ToggleButton(context);
            this.playPauseButton.setId(R.id.mmadsdk_inline_video_play_pause_button);
            this.playPauseButton.setTextOn("");
            this.playPauseButton.setTextOff("");
            this.playPauseButton.setChecked(z);
            this.playPauseButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.mmadsdk_play_pause));
            this.playPauseButton.setOnClickListener(new View.OnClickListener(InlineWebVideoView.this) {
                public void onClick(View view) {
                    InlineWebVideoView.this.fireClientSideOnClick();
                }
            });
            this.playPauseButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(InlineWebVideoView.this) {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    if (mMVideoView == null) {
                        return;
                    }
                    if (z) {
                        mMVideoView.start();
                    } else {
                        mMVideoView.pause();
                    }
                }
            });
            Rect access$100 = InlineWebVideoView.this.getButtonDimensions(false);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(access$100.width(), access$100.height());
            layoutParams.addRule(9);
            addView(this.playPauseButton, layoutParams);
            this.muteUnmuteButton = new ToggleButton(context);
            this.muteUnmuteButton.setId(R.id.mmadsdk_inline_video_mute_unmute_button);
            this.muteUnmuteButton.setTextOn("");
            this.muteUnmuteButton.setTextOff("");
            this.muteUnmuteButton.setChecked(z2);
            this.muteUnmuteButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.mmadsdk_mute_unmute));
            this.muteUnmuteButton.setOnClickListener(new View.OnClickListener(InlineWebVideoView.this) {
                public void onClick(View view) {
                    InlineWebVideoView.this.fireClientSideOnClick();
                }
            });
            this.muteUnmuteButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(InlineWebVideoView.this) {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    if (mMVideoView == null) {
                        return;
                    }
                    if (z) {
                        mMVideoView.mute();
                        return;
                    }
                    mMVideoView.unmute();
                    AudioManager audioManager = (AudioManager) InlineVideoControls.this.getContext().getSystemService("audio");
                    if (audioManager.getStreamVolume(3) == 0) {
                        audioManager.setStreamVolume(3, audioManager.getStreamMaxVolume(3) / 3, 0);
                    }
                }
            });
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(access$100.width(), access$100.height());
            layoutParams2.addRule(11);
            addView(this.muteUnmuteButton, layoutParams2);
            this.progressBar = new ProgressBar(context, null, 16842872);
            this.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.mmadsdk_inline_video_progress_bar));
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, access$100.height() / 2);
            layoutParams3.addRule(1, R.id.mmadsdk_inline_video_play_pause_button);
            layoutParams3.addRule(0, R.id.mmadsdk_inline_video_mute_unmute_button);
            layoutParams3.addRule(15);
            addView(this.progressBar, layoutParams3);
        }

        public void start() {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineVideoControls.this.playPauseButton.setChecked(true);
                }
            });
        }

        public void pause() {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineVideoControls.this.playPauseButton.setChecked(false);
                }
            });
        }

        public void mute() {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineVideoControls.this.muteUnmuteButton.setChecked(true);
                }
            });
        }

        public void unmute() {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineVideoControls.this.muteUnmuteButton.setChecked(false);
                }
            });
        }

        public void onProgress(final int i) {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineVideoControls.this.progressBar.setProgress(i);
                }
            });
        }

        public void setDuration(final int i) {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    InlineVideoControls.this.progressBar.setProgress(0);
                    InlineVideoControls.this.progressBar.setMax(i);
                }
            });
        }

        public void onComplete() {
            this.progressBar.setProgress(this.progressBar.getMax());
            pause();
        }

        /* access modifiers changed from: package-private */
        public void resize(boolean z) {
            Rect access$100 = InlineWebVideoView.this.getButtonDimensions(z);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.muteUnmuteButton.getLayoutParams();
            layoutParams.width = access$100.width();
            layoutParams.height = access$100.height();
            this.muteUnmuteButton.setLayoutParams(layoutParams);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.playPauseButton.getLayoutParams();
            layoutParams2.width = access$100.width();
            layoutParams2.height = access$100.height();
            this.playPauseButton.setLayoutParams(layoutParams2);
            RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.progressBar.getLayoutParams();
            layoutParams3.height = access$100.height() / 2;
            this.progressBar.setLayoutParams(layoutParams3);
        }
    }

    static class InlineWebViewViewabilityListener implements ViewUtils.ViewabilityListener {
        private boolean didPause;
        WeakReference<InlineWebVideoView> inlineWebVideoViewRef;

        InlineWebViewViewabilityListener(InlineWebVideoView inlineWebVideoView) {
            this.inlineWebVideoViewRef = new WeakReference<>(inlineWebVideoView);
        }

        public void onViewableChanged(boolean z) {
            InlineWebVideoView inlineWebVideoView = this.inlineWebVideoViewRef.get();
            if (inlineWebVideoView != null) {
                if (z) {
                    if (this.didPause) {
                        this.didPause = false;
                        inlineWebVideoView.inlineVideoControls.start();
                    }
                } else if (inlineWebVideoView.mmVideoView != null && inlineWebVideoView.mmVideoView.isPlaying()) {
                    this.didPause = true;
                    inlineWebVideoView.inlineVideoControls.pause();
                }
            }
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InlineWebVideoView(Context context, boolean z, boolean z2, boolean z3, boolean z4, int i, String str, InlineWebVideoViewListener inlineWebVideoViewListener2) {
        super(new MutableContextWrapper(context));
        final boolean z5 = z3;
        final boolean z6 = z4;
        this.inlineWebVideoViewListener = inlineWebVideoViewListener2;
        MutableContextWrapper mutableContextWrapper = (MutableContextWrapper) getContext();
        this.callbackId = str;
        this.timeUpdateInterval = i;
        this.showMediaControls = z5;
        this.showExpandControls = z6;
        this.viewabilityWatcher = new ViewUtils.ViewabilityWatcher(this, new InlineWebViewViewabilityListener(this));
        this.viewabilityWatcher.startWatching();
        this.videoContainer = new FrameLayout(mutableContextWrapper);
        setBackgroundColor(-16777216);
        this.mmVideoView = new MMVideoView(mutableContextWrapper, z, z2, null, this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
        layoutParams.gravity = 17;
        this.videoContainer.addView(this.mmVideoView, layoutParams);
        setTag(BASE_TAG + getNextTagID());
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, -1);
        this.placeholderView = new ImageView(mutableContextWrapper);
        this.placeholderView.setBackgroundColor(-16777216);
        this.placeholderView.setLayoutParams(layoutParams2);
        this.videoContainer.addView(this.placeholderView);
        addView(this.videoContainer, new RelativeLayout.LayoutParams(-1, -1));
        this.inlineVideoControls = new InlineVideoControls(mutableContextWrapper, this.mmVideoView, z, z2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(12);
        if (!z5) {
            this.inlineVideoControls.setVisibility(8);
        }
        addView(this.inlineVideoControls, layoutParams3);
        this.mmVideoView.setMediaController(this.inlineVideoControls);
        this.mmVideoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                InlineWebVideoView.this.fireClientSideOnClick();
                if (z5) {
                    InlineWebVideoView.this.inlineVideoControls.setAlpha(1.0f);
                    InlineWebVideoView.this.inlineVideoControls.setVisibility(0);
                }
                if (z6) {
                    InlineWebVideoView.this.expandCollapseToggleButton.setAlpha(1.0f);
                    InlineWebVideoView.this.expandCollapseToggleButton.setVisibility(0);
                }
                if (z5 || z6) {
                    InlineWebVideoView.this.scheduleAutoHideControls();
                }
            }
        });
        this.mmVideoView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 1) {
                    Integer valueOf = Integer.valueOf(ViewUtils.convertPixelsToDips(((int) motionEvent.getX()) + InlineWebVideoView.this.x));
                    Integer valueOf2 = Integer.valueOf(ViewUtils.convertPixelsToDips(((int) motionEvent.getY()) + InlineWebVideoView.this.y));
                    MMWebView mMWebView = (MMWebView) InlineWebVideoView.this.mmWebViewRef.get();
                    if (mMWebView != null) {
                        mMWebView.invokeCallback(InlineWebVideoView.this.callbackId, InlineWebVideoView.this.getTag(), "click", valueOf, valueOf2);
                    }
                }
                return false;
            }
        });
        this.expandCollapseToggleButton = new ToggleButton(mutableContextWrapper);
        this.expandCollapseToggleButton.setTextOff("");
        this.expandCollapseToggleButton.setTextOn("");
        this.expandCollapseToggleButton.setChecked(false);
        this.expandCollapseToggleButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.mmadsdk_expand_collapse));
        this.expandCollapseToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                InlineWebVideoView.this.fireClientSideOnClick();
                if (z) {
                    InlineWebVideoView.this.internalExpandToFullScreen();
                }
            }
        });
        if (!z6) {
            this.expandCollapseToggleButton.setVisibility(8);
        }
        Rect buttonDimensions = getButtonDimensions(false);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(buttonDimensions.width(), buttonDimensions.height());
        layoutParams4.addRule(10);
        layoutParams4.addRule(11);
        addView(this.expandCollapseToggleButton, layoutParams4);
    }

    public void release() {
        if (this.mmVideoView != null) {
            this.mmVideoView.release();
            this.mmVideoView = null;
        }
    }

    private static int getNextTagID() {
        lastTagID = nextTagID;
        int i = nextTagID;
        nextTagID = i + 1;
        return i;
    }

    public static int getLastTagID() {
        return lastTagID;
    }

    public void setAnchorView(MMWebView mMWebView, int i, int i2, int i3, int i4, InlineWebVideoViewAttachListener inlineWebVideoViewAttachListener) {
        if (i < 0 || i2 < 0 || i3 < 0 || i4 < 0) {
            MMLog.e(TAG, "All position parameters must be greater than or equal to zero.");
            inlineWebVideoViewAttachListener.attachFailed(this);
            return;
        }
        this.mmWebViewRef = new WeakReference<>(mMWebView);
        this.attachListener = inlineWebVideoViewAttachListener;
        this.x = i;
        this.y = i2;
        this.width = i3;
        this.height = i4;
        resizeButtons(false);
        if (this.uri != null && this.mmVideoView != null) {
            this.mmVideoView.setVideoURI(this.uri);
        }
    }

    public void setPlaceholder(final Uri uri2) {
        if (this.mmWebViewRef != null) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    final HttpUtils.Response bitmapFromGetRequest = HttpUtils.getBitmapFromGetRequest(uri2.toString());
                    if (bitmapFromGetRequest != null && bitmapFromGetRequest.code == 200 && bitmapFromGetRequest.bitmap != null) {
                        ThreadUtils.postOnUiThread(new Runnable() {
                            public void run() {
                                MMWebView mMWebView = (MMWebView) InlineWebVideoView.this.mmWebViewRef.get();
                                if (mMWebView != null) {
                                    InlineWebVideoView.this.placeholderView.setImageBitmap(bitmapFromGetRequest.bitmap);
                                    if (mMWebView.getWidth() - InlineWebVideoView.this.x < InlineWebVideoView.this.width || mMWebView.getHeight() - InlineWebVideoView.this.y < InlineWebVideoView.this.height) {
                                        MMLog.e(InlineWebVideoView.TAG, "Cannot attach the inline video; it will not fit within the anchor view.");
                                    } else {
                                        InlineWebVideoView.this.attachToAnchorView(mMWebView);
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void setVideoURI(Uri uri2, FiredEvents firedEvents2) {
        this.firedEvents = firedEvents2;
        this.error = false;
        this.uri = uri2;
        if (this.mmWebViewRef != null && this.mmVideoView != null) {
            this.mmVideoView.setVideoURI(uri2);
            MMWebView mMWebView = this.mmWebViewRef.get();
            if (mMWebView != null) {
                mMWebView.invokeCallback(this.callbackId, getTag(), "stateChange", "loading");
            }
        }
    }

    public void start() {
        if (!this.error) {
            this.inlineVideoControls.start();
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.start could not complete because of a previous error.");
        }
    }

    public void stop() {
        if (!this.error && this.mmVideoView != null) {
            this.mmVideoView.stop();
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.stop could not complete because of a previous error.");
        }
    }

    public void pause() {
        if (!this.error) {
            this.inlineVideoControls.pause();
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.pause could not complete because of a previous error.");
        }
    }

    public void seekTo(int i) {
        if (!this.error && this.mmVideoView != null) {
            this.mmVideoView.seekTo(i);
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.seekTo could not complete because of a previous error.");
        }
    }

    public void expandToFullScreen() {
        this.expandCollapseToggleButton.setChecked(true);
    }

    /* access modifiers changed from: private */
    public void internalExpandToFullScreen() {
        if (!this.error) {
            MMActivity.launch(getContext(), new MMActivity.MMActivityConfig(), new MMActivity.MMActivityListener() {
                public void onCreate(final MMActivity mMActivity) {
                    super.onCreate(mMActivity);
                    ViewUtils.removeFromParent(InlineWebVideoView.this);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                    InlineWebVideoView.this.expandCollapseToggleButton.setOnCheckedChangeListener(null);
                    InlineWebVideoView.this.expandCollapseToggleButton.setChecked(true);
                    InlineWebVideoView.this.expandCollapseToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                            if (!z) {
                                mMActivity.finish();
                            }
                        }
                    });
                    InlineWebVideoView.this.resizeButtons(true);
                    ViewUtils.attachView(mMActivity.getRootView(), InlineWebVideoView.this, layoutParams);
                    MMWebView mMWebView = (MMWebView) InlineWebVideoView.this.mmWebViewRef.get();
                    if (mMWebView != null) {
                        mMWebView.invokeCallback(InlineWebVideoView.this.callbackId, InlineWebVideoView.this.getTag(), "expand");
                    }
                }

                public void onResume(MMActivity mMActivity) {
                    super.onResume(mMActivity);
                }

                public void onPause(MMActivity mMActivity) {
                    super.onPause(mMActivity);
                }

                public void onDestroy(MMActivity mMActivity) {
                    ViewUtils.removeFromParent(InlineWebVideoView.this);
                    AbsoluteLayout.LayoutParams layoutParams = new AbsoluteLayout.LayoutParams(InlineWebVideoView.this.width, InlineWebVideoView.this.height, InlineWebVideoView.this.x, InlineWebVideoView.this.y);
                    InlineWebVideoView.this.expandCollapseToggleButton.setOnCheckedChangeListener(null);
                    InlineWebVideoView.this.expandCollapseToggleButton.setChecked(false);
                    InlineWebVideoView.this.expandCollapseToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                            if (z) {
                                InlineWebVideoView.this.internalExpandToFullScreen();
                            }
                        }
                    });
                    InlineWebVideoView.this.resizeButtons(false);
                    MMWebView mMWebView = (MMWebView) InlineWebVideoView.this.mmWebViewRef.get();
                    if (mMWebView != null) {
                        ViewUtils.attachView(mMWebView, InlineWebVideoView.this, layoutParams);
                        mMWebView.invokeCallback(InlineWebVideoView.this.callbackId, InlineWebVideoView.this.getTag(), "collapse");
                    }
                    super.onDestroy(mMActivity);
                }
            });
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.expandToFullScreen could not complete because of a previous error.");
        }
    }

    public void triggerTimeUpdate() {
        if (!this.error) {
            MMWebView mMWebView = this.mmWebViewRef.get();
            if (mMWebView != null && this.mmVideoView != null) {
                mMWebView.invokeCallback(this.callbackId, getTag(), "timeUpdate", Integer.valueOf(this.mmVideoView.getCurrentPosition()));
            }
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.triggerTimeUpdate could not complete because of a previous error.");
        }
    }

    public void mute() {
        if (!this.error) {
            this.inlineVideoControls.mute();
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.mute could not complete because of a previous error.");
        }
    }

    public void unmute() {
        if (!this.error) {
            this.inlineVideoControls.unmute();
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.unmute could not complete because of a previous error.");
        }
    }

    public void remove() {
        if (this.mmVideoView != null) {
            this.mmVideoView.stop();
        }
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            mMWebView.invokeCallback(this.callbackId, getTag(), "stateChange", "removed");
        }
        ViewUtils.removeFromParent(this);
    }

    public void reposition(int i, int i2, int i3, int i4) {
        if (!this.error) {
            if (i < 0 || i2 < 0 || i3 < 0 || i4 < 0) {
                MMLog.e(TAG, "All position parameters must be greater than or equal to zero.");
            } else if (this.mmWebViewRef != null) {
                MMWebView mMWebView = this.mmWebViewRef.get();
                if (mMWebView == null) {
                    MMLog.w(TAG, "Cannot position the InlineVideoView because the anchor view is gone.");
                } else if (mMWebView.getWidth() - i < i3 || mMWebView.getHeight() - i2 < i4) {
                    MMLog.e(TAG, "Cannot reposition the inline video as it will not fit within the anchor view.");
                } else {
                    this.width = i3;
                    this.height = i4;
                    this.x = i;
                    this.y = i2;
                    resizeButtons(false);
                    AbsoluteLayout.LayoutParams layoutParams = new AbsoluteLayout.LayoutParams(i3, i4, i, i2);
                    ViewUtils.removeFromParent(this);
                    ViewUtils.attachView(mMWebView, this, layoutParams);
                    DisplayMetrics displayMetrics = mMWebView.getResources().getDisplayMetrics();
                    mMWebView.invokeCallback(this.callbackId, getTag(), "reposition", Integer.valueOf(toDips(displayMetrics, i3)), Integer.valueOf(toDips(displayMetrics, i4)), Integer.valueOf(toDips(displayMetrics, i)), Integer.valueOf(toDips(displayMetrics, i2)));
                }
            } else {
                MMLog.w(TAG, "Cannot position the InlineVideoView because the anchor view has not been set.");
            }
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView.reposition could not complete because of a previous error.");
        }
    }

    public void onPrepared(final MMVideoView mMVideoView) {
        if (!this.error) {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MMWebView mMWebView = (MMWebView) InlineWebVideoView.this.mmWebViewRef.get();
                    if (mMWebView != null) {
                        mMWebView.invokeCallback(InlineWebVideoView.this.callbackId, InlineWebVideoView.this.getTag(), "stateChange", "loading");
                        mMWebView.invokeCallback(InlineWebVideoView.this.callbackId, InlineWebVideoView.this.getTag(), "updateVideoURL", InlineWebVideoView.this.uri.toString());
                        mMWebView.invokeCallback(InlineWebVideoView.this.callbackId, InlineWebVideoView.this.getTag(), "durationChange", Integer.valueOf(mMVideoView.getDuration()));
                        if (mMWebView.getWidth() - InlineWebVideoView.this.x < InlineWebVideoView.this.width || mMWebView.getHeight() - InlineWebVideoView.this.y < InlineWebVideoView.this.height) {
                            MMLog.e(InlineWebVideoView.TAG, "Cannot attach the inline video; it will not fit within the anchor view.");
                        } else {
                            InlineWebVideoView.this.attachToAnchorView(mMWebView);
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void attachToAnchorView(MMWebView mMWebView) {
        if (getParent() == null) {
            ViewUtils.attachView(mMWebView, this, new AbsoluteLayout.LayoutParams(this.width, this.height, this.x, this.y));
            this.attachListener.attachSucceeded(this);
        }
    }

    public void onReadyToStart(MMVideoView mMVideoView) {
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            mMWebView.invokeCallback(this.callbackId, getTag(), "stateChange", "readyToStart");
            mMWebView.invokeCallback(this.callbackId, getTag(), "updateVideoURL", this.uri.toString());
            mMWebView.invokeCallback(this.callbackId, getTag(), "durationChange", Integer.valueOf(mMVideoView.getDuration()));
        }
    }

    public void onStart(MMVideoView mMVideoView) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                InlineWebVideoView.this.setKeepScreenOn(true);
                ViewUtils.removeFromParent(InlineWebVideoView.this.placeholderView);
            }
        });
        scheduleAutoHideControls();
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            synchronized (this) {
                if (!isEventFired(TJAdUnitConstants.String.VIDEO_START)) {
                    fireTrackingEvent(mMWebView, TJAdUnitConstants.String.VIDEO_START);
                }
            }
            mMWebView.invokeCallback(this.callbackId, getTag(), "stateChange", TJAdUnitConstants.String.VIDEO_PLAYING);
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineWebVideoView anchor WebView is gone.  Tracking events disabled.");
        }
    }

    public void onStop(MMVideoView mMVideoView) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                InlineWebVideoView.this.setKeepScreenOn(false);
                if (InlineWebVideoView.this.placeholderView.getParent() == null) {
                    InlineWebVideoView.this.videoContainer.addView(InlineWebVideoView.this.placeholderView);
                }
            }
        });
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            mMWebView.invokeCallback(this.callbackId, getTag(), "stateChange", TJAdUnitConstants.String.VIDEO_STOPPED);
        }
    }

    public void onPause(MMVideoView mMVideoView) {
        setKeepScreenOnUiThread(false);
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            mMWebView.invokeCallback(this.callbackId, getTag(), "stateChange", TJAdUnitConstants.String.VIDEO_PAUSED);
        }
    }

    public void onComplete(MMVideoView mMVideoView) {
        mMVideoView.seekTo(0);
        setKeepScreenOnUiThread(false);
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            synchronized (this) {
                if (!isEventFired("end")) {
                    fireTrackingEvent(mMWebView, "end");
                }
            }
            mMWebView.invokeCallback(this.callbackId, getTag(), "timeUpdate", Integer.valueOf(mMVideoView.getDuration()));
            mMWebView.invokeCallback(this.callbackId, getTag(), "stateChange", TJAdUnitConstants.String.VIDEO_COMPLETE);
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineVideoView anchor WebView is gone.  Tracking events disabled.");
        }
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (InlineWebVideoView.this.placeholderView.getParent() == null) {
                    InlineWebVideoView.this.videoContainer.addView(InlineWebVideoView.this.placeholderView);
                }
            }
        });
    }

    public synchronized void onProgress(MMVideoView mMVideoView, int i) {
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            int duration = mMVideoView.getDuration() / 4;
            if (!isEventFired("q1") && i >= duration) {
                fireTrackingEvent(mMWebView, "q1");
            }
            if (!isEventFired("q2") && i >= duration * 2) {
                fireTrackingEvent(mMWebView, "q2");
            }
            if (!isEventFired("q3") && i >= duration * 3) {
                fireTrackingEvent(mMWebView, "q3");
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (this.timeUpdateInterval != -1 && currentTimeMillis - this.lastUpdateTime >= ((long) this.timeUpdateInterval)) {
                this.lastUpdateTime = currentTimeMillis;
                mMWebView.invokeCallback(this.callbackId, getTag(), "timeUpdate", Integer.valueOf(i));
            }
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "InlineVideoView anchor WebView is gone.  Tracking events disabled.");
        }
    }

    public void onSeek(MMVideoView mMVideoView) {
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            mMWebView.invokeCallback(this.callbackId, getTag(), "seek", Integer.valueOf(mMVideoView.getCurrentPosition()));
        }
    }

    public void onMuted(MMVideoView mMVideoView) {
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            mMWebView.invokeCallback(this.callbackId, getTag(), "mute", true);
        }
    }

    public void onUnmuted(MMVideoView mMVideoView) {
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            mMWebView.invokeCallback(this.callbackId, getTag(), "mute", false);
        }
    }

    public void onError(MMVideoView mMVideoView) {
        this.error = true;
        setKeepScreenOnUiThread(false);
        MMWebView mMWebView = this.mmWebViewRef.get();
        if (mMWebView != null) {
            mMWebView.invokeCallback(this.callbackId, getTag(), "error", "Inline video play back failed.");
        }
        if (getParent() == null) {
            this.attachListener.attachFailed(this);
        }
    }

    /* access modifiers changed from: private */
    public void scheduleAutoHideControls() {
        if (this.showExpandControls || this.showMediaControls) {
            if (this.hideControlsRunnable != null) {
                this.hideControlsRunnable.cancel();
            }
            this.hideControlsRunnable = ThreadUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    InlineWebVideoView.this.inlineVideoControls.animate().alpha(0.0f).setDuration(500).setListener(new Animator.AnimatorListener() {
                        public void onAnimationCancel(Animator animator) {
                        }

                        public void onAnimationRepeat(Animator animator) {
                        }

                        public void onAnimationStart(Animator animator) {
                        }

                        public void onAnimationEnd(Animator animator) {
                            InlineWebVideoView.this.inlineVideoControls.setVisibility(8);
                        }
                    }).start();
                    InlineWebVideoView.this.expandCollapseToggleButton.animate().alpha(0.0f).setDuration(500).setListener(new Animator.AnimatorListener() {
                        public void onAnimationCancel(Animator animator) {
                        }

                        public void onAnimationRepeat(Animator animator) {
                        }

                        public void onAnimationStart(Animator animator) {
                        }

                        public void onAnimationEnd(Animator animator) {
                            InlineWebVideoView.this.expandCollapseToggleButton.setVisibility(8);
                        }
                    }).start();
                }
            }, 2500);
        }
    }

    private void setKeepScreenOnUiThread(final boolean z) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                InlineWebVideoView.this.setKeepScreenOn(z);
            }
        });
    }

    /* access modifiers changed from: private */
    public void resizeButtons(boolean z) {
        this.inlineVideoControls.resize(z);
        Rect buttonDimensions = getButtonDimensions(z);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.expandCollapseToggleButton.getLayoutParams();
        layoutParams.width = buttonDimensions.width();
        layoutParams.height = buttonDimensions.height();
        this.expandCollapseToggleButton.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public Rect getButtonDimensions(boolean z) {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.mmadsdk_control_button_max_width_height);
        if (z) {
            return new Rect(0, 0, dimensionPixelSize, dimensionPixelSize);
        }
        int max = Math.max(getResources().getDimensionPixelSize(R.dimen.mmadsdk_control_button_min_width_height), Math.min(dimensionPixelSize, this.height / 5));
        return new Rect(0, 0, max, max);
    }

    private int toDips(DisplayMetrics displayMetrics, int i) {
        return (int) Math.ceil((double) (((float) i) / displayMetrics.density));
    }

    /* access modifiers changed from: private */
    public void fireClientSideOnClick() {
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                if (InlineWebVideoView.this.inlineWebVideoViewListener != null) {
                    InlineWebVideoView.this.inlineWebVideoViewListener.onClicked();
                }
            }
        });
    }

    private boolean isEventFired(String str) {
        if (this.firedEvents != null) {
            return this.firedEvents.isEventFiredForUri(this.uri, str);
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("InlineVideoView[%s]: Cannot check if event  '%s' was fired or not! Field 'firedEvents' is null!", getTag(), str));
        }
        return false;
    }

    private void fireTrackingEvent(MMWebView mMWebView, String str) {
        if (this.firedEvents != null) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, String.format("InlineVideoView[%s]: firing '%s' event", getTag(), str));
            }
            mMWebView.invokeCallback(this.callbackId, getTag(), TRACKING, str);
            this.firedEvents.recordForUri(this.uri, str);
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, String.format("InlineVideoView[%s]: Cannot fire event '%s'! Field 'firedEvents' is null!", getTag(), str));
        }
    }
}
