package com.millennialmedia.internal.video;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.Uri;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.millennialmedia.MMLog;
import com.millennialmedia.R;
import com.millennialmedia.internal.MMWebView;
import com.millennialmedia.internal.SizableStateManager;
import com.millennialmedia.internal.adcontrollers.LightboxController;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.TrackingEvent;
import com.millennialmedia.internal.utils.Utils;
import com.millennialmedia.internal.utils.VideoTrackingEvent;
import com.millennialmedia.internal.utils.ViewUtils;
import com.millennialmedia.internal.video.MMVideoView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@SuppressLint({"ViewConstructor"})
public class LightboxView extends RelativeLayout implements MMVideoView.MMVideoViewListener, View.OnTouchListener {
    private static final int COLLAPSING = 3;
    private static final int DEFAULT = 0;
    private static final int EXPANDED = 4;
    private static final int EXPANDING = 2;
    private static final int SWIPE_AWAY = 1;
    private static final String TAG = "LightboxView";
    /* access modifiers changed from: private */
    public volatile boolean animating = false;
    /* access modifiers changed from: private */
    public volatile boolean complete = false;
    private boolean completeFired = false;
    /* access modifiers changed from: private */
    public int defaultHeight;
    /* access modifiers changed from: private */
    public int defaultWidth;
    private float downX;
    private float downY;
    /* access modifiers changed from: private */
    public ImageView fullscreenCompanion;
    private boolean fullscreenCompanionLoadedFired = false;
    /* access modifiers changed from: private */
    public MMWebView fullscreenCompanionWebView;
    /* access modifiers changed from: private */
    public FrameLayout fullscreenContainer;
    /* access modifiers changed from: private */
    public int fullscreenContainerTopMargin;
    private boolean landscape = false;
    /* access modifiers changed from: private */
    public LightboxController.LightboxAd lightboxAd;
    /* access modifiers changed from: private */
    public int lightboxBottomMargin;
    /* access modifiers changed from: private */
    public int lightboxRightMargin;
    /* access modifiers changed from: private */
    public LightboxViewListener lightboxViewListener;
    private boolean midpointFired = false;
    /* access modifiers changed from: private */
    public ImageView minimizeButton;
    /* access modifiers changed from: private */
    public ThreadUtils.ScheduledRunnable minimizeFadeOutRunnable = null;
    private float originalX;
    private float originalY;
    private volatile boolean prepared = false;
    private boolean q1Fired = false;
    private boolean q3Fired = false;
    /* access modifiers changed from: private */
    public ImageView replayButton;
    private float scaleFactor;
    private boolean startFired = false;
    /* access modifiers changed from: private */
    public volatile int state = 0;
    /* access modifiers changed from: private */
    public int topMargin;
    /* access modifiers changed from: private */
    public MMVideoView videoView;
    private ViewUtils.ViewabilityWatcher videoViewabilityWatcher;
    private WindowManager windowManager;

    public interface LightboxViewListener {
        void onAdLeftApplication();

        void onClicked();

        void onCollapsed();

        void onExpanded();

        void onFailed();

        void onPrepared();

        void onReadyToStart();
    }

    public void onBufferingUpdate(MMVideoView mMVideoView, int i) {
    }

    public void onMuted(MMVideoView mMVideoView) {
    }

    public void onSeek(MMVideoView mMVideoView) {
    }

    public void onUnmuted(MMVideoView mMVideoView) {
    }

    static class MMVideoViewViewabilityListener implements ViewUtils.ViewabilityListener {
        WeakReference<LightboxView> lightBoxViewRef;

        MMVideoViewViewabilityListener(LightboxView lightboxView) {
            this.lightBoxViewRef = new WeakReference<>(lightboxView);
        }

        public void onViewableChanged(boolean z) {
            LightboxView lightboxView = this.lightBoxViewRef.get();
            if (lightboxView != null && !lightboxView.complete && lightboxView.videoView != null) {
                if (z) {
                    lightboxView.videoView.start();
                } else {
                    lightboxView.videoView.pause();
                }
            }
        }
    }

    public LightboxView(Context context, final LightboxController.LightboxAd lightboxAd2, LightboxViewListener lightboxViewListener2) {
        super(context);
        this.windowManager = (WindowManager) context.getSystemService("window");
        getDisplaySize();
        Resources resources = getResources();
        this.defaultWidth = resources.getDimensionPixelSize(R.dimen.mmadsdk_lightbox_width);
        this.defaultHeight = resources.getDimensionPixelSize(R.dimen.mmadsdk_lightbox_height);
        this.lightboxBottomMargin = resources.getDimensionPixelSize(R.dimen.mmadsdk_lightbox_bottom_margin);
        this.lightboxRightMargin = resources.getDimensionPixelSize(R.dimen.mmadsdk_lightbox_right_margin);
        this.topMargin = resources.getDimensionPixelSize(R.dimen.mmadsdk_lightbox_top_margin);
        this.fullscreenContainerTopMargin = resources.getDimensionPixelSize(R.dimen.mmadsdk_lightbox_fullscreen_companion_top_margin);
        setBackgroundColor(resources.getColor(17170445));
        setOnTouchListener(this);
        this.lightboxViewListener = lightboxViewListener2;
        this.lightboxAd = lightboxAd2;
        this.videoView = new MMVideoView(context, false, true, null, this);
        this.videoView.setId(R.id.mmadsdk_light_box_video_view);
        this.videoView.setVideoURI(Uri.parse(lightboxAd2.video.uri));
        this.videoView.setBackgroundColor(resources.getColor(17170444));
        this.minimizeButton = new ImageView(context);
        this.minimizeButton.setVisibility(8);
        this.minimizeButton.setBackgroundColor(0);
        this.minimizeButton.setImageDrawable(getResources().getDrawable(R.drawable.mmadsdk_lightbox_down));
        this.minimizeButton.setTag("mmLightboxVideo_minimizeButton");
        this.minimizeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LightboxView.this.animateFromExpandedToDefault();
            }
        });
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.mmadsdk_lightbox_minimize_button_width), getResources().getDimensionPixelSize(R.dimen.mmadsdk_lightbox_minimize_button_height));
        layoutParams.topMargin = getResources().getDimensionPixelSize(R.dimen.mmadsdk_lightbox_minimize_button_top_margin);
        layoutParams.rightMargin = getResources().getDimensionPixelSize(R.dimen.mmadsdk_lightbox_minimize_button_right_margin);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        this.videoView.addView(this.minimizeButton, layoutParams);
        this.replayButton = new ImageView(context);
        this.replayButton.setVisibility(8);
        this.replayButton.setBackgroundColor(0);
        this.replayButton.setImageDrawable(getResources().getDrawable(R.drawable.mmadsdk_lightbox_replay));
        this.replayButton.setTag("mmLightboxVideo_replayButton");
        this.replayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean unused = LightboxView.this.complete = false;
                LightboxView.this.replayButton.setVisibility(8);
                if (LightboxView.this.videoView != null) {
                    LightboxView.this.videoView.restart();
                }
                if (LightboxView.this.state == 4) {
                    LightboxView.this.startMinimizeFadeOut(0, 500);
                }
            }
        });
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.mmadsdk_lightbox_replay_button_width), getResources().getDimensionPixelSize(R.dimen.mmadsdk_lightbox_replay_button_height));
        layoutParams2.addRule(13);
        this.videoView.addView(this.replayButton, layoutParams2);
        this.videoViewabilityWatcher = new ViewUtils.ViewabilityWatcher(this.videoView, new MMVideoViewViewabilityListener(this));
        this.fullscreenContainer = new FrameLayout(context);
        this.fullscreenCompanion = new ImageView(context);
        this.fullscreenCompanion.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.fullscreenCompanion.setBackgroundColor(getResources().getColor(R.color.mmadsdk_lightbox_curtain_background));
        if (!Utils.isEmpty(lightboxAd2.fullscreen.imageUri)) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    final HttpUtils.Response bitmapFromGetRequest = HttpUtils.getBitmapFromGetRequest(lightboxAd2.fullscreen.imageUri);
                    if (bitmapFromGetRequest.code == 200) {
                        ThreadUtils.postOnUiThread(new Runnable() {
                            public void run() {
                                LightboxView.this.fullscreenCompanion.setImageBitmap(bitmapFromGetRequest.bitmap);
                            }
                        });
                    }
                }
            });
        }
        this.fullscreenContainer.addView(this.fullscreenCompanion);
        this.fullscreenCompanionWebView = new MMWebView(context, MMWebView.MMWebViewOptions.getDefault(), createMMWebViewListener(lightboxViewListener2));
        this.fullscreenCompanionWebView.setContent(lightboxAd2.fullscreen.webContent);
        this.fullscreenCompanionWebView.setTag("mmLightboxVideo_companionWebView");
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams3.topMargin = this.fullscreenContainerTopMargin;
        layoutParams3.addRule(3, R.id.mmadsdk_light_box_video_view);
        this.fullscreenContainer.setVisibility(8);
        ViewUtils.attachView(this, this.fullscreenContainer, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -1);
        if (!this.landscape) {
            layoutParams4.addRule(10);
        }
        setTag("mmLightboxVideo_videoView");
        ViewUtils.attachView(this, this.videoView, layoutParams4);
    }

    public void release() {
        releaseVideo();
    }

    /* access modifiers changed from: private */
    public void releaseVideo() {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (LightboxView.this.videoView != null) {
                    LightboxView.this.videoView.release();
                    MMVideoView unused = LightboxView.this.videoView = null;
                }
                if (LightboxView.this.fullscreenCompanionWebView != null) {
                    LightboxView.this.fullscreenCompanionWebView.release();
                    MMWebView unused2 = LightboxView.this.fullscreenCompanionWebView = null;
                }
            }
        });
    }

    public void start() {
        if (this.videoView != null) {
            this.videoView.start();
        }
    }

    public boolean isPrepared() {
        return this.prepared;
    }

    public void onPrepared(MMVideoView mMVideoView) {
        this.prepared = true;
        this.lightboxViewListener.onPrepared();
    }

    public void onReadyToStart(MMVideoView mMVideoView) {
        this.lightboxViewListener.onReadyToStart();
    }

    public void onStart(MMVideoView mMVideoView) {
        setScreenOn(true);
        if (!this.startFired) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "LightboxView firing start event");
            }
            this.startFired = true;
            fireVideoTrackingEvents(this.lightboxAd.video.trackingEvents.get(LightboxController.TrackableEvent.start), 0);
        }
    }

    public void onStop(MMVideoView mMVideoView) {
        setScreenOn(false);
    }

    public void onPause(MMVideoView mMVideoView) {
        setScreenOn(false);
    }

    public void onComplete(MMVideoView mMVideoView) {
        this.complete = true;
        if (!this.completeFired) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "LightboxView firing complete event");
            }
            this.completeFired = true;
            fireVideoTrackingEvents(this.lightboxAd.video.trackingEvents.get(LightboxController.TrackableEvent.complete), getDuration());
        }
        if (this.minimizeFadeOutRunnable != null) {
            this.minimizeFadeOutRunnable.cancel();
            this.minimizeFadeOutRunnable = null;
        }
        setScreenOn(false);
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (LightboxView.this.state == 4) {
                    LightboxView.this.minimizeButton.setVisibility(0);
                    LightboxView.this.minimizeButton.setAlpha(1.0f);
                }
                LightboxView.this.replayButton.setVisibility(0);
            }
        });
    }

    public synchronized void onProgress(MMVideoView mMVideoView, int i) {
        int duration = mMVideoView.getDuration() / 4;
        if (!this.q1Fired && i >= duration) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "LightboxView firing q1 event");
            }
            this.q1Fired = true;
            fireVideoTrackingEvents(this.lightboxAd.video.trackingEvents.get(LightboxController.TrackableEvent.firstQuartile), i);
        }
        if (!this.midpointFired && i >= duration * 2) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "LightboxView firing midpoint event");
            }
            this.midpointFired = true;
            fireVideoTrackingEvents(this.lightboxAd.video.trackingEvents.get(LightboxController.TrackableEvent.midpoint), i);
        }
        if (!this.q3Fired && i >= duration * 3) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "LightboxView firing q3 event");
            }
            this.q3Fired = true;
            fireVideoTrackingEvents(this.lightboxAd.video.trackingEvents.get(LightboxController.TrackableEvent.thirdQuartile), i);
        }
    }

    public void onError(MMVideoView mMVideoView) {
        setScreenOn(false);
        this.lightboxViewListener.onFailed();
    }

    /* JADX WARN: Type inference failed for: r18v0, types: [android.view.View] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r18, android.view.MotionEvent r19) {
        /*
            r17 = this;
            r0 = r17
            boolean r1 = r0.animating
            r2 = 1
            if (r1 != 0) goto L_0x037a
            com.millennialmedia.internal.video.MMVideoView r1 = r0.videoView
            if (r1 != 0) goto L_0x000d
            goto L_0x037a
        L_0x000d:
            int r1 = r19.getActionMasked()
            if (r1 != 0) goto L_0x0020
            float r1 = r19.getRawX()
            r0.downX = r1
            float r1 = r19.getRawY()
            r0.downY = r1
            return r2
        L_0x0020:
            int r1 = r19.getActionMasked()
            r3 = 3
            r4 = 4
            r5 = 2
            r6 = 0
            if (r1 != r5) goto L_0x02e3
            android.graphics.Point r1 = r17.getDisplaySize()
            float r7 = r19.getRawX()
            float r8 = r19.getRawY()
            float r9 = r0.downX
            float r9 = r9 - r7
            double r9 = (double) r9
            r11 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r9 = java.lang.Math.pow(r9, r11)
            float r13 = r0.downY
            float r13 = r13 - r8
            double r13 = (double) r13
            double r11 = java.lang.Math.pow(r13, r11)
            double r9 = r9 + r11
            int r9 = (int) r9
            double r9 = (double) r9
            double r9 = java.lang.Math.sqrt(r9)
            android.graphics.Point r11 = r17.getDefaultPosition()
            r12 = 4632233691727265792(0x4049000000000000, double:50.0)
            int r14 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
            r9 = 1065353216(0x3f800000, float:1.0)
            r10 = 0
            if (r14 <= 0) goto L_0x012d
            int r12 = r0.state
            if (r12 == 0) goto L_0x0064
            int r12 = r0.state
            if (r12 != r4) goto L_0x012d
        L_0x0064:
            int r12 = r0.state
            if (r12 != 0) goto L_0x0073
            int r12 = r11.x
            float r12 = (float) r12
            r0.originalX = r12
            int r12 = r11.y
            float r12 = (float) r12
            r0.originalY = r12
            goto L_0x0077
        L_0x0073:
            r0.originalX = r10
            r0.originalY = r10
        L_0x0077:
            float r12 = r0.downX
            float r12 = r12 - r7
            float r12 = java.lang.Math.abs(r12)
            float r13 = r0.downY
            float r13 = r13 - r8
            float r13 = java.lang.Math.abs(r13)
            int r12 = (r12 > r13 ? 1 : (r12 == r13 ? 0 : -1))
            if (r12 <= 0) goto L_0x0091
            int r12 = r0.state
            if (r12 == r4) goto L_0x0091
            r0.state = r2
            goto L_0x012d
        L_0x0091:
            float r12 = r0.downY
            int r12 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            r13 = -1
            r14 = -2
            r15 = 1063675494(0x3f666666, float:0.9)
            if (r12 >= 0) goto L_0x00df
            int r12 = r0.state
            if (r12 == r4) goto L_0x00df
            r0.state = r5
            android.widget.ImageView r4 = r0.fullscreenCompanion
            r4.setAlpha(r9)
            android.widget.ImageView r4 = r0.fullscreenCompanion
            r4.setVisibility(r6)
            com.millennialmedia.internal.MMWebView r4 = r0.fullscreenCompanionWebView
            android.view.ViewParent r4 = r4.getParent()
            if (r4 == 0) goto L_0x00bb
            android.widget.FrameLayout r4 = r0.fullscreenContainer
            com.millennialmedia.internal.MMWebView r12 = r0.fullscreenCompanionWebView
            r4.removeView(r12)
        L_0x00bb:
            int r4 = r1.y
            int r12 = r17.getHeight()
            int r4 = r4 - r12
            float r4 = (float) r4
            float r12 = r0.downY
            float r12 = r12 * r15
            float r4 = r4 / r12
            r0.scaleFactor = r4
            boolean r4 = r0.landscape
            if (r4 != 0) goto L_0x00d5
            com.millennialmedia.internal.video.MMVideoView r4 = r0.videoView
            android.view.ViewGroup$LayoutParams r4 = r4.getLayoutParams()
            r4.height = r14
        L_0x00d5:
            r0.setTranslationX(r10)
            android.view.ViewGroup$LayoutParams r4 = r17.getLayoutParams()
            r4.width = r13
            goto L_0x012d
        L_0x00df:
            float r4 = r0.downY
            int r4 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x012d
            r0.state = r3
            android.widget.ImageView r4 = r0.fullscreenCompanion
            r4.setAlpha(r9)
            android.widget.ImageView r4 = r0.fullscreenCompanion
            r4.setVisibility(r6)
            android.widget.FrameLayout r4 = r0.fullscreenContainer
            com.millennialmedia.internal.MMWebView r12 = r0.fullscreenCompanionWebView
            r4.removeView(r12)
            android.content.res.Resources r4 = r17.getResources()
            r12 = 17170445(0x106000d, float:2.461195E-38)
            int r4 = r4.getColor(r12)
            r0.setBackgroundColor(r4)
            int r4 = r17.getHeight()
            int r12 = r0.defaultHeight
            int r4 = r4 - r12
            int r12 = r1.y
            float r12 = (float) r12
            float r9 = r0.downY
            float r12 = r12 - r9
            float r12 = r12 * r15
            float r4 = (float) r4
            float r4 = r4 / r12
            r0.scaleFactor = r4
            boolean r4 = r0.landscape
            if (r4 != 0) goto L_0x0124
            com.millennialmedia.internal.video.MMVideoView r4 = r0.videoView
            android.view.ViewGroup$LayoutParams r4 = r4.getLayoutParams()
            r4.height = r14
        L_0x0124:
            r0.setTranslationX(r10)
            android.view.ViewGroup$LayoutParams r4 = r17.getLayoutParams()
            r4.width = r13
        L_0x012d:
            int r4 = r0.state
            if (r4 == 0) goto L_0x02e2
            int r4 = r0.state
            if (r4 != r2) goto L_0x0155
            float r3 = r0.downX
            float r3 = r3 - r7
            float r4 = r0.originalX
            float r4 = r4 - r3
            int r3 = r17.getWidth()
            float r3 = (float) r3
            float r3 = r3 + r4
            int r5 = r1.x
            float r5 = (float) r5
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x0150
            int r1 = r1.x
            int r3 = r17.getWidth()
            int r1 = r1 - r3
            float r4 = (float) r1
        L_0x0150:
            r0.setTranslationX(r4)
            goto L_0x02e2
        L_0x0155:
            int r4 = r0.state
            r7 = 8
            if (r4 != r5) goto L_0x0224
            float r3 = r0.downY
            float r3 = r3 - r8
            float r4 = r0.scaleFactor
            float r3 = r3 * r4
            float r4 = r0.originalY
            float r4 = r4 - r3
            int r5 = r0.defaultHeight
            float r5 = (float) r5
            float r5 = r5 + r3
            int r3 = r0.lightboxBottomMargin
            float r3 = (float) r3
            float r5 = r5 + r3
            int r3 = (int) r5
            int r5 = r0.defaultHeight
            int r5 = r3 - r5
            float r5 = (float) r5
            int r8 = r1.y
            int r9 = r0.defaultHeight
            int r8 = r8 - r9
            float r8 = (float) r8
            float r5 = r5 / r8
            int r8 = r0.defaultWidth
            int r9 = r1.x
            int r12 = r0.defaultWidth
            int r9 = r9 - r12
            float r9 = (float) r9
            float r9 = r9 * r5
            int r9 = (int) r9
            int r8 = r8 + r9
            int r9 = r1.x
            int r9 = r9 - r8
            int r12 = r0.lightboxRightMargin
            int r13 = r0.lightboxRightMargin
            float r13 = (float) r13
            float r13 = r13 * r5
            int r13 = (int) r13
            int r12 = r12 - r13
            int r9 = r9 - r12
            int r12 = r0.topMargin
            float r12 = (float) r12
            float r12 = r12 * r5
            int r12 = (int) r12
            int r13 = r0.topMargin
            int r12 = java.lang.Math.min(r12, r13)
            int r13 = r0.fullscreenContainerTopMargin
            int r14 = r0.fullscreenContainerTopMargin
            float r14 = (float) r14
            float r5 = r5 * r14
            int r5 = (int) r5
            int r13 = r13 - r5
            int r5 = java.lang.Math.max(r6, r13)
            int r13 = r0.defaultWidth
            if (r8 <= r13) goto L_0x01d8
            int r13 = r0.defaultHeight
            if (r3 <= r13) goto L_0x01d8
            int r13 = r11.x
            if (r9 >= r13) goto L_0x01d8
            int r13 = r11.y
            float r13 = (float) r13
            int r13 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
            if (r13 < 0) goto L_0x01bb
            goto L_0x01d8
        L_0x01bb:
            int r7 = r1.x
            if (r8 >= r7) goto L_0x01ce
            int r7 = r1.y
            if (r3 >= r7) goto L_0x01ce
            if (r9 <= 0) goto L_0x01ce
            int r7 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r7 > 0) goto L_0x01ca
            goto L_0x01ce
        L_0x01ca:
            r10 = r4
            r1 = r5
            r4 = r9
            goto L_0x01e9
        L_0x01ce:
            int r8 = r1.x
            int r3 = r1.y
            int r1 = r0.topMargin
            r12 = r1
            r1 = r6
            r4 = r1
            goto L_0x01e9
        L_0x01d8:
            int r1 = r0.fullscreenContainerTopMargin
            int r8 = r0.defaultWidth
            int r3 = r0.defaultHeight
            int r4 = r11.y
            float r10 = (float) r4
            int r4 = r11.x
            android.widget.FrameLayout r5 = r0.fullscreenContainer
            r5.setVisibility(r7)
            r12 = r6
        L_0x01e9:
            com.millennialmedia.internal.video.MMVideoView r5 = r0.videoView
            android.view.ViewGroup$LayoutParams r5 = r5.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r5 = (android.widget.RelativeLayout.LayoutParams) r5
            r5.topMargin = r12
            android.widget.FrameLayout r7 = r0.fullscreenContainer
            android.view.ViewGroup$LayoutParams r7 = r7.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r7 = (android.widget.RelativeLayout.LayoutParams) r7
            r7.topMargin = r1
            android.widget.FrameLayout r1 = r0.fullscreenContainer
            int r1 = r1.getVisibility()
            if (r1 == 0) goto L_0x020e
            boolean r1 = r0.landscape
            if (r1 != 0) goto L_0x020e
            android.widget.FrameLayout r1 = r0.fullscreenContainer
            r1.setVisibility(r6)
        L_0x020e:
            com.millennialmedia.internal.video.MMVideoView r1 = r0.videoView
            float r4 = (float) r4
            r1.setTranslationX(r4)
            r0.setTranslationY(r10)
            r5.width = r8
            r0.setHeight(r3)
            r17.requestLayout()
            r17.invalidate()
            goto L_0x02e2
        L_0x0224:
            int r4 = r0.state
            if (r4 != r3) goto L_0x02e2
            float r3 = r0.downY
            float r3 = r3 - r8
            float r4 = r0.scaleFactor
            float r3 = r3 * r4
            float r4 = r0.originalY
            float r4 = r4 - r3
            int r5 = r1.y
            float r5 = (float) r5
            float r5 = r5 + r3
            int r3 = (int) r5
            int r5 = r0.defaultHeight
            int r5 = r3 - r5
            float r5 = (float) r5
            int r8 = r1.y
            int r9 = r0.defaultHeight
            int r8 = r8 - r9
            float r8 = (float) r8
            float r5 = r5 / r8
            int r8 = r0.defaultWidth
            int r9 = r1.x
            int r12 = r0.defaultWidth
            int r9 = r9 - r12
            float r9 = (float) r9
            float r9 = r9 * r5
            int r9 = (int) r9
            int r8 = r8 + r9
            int r9 = r1.x
            int r9 = r9 - r8
            int r12 = r0.lightboxRightMargin
            int r13 = r0.lightboxRightMargin
            float r13 = (float) r13
            float r13 = r13 * r5
            int r13 = (int) r13
            int r12 = r12 - r13
            int r9 = r9 - r12
            int r12 = r0.topMargin
            float r12 = (float) r12
            float r12 = r12 * r5
            int r12 = (int) r12
            r13 = 1065353216(0x3f800000, float:1.0)
            float r5 = r13 - r5
            int r13 = r0.fullscreenContainerTopMargin
            float r13 = (float) r13
            float r5 = r5 * r13
            int r5 = (int) r5
            int r5 = java.lang.Math.max(r6, r5)
            int r13 = r0.defaultWidth
            if (r8 <= r13) goto L_0x029c
            int r13 = r0.defaultHeight
            if (r3 <= r13) goto L_0x029c
            int r13 = r11.x
            if (r9 >= r13) goto L_0x029c
            int r13 = r11.y
            float r13 = (float) r13
            int r13 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
            if (r13 < 0) goto L_0x027f
            goto L_0x029c
        L_0x027f:
            int r11 = r1.x
            if (r8 >= r11) goto L_0x0292
            int r11 = r1.y
            if (r3 >= r11) goto L_0x0292
            if (r9 <= 0) goto L_0x0292
            int r11 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r11 > 0) goto L_0x028e
            goto L_0x0292
        L_0x028e:
            r10 = r4
            r1 = r5
            r4 = r9
            goto L_0x02ad
        L_0x0292:
            int r8 = r1.x
            int r3 = r1.y
            int r1 = r0.topMargin
            r12 = r1
            r1 = r6
            r4 = r1
            goto L_0x02ad
        L_0x029c:
            int r1 = r0.fullscreenContainerTopMargin
            int r8 = r0.defaultWidth
            int r3 = r0.defaultHeight
            int r4 = r11.y
            float r10 = (float) r4
            int r4 = r11.x
            android.widget.FrameLayout r5 = r0.fullscreenContainer
            r5.setVisibility(r7)
            r12 = r6
        L_0x02ad:
            android.widget.ImageView r5 = r0.minimizeButton
            int r5 = r5.getVisibility()
            if (r5 != 0) goto L_0x02ba
            android.widget.ImageView r5 = r0.minimizeButton
            r5.setVisibility(r7)
        L_0x02ba:
            com.millennialmedia.internal.video.MMVideoView r5 = r0.videoView
            android.view.ViewGroup$LayoutParams r5 = r5.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r5 = (android.widget.RelativeLayout.LayoutParams) r5
            r5.topMargin = r12
            android.widget.FrameLayout r6 = r0.fullscreenContainer
            android.view.ViewGroup$LayoutParams r6 = r6.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r6 = (android.widget.RelativeLayout.LayoutParams) r6
            r6.topMargin = r1
            com.millennialmedia.internal.video.MMVideoView r1 = r0.videoView
            float r4 = (float) r4
            r1.setTranslationX(r4)
            r0.setTranslationY(r10)
            r5.width = r8
            r0.setHeight(r3)
            r17.requestLayout()
            r17.invalidate()
        L_0x02e2:
            return r2
        L_0x02e3:
            int r1 = r19.getActionMasked()
            if (r1 != r2) goto L_0x0379
            android.graphics.Point r1 = r17.getDisplaySize()
            int r7 = r0.state
            if (r7 != r5) goto L_0x0302
            int r3 = r17.getHeight()
            int r5 = r1.y
            int r5 = r5 / r4
            if (r3 < r5) goto L_0x02fe
            r0.animateToExpand(r1)
            goto L_0x0301
        L_0x02fe:
            r17.animateFromExpandedToDefault()
        L_0x0301:
            return r2
        L_0x0302:
            int r7 = r0.state
            if (r7 != r3) goto L_0x031d
            int r3 = r17.getHeight()
            double r3 = (double) r3
            int r5 = r1.y
            double r5 = (double) r5
            r7 = 4604930618986332160(0x3fe8000000000000, double:0.75)
            double r5 = r5 * r7
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 > 0) goto L_0x0319
            r17.animateFromExpandedToDefault()
            goto L_0x031c
        L_0x0319:
            r0.animateToExpand(r1)
        L_0x031c:
            return r2
        L_0x031d:
            int r3 = r0.state
            if (r3 != r2) goto L_0x033a
            float r3 = r17.getTranslationX()
            int r4 = r1.x
            int r6 = r17.getWidth()
            int r4 = r4 - r6
            int r4 = r4 / r5
            float r4 = (float) r4
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 >= 0) goto L_0x0336
            r0.animateToGone(r2)
            goto L_0x0339
        L_0x0336:
            r0.animateToDefault(r1)
        L_0x0339:
            return r2
        L_0x033a:
            int r3 = r0.state
            r7 = 200(0xc8, double:9.9E-322)
            if (r3 != 0) goto L_0x035f
            long r3 = r19.getEventTime()
            long r9 = r19.getDownTime()
            long r11 = r3 - r9
            int r3 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r3 > 0) goto L_0x0379
            r3 = r18
            if (r3 != r0) goto L_0x0379
            boolean r3 = r0.landscape
            if (r3 != 0) goto L_0x035b
            android.widget.ImageView r3 = r0.fullscreenCompanion
            r3.setVisibility(r6)
        L_0x035b:
            r0.animateToExpand(r1)
            return r2
        L_0x035f:
            int r1 = r0.state
            if (r1 != r4) goto L_0x0379
            long r3 = r19.getEventTime()
            long r9 = r19.getDownTime()
            long r11 = r3 - r9
            int r1 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r1 > 0) goto L_0x0379
            r3 = 2500(0x9c4, double:1.235E-320)
            r5 = 500(0x1f4, double:2.47E-321)
            r0.startMinimizeFadeOut(r3, r5)
            return r2
        L_0x0379:
            return r6
        L_0x037a:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.video.LightboxView.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    public void animateToGone(final boolean z) {
        Point displaySize = getDisplaySize();
        AnonymousClass6 r1 = new Animation() {
            float distanceToOffscreen;
            float translateX;
            int width;

            public boolean willChangeBounds() {
                return false;
            }

            public void initialize(int i, int i2, int i3, int i4) {
                this.width = i;
                this.translateX = LightboxView.this.getTranslationX();
                this.distanceToOffscreen = this.translateX + ((float) i);
            }

            /* access modifiers changed from: protected */
            public void applyTransformation(float f, Transformation transformation) {
                LightboxView.this.setTranslationX(f == 1.0f ? (float) (-1 * this.width) : this.translateX - (f * this.distanceToOffscreen));
            }
        };
        r1.setDuration((long) (((float) displaySize.x) / getContext().getResources().getDisplayMetrics().density));
        r1.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                if (LightboxView.this.videoView != null) {
                    LightboxView.this.videoView.stop();
                }
                if (LightboxView.this.state == 4) {
                    LightboxView.this.lightboxViewListener.onCollapsed();
                }
                if (z) {
                    LightboxView.this.fireVideoTrackingEvents(LightboxView.this.lightboxAd.video.trackingEvents.get(LightboxController.TrackableEvent.videoClose), 0);
                }
                ViewUtils.removeFromParent(LightboxView.this);
                LightboxView.this.releaseVideo();
            }
        });
        startAnimation(r1);
    }

    private void animateToDefault(Point point) {
        this.animating = true;
        this.state = 0;
        final Point defaultPosition = getDefaultPosition();
        AnonymousClass8 r1 = new Animation() {
            float distanceToDefault;
            float translateX;
            int width;

            public boolean willChangeBounds() {
                return false;
            }

            public void initialize(int i, int i2, int i3, int i4) {
                this.width = i;
                this.translateX = LightboxView.this.getTranslationX();
                this.distanceToDefault = ((float) defaultPosition.x) - this.translateX;
            }

            /* access modifiers changed from: protected */
            public void applyTransformation(float f, Transformation transformation) {
                float f2;
                if (f == 1.0f) {
                    f2 = (float) defaultPosition.x;
                } else {
                    f2 = (f * this.distanceToDefault) + this.translateX;
                }
                LightboxView.this.setTranslationX(f2);
            }
        };
        r1.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                boolean unused = LightboxView.this.animating = false;
            }
        });
        r1.setDuration((long) (((float) point.x) / getContext().getResources().getDisplayMetrics().density));
        startAnimation(r1);
    }

    /* access modifiers changed from: private */
    public void animateFromExpandedToDefault() {
        final boolean z = true;
        this.animating = true;
        final Point displaySize = getDisplaySize();
        if (this.state != 2) {
            z = false;
        }
        this.state = 0;
        this.minimizeButton.setVisibility(8);
        setBackgroundColor(getResources().getColor(17170445));
        if (this.fullscreenCompanionWebView.getParent() != null) {
            this.fullscreenContainer.removeView(this.fullscreenCompanionWebView);
        }
        this.fullscreenCompanion.setVisibility(0);
        this.fullscreenCompanion.setAlpha(1.0f);
        if (!this.landscape) {
            this.fullscreenContainer.setVisibility(0);
        }
        final Point defaultPosition = getDefaultPosition();
        AnonymousClass10 r3 = new Animation() {
            int heightDelta;
            int originalHeight;
            int originalWidth;
            int widthDelta;

            public boolean willChangeBounds() {
                return true;
            }

            public void initialize(int i, int i2, int i3, int i4) {
                this.originalHeight = i2;
                this.heightDelta = i2 - LightboxView.this.defaultHeight;
                this.originalWidth = i;
                this.widthDelta = i - LightboxView.this.defaultWidth;
            }

            /* access modifiers changed from: protected */
            public void applyTransformation(float f, Transformation transformation) {
                int i;
                int i2;
                if (LightboxView.this.videoView != null) {
                    int access$1400 = f == 1.0f ? LightboxView.this.defaultHeight : (int) (((float) this.originalHeight) - (((float) this.heightDelta) * f));
                    float access$14002 = ((float) (access$1400 - LightboxView.this.defaultHeight)) / ((float) (displaySize.y - LightboxView.this.defaultHeight));
                    int access$1500 = f == 1.0f ? LightboxView.this.defaultWidth : (int) (((float) LightboxView.this.defaultWidth) + (((float) this.widthDelta) * access$14002));
                    if (f == 1.0f) {
                        i = 0;
                    } else {
                        i = (int) (((float) LightboxView.this.topMargin) * access$14002);
                    }
                    if (f == 1.0f) {
                        i2 = LightboxView.this.fullscreenContainerTopMargin;
                    } else {
                        i2 = LightboxView.this.fullscreenContainerTopMargin - ((int) (((float) LightboxView.this.fullscreenContainerTopMargin) * access$14002));
                    }
                    int max = f == 1.0f ? defaultPosition.x : Math.max(0, (displaySize.x - access$1500) - (LightboxView.this.lightboxRightMargin - ((int) (((float) LightboxView.this.lightboxRightMargin) * access$14002))));
                    int max2 = f == 1.0f ? defaultPosition.y : Math.max(0, (displaySize.y - access$1400) - (LightboxView.this.lightboxBottomMargin - ((int) (((float) LightboxView.this.lightboxBottomMargin) * access$14002))));
                    if (access$1500 <= LightboxView.this.defaultWidth || access$1400 <= LightboxView.this.defaultHeight || max >= defaultPosition.x || max2 >= defaultPosition.y) {
                        i2 = LightboxView.this.fullscreenContainerTopMargin;
                        access$1500 = LightboxView.this.defaultWidth;
                        access$1400 = LightboxView.this.defaultHeight;
                        max2 = defaultPosition.y;
                        max = defaultPosition.x;
                        LightboxView.this.fullscreenContainer.setVisibility(8);
                        i = 0;
                    }
                    if (f == 1.0f) {
                        LightboxView.this.setTranslationX((float) defaultPosition.x);
                        LightboxView.this.setTranslationY((float) defaultPosition.y);
                        LightboxView.this.getLayoutParams().width = LightboxView.this.defaultWidth;
                        ((RelativeLayout.LayoutParams) LightboxView.this.fullscreenContainer.getLayoutParams()).topMargin = LightboxView.this.fullscreenContainerTopMargin;
                        LightboxView.this.setHeight(access$1400);
                        ((RelativeLayout.LayoutParams) LightboxView.this.videoView.getLayoutParams()).topMargin = 0;
                        LightboxView.this.videoView.setTranslationX(0.0f);
                        LightboxView.this.videoView.getLayoutParams().height = -1;
                        LightboxView.this.videoView.getLayoutParams().width = -1;
                    } else {
                        ((RelativeLayout.LayoutParams) LightboxView.this.fullscreenContainer.getLayoutParams()).topMargin = i2;
                        LightboxView.this.setHeight(access$1400);
                        ((RelativeLayout.LayoutParams) LightboxView.this.videoView.getLayoutParams()).topMargin = i;
                        LightboxView.this.videoView.getLayoutParams().width = access$1500;
                        LightboxView.this.setTranslationY((float) max2);
                        LightboxView.this.videoView.setTranslationX((float) max);
                    }
                    LightboxView.this.requestLayout();
                }
            }
        };
        r3.setDuration((long) (((float) displaySize.y) / (getContext().getResources().getDisplayMetrics().density / 2.0f)));
        r3.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                LightboxView.this.fullscreenContainer.setVisibility(8);
                if (LightboxView.this.videoView != null) {
                    LightboxView.this.videoView.mute();
                }
                if (!z) {
                    LightboxView.this.lightboxViewListener.onCollapsed();
                    LightboxView.this.fireVideoTrackingEvents(LightboxView.this.lightboxAd.video.trackingEvents.get(LightboxController.TrackableEvent.videoCollapse), 0);
                }
                boolean unused = LightboxView.this.animating = false;
            }
        });
        startAnimation(r3);
    }

    private void animateToExpand(final Point point) {
        if (this.videoView != null) {
            this.animating = true;
            final boolean z = this.state == 3;
            this.state = 4;
            setBackgroundColor(getResources().getColor(17170445));
            if (!this.fullscreenCompanionLoadedFired && !this.landscape) {
                this.fullscreenCompanionLoadedFired = true;
                fireVideoTrackingEvents(this.lightboxAd.fullscreen.trackingEvents, 0);
            }
            getLayoutParams().width = -1;
            if (!this.landscape) {
                this.videoView.getLayoutParams().height = -2;
            }
            setTranslationX(0.0f);
            if (!this.landscape) {
                this.fullscreenContainer.setVisibility(0);
            }
            AnonymousClass12 r0 = new Animation() {
                int heightDelta;
                int originalHeight;

                public boolean willChangeBounds() {
                    return true;
                }

                public void initialize(int i, int i2, int i3, int i4) {
                    this.originalHeight = i2;
                    this.heightDelta = i4 - i2;
                }

                /* access modifiers changed from: protected */
                public void applyTransformation(float f, Transformation transformation) {
                    int i;
                    int i2;
                    int i3;
                    int i4;
                    if (LightboxView.this.videoView != null) {
                        int i5 = f == 1.0f ? point.y : (int) (((float) this.originalHeight) + (((float) this.heightDelta) * f));
                        float access$1400 = ((float) (i5 - LightboxView.this.defaultHeight)) / ((float) (point.y - LightboxView.this.defaultHeight));
                        if (f == 1.0f) {
                            i = point.x;
                        } else {
                            i = (int) (((float) LightboxView.this.defaultWidth) + (((float) (point.x - LightboxView.this.defaultWidth)) * access$1400));
                        }
                        int access$1600 = f == 1.0f ? LightboxView.this.topMargin : (int) (((float) LightboxView.this.topMargin) * access$1400);
                        int i6 = 0;
                        if (f == 1.0f) {
                            i2 = 0;
                        } else {
                            i2 = LightboxView.this.fullscreenContainerTopMargin - ((int) (((float) LightboxView.this.fullscreenContainerTopMargin) * access$1400));
                        }
                        if (f == 1.0f) {
                            i3 = 0;
                        } else {
                            i3 = Math.max(0, (point.x - i) - (LightboxView.this.lightboxRightMargin - ((int) (((float) LightboxView.this.lightboxRightMargin) * access$1400))));
                        }
                        if (f == 1.0f) {
                            i4 = 0;
                        } else {
                            i4 = Math.max(0, (point.y - i5) - (LightboxView.this.lightboxBottomMargin - ((int) (((float) LightboxView.this.lightboxBottomMargin) * access$1400))));
                        }
                        if (i >= point.x || i5 >= point.y || i3 <= 0 || i4 <= 0) {
                            i = point.x;
                            i5 = point.y;
                            access$1600 = LightboxView.this.topMargin;
                            i4 = 0;
                            i2 = 0;
                        } else {
                            i6 = i3;
                        }
                        ((RelativeLayout.LayoutParams) LightboxView.this.fullscreenContainer.getLayoutParams()).topMargin = i2;
                        LightboxView.this.setHeight(i5);
                        ((RelativeLayout.LayoutParams) LightboxView.this.videoView.getLayoutParams()).topMargin = access$1600;
                        LightboxView.this.videoView.getLayoutParams().width = i;
                        LightboxView.this.setTranslationY((float) i4);
                        LightboxView.this.videoView.setTranslationX((float) i6);
                        LightboxView.this.requestLayout();
                    }
                }
            };
            r0.setDuration((long) (((float) point.y) / (getContext().getResources().getDisplayMetrics().density / 2.0f)));
            r0.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    LightboxView.this.setBackgroundColor(LightboxView.this.getResources().getColor(17170444));
                    if (LightboxView.this.videoView != null) {
                        LightboxView.this.videoView.unmute();
                    }
                    if (!z) {
                        LightboxView.this.lightboxViewListener.onClicked();
                        LightboxView.this.lightboxViewListener.onExpanded();
                        LightboxView.this.fireVideoTrackingEvents(LightboxView.this.lightboxAd.video.trackingEvents.get(LightboxController.TrackableEvent.videoExpand), 0);
                    }
                    LightboxView.this.crossFadeCurtainWebView();
                }
            });
            startAnimation(r0);
        }
    }

    /* access modifiers changed from: private */
    public void crossFadeCurtainWebView() {
        this.fullscreenCompanion.setAlpha(1.0f);
        this.fullscreenCompanionWebView.setAlpha(0.0f);
        if (this.fullscreenCompanionWebView.getParent() == null) {
            this.fullscreenContainer.addView(this.fullscreenCompanionWebView, 0);
        }
        this.fullscreenCompanion.animate().alpha(0.0f).setDuration(1000).setListener(new Animator.AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                LightboxView.this.fullscreenCompanion.setVisibility(8);
                LightboxView.this.startMinimizeFadeOut(2500, 500);
                boolean unused = LightboxView.this.animating = false;
            }
        }).start();
        this.fullscreenCompanionWebView.animate().alpha(1.0f).setDuration(1000).start();
    }

    public Point getDefaultPosition() {
        Point displaySize = getDisplaySize();
        return new Point((displaySize.x - this.lightboxRightMargin) - this.defaultWidth, (displaySize.y - this.lightboxBottomMargin) - this.defaultHeight);
    }

    public Point getDefaultDimensions() {
        return new Point(this.defaultWidth, this.defaultHeight);
    }

    public int getDuration() {
        if (this.videoView == null) {
            return -1;
        }
        return this.videoView.getDuration();
    }

    public int getCurrentPosition() {
        if (this.videoView == null) {
            return -1;
        }
        return this.videoView.getCurrentPosition();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Point displaySize = getDisplaySize();
        this.landscape = displaySize.x > displaySize.y;
        if (!this.landscape && this.videoView != null) {
            ((RelativeLayout.LayoutParams) this.videoView.getLayoutParams()).addRule(10);
        }
        this.videoViewabilityWatcher.startWatching();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.videoViewabilityWatcher.stopWatching();
        super.onDetachedFromWindow();
    }

    private void setScreenOn(final boolean z) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                LightboxView.this.setKeepScreenOn(z);
                if (LightboxView.this.videoView != null) {
                    LightboxView.this.videoView.setKeepScreenOn(z);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void setHeight(int i) {
        Point displaySize = getDisplaySize();
        getLayoutParams().height = Math.max(this.defaultHeight, Math.min(i, displaySize.y));
    }

    private Point getDisplaySize() {
        Point point = new Point();
        this.windowManager.getDefaultDisplay().getSize(point);
        return point;
    }

    /* access modifiers changed from: private */
    public void startMinimizeFadeOut(long j, final long j2) {
        if (this.minimizeFadeOutRunnable != null) {
            this.minimizeFadeOutRunnable.cancel();
        }
        this.minimizeButton.setVisibility(0);
        this.minimizeButton.setAlpha(1.0f);
        this.minimizeFadeOutRunnable = ThreadUtils.runOnUiThreadDelayed(new Runnable() {
            public void run() {
                if (!LightboxView.this.complete) {
                    LightboxView.this.minimizeButton.animate().alpha(0.0f).setDuration(j2).setListener(new Animator.AnimatorListener() {
                        public void onAnimationCancel(Animator animator) {
                        }

                        public void onAnimationRepeat(Animator animator) {
                        }

                        public void onAnimationStart(Animator animator) {
                        }

                        public void onAnimationEnd(Animator animator) {
                            ThreadUtils.ScheduledRunnable unused = LightboxView.this.minimizeFadeOutRunnable = null;
                        }
                    }).start();
                }
            }
        }, j);
    }

    public void onConfigurationChanged(Configuration configuration) {
        clearAnimation();
        Point displaySize = getDisplaySize();
        if (this.landscape && configuration.orientation == 1) {
            this.landscape = false;
            if (this.state == 3 || this.state == 4) {
                goToExpandedPortraitState(displaySize);
            } else if (this.state == 1) {
                ViewUtils.removeFromParent(this);
            } else {
                goToDefaultState();
            }
        } else if (!this.landscape && configuration.orientation == 2) {
            this.landscape = true;
            if (this.state == 3 || this.state == 4) {
                goToExpandedLandscapeState(displaySize);
            } else if (this.state == 1) {
                ViewUtils.removeFromParent(this);
            } else {
                goToDefaultState();
            }
        }
    }

    private void goToExpandedPortraitState(Point point) {
        if (this.videoView != null) {
            this.animating = true;
            this.state = 4;
            ViewUtils.removeFromParent(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(point.x, -2);
            layoutParams.topMargin = this.topMargin;
            this.videoView.setLayoutParams(layoutParams);
            setTranslationX(0.0f);
            setTranslationY(0.0f);
            this.videoView.setTranslationX(0.0f);
            ((RelativeLayout.LayoutParams) this.fullscreenContainer.getLayoutParams()).topMargin = 0;
            this.fullscreenContainer.setVisibility(0);
            setHeight(point.y);
            getLayoutParams().width = -1;
            if (!this.fullscreenCompanionLoadedFired) {
                this.fullscreenCompanionLoadedFired = true;
                fireVideoTrackingEvents(this.lightboxAd.fullscreen.trackingEvents, 0);
            }
            setBackgroundColor(getResources().getColor(17170444));
            ViewUtils.attachView(ViewUtils.getDecorView(this), this);
            this.videoView.unmute();
            crossFadeCurtainWebView();
        }
    }

    private void goToExpandedLandscapeState(Point point) {
        if (this.videoView != null) {
            this.animating = true;
            this.state = 4;
            ViewUtils.removeFromParent(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(point.x, -1);
            layoutParams.topMargin = this.topMargin;
            setTranslationX(0.0f);
            setTranslationY(0.0f);
            this.videoView.setTranslationX(0.0f);
            ((RelativeLayout.LayoutParams) this.fullscreenContainer.getLayoutParams()).topMargin = 0;
            setHeight(point.y);
            this.videoView.setLayoutParams(layoutParams);
            getLayoutParams().width = -1;
            this.fullscreenContainer.setVisibility(8);
            setBackgroundColor(getResources().getColor(17170444));
            ViewUtils.attachView(ViewUtils.getDecorView(this), this);
            this.videoView.unmute();
            crossFadeCurtainWebView();
        }
    }

    private void goToDefaultState() {
        if (this.videoView != null) {
            this.animating = true;
            this.state = 0;
            this.videoView.mute();
            ViewUtils.removeFromParent(this);
            Point defaultPosition = getDefaultPosition();
            setTranslationX((float) defaultPosition.x);
            setTranslationY((float) defaultPosition.y);
            this.videoView.setTranslationX(0.0f);
            setHeight(this.defaultHeight);
            getLayoutParams().width = this.defaultWidth;
            this.videoView.getLayoutParams().height = -1;
            this.videoView.getLayoutParams().width = -1;
            setBackgroundColor(getResources().getColor(17170445));
            this.fullscreenContainer.setVisibility(8);
            ((RelativeLayout.LayoutParams) this.fullscreenContainer.getLayoutParams()).topMargin = this.fullscreenContainerTopMargin;
            ((RelativeLayout.LayoutParams) this.videoView.getLayoutParams()).topMargin = 0;
            ViewUtils.attachView(ViewUtils.getDecorView(this), this);
            this.animating = false;
        }
    }

    /* access modifiers changed from: private */
    public void fireVideoTrackingEvents(List<TrackingEvent> list, int i) {
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (TrackingEvent videoTrackingEvent : list) {
                arrayList.add(new VideoTrackingEvent(videoTrackingEvent, i));
            }
            TrackingEvent.fireEvents(arrayList);
        }
    }

    private MMWebView.MMWebViewListener createMMWebViewListener(final LightboxViewListener lightboxViewListener2) {
        return new MMWebView.MMWebViewListener() {
            public void close() {
            }

            public boolean expand(SizableStateManager.ExpandParams expandParams) {
                return false;
            }

            public void onFailed() {
            }

            public void onLoaded() {
            }

            public void onReady() {
            }

            public void onUnload() {
            }

            public boolean resize(SizableStateManager.ResizeParams resizeParams) {
                return false;
            }

            public void setOrientation(int i) {
            }

            public void onClicked() {
                lightboxViewListener2.onClicked();
            }

            public void onAdLeftApplication() {
                lightboxViewListener2.onAdLeftApplication();
            }
        };
    }
}
