package com.millennialmedia.internal.video;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.widget.ExploreByTouchHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.millennialmedia.MMLog;
import com.millennialmedia.R;
import com.millennialmedia.XIncentivizedEventListener;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.MMWebView;
import com.millennialmedia.internal.SizableStateManager;
import com.millennialmedia.internal.adcontrollers.VASTVideoController;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.IOUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import com.millennialmedia.internal.utils.TrackingEvent;
import com.millennialmedia.internal.utils.Utils;
import com.millennialmedia.internal.utils.VideoTrackingEvent;
import com.millennialmedia.internal.utils.ViewUtils;
import com.millennialmedia.internal.video.MMVideoView;
import com.millennialmedia.internal.video.VASTParser;
import com.moat.analytics.mobile.aol.VideoTrackerListener;
import com.mopub.mobileads.resource.DrawableConstants;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@SuppressLint({"ViewConstructor"})
public class VASTVideoView extends RelativeLayout implements VASTVideoController.VideoViewActions, MMVideoView.MMVideoViewListener {
    private static final int ADCHOICES_DEFAULT_DURATION = 3600000;
    private static final int ADCHOICES_DEFAULT_OFFSET = 0;
    private static final int CACHE_EXPIRATION_TIME = 43200000;
    private static final int COMPANION_AD_MIN_HEIGHT = 250;
    private static final int COMPANION_AD_MIN_WIDTH = 300;
    private static final int COMPLETE = 2;
    private static final int DEFAULT_MAX_BITRATE = 800;
    private static final int IDLE = 0;
    private static final String IMAGE_BMP = "image/bmp";
    private static final String IMAGE_GIF = "image/gif";
    private static final String IMAGE_JPEG = "image/jpeg";
    private static final String IMAGE_PNG = "image/png";
    private static final int LTE_MAX_BITRATE = 800;
    private static final int MIN_BITRATE = 400;
    private static final int PLAYBACK = 1;
    private static final String PROGRESSIVE = "progressive";
    public static final int PROGRESS_UPDATES_DISABLED = -1;
    /* access modifiers changed from: private */
    public static final String TAG = "VASTVideoView";
    private static final int TIME_INVALID = -1;
    private static final String VIDEO_MP4 = "video/mp4";
    private static final int WIFI_MAX_BITRATE = 1200;
    private static final List<String> supportImageTypes = new ArrayList();
    private AdChoicesButton adChoicesButton;
    private FrameLayout backgroundFrame;
    /* access modifiers changed from: private */
    public VASTVideoWebView backgroundWebView = null;
    private LinearLayout buttonContainer;
    /* access modifiers changed from: private */
    public volatile boolean canSkip = false;
    private ImageView closeButton;
    /* access modifiers changed from: private */
    public VASTVideoWebView companionAdWebView = null;
    private RelativeLayout controlButtonContainer;
    /* access modifiers changed from: private */
    public TextView countdown;
    /* access modifiers changed from: private */
    public volatile int currentState = 0;
    /* access modifiers changed from: private */
    public FrameLayout endCardContainer;
    private ViewUtils.ViewabilityWatcher endCardViewabilityWatcher;
    private Set<VASTParser.TrackingEvent> firedTrackingEvents;
    private volatile Map<String, VASTParser.Icon> iconMap;
    private ViewUtils.ViewabilityWatcher impressionViewabilityWatcher;
    private VASTParser.InLineAd inLineAd;
    private boolean incentVideoCompleteEarned = false;
    private volatile boolean initialized = false;
    private boolean isVerticalVideo = false;
    private int lastKnownOrientation = 0;
    private int lastQuartileFired = 0;
    /* access modifiers changed from: private */
    public MMVideoView mmVideoView;
    /* access modifiers changed from: private */
    public VASTVideoWebView overlayWebView = null;
    private int previousTimeLeftToSkip = -1;
    private ImageView replayButton;
    /* access modifiers changed from: private */
    public VASTParser.CompanionAd selectedCompanionAd;
    /* access modifiers changed from: private */
    public VASTParser.Creative selectedCreative;
    private VASTParser.MediaFile selectedMediaFile;
    private boolean shouldHandleTrackingEvents = true;
    private ImageView skipButton;
    private int skipOffsetMilliseconds;
    /* access modifiers changed from: private */
    public VASTVideoViewListener vastVideoViewListener;
    /* access modifiers changed from: private */
    public File videoFile;
    private volatile String videoState = null;
    private ViewUtils.ViewabilityWatcher videoViewabilityWatcher;
    private List<VASTParser.WrapperAd> wrapperAds;

    private enum AdChoicesButtonState {
        READY,
        SHOWING,
        SHOWN,
        COMPLETE
    }

    public interface VASTVideoViewListener {
        void close();

        void onAdLeftApplication();

        void onClicked();

        void onFailed();

        void onIncentiveEarned(XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent);

        void onLoaded();
    }

    public void onUnmuted(MMVideoView mMVideoView) {
    }

    static {
        supportImageTypes.add(IMAGE_BMP);
        supportImageTypes.add(IMAGE_GIF);
        supportImageTypes.add(IMAGE_JPEG);
        supportImageTypes.add(IMAGE_PNG);
    }

    private class ImageButton extends ImageView implements View.OnClickListener {
        VASTParser.Button button = null;
        Integer offset = null;

        ImageButton(Context context, VASTParser.Button button2) {
            super(context);
            this.button = button2;
            if (getOffset() > 0) {
                setVisibility(4);
            }
            loadStaticResource();
            setOnClickListener(this);
        }

        /* access modifiers changed from: package-private */
        public int getOffset() {
            if (this.offset == null) {
                this.offset = Integer.valueOf(VASTVideoView.this.vastTimeToMilliseconds(this.button.offset, -1));
            }
            return this.offset.intValue();
        }

        /* access modifiers changed from: package-private */
        public boolean updateVisibility(int i) {
            if (i < getOffset()) {
                return false;
            }
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    ImageButton.this.setVisibility(0);
                }
            });
            return true;
        }

        private void loadStaticResource() {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    final HttpUtils.Response bitmapFromGetRequest = HttpUtils.getBitmapFromGetRequest(ImageButton.this.button.staticResource.uri);
                    if (bitmapFromGetRequest != null && bitmapFromGetRequest.code == 200) {
                        ThreadUtils.postOnUiThread(new Runnable() {
                            public void run() {
                                ImageButton.this.setImageBitmap(bitmapFromGetRequest.bitmap);
                            }
                        });
                    }
                }
            });
        }

        public void onClick(View view) {
            VASTVideoView.this.notifyListenerOnClick();
            VASTParser.ButtonClicks buttonClicks = this.button.buttonClicks;
            if (buttonClicks != null) {
                if (!Utils.isEmpty(buttonClicks.clickThrough)) {
                    VASTVideoView.this.notifyListenerOnAdLeftApplication();
                    Utils.startActivityFromUrl(buttonClicks.clickThrough);
                }
                TrackingEvent.fireUrls(buttonClicks.clickTrackingUrls, "click tracking");
            }
        }
    }

    class AdChoicesButton extends ImageView implements View.OnClickListener {
        private volatile AdChoicesButtonState buttonState = AdChoicesButtonState.READY;
        private int displayDuration;
        private volatile boolean firedTracking = false;
        VASTParser.Icon icon;
        private volatile int lastProgressTime = 0;
        private volatile boolean loadRequested = false;
        private volatile boolean loaded = false;
        int startOffset;
        private volatile int totalTimeDisplayed = 0;

        AdChoicesButton(Context context) {
            super(context);
            setTag("mmVastVideoView_adChoicesButton");
            setScaleType(ImageView.ScaleType.FIT_START);
            setVisibility(8);
        }

        /* access modifiers changed from: package-private */
        public void init() {
            this.icon = VASTVideoView.this.getIconWithProgram("adchoices");
            if (this.icon != null) {
                this.startOffset = VASTVideoView.this.vastTimeToMilliseconds(this.icon.offset, 0);
                this.displayDuration = VASTVideoView.this.vastTimeToMilliseconds(this.icon.duration, VASTVideoView.ADCHOICES_DEFAULT_DURATION);
                setOnClickListener(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void reset() {
            hideIcon();
            this.lastProgressTime = 0;
            this.totalTimeDisplayed = 0;
            this.buttonState = AdChoicesButtonState.READY;
        }

        /* access modifiers changed from: package-private */
        public void updateVisibility(int i, int i2) {
            int i3;
            if (this.icon != null) {
                if (this.buttonState == AdChoicesButtonState.SHOWN && i > this.lastProgressTime && (i3 = i - this.lastProgressTime) <= 1000) {
                    this.totalTimeDisplayed += i3;
                }
                this.lastProgressTime = i;
                if (this.buttonState != AdChoicesButtonState.COMPLETE && (this.totalTimeDisplayed >= this.displayDuration || VASTVideoView.this.currentState == 2)) {
                    hideIcon();
                } else if (this.buttonState == AdChoicesButtonState.READY && i >= this.startOffset) {
                    showIcon();
                }
            }
        }

        private void showIcon() {
            this.buttonState = AdChoicesButtonState.SHOWING;
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    AdChoicesButton.this.setVisibility(0);
                }
            });
            if (!this.loadRequested) {
                this.loadRequested = true;
                loadStaticResource();
            } else if (this.loaded) {
                onIconLoaded();
            }
        }

        /* access modifiers changed from: private */
        public void hideIcon() {
            this.buttonState = AdChoicesButtonState.COMPLETE;
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    AdChoicesButton.this.setVisibility(8);
                }
            });
        }

        /* access modifiers changed from: private */
        public void onIconLoaded() {
            this.loaded = true;
            if (this.buttonState == AdChoicesButtonState.SHOWING) {
                this.buttonState = AdChoicesButtonState.SHOWN;
                fireIconViewTracking();
            }
        }

        private void loadStaticResource() {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    final HttpUtils.Response bitmapFromGetRequest = HttpUtils.getBitmapFromGetRequest(AdChoicesButton.this.icon.staticResource.uri);
                    if (bitmapFromGetRequest != null && bitmapFromGetRequest.code == 200 && bitmapFromGetRequest.bitmap != null) {
                        int dimensionPixelSize = AdChoicesButton.this.getResources().getDimensionPixelSize(R.dimen.mmadsdk_adchoices_icon_height);
                        int height = bitmapFromGetRequest.bitmap.getHeight();
                        if (height <= 0) {
                            String access$400 = VASTVideoView.TAG;
                            MMLog.e(access$400, "Invalid icon height: " + height);
                            return;
                        }
                        final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((bitmapFromGetRequest.bitmap.getWidth() * dimensionPixelSize) / height, dimensionPixelSize);
                        layoutParams.leftMargin = 0;
                        layoutParams.topMargin = 0;
                        layoutParams.rightMargin = ExploreByTouchHelper.INVALID_ID;
                        ThreadUtils.postOnUiThread(new Runnable() {
                            public void run() {
                                AdChoicesButton.this.setImageBitmap(bitmapFromGetRequest.bitmap);
                                AdChoicesButton.this.setLayoutParams(layoutParams);
                                AdChoicesButton.this.onIconLoaded();
                            }
                        });
                    }
                }
            });
        }

        public void onClick(View view) {
            VASTVideoView.this.notifyListenerOnClick();
            if (this.icon.iconClicks != null && !Utils.isEmpty(this.icon.iconClicks.clickThrough)) {
                VASTVideoView.this.notifyListenerOnAdLeftApplication();
                Utils.startActivityFromUrl(this.icon.iconClicks.clickThrough);
            }
            fireIconClickTracking();
        }

        private void fireIconClickTracking() {
            if (this.icon.iconClicks != null) {
                TrackingEvent.fireUrls(this.icon.iconClicks.clickTrackingUrls, "icon click tracker");
            }
        }

        private void fireIconViewTracking() {
            if (!this.firedTracking) {
                this.firedTracking = true;
                TrackingEvent.fireUrls(this.icon.iconViewTrackingUrls, "icon view tracker");
            }
        }
    }

    public void setMoatVideoTrackerListener(VideoTrackerListener videoTrackerListener) {
        if (this.mmVideoView != null) {
            this.mmVideoView.setMoatVideoTrackerListener(videoTrackerListener);
        } else {
            MMLog.w(TAG, "Listener not set. mmVideoView is null.");
        }
    }

    public class VASTVideoWebView extends MMWebView {
        volatile int lastUpdateTime = 0;
        int updateTimeInterval = -1;

        VASTVideoWebView(Context context, boolean z, MMWebView.MMWebViewListener mMWebViewListener) {
            super(context, new MMWebView.MMWebViewOptions(true, z, false, false), mMWebViewListener);
        }

        public void play() {
            if (VASTVideoView.this.currentState != 2) {
                VASTVideoView.this.mmVideoView.start();
            }
        }

        public void pause() {
            if (VASTVideoView.this.currentState != 2) {
                VASTVideoView.this.mmVideoView.pause();
            }
        }

        public void close() {
            VASTVideoView.this.close();
        }

        public void skip() {
            if (VASTVideoView.this.currentState != 2) {
                boolean unused = VASTVideoView.this.canSkip = true;
                ThreadUtils.postOnUiThread(new Runnable() {
                    public void run() {
                        VASTVideoView.this.enableSkipControls();
                        VASTVideoView.this.skip();
                    }
                });
            }
        }

        public void restart() {
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    VASTVideoView.this.replay();
                }
            });
        }

        public void seek(int i) {
            if (VASTVideoView.this.currentState != 2) {
                VASTVideoView.this.mmVideoView.seekTo(i);
            }
        }

        public void triggerTimeUpdate() {
            callJavascript("MmJsBridge.vast.setCurrentTime", Integer.valueOf(VASTVideoView.this.mmVideoView.getCurrentPosition()));
        }

        public void setTimeInterval(int i) {
            this.updateTimeInterval = i;
        }

        /* access modifiers changed from: package-private */
        public void updateTime(int i) {
            if (this.updateTimeInterval == -1) {
                return;
            }
            if (this.lastUpdateTime == 0 || this.lastUpdateTime + this.updateTimeInterval <= i) {
                this.lastUpdateTime = i;
                callJavascript("MmJsBridge.vast.setCurrentTime", Integer.valueOf(i));
            }
        }
    }

    static class VASTImpressionViewabilityListener implements ViewUtils.ViewabilityListener {
        WeakReference<VASTVideoView> vastVideoViewRef;

        VASTImpressionViewabilityListener(VASTVideoView vASTVideoView) {
            this.vastVideoViewRef = new WeakReference<>(vASTVideoView);
        }

        public void onViewableChanged(boolean z) {
            VASTVideoView vASTVideoView = this.vastVideoViewRef.get();
            if (vASTVideoView != null && z) {
                vASTVideoView.fireImpressions();
            }
        }
    }

    static class VASTVideoViewabilityListener implements ViewUtils.ViewabilityListener {
        boolean didPause = false;
        WeakReference<MMVideoView> mmVideoViewRef;
        WeakReference<VASTVideoView> vastVideoViewRef;

        VASTVideoViewabilityListener(VASTVideoView vASTVideoView, MMVideoView mMVideoView) {
            this.vastVideoViewRef = new WeakReference<>(vASTVideoView);
            this.mmVideoViewRef = new WeakReference<>(mMVideoView);
        }

        public void onViewableChanged(boolean z) {
            MMVideoView mMVideoView = this.mmVideoViewRef.get();
            VASTVideoView vASTVideoView = this.vastVideoViewRef.get();
            if (vASTVideoView != null && mMVideoView != null) {
                if (z) {
                    vASTVideoView.fireVideoTrackingEvents(vASTVideoView.getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.creativeView), 0);
                    if (vASTVideoView.selectedCreative != null) {
                        vASTVideoView.fireVideoTrackingEvents(vASTVideoView.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.creativeView), 0);
                    }
                }
                if (!z && mMVideoView.isPlaying()) {
                    this.didPause = true;
                    mMVideoView.pause();
                } else if (this.didPause) {
                    mMVideoView.start();
                    this.didPause = false;
                }
            }
        }
    }

    static class VASTEndCardViewabilityListener implements ViewUtils.ViewabilityListener {
        WeakReference<VASTVideoView> vastVideoViewRef;

        VASTEndCardViewabilityListener(VASTVideoView vASTVideoView) {
            this.vastVideoViewRef = new WeakReference<>(vASTVideoView);
        }

        public void onViewableChanged(boolean z) {
            VASTVideoView vASTVideoView = this.vastVideoViewRef.get();
            if (vASTVideoView != null && z && vASTVideoView.selectedCompanionAd.trackingEvents != null && !vASTVideoView.selectedCompanionAd.trackingEvents.isEmpty()) {
                vASTVideoView.fireVideoTrackingEvents(vASTVideoView.selectedCompanionAd.trackingEvents.get(VASTParser.TrackableEvent.creativeView), 0);
            }
        }
    }

    public VASTVideoView(Context context, VASTParser.InLineAd inLineAd2, List<VASTParser.WrapperAd> list, VASTVideoViewListener vASTVideoViewListener) {
        super(context);
        boolean z = false;
        this.inLineAd = inLineAd2;
        this.wrapperAds = list;
        setBackgroundColor(-16777216);
        setId(R.id.mmadsdk_vast_video_view);
        if (isPortrait()) {
            this.lastKnownOrientation = 1;
        } else {
            this.lastKnownOrientation = 2;
        }
        this.firedTrackingEvents = Collections.synchronizedSet(new HashSet());
        this.vastVideoViewListener = vASTVideoViewListener;
        this.impressionViewabilityWatcher = new ViewUtils.ViewabilityWatcher(this, new VASTImpressionViewabilityListener(this));
        FrameLayout frameLayout = new FrameLayout(getContext());
        addView(frameLayout, new RelativeLayout.LayoutParams(-1, -1));
        this.backgroundFrame = new FrameLayout(context);
        this.backgroundFrame.setTag("mmVastVideoView_backgroundFrame");
        this.backgroundFrame.setVisibility(8);
        frameLayout.addView(this.backgroundFrame, new FrameLayout.LayoutParams(-1, -1));
        this.mmVideoView = new MMVideoView(context, true, false, Handshake.isMoatEnabled() ? getMoatIdentifiers() : null, this);
        this.mmVideoView.setTag("mmVastVideoView_videoView");
        this.videoViewabilityWatcher = new ViewUtils.ViewabilityWatcher(this.mmVideoView, new VASTVideoViewabilityListener(this, this.mmVideoView));
        selectMediaFileAndCreative();
        this.isVerticalVideo = isMediaFileVerticalVideo(this.selectedMediaFile);
        if (this.isVerticalVideo) {
            this.inLineAd.mmExtension = null;
        }
        addView(this.mmVideoView, getLayoutParamsForOrientation());
        this.adChoicesButton = new AdChoicesButton(context);
        addView(this.adChoicesButton);
        this.endCardContainer = new FrameLayout(context);
        this.endCardContainer.setTag("mmVastVideoView_endCardContainer");
        this.endCardContainer.setVisibility(8);
        this.endCardViewabilityWatcher = new ViewUtils.ViewabilityWatcher(this.endCardContainer, new VASTEndCardViewabilityListener(this));
        this.impressionViewabilityWatcher.startWatching();
        this.videoViewabilityWatcher.startWatching();
        this.endCardViewabilityWatcher.startWatching();
        frameLayout.addView(this.endCardContainer, new FrameLayout.LayoutParams(-1, -1));
        this.controlButtonContainer = new RelativeLayout(context);
        this.controlButtonContainer.setId(R.id.mmadsdk_vast_video_control_buttons);
        this.closeButton = new ImageView(context);
        this.closeButton.setImageDrawable(getResources().getDrawable(R.drawable.mmadsdk_vast_close));
        this.closeButton.setVisibility(8);
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VASTVideoView.this.close();
            }
        });
        this.closeButton.setTag("mmVastVideoView_closeButton");
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.mmadsdk_control_button_width), getResources().getDimensionPixelSize(R.dimen.mmadsdk_control_button_height));
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        this.controlButtonContainer.addView(this.closeButton, layoutParams);
        this.skipButton = new ImageView(context);
        this.skipButton.setImageDrawable(getResources().getDrawable(R.drawable.mmadsdk_vast_skip));
        this.skipButton.setTag("mmVastVideoView_skipButton");
        this.skipButton.setEnabled(false);
        this.countdown = new TextView(context);
        this.countdown.setBackground(getResources().getDrawable(R.drawable.mmadsdk_vast_opacity));
        this.countdown.setTextColor(getResources().getColor(17170443));
        this.countdown.setTypeface(null, 1);
        this.countdown.setGravity(17);
        this.countdown.setVisibility(4);
        this.countdown.setTag("mmVastVideoView_countdown");
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.mmadsdk_control_button_width), getResources().getDimensionPixelSize(R.dimen.mmadsdk_control_button_height));
        layoutParams2.addRule(10);
        layoutParams2.addRule(11);
        this.controlButtonContainer.addView(this.skipButton, layoutParams2);
        this.controlButtonContainer.addView(this.countdown, layoutParams2);
        this.replayButton = new ImageView(context);
        this.replayButton.setImageDrawable(getResources().getDrawable(R.drawable.mmadsdk_vast_replay));
        this.replayButton.setVisibility(8);
        this.replayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VASTVideoView.this.notifyListenerOnClick();
                VASTVideoView.this.replay();
            }
        });
        this.replayButton.setTag("mmVastVideoView_replayButton");
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.mmadsdk_control_button_width), getResources().getDimensionPixelSize(R.dimen.mmadsdk_control_button_height));
        layoutParams3.addRule(10);
        layoutParams3.addRule(9);
        this.controlButtonContainer.addView(this.replayButton, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.addRule(10);
        addView(this.controlButtonContainer, layoutParams4);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams5.addRule(12);
        this.buttonContainer = new LinearLayout(getContext());
        addView(this.buttonContainer, layoutParams5);
        loadInlineAd(context);
        this.shouldHandleTrackingEvents = (hasTrackingEvents(this.selectedCreative) || haveTrackingEvents(this.wrapperAds)) ? true : z;
        this.currentState = 1;
        updateComponentVisibility();
    }

    /* access modifiers changed from: private */
    public void close() {
        if (this.selectedCreative != null) {
            fireVideoTrackingEvents(getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.closeLinear), 0);
            fireVideoTrackingEvents(this.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.closeLinear), 0);
        }
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                if (VASTVideoView.this.vastVideoViewListener != null) {
                    VASTVideoView.this.vastVideoViewListener.close();
                }
            }
        });
    }

    private void updateButtonContainerVisibility() {
        if (this.currentState == 1) {
            if (isPortrait()) {
                if (this.inLineAd == null || this.inLineAd.mmExtension == null || this.inLineAd.mmExtension.background == null || !this.inLineAd.mmExtension.background.hideButtons) {
                    this.buttonContainer.setVisibility(0);
                } else {
                    this.buttonContainer.setVisibility(4);
                }
            } else if (this.inLineAd == null || this.inLineAd.mmExtension == null || this.inLineAd.mmExtension.overlay == null || !this.inLineAd.mmExtension.overlay.hideButtons) {
                this.buttonContainer.setVisibility(0);
            } else {
                this.buttonContainer.setVisibility(4);
            }
        } else if (this.currentState != 2) {
        } else {
            if (this.selectedCompanionAd == null || !this.selectedCompanionAd.hideButtons) {
                this.buttonContainer.setVisibility(0);
            } else {
                this.buttonContainer.setVisibility(4);
            }
        }
    }

    public void updateComponentVisibility() {
        if (this.currentState == 1) {
            this.backgroundFrame.setVisibility(isPortrait() ? 0 : 8);
            this.endCardContainer.setVisibility(8);
            if (this.overlayWebView != null) {
                if (isPortrait()) {
                    ViewUtils.removeFromParent(this.overlayWebView);
                } else if (this.overlayWebView.getParent() == null) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                    if (this.mmVideoView != null) {
                        this.mmVideoView.addView(this.overlayWebView, layoutParams);
                    }
                }
            }
            if (this.mmVideoView != null) {
                this.mmVideoView.setVisibility(0);
            }
        } else if (this.currentState == 2) {
            if (this.mmVideoView != null) {
                this.mmVideoView.setVisibility(8);
            }
            this.backgroundFrame.setVisibility(8);
            this.endCardContainer.setVisibility(0);
            if (this.overlayWebView != null) {
                ViewUtils.removeFromParent(this.overlayWebView);
            }
        }
        updateButtonContainerVisibility();
    }

    public int getCurrentPosition() {
        if (this.mmVideoView == null) {
            return -1;
        }
        return this.mmVideoView.getCurrentPosition();
    }

    public int getDuration() {
        if (this.mmVideoView == null) {
            return -1;
        }
        return this.mmVideoView.getDuration();
    }

    public void updateLayout() {
        boolean z = true;
        if ((!isPortrait() || this.lastKnownOrientation == 1) && (isPortrait() || this.lastKnownOrientation != 1)) {
            z = false;
        } else {
            this.mmVideoView.setLayoutParams(getLayoutParamsForOrientation());
            updateComponentVisibility();
        }
        if (z) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.mmadsdk_ad_button_width), getResources().getDimensionPixelSize(R.dimen.mmadsdk_ad_button_height), isPortrait() ? 1.0f : 0.0f);
            if (!isPortrait()) {
                layoutParams.leftMargin = getResources().getDimensionPixelSize(R.dimen.mmadsdk_ad_button_padding_left);
            } else {
                layoutParams.leftMargin = 0;
            }
            for (int i = 0; i < this.buttonContainer.getChildCount(); i++) {
                this.buttonContainer.getChildAt(i).setLayoutParams(layoutParams);
            }
        }
        this.buttonContainer.bringToFront();
        this.lastKnownOrientation = getResources().getConfiguration().orientation;
    }

    private ViewGroup.LayoutParams getLayoutParamsForOrientation() {
        if (!isPortrait() || this.isVerticalVideo) {
            return new RelativeLayout.LayoutParams(-1, -1);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(3, R.id.mmadsdk_vast_video_control_buttons);
        return layoutParams;
    }

    private void selectMediaFileAndCreative() {
        VASTParser.MediaFile selectMediaFile;
        if (this.inLineAd.creatives != null) {
            for (VASTParser.Creative creative : this.inLineAd.creatives) {
                if (creative.linearAd != null && (selectMediaFile = selectMediaFile(creative.linearAd.mediaFiles)) != null) {
                    this.selectedMediaFile = selectMediaFile;
                    this.selectedCreative = creative;
                    return;
                }
            }
        }
    }

    private void loadInlineAd(Context context) {
        if (this.selectedMediaFile != null) {
            File externalFilesDir = context.getExternalFilesDir(null);
            if (externalFilesDir == null) {
                MMLog.e(TAG, "Cannot access video cache directory. External storage is not available.");
                if (this.vastVideoViewListener != null) {
                    this.vastVideoViewListener.onFailed();
                    return;
                }
                return;
            }
            File file = new File(externalFilesDir.getAbsolutePath() + File.separator + "_mm_video_cache");
            file.mkdirs();
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    if (file2.isFile()) {
                        if (System.currentTimeMillis() - file2.lastModified() > 43200000) {
                            file2.delete();
                        }
                    }
                }
            }
            IOUtils.downloadFile(this.selectedMediaFile.url.trim(), null, file, new IOUtils.DownloadListener() {
                public void onDownloadSucceeded(final File file) {
                    ThreadUtils.postOnUiThread(new Runnable() {
                        public void run() {
                            if (VASTVideoView.this.mmVideoView == null) {
                                if (MMLog.isDebugEnabled()) {
                                    MMLog.d(VASTVideoView.TAG, "Unable to load the video asset. MMWebView instance is null.");
                                }
                                if (VASTVideoView.this.vastVideoViewListener != null) {
                                    VASTVideoView.this.vastVideoViewListener.onFailed();
                                    return;
                                }
                                return;
                            }
                            File unused = VASTVideoView.this.videoFile = file;
                            VASTVideoView.this.mmVideoView.setVideoURI(Uri.parse(file.getAbsolutePath()));
                            VASTVideoView.this.registerVideoClicks();
                        }
                    });
                }

                public void onDownloadFailed(Throwable th) {
                    MMLog.e(VASTVideoView.TAG, "Error occurred downloading the video file.", th);
                    if (VASTVideoView.this.vastVideoViewListener != null) {
                        VASTVideoView.this.vastVideoViewListener.onFailed();
                    }
                }
            });
            loadButtons();
            loadBackground();
            loadOverlay();
            loadCompanionAd();
            loadNonclickableTopRegion();
            this.adChoicesButton.init();
            return;
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "VAST init failed because it did not contain a compatible media file.");
        }
        if (this.vastVideoViewListener != null) {
            this.vastVideoViewListener.onFailed();
        }
    }

    private void loadNonclickableTopRegion() {
        FrameLayout frameLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, ViewUtils.convertPixelsToDips((int) DrawableConstants.CtaButton.WIDTH_DIPS));
        ignoreClicksOnView(frameLayout);
        if (this.mmVideoView != null) {
            this.mmVideoView.addView(frameLayout, layoutParams);
        }
    }

    private VASTParser.MediaFile selectMediaFile(List<VASTParser.MediaFile> list) {
        VASTParser.MediaFile mediaFile = null;
        if (list == null || list.isEmpty()) {
            return null;
        }
        String networkConnectionType = EnvironmentUtils.getNetworkConnectionType();
        int i = 800;
        if (TapjoyConstants.TJC_CONNECTION_TYPE_WIFI.equalsIgnoreCase(networkConnectionType)) {
            i = 1200;
        } else {
            boolean equalsIgnoreCase = "lte".equalsIgnoreCase(networkConnectionType);
        }
        if (MMLog.isDebugEnabled()) {
            MMLog.d("TAG", "Using bit rate range " + ((int) MIN_BITRATE) + " to " + i + " inclusive for network connectivity type = " + networkConnectionType);
        }
        for (VASTParser.MediaFile next : list) {
            if (!Utils.isEmpty(next.url)) {
                boolean equalsIgnoreCase2 = PROGRESSIVE.equalsIgnoreCase(next.delivery);
                boolean equalsIgnoreCase3 = VIDEO_MP4.equalsIgnoreCase(next.contentType);
                boolean z = false;
                boolean z2 = next.bitrate >= MIN_BITRATE && next.bitrate <= i;
                if (mediaFile == null || mediaFile.bitrate < next.bitrate) {
                    z = true;
                }
                if (equalsIgnoreCase2 && equalsIgnoreCase3 && z2 && z) {
                    mediaFile = next;
                }
            }
        }
        return mediaFile;
    }

    private void loadOverlay() {
        if (this.inLineAd.mmExtension != null && this.inLineAd.mmExtension.overlay != null && !Utils.isEmpty(this.inLineAd.mmExtension.overlay.uri)) {
            this.overlayWebView = new VASTVideoWebView(getContext(), true, new MMWebView.MMWebViewListener() {
                public void close() {
                }

                public boolean expand(SizableStateManager.ExpandParams expandParams) {
                    return false;
                }

                public void onFailed() {
                }

                public void onLoaded() {
                }

                public void onUnload() {
                }

                public boolean resize(SizableStateManager.ResizeParams resizeParams) {
                    return false;
                }

                public void setOrientation(int i) {
                }

                public void onReady() {
                    VASTVideoView.this.onWebViewReady(VASTVideoView.this.overlayWebView);
                }

                public void onClicked() {
                    VASTVideoView.this.notifyListenerOnClick();
                }

                public void onAdLeftApplication() {
                    VASTVideoView.this.notifyListenerOnAdLeftApplication();
                }
            });
            this.overlayWebView.setTag("mmVastVideoView_overlayWebView");
            loadContentIntoWebView(this.overlayWebView, this.inLineAd.mmExtension.overlay.uri);
        }
    }

    private void loadContentIntoWebView(final VASTVideoWebView vASTVideoWebView, final String str) {
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                final HttpUtils.Response contentFromGetRequest = HttpUtils.getContentFromGetRequest(str);
                if (contentFromGetRequest.code == 200 && !Utils.isEmpty(contentFromGetRequest.content)) {
                    ThreadUtils.postOnUiThread(new Runnable() {
                        public void run() {
                            vASTVideoWebView.setContent(contentFromGetRequest.content);
                        }
                    });
                }
            }
        });
    }

    private void loadCompanionAd() {
        VASTParser.CompanionAd next;
        if (this.inLineAd.creatives != null) {
            for (VASTParser.Creative creative : this.inLineAd.creatives) {
                if (creative.companionAds != null && !creative.companionAds.isEmpty()) {
                    Iterator<VASTParser.CompanionAd> it = creative.companionAds.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        next = it.next();
                        if (!(next == null || next.width == null || next.width.intValue() < COMPANION_AD_MIN_WIDTH || next.height == null || next.height.intValue() < COMPANION_AD_MIN_HEIGHT)) {
                            if ((next.staticResource != null && !Utils.isEmpty(next.staticResource.uri) && supportImageTypes.contains(next.staticResource.creativeType)) || ((next.htmlResource != null && !Utils.isEmpty(next.htmlResource.uri)) || (next.iframeResource != null && !Utils.isEmpty(next.iframeResource.uri)))) {
                                this.selectedCompanionAd = next;
                            }
                        }
                    }
                    this.selectedCompanionAd = next;
                }
                if (this.selectedCompanionAd != null && creative != this.selectedCreative) {
                    break;
                }
            }
        }
        if (this.selectedCompanionAd == null) {
            return;
        }
        if (this.selectedCompanionAd.iframeResource != null && !Utils.isEmpty(this.selectedCompanionAd.iframeResource.uri)) {
            createCompanionWebView(this.selectedCompanionAd.iframeResource.uri);
            this.endCardContainer.addView(this.companionAdWebView, new FrameLayout.LayoutParams(-1, -1));
            this.endCardContainer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    VASTVideoView.this.fireCompanionAdClickTracking();
                }
            });
        } else if (this.selectedCompanionAd.htmlResource != null && !Utils.isEmpty(this.selectedCompanionAd.htmlResource.uri)) {
            createCompanionWebView(this.selectedCompanionAd.htmlResource.uri);
            this.endCardContainer.addView(this.companionAdWebView, new FrameLayout.LayoutParams(-1, -1));
            this.endCardContainer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    VASTVideoView.this.fireCompanionAdClickTracking();
                }
            });
        } else if (this.selectedCompanionAd.staticResource != null && !Utils.isEmpty(this.selectedCompanionAd.staticResource.uri)) {
            ThreadUtils.runOnWorkerThread(new Runnable() {
                public void run() {
                    final HttpUtils.Response bitmapFromGetRequest = HttpUtils.getBitmapFromGetRequest(VASTVideoView.this.selectedCompanionAd.staticResource.uri);
                    if (bitmapFromGetRequest != null && bitmapFromGetRequest.code == 200) {
                        ThreadUtils.postOnUiThread(new Runnable() {
                            public void run() {
                                ImageView imageView = new ImageView(VASTVideoView.this.getContext());
                                imageView.setImageBitmap(bitmapFromGetRequest.bitmap);
                                imageView.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View view) {
                                        VASTVideoView.this.notifyListenerOnClick();
                                        if (!Utils.isEmpty(VASTVideoView.this.selectedCompanionAd.companionClickThrough)) {
                                            Utils.startActivityFromUrl(VASTVideoView.this.selectedCompanionAd.companionClickThrough);
                                        }
                                        VASTVideoView.this.fireCompanionAdClickTracking();
                                    }
                                });
                                imageView.setTag("mmVastVideoView_companionImageView");
                                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
                                VASTVideoView.this.endCardContainer.setBackgroundColor(VASTVideoView.this.getBackgroundColor(VASTVideoView.this.selectedCompanionAd.staticResource));
                                VASTVideoView.this.endCardContainer.addView(imageView, layoutParams);
                            }
                        });
                    }
                }
            });
        }
    }

    private void createCompanionWebView(String str) {
        this.companionAdWebView = new VASTVideoWebView(getContext(), false, new MMWebView.MMWebViewListener() {
            public void close() {
            }

            public boolean expand(SizableStateManager.ExpandParams expandParams) {
                return false;
            }

            public void onFailed() {
            }

            public void onLoaded() {
            }

            public void onUnload() {
            }

            public boolean resize(SizableStateManager.ResizeParams resizeParams) {
                return false;
            }

            public void setOrientation(int i) {
            }

            public void onReady() {
                VASTVideoView.this.onWebViewReady(VASTVideoView.this.companionAdWebView);
            }

            public void onClicked() {
                VASTVideoView.this.notifyListenerOnClick();
            }

            public void onAdLeftApplication() {
                VASTVideoView.this.notifyListenerOnAdLeftApplication();
            }
        });
        this.companionAdWebView.setTag("mmVastVideoView_companionWebView");
        loadContentIntoWebView(this.companionAdWebView, str);
    }

    private void loadBackground() {
        if (this.inLineAd.mmExtension != null && this.inLineAd.mmExtension.background != null) {
            final VASTParser.Background background = this.inLineAd.mmExtension.background;
            if (background.staticResource != null && !Utils.isEmpty(background.staticResource.uri)) {
                final ImageView imageView = new ImageView(getContext());
                imageView.setTag("mmVastVideoView_backgroundImageView");
                this.backgroundFrame.addView(imageView);
                this.backgroundFrame.setBackgroundColor(getBackgroundColor(background.staticResource));
                ThreadUtils.runOnWorkerThread(new Runnable() {
                    public void run() {
                        final HttpUtils.Response bitmapFromGetRequest = HttpUtils.getBitmapFromGetRequest(background.staticResource.uri);
                        if (bitmapFromGetRequest.code == 200) {
                            ThreadUtils.postOnUiThread(new Runnable() {
                                public void run() {
                                    imageView.setImageBitmap(bitmapFromGetRequest.bitmap);
                                }
                            });
                        }
                    }
                });
            } else if (background.webResource != null && !Utils.isEmpty(background.webResource.uri)) {
                this.backgroundWebView = new VASTVideoWebView(getContext(), false, new MMWebView.MMWebViewListener() {
                    public void close() {
                    }

                    public boolean expand(SizableStateManager.ExpandParams expandParams) {
                        return false;
                    }

                    public void onFailed() {
                    }

                    public void onLoaded() {
                    }

                    public void onUnload() {
                    }

                    public boolean resize(SizableStateManager.ResizeParams resizeParams) {
                        return false;
                    }

                    public void setOrientation(int i) {
                    }

                    public void onReady() {
                        VASTVideoView.this.onWebViewReady(VASTVideoView.this.backgroundWebView);
                    }

                    public void onClicked() {
                        VASTVideoView.this.notifyListenerOnClick();
                    }

                    public void onAdLeftApplication() {
                        VASTVideoView.this.notifyListenerOnAdLeftApplication();
                    }
                });
                this.backgroundWebView.setTag("mmVastVideoView_backgroundWebView");
                this.backgroundFrame.addView(this.backgroundWebView);
                loadContentIntoWebView(this.backgroundWebView, background.webResource.uri);
            }
        }
    }

    private void loadButtons() {
        if (this.inLineAd.mmExtension != null && this.inLineAd.mmExtension.buttons != null) {
            Collections.sort(this.inLineAd.mmExtension.buttons, new Comparator<VASTParser.Button>() {
                public int compare(VASTParser.Button button, VASTParser.Button button2) {
                    return button.position - button2.position;
                }
            });
            int i = 0;
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.mmadsdk_ad_button_width);
            int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.mmadsdk_ad_button_height);
            for (VASTParser.Button next : this.inLineAd.mmExtension.buttons) {
                if (i >= 3) {
                    return;
                }
                if (next.staticResource != null && !Utils.isEmpty(next.staticResource.uri) && !Utils.isEmpty(next.staticResource.creativeType) && next.staticResource.creativeType.trim().equalsIgnoreCase(IMAGE_PNG)) {
                    i++;
                    ImageButton imageButton = new ImageButton(getContext(), next);
                    imageButton.setTag("mmVastVideoView_mmExtensionButton_" + i);
                    FrameLayout frameLayout = new FrameLayout(getContext());
                    frameLayout.addView(imageButton, new FrameLayout.LayoutParams(-1, -1));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize2, isPortrait() ? 1.0f : 0.0f);
                    if (!isPortrait()) {
                        layoutParams.leftMargin = getResources().getDimensionPixelSize(R.dimen.mmadsdk_ad_button_padding_left);
                    }
                    this.buttonContainer.addView(frameLayout, layoutParams);
                }
            }
        }
    }

    static boolean isValidAdChoicesIcon(VASTParser.Icon icon) {
        if (icon != null && icon.program != null && icon.program.equalsIgnoreCase("adchoices") && icon.iconClicks != null && !Utils.isEmpty(icon.iconClicks.clickThrough) && icon.staticResource != null && !Utils.isEmpty(icon.staticResource.uri)) {
            return true;
        }
        if (!MMLog.isDebugEnabled()) {
            return false;
        }
        String str = TAG;
        MMLog.d(str, "Invalid adchoices icon: " + icon);
        return false;
    }

    private Map<String, VASTParser.Icon> getIconsClosestToCreative() {
        HashMap hashMap = new HashMap();
        if (this.wrapperAds != null) {
            for (VASTParser.WrapperAd next : this.wrapperAds) {
                if (next.creatives != null) {
                    for (VASTParser.Creative creative : next.creatives) {
                        if (!(creative.linearAd == null || creative.linearAd.icons == null)) {
                            for (VASTParser.Icon next2 : creative.linearAd.icons) {
                                if (isValidAdChoicesIcon(next2)) {
                                    hashMap.put(next2.program.toLowerCase(Locale.ROOT), next2);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!(this.selectedCreative == null || this.selectedCreative.linearAd.icons == null)) {
            for (VASTParser.Icon next3 : this.selectedCreative.linearAd.icons) {
                if (isValidAdChoicesIcon(next3)) {
                    hashMap.put(next3.program.toLowerCase(Locale.ROOT), next3);
                }
            }
        }
        return hashMap;
    }

    /* access modifiers changed from: private */
    public void registerVideoClicks() {
        final VASTParser.VideoClicks videoClicks = this.selectedCreative.linearAd.videoClicks;
        final List<VASTParser.VideoClicks> wrapperVideoClicks = getWrapperVideoClicks();
        if (hasVideoClicks(videoClicks) || haveVideoClicks(wrapperVideoClicks)) {
            this.mmVideoView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    VASTVideoView.this.notifyListenerOnClick();
                    if (videoClicks == null || Utils.isEmpty(videoClicks.clickThrough)) {
                        VASTVideoView.this.fireClickTracking(videoClicks, true);
                        VASTVideoView.this.fireWrappersClickTracking(wrapperVideoClicks, true);
                        return;
                    }
                    Utils.startActivityFromUrl(videoClicks.clickThrough);
                    VASTVideoView.this.notifyListenerOnAdLeftApplication();
                    VASTVideoView.this.fireClickTracking(videoClicks, false);
                    VASTVideoView.this.fireWrappersClickTracking(wrapperVideoClicks, false);
                }
            });
        }
    }

    public void onPrepared(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onPrepared");
        }
        this.skipOffsetMilliseconds = Math.max(0, vastTimeToMilliseconds(this.selectedCreative.linearAd.skipOffset, -1));
        if (!this.initialized) {
            this.initialized = true;
            if (this.vastVideoViewListener != null) {
                this.vastVideoViewListener.onLoaded();
            }
        }
        if (this.overlayWebView != null && this.overlayWebView.isJSBridgeReady()) {
            this.overlayWebView.callJavascript("MmJsBridge.vast.setDuration", Integer.valueOf(this.mmVideoView.getDuration()));
        }
        if (this.backgroundWebView != null && this.backgroundWebView.isJSBridgeReady()) {
            this.backgroundWebView.callJavascript("MmJsBridge.vast.setDuration", Integer.valueOf(this.mmVideoView.getDuration()));
        }
    }

    public void onReadyToStart(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onReadyToStart");
        }
    }

    private void setVideoState(String str) {
        this.videoState = str;
        if (this.overlayWebView != null && this.overlayWebView.isJSBridgeReady()) {
            this.overlayWebView.callJavascript("MmJsBridge.vast.setState", this.videoState);
        }
        if (this.backgroundWebView != null && this.backgroundWebView.isJSBridgeReady()) {
            this.backgroundWebView.callJavascript("MmJsBridge.vast.setState", this.videoState);
        }
    }

    /* access modifiers changed from: private */
    public void onWebViewReady(MMWebView mMWebView) {
        mMWebView.callJavascript("MmJsBridge.vast.enableWebOverlay", new Object[0]);
        mMWebView.callJavascript("MmJsBridge.vast.setDuration", Integer.valueOf(this.mmVideoView.getDuration()));
        if (this.videoState != null) {
            mMWebView.callJavascript("MmJsBridge.vast.setState", this.videoState);
        }
    }

    public synchronized void onStart(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onStart");
        }
        setKeepScreenOnUIThread(true);
        setVideoState(TJAdUnitConstants.String.VIDEO_PLAYING);
        if (this.selectedCreative != null) {
            fireVideoTrackingEvents(getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.start), 0);
            fireVideoTrackingEvents(this.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.start), 0);
        }
    }

    public void onStop(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onStop");
        }
        setKeepScreenOnUIThread(false);
    }

    public void onPause(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onPause");
        }
        setVideoState(TJAdUnitConstants.String.VIDEO_PAUSED);
        setKeepScreenOnUIThread(false);
    }

    public void onComplete(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onComplete");
        }
        if (this.selectedCreative != null) {
            fireVideoTrackingEvents(getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.complete), getDuration());
            fireVideoTrackingEvents(this.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.complete), getDuration());
        }
        setVideoState(TJAdUnitConstants.String.VIDEO_COMPLETE);
        if (!this.incentVideoCompleteEarned) {
            this.incentVideoCompleteEarned = true;
            if (this.vastVideoViewListener != null) {
                this.vastVideoViewListener.onIncentiveEarned(new XIncentivizedEventListener.XIncentiveEvent(XIncentivizedEventListener.XIncentiveEvent.INCENTIVE_VIDEO_COMPLETE, null));
            }
        }
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                VASTVideoView.this.doComplete();
                VASTVideoView.this.setKeepScreenOn(false);
            }
        });
    }

    public synchronized void onProgress(MMVideoView mMVideoView, int i) {
        if (this.overlayWebView != null) {
            this.overlayWebView.updateTime(i);
        }
        if (this.backgroundWebView != null) {
            this.backgroundWebView.updateTime(i);
        }
        if (this.buttonContainer != null) {
            updateButtonsVisibility(i);
        }
        if (!this.canSkip) {
            updateSkipButtonVisibility(i, mMVideoView.getDuration());
        }
        if (this.adChoicesButton != null) {
            this.adChoicesButton.updateVisibility(i, mMVideoView.getDuration());
        }
        if (this.selectedCreative != null && this.shouldHandleTrackingEvents) {
            processQuartileTrackingEvents(i, mMVideoView.getDuration());
            processProgressTrackingEvents(i);
        }
    }

    private void updateButtonsVisibility(int i) {
        for (int i2 = 0; i2 < this.buttonContainer.getChildCount(); i2++) {
            View childAt = this.buttonContainer.getChildAt(i2);
            if (childAt instanceof FrameLayout) {
                View childAt2 = ((FrameLayout) childAt).getChildAt(0);
                if (childAt2.getVisibility() != 0 && (childAt2 instanceof ImageButton)) {
                    ((ImageButton) childAt2).updateVisibility(i);
                }
            }
        }
    }

    private void updateSkipButtonVisibility(int i, int i2) {
        final int i3;
        int vASTVideoSkipOffsetMax = Handshake.getVASTVideoSkipOffsetMax();
        int vASTVideoSkipOffsetMin = Handshake.getVASTVideoSkipOffsetMin();
        if (vASTVideoSkipOffsetMin > vASTVideoSkipOffsetMax) {
            vASTVideoSkipOffsetMin = vASTVideoSkipOffsetMax;
        }
        int min = Math.min(Math.max(Math.min(vASTVideoSkipOffsetMax, this.skipOffsetMilliseconds), vASTVideoSkipOffsetMin), i2);
        if (i > min) {
            i3 = 0;
        } else {
            i3 = Double.valueOf(Math.ceil(((double) (min - i)) / 1000.0d)).intValue();
        }
        if (i3 <= 0) {
            this.canSkip = true;
            ThreadUtils.postOnUiThread(new Runnable() {
                public void run() {
                    VASTVideoView.this.enableSkipControls();
                }
            });
        } else if (i3 != this.previousTimeLeftToSkip) {
            this.previousTimeLeftToSkip = i3;
            ThreadUtils.postOnUiThread(new Runnable() {
                @SuppressLint({"SetTextI18n"})
                public void run() {
                    VASTVideoView.this.ignoreClicksOnView(VASTVideoView.this.countdown);
                    VASTVideoView.this.countdown.setVisibility(0);
                    TextView access$3000 = VASTVideoView.this.countdown;
                    access$3000.setText("" + i3);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void ignoreClicksOnView(View view) {
        if (view != null) {
            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (MMLog.isDebugEnabled()) {
                        MMLog.d(VASTVideoView.TAG, "Clicked on an unclickable region.");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public VASTParser.Icon getIconWithProgram(String str) {
        if (this.iconMap == null) {
            this.iconMap = getIconsClosestToCreative();
        }
        return this.iconMap.get(str);
    }

    private void setKeepScreenOnUIThread(final boolean z) {
        ThreadUtils.postOnUiThread(new Runnable() {
            public void run() {
                VASTVideoView.this.setKeepScreenOn(z);
            }
        });
    }

    private void processQuartileTrackingEvents(int i, int i2) {
        int i3 = i2 / 4;
        if (i >= i3 && this.lastQuartileFired < 1) {
            this.lastQuartileFired = 1;
            fireVideoTrackingEvents(getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.firstQuartile), i);
            fireVideoTrackingEvents(this.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.firstQuartile), i);
        }
        if (i >= i3 * 2 && this.lastQuartileFired < 2) {
            this.lastQuartileFired = 2;
            fireVideoTrackingEvents(getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.midpoint), i);
            fireVideoTrackingEvents(this.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.midpoint), i);
        }
        if (i >= i3 * 3 && this.lastQuartileFired < 3) {
            this.lastQuartileFired = 3;
            fireVideoTrackingEvents(getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.thirdQuartile), i);
            fireVideoTrackingEvents(this.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.thirdQuartile), i);
        }
    }

    private void processProgressTrackingEvents(int i) {
        ArrayList<VASTParser.TrackingEvent> arrayList = new ArrayList<>();
        List list = this.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.progress);
        if (list != null) {
            arrayList.addAll(list);
        }
        List<VASTParser.TrackingEvent> wrapperLinearTrackingEvents = getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.progress);
        if (wrapperLinearTrackingEvents != null) {
            arrayList.addAll(wrapperLinearTrackingEvents);
        }
        for (VASTParser.TrackingEvent trackingEvent : arrayList) {
            VASTParser.ProgressEvent progressEvent = (VASTParser.ProgressEvent) trackingEvent;
            int vastTimeToMilliseconds = vastTimeToMilliseconds(progressEvent.offset, -1);
            if (vastTimeToMilliseconds == -1) {
                if (MMLog.isDebugEnabled()) {
                    String str = TAG;
                    MMLog.d(str, "Progress event could not be fired because the time offset is invalid. url = " + progressEvent.url + ", offset = " + progressEvent.offset);
                }
                this.firedTrackingEvents.add(progressEvent);
            } else if (Utils.isEmpty(progressEvent.url)) {
                if (MMLog.isDebugEnabled()) {
                    String str2 = TAG;
                    MMLog.d(str2, "Progress event could not be fired because the url is empty. offset = " + progressEvent.offset);
                }
                this.firedTrackingEvents.add(progressEvent);
            } else if (!this.firedTrackingEvents.contains(trackingEvent) && i >= vastTimeToMilliseconds) {
                fireVideoTrackingEvent(progressEvent, i);
            }
        }
    }

    /* access modifiers changed from: private */
    public void enableSkipControls() {
        this.countdown.setVisibility(8);
        this.skipButton.setEnabled(true);
        this.skipButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VASTVideoView.this.skip();
            }
        });
    }

    public void onSeek(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onSeek");
        }
    }

    public void onMuted(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onMuted");
        }
    }

    public void onError(MMVideoView mMVideoView) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onError");
        }
        setKeepScreenOnUIThread(false);
        if (this.vastVideoViewListener != null) {
            this.vastVideoViewListener.onFailed();
        }
        if (this.overlayWebView != null) {
            this.overlayWebView.callJavascript("MmJsBridge.vast.fireErrorEvent", "Video playback error occurred.");
        }
        if (this.backgroundWebView != null) {
            this.backgroundWebView.callJavascript("MmJsBridge.vast.fireErrorEvent", "Video playback error occurred.");
        }
    }

    public void onBufferingUpdate(MMVideoView mMVideoView, int i) {
        if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "onBufferingUpdate");
        }
    }

    /* access modifiers changed from: private */
    public void fireVideoTrackingEvents(List<VASTParser.TrackingEvent> list, int i) {
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (VASTParser.TrackingEvent next : list) {
                if (next != null && !Utils.isEmpty(next.url) && !this.firedTrackingEvents.contains(next)) {
                    this.firedTrackingEvents.add(next);
                    arrayList.add(new VideoTrackingEvent(next.event.name(), next.url, i));
                }
            }
            TrackingEvent.fireEvents(arrayList);
        }
    }

    private void fireVideoTrackingEvent(VASTParser.TrackingEvent trackingEvent, int i) {
        fireVideoTrackingEvents(Arrays.asList(trackingEvent), i);
    }

    /* access modifiers changed from: private */
    public void fireImpressions() {
        if (this.inLineAd != null && this.inLineAd.impressions != null) {
            this.impressionViewabilityWatcher.stopWatching();
            ArrayList arrayList = new ArrayList();
            addTrackingEventUrls(arrayList, this.inLineAd.impressions, "impression");
            if (this.wrapperAds != null) {
                for (VASTParser.WrapperAd wrapperAd : this.wrapperAds) {
                    addTrackingEventUrls(arrayList, wrapperAd.impressions, "wrapper immpression");
                }
            }
            TrackingEvent.fireEvents(arrayList);
        }
    }

    /* access modifiers changed from: private */
    public int vastTimeToMilliseconds(String str, int i) {
        return vastTimeToMilliseconds(str, this.mmVideoView.getDuration(), i);
    }

    static int vastTimeToMilliseconds(String str, int i, int i2) {
        int i3;
        if (Utils.isEmpty(str)) {
            return i2;
        }
        String trim = str.trim();
        try {
            if (trim.contains("%")) {
                String replace = trim.replace("%", "");
                if (!Utils.isEmpty(replace)) {
                    return (int) ((Float.parseFloat(replace.trim()) / 100.0f) * ((float) i));
                }
                MMLog.e(TAG, "VAST time is missing percent value, parse value was: " + trim);
                return i2;
            }
            String[] split = trim.split("\\.");
            if (split.length <= 2) {
                if (split.length == 2) {
                    String str2 = split[0];
                    try {
                        i3 = Integer.parseInt(split[1]);
                        trim = str2;
                    } catch (NumberFormatException unused) {
                        trim = str2;
                        MMLog.e(TAG, "VAST time has invalid number format, parse value was: " + trim);
                        return i2;
                    }
                } else {
                    i3 = 0;
                }
                String[] split2 = trim.split(":");
                if (split2.length == 3) {
                    return (Integer.parseInt(split2[0]) * ADCHOICES_DEFAULT_DURATION) + (Integer.parseInt(split2[1]) * 60000) + (Integer.parseInt(split2[2]) * 1000) + i3;
                }
                MMLog.e(TAG, "VAST time has invalid HHMMSS format, parse value was: " + trim);
                return i2;
            }
            MMLog.e(TAG, "VAST time has invalid format, parse value was: " + trim);
            return i2;
        } catch (NumberFormatException unused2) {
            MMLog.e(TAG, "VAST time has invalid number format, parse value was: " + trim);
            return i2;
        }
    }

    /* access modifiers changed from: private */
    public void replay() {
        this.currentState = 1;
        if (this.overlayWebView != null) {
            this.overlayWebView.lastUpdateTime = 0;
        }
        if (this.backgroundWebView != null) {
            this.backgroundWebView.lastUpdateTime = 0;
        }
        updateComponentVisibility();
        this.replayButton.setVisibility(8);
        this.closeButton.setVisibility(8);
        this.skipButton.setVisibility(0);
        this.adChoicesButton.reset();
        this.mmVideoView.restart();
    }

    /* access modifiers changed from: private */
    public void skip() {
        if (this.selectedCreative != null) {
            fireVideoTrackingEvents(getWrapperLinearTrackingEvents(VASTParser.TrackableEvent.skip), 0);
            fireVideoTrackingEvents(this.selectedCreative.linearAd.trackingEvents.get(VASTParser.TrackableEvent.skip), 0);
        }
        this.mmVideoView.videoSkipped();
        doComplete();
    }

    /* access modifiers changed from: private */
    public void doComplete() {
        View childAt;
        this.currentState = 2;
        this.countdown.setVisibility(8);
        this.adChoicesButton.hideIcon();
        if (this.selectedCompanionAd == null || this.endCardContainer.getChildCount() <= 0) {
            close();
            return;
        }
        this.replayButton.setVisibility(0);
        this.skipButton.setVisibility(8);
        this.closeButton.setVisibility(0);
        for (int i = 0; i < this.buttonContainer.getChildCount(); i++) {
            View childAt2 = this.buttonContainer.getChildAt(i);
            if ((childAt2 instanceof FrameLayout) && (childAt = ((FrameLayout) childAt2).getChildAt(0)) != null) {
                childAt.setVisibility(0);
            }
        }
        updateComponentVisibility();
    }

    private boolean isPortrait() {
        return getResources().getConfiguration().orientation != 2;
    }

    private boolean isMediaFileVerticalVideo(VASTParser.MediaFile mediaFile) {
        return mediaFile != null && mediaFile.width <= mediaFile.height;
    }

    /* access modifiers changed from: private */
    public int getBackgroundColor(VASTParser.StaticResource staticResource) {
        if (!(staticResource == null || staticResource.backgroundColor == null)) {
            try {
                return Color.parseColor(staticResource.backgroundColor);
            } catch (IllegalArgumentException unused) {
                String str = TAG;
                MMLog.w(str, "Invalid hex color format specified = " + staticResource.backgroundColor);
            }
        }
        return -16777216;
    }

    public void release() {
        if (this.mmVideoView != null) {
            this.mmVideoView.stop();
            this.mmVideoView.release();
            this.mmVideoView = null;
        }
        if (this.videoFile != null) {
            if (!this.videoFile.delete()) {
                String str = TAG;
                MMLog.w(str, "Failed to delete video asset = " + this.videoFile.getAbsolutePath());
            }
            this.videoFile = null;
        }
        if (this.overlayWebView != null) {
            this.overlayWebView.release();
            this.overlayWebView = null;
        }
        if (this.companionAdWebView != null) {
            this.companionAdWebView.release();
            this.companionAdWebView = null;
        }
        if (this.backgroundWebView != null) {
            this.backgroundWebView.release();
            this.backgroundWebView = null;
        }
    }

    public boolean onBackPressed() {
        if (this.canSkip) {
            skip();
        }
        return this.canSkip;
    }

    /* access modifiers changed from: private */
    public void notifyListenerOnClick() {
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                if (VASTVideoView.this.vastVideoViewListener != null) {
                    VASTVideoView.this.vastVideoViewListener.onClicked();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void notifyListenerOnAdLeftApplication() {
        ThreadUtils.runOnWorkerThread(new Runnable() {
            public void run() {
                if (VASTVideoView.this.vastVideoViewListener != null) {
                    VASTVideoView.this.vastVideoViewListener.onAdLeftApplication();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void fireCompanionAdClickTracking() {
        if (this.selectedCompanionAd != null) {
            List<VASTParser.CompanionAd> wrapperCompanionAdTracking = getWrapperCompanionAdTracking();
            ArrayList arrayList = new ArrayList();
            addTrackingEventUrls(arrayList, this.selectedCompanionAd.companionClickTracking, "tracking");
            for (VASTParser.CompanionAd companionAd : wrapperCompanionAdTracking) {
                addTrackingEventUrls(arrayList, companionAd.companionClickTracking, "wrapper tracking");
            }
            TrackingEvent.fireEvents(arrayList);
        }
    }

    /* access modifiers changed from: private */
    public void fireClickTracking(VASTParser.VideoClicks videoClicks, boolean z) {
        if (videoClicks != null) {
            ArrayList arrayList = new ArrayList();
            addTrackingEventUrls(arrayList, videoClicks.clickTrackingUrls, "video click tracker");
            if (z) {
                addTrackingEventUrls(arrayList, videoClicks.customClickUrls, "custom click");
            }
            TrackingEvent.fireEvents(arrayList);
        }
    }

    /* access modifiers changed from: private */
    public void fireWrappersClickTracking(List<VASTParser.VideoClicks> list, boolean z) {
        if (list != null && !list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (VASTParser.VideoClicks next : list) {
                addTrackingEventUrls(arrayList, next.clickTrackingUrls, "wrapper video click tracker");
                if (z) {
                    addTrackingEventUrls(arrayList, next.customClickUrls, "wrapper custom click tracker");
                }
            }
            TrackingEvent.fireEvents(arrayList);
        }
    }

    private static void addTrackingEventUrls(List<TrackingEvent> list, List<String> list2, String str) {
        if (list2 != null) {
            for (String next : list2) {
                if (!Utils.isEmpty(next)) {
                    list.add(new TrackingEvent(str, next));
                }
            }
        }
    }

    private List<VASTParser.VideoClicks> getWrapperVideoClicks() {
        ArrayList arrayList = new ArrayList();
        if (this.wrapperAds != null) {
            for (VASTParser.WrapperAd next : this.wrapperAds) {
                if (next.creatives != null) {
                    for (VASTParser.Creative creative : next.creatives) {
                        if (!(creative.linearAd == null || creative.linearAd.videoClicks == null)) {
                            arrayList.add(creative.linearAd.videoClicks);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    private boolean hasVideoClicks(VASTParser.VideoClicks videoClicks) {
        return videoClicks != null && (!Utils.isEmpty(videoClicks.clickThrough) || !videoClicks.customClickUrls.isEmpty());
    }

    private boolean haveVideoClicks(List<VASTParser.VideoClicks> list) {
        for (VASTParser.VideoClicks hasVideoClicks : list) {
            if (hasVideoClicks(hasVideoClicks)) {
                return true;
            }
        }
        return false;
    }

    private List<VASTParser.CompanionAd> getWrapperCompanionAdTracking() {
        ArrayList arrayList = new ArrayList();
        if (this.wrapperAds == null) {
            return arrayList;
        }
        for (VASTParser.WrapperAd next : this.wrapperAds) {
            if (next.creatives != null) {
                for (VASTParser.Creative creative : next.creatives) {
                    if (creative.companionAds != null) {
                        Iterator<VASTParser.CompanionAd> it = creative.companionAds.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            VASTParser.CompanionAd next2 = it.next();
                            if (next2.htmlResource == null && next2.iframeResource == null && next2.staticResource == null) {
                                arrayList.add(next2);
                                break;
                            }
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public List<VASTParser.TrackingEvent> getWrapperLinearTrackingEvents(VASTParser.TrackableEvent trackableEvent) {
        List list;
        ArrayList arrayList = new ArrayList();
        if (this.wrapperAds != null) {
            for (VASTParser.WrapperAd next : this.wrapperAds) {
                if (next.creatives != null) {
                    for (VASTParser.Creative creative : next.creatives) {
                        if (!(creative.linearAd == null || (list = creative.linearAd.trackingEvents.get(trackableEvent)) == null)) {
                            arrayList.addAll(list);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    private boolean hasTrackingEvents(VASTParser.Creative creative) {
        if (creative == null) {
            return false;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(creative);
        return doCreativesHaveTrackingEvents(arrayList);
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean haveTrackingEvents(java.util.List<com.millennialmedia.internal.video.VASTParser.WrapperAd> r3) {
        /*
            r2 = this;
            r0 = 0
            if (r3 == 0) goto L_0x001b
            java.util.Iterator r3 = r3.iterator()
        L_0x0007:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x001b
            java.lang.Object r0 = r3.next()
            com.millennialmedia.internal.video.VASTParser$WrapperAd r0 = (com.millennialmedia.internal.video.VASTParser.WrapperAd) r0
            java.util.List r0 = r0.creatives
            boolean r0 = r2.doCreativesHaveTrackingEvents(r0)
            if (r0 == 0) goto L_0x0007
        L_0x001b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.video.VASTVideoView.haveTrackingEvents(java.util.List):boolean");
    }

    private boolean doCreativesHaveTrackingEvents(List<VASTParser.Creative> list) {
        if (list != null) {
            for (VASTParser.Creative next : list) {
                if (next.linearAd != null && !next.linearAd.trackingEvents.isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> getMoatIdentifiers() {
        VASTParser.MoatExtension moatExtension;
        StringBuilder sb = new StringBuilder();
        if (this.inLineAd.moatTrackingIds != null) {
            sb.append(this.inLineAd.moatTrackingIds);
        }
        if (this.wrapperAds != null) {
            moatExtension = null;
            for (VASTParser.WrapperAd next : this.wrapperAds) {
                if (next.moatExtension != null) {
                    moatExtension = next.moatExtension;
                }
                if (next.moatTrackingIds != null) {
                    if (sb.length() > 0) {
                        sb.append(';');
                    }
                    sb.append(next.moatTrackingIds);
                }
            }
        } else {
            moatExtension = null;
        }
        if (this.inLineAd.moatExtension != null) {
            moatExtension = this.inLineAd.moatExtension;
        }
        if (moatExtension == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        Utils.putIfNotNull(hashMap, "level1", moatExtension.level1);
        Utils.putIfNotNull(hashMap, "level2", moatExtension.level2);
        Utils.putIfNotNull(hashMap, "level3", moatExtension.level3);
        Utils.putIfNotNull(hashMap, "level4", moatExtension.level4);
        Utils.putIfNotNull(hashMap, "slicer1", moatExtension.slicer1);
        Utils.putIfNotNull(hashMap, "slicer2", moatExtension.slicer2);
        Utils.putIfNotNull(hashMap, "zMoatVASTIDs", sb.toString());
        return hashMap;
    }
}
