package com.millennialmedia.internal.video;

import android.text.TextUtils;
import android.util.Xml;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.utils.Utils;
import com.miniclip.videoads.ProviderConfig;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.mopub.mobileads.VastIconXmlManager;
import com.mopub.mobileads.VastResourceXmlManager;
import com.tapjoy.TJAdUnitConstants;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class VASTParser {
    private static final String MOAT_DEFAULT_EXTENSION_VALUE = "3pmoat";
    private static final String TAG = "VASTParser";

    public static class InLineAd extends Ad {
    }

    public enum TrackableEvent {
        creativeView,
        start,
        firstQuartile,
        midpoint,
        thirdQuartile,
        complete,
        closeLinear,
        skip,
        progress
    }

    public static class Ad {
        public List<Creative> creatives;
        public String error;
        public String id;
        public List<String> impressions = new ArrayList();
        public MMExtension mmExtension;
        public MoatExtension moatExtension;
        public String moatTrackingIds;
        public String title;

        Ad() {
        }

        public String toString() {
            return ((((((("Ad:[" + "id:" + this.id + ";") + "error:" + this.error + ";") + "impressions:" + this.impressions + ";") + "creatives:" + this.creatives + ";") + "mmExtension:" + this.mmExtension + ";") + "moatExtension:" + this.moatExtension + ";") + "moatTrackingIds:" + this.moatTrackingIds + ";") + "]";
        }
    }

    public static class WrapperAd extends Ad {
        public String adTagURI;

        public String toString() {
            return (("WrapperAd:[" + super.toString()) + "adTagURI:" + this.adTagURI + ";") + "]";
        }
    }

    public static class AdSystem {
        public String name;
        public String version;

        AdSystem(String str, String str2) {
            this.name = str;
            this.version = str2;
        }

        public String toString() {
            return (("AdSystem:[" + "name:" + this.name + ";") + "version:" + this.version + ";") + "]";
        }
    }

    public static class Creative {
        public List<CompanionAd> companionAds;
        public String id;
        public LinearAd linearAd;
        public Integer sequence;

        Creative(String str, Integer num) {
            this.id = str;
            this.sequence = num;
        }

        public String toString() {
            return (((("Creative:[" + "id:" + this.id + ";") + "sequence:" + this.sequence + ";") + "linearAd:" + this.linearAd + ";") + "companionAds:" + this.companionAds + ";") + "]";
        }
    }

    public static class LinearAd {
        public List<Icon> icons;
        public List<MediaFile> mediaFiles;
        public String skipOffset;
        public final Map<TrackableEvent, List<TrackingEvent>> trackingEvents = new HashMap();
        public VideoClicks videoClicks;

        LinearAd(String str) {
            this.skipOffset = str;
        }

        public String toString() {
            return (((("LinearAd:[" + "skipOffset:" + this.skipOffset + ";") + "mediaFiles:" + this.mediaFiles + ";") + "trackingEvents:" + this.trackingEvents + ";") + "videoClicks:" + this.videoClicks + ";") + "]";
        }
    }

    public static class Icon {
        public String apiFramework;
        public String duration;
        public Integer height;
        public WebResource htmlResource;
        public IconClicks iconClicks;
        public List<String> iconViewTrackingUrls = new ArrayList();
        public WebResource iframeResource;
        public String offset;
        public String program;
        public StaticResource staticResource;
        public Integer width;
        public String xPosition;
        public String yPosition;

        Icon(String str, Integer num, Integer num2, String str2, String str3, String str4, String str5, String str6) {
            this.program = str;
            this.width = num;
            this.height = num2;
            this.xPosition = str2;
            this.yPosition = str3;
            this.apiFramework = str4;
            this.offset = str5;
            this.duration = str6;
        }

        public String toString() {
            return ((((((((((((("Icon:[" + "program:" + this.program + ";") + "width:" + this.width + ";") + "height:" + this.height + ";") + "xPosition:" + this.xPosition + ";") + "yPosition:" + this.yPosition + ";") + "apiFramework:" + this.apiFramework + ";") + "offset:" + this.offset + ";") + "duration:" + this.duration + ";") + "staticResource:" + this.staticResource + ";") + "htmlResource:" + this.htmlResource + ";") + "iframeResource:" + this.iframeResource + ";") + "iconClicks:" + this.iconClicks + ";") + "iconViewTrackingUrls:" + this.iconViewTrackingUrls + ";") + "]";
        }
    }

    public static class MediaFile {
        public String apiFramework;
        public int bitrate;
        public String contentType;
        public String delivery;
        public int height;
        public boolean maintainAspectRatio;
        public String url;
        public int width;

        MediaFile(String str, String str2, String str3, String str4, int i, int i2, int i3, boolean z) {
            this.url = str;
            this.contentType = str2;
            this.delivery = str3;
            this.apiFramework = str4;
            this.width = i;
            this.height = i2;
            this.bitrate = i3;
            this.maintainAspectRatio = z;
        }

        public String toString() {
            return (((((((("MediaFile:[" + "url:" + this.url + ";") + "contentType:" + this.contentType + ";") + "delivery:" + this.delivery + ";") + "apiFramework:" + this.apiFramework + ";") + "width:" + this.width + ";") + "height:" + this.height + ";") + "bitrate:" + this.bitrate + ";") + "maintainAspectRatio:" + this.maintainAspectRatio + ";") + "]";
        }
    }

    public static class TrackingEvent {
        public TrackableEvent event;
        public String url;

        TrackingEvent(TrackableEvent trackableEvent, String str) {
            this.event = trackableEvent;
            this.url = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TrackingEvent)) {
                return false;
            }
            TrackingEvent trackingEvent = (TrackingEvent) obj;
            return this.event == trackingEvent.event && this.url.equals(trackingEvent.url);
        }

        public int hashCode() {
            return (31 * this.url.hashCode()) + this.event.hashCode();
        }

        public String toString() {
            return (("TrackingEvent:[" + "event:" + this.event + ";") + "url:" + this.url + ";") + "]";
        }
    }

    public static class ProgressEvent extends TrackingEvent {
        public String offset;

        ProgressEvent(String str, String str2) {
            super(TrackableEvent.progress, str);
            this.offset = str2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof ProgressEvent) && super.equals(obj) && this.offset.equals(((ProgressEvent) obj).offset);
        }

        public int hashCode() {
            return (31 * super.hashCode()) + this.offset.hashCode();
        }

        public String toString() {
            return (("ProgressEvent:[" + super.toString() + ";") + "offset:" + this.offset) + "]";
        }
    }

    public static class VideoClicks {
        public String clickThrough;
        public final List<String> clickTrackingUrls = new ArrayList();
        public final List<String> customClickUrls = new ArrayList();

        VideoClicks(String str, List<String> list, List<String> list2) {
            this.clickThrough = str;
            if (list != null) {
                this.clickTrackingUrls.addAll(list);
            }
            if (list2 != null) {
                this.customClickUrls.addAll(list2);
            }
        }

        public String toString() {
            return ((("VideoClicks:[" + "clickThrough:" + this.clickThrough + ";") + "clickTrackingUrls:" + this.clickTrackingUrls + ";") + "customClickUrls:" + this.customClickUrls + ";") + "]";
        }
    }

    public static class IconClicks {
        public String clickThrough;
        public List<String> clickTrackingUrls = new ArrayList();

        IconClicks() {
        }

        public String toString() {
            return (("IconClicks:[" + "clickThrough:" + this.clickThrough + ";") + "clickTrackingUrls:" + this.clickTrackingUrls + ";") + "]";
        }
    }

    public static class CompanionAd {
        public Integer assetHeight;
        public Integer assetWidth;
        public String companionClickThrough;
        public List<String> companionClickTracking = new ArrayList();
        public Integer height;
        public boolean hideButtons;
        public WebResource htmlResource;
        public String id;
        public WebResource iframeResource;
        public StaticResource staticResource;
        public Map<TrackableEvent, List<TrackingEvent>> trackingEvents;
        public Integer width;

        CompanionAd(String str, Integer num, Integer num2, Integer num3, Integer num4, boolean z) {
            this.id = str;
            this.width = num;
            this.height = num2;
            this.assetWidth = num3;
            this.assetHeight = num4;
            this.hideButtons = z;
        }

        public String toString() {
            return (((((((((((("CompanionAd:[" + "id:" + this.id + ";") + "width:" + this.width + ";") + "height:" + this.height + ";") + "assetWidth:" + this.assetWidth + ";") + "assetHeight:" + this.assetHeight + ";") + "hideButtons:" + this.hideButtons + ";") + "staticResource:" + this.staticResource + ";") + "htmlResource:" + this.htmlResource + ";") + "iframeResource:" + this.iframeResource + ";") + "companionClickThrough:" + this.companionClickThrough + ";") + "trackingEvents:" + this.trackingEvents + ";") + "companionClickTracking:" + this.companionClickTracking + ";") + "]";
        }
    }

    public static class StaticResource {
        public String backgroundColor;
        public String creativeType;
        public String uri;

        StaticResource(String str, String str2, String str3) {
            this.backgroundColor = str2;
            this.creativeType = str;
            this.uri = str3;
        }

        public String toString() {
            return ((("StaticResource:[" + "backgroundColor:" + this.backgroundColor + ";") + "creativeType:" + this.creativeType + ";") + "uri:" + this.uri + ";") + "]";
        }
    }

    public static class WebResource {
        public String uri;

        WebResource(String str) {
            this.uri = str;
        }

        public String toString() {
            return "WebResource:[uri:" + this.uri + "]";
        }
    }

    public static class MoatExtension {
        public String level1;
        public String level2;
        public String level3;
        public String level4;
        public String slicer1;
        public String slicer2;

        public String toString() {
            return (((((("MoatExtension:[" + "level1:" + this.level1 + ";") + "level2:" + this.level2 + ";") + "level3:" + this.level3 + ";") + "level4:" + this.level4 + ";") + "slicer1:" + this.slicer1 + ";") + "slicer2:" + this.slicer2 + ";") + "]";
        }
    }

    public static class MMExtension {
        public Background background;
        public List<Button> buttons;
        public Overlay overlay;

        MMExtension(Overlay overlay2, Background background2, List<Button> list) {
            this.overlay = overlay2;
            this.background = background2;
            this.buttons = list;
        }

        public String toString() {
            return ((("MMExtension:[" + "overlay:" + this.overlay + ";") + "background:" + this.background + ";") + "buttons:" + this.buttons + ";") + "]";
        }
    }

    public static class Overlay {
        public boolean hideButtons;
        public String uri;

        Overlay(String str, boolean z) {
            this.uri = str;
            this.hideButtons = z;
        }

        public String toString() {
            return (("Overlay:[" + "uri:" + this.uri + ";") + "hideButtons:" + this.hideButtons + ";") + "]";
        }
    }

    public static class Button {
        public ButtonClicks buttonClicks;
        public String name;
        public String offset;
        public int position;
        public StaticResource staticResource;

        public Button(String str, String str2, int i) {
            this.name = str;
            this.offset = str2;
            this.position = i;
        }

        public String toString() {
            return ((((("Button:[" + "name:" + this.name + ";") + "offset:" + this.offset + ";") + "position:" + this.position + ";") + "staticResource:" + this.staticResource + ";") + "buttonClicks:" + this.buttonClicks + ";") + "]";
        }
    }

    public static class ButtonClicks {
        public String clickThrough;
        public List<String> clickTrackingUrls;

        public ButtonClicks(String str, List<String> list) {
            this.clickThrough = str;
            this.clickTrackingUrls = list;
        }

        public String toString() {
            return (("ButtonClicks:[" + "clickThrough:" + this.clickThrough + ";") + "clickTrackingUrls:" + this.clickTrackingUrls + ";") + "]";
        }
    }

    public static class Background {
        public boolean hideButtons;
        public StaticResource staticResource;
        public WebResource webResource;

        Background(boolean z) {
            this.hideButtons = z;
        }

        public String toString() {
            return ((("Background:[" + "hideButtons:" + this.hideButtons + ";") + "staticResource:" + this.staticResource + ";") + "webResource:" + this.webResource + ";") + "]";
        }
    }

    public static Ad parse(String str) throws XmlPullParserException, IOException {
        if (str == null) {
            MMLog.w(TAG, "Ad content was null.");
            return null;
        }
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        newPullParser.setInput(new StringReader(str));
        newPullParser.nextTag();
        if (!newPullParser.getName().equals("VAST")) {
            return null;
        }
        String attributeValue = newPullParser.getAttributeValue("", ProviderConfig.MCVideoAdsVersionKey);
        if (!Utils.isEmpty(attributeValue)) {
            try {
                if (Integer.parseInt("" + attributeValue.trim().charAt(0)) > 1) {
                    newPullParser.nextTag();
                    return readAd(newPullParser);
                }
                String str2 = TAG;
                MMLog.e(str2, "Unsupported VAST version = " + attributeValue);
                return null;
            } catch (NumberFormatException e) {
                String str3 = TAG;
                MMLog.e(str3, "Invalid version format for VAST tag with version = " + attributeValue, e);
                return null;
            }
        } else {
            MMLog.e(TAG, "VAST version not provided.");
            return null;
        }
    }

    private static Ad readAd(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        Ad ad = null;
        xmlPullParser.require(2, null, "Ad");
        String attributeValue = xmlPullParser.getAttributeValue(null, "id");
        while (true) {
            if (xmlPullParser.next() == 3) {
                break;
            } else if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("InLine")) {
                    ad = readInLine(xmlPullParser);
                    break;
                } else if (xmlPullParser.getName().equals("Wrapper")) {
                    ad = readWrapper(xmlPullParser);
                    break;
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        if (ad != null) {
            ad.id = attributeValue;
        }
        return ad;
    }

    private static WrapperAd readWrapper(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "Wrapper");
        WrapperAd wrapperAd = new WrapperAd();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("VASTAdTagURI")) {
                    wrapperAd.adTagURI = nextText(xmlPullParser);
                } else if (xmlPullParser.getName().equals("Creatives")) {
                    wrapperAd.creatives = readCreatives(xmlPullParser);
                } else if (xmlPullParser.getName().equals("Impression")) {
                    String nextText = nextText(xmlPullParser);
                    if (!Utils.isEmpty(nextText)) {
                        wrapperAd.impressions.add(nextText);
                    }
                } else if (xmlPullParser.getName().equals("Extensions")) {
                    readExtensions(xmlPullParser, wrapperAd);
                } else if (xmlPullParser.getName().equals("Error")) {
                    String nextText2 = nextText(xmlPullParser);
                    if (!Utils.isEmpty(nextText2)) {
                        wrapperAd.error = nextText2;
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return wrapperAd;
    }

    private static InLineAd readInLine(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "InLine");
        InLineAd inLineAd = new InLineAd();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("Creatives")) {
                    inLineAd.creatives = readCreatives(xmlPullParser);
                } else if (xmlPullParser.getName().equals("Impression")) {
                    String nextText = nextText(xmlPullParser);
                    if (!Utils.isEmpty(nextText)) {
                        inLineAd.impressions.add(nextText);
                    }
                } else if (xmlPullParser.getName().equals("Extensions")) {
                    readExtensions(xmlPullParser, inLineAd);
                } else if (xmlPullParser.getName().equals("Error")) {
                    String nextText2 = nextText(xmlPullParser);
                    if (!Utils.isEmpty(nextText2)) {
                        inLineAd.error = nextText2;
                    }
                } else if (xmlPullParser.getName().equals("AdTitle")) {
                    String nextText3 = nextText(xmlPullParser);
                    if (!TextUtils.isEmpty(nextText3)) {
                        inLineAd.title = nextText3;
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return inLineAd;
    }

    private static void readExtensions(XmlPullParser xmlPullParser, Ad ad) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "Extensions");
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("Extension")) {
                    String attributeValue = xmlPullParser.getAttributeValue(null, "type");
                    if ("MMInteractiveVideo".equals(attributeValue)) {
                        ad.mmExtension = readMMExtension(xmlPullParser);
                    } else if ("AOLMoat".equals(attributeValue)) {
                        ad.moatExtension = readMoatExtension(xmlPullParser);
                    } else {
                        readUntypedExtension(xmlPullParser, ad);
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        if (ad.moatExtension == null && !Utils.isEmpty(ad.moatTrackingIds)) {
            ad.moatExtension = getDefaultMoatExtension();
        }
    }

    private static void readUntypedExtension(XmlPullParser xmlPullParser, Ad ad) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "Extension");
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (VastExtensionXmlManager.AD_VERIFICATIONS.equals(xmlPullParser.getName())) {
                    ad.moatTrackingIds = readAdVerifications(xmlPullParser);
                } else {
                    skip(xmlPullParser);
                }
            }
        }
    }

    private static MoatExtension readMoatExtension(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        MoatExtension moatExtension = null;
        xmlPullParser.require(2, null, "Extension");
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2 && xmlPullParser.getName().equals(VastExtensionXmlManager.MOAT)) {
                moatExtension = new MoatExtension();
                while (xmlPullParser.next() != 3) {
                    if (xmlPullParser.getEventType() == 2) {
                        if (xmlPullParser.getName().equals("Level1")) {
                            moatExtension.level1 = nextText(xmlPullParser);
                        } else if (xmlPullParser.getName().equals("Level2")) {
                            moatExtension.level2 = nextText(xmlPullParser);
                        } else if (xmlPullParser.getName().equals("Level3")) {
                            moatExtension.level3 = nextText(xmlPullParser);
                        } else if (xmlPullParser.getName().equals("Level4")) {
                            moatExtension.level4 = nextText(xmlPullParser);
                        } else if (xmlPullParser.getName().equals("Slicer1")) {
                            moatExtension.slicer1 = nextText(xmlPullParser);
                        } else if (xmlPullParser.getName().equals("Slicer2")) {
                            moatExtension.slicer2 = nextText(xmlPullParser);
                        } else {
                            skip(xmlPullParser);
                        }
                    }
                }
            }
        }
        return moatExtension;
    }

    private static String readAdVerifications(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, VastExtensionXmlManager.AD_VERIFICATIONS);
        StringBuilder sb = new StringBuilder(100);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (!xmlPullParser.getName().equals(VastExtensionXmlManager.VERIFICATION) || !"moat".equalsIgnoreCase(xmlPullParser.getAttributeValue(null, VastExtensionXmlManager.VENDOR))) {
                    skip(xmlPullParser);
                } else {
                    while (xmlPullParser.next() != 3) {
                        if (xmlPullParser.getEventType() == 2) {
                            if ("ViewableImpression".equals(xmlPullParser.getName())) {
                                if (sb.length() > 0) {
                                    sb.append(';');
                                }
                                sb.append(String.format("<ViewableImpression id=\"%s\">", xmlPullParser.getAttributeValue(null, "id")));
                                String replaceAll = xmlPullParser.nextText().replaceAll("\\s", "");
                                if (!Utils.isEmpty(replaceAll)) {
                                    sb.append(String.format("<![CDATA[%s]]>", replaceAll));
                                }
                                sb.append("</ViewableImpression>");
                            } else {
                                skip(xmlPullParser);
                            }
                        }
                    }
                }
            }
        }
        return sb.toString();
    }

    private static MMExtension readMMExtension(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "Extension");
        Overlay overlay = null;
        Background background = null;
        List<Button> list = null;
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("Overlay")) {
                    overlay = new Overlay(nextText(xmlPullParser), toBoolean(xmlPullParser.getAttributeValue(null, "hideButtons"), true));
                } else if (xmlPullParser.getName().equals("Background")) {
                    background = readBackground(xmlPullParser);
                } else if (xmlPullParser.getName().equals("Buttons")) {
                    list = readButtons(xmlPullParser);
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return new MMExtension(overlay, background, list);
    }

    private static Background readBackground(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "Background");
        Background background = new Background(toBoolean(xmlPullParser.getAttributeValue(null, "hideButtons"), false));
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals(VastResourceXmlManager.STATIC_RESOURCE)) {
                    background.staticResource = new StaticResource(xmlPullParser.getAttributeValue(null, VastResourceXmlManager.CREATIVE_TYPE), xmlPullParser.getAttributeValue(null, TJAdUnitConstants.String.BACKGROUND_COLOR), nextText(xmlPullParser));
                } else if (xmlPullParser.getName().equals("WebResource")) {
                    background.webResource = new WebResource(nextText(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return background;
    }

    private static List<Button> readButtons(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "Buttons");
        ArrayList arrayList = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("Button")) {
                    arrayList.add(readButton(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.millennialmedia.internal.video.VASTParser.Button readButton(org.xmlpull.v1.XmlPullParser r8) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            java.lang.String r0 = "Button"
            r1 = 2
            r2 = 0
            r8.require(r1, r2, r0)
            java.lang.String r0 = "name"
            java.lang.String r0 = r8.getAttributeValue(r2, r0)
            java.lang.String r3 = "offset"
            java.lang.String r3 = r8.getAttributeValue(r2, r3)
            java.lang.String r4 = "position"
            java.lang.String r4 = r8.getAttributeValue(r2, r4)
            boolean r5 = com.millennialmedia.internal.utils.Utils.isEmpty(r4)
            if (r5 != 0) goto L_0x003f
            int r5 = java.lang.Integer.parseInt(r4)     // Catch:{ NumberFormatException -> 0x0024 }
            goto L_0x0040
        L_0x0024:
            java.lang.String r5 = com.millennialmedia.internal.video.VASTParser.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Invalid position: "
            r6.append(r7)
            r6.append(r4)
            java.lang.String r4 = " for Button."
            r6.append(r4)
            java.lang.String r4 = r6.toString()
            com.millennialmedia.MMLog.w(r5, r4)
        L_0x003f:
            r5 = 0
        L_0x0040:
            com.millennialmedia.internal.video.VASTParser$Button r4 = new com.millennialmedia.internal.video.VASTParser$Button
            r4.<init>(r0, r3, r5)
        L_0x0045:
            int r0 = r8.next()
            r3 = 3
            if (r0 == r3) goto L_0x008e
            int r0 = r8.getEventType()
            if (r0 == r1) goto L_0x0053
            goto L_0x0045
        L_0x0053:
            java.lang.String r0 = r8.getName()
            java.lang.String r3 = "StaticResource"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x0077
            com.millennialmedia.internal.video.VASTParser$StaticResource r0 = new com.millennialmedia.internal.video.VASTParser$StaticResource
            java.lang.String r3 = "creativeType"
            java.lang.String r3 = r8.getAttributeValue(r2, r3)
            java.lang.String r5 = "backgroundColor"
            java.lang.String r5 = r8.getAttributeValue(r2, r5)
            java.lang.String r6 = nextText(r8)
            r0.<init>(r3, r5, r6)
            r4.staticResource = r0
            goto L_0x0045
        L_0x0077:
            java.lang.String r0 = r8.getName()
            java.lang.String r3 = "ButtonClicks"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x008a
            com.millennialmedia.internal.video.VASTParser$ButtonClicks r0 = readButtonClicks(r8)
            r4.buttonClicks = r0
            goto L_0x0045
        L_0x008a:
            skip(r8)
            goto L_0x0045
        L_0x008e:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.internal.video.VASTParser.readButton(org.xmlpull.v1.XmlPullParser):com.millennialmedia.internal.video.VASTParser$Button");
    }

    private static ButtonClicks readButtonClicks(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "ButtonClicks");
        ButtonClicks buttonClicks = new ButtonClicks(null, new ArrayList());
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("ButtonClickThrough")) {
                    buttonClicks.clickThrough = nextText(xmlPullParser);
                } else if (xmlPullParser.getName().equals("ButtonClickTracking")) {
                    buttonClicks.clickTrackingUrls.add(nextText(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return buttonClicks;
    }

    private static List<Creative> readCreatives(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "Creatives");
        ArrayList arrayList = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("Creative")) {
                    arrayList.add(readCreative(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    private static Creative readCreative(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        Integer num = null;
        xmlPullParser.require(2, null, "Creative");
        String attributeValue = xmlPullParser.getAttributeValue(null, "AdID");
        String attributeValue2 = xmlPullParser.getAttributeValue(null, "sequence");
        if (!Utils.isEmpty(attributeValue2)) {
            try {
                num = Integer.decode(attributeValue2);
            } catch (NumberFormatException unused) {
                String str = TAG;
                MMLog.w(str, "Invalid sequence number: " + attributeValue2 + " for Creative.");
            }
        }
        Creative creative = new Creative(attributeValue, num);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("Linear")) {
                    creative.linearAd = readLinear(xmlPullParser);
                } else if (xmlPullParser.getName().equals("CompanionAds")) {
                    creative.companionAds = readCompanionAds(xmlPullParser);
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return creative;
    }

    private static List<CompanionAd> readCompanionAds(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "CompanionAds");
        ArrayList arrayList = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("Companion")) {
                    CompanionAd readCompanionAd = readCompanionAd(xmlPullParser);
                    if (readCompanionAd != null) {
                        arrayList.add(readCompanionAd);
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    private static CompanionAd readCompanionAd(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        CompanionAd companionAd;
        xmlPullParser.require(2, null, "Companion");
        try {
            companionAd = new CompanionAd(xmlPullParser.getAttributeValue(null, "id"), toInteger(xmlPullParser.getAttributeValue(null, "width")), toInteger(xmlPullParser.getAttributeValue(null, "height")), toInteger(xmlPullParser.getAttributeValue(null, "assetWidth")), toInteger(xmlPullParser.getAttributeValue(null, "assetHeight")), toBoolean(xmlPullParser.getAttributeValue(null, "hideButtons"), false));
            while (xmlPullParser.next() != 3) {
                try {
                    if (xmlPullParser.getEventType() == 2) {
                        if (xmlPullParser.getName().equals(VastResourceXmlManager.STATIC_RESOURCE)) {
                            companionAd.staticResource = new StaticResource(xmlPullParser.getAttributeValue(null, VastResourceXmlManager.CREATIVE_TYPE), xmlPullParser.getAttributeValue(null, TJAdUnitConstants.String.BACKGROUND_COLOR), nextText(xmlPullParser));
                        } else if (xmlPullParser.getName().equals(VastResourceXmlManager.HTML_RESOURCE)) {
                            companionAd.htmlResource = new WebResource(nextText(xmlPullParser));
                        } else if (xmlPullParser.getName().equals(VastResourceXmlManager.IFRAME_RESOURCE)) {
                            companionAd.iframeResource = new WebResource(nextText(xmlPullParser));
                        } else if (xmlPullParser.getName().equals("TrackingEvents")) {
                            companionAd.trackingEvents = readTrackingEvents(xmlPullParser);
                        } else if (xmlPullParser.getName().equals("CompanionClickTracking")) {
                            String nextText = nextText(xmlPullParser);
                            if (!Utils.isEmpty(nextText)) {
                                companionAd.companionClickTracking.add(nextText);
                            }
                        } else if (xmlPullParser.getName().equals("CompanionClickThrough")) {
                            String nextText2 = nextText(xmlPullParser);
                            if (!Utils.isEmpty(nextText2)) {
                                companionAd.companionClickThrough = nextText2;
                            }
                        } else {
                            skip(xmlPullParser);
                        }
                    }
                } catch (NumberFormatException e) {
                    e = e;
                    MMLog.e(TAG, "Syntax error in Companion element; skipping.", e);
                    return companionAd;
                }
            }
        } catch (NumberFormatException e2) {
            e = e2;
            companionAd = null;
            MMLog.e(TAG, "Syntax error in Companion element; skipping.", e);
            return companionAd;
        }
        return companionAd;
    }

    private static LinearAd readLinear(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "Linear");
        LinearAd linearAd = new LinearAd(xmlPullParser.getAttributeValue(null, "skipoffset"));
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("MediaFiles")) {
                    linearAd.mediaFiles = readMediaFiles(xmlPullParser);
                } else if (xmlPullParser.getName().equals("TrackingEvents")) {
                    linearAd.trackingEvents.putAll(readTrackingEvents(xmlPullParser));
                } else if (xmlPullParser.getName().equals(VastLinearXmlManager.ICONS)) {
                    linearAd.icons = readIcons(xmlPullParser);
                } else if (xmlPullParser.getName().equals("VideoClicks")) {
                    linearAd.videoClicks = readVideoClicks(xmlPullParser);
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return linearAd;
    }

    private static VideoClicks readVideoClicks(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "VideoClicks");
        VideoClicks videoClicks = new VideoClicks(null, new ArrayList(), new ArrayList());
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("ClickThrough")) {
                    videoClicks.clickThrough = nextText(xmlPullParser);
                } else if (xmlPullParser.getName().equals("ClickTracking")) {
                    videoClicks.clickTrackingUrls.add(nextText(xmlPullParser));
                } else if (xmlPullParser.getName().equals("CustomClick")) {
                    videoClicks.customClickUrls.add(nextText(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return videoClicks;
    }

    private static IconClicks readIconClicks(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, VastIconXmlManager.ICON_CLICKS);
        IconClicks iconClicks = new IconClicks();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals(VastIconXmlManager.ICON_CLICK_THROUGH)) {
                    String nextText = nextText(xmlPullParser);
                    if (!Utils.isEmpty(nextText)) {
                        iconClicks.clickThrough = nextText;
                    }
                } else if (xmlPullParser.getName().equals(VastIconXmlManager.ICON_CLICK_TRACKING)) {
                    String nextText2 = nextText(xmlPullParser);
                    if (!Utils.isEmpty(nextText2)) {
                        iconClicks.clickTrackingUrls.add(nextText2);
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return iconClicks;
    }

    private static List<Icon> readIcons(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, VastLinearXmlManager.ICONS);
        ArrayList arrayList = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals(VastLinearXmlManager.ICON)) {
                    arrayList.add(readIcon(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    public static Icon readIcon(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, VastLinearXmlManager.ICON);
        String attributeValue = xmlPullParser.getAttributeValue(null, "program");
        String attributeValue2 = xmlPullParser.getAttributeValue(null, "width");
        String attributeValue3 = xmlPullParser.getAttributeValue(null, "height");
        Icon icon = new Icon(attributeValue, toInteger(attributeValue2), toInteger(attributeValue3), xmlPullParser.getAttributeValue(null, "xPosition"), xmlPullParser.getAttributeValue(null, "yPosition"), xmlPullParser.getAttributeValue(null, "apiFramework"), xmlPullParser.getAttributeValue(null, VastIconXmlManager.OFFSET), xmlPullParser.getAttributeValue(null, VastIconXmlManager.DURATION));
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals(VastResourceXmlManager.STATIC_RESOURCE)) {
                    icon.staticResource = new StaticResource(xmlPullParser.getAttributeValue(null, VastResourceXmlManager.CREATIVE_TYPE), xmlPullParser.getAttributeValue(null, TJAdUnitConstants.String.BACKGROUND_COLOR), nextText(xmlPullParser));
                } else if (xmlPullParser.getName().equals(VastResourceXmlManager.HTML_RESOURCE)) {
                    icon.htmlResource = new WebResource(nextText(xmlPullParser));
                } else if (xmlPullParser.getName().equals(VastResourceXmlManager.IFRAME_RESOURCE)) {
                    icon.iframeResource = new WebResource(nextText(xmlPullParser));
                } else if (xmlPullParser.getName().equals(VastIconXmlManager.ICON_CLICKS)) {
                    icon.iconClicks = readIconClicks(xmlPullParser);
                } else if (xmlPullParser.getName().equals(VastIconXmlManager.ICON_VIEW_TRACKING)) {
                    String nextText = nextText(xmlPullParser);
                    if (!Utils.isEmpty(nextText)) {
                        icon.iconViewTrackingUrls.add(nextText);
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return icon;
    }

    private static Map<TrackableEvent, List<TrackingEvent>> readTrackingEvents(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        Object obj;
        xmlPullParser.require(2, null, "TrackingEvents");
        HashMap hashMap = new HashMap();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("Tracking")) {
                    String attributeValue = xmlPullParser.getAttributeValue(null, "event");
                    String attributeValue2 = xmlPullParser.getAttributeValue(null, VastIconXmlManager.OFFSET);
                    String nextText = nextText(xmlPullParser);
                    if (!Utils.isEmpty(attributeValue)) {
                        try {
                            TrackableEvent valueOf = TrackableEvent.valueOf(attributeValue.trim());
                            if (TrackableEvent.progress.equals(valueOf)) {
                                obj = new ProgressEvent(nextText, attributeValue2);
                            } else {
                                obj = new TrackingEvent(valueOf, nextText);
                            }
                            List list = (List) hashMap.get(valueOf);
                            if (list == null) {
                                list = new ArrayList();
                                hashMap.put(valueOf, list);
                            }
                            list.add(obj);
                        } catch (IllegalArgumentException unused) {
                            if (MMLog.isDebugEnabled()) {
                                String str = TAG;
                                MMLog.d(str, "Unsupported VAST event type: " + attributeValue);
                            }
                        }
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return hashMap;
    }

    private static List<MediaFile> readMediaFiles(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "MediaFiles");
        ArrayList arrayList = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (xmlPullParser.getName().equals("MediaFile")) {
                    try {
                        arrayList.add(new MediaFile(nextText(xmlPullParser), xmlPullParser.getAttributeValue(null, "type"), xmlPullParser.getAttributeValue(null, "delivery"), xmlPullParser.getAttributeValue(null, "apiFramework"), toInt(xmlPullParser.getAttributeValue(null, "width"), 0), toInt(xmlPullParser.getAttributeValue(null, "height"), 0), toInt(xmlPullParser.getAttributeValue(null, "bitrate"), 0), Boolean.parseBoolean(xmlPullParser.getAttributeValue(null, "maintainAspectRatio"))));
                    } catch (NumberFormatException e) {
                        MMLog.e(TAG, "Skipping malformed MediaFile element in VAST response.", e);
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        return arrayList;
    }

    private static void skip(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        if (xmlPullParser.getEventType() != 2) {
            throw new IllegalStateException();
        }
        int i = 1;
        while (i != 0) {
            switch (xmlPullParser.next()) {
                case 2:
                    i++;
                    break;
                case 3:
                    i--;
                    break;
            }
        }
    }

    private static MoatExtension getDefaultMoatExtension() {
        MoatExtension moatExtension = new MoatExtension();
        moatExtension.level1 = MOAT_DEFAULT_EXTENSION_VALUE;
        moatExtension.level2 = MOAT_DEFAULT_EXTENSION_VALUE;
        moatExtension.level3 = MOAT_DEFAULT_EXTENSION_VALUE;
        moatExtension.level4 = MOAT_DEFAULT_EXTENSION_VALUE;
        moatExtension.slicer1 = MOAT_DEFAULT_EXTENSION_VALUE;
        moatExtension.slicer2 = MOAT_DEFAULT_EXTENSION_VALUE;
        return moatExtension;
    }

    private static boolean toBoolean(String str, boolean z) {
        return str == null ? z : Boolean.parseBoolean(str);
    }

    private static Integer toInteger(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        return Integer.valueOf(str);
    }

    private static int toInt(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return i;
        }
        return Integer.parseInt(str);
    }

    private static String nextText(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        String nextText = xmlPullParser.nextText();
        return nextText != null ? nextText.trim() : nextText;
    }
}
