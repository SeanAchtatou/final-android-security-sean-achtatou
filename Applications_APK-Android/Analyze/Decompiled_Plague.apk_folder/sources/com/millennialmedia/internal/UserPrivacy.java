package com.millennialmedia.internal;

import android.text.TextUtils;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.task.TaskFactory;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.Utils;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class UserPrivacy {
    private static final String TAG = "UserPrivacy";
    private static Map<String, String> consentData = new HashMap();
    private static boolean consentRequired = false;
    private static AtomicBoolean geoIpRequestInProgress = new AtomicBoolean(false);
    static Boolean locationRequiresConsent;

    public static void setConsentRequired(boolean z) {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Setting consent required: " + z);
        }
        consentRequired = z;
    }

    static boolean isConsentRequired() {
        return consentRequired;
    }

    public static Boolean isCoveredByGDPR() {
        if (!consentRequired && locationRequiresConsent == null) {
            return null;
        }
        return Boolean.valueOf(consentRequired || Boolean.TRUE.equals(locationRequiresConsent));
    }

    public static boolean canPassUserData() {
        Boolean isCoveredByGDPR = isCoveredByGDPR();
        if (Boolean.FALSE.equals(isCoveredByGDPR)) {
            return true;
        }
        if (!Boolean.TRUE.equals(isCoveredByGDPR) || consentData.isEmpty()) {
            return false;
        }
        return true;
    }

    public static void setConsentData(String str, String str2) {
        if (!Utils.isEmpty(str)) {
            if (Utils.isEmpty(str2)) {
                consentData.remove(str);
            } else {
                consentData.put(str, str2);
            }
        }
    }

    public static void clearConsentData() {
        consentData.clear();
    }

    public static Map<String, String> getConsentDataMap() {
        return consentData;
    }

    public static void geoIpCheck(boolean z) {
        if (MMLog.isDebugEnabled()) {
            String str = TAG;
            MMLog.d(str, "Requesting geo ip check, async mode <" + z + ">");
        }
        if (z) {
            TaskFactory.createGeoIpCheckRequestTask().execute();
        } else {
            geoIpCheckInternal();
        }
    }

    private static void geoIpCheckInternal() {
        if (geoIpRequestInProgress.compareAndSet(false, true)) {
            HttpUtils.Response contentFromGetRequest = HttpUtils.getContentFromGetRequest(Handshake.getGeoIpCheckUrl());
            if (contentFromGetRequest.code == 200 && !TextUtils.isEmpty(contentFromGetRequest.content)) {
                if (MMLog.isDebugEnabled()) {
                    String str = TAG;
                    MMLog.d(str, "Geo ip response content:\n" + contentFromGetRequest.content);
                }
                try {
                    locationRequiresConsent = Boolean.valueOf(new JSONObject(contentFromGetRequest.content).getBoolean("result"));
                    if (MMLog.isDebugEnabled()) {
                        String str2 = TAG;
                        MMLog.d(str2, "Location requires consent: " + locationRequiresConsent);
                    }
                } catch (JSONException e) {
                    MMLog.e(TAG, "Unable to parse geo ip check response", e);
                }
            } else if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, "Geo ip request failed");
            }
            geoIpRequestInProgress.set(false);
            TaskFactory.createGeoIpCheckRequestTask().execute((long) Handshake.getGeoIpCheckTtl());
        } else if (MMLog.isDebugEnabled()) {
            MMLog.d(TAG, "Geo ip request already in progress");
        }
    }

    static boolean isRequestInProgress() {
        return geoIpRequestInProgress.get();
    }
}
