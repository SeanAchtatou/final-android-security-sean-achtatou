package com.millennialmedia.internal;

public class HandshakeConstants {
    static final int CLIENT_MEDIATION_TIMEOUT_FLOOR = 1000;
    static final String DEFAULT_GEO_IP_CHECK_URL = "https://service.cmp.oath.com/cmp/v0/location/eu";
    static final String DEFAULT_HANDSHAKE_BASE_URL = "https://ads.nexage.com";
    static final String DEFAULT_HANDSHAKE_JSON = "mmadsdk/default_handshake.json";
    static final int EXCHANGE_TIMEOUT_FLOOR = 1000;
    static final int GEO_IP_CHECK_TTL_CEILING = 604800000;
    static final int GEO_IP_CHECK_TTL_DEFAULT = 86400000;
    static final int GEO_IP_CHECK_TTL_FLOOR = 1800000;
    static final int HANDSHAKE_REQUEST_TIMEOUT = 15000;
    static final int HANDSHAKE_TTL_FLOOR = 60000;
    static final int INLINE_TIMEOUT_FLOOR = 3000;
    static final int INTERSTITIAL_TIMEOUT_FLOOR = 3000;
    static final int MAX_HANDSHAKE_ATTEMPTS = 10;
    static final int MIN_IMPRESSION_DURATION_CEILING = 60000;
    static final int MIN_IMPRESSION_DURATION_DEFAULT = 0;
    static final int MIN_IMPRESSION_DURATION_FLOOR = 0;
    static final int MIN_IMPRESSION_VIEWABILITY_PERCENT_CEILING = 100;
    static final int MIN_IMPRESSION_VIEWABILITY_PERCENT_DEFAULT = 0;
    static final int MIN_IMPRESSION_VIEWABILITY_PERCENT_FLOOR = 0;
    static final int MIN_INLINE_REFRESH_RATE_FLOOR = 10000;
    static final int NATIVE_TIMEOUT_FLOOR = 3000;
    static final int REPORTING_BATCH_FREQUENCY_FLOOR = 120000;
    static final int REPORTING_BATCH_SIZE_FLOOR = 1;
    static final String SERVER_ADAPTER_KEY_GREEN = "green";
    static final String SERVER_ADAPTER_KEY_ORANGE = "orange";
    static final int SERVER_TO_SERVER_TIMEOUT_FLOOR = 1000;
    static final int SUPER_AUCTION_CACHE_TIMEOUT_CEILING = 86400000;
    static final int SUPER_AUCTION_CACHE_TIMEOUT_DEFAULT = 600000;
    static final int SUPER_AUCTION_CACHE_TIMEOUT_FLOOR = 1000;
    static final int VAST_VIDEO_SKIP_OFFSET_FLOOR = 0;
    static final int VPAID_AD_UNIT_TIMEOUT_FLOOR = 1000;
    static final int VPAID_HTML_END_CARD_TIMEOUT_FLOOR = 0;
    static final int VPAID_MAX_BACK_BUTTON_DELAY_FLOOR = 0;
    static final int VPAID_SKIP_AD_TIMEOUT_FLOOR = 500;
    static final int VPAID_START_AD_TIMEOUT_FLOOR = 1000;
}
