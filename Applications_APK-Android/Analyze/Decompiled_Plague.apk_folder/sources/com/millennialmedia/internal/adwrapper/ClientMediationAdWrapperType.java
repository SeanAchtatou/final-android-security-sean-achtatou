package com.millennialmedia.internal.adwrapper;

import android.text.TextUtils;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.AdAdapter;
import com.millennialmedia.internal.adadapters.MediatedAdAdapter;
import java.security.InvalidParameterException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public class ClientMediationAdWrapperType implements AdWrapperType {
    /* access modifiers changed from: private */
    public static final String TAG = "ClientMediationAdWrapperType";

    public AdWrapper createAdWrapperFromJSON(JSONObject jSONObject, String str) throws JSONException {
        String string = jSONObject.getString("adnet");
        JSONObject jSONObject2 = jSONObject.getJSONObject(AdWrapperType.REQ_KEY);
        return new ClientMediationAdWrapper(jSONObject.getString(AdWrapperType.ITEM_KEY), string, jSONObject2.getString("site"), jSONObject2.getString("posId"));
    }

    public static class ClientMediationAdWrapper extends AdWrapper {
        final String networkId;
        final String siteId;
        final String spaceId;

        public ClientMediationAdWrapper(String str, String str2, String str3, String str4) {
            super(str);
            if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) {
                throw new InvalidParameterException("networkId, siteId and spaceId are required");
            }
            this.networkId = str2;
            this.siteId = str3;
            this.spaceId = str4;
        }

        public AdAdapter getAdAdapter(AdPlacement adPlacement, AdPlacementReporter.PlayListItemReporter playListItemReporter, AtomicInteger atomicInteger) {
            if (MMLog.isDebugEnabled()) {
                String access$000 = ClientMediationAdWrapperType.TAG;
                MMLog.d(access$000, "Processing client mediation playlist item ID: " + this.itemId);
            }
            MediatedAdAdapter mediatedAdapterInstance = AdAdapter.getMediatedAdapterInstance(adPlacement.getClass());
            if (mediatedAdapterInstance == null) {
                String access$0002 = ClientMediationAdWrapperType.TAG;
                MMLog.e(access$0002, "Unable to find ad adapter for network ID: " + this.networkId);
                return null;
            } else if (!(mediatedAdapterInstance instanceof AdAdapter)) {
                String access$0003 = ClientMediationAdWrapperType.TAG;
                MMLog.e(access$0003, "Unable to use ad adapter <" + mediatedAdapterInstance + "> for <" + this.networkId + ">, does not extend from AdAdapter");
                return null;
            } else {
                mediatedAdapterInstance.setMediationInfo(new MediatedAdAdapter.MediationInfo(this.networkId, this.siteId, this.spaceId));
                AdAdapter adAdapter = (AdAdapter) mediatedAdapterInstance;
                adAdapter.requestTimeout = Handshake.getClientMediationTimeout();
                return adAdapter;
            }
        }
    }
}
