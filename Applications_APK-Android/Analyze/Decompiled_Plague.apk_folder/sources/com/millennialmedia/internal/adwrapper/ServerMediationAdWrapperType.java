package com.millennialmedia.internal.adwrapper;

import android.text.TextUtils;
import com.millennialmedia.CreativeInfo;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.AdAdapter;
import com.millennialmedia.internal.utils.HttpUtils;
import com.tapjoy.TJAdUnitConstants;
import java.security.InvalidParameterException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerMediationAdWrapperType implements AdWrapperType {
    /* access modifiers changed from: private */
    public static final String TAG = "ServerMediationAdWrapperType";

    public AdWrapper createAdWrapperFromJSON(JSONObject jSONObject, String str) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject(AdWrapperType.REQ_KEY);
        ServerMediationAdWrapper serverMediationAdWrapper = new ServerMediationAdWrapper(jSONObject.getString(AdWrapperType.ITEM_KEY), jSONObject2.getString(TJAdUnitConstants.String.URL), jSONObject.optBoolean(AdWrapperType.ENHANCED_AD_CONTROL_KEY, false));
        serverMediationAdWrapper.validateRegex = jSONObject2.optString("validRegex", null);
        serverMediationAdWrapper.postBody = jSONObject2.optString("postBody", null);
        serverMediationAdWrapper.postContentType = jSONObject2.optString("postType", null);
        serverMediationAdWrapper.cridHeaderField = jSONObject.optString("cridHeaderField", null);
        serverMediationAdWrapper.adnet = jSONObject.optString("adnet", null);
        return serverMediationAdWrapper;
    }

    public static class ServerMediationAdWrapper extends AdWrapper {
        public String adnet;
        public String cridHeaderField;
        public String postBody;
        public String postContentType;
        final String url;
        public String validateRegex;

        public ServerMediationAdWrapper(String str, String str2) {
            this(str, str2, false);
        }

        public ServerMediationAdWrapper(String str, String str2, boolean z) {
            super(str, z);
            if (TextUtils.isEmpty(str2)) {
                throw new InvalidParameterException("url is required");
            }
            this.url = str2;
        }

        public AdAdapter getAdAdapter(AdPlacement adPlacement, AdPlacementReporter.PlayListItemReporter playListItemReporter, AtomicInteger atomicInteger) {
            HttpUtils.Response response;
            if (MMLog.isDebugEnabled()) {
                String access$000 = ServerMediationAdWrapperType.TAG;
                MMLog.d(access$000, "Processing server mediation playlist item ID: " + this.itemId);
            }
            int serverToServerTimeout = Handshake.getServerToServerTimeout();
            if (!TextUtils.isEmpty(this.postBody)) {
                response = HttpUtils.getContentFromPostRequest(this.url, this.postBody, this.postContentType, serverToServerTimeout);
            } else {
                response = HttpUtils.getContentFromPostRequest(this.url, serverToServerTimeout);
            }
            if (response.code != 200 || TextUtils.isEmpty(response.content)) {
                String access$0002 = ServerMediationAdWrapperType.TAG;
                MMLog.e(access$0002, "Unable to retrieve content for server mediation playlist item, placement ID <" + adPlacement.placementId + ">");
                atomicInteger.set(setErrorStatusFromResponseCode(response));
                return null;
            }
            if (!TextUtils.isEmpty(this.validateRegex)) {
                String str = response.content;
                if (str.matches("(?s)" + this.validateRegex)) {
                    String access$0003 = ServerMediationAdWrapperType.TAG;
                    MMLog.e(access$0003, "Unable to validate content for server mediation playlist item due to \"no ad\" response for placement ID <" + adPlacement.placementId + "> and content <" + response.content + ">");
                    atomicInteger.set(-1);
                    return null;
                }
            }
            AdAdapter adAdapterForContent = getAdAdapterForContent(adPlacement, response.content);
            if (adAdapterForContent == null) {
                MMLog.e(ServerMediationAdWrapperType.TAG, String.format("Unable to find adapter for server mediation playlist item, placement ID <%s> and content <%s>", adPlacement.placementId, response.content));
                return null;
            }
            adAdapterForContent.setAdMetadata(response.adMetadata);
            if (response.adMetadata != null) {
                adAdapterForContent.setCreativeInfo(new CreativeInfo((String) response.adMetadata.get(this.cridHeaderField), this.adnet));
            }
            return adAdapterForContent;
        }
    }
}
