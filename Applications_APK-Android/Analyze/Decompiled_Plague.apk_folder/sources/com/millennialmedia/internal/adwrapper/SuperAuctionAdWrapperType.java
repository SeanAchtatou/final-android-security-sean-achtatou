package com.millennialmedia.internal.adwrapper;

import android.text.TextUtils;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.PlayList;
import com.millennialmedia.internal.adadapters.AdAdapter;
import com.millennialmedia.internal.utils.HttpUtils;
import com.millennialmedia.internal.utils.JSONUtils;
import com.millennialmedia.internal.utils.ThreadUtils;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SuperAuctionAdWrapperType implements AdWrapperType {
    /* access modifiers changed from: private */
    public static final String TAG = "SuperAuctionAdWrapperType";
    private static final Map<String, AdWrapperType> registeredDemandSourceTypes = new HashMap();

    static {
        try {
            registerPackagedDemandSourceTypes();
        } catch (Exception e) {
            MMLog.e(TAG, "Unable to register packaged demand source types", e);
        }
    }

    private static void registerPackagedDemandSourceTypes() {
        registerDemandSourceType("ad_content", new ContentDemandSourceAdWrapperType());
        registerDemandSourceType("server_demand", new ServerMediationAdWrapperType());
        registerDemandSourceType("client_demand", new ClientMediationAdWrapperType());
    }

    private static void registerDemandSourceType(String str, AdWrapperType adWrapperType) {
        if (adWrapperType == null) {
            throw new IllegalArgumentException("AdWrapperType cannot be null");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("DemandSourceTypeId cannot be null");
        } else if (registeredDemandSourceTypes.containsKey(str)) {
            String str2 = TAG;
            MMLog.w(str2, "DemandSourceTypeId <" + str + "> already registered");
        } else {
            if (MMLog.isDebugEnabled()) {
                String str3 = TAG;
                MMLog.d(str3, "Registering DemandSourceTypeId <" + str + ">");
            }
            registeredDemandSourceTypes.put(str, adWrapperType);
        }
    }

    public AdWrapper createAdWrapperFromJSON(JSONObject jSONObject, String str) throws JSONException {
        return new SuperAuctionAdWrapper(jSONObject.getString(AdWrapperType.ITEM_KEY), str, getDemandSources(jSONObject), getBiddersArray(jSONObject));
    }

    /* access modifiers changed from: private */
    public static AdWrapperType getDemandSourceType(String str) throws Exception {
        AdWrapperType adWrapperType = registeredDemandSourceTypes.get(str);
        if (adWrapperType != null) {
            return adWrapperType;
        }
        throw new Exception("Unable to find specified DemandSourceType for type ID " + str);
    }

    public static class SuperAuctionAdWrapper extends AdWrapper {
        private String bidPrice;
        private JSONObject bidderItem;
        private JSONArray bidderItems;
        private JSONArray demandSources;
        private final String playlistResponseId;
        private String winUrl;

        public SuperAuctionAdWrapper(String str, String str2, JSONArray jSONArray, JSONArray jSONArray2) {
            super(str);
            if (TextUtils.isEmpty(str2)) {
                throw new InvalidParameterException("playlistResponseId is required");
            }
            this.playlistResponseId = str2;
            this.demandSources = jSONArray;
            this.bidderItems = jSONArray2;
            this.bidderItem = SuperAuctionAdWrapperType.getWinningBidderObject(jSONArray2, jSONArray);
            if (this.bidderItem != null) {
                this.bidPrice = this.bidderItem.optString("bidPrice");
                this.winUrl = this.bidderItem.optString("winUrl");
            }
        }

        public AdAdapter getAdAdapter(AdPlacement adPlacement, AdPlacementReporter.PlayListItemReporter playListItemReporter, AtomicInteger atomicInteger) {
            if (MMLog.isDebugEnabled()) {
                String access$100 = SuperAuctionAdWrapperType.TAG;
                MMLog.d(access$100, "Processing item with ID <" + this.itemId + ">");
            }
            fireWinUrl(this.winUrl);
            if (this.bidderItem != null) {
                AdPlacementReporter.reportBidItem(this.bidderItem, playListItemReporter, 1);
            }
            return getAdAdapterForSuperAuctionRequest(adPlacement, playListItemReporter, atomicInteger);
        }

        public String getBidPrice() {
            return this.bidPrice;
        }

        /* access modifiers changed from: private */
        public void reportBidderItems(AdPlacementReporter.PlayListItemReporter playListItemReporter) {
            if (this.bidderItems != null && this.bidderItems.length() >= 1 && this.demandSources != null && this.demandSources.length() > 0) {
                for (int i = 0; i < this.bidderItems.length(); i++) {
                    try {
                        JSONObject jSONObject = this.bidderItems.getJSONObject(i);
                        if (jSONObject.getString("type").equals("server_bid") && !TextUtils.isEmpty(jSONObject.optString("bidPrice"))) {
                            AdPlacementReporter.reportBidItem(jSONObject, playListItemReporter, 1);
                            return;
                        }
                    } catch (JSONException e) {
                        MMLog.e(SuperAuctionAdWrapperType.TAG, "Error reporting bidder item.", e);
                    }
                }
            }
        }

        private AdAdapter getAdAdapterForSuperAuctionRequest(AdPlacement adPlacement, AdPlacementReporter.PlayListItemReporter playListItemReporter, AtomicInteger atomicInteger) {
            AdAdapter processDemandSources = processDemandSources(adPlacement, playListItemReporter, atomicInteger);
            if (playListItemReporter != null) {
                if (processDemandSources == null) {
                    playListItemReporter.getSuperAuction().status = AdPlacementReporter.PLAYLIST_LOADED_FROM_CACHE_DEMAND_ITEM_FAILED;
                } else {
                    playListItemReporter.getSuperAuction().status = AdPlacementReporter.PLAYLIST_LOADED_FROM_CACHE_DEMAND_ITEM_SUCCEEDED;
                }
            }
            return processDemandSources;
        }

        private void fireWinUrl(final String str) {
            if (!TextUtils.isEmpty(str)) {
                ThreadUtils.runOnWorkerThread(new Runnable() {
                    public void run() {
                        if (MMLog.isDebugEnabled()) {
                            String access$100 = SuperAuctionAdWrapperType.TAG;
                            MMLog.d(access$100, "Firing super auction win url = " + str);
                        }
                        HttpUtils.getContentFromGetRequest(str);
                    }
                });
            }
        }

        private AdAdapter processDemandSources(AdPlacement adPlacement, AdPlacementReporter.PlayListItemReporter playListItemReporter, AtomicInteger atomicInteger) {
            AdAdapter adAdapter = null;
            if (this.demandSources == null) {
                return null;
            }
            for (int i = 0; i < this.demandSources.length(); i++) {
                atomicInteger.set(-3);
                try {
                    JSONObject jSONObject = this.demandSources.getJSONObject(i);
                    String string = jSONObject.getString("type");
                    AdWrapperType access$200 = SuperAuctionAdWrapperType.getDemandSourceType(string);
                    if (access$200 == null) {
                        String access$100 = SuperAuctionAdWrapperType.TAG;
                        MMLog.e(access$100, "Unable to process demand source type <" + string + ">");
                    } else {
                        AdWrapper createAdWrapperFromJSON = access$200.createAdWrapperFromJSON(jSONObject, this.playlistResponseId);
                        AdPlacementReporter.DemandSource reportDemandSource = AdPlacementReporter.reportDemandSource(playListItemReporter, string, createAdWrapperFromJSON);
                        AdAdapter adAdapter2 = createAdWrapperFromJSON.getAdAdapter(adPlacement, playListItemReporter, atomicInteger);
                        if (adAdapter2 == null) {
                            try {
                                reportDemandSource.status = atomicInteger.get();
                                String access$1002 = SuperAuctionAdWrapperType.TAG;
                                MMLog.e(access$1002, "No adapter found for demand source <" + jSONObject + ">");
                                adAdapter = adAdapter2;
                            } catch (Exception e) {
                                e = e;
                                adAdapter = adAdapter2;
                                MMLog.e(SuperAuctionAdWrapperType.TAG, "Error processing super auction demand source", e);
                            }
                        } else {
                            reportDemandSource.status = 1;
                            if (playListItemReporter != null) {
                                playListItemReporter.getSuperAuction().itemId = createAdWrapperFromJSON.itemId;
                                if (jSONObject.has("buyer")) {
                                    playListItemReporter.buyer = jSONObject.getString("buyer");
                                }
                                if (jSONObject.has("price")) {
                                    playListItemReporter.pru = jSONObject.getString("price");
                                }
                            }
                            return adAdapter2;
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    MMLog.e(SuperAuctionAdWrapperType.TAG, "Error processing super auction demand source", e);
                }
            }
            return adAdapter;
        }
    }

    public static void reportBidFailed(PlayList playList, String str, int i) {
        AdWrapper item = playList.getItem(0);
        if (item != null) {
            reportBidFailed(playList, (SuperAuctionAdWrapper) item, str, i);
        }
    }

    public static void reportBidFailed(PlayList playList, SuperAuctionAdWrapper superAuctionAdWrapper, String str, int i) {
        AdPlacementReporter playListReporter = AdPlacementReporter.getPlayListReporter(playList, str);
        AdPlacementReporter.PlayListItemReporter playListItemReporter = AdPlacementReporter.getPlayListItemReporter(playListReporter);
        if (playListItemReporter != null) {
            playListItemReporter.getSuperAuction().status = i;
            if (superAuctionAdWrapper != null) {
                playListItemReporter.itemId = superAuctionAdWrapper.itemId;
                superAuctionAdWrapper.reportBidderItems(playListItemReporter);
            }
            AdPlacementReporter.reportPlayListItem(playListReporter, playListItemReporter);
            AdPlacementReporter.reportPlayList(playListReporter);
        }
    }

    /* access modifiers changed from: private */
    public static JSONObject getWinningBidderObject(JSONArray jSONArray, JSONArray jSONArray2) {
        if (!JSONUtils.isEmpty(jSONArray)) {
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    if (jSONObject.getString("type").equals("server_bid") && jSONArray2 != null && jSONArray2.length() > 0) {
                        return jSONObject;
                    }
                } catch (JSONException e) {
                    MMLog.e(TAG, "Error processing bidder item.", e);
                }
            }
            return null;
        }
        MMLog.e(TAG, "Super auction bidders array missing.");
        return null;
    }

    private static JSONArray getBiddersArray(JSONObject jSONObject) {
        try {
            return jSONObject.getJSONArray("bidders");
        } catch (JSONException e) {
            MMLog.w(TAG, "Super auction playlist item does not contain a bidders array", e);
            return null;
        }
    }

    private static JSONArray getDemandSources(JSONObject jSONObject) {
        try {
            return jSONObject.getJSONArray("demandSources");
        } catch (JSONException e) {
            MMLog.w(TAG, "Super auction playlist item does not contain a demand_sources array", e);
            return null;
        }
    }
}
