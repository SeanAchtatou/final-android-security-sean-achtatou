package com.millennialmedia.internal.adwrapper;

import android.text.TextUtils;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdMetadata;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.adadapters.AdAdapter;
import com.millennialmedia.internal.adcontrollers.AdController;
import com.millennialmedia.internal.utils.HttpUtils;
import java.security.InvalidParameterException;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AdWrapper {
    private static final String TAG = "AdWrapper";
    public AdMetadata adMetadata;
    public final String itemId;

    public abstract AdAdapter getAdAdapter(AdPlacement adPlacement, AdPlacementReporter.PlayListItemReporter playListItemReporter, AtomicInteger atomicInteger);

    AdWrapper(String str) {
        this(str, false);
    }

    AdWrapper(String str, boolean z) {
        if (TextUtils.isEmpty(str)) {
            throw new InvalidParameterException("itemId is required");
        }
        this.itemId = str;
        this.adMetadata = new AdMetadata();
        this.adMetadata.put(AdMetadata.ENHANCED_AD_CONTROL_ENABLED, String.valueOf(z));
    }

    public boolean isEnhancedAdControlEnabled() {
        if (this.adMetadata.containsKey(AdMetadata.ENHANCED_AD_CONTROL_ENABLED)) {
            return Boolean.parseBoolean((String) this.adMetadata.get(AdMetadata.ENHANCED_AD_CONTROL_ENABLED));
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public AdAdapter getAdAdapterForContent(AdPlacement adPlacement, String str) {
        if (MMLog.isDebugEnabled()) {
            String str2 = TAG;
            MMLog.d(str2, "Attempting to get ad adapter for ad placement ID: " + adPlacement.placementId);
        }
        if (str == null) {
            MMLog.e(TAG, "Unable to find ad adapter, ad content is null");
            return null;
        }
        Class<?> controllerClassForContent = AdController.getControllerClassForContent(str);
        if (controllerClassForContent == null) {
            String str3 = TAG;
            MMLog.e(str3, "Unable to determine ad controller type for specified ad content <" + str + ">");
            return null;
        }
        AdAdapter adapterInstance = AdAdapter.getAdapterInstance(adPlacement.getClass(), controllerClassForContent);
        if (adapterInstance != null) {
            if (MMLog.isDebugEnabled()) {
                MMLog.d(TAG, String.format("Found ad adapter <%s> for placement ID <%s>", adapterInstance.toString(), adPlacement.placementId));
            }
            adapterInstance.setContent(str);
        }
        return adapterInstance;
    }

    /* access modifiers changed from: package-private */
    public int setErrorStatusFromResponseCode(HttpUtils.Response response) {
        int i = response.code;
        return (i == 408 || i == 504) ? -2 : -1;
    }
}
