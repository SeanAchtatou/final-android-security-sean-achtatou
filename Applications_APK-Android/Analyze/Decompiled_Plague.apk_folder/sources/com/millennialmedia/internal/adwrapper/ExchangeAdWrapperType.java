package com.millennialmedia.internal.adwrapper;

import android.text.TextUtils;
import com.millennialmedia.CreativeInfo;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.Handshake;
import com.millennialmedia.internal.adadapters.AdAdapter;
import com.millennialmedia.internal.utils.HttpUtils;
import com.tapjoy.TJAdUnitConstants;
import java.security.InvalidParameterException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public class ExchangeAdWrapperType implements AdWrapperType {
    /* access modifiers changed from: private */
    public static final String TAG = "ExchangeAdWrapperType";

    public AdWrapper createAdWrapperFromJSON(JSONObject jSONObject, String str) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject(AdWrapperType.REQ_KEY);
        ExchangeAdWrapper exchangeAdWrapper = new ExchangeAdWrapper(jSONObject.getString(AdWrapperType.ITEM_KEY), jSONObject2.getString(TJAdUnitConstants.String.URL), jSONObject.optBoolean(AdWrapperType.ENHANCED_AD_CONTROL_KEY, false));
        exchangeAdWrapper.postBody = jSONObject2.optString("postBody", null);
        exchangeAdWrapper.postContentType = jSONObject2.optString("postType", null);
        return exchangeAdWrapper;
    }

    public static class ExchangeAdWrapper extends AdWrapper {
        public String postBody;
        public String postContentType;
        final String url;

        public ExchangeAdWrapper(String str, String str2) {
            this(str, str2, false);
        }

        public ExchangeAdWrapper(String str, String str2, boolean z) {
            super(str, z);
            if (TextUtils.isEmpty(str2)) {
                throw new InvalidParameterException("url is required");
            }
            this.url = str2;
        }

        public AdAdapter getAdAdapter(AdPlacement adPlacement, AdPlacementReporter.PlayListItemReporter playListItemReporter, AtomicInteger atomicInteger) {
            HttpUtils.Response response;
            AdAdapter adAdapter;
            if (MMLog.isDebugEnabled()) {
                MMLog.d(ExchangeAdWrapperType.TAG, "Processing exchange mediation playlist item ID: " + this.itemId);
            }
            int exchangeTimeout = Handshake.getExchangeTimeout();
            if (!TextUtils.isEmpty(this.postBody)) {
                response = HttpUtils.getContentFromPostRequest(this.url, this.postBody, this.postContentType, exchangeTimeout);
            } else {
                response = HttpUtils.getContentFromPostRequest(this.url, exchangeTimeout);
            }
            if (response.code != 200 || TextUtils.isEmpty(response.content)) {
                MMLog.e(ExchangeAdWrapperType.TAG, "Unable to retrieve content for exchange mediation playlist item, placement ID <" + adPlacement.placementId + ">");
                atomicInteger.set(setErrorStatusFromResponseCode(response));
                return null;
            }
            try {
                JSONObject jSONObject = new JSONObject(response.content);
                String string = jSONObject.getString("ad");
                String optString = jSONObject.optString("ad_buyer", null);
                String optString2 = jSONObject.optString("ad_pru", null);
                adAdapter = getAdAdapterForContent(adPlacement, string);
                if (adAdapter == null) {
                    try {
                        MMLog.e(ExchangeAdWrapperType.TAG, String.format("Unable to find adapter for exchange mediation playlist item, placement ID <%s> and content <%s>", adPlacement.placementId, string));
                        return null;
                    } catch (JSONException unused) {
                        MMLog.e(ExchangeAdWrapperType.TAG, "Error occurred when trying to parse ad content from exchange response");
                        return adAdapter;
                    }
                } else {
                    if (playListItemReporter != null) {
                        playListItemReporter.buyer = optString;
                        playListItemReporter.pru = optString2;
                    }
                    adAdapter.setAdMetadata(response.adMetadata);
                    adAdapter.setCreativeInfo(new CreativeInfo(jSONObject.optString("ad_crid", null), jSONObject.optString("ad_bidder_id", null)));
                    return adAdapter;
                }
            } catch (JSONException unused2) {
                adAdapter = null;
                MMLog.e(ExchangeAdWrapperType.TAG, "Error occurred when trying to parse ad content from exchange response");
                return adAdapter;
            }
        }
    }
}
