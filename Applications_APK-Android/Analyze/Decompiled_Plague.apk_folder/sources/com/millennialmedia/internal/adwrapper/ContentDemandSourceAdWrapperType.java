package com.millennialmedia.internal.adwrapper;

import com.millennialmedia.internal.adwrapper.ContentAdWrapperType;
import org.json.JSONException;
import org.json.JSONObject;

public class ContentDemandSourceAdWrapperType implements AdWrapperType {
    public AdWrapper createAdWrapperFromJSON(JSONObject jSONObject, String str) throws JSONException {
        return new ContentAdWrapperType.ContentAdWrapper(jSONObject.getString(AdWrapperType.ITEM_KEY), jSONObject.getString("adContent"), null, jSONObject.optBoolean(AdWrapperType.ENHANCED_AD_CONTROL_KEY, false));
    }
}
