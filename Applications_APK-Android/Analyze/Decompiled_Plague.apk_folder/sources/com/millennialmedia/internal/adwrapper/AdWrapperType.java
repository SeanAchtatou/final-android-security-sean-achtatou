package com.millennialmedia.internal.adwrapper;

import org.json.JSONException;
import org.json.JSONObject;

public interface AdWrapperType {
    public static final String ENHANCED_AD_CONTROL_KEY = "enableEnhancedAdControl";
    public static final String ITEM_KEY = "item";
    public static final String REQ_KEY = "req";

    AdWrapper createAdWrapperFromJSON(JSONObject jSONObject, String str) throws JSONException;
}
