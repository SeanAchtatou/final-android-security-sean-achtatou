package com.millennialmedia.internal.adwrapper;

import android.text.TextUtils;
import com.millennialmedia.CreativeInfo;
import com.millennialmedia.MMLog;
import com.millennialmedia.internal.AdMetadata;
import com.millennialmedia.internal.AdPlacement;
import com.millennialmedia.internal.AdPlacementReporter;
import com.millennialmedia.internal.adadapters.AdAdapter;
import java.security.InvalidParameterException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public class ContentAdWrapperType implements AdWrapperType {
    /* access modifiers changed from: private */
    public static final String TAG = "ContentAdWrapperType";

    public AdWrapper createAdWrapperFromJSON(JSONObject jSONObject, String str) throws JSONException {
        ContentAdWrapper contentAdWrapper = new ContentAdWrapper(jSONObject.getString(AdWrapperType.ITEM_KEY), jSONObject.getString("value"), null, jSONObject.optBoolean(AdWrapperType.ENHANCED_AD_CONTROL_KEY, false));
        contentAdWrapper.creativeId = jSONObject.optString("creativeid", null);
        contentAdWrapper.adnet = jSONObject.optString("adnet", null);
        return contentAdWrapper;
    }

    public static class ContentAdWrapper extends AdWrapper {
        public String adnet;
        public String creativeId;
        final String value;

        public ContentAdWrapper(String str, String str2, AdMetadata adMetadata) {
            this(str, str2, adMetadata, false);
        }

        public ContentAdWrapper(String str, String str2, AdMetadata adMetadata, boolean z) {
            super(str, z);
            if (TextUtils.isEmpty(str2)) {
                throw new InvalidParameterException("value is required");
            }
            this.value = str2;
            this.adMetadata.addAll(adMetadata);
        }

        public AdAdapter getAdAdapter(AdPlacement adPlacement, AdPlacementReporter.PlayListItemReporter playListItemReporter, AtomicInteger atomicInteger) {
            if (MMLog.isDebugEnabled()) {
                String access$000 = ContentAdWrapperType.TAG;
                MMLog.d(access$000, "Processing ad content playlist item ID: " + this.itemId);
            }
            AdAdapter adAdapterForContent = getAdAdapterForContent(adPlacement, this.value);
            if (adAdapterForContent == null) {
                MMLog.e(ContentAdWrapperType.TAG, String.format("Unable to find adapter for ad content playlist item, placement ID <%s> and content <%s>", adPlacement.placementId, this.value));
                return null;
            }
            adAdapterForContent.setCreativeInfo(new CreativeInfo(this.creativeId, this.adnet));
            adAdapterForContent.setAdMetadata(this.adMetadata);
            return adAdapterForContent;
        }
    }
}
