package com.millennialmedia;

public class MMInitializationException extends MMException {
    public MMInitializationException(String str) {
        super(str);
    }
}
