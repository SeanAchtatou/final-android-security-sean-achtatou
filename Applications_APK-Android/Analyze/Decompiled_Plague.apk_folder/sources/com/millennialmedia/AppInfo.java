package com.millennialmedia;

import com.millennialmedia.internal.utils.Utils;

public class AppInfo {
    private Boolean coppa;
    private String mediator;
    private boolean shareAdvertiserIdEnabled;
    private boolean shareApplicationIdEnabled;
    private String siteId;

    public AppInfo setSiteId(String str) {
        this.siteId = Utils.trimStringNotEmpty(str);
        return this;
    }

    public String getSiteId() {
        return this.siteId;
    }

    public AppInfo setMediator(String str) {
        this.mediator = str;
        return this;
    }

    public String getMediator() {
        return this.mediator;
    }

    public AppInfo setCoppa(boolean z) {
        this.coppa = Boolean.valueOf(z);
        return this;
    }

    public Boolean getCoppa() {
        return this.coppa;
    }

    public boolean isShareAdvertiserIdEnabled() {
        return this.shareAdvertiserIdEnabled;
    }

    public void setShareAdvertiserIdEnabled(boolean z) {
        this.shareAdvertiserIdEnabled = z;
    }

    public boolean isShareApplicationIdEnabled() {
        return this.shareApplicationIdEnabled;
    }

    public void setShareApplicationIdEnabled(boolean z) {
        this.shareApplicationIdEnabled = z;
    }
}
