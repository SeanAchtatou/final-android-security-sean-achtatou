package com.miniclip.newsfeed;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.miniclip.framework.Miniclip;

public class Newsfeed {
    private static Newsfeed instance = new Newsfeed();
    protected NewsfeedGUI gui = new NewsfeedGUI();

    public static native void dismissBoard();

    public static native void showBoard();

    public native void newsfeedBoardDidAppear();

    public native void newsfeedBoardDidDisappear();

    public native void newsfeedBoardWillAppear();

    public native void newsfeedBoardWillDisappear();

    public native void newsfeedMessageDisplayed(int i);

    public native void newsfeedMessageGetItPressed(int i);

    public native void newsfeedMessageLoaded(int i);

    public static Newsfeed instance() {
        return instance;
    }

    static void openURL(String str) {
        Activity activity = Miniclip.getActivity();
        if (activity == null) {
            Log.e("Newsfeed", "Cannot openURL: Newsfeed could not find the Miniclip Activity.");
            return;
        }
        try {
            activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Log.e("Newsfeed", "Failed to open message link.");
        }
    }
}
