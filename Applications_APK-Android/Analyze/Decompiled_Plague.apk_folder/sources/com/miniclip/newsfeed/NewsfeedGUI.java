package com.miniclip.newsfeed;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextPaint;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.millennialmedia.internal.utils.EnvironmentUtils;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.Miniclip;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.mcservices.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NewsfeedGUI extends AbstractActivityListener {
    /* access modifiers changed from: private */
    public static int bottomButtonHighlightColor = Color.argb(190, 215, 190, 100);
    /* access modifiers changed from: private */
    public static int closeButtonHighlightColor = Color.argb(220, 230, 200, 160);
    protected Activity activity;
    private float displayDensity;
    private int displayHeight = 1;
    private int displayWidth = 1;
    private View layout;
    protected HashMap<Integer, NewsfeedMessageView> loadedMessages = new HashMap<>();
    private int messageIndex = 0;
    PopupWindow popup;
    private Point popupDimensions;
    /* access modifiers changed from: private */
    public boolean showingNews = false;
    protected List<Integer> sortedMessages = new ArrayList();
    PopupWindow touchesBlockerPopup;

    public enum NesfeedImageOrientation {
        PORTRAIT,
        LANDSCAPE
    }

    public NewsfeedGUI() {
        Miniclip.addListener(this);
    }

    /* access modifiers changed from: package-private */
    public boolean showBoardInternal(int[] iArr) {
        if (iArr.length < 1) {
            Log.w("NewsfeedGUI", "Show Board called for empty list of messages.");
            return false;
        }
        this.activity = Miniclip.getActivity();
        if (this.activity == null) {
            Log.e("NewsfeedGUI", "Cannot show board: Newsfeed could not find the Miniclip Activity.");
            return false;
        }
        this.messageIndex = 0;
        this.sortedMessages.clear();
        for (int i : iArr) {
            if (!this.loadedMessages.containsKey(Integer.valueOf(i))) {
                Log.e("NewsfeedGUI", "Message " + i + " requested for show board was not found!!!");
            } else {
                if (this.activity.getResources().getConfiguration().orientation == 1) {
                    if (this.loadedMessages.get(Integer.valueOf(i)).messageBitmapPortrait == null) {
                    }
                } else if (this.loadedMessages.get(Integer.valueOf(i)).messageBitmapLandscape == null) {
                }
                this.sortedMessages.add(Integer.valueOf(i));
            }
        }
        if (this.sortedMessages.isEmpty()) {
            Log.e("NewsfeedGUI", "Messages requested for show board not found!!!");
            return false;
        } else if (this.loadedMessages.get(this.sortedMessages.get(0)).messageBitmapLandscape == null && this.loadedMessages.get(this.sortedMessages.get(0)).messageBitmapPortrait == null) {
            Log.e("NewsfeedGUI", "Cannot show board: Images not loaded yet!");
            return false;
        } else {
            Display defaultDisplay = this.activity.getWindowManager().getDefaultDisplay();
            this.displayWidth = defaultDisplay.getWidth();
            this.displayHeight = defaultDisplay.getHeight();
            this.displayDensity = this.activity.getResources().getDisplayMetrics().density;
            LayoutInflater layoutInflater = (LayoutInflater) this.activity.getSystemService("layout_inflater");
            if (this.touchesBlockerPopup == null) {
                View inflate = layoutInflater.inflate(R.layout.nf_back, (ViewGroup) null);
                this.touchesBlockerPopup = new PopupWindow(this.activity);
                this.touchesBlockerPopup.setContentView(inflate);
                this.touchesBlockerPopup.setWidth(this.displayWidth * 2);
                this.touchesBlockerPopup.setHeight(this.displayHeight * 2);
                this.touchesBlockerPopup.setFocusable(false);
                this.touchesBlockerPopup.setBackgroundDrawable(new ColorDrawable());
                this.touchesBlockerPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    public void onDismiss() {
                        NewsfeedGUI.this.touchesBlockerPopup = null;
                    }
                });
            }
            Point calculatePopupDimensions = calculatePopupDimensions();
            if (this.popup == null) {
                this.layout = layoutInflater.inflate(R.layout.newsfeed0, (ViewGroup) null);
                if (this.layout == null) {
                    Log.e("NewsfeedGUI", "Failed to inflate layout!");
                    return false;
                }
                setupCloseButton(this.layout);
                setupPreviousButton(this.layout);
                setupGetItNowButton(this.layout);
                setupNextButton(this.layout);
                setupImageButton(this.layout);
            }
            if (this.popup == null || calculatePopupDimensions != this.popupDimensions) {
                this.popupDimensions = calculatePopupDimensions;
                setupPopupWindow(this.activity, this.layout, this.popupDimensions.x, this.popupDimensions.y);
            }
            try {
                this.touchesBlockerPopup.showAtLocation(this.layout, 17, 0, 0);
                this.popup.showAtLocation(this.layout, 17, 0, 0);
                Newsfeed.instance().newsfeedBoardWillAppear();
                Newsfeed.instance().newsfeedBoardDidAppear();
                adjustActionButtonToText(this.layout, longestActionButtonTextString(iArr));
                displayMessage(this.layout);
                this.showingNews = true;
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                this.showingNews = false;
                return false;
            }
        }
    }

    private Point calculatePopupDimensions() {
        int i;
        int i2;
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float dimension = (this.activity.getResources().getDimension(R.dimen.nf_bg_component_size) * 2.0f) + (this.activity.getResources().getDimension(R.dimen.nf_bg_border_component_size) * 2.0f) + this.activity.getResources().getDimension(R.dimen.nf_bg_margin_left) + this.activity.getResources().getDimension(R.dimen.nf_bg_margin_right);
        float dimension2 = (((((this.activity.getResources().getDimension(R.dimen.nf_bg_margin_top) + (2.0f * this.activity.getResources().getDimension(R.dimen.nf_bg_border_component_size))) + this.activity.getResources().getDimension(R.dimen.nf_bg_component_size)) + this.activity.getResources().getDimension(R.dimen.nf_bg_border_bottom_component_height)) + this.activity.getResources().getDimension(R.dimen.nf_height_adjustment)) - this.activity.getResources().getDimension(R.dimen.nf_bottom_buttons_margin_bottom)) + this.activity.getResources().getDimension(R.dimen.nf_bg_margin_bottom);
        int i3 = 0;
        if (this.activity.getResources().getConfiguration().orientation == 1) {
            i2 = 1;
            i = 1;
            while (i2 == 1) {
                NewsfeedMessageView newsfeedMessageView = this.loadedMessages.get(this.sortedMessages.get(i3));
                if (newsfeedMessageView.messageBitmapPortrait == null) {
                    i3++;
                } else {
                    i2 = newsfeedMessageView.messageBitmapPortrait.getWidth();
                    i = newsfeedMessageView.messageBitmapPortrait.getHeight();
                }
            }
        } else {
            i2 = 1;
            i = 1;
            while (i2 == 1) {
                NewsfeedMessageView newsfeedMessageView2 = this.loadedMessages.get(this.sortedMessages.get(i3));
                if (newsfeedMessageView2.messageBitmapLandscape == null) {
                    i3++;
                } else {
                    i2 = newsfeedMessageView2.messageBitmapLandscape.getWidth();
                    i = newsfeedMessageView2.messageBitmapLandscape.getHeight();
                }
            }
        }
        float f6 = (float) i2;
        float f7 = (float) i;
        if (f6 / f7 > 1.0f) {
            f = ((float) this.displayWidth) / f6;
        } else {
            f = ((float) this.displayHeight) / f7;
        }
        int round = Math.round(((float) this.displayWidth) * 0.94f);
        int round2 = Math.round(((float) this.displayHeight) * 0.94f);
        float f8 = (f6 * f) + dimension;
        float f9 = (f7 * f) + dimension2;
        if (f8 / f9 > 1.0f) {
            f3 = (float) round;
            if (f8 > f3) {
                f2 = (f3 / f8) * f9;
            } else {
                f3 = f8;
                f2 = f9;
            }
            float f10 = (float) round2;
            if (f2 > f10) {
                f3 = (f10 / f9) * f8;
                f2 = f10;
            }
        } else {
            float f11 = (float) round2;
            if (f9 > f11) {
                f5 = f11;
                f4 = (f11 / f9) * f8;
            } else {
                f4 = f8;
                f5 = f9;
            }
            float f12 = (float) round;
            if (f3 > f12) {
                f2 = f9 * (f12 / f8);
                f3 = f12;
            }
        }
        return new Point((int) f3, (int) f2);
    }

    /* access modifiers changed from: package-private */
    public void setupPopupWindow(Context context, View view, int i, int i2) {
        this.popup = new PopupWindow(context);
        this.popup.setContentView(view);
        this.popup.setWidth(i);
        this.popup.setHeight(i2);
        this.popup.setFocusable(true);
        this.popup.setBackgroundDrawable(new ColorDrawable());
        this.popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            public void onDismiss() {
                Newsfeed.instance().newsfeedBoardWillDisappear();
                boolean unused = NewsfeedGUI.this.showingNews = false;
                Newsfeed.instance().newsfeedBoardDidDisappear();
                NewsfeedGUI.this.popup = null;
                NewsfeedGUI.this.touchesBlockerPopup.dismiss();
            }
        });
        this.popup.setTouchInterceptor(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();
                if ((motionEvent.getAction() != 0 || (x >= 0 && x < view.getWidth() && y >= 0 && y < view.getHeight())) && motionEvent.getAction() != 4) {
                    return view.onTouchEvent(motionEvent);
                }
                return true;
            }
        });
        if (Build.VERSION.SDK_INT >= 14) {
            int i3 = 2;
            if (Build.VERSION.SDK_INT >= 16) {
                i3 = 262;
                if (Build.VERSION.SDK_INT >= 19) {
                    i3 = 4358;
                }
            }
            this.popup.getContentView().setSystemUiVisibility(i3);
        }
    }

    /* access modifiers changed from: package-private */
    public void setupCloseButton(View view) {
        final ImageButton imageButton = (ImageButton) view.findViewById(R.id.newsfeed_button_close);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NewsfeedGUI.this.dismissBoard();
            }
        });
        imageButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        imageButton.setColorFilter(NewsfeedGUI.closeButtonHighlightColor);
                        return false;
                    case 1:
                        imageButton.clearColorFilter();
                        return false;
                    default:
                        return false;
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void setupPreviousButton(View view) {
        final ImageButton imageButton = (ImageButton) view.findViewById(R.id.newsfeed_button_prev);
        if (this.sortedMessages.size() < 2) {
            imageButton.setVisibility(4);
            return;
        }
        imageButton.setVisibility(0);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NewsfeedGUI.this.onClickedPreviousButton();
            }
        });
        imageButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        imageButton.setColorFilter(NewsfeedGUI.bottomButtonHighlightColor);
                        return false;
                    case 1:
                        imageButton.clearColorFilter();
                        return false;
                    default:
                        return false;
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void onClickedPreviousButton() {
        this.messageIndex--;
        if (this.messageIndex < 0) {
            this.messageIndex = this.sortedMessages.size() - 1;
        }
        displayMessage(this.layout);
    }

    /* access modifiers changed from: package-private */
    public void setupNextButton(View view) {
        final ImageButton imageButton = (ImageButton) view.findViewById(R.id.newsfeed_button_next);
        if (this.sortedMessages.size() < 2) {
            imageButton.setVisibility(4);
            return;
        }
        imageButton.setVisibility(0);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NewsfeedGUI.this.onClickedNextButton();
            }
        });
        imageButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        imageButton.setColorFilter(NewsfeedGUI.bottomButtonHighlightColor);
                        return false;
                    case 1:
                        imageButton.clearColorFilter();
                        return false;
                    default:
                        return false;
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void onClickedNextButton() {
        this.messageIndex++;
        if (this.messageIndex >= this.sortedMessages.size()) {
            this.messageIndex = 0;
        }
        displayMessage(this.layout);
    }

    /* access modifiers changed from: package-private */
    public String longestActionButtonTextString(int[] iArr) {
        if (iArr.length <= 0) {
            Log.e("NewsfeedGUI", "List of sorted messages is empty");
            return "";
        }
        String str = "";
        int i = 0;
        for (int i2 : iArr) {
            if (!this.loadedMessages.containsKey(Integer.valueOf(i2))) {
                Log.e("NewsfeedGUI", "Message '" + i2 + "' not found!");
            } else {
                NewsfeedMessageView newsfeedMessageView = this.loadedMessages.get(Integer.valueOf(i2));
                int length = newsfeedMessageView.actionButtonText.length();
                if (length > i) {
                    str = newsfeedMessageView.actionButtonText;
                    i = length;
                }
            }
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public void adjustActionButtonTextSizeToFit(View view, int i) {
        float dimension = view.getResources().getDimension(R.dimen.nf_action_button_text_size) / this.displayDensity;
        TextView textView = (TextView) view.findViewById(R.id.newsfeed_action_button_text_shadow);
        textView.setTextSize(dimension);
        TextView textView2 = (TextView) view.findViewById(R.id.newsfeed_action_button_text);
        textView2.setTextSize(dimension);
        TextPaint paint = textView2.getPaint();
        String charSequence = textView2.getText().toString();
        boolean z = false;
        float f = (float) i;
        if (paint.measureText(charSequence, 0, charSequence.length()) > f) {
            float dimension2 = f + view.getResources().getDimension(R.dimen.nf_action_button_side_section_width);
            while (!z) {
                dimension -= 0.5f;
                textView2.setTextSize(dimension);
                if (paint.measureText(charSequence) < dimension2) {
                    z = true;
                }
            }
            textView.setTextSize(dimension);
        }
    }

    /* access modifiers changed from: package-private */
    public void setActionButtonText(View view, String str) {
        ((TextView) view.findViewById(R.id.newsfeed_action_button_text_shadow)).setText(str);
        ((TextView) view.findViewById(R.id.newsfeed_action_button_text)).setText(str);
        adjustActionButtonTextSizeToFit(view, ((ImageView) view.findViewById(R.id.newsfeed_button_get_it_now_img_m)).getLayoutParams().width);
    }

    /* access modifiers changed from: package-private */
    public void adjustActionButtonToText(View view, String str) {
        Resources resources;
        int i;
        Resources resources2;
        int i2;
        ((TextView) view.findViewById(R.id.newsfeed_action_button_text_shadow)).setText(str);
        TextView textView = (TextView) view.findViewById(R.id.newsfeed_action_button_text);
        textView.setText(str);
        boolean z = true;
        if (this.activity.getResources().getConfiguration().orientation != 1) {
            z = false;
        }
        Boolean valueOf = Boolean.valueOf(z);
        int dimension = (int) view.getResources().getDimension(R.dimen.nf_action_button_middle_section_width);
        float dimension2 = (view.getResources().getDimension(R.dimen.nf_bottom_side_buttons_width) * 2.0f) + (view.getResources().getDimension(R.dimen.nf_bg_border_component_size) * 2.0f);
        if (valueOf.booleanValue()) {
            resources = view.getResources();
            i = R.dimen.nf_action_button_margin_sides_portrait;
        } else {
            resources = view.getResources();
            i = R.dimen.nf_action_button_margin_sides;
        }
        int width = this.popup.getWidth() - ((int) ((dimension2 + (2.0f * resources.getDimension(i))) + (45.0f * this.displayDensity)));
        if (dimension > width) {
            dimension = width;
        }
        float measureText = textView.getPaint().measureText(str, 0, str.length());
        int i3 = measureText < ((float) dimension) ? dimension : (int) measureText;
        float f = (float) width;
        if (measureText > f) {
            adjustActionButtonTextSizeToFit(view, (int) (f + view.getResources().getDimension(R.dimen.nf_action_button_side_section_width)));
            i3 = width;
        }
        ImageView imageView = (ImageView) view.findViewById(R.id.newsfeed_button_get_it_now_img_m);
        imageView.getLayoutParams().width = i3;
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.newsfeed_button_get_it_now);
        relativeLayout.getLayoutParams().width = imageView.getLayoutParams().width + (((int) view.getResources().getDimension(R.dimen.nf_action_button_side_section_width)) * 2);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) relativeLayout.getLayoutParams();
        if (valueOf.booleanValue()) {
            resources2 = view.getResources();
            i2 = R.dimen.nf_action_button_margin_sides_portrait;
        } else {
            resources2 = view.getResources();
            i2 = R.dimen.nf_action_button_margin_sides;
        }
        int dimension3 = (int) resources2.getDimension(i2);
        layoutParams.setMargins(dimension3, 0, dimension3, 0);
    }

    /* access modifiers changed from: package-private */
    public void setupGetItNowButton(View view) {
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.newsfeed_button_get_it_now);
        final ImageView imageView = (ImageView) view.findViewById(R.id.newsfeed_button_get_it_now_img_l);
        final ImageView imageView2 = (ImageView) view.findViewById(R.id.newsfeed_button_get_it_now_img_m);
        final ImageView imageView3 = (ImageView) view.findViewById(R.id.newsfeed_button_get_it_now_img_r);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NewsfeedGUI.this.onClickedGetItNowButton();
            }
        });
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        imageView.setColorFilter(NewsfeedGUI.bottomButtonHighlightColor);
                        imageView2.setColorFilter(NewsfeedGUI.bottomButtonHighlightColor);
                        imageView3.setColorFilter(NewsfeedGUI.bottomButtonHighlightColor);
                        return false;
                    case 1:
                        imageView.clearColorFilter();
                        imageView2.clearColorFilter();
                        imageView3.clearColorFilter();
                        return false;
                    default:
                        return false;
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void onClickedGetItNowButton() {
        Newsfeed.instance().newsfeedMessageGetItPressed(this.sortedMessages.get(this.messageIndex).intValue());
    }

    /* access modifiers changed from: package-private */
    public void setupImageButton(View view) {
        final ImageButton imageButton = (ImageButton) view.findViewById(R.id.newsfeed_imageview_content);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NewsfeedGUI.this.onClickedGetItNowButton();
            }
        });
        imageButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        imageButton.setColorFilter(Color.argb(100, 0, 0, 0));
                        break;
                    case 1:
                        imageButton.clearColorFilter();
                        break;
                }
                return false;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void displayMessage(View view) {
        Bitmap bitmap;
        if (view == null) {
            Log.e("NewsfeedGUI", "Can't display messages - Layout is null!");
            dismissBoard();
        } else if (this.sortedMessages.isEmpty()) {
            Log.e("NewsfeedGUI", "List of sorted messages is empty");
            dismissBoard();
        } else {
            if (this.messageIndex >= this.sortedMessages.size()) {
                Log.e("NewsfeedGUI", "Messages index out of bounds -- going back to 0");
                this.messageIndex = 0;
            }
            int intValue = this.sortedMessages.get(this.messageIndex).intValue();
            if (!this.loadedMessages.containsKey(Integer.valueOf(intValue))) {
                Log.e("NewsfeedGUI", "Message '" + intValue + "' not found!");
                dismissBoard();
                return;
            }
            NewsfeedMessageView newsfeedMessageView = this.loadedMessages.get(Integer.valueOf(intValue));
            Bitmap bitmap2 = newsfeedMessageView.messageBitmapLandscape;
            if (this.activity.getResources().getConfiguration().orientation == 1 && (bitmap = newsfeedMessageView.messageBitmapPortrait) != null) {
                bitmap2 = bitmap;
            }
            ImageButton imageButton = (ImageButton) view.findViewById(R.id.newsfeed_imageview_content);
            if (imageButton != null) {
                imageButton.setImageBitmap(bitmap2);
                Newsfeed.instance().newsfeedMessageDisplayed(intValue);
            } else {
                Log.e("NewsfeedGUI", "image view for news_content was Null :((((");
            }
            setActionButtonText(view, newsfeedMessageView.actionButtonText);
            RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.newsfeed_button_get_it_now);
            if (!newsfeedMessageView.hasUpsellLink) {
                relativeLayout.setVisibility(4);
            } else {
                relativeLayout.setVisibility(0);
            }
        }
    }

    public void dismissBoard() {
        if (this.showingNews) {
            this.showingNews = false;
            if (this.popup != null) {
                final PopupWindow popupWindow = this.popup;
                Miniclip.queueEvent(ThreadingContext.AndroidUi, new Runnable() {
                    public void run() {
                        if (popupWindow != null) {
                            popupWindow.dismiss();
                        }
                    }
                });
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isShowingNewsfeed() {
        return this.showingNews;
    }

    /* access modifiers changed from: package-private */
    public boolean hasAnyMessageReady() {
        this.activity = Miniclip.getActivity();
        if (this.activity == null) {
            Log.e("NewsfeedGUI", "Can't check if there are any messages ready - Newsfeed can't find the Miniclip activity.");
            return false;
        }
        boolean z = this.activity.getResources().getConfiguration().orientation == 2;
        for (NewsfeedMessageView next : this.loadedMessages.values()) {
            if (z) {
                if (next.messageBitmapLandscape != null) {
                    return true;
                }
            } else if (next.messageBitmapPortrait != null) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int numberLoadedMesssages() {
        this.activity = Miniclip.getActivity();
        if (this.activity == null) {
            Log.e("NewsfeedGUI", "Number of loaded messages requested but Newsfeed can't find the Miniclip activity.");
            return -1;
        }
        int i = 0;
        for (NewsfeedMessageView next : this.loadedMessages.values()) {
            int i2 = this.activity.getResources().getConfiguration().orientation;
            if (i2 == 2) {
                if (next.messageBitmapLandscape != null) {
                    i++;
                }
            } else if (i2 == 1 && next.messageBitmapPortrait != null) {
                i++;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public int[] loadedMessages() {
        this.activity = Miniclip.getActivity();
        if (this.activity == null) {
            Log.e("NewsfeedGUI", "Number of loaded messages requested but Newsfeed can't find the Miniclip activity.");
            return new int[0];
        }
        ArrayList arrayList = new ArrayList();
        for (NewsfeedMessageView next : this.loadedMessages.values()) {
            int i = this.activity.getResources().getConfiguration().orientation;
            if (i == 2) {
                if (next.messageBitmapLandscape != null) {
                    arrayList.add(next.messageID);
                }
            } else if (i == 1 && next.messageBitmapPortrait != null) {
                arrayList.add(next.messageID);
            }
        }
        int[] iArr = new int[arrayList.size()];
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            iArr[i2] = ((Integer) arrayList.get(i2)).intValue();
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public void addMessage(Integer num, byte[] bArr, boolean z, boolean z2, String str) {
        NewsfeedMessageView newsfeedMessageView;
        if (this.loadedMessages.containsKey(num)) {
            newsfeedMessageView = this.loadedMessages.get(num);
        } else {
            NewsfeedMessageView newsfeedMessageView2 = new NewsfeedMessageView(num, z2, str);
            this.loadedMessages.put(num, newsfeedMessageView2);
            newsfeedMessageView = newsfeedMessageView2;
        }
        if ((!z || newsfeedMessageView.messageBitmapLandscape == null) && (z || newsfeedMessageView.messageBitmapPortrait == null)) {
            newsfeedMessageView.preload(bArr, z);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(" Repeated request for message (");
        sb.append(num);
        sb.append(") to load (");
        sb.append(z ? "landscape" : EnvironmentUtils.ORIENTATION_PORTRAIT);
        sb.append(").");
        Log.i("NewsfeedGUI", sb.toString());
    }

    /* access modifiers changed from: package-private */
    public void removeMessage(Integer num) {
        this.loadedMessages.remove(num);
    }

    /* access modifiers changed from: package-private */
    public void clearAllMessages() {
        this.loadedMessages = new HashMap<>();
    }

    protected class NewsfeedMessageView {
        String actionButtonText = "GET IT NOW";
        boolean hasUpsellLink;
        Bitmap messageBitmapLandscape;
        Bitmap messageBitmapPortrait;
        Integer messageID;

        public NewsfeedMessageView(Integer num, boolean z, String str) {
            this.messageID = num;
            this.hasUpsellLink = z;
            this.actionButtonText = str;
        }

        public void preload(byte[] bArr, boolean z) {
            StringBuilder sb = new StringBuilder();
            sb.append("Preloading message ");
            sb.append(this.messageID);
            sb.append(" - ");
            sb.append(z ? "landscape" : EnvironmentUtils.ORIENTATION_PORTRAIT);
            sb.append(".");
            Log.i("NewsfeedGUI", sb.toString());
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            if (z) {
                this.messageBitmapLandscape = decodeByteArray;
            } else {
                this.messageBitmapPortrait = decodeByteArray;
            }
            onPreloadComplete();
        }

        public void onPreloadComplete() {
            Newsfeed.instance().newsfeedMessageLoaded(this.messageID.intValue());
        }
    }

    public void onStop() {
        dismissBoard();
    }

    static boolean showBoard(final int[] iArr) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                Newsfeed.instance().gui.showBoardInternal(iArr);
            }
        });
        return true;
    }

    private static void preloadMessage(int i, byte[] bArr, boolean z, boolean z2, String str) {
        Newsfeed.instance().gui.addMessage(Integer.valueOf(i), bArr, z, z2, str);
    }

    private static void dismissNewsfeedBoard() {
        Newsfeed.instance().gui.dismissBoard();
    }

    private static void removeNewsfeedMessage(int i) {
        Newsfeed.instance().gui.removeMessage(Integer.valueOf(i));
    }

    private static void clearNewsfeedMessages() {
        Newsfeed.instance().gui.clearAllMessages();
    }

    private static boolean isShowingNews() {
        return Newsfeed.instance().gui.showingNews;
    }

    private static boolean hasReadyMessages() {
        return Newsfeed.instance().gui.hasAnyMessageReady();
    }

    private static int loadedMessagesCount() {
        return Newsfeed.instance().gui.numberLoadedMesssages();
    }

    private static int[] getLoadedMessages() {
        return Newsfeed.instance().gui.loadedMessages();
    }

    static void showNext() {
        if (Newsfeed.instance().gui.isShowingNewsfeed()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    Newsfeed.instance().gui.onClickedNextButton();
                }
            });
        }
    }

    static void showPrevious() {
        if (Newsfeed.instance().gui.isShowingNewsfeed()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    Newsfeed.instance().gui.onClickedPreviousButton();
                }
            });
        }
    }
}
