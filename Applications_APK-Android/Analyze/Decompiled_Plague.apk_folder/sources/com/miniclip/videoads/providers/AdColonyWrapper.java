package com.miniclip.videoads.providers;

import android.util.Log;
import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.videoads.MCVideoAds;
import com.miniclip.videoads.ProviderConfig;
import com.miniclip.videoads.ProviderWrapper;
import com.tapjoy.TapjoyAuctionFlags;

public class AdColonyWrapper extends AbstractActivityListener implements ProviderWrapper {
    private MiniclipAndroidActivity _activity;
    /* access modifiers changed from: private */
    public AdColonyInterstitial _adInterstitial;
    private boolean _adsAvailable = false;
    /* access modifiers changed from: private */
    public String _appId;
    private boolean _enabled;
    /* access modifiers changed from: private */
    public boolean _initialized = false;
    /* access modifiers changed from: private */
    public boolean _isRequestingVideo = false;
    private int _order;
    /* access modifiers changed from: private */
    public String _userId;
    private String _zoneId;
    AdColonyInterstitialListener listener = new AdColonyInterstitialListener() {
        public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
            Log.i("AdColonyWrapper", "onRequestFilled");
            boolean unused = AdColonyWrapper.this._isRequestingVideo = false;
            AdColonyInterstitial unused2 = AdColonyWrapper.this._adInterstitial = adColonyInterstitial;
        }

        public void onRequestNotFilled(AdColonyZone adColonyZone) {
            Log.i("AdColonyWrapper", String.format("onRequestNotFilled: %s - %s", AdColonyWrapper.this._appId, adColonyZone.getZoneID()));
            Log.i("AdColonyWrapper", "onRequestNotFilled");
        }

        public void onClosed(AdColonyInterstitial adColonyInterstitial) {
            Log.i("AdColonyWrapper", String.format("onClosed", new Object[0]));
            MCVideoAds.signalHandleReward(AdColonyWrapper.this.getName());
        }
    };

    public String getName() {
        return MCVideoAds.AD_COLONY_NAME;
    }

    public AdColonyWrapper(MiniclipAndroidActivity miniclipAndroidActivity, String str, String str2, String str3, int i, boolean z) throws IllegalArgumentException {
        if (str2 == null || str2.isEmpty() || !z) {
            throw new IllegalArgumentException("invalid user id or not enabled");
        }
        this._activity = miniclipAndroidActivity;
        this._appId = str;
        this._userId = str2;
        this._zoneId = str3;
        this._activity.addListener(this);
        this._order = i;
        this._enabled = z;
    }

    public void init() {
        this._activity.runOnUiThread(new Runnable() {
            public void run() {
                if (!AdColonyWrapper.this._initialized) {
                    Log.i("AdColonyWrapper", String.format("AdColony configured - appId: %s uid: %s", AdColonyWrapper.this._appId, AdColonyWrapper.this._userId));
                    boolean unused = AdColonyWrapper.this._initialized = true;
                }
            }
        });
    }

    public boolean isInitialized() {
        return this._initialized;
    }

    public void setConsent(boolean z) {
        AdColony.configure(this._activity, new AdColonyAppOptions().setGDPRConsentString(z ? TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE : "0").setGDPRRequired(true), this._appId, this._zoneId);
    }

    public boolean reconfigureProvider(ProviderConfig providerConfig) {
        Log.i("AdColonyWrapper", "reconfigureProvider");
        return true;
    }

    public void requestAd() {
        Log.i("AdColonyWrapper", "requestAd");
        this._isRequestingVideo = true;
        AdColony.requestInterstitial(this._zoneId, this.listener);
    }

    public void displayAd() {
        Log.i("AdColonyWrapper", "displayAd");
        if (this._adInterstitial == null) {
            Log.i("MCVideosAds", "Does not have ad to show!");
        } else {
            this._adInterstitial.show();
        }
    }

    public boolean isAdAvailable() {
        Log.i("AdColonyWrapper", "isAdAvailable");
        if (this._adInterstitial != null) {
            return true;
        }
        requestAd();
        return false;
    }

    public void disable() {
        Log.i("AdColonyWrapper", "disable");
        this._enabled = false;
    }

    public boolean isRequestingVideo() {
        Log.i("AdColonyWrapper", getClass().toString() + " does not support functionality");
        return this._isRequestingVideo;
    }

    public int getOrder() {
        return this._order;
    }
}
