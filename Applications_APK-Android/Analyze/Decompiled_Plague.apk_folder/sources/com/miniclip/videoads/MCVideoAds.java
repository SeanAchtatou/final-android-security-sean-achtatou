package com.miniclip.videoads;

import android.util.Log;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.videoads.providers.AdColonyWrapper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class MCVideoAds {
    public static final String AD_COLONY_NAME = "AdColony";
    public static final String VUNGLE_NAME = "Vungle";
    private static MiniclipAndroidActivity _activity;
    private static Map<String, ProviderWrapper> _providers = new HashMap();

    public static native void adsAvailabilityChange(String str, boolean z);

    /* access modifiers changed from: private */
    public static native void adsClosed();

    /* access modifiers changed from: private */
    public static native void handleReward(String str);

    public static native void handleVideoAdStarted();

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        _activity = miniclipAndroidActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public static String registerListOfAdProviders(String str) throws Exception {
        Log.i("MCVideoAds", "registerListOfAdProviders");
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject(str);
        Iterator<String> keys = jSONObject2.keys();
        ArrayList arrayList = new ArrayList();
        while (keys.hasNext()) {
            arrayList.add(keys.next());
        }
        disableProvidersNotIn(arrayList);
        Iterator<String> keys2 = jSONObject2.keys();
        while (keys2.hasNext()) {
            String next = keys2.next();
            Log.i("MCVideoAds", "Provider " + next);
            JSONObject jSONObject3 = jSONObject2.getJSONObject(next);
            Log.i("MCVideoAds", "Provider configuration " + jSONObject3.toString());
            try {
                boolean registerAdProvider = registerAdProvider(next, new ProviderConfig(next, jSONObject3));
                Log.i("MCVideoAds", "Provider " + next + " registered with success: " + registerAdProvider);
                jSONObject.put(next, registerAdProvider);
            } catch (JSONException unused) {
                Log.i("MCVideoAds", next + ": JSONException from provider config");
                Log.i("MCVideoAds", next + ": Values could be missing or invalid");
                Log.i("MCVideoAds", "Provider " + next + " registered with success: false");
                jSONObject.put(next, false);
            }
        }
        return jSONObject.toString();
    }

    public static boolean isRequestingVideo() {
        for (ProviderWrapper next : orderedProviders(_providers.values())) {
            Log.i("MCVideoAd", "isRequestingVideo: " + next.getName() + " " + next.isRequestingVideo());
            if (next.isRequestingVideo()) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAdAvailable() {
        for (ProviderWrapper next : orderedProviders(_providers.values())) {
            Log.i("MCVideoAd", "isAdAvailable: " + next.getName() + " " + next.isAdAvailable());
            if (next.isAdAvailable()) {
                return true;
            }
        }
        return false;
    }

    public static void displayAd() {
        for (ProviderWrapper next : orderedProviders(_providers.values())) {
            if (next.isAdAvailable()) {
                next.displayAd();
                return;
            }
        }
    }

    public static ProviderWrapper getProvider(String str) {
        return _providers.get(str);
    }

    public static void signalHandleReward(final String str) {
        _activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCVideoAds.handleReward(str);
            }
        });
    }

    public static void signalAdsClosed() {
        _activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCVideoAds.adsClosed();
            }
        });
    }

    public static void signalAdsAvailabilityChange(final String str, final boolean z) {
        _activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCVideoAds.adsAvailabilityChange(str, z);
            }
        });
    }

    public static void signalVideoAdStarted() {
        _activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCVideoAds.handleVideoAdStarted();
            }
        });
    }

    static void disableProvidersNotIn(List<String> list) {
        Set<String> keySet = _providers.keySet();
        keySet.removeAll(list);
        for (String next : keySet) {
            Log.i("MCVideosAds", next + " will be disabled since it wasn't present in the current providers to register");
            _providers.get(next).disable();
        }
    }

    static boolean registerAdProvider(String str, ProviderConfig providerConfig) {
        Log.i("MCVideoAds", "Registering provider " + str);
        if (_providers.containsKey(str)) {
            Log.i("MCVideoAds", "Provider " + str + " already found, reconfiguring instead");
            return _providers.get(str).reconfigureProvider(providerConfig);
        } else if (!providerConfig.enabled.booleanValue()) {
            Log.i("MCVideoAds", "Provider " + str + " is disabled");
            return false;
        } else if (str.equalsIgnoreCase(AD_COLONY_NAME)) {
            return registerAdColony(providerConfig.appId, providerConfig.userId, providerConfig.zoneId, providerConfig.order.intValue(), providerConfig.enabled.booleanValue());
        } else {
            boolean equalsIgnoreCase = str.equalsIgnoreCase(VUNGLE_NAME);
            return false;
        }
    }

    static boolean registerAdColony(String str, String str2, String str3, int i, boolean z) {
        try {
            AdColonyWrapper adColonyWrapper = new AdColonyWrapper(_activity, str, str2, str3, i, z);
            adColonyWrapper.init();
            _providers.put(AD_COLONY_NAME, adColonyWrapper);
            return true;
        } catch (Exception unused) {
            Log.i("MCVideoAds", "Something went wrong while initializing AdColony provider");
            return false;
        }
    }

    static List<ProviderWrapper> orderedProviders(Collection<ProviderWrapper> collection) {
        ArrayList arrayList = new ArrayList(collection);
        Collections.sort(arrayList, new Comparator<ProviderWrapper>() {
            public int compare(ProviderWrapper providerWrapper, ProviderWrapper providerWrapper2) {
                return -1 * (providerWrapper2.getOrder() - providerWrapper.getOrder());
            }
        });
        return arrayList;
    }

    public static void setConsent(boolean z) {
        for (ProviderWrapper consent : orderedProviders(_providers.values())) {
            consent.setConsent(z);
        }
    }
}
