package com.miniclip.videoads;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class ProviderConfig {
    public static final String MCVideoAdsAdIdKey = "zoneId";
    public static final String MCVideoAdsAppIdKey = "appId";
    public static final String MCVideoAdsCredentialsKey = "credentials";
    public static final String MCVideoAdsEnabledKey = "enabled";
    public static final String MCVideoAdsPriorityKey = "priority";
    public static final String MCVideoAdsTestAdsKey = "testAds";
    public static final String MCVideoAdsUserIdKey = "uid";
    public static final String MCVideoAdsVersionKey = "version";
    public String appId;
    public Boolean enabled;
    public Boolean isTest = false;
    public Integer order;
    public String provider;
    public String userId;
    public String zoneId = "";

    public ProviderConfig(String str, JSONObject jSONObject) throws JSONException {
        this.provider = str;
        this.enabled = Boolean.valueOf(jSONObject.getBoolean("enabled"));
        Log.i("MCVideoAds", "is enabled: " + this.enabled);
        this.appId = jSONObject.getString(MCVideoAdsAppIdKey);
        Log.i("MCVideoAds", "appId: " + this.appId);
        this.userId = jSONObject.getString(MCVideoAdsUserIdKey);
        Log.i("MCVideoAds", "userId: " + this.userId);
        this.order = Integer.valueOf(jSONObject.getInt(MCVideoAdsPriorityKey));
        Log.i("MCVideoAds", "order: " + this.order);
        if (jSONObject.has(MCVideoAdsAdIdKey)) {
            this.zoneId = jSONObject.getString(MCVideoAdsAdIdKey);
            Log.i("MCVideoAds", "zoneId: " + this.zoneId);
        }
        if (jSONObject.has(MCVideoAdsTestAdsKey)) {
            this.isTest = Boolean.valueOf(jSONObject.getBoolean(MCVideoAdsTestAdsKey));
            Log.i("MCVideoAds", "isTest: " + this.isTest);
        }
    }
}
