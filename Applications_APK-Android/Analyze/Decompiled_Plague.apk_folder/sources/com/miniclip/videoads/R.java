package com.miniclip.videoads;

public final class R {

    public static final class drawable {
        public static final int close_webpage_big = 2131099652;
        public static final int close_webpage_small = 2131099653;
    }

    public static final class id {
        public static final int close_webpage_clickable = 2131165200;
        public static final int mainTextView = 2131165233;
        public static final int progressBar1 = 2131165282;
        public static final int webview_with_webpage = 2131165296;
    }

    public static final class layout {
        public static final int activity_test_video_ads = 2131296257;
        public static final int progress_view = 2131296291;
    }

    public static final class string {
        public static final int app_name = 2131427333;
        public static final int hello_world = 2131427394;
        public static final int title_activity_test_video_ads = 2131427430;
    }
}
