package com.miniclip.videoads;

public interface ProviderWrapper {
    void disable();

    void displayAd();

    String getName();

    int getOrder();

    boolean isAdAvailable();

    boolean isRequestingVideo();

    boolean reconfigureProvider(ProviderConfig providerConfig);

    void setConsent(boolean z);
}
