package com.miniclip.ui;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.facebook.widget.WebDialog;
import com.miniclip.framework.Miniclip;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.prime.R;
import java.io.UnsupportedEncodingException;

public class HTMLDialog extends Dialog {
    static final FrameLayout.LayoutParams FILL = new FrameLayout.LayoutParams(-1, -1);
    /* access modifiers changed from: private */
    public boolean _isWebViewAttached;
    /* access modifiers changed from: private */
    public boolean _wasWebviewDismissed;
    private String mBaseURL = "";
    /* access modifiers changed from: private */
    public FrameLayout mContent;
    /* access modifiers changed from: private */
    public ImageView mCrossImage;
    private String mHtml;
    private int mIsInternalURL = 1;
    private WebView mWebView;
    /* access modifiers changed from: private */
    public LinearLayout mWebViewContainer;
    /* access modifiers changed from: private */
    public long mWebpageID;

    /* access modifiers changed from: private */
    public native void dismissLoadingPopupNative(long j);

    /* access modifiers changed from: private */
    public native void handleBackButtonPressNative(long j);

    private native boolean useSmallCloseButtonNative();

    public HTMLDialog(String str, byte[] bArr, int i, long j) {
        super(Miniclip.getActivity(), WebDialog.DEFAULT_THEME);
        try {
            this.mHtml = new String(bArr, "UTF-8");
            this.mBaseURL = str;
            this.mWebpageID = j;
            this.mIsInternalURL = i;
            setCanceledOnTouchOutside(false);
        } catch (UnsupportedEncodingException unused) {
            Log.w("Webpage", "Html source code not in UTF encoding");
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            setSystemUiVisibility();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            handleBackButtonPressNative(this.mWebpageID);
            dismiss();
            this._isWebViewAttached = false;
            this._wasWebviewDismissed = true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mContent = new FrameLayout(getContext());
        createCrossImage();
        setUpWebView(this.mCrossImage.getDrawable().getIntrinsicWidth() / 2);
        addContentView(this.mContent, new ViewGroup.LayoutParams(-1, -1));
    }

    private void createCrossImage() {
        int i;
        this.mCrossImage = new ImageView(getContext());
        this.mCrossImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                HTMLDialog.this.handleBackButtonPressNative(HTMLDialog.this.mWebpageID);
                HTMLDialog.this.dismiss();
                boolean unused = HTMLDialog.this._isWebViewAttached = false;
                boolean unused2 = HTMLDialog.this._wasWebviewDismissed = true;
            }
        });
        if (useSmallCloseButtonNative()) {
            i = R.drawable.close_webpage_small;
        } else {
            i = R.drawable.close_webpage_big;
        }
        this.mCrossImage.setImageDrawable(getContext().getResources().getDrawable(i));
        this.mCrossImage.setVisibility(0);
        this.mCrossImage.setId(R.id.close_webpage_clickable);
    }

    private void setUpWebView(int i) {
        this.mWebViewContainer = new LinearLayout(getContext());
        this.mWebView = new WebView(getContext());
        this.mWebView.setId(R.id.webview_with_webpage);
        this.mWebView.setVerticalScrollBarEnabled(true);
        this.mWebView.setHorizontalScrollBarEnabled(false);
        this.mWebView.setWebViewClient(new WebViewClient());
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setDomStorageEnabled(true);
        this.mWebView.getSettings().setDefaultTextEncodingName("utf-8");
        this.mWebView.getSettings().setLoadWithOverviewMode(true);
        this.mWebView.getSettings().setUseWideViewPort(true);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= 11) {
            this.mWebView.getSettings().setDisplayZoomControls(false);
        }
        this._wasWebviewDismissed = false;
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (str == null || !str.startsWith("market:")) {
                    return false;
                }
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                for (ResolveInfo next : Miniclip.getActivity().getPackageManager().queryIntentActivities(intent, 0)) {
                    if (next.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {
                        ActivityInfo activityInfo = next.activityInfo;
                        ComponentName componentName = new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name);
                        intent.setFlags(270532608);
                        intent.setComponent(componentName);
                        Miniclip.getActivity().startActivity(intent);
                        return true;
                    }
                }
                return true;
            }

            public void onPageFinished(WebView webView, String str) {
                if (!HTMLDialog.this._isWebViewAttached && !HTMLDialog.this._wasWebviewDismissed) {
                    HTMLDialog.this.mContent.addView(HTMLDialog.this.mWebViewContainer);
                    HTMLDialog.this.mContent.addView(HTMLDialog.this.mCrossImage, new ViewGroup.LayoutParams(-2, -2));
                    boolean unused = HTMLDialog.this._isWebViewAttached = true;
                    Miniclip.queueEvent(ThreadingContext.Main, new Runnable() {
                        public void run() {
                            HTMLDialog.this.dismissLoadingPopupNative(HTMLDialog.this.mWebpageID);
                        }
                    });
                }
            }
        });
        if (this.mIsInternalURL == 0) {
            this.mWebView.loadUrl(this.mHtml);
        } else {
            this.mWebView.loadDataWithBaseURL(this.mBaseURL, this.mHtml, "text/html", null, null);
        }
        this.mWebView.setScrollContainer(true);
        this.mWebView.setLayoutParams(FILL);
        this.mContent.setBackgroundColor(0);
        this.mWebView.setVisibility(0);
        this.mWebViewContainer.addView(this.mWebView);
        this._isWebViewAttached = false;
    }

    private void setSystemUiVisibility() {
        if (Build.VERSION.SDK_INT >= 14) {
            int i = 2;
            if (Build.VERSION.SDK_INT >= 16) {
                i = 1798;
                if (Build.VERSION.SDK_INT >= 19) {
                    i = 5894;
                }
            }
            getWindow().getDecorView().setSystemUiVisibility(i);
        }
    }

    public void show() {
        getWindow().setFlags(8, 8);
        setSystemUiVisibility();
        super.show();
        getWindow().clearFlags(8);
    }

    public static void showHTMLDialog(String str, byte[] bArr, int i, long j) {
        final String str2 = str;
        final byte[] bArr2 = bArr;
        final int i2 = i;
        final long j2 = j;
        Miniclip.queueEvent(ThreadingContext.AndroidUi, new Runnable() {
            public void run() {
                new HTMLDialog(str2, bArr2, i2, j2).show();
            }
        });
    }
}
