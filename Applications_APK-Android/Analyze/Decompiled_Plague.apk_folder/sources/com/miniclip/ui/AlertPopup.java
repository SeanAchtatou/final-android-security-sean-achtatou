package com.miniclip.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.Miniclip;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.prime.R;

public class AlertPopup implements DialogInterface.OnClickListener {
    private AbstractActivityListener activityListener = new AbstractActivityListener() {
        public void onDestroy() {
            if (AlertPopup.this.mDialog != null) {
                AlertPopup.this.mDialog.dismiss();
            }
            AlertPopup.nativeClearRunningPopups();
        }
    };
    private String[] mButtonTitles;
    /* access modifiers changed from: private */
    public boolean mCancelable;
    /* access modifiers changed from: private */
    public AlertDialog mDialog;
    private String mMessage;
    /* access modifiers changed from: private */
    public long mNativeDialogId;
    /* access modifiers changed from: private */
    public Number mNativeObjectReference;
    private boolean mShowActivityIndicator;
    private String mTitle;

    private native void handleButtonPressNative(long j, int i);

    /* access modifiers changed from: private */
    public static native void nativeClearRunningPopups();

    /* access modifiers changed from: private */
    public native void nativePopupDidAppear(long j);

    private native void nativePopupDidDisappear(long j);

    private native void nativePopupWillAppear(long j);

    private native void nativePopupWillDisappear(long j);

    AlertPopup(long j, long j2) {
        Miniclip.addListener(this.activityListener);
        this.mNativeObjectReference = Long.valueOf(j);
        this.mNativeDialogId = j2;
    }

    public void showAlertPopup(String str, String str2, boolean z, boolean z2, String[] strArr) {
        Log.d("AlertPopup", "showAlertPopup start, in java");
        this.mTitle = str;
        this.mMessage = str2;
        this.mCancelable = z;
        this.mButtonTitles = strArr;
        this.mShowActivityIndicator = z2;
        Miniclip.queueEvent(ThreadingContext.AndroidUi, new Runnable() {
            public void run() {
                AlertPopup.this.showDialog();
            }
        });
    }

    public void dismissAlertPopup() {
        Miniclip.queueEvent(ThreadingContext.AndroidUi, new Runnable() {
            public void run() {
                AlertPopup.this.closeDialog();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showDialog() {
        Log.d("AlertPopup", " Creating a dialog for message: " + this.mNativeDialogId + " withTitle:" + '\"' + this.mTitle + '\"' + " withMessage: " + '\"' + this.mMessage + '\"' + " and with " + this.mButtonTitles.length + " buttons.");
        if (this.mDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Miniclip.getActivity());
            if (!this.mTitle.equals("")) {
                builder.setTitle(this.mTitle);
            }
            if (this.mButtonTitles.length < 4 && !this.mMessage.equals("")) {
                builder.setMessage(this.mMessage);
            }
            if (this.mButtonTitles.length == 1) {
                builder.setPositiveButton(this.mButtonTitles[0], this);
            } else if (this.mButtonTitles.length == 2) {
                builder.setNegativeButton(this.mButtonTitles[0], this);
                builder.setPositiveButton(this.mButtonTitles[1], this);
            } else if (this.mButtonTitles.length == 3) {
                builder.setMessage(this.mMessage);
                builder.setNeutralButton(this.mButtonTitles[0], this);
                builder.setNegativeButton(this.mButtonTitles[1], this);
                builder.setPositiveButton(this.mButtonTitles[2], this);
            } else if (this.mButtonTitles.length > 3) {
                if (!this.mTitle.equals("")) {
                    if (!this.mMessage.equals("")) {
                        builder.setTitle(this.mTitle + "\n" + this.mMessage);
                    } else {
                        builder.setTitle(this.mTitle);
                    }
                } else if (!this.mMessage.equals("")) {
                    builder.setTitle(this.mMessage);
                } else {
                    Log.d("AlertPopup", "Both title and message not set, we won't show this popup!");
                    return;
                }
                builder.setItems(this.mButtonTitles, this);
            }
            if (this.mShowActivityIndicator) {
                builder.setView(Miniclip.getActivity().getLayoutInflater().inflate(R.layout.progress_view, (ViewGroup) null));
            }
            this.mDialog = builder.create();
            this.mDialog.setCancelable(true);
            this.mDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    if (!AlertPopup.this.mCancelable || i != 4 || keyEvent.getAction() != 0) {
                        return false;
                    }
                    AlertPopup.this.dismissAlertPopup();
                    return true;
                }
            });
            this.mDialog.setCanceledOnTouchOutside(false);
            if (this.mNativeObjectReference != null) {
                nativePopupWillAppear(this.mNativeObjectReference.longValue());
            } else {
                Log.d("AlertPopup", "Tried calling nativePopupWillAppear with null messageId: " + this.mNativeDialogId);
            }
            this.mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                public void onShow(DialogInterface dialogInterface) {
                    if (AlertPopup.this.mNativeObjectReference != null) {
                        AlertPopup.this.nativePopupDidAppear(AlertPopup.this.mNativeObjectReference.longValue());
                        return;
                    }
                    Log.d("AlertPopup", "Tried calling nativePopupDidAppear with null messageId: " + AlertPopup.this.mNativeDialogId);
                }
            });
            try {
                this.mDialog.show();
            } catch (Exception e) {
                Log.d("AlertPopup", "¡Error! Failed to show Alert Popup!!");
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void closeDialog() {
        Log.d("AlertPopup", " Closing AlertDialog with id: " + this.mNativeDialogId);
        if (this.mDialog == null || !this.mDialog.isShowing()) {
            Log.d("AlertPopup", "Trying to dismiss dialog in invalid state " + this.mNativeDialogId);
        } else if (this.mNativeObjectReference == null) {
            Log.d("AlertPopup", "Dismissing Dialog without native object associated id" + this.mNativeDialogId);
            this.mDialog.dismiss();
        } else {
            nativePopupWillDisappear(this.mNativeObjectReference.longValue());
            this.mDialog.dismiss();
            nativePopupDidDisappear(this.mNativeObjectReference.longValue());
        }
    }

    /* access modifiers changed from: package-private */
    public int buttonIndexAjustment(AlertDialog alertDialog, int i) {
        if (i < 0) {
            String str = (String) alertDialog.getButton(-3).getText();
            String str2 = (String) alertDialog.getButton(-2).getText();
            if (str != "") {
                if (i == -1) {
                    return 2;
                }
                if (i == -2) {
                    return 1;
                }
                if (i == -3) {
                    return 0;
                }
            } else if (str2 == "") {
                return 0;
            } else {
                if (i == -1) {
                    return 1;
                }
                if (i == -2) {
                    return 0;
                }
            }
        }
        return i;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        int buttonIndexAjustment = buttonIndexAjustment((AlertDialog) dialogInterface, i);
        if (this.mNativeObjectReference != null) {
            handleButtonPressNative(((Long) this.mNativeObjectReference).longValue(), buttonIndexAjustment);
        }
        closeDialog();
    }
}
