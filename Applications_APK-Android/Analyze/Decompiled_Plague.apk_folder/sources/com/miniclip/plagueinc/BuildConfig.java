package com.miniclip.plagueinc;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.miniclip.plagueinc";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 91;
    public static final String VERSION_NAME = "1.16.3";
}
