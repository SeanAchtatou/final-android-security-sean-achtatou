package com.miniclip.plagueinc;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.miniclip.cloudservices.CloudServices;
import com.miniclip.facebook.FacebookHelper;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.gameservices.GameServices;
import com.miniclip.inapppurchases.MCInAppPurchases;
import com.miniclip.input.MCKeyboard;
import com.miniclip.nativeJNI.GooglePlayServices;
import com.miniclip.nativeJNI.cocojava;
import com.miniclip.notification.BootReceiver;
import com.miniclip.obsolete.Obsolete;
import com.miniclip.platform.MCViewManager;
import com.miniclip.udid.OpenUDID;
import com.miniclip.videoads.MCVideoAds;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PlagueIncActivity extends cocojava implements GooglePlayServices.GooglePlayServicesListener, GameServices.GameServicesListener, CloudServices.CloudServicesListener, MCViewManager.MCViewManagerEventsListener {
    static final int CLOUD_SAVE_SLOT = 0;
    static final int SCREEN_ORIENTATION_SENSOR_LANDSCAPE = 6;
    static final String gameID = "417402";
    static final String gameKey = "nRsWm1jST3tFPWd6ufkZA";
    static final String gameName = "Plague Inc";
    static final String gameSecret = "jCQbkGbRp50L0gbS0D8r6UpMWEpxgiC4UQL9dJBZbc";
    protected HashMap<String, String> achievementsMap;
    protected HashMap<String, String> leaderboardsMap;
    protected GooglePlayServices mHelper;

    /* access modifiers changed from: protected */
    public String getAnalyticsID() {
        return "UA-34391477-1";
    }

    /* access modifiers changed from: protected */
    public String getFullAppURI() {
        return "market://details?id=com.miniclip.plagueinc";
    }

    /* access modifiers changed from: protected */
    public String getFullVersionGameImageId() {
        return "buynow_v2";
    }

    public void onCreate(Bundle bundle) {
        mHAS_RETINA = true;
        super.onCreate(bundle);
        MCViewManager.addListener(this);
        MCKeyboard.mSHOW_KEYBOARD_INPUT = true;
        MCKeyboard.mKEYBOARD_INPUT_SINGLE_LINE = true;
        MCKeyboard.mKEYBOARD_FULLSCREEN = false;
        MCKeyboard.init(this);
        getWindow().addFlags(128);
        MCInAppPurchases.init(this);
        FacebookHelper.init(this);
        Obsolete.init(this);
        BootReceiver.init(this);
        if (Obsolete.getAdsDisabled() == 1) {
            Obsolete.mUSE_ADS = false;
        } else {
            Obsolete.mUSE_ADS = true;
        }
        if (Build.VERSION.SDK_INT >= 9) {
            setRequestedOrientation(6);
        }
        String SharedPreferences_getString = SharedPreferences_getString("APP_VERSION_NUMBER");
        final String appVersionNumber = getAppVersionNumber();
        if (!appVersionNumber.equals(SharedPreferences_getString)) {
            final File filesDir = getFilesDir();
            mUpdateRunable = new Runnable() {
                public void run() {
                    File file = new File(filesDir, "Contents/Resources");
                    file.mkdirs();
                    for (File file2 : file.listFiles()) {
                        if (!file2.isFile()) {
                            cocojava.deleteDirectory(file2);
                        } else if (!(file2.getName().compareTo("global_settings.ini") == 0 || file2.getName().compareTo("achievements.db") == 0 || file2.getName().compareTo("hiscore.ini") == 0 || file2.getName().compareTo("quicksave.k3a.rt") == 0 || file2.getName().compareTo("quicksave.k3a") == 0 || file2.getName().compareTo("quicksave.k3a.sdrt") == 0 || file2.getName().compareTo("NSUserDefaults.plist") == 0 || file2.getName().compareTo("speedrun_bacteria.ini") == 0 || file2.getName().compareTo("speedrun_virus.ini") == 0 || file2.getName().compareTo("speedrun_parasite.ini") == 0 || file2.getName().compareTo("speedrun_fungus.ini") == 0 || file2.getName().compareTo("speedrun_prion.ini") == 0 || file2.getName().compareTo("speedrun_rogue_nanobot.ini") == 0 || file2.getName().compareTo("speedrun_escaped_bio_weapon.ini") == 0 || file2.getName().compareTo("speedrun_neurax.ini") == 0 || file2.getName().compareTo("speedrun_zombie.ini") == 0)) {
                            file2.delete();
                        }
                    }
                    cocojava.SharedPreferences_setString("APP_VERSION_NUMBER", appVersionNumber);
                }
            };
            mUpdateRunableCall = true;
        }
        this.achievementsMap = new HashMap<>();
        this.achievementsMap.put("1662232", new String("CgkIoKSYmJkOEAIQAg"));
        this.achievementsMap.put("1662242", new String("CgkIoKSYmJkOEAIQAw"));
        this.achievementsMap.put("1664742", new String("CgkIoKSYmJkOEAIQBA"));
        this.achievementsMap.put("1664752", new String("CgkIoKSYmJkOEAIQBQ"));
        this.achievementsMap.put("1664762", new String("CgkIoKSYmJkOEAIQBg"));
        this.achievementsMap.put("1664772", new String("CgkIoKSYmJkOEAIQBw"));
        this.achievementsMap.put("1664782", new String("CgkIoKSYmJkOEAIQCA"));
        this.achievementsMap.put("1664792", new String("CgkIoKSYmJkOEAIQCQ"));
        this.achievementsMap.put("1664802", new String("CgkIoKSYmJkOEAIQCg"));
        this.achievementsMap.put("1664812", new String("CgkIoKSYmJkOEAIQCw"));
        this.achievementsMap.put("1664822", new String("CgkIoKSYmJkOEAIQDA"));
        this.achievementsMap.put("1664832", new String("CgkIoKSYmJkOEAIQDQ"));
        this.achievementsMap.put("1664842", new String("CgkIoKSYmJkOEAIQDg"));
        this.achievementsMap.put(NativeContentAd.ASSET_HEADLINE, new String("CgkIoKSYmJkOEAIQDw"));
        this.achievementsMap.put(NativeContentAd.ASSET_BODY, new String("CgkIoKSYmJkOEAIQEA"));
        this.achievementsMap.put(NativeContentAd.ASSET_CALL_TO_ACTION, new String("CgkIoKSYmJkOEAIQEQ"));
        this.achievementsMap.put(NativeContentAd.ASSET_ADVERTISER, new String("CgkIoKSYmJkOEAIQEg"));
        this.achievementsMap.put(NativeContentAd.ASSET_IMAGE, new String("CgkIoKSYmJkOEAIQEw"));
        this.achievementsMap.put(NativeContentAd.ASSET_LOGO, new String("CgkIoKSYmJkOEAIQFA"));
        this.achievementsMap.put(NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE, new String("CgkIoKSYmJkOEAIQFQ"));
        this.achievementsMap.put("1008", new String("CgkIoKSYmJkOEAIQFg"));
        this.achievementsMap.put(NativeContentAd.ASSET_MEDIA_VIDEO, new String("CgkIoKSYmJkOEAIQFw"));
        this.achievementsMap.put("1010", new String("CgkIoKSYmJkOEAIQGA"));
        this.achievementsMap.put("1011", new String("CgkIoKSYmJkOEAIQGQ"));
        this.achievementsMap.put("1012", new String("CgkIoKSYmJkOEAIQGg"));
        this.achievementsMap.put("1013", new String("CgkIoKSYmJkOEAIQGw"));
        this.achievementsMap.put("1014", new String("CgkIoKSYmJkOEAIQHA"));
        this.achievementsMap.put("1015", new String("CgkIoKSYmJkOEAIQHQ"));
        this.achievementsMap.put("1016", new String("CgkIoKSYmJkOEAIQOA"));
        this.achievementsMap.put("1017", new String("CgkIoKSYmJkOEAIQHg"));
        this.achievementsMap.put("1018", new String("CgkIoKSYmJkOEAIQIA"));
        this.achievementsMap.put("1019", new String("CgkIoKSYmJkOEAIQIQ"));
        this.achievementsMap.put("1020", new String("CgkIoKSYmJkOEAIQIg"));
        this.achievementsMap.put("1021", new String("CgkIoKSYmJkOEAIQIw"));
        this.achievementsMap.put("1022", new String("CgkIoKSYmJkOEAIQJA"));
        this.achievementsMap.put("1023", new String("CgkIoKSYmJkOEAIQJQ"));
        this.achievementsMap.put("1024", new String("CgkIoKSYmJkOEAIQNw"));
        this.achievementsMap.put("1025", new String("CgkIoKSYmJkOEAIQJg"));
        this.achievementsMap.put("1026", new String("CgkIoKSYmJkOEAIQJw"));
        this.achievementsMap.put("1027", new String("CgkIoKSYmJkOEAIQKA"));
        this.achievementsMap.put("1028", new String("CgkIoKSYmJkOEAIQHw"));
        this.achievementsMap.put("1029", new String("CgkIoKSYmJkOEAIQKQ"));
        this.achievementsMap.put("1030", new String("CgkIoKSYmJkOEAIQKg"));
        this.achievementsMap.put("1031", new String("CgkIoKSYmJkOEAIQKw"));
        this.achievementsMap.put("1032", new String("CgkIoKSYmJkOEAIQLA"));
        this.achievementsMap.put("1033", new String("CgkIoKSYmJkOEAIQLQ"));
        this.achievementsMap.put("1034", new String("CgkIoKSYmJkOEAIQLg"));
        this.achievementsMap.put("1035", new String("CgkIoKSYmJkOEAIQLw"));
        this.achievementsMap.put("1036", new String("CgkIoKSYmJkOEAIQMA"));
        this.achievementsMap.put("1037", new String("CgkIoKSYmJkOEAIQMQ"));
        this.achievementsMap.put("1038", new String("CgkIoKSYmJkOEAIQMg"));
        this.achievementsMap.put("1039", new String("CgkIoKSYmJkOEAIQMw"));
        this.achievementsMap.put("1040", new String("CgkIoKSYmJkOEAIQNA"));
        this.achievementsMap.put("1041", new String("CgkIoKSYmJkOEAIQNg"));
        this.achievementsMap.put("1042", new String("CgkIoKSYmJkOEAIQQg"));
        this.achievementsMap.put("1043", new String("CgkIoKSYmJkOEAIQQw"));
        this.achievementsMap.put("1044", new String("CgkIoKSYmJkOEAIQRA"));
        this.achievementsMap.put("1045", new String("CgkIoKSYmJkOEAIQRQ"));
        this.achievementsMap.put("1046", new String("CgkIoKSYmJkOEAIQRg"));
        this.achievementsMap.put("1047", new String("CgkIoKSYmJkOEAIQRw"));
        this.achievementsMap.put("1048", new String("CgkIoKSYmJkOEAIQSA"));
        this.achievementsMap.put("1049", new String("CgkIoKSYmJkOEAIQSQ"));
        this.achievementsMap.put("1050", new String("CgkIoKSYmJkOEAIQSg"));
        this.achievementsMap.put("1051", new String("CgkIoKSYmJkOEAIQSw"));
        this.achievementsMap.put("1052", new String("CgkIoKSYmJkOEAIQTA"));
        this.achievementsMap.put("1053", new String("CgkIoKSYmJkOEAIQTQ"));
        this.achievementsMap.put("1054", new String("CgkIoKSYmJkOEAIQTg"));
        this.achievementsMap.put("1055", new String("CgkIoKSYmJkOEAIQTw"));
        this.achievementsMap.put("1056", new String("CgkIoKSYmJkOEAIQUA"));
        this.achievementsMap.put("1057", new String("CgkIoKSYmJkOEAIQUQ"));
        this.achievementsMap.put("1058", new String("CgkIoKSYmJkOEAIQUg"));
        this.achievementsMap.put("1059", new String("CgkIoKSYmJkOEAIQUw"));
        this.achievementsMap.put("1060", new String("CgkIoKSYmJkOEAIQVA"));
        this.achievementsMap.put("1061", new String("CgkIoKSYmJkOEAIQVQ"));
        this.achievementsMap.put("1062", new String("CgkIoKSYmJkOEAIQVg"));
        this.achievementsMap.put("1063", new String("CgkIoKSYmJkOEAIQVw"));
        this.achievementsMap.put("1064", new String("CgkIoKSYmJkOEAIQWA"));
        this.achievementsMap.put("1065", new String("CgkIoKSYmJkOEAIQWQ"));
        this.achievementsMap.put("1066", new String("CgkIoKSYmJkOEAIQWg"));
        this.achievementsMap.put("1067", new String("CgkIoKSYmJkOEAIQWw"));
        this.achievementsMap.put("1068", new String("CgkIoKSYmJkOEAIQXA"));
        this.achievementsMap.put("1069", new String("CgkIoKSYmJkOEAIQXQ"));
        this.achievementsMap.put("1070", new String("CgkIoKSYmJkOEAIQXg"));
        this.achievementsMap.put("1071", new String("CgkIoKSYmJkOEAIQXw"));
        this.achievementsMap.put("1072", new String("CgkIoKSYmJkOEAIQYA"));
        this.achievementsMap.put("1073", new String("CgkIoKSYmJkOEAIQYQ"));
        this.achievementsMap.put("1074", new String("CgkIoKSYmJkOEAIQYg"));
        this.achievementsMap.put("1075", new String("CgkIoKSYmJkOEAIQYw"));
        this.achievementsMap.put("1076", new String("CgkIoKSYmJkOEAIQZA"));
        this.achievementsMap.put("1077", new String("CgkIoKSYmJkOEAIQZQ"));
        this.achievementsMap.put("1078", new String("CgkIoKSYmJkOEAIQZg"));
        this.achievementsMap.put("1079", new String("CgkIoKSYmJkOEAIQZw"));
        this.achievementsMap.put("1080", new String("CgkIoKSYmJkOEAIQaA"));
        this.achievementsMap.put("1081", new String("CgkIoKSYmJkOEAIQaQ"));
        this.achievementsMap.put("1082", new String("CgkIoKSYmJkOEAIQag"));
        this.achievementsMap.put("1083", new String("CgkIoKSYmJkOEAIQaw"));
        this.achievementsMap.put("1084", new String("CgkIoKSYmJkOEAIQbA"));
        this.achievementsMap.put("1085", new String("CgkIoKSYmJkOEAIQew"));
        this.achievementsMap.put("1086", new String("CgkIoKSYmJkOEAIQbg"));
        this.achievementsMap.put("1087", new String("CgkIoKSYmJkOEAIQcA"));
        this.achievementsMap.put("1088", new String("CgkIoKSYmJkOEAIQhAE"));
        this.achievementsMap.put("1089", new String("CgkIoKSYmJkOEAIQhQE"));
        this.achievementsMap.put("1090", new String("CgkIoKSYmJkOEAIQhgE"));
        this.achievementsMap.put("1091", new String("CgkIoKSYmJkOEAIQhwE"));
        this.achievementsMap.put("1092", new String("CgkIoKSYmJkOEAIQiAE"));
        this.achievementsMap.put("1093", new String("CgkIoKSYmJkOEAIQiQE"));
        this.achievementsMap.put("1094", new String("CgkIoKSYmJkOEAIQigE"));
        this.achievementsMap.put("1095", new String("CgkIoKSYmJkOEAIQiwE"));
        this.achievementsMap.put("1096", new String("CgkIoKSYmJkOEAIQjAE"));
        this.achievementsMap.put("1097", new String("CgkIoKSYmJkOEAIQjQE"));
        this.achievementsMap.put(NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW, new String("CgkIoKSYmJkOEAIQjgE"));
        this.achievementsMap.put("1099", new String("CgkIoKSYmJkOEAIQjwE"));
        this.achievementsMap.put("1100", new String("CgkIoKSYmJkOEAIQkAE"));
        this.achievementsMap.put("1101", new String("CgkIoKSYmJkOEAIQkQE"));
        this.achievementsMap.put("1102", new String("CgkIoKSYmJkOEAIQkgE"));
        this.achievementsMap.put("1103", new String("CgkIoKSYmJkOEAIQkwE"));
        this.achievementsMap.put("1104", new String("CgkIoKSYmJkOEAIQlAE"));
        this.achievementsMap.put("1105", new String("CgkIoKSYmJkOEAIQlQE"));
        this.achievementsMap.put("1106", new String("CgkIoKSYmJkOEAIQlgE"));
        this.achievementsMap.put("1107", new String("CgkIoKSYmJkOEAIQlwE"));
        this.achievementsMap.put("1108", new String("CgkIoKSYmJkOEAIQmAE"));
        this.achievementsMap.put("1109", new String("CgkIoKSYmJkOEAIQmQE"));
        this.achievementsMap.put("1110", new String("CgkIoKSYmJkOEAIQmgE"));
        this.achievementsMap.put("1111", new String("CgkIoKSYmJkOEAIQmwE"));
        this.achievementsMap.put("1112", new String("CgkIoKSYmJkOEAIQnAE"));
        this.achievementsMap.put("1114", new String("CgkIoKSYmJkOEAIQngE"));
        this.achievementsMap.put("1115", new String("CgkIoKSYmJkOEAIQnwE"));
        this.achievementsMap.put("1116", new String("CgkIoKSYmJkOEAIQoAE"));
        this.achievementsMap.put("1117", new String("CgkIoKSYmJkOEAIQoQE"));
        this.achievementsMap.put("1118", new String("CgkIoKSYmJkOEAIQogE"));
        this.achievementsMap.put("1119", new String("CgkIoKSYmJkOEAIQowE"));
        this.achievementsMap.put("1120", new String("CgkIoKSYmJkOEAIQpAE"));
        this.achievementsMap.put("1121", new String("CgkIoKSYmJkOEAIQpQE"));
        this.achievementsMap.put("1122", new String("CgkIoKSYmJkOEAIQpgE"));
        this.achievementsMap.put("1123", new String("CgkIoKSYmJkOEAIQpwE"));
        this.achievementsMap.put("1124", new String("CgkIoKSYmJkOEAIQqAE"));
        this.achievementsMap.put("1125", new String("CgkIoKSYmJkOEAIQqQE"));
        this.achievementsMap.put("1126", new String("CgkIoKSYmJkOEAIQqgE"));
        this.achievementsMap.put("1127", new String("CgkIoKSYmJkOEAIQqwE"));
        this.achievementsMap.put("1128", new String("CgkIoKSYmJkOEAIQrAE"));
        this.achievementsMap.put("1129", new String("CgkIoKSYmJkOEAIQrQE"));
        this.achievementsMap.put("1130", new String("CgkIoKSYmJkOEAIQrgE"));
        this.achievementsMap.put("1131", new String("CgkIoKSYmJkOEAIQrwE"));
        this.achievementsMap.put("1132", new String("CgkIoKSYmJkOEAIQsAE"));
        this.achievementsMap.put("1133", new String("CgkIoKSYmJkOEAIQsQE"));
        this.achievementsMap.put("1134", new String("CgkIoKSYmJkOEAIQsgE"));
        this.achievementsMap.put("1135", new String("CgkIoKSYmJkOEAIQswE"));
        this.achievementsMap.put("1136", new String("CgkIoKSYmJkOEAIQtAE"));
        this.achievementsMap.put("1137", new String("CgkIoKSYmJkOEAIQtQE"));
        this.achievementsMap.put("1138", new String("CgkIoKSYmJkOEAIQtgE"));
        this.achievementsMap.put("1139", new String("CgkIoKSYmJkOEAIQtwE"));
        this.achievementsMap.put("1140", new String("CgkIoKSYmJkOEAIQuAE"));
        this.achievementsMap.put("1141", new String("CgkIoKSYmJkOEAIQuQE"));
        this.achievementsMap.put("1142", new String("CgkIoKSYmJkOEAIQugE"));
        this.achievementsMap.put("1143", new String("CgkIoKSYmJkOEAIQuwE"));
        this.achievementsMap.put("1144", new String("CgkIoKSYmJkOEAIQvAE"));
        this.achievementsMap.put("1145", new String("CgkIoKSYmJkOEAIQvQE"));
        this.achievementsMap.put("1146", new String("CgkIoKSYmJkOEAIQvgE"));
        this.achievementsMap.put("1147", new String("CgkIoKSYmJkOEAIQvwE"));
        this.achievementsMap.put("1148", new String("CgkIoKSYmJkOEAIQwAE"));
        this.achievementsMap.put("1149", new String("CgkIoKSYmJkOEAIQwQE"));
        this.achievementsMap.put("1150", new String("CgkIoKSYmJkOEAIQwgE"));
        this.achievementsMap.put("1151", new String("CgkIoKSYmJkOEAIQwwE"));
        this.achievementsMap.put("1152", new String("CgkIoKSYmJkOEAIQxAE"));
        this.achievementsMap.put("1153", new String("CgkIoKSYmJkOEAIQxQE"));
        this.achievementsMap.put("1154", new String("CgkIoKSYmJkOEAIQxgE"));
        this.achievementsMap.put("1155", new String("CgkIoKSYmJkOEAIQxwE"));
        this.achievementsMap.put("1156", new String("CgkIoKSYmJkOEAIQyAE"));
        this.achievementsMap.put("1157", new String("CgkIoKSYmJkOEAIQyQE"));
        this.achievementsMap.put("1158", new String("CgkIoKSYmJkOEAIQygE"));
        this.achievementsMap.put("1159", new String("CgkIoKSYmJkOEAIQywE"));
        this.achievementsMap.put("1160", new String("CgkIoKSYmJkOEAIQzAE"));
        this.achievementsMap.put("1161", new String("CgkIoKSYmJkOEAIQzQE"));
        this.achievementsMap.put("1162", new String("CgkIoKSYmJkOEAIQzgE"));
        this.achievementsMap.put("1163", new String("CgkIoKSYmJkOEAIQzwE"));
        this.achievementsMap.put("1164", new String("CgkIoKSYmJkOEAIQ0AE"));
        this.achievementsMap.put("1165", new String("CgkIoKSYmJkOEAIQ0QE"));
        this.achievementsMap.put("1166", new String("CgkIoKSYmJkOEAIQ0gE"));
        this.achievementsMap.put("1167", new String("CgkIoKSYmJkOEAIQ1gE"));
        this.achievementsMap.put("1168", new String("CgkIoKSYmJkOEAIQ0wE"));
        this.achievementsMap.put("1169", new String("CgkIoKSYmJkOEAIQ1AE"));
        this.achievementsMap.put("1170", new String("CgkIoKSYmJkOEAIQ1QE"));
        this.leaderboardsMap = new HashMap<>();
        this.leaderboardsMap.put("HIGHSCORE", new String("CgkIoKSYmJkOEAIQNQ"));
        this.leaderboardsMap.put("speedrun_bacteria", new String("CgkIoKSYmJkOEAIQOQ"));
        this.leaderboardsMap.put("speedrun_virus", new String("CgkIoKSYmJkOEAIQOg"));
        this.leaderboardsMap.put("speedrun_parasite", new String("CgkIoKSYmJkOEAIQOw"));
        this.leaderboardsMap.put("speedrun_fungus", new String("CgkIoKSYmJkOEAIQPA"));
        this.leaderboardsMap.put("speedrun_prion", new String("CgkIoKSYmJkOEAIQPQ"));
        this.leaderboardsMap.put("speedrun_rogue_nanobot", new String("CgkIoKSYmJkOEAIQPg"));
        this.leaderboardsMap.put("speedrun_escaped_bio_weapon", new String("CgkIoKSYmJkOEAIQPw"));
        this.leaderboardsMap.put("speedrun_neurax", new String("CgkIoKSYmJkOEAIQQA"));
        this.leaderboardsMap.put("speedrun_zombie", new String("CgkIoKSYmJkOEAIQQQ"));
        this.mHelper = new GooglePlayServices(this);
        this.mHelper.setup(this);
        GameServices.setup(this, this);
        CloudServices.setup(this, this);
        OpenUDID.initialize(this);
        MCVideoAds.init(this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mHelper.onStart(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mHelper.onStop();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.mHelper.onActivityResult(i, i2, intent);
    }

    public void onSignInFailed() {
        Log.i("PlagueIncActivity", "onSignInFailed");
    }

    public void onSignInSucceeded() {
        Log.i("PlagueIncActivity", "onSignInSucceeded");
        queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                GameServices.MonGameServicesLoginComplete();
            }
        });
    }

    public int gameServices_isSignedIn() {
        return this.mHelper.isSignedIn() ? 1 : 0;
    }

    public void gameServices_login() {
        this.mHelper.signIn();
    }

    public void gameServices_logout() {
        this.mHelper.signOut();
    }

    public void gameServices_updateScore(String str, long j, Object obj) {
        if (this.leaderboardsMap.containsKey(str)) {
            this.mHelper.updateScore(this.leaderboardsMap.get(str), j, obj);
        }
    }

    public void gameServices_showLeaderboard(String str) {
        if (this.leaderboardsMap.containsKey(str)) {
            this.mHelper.showLeaderboard(this.leaderboardsMap.get(str));
        }
    }

    public void gameServices_showLeaderboards() {
        this.mHelper.showLeaderboards();
    }

    public void gameServices_updateAchievement(String str, float f, Object obj) {
        if (this.achievementsMap.containsKey(str)) {
            this.mHelper.unlockAchievement(this.achievementsMap.get(str));
        }
    }

    public void gameServices_showAchievements() {
        this.mHelper.showAchievements();
    }

    public String gameServices_getCurrentPlayerName() {
        return this.mHelper.getCurrentPlayerName();
    }

    public String gameServices_getDeviceId() {
        try {
            return Settings.Secure.getString(getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void gameServices_loadLeaderboardScores(final int i, final int i2, String str, int i3, int i4, int i5) {
        if (!this.leaderboardsMap.containsKey(str)) {
            queueEvent(ThreadingContext.GlThread, new Runnable() {
                final String[] arr = new String[0];

                public void run() {
                    GameServices.MonGameServicesLeaderboardScoredLoaded(this.arr, 0, i, i2);
                }
            });
            return;
        }
        this.mHelper.loadTopScores(new GooglePlayServices.OnLeaderboardScoresLoadedListener() {
            public void onLeaderboardScoresLoaded(ArrayList<GooglePlayServices.LeaderboardEntry> arrayList) {
                final String[] strArr = new String[(arrayList.size() * 2)];
                Iterator<GooglePlayServices.LeaderboardEntry> it = arrayList.iterator();
                final int i = 0;
                while (it.hasNext()) {
                    GooglePlayServices.LeaderboardEntry next = it.next();
                    strArr[i] = next.displayName;
                    strArr[i + 1] = next.displayScore;
                    i += 2;
                }
                PlagueIncActivity.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        GameServices.MonGameServicesLeaderboardScoredLoaded(strArr, i, i, i2);
                    }
                });
            }
        }, this.leaderboardsMap.get(str), i3, i4, i5);
    }

    public void gameServices_loadAchievements(final int i, final int i2) {
        this.mHelper.loadAchievements(new GooglePlayServices.OnAchievementsLoadedListener() {
            public void onAchievementsLoaded(ArrayList<GooglePlayServices.AchievementEntry> arrayList) {
                final String[] strArr = new String[(arrayList.size() * 4)];
                Iterator<GooglePlayServices.AchievementEntry> it = arrayList.iterator();
                final int i = 0;
                while (it.hasNext()) {
                    GooglePlayServices.AchievementEntry next = it.next();
                    String str = next.achievementId;
                    if (PlagueIncActivity.this.achievementsMap.containsValue(str)) {
                        Iterator<Map.Entry<String, String>> it2 = PlagueIncActivity.this.achievementsMap.entrySet().iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            Map.Entry next2 = it2.next();
                            if (str.equals(next2.getValue())) {
                                str = (String) next2.getKey();
                                break;
                            }
                        }
                        strArr[i] = str;
                        strArr[i + 1] = next.name;
                        strArr[i + 2] = next.description;
                        strArr[i + 3] = Integer.toString(next.state);
                        i += 4;
                    }
                }
                PlagueIncActivity.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        GameServices.MonGameServicesAchievementsLoaded(strArr, i, i, i2);
                    }
                });
            }
        });
    }

    public void cloudServices_load(final int i, final int i2) {
        this.mHelper.loadCloudSave(new GooglePlayServices.OnStateLoadedListener() {
            public void onStateConflict(int i, final String str, byte[] bArr, byte[] bArr2) {
                Log.i("PlagueIncActivity", "Cloud save loaded with CONFLICT");
                final String str2 = new String(bArr);
                final String str3 = new String(bArr2);
                PlagueIncActivity.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        CloudServices.MonCloudServicesDataLoaded(i, i2, 0, str3, 1, str2, str, 1);
                    }
                });
            }

            public void onStateLoaded(final int i, int i2, byte[] bArr) {
                Log.i("PlagueIncActivity", "Cloud save loaded with error code :: " + i);
                final String str = i == 0 ? new String(bArr) : "";
                PlagueIncActivity.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        CloudServices.MonCloudServicesDataLoaded(i, i2, i, str, 0, "", "", 1);
                    }
                });
            }
        }, 0);
    }

    public void cloudServices_update(final int i, final int i2, String str) {
        this.mHelper.updateCloudSaveImmediate(new GooglePlayServices.OnStateLoadedListener() {
            public void onStateConflict(int i, final String str, byte[] bArr, byte[] bArr2) {
                Log.i("PlagueIncActivity", "Cloud save updated with CONFLICT");
                final String str2 = new String(bArr);
                final String str3 = new String(bArr2);
                PlagueIncActivity.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        CloudServices.MonCloudServicesDataLoaded(i, i2, 0, str3, 1, str2, str, 0);
                    }
                });
            }

            public void onStateLoaded(int i, int i2, byte[] bArr) {
                Log.i("PlagueIncActivity", "Cloud save updated with error code :: " + i);
            }
        }, 0, str.getBytes());
    }

    public void cloudServices_resolve(final int i, final int i2, String str, String str2, final int i3) {
        this.mHelper.resolveCloudSave(new GooglePlayServices.OnStateLoadedListener() {
            public void onStateConflict(int i, final String str, byte[] bArr, byte[] bArr2) {
                Log.i("PlagueIncActivity", "Cloud save resolve with CONFLICT");
                final String str2 = new String(bArr);
                final String str3 = new String(bArr2);
                PlagueIncActivity.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        CloudServices.MonCloudServicesDataLoaded(i, i2, 0, str3, 1, str2, str, i3);
                    }
                });
            }

            public void onStateLoaded(final int i, int i2, byte[] bArr) {
                Log.i("PlagueIncActivity", "Cloud save resolved with error code :: " + i);
                if (i3 != 0) {
                    final String str = i == 0 ? new String(bArr) : "";
                    PlagueIncActivity.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                        public void run() {
                            CloudServices.MonCloudServicesDataLoaded(i, i2, i, str, 0, "", "", i3);
                        }
                    });
                }
            }
        }, 0, str, str2.getBytes());
    }

    public void onShowDefaultView(RelativeLayout relativeLayout) {
        ImageView imageView = new ImageView(this);
        imageView.setImageDrawable(getResources().getDrawable(mContext.getResources().getIdentifier("default1", "drawable", mContext.getPackageName())));
        imageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        relativeLayout.addView(imageView);
    }
}
