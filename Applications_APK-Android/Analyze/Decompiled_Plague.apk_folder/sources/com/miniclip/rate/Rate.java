package com.miniclip.rate;

import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.Miniclip;
import com.miniclip.framework.ThreadingContext;

public class Rate {
    /* access modifiers changed from: private */
    public static native void sessionStarted();

    /* access modifiers changed from: private */
    public static native void sessionStopped();

    protected static void init() {
        Miniclip.queueEvent(ThreadingContext.Main, new Runnable() {
            public void run() {
                Rate.regiterSessionStart();
                Rate.registerSessionStop();
            }
        });
    }

    /* access modifiers changed from: private */
    public static void regiterSessionStart() {
        Miniclip.addListener(new AbstractActivityListener() {
            public void onStart() {
                Miniclip.queueEvent(ThreadingContext.Main, new Runnable() {
                    public void run() {
                        Rate.sessionStarted();
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public static void registerSessionStop() {
        Miniclip.addListener(new AbstractActivityListener() {
            public void onStop() {
                Miniclip.queueEvent(ThreadingContext.Main, new Runnable() {
                    public void run() {
                        Rate.sessionStopped();
                    }
                });
            }
        });
    }

    public static String getVersionName() {
        try {
            return Miniclip.getActivity().getPackageManager().getPackageInfo(Miniclip.getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getAppId() {
        return Miniclip.getActivity().getPackageName();
    }

    public static boolean internetConnectionStatus() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) Miniclip.getActivity().getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return activeNetworkInfo.isConnectedOrConnecting();
    }
}
