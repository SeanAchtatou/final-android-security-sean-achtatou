package com.miniclip.rate;

/* compiled from: MessageBox */
class DialogMessage {
    public String[] buttonTitles = null;
    public boolean cancelable = true;
    public String message = null;
    public int msgId;
    public String title = null;

    public DialogMessage(int i) {
        this.msgId = i;
    }
}
