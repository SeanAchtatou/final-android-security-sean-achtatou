package com.miniclip.rate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import com.miniclip.framework.Miniclip;
import com.miniclip.inapppurchases.MCInAppPurchases;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MessageBox implements DialogInterface.OnClickListener {
    private static final int HANDLER_CLOSE_DIALOG = 3;
    private static final int HANDLER_EXIT_APP = 5;
    private static final int HANDLER_HIDE_DIALOG = 2;
    private static final int HANDLER_OPEN_MARKET = 4;
    private static final int HANDLER_SHOW_DIALOG = 1;
    private static Activity mActivity;
    private static Handler mHandler;
    /* access modifiers changed from: private */
    public HashMap<Integer, Dialog> mDialogs = new HashMap<>();
    long mMessageBoxHelperObjectReference;

    private native void handleButtonPressNative(long j, int i, int i2);

    private native void ratePopupDidAppear(long j);

    private native void ratePopupDidDisappear(long j);

    private native void ratePopupWillAppear(long j);

    private native void ratePopupWillDisappear(long j);

    /* access modifiers changed from: package-private */
    public int buttonValue2Int(int i) {
        switch (i) {
            case -3:
                return 2;
            case -2:
                return 1;
            case -1:
                return 0;
            default:
                return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public int int2ButtonValue(int i) {
        switch (i) {
            case 0:
                return -1;
            case 1:
                return -2;
            case 2:
                return -3;
            default:
                return -2;
        }
    }

    MessageBox() {
        mActivity = Miniclip.getActivity();
        mHandler = new MyHandler(this);
    }

    public void showMessageBox(long j, int i, String str, String str2, boolean z, String[] strArr) {
        this.mMessageBoxHelperObjectReference = j;
        DialogMessage dialogMessage = new DialogMessage(i);
        dialogMessage.title = str;
        dialogMessage.message = str2;
        dialogMessage.cancelable = z;
        dialogMessage.buttonTitles = strArr;
        Message message = new Message();
        message.what = 1;
        message.obj = dialogMessage;
        mHandler.sendMessage(message);
    }

    public void hideMessageBox(int i) {
        Message message = new Message();
        message.what = 2;
        message.obj = new DialogMessage(i);
        mHandler.sendMessage(message);
    }

    public void closeMessageBox(int i) {
        Message message = new Message();
        message.what = 3;
        message.obj = new DialogMessage(i);
        mHandler.sendMessage(message);
    }

    public void openAppMarketPage(String str) {
        Message message = new Message();
        message.what = 4;
        message.obj = str;
        mHandler.sendMessage(message);
    }

    /* access modifiers changed from: private */
    public void openAppMarketPageOnPlayStoreOrBrowser(String str) {
        Intent intent;
        Intent intent2;
        if (MCInAppPurchases.AMAZON_NAME.equals(Build.MANUFACTURER)) {
            intent = new Intent("android.intent.action.VIEW", Uri.parse("amzn://apps/android?p=" + str));
        } else {
            intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + str + "&hl=en"));
        }
        boolean z = false;
        List<ResolveInfo> queryIntentActivities = mActivity.getPackageManager().queryIntentActivities(intent, 0);
        if (!MCInAppPurchases.AMAZON_NAME.equals(Build.MANUFACTURER) || queryIntentActivities.isEmpty()) {
            Iterator<ResolveInfo> it = queryIntentActivities.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ResolveInfo next = it.next();
                if (next.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {
                    ActivityInfo activityInfo = next.activityInfo;
                    ComponentName componentName = new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name);
                    intent.setFlags(270532608);
                    intent.setComponent(componentName);
                    mActivity.startActivity(intent);
                    z = true;
                    break;
                }
            }
            if (!z) {
                if (MCInAppPurchases.AMAZON_NAME.equals(Build.MANUFACTURER)) {
                    intent2 = new Intent("android.intent.action.VIEW", Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=" + str));
                } else {
                    intent2 = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + str));
                }
                mActivity.startActivity(intent2);
                return;
            }
            return;
        }
        mActivity.startActivity(intent);
    }

    private class MyHandler extends Handler {
        private final WeakReference<MessageBox> _messageBox;

        public MyHandler(MessageBox messageBox) {
            super(Looper.getMainLooper());
            this._messageBox = new WeakReference<>(messageBox);
        }

        public void handleMessage(Message message) {
            if (this._messageBox.get() == null) {
                Log.d("MessageBox", "MessageBox: weakReference object is null!");
                return;
            }
            switch (message.what) {
                case 1:
                    DialogMessage dialogMessage = (DialogMessage) message.obj;
                    this._messageBox.get().showDialog(dialogMessage.msgId, dialogMessage.title, dialogMessage.message, dialogMessage.cancelable, dialogMessage.buttonTitles);
                    return;
                case 2:
                    this._messageBox.get().hideDialog(((DialogMessage) message.obj).msgId);
                    return;
                case 3:
                    this._messageBox.get().closeDialog(((DialogMessage) message.obj).msgId);
                    return;
                case 4:
                    MessageBox.this.openAppMarketPageOnPlayStoreOrBrowser((String) message.obj);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void showDialog(int i, String str, String str2, final boolean z, String[] strArr) {
        Dialog dialog;
        if (!this.mDialogs.containsKey(Integer.valueOf(i))) {
            int i2 = 0;
            if (strArr.length < 4) {
                AlertDialog.Builder message = new AlertDialog.Builder(mActivity).setTitle(str).setMessage(str2);
                while (i2 < strArr.length) {
                    if (int2ButtonValue(i2) == -1) {
                        message.setPositiveButton(strArr[i2], this);
                    } else if (int2ButtonValue(i2) == -2) {
                        message.setNegativeButton(strArr[i2], this);
                    } else if (int2ButtonValue(i2) == -3) {
                        message.setNeutralButton(strArr[i2], this);
                    }
                    i2++;
                }
                dialog = message.create();
            } else {
                AlertDialog create = new AlertDialog.Builder(mActivity).create();
                create.setTitle(str);
                AlertDialog alertDialog = create;
                alertDialog.setMessage(str2);
                while (i2 < strArr.length) {
                    alertDialog.setButton(int2ButtonValue(i2), strArr[i2], this);
                    i2++;
                }
                dialog = create;
            }
            this.mDialogs.put(Integer.valueOf(i), dialog);
        } else {
            dialog = this.mDialogs.get(Integer.valueOf(i));
        }
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (!z || i != 4 || keyEvent.getAction() != 0) {
                    return false;
                }
                Log.i("key down", "KEYCODE_BACK");
                for (Map.Entry entry : MessageBox.this.mDialogs.entrySet()) {
                    if (entry.getValue() == dialogInterface) {
                        MessageBox.this.closeDialog(((Integer) entry.getKey()).intValue());
                        return true;
                    }
                }
                return false;
            }
        });
        dialog.setCancelable(z);
        ratePopupWillAppear(this.mMessageBoxHelperObjectReference);
        try {
            dialog.show();
        } catch (Exception e) {
            Log.d("MessageBox", "Failed to show dialog!!");
            e.printStackTrace();
        }
        ratePopupDidAppear(this.mMessageBoxHelperObjectReference);
    }

    /* access modifiers changed from: package-private */
    public void hideDialog(int i) {
        closeDialog(i);
    }

    /* access modifiers changed from: package-private */
    public void closeDialog(int i) {
        ratePopupWillDisappear(this.mMessageBoxHelperObjectReference);
        if (this.mDialogs.containsKey(Integer.valueOf(i))) {
            Dialog dialog = this.mDialogs.get(Integer.valueOf(i));
            if (dialog != null) {
                dialog.dismiss();
            }
            ratePopupDidDisappear(this.mMessageBoxHelperObjectReference);
            this.mDialogs.remove(Integer.valueOf(i));
        }
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        for (Map.Entry next : this.mDialogs.entrySet()) {
            if (next.getValue() == dialogInterface) {
                ratePopupWillDisappear(this.mMessageBoxHelperObjectReference);
                handleButtonPressNative(this.mMessageBoxHelperObjectReference, ((Integer) next.getKey()).intValue(), i);
                return;
            }
        }
    }
}
