package com.miniclip.inapppurchases.providers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.googlebilling.IabHelper;
import com.miniclip.googlebilling.IabResult;
import com.miniclip.googlebilling.Inventory;
import com.miniclip.googlebilling.Purchase;
import com.miniclip.googlebilling.SkuDetails;
import com.miniclip.inapppurchases.MCInAppPurchases;
import com.miniclip.inapppurchases.ProviderWrapper;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import org.json.JSONException;

public class GoogleWrapper extends AbstractActivityListener implements ProviderWrapper {
    /* access modifiers changed from: private */
    public static final String TAG = "GoogleWrapper";
    /* access modifiers changed from: private */
    public MiniclipAndroidActivity _activity = null;
    /* access modifiers changed from: private */
    public String _publicKey;
    /* access modifiers changed from: private */
    public boolean mBusy = false;
    /* access modifiers changed from: private */
    public IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult iabResult) {
            String access$100 = GoogleWrapper.TAG;
            Log.d(access$100, "Consumption finished. Purchase: " + purchase + ", result: " + iabResult);
            if (iabResult.getResponse() == 8) {
                MCInAppPurchases.setItemOwned(MCInAppPurchases.GOOGLE_NAME, purchase.getSku(), false);
            }
            if (iabResult.isSuccess()) {
                Log.d(GoogleWrapper.TAG, "Consumption successful. Provisioning.");
                MCInAppPurchases.setItemOwned(MCInAppPurchases.GOOGLE_NAME, purchase.getSku(), false);
                SharedPreferences.Editor edit = GoogleWrapper.this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0).edit();
                edit.putBoolean(purchase.getSku() + "_IS_PENDING", false);
                edit.commit();
                synchronized (GoogleWrapper.this.pendingSkus) {
                    GoogleWrapper.this.pendingSkus.remove(purchase.getSku());
                }
            } else {
                GoogleWrapper googleWrapper = GoogleWrapper.this;
                googleWrapper.complain("Error while consuming: " + iabResult);
            }
            Log.d(GoogleWrapper.TAG, "End consumption flow.");
            GoogleWrapper.this.setWaitScreen(false);
            boolean unused = GoogleWrapper.this.mBusy = false;
        }
    };
    /* access modifiers changed from: private */
    public IabHelper mHelper;
    /* access modifiers changed from: private */
    public boolean mHelperReady = false;
    /* access modifiers changed from: private */
    public String mInAppProductId;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult iabResult, Purchase purchase) {
            GoogleWrapper.this.purchaseFinishedCallback(iabResult, purchase, false);
        }
    };
    /* access modifiers changed from: private */
    public IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListenerManaged = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult iabResult, Purchase purchase) {
            GoogleWrapper.this.purchaseFinishedCallback(iabResult, purchase, true);
        }
    };
    /* access modifiers changed from: private */
    public Set<String> pendingSkus = new HashSet();
    private Queue<Runnable> queue = new ArrayDeque();
    /* access modifiers changed from: private */
    public Set<String> registeredSkus = new HashSet();

    public GoogleWrapper(MiniclipAndroidActivity miniclipAndroidActivity, String str) {
        this._activity = miniclipAndroidActivity;
        this._publicKey = str;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        String str = TAG;
        Log.d(str, "onActivityResult(" + i + "," + i2 + "," + intent);
        if (this.mHelper.handleActivityResult(i, i2, intent)) {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    public void register(final List<String> list) {
        this._activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                if (!GoogleWrapper.this.mHelperReady) {
                    Log.d(GoogleWrapper.TAG, "Creating IAB helper.");
                    IabHelper unused = GoogleWrapper.this.mHelper = new IabHelper(GoogleWrapper.this._activity, GoogleWrapper.this._publicKey);
                    GoogleWrapper.this.mHelper.enableDebugLogging(false);
                    Log.d(GoogleWrapper.TAG, "Starting setup.");
                    GoogleWrapper.this.mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                        public void onIabSetupFinished(IabResult iabResult) {
                            Log.d(GoogleWrapper.TAG, "Setup finished.");
                            if (iabResult.isSuccess()) {
                                Log.d(GoogleWrapper.TAG, "Setup successful.");
                                boolean unused = GoogleWrapper.this.mHelperReady = true;
                                GoogleWrapper.this._activity.addListener(GoogleWrapper.this);
                                boolean unused2 = GoogleWrapper.this.mBusy = true;
                                GoogleWrapper.this.mHelper.queryInventoryAsync(true, list, new QueryInventoryFinishedListener(list));
                                return;
                            }
                            GoogleWrapper googleWrapper = GoogleWrapper.this;
                            googleWrapper.complain("Problem setting up in-app billing: " + iabResult);
                            GoogleWrapper.this.onRegisterProviderResult(false, list);
                        }
                    });
                    return;
                }
                boolean unused2 = GoogleWrapper.this.mBusy = true;
                GoogleWrapper.this.mHelper.queryInventoryAsync(true, list, new QueryInventoryFinishedListener(list));
            }
        });
    }

    public void restorePurchases() {
        if (!this.mHelperReady) {
            Log.w(TAG, "requestRestore not ready - ");
            onPurchasesRestoredResult(false, null, null, null);
            return;
        }
        final AnonymousClass2 r0 = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {
                boolean z;
                if (iabResult == null || iabResult.isFailure()) {
                    GoogleWrapper googleWrapper = GoogleWrapper.this;
                    googleWrapper.complain("Failed to query inventory: " + iabResult);
                    z = false;
                } else {
                    Log.d(GoogleWrapper.TAG, "Query inventory was successful.");
                    z = true;
                    GoogleWrapper.this.syncInventory(inventory);
                    Log.d(GoogleWrapper.TAG, "Inventory query finished.");
                }
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                if (!(inventory == null || inventory.getAllPurchases() == null)) {
                    for (Purchase next : inventory.getAllPurchases()) {
                        if (next.getPurchaseState() == 0) {
                            arrayList.add(next.getSku());
                            arrayList2.add(next.getOriginalJson());
                            arrayList3.add(next.getSignature());
                        }
                    }
                }
                boolean unused = GoogleWrapper.this.mBusy = false;
                GoogleWrapper.this.onPurchasesRestoredResult(z, arrayList, arrayList2, arrayList3);
            }
        };
        this._activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                if (!GoogleWrapper.this.mBusy) {
                    Log.d(GoogleWrapper.TAG, "Restoring purchases.");
                    boolean unused = GoogleWrapper.this.mBusy = true;
                    GoogleWrapper.this.mHelper.queryInventoryAsync(true, new ArrayList(GoogleWrapper.this.registeredSkus), r0);
                }
            }
        });
    }

    public void refreshPurchases() {
        final AnonymousClass4 r0 = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {
                if (iabResult == null || iabResult.isFailure()) {
                    GoogleWrapper googleWrapper = GoogleWrapper.this;
                    googleWrapper.complain("Failed to query inventory: " + iabResult);
                } else {
                    Log.d(GoogleWrapper.TAG, "Query inventory was successful.");
                    GoogleWrapper.this.syncInventory(inventory);
                }
                boolean unused = GoogleWrapper.this.mBusy = false;
                GoogleWrapper.this.requestPendingPurchases();
            }
        };
        this._activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                if (!GoogleWrapper.this.mBusy) {
                    Log.d(GoogleWrapper.TAG, "Refreshing purchases.");
                    boolean unused = GoogleWrapper.this.mBusy = true;
                    GoogleWrapper.this.mHelper.queryInventoryAsync(true, new ArrayList(GoogleWrapper.this.registeredSkus), r0);
                }
            }
        });
    }

    public void requestPendingPurchases() {
        onPendingPurchasesResult();
    }

    private void onPendingPurchasesResult() {
        synchronized (this.pendingSkus) {
            for (String next : this.pendingSkus) {
                String str = TAG;
                Log.i(str, "onPendingPurchasesResult pending: " + next);
                SharedPreferences sharedPreferences = this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0);
                String string = sharedPreferences.getString(next + "_DATA_UNIQUE", "");
                onPurchaseResponse(MCInAppPurchases.PURCHASE_SUCCESS, next, string, sharedPreferences.getString(next + "_SIGNATURE_UNIQUE", ""));
            }
        }
    }

    private class QueryInventoryFinishedListener implements IabHelper.QueryInventoryFinishedListener {
        Collection<String> skus;

        QueryInventoryFinishedListener(Collection<String> collection) {
            this.skus = collection;
        }

        public void onQueryInventoryFinished(IabResult iabResult, final Inventory inventory) {
            final boolean z;
            if (iabResult == null || iabResult.isFailure()) {
                GoogleWrapper googleWrapper = GoogleWrapper.this;
                googleWrapper.complain("Failed to query inventory: " + iabResult);
                z = false;
            } else {
                Log.d(GoogleWrapper.TAG, "Query inventory was successful.");
                z = true;
                GoogleWrapper.this.syncInventory(inventory);
                Log.d(GoogleWrapper.TAG, "Initial inventory query finished.");
            }
            GoogleWrapper.this._activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                public void run() {
                    HashSet hashSet = new HashSet(QueryInventoryFinishedListener.this.skus);
                    SharedPreferences.Editor edit = GoogleWrapper.this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0).edit();
                    List asList = Arrays.asList(MCInAppPurchases.getConsumableSkus(GoogleWrapper.this.CollectionToArray(GoogleWrapper.this.registeredSkus)));
                    if (!(inventory == null || inventory.getAllSkuDetails() == null)) {
                        for (SkuDetails next : inventory.getAllSkuDetails()) {
                            boolean contains = asList.contains(next.getSku());
                            edit.putBoolean(next.getSku() + "_IS_CONSUMABLE", contains);
                            hashSet.remove(next.getSku());
                        }
                    }
                    edit.commit();
                    boolean unused = GoogleWrapper.this.mBusy = false;
                    GoogleWrapper.this.onRegisterProviderResult(z, hashSet);
                    GoogleWrapper.this.requestPendingPurchases();
                }
            });
        }
    }

    public void requestPurchase(final String str, final boolean z) {
        this._activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                if (!GoogleWrapper.this.mBusy) {
                    String unused = GoogleWrapper.this.mInAppProductId = str;
                    String access$100 = GoogleWrapper.TAG;
                    Log.i(access$100, "requestPurchase - " + str);
                    if (!GoogleWrapper.this.mHelperReady) {
                        String access$1002 = GoogleWrapper.TAG;
                        Log.w(access$1002, "requestPurchase not ready - " + str);
                        GoogleWrapper.this.onPurchaseResponse(MCInAppPurchases.PURCHASE_UNKNOWN_ERROR, str, "", "");
                        return;
                    }
                    SharedPreferences sharedPreferences = GoogleWrapper.this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putBoolean(str + "_IS_CONSUMABLE", !z);
                    edit.putBoolean(str + "_IS_PENDING", true);
                    edit.commit();
                    if (MCInAppPurchases.isItemOwned(MCInAppPurchases.GOOGLE_NAME, str)) {
                        String access$1003 = GoogleWrapper.TAG;
                        Log.i(access$1003, "requestPurchaseAct restore: " + GoogleWrapper.this.mInAppProductId);
                        String string = sharedPreferences.getString(str + "_DATA_UNIQUE", "");
                        GoogleWrapper.this.onPurchaseResponse(MCInAppPurchases.PURCHASE_SUCCESS, str, string, sharedPreferences.getString(str + "_SIGNATURE_UNIQUE", ""));
                        return;
                    }
                    boolean unused2 = GoogleWrapper.this.mBusy = true;
                    GoogleWrapper.this.setWaitScreen(true);
                    if (z) {
                        GoogleWrapper.this.mHelper.launchPurchaseFlow(GoogleWrapper.this._activity, str, GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED, GoogleWrapper.this.mPurchaseFinishedListenerManaged);
                    } else {
                        GoogleWrapper.this.mHelper.launchPurchaseFlow(GoogleWrapper.this._activity, str, GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED, GoogleWrapper.this.mPurchaseFinishedListener);
                    }
                }
            }
        });
    }

    public String[] getPurchaseReceipt(String str) {
        SharedPreferences sharedPreferences = this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0);
        String string = sharedPreferences.getString(str + "_DATA_UNIQUE", "");
        return new String[]{string, sharedPreferences.getString(str + "_SIGNATURE_UNIQUE", "")};
    }

    public void finishPurchase(final String str) {
        this._activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                boolean z;
                if (!GoogleWrapper.this.mBusy) {
                    if (MCInAppPurchases.isItemOwned(MCInAppPurchases.GOOGLE_NAME, str)) {
                        SharedPreferences sharedPreferences = GoogleWrapper.this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0);
                        z = sharedPreferences.getBoolean(str + "_IS_CONSUMABLE", false);
                        if (z) {
                            try {
                                Purchase purchase = new Purchase(IabHelper.ITEM_TYPE_INAPP, sharedPreferences.getString(str + "_DATA_UNIQUE", ""), sharedPreferences.getString(str + "_SIGNATURE_UNIQUE", ""));
                                boolean unused = GoogleWrapper.this.mBusy = true;
                                GoogleWrapper.this.setWaitScreen(true);
                                GoogleWrapper.this.mHelper.consumeAsync(purchase, GoogleWrapper.this.mConsumeFinishedListener);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        z = false;
                    }
                    if (!z) {
                        SharedPreferences.Editor edit = GoogleWrapper.this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0).edit();
                        edit.putBoolean(str + "_IS_PENDING", false);
                        edit.putBoolean(str + "_COMPLETED_PROCESS", true);
                        edit.commit();
                        synchronized (GoogleWrapper.this.pendingSkus) {
                            GoogleWrapper.this.pendingSkus.remove(str);
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void purchaseFinishedCallback(IabResult iabResult, Purchase purchase, boolean z) {
        int i;
        String str = TAG;
        Log.d(str, "Purchase finished: " + iabResult + ", purchase: " + purchase);
        int i2 = MCInAppPurchases.PURCHASE_UNKNOWN_ERROR;
        String str2 = this.mInAppProductId;
        String str3 = "";
        String str4 = "";
        if (iabResult.isFailure()) {
            complain("Error purchasing: " + iabResult);
            String str5 = TAG;
            Log.d(str5, "purchaseFinishedCallback failed: " + this.mInAppProductId);
            if (purchase == null || purchase.getPurchaseState() == 1) {
                i = MCInAppPurchases.PURCHASE_CANCELED;
            } else {
                i = MCInAppPurchases.PURCHASE_SERVER_FAILURE;
            }
        } else {
            String str6 = TAG;
            Log.d(str6, "purchaseFinishedCallback success: " + this.mInAppProductId);
            i = MCInAppPurchases.PURCHASE_SUCCESS;
            str2 = purchase.getSku();
            str3 = purchase.getOriginalJson();
            str4 = purchase.getSignature();
            SharedPreferences.Editor edit = this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0).edit();
            edit.putString(str2 + "_DATA_UNIQUE", str3);
            edit.putString(str2 + "_SIGNATURE_UNIQUE", str4);
            edit.putBoolean(str2 + "_IS_CONSUMABLE", !z);
            edit.commit();
            MCInAppPurchases.setItemOwned(MCInAppPurchases.GOOGLE_NAME, str2, true);
        }
        setWaitScreen(false);
        this.mBusy = false;
        onPurchaseResponse(i, str2, str3, str4);
    }

    /* access modifiers changed from: private */
    public void syncInventory(Inventory inventory) {
        if (inventory == null) {
            Log.e(TAG, "syncInventory error: Inventory is null");
            return;
        }
        SharedPreferences sharedPreferences = this._activity.getSharedPreferences("INAPP_PURCHASED_OWNED_EXTRAv3", 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        List<Purchase> allPurchases = inventory.getAllPurchases();
        for (int i = 0; i < allPurchases.size(); i++) {
            if (allPurchases.get(i).getPurchaseState() == 0) {
                String sku = allPurchases.get(i).getSku();
                Purchase purchase = inventory.getPurchase(sku);
                edit.putString(sku + "_DATA_UNIQUE", purchase.getOriginalJson());
                edit.putString(sku + "_SIGNATURE_UNIQUE", purchase.getSignature());
                boolean z = sharedPreferences.getBoolean(sku + "_IS_PENDING", false);
                if (!sharedPreferences.getBoolean(sku + "_COMPLETED_PROCESS", false)) {
                    edit.putBoolean(sku + "_IS_PENDING", true);
                    z = true;
                }
                if (z) {
                    synchronized (this.pendingSkus) {
                        this.pendingSkus.add(sku);
                    }
                }
                MCInAppPurchases.setItemOwned(MCInAppPurchases.GOOGLE_NAME, sku, true);
            }
        }
        edit.commit();
        List<SkuDetails> allSkuDetails = inventory.getAllSkuDetails();
        for (int i2 = 0; i2 < allSkuDetails.size(); i2++) {
            String sku2 = allSkuDetails.get(i2).getSku();
            this.registeredSkus.add(sku2);
            MCInAppPurchases.setItemPrice(MCInAppPurchases.GOOGLE_NAME, sku2, allSkuDetails.get(i2).getPrice());
            MCInAppPurchases.setItemPriceAmountMicros(MCInAppPurchases.GOOGLE_NAME, sku2, allSkuDetails.get(i2).getPriceAmountMicros());
            MCInAppPurchases.setItemCurrencyCode(MCInAppPurchases.GOOGLE_NAME, sku2, allSkuDetails.get(i2).getPriceCurrencyCode());
        }
    }

    /* access modifiers changed from: private */
    public void onRegisterProviderResult(boolean z, Collection<String> collection) {
        MCInAppPurchases.signalRegisterProviderResult(z, CollectionToArray(collection));
    }

    /* access modifiers changed from: private */
    public void onPurchasesRestoredResult(boolean z, List<String> list, List<String> list2, List<String> list3) {
        MCInAppPurchases.signalPurchasesRestoredResult(z, CollectionToArray(list), CollectionToArray(list2), CollectionToArray(list3));
    }

    /* access modifiers changed from: private */
    public void onPurchaseResponse(int i, String str, String str2, String str3) {
        MCInAppPurchases.signalPurchaseResponse(i, str, str2, str3);
    }

    /* access modifiers changed from: private */
    public String[] CollectionToArray(Collection<String> collection) {
        if (collection == null || collection.size() == 0) {
            return null;
        }
        return (String[]) collection.toArray(new String[collection.size()]);
    }

    /* access modifiers changed from: private */
    public void setWaitScreen(final boolean z) {
        this._activity.runOnUiThread(new Runnable() {
            public void run() {
                if (z) {
                    ProgressDialog unused = GoogleWrapper.this.mProgressDialog = ProgressDialog.show(GoogleWrapper.this._activity, "Processing Transaction", "Please Wait");
                } else if (GoogleWrapper.this.mProgressDialog != null) {
                    GoogleWrapper.this.mProgressDialog.dismiss();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void complain(String str) {
        String str2 = TAG;
        Log.e(str2, "**** InAppActivity Error: " + str);
    }
}
