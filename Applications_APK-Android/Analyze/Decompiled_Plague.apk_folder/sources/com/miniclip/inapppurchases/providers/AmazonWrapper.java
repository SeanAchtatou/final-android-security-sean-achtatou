package com.miniclip.inapppurchases.providers;

import android.content.SharedPreferences;
import android.util.Log;
import com.amazon.device.iap.PurchasingListener;
import com.amazon.device.iap.PurchasingService;
import com.amazon.device.iap.model.FulfillmentResult;
import com.amazon.device.iap.model.Product;
import com.amazon.device.iap.model.ProductDataResponse;
import com.amazon.device.iap.model.ProductType;
import com.amazon.device.iap.model.PurchaseResponse;
import com.amazon.device.iap.model.PurchaseUpdatesResponse;
import com.amazon.device.iap.model.Receipt;
import com.amazon.device.iap.model.UserDataResponse;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.inapppurchases.MCInAppPurchases;
import com.miniclip.inapppurchases.ProviderWrapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AmazonWrapper implements ProviderWrapper, PurchasingListener {
    private static String settingsTag = "InAppPurchasesInfo";
    private MiniclipAndroidActivity _activity = null;
    /* access modifiers changed from: private */
    public List<String> failedSkus = null;
    private boolean inited = false;
    /* access modifiers changed from: private */
    public boolean productDataFailed = false;
    private List<String> remainingSkus = null;
    private String requestedProductId = null;

    public void refreshPurchases() {
    }

    public AmazonWrapper(MiniclipAndroidActivity miniclipAndroidActivity) {
        this._activity = miniclipAndroidActivity;
    }

    /* access modifiers changed from: private */
    public boolean checkRemainingSkus() {
        if (this.remainingSkus.size() == 0) {
            return false;
        }
        HashSet hashSet = new HashSet();
        for (int i = 0; i < 100 && this.remainingSkus.size() != 0; i++) {
            hashSet.add(this.remainingSkus.remove(0));
        }
        PurchasingService.getProductData(hashSet);
        return true;
    }

    public void register(List<String> list) {
        if (!this.inited) {
            PurchasingService.registerListener(this._activity, this);
            this.inited = true;
        }
        this.remainingSkus = new ArrayList(list);
        this.failedSkus = new ArrayList();
        this.productDataFailed = false;
        checkRemainingSkus();
        PurchasingService.getUserData();
    }

    public void requestPurchase(String str, boolean z) {
        this.requestedProductId = str;
        Log.i("AmazonWrapper", "requestPurchase - " + str);
        SharedPreferences sharedPreferences = this._activity.getSharedPreferences(settingsTag, 0);
        if (sharedPreferences.getBoolean(str + "_IS_PENDING", false)) {
            Log.i("AmazonWrapper", "Purchase for product " + str + " pending...");
            return;
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(str + "_IS_PENDING", true);
        edit.commit();
        PurchasingService.purchase(str);
    }

    public void finishPurchase(String str) {
        SharedPreferences sharedPreferences = this._activity.getSharedPreferences(settingsTag, 0);
        String string = sharedPreferences.getString(str + "_RECEIPT_ID", "");
        if (string.isEmpty()) {
            Log.i("AmazonWrapper", "Failed to retrieve the receiptID for the last purchase!");
        } else {
            PurchasingService.notifyFulfillment(string, FulfillmentResult.FULFILLED);
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(str + "_RECEIPT_ID", "");
        edit.putString(str + "_RECEIPT_ID_STORED", string);
        edit.putBoolean(str + "_IS_PENDING", false);
        edit.commit();
    }

    public void restorePurchases() {
        PurchasingService.getPurchaseUpdates(true);
    }

    public void requestPendingPurchases() {
        PurchasingService.getPurchaseUpdates(false);
    }

    public void onUserDataResponse(UserDataResponse userDataResponse) {
        SharedPreferences.Editor edit = this._activity.getSharedPreferences(settingsTag, 0).edit();
        edit.putString("_USER_ID", userDataResponse.getUserData().getUserId());
        edit.commit();
    }

    public void onProductDataResponse(ProductDataResponse productDataResponse) {
        Log.i("AmazonInAppPurchase", "onProductDataResponse: " + productDataResponse.toString());
        for (Map.Entry next : productDataResponse.getProductData().entrySet()) {
            String sku = ((Product) next.getValue()).getSku();
            Log.i("AmazonInAppPurchase", "onProductDataResponse: " + sku);
            MCInAppPurchases.setItemPrice(MCInAppPurchases.AMAZON_NAME, sku, ((Product) next.getValue()).getPrice());
            MCInAppPurchases.setItemPriceAmountMicros(MCInAppPurchases.AMAZON_NAME, sku, -1000000);
            MCInAppPurchases.setItemCurrencyCode(MCInAppPurchases.AMAZON_NAME, sku, "USD");
        }
        if (productDataResponse.getRequestStatus() != ProductDataResponse.RequestStatus.SUCCESSFUL) {
            this.productDataFailed = true;
        }
        Set<String> unavailableSkus = productDataResponse.getUnavailableSkus();
        if (unavailableSkus != null && !unavailableSkus.isEmpty()) {
            for (String add : unavailableSkus) {
                this.failedSkus.add(add);
            }
        }
        this._activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                if (!AmazonWrapper.this.checkRemainingSkus()) {
                    String[] strArr = AmazonWrapper.this.failedSkus.isEmpty() ? new String[0] : (String[]) AmazonWrapper.this.failedSkus.toArray(new String[0]);
                    for (String str : strArr) {
                        Log.i("AmazonInAppPurchase", "signalRegisterProviderResult failed: " + str);
                    }
                    MCInAppPurchases.signalRegisterProviderResult(!AmazonWrapper.this.productDataFailed, strArr);
                }
            }
        });
    }

    public String[] getPurchaseReceipt(String str) {
        SharedPreferences sharedPreferences = this._activity.getSharedPreferences(settingsTag, 0);
        return new String[]{sharedPreferences.getString(str + "_RECEIPT_ID_STORED", ""), sharedPreferences.getString("_USER_ID", "")};
    }

    public void onPurchaseResponse(PurchaseResponse purchaseResponse) {
        final String str;
        final String str2;
        final String str3;
        final int i;
        int i2;
        Log.i("AmazonInAppPurchase", "onPurchaseResponse " + purchaseResponse.getRequestStatus() + ":" + purchaseResponse);
        PurchaseResponse.RequestStatus requestStatus = purchaseResponse.getRequestStatus();
        if (requestStatus == PurchaseResponse.RequestStatus.SUCCESSFUL) {
            Receipt receipt = purchaseResponse.getReceipt();
            int i3 = MCInAppPurchases.PURCHASE_SUCCESS;
            String sku = receipt.getSku();
            String receiptId = receipt.getReceiptId();
            String userId = purchaseResponse.getUserData().getUserId();
            if (receipt.getProductType() == ProductType.ENTITLED) {
                MCInAppPurchases.setItemOwned(MCInAppPurchases.AMAZON_NAME, purchaseResponse.getReceipt().getSku(), true);
            }
            i = i3;
            str3 = sku;
            str2 = receiptId;
            str = userId;
        } else if (requestStatus == PurchaseResponse.RequestStatus.ALREADY_PURCHASED) {
            String str4 = this.requestedProductId;
            int i4 = MCInAppPurchases.PURCHASE_SUCCESS;
            str = purchaseResponse.getUserData().getUserId();
            str2 = "";
            i = i4;
            str3 = str4;
        } else {
            Log.d("AmazonInAppPurchase", "onPurchaseResponse failed: " + this.requestedProductId);
            if (requestStatus == PurchaseResponse.RequestStatus.INVALID_SKU) {
                i2 = MCInAppPurchases.PURCHASE_INVALID_SKU;
            } else {
                i2 = MCInAppPurchases.PURCHASE_SERVER_FAILURE;
            }
            int i5 = i2;
            str3 = this.requestedProductId;
            i = i5;
            str2 = null;
            str = null;
        }
        this._activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCInAppPurchases.signalPurchaseResponse(i, str3, str2, str);
            }
        });
    }

    public void onPurchaseUpdatesResponse(PurchaseUpdatesResponse purchaseUpdatesResponse) {
        final String[] strArr;
        final String[] strArr2;
        final String[] strArr3;
        final boolean z;
        if (purchaseUpdatesResponse.getRequestStatus() == PurchaseUpdatesResponse.RequestStatus.SUCCESSFUL) {
            Log.i("AmazonInAppPurchase", "getPurchaseUpdatesRequestStatus :: successful");
            ArrayList<Receipt> arrayList = new ArrayList<>();
            for (Receipt next : purchaseUpdatesResponse.getReceipts()) {
                String sku = next.getSku();
                Log.v("AmazonInAppPurchase", String.format("Receipt: type: %s sku: %s", next.getProductType(), sku));
                if (next.getProductType() == ProductType.ENTITLED) {
                    MCInAppPurchases.setItemOwned(MCInAppPurchases.AMAZON_NAME, sku, true);
                }
                arrayList.add(next);
                SharedPreferences.Editor edit = this._activity.getSharedPreferences(settingsTag, 0).edit();
                edit.putString(sku + "_RECEIPT_ID", next.getReceiptId());
                edit.commit();
            }
            String[] strArr4 = new String[arrayList.size()];
            String[] strArr5 = new String[arrayList.size()];
            String[] strArr6 = new String[arrayList.size()];
            int i = 0;
            for (Receipt receipt : arrayList) {
                strArr4[i] = receipt.getSku();
                strArr5[i] = receipt.getReceiptId();
                strArr6[i] = purchaseUpdatesResponse.getUserData().getUserId();
                i++;
            }
            strArr2 = strArr5;
            strArr3 = strArr4;
            z = true;
            strArr = strArr6;
        } else {
            Log.i("AmazonInAppPurchase", "getPurchaseUpdatesRequestStatus :: failed");
            strArr3 = null;
            strArr2 = null;
            strArr = null;
            z = false;
        }
        this._activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCInAppPurchases.signalPurchasesRestoredResult(z, strArr3, strArr2, strArr);
            }
        });
        if (purchaseUpdatesResponse.getRequestStatus() == PurchaseUpdatesResponse.RequestStatus.SUCCESSFUL && purchaseUpdatesResponse.hasMore()) {
            Log.v("AmazonInAppPurchase", "has more");
            PurchasingService.getPurchaseUpdates(false);
        }
    }
}
