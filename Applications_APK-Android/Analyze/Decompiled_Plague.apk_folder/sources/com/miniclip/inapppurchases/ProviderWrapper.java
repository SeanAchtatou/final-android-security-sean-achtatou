package com.miniclip.inapppurchases;

import java.util.List;

public interface ProviderWrapper {
    void finishPurchase(String str);

    void register(List<String> list);

    void requestPendingPurchases();

    void requestPurchase(String str, boolean z);

    void restorePurchases();
}
