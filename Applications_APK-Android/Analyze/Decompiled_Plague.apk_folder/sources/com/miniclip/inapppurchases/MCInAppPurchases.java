package com.miniclip.inapppurchases;

import android.content.SharedPreferences;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.inapppurchases.providers.AmazonWrapper;
import com.miniclip.inapppurchases.providers.GoogleWrapper;
import java.util.Arrays;
import java.util.List;

public class MCInAppPurchases {
    public static final String AMAZON_NAME = "Amazon";
    public static final String GOOGLE_NAME = "Google";
    private static final String ITEM_CURRENCYCODE_FORMAT = "ITEM_CURRENCYCODES_%s";
    private static final String ITEM_PRICEAMOUNTMICROS_FORMAT = "ITEM_PRICEAMOUNTMICROS_%s";
    private static final String ITEM_PRICES_FORMAT = "ITEM_PRICES_%s";
    private static final String OWNED_ITEMS_FORMAT = "OWNED_ITEMS_%s";
    public static int PURCHASE_CANCELED = 1;
    public static int PURCHASE_INVALID_SKU = 3;
    public static int PURCHASE_SERVER_FAILURE = 2;
    public static int PURCHASE_SUCCESS = 0;
    public static int PURCHASE_UNKNOWN_ERROR = -1;
    private static MiniclipAndroidActivity _activity;
    private static AmazonWrapper _amazon;
    private static GoogleWrapper _google;

    public static native String[] getConsumableSkus(String[] strArr);

    /* access modifiers changed from: private */
    public static native void onPurchaseResponse(int i, String str, String str2, String str3);

    /* access modifiers changed from: private */
    public static native void onPurchasesRestoredResult(boolean z, String[] strArr, String[] strArr2, String[] strArr3);

    /* access modifiers changed from: private */
    public static native void onRegisterProviderResult(boolean z, String[] strArr);

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        _activity = miniclipAndroidActivity;
    }

    public static boolean registerProvider(String str, String[] strArr, String str2) {
        List asList = Arrays.asList(strArr);
        if (str.equalsIgnoreCase(GOOGLE_NAME)) {
            if (_google == null) {
                _google = new GoogleWrapper(_activity, str2);
            }
            _google.register(asList);
            return true;
        } else if (!str.equalsIgnoreCase(AMAZON_NAME)) {
            return false;
        } else {
            if (_amazon == null) {
                _amazon = new AmazonWrapper(_activity);
            }
            _amazon.register(asList);
            return true;
        }
    }

    public static String getItemPrice(String str) {
        if (_google != null) {
            return getItemPrice(GOOGLE_NAME, str);
        }
        return _amazon != null ? getItemPrice(AMAZON_NAME, str) : "";
    }

    public static long getItemPriceAmountMicros(String str) {
        if (_google != null) {
            return getItemPriceAmountMicros(GOOGLE_NAME, str);
        }
        if (_amazon != null) {
            return getItemPriceAmountMicros(AMAZON_NAME, str);
        }
        return 0;
    }

    public static String getItemCurrencyCode(String str) {
        if (_google != null) {
            return getItemCurrencyCode(GOOGLE_NAME, str);
        }
        return _amazon != null ? getItemCurrencyCode(AMAZON_NAME, str) : "";
    }

    public static void requestPurchase(String str, boolean z) {
        if (_google != null) {
            _google.requestPurchase(str, z);
        } else if (_amazon != null) {
            _amazon.requestPurchase(str, z);
        }
    }

    public static void restorePurchases() {
        if (_google != null) {
            _google.restorePurchases();
        } else if (_amazon != null) {
            _amazon.restorePurchases();
        }
    }

    public static void refreshPurchases() {
        if (_google != null) {
            _google.refreshPurchases();
        } else if (_amazon != null) {
            _amazon.refreshPurchases();
        }
    }

    public static void requestPendingPurchases() {
        if (_google != null) {
            _google.requestPendingPurchases();
        } else if (_amazon != null) {
            _amazon.requestPendingPurchases();
        }
    }

    public static void finishPurchase(String str) {
        if (_google != null) {
            _google.finishPurchase(str);
        } else if (_amazon != null) {
            _amazon.finishPurchase(str);
        }
    }

    public static boolean isItemOwned(String str) {
        if (_google != null) {
            return isItemOwned(GOOGLE_NAME, str);
        }
        if (_amazon != null) {
            return isItemOwned(AMAZON_NAME, str);
        }
        return false;
    }

    public static String[] getPurchaseReceipt(String str) {
        if (_google != null) {
            return _google.getPurchaseReceipt(str);
        }
        if (_amazon != null) {
            return _amazon.getPurchaseReceipt(str);
        }
        return null;
    }

    public static String getItemPrice(String str, String str2) {
        return _activity.getSharedPreferences(String.format(ITEM_PRICES_FORMAT, str), 0).getString(str2, "");
    }

    public static void setItemPrice(String str, String str2, String str3) {
        SharedPreferences.Editor edit = _activity.getSharedPreferences(String.format(ITEM_PRICES_FORMAT, str), 0).edit();
        edit.putString(str2, str3);
        edit.commit();
    }

    public static long getItemPriceAmountMicros(String str, String str2) {
        return _activity.getSharedPreferences(String.format(ITEM_PRICEAMOUNTMICROS_FORMAT, str), 0).getLong(str2, 0);
    }

    public static void setItemPriceAmountMicros(String str, String str2, long j) {
        SharedPreferences.Editor edit = _activity.getSharedPreferences(String.format(ITEM_PRICEAMOUNTMICROS_FORMAT, str), 0).edit();
        edit.putLong(str2, j);
        edit.commit();
    }

    public static String getItemCurrencyCode(String str, String str2) {
        return _activity.getSharedPreferences(String.format(ITEM_CURRENCYCODE_FORMAT, str), 0).getString(str2, "");
    }

    public static void setItemCurrencyCode(String str, String str2, String str3) {
        SharedPreferences.Editor edit = _activity.getSharedPreferences(String.format(ITEM_CURRENCYCODE_FORMAT, str), 0).edit();
        edit.putString(str2, str3);
        edit.commit();
    }

    public static boolean isItemOwned(String str, String str2) {
        return _activity.getSharedPreferences(String.format(OWNED_ITEMS_FORMAT, str), 0).getBoolean(str2, false);
    }

    public static void setItemOwned(String str, String str2, boolean z) {
        SharedPreferences.Editor edit = _activity.getSharedPreferences(String.format(OWNED_ITEMS_FORMAT, str), 0).edit();
        edit.putBoolean(str2, z);
        edit.commit();
    }

    public static void signalRegisterProviderResult(final boolean z, final String[] strArr) {
        _activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCInAppPurchases.onRegisterProviderResult(z, strArr);
            }
        });
    }

    public static void signalPurchaseResponse(final int i, final String str, final String str2, final String str3) {
        _activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCInAppPurchases.onPurchaseResponse(i, str, str2, str3);
            }
        });
    }

    public static void signalPurchasesRestoredResult(final boolean z, final String[] strArr, final String[] strArr2, final String[] strArr3) {
        _activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCInAppPurchases.onPurchasesRestoredResult(z, strArr, strArr2, strArr3);
            }
        });
    }
}
