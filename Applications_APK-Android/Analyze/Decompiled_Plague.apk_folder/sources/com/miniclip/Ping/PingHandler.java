package com.miniclip.Ping;

import android.os.Handler;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.nativeJNI.CocoJNI;
import com.miniclip.nativeJNI.cocojava;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class PingHandler {
    private Handler _handler = new Handler();
    /* access modifiers changed from: private */
    public int _status = 0;

    /* access modifiers changed from: private */
    public boolean simplePing(String str, int i) throws IOException {
        InetAddress byName = InetAddress.getByName(str);
        try {
            new Socket(byName, 53).close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return byName.isReachable(i);
        }
    }

    /* access modifiers changed from: private */
    public void returnResult(final int i, final boolean z) {
        cocojava.mContext.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                CocoJNI.MsimplePingResponce(i, z ^ true ? 1 : 0);
            }
        });
    }

    public void simplePingAsync(final String str, final int i, final int i2) {
        if (this._status == 0) {
            this._status = 1;
            new Thread() {
                public void run() {
                    try {
                        boolean access$000 = PingHandler.this.simplePing(str, i2);
                        if (PingHandler.this._status == 1) {
                            int unused = PingHandler.this._status = 0;
                            PingHandler.this.returnResult(i, access$000);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        if (PingHandler.this._status == 1) {
                            int unused2 = PingHandler.this._status = 0;
                            PingHandler.this.returnResult(i, false);
                        }
                    }
                }
            }.start();
            this._handler.postDelayed(new Runnable() {
                public void run() {
                    if (PingHandler.this._status == 1) {
                        int unused = PingHandler.this._status = 0;
                        PingHandler.this.returnResult(i, false);
                    }
                }
            }, (long) i2);
        }
    }
}
