package com.miniclip.info;

import com.miniclip.framework.Miniclip;

public class ScreenInfo {
    public static float screenWidth() {
        return (float) Miniclip.getActivity().getResources().getDisplayMetrics().widthPixels;
    }

    public static float screenHeight() {
        return (float) Miniclip.getActivity().getResources().getDisplayMetrics().heightPixels;
    }

    public static float pixelsPerInch() {
        return Miniclip.getActivity().getResources().getDisplayMetrics().ydpi;
    }

    public static float windowWidth() {
        return (float) Miniclip.getActivity().findViewById(16908290).getWidth();
    }

    public static float windowHeight() {
        return (float) Miniclip.getActivity().findViewById(16908290).getHeight();
    }
}
