package com.miniclip.info;

import android.os.Build;
import java.util.Locale;

public class DeviceInfo {
    public static String model() {
        return Build.DEVICE;
    }

    public static String operatingSystemVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String brandName() {
        return Build.BRAND;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String systemLanguage() {
        if (Build.VERSION.SDK_INT >= 21) {
            return Locale.getDefault().toLanguageTag();
        }
        return Locale.getDefault().toString().replace('_', '-');
    }

    public static String country() {
        return Locale.getDefault().getCountry();
    }
}
