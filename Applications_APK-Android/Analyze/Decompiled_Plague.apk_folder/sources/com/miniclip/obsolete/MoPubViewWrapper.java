package com.miniclip.obsolete;

import android.content.Context;
import android.util.AttributeSet;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;

public class MoPubViewWrapper extends MoPubView {
    private boolean mAdLoaded;

    public MoPubViewWrapper(Context context) {
        this(context, null);
    }

    public MoPubViewWrapper(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mAdLoaded = false;
    }

    public boolean AdLoaded() {
        return this.mAdLoaded;
    }

    /* access modifiers changed from: protected */
    public void adLoaded() {
        this.mAdLoaded = true;
        super.adLoaded();
    }

    /* access modifiers changed from: protected */
    public void adFailed(MoPubErrorCode moPubErrorCode) {
        this.mAdLoaded = false;
        super.adFailed(moPubErrorCode);
    }
}
